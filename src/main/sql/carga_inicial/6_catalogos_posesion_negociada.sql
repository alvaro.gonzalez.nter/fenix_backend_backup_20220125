INSERT INTO `LKUP_TIPO_OCUPANTE` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'TIT', 'Titular anterior');
INSERT INTO `LKUP_TIPO_OCUPANTE` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, 'DEU', 'Deudor');

INSERT INTO `LKUP_ESTADO_OCUPACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'OCL', 'Ocupación legal');
INSERT INTO `LKUP_ESTADO_OCUPACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, 'OCI', 'Ocupación ilegal');
INSERT INTO `LKUP_ESTADO_OCUPACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (3, 1, 'VAC', 'Vacío');
INSERT INTO `LKUP_ESTADO_OCUPACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (4, 1, 'PEN', 'Pendiente de informar');

INSERT INTO `LKUP_ESTADO_EXP_POSESION_NEGOCIADA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'ACT', 'Activo');
INSERT INTO `LKUP_ESTADO_EXP_POSESION_NEGOCIADA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, 'REV', 'En revisión');
INSERT INTO `LKUP_ESTADO_EXP_POSESION_NEGOCIADA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (3, 1, 'CER', 'Cerrado');

INSERT INTO `LKUP_ESTIMACION_CUMPLIDA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'SI', 'Cumplida');
INSERT INTO `LKUP_ESTIMACION_CUMPLIDA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, 'NO', 'No cumplida');
INSERT INTO `LKUP_ESTIMACION_CUMPLIDA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (3, 1, 'PTE', 'Pendiente');

INSERT INTO `LKUP_ORIGEN_EXPEDIENTE_POSESION_NEGOCIADA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'AMI', 'Amistoso');
INSERT INTO `LKUP_ORIGEN_EXPEDIENTE_POSESION_NEGOCIADA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, 'POS', 'Posesión negociada');

INSERT INTO `LKUP_TIPO_PROPUESTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (8, 1, 'DP', 'De posesión');

INSERT INTO `LKUP_SUBTIPO_PROPUESTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`, `IND_FORMALIZACION`, `IND_PBC`, `ID_TIPO_PROPUESTA`) VALUES (32, 1, 'DP1', 'Sin contraprestación', 1, 0, 8);
INSERT INTO `LKUP_SUBTIPO_PROPUESTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`, `IND_FORMALIZACION`, `IND_PBC`, `ID_TIPO_PROPUESTA`) VALUES (33, 1, 'DP2', 'Con condonación parcial', 1, 0, 8);
INSERT INTO `LKUP_SUBTIPO_PROPUESTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`, `IND_FORMALIZACION`, `IND_PBC`, `ID_TIPO_PROPUESTA`) VALUES (34, 1, 'DP3', 'Con condonación total', 1, 0, 8);
INSERT INTO `LKUP_SUBTIPO_PROPUESTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`, `IND_FORMALIZACION`, `IND_PBC`, `ID_TIPO_PROPUESTA`) VALUES (35, 1, 'DP4', 'Con condonación total e indemnización', 1, 0, 8);
INSERT INTO `LKUP_SUBTIPO_PROPUESTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`, `IND_FORMALIZACION`, `IND_PBC`, `ID_TIPO_PROPUESTA`) VALUES (36, 1, 'DP5', 'Con indemnización', 1, 0, 8);
INSERT INTO `LKUP_SUBTIPO_PROPUESTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`, `IND_FORMALIZACION`, `IND_PBC`, `ID_TIPO_PROPUESTA`) VALUES (37, 1, 'DP6', 'Con condonación total y alquiler', 1, 0, 8);
INSERT INTO `LKUP_SUBTIPO_PROPUESTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`, `IND_FORMALIZACION`, `IND_PBC`, `ID_TIPO_PROPUESTA`) VALUES (38, 1, 'DP7', 'Con alquiler', 1, 0, 8);

INSERT INTO `LKUP_NIVEL_EVENTO` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (7, 1, 'EXP_PN', 'Expediente (Posesión negociada)');
INSERT INTO `LKUP_NIVEL_EVENTO` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (8, 1, 'BIEN_PN', 'Bien (Posesión negociada)');

INSERT INTO `LKUP_TRATAMIENTO_REO` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'R', 'REM');
INSERT INTO `LKUP_TRATAMIENTO_REO` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, 'NR', 'No REM');
INSERT INTO `LKUP_TRATAMIENTO_REO` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (3, 1, 'SR', 'Si REM');


INSERT INTO `LKUP_FACTURACION_CARTERA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'C', 'Cobro');
INSERT INTO `LKUP_FACTURACION_CARTERA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, 'R', 'Refinanciación');
INSERT INTO `LKUP_FACTURACION_CARTERA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (3, 1, 'P', 'Propuesta');

INSERT INTO `LKUP_REO_ORIGEN` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'A', 'Adjudicacion Judicial');
INSERT INTO `LKUP_REO_ORIGEN` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, 'D', 'Dación');
INSERT INTO `LKUP_REO_ORIGEN` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (3, 1, 'C', 'Compraventa');

INSERT INTO `LKUP_TIPO_ACCION_PROCEDIMIENTO` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'S1', 'solicitud inicio reclamación judicial');
INSERT INTO `LKUP_RESULTADO_VISITA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'TEST', 'TEST');

INSERT INTO `LKUP_RESULTADO_SUBASTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, '01', 'Pendiente');
INSERT INTO `LKUP_RESULTADO_SUBASTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, '02', 'Realizada');
INSERT INTO `LKUP_RESULTADO_SUBASTA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (3, 1, '03', 'Suspendida');
