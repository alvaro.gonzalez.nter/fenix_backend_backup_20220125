INSERT INTO LKUP_CRITERIO_CONTRATO_REPRESENTANTE (ID, IND_ACTIVO, DES_CODIGO, DES_VALOR) VALUES(NULL, 1, 'producto', 'Producto del contrato');
INSERT INTO LKUP_CRITERIO_CONTRATO_REPRESENTANTE (ID, IND_ACTIVO, DES_CODIGO, DES_VALOR) VALUES(NULL, 1, 'ant-deuda', 'Antigüedad de la deuda del contrato');
INSERT INTO LKUP_CRITERIO_CONTRATO_REPRESENTANTE (ID, IND_ACTIVO, DES_CODIGO, DES_VALOR) VALUES(NULL, 1, 'deuda', 'Deuda impagada del contrato');
INSERT INTO LKUP_CRITERIO_CONTRATO_REPRESENTANTE (ID, IND_ACTIVO, DES_CODIGO, DES_VALOR) VALUES(NULL, 1, 'riesgo', 'Riesgo total del contrato');
INSERT INTO LKUP_CRITERIO_CONTRATO_REPRESENTANTE (ID, IND_ACTIVO, DES_CODIGO, DES_VALOR) VALUES(NULL, 1, 'num', 'Numeración del contrato');

INSERT INTO LKUP_CRITERIO_GENERACION_EXPEDIENTE (ID, IND_ACTIVO, DES_CODIGO, DES_VALOR) VALUES(NULL, 1, 'heredado', 'Expediente heredado');
INSERT INTO LKUP_CRITERIO_GENERACION_EXPEDIENTE (ID, IND_ACTIVO, DES_CODIGO, DES_VALOR) VALUES(NULL, 1, 'cliente', 'Expediente visión cliente');
INSERT INTO LKUP_CRITERIO_GENERACION_EXPEDIENTE (ID, IND_ACTIVO, DES_CODIGO, DES_VALOR) VALUES(NULL, 1, 'contrato', 'Un expediente por contrato');

INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '10', 'Hipoteca');
INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '20', 'Préstamo promotor');
INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '21', 'Préstamo personal');
INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '22', 'Préstamo consumo');
INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '30', 'Tarjeta');
INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '40', 'Descubierto');
INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '50', 'Pólizas');
INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '60', 'Leasing');
INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '70', 'Renting');
INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '80', 'Aval');
INSERT INTO LKUP_PRODUCTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '99', 'Otros');

INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '01', 'amortización constante');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '02', 'amortización progresiva');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '03', 'cuota irregular');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '04', 'al vencimiento');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '05', 'cuota constante');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '06', 'cuota constante extras');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '07', 'cuota progresiva');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '08', 'cuota anticipada');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '09', 'cuota mixta');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '10', 'constante con pago final');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '99', 'Otros');
INSERT INTO LKUP_SISTEMA_AMORTIZACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '88', 'No aplica');

INSERT INTO LKUP_PERIOCIDAD_LIQUIDACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '030', 'mensual');
INSERT INTO LKUP_PERIOCIDAD_LIQUIDACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '090', 'trimestral');
INSERT INTO LKUP_PERIOCIDAD_LIQUIDACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '120', 'semestra');
INSERT INTO LKUP_PERIOCIDAD_LIQUIDACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '360', 'anual');
INSERT INTO LKUP_PERIOCIDAD_LIQUIDACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '999', 'irregular');
INSERT INTO LKUP_PERIOCIDAD_LIQUIDACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '888', 'No aplica');

INSERT INTO LKUP_INDICE_REFERENCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'E6M', 'Euribor 6m');
INSERT INTO LKUP_INDICE_REFERENCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'E1A', 'Euribor 1a');
INSERT INTO LKUP_INDICE_REFERENCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'M1A', 'Mibor');
INSERT INTO LKUP_INDICE_REFERENCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '888', 'No aplica');
INSERT INTO LKUP_INDICE_REFERENCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '999', 'OTROS');

INSERT INTO LKUP_TIPO_REESTRUCTURACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'Carencia capital');
INSERT INTO LKUP_TIPO_REESTRUCTURACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '2', 'Carencia intereses');
INSERT INTO LKUP_TIPO_REESTRUCTURACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '3', 'Refinanciación');
INSERT INTO LKUP_TIPO_REESTRUCTURACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '4', 'Reestructuración');
INSERT INTO LKUP_TIPO_REESTRUCTURACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '8', 'No aplica');
INSERT INTO LKUP_TIPO_REESTRUCTURACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '9', 'Otros');

INSERT INTO LKUP_CLASIFICACION_CONTABLE (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0', 'Normal');
INSERT INTO LKUP_CLASIFICACION_CONTABLE (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'Impagado');
INSERT INTO LKUP_CLASIFICACION_CONTABLE (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '3', 'Dudoso');
INSERT INTO LKUP_CLASIFICACION_CONTABLE (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '4', 'Fallido');
INSERT INTO LKUP_CLASIFICACION_CONTABLE (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '9', 'Subestandar');
INSERT INTO LKUP_CLASIFICACION_CONTABLE (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '88', 'No aplica');
INSERT INTO LKUP_CLASIFICACION_CONTABLE (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '99', 'Otros');

INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'TITULAR');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '3', 'AVALISTA');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '18', 'HIPOTECANTE NO DEUDOR');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '16', 'TUTOR');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '15', 'REPR. LEGAL');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '8', 'CORREDOR COMERCIO');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '10', 'ORGANISMO DE SUBVENCIÓN');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '7', 'NOTARIO');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '5', 'APODERADO');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '4', 'AVALISTA NO SOLIDARIO');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '24', 'USUFRUCTUARIO');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '28', 'APODERADO DEL AVALISTA');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '29', 'APODERADO AVALISTA NO SOLIDARIO');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '31', 'APOD. DEL BENEFICIA');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '34', 'APOD. HIPOTECANT');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '35', 'APOD. DE AVAL. SUBSI');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '37', 'ASEGURADO');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '65', 'PIGNO.N.DE');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '66', 'APO.PIGNOD');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '69', 'BANCO AGENTE');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '70', 'ENT. GESTORA');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '71', 'ENT. CEDENTE');
INSERT INTO LKUP_TIPO_INTERVENCION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '72', 'ASEGURADOR');

INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '01', 'C.I.F. NACIONAL');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '02', 'C.I.F. EXTRANJERO');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '03', 'C.I.F. PROVISIONAL');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '04', 'NUMERO CSB (OFI.C.R)');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '05', 'DESCONOCIDO (INTERF)');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '06', 'CODIGO DE SWIFT');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '07', 'COD.NRBE DE UNA E.B');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '08', 'OTRAS (P. JURIDICAS)');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '09', 'DELEGACION AEAT');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '10', 'REFERENCIA NOMINA');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '11', 'REFERENCIA EMISOR');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '12', 'NUMERO PARTICIPE JUR');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '13', 'RGA TOGA');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '14', 'OFICINA INTEGRADA');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '15', 'ASOCIADO A CAPITAL');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '16', 'C.I.F.EXTRANJERO DE');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '17', 'CODIGO FISCAL');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '51', 'N.I.F. NACIONAL');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '52', 'PASAPORTE');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '53', 'NIE');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '54', 'ESP. NO RESI.SIN NIF');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '55', '< 14 NIF PROV. AEAT');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '56', 'DESCON. (INTER.FIS.)');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '57', 'NUMERO SEG. SOCIAL');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '58', 'PERMISO DE CONDUCIR');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '59', 'D.N.I.');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '60', 'OTRAS (P. FISICAS)');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '61', 'REFERENCIA NOMINA');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '62', 'REFERENCIA EMISOR');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '63', 'MATRICULA EMPLEADO');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '64', 'NUMERO PARTICIPE FIS');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '65', 'N.I.P. TOGA');
INSERT INTO LKUP_TIPO_DOCUMENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '70', 'MATRICULA AUTOMOVIL');

INSERT INTO LKUP_TIPO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '01', 'Indefinido');
INSERT INTO LKUP_TIPO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '02', 'Temporal');
INSERT INTO LKUP_TIPO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '03', 'Por Obra o Servicio');
INSERT INTO LKUP_TIPO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '04', 'Eventual');
INSERT INTO LKUP_TIPO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '05', 'De Interinidad');
INSERT INTO LKUP_TIPO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '06', 'De Relevo');
INSERT INTO LKUP_TIPO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '07', 'De Formación y Aprendiza');
INSERT INTO LKUP_TIPO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '08', 'En Prácticas');

INSERT INTO LKUP_VULNERABILIDAD (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'Sentencia malos tratos');
INSERT INTO LKUP_VULNERABILIDAD (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '2', 'Enfermedad');
INSERT INTO LKUP_VULNERABILIDAD (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '3', 'Otros');

INSERT INTO LKUP_ENTORNO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '01', 'Barrio');
INSERT INTO LKUP_ENTORNO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '02', 'Extrarradio');
INSERT INTO LKUP_ENTORNO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '03', 'Polígono');
INSERT INTO LKUP_ENTORNO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '04', 'Rural');
INSERT INTO LKUP_ENTORNO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '05', 'Aislado');
INSERT INTO LKUP_ENTORNO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '88', 'No aplica');
INSERT INTO LKUP_ENTORNO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '99', 'Otros');

INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '01', 'ARABA/ALAVA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '02', 'ALBACETE');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '03', 'ALICANTE');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '04', 'ALMERIA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '33', 'ASTURIAS');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '05', 'AVILA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '06', 'BADAJOZ');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '07', 'BALEARES');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '08', 'BARCELONA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '48', 'BIZKAIA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '09', 'BURGOS');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '10', 'CACERES');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '11', 'CADIZ');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '39', 'CANTABRIA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '12', 'CASTELLON DE LA PLANA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '51', 'CEUTA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '13', 'CIUDAD REAL');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '14', 'CORDOBA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '15', 'CORUÑA (A)');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '16', 'CUENCA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '99', 'DESCONOCIDA (CARGA)');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '20', 'GIPUZKOA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '17', 'GIRONA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '18', 'GRANADA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '19', 'GUADALAJARA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '21', 'HUELVA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '22', 'HUESCA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '23', 'JAEN');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '24', 'LEON');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '25', 'LLEIDA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '27', 'LUGO');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '28', 'MADRID');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '29', 'MALAGA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '52', 'MELILLA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '30', 'MURCIA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '31', 'NAVARRA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '32', 'OURENSE');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '34', 'PALENCIA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '35', 'PALMAS (LAS)');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '36', 'PONTEVEDRA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '26', 'RIOJA (LA)');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '37', 'SALAMANCA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '38', 'SANTA CRUZ DE TENERIFE');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '40', 'SEGOVIA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '41', 'SEVILLA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '42', 'SORIA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '43', 'TARRAGONA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '44', 'TERUEL');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '45', 'TOLEDO');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '46', 'VALENCIA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '47', 'VALLADOLID');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '49', 'ZAMORA');
INSERT INTO LKUP_PROVINCIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '50', 'ZARAGOZA');

INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '001', 'FRANCIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '002', 'BELGICA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '003', 'PAISES BAJOS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '004', 'ALEMANIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '005', 'ITALIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '006', 'REINO UNIDO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '007', 'IRLANDA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '008', 'DINAMARCA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '009', 'GRECIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '010', 'PORTUGAL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '011', 'ESPAÑA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '017', 'BELGICA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '018', 'LUXEMBURGO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '024', 'ISLANDIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '027', 'SVALBARD Y JAN MAYEN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '028', 'NORUEGA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '030', 'SUECIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '032', 'FINLANDIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '036', 'SUIZA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '037', 'LIECHTENSTEIN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '038', 'AUSTRIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '039', 'SUIZA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '041', 'FEROE, ISLAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '043', 'ANDORRA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '044', 'GIBRALTAR');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '045', 'VATICANO, CIUDAD DEL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '046', 'MALTA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '047', 'SAN MARINO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '052', 'TURQUIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '053', 'ESTONIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '054', 'LETONIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '055', 'LITUANIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '060', 'POLONIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '061', 'CHECA, REPÚBLICA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '063', 'ESLOVAQUIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '064', 'HUNGRIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '066', 'RUMANIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '068', 'BULGARIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '070', 'ALBANIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '072', 'UCRANIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '073', 'BIELORRUSIA (BELARÚS)');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '074', 'MOLDAVIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '075', 'RUSIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '076', 'GEORGIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '077', 'ARMENIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '078', 'AZERBAIYAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '079', 'KAZAJISTAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '080', 'TURKMENISTAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '081', 'UZBEKISTAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '082', 'TAYIKISTAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '083', 'KIRGUISTAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '091', 'ESLOVENIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '092', 'CROACIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '093', 'BOSNIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '094', 'SERBIA Y MONTENEGRO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '096', 'MACEDONIA (Antigua República Yugoslava)');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '097', 'MONTENEGRO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '098', 'SERBIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '099', 'KOSOVO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '101', 'MONACO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '103', 'ISLAS ANGLONORMANDAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '104', 'ISLAS MAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '107', 'ISLAS COOK');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '108', 'LUXEMBURGO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '204', 'MARRUECOS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '208', 'ARGELIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '212', 'TUNEZ');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '216', 'LIBIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '220', 'EGIPTO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '224', 'SUDAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '228', 'MAURITANIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '232', 'MALI');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '236', 'BURKINA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '240', 'NIGER');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '244', 'CHAD');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '247', 'REPUBLICA DE CABO VERDE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '248', 'SENEGAL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '252', 'GAMBIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '257', 'GUINEA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '260', 'GUINEA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '264', 'SIERRA LEONA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '268', 'LIBERIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '272', 'COSTA DE MARFIL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '276', 'GHANA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '280', 'TOGO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '284', 'BENIN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '288', 'NIGERIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '302', 'CAMERUN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '306', 'R. CENTRO AFRICANA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '310', 'GUINEA ECUATORIAL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '311', 'SANTO TOME Y PRINCIPE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '314', 'GABON');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '318', 'CONGO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '322', 'REP.DEM. CONGO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '324', 'RUANDA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '328', 'BURUNDI');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '329', 'SANTA HELENA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '330', 'ANGOLA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '334', 'ETIOPIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '336', 'ERITREA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '338', 'DJIBOUTI');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '342', 'REPUBLICA FEDERAL DE SOMALIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '346', 'KENYA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '350', 'UGANDA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '352', 'TANZANIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '355', 'SEYCHELLES');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '357', 'TERRITORIO BRITÁNICO O.INDICO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '366', 'MOZAMBIQUE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '370', 'MADAGASCAR');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '372', 'REUNION');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '373', 'MAURICIO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '375', 'COMORES');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '377', 'MAYOTE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '378', 'ZAMBIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '382', 'ZIMBABWE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '386', 'MALAWI');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '388', 'SUDAFRICA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '389', 'NAMIBIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '391', 'BOTSWANA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '392', 'SAHARA OCCIDENTAL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '393', 'SWAZILANDIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '395', 'LESOTHO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '400', 'ESTADOS UNIDOS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '401', 'PUERTO RICO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '404', 'CANADA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '406', 'GROENLANDIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '408', 'SAN PEDRO Y MIQUELON');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '410', 'COREA DEL SUR');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '412', 'MEXICO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '413', 'BERMUDAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '416', 'GUATEMALA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '421', 'BELIZE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '424', 'HONDURAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '428', 'EL SALVADOR');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '432', 'NICARAGUA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '436', 'COSTA RICA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '442', 'PANAMA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '446', 'ANGUILLA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '448', 'CUBA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '449', 'SAN CRISTOBAL Y NEVIS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '452', 'HAITI');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '453', 'BAHAMAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '454', 'ISLAS TURQUESAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '456', 'REPUBLICA DOMINICANA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '457', 'ISLAS VIRGENES EEUU');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '458', 'GUADALUPE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '459', 'ANTIGUA Y BARBUDA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '460', 'DOMINICA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '462', 'MARTINICA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '463', 'ISLAS CAYMAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '464', 'JAMAICA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '465', 'SANTA LUCIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '467', 'SAN VICENTE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '468', 'ISLAS VIRGENES BRITANICAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '469', 'BARBADOS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '470', 'MONSERRAT');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '472', 'TRINIDAD Y TOBAGO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '473', 'GRANADA, ISLA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '474', 'ARUBA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '476', 'CURAÇAO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '477', 'SAN MARTIN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '478', '**EXTINGUIDO* ANTILLAS NEERLAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '479', 'ISLAS BONAIRE S.EUSTAQUIO SABA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '480', 'COLOMBIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '484', 'VENEZUELA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '488', 'GUYANA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '492', 'SURINAM');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '496', 'GUAYANA FRANCESA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '500', 'ECUADOR');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '504', 'PERU');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '508', 'BRASIL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '512', 'CHILE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '516', 'BOLIVIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '520', 'PARAGUAY');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '524', 'URUGUAY');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '528', 'ARGENTINA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '529', 'ISLAS MALVINAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '531', 'CURAÇAO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '534', 'SAN MARTÍN (parte meridional)');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '535', 'BONAIRE, SAN EUSTAQUIO Y SABA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '600', 'CHIPRE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '604', 'LIBANO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '608', 'SIRIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '612', 'IRAQ');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '616', 'IRAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '624', 'ISRAEL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '625', 'AUTONOMIA PALESTINA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '626', 'TIMOR LESTE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '628', 'JORDANIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '630', 'PUERTO RICO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '632', 'ARABIA SAUDI');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '636', 'KUWAIT');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '640', 'BAHREIN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '644', 'QATAR');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '647', 'EMIRATOS ARABES');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '649', 'OMAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '652', 'SAN BARTOLOMÉ');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '653', 'YEMEN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '660', 'AFGANISTAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '662', 'PAKISTAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '664', 'INDIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '666', 'BANGLADESH');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '667', 'MALDIVAS (ISLAS)');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '669', 'SRI LANKA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '672', 'NEPAL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '675', 'BUTAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '676', 'MYANMAR (BIRMANIA)');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '680', 'THAILANDIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '684', 'LAOS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '686', 'ĊLAND, ISLAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '690', 'VIETNAM');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '696', 'KAMPUCHEA (CAMBOYA)');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '700', 'INDONESIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '701', 'MALASIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '703', 'BRUNEI');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '706', 'SINGAPUR');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '708', 'FILIPINAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '716', 'MONGOLIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '720', 'CHINA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '724', 'COREA DEL NORTE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '728', 'COREA DEL SUR');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '729', 'SUDAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '732', 'JAPON');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '736', 'TAIWAN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '740', 'HONG');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '743', 'MACAO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '750', 'TIMOR LESTE / ORIENTAL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '777', 'JERSEY');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '778', 'GUERNSEY');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '779', 'SAMOA AMERICANA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '780', 'ISLAS MENORES EE.UU.');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '781', 'ISLAS SANDWICH SUR');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '782', 'ISLAS BOUVET');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '783', 'TIERRAS AUSTRALES FRANCESAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '800', 'AUSTRALIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '801', 'EST. INDEP. PAPUA NUEVA GUINEA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '802', 'OCEANIA AUSTRALIANA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '803', 'NAURU');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '804', 'NUEVA ZELANDA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '806', 'ISLAS SALOMON');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '807', 'TUVALU');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '809', 'NUEVA CALEDONIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '810', 'GUAM (OCEANIA AMERICANA)');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '811', 'ISLAS WALLIS Y FORTUNA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '812', 'KIRIBATI');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '813', 'ISLAS PITCAIRN');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '814', 'OCEANIA NEOZELANDESA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '815', 'FIJI');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '816', 'VANUATU');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '817', 'TONGA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '819', 'SAMOA OCCIDENTAL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '820', 'ISLAS MARIANAS DEL NORTE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '822', 'POLINESIA FRANCESA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '823', 'FEDERACION ESTADOS MICRONESIA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '824', 'ISLAS MARSHALL');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '825', 'PALAU');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '830', 'SAMOA AMERICANA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '831', 'GUAM');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '832', 'MENORES ALEJADAS DE LOS EE.UU,ISLAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '833', 'COCOS (KEELING), ISLAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '834', 'NAVIDAD (CHRISTMAS), ISLA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '835', 'HEARD Y MCDONALD, ISLAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '836', 'NORFOLK, ISLA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '837', 'COOK, ISLAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '838', 'NIUE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '839', 'TOKELAU');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '854', 'COCOS (ISLA) (ISLAS KEELING)');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '855', 'ISLA HERARD E ISLAS MCDONALD');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '856', 'NAVIDAD (ISLA)');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '859', 'TOKELAU');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '890', 'REGIONES POLARES');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '891', 'ANTARTIDA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '892', 'BOUVET, ISLA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '893', 'GEORGIA DEL SUR Y LAS ISLAS SANDWICH DEL SUR');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '894', 'TIERRAS AUSTRALES FRANCESAS');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '900', 'FONDO EUROPEO INVERSIONES');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '910', 'INSTITUCIONES DE LA UNION EUROPEA');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '920', 'ORG. INTERN. DIST. DE LAS INST DE UE Y BCE');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '921', 'ORGANISMOS INTERNACIONALES');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '930', 'BANCO CENTRAL EUROPEO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '957', 'NO DETERMINADO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '958', 'PAIS FICTICIO PARA OPERATIVA EXTRANJERO');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '972', 'JERSEY');
INSERT INTO LKUP_PAIS (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '990', 'DESCONOCIDO');

INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'AC', 'ACEQUIA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'AD', 'ALDEA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'AG', 'AGREGADO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'AL', 'ALAMEDA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'AP', 'APARTAMENTO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'AR', 'AREA,ARRABAL');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'AT', 'APTDO. CORREOS');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'AU', 'AUTOMOVIL');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'AV', 'AVENIDA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'AY', 'ARROYO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'BA', 'BARCO,NAVE');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'BD', 'BARRIADA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'BJ', 'BAJADA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'BL', 'BLOQUE');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'BO', 'BARRIO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'BR', 'BARRANCO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CA', 'CARRIL');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CD', 'CORREGIDOR');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CG', 'COLEGIO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CH', 'CHALET');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CI', 'CORTIJO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CJ', 'CALLEJA/JON');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CL', 'CALLE');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CM', 'CAMINO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CN', 'CONJUNTO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CO', 'COLONIA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CP', 'CAMPA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CR', 'CARRETERA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CS', 'CASERIO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CT', 'CUESTA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CV', 'CUEVA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'DP', 'DIPUTACION');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'DS', 'DISEMINADOR');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'ED', 'EDIFICIO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'EJ', 'EXTRANJERO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'EM', 'EXTRAMUROS');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'EN', 'ENTRADA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'ER', 'EXTRARRADIO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'ES', 'ESCALINATA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'EX', 'EXPLANADA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'FC', 'FERROCARRIL');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'GA', 'GALERIA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'GL', 'GLORIETA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'GR', 'GRUPO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'GV', 'GRAN VIA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'HT', 'HUERTA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'JR', 'JARDINES');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'KM', 'KILOMETRO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'LD', 'LADO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'LG', 'LUGAR');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'MC', 'MERCADO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'ML', 'MUELLE');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'MN', 'MUNICIPIO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'MS', 'MASIA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'MT', 'MONTE');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'MZ', 'MANZANA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PA', 'PARAJE');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PB', 'POBLADO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PC', 'PARCELA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PD', 'PARTIDA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PE', 'PARQUE');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PG', 'POLIGONO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PJ', 'PASAJE');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PL', 'PLAZUELA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PQ', 'PARROQUIA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PR', 'PROLONGACION');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PS', 'PASEO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PT', 'PUENTE');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PU', 'PUERTA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PZ', 'PLAZA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'QT', 'QUINTA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'RB', 'RAMBLA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'RC', 'RINCON');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'RD', 'RONDA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'RM', 'RAMAL');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'RP', 'RAMPA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'RR', 'RIERA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'RS', 'RESIDENCIAL');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'RU', 'RUA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'SA', 'SALIDA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'SB', 'SUBIDA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'SD', 'SENDA,SENDERO');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'SL', 'SOLAR,SALON');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'TN', 'TERRENOS');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'TO', 'TORRENTE');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'TR', 'TRAVESIA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'UR', 'URBANIZACION');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'VI', 'VIA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'VP', 'VIA PUBLICA');
INSERT INTO LKUP_TIPO_VIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'VV', 'VIVIENDAS');

INSERT INTO LKUP_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'Suelo');
INSERT INTO LKUP_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '2', 'Vivienda');
INSERT INTO LKUP_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '3', 'Comercial y terciario');
INSERT INTO LKUP_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '4', 'Industrial');
INSERT INTO LKUP_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '5', 'Edificio completo');
INSERT INTO LKUP_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '6', 'En construcción');
INSERT INTO LKUP_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '8', 'Dotacional');
INSERT INTO LKUP_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '9', 'Concesión Administrativa');
INSERT INTO LKUP_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '88', 'No aplica');
INSERT INTO LKUP_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '99', 'Otros');

INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'No urbanizable (rústico)');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '2', 'Urbanizable no programado');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '3', 'Urbanizable programado');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '4', 'Urbano (solar)');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '5', 'Unifamiliar aislada');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '6', 'Unifamiliar adosada');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '7', 'Unifamiliar pareada');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '8', 'Unifamiliar casa de pueblo');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '9', 'Piso');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '10', 'Piso dúplex');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '11', 'Ático');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '12', 'Estudio/Loft');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '13', 'Local comercial');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '14', 'Oficina');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '15', 'Almacén');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '16', 'Hotel');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '17', 'Nave aislada');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '18', 'Nave adosada');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '19', 'Aparcamiento');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '20', 'Hotel/Apartamentos turísticos');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '21', 'Oficina');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '22', 'Comercial');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '23', 'En construcción (promoción)');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '24', 'Garaje');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '25', 'Trastero');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '26', 'Otros');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '38', 'Residencia Estudiantes');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '39', 'Dotacional Privado');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '27', 'No Urbanizable');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '28', 'Parque Medianas');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '29', 'Gasolinera');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '30', 'Dotacional Deportivo');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '31', 'Dotacional Sanitario');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '32', 'Dotacional Recreativo');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '33', 'Otros Derechos');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '34', 'Dotacional Asistencial');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '35', 'Infraestructura Técnica');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '36', 'Superficie en Zona Común');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '37', 'Nave en varias plantas');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '88', 'No aplica');
INSERT INTO LKUP_SUB_TIPO_BIEN (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '99', 'Otros');

INSERT INTO LKUP_TIPO_GARANTIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'Hipotecaria');
INSERT INTO LKUP_TIPO_GARANTIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '2', 'Personal');
INSERT INTO LKUP_TIPO_GARANTIA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '3', 'Pignoratícia');

INSERT INTO LKUP_USO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '01', '1ª Residencia');
INSERT INTO LKUP_USO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '02', 'Terciario');
INSERT INTO LKUP_USO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '03', 'Industrial');
INSERT INTO LKUP_USO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '04', 'Dotacional');
INSERT INTO LKUP_USO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '05', 'Otros');
INSERT INTO LKUP_USO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '06', '2ª Residencia');
INSERT INTO LKUP_USO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '07', 'Comercial');
INSERT INTO LKUP_USO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '08', 'Rústico');

INSERT INTO LKUP_TIPO_ACTIVO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'Inmobiliario (INMUEBLE)');
INSERT INTO LKUP_TIPO_ACTIVO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0', 'Mobiliario(MUEBLE, DERECHOS DE CREDITO, OTROS BIENES,VALORES, etc)');

INSERT INTO LKUP_TIPO_CARGA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'Anterior');
INSERT INTO LKUP_TIPO_CARGA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '2', 'Posterior');
INSERT INTO LKUP_TIPO_CARGA (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '3', 'Igualdad de rango');

INSERT INTO LKUP_TIPO_ACREEDOR (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '01', 'Entidad financiera');
INSERT INTO LKUP_TIPO_ACREEDOR (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '02', 'Entes públicos');
INSERT INTO LKUP_TIPO_ACREEDOR (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '03', 'Otros');

INSERT INTO LKUP_TIPO_TASACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'Desktop');
INSERT INTO LKUP_TIPO_TASACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '2', 'Drive by');
INSERT INTO LKUP_TIPO_TASACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '3', 'Drive in');

INSERT INTO LKUP_TIPO_VALORACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'COMPARACIÓN');
INSERT INTO LKUP_TIPO_VALORACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '2', 'RESIDUAL');
INSERT INTO LKUP_TIPO_VALORACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '3', 'COSTE');
INSERT INTO LKUP_TIPO_VALORACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '4', 'ACTUALIZACIÓN');

INSERT INTO LKUP_TIPO_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '1', 'Cargo');
INSERT INTO LKUP_TIPO_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '2', 'Abono');

INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0002', 'CONSTITUCIÓN');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0003', 'CANCELACIÓN');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0004', 'CANCEL. SUBROGACION');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0005', 'FACTURACIÓN');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0013', 'PERIODIFICACIÓN');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0016', 'DISPOSICIÓN PARCIAL');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0017', 'AMORTIZACION');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0018', 'AMORTI. DIVISIÓN');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0040', 'REDUCCIÓN DEL LIMITE');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0041', 'AUMENTO DEL LIMITE');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0051', 'CANCELACIÓN DEL DISPONIBLE');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0052', 'AMORTIZACIÓN POR SUBVENCIÓN');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0068', 'CAMBIO DE MONEDA');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0069', 'BONIF. CONTRACTUAL');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0070', 'BONIF. GRACIAL');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0071', 'PERIODIFICACIÓN BONIFICACIÓN CONTRACTUAL');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0072', 'PERIODIFICACIÓN BONIFICACIÓN GRACIAL');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0163', 'GASTOS SEGURO');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0168', 'PERIOD COMISIONES CONSTITUCIÓN');
INSERT INTO LKUP_DESCRIPCION_MOVIMIENTO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '0169', 'SALDAR COMISIONES');

INSERT INTO LKUP_TIPO_OCUPACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '01', 'Pensionista');
INSERT INTO LKUP_TIPO_OCUPACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '02', 'Cuenta ajena');
INSERT INTO LKUP_TIPO_OCUPACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '88', 'Otros');
INSERT INTO LKUP_TIPO_OCUPACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '99', 'No Aplica');

INSERT INTO LKUP_TIPO_PRESTACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '01', 'Complementaria');
INSERT INTO LKUP_TIPO_PRESTACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '02', 'Desempleo');
INSERT INTO LKUP_TIPO_PRESTACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '88', 'Otros');
INSERT INTO LKUP_TIPO_PRESTACION (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, '99', 'No aplica');

INSERT INTO LKUP_ESTADO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'ACT', 'ACTIVO');
INSERT INTO LKUP_ESTADO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'REV', 'EN REVISIÓN');
INSERT INTO LKUP_ESTADO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CER', 'CERRADO');
INSERT INTO LKUP_ESTADO_CONTRATO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'PEN', 'PENDIENTE DE GESTIÓN');

INSERT INTO LKUP_ESTADO_CIVIL (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'CAS', 'CASADO');
INSERT INTO LKUP_ESTADO_CIVIL (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'DIV', 'DIVORCIADO');
INSERT INTO LKUP_ESTADO_CIVIL (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'SOL', 'SOLTERO');
INSERT INTO LKUP_ESTADO_CIVIL (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'VID', 'VIUDO');
INSERT INTO LKUP_ESTADO_CIVIL (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'SEP', 'SEPARADO');

INSERT INTO LKUP_SEXO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'V', 'Varón');
INSERT INTO LKUP_SEXO (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'M', 'Mujer');

INSERT INTO LKUP_LIQUIDEZ (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'A', 'Muy Líquido');
INSERT INTO LKUP_LIQUIDEZ (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'B', 'Líquido');
INSERT INTO LKUP_LIQUIDEZ (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'C', 'Liquidez media / baja');
INSERT INTO LKUP_LIQUIDEZ (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'D', 'Liquidez baja');
INSERT INTO LKUP_LIQUIDEZ (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (NULL, 1, 'E', 'Liquidez muy baja');

INSERT INTO `LKUP_TIPO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (1, 1, 'HIP', 'Hipotecario');
INSERT INTO `LKUP_TIPO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (2, 1, 'ETNJ', 'ETNJ');
INSERT INTO `LKUP_TIPO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (3, 1, 'EN', 'Ejecución notarial');
INSERT INTO `LKUP_TIPO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (4, 1, 'MON', 'Monitorio');
INSERT INTO `LKUP_TIPO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (5, 1, 'VERB', 'Verbal');
INSERT INTO `LKUP_TIPO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (6, 1, 'ORD', 'Ordinario');
INSERT INTO `LKUP_TIPO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (7, 1, 'ETJ', 'ETJ');
INSERT INTO `LKUP_TIPO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (8, 1, 'CON', 'Concursal');

INSERT INTO `LKUP_ESTADO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (1, 1, '1', 'Abierto');
INSERT INTO `LKUP_ESTADO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (2, 1, '2', 'Cerrado');
INSERT INTO `LKUP_ESTADO_PROCEDIMIENTO` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (3, 1, '3', 'Paralizado');

INSERT INTO `LKUP_ESTADO_SUBASTA` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (1, 1, '01', 'Pendiente');
INSERT INTO `LKUP_ESTADO_SUBASTA` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (2, 1, '02', 'Suspendida');
INSERT INTO `LKUP_ESTADO_SUBASTA` (`id`, `ind_activo`, `des_codigo`, `des_valor`) VALUES (3, 1, '03', 'Cancelada');

INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (1, b'1', 'Gestor', NULL);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (10, b'1', 'Gestor Especialista', 1);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (11, b'1', 'Gestor Telefónico', 1);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (12, b'1', 'Perfil consulta externo', NULL);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (13, b'1', 'Gestor Central', 1);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (14, b'1', 'Gestor Especialista', 1);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (15, b'1', 'Gestor Telefónico',1);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (16, b'1', 'Gestor Presencial', 1);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (17, b'1', 'Soporte', NULL);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (2, b'1', 'Responsable de cartera', NULL);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (3, b'1', 'Supervisor', NULL);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (4, b'1', 'Soporte', NULL);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (5, b'1', 'Gestor formalización', NULL);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (6, b'1', 'Gestor skip tracing', NULL);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (7, b'1', 'Perfil consulta interno', NULL);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (8, b'1', 'Gestor Presencial', 1);
INSERT INTO `MSTR_PERFIL` (`ID`, `IND_ACTIVO`, `DES_NOMBRE`, `ID_PERFIL_PADRE`) VALUES (9, b'1', 'Gestor Central', 1);

INSERT INTO `LKUP_ROL_HAYA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'admin', 'Administrador');
INSERT INTO `LKUP_ROL_HAYA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, 'usuario', 'Usuario');
INSERT INTO `LKUP_ROL_HAYA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (3, 1, 'interv', 'Interviniente');
INSERT INTO `LKUP_ROL_HAYA` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (4, 1, 'cliente', 'Cliente');

INSERT INTO `LKUP_SITUACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, '1', 'Pendiente de Gestion');
INSERT INTO `LKUP_SITUACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, '2', 'Sin Incidencias');
INSERT INTO `LKUP_SITUACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (3, 1, '3', 'Cargas Anteriores');
INSERT INTO `LKUP_SITUACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (4, 1, '4', 'Cargas Posteriores');
INSERT INTO `LKUP_SITUACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (5, 1, '5', 'Colaterales Adjudicados');
INSERT INTO `LKUP_SITUACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (6, 1, '6', 'Concurso');
INSERT INTO `LKUP_SITUACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (7, 1, '7', 'Incidencia Legal');
INSERT INTO `LKUP_SITUACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (8, 1, '8', 'Esclusion Perimetro/Putback');
INSERT INTO `LKUP_SITUACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (9, 1, '9', 'Cerrado/Resuelto amistoso');
INSERT INTO `LKUP_SITUACION` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (10, 1, '10', 'Cerrado/Lanzamiento');

INSERT INTO `LKUP_AREA_PERFIL` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (1, 1, 'te', 'Territorial');
INSERT INTO `LKUP_AREA_PERFIL` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (2, 1, 'cent', 'Central');
INSERT INTO `LKUP_AREA_PERFIL` (`ID`, `IND_ACTIVO`, `DES_CODIGO`, `DES_VALOR`) VALUES (3, 1, 'prov', 'Proveedor');

INSERT INTO `LKUP_HITO_PROCEDIMIENTO` (ID, IND_ACTIVO, DES_CODIGO, DES_VALOR, ID_TIPO_PROCEDIMIENTO) VALUES
(1,1,'HIP_01','Resultado Comparecencia Oposición', 1),
(2,1,'HIP_02','Fecha Oposición', 1),
(3,1,'HIP_03','Domicilio Notificación Requerimiento de Pago', 1),
(4,1,'HIP_04','Resultado Requerimiento de Pago', 1),
(5,1,'HIP_05','Fecha Requerimiento de Pago', 1),
(6,1,'HIP_06','Fecha Certificación Registral', 1),
(7,1,'HIP_07','Auto despachando ejecución', 1),
(8,1,'HIP_08','Importe Interposición Demanda', 1),
(9,1,'HIP_09','Fecha Interposición Demanda', 1),

(10,1,'ETNJ_01','Resultado Comparecencia Oposición', 2),
(11,1,'ETNJ_02','Fecha Oposición', 2),
(12,1,'ETNJ_03','Domicilio Notificación Requerimiento de Pago', 2),
(13,1,'ETNJ_04','Fecha Requerimiento de Pago', 2),
(14,1,'ETNJ_05','Fecha Auto despachando ejecución', 2),
(15,1,'ETNJ_06','Importe Interposición Demanda', 2),
(16,1,'ETNJ_07','Fecha Interposición Demanda', 2),

(17,1,'EN_01','Fecha Posesión', 3),
(18,1,'EN_02','Fecha Escritura Compraventa', 3),
(19,1,'EN_03','Motivo suspensión Celebración Subasta', 3),
(20,1,'EN_04','Resultado Celebración Subasta', 3),
(21,1,'EN_05','Celebración Subasta', 3),
(22,1,'EN_06','Fecha Subasta', 3),
(23,1,'EN_07','Fecha Anuncio Subasta', 3),
(24,1,'EN_08','Fecha Requerimiento al deudor', 3),
(25,1,'EN_09','Fecha Certificación Registral', 3),
(26,1,'EN_10','Fecha Entrega acta requerimiento a notario', 3),

(27,1,'MON_01','Resultado Comparecencia Oposición', 4),
(28,1,'MON_02','Fecha Oposición', 4),
(29,1,'MON_03','Fecha Requerimiento de Pago', 4),
(30,1,'MON_04','Fecha Admisión Demanda', 4),
(31,1,'MON_05','Importe Interposición Demanda', 4),
(32,1,'MON_06','Fecha Interposición Demanda', 4),

(33,1,'VERB_01','Resolución', 5),
(34,1,'VERB_02','Fecha Juicio', 5),
(35,1,'VERB_03','Resultado Notificación de la Demanda', 5),
(36,1,'VERB_04','Fecha Notificación de la Demanda', 5),
(37,1,'VERB_05','Fecha Admisión Demanda', 5),
(38,1,'VERB_06','Importe Interposición Demanda', 5),
(39,1,'VERB_07','Fecha Interposición Demanda', 5),

(40,1,'ORD_01','Resultado Comparecencia Oposición', 6),
(41,1,'ORD_02','Fecha Oposición', 6),
(42,1,'ORD_03','Fecha Notificación de la Demanda.', 6),
(43,1,'ORD_04','Domicilio Notificación Demanda', 6),
(44,1,'ORD_05','Resultado Notificación de la Demanda.', 6),
(45,1,'ORD_06','Fecha Admisión Demanda', 6),
(46,1,'ORD_07','Importe Interposición Demanda', 6),
(47,1,'ORD_08','Fecha Interposición Demanda', 6),

(48,1,'ETJ_01','Resultado Comparecencia Oposición', 7),
(49,1,'ETJ_02','Fecha Oposición', 7),
(50,1,'ETJ_03','Fecha Requerimiento de Pago', 7),
(51,1,'ETJ_04','Fecha Auto despachando ejecución', 7),
(52,1,'ETJ_05','Importe Interposición Demanda', 7),
(53,1,'ETJ_06','Fecha Interposición Demanda', 7),

(54,1,'CON_01','Fecha decreto convocatoria subasta', 8),
(55,1,'CON_02','Fecha aprobación plan liquidación', 8),
(56,1,'CON_03','Fecha apertura fase liquidación', 8),
(57,1,'CON_04','Fecha aprobación convenio', 8),
(58,1,'CON_05','Fecha apertura fase convenio', 8),
(59,1,'CON_06','Fecha Auto Concurso', 8),
(60,1,'CON_07','Fecha publicación BOE', 8);
