package com.haya.alaska.asignacion_expediente.domain;

import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_RELA_ASIGNACION_EXPEDIENTE")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "RELA_ASIGNACION_EXPEDIENTE")
public class AsignacionExpediente implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_EXPEDIENTE")
  private Expediente expediente;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_USUARIO")
  @NotNull
  private Usuario usuario;

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
    name = "RELA_ASIGNACION_GA_SUPLENTE",
    joinColumns = @JoinColumn(name = "ID_ASIGNACION_EXPEDIENTE"),
    inverseJoinColumns = @JoinColumn(name = "ID_SUPLENTE"))
  @AuditJoinTable(name = "HIST_RELA_ASIGNACION_GA_SUPLENTE")
  private Set<Usuario> suplente;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_USUARIO_ASIGNACION")
  @NotNull
  private Usuario usuarioAsignacion;



  public AsignacionExpediente(Expediente expediente, Usuario usuarioAsignacion, Usuario usuario) {
    this.expediente=expediente;
    this.usuarioAsignacion=usuarioAsignacion;
    this.usuario=usuario;
  }

}
