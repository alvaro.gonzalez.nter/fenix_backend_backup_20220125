package com.haya.alaska.asignacion_expediente.infrastructure.mapper;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.infrastructure.controller.dto.AsignacionExpedienteDTO;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTO;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.stereotype.Component;

@Component
public class AsignacionExpedienteMapper {

  public AsignacionExpedienteDTO parse(AsignacionExpediente asignacionExpediente) {
    AsignacionExpedienteDTO asignacionExpedienteDTO = new AsignacionExpedienteDTO();
    asignacionExpedienteDTO.setUsuario(asignacionExpediente.getUsuario().getId());
    asignacionExpedienteDTO.setExpediente(asignacionExpediente.getExpediente().getId());
    asignacionExpedienteDTO.setIdConcatenado(asignacionExpediente.getExpediente().getIdConcatenado());
    asignacionExpedienteDTO.setPerfil(asignacionExpediente.getUsuario().getPerfil().getNombre());
    return asignacionExpedienteDTO;
  }
}
