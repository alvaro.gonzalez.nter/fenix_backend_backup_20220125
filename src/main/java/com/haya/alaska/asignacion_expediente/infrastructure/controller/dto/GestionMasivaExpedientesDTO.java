package com.haya.alaska.asignacion_expediente.infrastructure.controller.dto;

import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GestionMasivaExpedientesDTO {

  private List<AsignacionExpedienteDTO> listaAsignacionExpedientes;
  private List<ExpedientePosesionNegociadaDTO> listaAsignacionExpedientesPN;
}
