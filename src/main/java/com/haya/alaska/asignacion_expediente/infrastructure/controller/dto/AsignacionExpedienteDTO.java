package com.haya.alaska.asignacion_expediente.infrastructure.controller.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AsignacionExpedienteDTO implements Serializable {

  private Integer Usuario;
  private Integer Expediente;
  private String idConcatenado;
  private String perfil;
}
