package com.haya.alaska.asignacion_expediente.infrastructure.repository;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AsignacionExpedienteRepository
    extends JpaRepository<AsignacionExpediente, Integer> {

  List<AsignacionExpediente> findAllByUsuarioId(Integer usuarioId);
  Integer countAllByUsuarioIdAndExpedienteContratosEstadoContratoValorNot(Integer usuarioId, String estado);

  List<AsignacionExpediente> findAllByExpedienteIdAndUsuarioPerfilId(
    Integer expedienteId, Integer perfilId);

  Integer countByUsuarioId(Integer usuarioId);
  void deleteAllByUsuarioId(Integer usuarioId);
  List<AsignacionExpediente> findAllByExpedienteId(Integer expedienteId);
  List<AsignacionExpediente> findAllByExpedienteIdAndUsuarioId(Integer expedienteId, Integer usuarioId);
  void deleteAllByExpedienteIdAndUsuarioId(Integer expedienteId, Integer usuarioId);

  void deleteAllByUsuarioIdAndExpedienteIdIn(Integer usuarioId, List<Integer> expedientesId);
  void deleteAllByUsuarioIsNullAndExpedienteIdIn(List<Integer> expedientesId);
}
