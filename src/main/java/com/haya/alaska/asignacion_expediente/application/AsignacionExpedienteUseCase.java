package com.haya.alaska.asignacion_expediente.application;

import com.haya.alaska.asignacion_expediente.infrastructure.controller.dto.GestionMasivaExpedientesDTO;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioOutputDTO;

import java.util.List;

public interface AsignacionExpedienteUseCase {

  GestionMasivaExpedientesDTO modificarGestor(List<Integer> expedienteIdList, List<Integer> expedientePNIdList, Integer usuarioId, boolean isSuplente, boolean todos, boolean todosPN, BusquedaDto busqueda, Usuario usuario) throws Exception;
  List<UsuarioOutputDTO> getUsuariosIncluidos(List<Integer> expedienteIdList, List<Integer> expedientePNIdList, boolean todos, boolean todosPN,Integer perfil, Integer area, BusquedaDto busqueda, Usuario usuario) throws NotFoundException;
  List<AreaUsuarioOutputDTO> getAreaUsuarioIncluidos(List<Integer> expedienteIdList, List<Integer> expedientePNIdList, boolean todos, boolean todosPN, BusquedaDto busqueda, Usuario usuario) throws NotFoundException;
  List<PerfilSimpleOutputDTO> getPerfilesIncluidos(List<Integer> expedienteIdList, List<Integer> expedientePNIdList, boolean todos, boolean todosPN, BusquedaDto busqueda, Usuario usuario) throws NotFoundException;
  GestionMasivaExpedientesDTO desasignar(List<Integer> expedienteIdList, List<Integer> expedientePNIdList, Integer usuarioId, boolean todos, boolean todosPN, Usuario usuario);

}
