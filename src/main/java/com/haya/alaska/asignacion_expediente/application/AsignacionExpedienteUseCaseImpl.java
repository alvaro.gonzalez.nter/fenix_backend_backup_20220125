package com.haya.alaska.asignacion_expediente.application;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.area_usuario.infrastructure.repository.AreaUsuarioRepository;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.infrastructure.controller.dto.AsignacionExpedienteDTO;
import com.haya.alaska.asignacion_expediente.infrastructure.controller.dto.GestionMasivaExpedientesDTO;
import com.haya.alaska.asignacion_expediente.infrastructure.mapper.AsignacionExpedienteMapper;
import com.haya.alaska.asignacion_expediente.infrastructure.repository.AsignacionExpedienteRepository;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.asignacion_expediente_posesion_negociada.repository.AsignacionExpedientePosesionNegociadaRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.busqueda.application.BusquedaUseCaseImpl;
import com.haya.alaska.busqueda.domain.BusquedaBuilderPN;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCase;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada_;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.mapper.ExpedientePosesionNegociadaMapper;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.infrastructure.repository.PerfilRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import com.haya.alaska.segmentacion_pn.infrastructure.repository.SegmentacionPNRepository;
import com.haya.alaska.segmentacion_pn.infrastructure.util.SegmentacionPNUtil;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.mapper.UsuarioMapper;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class AsignacionExpedienteUseCaseImpl implements AsignacionExpedienteUseCase {

  @PersistenceContext EntityManager entityManager;
  @Autowired AsignacionExpedienteRepository asignacionExpedienteRepository;

  @Autowired
  AsignacionExpedientePosesionNegociadaRepository asignacionExpedientePosesionNegociadaRepository;

  @Autowired UsuarioRepository usuarioRepository;
  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired ExpedientePosesionNegociadaCase expedientePosesionNegociadaCase;
  @Autowired ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;

  @Autowired PerfilRepository perfilRepository;
  @Autowired AsignacionExpedienteMapper asignacionexpedienteMapper;
  @Autowired ExpedientePosesionNegociadaMapper expedientePosesionNegociadaMapper;
  @Autowired EventoUseCase eventoUseCase;
  @Autowired EventoUtil eventoUtil;
  @Autowired BusquedaUseCaseImpl busquedaUseCase;
  @Autowired SegmentacionPNRepository segmentacionPNRepository;
  @Autowired SegmentacionPNUtil segmentacionPNUtil;
  @Autowired UsuarioMapper usuarioMapper;
  @Autowired AreaUsuarioRepository areaUsuarioRepository;

  @Override
  public GestionMasivaExpedientesDTO modificarGestor(
      List<Integer> expedienteIdList,
      List<Integer> expedientePNIdList,
      Integer usuarioId,
      boolean isSuplente,
      boolean todos,
      boolean todosPN,
      BusquedaDto busqueda,
      Usuario logUser)
      throws Exception {
    Usuario usuario =
        usuarioRepository
            .findById(logUser.getId())
            .orElseThrow(() -> new NotFoundException("Usuario", logUser.getId()));

    GestionMasivaExpedientesDTO outputDTO = new GestionMasivaExpedientesDTO();

    List<AsignacionExpedienteDTO> asignacionExpedienteDTOS = new ArrayList<>();
    List<ExpedientePosesionNegociadaDTO> asignacionExpedientePNDTOS = new ArrayList<>();

    ArrayList<AsignacionExpediente> listaAsignaciones = new ArrayList<>();
    ArrayList<AsignacionExpedientePosesionNegociada> listaAsignacionesPN = new ArrayList<>();

    comprobacionesExpedientesGestionMasiva(
        expedienteIdList,
        usuarioId,
        isSuplente,
        todos,
        busqueda,
        usuario,
        asignacionExpedienteDTOS,
        listaAsignaciones);

    comprobacionesExpedientesPNGestionMasiva(
        expedientePNIdList,
        usuarioId,
        isSuplente,
        todosPN,
        busqueda,
        usuario,
        asignacionExpedienteDTOS,
        listaAsignacionesPN);

    // Prepara el DTO de expedientes de Recuperacion Amistosa para devolverlo al FRONT
    for (AsignacionExpediente asignacionconversion : listaAsignaciones) {
      Expediente nuevo = asignacionconversion.getExpediente();
      AsignacionExpedienteDTO asignacionExpedienteDTO =
          asignacionexpedienteMapper.parse(asignacionconversion);
      asignacionExpedienteDTOS.add(asignacionExpedienteDTO);
    }

    // Prepara el DTO de expedientes de Posesion Negociada para devolverlo al FRONT
    for (var asignacionconversion : listaAsignacionesPN) {
      var nuevo = asignacionconversion.getExpediente();
      ExpedientePosesionNegociadaDTO asignacionExpedienteDTO =
          expedientePosesionNegociadaMapper.parse(asignacionconversion.getExpediente());
      asignacionExpedientePNDTOS.add(asignacionExpedienteDTO);
    }

    outputDTO.setListaAsignacionExpedientes(asignacionExpedienteDTOS);
    outputDTO.setListaAsignacionExpedientesPN(asignacionExpedientePNDTOS);

    Usuario newUser = usuarioRepository
      .findById(usuarioId)
      .orElseThrow(
        () ->
          new NotFoundException("Usuario", usuarioId));

    if (getPerfilesAlerta().contains(newUser.getPerfil().getNombre())){
      List<Integer> idsRA = asignacionExpedienteDTOS.stream().map(AsignacionExpedienteDTO::getExpediente).collect(Collectors.toList());
      List<Integer> idsPN = asignacionExpedientePNDTOS.stream().map(ExpedientePosesionNegociadaDTO::getId).collect(Collectors.toList());

      eventoUseCase.alertaReasignaciones(idsRA, idsPN, usuario, usuarioId);
    }

    return outputDTO;
  }

  private List<String> getPerfilesAlerta() {
    List<String> perfiles = new ArrayList<>();

    perfiles.add("Gestor");
    perfiles.add("Gestor formalización");
    perfiles.add("Gestor skip tracing");
    perfiles.add("Agencias Externas");

    return perfiles;
  }

  private void comprobacionesExpedientesPNGestionMasiva(
      List<Integer> expedientePNIdList,
      Integer usuarioId,
      boolean isSuplente,
      boolean todosPN,
      BusquedaDto busqueda,
      Usuario loggedUser,
      List<AsignacionExpedienteDTO> asignacionExpedienteDTOS,
      ArrayList<AsignacionExpedientePosesionNegociada> listaAsignaciones)
      throws Exception {
    Perfil perfil = null;
    Usuario usuario = null;
    List<Integer> ids = new ArrayList<>();

    // Comprobamos que el usuario exista
    if (usuarioId != null) {
      usuario = usuarioRepository.findById(usuarioId).get();
      perfil = usuario.getPerfil();
    }
    if (isSuplente && perfil.getNombre().compareTo("Gestor") != 0)
      throw new IllegalArgumentException(
          "El tipo del usuario debe ser gestor para poder ser suplente de otro expediente");

    if (todosPN == true) {
      List<Predicate> predicates = new ArrayList<>();

      var tempCriteriaBuilder = entityManager.getCriteriaBuilder();
      var query = tempCriteriaBuilder.createQuery(ExpedientePosesionNegociada.class);
      BusquedaBuilderPN busquedaBuilderPN =
          new BusquedaBuilderPN(query.from(ExpedientePosesionNegociada.class), tempCriteriaBuilder);

      predicates =
          expedientePosesionNegociadaCase.getDataExpedientesPN(
              predicates, busqueda, loggedUser, busquedaBuilderPN);

      var rs =
          query
              .select(busquedaBuilderPN.getRoot())
              .where(predicates.toArray(new Predicate[predicates.size()]));
      List<ExpedientePosesionNegociada> expedientesLista =
          entityManager.createQuery(rs).getResultList();

      for (ExpedientePosesionNegociada ex : expedientesLista) {
        ids.add(ex.getId());
      }

      // TODO: sacar los que esten deseleccionados, cuando se implemente esa parte
    } else {
      ids = expedientePNIdList;
    }

    if (ids != null) {
      Locale loc = LocaleContextHolder.getLocale();
      //comprobar que el usuario tiene permiso para las carteras de los expedientes. si no tiene para alguna de ellas, se tira excepcion
      for(Integer expedienteId: ids) {
        ExpedientePosesionNegociada expediente =
          expedientePosesionNegociadaRepository
            .findById(expedienteId)
            .orElseThrow(() -> new NotFoundException("Expediente PN", expedienteId));
        if(usuario==null) {
          if (loc.getLanguage().equals("en"))
            throw new Exception("User does not exist");
          else
            throw new Exception("El usuario no existe");
        }else if(usuario.getAsignacionesUsuarioCartera()==null || usuario.getAsignacionesUsuarioCartera().size()==0) {
          if (loc.getLanguage().equals("en"))
            throw new Exception("User "+usuario.getNombre()+" does not have permission for portfolio "+expediente.getCartera().getNombre());
          else
            throw new Exception("El usuario "+usuario.getNombre()+" no tiene permiso para la cartera "+expediente.getCartera().getNombre());
        } else {
          boolean tieneAsignacion = false;
          for (var asignacion : usuario.getAsignacionesUsuarioCartera()) {
            if (asignacion.getCartera().getId().equals(expediente.getCartera().getId()))
              tieneAsignacion = true;
          }
          if (!tieneAsignacion) {
            if (loc.getLanguage().equals("en"))
              throw new Exception("User " + usuario.getNombre() + " does not have permission for portfolio " + expediente.getCartera().getNombre());
            else
              throw new Exception("El usuario " + usuario.getNombre() + " no tiene permiso para la cartera " + expediente.getCartera().getNombre());
          }
        }
      }
      // comprobar si alguno de ellos no tiene gestor principal y se les quiere cambiar el suplente
      if (isSuplente) {
        for (Integer expedienteId : ids) {
          ExpedientePosesionNegociada expediente =
              expedientePosesionNegociadaRepository
                  .findById(expedienteId)
                  .orElseThrow(
                      () -> new NotFoundException("Expediente Posesión Negociada", expedienteId));
          Set<AsignacionExpedientePosesionNegociada> asignaciones = expediente.getAsignaciones();
          if (asignaciones.size() == 0) {
            throw new Exception("No se puede añadir un suplente si no hay un gestor");
          }
          for (AsignacionExpedientePosesionNegociada a : asignaciones) {
            if (a.getUsuario() == null) {
              throw new Exception("No se puede añadir un suplente si no hay un gestor");
            }
          }
        }
      }

      for (Integer expedienteId : ids) {
        // Comprueba que exista el expediente antes de nada, y se declara la variable expediente
        // para
        // utilizarla más adelante
        ExpedientePosesionNegociada expediente =
            expedientePosesionNegociadaRepository
                .findById(expedienteId)
                .orElseThrow(
                    () -> new NotFoundException("Expediente Posesión Negociada", expedienteId));

        // Si no hay perfil, borramos de manera lógica todas las asignaciones con el id expediente
        // de
        // la lista
        if (perfil == null) {
          var listaAsignacionExpediente =
              asignacionExpedientePosesionNegociadaRepository.findAllByExpedienteId(expedienteId);
          for (var asignacion : listaAsignacionExpediente) {
            asignacion.setActivo(false);
          }
          asignacionExpedientePosesionNegociadaRepository.flush();
          continue;
        }

        // En caso de que el perfil sea suplente, se buscan todos los de tipo gestor para
        // actualizarles a estos el suplente
        List<AsignacionExpedientePosesionNegociada> listaAsignacionExpedientes;
        if (isSuplente) {
          Perfil perfil_gestor =
              perfilRepository
                  .findByNombre("Gestor")
                  .orElseThrow(() -> new NotFoundException("Perfil", "Nombre", "Gestor"));
          listaAsignacionExpedientes =
              asignacionExpedientePosesionNegociadaRepository
                  .findAllByExpedienteIdAndUsuarioPerfilId(expedienteId, perfil_gestor.getId());
        } else {
          listaAsignacionExpedientes =
              asignacionExpedientePosesionNegociadaRepository
                  .findAllByExpedienteIdAndUsuarioPerfilId(expedienteId, perfil.getId());
        }

        // Comprueba si la asignación actual existe, si no, la crea
        AsignacionExpedientePosesionNegociada asignacionActual = null;
        if (perfil.getNombre().toUpperCase().contains("CONSULTA")
            || perfil.getNombre().toUpperCase().equals("SOPORTE")
            || perfil.getNombre().toUpperCase().equals("SOPORTE FORMALIZACIÓN")
          || perfil.getNombre().toUpperCase().equals("GESTOR RIESGO")
          || perfil.getNombre().toUpperCase().equals("CLIENTE")) {
          // Buscar por idexpediente y
          List<AsignacionExpedientePosesionNegociada> asignacionExpedientePosesionNegociadaList =
              asignacionExpedientePosesionNegociadaRepository.findAllByExpedienteIdAndUsuarioId(
                  expedienteId, usuarioId);
          eventoUtil.reasignarDestinatarios(null, usuario, expedienteId, "EXP_PN");
          if (asignacionExpedientePosesionNegociadaList.isEmpty()) {
            asignacionActual = new AsignacionExpedientePosesionNegociada();
          } else {
            asignacionActual = asignacionExpedientePosesionNegociadaList.get(0);
          }
        } else {
          if (listaAsignacionExpedientes.isEmpty() && isSuplente == false) {
            asignacionActual = new AsignacionExpedientePosesionNegociada();
          } else if (!listaAsignacionExpedientes.isEmpty()) {
            asignacionActual = listaAsignacionExpedientes.get(0);
          }
        }

        // Realiza las comprobaciones de los límites de expedientes que un usuario puede gestionar
        Integer n_asignados = asignacionExpedienteRepository.countByUsuarioId(usuarioId);
        Integer n_expedientes = usuario.getLimiteExpedientes();

        if (n_expedientes == null) // Puede gestionar ilimitados expedientes
        {
          if (asignacionActual.getUsuario() != null) {
            if (isSuplente) asignacionActual.getSuplente().add(usuario);
            eventoUtil.reasignarDestinatarios(
                asignacionActual.getUsuario(), usuario, expedienteId, "EXP_PN");
          }
          asignacionActual.setExpediente(expediente);
          asignacionActual.setUsuarioAsignacion(loggedUser);
          if (isSuplente) asignacionActual.getSuplente().add(usuario);
          else {
            asignacionActual.setUsuario(usuario);
            eventoUtil.anadirEventosPerdidos(usuario, expedienteId, "EXP_PN");
          }
          listaAsignaciones.add(
              asignacionExpedientePosesionNegociadaRepository.save(asignacionActual));
        } else if (n_asignados < n_expedientes) // Puede gestionar un número limitado de expedientes
        {
          if (asignacionActual != null) {
            if (asignacionActual.getUsuario() != null)
              eventoUtil.reasignarDestinatarios(
                  asignacionActual.getUsuario(), usuario, expedienteId, "EXP_PN");
            asignacionActual.setExpediente(expediente);
            if (isSuplente) asignacionActual.getSuplente().add(usuario);
            else {
              asignacionActual.setUsuario(usuario);
              eventoUtil.anadirEventosPerdidos(usuario, expedienteId, "EXP_PN");
            }
            asignacionActual.setUsuarioAsignacion(loggedUser);
            listaAsignaciones.add(
                asignacionExpedientePosesionNegociadaRepository.save(asignacionActual));
          } else {
            throw new Exception("No se puede añadir un suplente si no hay un gestor");
          }
        } else {
          // System.out.println("Fallo");
          throw new IllegalArgumentException(
              "Se ha sobrepasado el límite de expedientes para este usuario numero limite expedientes permitidos:"
                  + n_asignados
                  + "asignados y los que se pueden asignar son"
                  + usuario.getLimiteExpedientes());
          // System.out.println("Se ha sobrepasado el límite de expedientes para este usuario");
        }
      }
    }
  }

  public void altaAsignacionesCargaPN(
      Usuario loggedUser,
      List<ExpedientePosesionNegociada> expedientesLista,
      Usuario responsableCartera,
      Usuario responsableSkipTracing,
      Usuario responsableFormalizacion)
      throws Exception {

    // Comprobamos que no se sobrepasen el límite de expedientes asignados
    // a cada uno de los responsables de la cartera asignados
    if (responsableCartera.getLimiteExpedientes() != null
        && expedientesLista.size() > responsableCartera.getLimiteExpedientes())
      throw new RequiredValueException(responsableCartera.getId());
    if (responsableSkipTracing.getLimiteExpedientes() != null
        && expedientesLista.size() > responsableSkipTracing.getLimiteExpedientes())
      throw new RequiredValueException(responsableSkipTracing.getId());
    if (responsableFormalizacion.getLimiteExpedientes() != null
        && expedientesLista.size() > responsableFormalizacion.getLimiteExpedientes())
      throw new RequiredValueException(responsableFormalizacion.getId());
    if (loggedUser != null) {
      Integer loggedId = loggedUser.getId();
      loggedUser =
          usuarioRepository
              .findById(loggedId)
              .orElseThrow(() -> new NotFoundException("Usuario", loggedId));
    }

    for (ExpedientePosesionNegociada exp : expedientesLista) {
      // creamos asignación
      AsignacionExpedientePosesionNegociada asignacionRC =
          new AsignacionExpedientePosesionNegociada();
      asignacionRC.setExpediente(exp);
      asignacionRC.setUsuario(responsableCartera);
      asignacionRC.setUsuarioAsignacion(loggedUser);
      asignacionExpedientePosesionNegociadaRepository.save(asignacionRC);

      AsignacionExpedientePosesionNegociada asignacionST =
          new AsignacionExpedientePosesionNegociada();
      asignacionST.setExpediente(exp);
      asignacionST.setUsuario(responsableSkipTracing);
      asignacionST.setUsuarioAsignacion(loggedUser);
      asignacionExpedientePosesionNegociadaRepository.save(asignacionST);

      AsignacionExpedientePosesionNegociada asignacionF =
          new AsignacionExpedientePosesionNegociada();
      asignacionF.setExpediente(exp);
      asignacionF.setUsuario(responsableFormalizacion);
      asignacionF.setUsuarioAsignacion(loggedUser);
      asignacionExpedientePosesionNegociadaRepository.save(asignacionF);
    }
    // asignacion de gestor segun segmentacion
    asignarSegmentacionPN();
  }

  public void asignarSegmentacionPN() {

    List<SegmentacionPN> segmentaciones = segmentacionPNRepository.findByGuardadoTrue();
    Perfil perfil = perfilRepository.findByNombre("Gestor").orElse(null);

    for (SegmentacionPN segmentacion : segmentaciones) {

      if (segmentacion.getCartera() == null) continue;

      List<ExpedientePosesionNegociada> expedientesSegmentacion =
          segmentacionPNUtil.sacarExpedientesFiltrados(segmentacion);

      for (ExpedientePosesionNegociada expediente : expedientesSegmentacion) {

        boolean asignado = false;

        if (expediente.getGestor() != null) continue;

        if (!expedientesSegmentacion.contains(expediente)) continue;

        // Si se selecciona área, gestor y grupo, a ese gestor es al que se le asigna.
        // Si ese gestor ya tiene el máximo de expedientes asignados, no se asigna a nadie.
        if (segmentacion.getGestor() != null) {

          AsignacionExpedientePosesionNegociada asignacionActual =
              new AsignacionExpedientePosesionNegociada();

          // Realiza las comprobaciones de los límites de expedientes que un usuario puede gestiona
          Integer n_asignados = contarExpedientesPNAsignados(segmentacion.getGestor());
          Integer n_expedientes = segmentacion.getGestor().getLimiteExpedientes();

          if (n_expedientes != null && n_expedientes - n_asignados > 0) {
            asignacionActual.setExpediente(expediente);
            asignacionActual.setUsuarioAsignacion(expediente.getCartera().getResponsableCartera());
            asignacionActual.setUsuario(segmentacion.getGestor());
            asignacionActual =
                asignacionExpedientePosesionNegociadaRepository.saveAndFlush(asignacionActual);
            Set<AsignacionExpedientePosesionNegociada> asignaciones = expediente.getAsignaciones();
            asignaciones.add(asignacionActual);
            expediente.setAsignaciones(asignaciones);
            expedientePosesionNegociadaRepository.saveAndFlush(expediente);
            asignado = true;
          }

        } else {
          // Si no se selecciona gestor y sí los otros dos, se hace lo siguiente:
          List<Usuario> usuariosPorGrupoArea = null;
          if (segmentacion.getGrupo() != null && segmentacion.getArea() != null) {
            usuariosPorGrupoArea =
                usuarioRepository.findByGrupoUsuariosIdAndAreasIdAndPerfilId(
                    segmentacion.getGrupo().getId(),
                    segmentacion.getArea().getId(),
                    perfil.getId());
          } else if (segmentacion.getGrupo() != null) {
            usuariosPorGrupoArea =
                usuarioRepository.findByGrupoUsuariosIdAndPerfilId(
                    segmentacion.getGrupo().getId(), perfil.getId());
          } else if (segmentacion.getArea() != null) {
            usuariosPorGrupoArea =
                usuarioRepository.findByAreasIdAndPerfilId(
                    segmentacion.getArea().getId(), perfil.getId());
          }
          if (usuariosPorGrupoArea != null && usuariosPorGrupoArea.size() > 0) {

            AsignacionExpedientePosesionNegociada asignacionActual =
                new AsignacionExpedientePosesionNegociada();
            // -Si NO existen gestores en esa área y grupo, no se asigna gestor
            for (Usuario usuario : usuariosPorGrupoArea) {

              // -Si SÍ existen gestores en esa área y grupo, se REPARTEN de esta forma:
              // Se asignan a los gestores de la misma provincia que el primer titular del contrato
              // representante, si no han llegado al límite de expedientes asignados
              BienPosesionNegociada bienPrincipal =
                  segmentacionPNUtil.sacarBienPNPrincipal(expediente);
              Provincia provinciaBienPrincipal = null;
              if (bienPrincipal != null) {
                provinciaBienPrincipal = bienPrincipal.getBien().getProvincia();
              }

              if (provinciaBienPrincipal != null
                  && usuario.getProvincias() != null
                  && usuario.getProvincias().contains(provinciaBienPrincipal)) {

                Integer n_asignados = contarExpedientesPNAsignados(usuario);
                Integer n_expedientes = usuario.getLimiteExpedientes();

                if (n_expedientes != null && n_expedientes - n_asignados > 0) {
                  asignacionActual.setExpediente(expediente);
                  asignacionActual.setUsuarioAsignacion(
                      expediente.getCartera().getResponsableCartera());
                  asignacionActual.setUsuario(usuario);
                  asignacionActual =
                      asignacionExpedientePosesionNegociadaRepository.saveAndFlush(
                          asignacionActual);
                  Set<AsignacionExpedientePosesionNegociada> asignaciones =
                      expediente.getAsignaciones();
                  asignaciones.add(asignacionActual);
                  expediente.setAsignaciones(asignaciones);
                  expedientePosesionNegociadaRepository.saveAndFlush(expediente);
                  asignado = true;
                  break;
                }
              }
            }

            // Si no se ha podido, se asignan a los gestores de otra provincia del mismo área y
            // grupo, también teniendo en cuenta los limites.
            if (!asignado) {
              for (Usuario usuario : usuariosPorGrupoArea) {

                Integer n_asignados = contarExpedientesPNAsignados(usuario);
                Integer n_expedientes = usuario.getLimiteExpedientes();

                if (n_expedientes != null && n_expedientes - n_asignados > 0) {
                  asignacionActual.setExpediente(expediente);
                  asignacionActual.setUsuarioAsignacion(
                      expediente.getCartera().getResponsableCartera());
                  asignacionActual.setUsuario(usuario);
                  asignacionActual =
                      asignacionExpedientePosesionNegociadaRepository.saveAndFlush(
                          asignacionActual);
                  Set<AsignacionExpedientePosesionNegociada> asignaciones =
                      expediente.getAsignaciones();
                  asignaciones.add(asignacionActual);
                  expediente.setAsignaciones(asignaciones);
                  expedientePosesionNegociadaRepository.saveAndFlush(expediente);
                  asignado = true;
                  break;
                }
              }
            }
          }
        }
      }
    }
  }

  public Integer contarExpedientesPNAsignados(Usuario gestor) {
    Integer n_asignados =
        asignacionExpedienteRepository
            .countAllByUsuarioIdAndExpedienteContratosEstadoContratoValorNot(
                gestor.getId(), "CERRADO");
    n_asignados +=
        asignacionExpedientePosesionNegociadaRepository.countAllByUsuarioId(gestor.getId());
    return n_asignados;
  }

  private void comprobacionesExpedientesGestionMasiva(
      List<Integer> expedienteIdList,
      Integer usuarioId,
      boolean isSuplente,
      boolean todos,
      BusquedaDto busqueda,
      Usuario usuarioParam,
      List<AsignacionExpedienteDTO> asignacionExpedienteDTOS,
      ArrayList<AsignacionExpediente> listaAsignaciones)
      throws Exception {
    Perfil perfil = null;
    Usuario usuario = null;
    List<Integer> ids = new ArrayList<>();

    // Comprobamos que el usuario exista
    if (usuarioId != null) {
      usuario = usuarioRepository.findById(usuarioId).get();
      perfil = usuario.getPerfil();
    }
    if (isSuplente && perfil.getNombre().compareTo("Gestor") != 0)
      throw new IllegalArgumentException(
          "El tipo del usuario debe ser gestor para poder ser suplente de otro expediente");
    // System.out.println(todos);
    if (todos == true) {
      List<Expediente> expedientesLista =
          busquedaUseCase.getDataExpedientesFilter(busqueda, usuarioParam, null, null);
      for (Expediente ex : expedientesLista) {
        ids.add(ex.getId());
      }

      // TODO: sacar los que esten deseleccionados, cuando se implemente esa parte
    } else {
      ids = expedienteIdList;
    }
    if (ids != null) {
      //comprobar que el usuario tiene permiso para las carteras de los expedientes. si no tiene para alguna de ellas, se tira excepcion
      Locale loc = LocaleContextHolder.getLocale();
      for(Integer expedienteId: ids) {
        Expediente expediente =
          expedienteRepository
            .findById(expedienteId)
            .orElseThrow(() -> new NotFoundException("Expediente", expedienteId));
        if(usuario==null) {
          if (loc.getLanguage().equals("en"))
            throw new Exception("User does not exist");
          else
            throw new Exception("El usuario no existe");
        }else if(usuario.getAsignacionesUsuarioCartera()==null || usuario.getAsignacionesUsuarioCartera().size()==0) {
          if (loc.getLanguage().equals("en"))
            throw new Exception("User "+usuario.getNombre()+" does not have permission for portfolio "+expediente.getCartera().getNombre());
          else
            throw new Exception("El usuario "+usuario.getNombre()+" no tiene permiso para la cartera "+expediente.getCartera().getNombre());

        } else{
          boolean tieneAsignacion=false;
          for(var asignacion: usuario.getAsignacionesUsuarioCartera()) {
            if(asignacion.getCartera().getId().equals(expediente.getCartera().getId()))
              tieneAsignacion=true;
          }
          if(!tieneAsignacion) {
            if (loc.getLanguage().equals("en"))
              throw new Exception("User "+usuario.getNombre()+" does not have permission for portfolio "+expediente.getCartera().getNombre());
            else
              throw new Exception("El usuario "+usuario.getNombre()+" no tiene permiso para la cartera "+expediente.getCartera().getNombre());
          }
        }
      }
      // comprobar si alguno de ellos no tiene gestor principal y se les quiere cambiar el suplente
      if (isSuplente) {
        for (Integer expedienteId : ids) {
          Expediente expediente =
              expedienteRepository
                  .findById(expedienteId)
                  .orElseThrow(() -> new NotFoundException("Expediente", expedienteId));
          Set<AsignacionExpediente> asignaciones = expediente.getAsignaciones();
          if (asignaciones.size() == 0) {
            throw new Exception("No se puede añadir un suplente si no hay un gestor");
          }
          for (AsignacionExpediente a : asignaciones) {
            if (a.getUsuario() == null) {
              throw new Exception("No se puede añadir un suplente si no hay un gestor");
            }
          }
        }
      }
      for (Integer expedienteId : ids) {
        // Comprueba que exista el expediente antes de nada, y se declara la variable expediente
        // para utilizarla más adelante
        Expediente expediente =
            expedienteRepository
                .findById(expedienteId)
                .orElseThrow(() -> new NotFoundException("Expediente", expedienteId));

        // Si no hay perfil, borramos de manera lógica todas las asignaciones con el id expediente
        // de la lista
        if (perfil == null) {
          List<AsignacionExpediente> listaAsignacionExpediente =
              asignacionExpedienteRepository.findAllByExpedienteId(expedienteId);
          for (AsignacionExpediente asignacion : listaAsignacionExpediente) {
            asignacion.setActivo(false);
          }
          asignacionExpedienteRepository.flush();
          continue;
        }

        // En caso de que el perfil sea suplente, se buscan todos los de tipo gestor para
        // actualizarles a estos el suplente
        List<AsignacionExpediente> listaAsignacionExpedientes;
        if (isSuplente) {
          Perfil perfil_gestor =
              perfilRepository
                  .findByNombre("Gestor")
                  .orElseThrow(() -> new NotFoundException("Perfil", "Nombre", "Gestor"));
          listaAsignacionExpedientes =
              asignacionExpedienteRepository.findAllByExpedienteIdAndUsuarioPerfilId(
                  expedienteId, perfil_gestor.getId());
        } else {
          listaAsignacionExpedientes =
              asignacionExpedienteRepository.findAllByExpedienteIdAndUsuarioPerfilId(
                  expedienteId, perfil.getId());
        }
        // Buscar por idexpediente y
        // Comprueba si la asignación actual existe, si no, la crea
        AsignacionExpediente asignacionActual = null;
        if (perfil.getNombre().toUpperCase().contains("CONSULTA")
            || perfil.getNombre().toUpperCase().equals("SOPORTE")
          || perfil.getNombre().toUpperCase().equals("SOPORTE FORMALIZACIÓN")
          || perfil.getNombre().toUpperCase().equals("GESTOR RIESGO")
          || perfil.getNombre().toUpperCase().equals("CLIENTE")) {
          List<AsignacionExpediente> asignacionExpedienteList =
              asignacionExpedienteRepository.findAllByExpedienteIdAndUsuarioId(
                  expedienteId, usuarioId);
          eventoUtil.reasignarDestinatarios(null, usuario, expedienteId, "EXP");
          if (asignacionExpedienteList.isEmpty()) {
            asignacionActual = new AsignacionExpediente();
          } else {
            asignacionActual = asignacionExpedienteList.get(0);
          }
        } else {
          if (listaAsignacionExpedientes.isEmpty() && isSuplente == false) {
            asignacionActual = new AsignacionExpediente();
          } else if (!listaAsignacionExpedientes.isEmpty()) {
            asignacionActual = listaAsignacionExpedientes.get(0);
          }
        }

        // Realiza las comprobaciones de los límites de expedientes que un usuario puede gestionar
        Integer n_asignados = asignacionExpedienteRepository.countByUsuarioId(usuarioId);
        Integer n_expedientes = usuario.getLimiteExpedientes();

        if (n_expedientes == null) // Puede gestionar ilimitados expedientes
        {
          asignacionActual.setExpediente(expediente);
          if (asignacionActual != null) {
            if (asignacionActual.getUsuario() != null) {
              eventoUtil.reasignarDestinatarios(
                  asignacionActual.getUsuario(), usuario, expedienteId, "EXP");
              if (isSuplente) asignacionActual.getSuplente().add(usuario);
              else asignacionActual.setUsuario(usuario);
            } else {
              asignacionActual.setUsuarioAsignacion(
                  expediente.getCartera().getResponsableCartera());
              asignacionActual.setUsuario(usuario);
              eventoUtil.anadirEventosPerdidos(usuario, expedienteId, "EXP");
              if (isSuplente)
                throw new Exception(
                    "Para asignar un suplenete es necesario tener un usuario gestor asignado previamente");
            }
            listaAsignaciones.add(asignacionExpedienteRepository.save(asignacionActual));
          }

        } else if (n_asignados < n_expedientes) // Puede gestionar un número limitado de expedientes
        {
          // ojo
          if (asignacionActual != null) {
            if (asignacionActual.getUsuario() != null)
              eventoUtil.reasignarDestinatarios(
                  asignacionActual.getUsuario(), usuario, expedienteId, "EXP");
            asignacionActual.setExpediente(expediente);
            if (isSuplente) asignacionActual.getSuplente().add(usuario);
            else {
              asignacionActual.setUsuario(usuario);
              eventoUtil.anadirEventosPerdidos(usuario, expedienteId, "EXP");
            }
            asignacionActual.setUsuarioAsignacion(expediente.getCartera().getResponsableCartera());
            listaAsignaciones.add(asignacionExpedienteRepository.save(asignacionActual));
          } else {
            throw new Exception("No se puede añadir un suplente si no hay un gestor");
          }
        } else {
          // System.out.println("Fallo");
          throw new IllegalArgumentException(
              "Se ha sobrepasado el límite de expedientes para este usuario numero limite expedientes permitidos:"
                  + n_asignados
                  + "asignados y los que se pueden asignar son"
                  + usuario.getLimiteExpedientes());
          // System.out.println("Se ha sobrepasado el límite de expedientes para este usuario");
        }
      }
    }
  }

  public List<PerfilSimpleOutputDTO> getPerfilesIncluidos(
      List<Integer> expedienteIdList,
      List<Integer> expedientePNIdList,
      boolean todos,
      boolean todosPN,
      BusquedaDto busqueda,
      Usuario usuario)
      throws NotFoundException {

    if (todos) {
      List<Expediente> expedientesLista =
          busquedaUseCase.getDataExpedientesFilter(busqueda, usuario, null, null);
      for (Expediente ex : expedientesLista) {
        expedienteIdList.add(ex.getId());
      }
    }
    if (todosPN) expedientePNIdList = getDataExpedientesPNIdsFilter(busqueda, usuario);

    List<Perfil> perfiles = new ArrayList<>();
    if (expedienteIdList != null && !expedienteIdList.isEmpty())
      perfiles.addAll(
          perfilRepository.findAllDistinctByUsuariosAsignacionExpedientesExpedienteIdIn(
              expedienteIdList));
    if (expedientePNIdList != null && !expedientePNIdList.isEmpty())
      perfiles.addAll(
          perfilRepository.findAllDistinctByUsuariosAsignacionExpedientesPNExpedienteIdIn(
              expedientePNIdList));

    return perfiles.stream().map(PerfilSimpleOutputDTO::new).collect(Collectors.toList());
  }

  public List<AreaUsuarioOutputDTO> getAreaUsuarioIncluidos(
      List<Integer> expedienteIdList,
      List<Integer> expedientePNIdList,
      boolean todos,
      boolean todosPN,
      BusquedaDto busqueda,
      Usuario usuario)
      throws NotFoundException {

    if (todos) {
      List<Expediente> expedientesLista =
          busquedaUseCase.getDataExpedientesFilter(busqueda, usuario, null, null);
      for (Expediente ex : expedientesLista) {
        expedienteIdList.add(ex.getId());
      }
    }
    if (todosPN) expedientePNIdList = getDataExpedientesPNIdsFilter(busqueda, usuario);

    List<AreaUsuario> areaUsuarioes = new ArrayList<>();
    if (expedienteIdList != null && !expedienteIdList.isEmpty())
      areaUsuarioes.addAll(
          areaUsuarioRepository.findAllDistinctByUsuarioAsignacionExpedientesExpedienteIdIn(
              expedienteIdList));
    if (expedientePNIdList != null && !expedientePNIdList.isEmpty())
      areaUsuarioes.addAll(
          areaUsuarioRepository.findAllDistinctByUsuarioAsignacionExpedientesPNExpedienteIdIn(
              expedientePNIdList));

    return areaUsuarioes.stream().map(AreaUsuarioOutputDTO::new).collect(Collectors.toList());
  }

  public List<UsuarioOutputDTO> getUsuariosIncluidos(
      List<Integer> expedienteIdList,
      List<Integer> expedientePNIdList,
      boolean todos,
      boolean todosPN,
      Integer perfil,
      Integer area,
      BusquedaDto busqueda,
      Usuario usuario)
      throws NotFoundException {
    if (todos) {
      List<Expediente> expedientesLista =
          busquedaUseCase.getDataExpedientesFilter(busqueda, usuario, null, null);
      for (Expediente ex : expedientesLista) {
        expedienteIdList.add(ex.getId());
      }
    }
    if (todosPN) expedientePNIdList = getDataExpedientesPNIdsFilter(busqueda, usuario);

    List<Usuario> usuarios = new ArrayList<>();
    if (expedienteIdList != null && !expedienteIdList.isEmpty()) {
      if (area != null) {
        usuarios.addAll(
            usuarioRepository
                .findAllDistinctByAsignacionExpedientesExpedienteIdInAndPerfilIdAndAreasId(
                    expedienteIdList, perfil, area));
      } else {
        usuarios.addAll(
            usuarioRepository.findAllDistinctByAsignacionExpedientesExpedienteIdInAndPerfilId(
                expedienteIdList, perfil));
      }
    }
    if (expedientePNIdList != null && !expedientePNIdList.isEmpty()) {
      List<Usuario> usuariosAux;
      if(area != null){
        usuariosAux = usuarioRepository
          .findAllDistinctByAsignacionExpedientesPNExpedienteIdInAndPerfilIdAndAreasId(
            expedientePNIdList, perfil, area);
      }else{
        usuariosAux =  usuarioRepository
          .findAllDistinctByAsignacionExpedientesPNExpedienteIdInAndPerfilId(
            expedientePNIdList, perfil);
      }
      for (Usuario usuarioAux :usuariosAux) {
        if (!usuarios.contains(usuarioAux)) usuarios.add(usuarioAux);
      }
    }
    return usuarios.stream().map(usuarioMapper::entityToDTO).collect(Collectors.toList());
  }

  @Transactional
  public GestionMasivaExpedientesDTO desasignar(
      List<Integer> expedienteIdList,
      List<Integer> expedientePNIdList,
      Integer usuarioId,
      boolean todos,
      boolean todosPN,
      Usuario usuario) {
    if (todos) asignacionExpedienteRepository.deleteAllByUsuarioId(usuarioId);
    if (todosPN) asignacionExpedientePosesionNegociadaRepository.deleteAllByUsuarioId(usuarioId);
    if (expedienteIdList != null)
      asignacionExpedienteRepository.deleteAllByUsuarioIdAndExpedienteIdIn(
          usuarioId, expedienteIdList);
    if (expedientePNIdList != null)
      asignacionExpedientePosesionNegociadaRepository.deleteAllByUsuarioIdAndExpedienteIdIn(
          usuarioId, expedientePNIdList);
    return null;
  }

  private List<Integer> getDataExpedientesPNIdsFilter(BusquedaDto busquedaDto, Usuario loggedUser)
      throws NotFoundException {
    var tempCriteriaBuilder = entityManager.getCriteriaBuilder();
    var query = tempCriteriaBuilder.createQuery(Integer.class);
    BusquedaBuilderPN busquedaBuilderPN =
        new BusquedaBuilderPN(query.from(ExpedientePosesionNegociada.class), tempCriteriaBuilder);

    List<Predicate> predicates = new ArrayList<>();
    predicates =
        expedientePosesionNegociadaCase.getDataExpedientesPN(
            predicates, busquedaDto, loggedUser, busquedaBuilderPN);

    var rs =
        query
            .select(busquedaBuilderPN.getRoot().get(ExpedientePosesionNegociada_.ID))
            .where(predicates.toArray(new Predicate[predicates.size()]));
    List<Integer> result = entityManager.createQuery(rs).getResultList();

    return result;
  }
}
