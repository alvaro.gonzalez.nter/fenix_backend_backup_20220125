package com.haya.alaska.uso.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.uso.domain.Uso;

@Repository
public interface UsoRepository extends CatalogoRepository<Uso, Integer> {
	 
}
