package com.haya.alaska.uso.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.domain.Catalogo;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_USO")
@Entity
@Table(name = "LKUP_USO")
public class Uso extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USO")
  @NotAudited
  private Set<Bien> bienes = new HashSet<>();
}
