package com.haya.alaska.cliente_cartera.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cliente.domain.Cliente;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_RELA_CLIENTE_CARTERA")
@Entity
@Getter
@Setter
@Table(name = "RELA_CLIENTE_CARTERA")
public class ClienteCartera implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLIENTE")
  private Cliente cliente;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  @Column(name = "DES_ID_HAYA")
  private Integer idHaya;

  @Column(name = "DES_PROPIEDAD_CARTERA")
  private String ciaPropiedadCartera;

  @Column(name = "DES_CIA_FACTURACION")
  private String ciaFacturacion;

  @Column(name = "DES_DOCUMENTO_CIA_FACTURACION")
  private String documentoCiaFacturacion;
}
