package com.haya.alaska.cliente_cartera.infrastructure.repository;

import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ClienteCarteraRepository extends JpaRepository<ClienteCartera, Integer> {
  List<ClienteCartera> findAllByCarteraIdAndClienteId(Integer idCartera, Integer idCliente);
}
