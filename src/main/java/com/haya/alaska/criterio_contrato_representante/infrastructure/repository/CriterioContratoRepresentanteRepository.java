package com.haya.alaska.criterio_contrato_representante.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.criterio_contrato_representante.domain.CriterioContratoRepresentante;

import java.util.List;

@Repository
public interface CriterioContratoRepresentanteRepository
    extends CatalogoRepository<CriterioContratoRepresentante, Integer> {
    Long countByActivoIsTrue();
}
