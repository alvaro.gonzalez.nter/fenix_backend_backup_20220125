package com.haya.alaska.criterio_contrato_representante.domain;

import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;
import com.haya.alaska.catalogo.domain.Catalogo;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_CRITERIO_CONTRATO_REPRESENTANTE")
@Entity
@Table(name = "LKUP_CRITERIO_CONTRATO_REPRESENTANTE")
public class CriterioContratoRepresentante extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CRITERIO_CONTRATO_REPRESENTANTE")
  @NotAudited
  private Set<CarteraContratoRepresentante> criterioContratoCartera;
}
