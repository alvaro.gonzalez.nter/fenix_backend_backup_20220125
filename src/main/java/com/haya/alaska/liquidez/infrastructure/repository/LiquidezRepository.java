package com.haya.alaska.liquidez.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.liquidez.domain.Liquidez;

@Repository
public interface LiquidezRepository extends CatalogoRepository<Liquidez, Integer> {}
