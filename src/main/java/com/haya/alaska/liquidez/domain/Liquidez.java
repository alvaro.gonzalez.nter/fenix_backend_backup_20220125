package com.haya.alaska.liquidez.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.valoracion.domain.Valoracion;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_LIQUIDEZ")
@Entity
@Getter
@Table(name = "LKUP_LIQUIDEZ")
public class Liquidez extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LIQUIDEZ")
  @NotAudited
  private Set<Tasacion> tasaciones = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LIQUIDEZ")
  @NotAudited
  private Set<Valoracion> valoraciones = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LIQUIDEZ")
  @NotAudited
  private Set<ContratoBien> bienes = new HashSet<>();

  @Column(name = "NUM_0_100")
  private Integer n_0_100;

  @Column(name = "NUM_100_250")
  private Integer n_100_250;

  @Column(name = "NUM_250_max")
  private Integer n_250_max;
}
