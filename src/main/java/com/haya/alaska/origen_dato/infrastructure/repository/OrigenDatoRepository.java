package com.haya.alaska.origen_dato.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.origen_dato.domain.OrigenDato;
import org.springframework.stereotype.Repository;

@Repository
public interface OrigenDatoRepository extends CatalogoRepository<OrigenDato, Integer> {}
