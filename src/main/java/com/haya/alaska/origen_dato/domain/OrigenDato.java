package com.haya.alaska.origen_dato.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.datos_origen.domain.DatosOrigen;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_ORIGEN_DATO")
@Entity
@Table(name = "LKUP_ORIGEN_DATO")
public class OrigenDato extends Catalogo {
  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ORIGEN_DATO")
  @NotAudited
  private Set<DatosOrigen> datosOrigenes = new HashSet<>();
}
