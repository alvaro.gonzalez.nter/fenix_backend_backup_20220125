package com.haya.alaska.workflow_propuesta.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.resultado_evento.domain.ResultadoEvento;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_MSTR_WORKFLOW_ELEVACION_PROPUESTA")
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "MSTR_WORKFLOW_ELEVACION_PROPUESTA",
        uniqueConstraints = {@UniqueConstraint(columnNames = {
        "ID_EVENTO_ORIGEN", "ID_RESULTADO_EVENTO"
})})
public class WorkflowElevacionPropuesta implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_CARTERA", referencedColumnName = "id")
    private Cartera cartera;

    @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
    private Boolean activo = true;

    @ManyToOne
    @JoinColumn(name = "ID_EVENTO_ORIGEN", referencedColumnName = "id", nullable = false)
    private SubtipoEvento eventoOrigen;

    @ManyToOne
    @JoinColumn(name = "ID_EVENTO_DESTINO", referencedColumnName = "id")
    private SubtipoEvento eventoDestino;

    @ManyToOne
    @JoinColumn(name = "ID_RESULTADO_EVENTO", referencedColumnName = "id", nullable = false)
    private ResultadoEvento resultadoEvento;

    @ManyToOne
    @JoinColumn(name = "ID_ESTADO_PROPUESTA", referencedColumnName = "id")
    private EstadoPropuesta estadoPropuesta;

    public WorkflowElevacionPropuesta(EstadoPropuesta estadoPropuesta, SubtipoEvento eventoDestino,
                                      SubtipoEvento eventoOrigen, ResultadoEvento resultadoEvento){
        this.estadoPropuesta = estadoPropuesta;
        this.eventoDestino = eventoDestino;
        this.eventoOrigen = eventoOrigen;
        this.resultadoEvento = resultadoEvento;
    }
}
