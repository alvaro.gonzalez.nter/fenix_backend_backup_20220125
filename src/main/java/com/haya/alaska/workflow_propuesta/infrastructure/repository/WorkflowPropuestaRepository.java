package com.haya.alaska.workflow_propuesta.infrastructure.repository;

import com.haya.alaska.workflow_propuesta.domain.WorkflowElevacionPropuesta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkflowPropuestaRepository extends JpaRepository<WorkflowElevacionPropuesta, Integer> {
    List<WorkflowElevacionPropuesta> findAllByEventoOrigenIdAndResultadoEventoId(Integer origenId, Integer resultadoId);
    List<WorkflowElevacionPropuesta> findAllByEventoDestinoIdAndResultadoEventoId(Integer origenId, Integer resultadoId);
  List<WorkflowElevacionPropuesta> findAllByEventoOrigenIdAndResultadoEventoIdAndCarteraId(Integer origenId, Integer resultadoId, Integer idCartera);
  List<WorkflowElevacionPropuesta> findAllByEventoDestinoIdAndResultadoEventoIdAndCarteraId(Integer origenId, Integer resultadoId, Integer idCartera);
  List<WorkflowElevacionPropuesta> findAllByCarteraIdAndEstadoPropuestaId(Integer cartera, Integer estado);
  List<WorkflowElevacionPropuesta> findAllByCarteraId(Integer carteraId);
}
