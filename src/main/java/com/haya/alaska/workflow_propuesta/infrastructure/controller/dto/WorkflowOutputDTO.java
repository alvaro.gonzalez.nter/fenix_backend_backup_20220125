package com.haya.alaska.workflow_propuesta.infrastructure.controller.dto;

import com.haya.alaska.workflow_propuesta.domain.WorkflowElevacionPropuesta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
@Getter
@Setter
@ToString
@NoArgsConstructor
public class WorkflowOutputDTO implements Serializable {
  Integer id;
  String eventoOrigen;
  String eventoDestino;
  String resultadoEvento;
  String estadoPropuesta;

  public WorkflowOutputDTO(WorkflowElevacionPropuesta workflowElevacionPropuesta){
    if (workflowElevacionPropuesta!=null){
      this.id=workflowElevacionPropuesta.getId();
      this.eventoOrigen=workflowElevacionPropuesta.getEventoOrigen().getValor();
      if (workflowElevacionPropuesta.getEventoDestino() != null) {
        this.eventoDestino = workflowElevacionPropuesta.getEventoDestino().getValor();
      }
      if (workflowElevacionPropuesta.getResultadoEvento() != null) {
        this.resultadoEvento = workflowElevacionPropuesta.getResultadoEvento().getValor();
      }
      if (workflowElevacionPropuesta.getEstadoPropuesta() != null) {
        this.estadoPropuesta = workflowElevacionPropuesta.getEstadoPropuesta().getValor();
      }
    }
  }
}
