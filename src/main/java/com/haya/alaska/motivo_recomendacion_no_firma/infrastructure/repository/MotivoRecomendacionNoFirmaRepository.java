package com.haya.alaska.motivo_recomendacion_no_firma.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.motivo_recomendacion_no_firma.domain.MotivoRecomendacionNoFirma;
import org.springframework.stereotype.Repository;

@Repository
public interface MotivoRecomendacionNoFirmaRepository extends CatalogoRepository<MotivoRecomendacionNoFirma, Integer> {
}
