package com.haya.alaska.motivo_recomendacion_no_firma.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@AuditTable(value = "HIST_LKUP_MOTIVO_RECOMENDACION_NO_FIRMA")
@Entity
@Table(name = "LKUP_MOTIVO_RECOMENDACION_NO_FIRMA")
@NoArgsConstructor
public class MotivoRecomendacionNoFirma extends Catalogo {
}
