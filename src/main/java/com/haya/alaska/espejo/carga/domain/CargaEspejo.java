package com.haya.alaska.espejo.carga.domain;

import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.tipo_acreedor.domain.TipoAcreedor;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_CARGA_ESPEJO")
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_CARGA_ESPEJO")
public class CargaEspejo implements Serializable {

	private static final long serialVersionUID = 1L;

	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @Column(name = "ID")
    private Integer id;


  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(name = "RELA_BIEN_CARGA_ESPEJO",
    joinColumns = @JoinColumn(name = "ID_CARGA"),
    inverseJoinColumns = @JoinColumn(name = "ID_BIEN"))
  @AuditJoinTable(name = "HIST_RELA_BIEN_CARGA_ESPEJO")
  private Set<BienEspejo> bienes = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_CARGA")
  private TipoCarga tipoCarga;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ACREEDOR")
  private TipoAcreedor tipoAcreedor;

  @Column(name = "NUM_RANGO")
  private Integer rango;
  @Column(name = "NUM_IMPORTE")
  private Double importe;
  @Column(name = "DES_NOMBRE_ACREEDOR")
  private String nombreAcreedor;
	  @Temporal(TemporalType.DATE)
	  @Column(name = "FCH_ANOTACION")
	  private Date fechaAnotacion;
	  @Column(name = "IND_INCL_OTROS_COLATERALES")
	  private Boolean incluidaOtrosColaterales;
	  @Temporal(TemporalType.DATE)
	  @Column(name = "FCH_VENCIMIENTO")
	  private Date fechaVencimiento;
	  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
	  private Boolean activo = true;

    @Column(name = "IND_VALIDO")
    private Boolean valido;

    @Column(name = "IND_MODIFICADO")
    private Boolean modificado;

    @Column(name = "DES_MODIFICADO", columnDefinition = "varchar(255)")
    private String descripcionMod;
}
