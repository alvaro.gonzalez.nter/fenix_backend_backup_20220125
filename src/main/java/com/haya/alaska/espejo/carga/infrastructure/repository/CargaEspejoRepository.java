package com.haya.alaska.espejo.carga.infrastructure.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.espejo.carga.domain.CargaEspejo;

@Repository
public interface CargaEspejoRepository extends JpaRepository<CargaEspejo, String> {

  List<CargaEspejo> findByBienesId(String id);

}
