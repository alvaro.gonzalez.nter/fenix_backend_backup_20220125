package com.haya.alaska.espejo.saldo.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.espejo.saldo.domain.SaldoEspejo;

@Repository
public interface SaldoEspejoRepository extends JpaRepository<SaldoEspejo, String> {



}
