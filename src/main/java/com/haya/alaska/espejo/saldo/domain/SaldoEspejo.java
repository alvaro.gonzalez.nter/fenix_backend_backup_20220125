package com.haya.alaska.espejo.saldo.domain;

import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_SALDO_ESPEJO")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_SALDO_ESPEJO")
public class SaldoEspejo implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private ContratoEspejo contrato;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_DIA")
  private Date dia;

  @Column(name = "NUM_CAPITAL_PENDIENTE")
  private Double capitalPendiente = 0.0; // Principal pendiente de vencer

	  @Column(name = "NUM_SALDO_GESTION")
	  private Double saldoGestion = 0.0; // Saldo en gestion

	  @Column(name = "NUM_PRINCIPAL_VENCIDO_IMAGADO")
	  private Double principalVencidoImpagado = 0.0; // Principal en impago

	  @Column(name = "NUM_INTERESES_ORDINARIOS_IMPAGADOS")
	  private Double interesesOrdinariosImpagados = 0.0; // Intereses ordinarios en impago

	  @Column(name = "NUM_INTERESES_DEMORA")
	  private Double interesesDemora = 0.0; // Interese en demora

	  @Column(name = "NUM_COMISIONES_IMPAGADOS")
	  private Double comisionesImpagados = 0.0; // Comisiones Impagados. En el front aparece la suma con los gastos impagados

	  @Column(name = "NUM_GASTOS_IMPAGADOS")
	  private Double gastosImpagados = 0.0; // Gastos Impagados. En el front aparece la suma con las comisiones impagados

	  @Column(name = "NUM_IMPORTE_RECUPERADO")
	  private Double importeRecuperado = 0.0; // Importe recuperado = sumatorio del importe de cargos

	  @Column(name = "NUM_DEUDA_IMPAGADA")
	  private Double deudaImpagada = 0.0; // Saldo en impago = principal en impago + intereses (ordinarios y de demora) + comisiones y gastos

	  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
	  private Boolean activo = true;

	  public SaldoEspejo(SaldoEspejo saldo) {
	    BeanUtils.copyProperties(saldo, this, "id");
	    this.contrato = saldo.contrato;
	    this.activo = true;
	  }
}
