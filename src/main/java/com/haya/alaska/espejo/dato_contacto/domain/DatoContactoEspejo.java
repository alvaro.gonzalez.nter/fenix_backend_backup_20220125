package com.haya.alaska.espejo.dato_contacto.domain;

import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_DATO_CONTACTO_ESPEJO")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_DATO_CONTACTO_ESPEJO")
public class DatoContactoEspejo implements Serializable {

	private static final long serialVersionUID = 1L;

	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @Column(name = "ID")
	  @ToString.Include
	  private Integer id;

	  @Column(name = "DES_FIJO")
	  private String fijo;
	  @Column(name = "IND_FIJO_VALIDADO")
	  private Boolean fijoValidado;
	  @Column(name = "DES_MOVIL")
	  private String movil;
	  @Column(name = "IND_MOVIL_VALIDADO")
	  private Boolean movilValidado;
	  @Column(name = "DES_EMAIL")
	  private String email;
	  @Column(name = "IND_EMAIL_VALIDADO")
	  private Boolean emailValidado;
	  @Column(name = "DES_ORIGEN")
	  private String origen;
	  @Column(name = "FCH_ACTUALIZACION")
	  private Date fechaActualizacion;
	  @Column(name = "NUM_ORDEN")
	  private Integer orden;

	  @ManyToOne
	  @JoinColumn(name = "ID_INTERVINIENTE")
	  private IntervinienteEspejo interviniente;

	  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
	  private Boolean activo = true;

    @Column(name = "IND_VALIDO")
    private Boolean valido;

    @Column(name = "IND_MODIFICADO")
    private Boolean modificado;

    @Column(name = "DES_MODIFICADO", columnDefinition = "varchar(255)")
    private String descripcionMod;
}
