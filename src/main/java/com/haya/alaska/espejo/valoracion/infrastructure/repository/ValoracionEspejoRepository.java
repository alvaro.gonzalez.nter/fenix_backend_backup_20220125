package com.haya.alaska.espejo.valoracion.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.espejo.valoracion.domain.ValoracionEspejo;

@Repository
public interface ValoracionEspejoRepository extends JpaRepository<ValoracionEspejo, String> {}
