package com.haya.alaska.espejo.interviniente.infrastructure.repository;

import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface IntervinienteEspejoRepository extends JpaRepository<IntervinienteEspejo, String> {

  Optional<IntervinienteEspejo> findByIdCarga(String idCarga);


  Optional<IntervinienteEspejo> findById(Integer id);

  Optional<IntervinienteEspejo> findByContratosId(Integer contratoId);

  void deleteByIdCarga(String s);


  @Query(
    value =
      "SELECT COUNT(1)   " +
        "FROM MSTR_INTERVINIENTE T1   " +
        "INNER JOIN RELA_CONTRATO_INTERVINIENTE T2 ON T1.ID =T2.ID_INTERVINIENTE   " +
        "INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO =T3.ID   " +
        "INNER JOIN MSTR_EXPEDIENTE T4 ON T3.ID_EXPEDIENTE = T4.ID   " +
        "WHERE T1.IND_ACTIVO = 1   " +
        "AND T1.FCH_CARGA = (SELECT MAX(T1.FCH_CARGA) FROM MSTR_INTERVINIENTE T1   " +
        "INNER JOIN RELA_CONTRATO_INTERVINIENTE T2 ON T1.ID =T2.ID_INTERVINIENTE   " +
        "INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO =T3.ID   " +
        "INNER JOIN MSTR_EXPEDIENTE T4 ON T3.ID_EXPEDIENTE = T4.ID   " +
        "WHERE T4.ID_CARTERA IN(:idCartera))   " +
        "AND T4.ID_CARTERA IN(:idCartera);"
    , nativeQuery = true)
  int findVigentesAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT count(1) " +
        "FROM MSTR_INTERVINIENTE_ESPEJO " +
        "WHERE IND_ACTIVO = 1 "
    , nativeQuery = true)
  int findVigentes();

  @Query(
    value =
      "SELECT CASE WHEN T2.ID_TIPO_INTERVENCION  IS NULL THEN 0    " +
        " ELSE (SELECT DES_VALOR FROM LKUP_TIPO_INTERVENCION WHERE ID = T2.ID_TIPO_INTERVENCION)   " +
        " END AS 'Tipo',  " +
        " COUNT(1) AS 'Contador'   " +
        " FROM MSTR_INTERVINIENTE T1   " +
        " INNER JOIN RELA_CONTRATO_INTERVINIENTE T2 ON T1.ID = T2.ID_INTERVINIENTE    " +
        " INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO =T3.ID   " +
        " INNER JOIN MSTR_EXPEDIENTE T4 ON T4.ID = T3.ID_EXPEDIENTE   " +
        " WHERE T1.IND_ACTIVO = 1    " +
        " AND T1.FCH_CARGA =    " +
        " (SELECT MAX(T1.FCH_CARGA) FROM MSTR_INTERVINIENTE T1   " +
        " INNER JOIN RELA_CONTRATO_INTERVINIENTE T2 ON T1.ID = T2.ID_INTERVINIENTE    " +
        " INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO =T3.ID   " +
        " INNER JOIN MSTR_EXPEDIENTE T4 ON T4.ID = T3.ID_EXPEDIENTE   " +
        " WHERE T1.IND_ACTIVO = 1  " +
        " AND T4.ID_CARTERA IN(:idCartera))    " +
        "AND T4.ID_CARTERA IN(:idCartera)   " +
        " GROUP BY T2.ID_TIPO_INTERVENCION"
    , nativeQuery = true)
  List<Map> findTpIntervinientesAyer(List<Integer> idCartera);

  @Query(
    value = "SELECT CASE WHEN T2.ID_TIPO_INTERVENCION IS NULL THEN 0  " +
      "ELSE (select des_valor from LKUP_TIPO_INTERVENCION WHERE ID = T2.ID_TIPO_INTERVENCION) " +
      "END as 'Tipo', " +
      "COUNT(1) as 'Contador' " +
      "FROM MSTR_INTERVINIENTE_ESPEJO T1 " +
      "INNER JOIN RELA_CONTRATO_INTERVINIENTE_ESPEJO T2 on T1.ID = T2.ID_INTERVINIENTE  " +
      "WHERE T1.IND_ACTIVO = 1  " +
      "AND T1.FCH_CARGA =  " +
      "(SELECT MAX(FCH_CARGA)  " +
      "FROM MSTR_INTERVINIENTE_ESPEJO)  " +
      "GROUP BY T2.ID_TIPO_INTERVENCION"
    , nativeQuery = true)
  List<Map> findTpIntervinientes();

  @Query(
    value =
      "SELECT CASE WHEN ID_TIPO_DOCUMENTO IS NULL THEN 0   " +
        "ELSE (SELECT DES_VALOR FROM LKUP_TIPO_DOCUMENTO WHERE ID = ID_TIPO_DOCUMENTO)   " +
        "END AS 'Tipo',   " +
        "COUNT(1) AS 'Contador'   " +
        "FROM MSTR_INTERVINIENTE T1   " +
        "INNER JOIN RELA_CONTRATO_INTERVINIENTE T2 ON T1.ID = T2.ID_INTERVINIENTE    " +
        " INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO =T3.ID   " +
        " INNER JOIN MSTR_EXPEDIENTE T4 ON T4.ID = T3.ID_EXPEDIENTE   " +
        " WHERE T1.IND_ACTIVO = 1    " +
        " AND T1.FCH_CARGA =   " +
        "(SELECT MAX(T1.FCH_CARGA) FROM MSTR_INTERVINIENTE T1   " +
        "INNER JOIN RELA_CONTRATO_INTERVINIENTE T2 ON T1.ID = T2.ID_INTERVINIENTE    " +
        " INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO =T3.ID   " +
        " INNER JOIN MSTR_EXPEDIENTE T4 ON T4.ID = T3.ID_EXPEDIENTE   " +
        " WHERE T1.IND_ACTIVO = 1   " +
        " AND T4.ID_CARTERA  IN(:idCartera))   " +
        "AND T4.ID_CARTERA  IN(:idCartera)   " +
        "GROUP BY ID_TIPO_DOCUMENTO"
    , nativeQuery = true)
  List<Map> findTpDocumentoAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT CASE WHEN ID_TIPO_DOCUMENTO IS NULL THEN 0 " +
        "ELSE (select des_valor from LKUP_TIPO_DOCUMENTO WHERE id = ID_TIPO_DOCUMENTO) " +
        "END as 'Tipo', " +
        "COUNT(1) as 'Contador' " +
        "FROM MSTR_INTERVINIENTE_ESPEJO " +
        "WHERE IND_ACTIVO = 1 " +
        "AND FCH_CARGA = " +
        "(SELECT MAX(FCH_CARGA) " +
        "FROM MSTR_INTERVINIENTE_ESPEJO) " +
        "GROUP BY ID_TIPO_DOCUMENTO"
    , nativeQuery = true)
  List<Map> findTpDocumento();
}

