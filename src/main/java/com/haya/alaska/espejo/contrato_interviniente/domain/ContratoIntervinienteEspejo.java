package com.haya.alaska.espejo.contrato_interviniente.domain;

import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_RELA_CONTRATO_INTERVINIENTE_ESPEJO")
@Entity
@Getter
@Setter
@Table(name = "RELA_CONTRATO_INTERVINIENTE_ESPEJO")
public class ContratoIntervinienteEspejo implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private ContratoEspejo contrato;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_INTERVINIENTE")
  private IntervinienteEspejo interviniente;

  @Column(name = "NUM_ORDEN_INTERVENCION")
  private Integer ordenIntervencion;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_INTERVENCION")
  private TipoIntervencion tipoIntervencion;

  public boolean isPrimerInterviniente() {
    if (this.getOrdenIntervencion() == null || this.getOrdenIntervencion() != 1) return false;
    return this.getTipoIntervencion().getValor().equals("TITULAR");
  }

}
