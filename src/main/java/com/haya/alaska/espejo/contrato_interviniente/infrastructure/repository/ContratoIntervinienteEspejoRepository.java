package com.haya.alaska.espejo.contrato_interviniente.infrastructure.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.espejo.contrato_interviniente.domain.ContratoIntervinienteEspejo;
import com.haya.alaska.interviniente.domain.Interviniente;

@Repository
public interface ContratoIntervinienteEspejoRepository
    extends JpaRepository<ContratoIntervinienteEspejo, String> {

  Optional<ContratoIntervinienteEspejo> findByContratoIdAndIntervinienteId(
          Integer idContrato, Integer idInterviniente);

  Optional<ContratoIntervinienteEspejo> findByIntervinienteIdAndContratoIdAndOrdenIntervencionAndTipoIntervencionId(String intervinienteId, String contratoId, Integer ordenIntervencion, String tipoIntervencion);
}
