package com.haya.alaska.espejo.direccion.domain;

import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.tipo_via.domain.TipoVia;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_DIRECCION_ESPEJO")
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "MSTR_DIRECCION_ESPEJO")
public class DireccionEspejo implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "IND_PRINCIPAL")
  private Boolean principal = false;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_INTERVINIENTE")
  private IntervinienteEspejo interviniente;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_VIA")
  private TipoVia tipoVia;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ENTORNO")
  private Entorno entorno;

  @Column(name = "DES_NOMBRE")
  private String nombre;
  @Column(name = "DES_NUMERO")
  private String numero;
  @Column(name = "DES_PORTAL")
  private String portal;
  @Column(name = "DES_BLOQUE")
  private String bloque;
  @Column(name = "DES_ESCALERA")
  private String escalera;
  @Column(name = "DES_PISO")
  private String piso;
  @Column(name = "DES_PUERTA")
  private String puerta;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO")
  private Municipio municipio;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LOCALIDAD")
  private Localidad localidad;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA")
  private Provincia provincia;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PAIS")
  private Pais pais;

  @Column(name = "DES_COMENTARIO")
  private String comentario;
  @Column(name = "DES_CODIGO_POSTAL")
  private String codigoPostal;
  @Column(name = "NUM_LONGITUD")
  private Double longitud;
  @Column(name = "NUM_LATITUD")
  private Double latitud;
  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;
  @Column(name = "IND_VALIDADA")
  private Boolean validada;
  @Column(name = "IND_DIRECCION_GARANTIA")
  private Boolean direccionGarantia;
  @Column(name = "DES_ORIGEN")
  private String origen;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ACTUALIZACION")
  private Date fechaActualizacion;

  @Column(name = "IND_VALIDO")
  private Boolean valido;

  @Column(name = "IND_MODIFICADO")
  private Boolean modificado;

  @Column(name = "DES_MODIFICADO", columnDefinition = "varchar(255)")
  private String descripcionMod;

  public DireccionEspejo(
          IntervinienteEspejo interviniente,
          Boolean principal,
          TipoVia tipoVia,
          String nombre,
          String numero,
          String portal,
          String bloque,
          String escalera,
          String piso,
          String puerta,
          Municipio municipio,
          Localidad localidad,
          Entorno entorno,
          Provincia provincia,
          Pais pais,
          String comentario,
          String codigoPostal,
          Double longitud,
          Double latitud,
          Boolean activo,
          Boolean validada,
          Boolean direccionGarantia,
          String origen,
          Date fechaActualizacion) {
    this.interviniente = interviniente;
    this.principal = principal;
    this.tipoVia = tipoVia;
    this.nombre = nombre;
    this.numero = numero;
    this.portal = portal;
    this.bloque = bloque;
    this.escalera = escalera;
    this.piso = piso;
    this.puerta = puerta;
    this.municipio = municipio;
    this.localidad = localidad;
    this.entorno = entorno;
    this.provincia = provincia;
    this.pais = pais;
    this.comentario = comentario;
    this.codigoPostal = codigoPostal;
    this.longitud = longitud;
    this.latitud = latitud;
    this.activo = activo;
    this.validada = validada;
    this.direccionGarantia = direccionGarantia;
    this.origen = origen;
    this.fechaActualizacion = fechaActualizacion;
  }

  public String verEnGoogle() {
    if ((this.latitud != null) && (this.longitud != null)) {
      return "https://www.google.com/maps/search/?api=1&query="
              + this.latitud
              + " ,"
              + this.longitud;
    } else {
      return null;
    }
  }
}
