package com.haya.alaska.espejo.direccion.infrastructure.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.espejo.direccion.domain.DireccionEspejo;

@Repository
public interface DireccionEspejoRepository extends JpaRepository<DireccionEspejo, String> {
  
  List<DireccionEspejo> findByIntervinienteId(String id);

}
