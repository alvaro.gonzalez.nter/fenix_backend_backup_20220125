package com.haya.alaska.espejo.contrato.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.espejo.contrato.infrastructure.repository.ContratoEspejoRepository;

@Component
public class ContratoEspejoUseCaseImpl implements ContratoEspejoUseCase {
	
	public static final String DEUDA = "deuda";
	public static final String RIESGO = "riesgo";
	public static final String ANTIGUEDAD = "ant-deuda";
	public static final String NUMERO = "num";
	public static final String PRODUCTO = "producto";
	
	 @Autowired 
	 private ContratoEspejoRepository contratoRepository;

	@Override
	public List<ContratoEspejo> findAllByExpedienteId(Integer idExpediente,List<CarteraContratoRepresentante>criterios) {
		
		List<Sort.Order> orders = new ArrayList<Sort.Order>();
		for(CarteraContratoRepresentante crit : criterios) {
			Sort.Order order = null;
			if(crit.getOrdenAsc()) {
				order = new Sort.Order(Sort.Direction.ASC, getPropiedadCriterio(crit));
			}else {
				order = new Sort.Order(Sort.Direction.DESC, getPropiedadCriterio(crit));	
			}
			orders.add(order);
		}

		List<ContratoEspejo> contratos = contratoRepository.findAllByExpedienteId(idExpediente, Sort.by(orders));
		return contratos;
	}
	
	private String getPropiedadCriterio (CarteraContratoRepresentante criterio) {
		String resultado = "";
		switch (criterio.getCriterioContratoCartera().getCodigo())
		{
		     case DEUDA:
		    	 resultado = "deudaImpagada";
		    	 break;
		     case RIESGO:
		    	 resultado=  "saldoGestion";
		    	 break;
		     case ANTIGUEDAD:
		    	 resultado =  "fechaPrimerImpago";
		    	 break;
		     case NUMERO:
		    	 resultado = "idCarga";
		    	 break;
		     case PRODUCTO:
		    	// resultado = "producto.codigo";
		    	 resultado = "expediente.cartera.criterioContratoRepresentante.productos.orden";
		    	 break;
		}
		return resultado;
	}
}
