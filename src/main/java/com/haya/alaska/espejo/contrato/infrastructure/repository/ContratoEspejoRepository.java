package com.haya.alaska.espejo.contrato.infrastructure.repository;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface ContratoEspejoRepository extends JpaRepository<ContratoEspejo, String> {

  List<ContratoEspejo> findAllByExpedienteId(String idExpediente);

  Optional<ContratoEspejo> findByIdCarga(String idCarga);

  @Query(value = "select mce.ID, mce.DES_ID_HAYA from mstr_contrato_espejo mce where mce.ID_CARGA = :idCarga", nativeQuery = true)
  List<Object> processContratoEspejo_select(String idCarga);

  List<ContratoEspejo> findAllByExpedienteCartera(Cartera cartera);

  List<ContratoEspejo> findAllByCalculado(String calculado);

  List<ContratoEspejo> findAllByExpedienteId(Integer idExpediente, Sort order);

  @Query(
    value =
      " SELECT C.ID AS IDCONTRATO, SAL.NUM_SALDO_GESTION AS SALDOGESTION, C.ID_CARGA AS IDCARGACONTRATO, I.ID AS ID_INTERVINIENTE, RCI.NUM_ORDEN_INTERVENCION, RCI.ID_TIPO_INTERVENCION FROM " +
        " ALASKA.MSTR_CONTRATO C" +
        " INNER JOIN ALASKA.MSTR_SALDO SAL" +
        "	ON C.ID = SAL.ID_CONTRATO" +
        "	INNER JOIN ALASKA.RELA_CONTRATO_INTERVINIENTE RCI " +
        "	ON C.ID = RCI.ID_CONTRATO " +
        "	RIGHT JOIN ALASKA.MSTR_INTERVINIENTE I" +
        "	ON I.ID = RCI.ID_INTERVINIENTE" +
        "	WHERE  ISNULL( C.ID_EXPEDIENTE)" +
        "	AND RCI.NUM_ORDEN_INTERVENCION = 1 " +
        "	 AND RCI.ID_TIPO_INTERVENCION = (SELECT ID FROM ALASKA.LKUP_TIPO_INTERVENCION TI WHERE TI.DES_VALOR = 'TITULAR')" +
        "	ORDER BY SAL.NUM_SALDO_GESTION DESC, C.ID DESC"
    //"ORDER BY :crit1, :crit2"
    , nativeQuery = true)
  List<ContratoEspejo> findAllByExpedienteIdAndCriterios(String expedienteId,
                                                         String crit1,
                                                         String crit2,
                                                         String crit3,
                                                         String crit4,
                                                         String crit5);

  @Query(
    value =
      "SELECT CASE WHEN ID_PRODUCTO IS NULL THEN 0   " +
        "ELSE (SELECT DES_VALOR FROM LKUP_PRODUCTO WHERE ID = ID_PRODUCTO)   " +
        "END AS 'Tipo',   " +
        "COUNT(1) AS 'Contador'   " +
        "FROM MSTR_CONTRATO T1   " +
        "INNER JOIN MSTR_EXPEDIENTE T2 ON T1.ID_EXPEDIENTE = T2.ID   " +
        "WHERE T1.FCH_CARGA =   " +
        "(SELECT MAX(FCH_CARGA) FROM MSTR_CONTRATO T1   " +
        "INNER JOIN MSTR_EXPEDIENTE T2 ON T1.ID_EXPEDIENTE = T2.ID   " +
        "WHERE T2.ID_CARTERA  IN(:idCartera))   " +
        "AND T2.ID_CARTERA  IN(:idCartera)"
    , nativeQuery = true)
  List<Map> findProductoAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT  CASE WHEN ID_PRODUCTO IS NULL THEN 0 " +
        "ELSE (select des_valor from LKUP_PRODUCTO where id = ID_PRODUCTO) " +
        "END as 'Tipo', " +
        "COUNT(1) as 'Contador' " +
        "FROM MSTR_CONTRATO_ESPEJO " +
        "WHERE FCH_CARGA = " +
        "(SELECT MAX(FCH_CARGA) " +
        "FROM MSTR_CONTRATO_ESPEJO) " +
        "GROUP BY ID_PRODUCTO "
    , nativeQuery = true)
  List<Map> findProducto();

  @Query(
    value =
      "SELECT COUNT(1)    " +
        "FROM MSTR_CONTRATO T1   " +
        "INNER JOIN MSTR_EXPEDIENTE T2 ON T1.ID_EXPEDIENTE = T2.ID  " +
        "WHERE T1.FCH_CARGA = (SELECT MAX(FCH_CARGA) FROM MSTR_CONTRATO T1  " +
        "INNER JOIN MSTR_EXPEDIENTE T2 ON T1.ID_EXPEDIENTE = T2.ID  " +
        "AND T2.ID_CARTERA  IN(:idCartera))   " +
        "AND T2.ID_CARTERA  IN(:idCartera) ",
    nativeQuery = true)
  int findContadordestAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT COUNT(1) " +
        "FROM MSTR_CONTRATO_ESPEJO"
    , nativeQuery = true)
  int findContadordest();

  @Query(
    value =
      "SELECT SUM(T1.NUM_PRINCIPAL_VENCIDO_IMAGADO) FROM MSTR_SALDO T1 " +
        "INNER JOIN MSTR_CONTRATO T2 ON T1.ID_CONTRATO = T1.ID " +
        "INNER JOIN MSTR_EXPEDIENTE T3 ON T2.ID_EXPEDIENTE = T3.ID " +
        "AND T3.ID_CARTERA IN(:idCartera)"
    , nativeQuery = true)
  Double findSumaImpagodestAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT SUM(NUM_PRINCIPAL_VENCIDO_IMAGADO) " +
        "FROM MSTR_SALDO_ESPEJO"
    , nativeQuery = true)
  Double findSumaImpagodest();

  @Query(
    value =
      "SELECT SUM(T1.NUM_CAPITAL_PENDIENTE) FROM MSTR_SALDO T1 " +
        "INNER JOIN MSTR_CONTRATO T2 ON T1.ID_CONTRATO = T1.ID " +
        "INNER JOIN MSTR_EXPEDIENTE T3 ON T2.ID_EXPEDIENTE = T3.ID " +
        "AND T3.ID_CARTERA IN(:idCartera)"
    , nativeQuery = true)
  Double findSumaPendientedestAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT SUM(NUM_CAPITAL_PENDIENTE) " +
        "FROM MSTR_SALDO_ESPEJO"
    , nativeQuery = true)
  Double findSumaPendientedest();

  @Query(
    value =
      "SELECT SUM(T1.NUM_DEUDA_IMPAGADA) FROM MSTR_SALDO T1 " +
        "INNER JOIN MSTR_CONTRATO T2 ON T1.ID_CONTRATO = T1.ID " +
        "INNER JOIN MSTR_EXPEDIENTE T3 ON T2.ID_EXPEDIENTE = T3.ID " +
        "AND T3.ID_CARTERA IN(:idCartera)"
    , nativeQuery = true)
  Double findSumaDeudadestAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT SUM(NUM_DEUDA_IMPAGADA)" +
        "FROM MSTR_SALDO_ESPEJO"
    , nativeQuery = true)
  Double findSumaDeudadest();

  @Query(
    value =
      "SELECT CASE WHEN ID_CLASIFICACION_CONTABLE IS NULL THEN 0   " +
        "ELSE (SELECT DES_VALOR FROM LKUP_CLASIFICACION_CONTABLE WHERE ID = ID_CLASIFICACION_CONTABLE)   " +
        "END AS 'Tipo',   " +
        "COUNT(1) AS 'Contador'   " +
        "FROM MSTR_CONTRATO T1   " +
        "INNER JOIN MSTR_EXPEDIENTE T2 ON T1.ID_EXPEDIENTE = T2.ID   " +
        "WHERE T1.FCH_CARGA =   " +
        "(SELECT MAX(FCH_CARGA) FROM MSTR_CONTRATO T1   " +
        "INNER JOIN MSTR_EXPEDIENTE T2 ON T1.ID_EXPEDIENTE = T2.ID   " +
        "WHERE T2.ID_CARTERA IN(:idCartera))   " +
        "AND T2.ID_CARTERA IN(:idCartera)"
    , nativeQuery = true)
  List<Map> findTpContableAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT CASE WHEN ID_CLASIFICACION_CONTABLE IS NULL THEN 0 " +
        "ELSE (select des_valor from lkup_clasificacion_contable where id = ID_CLASIFICACION_CONTABLE) " +
        "END as 'Tipo', " +
        "COUNT(1) as 'Contador' " +
        "FROM MSTR_CONTRATO_ESPEJO " +
        "WHERE FCH_CARGA = " +
        "(SELECT MAX(FCH_CARGA) " +
        "FROM MSTR_CONTRATO_ESPEJO) " +
        "GROUP BY ID_CLASIFICACION_CONTABLE "
    , nativeQuery = true)
  List<Map> findTpContable();
}
