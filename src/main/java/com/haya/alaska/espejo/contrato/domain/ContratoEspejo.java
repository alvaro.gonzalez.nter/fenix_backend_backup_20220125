package com.haya.alaska.espejo.contrato.domain;

import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable;
import com.haya.alaska.espejo.contrato_bien.domain.ContratoBienEspejo;
import com.haya.alaska.espejo.contrato_interviniente.domain.ContratoIntervinienteEspejo;
import com.haya.alaska.espejo.expediente.domain.ExpedienteEspejo;
import com.haya.alaska.espejo.movimiento.domain.MovimientoEspejo;
import com.haya.alaska.espejo.saldo.domain.SaldoEspejo;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.indice_referencia.domain.IndiceReferencia;
import com.haya.alaska.periodicidad_liquidacion.domain.PeriodicidadLiquidacion;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.sistema_amortizacion.domain.SistemaAmortizacion;
import com.haya.alaska.situacion.domain.Situacion;
import com.haya.alaska.tipo_reestructuracion.domain.TipoReestructuracion;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Audited
@AuditTable(value = "HIST_MSTR_CONTRATO_ESPEJO")
@Entity
@Getter
@Setter
@Table(name = "MSTR_CONTRATO_ESPEJO")
public class ContratoEspejo implements Serializable {

  private static final long serialVersionUID = 1L;


  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE")
  private ExpedienteEspejo expediente;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private Set<ContratoBienEspejo> bienes = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private Set<SaldoEspejo> saldos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private Set<ContratoIntervinienteEspejo> intervinientes = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private Set<MovimientoEspejo> movimientos = new HashSet<>();

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
    name = "RELA_CONTRATO_PROCEDIMIENTO",
    joinColumns = @JoinColumn(name = "ID_CONTRATO"),
    inverseJoinColumns = @JoinColumn(name = "ID_PROCEDIMIENTO"))
  private Set<Procedimiento> procedimientos = new HashSet<>();

  @Column(name = "IND_REPRESENTANTE")
  private Boolean esRepresentante;
  @Column(name = "DES_ID_HAYA")
  private String idHaya;
  @Column(name = "DES_ID_ORIGEN")
  private String idOrigen;
  @Column(name = "DES_ID_ORIGEN_2")
  private String idOrigen2;
  @Column(name = "DES_ID_DATATAPE")
  private String idDatatape;
  @Column(name = "IND_EN_REVISION")
  private Boolean enRevision;
  @Column(name = "IND_VENCIMIENTO_ANTICIPADO")
  private Boolean vencimientoAnticipado;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PRODUCTO")
  private Producto producto;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SISTEMA_AMORTIZACION")
  private SistemaAmortizacion sistemaAmortizacion;

  @Column(name = "DES_PRODUCTO")
  private String descripcionProducto;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERIOCIDAD_LIQUIDACION")
  private PeriodicidadLiquidacion periodicidadLiquidacion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_INDICE_REFERENCIA")
  private IndiceReferencia indiceReferencia;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SITUACION")
  private Situacion situacion;

  @Column(name = "NUM_DIFERENCIAL")
  private Double diferencial;
  @Column(name = "NUM_TIN")
  private Double tin;
  @Column(name = "NUM_TAE")
  private Double tae;
  @Column(name = "IND_REESTRUCTURADO")
  private Boolean reestructurado;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_REESTRUCTURACION")
  private TipoReestructuracion tipoReestructuracion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLASIFICACION_CONTABLE")
  private ClasificacionContable clasificacionContable;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FORMALIZACION")
  private Date fechaFormalizacion;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_VENC_INICIAL")
  private Date fechaVencimientoInicial;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_VENC_ANTICIPADO")
  private Date fechaVencimientoAnticipado;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PASE_REGULAR")
  private Date fechaPaseRegular;
  @Column(name = "NUM_CUOTAS_PENDIENTES")
  private Integer cuotasPendientes;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_LIQUIDACION_CUOTA")
  private Date fechaLiquidacionCuota;
  @Column(name = "NUM_IMPORTE_INICIAL")
  private Double importeInicial;
  @Column(name = "NUM_IMPORTE_DISPUESTO")
  private Double importeDispuesto;
  @Column(name = "NUM_CUOTAS_TOTALES")
  private Integer cuotasTotales;
  @Column(name = "NUM_IMPORTE_ULTIMA_CUOTA")
  private Double importeUltimaCuota;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PRIMER_IMPAGO")
  private Date fechaPrimerImpago;
  @Column(name = "NUM_DIAS_IMPAGO")
  private Integer diasImpago;
  @Column(name = "NUM_CUOTAS_IMPAGADAS")
  private Integer numeroCuotasImpagadas;
  @Column(name = "NUM_RESTO_HIPOTECARIO")
  private Double restoHipotecario;
  @Column(name = "DES_ORIGEN")
  private String origen;
  @Column(name = "NUM_GASTOS_JUDICIALES")
  private Double gastosJudiciales;
  @Column(name = "NUM_IMPORTE_CUOTAS_IMPAGADAS")
  private Double importeCuotasImpagadas;
  @Column(name = "DES_NOTAS_1")
  private String notas1;
  @Column(name = "DES_NOTAS_2")
  private String notas2;
  @Column(name = "DES_COMPANIA")
  private String campania;
  @Column(name = "NUM_DEUDA_IMAGADA")
  private Double deudaImpagada;
  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;
  @Column(name = "ID_CARGA")
  private String idCarga;
  @Column(name = "DES_CARTERA_REM")
  private String carteraREM;
  @Column(name = "DES_SUBCARTERA_REM")
  private String subcarteraREM;
  @Column(name = "DES_RANGO_HIPOTECARIO")
  private String rangoHipotecario;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_CONTRATO")
  private EstadoContrato estadoContrato;

  //columnas de apoyo para el cálculo de xontratoRepresentante
  @Column(name = "IND_CALCULADO")
  private Boolean calculado;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CARGA")
  private Date fechaCarga;

  @Column(name = "NUM_SALDO_GESTION")
  private Double saldoGestion;

  public boolean isRepresentante() {
    if (this.esRepresentante == null) return false;
    return this.esRepresentante;
  }

  public SaldoEspejo getSaldoContrato() {
    if (this.getSaldos().isEmpty()) {
      return new SaldoEspejo();
    }
    List<SaldoEspejo> saldosOrdenados = new ArrayList<>(this.saldos);
    saldosOrdenados.sort((a, b) -> b.getDia().compareTo(a.getDia()));
    return !saldosOrdenados.isEmpty() ? saldosOrdenados.get(0) : new SaldoEspejo();
  }
}
