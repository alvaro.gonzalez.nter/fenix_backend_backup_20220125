package com.haya.alaska.espejo.contrato.application;

import java.util.List;
import java.util.Map;

import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;

public interface ContratoEspejoUseCase {

	  List<ContratoEspejo> findAllByExpedienteId (Integer idExpediente, List<CarteraContratoRepresentante> criterios);
}
