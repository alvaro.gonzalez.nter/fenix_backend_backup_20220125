package com.haya.alaska.espejo.expediente.infrastructure.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.espejo.expediente.domain.ExpedienteEspejo;

@Repository
public interface ExpedienteEspejoRepository extends JpaRepository<ExpedienteEspejo, String> {

  Optional<ExpedienteEspejo> findByIdCarga(String idCarga);
  Optional<ExpedienteEspejo> findByIdCargaAndCarteraIdCargaAndIdOrigen(String idCarga, String carteraIdCarga,String idOrigen);
}
