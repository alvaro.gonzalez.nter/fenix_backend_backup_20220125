package com.haya.alaska.espejo.movimiento.infrastructure.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.espejo.movimiento.domain.MovimientoEspejo;

@Repository
public interface MovimientoEspejoRepository extends JpaRepository<MovimientoEspejo, String> {
 
  Optional<MovimientoEspejo> findByIdCarga(String idCarga);
}
