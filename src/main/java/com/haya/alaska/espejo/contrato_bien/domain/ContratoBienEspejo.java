package com.haya.alaska.espejo.contrato_bien.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;

import lombok.Getter;
import lombok.Setter;

@Audited
@AuditTable(value = "HIST_RELA_CONTRATO_BIEN_ESPEJO")
@Entity
@Getter
@Setter
@Table(name = "RELA_CONTRATO_BIEN_ESPEJO")
public class ContratoBienEspejo implements Serializable {
	private static final long serialVersionUID = 1L;

	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @Column(name = "ID")
	  private Integer id;

	  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
	  private Boolean activo = true;

	  @ManyToOne(
	      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	  @JoinColumn(name = "ID_CONTRATO")
	  private ContratoEspejo contrato;

	  @ManyToOne(
	      cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH})
	  @JoinColumn(name = "ID_BIEN")
	  private BienEspejo bien;

	  @ManyToOne(
	      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	  @JoinColumn(name = "ID_TIPO_GARANTIA")
	  private TipoGarantia tipoGarantia;

	  @Column(name = "IMP_GARANTIZADO")
	  private Double importeGarantizado;

	  @Column(name = "NUM_RESP_HIPOTECARIA")
	  private Double responsabilidadHipotecaria;

	  @Column(name = "NUM_RANGO")
	  private Double rango;

	  @Column(name = "IND_ESTADO")
	  private Boolean estado; //true = activo, false = liberado

	  @ManyToOne(
	      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	  @JoinColumn(name = "ID_LIQUIDEZ")
	  private Liquidez liquidez;
}
