package com.haya.alaska.espejo.contrato_bien.infrastructure.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.haya.alaska.espejo.contrato_bien.domain.ContratoBienEspejo;

public interface ContratoBienEspejoRepository extends JpaRepository<ContratoBienEspejo, String> {

	  Optional<ContratoBienEspejo> findByContratoIdAndBienId(
	          Integer idContrato, Integer idBien);

}
