package com.haya.alaska.espejo.tasacion.infrastructure.repository;

import com.haya.alaska.espejo.tasacion.domain.TasacionEspejo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TasacionEspejoRepository extends JpaRepository<TasacionEspejo, String> {
  @Query(
    value =
      "SELECT SUM(T1.NUM_IMPORTE) " +
        "FROM MSTR_TASACION T1 " +
        "INNER JOIN MSTR_BIEN T2 ON T1.ID_BIEN = T2.ID " +
        "INNER JOIN RELA_CONTRATO_BIEN T3 on T3.ID_BIEN = T1.ID " +
        "INNER JOIN MSTR_CONTRATO T4 on T3.ID_CONTRATO = T4.ID " +
        "INNER JOIN MSTR_EXPEDIENTE T5 on T4.ID_EXPEDIENTE = T5.ID " +
        "WHERE T1.FCH_FECHA = (SELECT MAX(FCH_FECHA) FROM MSTR_TASACION T1  " +
        "INNER JOIN MSTR_BIEN T2 ON T1.ID_BIEN = T2.ID  " +
        "INNER JOIN RELA_CONTRATO_BIEN T3 on T3.ID_BIEN = T1.ID  " +
        "INNER JOIN MSTR_CONTRATO T4 on T3.ID_CONTRATO = T4.ID  " +
        "INNER JOIN MSTR_EXPEDIENTE T5 on T4.ID_EXPEDIENTE = T5.ID  " +
        "and T5.ID_CARTERA IN(:idCartera)) " +
        "AND T5.ID_CARTERA IN(:idCartera)"
    , nativeQuery = true)
  Double findSumaImporteAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT SUM(NUM_IMPORTE) " +
        "FROM MSTR_TASACION_ESPEJO"
    , nativeQuery = true)
  Double findSumaImporte();

}
