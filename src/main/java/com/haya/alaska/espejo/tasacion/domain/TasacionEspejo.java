package com.haya.alaska.espejo.tasacion.domain;

import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.tipo_tasacion.domain.TipoTasacion;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_TASACION_ESPEJO")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_TASACION_ESPEJO")
public class TasacionEspejo implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  private BienEspejo bien;

  private String tasadora;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_TASACION")
  private TipoTasacion tipoTasacion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LIQUIDEZ")
  private Liquidez liquidez;

  @Column(name = "NUM_IMPORTE")
  private Double importe;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA")
  private Date fecha;

	  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
	  private Boolean activo = true;

    @Column(name = "IND_VALIDO")
    private Boolean valido;

    @Column(name = "IND_MODIFICADO")
    private Boolean modificado;

    @Column(name = "DES_MODIFICADO", columnDefinition = "varchar(255)")
    private String descripcionMod;
 }
