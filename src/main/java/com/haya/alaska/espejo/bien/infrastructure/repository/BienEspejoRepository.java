package com.haya.alaska.espejo.bien.infrastructure.repository;

import com.haya.alaska.espejo.bien.domain.BienEspejo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BienEspejoRepository extends JpaRepository<BienEspejo, String> {

  Optional<BienEspejo> findByIdCarga(String idCarga);

  @Query(
    value =
      "SELECT DES_ID_HAYA " +
        "FROM MSTR_CARTERA " +
        "WHERE ID = ?1"
    , nativeQuery = true)
  String findId(Integer id);

  @Query(
    value =
      "SELECT DES_ID_HAYA " +
        "FROM MSTR_CARTERA " +
        "WHERE DES_ID_CARGA = ?1"
    , nativeQuery = true)
  String findCliente(String id);

  @Query(
    value =
      "SELECT count(1)    " +
        "FROM MSTR_BIEN T1   " +
        "INNER JOIN RELA_CONTRATO_BIEN T2 ON T1.ID =T2.ID_BIEN    " +
        "INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO = T3.ID    " +
        "INNER JOIN MSTR_EXPEDIENTE T4 ON T3.ID_EXPEDIENTE = T4.ID    " +
        "WHERE T1.FCH_CARGA =    " +
        "(SELECT MAX(T1.FCH_CARGA) FROM MSTR_BIEN T1   " +
        "INNER JOIN RELA_CONTRATO_BIEN T2 ON T1.ID =T2.ID_BIEN    " +
        "INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO = T3.ID    " +
        "INNER JOIN MSTR_EXPEDIENTE T4 ON T3.ID_EXPEDIENTE = T4.ID    " +
        "where T4.ID_CARTERA IN(:idCartera))   " +
        "AND T4.ID_CARTERA IN(:idCartera)"
    , nativeQuery = true)
  int findEstadoAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT count(1) " +
        "FROM MSTR_BIEN_ESPEJO "
    , nativeQuery = true)
  int findEstado();

  @Query(
    value =
      "SELECT COUNT(1)    " +
        "FROM MSTR_BIEN T1   " +
        "INNER JOIN RELA_CONTRATO_BIEN T2 ON T1.ID =T2.ID_BIEN    " +
        "INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO = T3.ID    " +
        "INNER JOIN MSTR_EXPEDIENTE T4 ON T3.ID_EXPEDIENTE = T4.ID    " +
        "WHERE T1.DES_REF_CATASTRAL IS NOT NULL    " +
        "AND T1.FCH_CARGA =    " +
        "(SELECT MAX(T1.FCH_CARGA) FROM MSTR_BIEN T1   " +
        "INNER JOIN RELA_CONTRATO_BIEN T2 ON T1.ID =T2.ID_BIEN    " +
        "INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO = T3.ID    " +
        "INNER JOIN MSTR_EXPEDIENTE T4 ON T3.ID_EXPEDIENTE = T4.ID    " +
        "WHERE T1.DES_REF_CATASTRAL IS NOT NULL  " +
        "AND T4.ID_CARTERA IN(:idCartera))   " +
        "AND T4.ID_CARTERA IN(:idCartera)"
    , nativeQuery = true)
  int findRefCatastralAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT count(1) " +
        "FROM MSTR_BIEN_ESPEJO " +
        "WHERE DES_REF_CATASTRAL IS NOT NULL "
    , nativeQuery = true)
  int findRefCatastral();

  @Query(
    value =
      "SELECT COUNT(1)    " +
        "FROM MSTR_BIEN T1   " +
        "INNER JOIN RELA_CONTRATO_BIEN T2 ON T1.ID =T2.ID_BIEN    " +
        "INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO = T3.ID    " +
        "INNER JOIN MSTR_EXPEDIENTE T4 ON T3.ID_EXPEDIENTE = T4.ID    " +
        "WHERE T1.ID_TIPO_ACTIVO IS NOT NULL    " +
        "AND T1.FCH_CARGA =    " +
        "(SELECT MAX(T1.FCH_CARGA) FROM MSTR_BIEN T1   " +
        "INNER JOIN RELA_CONTRATO_BIEN T2 ON T1.ID =T2.ID_BIEN    " +
        "INNER JOIN MSTR_CONTRATO T3 ON T2.ID_CONTRATO = T3.ID    " +
        "INNER JOIN MSTR_EXPEDIENTE T4 ON T3.ID_EXPEDIENTE = T4.ID    " +
        "WHERE T1.ID_TIPO_ACTIVO IS NOT NULL  " +
        "AND T4.ID_CARTERA IN(:idCartera))   " +
        "AND T4.ID_CARTERA IN(:idCartera)"
    , nativeQuery = true)
  int findActivosAyer(List<Integer> idCartera);

  @Query(
    value =
      "SELECT count(1) " +
        "FROM MSTR_BIEN_ESPEJO " +
        "WHERE ID_TIPO_ACTIVO IS NOT NULL "
    , nativeQuery = true)
  int findActivos();

  @Query(value = "SELECT T1.ID_CARGA 'idBien' FROM MSTR_BIEN T1  " +
    "LEFT JOIN MSTR_BIEN_ESPEJO T2 on T1.ID_CARGA = T2.ID_CARGA  " +
    "INNER JOIN RELA_CONTRATO_BIEN T3 on T3.ID_BIEN = T1.ID  " +
    "INNER JOIN MSTR_CONTRATO T4 on T3.ID_CONTRATO = T4.ID  " +
    "INNER JOIN MSTR_EXPEDIENTE T5 on T4.ID_EXPEDIENTE = T5.ID  " +
    "WHERE T1.FCH_CARGA = (select MAX(FCH_CARGA) FROM MSTR_BIEN) " +
    "AND T2.ID_CARGA is null " +
    "AND T5.ID_CARTERA  IN(:idCartera)", nativeQuery = true)
  List<String> findBienesNoCargados(List<Integer> idCartera);

  @Query(value = "SELECT COUNT(DISTINCT(T1.ID_CARGA)) FROM MSTR_BIEN T1  " +
    "LEFT JOIN MSTR_BIEN_ESPEJO T2 on T1.ID_CARGA = T2.ID_CARGA  " +
    "INNER JOIN RELA_CONTRATO_BIEN T3 on T3.ID_BIEN = T1.ID  " +
    "INNER JOIN MSTR_CONTRATO T4 on T3.ID_CONTRATO = T4.ID  " +
    "INNER JOIN MSTR_EXPEDIENTE T5 on T4.ID_EXPEDIENTE = T5.ID  " +
    "WHERE T1.FCH_CARGA = (select MAX(FCH_CARGA) FROM MSTR_BIEN) " +
    "AND T2.ID_CARGA is null " +
    "AND T5.ID_CARTERA  IN(:idCartera)", nativeQuery = true)
  int findContratosLiberacion(List<Integer> idCartera);
}

