package com.haya.alaska.espejo.bien.domain;

import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.espejo.carga.domain.CargaEspejo;
import com.haya.alaska.espejo.contrato_bien.domain.ContratoBienEspejo;
import com.haya.alaska.espejo.tasacion.domain.TasacionEspejo;
import com.haya.alaska.espejo.valoracion.domain.ValoracionEspejo;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.uso.domain.Uso;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;


@Audited
@AuditTable(value = "HIST_MSTR_BIEN_ESPEJO")
@Entity
@Setter
@Getter
@Table(name = "MSTR_BIEN_ESPEJO")
public class BienEspejo implements Serializable {

	private static final long serialVersionUID = 1L;

	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  @Column(name = "ID")
	  @ToString.Include
	  private Integer id;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;
  @Column(name = "DES_ID_DATATAPE")
  private String idDatatape;
  @Column(name = "DES_ID_ORIGEN")
  private String idOrigen;
  @Column(name = "DES_ID_ORIGEN_2")
  private String idOrigen2;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  private Set<ContratoBienEspejo> contratos = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_BIEN")
  private TipoBien tipoBien;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SUB_TIPO_BIEN")
  private SubTipoBien subTipoBien;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  private Set<TasacionEspejo> tasaciones = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  private Set<ValoracionEspejo> valoraciones = new HashSet<>();

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
    name = "RELA_BIEN_CARGA_ESPEJO",
    joinColumns = @JoinColumn(name = "ID_BIEN"),
    inverseJoinColumns = @JoinColumn(name = "ID_CARGA"))
    @AuditJoinTable(name = "HIST_RELA_BIEN_CARGA_ESPEJO")
	  private Set<CargaEspejo> cargas = new HashSet<>();

	  @Column(name = "DES_REF_CATASTRAL")
	  private String referenciaCatastral;
	  @Column(name = "DES_FINCA")
	  private String finca;
	  @Column(name = "DES_LOC_REGISTRO")
	  private String localidadRegistro;
	  @Column(name = "DES_REGISTRO")
	  private String registro;
	  @Column(name = "DES_TOMO")
	  private String tomo;
	  @Column(name = "DES_LIBRO")
	  private String libro;
	  @Column(name = "DES_FOLIO")
	  private String folio;
	  @Temporal(TemporalType.DATE)
	  @Column(name = "FCH_INSCRIPCION")
	  private Date fechaInscripcion;
	  @Column(name = "DES_IDUFIR")
	  private String idufir;
  @Column(name = "DES_REO_ORIGEN")
  private String reoOrigen;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_REO_COVERSION")
  private Date fechaReoConversion;
  @Column(name = "IND_POS_NEGOCIADA")
  private Boolean posesionNegociada;
  @Column(name = "IND_SUBASTA")
  private Boolean subasta;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  private Set<BienSubastado> bienesSubastados = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_VIA")
  private TipoVia tipoVia;

  @Column(name = "DES_NOMBRE_VIA")
  private String nombreVia;
  @Column(name = "DES_NUMERO")
  private String numero;
  @Column(name = "DES_PORTAL")
  private String portal;
  @Column(name = "DES_BLOQUE")
  private String bloque;
  @Column(name = "DES_ESCALERA")
  private String escalera;
  @Column(name = "DES_PISO")
  private String piso;
  @Column(name = "DES_PUERTA")
  private String puerta;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ENTORNO")
  private Entorno entorno;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO")
  private Municipio municipio;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LOCALIDAD")
  private Localidad localidad;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA")
  private Provincia provincia;

  @Column(name = "DES_COMENT_DIRECC")
  private String comentarioDireccion;
  @Column(name = "DES_COD_POSTAL")
  private String codigoPostal;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PAIS")
  private Pais pais;

  @Column(name = "NUM_LATITUD")
  private Double latitud;
  @Column(name = "ID_LONGITUD")
  private Double longitud;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ACTIVO")
  private TipoActivo tipoActivo;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USO")
  private Uso uso;

  @Column(name = "NUM_ANIO_CONTRUCT")
  private Integer anioConstruccion;
  @Column(name = "NUM_SUPERF_SUELO")
  private Double superficieSuelo;
  @Column(name = "NUM_SUPERF_CONTRUIDA")
  private Double superficieConstruida;
	  @Column(name = "NUM_SUPERF_UTIL")
	  private Double superficieUtil;
	  @Column(name = "DES_ANEJO_GARAJE")
	  private String anejoGaraje;
	  @Column(name = "DES_ANEJO_TRASTERO")
	  private String anejoTrastero;
	  @Column(name = "DES_ANEJO_OTROS")
	  private String anejoOtros;
	  @Column(name = "DES_NOTAS_1")
	  private String notas1;
	  @Column(name = "DES_NOTAS_2")
	  private String notas2;
	  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
	  private Boolean activo = true;
	  @Column(name = "ID_CARGA")
	  private String idCarga;
	  @Column(name = "DES_CARTERA_REM")
	  private String carteraREM;
	  @Column(name = "DES_SUBCARTERA_REM")
	  private String subcarteraREM;
    @Column(name = "IMP_GARANTIZADO")
    private Double importeGarantizado;
    @Column(name = "NUM_RESP_HIPOTECARIA")
    private Double responsabilidadHipotecaria;
    @Column(name = "IND_TRAMITADO", nullable = false, columnDefinition = "boolean default false")
    private Boolean tramitado = false;
    @Column(name = "NUM_RANGO")
    private Double rango;
    @Column(name="IMPORTE_BIEN")
    private Double importeBien;

	  @Column(name = "NUM_VALOR")
	  private Double valor;
	  @Column(name = "DES_DESCRIPCION")
	  private String descripcion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CARGA")
  private Date fechaCarga;

   @Column(name = "IND_VALIDO")
   private Boolean valido;

  @Column(name = "IND_MODIFICADO")
  private Boolean modificado;

  @Column(name = "DES_MODIFICADO", columnDefinition = "varchar(255)")
  private String descripcionMod;

	  public Double getValor() {
	    if (this.getTipoActivo() != null && this.getTipoActivo().getValor().equals("Inmobiliario")) {
	      if (!this.getTasaciones().isEmpty()) {
	        return getUltimaTasacion().getImporte();
	      }
	    } else if (!this.getValoraciones().isEmpty()) {
	      return getUltimaValoracion().getImporte();
	    }
	    return 0.0;
	  }

	  public TasacionEspejo getUltimaTasacion() {
	    return this.tasaciones.stream().max(Comparator.comparing(TasacionEspejo::getFecha)).orElse(null);
	  }

	  public ValoracionEspejo getUltimaValoracion() {
	    return this.valoraciones.stream().max(Comparator.comparing(ValoracionEspejo::getFecha)).orElse(null);
	  }

	  // TODO Descomentar en caso de que sea necesario enviar la lista ordenada

	  //  public List<Tasacion> getSortedTasaciones() {
	  //    List<Tasacion> tasacionesOrdenadas = new ArrayList<>(this.tasaciones);
	  //    // TODO comprobar si está ordenado descendentemente
	  //    tasacionesOrdenadas.sort((a, b) -> b.getFecha().compareTo(a.getFecha()));
	  //    return tasacionesOrdenadas;
	  //  }
	  //
	  //  public List<Valoracion> getSortedValoraciones() {
	  //    List<Valoracion> valoracionesOrdenadas = new ArrayList<>(this.valoraciones);
	  //    // TODO comprobar si está ordenado descendentemente
	  //    valoracionesOrdenadas.sort((a, b) -> b.getFecha().compareTo(a.getFecha()));
	  //    return valoracionesOrdenadas;
	  //  }

	  public String getDescripcion() {
	    if (this.getTipoBien() != null && this.getSubTipoBien() != null) {
	      return this.getTipoBien().getValor().concat(" ").concat(this.getSubTipoBien().getValor());
	    } else if (this.getTipoBien() != null) {
	      return this.getTipoBien().getValor();
	    } else if (this.getSubTipoBien() != null) { // TODO no tiene sentido
	      return this.getSubTipoBien().getValor();
	    }
	    return "";
	  }

	  public boolean isMismaDireccion(Direccion d) {
	    if (this.latitud != null
	            && this.longitud != null
	            && d.getLatitud() != null
	            && d.getLongitud() != null) {
	      if (this.latitud.equals(d.getLatitud()) && this.longitud.equals(d.getLongitud())) {
	        return true;
	      }
	    }
	    Boolean isNombreVia = compareIfNotNull(this.nombreVia, d.getNombre());
	    Boolean isMunicipio = compareIfNotNull(this.municipio, d.getMunicipio());
	    Boolean isProvincia = compareIfNotNull(this.provincia, d.getProvincia());

	    if (isNombreVia != null && isMunicipio != null && isProvincia != null) {
	      if (!isNombreVia || !isMunicipio || !isProvincia) {
	        return false;
	      }
	    } else {
	      return false;
	    }

	    if (!Objects.equals(this.numero, d.getNumero())) {
	      return false;
	    }
	    if (!Objects.equals(this.portal, d.getPortal())) {
	      return false;
	    }
	    if (!Objects.equals(this.bloque, d.getBloque())) {
	      return false;
	    }
	    if (!Objects.equals(this.escalera, d.getEscalera())) {
	      return false;
	    }
	    if (!Objects.equals(this.piso, d.getPiso())) {
	      return false;
	    }
	    if (!Objects.equals(this.puerta, d.getPuerta())) {
	      return false;
	    }
	    return Objects.equals(this.pais, d.getPais());
	  }

	  private static Boolean compareIfNotNull(Object a, Object b) {
	    if (a == null || b == null) return null;
	    return Objects.equals(a, b);
	  }

	  public ContratoBienEspejo getContratoBien(String contrato) {
	    for (ContratoBienEspejo cb : this.getContratos()) {
	      if (cb.getContrato().getId().equals(contrato)) {
	        return cb;
	      }
	    }
	    return null;
	  }

  public int compareTo(BienEspejo o) {
    return Comparator.comparing(BienEspejo::getIdCarga,Comparator.nullsFirst(Comparator.naturalOrder()))
      .thenComparing(BienEspejo::getActivo, Comparator.nullsFirst(Comparator.naturalOrder()))
      .thenComparing(BienEspejo::getReferenciaCatastral, Comparator.nullsFirst(Comparator.naturalOrder()))
      .thenComparing(BienEspejo::getIdufir, Comparator.nullsFirst(Comparator.naturalOrder()))
      .thenComparing(BienEspejo::getRegistro, Comparator.nullsFirst(Comparator.naturalOrder()))
      .thenComparing(BienEspejo::getFechaInscripcion, Comparator.nullsFirst(Comparator.naturalOrder()))
      .thenComparing(BienEspejo::getFinca, Comparator.nullsFirst(Comparator.naturalOrder()))
      .thenComparing(BienEspejo::getTomo, Comparator.nullsFirst(Comparator.naturalOrder()))
      .thenComparing(BienEspejo::getLibro, Comparator.nullsFirst(Comparator.naturalOrder()))
      .thenComparing(BienEspejo::getFolio, Comparator.nullsFirst(Comparator.naturalOrder()))
      .compare(this, o);
  }

}
