package com.haya.alaska.periodicidad_acuerdo_pago.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * PeriocidadAcuerdoPago entity
 *
 * @author jesus.javier
 */
@Audited
@AuditTable(value = "HIST_LKUP_PERIODICIDAD_ACUERDO_PAGO")
@NoArgsConstructor
@Entity
@Table(name = "LKUP_PERIODICIDAD_ACUERDO_PAGO")
public class PeriodicidadAcuerdoPago extends Catalogo {
	private static final long serialVersionUID = -1261890538323698925L;
}
