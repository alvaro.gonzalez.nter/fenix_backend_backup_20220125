package com.haya.alaska.periodicidad_acuerdo_pago.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.periodicidad_acuerdo_pago.domain.PeriodicidadAcuerdoPago;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriodicidadAcuerdoPagoRepository extends CatalogoRepository<PeriodicidadAcuerdoPago, Integer> {}
