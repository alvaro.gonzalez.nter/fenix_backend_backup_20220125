package com.haya.alaska.minuta_revisada_fiscal.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.minuta_revisada_fiscal.domain.MinutaRevisadaFiscal;
import org.springframework.stereotype.Repository;

@Repository
public interface MinutaRevisadaFiscalRepository extends CatalogoRepository<MinutaRevisadaFiscal, Integer> {
}
