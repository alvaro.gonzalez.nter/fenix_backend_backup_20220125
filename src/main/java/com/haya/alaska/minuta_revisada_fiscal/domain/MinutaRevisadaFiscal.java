package com.haya.alaska.minuta_revisada_fiscal.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.valoracion.domain.Valoracion;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_MINUTA_REVISADA_FISCAL")
@Entity
@Getter
@Table(name = "LKUP_MINUTA_REVISADA_FISCAL")
public class MinutaRevisadaFiscal extends Catalogo {
}
