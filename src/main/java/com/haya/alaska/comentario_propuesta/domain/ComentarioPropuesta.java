package com.haya.alaska.comentario_propuesta.domain;

import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_COMENTARIO_PROPUESTA")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_COMENTARIO_PROPUESTA")
public class ComentarioPropuesta implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @ToString.Include
    private Integer id;

    @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
    private Boolean activo = true;

    @Column(name = "FCH_FECHA_COMENTARIO")
    private Date fechaComentario;

    @Column(name = "FCH_FECHA_EDICION")
    private Date fechaEdicion;

    @ManyToOne
    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "id", nullable = false)
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "ID_PROPUESTA", referencedColumnName = "id", nullable = false)
    private Propuesta propuesta;

    @Column(name = "DES_COMENTARIO", nullable = false)
    @Lob
    private String comentario;
}
