package com.haya.alaska.comentario_propuesta.infrastructure.repository;

import com.haya.alaska.comentario_propuesta.domain.ComentarioPropuesta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComentarioPropuestaRepository extends JpaRepository<ComentarioPropuesta, Integer> {}
