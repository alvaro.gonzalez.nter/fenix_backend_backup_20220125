package com.haya.alaska.grupo_cliente.application;

import com.haya.alaska.grupo_cliente.infrastructure.controller.dto.GrupoClienteDTO;

import java.util.List;

public interface GrupoClienteUseCase {

  List<GrupoClienteDTO> getAll();
}
