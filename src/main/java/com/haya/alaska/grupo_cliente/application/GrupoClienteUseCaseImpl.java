package com.haya.alaska.grupo_cliente.application;

import com.haya.alaska.grupo_cliente.infrastructure.controller.dto.GrupoClienteDTO;
import com.haya.alaska.grupo_cliente.infrastructure.repository.GrupoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GrupoClienteUseCaseImpl implements GrupoClienteUseCase{

  @Autowired
  GrupoClienteRepository grupoClienteRepository;

  @Override
  public List<GrupoClienteDTO> getAll(){
    return GrupoClienteDTO.newList(grupoClienteRepository.findAll());
  }
}
