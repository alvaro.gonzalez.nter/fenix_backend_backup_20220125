package com.haya.alaska.grupo_cliente.infrastructure.controller;

import com.haya.alaska.grupo_cliente.application.GrupoClienteUseCase;
import com.haya.alaska.grupo_cliente.infrastructure.controller.dto.GrupoClienteDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/grupos")
public class GrupoClienteController {

  @Autowired
  private GrupoClienteUseCase grupoClienteUseCase;

  @ApiOperation(value = "Listado de grupos de clientes",
    notes = "Devuelve todos los grupos de clientes registrados en bbdd")
  @GetMapping
  public List<GrupoClienteDTO> getAll() {
    return grupoClienteUseCase.getAll();
  }
}
