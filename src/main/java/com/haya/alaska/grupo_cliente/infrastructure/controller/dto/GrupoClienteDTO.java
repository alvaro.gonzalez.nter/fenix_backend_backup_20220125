package com.haya.alaska.grupo_cliente.infrastructure.controller.dto;

import com.haya.alaska.grupo_cliente.domain.GrupoCliente;
import lombok.*;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GrupoClienteDTO implements Serializable {

  Integer id;
  Boolean activo;
  String nombre;

  public GrupoClienteDTO(GrupoCliente grupo){
    this.id = grupo.getId();
    this.activo = grupo.getActivo();
    this.nombre = grupo.getNombre() != null ? grupo.getNombre() : null;
  }

  public static List<GrupoClienteDTO> newList(List<GrupoCliente> grupos) {
    return grupos.stream().map(GrupoClienteDTO::new).collect(Collectors.toList());
  }

}
