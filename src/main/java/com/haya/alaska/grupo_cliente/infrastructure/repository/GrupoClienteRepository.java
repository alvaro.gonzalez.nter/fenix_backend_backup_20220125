package com.haya.alaska.grupo_cliente.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.grupo_cliente.domain.GrupoCliente;

import java.util.List;

@Repository
public interface GrupoClienteRepository extends JpaRepository<GrupoCliente, Integer> {

  public List<GrupoCliente> findAllByClientesIdIn(List<Integer> idClientes);
}
