package com.haya.alaska.plazo_acuerdo_pago.infrastructure.repository;

import com.haya.alaska.plazo_acuerdo_pago.domain.PlazoAcuerdoPago;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PlazoAcuerdoPagoRepository extends JpaRepository<PlazoAcuerdoPago, Integer> {

    public List<PlazoAcuerdoPago> findAllByAcuerdoPagoIdAndFecha(Integer acuerdoPagoId, Date fecha);

  public List<PlazoAcuerdoPago> findAllByAcuerdoPagoIdAndFechaBetweenAndAbonadoIsFalse(
      Integer acuerdoPagoId, Date fecha1, Date fecha2);

  public List<PlazoAcuerdoPago> findAllByAcuerdoPagoId(Integer acuerdoPagoId);

  List<PlazoAcuerdoPago> findAllByAcuerdoPagoIdOrderByFechaAsc(Integer acuerdoPagoId);
}
