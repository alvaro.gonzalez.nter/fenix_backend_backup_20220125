package com.haya.alaska.plazo_acuerdo_pago.domain;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_PLAZO_ACUERDO_PAGO")
@NoArgsConstructor
@Entity
@Setter
@Getter
@Builder
@AllArgsConstructor
@Table(name = "MSTR_PLAZO_ACUERDO_PAGO")
public class PlazoAcuerdoPago {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@ToString.Include
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ACUERDO_PAGO")
    private AcuerdoPago acuerdoPago;

	@Temporal(TemporalType.DATE)
	@Column(name = "FCH_FECHA")
	Date fecha;

	@Column(name = "NUM_CANTIDAD", columnDefinition = "decimal(15,2)")
	Double cantidad;

	@Column(name = "IND_ABONADO", nullable = false, columnDefinition = "boolean default true")
	Boolean abonado = false;
}
