package com.haya.alaska.estado_evento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoEventoRepository extends CatalogoRepository<EstadoEvento, Integer> {}
