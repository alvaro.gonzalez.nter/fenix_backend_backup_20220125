package com.haya.alaska.alquiler.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@AuditTable(value = "HIST_LKUP_ALQUILER")
@NoArgsConstructor
@Entity
@Table(name = "LKUP_ALQUILER")
public class Alquiler extends Catalogo {
}
