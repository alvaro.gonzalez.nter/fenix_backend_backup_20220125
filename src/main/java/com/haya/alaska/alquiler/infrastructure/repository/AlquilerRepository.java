package com.haya.alaska.alquiler.infrastructure.repository;

import com.haya.alaska.alquiler.domain.Alquiler;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlquilerRepository extends CatalogoRepository<Alquiler, Integer> {}
