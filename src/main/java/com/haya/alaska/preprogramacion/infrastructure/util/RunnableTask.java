package com.haya.alaska.preprogramacion.infrastructure.util;

import lombok.AllArgsConstructor;

import java.util.Date;

@AllArgsConstructor
class RunnableTask implements Runnable {
  private Integer id;
  private String message;

  public RunnableTask(String message){
    this.message = message;
  }

  @Override
  public void run() {
    System.out.println(new Date()+" Runnable Task with "+message
      +" on thread "+Thread.currentThread().getName());
  }
}
