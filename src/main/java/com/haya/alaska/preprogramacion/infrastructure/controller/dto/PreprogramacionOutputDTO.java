package com.haya.alaska.preprogramacion.infrastructure.controller.dto;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaGuardadaDto;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class PreprogramacionOutputDTO {
  private Integer id;
  private Boolean activo;
  private String nombre;
  private String asunto;
  private String mensaje;
  private String codigo;
  private BusquedaGuardadaDto busqueda;
  private CatalogoMinInfoDTO periodicidad;
  private CatalogoMinInfoDTO tipo;
  private Date fechaInicio;
  private String horaInicio;
  private Date fechaFin;
  private String horaFin;
  Boolean posesionNegociada;
}
