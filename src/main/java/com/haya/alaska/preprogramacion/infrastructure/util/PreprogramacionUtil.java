package com.haya.alaska.preprogramacion.infrastructure.util;

import com.haya.alaska.preprogramacion.domain.Preprogramacion;
import com.haya.alaska.preprogramacion.domain.Preprogramacion_;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class PreprogramacionUtil {
  @PersistenceContext
  private EntityManager entityManager;

  public List<Preprogramacion> getFilteredPreprogramacion(
    String orderField, String orderDirection, Integer page, Integer size){
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Preprogramacion> query = cb.createQuery(Preprogramacion.class);

    Root<Preprogramacion> root = query.from(Preprogramacion.class);

    List<Predicate> predicates = getPredicates(cb, root);

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    if (orderField != null) {
      if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
        rs.orderBy(cb.desc(root.get(orderField)));
      else
        rs.orderBy(cb.asc(root.get(orderField)));
    }
    Set<ParameterExpression<?>> q = query.getParameters();

    Query queryResult = entityManager.createQuery(query);

    if (page != null && size != null){
      queryResult.setFirstResult(page * size);
      queryResult.setMaxResults(size);
    }

    List<Preprogramacion> listaPropuesta = queryResult.getResultList();

    return listaPropuesta;
  }

  public Long getNumeroFilteredPreprogramacion(){
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Long> query = cb.createQuery(Long.class);

    Root<Preprogramacion> root = query.from(Preprogramacion.class);

    List<Predicate> predicates = getPredicates(cb, root);

    query.where(predicates.toArray(new Predicate[predicates.size()]));
    query.select(cb.count(root));

    return entityManager.createQuery(query).getSingleResult();
  }

  private List<Predicate> getPredicates(CriteriaBuilder cb, Root<Preprogramacion> root){
    List<Predicate> predicates = new ArrayList<>();

    predicates.add(cb.isTrue(root.get(Preprogramacion_.ACTIVO)));

    return predicates;
  }

  public Date fuseDateTime(String fecha, String time) throws ParseException {
    if (time != null) {
      String fechaS = fecha + " " + time + ":00";
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Date result = format.parse(fechaS);
      return result;
    } else {
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      Date result = format.parse(fecha);
      return result;
    }
  }

  public String getFromDate(Date date){
    String pattern = "yyyy-MM-dd";
    SimpleDateFormat format = new SimpleDateFormat(pattern);
    String strDate = format.format(date);
    return strDate;
  }

  private String dosDigitosHora(String hora){
    if (hora == null) return null;
    String result = hora;
    if (result.length() == 1) result = "0" + hora;
    return result;
  }
}
