package com.haya.alaska.preprogramacion.infrastructure.mapper;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaGuardadaInputDto;
import com.haya.alaska.busqueda.infrastructure.mapper.BusquedaMapper;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.preprogramacion.domain.Preprogramacion;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionInputDTO;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionOutputDTO;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionOutputDTOToList;
import com.haya.alaska.preprogramacion.infrastructure.util.PreprogramacionUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;

@Component
public class PreprogramacionMapper {
  @Autowired
  BusquedaMapper busquedaMapper;
  @Autowired
  PreprogramacionUtil preprogramacionUtil;

  public Preprogramacion inputToEntity(PreprogramacionInputDTO input){
    Preprogramacion p = new Preprogramacion();

    BeanUtils.copyProperties(input, p);

    return p;
  }

  public BusquedaGuardadaInputDto inputToEntity(String nombre, BusquedaDto busqueda){
    BusquedaGuardadaInputDto result = new BusquedaGuardadaInputDto();
    result.setNombre(nombre);
    result.setConsulta(busqueda);
    return result;
  }

  public PreprogramacionOutputDTO entityToOutput(Preprogramacion p) throws ParseException {
    PreprogramacionOutputDTO output = new PreprogramacionOutputDTO();

    BeanUtils.copyProperties(p, output);
    if (p.getPeriodicidad() != null)
      output.setPeriodicidad(new CatalogoMinInfoDTO(p.getPeriodicidad()));
    if (p.getTipo() != null)
      output.setTipo(new CatalogoMinInfoDTO(p.getTipo()));

    output.setFechaInicio(preprogramacionUtil.fuseDateTime(p.getFechaInicio(), null));
    output.setFechaFin(preprogramacionUtil.fuseDateTime(p.getFechaFin(), null));

    output.setBusqueda(busquedaMapper.busquedaGuardadaToDto(p.getBusqueda()));

    return output;
  }

  public PreprogramacionOutputDTOToList entityToOutputToList(Preprogramacion p) throws ParseException {
    PreprogramacionOutputDTOToList output = new PreprogramacionOutputDTOToList();

    BeanUtils.copyProperties(p, output);
    if (p.getPeriodicidad() != null)
      output.setPeriodicidad(new CatalogoMinInfoDTO(p.getPeriodicidad()));
    if (p.getTipo() != null)
      output.setTipo(new CatalogoMinInfoDTO(p.getTipo()));

    output.setFechaInicio(preprogramacionUtil.fuseDateTime(p.getFechaInicio(), null));
    output.setFechaFin(preprogramacionUtil.fuseDateTime(p.getFechaFin(), null));

    return output;
  }
}
