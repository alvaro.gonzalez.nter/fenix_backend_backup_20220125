package com.haya.alaska.preprogramacion.infrastructure.controller;

import com.haya.alaska.preprogramacion.application.PreprogramacionUseCaseImpl;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionInputDTO;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionOutputDTO;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionOutputDTOToList;
import com.haya.alaska.procedimiento.application.ProcedimientoUseCase;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.input.ProcedimientoInputDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.text.ParseException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/preprogramacion")
public class PreprogramacionController {
  @Autowired
  private PreprogramacionUseCaseImpl procedimientoUseCase;

  @ApiOperation(value = "Añadir Preprocedimiento", notes = "Crea un nuevo procedimiento autoejecutable")
  @PostMapping
  public void createSchedule(
    @RequestBody PreprogramacionInputDTO input,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException, InvalidCodeException, ParseException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    procedimientoUseCase.createSchedule(input, usuario);
  }

  @ApiOperation(value = "Borrar Preprocedimiento", notes = "Desactiva el preprocedimiento con el id enviado")
  @DeleteMapping("/{id}")
  public void deleteSchedule(@PathVariable Integer id) throws NotFoundException {
    procedimientoUseCase.deleteSchedule(id);
  }

  @ApiOperation(value = "Obtener Preprocedimiento", notes = "Obtener el preprocedimiento con el id enviado")
  @GetMapping("/{id}")
  public PreprogramacionOutputDTO getSchedule(@PathVariable Integer id) throws NotFoundException, ParseException {
    return procedimientoUseCase.getById(id);
  }

  @ApiOperation(value = "Obtener Preprocedimiento", notes = "Obtener el preprocedimiento con el id enviado")
  @GetMapping
  public ListWithCountDTO<PreprogramacionOutputDTOToList> getAllSchedules(
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page
  ) throws NotFoundException, ParseException {
    return procedimientoUseCase.getAll(orderField, orderDirection, page, size);
  }
}
