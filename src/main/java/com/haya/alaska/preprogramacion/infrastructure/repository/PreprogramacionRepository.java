package com.haya.alaska.preprogramacion.infrastructure.repository;

import com.haya.alaska.preprogramacion.domain.Preprogramacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface PreprogramacionRepository extends JpaRepository<Preprogramacion, Integer> {
  List<Preprogramacion> getAllByActivoIsTrue();
  Optional<Preprogramacion> getById(Integer integer);

  List<Preprogramacion> getAllByActivoIsTrueAndFechaFinLessThan(Date fecha);
}
