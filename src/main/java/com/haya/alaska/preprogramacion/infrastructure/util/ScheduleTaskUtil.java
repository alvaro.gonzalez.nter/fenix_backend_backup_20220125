package com.haya.alaska.preprogramacion.infrastructure.util;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

@Component
@Getter
public class ScheduleTaskUtil {
  @Autowired TaskScheduler scheduler;
  Map<Integer, ScheduledFuture<?>> jobsMap = new HashMap<>();

  public void addTaskToSchedulerCron(Integer id, Runnable task, CronTrigger cron) {
    ScheduledFuture<?> scheduledTask = scheduler.schedule(task, cron);
    jobsMap.put(id, scheduledTask);
  }

  public void addTaskToScheduler(Integer id, Runnable task, CronTrigger cron) {
    ScheduledFuture<?> scheduledTask = scheduler.schedule(task, cron);
    jobsMap.put(id, scheduledTask);
  }

  public void removeTaskFromScheduler(Integer id) {
    ScheduledFuture<?> scheduledTask = jobsMap.get(id);
    if(scheduledTask != null) {
      scheduledTask.cancel(true);
      jobsMap.put(id, null);
    }
  }

  public void clearAll(){
    for (Integer sf : this.getJobsMap().keySet()){
      this.removeTaskFromScheduler(sf);
    }
    jobsMap = new HashMap<>();
  }
}
