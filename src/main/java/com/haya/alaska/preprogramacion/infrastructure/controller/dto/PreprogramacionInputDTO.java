package com.haya.alaska.preprogramacion.infrastructure.controller.dto;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class PreprogramacionInputDTO implements Serializable {
  private String nombre;
  private String asunto;
  private String mensaje;
  private String codigo;
  private BusquedaDto busqueda;
  private Integer periodicidad;
  private Integer tipo;
  private Date fechaInicio;
  private String horaInicio;
  private Date fechaFin;
  private String horaFin;
  private Boolean posesionNegociada;
}
