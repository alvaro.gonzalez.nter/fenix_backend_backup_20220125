package com.haya.alaska.preprogramacion.domain;

import com.haya.alaska.busqueda.domain.BusquedaGuardada;
import com.haya.alaska.periodicidad_preprogramacion.domain.PeriodicidadPreprogramacion;
import com.haya.alaska.tipo_preprogramacion.domain.TipoPreprogramacion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_PREPROGRAMACION")
@Entity
@Table(name = "MSTR_PREPROGRAMACION")
@Getter
@Setter
@NoArgsConstructor
public class Preprogramacion implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @Column(name = "DES_NOMBRE")
  private String nombre;

  @Column(name = "DES_ASUNTO")
  private String asunto;

  @Lob
  @Column(name = "DES_MENSAJE")
  private String mensaje;

  @Column(name = "DES_CODIGO")
  private String codigo;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BUSQUEDA")
  private BusquedaGuardada busqueda;

  @ManyToOne
  @JoinColumn(name = "ID_PERIODICIDAD", referencedColumnName = "ID", nullable = false)
  private PeriodicidadPreprogramacion periodicidad;

  @ManyToOne
  @JoinColumn(name = "ID_TIPO_PREPROGRAMACION", referencedColumnName = "ID")
  private TipoPreprogramacion tipo;

  /*@Column(name = "FCH_FECHA_INICIO", nullable = false)
  Date fechaInicio;
  @Column(name = "FCH_FECHA_FIN")
  Date fechaFin;*/
  @Column(name = "DES_FECHA_INICIO")
  private String fechaInicio;
  @Column(name = "DES_HORA_INICIO")
  private String horaInicio;
  @Column(name = "DES_FECHA_FIN")
  private String fechaFin;
  @Column(name = "DES_HORA_FIN")
  private String horaFin;

  @Column(name = "IND_POSESION_NEGOCIADA")
  private Boolean posesionNegociada;
}
