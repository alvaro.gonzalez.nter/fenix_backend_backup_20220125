package com.haya.alaska.preprogramacion.application;

import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionInputDTO;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionOutputDTO;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionOutputDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;

import java.text.ParseException;

public interface PreprogramacionUseCase {
  void createSchedule(PreprogramacionInputDTO input, Usuario logged) throws NotFoundException, ParseException;
  void deleteSchedule(Integer id) throws NotFoundException;
  PreprogramacionOutputDTO getById(Integer id) throws NotFoundException, ParseException;
  ListWithCountDTO<PreprogramacionOutputDTOToList> getAll(String orderField, String orderDirection, Integer page, Integer size) throws ParseException;
}
