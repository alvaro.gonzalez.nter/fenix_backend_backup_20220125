package com.haya.alaska.preprogramacion.application;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haya.alaska.busqueda.application.BusquedaUseCase;
import com.haya.alaska.busqueda.domain.BusquedaGuardada;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaGuardadaInputDto;
import com.haya.alaska.busqueda.infrastructure.mapper.BusquedaMapper;
import com.haya.alaska.busqueda.infrastructure.repository.BusquedaGuardadaRepository;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteBusquedaDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteBusquedaDTOToList;
import com.haya.alaska.periodicidad_preprogramacion.domain.PeriodicidadPreprogramacion;
import com.haya.alaska.periodicidad_preprogramacion.infrastructure.repository.PeriodicidadPreprogramacionRepository;
import com.haya.alaska.preprogramacion.domain.Preprogramacion;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionInputDTO;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionOutputDTO;
import com.haya.alaska.preprogramacion.infrastructure.controller.dto.PreprogramacionOutputDTOToList;
import com.haya.alaska.preprogramacion.infrastructure.mapper.PreprogramacionMapper;
import com.haya.alaska.preprogramacion.infrastructure.repository.PreprogramacionRepository;
import com.haya.alaska.preprogramacion.infrastructure.util.PreprogramacionUtil;
import com.haya.alaska.preprogramacion.infrastructure.util.ScheduleTaskUtil;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.tipo_preprogramacion.domain.TipoPreprogramacion;
import com.haya.alaska.tipo_preprogramacion.infrastructure.repository.TipoPreprogramacionRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.haya.alaska.utilidades.comunicaciones.application.ComunicacionesUseCase;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.*;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.BurofaxDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.CartaOrdinariaDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.WhatsappWebDTO;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class PreprogramacionUseCaseImpl implements PreprogramacionUseCase{
  private static final ObjectMapper objectMapper = new ObjectMapper();

  static {
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  @Autowired
  ScheduleTaskUtil scheduleTaskUtil;

  @Autowired
  PreprogramacionRepository preprogramacionRepository;
  @Autowired
  PreprogramacionMapper preprogramacionMapper;
  @Autowired
  PreprogramacionUtil preprogramacionUtil;

  @Autowired
  UsuarioRepository usuarioRepository;
  @Autowired
  BusquedaMapper busquedaMapper;
  @Autowired
  BusquedaUseCase busquedaUseCase;
  @Autowired
  BusquedaGuardadaRepository busquedaGuardadaRepository;
  @Autowired
  ComunicacionesUseCase comunicacionesUseCase;
  @Autowired
  PeriodicidadPreprogramacionRepository periodicidadPreprogramacionRepository;
  @Autowired
  TipoPreprogramacionRepository tipoPreprogramacionRepository;

  public PreprogramacionOutputDTO getById(Integer id) throws NotFoundException, ParseException {
    Preprogramacion p = preprogramacionRepository.getById(id).orElseThrow(
      () -> new NotFoundException("Preprogramacion", id));

    PreprogramacionOutputDTO output = preprogramacionMapper.entityToOutput(p);

    return output;
  }

  public ListWithCountDTO<PreprogramacionOutputDTOToList> getAll(String orderField, String orderDirection, Integer page, Integer size) throws ParseException {
    List<Preprogramacion> list = preprogramacionUtil.getFilteredPreprogramacion(orderField, orderDirection, page, size);
    Long numero = preprogramacionUtil.getNumeroFilteredPreprogramacion();

    List<PreprogramacionOutputDTOToList> outputs = new ArrayList<>();
    for (Preprogramacion pre : list){
      outputs.add(preprogramacionMapper.entityToOutputToList(pre));
    }

    return new ListWithCountDTO<>(outputs, numero.intValue());
  }

  public void deleteSchedule(Integer id) throws NotFoundException {
    Preprogramacion p = preprogramacionRepository.getById(id).orElseThrow(
      () -> new NotFoundException("Preprogramacion", id));
    BusquedaGuardada bg = p.getBusqueda();

    p.setActivo(false);
    p.setBusqueda(null);
    scheduleTaskUtil.removeTaskFromScheduler(id);
    preprogramacionRepository.saveAndFlush(p);
    busquedaGuardadaRepository.delete(bg);
  }

  public void createSchedule(PreprogramacionInputDTO input, Usuario logged) throws NotFoundException, ParseException {
    Usuario usuario = usuarioRepository.findById(logged.getId()).orElseThrow(
      () -> new NotFoundException("Usuario", logged.getId()));

    PeriodicidadPreprogramacion pp = periodicidadPreprogramacionRepository.findById(input.getPeriodicidad()).orElseThrow(
      () -> new NotFoundException("PeriodicidadPreprogramacion", input.getPeriodicidad()));

    TipoPreprogramacion tp = tipoPreprogramacionRepository.findById(input.getTipo()).orElseThrow(
      () -> new NotFoundException("TipoPreprogramacion", input.getTipo()));

    BusquedaGuardadaInputDto busquedaInput = preprogramacionMapper.inputToEntity(input.getNombre(), input.getBusqueda());
    BusquedaGuardada busquedaGuardada = this.busquedaMapper.parseBusquedaGuardadaDto(usuario, busquedaInput);

    busquedaGuardada.setNumeroResultados(0);
    busquedaGuardada.setFecha(new Date());

    BusquedaGuardada bg = this.busquedaGuardadaRepository.save(busquedaGuardada);

    Preprogramacion p = preprogramacionMapper.inputToEntity(input);

    p.setFechaInicio(preprogramacionUtil.getFromDate(input.getFechaInicio()));
    p.setHoraInicio(input.getHoraInicio());
    p.setFechaFin(preprogramacionUtil.getFromDate(input.getFechaFin()));
    p.setHoraFin(input.getHoraFin());
    p.setAsunto(input.getAsunto());
    p.setBusqueda(bg);
    p.setPeriodicidad(pp);
    p.setTipo(tp);
    Preprogramacion pSaved = preprogramacionRepository.save(p);
    iniciarSchedule(pSaved);
  }

  private void iniciarSchedule(Preprogramacion p) throws ParseException {
    Calendar c = Calendar.getInstance();
    c.setTime(preprogramacionUtil.fuseDateTime(p.getFechaInicio(), p.getHoraInicio()));

    String cron = "";
    cron += c.get(Calendar.SECOND) + " ";
    cron += c.get(Calendar.MINUTE) + " ";
    cron += c.get(Calendar.HOUR_OF_DAY) + " ";

    if (p.getPeriodicidad() == null){
      cron = "0 * * * * *";
    } else
    switch (p.getPeriodicidad().getCodigo()){
      case "D":
        cron += "* ";
        cron += "* ";
        cron += "*";
        break;
      case "W":
        cron += "? ";
        cron += "* ";
        cron += c.get(Calendar.DAY_OF_WEEK) + "";
        break;
      case "M":
        cron += c.get(Calendar.DATE) + " ";
        cron += "* ";
        cron += "?";
        break;
      case "T":
        cron += c.get(Calendar.DATE) + " ";
        cron += c.get(Calendar.MONTH) + "/3 ";
        cron += "?";
        break;
      case "S":
        cron += c.get(Calendar.DATE) + " ";
        cron += c.get(Calendar.MONTH) + "/6 ";
        cron += "?";
        break;
      case "A":
        cron += c.get(Calendar.DATE) + " ";
        cron += c.get(Calendar.MONTH) + " ";
        cron += "?";
        break;
      default:
        cron = "30 * * * * *";
    }
    scheduleTaskUtil.addTaskToSchedulerCron(p.getId(), () -> {
      try {
        ejecutarSchedule(p.getId());
      } catch (RequiredValueException e) {
        e.printStackTrace();
      }
    }, new CronTrigger(cron));
  }

  private void ejecutarSchedule(Integer id) throws RequiredValueException {
    Preprogramacion pre = preprogramacionRepository.getById(id).orElse(null);

    if (pre != null){
      Calendar c = Calendar.getInstance();
      try {
        c.setTime(preprogramacionUtil.fuseDateTime(pre.getFechaInicio(), pre.getHoraInicio()));
        if (new Date().compareTo(c.getTime()) >= 0){
          BusquedaGuardada busquedaGuardada = pre.getBusqueda();
          BusquedaDto dto = this.parseJsonConsulta(busquedaGuardada.getJsonConsulta());
          Usuario user = usuarioRepository.findFirstByRolHayaCodigo("admin").orElse(null);
          if (user != null){
            try {
              if (pre.getTipo() != null)
                switch (pre.getTipo().getCodigo()){
                  case "S":
                    ejecutarSMS(pre, dto, user);
                    break;
                  case "E":
                    ejecutarEmail(pre, dto, user);
                    break;
                  case "W":
                    ejecutarWhatsapp(pre, dto, user);
                    break;
                  case "B":
                    ejecutarBurofax(pre, dto, user);
                    break;
                  case "C":
                    ejecutarCarta(pre, dto, user);
                    break;
                }
            } catch (NotFoundException e) {
              e.printStackTrace();
            } catch (IOException e) {
              e.printStackTrace();
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      } catch (ParseException ignored) {

      }
    }
  }

  @SneakyThrows
  private void ejecutarEmail(Preprogramacion pre, BusquedaDto dto, Usuario user) throws NotFoundException, IOException, InterruptedException {
    List<IntervinienteBusquedaDTOToList> intervinientes = new ArrayList<>();
    List<OcupanteBusquedaDTOToList> ocupantes = new ArrayList<>();

    if (pre.getPosesionNegociada() == null || !pre.getPosesionNegociada())
      intervinientes = busquedaUseCase.getAllFilteredIntervinientes(
        pre.getTipo().getValor(), dto, user,
        null, null, null, null, null, false).getList();
    if (pre.getPosesionNegociada() == null || pre.getPosesionNegociada())
      ocupantes = busquedaUseCase.getAllFilteredOcupantesGestionMasiva(
        pre.getTipo().getValor(), dto, user,
        null, null, null, null, null).getList();

    List<SendEmailDTO> emails = new ArrayList<>();
    for (IntervinienteBusquedaDTOToList i : intervinientes){
      SendEmailDTO emailDTO = new SendEmailDTO();
      emailDTO.setAddress(i.getEmail());
      emailDTO.setSubscriberKey(i.getId());

      ContactAttributes ca = new ContactAttributes();
      //SubscriberAttributes
      SubscriberAttributes sa = new SubscriberAttributes();
      sa.setNombre(i.getNombre());
      sa.setIdInterviniente(i.getId());
      sa.setIdExpediente(i.getIdConcatenado());
      sa.setSubject(pre.getAsunto());
      //ContactAtributtes
      ContactAttributes contactAttributes=new ContactAttributes();
      contactAttributes.setSubscriberAttributes(sa);
      //DatosEmail
      emailDTO.setAddress(i.getEmail());
      emailDTO.setContactAttributes(contactAttributes);
      emailDTO.setSubscriberKey(0);

      comunicacionesUseCase.envioEmail(pre.getCodigo(),emailDTO,user);
   }
    for (OcupanteBusquedaDTOToList o : ocupantes){
      SendEmailDTO emailDTO = new SendEmailDTO();
      emailDTO.setAddress(o.getEmail());
      emailDTO.setSubscriberKey(o.getId());

      ContactAttributes ca = new ContactAttributes();
      //SubscriberAttributes
      SubscriberAttributes sa = new SubscriberAttributes();
      sa.setNombre(o.getNombre());
      sa.setIdInterviniente(o.getId());
      sa.setIdExpediente(o.getIdExpediente());
      //ContactAtributtes
      ContactAttributes contactAttributes=new ContactAttributes();
      contactAttributes.setSubscriberAttributes(sa);
      //DatosEmail
      emailDTO.setAddress(o.getEmail());
      emailDTO.setContactAttributes(contactAttributes);
      emailDTO.setSubscriberKey(0);

      comunicacionesUseCase.envioEmail(pre.getCodigo(),emailDTO,user);
    }
    //emailDTO.setAddress();
    //TODO
  }

  private void ejecutarSMS(Preprogramacion pre, BusquedaDto dto, Usuario user) throws NotFoundException, IOException, RequiredValueException {
    List<IntervinienteBusquedaDTOToList> intervinientes = new ArrayList<>();
    List<OcupanteBusquedaDTOToList> ocupantes = new ArrayList<>();

    if (pre.getPosesionNegociada() == null || !pre.getPosesionNegociada())
      intervinientes = busquedaUseCase.getAllFilteredIntervinientes(
        pre.getTipo().getValor(), dto, user,
        null, null, null, null, null, false).getList();

    if (pre.getPosesionNegociada() == null || pre.getPosesionNegociada())
      ocupantes = busquedaUseCase.getAllFilteredOcupantesGestionMasiva(
        pre.getTipo().getValor(), dto, user,
        null, null, null, null, null).getList();

    SendSmsDTO smsDto = new SendSmsDTO();
    smsDto.setSubscribe(true);
    smsDto.setResubscribe(true);
    smsDto.setOverride(false);
    smsDto.setKeyword("Fenix");
    smsDto.setMessageText(pre.getMensaje());
    List<Subscribers> subscribers = new ArrayList<>();
    for (IntervinienteBusquedaDTOToList i : intervinientes){
      Subscribers subs = new Subscribers();
      subs.setMobileNumber(i.getTelefonoPrincipal());
      if (i.getTelefonoPrincipal() != null && !i.getTelefonoPrincipal().substring(2).equals("34"))
        subs.setMobileNumber("34" + i.getTelefonoPrincipal());
      subs.setSubscriberKey(i.getId().toString());

      Attributes att = new Attributes();
      att.setIdExpediente(i.getIdConcatenado());
      att.setNombreGestor(user.getNombre());
      att.setTelefono(i.getTelefonoPrincipal());
      att.setCliente(i.getCliente());
      att.setTextoSMS(pre.getMensaje());
      att.setIdInterviniente(i.getId());

      subs.setAttributes(att);
      subscribers.add(subs);
    }
    for (OcupanteBusquedaDTOToList o : ocupantes){
      Subscribers subs = new Subscribers();
      subs.setMobileNumber(o.getTelefonoMovil());
      if (o.getTelefonoMovil() != null && !o.getTelefonoMovil().substring(2).equals("34"))
        subs.setMobileNumber("34" + o.getTelefonoMovil());
      subs.setSubscriberKey(o.getId().toString());

      Attributes att = new Attributes();
      att.setIdExpediente(o.getIdExpediente());
      att.setNombreGestor(user.getNombre());
      att.setTelefono(o.getTelefonoFijo());
      att.setCliente(o.getCliente());
      att.setTextoSMS(pre.getMensaje());
      att.setIdInterviniente(o.getId());

      subs.setAttributes(att);
      subscribers.add(subs);
    }
    smsDto.setSubscribers(subscribers);

    comunicacionesUseCase.envioSMS(pre.getCodigo(), smsDto, user);
  }

  private void ejecutarCarta(Preprogramacion pre, BusquedaDto dto, Usuario user) throws NotFoundException, IOException{
    List<IntervinienteBusquedaDTOToList> intervinientes = new ArrayList<>();
    List<OcupanteBusquedaDTOToList> ocupantes = new ArrayList<>();

    if (pre.getPosesionNegociada() == null || !pre.getPosesionNegociada())
      intervinientes = busquedaUseCase.getAllFilteredIntervinientes(
        pre.getTipo().getValor(), dto, user,
        null, null, null, null, null, false).getList();
    if (pre.getPosesionNegociada() == null || pre.getPosesionNegociada())
      ocupantes = busquedaUseCase.getAllFilteredOcupantesGestionMasiva(
        pre.getTipo().getValor(), dto, user,
        null, null, null, null, null).getList();

    CartaOrdinariaDTO cartaDto = new CartaOrdinariaDTO();

  }

  private void ejecutarWhatsapp(Preprogramacion pre, BusquedaDto dto, Usuario user) throws NotFoundException, IOException{
    List<IntervinienteBusquedaDTOToList> intervinientes = new ArrayList<>();
    List<OcupanteBusquedaDTOToList> ocupantes = new ArrayList<>();

    if (pre.getPosesionNegociada() == null || !pre.getPosesionNegociada())
      intervinientes = busquedaUseCase.getAllFilteredIntervinientes(
        pre.getTipo().getValor(), dto, user,
        null, null, null, null, null, false).getList();
    if (pre.getPosesionNegociada() == null || pre.getPosesionNegociada())
      ocupantes = busquedaUseCase.getAllFilteredOcupantesGestionMasiva(
        pre.getTipo().getValor(), dto, user,
        null, null, null, null, null).getList();

    WhatsappWebDTO whaDto = new WhatsappWebDTO();
  }

  private void ejecutarBurofax(Preprogramacion pre, BusquedaDto dto, Usuario user) throws NotFoundException, IOException{
    List<IntervinienteBusquedaDTOToList> intervinientes = new ArrayList<>();
    List<OcupanteBusquedaDTOToList> ocupantes = new ArrayList<>();

    if (pre.getPosesionNegociada() == null || !pre.getPosesionNegociada())
      intervinientes = busquedaUseCase.getAllFilteredIntervinientes(
        pre.getTipo().getValor(), dto, user,
        null, null, null, null, null, false).getList();
    if (pre.getPosesionNegociada() == null || pre.getPosesionNegociada())
      ocupantes = busquedaUseCase.getAllFilteredOcupantesGestionMasiva(
        pre.getTipo().getValor(), dto, user,
        null, null, null, null, null).getList();

    List<BurofaxDTO> burofaxes = new ArrayList<>();
    BurofaxDTO buroDto = new BurofaxDTO();
  }

  @SneakyThrows
  public BusquedaDto parseJsonConsulta(String consulta) {
    return objectMapper.readValue(consulta, BusquedaDto.class);
  }

  @Scheduled(cron = "0 30 1 * * *") //TODO
  public void activarSchedules() throws ParseException {
    List<Preprogramacion> pList = preprogramacionRepository.getAllByActivoIsTrue();
    for (Preprogramacion p : pList){
      Date fecha = preprogramacionUtil.fuseDateTime(p.getFechaFin(), p.getHoraFin());
      Date hoy = new Date();
      if (hoy.compareTo(fecha) > 0){
        try {
          deleteSchedule(p.getId());
        } catch (NotFoundException e) {
          e.printStackTrace();
        }
      }
    }

    scheduleTaskUtil.clearAll();
    List<Preprogramacion> pListAct = preprogramacionRepository.getAllByActivoIsTrue();
    for (Preprogramacion p : pListAct){
      iniciarSchedule(p);
    }
  }
}
