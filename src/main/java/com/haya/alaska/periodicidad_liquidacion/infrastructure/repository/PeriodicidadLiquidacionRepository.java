package com.haya.alaska.periodicidad_liquidacion.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.periodicidad_liquidacion.domain.PeriodicidadLiquidacion;

@Repository
public interface PeriodicidadLiquidacionRepository extends CatalogoRepository<PeriodicidadLiquidacion, Integer> {
	  
}
