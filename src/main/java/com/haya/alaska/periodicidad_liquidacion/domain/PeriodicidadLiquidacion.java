package com.haya.alaska.periodicidad_liquidacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.contrato.domain.Contrato;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_PERIOCIDAD_LIQUIDACION")
@Entity
@Table(name = "LKUP_PERIOCIDAD_LIQUIDACION")
public class PeriodicidadLiquidacion extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERIOCIDAD_LIQUIDACION")
  @NotAudited
  private Set<Contrato> contratos = new HashSet<>();
}
