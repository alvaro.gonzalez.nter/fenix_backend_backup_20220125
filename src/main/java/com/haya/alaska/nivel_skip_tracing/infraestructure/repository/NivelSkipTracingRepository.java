package com.haya.alaska.nivel_skip_tracing.infraestructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.nivel_skip_tracing.domain.NivelSkipTracing;
import org.springframework.stereotype.Repository;

@Repository
public interface NivelSkipTracingRepository extends CatalogoRepository<NivelSkipTracing, Integer> {
}
