package com.haya.alaska.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/** @author diegotobalina created on 14/08/2020 */
@Configuration
@EnableScheduling
public class SchedulingConfigurator {}
