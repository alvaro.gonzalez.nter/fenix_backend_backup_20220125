package com.haya.alaska.municipio.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_MUNICIPIO")
@Entity
@Getter
@Setter
@Table(name = "LKUP_MUNICIPIO")
public class Municipio extends Catalogo {

  private static final long serialVersionUID = 1L;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA")
  private Provincia provincia;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO")
  @NotAudited
  private Set<Localidad> localidades = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO")
  @NotAudited
  private Set<Direccion> direcciones = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO")
  @NotAudited
  private Set<Bien> bienes = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO")
  @NotAudited
  private Set<DatoDireccionST> direccionesST = new HashSet<>();

  /*@OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO_BIEN_PN")
  @NotAudited
  private Set<SegmentacionPN> segmentacionesBienPN = new HashSet<>();*/
}
