package com.haya.alaska.municipio.infrastructure.controller;

import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.controller.dto.MunicipioDTO;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/municipios")
public class MunicipioController {
  @Autowired
  private MunicipioRepository municipioRepository;

  @ApiOperation(
          value = "Listado de municipios",
          notes = "Devuelve todos los municipios, opcionalmente filtrando por provincia")
  @GetMapping()
  public List<MunicipioDTO> getAll(@RequestParam(value = "provincia", required = false) Integer provincia) {
    List<Municipio> lista;
    if (provincia != null) {
      lista = municipioRepository.findByProvinciaIdAndActivoIsTrueOrderByValor(provincia);
    } else {
      lista = municipioRepository.findAll();
    }
    return lista.stream().map(MunicipioDTO::new).collect(Collectors.toList());
  }
}
