package com.haya.alaska.municipio.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.municipio.domain.Municipio;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MunicipioRepository extends CatalogoRepository<Municipio, Integer> {
	List<Municipio> findByProvinciaIdAndActivoIsTrueOrderByValor(Integer provincia);
}
