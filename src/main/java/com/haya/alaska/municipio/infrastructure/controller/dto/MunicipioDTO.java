package com.haya.alaska.municipio.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.municipio.domain.Municipio;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;

@Getter
@NoArgsConstructor
public class MunicipioDTO implements Serializable {
  private Integer id;
  private String codigo;
  private String valor;
  private Boolean activo;
  private CatalogoMinInfoDTO provincia;

  public MunicipioDTO(Municipio municipio) {
    this.id = municipio.getId();
    this.codigo = municipio.getCodigo();
    String valor = null;
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();
    if (defaultLocal == null || defaultLocal.equals("es")) valor = municipio.getValor();
    else if (defaultLocal.equals("en")) valor = municipio.getValorIngles();
    this.valor = valor;
    this.activo = municipio.getActivo();
    this.provincia = new CatalogoMinInfoDTO(municipio.getProvincia());
  }
}
