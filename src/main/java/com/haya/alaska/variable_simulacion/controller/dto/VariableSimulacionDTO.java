package com.haya.alaska.variable_simulacion.controller.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class VariableSimulacionDTO implements Serializable {
  private Integer id;
  private String tipo;
  private String descripcion;
  private Double valor;
  private String nombreCampo;
}
