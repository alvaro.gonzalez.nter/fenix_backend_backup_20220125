package com.haya.alaska.variable_simulacion.controller.dto.input;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class ModificarVariableSimulacionInputDTO implements Serializable {
  private Integer id;
  private Double nuevoValor;
}
