package com.haya.alaska.variable_simulacion.controller.dto;

import com.haya.alaska.variable_ajd.controller.dto.VariableAJDDTO;
import com.haya.alaska.variable_itp.controller.dto.VariableITPDTO;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class VariableSimulacionBienDTO implements Serializable {
  private List<VariableSimulacionDTO> variablesGenerales;
  private List<VariableITPDTO> variablesITP;
  private List<VariableAJDDTO> variablesAJD;
}
