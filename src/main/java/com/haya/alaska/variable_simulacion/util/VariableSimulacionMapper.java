package com.haya.alaska.variable_simulacion.util;

import com.haya.alaska.variable_simulacion.controller.dto.VariableSimulacionDTO;
import com.haya.alaska.variable_simulacion.domain.VariableSimulacion;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class VariableSimulacionMapper {

  public VariableSimulacionDTO entidadADto(VariableSimulacion entidad) {
    VariableSimulacionDTO variableSimulacionDTO = new VariableSimulacionDTO();
    variableSimulacionDTO.setId(entidad.getId());
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      variableSimulacionDTO.setTipo(entidad.getTipo());
      variableSimulacionDTO.setDescripcion(entidad.getDescripcion());
    } else {
      variableSimulacionDTO.setTipo(entidad.getTipoIngles());
      variableSimulacionDTO.setDescripcion(entidad.getDescripcionIngles());
    }
    variableSimulacionDTO.setValor(entidad.getValor());
    variableSimulacionDTO.setNombreCampo(entidad.getCodigo());
    return variableSimulacionDTO;
  }
}
