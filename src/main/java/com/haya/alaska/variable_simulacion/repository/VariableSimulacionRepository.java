package com.haya.alaska.variable_simulacion.repository;

import com.haya.alaska.variable_simulacion.domain.VariableSimulacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VariableSimulacionRepository extends JpaRepository<VariableSimulacion, Integer> {
  Optional<VariableSimulacion> findByCodigo(String codigo);

  List<VariableSimulacion> findAllByNivel(String nivel);
}
