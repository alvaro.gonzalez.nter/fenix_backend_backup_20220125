package com.haya.alaska.variable_simulacion.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_MSTR_VARIABLE_SIMULACION")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_VARIABLE_SIMULACION")
public class VariableSimulacion implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "TIPO")
  private String tipo;

  @Column(name = "TIPO_INGLES")
  private String tipoIngles;

  @Column(name = "CODIGO")
  private String codigo;

  @Column(name = "DESCRIPCION")
  private String descripcion;

  @Column(name = "DESCRIPCION_INGLES")
  private String descripcionIngles;

  @Column(name = "VALOR")
  private Double valor;

  @Column(name = "NIVEL")
  private String nivel;
}
