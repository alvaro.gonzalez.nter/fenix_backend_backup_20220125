package com.haya.alaska.documento.infrastructure.controller.dto;

import com.haya.alaska.integraciones.gd.model.Documento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
public class DocumentoDeudorDTO implements Serializable {

  private Long id;
  // private String concepto; //Campo Concepto preguntar
  //private String usuarioCreador;
  private String tipoDocumento;
  private String fechaAlta;
  private Date fechaFirma;
  private String estado;
  private String nombreArchivo;
  private String usuarioCreador;
  private String idCliente;


  public DocumentoDeudorDTO(Documento documento) {
    this.id = documento.getIdentificadorNodo();
  //  this.usuarioCreador = "";
    this.fechaAlta = documento.getCreateDate();
    this.fechaFirma = null;
    this.tipoDocumento = documento.getTdn2Desc();
    this.estado = "";
    this.nombreArchivo = documento.getNombreNodo();
    this.idCliente="";
    this.usuarioCreador="";


  }
  }
