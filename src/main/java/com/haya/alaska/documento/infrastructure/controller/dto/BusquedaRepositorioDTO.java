package com.haya.alaska.documento.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaRepositorioDTO {
  String documentoId;
  String usuarioCreador;
  String usuarioDestinatario;
  Date fechaAlta;
  Date fechaActualizacion;
  String nombreDocumento;
  String tipoDocumento;
}
