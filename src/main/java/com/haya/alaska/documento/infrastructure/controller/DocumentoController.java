package com.haya.alaska.documento.infrastructure.controller;

import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.model.respuesta.RespuestaDescargaDocumento;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/documentos")
public class DocumentoController {

  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;

  @ApiOperation(value = "Descargar documento", notes = "Obtiene un documento del gestor documental")
  @GetMapping("/{id}")
  public ResponseEntity<InputStreamResource> descargar(@PathVariable("id") Integer id) throws IOException {
    RespuestaDescargaDocumento respuestaDocumento = servicioGestorDocumental.descargarDocumento(id);

    String nombrearchivo=respuestaDocumento.getContentDisposition();

    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Type", respuestaDocumento.getContentType());
    //headers.add("Content-Disposition", respuestaDocumento.getContentDisposition());
    String filename=respuestaDocumento.getContentDisposition();
    String []parts=filename.split("filename=");
    String nombrearchivo2=parts[1];
    String nombrearchivofinal=nombrearchivo2.replace('"',' ');

    headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
    headers.add("Content-disposition", nombrearchivofinal);
    headers.add("Access-Control-Expose-Headers","Content-Disposition");
    return ResponseEntity
      .ok()
      .headers(headers)
      .body(new InputStreamResource(respuestaDocumento.getBody()));
  }

  @ApiOperation(value = "Eliminar documento", notes = "Elmina un documento del gestor documental")
  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void eliminar(@PathVariable("id") Integer id) throws IOException {
    servicioGestorDocumental.eliminarDocumento(id);
  }

  @ApiOperation(value = "Eliminar lista de documentos", notes = "Elmina una lista de documentos del gestor documental")
  @DeleteMapping("/eliminarvarios")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void eliminarVarios(@RequestParam List<Integer> documentoIdList) throws IOException {
    servicioGestorDocumental.eliminarDocumentos(documentoIdList);
  }


  @ApiOperation(value = "Eliminar documento", notes = "Elmina un documento del gestor documental y de nuestra base de datps")
  @DeleteMapping("/documento/{id}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void eliminarDocumentoUsuarios(@PathVariable("id") Integer id) throws IOException {
    servicioGestorDocumental.eliminarDocumentoUsuarios(id);
  }

  @ApiOperation(value = "Eliminar lista de documentos", notes = "Elmina una lista de documentos del gestor documental")
  @DeleteMapping("/eliminarDocumentosUsuarios")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void eliminarVariosDocumentosUsuarios(@RequestParam List<Integer> documentoIdList) throws IOException {
    servicioGestorDocumental.eliminarDocumentosVarios(documentoIdList);
  }


}
