package com.haya.alaska.documento.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class DocumentoFirmaDto {
  private Integer id;
  private String tipoDocumento;
  private String concepto;
  private String estado;
  private Date fechaAlta;
  private Date fechaFirma;
}
