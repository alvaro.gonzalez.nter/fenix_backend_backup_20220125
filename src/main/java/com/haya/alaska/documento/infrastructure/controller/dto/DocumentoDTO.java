package com.haya.alaska.documento.infrastructure.controller.dto;

import com.haya.alaska.integraciones.gd.model.Documento;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DocumentoDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  public Long id;
  public String tipo;
  public String usuarioCreador;
  public String idEntidad; // Devuelve el idBien, el idContrato, el idInterviniente etc
  public String fechaActualizacion;
  public String nombreArchivo;
  public String idHaya;

  public DocumentoDTO(Documento documento) {
    this.id = documento.getIdentificadorNodo();
    this.tipo = documento.getTdn2Desc();
    this.usuarioCreador = "";
    this.idEntidad = "";
    this.fechaActualizacion = documento.getCreateDate();
    this.nombreArchivo = documento.getNombreNodo();
    this.idHaya=documento.getIdActivo();
  }
}
