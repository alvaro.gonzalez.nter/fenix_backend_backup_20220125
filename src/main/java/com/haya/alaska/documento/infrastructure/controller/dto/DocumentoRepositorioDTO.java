package com.haya.alaska.documento.infrastructure.controller.dto;

import com.haya.alaska.integraciones.gd.model.Documento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class DocumentoRepositorioDTO implements Serializable {

  private Long id;
  //private String tipo;
  private String usuarioCreador;
  private String tipoDocumento;
  private Date fechaAlta;
  private String fechaActualizacion;
  private String nombreArchivo;
  private String tipoModeloCartera;
  private String usuarioDestino;
  private String nombreCartera;
  private String nombreCliente;


  public DocumentoRepositorioDTO(Documento documento) {
    this.id = documento.getIdentificadorNodo();
  //  this.tipo = documento.getTdn2Desc();
    this.usuarioCreador = "";
    this.usuarioDestino="";
   // this.idEntidad = documento.getIdActivo();
    this.fechaAlta=new Date();
    this.fechaActualizacion = documento.getCreateDate();
    this.nombreArchivo=documento.getNombreNodo();
    this.tipoDocumento=documento.getTdn2Desc();
    this.tipoModeloCartera="";
    this.nombreCartera="";
    this.nombreCliente="";
  }
}
