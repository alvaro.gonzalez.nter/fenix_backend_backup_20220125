package com.haya.alaska.clase_evento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import org.springframework.stereotype.Repository;

@Repository
public interface ClaseEventoRepository extends CatalogoRepository<ClaseEvento, Integer> {}
