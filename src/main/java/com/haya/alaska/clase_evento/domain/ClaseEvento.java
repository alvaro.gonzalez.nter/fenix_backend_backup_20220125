package com.haya.alaska.clase_evento.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_CLASE_EVENTO")
@Entity
@Table(name = "LKUP_CLASE_EVENTO")
@NoArgsConstructor
public class ClaseEvento extends Catalogo {
    @OneToMany(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_CLASE_EVENTO")
    @NotAudited
    private Set<TipoEvento> tipos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLASE_EVENTO")
  @NotAudited
  private Set<Evento> eventos = new HashSet<>();
}
