package com.haya.alaska.asignacion_usuario_cartera.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_RELA_USUARIO_CARTERA")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "RELA_USUARIO_CARTERA")
public class AsignacionUsuarioCartera implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  @NotNull
  private Usuario usuario;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotNull
  private Cartera cartera;


  public AsignacionUsuarioCartera(Cartera cartera, Usuario usuario) {
    this.cartera= cartera;
    this.usuario=usuario;
  }

}
