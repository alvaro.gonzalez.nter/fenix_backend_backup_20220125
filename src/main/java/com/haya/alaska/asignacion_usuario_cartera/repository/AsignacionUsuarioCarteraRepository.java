package com.haya.alaska.asignacion_usuario_cartera.repository;

import com.haya.alaska.asignacion_usuario_cartera.domain.AsignacionUsuarioCartera;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AsignacionUsuarioCarteraRepository extends JpaRepository<AsignacionUsuarioCartera, Integer> {
}
