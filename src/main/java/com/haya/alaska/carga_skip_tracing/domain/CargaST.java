package com.haya.alaska.carga_skip_tracing.domain;

import com.haya.alaska.busqueda_skip_tracing.domain.BusquedaSkipTracing;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import lombok.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_CARGA_SKIP_TRACING")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_CARGA_SKIP_TRACING")
public class CargaST implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_DESCARGA")
  private Date fechaDescarga;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ACTUALIZACION")
  private Date fechaActualizacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_INPUT")
  private Date fechaInput;

  @Column(name = "IND_VALIDADO")
  private Boolean validado;

  @Column(name = "DES_ENCARGADO")
  private String encargado;

  @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
  @JoinTable(
    name = "RELA_CARGA_SKIP_TRACING_BUSQUEDA_SKIP_TRACING",
    joinColumns = @JoinColumn(name = "ID_CARGA_SKIP_TRACING"),
    inverseJoinColumns = @JoinColumn(name = "ID_BUSQUEDA"))
  @AuditJoinTable(name = "HIST_RELA_CARGA_SKIP_TRACING_BUSQUEDA_SKIP_TRACING")
  private Set<BusquedaSkipTracing> busquedas = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARGA_SKIP_TRACING")
  @NotAudited
  private Set<SkipTracing> skipTracings = new HashSet<>();

  @Column(name = "DES_COMENTARIO")
  @Lob
  private String comentario;
}
