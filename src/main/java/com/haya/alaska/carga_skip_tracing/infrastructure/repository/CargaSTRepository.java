package com.haya.alaska.carga_skip_tracing.infrastructure.repository;

import com.haya.alaska.carga_skip_tracing.domain.CargaST;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CargaSTRepository extends JpaRepository<CargaST, Integer> {
}
