package com.haya.alaska.schedulingtasks;

import com.haya.alaska.integraciones.solicitud_firma.application.SolicitudFirmaUseCase;
import com.haya.alaska.shared.exceptions.ExternalApiErrorException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
@AllArgsConstructor
public class ActualizarEstadoSolicitudesFirma {

  private final SolicitudFirmaUseCase service;

  @Scheduled(cron = "${api.backup.update-time}")
  public void actualizarEstadoSolicitudesEnCurso()
      throws IOException, ExternalApiErrorException, NotFoundException {
    log.info("CRON: Actualizar solicitudes de firma en curso...");
    service.actualizarEstadoSolicitudesEnCurso();
    log.info("CRON: Actualizar solicitudes de firma completado");
  }
}
