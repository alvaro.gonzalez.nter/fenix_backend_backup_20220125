package com.haya.alaska.schedulingtasks;

import com.haya.alaska.integraciones.solicitud_firma.application.SolicitudFirmaUseCase;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CerrarSolicitudesFirmaCaducadas {

  private final SolicitudFirmaUseCase service;

  @Scheduled(cron = "${api.backup.expiration-time}")
  public void cerrarSolicitudesCaducadas() {
    service.cerrarSolicitudesCaducadas();
  }
}
