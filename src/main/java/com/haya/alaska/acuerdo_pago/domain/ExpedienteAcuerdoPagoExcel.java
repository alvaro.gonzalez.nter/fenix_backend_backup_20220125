package com.haya.alaska.acuerdo_pago.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class ExpedienteAcuerdoPagoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Number"); // 0
      cabeceras.add("Agreement Id"); // 1
      cabeceras.add("Loan"); // 2
      cabeceras.add("Periodicity"); // 3
      cabeceras.add("Origin"); // 4
      cabeceras.add("Total Amount"); // 5
      cabeceras.add("Quota Amount"); // 6
      cabeceras.add("Start Date"); // 7
      cabeceras.add("End Date"); // 8
      cabeceras.add("Number Quotas"); // 9
      cabeceras.add("Status"); // 10
      cabeceras.add("User"); // 11
    } else {
      cabeceras.add("Numero Expediente"); // 0
      cabeceras.add("Id_Acuerdo"); // 1
      cabeceras.add("Contrato"); // 2
      cabeceras.add("Periocidad"); // 3
      cabeceras.add("Origen"); // 4
      cabeceras.add("Importe Total"); // 5
      cabeceras.add("Importe Cuota"); // 6
      cabeceras.add("Fecha Inicio"); // 7
      cabeceras.add("Fecha Final"); // 8
      cabeceras.add("Numero Cuotas"); // 9
      cabeceras.add("Estado"); // 10
      cabeceras.add("Usuario"); // 11
    }
  }

  public ExpedienteAcuerdoPagoExcel(AcuerdoPago acuerdoPago) {
    super();
    addAcuerdoPago(acuerdoPago);
  }

  public ExpedienteAcuerdoPagoExcel(List<AcuerdoPago> acuerdosPago) {
    super();
    for (AcuerdoPago pago : acuerdosPago) {
      addAcuerdoPago(pago);
    }
  }

  private void addAcuerdoPago(AcuerdoPago acuerdoPago) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Connection Number", acuerdoPago.getExpediente().getIdConcatenado());
      this.add("Agreement Id", acuerdoPago.getId());
      this.add(
          "Loan",
          acuerdoPago.getContratos().stream()
              .map(c -> "" + c.getIdCarga())
              .collect(Collectors.joining(",")));
      this.add("Periodicity", acuerdoPago.getPeriodicidadAcuerdoPago().getValorIngles());
      this.add("Origin", acuerdoPago.getOrigenAcuerdo().getValorIngles());
      this.add("Total Amount", acuerdoPago.getImporteTotal());
      this.add("Quota Amount", acuerdoPago.getImportePlazos());
      this.add("Start Date", acuerdoPago.getFechaInicio());
      this.add("End Date", acuerdoPago.getFechaFinal());
      this.add("Number Quotas", acuerdoPago.getNumeroPlazos());
      this.add("Status", acuerdoPago.getActivo() ? "Active" : "Inactive");
      this.add("User", acuerdoPago.getUsuario().getNombre());
    } else {
      this.add("Numero Expediente", acuerdoPago.getExpediente().getIdConcatenado());
      this.add("Id_Acuerdo", acuerdoPago.getId());
      this.add(
          "Contrato",
          acuerdoPago.getContratos().stream()
              .map(c -> "" + c.getIdCarga())
              .collect(Collectors.joining(",")));
      this.add("Periocidad", acuerdoPago.getPeriodicidadAcuerdoPago().getValor());
      this.add("Origen", acuerdoPago.getOrigenAcuerdo().getValor());
      this.add("Importe Total", acuerdoPago.getImporteTotal());
      this.add("Importe Cuota", acuerdoPago.getImportePlazos());
      this.add("Fecha Inicio", acuerdoPago.getFechaInicio());
      this.add("Fecha Final", acuerdoPago.getFechaFinal());
      this.add("Numero Cuotas", acuerdoPago.getNumeroPlazos());
      this.add("Estado", acuerdoPago.getActivo() ? "Activo" : "Inactivo");
      this.add("Usuario", acuerdoPago.getUsuario().getNombre());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
