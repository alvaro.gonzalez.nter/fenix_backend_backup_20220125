package com.haya.alaska.acuerdo_pago.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class CuadroAcuerdoPagoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Agreement Id"); // 0
      cabeceras.add("Loan"); // 1
      cabeceras.add("Start Date"); // 2
      cabeceras.add("End Date"); // 3
      cabeceras.add("Amount"); // 4
      cabeceras.add("Status"); // 5
    } else {
      cabeceras.add("Id Acuerdo"); // 0
      cabeceras.add("Contrato"); // 1
      cabeceras.add("Fecha Inicio"); // 2
      cabeceras.add("Fecha Fin"); // 3
      cabeceras.add("Importe"); // 4
      cabeceras.add("Estado"); // 5
    }
  }

  public CuadroAcuerdoPagoExcel(AcuerdoPago acuerdoPago) {
    super();
    addAcuerdoPago(acuerdoPago);
  }

  public void addAcuerdoPago(AcuerdoPago acuerdoPago) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Agreement Id", acuerdoPago.getId());
      this.add(
          "Loan",
          acuerdoPago.getContratos().stream()
              .map(c -> "" + c.getIdCarga())
              .collect(Collectors.joining(",")));
      this.add("Start Date", acuerdoPago.getFechaInicio());
      this.add("End Date", acuerdoPago.getFechaFinal());
      this.add("Amount", acuerdoPago.getImporteTotal());
      this.add("Status", Boolean.TRUE.equals(acuerdoPago.getActivo()) ? "Active" : "Inactive");
    } else {
      this.add("Id Acuerdo", acuerdoPago.getId());
      this.add(
          "Contrato",
          acuerdoPago.getContratos().stream()
              .map(c -> "" + c.getIdCarga())
              .collect(Collectors.joining(",")));
      this.add("Fecha Inicio", acuerdoPago.getFechaInicio());
      this.add("Fecha Fin", acuerdoPago.getFechaFinal());
      this.add("Importe", acuerdoPago.getImporteTotal());
      this.add("Estado", Boolean.TRUE.equals(acuerdoPago.getActivo()) ? "Activo" : "Inactivo");
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
