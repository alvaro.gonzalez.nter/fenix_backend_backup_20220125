package com.haya.alaska.acuerdo_pago.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class AcuerdoInformeExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Agreement Id");
      cabeceras.add("Commentary");
      cabeceras.add("Start Date");
      cabeceras.add("Final Date");
      cabeceras.add("Number Amounts Installments");
      cabeceras.add("Number Total Amount");
      cabeceras.add("Number of Installments");
      cabeceras.add("Origin Agreement");
      cabeceras.add("Periodicity Payment Agreement Id");
      cabeceras.add("User");
    } else {
      cabeceras.add("Id Acuerdo");
      cabeceras.add("Comentario");
      cabeceras.add("Fecha Inicio");
      cabeceras.add("Fecha Final");
      cabeceras.add("Numero Importes Plazos");
      cabeceras.add("Numero Importe Total");
      cabeceras.add("Numero Plazos");
      cabeceras.add("Origen Acuerdo");
      cabeceras.add("Id Periodicidad Acuerdo Pago");
      cabeceras.add("Usuario");
    }
  }

  public AcuerdoInformeExcel(AcuerdoPago acuerdoPago) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Agreement Id", acuerdoPago.getId());
      this.add("Commentary", acuerdoPago.getComentario());
      this.add("Start Date", acuerdoPago.getFechaInicio());
      this.add("Final Date", acuerdoPago.getFechaFinal());
      this.add("Number Amounts Installments", acuerdoPago.getImportePlazos());
      this.add("Number Total Amount", acuerdoPago.getImporteTotal());
      this.add("Number of Installments", acuerdoPago.getNumeroPlazos());
      this.add(
          "Origin Agreement",
          acuerdoPago.getOrigenAcuerdo() != null
              ? acuerdoPago.getOrigenAcuerdo().getValorIngles()
              : null);
      this.add(
          "Periodicity Payment Agreement Id",
          acuerdoPago.getPeriodicidadAcuerdoPago() != null
              ? acuerdoPago.getPeriodicidadAcuerdoPago().getValorIngles()
              : null);
      this.add(
          "User", acuerdoPago.getUsuario() != null ? acuerdoPago.getUsuario().getNombre() : null);
    } else {
      this.add("Id Acuerdo", acuerdoPago.getId());
      this.add("Comentario", acuerdoPago.getComentario());
      this.add("Fecha Inicio", acuerdoPago.getFechaInicio());
      this.add("Fecha Final", acuerdoPago.getFechaFinal());
      this.add("Numero Importes Plazos", acuerdoPago.getImportePlazos());
      this.add("Numero Importe Total", acuerdoPago.getImporteTotal());
      this.add("Numero Plazos", acuerdoPago.getNumeroPlazos());
      this.add(
          "Origen Acuerdo",
          acuerdoPago.getOrigenAcuerdo() != null
              ? acuerdoPago.getOrigenAcuerdo().getValor()
              : null);
      this.add(
          "Id Periodicidad Acuerdo Pago",
          acuerdoPago.getPeriodicidadAcuerdoPago() != null
              ? acuerdoPago.getPeriodicidadAcuerdoPago().getValor()
              : null);
      this.add(
          "Usuario",
          acuerdoPago.getUsuario() != null ? acuerdoPago.getUsuario().getNombre() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
