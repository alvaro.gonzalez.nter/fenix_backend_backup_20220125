package com.haya.alaska.acuerdo_pago.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.origen_acuerdo.domain.OrigenAcuerdo;
import com.haya.alaska.periodicidad_acuerdo_pago.domain.PeriodicidadAcuerdoPago;
import com.haya.alaska.plazo_acuerdo_pago.domain.PlazoAcuerdoPago;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_ACUERDO_PAGO")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_ACUERDO_PAGO")
public class AcuerdoPago implements Serializable{
	public static final String PERIOCIDAD="periocidadAcuerdoPago";
	public static final String ORIGEN="origenAcuerdo";
	public static final String IMPORTETOTAL="importeTotal";

	private static final long serialVersionUID = 2185238555232983709L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	@ToString.Include
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
	private Expediente expediente;

  @ManyToOne
  @JoinColumn(name = "ID_EXP_POS_NEGOCIADA")
  private ExpedientePosesionNegociada expedientePosesionNegociada;

	@ManyToMany(mappedBy = "acuerdosPago")
	@JsonIgnore
	private Set<Propuesta> propuestas = new HashSet<>();

	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(name = "RELA_CONTRATO_ACUERDO_PAGO",
	        joinColumns = { @JoinColumn(name = "ID_ACUERDO_PAGO") },
	        inverseJoinColumns = { @JoinColumn(name = "ID_CONTRATO") }
	    )
	@JsonIgnore
	@AuditJoinTable(name = "HIST_RELA_CONTRATO_ACUERDO_PAGO")
	private Set<Contrato> contratos = new HashSet<>();

	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(name = "RELA_BIEN_POSESION_NEGOCIADA_ACUERDO_PAGO",
	        joinColumns = { @JoinColumn(name = "ID_ACUERDO_PAGO") },
	        inverseJoinColumns = { @JoinColumn(name = "ID_BIEN_POSESION_NEGOCIADA") }
	    )
	@JsonIgnore
	@AuditJoinTable(name = "HIST_RELA_BIEN_POSESION_NEGOCIADA_ACUERDO_PAGO")
	private Set<BienPosesionNegociada> bienes = new HashSet<>();

	@ManyToOne
    @JoinColumn(name = "ID_PERIODICIDAD_ACUERDO_PAGO", referencedColumnName = "ID", nullable = false)
	private PeriodicidadAcuerdoPago periodicidadAcuerdoPago;

	@ManyToOne
	@JoinColumn(name = "ID_ORIGEN_ACUERDO", referencedColumnName = "ID", nullable = false)
	private OrigenAcuerdo origenAcuerdo;

	@ManyToOne
	@JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID", nullable = false)
	private Usuario usuario;


	@Column(name = "NUM_IMPORTE_TOTAL", columnDefinition = "decimal(15,2)", nullable = false)
	Double importeTotal;

	@Column(name = "NUM_IMPORTE_PLAZOS", columnDefinition = "decimal(15,2)")
	Double importePlazos;

	@Temporal(TemporalType.DATE)
	@Column(name = "FCH_FECHA_INICIO", nullable = false)
	Date fechaInicio;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA_FINAL")
  Date fechaFinal;

  @Column(name = "NUM_NUMERO_PLAZOS", nullable = false)
  Integer numeroPlazos;

  @Column(name = "DES_COMENTARIO")
  String comentario;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ACUERDO_PAGO")
  @AuditJoinTable(name = "HIST_RELA_ACUERDO_PAGO_PLAZO_ACUERDO_PAGO")
  private Set<PlazoAcuerdoPago> plazosAcuerdoPago = new HashSet<>();


  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @Column(name = "IND_ABONADO", columnDefinition = "boolean default false")
	private Boolean abonado = false;

}
