package com.haya.alaska.acuerdo_pago.infrastructure.controller.dto;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class AcuerdoPagoDTO implements Serializable{
	private static final long serialVersionUID = 5579640048016003004L;

	private Integer id;
	private CatalogoDTO periodicidadAcuerdoPago;

	private CatalogoDTO origenAcuerdo;

	private UsuarioDTO usuario;

	Double importeTotal;

	Double importePlazos;

	Date fechaInicio;
	Date fechaFinal;

	Integer numeroPlazos;
	String comentario;

	List<PlazoAcuerdoPagoDTO> plazosAcuerdoPago = new ArrayList();
	List<Integer> contratos = new ArrayList<>();
	List<String> contratosIdOrigen = new ArrayList<>();
	Boolean activo;

	public AcuerdoPagoDTO(AcuerdoPago acuerdoPago, Boolean pn)
	{
		BeanUtils.copyProperties(acuerdoPago, this,"plazosAcuerdoPago");
//		this.setNumeroPlazos(numeroPlazos);
		this.setOrigenAcuerdo(new CatalogoDTO(acuerdoPago.getOrigenAcuerdo()));
		this.setPeriodicidadAcuerdoPago(new CatalogoDTO(acuerdoPago.getPeriodicidadAcuerdoPago()));
		if (acuerdoPago.getPlazosAcuerdoPago()!=null )
		{
			List<PlazoAcuerdoPagoDTO> plazosAcuerdoPagoDto = new ArrayList<>();
			for (var plazo: acuerdoPago.getPlazosAcuerdoPago())
			{
				plazosAcuerdoPagoDto.add(PlazoAcuerdoPagoDTO.builder().
						cantidad(plazo.getCantidad()).
						fecha(plazo.getFecha()).
						abonado(plazo.getAbonado()).
						build());
			}
			this.setPlazosAcuerdoPago(plazosAcuerdoPagoDto);
		}
		this.setUsuario(new UsuarioDTO(acuerdoPago.getUsuario()));
		if (pn){
      if (acuerdoPago.getBienes()!=null )
      {
        this.setContratos(acuerdoPago.getBienes().stream().
          map(b -> b.getId()).
          collect(Collectors.toList())) ;
      }
    } else {
      if (acuerdoPago.getContratos() != null)
      {
        this.setContratos(acuerdoPago.getContratos().stream().
          map(Contrato::getId).
          collect(Collectors.toList())) ;
        this.setContratosIdOrigen(acuerdoPago.getContratos().stream().
          map(Contrato::getIdCarga).
          collect(Collectors.toList())); ;
      }
    }
	}

	public static List<AcuerdoPagoDTO> newList(Collection<AcuerdoPago> acuerdosPago, Boolean pn) {
		return acuerdosPago.stream().filter(AcuerdoPago::getActivo).
				map(acuerdoPago -> new AcuerdoPagoDTO(acuerdoPago, pn)).collect(Collectors.toList());
	}
}
