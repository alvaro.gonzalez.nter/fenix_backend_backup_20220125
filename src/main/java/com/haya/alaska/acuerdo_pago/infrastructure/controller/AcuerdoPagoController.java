package com.haya.alaska.acuerdo_pago.infrastructure.controller;

import com.haya.alaska.acuerdo_pago.aplication.AcuerdoPagoUseCase;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoDTO;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoInputDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.ErrorResponse;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

/**
 * @author diegotobalina created on 24/06/2020
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientes/{idExpediente}/acuerdopago")
public class AcuerdoPagoController {
  @Autowired
  AcuerdoPagoUseCase acuerdoPagoUseCase;

  @ApiOperation(value = "Añadir un acuerdo de pago  a un expediente")
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @Transactional(rollbackFor = Exception.class)
  public AcuerdoPagoDTO createAcuerdoPago(@PathVariable("idExpediente") Integer idExpediente,
                                          @RequestBody @Valid AcuerdoPagoInputDTO acuerdoPagoInputDTO,
                                          @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                          @ApiIgnore CustomUserDetails principal) throws Exception {
    return acuerdoPagoUseCase.createAcuerdo(idExpediente, acuerdoPagoInputDTO, (Usuario) principal.getPrincipal(), posesionNegociada);
  }

  @ApiOperation(value = "Listado Acuerdos de Pago ",
    notes = "Devuelve todos los acuerdos de pago")
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public ListWithCountDTO getAllAcuerdos(
    @PathVariable("idExpediente") Integer idExpediente,
    @RequestParam(value = "periodicidadAcuerdoPago", required = false) String periodicidad,
    @RequestParam(value = "origenAcuerdo", required = false) String origen,
    @RequestParam(value = "importeTotal", required = false) String importeTotal,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size", required = true) Integer size,
    @RequestParam(value = "page", required = true) Integer page,
    @RequestParam(value = "importePlazos", required = false) String importePlazos,
    @RequestParam(value = "fechaInicio", required = false) String fechaInicio,
    @RequestParam(value = "fechaFinal", required = false) String fechaFinal,
    @RequestParam(value = "numeroPlazos", required = false) String numeroPlazos,
    @RequestParam(value = "usuario", required = false) String usuario) {

    return acuerdoPagoUseCase.getAllAcuerdos(idExpediente, periodicidad, origen, importeTotal, orderField,
      orderDirection, importePlazos, fechaInicio,  fechaFinal, numeroPlazos,usuario,size, page);
  }

  @ApiOperation(value = "Devuelve los datos de un acuerdo de pago")
  @GetMapping("/{idAcuerdo}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<AcuerdoPagoDTO> getAcuerdo(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idAcuerdo") Integer idAcuerdo) {
    try {
      var acuerdoPagoDTO = acuerdoPagoUseCase.getAcuerdo(idAcuerdo);
      return ResponseEntity.ok(acuerdoPagoDTO);
    } catch (NotFoundException e) {
      return ResponseEntity.notFound().build();
    }
  }

  @ApiOperation(value = "Modificar un acuerdo de pago  de un expediente")
  @PutMapping("/{idAcuerdo}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @Transactional(rollbackFor = Exception.class)
  public AcuerdoPagoDTO updateDocumentoContrato(@PathVariable("idExpediente") Integer idExpediente,
                                                @PathVariable("idAcuerdo") Integer idAcuerdo,
                                                @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                                @RequestBody @Valid AcuerdoPagoInputDTO acuerdoPagoInputDTO) throws NotFoundException, InvalidCodeException {
    return acuerdoPagoUseCase.updateAcuerdo(idExpediente, idAcuerdo, acuerdoPagoInputDTO, posesionNegociada);
  }

  @ApiOperation(value = "Borrar un acuerdo de pago  de un expediente")
  @DeleteMapping("/{idAcuerdo}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void deleteDocumentoContrato(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idAcuerdo") Integer idAcuerdo)
    throws NotFoundException {
    acuerdoPagoUseCase.ponerAcuerdoInactivo(idAcuerdo);
  }

  @ApiOperation(value = "Descargar información de un acuerdo  en Excel",
    notes = "Descargar información en Excel del acuerdo  enviado")
  @GetMapping("/{idAcuerdo}/excel")
  public ResponseEntity<InputStreamResource> getExcelByAcuerdo(@PathVariable("idExpediente") Integer idExpediente,
                                                               @PathVariable("idAcuerdo") Integer idAcuerdo) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport
      .create(acuerdoPagoUseCase.downloadExcelAcuerdo(idAcuerdo));

    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    return excelExport.download(excel, "acuerdopago_" + idAcuerdo + "_" + date);
  }

  @ApiOperation(value = "Descargar información de los acuerdos de un expediente  en Excel",
    notes = "Descargar información de los acuerdos de un expediente eh Excel")
  @GetMapping("/excel")
  public ResponseEntity<InputStreamResource> getExcelByExpediente(@PathVariable("idExpediente") Integer idExpediente) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport
      .create(acuerdoPagoUseCase.downloadExcelExpediente(idExpediente));

    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    return excelExport.download(excel, "acuerdopago_expediente" + idExpediente + date);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ErrorResponse handleValidationExceptions(MethodArgumentNotValidException ex) {
    StringBuffer errorStr = new StringBuffer();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      String errorMessage = error.getDefaultMessage();
      errorStr.append(" Campo: '" + fieldName + "' " + errorMessage + "   ");
    });

    return ErrorResponse.builder().status(HttpStatus.BAD_REQUEST.value()).
      error(errorStr.toString()).
      message("Error al validar JSON").
      timestamp(Instant.now().toString()).
      build();
  }
}
