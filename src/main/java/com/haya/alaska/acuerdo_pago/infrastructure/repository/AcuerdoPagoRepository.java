package com.haya.alaska.acuerdo_pago.infrastructure.repository;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface AcuerdoPagoRepository extends JpaRepository<AcuerdoPago, Integer> {
    @Query(
            value =
                    "SELECT  a  FROM AcuerdoPago a "
                            + "INNER JOIN Expediente e ON a.expediente.id = e.id "
                            + "WHERE e.id = :idExpediente "
                            + "AND a.activo = true order by a.id"
    )
    List<AcuerdoPago> buscaPorExpediente(Integer idExpediente);
    Optional<AcuerdoPago> findByIdAndActivoIsTrue(Integer id);
    List<AcuerdoPago> findAllByActivoIsTrue();

    List<AcuerdoPago> findAllByActivoIsTrueAndAbonadoIsFalseAndFechaFinalBetween(Date fecha1, Date fecha2);
    List<AcuerdoPago> findAllByActivoIsTrueAndAbonadoIsFalse();

}
