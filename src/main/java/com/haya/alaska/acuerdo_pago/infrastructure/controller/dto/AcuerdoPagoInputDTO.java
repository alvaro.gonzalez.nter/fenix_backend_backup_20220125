package com.haya.alaska.acuerdo_pago.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@ToString
@NoArgsConstructor
public class AcuerdoPagoInputDTO implements Serializable{

	private static final long serialVersionUID = 7059002376350390862L;

	private Integer id;

	@NotNull
	private List<Integer> contratos; // Si es posesion negociada, son ids de BienPosesionNegociada
	@NotNull
  	private Integer periodicidadAcuerdoPago;
	@NotNull
  	private Integer origenAcuerdo;
	@NotNull
  	private Double importeTotal;

  	private Double importePlazos;
	@NotNull
	private Date fechaInicio;

  	private Date fechaFinal;

	@NotNull
  	private Integer numeroPlazos;
  	private Boolean activo;

  	String comentario;

  	@NotNull
  	List<PlazoAcuerdoPagoDTO> plazosAcuerdoPago;
}
