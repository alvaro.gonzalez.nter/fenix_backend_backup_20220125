package com.haya.alaska.acuerdo_pago.infrastructure.mapper;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoInputDTO;
import com.haya.alaska.origen_acuerdo.infrastructure.repository.OrigenAcuerdoRepository;
import com.haya.alaska.periodicidad_acuerdo_pago.infrastructure.repository.PeriodicidadAcuerdoPagoRepository;
import com.haya.alaska.plazo_acuerdo_pago.domain.PlazoAcuerdoPago;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class AcuerdoPagoMapper {

	@Autowired
	PeriodicidadAcuerdoPagoRepository periodicidadAcuerdoPagoRepository;

	@Autowired
	OrigenAcuerdoRepository origenAcuerdoRepository;

	@Autowired
	UsuarioRepository usuarioRepository;

/**
 * Convierte un objeto AcuerdoPagoInputDTO a AcuerdoPago comprobando que las dependencias con otras tablas son correctas.
 * @param acuerdoPagoInputDTO
 * @param acuerdoPago
 * @param validDisable Aceptar valores si los valores de los campos estan disabled
 * @return
 * @throws NotFoundException
 */
	public AcuerdoPago acuerdoPagoInputDtoToAcuerdoPago(AcuerdoPagoInputDTO acuerdoPagoInputDTO, AcuerdoPago acuerdoPago, boolean validDisable ) throws NotFoundException
	{
		BeanUtils.copyProperties(acuerdoPagoInputDTO, acuerdoPago, "id", "usuario",  "periocidadAcuerdoPago","origenAcuerdo","plazosAcuerdoPago");

		if (acuerdoPagoInputDTO.getPlazosAcuerdoPago()!=null )
		{
			Set<PlazoAcuerdoPago> plazosAcuerdoPago = new HashSet<>();
			for (var plazo: acuerdoPagoInputDTO.getPlazosAcuerdoPago())
			{
				plazosAcuerdoPago.add(PlazoAcuerdoPago.builder().
						cantidad(plazo.getCantidad()).
						fecha(plazo.getFecha()).
						acuerdoPago(acuerdoPago).
						abonado(false).
						build());
			}
			acuerdoPago.setPlazosAcuerdoPago(plazosAcuerdoPago);
		}


		var  periocidadAcuerdoPago = periodicidadAcuerdoPagoRepository.findById(acuerdoPagoInputDTO.getPeriodicidadAcuerdoPago());
		if (periocidadAcuerdoPago.isEmpty())
			throw new NotFoundException("Periodicidad ", acuerdoPagoInputDTO.getPeriodicidadAcuerdoPago());
		if (!periocidadAcuerdoPago.get().getActivo() && !validDisable)
			throw new NotFoundException("Periodicidad ", acuerdoPagoInputDTO.getPeriodicidadAcuerdoPago(), false);
		var origenAcuerdo =  origenAcuerdoRepository.findById(acuerdoPagoInputDTO.getOrigenAcuerdo());
		if (origenAcuerdo.isEmpty())
			throw new NotFoundException("Origen de Acuerdo", acuerdoPagoInputDTO.getOrigenAcuerdo());
		if (!origenAcuerdo.get().getActivo() && ! validDisable)
			throw new NotFoundException("Origen de Acuerdo", acuerdoPagoInputDTO.getOrigenAcuerdo(), false);

		acuerdoPago.setPeriodicidadAcuerdoPago(periocidadAcuerdoPago.get());
		acuerdoPago.setOrigenAcuerdo(origenAcuerdo.get());

		acuerdoPago.setActivo(true);
		return acuerdoPago;
	}
//	public AcuerdoPagoDTO acuerdoPagoToAcuerdoPagoDto(AcuerdoPago acuerdoPago, AcuerdoPagoDTO acuerdoPagoDto)
//	{
//		BeanUtils.copyProperties(acuerdoPago, acuerdoPagoDto,"plazosAcuerdoPago");
//		acuerdoPagoDto.setNumeroPlazos(acuerdoPago.getNumeroPlazos());
//		acuerdoPagoDto.setUsuario(new UsuarioDTO(acuerdoPago.getUsuario()));
//		acuerdoPagoDto.setOrigenAcuerdo(new CatalogoDTO(acuerdoPago.getOrigenAcuerdo()));
//		acuerdoPagoDto.setPeriocidadAcuerdoPago(new CatalogoDTO(acuerdoPago.getPeriocidadAcuerdoPago()));
//		if (acuerdoPago.getPlazosAcuerdoPago()!=null )
//		{
//			List<PlazoAcuerdoPagoDTO> plazosAcuerdoPagoDto = new ArrayList<>();
//			for (var plazo: acuerdoPago.getPlazosAcuerdoPago())
//			{
//				plazosAcuerdoPagoDto.add(PlazoAcuerdoPagoDTO.builder().
//						cantidad(plazo.getCantidad()).
//						fecha(plazo.getFecha()).
//						build());
//			}
//			acuerdoPagoDto.setPlazosAcuerdoPago(plazosAcuerdoPagoDto);
//		}
//		if (acuerdoPago.getContratosNuevos()!=null )
//		{
//			acuerdoPagoDto.setContratos(acuerdoPago.getContratosNuevos().stream().
//					map(t -> t.getId()).
//					collect(Collectors.toList())) ;
//		}
//		return acuerdoPagoDto;
//	}

}
