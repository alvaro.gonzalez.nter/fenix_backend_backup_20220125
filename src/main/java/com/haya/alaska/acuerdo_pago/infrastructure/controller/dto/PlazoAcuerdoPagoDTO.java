package com.haya.alaska.acuerdo_pago.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PlazoAcuerdoPagoDTO implements Serializable{	
	private static final long serialVersionUID = 1L;
	Date fecha;
	double cantidad;
	boolean abonado;
}
