package com.haya.alaska.acuerdo_pago.aplication;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoDTO;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoInputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

public interface AcuerdoPagoUseCase {
    AcuerdoPagoDTO createAcuerdo(Integer idExpediente, AcuerdoPagoInputDTO acuerdoPagoInputDTO, Usuario usuario, Boolean posesionNegociada) throws NotFoundException;
    ListWithCountDTO<AcuerdoPagoDTO> getAllAcuerdos(Integer idExpediente, String periocidad, String origen, String importeTotal, String orderField, String orderDirection,String importePlazos, String fechaInicio, String fechaFinal, String numeroPlazos, String usuario,
                                    int size, int  page);
    AcuerdoPagoDTO getAcuerdo(Integer idAcuerdo) throws NotFoundException;
    AcuerdoPagoDTO updateAcuerdo(Integer idExpediente, Integer idAcuerdo,AcuerdoPagoInputDTO acuerdoPagoInputDTO, Boolean posesionNegociada) throws NotFoundException, InvalidCodeException;
    void ponerAcuerdoInactivo(Integer idAcuerdo) throws NotFoundException;
    LinkedHashMap<String, List<Collection<?>>> downloadExcelExpediente(Integer idExpediente) throws NotFoundException;
    LinkedHashMap<String, List<Collection<?>>> downloadExcelAcuerdo(Integer idAcuerdo) throws NotFoundException;
    AcuerdoPago createAcuerdoPago(Integer idExpediente, AcuerdoPagoInputDTO acuerdoPagoInputDTO, Usuario usuario, Boolean posesionNegociada) throws NotFoundException;
    List<AcuerdoPago> getAllAcuerdosDePagoByDates(Date fecha1, Date fecha2);
    List<AcuerdoPago> getAllAcuerdosDePagoByAbonadoIsFalse();

}
