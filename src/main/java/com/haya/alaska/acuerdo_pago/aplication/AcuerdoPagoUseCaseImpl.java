package com.haya.alaska.acuerdo_pago.aplication;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago_;
import com.haya.alaska.acuerdo_pago.domain.CuadroAcuerdoPagoExcel;
import com.haya.alaska.acuerdo_pago.domain.ExpedienteAcuerdoPagoExcel;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoDTO;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoInputDTO;
import com.haya.alaska.acuerdo_pago.infrastructure.mapper.AcuerdoPagoMapper;
import com.haya.alaska.acuerdo_pago.infrastructure.repository.AcuerdoPagoRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.origen_acuerdo.domain.OrigenAcuerdo;
import com.haya.alaska.origen_acuerdo.domain.OrigenAcuerdo_;
import com.haya.alaska.periodicidad_acuerdo_pago.domain.PeriodicidadAcuerdoPago;
import com.haya.alaska.periodicidad_acuerdo_pago.domain.PeriodicidadAcuerdoPago_;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class AcuerdoPagoUseCaseImpl implements AcuerdoPagoUseCase {
  @Autowired ContratoRepository contratoRepository;

  @Autowired BienPosesionNegociadaRepository bpnRepository;

  @Autowired ExpedienteRepository expedienteRepository;

  @Autowired ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;

  @Autowired AcuerdoPagoMapper acuerdoPagoMapper;

  @Autowired AcuerdoPagoRepository acuerdoPagoRepository;

  @PersistenceContext private EntityManager entityManager;

  @Override
  public List<AcuerdoPago> getAllAcuerdosDePagoByDates(Date fecha1, Date fecha2) {
    return acuerdoPagoRepository.findAllByActivoIsTrueAndAbonadoIsFalseAndFechaFinalBetween(
        fecha1, fecha2);
  }

  @Override
  public List<AcuerdoPago> getAllAcuerdosDePagoByAbonadoIsFalse() {
    return acuerdoPagoRepository.findAllByActivoIsTrueAndAbonadoIsFalse();
  }

  public AcuerdoPagoDTO createAcuerdo(
      Integer idExpediente,
      AcuerdoPagoInputDTO acuerdoPagoInputDTO,
      Usuario usuario,
      Boolean posesionNegociada)
      throws NotFoundException {
    var acuerdoPago =
        acuerdoPagoMapper.acuerdoPagoInputDtoToAcuerdoPago(
            acuerdoPagoInputDTO, new AcuerdoPago(), false);
    acuerdoPago.setUsuario(usuario);
    actualizaReferencias(idExpediente, acuerdoPagoInputDTO, acuerdoPago, posesionNegociada);

    var acuerdoPagoSaved = acuerdoPagoRepository.saveAndFlush(acuerdoPago);

    return new AcuerdoPagoDTO(acuerdoPagoSaved, posesionNegociada);
  }

  public AcuerdoPago createAcuerdoPago(
      Integer idExpediente,
      AcuerdoPagoInputDTO acuerdoPagoInputDTO,
      Usuario usuario,
      Boolean posesionNegociada)
      throws NotFoundException {
    var acuerdoPago =
        acuerdoPagoMapper.acuerdoPagoInputDtoToAcuerdoPago(
            acuerdoPagoInputDTO, new AcuerdoPago(), false);
    acuerdoPago.setUsuario(usuario);
    actualizaReferencias(idExpediente, acuerdoPagoInputDTO, acuerdoPago, posesionNegociada);
    return acuerdoPagoRepository.saveAndFlush(acuerdoPago);
  }

  void actualizaReferencias(
      Integer idExpediente,
      AcuerdoPagoInputDTO acuerdoPagoInputDTO,
      AcuerdoPago acuerdoPago,
      Boolean posesionNegociada)
      throws NotFoundException {
    /*Dependiendo de si es en posesiónNegociada o nó, el array de Contratos se refiere a los ids de
     * bienesPosesionNegociada o contratos, respectivamente*/
    if (posesionNegociada) {
      Set<BienPosesionNegociada> bienes = new HashSet<>();
      for (Integer idBienPN : acuerdoPagoInputDTO.getContratos()) {
        var bienPN = bpnRepository.findById(idBienPN);
        if (bienPN.isEmpty()) {
          throw new NotFoundException("Bien posesión negociada ", idBienPN);
        }
        if (!bienPN.get().getActivo()) {
          throw new NotFoundException("Bien posesión negociada ", idBienPN, false);
        }
        bienes.add(bienPN.get());
      }
      acuerdoPago.setBienes(bienes);
    } else {
      Set<Contrato> contratos = new HashSet<>();
      for (Integer idContrato : acuerdoPagoInputDTO.getContratos()) {
        var contrato = contratoRepository.findById(idContrato);
        if (contrato.isEmpty()) {
          throw new NotFoundException("Contrato ", idContrato);
        }
        if (!contrato.get().getActivo()) {
          throw new NotFoundException("Contrato ", idContrato, false);
        }
        contratos.add(contrato.get());
      }
      acuerdoPago.setContratos(contratos);
    }

    if (posesionNegociada) {
      var expendientePN =
          expedientePosesionNegociadaRepository
              .findById(idExpediente)
              .orElseThrow(
                  () -> new NotFoundException("Expediente de posesión negociada", idExpediente));
      if (!expendientePN.getActivo()) {
        throw new NotFoundException("Expediente de posesión negociada ", idExpediente, false);
      }
      acuerdoPago.setExpedientePosesionNegociada(expendientePN);
    } else {
      var expendiente =
          expedienteRepository
              .findById(idExpediente)
              .orElseThrow(() -> new NotFoundException("Expediente", idExpediente));
      if (!expendiente.getActivo()) {
        throw new NotFoundException("Expediente ", idExpediente, false);
      }
      acuerdoPago.setExpediente(expendiente);
    }
  }

  @Transactional
  public AcuerdoPagoDTO updateAcuerdo(
      Integer idExpediente,
      Integer idAcuerdo,
      AcuerdoPagoInputDTO acuerdoPagoInputDTO,
      Boolean posesionNegociada)
      throws NotFoundException, InvalidCodeException {
    if (acuerdoPagoInputDTO.getId() != null && acuerdoPagoInputDTO.getId() != idAcuerdo) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCodeException("Parameter ID does not match the JSON Object");
      else throw new InvalidCodeException("ID de parametro no coincide con el del Objeto JSON");
    }
    var acuerdoPago =
        acuerdoPagoMapper.acuerdoPagoInputDtoToAcuerdoPago(
            acuerdoPagoInputDTO, new AcuerdoPago(), false);
    var acuerdoPagoOpt =
        acuerdoPagoRepository
            .findByIdAndActivoIsTrue(idAcuerdo)
            .orElseThrow(() -> new NotFoundException("Acuerdo", idAcuerdo));
    acuerdoPago.setId(idAcuerdo);
    actualizaReferencias(idExpediente, acuerdoPagoInputDTO, acuerdoPago, false);
    acuerdoPago.setUsuario(acuerdoPagoOpt.getUsuario());
    acuerdoPago.setActivo(true);
    acuerdoPagoRepository.save(acuerdoPago);

    return new AcuerdoPagoDTO(acuerdoPago, false);
  }

  @Transactional
  public void ponerAcuerdoInactivo(Integer idAcuerdo) throws NotFoundException {
    AcuerdoPago acuerdoPago =
        acuerdoPagoRepository
            .findByIdAndActivoIsTrue(idAcuerdo)
            .orElseThrow(() -> new NotFoundException("Acuerdo", idAcuerdo));
    acuerdoPago.setActivo(false);
    acuerdoPagoRepository.save(acuerdoPago);
  }

  public AcuerdoPago findAcuerdoById(Integer idAcuerdo) throws NotFoundException {
    var acuerdoPago =
        acuerdoPagoRepository
            .findByIdAndActivoIsTrue(idAcuerdo)
            .orElseThrow(() -> new NotFoundException("Acuerdo", idAcuerdo));
    return acuerdoPago;
  }

  public AcuerdoPagoDTO getAcuerdo(Integer idAcuerdo) throws NotFoundException {
    var acuerdoPago =
        acuerdoPagoRepository
            .findByIdAndActivoIsTrue(idAcuerdo)
            .orElseThrow(() -> new NotFoundException("Acuerdo", idAcuerdo));
    return new AcuerdoPagoDTO(acuerdoPago, false);
  }

  public ListWithCountDTO<AcuerdoPagoDTO> getAllAcuerdos(
      Integer idExpediente,
      String periocidad,
      String origen,
      String importeTotal,
      String orderField,
      String orderDirection,
      String importePlazos,
      String fechaInicio,
      String fechaFinal,
      String numeroPlazos,
      String usuario,
      int size,
      int page) {
    List<AcuerdoPago> acuerdosPago =
        getDataFiltered(
            periocidad,
            origen,
            importeTotal,
            orderField,
            orderDirection,
            importePlazos,
            fechaInicio,
            fechaFinal,
            numeroPlazos,
            usuario,
            idExpediente);
    int numElementos = acuerdosPago.size();
    List<AcuerdoPago> listaAcuerdos =
        acuerdosPago.stream().skip(size * page).limit(size).collect(Collectors.toList());
    List<AcuerdoPagoDTO> acuerdosPagoDTO = new ArrayList<>();
    listaAcuerdos.forEach(
        acuerdoPago -> acuerdosPagoDTO.add(new AcuerdoPagoDTO(acuerdoPago, false)));
    return new ListWithCountDTO<>(acuerdosPagoDTO, numElementos);
  }

  public LinkedHashMap<String, List<Collection<?>>> downloadExcelExpediente(Integer idExpediente)
      throws NotFoundException {
    var acuerdosPago = acuerdoPagoRepository.buscaPorExpediente(idExpediente);
    if (acuerdosPago.size() == 0)
      throw new NotFoundException("Acuerdos", "Expediente", idExpediente, false);
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> cuadros = new ArrayList<>();
    cuadros.add(CuadroAcuerdoPagoExcel.cabeceras);
    List<Collection<?>> expediente = new ArrayList<>();
    expediente.add(ExpedienteAcuerdoPagoExcel.cabeceras);
    for (AcuerdoPago acuerdoPago : acuerdosPago) {
      cuadros.add(new CuadroAcuerdoPagoExcel(acuerdoPago).getValuesList());
      expediente.add(new ExpedienteAcuerdoPagoExcel(acuerdoPago).getValuesList());
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      datos.put("Tables", cuadros);
      datos.put("Connection", expediente);

    } else {
      datos.put("Cuadros", cuadros);
      datos.put("expediente", expediente);
    }
    return datos;
  }

  /**
   * Busca acuerdos de expedientes activos con diferentes filtros
   *
   * @param periodicidad
   * @param origen
   * @param importeTotal
   * @param orderField
   * @param orderDirection
   * @param idExpediente
   * @return
   */
  public List<AcuerdoPago> getDataFiltered(
      String periodicidad,
      String origen,
      String importeTotal,
      String orderField,
      String orderDirection,
      String importePlazos,
      String fechaInicio,
      String fechaFinal,
      String numeroPlazos,
      String usuario,
      Integer idExpediente) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<AcuerdoPago> query = cb.createQuery(AcuerdoPago.class);

    Root<AcuerdoPago> root = query.from(AcuerdoPago.class);
    List<Predicate> predicates = new ArrayList<>();
    predicates.add(cb.equal(root.get(AcuerdoPago_.activo), true)); // Buscar solo activos.

    Join<AcuerdoPago, Expediente> ExpedienteJoin =
        root.join(AcuerdoPago_.expediente, JoinType.INNER);
    Predicate onCondExpediente = cb.equal(ExpedienteJoin.get(Expediente_.id), idExpediente);
    ExpedienteJoin.on(onCondExpediente);

    if (origen != null) {
      Join<AcuerdoPago, OrigenAcuerdo> acuerdoJoin =
          root.join(AcuerdoPago_.origenAcuerdo, JoinType.INNER);
      Predicate onCond =
          cb.like(
              cb.upper(acuerdoJoin.get(OrigenAcuerdo_.valor)), "%" + origen.toUpperCase() + "%");
      acuerdoJoin.on(onCond);
    }
    if (periodicidad != null) {
      Join<AcuerdoPago, PeriodicidadAcuerdoPago> periocidadJoin =
          root.join(AcuerdoPago_.periodicidadAcuerdoPago, JoinType.INNER);
      Predicate onCond =
          cb.like(
              cb.upper(periocidadJoin.get(PeriodicidadAcuerdoPago_.valor)),
              "%" + periodicidad.toUpperCase() + "%");
      periocidadJoin.on(onCond);
    }
    if (importeTotal != null) {
      predicates.add(
          cb.like(root.get(AcuerdoPago_.importeTotal).as(String.class), "%" + importeTotal + "%"));
    }
    if (fechaInicio != null) {
      predicates.add(
          cb.like(root.get(AcuerdoPago_.fechaInicio).as(String.class), "%" + fechaInicio + "%"));
    }
    if (fechaFinal != null) {
      predicates.add(
          cb.like(root.get(AcuerdoPago_.fechaFinal).as(String.class), "%" + fechaFinal + "%"));
    }
    if (importePlazos != null) {
      predicates.add(
          cb.like(
              root.get(AcuerdoPago_.importePlazos).as(String.class), "%" + importePlazos + "%"));
    }
    if (numeroPlazos != null) {
      predicates.add(
          cb.like(root.get(AcuerdoPago_.numeroPlazos).as(String.class), "%" + numeroPlazos + "%"));
    }
    if (usuario != null) {
      predicates.add(cb.like(root.get(AcuerdoPago_.usuario).as(String.class), "%" + usuario + "%"));
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    if (orderField != null) {
      if (orderDirection.equalsIgnoreCase("desc")) rs.orderBy(cb.desc(root.get(orderField)));
      else rs.orderBy(cb.asc(root.get(orderField)));
    }

    List<AcuerdoPago> acuerdosPago = entityManager.createQuery(query).getResultList();

    return acuerdosPago;
  }

  public LinkedHashMap<String, List<Collection<?>>> downloadExcelAcuerdo(Integer idAcuerdo)
      throws NotFoundException {
    var acuerdoPago =
        acuerdoPagoRepository
            .findByIdAndActivoIsTrue(idAcuerdo)
            .orElseThrow(() -> new NotFoundException("Acuerdo", idAcuerdo));

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> cuadros = new ArrayList<>();
    cuadros.add(CuadroAcuerdoPagoExcel.cabeceras);
    cuadros.add(new CuadroAcuerdoPagoExcel(acuerdoPago).getValuesList());

    List<Collection<?>> expediente = new ArrayList<>();
    expediente.add(ExpedienteAcuerdoPagoExcel.cabeceras);
    expediente.add(new ExpedienteAcuerdoPagoExcel(acuerdoPago).getValuesList());

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      datos.put("Tables", cuadros);
      datos.put("Connection", expediente);

    } else {
      datos.put("Cuadros", cuadros);
      datos.put("Expediente", expediente);
    }
    return datos;
  }
}
