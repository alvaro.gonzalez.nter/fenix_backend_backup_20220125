package com.haya.alaska.movil_gestor_documental.infrastructure.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DocumentoDownloadDto {
  private String base64;
  private String tipo;
}
