package com.haya.alaska.movil_gestor_documental.infrastructure.controller;

import com.haya.alaska.movil_gestor_documental.application.Base64ConverterUseCase;
import com.haya.alaska.movil_gestor_documental.infrastructure.dto.VisitaRAMovilUploadDto;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.visita_ra.application.VisitaRAUseCase;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRAOutputDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/movil/expediente/{idExpediente}/bien/{idBien}/visitas")
public class MovilVisitaRAController {

  @Autowired
  private VisitaRAUseCase visitaUseCase;

  @Autowired
  private Base64ConverterUseCase base64Converter;

  @ApiOperation(value = "Crear visita")
  @PostMapping
  public VisitaRAOutputDto createVisita(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idBien") Integer idBien,
    @RequestBody VisitaRAMovilUploadDto visita,
    /*@RequestParam(value = "fotos", required = false) List<MultipartFile> fotos,
    @RequestPart(value = "visita") @Valid VisitaInputDto visitaInputDto,*/
    @ApiIgnore CustomUserDetails principal
  ) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();

    List<MultipartFile> multipartFiles = new ArrayList<MultipartFile>();
    if(visita.getFiles()!= null && !visita.getFiles().isEmpty()){
      for(int i=0; i<visita.getFiles().size();i++) {
        MultipartFile file = base64Converter.converter(visita.getFiles().get(i));
        multipartFiles.add(file);
      }
    }
    return visitaUseCase.createVisita(idExpediente, idBien, visita.getVisitaInputDto(), multipartFiles,usuario);
  }

  // IMPORTANTE, el PutMapping no tiene soporte para el mapeo del RequestPart que utilizamos aqui, por eso es un Post:
  // https://stackoverflow.com/questions/16230291/requestpart-with-mixed-multipart-request-spring-mvc-3-2
  @ApiOperation(value = "Actualizar visita", notes = "Actualizar la visita con id enviado")
  @PutMapping("/{idVisita}")
  public VisitaRAOutputDto updateVisita(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idBien") Integer idBien,
    @PathVariable("idVisita") Integer idVisita,
    @RequestBody VisitaRAMovilUploadDto visita,
    @ApiIgnore CustomUserDetails principal
  ) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();

    List<MultipartFile> multipartFiles = new ArrayList<MultipartFile>();
    if(visita.getFiles()!= null && !visita.getFiles().isEmpty()){
      for(int i=0; i<visita.getFiles().size();i++) {
        MultipartFile file = base64Converter.converter(visita.getFiles().get(i));
        multipartFiles.add(file);
      }
    }
    return visitaUseCase.updateVisita(idBien, idVisita, visita.getVisitaInputDto(), multipartFiles, usuario);
  }
}
