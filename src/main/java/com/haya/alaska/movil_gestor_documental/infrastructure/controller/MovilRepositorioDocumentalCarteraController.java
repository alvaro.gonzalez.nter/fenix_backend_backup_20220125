package com.haya.alaska.movil_gestor_documental.infrastructure.controller;


import com.haya.alaska.cartera.application.CarteraUseCase;
import com.haya.alaska.movil_gestor_documental.application.Base64ConverterUseCase;
import com.haya.alaska.movil_gestor_documental.infrastructure.dto.DocumentoUploadDto;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Locale;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/movil/carteras")
public class MovilRepositorioDocumentalCarteraController {

  @Autowired
  private CarteraUseCase carteraUseCase;
  @Autowired
  private Base64ConverterUseCase base64Converter;


  @ApiOperation(value = "Añadir un documento a una cartera")
  @PostMapping("/{idCartera}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoCartera(
    @PathVariable("idCartera") Integer idCartera,
    @RequestBody DocumentoUploadDto document,
    @RequestParam("idMatricula") String idMatricula,
    @RequestParam("tipoModeloCartera") String tipoModeloCartera,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    if(document.getFile()!= null && !document.getFile().isEmpty()){
      MultipartFile file = base64Converter.converter(document.getFile());
      carteraUseCase.createDocumento(idCartera, idMatricula,file, usuario,tipoModeloCartera);
    }else{
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The new document must contain a file");
      else throw new NotFoundException("Debe de contener archivo el nuevo documento");
    }
  }

  @ApiOperation(value = "Añadir un documento a un contenedor general")
  @PostMapping("/documentosGeneral")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoGeneral(
    @RequestBody DocumentoUploadDto document,
    @RequestParam("idMatricula") String idMatricula,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    if(document.getFile()!= null && !document.getFile().isEmpty()){
      MultipartFile file = base64Converter.converter(document.getFile());
      carteraUseCase.createDocumentoGeneral(idMatricula, file, usuario);
    }else{
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The new document must contain a file");
      else throw new NotFoundException("Debe de contener archivo el nuevo documento");
    }


  }
}
