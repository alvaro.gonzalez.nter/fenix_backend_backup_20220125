package com.haya.alaska.movil_gestor_documental.infrastructure.dto;

import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaInputDto;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VisitaMovilUploadDto {
  private List<String> files;
  private VisitaInputDto visitaInputDto;
}
