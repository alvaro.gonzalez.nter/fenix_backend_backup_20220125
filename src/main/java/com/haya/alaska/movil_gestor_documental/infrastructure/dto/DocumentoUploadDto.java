package com.haya.alaska.movil_gestor_documental.infrastructure.dto;

import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import lombok.*;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DocumentoUploadDto {
  private String file;
  private MetadatoDocumentoInputDTO metadatoDocumento;
  private String nombre;
}
