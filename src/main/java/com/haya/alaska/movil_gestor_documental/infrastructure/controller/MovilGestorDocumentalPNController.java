package com.haya.alaska.movil_gestor_documental.infrastructure.controller;

import com.haya.alaska.bien_posesion_negociada.application.BienPosesionNegociadaUseCase;
import com.haya.alaska.movil_gestor_documental.application.Base64ConverterUseCase;
import com.haya.alaska.movil_gestor_documental.infrastructure.dto.DocumentoUploadDto;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Locale;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/movil/expedientePosesionNegociada/{idExpediente}/bien")
public class MovilGestorDocumentalPNController {

  @Autowired
  private BienPosesionNegociadaUseCase bienPosesionNegociadaUseCase;
  @Autowired
  private Base64ConverterUseCase base64Converter;

  @ApiOperation(value="Añadir un documento a un bienPN")
  @PostMapping("/{idBienPN}/documentos")
  public void createDocumentoBienPN(@PathVariable("idExpediente")Integer idExpediente,
                                    @PathVariable("idBienPN") Integer idBienPN,
                                    @RequestBody DocumentoUploadDto document,
                                    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();

    if(document.getFile()!= null && !document.getFile().isEmpty()){
      MultipartFile file = base64Converter.converter(document.getFile());
      bienPosesionNegociadaUseCase.createDocumento(idBienPN,file,document.getMetadatoDocumento(),usuario);
    }else{
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The new document must contain a file");
      else throw new NotFoundException("Debe de contener archivo el nuevo documento");
    }
  }
}
