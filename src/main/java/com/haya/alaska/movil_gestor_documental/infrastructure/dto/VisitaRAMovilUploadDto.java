package com.haya.alaska.movil_gestor_documental.infrastructure.dto;

import com.haya.alaska.visita.infraestructure.controller.dto.VisitaInputDto;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRAInputDto;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VisitaRAMovilUploadDto {
  private List<String> files;
  private VisitaRAInputDto visitaInputDto;
}
