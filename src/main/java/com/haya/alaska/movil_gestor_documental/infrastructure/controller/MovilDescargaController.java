package com.haya.alaska.movil_gestor_documental.infrastructure.controller;

import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.model.respuesta.RespuestaDescargaDocumento;
import com.haya.alaska.movil_gestor_documental.infrastructure.dto.DocumentoDownloadDto;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Base64;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/movil/documentos")
public class MovilDescargaController {

  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;


  @ApiOperation(value = "Descargar documento", notes = "Obtiene un documento del gestor documental")
  @GetMapping("/{id}")
  public ResponseEntity<DocumentoDownloadDto> descargar(@PathVariable("id") Integer id) throws IOException {
    RespuestaDescargaDocumento respuestaDocumento = servicioGestorDocumental.descargarDocumento(id);

    String nombrearchivo=respuestaDocumento.getContentDisposition();
    String tipo = StringUtils.remove(nombrearchivo.substring(nombrearchivo.lastIndexOf('.') + 1),'"');

    byte[] bytes = IOUtils.toByteArray(respuestaDocumento.getBody());
    String encoded = Base64.getEncoder().encodeToString(bytes);


    DocumentoDownloadDto documento = new DocumentoDownloadDto();
    documento.setBase64(encoded);
    documento.setTipo(tipo);

    return ResponseEntity.ok(documento);

  }

}
