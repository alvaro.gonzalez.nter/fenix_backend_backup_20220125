package com.haya.alaska.movil_gestor_documental.infrastructure.controller;


import com.haya.alaska.bien.application.BienUseCase;
import com.haya.alaska.contrato.application.ContratoUseCase;
import com.haya.alaska.interviniente.application.IntervinienteUseCase;
import com.haya.alaska.movil_gestor_documental.application.Base64ConverterUseCase;
import com.haya.alaska.movil_gestor_documental.infrastructure.dto.DocumentoUploadDto;
import com.haya.alaska.propuesta.aplication.PropuestaUseCase;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.Locale;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/movil/expedientes/{idExpediente}")
public class MovilGestorDocumentalRAController {

  @Autowired
  private ContratoUseCase contratoUseCase;
  @Autowired
  private BienUseCase bienUseCase;
  @Autowired
  private IntervinienteUseCase intervinienteUseCase;
  @Autowired
  private PropuestaUseCase propuestaUseCase;
  @Autowired
  private Base64ConverterUseCase base64Converter;

  @ApiOperation(value = "Añadir un documento a un contrato")
  @PostMapping("/contratos/{idContrato}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoContratoMovil(@PathVariable("idExpediente") Integer idExpediente,
                                           @PathVariable("idContrato") Integer idContrato,
                                           @RequestBody DocumentoUploadDto document,
                                           @RequestParam(value = "tipoDocumento", required = false) String tipoDocumento,
                                           @ApiIgnore CustomUserDetails principal)
    throws NotFoundException, IOException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    if(document.getFile()!= null && !document.getFile().isEmpty()){
      MultipartFile file = base64Converter.converter(document.getFile());
      contratoUseCase.createDocumento(idContrato, file,document.getMetadatoDocumento(), usuario,tipoDocumento);
    }else{
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The new document must contain a file");
      else throw new NotFoundException("Debe de contener archivo el nuevo documento");
    }
  }

  @ApiOperation(value = "Añadir un documento a un bien")
  @PostMapping("/contratos/{idContrato}/bienes/{idBien}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoBien(@PathVariable("idExpediente") Integer idExpediente,
                                  @PathVariable("idContrato") Integer idContrato,
                                  @PathVariable("idBien") Integer idBien,
                                  @RequestParam(value = "tipoDocumento", required = false) String tipoDocumento,
                                  @RequestBody DocumentoUploadDto document,
                                  @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();

    if(document.getFile()!= null && !document.getFile().isEmpty()){
      MultipartFile file = base64Converter.converter(document.getFile());
      bienUseCase.createDocumento(idBien, file,document.getMetadatoDocumento(),usuario,tipoDocumento);
    }else{
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The new document must contain a file");
      else throw new NotFoundException("Debe de contener archivo el nuevo documento");
    }
  }

  @ApiOperation(value = "Añadir un documento a un interviniente")
  @PostMapping("/contratos/{idContrato}/intervinientes/{idInterviniente}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoInterviniente(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idContrato") Integer idContrato,
    @PathVariable("idInterviniente") Integer idInterviniente,
    @RequestParam(value = "tipoDocumento", required = false) String tipoDocumento,
    @RequestBody DocumentoUploadDto document,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();

    if(document.getFile()!= null && !document.getFile().isEmpty()){
      MultipartFile file = base64Converter.converter(document.getFile());
      intervinienteUseCase.createDocumentoInterviniente(idInterviniente, idExpediente, file, document.getMetadatoDocumento(),usuario,tipoDocumento);
    }else{
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The new document must contain a file");
      else throw new NotFoundException("Debe de contener archivo el nuevo documento");
    }
  }



  @ApiOperation(value = "Añadir un documento a una propuesta")
  @PostMapping("/propuesta/{idPropuesta}/contratos/{idContrato}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                       @PathVariable("idContrato") Integer idContrato,
                                       @PathVariable("idPropuesta") Integer idPropuesta,
                                       @RequestParam("idMatricula") String idMatricula,
                                       @RequestParam("idTipoPropuesta") Integer idTipoPropuesta,
                                       @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                       @RequestBody DocumentoUploadDto document,
                                       @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();

    if(document.getFile()!= null && !document.getFile().isEmpty()){
      MultipartFile file = base64Converter.converter(document.getFile());
      propuestaUseCase.createDocumento(idExpediente, idPropuesta, file, idMatricula, idTipoPropuesta, posesionNegociada, usuario);
    }else{
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The new document must contain a file");
      else throw new NotFoundException("Debe de contener archivo el nuevo documento");
    }

  }

}
