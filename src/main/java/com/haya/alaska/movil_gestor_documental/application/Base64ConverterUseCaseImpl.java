package com.haya.alaska.movil_gestor_documental.application;

import com.haya.alaska.movil_gestor_documental.util.Base64Decoder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;

@Component
public class Base64ConverterUseCaseImpl implements Base64ConverterUseCase {

  @Override
  public MultipartFile converter(String source){
    String [] charArray = source.split(",");
    Base64.Decoder decoder = Base64.getDecoder();
    byte[] bytes = new byte[0];
    bytes = decoder.decode(charArray[1]);
    for (int i=0;i<bytes.length;i++){
      if(bytes[i]<0){
        bytes[i]+=256;
      }
    }
    return Base64Decoder.multipartFile(bytes,charArray[0]);
  }
}
