package com.haya.alaska.movil_gestor_documental.application;

import org.springframework.web.multipart.MultipartFile;

public interface Base64ConverterUseCase {
  MultipartFile converter(String source);
}
