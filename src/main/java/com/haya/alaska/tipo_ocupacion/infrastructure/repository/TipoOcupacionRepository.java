package com.haya.alaska.tipo_ocupacion.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_ocupacion.domain.TipoOcupacion;

@Repository
public interface TipoOcupacionRepository extends CatalogoRepository<TipoOcupacion, Integer> {
	 
}
