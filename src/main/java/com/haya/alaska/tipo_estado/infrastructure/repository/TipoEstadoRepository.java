package com.haya.alaska.tipo_estado.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoEstadoRepository extends CatalogoRepository<TipoEstado, Integer> {}
