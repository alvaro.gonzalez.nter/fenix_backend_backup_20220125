package com.haya.alaska.exception;

import com.haya.alaska.shared.dto.CustomErrorDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** @author diegotobalina created on 24/06/2020 */
@ControllerAdvice
@Slf4j
public class DefaultExceptionHandler {

  @ResponseBody
  @ExceptionHandler({Exception.class})
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  private CustomErrorDTO exception(
      HttpServletRequest request, HttpServletResponse response, Exception ex) {
    String queryString = request.getQueryString();
    String url = request.getRequestURL().toString();
    if (queryString != null) {
      url += '?' + queryString;
    }
    PowerException powerException = new PowerException(ex);
    log.error(request.getMethod() + " " + url);
    powerException.printStackTrace(); // for debugging
    return new CustomErrorDTO(powerException);
  }
}
