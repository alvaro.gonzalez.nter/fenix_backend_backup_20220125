package com.haya.alaska.informes.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NonNull;

@Getter
public class BombinDTO {
  private String nombreInterviniente;
  private String dniInterviniente;
  private String localidadInterviniente;
  private String empresa;
  private String cif;
  private String fechaEntrega;
  private Integer llaves;
  @NonNull
  private Integer idBien;
}
