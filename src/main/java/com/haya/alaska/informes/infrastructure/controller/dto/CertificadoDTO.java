package com.haya.alaska.informes.infrastructure.controller.dto;

import lombok.Getter;

@Getter
public class CertificadoDTO {
  private String deudas;
  private String situacion;
  private Integer numReferencia;
}
