package com.haya.alaska.informes.infrastructure.controller;

import com.haya.alaska.cartera.application.CarteraExcelUseCase;
import com.haya.alaska.cartera.infrastructure.controller.dto.InformeCarteraInputDTO;
import com.haya.alaska.contrato.application.ContratoExcelUseCase;
import com.haya.alaska.contrato.infrastructure.controller.dto.InformeContratoInputDTO;
import com.haya.alaska.estimacion.aplication.EstimacionExcelUseCase;
import com.haya.alaska.estimacion.infrastructure.controller.dto.InformeEstimacionDTO;
import com.haya.alaska.evento.application.EventoExcelUseCase;
import com.haya.alaska.evento.infrastructure.controller.dto.InformeEventoInputDTO;
import com.haya.alaska.expediente.application.ExpedienteExcelUseCase;
import com.haya.alaska.expediente.infrastructure.controller.dto.InformeExpedienteInputDTO;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaExcelUseCase;
import com.haya.alaska.formalizacion.aplication.FormalizacionUseCase;
import com.haya.alaska.informes.infrastructure.controller.dto.BombinDTO;
import com.haya.alaska.informes.infrastructure.controller.dto.FormalizacionDTO;
import com.haya.alaska.informes.infrastructure.controller.dto.VisitaDTO;
import com.haya.alaska.movimiento.application.MovimientoExcelUseCase;
import com.haya.alaska.movimiento.infrastructure.controller.dto.InformeMovimientoInputDTO;
import com.haya.alaska.propuesta.aplication.PropuestaExcelUseCase;
import com.haya.alaska.propuesta.infrastructure.controller.dto.InformePropuestaInputDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.visita.application.VisitaUseCase;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/informes")
public class InformeController {


  @Autowired
  private CarteraExcelUseCase carteraExcelUseCase;
  @Autowired
  private PropuestaExcelUseCase propuestaExcelUseCase;
  @Autowired
  private EstimacionExcelUseCase estimacionExcelUseCase;
  @Autowired
  private ContratoExcelUseCase contratoExcelUseCase;
  @Autowired
  MovimientoExcelUseCase movimientoExcelUseCase;
  @Autowired
  private EventoExcelUseCase eventoExcelUseCase;
  @Autowired
  private ExpedienteExcelUseCase expedienteExcelUseCase;
  @Autowired
  private ExpedientePosesionNegociadaExcelUseCase expedientePosesionNegociadaExcelUseCase;
  @Autowired
  private VisitaUseCase visitaUseCase;
  @Autowired
  private FormalizacionUseCase formalizacionUseCase;

  @ApiOperation(value = "Descargar Informe de cartera en Excel",
    notes = "Descargar información en Excel de la cartera indicada")
  @PutMapping("/cartera/excel")
  public ResponseEntity<InputStreamResource> getExcelCartera(@RequestParam Integer carteraId,
                                                             @RequestBody(required = false) @Valid InformeCarteraInputDTO informeCarteraInputDTO,
                                                             @RequestParam Boolean posesionNegociada,
                                                             @ApiIgnore CustomUserDetails usuario) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(carteraExcelUseCase.findExcelCarteraById(carteraId, informeCarteraInputDTO, (Usuario) usuario.getPrincipal(), posesionNegociada));

    return excelExport.download(excel, "informe-cartera" + "_" + (carteraId != null ? carteraId : 0) + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
  }


  @ApiOperation(value = "Descargar el Informe de propuestas en Excel",
    notes = "Descargar información en Excel de todas las propuestas")
  @PutMapping("/propuestas/excel")
  public ResponseEntity<InputStreamResource> getExcelPropuestas(@RequestParam Integer carteraId,
                                                                @RequestParam Integer mes,
                                                                @RequestParam Boolean posesionNegociada,
                                                                @RequestBody(required = false) @Valid InformePropuestaInputDTO informePropuestaInputDTO,
                                                                @ApiIgnore CustomUserDetails usuario) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(propuestaExcelUseCase.findExcelPropuestas(carteraId, mes, informePropuestaInputDTO, (Usuario) usuario.getPrincipal(), posesionNegociada));

    return excelExport.download(excel, "informe-propuestas-" + (carteraId != null ? carteraId : 0) + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
  }


  @ApiOperation(value = "Descargar informe de Estimaciones en Excel",
    notes = "Descargar información en Excel de las estimaciones con id cartera indicado")
  @PutMapping("/estimaciones/excel")
  public ResponseEntity<InputStreamResource> getExcelEstimacionesById(@RequestParam Integer carteraId,
                                                                      @RequestParam Integer mes,
                                                                      @RequestParam Boolean posesionNegociada,
                                                                      @RequestBody(required = false) @Valid InformeEstimacionDTO informeEstimacionDTO,
                                                                      @ApiIgnore CustomUserDetails usuario) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(estimacionExcelUseCase.findExcelEstimacionesById(carteraId, mes, informeEstimacionDTO, (Usuario) usuario.getPrincipal(), posesionNegociada));

    return excelExport.download(excel, "informe_estimaciones_" + (carteraId != null ? carteraId : 0) + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
  }


  @ApiOperation(value = "Descargar informe de Planes de Pago en Excel",
    notes = "Descargar información en Excel de los Planes de Pago con id cartera indicado")
  @PutMapping("/planes-pago/excel")
  public ResponseEntity<InputStreamResource> getExcelPlanesPagoById(@RequestParam Integer carteraId,
                                                                    @RequestParam Integer mes,
                                                                    @RequestBody(required = false) @Valid InformeContratoInputDTO informeContratoInputDTO,
                                                                    @ApiIgnore CustomUserDetails usuario) throws Exception {

    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(contratoExcelUseCase.findExcelPlanesPagoById(carteraId, mes, informeContratoInputDTO,
      (Usuario) usuario.getPrincipal()));

    return excelExport.download(excel, "informe_plan_de_pagos_" + (carteraId != null ? carteraId : 0) + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date()));

  }


  @ApiOperation(value = "Descargar Informe de Movimientos en Excel",
    notes = "Descargar información en Excel de los Movimientos con id cartera indicado")
  @PutMapping("/movimientos/excel")
  public ResponseEntity<InputStreamResource> getExcelMovimientosById(@RequestParam Integer carteraId,
                                                                     @RequestParam Integer mes,
                                                                     @RequestBody(required = false) @Valid InformeMovimientoInputDTO informeMovimientoInputDTO,
                                                                     @ApiIgnore CustomUserDetails usuario) throws Exception {

    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(movimientoExcelUseCase.findExcelMovimientosById(carteraId, mes, informeMovimientoInputDTO,
      (Usuario) usuario.getPrincipal()));

    return excelExport.download(excel, "informe_movimientos_" + (carteraId != null ? carteraId : 0) + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date()));

  }


  //REVISAR
  @ApiOperation(value = "Descargar Informe de Actividad en Excel",
    notes = "Descargar información en Excel de la actividad con cartera indicada")
  @PutMapping("/actividad/excel")
  public ResponseEntity<InputStreamResource> getExcelActividad(@RequestParam Integer carteraId,
                                                               @RequestParam Integer mes,
                                                               @RequestParam boolean posesionNegociada,
                                                               @RequestBody(required = false) @Valid InformeEventoInputDTO informeEventoInputDTO,
                                                               @ApiIgnore CustomUserDetails usuario) throws Exception {

    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(eventoExcelUseCase.findExcelActividadById(carteraId, mes, informeEventoInputDTO, (Usuario) usuario.getPrincipal(), posesionNegociada));
    return excelExport.download(excel, "informe_actividad_" + (carteraId != null ? carteraId : 0) + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date()));

  }


  @ApiOperation(value = "Descargar Informe Posesiones Negociadas en excel",
    notes = "Descargar información en Excel de la  Posesiones Negociadas con cartera indicada")
  @PutMapping("/posesiones-negociadas/excel")
  public ResponseEntity<InputStreamResource> getExcelPosesiones(@RequestParam Integer carteraId,
                                                                @RequestParam Integer mes,
                                                                @RequestBody(required = false) @Valid InformeExpedienteInputDTO informeExpedienteInputDTO,
                                                                @ApiIgnore CustomUserDetails usuario) throws NotFoundException, IOException, ParseException {

    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(expedientePosesionNegociadaExcelUseCase.findExcelPosesionesNegociadasById(carteraId, mes, informeExpedienteInputDTO,
      (Usuario) usuario.getPrincipal()));
    return excelExport.download(excel, "informe-PN_" + (carteraId != null ? carteraId : 0) + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
  }


  @ApiOperation(value = "Informe visitas inmueble", notes = "Genera el informe de la solicitud de visita inmuebles")
  @PostMapping("/visita-word")
  public ResponseEntity<InputStreamResource> informeSolicitudVisitaPlantillaWord(@RequestBody VisitaDTO visitaDTO,
                                                                                 @RequestParam boolean posesionNegociada) throws Exception {
    return visitaUseCase.informeSolicitudVisitaPlantillaWord(visitaDTO, posesionNegociada);

  }

  @ApiOperation(value = "Informe visitas inmueble", notes = "Genera el informe de la solicitud de visita inmuebles")
  @PostMapping("/visita-pdf")
  public ResponseEntity<byte[]> informeSolicitudVisitaPlantillaPdf(@RequestBody VisitaDTO visitaDTO,
                                                                   @RequestParam boolean posesionNegociada) throws Exception {
    return visitaUseCase.informeSolicitudVisitaPlantillaPdf(visitaDTO, posesionNegociada);

  }

  @ApiOperation(value = "Autorización Cambio Bombín")
  @PostMapping("/cambio-bombin-word")
  public ResponseEntity<InputStreamResource> autorizaciónCambioBombínWord(@RequestBody BombinDTO bombinDTO) throws Exception {
    return formalizacionUseCase.autorizaciónCambioBombínWord(bombinDTO);
  }

  @ApiOperation(value = "Autorización Cambio Bombín")
  @PostMapping("/cambio-bombin-pdf")
  public ResponseEntity<byte[]> autorizaciónCambioBombínPdf(@RequestBody BombinDTO bombinDTO) throws Exception {
    return formalizacionUseCase.autorizaciónCambioBombínPdf(bombinDTO);
  }


  @ApiOperation(value = "Comunicacion Formalizacion en word")
  @PostMapping("/comunicacion-formalizacion-word")
  public ResponseEntity<byte[]> comunicacionFormalizacionWord(@RequestParam(value = "files", required = false) List<MultipartFile> files,
                                                              @RequestPart(value = "dto") FormalizacionDTO formalizacionDTO,
                                                              @RequestParam boolean posesionNegociada) throws Exception {
    var doc = formalizacionUseCase.comunicacionFormalizacionWord(formalizacionDTO, posesionNegociada);
    var word = doc.getBody().getInputStream();

    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ZipOutputStream zos = new ZipOutputStream(bos);

    for (MultipartFile file : files) {
      var certificado = file.getInputStream();
      ZipEntry zip = new ZipEntry(file.getOriginalFilename());
      zos.putNextEntry(zip);
      int count;
      byte data[] = new byte[2048];
      BufferedInputStream entryStream = new BufferedInputStream(certificado);
      while ((count = entryStream.read(data, 0, 2048)) != -1) {
        zos.write( data, 0, count );
      }
      entryStream.close();
      zos.closeEntry();
    }


    //WORD
    zos.putNextEntry(new ZipEntry(doc.getHeaders().get("nombre").get(0)));
    int count;
    byte data[] = new byte[2048];
    BufferedInputStream entryStream = new BufferedInputStream(word, 2048);
    while ((count = entryStream.read(data, 0, 2048)) != -1) {
      zos.write( data, 0, count );
    }
    entryStream.close();
    zos.closeEntry();
    zos.close();
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
    return ResponseEntity
      .ok()
      .headers(headers)
      .body(bos.toByteArray());


  }

  @ApiOperation(value = "Comunicacion Formalizacion en pdf")
  @PostMapping("/comunicacion-formalizacion-pdf")
  public ResponseEntity<byte[]> comunicacionFormalizacionPDF(@RequestParam(value = "files", required = false) List<MultipartFile> files,
                                                             @RequestPart(value = "dto") FormalizacionDTO formalizacionDTO,
                                                             @RequestParam boolean posesionNegociada) throws Exception {

    var doc = formalizacionUseCase.comunicacionFormalizacionPDF(formalizacionDTO, posesionNegociada);
    var word = doc.getBody().getInputStream();

    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ZipOutputStream zos = new ZipOutputStream(bos);

    for (MultipartFile file : files) {
      var certificado = file.getInputStream();
      ZipEntry zip = new ZipEntry(file.getOriginalFilename());
      zos.putNextEntry(zip);
      int count;
      byte data[] = new byte[2048];
      BufferedInputStream entryStream = new BufferedInputStream(certificado);
      while ((count = entryStream.read(data, 0, 2048)) != -1) {
        zos.write( data, 0, count );
      }
      entryStream.close();
      zos.closeEntry();
    }


    //WORD
    zos.putNextEntry(new ZipEntry(doc.getHeaders().get("nombre").get(0)));
    int count;
    byte data[] = new byte[2048];
    BufferedInputStream entryStream = new BufferedInputStream(word, 2048);
    while ((count = entryStream.read(data, 0, 2048)) != -1) {
      zos.write( data, 0, count );
    }
    entryStream.close();
    zos.closeEntry();
    zos.close();


    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
    return ResponseEntity
      .ok()
      .headers(headers)
      .body(bos.toByteArray());


  }
}
