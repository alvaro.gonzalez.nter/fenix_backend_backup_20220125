package com.haya.alaska.informes.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NonNull;

@Getter
public class VisitaDTO {

  private String cliente;
  private String contacto;
  private Boolean alquiler;
  private String modalidad;
  private String fecha;
  private String hora;
  @NonNull
  private Integer idBien;
  @NonNull
  private Integer idPropuesta;
}
