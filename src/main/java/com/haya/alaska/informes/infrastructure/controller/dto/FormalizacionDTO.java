package com.haya.alaska.informes.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NonNull;

import java.util.List;

@Getter
public class FormalizacionDTO {
  private String cliente;
  private String contacto;
  private String comprador;
  private String oficina;
  private String gestorInclumplimiento;
  private String gestoria;
  private String necesitaRatificacion;
  private String llaves;
  private String observaciones;
  private String fechaVisita;

  @NonNull
  private Integer idBien;
  @NonNull
  private Integer idPropuesta;


  private List<CertificadoDTO> certificados;

}
