package com.haya.alaska.interviniente.application;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato.infrastructure.util.ContratoUtil;
import com.haya.alaska.contrato_interviniente.application.ContratoIntervinienteUseCase;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente_;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.domain.DatoContacto_;
import com.haya.alaska.dato_contacto.domain.DatosContactoExcel;
import com.haya.alaska.dato_contacto.infrastructure.DatoContactoUtil;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.dato_contacto.infrastructure.mapper.DatoContactoMapper;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionInputDTO;
import com.haya.alaska.direccion.infrastructure.mapper.DireccionMapper;
import com.haya.alaska.direccion.infrastructure.util.DireccionUtil;
import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDeudorDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.application.GestorDocumentalUseCase;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.domain.enums.EstadoDocumentoEnum;
import com.haya.alaska.integraciones.gd.domain.enums.TipoEntidadEnum;
import com.haya.alaska.integraciones.gd.infraestructure.mapper.GestorDocumentalMapper;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.integraciones.portal_deudor.application.PortalDeudorUseCase;
import com.haya.alaska.interviniente.domain.*;
import com.haya.alaska.interviniente.infrastructure.controller.dto.*;
import com.haya.alaska.interviniente.infrastructure.mapper.IntervinienteMapper;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.interviniente.infrastructure.util.IntervinienteUtil;
import com.haya.alaska.localidad.domain.DatosLocalizacionExcel;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoContenedorInputDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion_;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class IntervinienteUseCaseImpl implements IntervinienteUseCase {

  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired IntervinienteMapper intervinienteMapper;
  @Autowired ContratoRepository contratoRepository;
  @Autowired ContratoIntervinienteRepository contratoIntervinienteRepository;
  @Autowired IntervinienteUtil intervinienteUtil;
  @Autowired DatoContactoUtil datoContactoUtil;
  @Autowired DatoContactoMapper datoContactoMapper;
  @Autowired DireccionMapper direccionMapper;
  @Autowired DireccionUtil direccionUtil;
  @Autowired ContratoUtil contratoUtil;
  @Autowired ServicioGestorDocumental servicioGestorDocumental;
  @Autowired ContratoIntervinienteUseCase contratoIntervinienteUseCase;
  @Autowired TipoIntervencionRepository tipoIntervencionRepository;
  @Autowired private ExpedienteRepository expedienteRepository;
  @Autowired private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired private GestorDocumentalUseCase gestorDocumentalUseCase;
  @Autowired PortalDeudorUseCase portalDeudorUseCase;
  @Autowired UsuarioRepository usuarioRepository;
  @PersistenceContext private EntityManager entityManager;
  @Autowired ClienteRepository clienteRepository;

  @Override
  public IntervinienteDTO findIntervinienteById(Integer id, Integer idContrato, Integer idTipoIntervencion) throws Exception {

    Interviniente interviniente =
        intervinienteRepository
            .findById(id)
            .orElseThrow(() -> new NotFoundException("Interviniente", id));
    Contrato contrato =
        contratoRepository
            .findById(idContrato)
            .orElseThrow(() -> new NotFoundException("Contrato", idContrato));
    ContratoInterviniente contratoInterviniente =
      contratoIntervinienteRepository
        .findByContratoIdAndIntervinienteIdAndTipoIntervencionId(idContrato,id,idTipoIntervencion)
        .orElseThrow(() -> new NotFoundException("ContratoInterviniente", "Contrato", "idContrato", "Interviniente", id));

    List<Bien> bienes =
        contrato.getBienes().stream().map(cb -> cb.getBien()).collect(Collectors.toList());
    Map intervinienteHistorico =
        intervinienteRepository.findHistoricoById(interviniente.getId()).orElse(null);
    //var contratoInterviniente = interviniente.getContratoInterviniente(idContrato);
    if (contratoInterviniente == null)
      throw new NotFoundException("Contrato", idContrato);
    IntervinienteDTO intervinienteDTO =
        new IntervinienteDTO(interviniente, bienes, contratoInterviniente);
    intervinienteDTO.setOrdenIntervencion(
        this.getOrdenIntervencion(idContrato, interviniente.getId(), idTipoIntervencion));
    IntervinienteDTO intervinienteHistoricoDTO =
        intervinienteMapper.parseHistorico(intervinienteHistorico);
    /*Map contInt = contratoIntervinienteRepository.findHistoricoById(id, idContrato).orElse(null);
    if (contInt != null) {
      if (contInt.get("tipoIntervencion") != null) {
        TipoIntervencion ti =
            tipoIntervencionRepository
                .findById((Integer) contInt.get("tipoIntervencion"))
                .orElse(null);
        intervinienteDTO.setTipoIntervencion(ti != null ? new CatalogoMinInfoDTO(ti) : null);
      }
      // intervinienteDTO.setOrdenIntervencion((Integer) contInt.get("ordenIntervencion"));
    }*/
    intervinienteDTO.setHistorico(intervinienteHistoricoDTO);
    return intervinienteDTO;
  }

  @Override
  public IntervinienteDTO findIntervinienteByIdOrigen(String id) {

    Interviniente interviniente = intervinienteRepository.findByIdCarga(id).orElse(null);

    IntervinienteDTO intervinienteDTO = null;
    if (interviniente != null)
      intervinienteDTO = new IntervinienteDTO(interviniente, new ArrayList<>(), null);

    return intervinienteDTO;
  }

  @Override
  public ListWithCountDTO<IntervinienteDTOToList> getAllFilteredIntervinientes(
      Integer idContrato,
      Integer id,
      String nombre,
      String tipoIntervencion,
      String ordenIntervencion,
      Boolean estado,
      String fechaNacimiento,
      String docId,
      String telefono,
      Boolean contactado,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page)
      throws NotFoundException {

    List<Interviniente> intervinientesSinPaginar =
      filterIntervinientesInMemory(
        idContrato,
        id,
        nombre,
        tipoIntervencion,
        ordenIntervencion,
        estado,
        fechaNacimiento,
        docId,
        telefono,
        contactado,
        orderField,
        orderDirection);
    List<Interviniente> intervinientes =
      intervinientesSinPaginar.stream()
        .skip(size * page)
        .limit(size)
        .collect(Collectors.toList());

    List<IntervinienteDTOToList> intervinientesDTOs = new ArrayList<>();
    for (Interviniente interviniente : intervinientes) {
      boolean metido = false;
      List<ContratoInterviniente> lista = contratoIntervinienteRepository.findAllByContratoIdAndIntervinienteId(idContrato,interviniente.getId());
      for(var elemento: intervinientesDTOs) {
        if(interviniente.getId()==elemento.getId()) {
          metido=true;
        }
      }
      for(var contratointerviniente : lista) {
        if(metido == false) {
          intervinientesDTOs.add(
            intervinienteMapper.parseEntityToDTOList(interviniente, idContrato, contratointerviniente));
        }

      }
    }
    return new ListWithCountDTO<>(intervinientesDTOs, intervinientesSinPaginar.size());
  }

  @Override
  public Integer getOrdenIntervencion(Integer idContrato, Integer idInterviniente, Integer idTipoIntervencion)
    throws NotFoundException {
    var contratoInterviniente =
      contratoIntervinienteRepository.findByContratoIdAndIntervinienteIdAndTipoIntervencionId(
        idContrato, idInterviniente, idTipoIntervencion);
    //aqui
    if (contratoInterviniente.isEmpty()){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("Error, no intervention order found for contract with ID "
                + idContrato
                + " and participant with ID "
                + idInterviniente);
      else throw new NotFoundException(
              "Error, no se han encontrado orden de intervención para contrato con ID "
                      + idContrato
                      + " y interviniente con ID "
                      + idInterviniente);
    }
    return contratoInterviniente.get().getOrdenIntervencion();
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public IntervinienteDTO createInterviniente(
    Integer idContrato, IntervinienteInputDTO intervinienteDTO) throws Exception {

    // crear interviniente
    Interviniente intervinienteGenerado =
      intervinienteUtil.createInterviniente(intervinienteDTO, idContrato);

    // crear datos contacto
    List<DatosContactoDTOToList> datosContactoDto = intervinienteDTO.getDatosContacto();
    if (datosContactoDto != null && !datosContactoDto.isEmpty()) {
      datoContactoUtil.createDatosContactoToInterviniente(datosContactoDto, intervinienteGenerado);
    }

    List<DireccionInputDTO> direccionesDTO = intervinienteDTO.getDirecciones();
    if (direccionesDTO != null && !direccionesDTO.isEmpty()) {
      direccionUtil.createDireccionesToInterviniente(direccionesDTO, intervinienteGenerado);
    }

    // añadimos interviniente al contrato
    List<Integer> intervinientes = new ArrayList<>();
    intervinientes.add(intervinienteGenerado.getId());
    contratoUtil.addIntervinientesContrato(idContrato, intervinientes);

    Contrato contrato = contratoRepository.findById(idContrato).orElse(null);
    Collection<Bien> bienes =
      contrato.getBienes().stream().map(cb -> cb.getBien()).collect(Collectors.toList());
    var contratoInterviniente =
      contratoIntervinienteUseCase
        .findByIntervinienteIdAndContratoIdAndOrdenIntervencionAndTipoIntervencionId(
          intervinienteGenerado.getId(),
          idContrato,
          intervinienteDTO.getOrdenIntervencion(),
          intervinienteDTO.getTipoIntervencion());
    return new IntervinienteDTO(
      intervinienteRepository.findById(intervinienteGenerado.getId()).orElse(null),
      bienes,
      contratoInterviniente);
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public IntervinienteDTO updateIntervininte(
    Integer idInterviniente, IntervinienteInputDTO intervinienteDTO, Integer idContrato, Integer idTipoIntervencion)
    throws Exception {

    ContratoInterviniente contratoInterviniente =
      contratoIntervinienteRepository
        .findByContratoIdAndIntervinienteIdAndTipoIntervencionId(idContrato,idInterviniente,idTipoIntervencion)
        .orElseThrow(
          () ->
            new NotFoundException(
              "ContratoInterviniente", idInterviniente));

    Interviniente interviniente =
      intervinienteRepository
        .findById(contratoInterviniente.getInterviniente().getId())
        .orElseThrow(
          () ->
            new NotFoundException(
              "Interviniente", idInterviniente));
    interviniente = intervinienteUtil.actualizarCampos(interviniente, intervinienteDTO, idContrato, contratoInterviniente, idTipoIntervencion);

    Interviniente intervinienteActualizado = intervinienteRepository.saveAndFlush(interviniente);

    if (intervinienteDTO.getDatosContacto() != null
      && !intervinienteDTO.getDatosContacto().isEmpty()) {
      List<DatosContactoDTOToList> datosContactoDto = intervinienteDTO.getDatosContacto();
      List<DatoContacto> datosContactoSaved = new ArrayList<>();
      for (DatosContactoDTOToList datoContactoDto : datosContactoDto) {
        if (!datoContactoUtil.checkDatoContactoOrder(datoContactoDto, interviniente.getId())){
          Locale loc = LocaleContextHolder.getLocale();
          if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
            throw new RequiredValueException("There is already a contact data for the order:" + datoContactoDto.getOrden());
          else throw new RequiredValueException("Ya existe un dato contacto para el orden: " + datoContactoDto.getOrden());
        }
        // caso en el que se ha modificado un dato contacto existente
        if (datoContactoDto.getId() != null) {
          datoContactoUtil.updateFields(datoContactoMapper.parse(datoContactoDto), interviniente);
        } else {
          // caso en el que se ha añadido un nuevo dato contacto
          datoContactoUtil.createDatoContactoToInterviniente(datoContactoDto, interviniente);
        }
      }
    }

    if (intervinienteDTO.getDirecciones() != null && !intervinienteDTO.getDirecciones().isEmpty()) {
      List<DireccionInputDTO> direccionesDto = intervinienteDTO.getDirecciones();
      List<Direccion> direccionesSaved = new ArrayList<>();
      for (DireccionInputDTO direccionDto : direccionesDto) {
        // comprobamos que no haya una dirección para el interviniente marcada como principal
        if (!direccionUtil.checkDireccionAsPrincipal(direccionDto, interviniente.getId())){
          Locale loc = LocaleContextHolder.getLocale();
          if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
            throw new RequiredValueException("There is already an address marked as primary for the intervener with the id:" + interviniente.getId ());
          else throw new RequiredValueException("Ya existe una dirección marcada como principal para el interviniente con el id: " + interviniente.getId());
        }
        // caso en el que se ha modificado una direccion existente
        if (direccionDto.getId() != null) {
          Direccion dirInput = direccionMapper.parse(direccionDto);
          direccionUtil.updateFields(dirInput, direccionDto, interviniente);
        } else {
          // caso en el que se ha añadido una nueva direccion
          direccionUtil.createDireccionToInterviniente(direccionDto, interviniente);
        }
      }
    }

    Contrato contrato =
      contratoRepository
        .findById(idContrato)
        .orElseThrow(
          () ->
            new NotFoundException("contrato", idContrato));
    Collection<Bien> bienes =
      contrato.getBienes().stream().map(cb -> cb.getBien()).collect(Collectors.toList());
    var contratoIntervinienteNuevo =
      contratoIntervinienteRepository
        .findByContratoIdAndIntervinienteIdAndTipoIntervencionId(
          idContrato,
          idInterviniente,
          intervinienteDTO.getTipoIntervencion())
        .orElseThrow(() -> new NotFoundException("ContratoInterviniente", idInterviniente));
    return new IntervinienteDTO(
      intervinienteRepository
        .findById(intervinienteActualizado.getId())
        .orElseThrow(
          () ->
            new NotFoundException(
              "objeto", intervinienteActualizado.getId())),
      bienes,
      contratoIntervinienteNuevo);
  }

  @Override
  public void delete(Integer idInterviniente, Integer idContrato) throws Exception {
    Interviniente interviniente =
      intervinienteRepository
        .findById(idInterviniente)
        .orElseThrow(
          () ->
            new NotFoundException(
              "interviniente", idInterviniente));
    Set<ContratoInterviniente> contratoIntervinientes = interviniente.getContratos();
    for (ContratoInterviniente contratoInterviniente : contratoIntervinientes) {
      if (contratoInterviniente.getContrato().getId().equals(idContrato)) {
        interviniente.getContratos().remove(contratoInterviniente);
        contratoIntervinienteRepository.delete(contratoInterviniente);
      }
    }
  }

  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosIntervinientesByContrato(
    Integer idContrato,
    String idDocumento,
    String usuarioCreador,
    String tipo,
    String idEntidad,
    String fechaActualizacion,
    String nombreArchivo,
    String idHaya,
    String orderDirection,
    String orderField,
    Integer size,
    Integer page)
    throws NotFoundException, IOException {
    Contrato contrato =
      contratoRepository
        .findById(idContrato)
        .orElseThrow(() -> new NotFoundException("Contrato", idContrato));
    List<String> idsIntervinientes = new ArrayList<>();
    List<DocumentoDTO> listavacia = new ArrayList<>();
    for (ContratoInterviniente ci : contrato.getIntervinientes()) {
      Interviniente interviniente = ci.getInterviniente();
      String idHayaT = interviniente.getIdHaya();
      if (idHayaT != null) {
        idsIntervinientes.add(idHaya);
      }
    }
    if (idsIntervinientes.size() == 0) {
      if (contrato.getIntervinientes().size() != 0) {
        log.warn("Ningún interviniente del contrado " + idContrato + " tiene idHaya");
      }
      return new ListWithCountDTO<>(listavacia, listavacia.size());
    }

    List<DocumentoDTO> listadoIdGestor =
      this.servicioGestorDocumental
        .listarDocumentosMulti(CodigoEntidad.INTERVINIENTE, idsIntervinientes)
        .stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());
    List<DocumentoDTO> list =
      gestorDocumentalUseCase.listarDocumentosGestorIntervinientes(listadoIdGestor).stream()
        .collect(Collectors.toList());
    List<DocumentoDTO> listFinal =
      gestorDocumentalUseCase.filterOrderDocumentosDTO(
        list,
        idDocumento,
        usuarioCreador,
        tipo,
        idEntidad,
        fechaActualizacion,
        nombreArchivo,
        idHaya,
        orderDirection,
        orderField);
    List<DocumentoDTO> listaFiltradaPaginada = listFinal.stream()
      .skip(size * page)
      .limit(size)
      .collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
  }

  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosByExpedienteId(
    Integer idExpediente,
    String idDocumento,
    String usuarioCreador,
    String tipo,
    String idEntidad,
    String fechaActualizacion,
    String nombreArchivo,
    String idHaya,
    String orderDirection,
    String orderField,
    Integer size,
    Integer page)
    throws NotFoundException, IOException {
    Expediente expediente = expedienteRepository.findById(idExpediente).orElse(null);
    Set<Contrato> listadoContratos = expediente.getContratos();
    List<Interviniente> listaIntervinientes = new ArrayList<>();
    for (Contrato contrato : listadoContratos) {
      for (ContratoInterviniente ci : contrato.getIntervinientes()) {
        listaIntervinientes.add(ci.getInterviniente());
      }
    }
    List<String> listaActivosIntervinientes = new ArrayList<>();
    for (Interviniente interviniente : listaIntervinientes) {
      if (interviniente.getIdHaya() != null)
        listaActivosIntervinientes.add(interviniente.getIdHaya());
    }
    List<DocumentoDTO> listadoIdGestor =
      this.servicioGestorDocumental
        .listarDocumentosMulti(CodigoEntidad.INTERVINIENTE, listaActivosIntervinientes)
        .stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());

    //Busco los no firmados
    //Buscar los firmados me fijo en la base de datos

    //Listado de intervinientes del expediente

    List<GestorDocumental> documentosFirmados = gestorDocumentalRepository.findAllByEstadoAndIdHayaIn(EstadoDocumentoEnum.FIRMADO, listaActivosIntervinientes);
    List<DocumentoDTO> listaFirmadosDTO = documentosFirmados.stream().map(GestorDocumentalMapper.INSTANCE::toDocumentoDto).collect(Collectors.toList());
    List<DocumentoDTO> list =
      gestorDocumentalUseCase.listarDocumentosGestorIntervinientes(listadoIdGestor).stream()
        .collect(Collectors.toList());
    list.addAll(listaFirmadosDTO);
    List<DocumentoDTO> listFinal =
      gestorDocumentalUseCase.filterOrderDocumentosDTO(
        list,
        idDocumento,
        usuarioCreador,
        tipo,
        idEntidad,
        fechaActualizacion,
        nombreArchivo,
        idHaya,
        orderDirection,
        orderField);
    List<DocumentoDTO> listaFiltradaPaginada = listFinal.stream()
      .skip(size * page)
      .limit(size)
      .collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
  }

  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosByIntervinienteId(
    Integer idInterviniente,
    String idDocumento,
    String usuarioCreador,
    String tipo,
    String idEntidad,
    String fechaActualizacion,
    String nombreArchivo,
    String idHaya,
    String orderDirection,
    String orderField,
    Integer size,
    Integer page)
    throws NotFoundException, IOException {

    Interviniente interviniente =
      intervinienteRepository
        .findById(idInterviniente)
        .orElseThrow(
          () ->
            new NotFoundException("Interviniente", idInterviniente));
    String idHayaT = interviniente.getIdHaya();
    List<DocumentoDTO> listavacia = new ArrayList<>();
    if (idHayaT == null) {
      return new ListWithCountDTO<>(listavacia, listavacia.size());
    }

    List<DocumentoDTO> listadoIdGestor =
      this.servicioGestorDocumental.listarDocumentos(CodigoEntidad.INTERVINIENTE, idHaya).stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());

    List<DocumentoDTO> list =
      gestorDocumentalUseCase.listarDocumentosGestorIntervinientes(listadoIdGestor).stream()
        .collect(Collectors.toList());
    List<DocumentoDTO> listFinal =
      gestorDocumentalUseCase.filterOrderDocumentosDTO(
        list,
        idDocumento,
        usuarioCreador,
        tipo,
        idEntidad,
        fechaActualizacion,
        nombreArchivo,
        idHaya,
        orderDirection,
        orderField);
    List<DocumentoDTO> listaFiltradaPaginada = listFinal.stream()
      .skip(size * page)
      .limit(size)
      .collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
  }

  @Override
  public void createDocumentoInterviniente(
      Integer idInterviniente,
      Integer idExpediente,
      MultipartFile file,
      MetadatoDocumentoInputDTO metadatoDocumento,
      Usuario usuario,
      String tipoDocumento)
      throws NotFoundException, IOException {

    Interviniente interviniente =
        intervinienteRepository
            .findById(idInterviniente)
            .orElseThrow(
                () ->
                  new NotFoundException("Interviniente", idInterviniente));
    Expediente expediente =
            expedienteRepository
                    .findById(idExpediente)
                    .orElseThrow(
                            () ->
                                    new NotFoundException("Interviniente", idInterviniente));

    if (interviniente.getIdHaya() == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("This participant does not have an identifier in the Master");
      else throw new NotFoundException("Este interviniente no tiene un identificador en el Maestro");
    }
    JSONObject metadatos = new JSONObject();
    JSONObject archivoFisico = new JSONObject();
    archivoFisico.put("contenedor", "CONT");
    metadatos.put("General documento", metadatoDocumento.createDescripcionGeneralDocumento());
    metadatos.put("Archivo físico", archivoFisico);

    long id =
        servicioGestorDocumental
            .crearDocumento(CodigoEntidad.INTERVINIENTE, interviniente.getIdHaya(), file, metadatos)
            .getIdDocumento();
    Integer idgestorDocumental = (int) id;
    GestorDocumental gestorDocumental =
        new GestorDocumental(
            idgestorDocumental,
            usuario,
            null,
            new Date(),
            new Date(),
            metadatoDocumento.getNombreFichero(),
            MatriculasEnum.obtenerPorMatricula(metadatoDocumento.getCatalogoDocumental()),
            null,
            idInterviniente.toString(),
            TipoEntidadEnum.INTERVINIENTE,
            interviniente.getIdHaya(),
            expediente.getCartera().getCliente());
    gestorDocumentalRepository.save(gestorDocumental);
  }

  @Override
  public ListWithCountDTO<IntervinienteVinculacionDTO> findVinculadosIntervinienteById(Integer id)
    throws Exception {
    Interviniente interviniente =
      intervinienteRepository
        .findById(id)
        .orElseThrow(() -> new NotFoundException("objeto", id));
    Set<ContratoInterviniente> contratos = interviniente.getContratos();
    List<IntervinienteVinculacionDTO> intervinienteVinculacionDTOS = new ArrayList<>();
    contratos.forEach(
      contratoInterviniente ->
        intervinienteVinculacionDTOS.add(
          new IntervinienteVinculacionDTO(
            contratoInterviniente.getContrato().getExpediente().getId(),
            contratoInterviniente.getContrato().getExpediente().getIdConcatenado(),
            new UsuarioDTO(
              contratoInterviniente
                .getContrato()
                .getExpediente()
                .getCartera()
                .getResponsableCartera()),
            contratoInterviniente.getContrato().getIdCarga(),
            contratoInterviniente.getTipoIntervencion().getValor(),
            contratoInterviniente.getOrdenIntervencion())));
    return new ListWithCountDTO<>(
      intervinienteVinculacionDTOS, intervinienteVinculacionDTOS.size());
  }

  @Override
  public ListWithCountDTO<IntervinienteDTOToList> getAll(
    Integer size, Integer page, List<Integer> contratos) {
    int count;
    List<IntervinienteDTOToList> intervinientesDTOS = new ArrayList<>();
    if (contratos != null && !contratos.isEmpty()) {
      count = contratoIntervinienteRepository.countDistinctByContratoIdIn(contratos);

      List<Interviniente> intervinientes = obtainIntervinientes(size, page, contratos);

      for (var contrato : contratos) {
        for (Interviniente interviniente : intervinientes) {
          if (interviniente.getContratoInterviniente(contrato) != null) {
            intervinientesDTOS.add(new IntervinienteDTOToList(interviniente, contrato));
          }
        }
      }
    } else {
      count = (int) intervinienteRepository.count();
    }

    return new ListWithCountDTO<>(intervinientesDTOS, count);
  }

  @Override
  public ListWithCountDTO<IntervinienteDTOToList> getAllFiltered(
    Integer size, Integer page, List<Integer> contratos) throws NotFoundException {
    List<IntervinienteDTOToList> intervinientesDTOS = new ArrayList<>();
    if (contratos != null && !contratos.isEmpty()) {
      for (Integer contrato : contratos) {
        var lista = getAllFilteredIntervinientes(
          contrato, null, null, null, null, null, null, null, null, null, null, null, size, page).getList();
        for (var interviniente : lista) {
          boolean contiene = false;
          for (var intervinienteLista : intervinientesDTOS) {
            if (intervinienteLista.getId().equals(interviniente.getId()))
              contiene = true;
          }
          if (!contiene)
            intervinientesDTOS.add(interviniente);
        }
      }
    }
      return new ListWithCountDTO<>(intervinientesDTOS, intervinientesDTOS.size());
  }

  public List<Interviniente> obtainIntervinientes(
    Integer size, Integer page, List<Integer> contratos) {
    Page<ContratoInterviniente> paged;
    Pageable pageable = PageRequest.of(page, size);
    if (contratos != null && !contratos.isEmpty()) {
      paged = contratoIntervinienteRepository.findAllDistinctByContratoIdIn(pageable, contratos);
    } else {
      paged = contratoIntervinienteRepository.findAllBy(pageable);
    }
    List<Interviniente> intervinientes =
      paged.getContent().stream()
        .map(ContratoInterviniente::getInterviniente)
        .distinct()
        .filter(Interviniente::getActivo)
        .collect(Collectors.toList());
    return intervinientes;
  }

  public LinkedHashMap<String, List<Collection<?>>> findExcelIntervinienteByIdContrato(
    Integer idContrato) throws Exception {
    Contrato contrato =
      contratoRepository
        .findById(idContrato)
        .orElseThrow(
          () -> new NotFoundException("objeto", idContrato));

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> intervinientePersonaFisica = new ArrayList<>();
    intervinientePersonaFisica.add(IntervinientePersonaFisicaExcel.cabeceras);
    List<Collection<?>> intervinientePersonaJuridica = new ArrayList<>();
    intervinientePersonaJuridica.add(IntervinientePersonaJuridicaExcel.cabeceras);
    List<Collection<?>> datosLocalizacion = new ArrayList<>();
    datosLocalizacion.add(DatosLocalizacionExcel.cabeceras);
    List<Collection<?>> datosContacto = new ArrayList<>();
    datosContacto.add(DatosContactoExcel.cabeceras);
    List<Collection<?>> intervinientes = new ArrayList<>();
    intervinientes.add(IntervinienteExcel.cabeceras);

    for (ContratoInterviniente contratoInterviniente : contrato.getIntervinientes()) {
      intervinientes.add(
        new IntervinienteExcel(
          contratoInterviniente.getInterviniente(),
          contrato.getIdCarga(),
          contratoInterviniente.getOrdenIntervencion(),
          contratoInterviniente.getTipoIntervencion(),
          contrato.getExpediente().getIdConcatenado())
          .getValuesList());

      for (Direccion direccion : contratoInterviniente.getInterviniente().getDirecciones()) {
        datosLocalizacion.add(
          new DatosLocalizacionExcel(
            direccion, contrato.getIdCarga(), contrato.getExpediente().getIdConcatenado())
            .getValuesList());
      }
      for (DatoContacto datoContacto :
        contratoInterviniente.getInterviniente().getDatosContacto()) {
        datosContacto.add(
          new DatosContactoExcel(
            datoContacto,
            contrato.getIdCarga(),
            contrato.getExpediente().getIdConcatenado())
            .getValuesList());
      }
    }



    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      // datos.put("IntervinientesPersonasFisicas", intervinientePersonaFisica);
      // datos.put("IntervinientesPersonasJuridicaes", intervinientePersonaJuridica);
      datos.put("Borrowers", intervinientes);
      datos.put("Data Locations ", datosLocalizacion);
      datos.put("Contact Data", datosContacto);

    } else {

      // datos.put("IntervinientesPersonasFisicas", intervinientePersonaFisica);
      // datos.put("IntervinientesPersonasJuridicaes", intervinientePersonaJuridica);
      datos.put("Intervinientes", intervinientes);
      datos.put("DatosLocalizaciones", datosLocalizacion);
      datos.put("DatosContactos", datosContacto);
    }

    return datos;
  }

  /**
   * Filtro intervinientes en memoria, para saber el total hacer el .size de lo que devuelve Para
   * paginar los resultados hacer .subList(page*size,(page*size)+size) del resultado Una vez
   * devueltos ordenar antes de paginar los resultados
   *
   * @throws NotFoundException
   */
  private List<Interviniente> filterIntervinientesInMemory(
    Integer idContrato,
    Integer id,
    String nombre,
    String tipoIntervencion,
    String ordenIntervencion,
    Boolean estado,
    String fechaNacimiento,
    String docId,
    String telefono,
    Boolean contactado,
    String orderField,
    String orderDirection)
    throws NotFoundException {

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Interviniente> query = cb.createQuery(Interviniente.class);

    Root<Interviniente> root = query.from(Interviniente.class);
    List<Predicate> predicates = new ArrayList<>();

    Join<Interviniente, ContratoInterviniente> destinatarioJoin =
      root.join(Interviniente_.CONTRATOS, JoinType.INNER);
    Predicate onDestinatario =
      cb.equal(
        destinatarioJoin.get(ContratoInterviniente_.CONTRATO).get(Contrato_.ID), idContrato);
    destinatarioJoin.on(onDestinatario);

    if (id != null)
      predicates.add(cb.like(root.get(Interviniente_.ID).as(String.class), "%" + id + "%"));
    if (nombre != null)
      predicates.add(cb.like(root.get(Interviniente_.NOMBRE), "%" + nombre + "%"));
    if (tipoIntervencion != null || ordenIntervencion != null) {
      Join<Interviniente, ContratoInterviniente> join =
        root.join(Interviniente_.contratos, JoinType.INNER);
      predicates.add(
        cb.equal(join.get(ContratoInterviniente_.CONTRATO).get(Contrato_.ID), idContrato));
      if (tipoIntervencion != null) {
        Predicate pInt =
          cb.like(
            join.get(ContratoInterviniente_.tipoIntervencion).get(TipoIntervencion_.VALOR),
            "%" + tipoIntervencion + "%");
        join.on(pInt);
      }
      if (ordenIntervencion != null) {
        Predicate pOrd =
          cb.like(
            join.get(ContratoInterviniente_.ordenIntervencion).as(String.class),
            "%" + ordenIntervencion + "%");
        join.on(pOrd);
      }
    }
    if (estado != null) predicates.add(cb.equal(root.get(Interviniente_.activo), estado));
    if (fechaNacimiento != null)
      predicates.add(
        cb.like(
          root.get(Interviniente_.fechaNacimiento).as(String.class),
          "%" + fechaNacimiento + "%"));
    if (docId != null)
      predicates.add(cb.like(root.get(Interviniente_.numeroDocumento), "%" + docId + "%"));
    if (telefono != null) {
      Join<Interviniente, DatoContacto> join =
        root.join(Interviniente_.DATOS_CONTACTO, JoinType.INNER);
      Predicate onCond = cb.like(join.get(DatoContacto_.MOVIL), "%" + telefono + "%");
      join.on(onCond);
    }
    if (contactado != null)
      predicates.add(cb.equal(root.get(Interviniente_.contactado), contactado));

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
    if (orderField != null) {
      if (orderField.equals("tipoIntervencion") || orderField.equals("ordenIntervencion")) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(destinatarioJoin.get(orderField)));
        } else {
          rs.orderBy(cb.asc(destinatarioJoin.get(orderField)));
        }
      } else if (orderField.equals("telefono")) {
        orderField = "movil";
        Join<Interviniente, DatoContacto> joinTelef =
          root.join(Interviniente_.DATOS_CONTACTO, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(joinTelef.get(orderField)));
        } else {
          rs.orderBy(cb.asc(joinTelef.get(orderField)));
        }
      } else {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(orderField)));
        } else {
          rs.orderBy(cb.asc(root.get(orderField)));
        }
      }
    }
    return entityManager.createQuery(query).getResultList();
  }

  @Override
  public List<IntervinienteDTOToList> getAllByContratosIds(List<Integer> contratosIds) {
    List<Interviniente> intervinientes =
      intervinienteRepository.findAllByContratosIdIn(contratosIds);
    List<IntervinienteDTOToList> result = new ArrayList<>();
    for (var contrato : contratosIds) {
      var intervinientesContrato = IntervinienteDTOToList.newList(intervinientes, contrato);
      result.addAll(intervinientesContrato);
    }
    return result;
  }

  @Override
  public List<IntervinienteDTOToList> getAllByExpedienteId(Integer idExpediente) throws Exception {
    List<Interviniente> intervinientes =
      intervinienteRepository.findAllByExpedienteId(idExpediente);
    return IntervinienteDTOToList.newListExpediente(intervinientes, idExpediente);
  }

  @Override
  public List<IntervinienteOcupanteDatosContactoDTO> getAllIntervinienteDatosContacto(
    String tipo, Integer idContrato) {

    List<Interviniente> listIntervinientes = new ArrayList<>();

    switch (tipo) {
      case "sms":
        listIntervinientes = intervinienteRepository.findAllIntervinientesWithMovil(idContrato);
        break;
      case "email":
        listIntervinientes = intervinienteRepository.findAllIntervinientesWithEmail(idContrato);
        break;
      default:
        listIntervinientes = intervinienteRepository.findAllIntervinientesByContratoId(idContrato);
        break;
    }

    List<IntervinienteOcupanteDatosContactoDTO> listIntervinienteDatosContacto =
      new ArrayList<IntervinienteOcupanteDatosContactoDTO>();

    for (Interviniente interviniente : listIntervinientes) {

      IntervinienteOcupanteDatosContactoDTO intervinienteDTO =
        new IntervinienteOcupanteDatosContactoDTO();
      BeanUtils.copyProperties(interviniente, intervinienteDTO);

      // Recogemos el id del Interviniente
      Integer idInterviniente = interviniente.getId();

      // Recorremos el listado de datos de contacto para asociar los datos con el interviniente
      List<DatoContacto> listDatoContacto =
        interviniente.getDatosContacto().stream()
          .collect(Collectors.toCollection(ArrayList::new));

      if (listDatoContacto != null && listDatoContacto.size() > 0) {
        for (DatoContacto datoContacto : listDatoContacto) {
          if (datoContacto.getInterviniente().getId() == idInterviniente) {
            /*     intervinienteUtil.parteDatosContactoToIntervinienteDatosContactoDTO(
                intervinienteDTO, datoContacto);*/
          }
        }
      }

      // Si el tipo es carta, además deberemos añadir los datos de dirección al interviniente
      if ("carta".equals(tipo)) {
        List<Direccion> listDireccion =
          interviniente.getDirecciones().stream()
            .collect(Collectors.toCollection(ArrayList::new));
        if (listDireccion != null && listDireccion.size() > 0) {
          for (Direccion direccion : listDireccion) {
            if (direccion.getInterviniente().getId() == idInterviniente) {
              intervinienteUtil.parseDireccionToString(intervinienteDTO, direccion);
            }
          }
        }
      }

      listIntervinienteDatosContacto.add(intervinienteDTO);
    }

    return listIntervinienteDatosContacto;
  }

  @Override
  public List<IntervinienteOcupanteDatosContactoDTO> getAllIntervinienteContacto(Integer IdExpediente) throws NotFoundException {

    Expediente expediente =
      expedienteRepository
        .findById(IdExpediente)
        .orElseThrow(
          () -> new NotFoundException("expediente", IdExpediente));
    List<Contrato> contratoList = expediente.getContratos().stream().collect(Collectors.toList());
    List<Interviniente> intervinienteList = new ArrayList<>();
    List<IntervinienteOcupanteDatosContactoDTO> intervinienteOcupanteDatosContactoDTOList =
      new ArrayList<>();
    for (Contrato contrato : contratoList) {
      List<ContratoInterviniente> contratoIntervinienteList =
        contrato.getIntervinientes().stream().collect(Collectors.toList());
      for (ContratoInterviniente contratoInterviniente : contratoIntervinienteList) {
        Interviniente interviniente = contratoInterviniente.getInterviniente();
        intervinienteList.add(interviniente);
      }
    }
    intervinienteList=intervinienteList.stream().distinct().collect(Collectors.toList());
    for (Interviniente interviniente : intervinienteList) {
     /* IntervinienteOcupanteDatosContactoDTO intervinienteOcupanteDatosContactoDTO =
          new IntervinienteOcupanteDatosContactoDTO();*/

      List<DatoContacto> datoContactoList =
        interviniente.getDatosContacto().stream().collect(Collectors.toList());

      List<String> movilList = new ArrayList<>();
      List<String> emailList = new ArrayList<>();
      List<String> fijoList = new ArrayList<>();

      if (!datoContactoList.isEmpty()) {
        IntervinienteOcupanteDatosContactoDTO intervinienteOcupanteDatosContactoDTO =
          new IntervinienteOcupanteDatosContactoDTO();
        for (DatoContacto datoContacto : datoContactoList) {
          /*IntervinienteOcupanteDatosContactoDTO intervinienteOcupanteDatosContactoDTO =
          new IntervinienteOcupanteDatosContactoDTO();*/
          if (datoContacto.getMovil() != null && !datoContacto.getMovil().equals("")) {
            movilList.add(datoContacto.getMovil());
          }
          if (datoContacto.getEmail() != null && !datoContacto.getEmail().equals("")) {
            emailList.add(datoContacto.getEmail());
          }
          if (datoContacto.getFijo() != null && !datoContacto.getFijo().equals("")) {
            fijoList.add(datoContacto.getFijo());
          }
          intervinienteOcupanteDatosContactoDTO.setMovilList(movilList);
          intervinienteOcupanteDatosContactoDTO.setEmailList(emailList);
          intervinienteOcupanteDatosContactoDTO.setTelefonoList(fijoList);

          intervinienteOcupanteDatosContactoDTO.setId(interviniente.getId());
          intervinienteOcupanteDatosContactoDTO.setNombre(
            interviniente.getNombre() != null ? interviniente.getNombre() : null);
          intervinienteOcupanteDatosContactoDTO.setApellidos(
            interviniente.getApellidos() != null ? interviniente.getApellidos() : null);
          intervinienteOcupanteDatosContactoDTO.setNumeroDocumento(
            interviniente.getNumeroDocumento() != null
              ? interviniente.getNumeroDocumento()
              : null);
          intervinienteOcupanteDatosContactoDTO.setMovilvalidado(
            Boolean.TRUE.equals(datoContacto.getMovilValidado()) ? true : false);
          intervinienteOcupanteDatosContactoDTO.setEmailvalidado(
            Boolean.TRUE.equals(datoContacto.getEmailValidado()) ? true : false);
          intervinienteOcupanteDatosContactoDTO.setTelefonovalidado(
            Boolean.TRUE.equals(datoContacto.getFijoValidado()) ? true : false);
        }
        intervinienteOcupanteDatosContactoDTOList.add(intervinienteOcupanteDatosContactoDTO);
      }
    }
    return intervinienteOcupanteDatosContactoDTOList;
  }
//Informacion que viene desde portal deudor a fenix
  @Override
  public ListWithCountDTO<DocumentoDeudorDTO> findDocumentosDeudorByIntervinienteId(BusquedaRepositorioDTO busquedaRepositorioDTO,String idDocumento, String nombreDocumento, String tipoDocumento,String fechaAlta, String usuarioCreador, Integer idCliente, String orderDirection, String orderField, Integer size, Integer page) throws NotFoundException,IOException{


    List<DocumentoDeudorDTO> listavacia = new ArrayList<>();
    //Si hago la peticion al gestor y no al fenix (el usuarioCreador, idCliente)
    Cliente cliente =
      clienteRepository
        .findById(idCliente)
        .orElseThrow(
          () ->
            new NotFoundException("Cartera", "idCliente", idCliente));
    String nombreCliente=cliente.getNombre().replaceAll(" ","");
try{
    List<DocumentoDeudorDTO> listadoIdGestor =
      this.servicioGestorDocumental
        .listarDocumentos(CodigoEntidad.DEUDOR, nombreCliente)
        .stream()
        .map(DocumentoDeudorDTO::new)
        .collect(Collectors.toList());

    //List<DocumentoDeudorDTO>documentoDeudorDTOList=documentoMapper.parseGestorToDocumento(gestorDocumentalList);

    List<DocumentoDeudorDTO>list=gestorDocumentalUseCase.listarDocumentoDeudorClienteDTO(listadoIdGestor,idCliente).stream().collect(Collectors.toList());
    List<DocumentoDeudorDTO> listbusqueda =
      gestorDocumentalUseCase.busquedaDocumentoDeudorRepositorio(busquedaRepositorioDTO, list);
    List<DocumentoDeudorDTO>listFinal=gestorDocumentalUseCase.filterDocumentoDeudor(listbusqueda,idDocumento,nombreDocumento,tipoDocumento,fechaAlta,usuarioCreador,orderDirection,orderField);
    List<DocumentoDeudorDTO> listaFiltradaPaginada = listFinal.stream()
        .skip(size * page)
        .limit(size)
        .collect(Collectors.toList());

    return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
    }catch (Exception ex){
    return new ListWithCountDTO<>(new ArrayList<>(),0);
    }
  }

  //Aquí que llegue el gestor del deudor y aquí generar el aviso de que se ha subido información
  //Pasar el idGestor desde portal Deudor
  @Override
  public void createDocumentoDeudorInterviniente(
    Integer idInterviniente,
    MultipartFile file,
    Integer idGestor,
    String comentario,
    String titulo,
    Integer idCliente)
    throws Exception {
    //Pasamos el idGestor desde
    Usuario usuario =
      usuarioRepository
        .findById(idGestor)
        .orElseThrow(
          () ->
            new NotFoundException("Usuario", "idGestor", idGestor));
    Cliente cliente =
      clienteRepository
        .findById(idCliente)
        .orElseThrow(
          () ->
            new NotFoundException("Cartera", "idCliente", idCliente));


    Interviniente interviniente =
      intervinienteRepository
        .findById(idInterviniente)
        .orElseThrow(
          () ->
            new NotFoundException("Interviniente", idInterviniente));
    if (interviniente.getIdHaya() == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("This participant does not have an identifier in the Master");
      else throw new NotFoundException("Este interviniente no tiene un identificador en el Maestro");
    }
    String nombreCliente=cliente.getNombre().replaceAll(" ","");
    String nombreFichero = file.getOriginalFilename();
    MetadatoDocumentoInputDTO metadatoDocumento = new MetadatoDocumentoInputDTO(nombreFichero, "HA-03-OTRO-20");
    try {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO = new MetadatoContenedorInputDTO(nombreCliente,"");
      this.servicioGestorDocumental.procesarContenedorCartera(CodigoEntidad.DEUDOR, metadatoContenedorInputDTO);
      long id = servicioGestorDocumental.procesarDocumento(CodigoEntidad.DEUDOR,nombreCliente, file, metadatoDocumento).getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental = new GestorDocumental(idgestorDocumental,usuario,comentario,titulo,interviniente,cliente,nombreFichero,new Date());
      gestorDocumentalRepository.save(gestorDocumental);
      portalDeudorUseCase.eventoRevisionDeudor(interviniente.getId(),usuario.getId(),null,comentario);
    } catch (Exception e) {
      long id = servicioGestorDocumental.procesarDocumento(CodigoEntidad.DEUDOR,nombreCliente, file, metadatoDocumento).getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental = new GestorDocumental(idgestorDocumental,usuario,comentario,titulo,interviniente,cliente,nombreFichero,new Date());
      gestorDocumentalRepository.save(gestorDocumental);
      portalDeudorUseCase.eventoRevisionDeudor(interviniente.getId(),usuario.getId(),null,comentario);

    }
  }
}
