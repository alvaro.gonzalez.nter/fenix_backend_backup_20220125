package com.haya.alaska.interviniente.application;

import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDeudorDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.*;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface IntervinienteUseCase {

  ListWithCountDTO<IntervinienteVinculacionDTO> findVinculadosIntervinienteById(Integer id) throws Exception;

  ListWithCountDTO<IntervinienteDTOToList> getAll(Integer size, Integer page, List<Integer> contratos);

  ListWithCountDTO<IntervinienteDTOToList> getAllFiltered(Integer size, Integer page, List<Integer> contratos) throws NotFoundException;

  IntervinienteDTO findIntervinienteById(Integer id, Integer idContrato, Integer idTipoIntervencion) throws Exception;
  IntervinienteDTO findIntervinienteByIdOrigen(String id);

  ListWithCountDTO<IntervinienteDTOToList> getAllFilteredIntervinientes(
      Integer idContrato,
      Integer id,
      String nombre,
      String tipoIntervencion,
      String ordenIntervencion,
      Boolean estado,
      String fechaNacimiento,
      String docId,
      String telefono,
      Boolean contactado,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page)
      throws NotFoundException;

  Integer getOrdenIntervencion(Integer idContrato, Integer idInterviniente, Integer idTipoIntervencion) throws NotFoundException;

  IntervinienteDTO createInterviniente(Integer idContrato, IntervinienteInputDTO intervinienteDTO) throws Exception;

  IntervinienteDTO updateIntervininte(Integer idInterviniente, IntervinienteInputDTO intervinienteDTO, Integer idContrato, Integer idTipoIntervencion) throws Exception;

  void delete(Integer idInterviniente, Integer idContrato) throws Exception;

  ListWithCountDTO<DocumentoDTO> findDocumentosIntervinientesByContrato(Integer idContrato,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size, Integer page) throws NotFoundException, IOException;

  ListWithCountDTO<DocumentoDTO> findDocumentosByExpedienteId(Integer idExpediente,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size, Integer page) throws NotFoundException, IOException;

  ListWithCountDTO<DocumentoDTO> findDocumentosByIntervinienteId(Integer idInterviniente,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size, Integer page) throws NotFoundException,IOException;

  void createDocumentoInterviniente(Integer idInterviniente, Integer idExpediente, MultipartFile file, MetadatoDocumentoInputDTO metadatoDocumento, Usuario usuario,String tipoDocumento) throws NotFoundException, IOException;

  void createDocumentoDeudorInterviniente(Integer idInterviniente, MultipartFile file, Integer idGestori,String comentario,String titulo,Integer idCliente) throws Exception;

  LinkedHashMap<String, List<Collection<?>>> findExcelIntervinienteByIdContrato(Integer idContrato) throws Exception;

  List<IntervinienteDTOToList> getAllByContratosIds(List<Integer> contratosIds);

  List<IntervinienteDTOToList>getAllByExpedienteId(Integer idExpediente)throws Exception;

  List<IntervinienteOcupanteDatosContactoDTO> getAllIntervinienteDatosContacto(String tipo, Integer IdExpediente);


  List<IntervinienteOcupanteDatosContactoDTO> getAllIntervinienteContacto(Integer IdExpediente) throws NotFoundException;

  ListWithCountDTO<DocumentoDeudorDTO> findDocumentosDeudorByIntervinienteId(BusquedaRepositorioDTO busquedaRepositorioDTO,String idDocumento, String nombreDocumento, String tipoDocumento,String fechaAlta,String usuarioCreador, Integer idCliente, String orderDirection, String orderField, Integer size, Integer page) throws NotFoundException,IOException;

}
