package com.haya.alaska.interviniente.infrastructure.controller.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.dato_contacto.infrastructure.mapper.DatoContactoMapper;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionDTO;
import com.haya.alaska.direccion.infrastructure.mapper.DireccionMapper;
import com.haya.alaska.interviniente.domain.Interviniente;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class IntervinienteDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private Integer id;
  private CatalogoMinInfoDTO tipoIntervencion;
  private CatalogoMinInfoDTO tipoDocumento;
  private String numeroDocumento;
  private Boolean activo;
  private Boolean personaJuridica;
  private Integer ordenIntervencion;
  private String nombre;
  private String apellidos;
  private CatalogoMinInfoDTO sexo;
  private CatalogoMinInfoDTO estadoCivil;
  private CatalogoMinInfoDTO residencia;
  private CatalogoMinInfoDTO nacionalidad;
  private CatalogoMinInfoDTO paisNacimiento;
  private Date fechaNacimiento;
  private String razonSocial;
  private Date fechaConstitucion;
  private Boolean contactado;
  private List<DatosContactoDTOToList> datosContacto;
  private List<DireccionDTO> direcciones;
  private CatalogoMinInfoDTO tipoOcupacion;
  private String empresa;
  private String telefonoEmpresa;
  private String contactoEmpresa;
  private CatalogoMinInfoDTO tipoContrato;
  private String codigoSocioProfesional;
  private String cnae;
  private Double ingresosNetosMensuales;
  private String descripcionActividad;
  private Integer numHijosDependientes;
  private Boolean otrosRiesgos;
  private Boolean vulnerabilidad;
  private Boolean pensionista;
  private CatalogoMinInfoDTO tipoPrestacion;
  private Boolean dependiente;
  private Boolean discapacitados;
  private CatalogoMinInfoDTO otrasSituacionesVulnerabilidad;
  private String notas1;
  private String notas2;
  private IntervinienteDTO historico;
  private String origen;
  private String porcentajeIntervencion;

  public IntervinienteDTO(Interviniente interviniente, Collection<Bien> bienes, ContratoInterviniente contratoInterviniente) {

    BeanUtils.copyProperties(interviniente, this);
    this.origen = interviniente.getIdCarga();

    this.estadoCivil = interviniente.getEstadoCivil() != null ? new CatalogoMinInfoDTO(interviniente.getEstadoCivil()) : null;
    this.nacionalidad =
        interviniente.getNacionalidad() != null
            ? new CatalogoMinInfoDTO(interviniente.getNacionalidad())
            : null;
    this.paisNacimiento =
        interviniente.getPaisNacimiento() != null
            ? new CatalogoMinInfoDTO(interviniente.getPaisNacimiento())
            : null;
    this.residencia =
        interviniente.getResidencia() != null
            ? new CatalogoMinInfoDTO(interviniente.getResidencia())
            : null;
    this.otrasSituacionesVulnerabilidad =
        interviniente.getOtrasSituacionesVulnerabilidad() != null
            ? new CatalogoMinInfoDTO(interviniente.getOtrasSituacionesVulnerabilidad())
            : null;
    this.sexo = interviniente.getSexo() != null ? new CatalogoMinInfoDTO(interviniente.getSexo()) : null;
    this.tipoContrato =
        interviniente.getTipoContrato() != null
            ? new CatalogoMinInfoDTO(interviniente.getTipoContrato())
            : null;
    this.tipoDocumento =
        interviniente.getTipoDocumento() != null
            ? new CatalogoMinInfoDTO(interviniente.getTipoDocumento())
            : null;
    if (contratoInterviniente != null){
      this.tipoIntervencion = new CatalogoMinInfoDTO(contratoInterviniente.getTipoIntervencion());
      this.ordenIntervencion = contratoInterviniente.getOrdenIntervencion();
    }
    this.tipoOcupacion =
        interviniente.getTipoOcupacion() != null
            ? new CatalogoMinInfoDTO(interviniente.getTipoOcupacion())
            : null;

    this.tipoPrestacion =
        interviniente.getTipoPrestacion() != null
            ? new CatalogoMinInfoDTO(interviniente.getTipoPrestacion())
            : null;

    this.datosContacto =
        interviniente
            .getDatosContacto()
            .stream()
            .map(DatoContactoMapper::parseEntityToListDTO)
            .collect(Collectors.toList());

    for (Direccion d : interviniente.getDirecciones()) {
      for (Bien b : bienes) {
        if (b.isMismaDireccion(d)) {
          d.setDireccionGarantia(true);
          break;
        }
      }
    }

    this.direcciones =
        interviniente
            .getDirecciones()
            .stream()
            .map(DireccionMapper::parseEntityToDTO)
            .collect(Collectors.toList());

    for (Direccion d : new ArrayList<>(interviniente.getDirecciones())) {
      d.setDireccionGarantia(false);
    }
  }
}
