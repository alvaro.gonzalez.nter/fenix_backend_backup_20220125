package com.haya.alaska.interviniente.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class IntervinienteDTOToList implements Serializable {

  private static final long serialVersionUID = 1L;
  private Integer id;
  private String idCarga;
  private String nombre;
  private String razonSocial;
  private String dni;
  private String localidad;
  private CatalogoMinInfoDTO tipoIntervencion;
  private Integer ordenIntervencion;
  private Boolean activo;
  private Date fechaNacimiento;
  private CatalogoMinInfoDTO tipoDocumento;
  private CatalogoMinInfoDTO otrasSituacionesDeVulnerabilidad;
  private Boolean vulnerabilidad;
  private String telefono;
  private Boolean telefonoValidado;
  private String movil;
  private Boolean movilValidado;
  private String email;
  private Boolean emailValidado;
  private String nacionalidad;
  private String numeroDocumento;
  private Boolean contactado;
  private String producto;
  private String porcentajeIntervencion;
  private List<ContratoDTOToList> listaContratos;

  public IntervinienteDTOToList(Interviniente interviniente, Integer idContrato) {
    ContratoInterviniente ci = null;
     if(idContrato != null) ci= interviniente.getContratoInterviniente(idContrato);
    BeanUtils.copyProperties(interviniente, this);
    if(interviniente.getNombre() != null && interviniente.getApellidos() != null)
      this.nombre = interviniente.getNombre() + " " + interviniente.getApellidos();
    else
      this.nombre = interviniente.getNombre();
    this.razonSocial = interviniente.getRazonSocial();
    this.dni = interviniente.getNumeroDocumento() != null ? interviniente.getNumeroDocumento() : "";
    this.localidad = interviniente.getLocalidad();
    this.telefono = interviniente.getTelefonoPrincipal();
    DatoContacto principal = interviniente.getDatoPrincipal();
    if (principal != null){
      this.telefonoValidado = principal.getMovilValidado();
      this.email = principal.getEmail();
      this.emailValidado = principal.getEmailValidado();
    }
    this.nacionalidad = interviniente.getNacionalidad() != null ? interviniente.getNacionalidad().getValor() : null;
    this.tipoDocumento =
      interviniente.getTipoDocumento() != null
        ? new CatalogoMinInfoDTO(interviniente.getTipoDocumento())
        : null;
    this.otrasSituacionesDeVulnerabilidad =
      interviniente.getOtrasSituacionesVulnerabilidad() != null
        ? new CatalogoMinInfoDTO(interviniente.getOtrasSituacionesVulnerabilidad())
        : null;
    if(ci != null){
      if( ci.getContrato() != null && ci.getContrato().getProducto() != null ){
        this.setProducto(ci.getContrato().getProducto().getValor());
      }
      this.tipoIntervencion =
              ci.getTipoIntervencion() != null
                      ? new CatalogoMinInfoDTO(ci.getTipoIntervencion())
                      : null;
      this.ordenIntervencion = ci.getOrdenIntervencion();
      DatoContacto dc = interviniente.getDatoPrincipal();
      if (dc != null){
        this.setTelefono(dc.getFijo());
        this.setTelefonoValidado(dc.getFijoValidado());
        this.setMovil(dc.getMovil());
        this.setMovilValidado(dc.getMovilValidado());
        this.setEmail(dc.getEmail());
        this.setEmailValidado(dc.getEmailValidado());
      }
    }
  }

  public static List<IntervinienteDTOToList> newList(List<Interviniente> intervinientes, Integer idContrato) {
    return intervinientes.stream().map(interviniente -> new IntervinienteDTOToList(interviniente, idContrato)).collect(Collectors.toList());
  }

  public static List<IntervinienteDTOToList> newListExpediente(
    List<Interviniente> intervinientes, Integer idExpediente) {
    List<IntervinienteDTOToList> list = new ArrayList<>();
    for (Interviniente interviniente : intervinientes) {
      Integer idContrato = null;
      for (ContratoInterviniente ci : interviniente.getContratos()) {
        if (ci.getContrato().getExpediente().getId().equals(idExpediente)) {
          idContrato = ci.getContrato().getId();
          break;
        }
      }
      if (idContrato != null) {
        list.add(new IntervinienteDTOToList(interviniente, idContrato));
      }
    }
    return list;
  }
}
