package com.haya.alaska.interviniente.infrastructure.repository;

import com.haya.alaska.interviniente.domain.Interviniente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface IntervinienteRepository extends JpaRepository<Interviniente, Integer> {

  @Query(
      value =
          "SELECT " + //ContratoInterviniente
            "i.ID as id, " +
            "i.ID_TIPO_DOCUMENTO as tipoDocumento, " +
            "i.DES_NUM_DOCUMENTO as numeroDocumento, " +
            "i.IND_ACTIVO as activo, " +
            "i.IND_PERSONA_JURIDICA as personaJuridica, " +
            "i.DES_NOMBRE as nombre, " +
            "i.DES_APELLIDOS as apellidos, " +
            "i.ID_SEXO as sexo, " +
            "i.ID_ESTADO_CIVIL as estadoCivil, " +
            "i.ID_RESIDENCIA as residencia, " +
            "i.ID_NACIONALIDAD as nacionalidad, " +
            "i.ID_PAIS_NACIMIENTO as paisNacimiento, " +
            "i.FCH_NACIMIENTO as fechaNacimiento, " +
            "i.FCH_CONSTITUCION as fechaConstitucion, " +
            "i.DES_RAZON_SOCIAL as razonSocial, " +
            "i.IND_CONTACTADO as contactado, " +
            "i.ID_TIPO_OCUPACION as tipoOcupacion, " +
            "i.DES_EMPRESA as empresa, " +
            "i.DES_TELEFONO_EMPRESA as telefonoEmpresa, " +
            "i.DES_CONTACTO_EMPRESA as contactoEmpresa, " +
            "i.ID_TIPO_CONTRATO as tipoContrato, " +
            "i.DES_COD_SOCIO_PROFESIONAL as codigoSocioProfesional, " +
            "i.DES_CNAE as cnae, " +
            "i.NUM_INGRESOS_NETOS_MENSUALES as ingresosNetosMensuales, " +
            "i.DES_DESC_ACTIVIDAD as descripcionActividad, " +
            "i.NUM_HIJOS_DEPENDIENTES as numHijosDependientes, " +
            "i.IND_OTROS_RIESGOS as otrosRiesgos, " +
            "i.IND_VULNERABILIDAD as vulnerabilidad, " +
            "i.IND_PENSIONISTA as pensionista, " +
            "i.ID_TIPO_PRESTACION as tipoPrestacion, " +
            "i.IND_DEPENDIENTE as dependiente, " +
            "i.IND_DISCAPACITADOS as discapacitados, " +
            "i.ID_VULNERABILIDAD as otrasSituacionesVulnerabilidad, " +
            "i.DES_NOTAS_1 as notas1, " +
            "i.DES_NOTAS_2 as notas2 " +
            "FROM HIST_MSTR_INTERVINIENTE i JOIN REGISTRO_HISTORICO r ON i.REV = r.id " +
            "WHERE i.ID = ?1 AND r.ID_USUARIO IS NULL ORDER BY timestamp DESC LIMIT 1",
      nativeQuery = true)
  Optional<Map> findHistoricoById(Integer id);

  Page<Interviniente> findAllBy(Pageable pageable);

  Page<Interviniente> findAllDistinctByContratosIdIn(Pageable pageable, Iterable<Integer> contratos);

  Long countDistinctByContratosIdIn(Iterable<Integer> contratos);

  List<Interviniente> findAllByContratosIdIn(List<Integer> contratosIds);

  Optional<Interviniente> findByIdCarga(String idCarga);

  Optional<Interviniente> findByContratosId(Integer contratoId);

  //Optional<Interviniente> findByIdAndContratoIntervinienteId(Integer contratoId, Integer contratoIntervinienteId);

  List<Interviniente> findByContratosContratoExpedienteCarteraIdCargaAndContratosContratoExpedienteCarteraIdCargaSubcarteraIsNotNull(String carteraId);
  Optional<Interviniente> findByNumeroDocumento(String dni);
  Optional<Interviniente> findByRazonSocial(String razon);
  Optional<Interviniente> findByDatosContactoEmail(String email);


  @Query(
    value =
      "SELECT *"
        + "FROM MSTR_INTERVINIENTE i "
        + "INNER join RELA_CONTRATO_INTERVINIENTE ci on i.id = ci.ID_INTERVINIENTE "
        + "INNER join MSTR_CONTRATO c on c.id = ci.ID_CONTRATO "
        + "INNER JOIN MSTR_EXPEDIENTE e ON e.id = c.ID_EXPEDIENTE "
        + "where e.id= :idExpediente ",
    nativeQuery = true)
  List<Interviniente> findAllByExpedienteId(@Param("idExpediente") Integer idExpediente);

  @Query(
    value =
      "SELECT i.* FROM MSTR_INTERVINIENTE i \r\n"
	+ "INNER JOIN RELA_CONTRATO_INTERVINIENTE ci on i.id = ci.ID_INTERVINIENTE\r\n"
	+ "INNER JOIN MSTR_CONTRATO c on c.id = ci.ID_CONTRATO\r\n"
        + "INNER JOIN MSTR_EXPEDIENTE e ON e.id = c.ID_EXPEDIENTE\r\n"
	+ "WHERE c.id = :idContrato",
    nativeQuery = true)
  List<Interviniente> findAllIntervinientesByContratoId(@Param("idContrato") Integer idContrato);

  @Query(
    value =
      "SELECT DISTINCT i.* FROM MSTR_INTERVINIENTE i \r\n"
        + "INNER JOIN RELA_CONTRATO_INTERVINIENTE ci on i.id = ci.ID_INTERVINIENTE\r\n"
        + "INNER JOIN MSTR_CONTRATO c on c.id = ci.ID_CONTRATO\r\n"
        + "INNER JOIN MSTR_EXPEDIENTE e ON e.id = c.ID_EXPEDIENTE\r\n"
        + "INNER JOIN mstr_dato_contacto dc ON i.ID = dc.ID_INTERVINIENTE\r\n"
        + "INNER JOIN mstr_direccion di ON i.ID = di.ID_INTERVINIENTE\r\n"
        + "WHERE c.id = :idContrato",
    nativeQuery = true)
  List<Interviniente> findAllIntervinientesWithDatosContacto(@Param("idContrato") Integer idContrato);

  @Query(
    value =
      "SELECT DISTINCT i.* FROM MSTR_INTERVINIENTE i \r\n"
        + "INNER JOIN RELA_CONTRATO_INTERVINIENTE ci on i.id = ci.ID_INTERVINIENTE\r\n"
        + "INNER JOIN MSTR_CONTRATO c on c.id = ci.ID_CONTRATO \r\n"
        + "INNER JOIN MSTR_EXPEDIENTE e ON e.id = c.ID_EXPEDIENTE\r\n"
        + "INNER JOIN mstr_dato_contacto dc ON i.ID = dc.ID_INTERVINIENTE\r\n"
        + "WHERE c.id = :idContrato\r\n"
        + "AND dc.DES_EMAIL IS NOT NULL\r\n"
        + "AND DES_EMAIL <> ''",
    nativeQuery = true)
  List<Interviniente> findAllIntervinientesWithEmail(@Param("idContrato") Integer idContrato);

  @Query(
    value =
      "SELECT DISTINCT i.* FROM MSTR_INTERVINIENTE i \r\n"
        + "INNER JOIN RELA_CONTRATO_INTERVINIENTE ci on i.id = ci.ID_INTERVINIENTE\r\n"
        + "INNER JOIN MSTR_CONTRATO c on c.id = ci.ID_CONTRATO \r\n"
        + "INNER JOIN MSTR_EXPEDIENTE e ON e.id = c.ID_EXPEDIENTE\r\n"
        + "INNER JOIN mstr_dato_contacto dc ON i.ID = dc.ID_INTERVINIENTE\r\n"
        + "WHERE c.id = :idContrato\r\n"
        + "AND dc.DES_MOVIL IS NOT NULL\r\n"
        + "AND dc.DES_MOVIL <> ''",
    nativeQuery = true)
  List<Interviniente> findAllIntervinientesWithMovil(@Param("idContrato") Integer idContrato);

  @Query(
    value =
      "SELECT * FROM (SELECT ID_INTERVINIENTE, NUM_ORDEN_INTERVENCION ,ID_TIPO_INTERVENCION, ID_CONTRATO, ROW_NUMBER ()\n"
        + "OVER(PARTITION BY NUM_ORDEN_INTERVENCION,ID_TIPO_INTERVENCION,ID_INTERVINIENTE)\n"
        + "AS CONTEO FROM RELA_CONTRATO_INTERVINIENTE rci ORDER BY ID_INTERVINIENTE) T WHERE ID_INTERVINIENTE IN ?1",
    nativeQuery = true)
  List<Map> findAllIntervinientesOrden(List<Integer>idsInterviniente);

  List<Interviniente>findAllByIdNotIn(List<Integer>lista);

  List<Interviniente>findAllByIdIn(List<Integer>lista);

  List<Interviniente> findDistinctAllByContratosContratoExpedienteIdIn(List<Integer> lista);

  List<Interviniente> findAllByFechaCargaAndIdHayaIsNull(Date fecha);

  Optional<Interviniente> findAllByIdHaya(String idHaya);
}
