package com.haya.alaska.interviniente.infrastructure.controller.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class IntervinienteOcupanteDatosContactoDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer id;
  private String nombre;
  private String apellidos;
  private String numeroDocumento;
 // private String movil;
  private boolean movilvalidado;
  //private String email;
  private boolean emailvalidado;
  private String direccion;
 // private String telefono;
  private boolean telefonovalidado;
  private List<String>movilList;
  private List<String>emailList;
  private List<String>telefonoList;



}
