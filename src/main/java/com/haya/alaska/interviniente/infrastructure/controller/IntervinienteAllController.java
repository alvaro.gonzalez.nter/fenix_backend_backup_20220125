package com.haya.alaska.interviniente.infrastructure.controller;

import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDtoOutput;
import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDeudorDTO;
import com.haya.alaska.interviniente.application.IntervinienteUseCase;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteOcupanteDatosContactoDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteVinculacionDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/intervinientes")
public class IntervinienteAllController {


  @Autowired IntervinienteUseCase intervinienteUseCase;


  @ApiOperation(
    value = "Listado de todos los intervinientes sin duplicar",
    notes = "Devuelve todos los intervinientes registrados en la base de datos, filtrando opcionalmente por contratos")
  @GetMapping
  public ListWithCountDTO<IntervinienteDTOToList> getAllFiltered(
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @RequestParam(value = "contratos", required = false) List<Integer> contratos) throws NotFoundException {

    return intervinienteUseCase.getAllFiltered(size, page, contratos);
  }

  @ApiOperation(
    value = "Listado de todos los intervinientes",
    notes = "Devuelve todos los intervinientes registrados en la base de datos, filtrando opcionalmente por contratos")
  @GetMapping("/dupli")
  public ListWithCountDTO<IntervinienteDTOToList> getAll(
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @RequestParam(value = "contratos", required = false) List<Integer> contratos) {

    return intervinienteUseCase.getAll(size, page, contratos);
  }

  @ApiOperation(
    value = "Obtener interviniente y contratos de los interviniente",
    notes = "Devuelve el interviniente y los contratos relacionados")
  @GetMapping("/{id}/contratos")
  @Transactional
  public ListWithCountDTO<IntervinienteVinculacionDTO> getIntervinienteContratosById(
    @PathVariable("id") Integer id) throws Exception {
    return intervinienteUseCase.findVinculadosIntervinienteById(id);
  }

 /* @ApiOperation(
	  value = "Obtener los intervinientes con sus datos de contacto",
	  notes = "Devuelve el interviniente")
  @GetMapping("/{idContrato}/datoscontacto")
  public List<IntervinienteOcupanteDatosContactoDTO> getIntervinientesDatosContacto(@PathVariable("idContrato") int idContrato,
      @RequestParam(value = "tipo", required = false) String tipo) throws Exception {
    return intervinienteUseCase.getAllIntervinienteDatosContacto(tipo, idContrato);
  }
*/
 @ApiOperation(value = "Obtener interviniente", notes = "Devuelve el interviniente con id Origen enviado")
 @GetMapping("/byIdOrigen/{idIntervinienteOrigen}")
 @Transactional
 public IntervinienteDTO getContratoByIdCarga(
   @PathVariable("idIntervinienteOrigen") String idIntervinienteOrigen) {
   return intervinienteUseCase.findIntervinienteByIdOrigen(idIntervinienteOrigen);
 }



  @ApiOperation(
    value = "Obtener los intervinientes con sus datos de contacto",
    notes = "Devuelve el interviniente")
  @GetMapping("/{idExpediente}/datoscontacto")
  public List<IntervinienteOcupanteDatosContactoDTO> getIntervinientesDatosContacto(@PathVariable("idExpediente") int idExpediente) throws Exception {
    return intervinienteUseCase.getAllIntervinienteContacto(idExpediente);
  }
  //Problemas a la hora de llamar al endpoint en interviniente controller
  @ApiOperation(
    value = "Obtener documentación del portal deudor por cliente",
    notes =
      "Devuelve la documentación disponible del cliente con id enviado")
  @PostMapping("/documentosInterviniente/cliente/{idCliente}")
  @Transactional
  public ListWithCountDTO<DocumentoDeudorDTO> getDocumentosByInterviniente(
   // @PathVariable("idInterviniente") Integer idInterviniente,
   @RequestBody(required = false) BusquedaRepositorioDTO busquedaRepositorioDTO,
   @RequestParam(value="tipoDocumento",required = false) String tipoDocumento,
   @RequestParam(value="id",required = false) String idDocumento,
   @RequestParam(value="usuarioCreador",required = false) String usuarioCreador,
   @RequestParam(value="nombreArchivo",required = false) String nombreDocumento,
   @RequestParam(value = "fechaAlta", required = false) String fechaAlta,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @PathVariable(value="idCliente")Integer idCliente,
    @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page)
    throws NotFoundException, IOException {
    return intervinienteUseCase.findDocumentosDeudorByIntervinienteId(busquedaRepositorioDTO,idDocumento,nombreDocumento,tipoDocumento,fechaAlta,usuarioCreador,idCliente,orderDirection,orderField,size,page);
  }
  //Endpoint Portal Deudor para enviar documentos del deudor al gestor
  @ApiOperation(value = "Añadir un documento a un interviniente")
  @PostMapping("/{idInterviniente}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoInterviniente(
    @PathVariable("idInterviniente") Integer idInterviniente,
    @RequestPart("file") @NotNull @NotBlank MultipartFile file,
    @RequestParam(value = "comentario", required = false)String comentario,
    @RequestParam(value = "idGestor", required = false)Integer idGestor,
    @RequestParam(value="titulo",required = false)String titulo,
    @RequestParam(value="idCliente",required = false)Integer idCliente)
    throws Exception {
    intervinienteUseCase.createDocumentoDeudorInterviniente(idInterviniente, file,idGestor,comentario,titulo,idCliente);
  }
}



