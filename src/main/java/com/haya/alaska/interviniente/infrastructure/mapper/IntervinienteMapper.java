package com.haya.alaska.interviniente.infrastructure.mapper;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.mapper.ContratoMapper;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.dato_contacto.infrastructure.mapper.DatoContactoMapper;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionDTO;
import com.haya.alaska.direccion.infrastructure.mapper.DireccionMapper;
import com.haya.alaska.interviniente.application.IntervinienteUseCase;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteBusquedaDTOToList;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class IntervinienteMapper {

  @Autowired IntervinienteUseCase intervinienteUseCase;
  @Autowired PaisRepository paisRepository;
  @Autowired Map<String, CatalogoRepository<? extends Catalogo, Integer>> catalogoRepository;
  @Autowired private ContratoMapper contratoMapper;
  @Autowired private IntervinienteRepository intervinienteRepository;
  @Autowired private TipoIntervencionRepository tipoIntervencionRepository;
  @Autowired private ContratoRepository contratoRepository;

  public Interviniente parseDTOToEntity(IntervinienteDTO intervinienteDTO) {
    Interviniente interviniente = new Interviniente();
    BeanUtils.copyProperties(intervinienteDTO, interviniente);
    return interviniente;
  }

  public IntervinienteDTO parseEntityToDTO(
      Interviniente interviniente, ContratoInterviniente contratoInterviniente) {

    IntervinienteDTO intervinienteDTO = new IntervinienteDTO();

    BeanUtils.copyProperties(interviniente, intervinienteDTO);
    intervinienteDTO.setOrdenIntervencion(contratoInterviniente.getOrdenIntervencion());
    intervinienteDTO.setTipoIntervencion(
        new CatalogoMinInfoDTO(contratoInterviniente.getTipoIntervencion()));
    List<DatoContacto> datosContacto = new ArrayList<>(interviniente.getDatosContacto());
    List<DatosContactoDTOToList> datosContactoDTO =
        datosContacto.stream()
            .map(DatoContactoMapper::parseEntityToListDTO)
            .collect(Collectors.toList());
    List<Direccion> direcciones = new ArrayList<>(interviniente.getDirecciones());
    List<DireccionDTO> direccionesDTO =
        direcciones.stream().map(DireccionMapper::parseEntityToDTO).collect(Collectors.toList());
    intervinienteDTO.setDatosContacto(datosContactoDTO);
    intervinienteDTO.setDirecciones(direccionesDTO);

    return intervinienteDTO;
  }

  public IntervinienteDTO parseHistorico(Map interviniente) throws IllegalAccessException {
    IntervinienteDTO intervinienteDTO = new IntervinienteDTO();
    Field[] declaredFields = IntervinienteDTO.class.getDeclaredFields();
    for (Field declaredField : declaredFields) { // non entity fields
      declaredField.setAccessible(true);
      String name = declaredField.getName();
      if (interviniente.get(name) == null) continue;
      if (declaredField.getType() == Double.class) {
        declaredField.set(intervinienteDTO, Double.valueOf(interviniente.get(name).toString()));
      } else if (interviniente.get(name).getClass().equals(Integer.class)) {
        CatalogoRepository<? extends Catalogo, Integer> repo =
            catalogoRepository.get(name + "Repository");
        if (repo != null) {
          Catalogo catalogo = repo.findById((Integer) interviniente.get(name)).orElse(null);
          if (catalogo != null)
            declaredField.set(intervinienteDTO, new CatalogoMinInfoDTO(catalogo));
        } else if (name.equals("nacionalidad")
            || name.equals("paisNacimiento")
            || name.equals("residencia")) {
          Pais p = paisRepository.findById((Integer) interviniente.get(name)).orElse(null);
          if (p != null) declaredField.set(intervinienteDTO, new CatalogoMinInfoDTO(p));
        } /* else if (name.equals("saldo") ) {
            Saldo saldo = saldoRepository.findTopByContratoIdOrderByDiaDesc((Integer) interviniente.get("id")).orElse(null);
            if (saldo != null)
              declaredField.set(intervinienteDTO, new SaldoDto(saldo));
          }*/ else {
          declaredField.set(intervinienteDTO, interviniente.get(name));
        }
      } else {
        declaredField.set(intervinienteDTO, interviniente.get(name));
      }
    }
    if (intervinienteDTO.getHistorico() != null) intervinienteDTO.getHistorico().setHistorico(null);
    return intervinienteDTO;
  }

  public IntervinienteDTOToList parseEntityToDTOList(
      Interviniente interviniente, Integer idContrato, ContratoInterviniente contratoInterviniente)
      throws NotFoundException {

    IntervinienteDTOToList intervinienteDTOToList =
        new IntervinienteDTOToList(interviniente, idContrato);

    BeanUtils.copyProperties(interviniente, intervinienteDTOToList);
    if (interviniente.getPersonaJuridica() != null && interviniente.getPersonaJuridica()) {
      intervinienteDTOToList.setNombre(interviniente.getRazonSocial());
    }
    if (interviniente.getNombre() != null && interviniente.getApellidos() != null)
      intervinienteDTOToList.setNombre(
          interviniente.getNombre() + " " + interviniente.getApellidos());
    intervinienteDTOToList.setOrdenIntervencion(contratoInterviniente.getOrdenIntervencion());
    intervinienteDTOToList.setTipoIntervencion(
        new CatalogoMinInfoDTO(contratoInterviniente.getTipoIntervencion()));
    if (contratoInterviniente.getContrato() != null
        && contratoInterviniente.getContrato().getProducto() != null) {
      intervinienteDTOToList.setProducto(
          contratoInterviniente.getContrato().getProducto().getValor());
    }
    intervinienteDTOToList.setTipoDocumento(
        interviniente.getTipoDocumento() != null
            ? new CatalogoMinInfoDTO(interviniente.getTipoDocumento())
            : null);
    intervinienteDTOToList.setOtrasSituacionesDeVulnerabilidad(
        interviniente.getOtrasSituacionesVulnerabilidad() != null
            ? new CatalogoMinInfoDTO(interviniente.getOtrasSituacionesVulnerabilidad())
            : null);
    intervinienteDTOToList.setTelefono(interviniente.getTelefonoPrincipal());
    DatoContacto dc = interviniente.getDatoPrincipal();
    if (dc != null) {
      intervinienteDTOToList.setTelefono(dc.getFijo());
      intervinienteDTOToList.setTelefonoValidado(dc.getFijoValidado());
      intervinienteDTOToList.setMovil(dc.getMovil());
      intervinienteDTOToList.setMovilValidado(dc.getMovilValidado());
      intervinienteDTOToList.setEmail(dc.getEmail());
      intervinienteDTOToList.setEmailValidado(dc.getEmailValidado());
    }

    List<Contrato> listaContratos = new ArrayList<>();
    for (var contratoIntervinienteProv : interviniente.getContratos()) {
      if (contratoIntervinienteProv.getContrato() != null)
        listaContratos.add(contratoIntervinienteProv.getContrato());
    }
    List<ContratoDTOToList> listaContratosDtos =
        contratoMapper.listContratoToListContratoDTOToList(listaContratos, interviniente);
    intervinienteDTOToList.setListaContratos(listaContratosDtos);
    return intervinienteDTOToList;
  }

  public IntervinienteDTOToList parseEntityToDTOListAgenda(Interviniente interviniente)
      throws NotFoundException {

    IntervinienteDTOToList intervinienteDTOToList = new IntervinienteDTOToList();

    BeanUtils.copyProperties(interviniente, intervinienteDTOToList);
    intervinienteDTOToList.setOrdenIntervencion(null);
    intervinienteDTOToList.setTipoIntervencion(null);
    intervinienteDTOToList.setTipoDocumento(
        interviniente.getTipoDocumento() != null
            ? new CatalogoMinInfoDTO(interviniente.getTipoDocumento())
            : null);
    intervinienteDTOToList.setTelefono(interviniente.getTelefonoPrincipal());
    return intervinienteDTOToList;
  }

  public IntervinienteDTOToList parseDTOToList(Interviniente interviniente) {

    IntervinienteDTOToList intervinienteDTOToList = new IntervinienteDTOToList();

    BeanUtils.copyProperties(interviniente, intervinienteDTOToList);
    if (interviniente.getPersonaJuridica() != null && interviniente.getPersonaJuridica()) {
      intervinienteDTOToList.setNombre(interviniente.getRazonSocial());
    }
    if (interviniente.getNombre() != null && interviniente.getApellidos() != null)
      intervinienteDTOToList.setNombre(
          interviniente.getNombre() + " " + interviniente.getApellidos());
    intervinienteDTOToList.setTipoDocumento(
        interviniente.getTipoDocumento() != null
            ? new CatalogoMinInfoDTO(interviniente.getTipoDocumento())
            : null);
    intervinienteDTOToList.setOtrasSituacionesDeVulnerabilidad(
        interviniente.getOtrasSituacionesVulnerabilidad() != null
            ? new CatalogoMinInfoDTO(interviniente.getOtrasSituacionesVulnerabilidad())
            : null);
    intervinienteDTOToList.setTelefono(interviniente.getTelefonoPrincipal());
    List<ContratoInterviniente> contratoIntervinienteList =
        interviniente.getContratos().stream().collect(Collectors.toList());

    List<Contrato> listaContratos = new ArrayList<>();
    for (var contratoIntervinienteProv : interviniente.getContratos()) {
      listaContratos.add(contratoIntervinienteProv.getContrato());
    }
    return intervinienteDTOToList;
  }

  public List<IntervinienteBusquedaDTOToList> parseIntervinienteBusqueda(Map map, boolean firma) {
    Object idInterviniente = map.get("ID_INTERVINIENTE");
    Object numOrdenIntervencion = map.get("NUM_ORDEN_INTERVENCION");
    Object idTipoIntervencion = map.get("ID_TIPO_INTERVENCION");
    Object idContrato = map.get("ID_CONTRATO");
    Integer idIntervinienteParse = Integer.parseInt(idInterviniente.toString());
    Integer numOrdenIntervencionParse = Integer.parseInt(numOrdenIntervencion.toString());
    Integer idTipoIntervencionParse = idTipoIntervencion != null ?  Integer.parseInt(idTipoIntervencion.toString())  : 0;
    int idContratoParse;
    Contrato contrato = new Contrato();
    if (idContrato != null) {
      idContratoParse = Integer.parseInt(idContrato.toString());
      contrato = contratoRepository.findById(idContratoParse).orElse(null);
    }

    Interviniente interviniente =
        intervinienteRepository.findById(idIntervinienteParse).orElse(null);
    List<IntervinienteBusquedaDTOToList> intervinienteBusquedaDTOToLists = new ArrayList<>();

    List<DatoContacto> datoContactoList;
    if (firma) datoContactoList = interviniente.getDatoPrincipal() != null ? Collections.singletonList(interviniente.getDatoPrincipal()) : new ArrayList<>();
    else datoContactoList = interviniente.getDatosContacto().stream().collect(Collectors.toList());

    if (interviniente != null) {

      // if (interviniente.getDirecciones()!=null && !interviniente.getDirecciones().isEmpty()){
      //  for (ContratoInterviniente contratoInterviniente : interviniente.getContratos()) {
      //  Contrato contrato=contratoInterviniente.getContrato();
      for (DatoContacto datoContacto : datoContactoList) {
        //  Contrato contrato = contratoInterviniente.getContrato();
        IntervinienteBusquedaDTOToList intervinienteDTOToList =
            new IntervinienteBusquedaDTOToList();
        intervinienteDTOToList.setId(idIntervinienteParse);

        //   intervinienteDTOToList.setIdContrato(idContratoParse);

        intervinienteDTOToList.setEmail(datoContacto.getEmail());

        if (datoContacto.getEmailValidado() != null && datoContacto.getEmailValidado()) {
          intervinienteDTOToList.setEmailValidado(datoContacto.getEmailValidado());
        }
        if (datoContacto.getFijoValidado() != null && datoContacto.getFijoValidado()) {
          intervinienteDTOToList.setTelefonoValidado(datoContacto.getFijoValidado());
        }
        if (interviniente.getPersonaJuridica() != null && interviniente.getPersonaJuridica()) {
          intervinienteDTOToList.setNombre(interviniente.getRazonSocial());
        }
        if (interviniente.getNombre() != null && interviniente.getApellidos() != null)
          intervinienteDTOToList.setNombre(
              interviniente.getNombre() + " " + interviniente.getApellidos());

        intervinienteDTOToList.setOrdenIntervencion(numOrdenIntervencionParse);

        tipoIntervencionRepository
            .findById(idTipoIntervencionParse)
            .ifPresent(
                tipoIntervencion1 ->
                    intervinienteDTOToList.setTipoIntervencion(
                        new CatalogoMinInfoDTO(tipoIntervencion1)));

        intervinienteDTOToList.setNumeroDocumento(
            interviniente.getNumeroDocumento() != null ? interviniente.getNumeroDocumento() : null);
        intervinienteDTOToList.setIdContrato(
            contrato.getIdCarga() != null ? contrato.getIdCarga() : null);
        intervinienteDTOToList.setTipoDocumento(
            interviniente.getTipoDocumento() != null
                ? new CatalogoMinInfoDTO(interviniente.getTipoDocumento())
                : null);
        intervinienteDTOToList.setTelefonoMovil(
            datoContacto.getMovil() != null ? datoContacto.getMovil() : null);
        intervinienteDTOToList.setTelefonoFijo(
            datoContacto.getFijo() != null ? datoContacto.getFijo() : null);
        intervinienteDTOToList.setTelefonoPrincipal(
            interviniente.getTelefonoPrincipal() != null
                ? interviniente.getTelefonoPrincipal()
                : null);

        intervinienteDTOToList.setFechaNacimiento(
            interviniente.getFechaNacimiento() != null ? interviniente.getFechaNacimiento() : null);
        intervinienteDTOToList.setActivo(interviniente.getActivo());
        Cliente cliente = null;
        if (contrato.getExpediente() != null && contrato.getExpediente().getCartera() != null) {
          cliente = contrato.getExpediente().getCartera().getCliente();
        }

        if (contrato.getExpediente() != null) {
          intervinienteDTOToList.setIdConcatenado(contrato.getExpediente().getIdConcatenado());
        }

        if (cliente != null) intervinienteDTOToList.setCliente(cliente.getNombre());
        intervinienteBusquedaDTOToLists.add(intervinienteDTOToList);
      }
    }
    //  }
    return intervinienteBusquedaDTOToLists;
  }

  public List<IntervinienteBusquedaDTOToList> parseIntervinienteBusquedaSinRelacion(
      Interviniente interviniente) {
    List<IntervinienteBusquedaDTOToList> intervinienteBusquedaDTOToLists = new ArrayList<>();
    if (interviniente.getDirecciones() != null) {
      for (Direccion direccion : interviniente.getDirecciones()) {
        IntervinienteBusquedaDTOToList intervinienteDTOToList =
            new IntervinienteBusquedaDTOToList();
        intervinienteDTOToList.setId(interviniente.getId());
        if (interviniente.getPersonaJuridica() != null && interviniente.getPersonaJuridica()) {
          intervinienteDTOToList.setNombre(interviniente.getRazonSocial());
        }
        if (interviniente.getNombre() != null && interviniente.getApellidos() != null)
          intervinienteDTOToList.setNombre(
              interviniente.getNombre() + " " + interviniente.getApellidos());
        intervinienteDTOToList.setTipoDocumento(
            interviniente.getTipoDocumento() != null
                ? new CatalogoMinInfoDTO(interviniente.getTipoDocumento())
                : null);
        intervinienteDTOToList.setTelefonoMovil(
            interviniente.getTelefonoPrincipal() != null
                ? interviniente.getTelefonoPrincipal()
                : null);
        intervinienteDTOToList.setFechaNacimiento(
            interviniente.getFechaNacimiento() != null ? interviniente.getFechaNacimiento() : null);
        intervinienteDTOToList.setActivo(interviniente.getActivo());
        intervinienteDTOToList.setTipoDireccion(
            direccion.getTipoVia() != null ? direccion.getTipoVia().getValor() : null);
        intervinienteDTOToList.setCodigoPostal(direccion.getCodigoPostal());
        intervinienteDTOToList.setMunicipio(
            direccion.getMunicipio() != null ? direccion.getMunicipio().getValor() : null);
        intervinienteDTOToList.setProvincia(
          direccion.getProvincia() != null ? direccion.getProvincia().getValor() : null);
        intervinienteDTOToList.setDireccion(
            direccion.getNombre()
                + ""
                + direccion.getNumero()
                + ""
                + direccion.getPortal()
                + ""
                + direccion.getBloque()
                + ""
                + direccion.getEscalera()
                + ""
                + direccion.getPiso()
                + ""
                + direccion.getPuerta());
        intervinienteBusquedaDTOToLists.add(intervinienteDTOToList);
      }
    } else {
      IntervinienteBusquedaDTOToList intervinienteDTOToList = new IntervinienteBusquedaDTOToList();
      intervinienteDTOToList.setId(interviniente.getId());

      if (interviniente.getPersonaJuridica() != null && interviniente.getPersonaJuridica()) {
        intervinienteDTOToList.setNombre(interviniente.getRazonSocial());
      }
      if (interviniente.getNombre() != null && interviniente.getApellidos() != null)
        intervinienteDTOToList.setNombre(
            interviniente.getNombre() + " " + interviniente.getApellidos());
      intervinienteDTOToList.setTipoDocumento(
          interviniente.getTipoDocumento() != null
              ? new CatalogoMinInfoDTO(interviniente.getTipoDocumento())
              : null);
      // intervinienteDTOToList.setTelefono(interviniente.getTelefonoPrincipal()!=null ?
      // interviniente.getTelefonoPrincipal():null);
      intervinienteDTOToList.setFechaNacimiento(
          interviniente.getFechaNacimiento() != null ? interviniente.getFechaNacimiento() : null);
      intervinienteDTOToList.setActivo(interviniente.getActivo());
      intervinienteBusquedaDTOToLists.add(intervinienteDTOToList);
    }
    return intervinienteBusquedaDTOToLists;
  }
}
