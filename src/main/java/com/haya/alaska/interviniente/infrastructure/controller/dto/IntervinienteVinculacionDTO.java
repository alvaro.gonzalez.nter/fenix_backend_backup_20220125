package com.haya.alaska.interviniente.infrastructure.controller.dto;


import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class IntervinienteVinculacionDTO {
  private Integer expedienteID;
  private String idConcatenado;
  private UsuarioDTO nombreGestor;
  private String n0Contrato;
  private String tipoIntervencion;
  private Integer ordenIntervencion;

  public IntervinienteVinculacionDTO(Integer idExpediente, String concatenado, UsuarioDTO gestor, String idContrato, String tipoIntervencion, Integer ordenIntervencion) {
      this.expedienteID = idExpediente;
      this.idConcatenado = concatenado;
      this.nombreGestor = gestor;
      this.n0Contrato = idContrato;
      this.tipoIntervencion = tipoIntervencion;
      this.ordenIntervencion = ordenIntervencion;
  }
}
