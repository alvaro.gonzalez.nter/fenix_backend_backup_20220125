package com.haya.alaska.interviniente.infrastructure.controller.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionInputDTO;
import com.haya.alaska.estado_civil.domain.EstadoCivil;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class IntervinienteInputDTO implements Serializable{

  private static final long serialVersionUID = 1L;

  private String numeroDocumento;
  private Boolean activo;
  private Boolean personaJuridica;
  private Integer ordenIntervencion;
  private String nombre;
  private String apellidos;
  private Integer residencia;
  private Date fechaNacimiento;
  private String razonSocial;
  private Date fechaConstitucion;
  private Boolean contactado;
  private List<DatosContactoDTOToList> datosContacto;
  private String empresa;
  private String telefonoEmpresa;
  private String contactoEmpresa;
  private String codigoSocioProfesional;
  private String cnae;
  private Double ingresosNetosMensuales;
  private String descripcionActividad;
  private Integer numHijosDependientes;
  private Boolean otrosRiesgos;
  private Boolean vulnerabilidad;
  private Boolean pensionista;
  private Boolean dependiente;
  private Boolean discapacitados;
  private String notas1;
  private String notas2;
  private Integer nacionalidad;
  private Integer paisNacimiento;

  private Integer tipoIntervencion;
  private Integer tipoDocumento;
  private Integer sexo;
  private Integer estadoCivil;
  private Integer tipoOcupacion;
  private Integer tipoContrato;
  private Integer tipoPrestacion;
  private Integer otrasSituacionesVulnerabilidad;

  private String porcentajeIntervencion;

  private List<DireccionInputDTO> direcciones;

}
