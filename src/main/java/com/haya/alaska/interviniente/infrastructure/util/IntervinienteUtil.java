package com.haya.alaska.interviniente.infrastructure.util;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.dato_contacto.infrastructure.DatoContactoUtil;
import com.haya.alaska.dato_contacto.infrastructure.mapper.DatoContactoMapper;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.mapper.DireccionMapper;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.direccion.infrastructure.util.DireccionUtil;
import com.haya.alaska.estado_civil.infrastructure.repository.EstadoCivilRepository;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteInputDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteOcupanteDatosContactoDTO;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.sexo.infrastructure.repository.SexoRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.tipo_contrato.infrastructure.repository.TipoContratoRepository;
import com.haya.alaska.tipo_documento.infrastructure.repository.TipoDocumentoRepository;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import com.haya.alaska.tipo_ocupacion.infrastructure.repository.TipoOcupacionRepository;
import com.haya.alaska.tipo_prestacion.infrastructure.repository.TipoPrestacionRepository;
import com.haya.alaska.vulnerabilidad.infrastructure.repository.VulnerabilidadRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;
import java.util.StringJoiner;

@Component
@Slf4j
public class IntervinienteUtil {

  @Autowired DatoContactoMapper datoContactoMapper;
  @Autowired DireccionMapper direccionMapper;
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired TipoIntervencionRepository tipoIntervencionRepository;
  @Autowired TipoDocumentoRepository tipoDocumentoRepository;
  @Autowired TipoOcupacionRepository tipoOcupacionRepository;
  @Autowired TipoContratoRepository tipoContratoRepository;
  @Autowired VulnerabilidadRepository vulnerabilidadRepository;
  @Autowired PaisRepository paisRepository;
  @Autowired TipoPrestacionRepository tipoPrestacionRepository;
  @Autowired DireccionRepository direccionRepository;
  @Autowired ContratoIntervinienteRepository contratoIntervinienteRepository;
  @Autowired ContratoRepository contratoRepository;
  @Autowired SexoRepository sexoRepository;
  @Autowired EstadoCivilRepository estadoCivilRepository;
  @Autowired ServicioMaestro servicioMaestro;
  @Autowired DatoContactoUtil datoContactoUtil;
  @Autowired DireccionUtil direccionUtil;
  @Autowired DatoContactoRepository datoContactoRepository;
  @Autowired BienRepository bienRepository;

  public Interviniente createInterviniente(
    IntervinienteInputDTO intervinienteDTO, Integer idContrato) throws Exception {
    Interviniente interviniente = new Interviniente();

    // validación de campos

    if (intervinienteDTO.getPersonaJuridica() == null) {
      throw new RequiredValueException("personaJuridica");
    }
    if (intervinienteDTO.getTipoIntervencion() == null)
      throw new RequiredValueException("tipo intervención");
    if (intervinienteDTO.getOrdenIntervencion() == null)
      throw new RequiredValueException("orden intervención");
    // comprobamos que no haya más intervinientes con el orden introducido
    if (!checkOrdenInterviniente(interviniente.getId(), idContrato, intervinienteDTO.getOrdenIntervencion(),
            intervinienteDTO.getTipoIntervencion())){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("There is already an intervener for this contract with the order of intervention and type of intervention entered");
      else throw new RequiredValueException("Ya existe un interviniente para este contrato con el orden intervención y tipo de intervención introducidos");
    }
    if (intervinienteDTO.getContactado() == null)
      throw new RequiredValueException("contactado");

    BeanUtils.copyProperties(intervinienteDTO, interviniente, "datosContacto","direcciones");
    interviniente.setActivo(true);

    if (intervinienteDTO.getSexo() != null)
      interviniente.setSexo(sexoRepository.findById(intervinienteDTO.getSexo()).orElseThrow(
              ()-> new NotFoundException("género", intervinienteDTO.getSexo())));
    if (intervinienteDTO.getEstadoCivil() != null)
      interviniente.setEstadoCivil(estadoCivilRepository.findById(intervinienteDTO.getEstadoCivil()).orElseThrow(
              ()-> new NotFoundException("estado civil", intervinienteDTO.getEstadoCivil())));
    if (intervinienteDTO.getPorcentajeIntervencion() != null)
      interviniente.setPorcentajeIntervencion(intervinienteDTO.getPorcentajeIntervencion());

    interviniente = intervinienteRepository.saveAndFlush(interviniente);
    ContratoInterviniente newContratoInterviniente = new ContratoInterviniente();
    newContratoInterviniente.setContrato(contratoRepository.findById(idContrato).orElseThrow(()->
            new NotFoundException("contrato", idContrato)));
    newContratoInterviniente.setInterviniente(interviniente);
    newContratoInterviniente.setOrdenIntervencion(intervinienteDTO.getOrdenIntervencion());
    if (intervinienteDTO.getTipoIntervencion() != null)
      newContratoInterviniente.setTipoIntervencion(
              tipoIntervencionRepository.findById(intervinienteDTO.getTipoIntervencion()).orElse(null));
    newContratoInterviniente = contratoIntervinienteRepository.saveAndFlush(newContratoInterviniente);
    // Las 2 lineas siguientes provocan duplicidad en la BD revisar si son necesarias
    //interviniente.setDatosContacto(datoContactoMapper.dTOListToEntityList(intervinienteDTO.getDatosContacto(),interviniente.getId()));
    //interviniente.setDirecciones(direccionMapper.listInputDtoToEntityList(intervinienteDTO.getDirecciones()));
    // activamos el interviniente

    if (intervinienteDTO.getTipoDocumento() != null)
      interviniente.setTipoDocumento(
          tipoDocumentoRepository.findById(intervinienteDTO.getTipoDocumento()).orElse(null));
    if (intervinienteDTO.getSexo() != null){
      Integer sexoValor = intervinienteDTO.getSexo();
      interviniente.setSexo(sexoRepository.findById(sexoValor).orElse(null));
    }
    if (intervinienteDTO.getEstadoCivil() != null){
      Integer estadoCivilValor = intervinienteDTO.getEstadoCivil();
      interviniente.setEstadoCivil(estadoCivilRepository.findById(estadoCivilValor).orElse(null));
    }

    if (intervinienteDTO.getTipoOcupacion() != null)
      interviniente.setTipoOcupacion(
          tipoOcupacionRepository.findById(intervinienteDTO.getTipoOcupacion()).orElse(null));
    if (intervinienteDTO.getTipoContrato() != null)
      interviniente.setTipoContrato(
          tipoContratoRepository.findById(intervinienteDTO.getTipoContrato()).orElse(null));
    if (intervinienteDTO.getOtrasSituacionesVulnerabilidad() != null)
      interviniente.setOtrasSituacionesVulnerabilidad(
          vulnerabilidadRepository
              .findById(intervinienteDTO.getOtrasSituacionesVulnerabilidad())
              .orElse(null));
    if (intervinienteDTO.getNacionalidad() != null)
      interviniente.setNacionalidad(
          paisRepository.findById(intervinienteDTO.getNacionalidad()).orElse(null));
    if (intervinienteDTO.getPaisNacimiento() != null)
      interviniente.setPaisNacimiento(
          paisRepository.findById(intervinienteDTO.getPaisNacimiento()).orElse(null));
    if (intervinienteDTO.getResidencia() != null)
      interviniente.setResidencia(
          paisRepository.findById(intervinienteDTO.getResidencia()).orElse(null));
    if (intervinienteDTO.getTipoPrestacion() != null)
      interviniente.setTipoPrestacion(
          tipoPrestacionRepository.findById(intervinienteDTO.getTipoPrestacion()).orElse(null));
    // La relacion con datoContactos se genera tras el metodo, en IntervinienteServicePrincipal
    //registramos en maestro
    /*TODO descomentar cuando veamos con Diana el error en la función de registro en maestro*/
    Contrato contrato = contratoRepository.findById(idContrato).orElseThrow( () -> new NotFoundException("No se ha encontrato el contrato con ID: "+idContrato) );
    servicioMaestro.registrarInterviniente(interviniente, contrato.getExpediente().getCartera().getCliente().getNombre(), newContratoInterviniente.getTipoIntervencion());
    return intervinienteRepository.saveAndFlush(interviniente);
  }

  public Interviniente actualizarCampos(
      Interviniente interviniente, IntervinienteInputDTO intervinienteDTO, Integer idContrato, ContratoInterviniente contratoInterviniente, Integer idTipoIntervencion)
      throws NotFoundException, RequiredValueException {

    if (intervinienteDTO.getTipoIntervencion() == null)
      throw new RequiredValueException("tipo intervención");
    if (intervinienteDTO.getOrdenIntervencion() == null)
      throw new RequiredValueException("orden intervención");
    // comprobamos que no haya más intervinientes con el orden introducido
    if (!checkOrdenInterviniente(
        interviniente.getId(), idContrato, intervinienteDTO.getOrdenIntervencion(), intervinienteDTO.getTipoIntervencion())){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("There is already an intervener for this contract with the order of intervention and type of intervention entered");
      else throw new RequiredValueException("Ya existe un interviniente para este contrato con el orden intervención y tipo de intervención introducidos");
    }
    if (intervinienteDTO.getContactado() == null)
      throw new RequiredValueException("contactado");

    BeanUtils.copyProperties(intervinienteDTO, interviniente, "id", "personaJuridica","datosContacto","direcciones");
    var ordenIntervencion = intervinienteDTO.getOrdenIntervencion();
    //var tipoIntervencionViejo = tipoIntervencionRepository.findById(idTipoIntervencion).orElse(null);
    var tipoIntervencionNuevo = intervinienteDTO.getTipoIntervencion();
    if (intervinienteDTO.getSexo() != null)
      interviniente.setSexo(sexoRepository.findById(intervinienteDTO.getSexo()).orElseThrow(
              ()-> new NotFoundException("género", intervinienteDTO.getSexo())));
    if (intervinienteDTO.getEstadoCivil() != null)
      interviniente.setEstadoCivil(estadoCivilRepository.findById(intervinienteDTO.getEstadoCivil()).orElseThrow(
              ()-> new NotFoundException("estado civil", intervinienteDTO.getEstadoCivil())));

    if (tipoIntervencionNuevo != null && ordenIntervencion != null ){
      /*ContratoInterviniente ci = contratoIntervinienteRepository.findByContratoIdAndIntervinienteIdAndTipoIntervencionId(
              idContrato, interviniente.getId(),tipoIntervencionViejo.getId()).orElseThrow(() ->
              new NotFoundException("No se encontró un ContratoInterviniente para estos valores:" +
                      " contrato: " + idContrato + ", orden: " + ordenIntervencion + " tipoIntervencion: " + tipoIntervencionViejo));
      ContratoInterviniente newCi = contratoIntervinienteRepository.findByContratoIdAndOrdenIntervencionAndTipoIntervencionId(
              idContrato, intervinienteDTO.getOrdenIntervencion(), intervinienteDTO.getTipoIntervencion()).orElse(null);
      if (newCi != null && !newCi.getInterviniente().getId().equals(interviniente.getId()))
        throw new NotFoundException("Ya existe un Interviniente para el contrato " + idContrato +
              " de tipoIntervencion " + intervinienteDTO.getTipoIntervencion() + " con orden " + intervinienteDTO.getOrdenIntervencion());*/
      contratoInterviniente.setTipoIntervencion(tipoIntervencionRepository.findById(tipoIntervencionNuevo).orElseThrow(() ->
              new NotFoundException("tipoIntervencion", tipoIntervencionNuevo)));
      contratoInterviniente.setOrdenIntervencion(ordenIntervencion);
      contratoIntervinienteRepository.saveAndFlush(contratoInterviniente);
    }
    if (intervinienteDTO.getTipoDocumento() != null)
      interviniente.setTipoDocumento(
          tipoDocumentoRepository.findById(intervinienteDTO.getTipoDocumento()).orElse(null));
    if (intervinienteDTO.getSexo() != null){
      Integer sexoValor = intervinienteDTO.getSexo();
      interviniente.setSexo(sexoRepository.findById(sexoValor).orElse(null));
    }
    if (intervinienteDTO.getEstadoCivil() != null){
      Integer estadoCivilValor = intervinienteDTO.getEstadoCivil();
      interviniente.setEstadoCivil(estadoCivilRepository.findById(estadoCivilValor).orElse(null));
    }
    if (intervinienteDTO.getTipoOcupacion() != null)
      interviniente.setTipoOcupacion(
          tipoOcupacionRepository.findById(intervinienteDTO.getTipoOcupacion()).orElse(null));
    if (intervinienteDTO.getTipoContrato() != null)
      interviniente.setTipoContrato(
          tipoContratoRepository.findById(intervinienteDTO.getTipoContrato()).orElse(null));
    if (intervinienteDTO.getOtrasSituacionesVulnerabilidad() != null)
      interviniente.setOtrasSituacionesVulnerabilidad(
          vulnerabilidadRepository
              .findById(intervinienteDTO.getOtrasSituacionesVulnerabilidad())
              .orElse(null));
    if (intervinienteDTO.getNacionalidad() != null)
      interviniente.setNacionalidad(
          paisRepository.findById(intervinienteDTO.getNacionalidad()).orElse(null));
    if (intervinienteDTO.getPaisNacimiento() != null)
      interviniente.setPaisNacimiento(
          paisRepository.findById(intervinienteDTO.getPaisNacimiento()).orElse(null));
    if (intervinienteDTO.getResidencia() != null)
      interviniente.setResidencia(
              paisRepository.findById(intervinienteDTO.getResidencia()).orElse(null));
    if (intervinienteDTO.getTipoPrestacion() != null)
      interviniente.setTipoPrestacion(
          tipoPrestacionRepository.findById(intervinienteDTO.getTipoPrestacion()).orElse(null));
    // La relacion con datoContactos se genera tras el metodo, en IntervinienteServicePrincipal

    return interviniente;
  }

  public Boolean checkOrdenInterviniente(
          Integer idInterviniente, Integer idContrato, Integer orden, Integer idTipoIntervencion) {
    ContratoInterviniente ci =
        contratoIntervinienteRepository
            .findByContratoIdAndOrdenIntervencionAndTipoIntervencionId(idContrato, orden, idTipoIntervencion)
            .orElse(null);
    if(ci != null && idInterviniente != ci.getInterviniente().getId() ) return false;
    return true;
  }

  public Interviniente addDireccion(Integer idInterviniente, Integer idDireccion) throws Exception {
    Interviniente interviniente =
        intervinienteRepository
            .findById(idInterviniente)
            .orElseThrow(
                () -> new NotFoundException("objeto", idInterviniente));
    Direccion direccion =
        direccionRepository
            .findById(idDireccion)
            .orElseThrow(
                () -> new NotFoundException("objeto", idDireccion));
    interviniente.addDireccion(direccion);
    return intervinienteRepository.save(interviniente);
  }


  /**
   * Parse de los datos de contacto de un Interviniente
   *
   * @param intervinienteDCDTO
   * @param datoContacto
   */
 /* public IntervinienteOcupanteDatosContactoDTO parteDatosContactoToIntervinienteDatosContactoDTO(IntervinienteOcupanteDatosContactoDTO intervinienteDCDTO, DatoContacto datoContacto) {
      if(datoContacto != null) {
	  String email = datoContacto.getEmail() != null ? datoContacto.getEmail() : "";
	  intervinienteDCDTO.setEmail(email);

	  boolean emailValidado = datoContacto.getEmailValidado() != null ? datoContacto.getEmailValidado() : false;
	  intervinienteDCDTO.setEmailvalidado(emailValidado);

	  String movil = datoContacto.getMovil() != null ? datoContacto.getMovil() : "";
	  intervinienteDCDTO.setMovil(movil);

	  boolean movilValidado = datoContacto.getMovilValidado() != null ? datoContacto.getMovilValidado() : false;
	  intervinienteDCDTO.setEmailvalidado(movilValidado);

	  String fijo = datoContacto.getFijo() != null ? datoContacto.getFijo() : "";
	  intervinienteDCDTO.setTelefono(fijo);

	  boolean fijoValidado = datoContacto.getFijoValidado() != null ? datoContacto.getFijoValidado() : false;
	  intervinienteDCDTO.setEmailvalidado(fijoValidado);
      }

      return intervinienteDCDTO;
  }*/

  /**
   * Parse de campos de Dirección a String concatenado de Direccion para Datos de Contacto
   *
   * @param direccion
   * @return String
   */
  public IntervinienteOcupanteDatosContactoDTO parseDireccionToString(IntervinienteOcupanteDatosContactoDTO intervinienteDCDTO, Direccion direccion) {
      StringJoiner direccionContacto = new StringJoiner(" ");

      if(direccion != null) {
          direccionContacto.add(direccion.getNombre() != null ? direccion.getNombre() : "");
          direccionContacto.add(direccion.getPortal() != null ? direccion.getNombre() : "");
          direccionContacto.add(direccion.getBloque() != null ? direccion.getBloque() : "");
          direccionContacto.add(direccion.getEscalera() != null ? direccion.getEscalera() : "");
          direccionContacto.add(direccion.getPiso() != null ? direccion.getPiso() : "");
          direccionContacto.add(direccion.getPuerta() != null ? direccion.getPuerta() : "");
          direccionContacto.add(direccion.getCodigoPostal() != null ? direccion.getCodigoPostal() : "");

          if(direccion.getMunicipio() != null) {
              direccionContacto.add(direccion.getMunicipio().getValor() != null ? direccion.getMunicipio().getValor() : "");
          }

          if(direccion.getLocalidad() != null) {
              direccionContacto.add(direccion.getLocalidad().getValor() != null ? direccion.getLocalidad().getValor() : "");
          }

          if(direccion.getProvincia() != null) {
              direccionContacto.add(direccion.getProvincia().getValor() != null ? direccion.getProvincia().getValor() : "");
          }

          if(direccion.getPais() != null) {
              direccionContacto.add(direccion.getPais().getValor() != null ? direccion.getPais().getValor() : "");
          }
      }

      intervinienteDCDTO.setDireccion(direccionContacto.toString());

      return intervinienteDCDTO;
  }

  public void direccionGoogle(Direccion d) throws NotFoundException {
    String direc = "";
    if (d.getNombre() != null){
      direc += d.getNombre();
      if (d.getTipoVia() != null){
        direc = d.getTipoVia().getValor() + " " + direc;
      }
      /*if (d.getPortal() != null){
        direc = d.getPortal() + " " + direc;
      }*/
      if (d.getNumero() != null){
        direc = direc + ", " + d.getNumero();
      }
      /*if (d.getCodigoPostal() != null){
        direc = direc + ", " + d.getCodigoPostal();
      }*/
      if (d.getPais() != null){
        direc = direc + ", " + d.getPais().getValor();
      }
      if (d.getProvincia() != null){
        direc = direc + ", " + d.getProvincia().getValor();
      }
      if (d.getMunicipio() != null){
        direc = direc + ", " + d.getMunicipio().getValor();
      }
      if (d.getLocalidad() != null){
        direc = direc + ", " + d.getLocalidad().getValor();
      }

      if (!direc.equals("")){
        LatLng ll = getGoogleLocal(direc);
        if (ll != null){
          d.setLatitud(ll.lat);
          d.setLongitud(ll.lng);
        }
      }
    }
  }

  public void direccionGoogle(Bien d) throws NotFoundException {
    String direc = "";
    if (d.getNombreVia() != null){
      direc += d.getNombreVia();
      if (d.getTipoVia() != null){
        direc = d.getTipoVia().getValor() + " " + direc;
      }
      /*if (d.getPortal() != null){
        direc = d.getPortal() + " " + direc;
      }*/
      if (d.getNumero() != null){
        direc = direc + ", " + d.getNumero();
      }
      /*if (d.getCodigoPostal() != null){
        direc = direc + ", " + d.getCodigoPostal();
      }*/
      if (d.getPais() != null){
        direc = direc + ", " + d.getPais().getValor();
      }
      if (d.getProvincia() != null){
        direc = direc + ", " + d.getProvincia().getValor();
      }
      if (d.getMunicipio() != null){
        direc = direc + ", " + d.getMunicipio().getValor();
      }
      if (d.getLocalidad() != null){
        direc = direc + ", " + d.getLocalidad().getValor();
      }

      if (!direc.equals("")){
        LatLng ll = getGoogleLocal(direc);
        if (ll != null){
          d.setLatitud(ll.lat);
          d.setLongitud(ll.lng);
        }
      }
    }
  }

  private LatLng getGoogleLocal(String direccion) throws NotFoundException {
    try {
      GeoApiContext context = new GeoApiContext.Builder()
        .apiKey("AIzaSyBOHbgPYH-aLXyoddhQuCNZapHhzvAoYjs")
        .build();
      GeocodingResult[] results = GeocodingApi.geocode(context, direccion).await();
      try{
        GeocodingResult result = results[0];
        context.shutdown();
        return result.geometry.location;
      } catch (Exception e) {
        context.shutdown();
        log.warn("*-> " + e.getMessage() + ". En la direccion: " + direccion);
        return null;
      }
    } catch (Exception e) {
      //throw new NotFoundException(e.getMessage() + ". En la direccion: " + direccion);
      log.warn("*-> " + e.getMessage() + ". En la direccion: " + direccion);
      return null;
    }
  }

  @Scheduled(cron = "0 0 18 29 07 ?")
  public void asignarGoogle() throws NotFoundException {
    List<Direccion> direcciones = direccionRepository.findAll();
    for (Direccion d : direcciones){
      direccionGoogle(d);
      direccionRepository.save(d);
    }

    List<Bien> bienes = bienRepository.findAll();
    for (Bien b : bienes){
      direccionGoogle(b);
      bienRepository.save(b);
    }
  }
}
