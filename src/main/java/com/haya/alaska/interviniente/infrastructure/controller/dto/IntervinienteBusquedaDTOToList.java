package com.haya.alaska.interviniente.infrastructure.controller.dto;

import java.util.Date;
import java.util.List;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;

import com.haya.alaska.procedimiento.infrastructure.controller.dto.ProcedimientoDTOToList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class IntervinienteBusquedaDTOToList {
  private static final long serialVersionUID = 1L;
  private Integer id;
  private String nombre;
  private CatalogoMinInfoDTO tipoIntervencion;
  private Integer ordenIntervencion;
  private Boolean activo;
  private Date fechaNacimiento;
  private CatalogoMinInfoDTO tipoDocumento;
  private String telefonoFijo;
  private String telefonoMovil;
  private String numeroDocumento;
  private String idContrato;
  private String telefonoPrincipal;
  private String cliente;
  private String idConcatenado;

  //De direccion
  private String tipoDireccion;
  private String direccion;
  private String codigoPostal;
  private String municipio;
  private String provincia;
  private String email;
  //validados
  private Boolean telefonoValidado;
  private Boolean emailValidado;
  private Boolean direccionValidada;

  //procedimientos, solo se utilizan en la parte de comunicaciones
  private List<ProcedimientoDTOToList> procedimientos;
}
