package com.haya.alaska.interviniente.infrastructure.controller;

import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.interviniente.application.IntervinienteUseCase;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteInputDTO;
import com.haya.alaska.interviniente.infrastructure.util.IntervinienteUtil;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/v1/expedientes/{idExpediente}/contratos/{idContrato}/intervinientes")
public class IntervinienteController {

  @Autowired private IntervinienteUseCase intervinienteUseCase;
  @Autowired private IntervinienteUtil intervinienteUtil;

  @ApiOperation(value = "Obtener interviniente", notes = "Devuelve el interviniente con id enviado")
  @GetMapping("/{id}")
  public IntervinienteDTO getIntervinienteById(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idContrato") Integer idContrato,
      @PathVariable("id") Integer id,
      @RequestParam("idTipoIntervencion") Integer idTipoIntervencion)
      throws Exception {
    return intervinienteUseCase.findIntervinienteById(id, idContrato, idTipoIntervencion);
  }

  @ApiOperation(value = "Obtener interviniente", notes = "Devuelve el interviniente con id enviado")
  @GetMapping("/all")
  public List<IntervinienteDTOToList> getIntervinienteByIdExpediente(
      @PathVariable("idExpediente") Integer idExpediente)
      throws Exception {
    return intervinienteUseCase.getAllByExpedienteId(idExpediente);
  }

  @ApiOperation(
      value = "Obtener intervinientes",
      notes = "Devuelve los intervinientes del contrato con id enviado")
  @GetMapping
  public ListWithCountDTO<IntervinienteDTOToList> getIntervinientes(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idContrato") Integer idContrato,
      @RequestParam(value = "id", required = false) Integer id,
      @RequestParam(value = "nombre", required = false) String nombre,
      @RequestParam(value = "tipoIntervencion", required = false) String tipoIntervencion,
      @RequestParam(value = "ordenIntervencion", required = false) String ordenIntervencion,
      @RequestParam(value = "activo", required = false) Boolean estado,
      @RequestParam(value = "fechaNacimiento", required = false) String fechaNacimiento,
      @RequestParam(value = "numeroDocumento", required = false) String docId,
      @RequestParam(value = "telefono", required = false) String telefono,
      @RequestParam(value = "contactado", required = false) Boolean contactado,
      @RequestParam(value = "orderField", required = false) String orderField,
      @RequestParam(value = "orderDirection", required = false) String orderDirection,
      @RequestParam("page") Integer page,
      @RequestParam("size") Integer size)
      throws NotFoundException {

    return intervinienteUseCase.getAllFilteredIntervinientes(
        idContrato,
        id,
        nombre,
        tipoIntervencion,
        ordenIntervencion,
        estado,
        fechaNacimiento,
        docId,
        telefono,
        contactado,
        orderField,
        orderDirection,
        size,
        page);
  }

  @ApiOperation(
      value = "Crea interviniente",
      notes = "Añade un nuevo interviniente a base de datos al contrato con el id enviado")
  @PostMapping
  public IntervinienteDTO createInterviniente(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idContrato") Integer idContrato,
      @RequestBody IntervinienteInputDTO intervinienteDTO)
      throws Exception {
    return intervinienteUseCase.createInterviniente(idContrato, intervinienteDTO);
  }

  @ApiOperation(
      value = "Actualiza interviniente",
      notes = "Actualiza el interviniente del id enviado")
  @PutMapping("{id}")
  public IntervinienteDTO updateIntervinienteById(
      @PathVariable("id") Integer id,
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idContrato") Integer idContrato,
      @RequestBody @Valid IntervinienteInputDTO intervinienteDTO,
      @RequestParam("idTipoIntervencion") Integer idTipoIntervencion)
      throws Exception {
    return intervinienteUseCase.updateIntervininte(id, intervinienteDTO, idContrato, idTipoIntervencion);
  }

  @ApiOperation(value = "Elimina interviniente", notes = "Devuelve el interviniente eliminado")
  @DeleteMapping("{id}")
  public void deleteIntervinienteById(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("id") Integer idInterviniente,
      @PathVariable("idContrato") Integer idContrato)
      throws Exception {
    intervinienteUseCase.delete(idInterviniente, idContrato);
  }

  @ApiOperation(
      value = "Obtener documentación de los intervinientes de un contrato",
      notes =
          "Devuelve la documentación disponible de los intervinientes del contrato con id enviado")
  @GetMapping("/documentos")
  @Transactional
  public ListWithCountDTO<DocumentoDTO> getDocumentosByExpediente(
      @PathVariable("idContrato") Integer idContrato,
      @PathVariable("idExpediente") Integer idExpediente,
      @RequestParam(value="id",required = false) String idDocumento,
      @RequestParam(value = "nombreArchivo", required = false) String nombreArchivo,
      @RequestParam(value = "idHaya", required = false) String idHaya,
      @RequestParam(value="usuarioCreador",required = false) String usuarioCreador,
      @RequestParam(value="tipo",required = false) String tipo,
      @RequestParam(value = "idEntidad", required = false) String idEntidad,
      @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
      @RequestParam(value = "orderField", required = false) String orderField,
      @RequestParam(value = "orderDirection", required = false) String orderDirection,
      @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page)
      throws NotFoundException, IOException {
    if (idContrato == -1) {
      return intervinienteUseCase.findDocumentosByExpedienteId(idExpediente,idDocumento,usuarioCreador,tipo,idEntidad,fechaActualizacion,nombreArchivo,idHaya,orderDirection,orderField,size,page);
    } else {
      return intervinienteUseCase.findDocumentosIntervinientesByContrato(idContrato,idDocumento,usuarioCreador,tipo,idEntidad,fechaActualizacion, nombreArchivo, idHaya, orderDirection,orderField,size,page);
    }
  }

  @ApiOperation(
    value = "Obtener documentación de un interviniente",
    notes =
      "Devuelve la documentación disponible del interviniente con id enviado")
  @GetMapping("/{idInterviniente}/documentosInterviniente")
  @Transactional
  public ListWithCountDTO<DocumentoDTO> getDocumentosByInterviniente(
    @PathVariable("idInterviniente") Integer idInterviniente,
    @RequestParam(value="id",required = false) String idDocumento,
    @RequestParam(value = "nombreArchivo", required = false) String nombreArchivo,
    @RequestParam(value = "idHaya", required = false) String idHaya,
    @RequestParam(value="usuarioCreador",required = false) String usuarioCreador,
    @RequestParam(value="tipo",required = false) String tipo,
    @RequestParam(value = "idEntidad", required = false) String idEntidad,
    @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page)
    throws NotFoundException, IOException {
   return intervinienteUseCase.findDocumentosByIntervinienteId(idInterviniente,idDocumento,usuarioCreador,tipo,idEntidad,fechaActualizacion, nombreArchivo, idHaya, orderDirection,orderField,size,page);
  }

  @ApiOperation(value = "Añadir un documento a un interviniente")
  @PostMapping("/{idInterviniente}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoInterviniente(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idContrato") Integer idContrato,
      @PathVariable("idInterviniente") Integer idInterviniente,
      @RequestPart("file") @NotNull @NotBlank MultipartFile file,
      @RequestPart("metadatos") MetadatoDocumentoInputDTO metadatoDocumento,
      @RequestParam(value = "tipoDocumento", required = false) String tipoDocumento,
      @ApiIgnore CustomUserDetails principal)
      throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    intervinienteUseCase.createDocumentoInterviniente(idInterviniente, idExpediente, file, metadatoDocumento,usuario,tipoDocumento);
  }

  @ApiOperation(
      value = "Descargar información de interviniente en Excel",
      notes = "Descargar información en Excel de los intervinientes por id de contrato")
  @GetMapping("/excel")
  public ResponseEntity<InputStreamResource> getExcelIntervinienteById(
      @PathVariable Integer idContrato) throws Exception {

    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel =
        excelExport.create(
            this.intervinienteUseCase.findExcelIntervinienteByIdContrato(idContrato));

    return excelExport.download(
        excel,
        idContrato + "_intervinientes_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(value = "Devuelve los intervinientes asociados a un listado de contratos")
  @GetMapping("/all-by-contratos")
  @ResponseStatus(HttpStatus.OK)
  public List<IntervinienteDTOToList> getAllIntervinientesByContratos(
      @RequestParam(value = "contratosIds", required = true) List<Integer> contratosIds) {
    List<IntervinienteDTOToList> intervinientes =
        intervinienteUseCase.getAllByContratosIds(contratosIds);
    return intervinientes;
  }

  @GetMapping("/geo")
  public void actualizarGeo() throws NotFoundException {
    intervinienteUtil.asignarGoogle();
  }
}
