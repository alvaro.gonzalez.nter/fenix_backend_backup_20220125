package com.haya.alaska.interviniente.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class IntervinienteCarteraExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();


  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Borrower Id");
      cabeceras.add("Loan Id");
      cabeceras.add("Origin Id");
      cabeceras.add("Borrower type");
      cabeceras.add("Status");
      cabeceras.add("Intervention order");
      cabeceras.add("Document type");
      cabeceras.add("Document number");
      cabeceras.add("Name");
      cabeceras.add("Surnames");
      cabeceras.add("Gender");
      cabeceras.add("Marital status");
      cabeceras.add("Resident");
      cabeceras.add("Date of birth");
      cabeceras.add("Social reason");
      cabeceras.add("Incorporation date");
      cabeceras.add("Residence");
      cabeceras.add("Nationality");
      cabeceras.add("Country");
      cabeceras.add("Occupation type");
      cabeceras.add("Company");
      cabeceras.add("Company Phone");
      cabeceras.add("Company contact");
      cabeceras.add("Loan type");
      cabeceras.add("Professional Partner Code");
      cabeceras.add("Cnae");
      cabeceras.add("Monthly net income");
      cabeceras.add("Activity description");
      cabeceras.add("Num dependent children");
      cabeceras.add("Other risks");
      cabeceras.add("Vulnerability");
      cabeceras.add("Pensioner");
      cabeceras.add("Type benefit");
      cabeceras.add("Dependent");
      cabeceras.add("Other vulnerability situations");
      cabeceras.add("Disabled");

    } else {

      cabeceras.add("Id interviniente");
      cabeceras.add("Id contrato");
      cabeceras.add("Id origen");
      cabeceras.add("Tipo interviniente");
      cabeceras.add("Estado");
      cabeceras.add("Orden intervencion");
      cabeceras.add("Tipo documento");
      cabeceras.add("Numero Documento");
      cabeceras.add("Nombre");
      cabeceras.add("Apellidos");
      cabeceras.add("Sexo");
      cabeceras.add("Estado Civil");
      cabeceras.add("Residente");
      cabeceras.add("Fecha Nacimiento");
      cabeceras.add("Razon Social");
      cabeceras.add("Fecha Constitucion");
      cabeceras.add("Residencia");
      cabeceras.add("Nacionalidad");
      cabeceras.add("Pais");
      cabeceras.add("Tipo Ocupacion");
      cabeceras.add("Empresa");
      cabeceras.add("Telefono Empresa");
      cabeceras.add("Contacto Empresa");
      cabeceras.add("Tipo Contrato");
      cabeceras.add("Codigo Socio Profesional");
      cabeceras.add("Cnae");
      cabeceras.add("Ingresos Netos Mensuales");
      cabeceras.add("Descripcion Actividad");
      cabeceras.add("Num Hijos Dependientes");
      cabeceras.add("Otros Riesgos");
      cabeceras.add("Vulnerabilidad");
      cabeceras.add("Pensionista");
      cabeceras.add("Tipo Prestacion");
      cabeceras.add("Dependiente");
      cabeceras.add("Otras Situaciones Vulnerabilidad");
      cabeceras.add("Discapacitados");
    }
  }


  public IntervinienteCarteraExcel(Interviniente interviniente, String idContrato, Integer orden, TipoIntervencion tipoIntervencion) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Borrower Id", interviniente.getId());
      this.add("Loan Id", idContrato);
      this.add("Origin Id", interviniente.getIdCarga()!=null ? interviniente.getIdCarga():null);
      this.add("Borrower type", tipoIntervencion!=null ? tipoIntervencion.getValorIngles():null);
      this.add("Status", Boolean.TRUE.equals(interviniente.getActivo()) ? "ACTIVE":"NOT ACTIVE");
      this.add("Intervention order", orden);
      this.add("Document type", interviniente.getTipoDocumento()!=null ? interviniente.getTipoDocumento().getValorIngles():null);
      this.add("Document number", interviniente.getNumeroDocumento()!=null ?interviniente.getNumeroDocumento():null);
      this.add("Name", interviniente.getNombre() !=null ? interviniente.getNombre():null);
      this.add("Surnames", interviniente.getApellidos()!=null ? interviniente.getApellidos():null);
      this.add("Gender", interviniente.getSexo()!=null ? interviniente.getSexo().getValorIngles():null);
      this.add("Marital status", interviniente.getEstadoCivil()!=null ? interviniente.getEstadoCivil().getValorIngles():null);
      this.add("Resident", interviniente.getResidencia()!=null ? interviniente.getResidencia().getValorIngles():null);
      this.add("Date of birth", interviniente.getFechaNacimiento()!=null ? interviniente.getFechaNacimiento():null);
      this.add("Social reason", interviniente.getRazonSocial()!=null ? interviniente.getRazonSocial():null);
      this.add("Incorporation date", interviniente.getFechaConstitucion()!=null ? interviniente.getFechaConstitucion():null);
      this.add("Residence", interviniente.getResidencia()!=null ? interviniente.getResidencia():null);
      this.add("Nationality", interviniente.getNacionalidad()!=null ? interviniente.getNacionalidad().getValorIngles():null);
      this.add("Country", interviniente.getPaisNacimiento()!=null ? interviniente.getPaisNacimiento().getValorIngles():null);
      this.add("Occupation type", interviniente.getTipoOcupacion()!=null ? interviniente.getTipoOcupacion().getValorIngles():null);
      this.add("Company", interviniente.getEmpresa()!=null ? interviniente.getEmpresa():null);
      this.add("Company Phone", interviniente.getTelefonoEmpresa()!=null ? interviniente.getTelefonoEmpresa():null);
      this.add("Company contact", interviniente.getContactoEmpresa()!=null ? interviniente.getContactoEmpresa():null);
      this.add("Loan type", interviniente.getTipoContrato()!=null ? interviniente.getTipoContrato().getValorIngles():null);
      this.add("Professional Partner Code", interviniente.getCodigoSocioProfesional()!=null ? interviniente.getCodigoSocioProfesional():null);
      this.add("Cnae", interviniente.getCnae()!=null ?  interviniente.getCnae():null);
      this.add("Monthly net income", interviniente.getIngresosNetosMensuales()!=null ? interviniente.getIngresosNetosMensuales():null);
      this.add("Activity description", interviniente.getDescripcionActividad()!=null ? interviniente.getDescripcionActividad():null);
      this.add("Num dependent children", interviniente.getNumHijosDependientes()!=null ? interviniente.getNumHijosDependientes():null);
      this.add("Other risks", Boolean.TRUE.equals(interviniente.getOtrosRiesgos()) ? "YES":"NO");
      this.add("Vulnerability", Boolean.TRUE.equals(interviniente.getVulnerabilidad()) ? "YES":"NO");
      this.add("Pensioner", Boolean.TRUE.equals(interviniente.getPensionista()) ? "YES":"NO");
      this.add("Type benefit", interviniente.getTipoPrestacion()!=null? interviniente.getTipoPrestacion().getValorIngles():null);
      this.add("Dependent", Boolean.TRUE.equals(interviniente.getDependiente()) ? "YES":"NO");
      this.add("Other vulnerability situations", interviniente.getOtrasSituacionesVulnerabilidad()!=null ? interviniente.getOtrasSituacionesVulnerabilidad().getValorIngles():null);
      this.add("Disabled", Boolean.TRUE.equals(interviniente.getDiscapacitados()) ? "YES":"NO");

    }

    else{

    this.add("Id Interviniente", interviniente.getId());
    this.add("Id Contrato", idContrato);
    this.add("Id Origen", interviniente.getIdCarga()!=null ? interviniente.getIdCarga():null);
    this.add("Tipo Intervencion", tipoIntervencion!=null ? tipoIntervencion.getValor():null);
    this.add("Estado", Boolean.TRUE.equals(interviniente.getActivo()) ? "ACTIVO":"NO ACTIVO");
    this.add("Orden Intervencion", orden);
    this.add("Tipo Documento", interviniente.getTipoDocumento()!=null ? interviniente.getTipoDocumento().getValor():null);
    this.add("Numero Documento", interviniente.getNumeroDocumento()!=null ?interviniente.getNumeroDocumento():null);
    this.add("Nombre", interviniente.getNombre() !=null ? interviniente.getNombre():null);
    this.add("Apellidos", interviniente.getApellidos()!=null ? interviniente.getApellidos():null);
    this.add("Sexo", interviniente.getSexo()!=null ? interviniente.getSexo().getValor():null);
    this.add("Estado Civil", interviniente.getEstadoCivil()!=null ? interviniente.getEstadoCivil().getValor():null);
    this.add("Residente", interviniente.getResidencia()!=null ? interviniente.getResidencia().getValor():null);
    this.add("Fecha Nacimiento", interviniente.getFechaNacimiento()!=null ? interviniente.getFechaNacimiento():null);
    this.add("Razon Social", interviniente.getRazonSocial()!=null ? interviniente.getRazonSocial():null);
    this.add("Fecha Constitucion", interviniente.getFechaConstitucion()!=null ? interviniente.getFechaConstitucion():null);
    this.add("Residencia", interviniente.getResidencia()!=null ? interviniente.getResidencia():null);
    this.add("Nacionalidad", interviniente.getNacionalidad()!=null ? interviniente.getNacionalidad().getValor():null);
    this.add("Pais", interviniente.getPaisNacimiento()!=null ? interviniente.getPaisNacimiento().getValor():null);
    this.add("Tipo Ocupacion", interviniente.getTipoOcupacion()!=null ? interviniente.getTipoOcupacion().getValor():null);
    this.add("Empresa", interviniente.getEmpresa()!=null ? interviniente.getEmpresa():null);
    this.add("Telefono Empresa", interviniente.getTelefonoEmpresa()!=null ? interviniente.getTelefonoEmpresa():null);
    this.add("Contacto Empresa", interviniente.getContactoEmpresa()!=null ? interviniente.getContactoEmpresa():null);
    this.add("Tipo Contrato", interviniente.getTipoContrato()!=null ? interviniente.getTipoContrato().getValor():null);
    this.add("Codigo Socio Profesional", interviniente.getCodigoSocioProfesional()!=null ? interviniente.getCodigoSocioProfesional():null);
    this.add("Cnae", interviniente.getCnae()!=null ?  interviniente.getCnae():null);
    this.add("Ingresos Netos Mensuales", interviniente.getIngresosNetosMensuales()!=null ? interviniente.getIngresosNetosMensuales():null);
    this.add("Descripcion Actividad", interviniente.getDescripcionActividad()!=null ? interviniente.getDescripcionActividad():null);
    this.add("Num Hijos Dependientes", interviniente.getNumHijosDependientes()!=null ? interviniente.getNumHijosDependientes():null);
    this.add("Otros Riesgos", Boolean.TRUE.equals(interviniente.getOtrosRiesgos()) ? "SÍ":"NO");
    this.add("Vulnerabilidad", Boolean.TRUE.equals(interviniente.getVulnerabilidad()) ? "SÍ":"NO");
    this.add("Pensionista", Boolean.TRUE.equals(interviniente.getPensionista()) ? "SÍ":"NO");
    this.add("Tipo Prestacion", interviniente.getTipoPrestacion()!=null? interviniente.getTipoPrestacion().getValor():null);
    this.add("Dependiente", Boolean.TRUE.equals(interviniente.getDependiente()) ? "SÍ":"NO");
    this.add("Otras Situaciones Vulnerabilidad", interviniente.getOtrasSituacionesVulnerabilidad()!=null ? interviniente.getOtrasSituacionesVulnerabilidad().getValor():null);
    this.add("Discapacitados", Boolean.TRUE.equals(interviniente.getDiscapacitados()) ? "SÍ":"NO");
    }
  }



  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
