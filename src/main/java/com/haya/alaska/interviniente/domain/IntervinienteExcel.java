package com.haya.alaska.interviniente.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class IntervinienteExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Borrower Id");
      cabeceras.add("Loan Id");
      cabeceras.add("Origin Id");
      cabeceras.add("Intervention type");
      cabeceras.add("Status");
      cabeceras.add("Intervention order");
      cabeceras.add("Document type");
      cabeceras.add("Document number");
      cabeceras.add("Name");
      cabeceras.add("Surnames");
      cabeceras.add("Gender");
      cabeceras.add("Marital status");
      cabeceras.add("Date of birth");
      cabeceras.add("Residence");
      cabeceras.add("Nationality");
      cabeceras.add("Country of birth");
      cabeceras.add("Occupation type");
      cabeceras.add("Company");
      cabeceras.add("Company Phone");
      cabeceras.add("Company contact");
      cabeceras.add("Loan type");
      cabeceras.add("Professional Partner Code");
      cabeceras.add("Cnae");
      cabeceras.add("Monthly net income");
      cabeceras.add("Activity description");
      cabeceras.add("Num dependent children");
      cabeceras.add("Other risks");
      cabeceras.add("Vulnerability");
      cabeceras.add("Pensioner");
      cabeceras.add("Type benefit");
      cabeceras.add("Dependent");
      cabeceras.add("Other vulnerability situations");
      cabeceras.add("Disabled");
      cabeceras.add("Additional data");
      cabeceras.add("Additional data 2");
      cabeceras.add("Type person");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Interviniente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Origen");
      cabeceras.add("Tipo Intervención");
      cabeceras.add("Estado");
      cabeceras.add("Orden Intervención");
      cabeceras.add("Tipo Documento");
      cabeceras.add("Numero Documento");
      cabeceras.add("Nombre");
      cabeceras.add("Apellidos");
      cabeceras.add("Sexo");
      cabeceras.add("Estado Civil");
      cabeceras.add("Fecha Nacimiento");
      cabeceras.add("Residencia");
      cabeceras.add("Nacionalidad");
      cabeceras.add("Pais Nacimiento");
      cabeceras.add("Tipo Ocupacion");
      cabeceras.add("Empresa");
      cabeceras.add("Telefono Empresa");
      cabeceras.add("Contacto Empresa");
      cabeceras.add("Tipo Contrato");
      cabeceras.add("Codigo Socio Profesional");
      cabeceras.add("Cnae");
      cabeceras.add("Ingresos Netos Mensuales");
      cabeceras.add("Descripcion Actividad");
      cabeceras.add("Numero Hijos Dependientes");
      cabeceras.add("Otros Riesgos");
      cabeceras.add("Vulnerabilidad");
      cabeceras.add("Pensionista");
      cabeceras.add("Tipo Prestacion");
      cabeceras.add("Dependiente");
      cabeceras.add("Otras Situaciones Vulnerabilidad");
      cabeceras.add("Discapacitados");
      cabeceras.add("Datos adicionales");
      cabeceras.add("Datos adicionales 2");
      cabeceras.add("Tipo persona");
    }
  }

  public IntervinienteExcel(
      Interviniente interviniente,
      String idContrato,
      Integer orden,
      TipoIntervencion tipoIntervencion, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Borrower Id", interviniente.getId());
      this.add("Loan Id", idContrato);
      this.add("Origin Id", interviniente.getIdCarga());
      this.add("Intervention type", tipoIntervencion != null ? tipoIntervencion.getValorIngles() : null);
      this.add("Status", Boolean.TRUE.equals(interviniente.getActivo()) ? "ACTIVE" : "NOT ACTIVE");
      this.add("Intervention order", orden);
      this.add(
        "Document type",
        interviniente.getTipoDocumento() != null
          ? interviniente.getTipoDocumento().getValorIngles()
          : null);
      this.add("Document number", interviniente.getNumeroDocumento());
      if (interviniente.getPersonaJuridica() != null && interviniente.getPersonaJuridica())
        this.add(
          "Name",
          interviniente.getRazonSocial() != null ? interviniente.getRazonSocial() : null);
      else this.add("Name", interviniente.getNombre() != null ? interviniente.getNombre() : null);
      this.add(
        "Surnames", interviniente.getApellidos() != null ? interviniente.getApellidos() : null);
      this.add(
        "Nationality",
        interviniente.getNacionalidad() != null
          ? interviniente.getNacionalidad().getValorIngles()
          : null);
      this.add(
        "Occupation type",
        interviniente.getTipoOcupacion() != null
          ? interviniente.getTipoOcupacion().getValorIngles()
          : null);
      this.add("Company", interviniente.getEmpresa());
      this.add("Company Phone", interviniente.getTelefonoEmpresa());
      this.add("Company contact", interviniente.getContactoEmpresa());
      this.add(
        "Loan type",
        interviniente.getTipoContrato() != null
          ? interviniente.getTipoContrato().getValorIngles()
          : null);
      this.add("Professional Partner Code", interviniente.getCodigoSocioProfesional());
      this.add("Cnae", interviniente.getCnae());
      this.add("Monthly net income", interviniente.getIngresosNetosMensuales());
      this.add("Activity description", interviniente.getDescripcionActividad());
      this.add("Num dependent children", interviniente.getNumHijosDependientes());
      this.add("Other risks", interviniente.getOtrosRiesgos());
      this.add(
        "Vulnerability", Boolean.TRUE.equals(interviniente.getVulnerabilidad()) ? "YES" : "NO");
      this.add("Pensioner", Boolean.TRUE.equals(interviniente.getPensionista()) ? "YES" : "NO");
      this.add(
        "Type benefit",
        interviniente.getTipoPrestacion() != null
          ? interviniente.getTipoPrestacion().getValorIngles()
          : null);
      this.add("Dependent", Boolean.TRUE.equals(interviniente.getDependiente()) ? "YES" : "NO");
      this.add(
        "Other vulnerability situations",
        interviniente.getOtrasSituacionesVulnerabilidad() != null
          ? interviniente.getOtrasSituacionesVulnerabilidad().getValorIngles()
          : null);
      this.add(
        "Disabled", Boolean.TRUE.equals(interviniente.getDiscapacitados()) ? "YES" : "NO");
      this.add("Additional data", interviniente.getNotas1());
      this.add("Additional data 2", interviniente.getNotas2());

      if (interviniente.getPersonaJuridica() == true) {
        this.add("Type person", "PJ");
      } else {
        this.add("Type person", "PF");
      }

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Interviniente", interviniente.getId());
      this.add("Id Contrato", idContrato);
      this.add("Id Origen", interviniente.getIdCarga());
      this.add("Tipo Intervención", tipoIntervencion != null ? tipoIntervencion.getValor() : null);
      this.add("Estado", Boolean.TRUE.equals(interviniente.getActivo()) ? "ACTIVO" : "DESACTIVADO");
      this.add("Orden Intervención", orden);
      this.add(
          "Tipo Documento",
          interviniente.getTipoDocumento() != null
              ? interviniente.getTipoDocumento().getValor()
              : null);
      this.add("Numero Documento", interviniente.getNumeroDocumento());
      if (interviniente.getPersonaJuridica() != null && interviniente.getPersonaJuridica())
        this.add(
            "Nombre",
            interviniente.getRazonSocial() != null ? interviniente.getRazonSocial() : null);
      else this.add("Nombre", interviniente.getNombre() != null ? interviniente.getNombre() : null);
      this.add(
          "Apellidos", interviniente.getApellidos() != null ? interviniente.getApellidos() : null);
      this.add(
          "Nacionalidad",
          interviniente.getNacionalidad() != null
              ? interviniente.getNacionalidad().getValor()
              : null);
      this.add(
          "Tipo Ocupacion",
          interviniente.getTipoOcupacion() != null
              ? interviniente.getTipoOcupacion().getValor()
              : null);
      this.add("Empresa", interviniente.getEmpresa());
      this.add("Telefono Empresa", interviniente.getTelefonoEmpresa());
      this.add("Contacto Empresa", interviniente.getContactoEmpresa());
      this.add(
          "Tipo Contrato",
          interviniente.getTipoContrato() != null
              ? interviniente.getTipoContrato().getValor()
              : null);
      this.add("Codigo Socio Profesional", interviniente.getCodigoSocioProfesional());
      this.add("Cnae", interviniente.getCnae());
      this.add("Ingresos Netos Mensuales", interviniente.getIngresosNetosMensuales());
      this.add("Descripcion Actividad", interviniente.getDescripcionActividad());
      this.add("Numero Hijos Dependientes", interviniente.getNumHijosDependientes());
      this.add("Otros Riesgos", interviniente.getOtrosRiesgos());
      this.add(
          "Vulnerabilidad", Boolean.TRUE.equals(interviniente.getVulnerabilidad()) ? "SI" : "NO");
      this.add("Pensionista", Boolean.TRUE.equals(interviniente.getPensionista()) ? "SI" : "NO");
      this.add(
          "Tipo Prestacion",
          interviniente.getTipoPrestacion() != null
              ? interviniente.getTipoPrestacion().getValor()
              : null);
      this.add("Dependiente", Boolean.TRUE.equals(interviniente.getDependiente()) ? "SI" : "NO");
      this.add(
          "Otras Situaciones Vulnerabilidad",
          interviniente.getOtrasSituacionesVulnerabilidad() != null
              ? interviniente.getOtrasSituacionesVulnerabilidad().getValor()
              : null);
      this.add(
          "Discapacitados", Boolean.TRUE.equals(interviniente.getDiscapacitados()) ? "SI" : "NO");
      this.add("Datos adicionales", interviniente.getNotas1());
      this.add("Datos adicionales 2", interviniente.getNotas2());

      if (interviniente.getPersonaJuridica() == true) {
        this.add("Tipo persona", "PJ");
      } else {
        this.add("Tipo persona", "PF");
      }
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
