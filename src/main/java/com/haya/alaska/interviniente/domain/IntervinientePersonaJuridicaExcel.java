package com.haya.alaska.interviniente.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class IntervinientePersonaJuridicaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      cabeceras.add("Borrower Id");
      cabeceras.add("Loan Id");
      cabeceras.add("Origin Id");
      cabeceras.add("Intervention type");
      cabeceras.add("Status");
      cabeceras.add("Intervention order");
      cabeceras.add("Document type");
      cabeceras.add("Document Number");
      cabeceras.add("Name");
      cabeceras.add("Social reason");
      cabeceras.add("Constitution date");
      cabeceras.add("Nationality");
      cabeceras.add("Occupation type");
      cabeceras.add("Company");
      cabeceras.add("Company Phone");
      cabeceras.add("Company contact");
      cabeceras.add("Contract type");
      cabeceras.add("Professional Partner Code");
      cabeceras.add("Cnae");
      cabeceras.add("Monthly net income");
      cabeceras.add("Activity description");
      cabeceras.add("Num dependent children");
      cabeceras.add("Other risks");
      cabeceras.add("Vulnerability");
      cabeceras.add("Pensioner");
      cabeceras.add("Type benefit");
      cabeceras.add("Dependent");
      cabeceras.add("Other vulnerability situations");
      cabeceras.add("Disabled");
      cabeceras.add("Additional data");
      cabeceras.add("Additional data 2");

    } else {
      cabeceras.add("Id Interviniente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Origen");
      cabeceras.add("Tipo Intervencion");
      cabeceras.add("Estado");
      cabeceras.add("Orden Intervencion");
      cabeceras.add("Tipo Documento");
      cabeceras.add("Numero Documento");
      cabeceras.add("Nombre");
      cabeceras.add("Razón Social");
      cabeceras.add("Fecha Constitucion");
      cabeceras.add("Nacionalidad");
      cabeceras.add("Tipo Ocupacion");
      cabeceras.add("Empresa");
      cabeceras.add("Telefono Empresa");
      cabeceras.add("Contacto Empresa");
      cabeceras.add("Tipo Contrato");
      cabeceras.add("Codigo Socio Profesional");
      cabeceras.add("Cnae");
      cabeceras.add("Ingresos Netos Mensuales");
      cabeceras.add("Descripcion Actividad");
      cabeceras.add("Numero Hijos Dependientes");
      cabeceras.add("Otros Riesgos");
      cabeceras.add("Vulnerabilidad");
      cabeceras.add("Pensionista");
      cabeceras.add("Tipo Prestacion");
      cabeceras.add("Dependiente");
      cabeceras.add("Otras Situaciones Vulnerabilidad");
      cabeceras.add("Discapacitados");
      cabeceras.add("Notas1");
      cabeceras.add("Notas2");
    }
  }

  public IntervinientePersonaJuridicaExcel(Interviniente interviniente, Integer idContrato, Integer orden, TipoIntervencion tipoIntervencion) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Borrower Id", interviniente.getId());
      this.add("Loan Id", idContrato);
      this.add("Origin Id", interviniente.getIdCarga());
      this.add("Intervention type", tipoIntervencion);
      this.add("Status", interviniente.getActivo());
      this.add("Intervention order", orden);
      this.add("Document type", interviniente.getTipoDocumento());
      this.add("Document Number", interviniente.getNumeroDocumento());
      this.add("Name", interviniente.getNombre());
      this.add("Social reason", interviniente.getRazonSocial());
      this.add("Constitution date", interviniente.getFechaConstitucion());
      this.add("Nationality", interviniente.getNacionalidad());
      this.add("Occupation type", interviniente.getTipoOcupacion());
      this.add("Company", interviniente.getEmpresa());
      this.add("Company Phone", interviniente.getTelefonoEmpresa());
      this.add("Company contact", interviniente.getContactoEmpresa());
      this.add("Contract type", interviniente.getTipoContrato());
      this.add("Professional Partner Code", interviniente.getCodigoSocioProfesional());
      this.add("Cnae", interviniente.getCnae());
      this.add("Monthly net income", interviniente.getIngresosNetosMensuales());
      this.add("Activity description", interviniente.getDescripcionActividad());
      this.add("Num dependent children", interviniente.getNumHijosDependientes());
      this.add("Other risks", interviniente.getOtrosRiesgos());
      this.add("Vulnerability", interviniente.getVulnerabilidad());
      this.add("Pensioner", interviniente.getPensionista());
      this.add("Type benefit", interviniente.getTipoPrestacion());
      this.add("Dependent", interviniente.getDependiente());
      this.add("Other vulnerability situations", interviniente.getOtrasSituacionesVulnerabilidad());
      this.add("Disabled", interviniente.getDiscapacitados());
      this.add("Additional data", interviniente.getNotas1());
      this.add("Additional data 2", interviniente.getNotas2());


    }

    else{

    this.add("Id Interviniente", interviniente.getId());
    this.add("Id Contrato", idContrato);
    this.add("Id Origen", interviniente.getIdCarga());
    this.add("Tipo Intervencion", tipoIntervencion);
    this.add("Estado", interviniente.getActivo());
    this.add("Orden Intervencion", orden);
    this.add("Tipo Documento", interviniente.getTipoDocumento());
    this.add("Numero Documento", interviniente.getNumeroDocumento());
    this.add("Nombre", interviniente.getNombre());
    this.add("Razón Social", interviniente.getRazonSocial());
    this.add("Fecha Constitucion", interviniente.getFechaConstitucion());
    this.add("Nacionalidad", interviniente.getNacionalidad());
    this.add("Tipo Ocupacion", interviniente.getTipoOcupacion());
    this.add("Empresa", interviniente.getEmpresa());
    this.add("Telefono Empresa", interviniente.getTelefonoEmpresa());
    this.add("Contacto Empresa", interviniente.getContactoEmpresa());
    this.add("Tipo Contrato", interviniente.getTipoContrato());
    this.add("Codigo Socio Profesional", interviniente.getCodigoSocioProfesional());
    this.add("Cnae", interviniente.getCnae());
    this.add("Ingresos Netos Mensuales", interviniente.getIngresosNetosMensuales());
    this.add("Descripcion Actividad", interviniente.getDescripcionActividad());
    this.add("Numero Hijos Dependientes", interviniente.getNumHijosDependientes());
    this.add("Otros Riesgos", interviniente.getOtrosRiesgos());
    this.add("Vulnerabilidad", interviniente.getVulnerabilidad());
    this.add("Pensionista", interviniente.getPensionista());
    this.add("Tipo Prestacion", interviniente.getTipoPrestacion());
    this.add("Dependiente", interviniente.getDependiente());
    this.add("Otras Situaciones Vulnerabilidad", interviniente.getOtrasSituacionesVulnerabilidad());
    this.add("Discapacitados", interviniente.getDiscapacitados());
    this.add("Notas1", interviniente.getNotas1());
    this.add("Notas2", interviniente.getNotas2());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
