package com.haya.alaska.interviniente.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.estado_civil.domain.EstadoCivil;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.sexo.domain.Sexo;
import com.haya.alaska.tipo_contrato.domain.TipoContrato;
import com.haya.alaska.tipo_documento.domain.TipoDocumento;
import com.haya.alaska.tipo_ocupacion.domain.TipoOcupacion;
import com.haya.alaska.tipo_prestacion.domain.TipoPrestacion;
import com.haya.alaska.vulnerabilidad.domain.Vulnerabilidad;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Audited
@AuditTable(value = "HIST_MSTR_INTERVINIENTE")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_INTERVINIENTE")
public class Interviniente implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;
  @Column(name = "DES_ID_DATA_TYPE")
  private String idDataType;
  @Column(name = "DES_ID_ORIGEN")
  private String idOrigen;
  @Column(name = "DES_ID_ORIGEN_2")
  private String idOrigen2;
  @Column(name = "DES_ID_CARGA")
  private String idCarga;
  @Column(name = "IND_PERSONA_JURIDICA")
  private Boolean personaJuridica;
  @Column(name = "DES_NUM_DOCUMENTO")
  private String numeroDocumento;
  @Column(name = "DES_NOMBRE")
  private String nombre;
  @Column(name = "DES_APELLIDOS")
  private String apellidos;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEXO")
  private Sexo sexo;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_CIVIL")
  private EstadoCivil estadoCivil;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_NACIMIENTO")
  private Date fechaNacimiento;
  @Column(name = "DES_RAZON_SOCIAL")
  private String razonSocial;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CONSTITUCION")
  private Date fechaConstitucion;
  @Column(name = "DES_EMPRESA")
  private String empresa;
  @Column(name = "DES_TELEFONO_EMPRESA")
  private String telefonoEmpresa;
  @Column(name = "DES_CONTACTO_EMPRESA")
  private String contactoEmpresa;
  @Column(name = "DES_CNAE")
  private String cnae;
  @Column(name = "NUM_INGRESOS_NETOS_MENSUALES")
  private Double ingresosNetosMensuales;
  @Column(name = "DES_DESC_ACTIVIDAD")
  private String descripcionActividad;
  @Column(name = "NUM_HIJOS_DEPENDIENTES")
  private Integer numHijosDependientes;
  @Column(name = "IND_OTROS_RIESGOS")
  private Boolean otrosRiesgos;
  @Column(name = "IND_VULNERABILIDAD")
  private Boolean vulnerabilidad;
  @Column(name = "IND_PENSIONISTA")
  private Boolean pensionista;
  @Column(name = "IND_DEPENDIENTE")
  private Boolean dependiente;
  @Column(name = "IND_DISCAPACITADOS")
  private Boolean discapacitados;
  @Column(name = "DES_PRESTACIONES")
  private String prestaciones;
  @Column(name = "DES_NOTAS_1")
  private String notas1;
  @Column(name = "DES_NOTAS_2")
  private String notas2;
  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;
  @Column(name = "IND_CONTACTADO")
  private Boolean contactado;
  @Column(name = "DES_COD_SOCIO_PROFESIONAL")
  private String codigoSocioProfesional;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_NACIONALIDAD")
  private Pais nacionalidad;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PAIS_NACIMIENTO")
  private Pais paisNacimiento;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESIDENCIA")
  private Pais residencia;

  @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_INTERVINIENTE")
  @NotAudited
  private Set<ContratoInterviniente> contratos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_INTERVINIENTE")
  @NotAudited
  private Set<Direccion> direcciones = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "ID_INTERVINIENTE")
  @NotAudited
  private Set<DatoContacto> datosContacto = new HashSet<>();

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
          name = "RELA_PROCEDIMIENTO_INTERVINIENTE",
          joinColumns = @JoinColumn(name = "ID_DEMANDADO"),
          inverseJoinColumns = @JoinColumn(name = "ID_PROCEDIMIENTO"))
  @AuditJoinTable(name = "HIST_RELA_PROCEDIMIENTO_INTERVINIENTE")
  private Set<Procedimiento> procedimientos = new HashSet<>();

  @ManyToMany
  @JoinTable(name = "RELA_INTERVINIENTE_PROPUESTA",
          joinColumns = { @JoinColumn(name = "ID_INTERVINIENTE") },
          inverseJoinColumns = { @JoinColumn(name = "ID_PROPUESTA") }
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_INTERVINIENTE_PROPUESTA")
  private Set<Propuesta> propuestas;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_DOCUMENTO")
  private TipoDocumento tipoDocumento;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_OCUPACION")
  private TipoOcupacion tipoOcupacion;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_CONTRATO")
  private TipoContrato tipoContrato;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_PRESTACION")
  private TipoPrestacion tipoPrestacion;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_VULNERABILIDAD")
  private Vulnerabilidad otrasSituacionesVulnerabilidad;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CARGA")
  private Date fechaCarga;

  @Column(name = "DES_PORCENTAJE_INTERVENCION")
  private String porcentajeIntervencion;

  public String getTelefonoPrincipal() {
    List<DatoContacto> datosContacto =
            this.datosContacto
                    .stream()
                    .sorted(Comparator.comparing(DatoContacto::getOrden))
                    .collect(Collectors.toList());
    if (datosContacto.size() > 0) {
      if (datosContacto.get(0).getMovil() != null) {
        return datosContacto.get(0).getMovil();
      } /*else if (datosContacto.get(0).getFijo() != null) {
        return datosContacto.get(0).getFijo();
      }*/
    }
    return "";
  }

  public String getFijoPrincipal() {
    List<DatoContacto> datosContacto =
      this.datosContacto
        .stream()
        .sorted(Comparator.comparing(DatoContacto::getOrden))
        .collect(Collectors.toList());
    if (datosContacto.size() > 0) {
       if (datosContacto.get(0).getFijo() != null) {
        return datosContacto.get(0).getFijo();
      }
    }
    return "";

  }

  public String getEmailPrincipal() {
    List<DatoContacto> datosContacto =
      this.datosContacto
        .stream()
        .sorted(Comparator.comparing(DatoContacto::getOrden))
        .collect(Collectors.toList());
    if (datosContacto.size() > 0) {
      if (datosContacto.get(0).getEmail() != null) {
        return datosContacto.get(0).getEmail();
      }
    }
    return "";

  }

  public DatoContacto getDatoPrincipal() {
    List<DatoContacto> datosContacto =
            this.datosContacto
                    .stream()
                    .sorted(Comparator.comparing(DatoContacto::getOrden))
                    .collect(Collectors.toList());
    if (datosContacto.size() > 0) {
      if (datosContacto.get(0).getMovil() != null) {
        return datosContacto.get(0);
      } /*else if (datosContacto.get(0).getFijo() != null) {
        return datosContacto.get(0).getFijo();
      }*/
    }
    return null;
  }

  public String getLocalidad() {
    List<Direccion> direcciones =
      this.direcciones
        .stream()
        .sorted(Comparator.comparing(Direccion::getPrincipal))
        .collect(Collectors.toList());
    if (direcciones.size() > 0) {
      if (direcciones.get(0).getLocalidad() != null) {
        return direcciones.get(0).getLocalidad().getValor();
      }
    }
    return "";
  }

  public void addDatosContactos(List<DatoContacto> datosContactos) {
    datosContactos.forEach(datoContacto -> this.datosContacto.add(datoContacto));
  }

  public void addDireccion(Direccion direccion) {
    this.direcciones.add(direccion);
  }

  public void addDirecciones(List<Direccion> direcciones) {
    this.direcciones.addAll(direcciones);
  }

  public void addDatoContacto(DatoContacto datoContacto) {
    this.datosContacto.add(datoContacto);
  }

  public ContratoInterviniente getContratoInterviniente(Integer idContrato){
    for(ContratoInterviniente ci: this.getContratos()){
      if(ci.getContrato().getId().equals(idContrato)) return ci;
    }
    return null;
  }

  public String getNombreReal(){
    if (this.getPersonaJuridica() == null){
      return null;
    } else if (this.getPersonaJuridica()){
      String nombre = "";
      if (this.getRazonSocial() != null) nombre = this.getRazonSocial();
      return nombre;
    } else {
      String nombre = "";
      if (this.getNombre() != null) nombre += this.getNombre();
      if (this.getApellidos() != null) nombre += " " + this.getApellidos();
      return nombre;
    }
  }
}
