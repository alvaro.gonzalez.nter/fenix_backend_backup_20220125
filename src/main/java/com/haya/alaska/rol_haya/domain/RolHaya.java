package com.haya.alaska.rol_haya.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.usuario.domain.Usuario;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_ROL_HAYA")
@Entity
@Table(name = "LKUP_ROL_HAYA")
public class RolHaya extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ROL_HAYA")
  @NotAudited
  private Set<Usuario> usuarios = new HashSet<>();
}
