package com.haya.alaska.rol_haya.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.rol_haya.domain.RolHaya;

@Repository
public interface RolHayaRepository extends CatalogoRepository<RolHaya, Integer> {}
