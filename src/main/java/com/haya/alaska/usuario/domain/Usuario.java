package com.haya.alaska.usuario.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.haya.alaska.asignacion_usuario_cartera.domain.AsignacionUsuarioCartera;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.rol_haya.domain.RolHaya;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.subtipo_gestor.domain.SubtipoGestor;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Audited
@AuditTable(value = "HIST_MSTR_USUARIO")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_USUARIO")
public class Usuario implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ROL_HAYA")
  private RolHaya rolHaya;

  @Column(name = "DES_NOMBRE")
  private String nombre;

  @Column(name = "DES_EMAIL")
  private String email;

  @Column(name = "DES_TELEFONO")
  private String telefono;

  @Column(name = "NUM_LIMITE_EXPEDIENTES")
  private Integer limiteExpedientes;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERFIL")
  private Perfil perfil;

  @ManyToMany
  @JoinTable(name = "RELA_USUARIO_AREA_USUARIO",
    joinColumns = {@JoinColumn(name = "ID_USUARIO")},
    inverseJoinColumns = {@JoinColumn(name = "ID_AREA_USUARIO")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_USUARIO_AREA_USUARIO")
  private Set<AreaUsuario> areas;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SUBTIPO_GESTOR")
  private SubtipoGestor subtipoGestor;

  @ManyToMany
  @JoinTable(name = "RELA_USUARIO_GRUPO_USUARIO",
    joinColumns = {@JoinColumn(name = "ID_USUARIO")},
    inverseJoinColumns = {@JoinColumn(name = "ID_GRUPO_USUARIO")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_USUARIO_GRUPO_USUARIO")
  private Set<GrupoUsuario> grupoUsuarios = new HashSet<>();

  @ManyToMany
  @JoinTable(name = "RELA_USUARIO_PROVINCIA",
    joinColumns = {@JoinColumn(name = "ID_USUARIO")},
    inverseJoinColumns = {@JoinColumn(name = "ID_PROVINCIA")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_USUARIO_PROVINCIA")
  private Set<Provincia> provincias = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESPONSABLE")
  @NotAudited
  private Set<Cartera> carteras = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  @NotAudited
  private Set<AsignacionExpediente> asignacionExpedientes = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  @NotAudited
  private Set<AsignacionExpedientePosesionNegociada> asignacionExpedientesPN = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO_ASIGNACION")
  @NotAudited
  private Set<AsignacionExpediente> asignacionExpedientesUsuario = new HashSet<>();

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  @NotAudited
  private Set<Propuesta> propuestas = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  @NotAudited
  private Set<BusinessPlanEstrategia> businessPlanEstrategias = new HashSet<>();


  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  @NotAudited
  private Set<SkipTracing> asignacionAnalistaST = new HashSet<>();


  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  @NotAudited
  private Set<SkipTracing> asignacionResponsableCartera = new HashSet<>();

  @Column(name = "ID_USUARIO_CLIENTE")
  private Integer idCliente;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  @NotAudited
  private Set<Segmentacion> segmentacion = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  @NotAudited
  private Set<SegmentacionPN> segmentacionPN = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  @NotAudited
  private Set<AsignacionUsuarioCartera> asignacionesUsuarioCartera = new HashSet<>();


 /* @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO_GESTOR")
  @NotAudited
  private Set<SkipTracing> asignacionGestorST = new HashSet<>();*/

  public Usuario(String email) {
    this.email = email;
  }

  public boolean esAdministrador() {
    return this.rolHaya != null && this.rolHaya.getCodigo().equals("admin");
  }

  public boolean esResponsableFormalizacion() {
    return this.perfil != null && this.perfil.getNombre().equals(Perfil.RESPONSABLE_FORMALIZACION);
  }

  public boolean esWebService() {
    return this.rolHaya != null && this.rolHaya.getCodigo().equals("webservice");
  }

  public boolean contieneExpediente(Integer idExpediente, boolean posesionNegociada) {
    if (this.esAdministrador()) return true;
    if (!posesionNegociada) {
      for (AsignacionExpediente asignacionExpediente : this.asignacionExpedientes) {
        if (asignacionExpediente.getExpediente().getId().equals(idExpediente)) return true;
      }
    } else {
      for (AsignacionExpedientePosesionNegociada asignacionExpedientePosesionNegociada : this.asignacionExpedientesPN) {
        if (asignacionExpedientePosesionNegociada.getExpediente().getId().equals(idExpediente)) return true;
      }
    }
    return false;
  }

}
