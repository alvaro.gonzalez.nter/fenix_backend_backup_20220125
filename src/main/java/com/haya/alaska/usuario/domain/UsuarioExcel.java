package com.haya.alaska.usuario.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.rol_haya.domain.RolHaya;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.subtipo_gestor.domain.SubtipoGestor;
import org.hibernate.envers.AuditJoinTable;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.persistence.*;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

public class UsuarioExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      //cabeceras.add("Id");
      //cabeceras.add("Haya id");
      //cabeceras.add("Haya role");
      cabeceras.add("Email");
      cabeceras.add("Name");
      //cabeceras.add("Phone number");
      //cabeceras.add("Connection max number");
      //cabeceras.add("Profile");
      //cabeceras.add("Manager subtype");

    } else {
      //cabeceras.add("Id");
      //cabeceras.add("Id haya");
      //cabeceras.add("Rol haya");
      cabeceras.add("Email");
      cabeceras.add("Nombre");
      //cabeceras.add("Telefono");
      //cabeceras.add("Limite expedientes");
      //cabeceras.add("Perfil");
      //cabeceras.add("Subtipo gestor");
    }
  }

  public UsuarioExcel(Usuario usuario) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      //this.add("Id", usuario.getId());
      //this.add("Haya id", usuario.getIdHaya());
      //this.add("Haya role", usuario.getRolHaya());
      this.add("Email", usuario.getEmail());
      this.add("Name", usuario.getNombre());
      //this.add("Phone number", usuario.getTelefono());
      //this.add("Connection max number", usuario.getLimiteExpedientes());
      //this.add("Profile", usuario.getPerfil() != null ? usuario.getPerfil().getNombre() : null);
      //this.add("Manager subtype", usuario.getSubtipoGestor().getValor());

    } else {
      //this.add("Id", usuario.getId());
      //this.add("Id haya", usuario.getIdHaya());
      //this.add("Rol haya", usuario.getRolHaya());
      this.add("Email", usuario.getEmail());
      this.add("Nombre", usuario.getNombre());
      //this.add("Telefono", usuario.getTelefono());
      //this.add("Limite expedientes", usuario.getLimiteExpedientes());
      //this.add("Perfil", usuario.getPerfil().getNombre());
      //this.add("Perfil", usuario.getSubtipoGestor() != null ? usuario.getSubtipoGestor().getValor() : null);
    }
  }


  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
