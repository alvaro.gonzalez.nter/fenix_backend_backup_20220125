package com.haya.alaska.usuario.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UsuarioInputDTO implements Serializable {
  String nombre;
  Integer perfil; // Perfil  Rol en el front
  Integer subtipoGestor;
  List<Integer> provincia;
  List<Integer> area;
  List<Integer> grupos;
  Integer numeroExpedientes;
  List<Integer> carteras;
}
