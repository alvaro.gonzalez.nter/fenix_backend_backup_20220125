package com.haya.alaska.usuario.infrastructure.controller.dto.area;

import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class GrupoUsuarioOutputDTO implements Serializable {
  Integer id;
  String nombre;

  public GrupoUsuarioOutputDTO(GrupoUsuario gu){
    BeanUtils.copyProperties(gu, this);
  }
}
