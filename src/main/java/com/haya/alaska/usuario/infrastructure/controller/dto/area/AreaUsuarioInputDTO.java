package com.haya.alaska.usuario.infrastructure.controller.dto.area;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class AreaUsuarioInputDTO implements Serializable {
  String nombre;
  List<GrupoUsuarioInputDTO> grupos;
}
