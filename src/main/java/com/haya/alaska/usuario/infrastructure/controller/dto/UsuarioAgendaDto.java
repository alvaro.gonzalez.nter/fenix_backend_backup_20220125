package com.haya.alaska.usuario.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

@Getter
@NoArgsConstructor
@ToString
public class UsuarioAgendaDto {
    private Integer id;
    private String email;
    private String nombre;
    private String perfil;
    private String perfilMostrar;
    private CatalogoMinInfoDTO rol;

    public UsuarioAgendaDto(Usuario usuario) {
        this.id = usuario.getId();
        this.email = usuario.getEmail();
        this.nombre = usuario.getNombre();
        if (usuario.getPerfil() != null) {
          this.perfil = usuario.getPerfil().getNombre();
          Locale loc = LocaleContextHolder.getLocale();
          String defaultLocal = loc.getLanguage();
          if (defaultLocal == null || defaultLocal.equals("es"))
            this.perfilMostrar = usuario.getPerfil().getNombre();
          else if (defaultLocal.equals("en"))
            this.perfilMostrar = usuario.getPerfil().getNombreIngles();
        }

        this.rol = usuario.getRolHaya() != null ? new CatalogoMinInfoDTO(usuario.getRolHaya()) : null;
    }
}
