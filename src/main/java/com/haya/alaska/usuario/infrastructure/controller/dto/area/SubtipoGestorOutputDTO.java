package com.haya.alaska.usuario.infrastructure.controller.dto.area;

import com.haya.alaska.subtipo_gestor.domain.SubtipoGestor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


@Getter
@Setter
@ToString
@NoArgsConstructor
public class SubtipoGestorOutputDTO implements Serializable {

  private Integer id;
  private String codigo;
  private String valor;
  private Boolean activo = true;

  public SubtipoGestorOutputDTO (SubtipoGestor subtipoGestor){
    if(subtipoGestor != null) {
      this.id = subtipoGestor.getId();
      this.activo = subtipoGestor.getActivo();
      this.codigo = subtipoGestor.getCodigo();
      this.valor = subtipoGestor.getValor();
    }
  }
}
