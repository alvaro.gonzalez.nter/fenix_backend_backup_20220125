package com.haya.alaska.usuario.infrastructure.controller.dto;

import com.haya.alaska.usuario.domain.Usuario;
import lombok.*;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.List;
import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDTO {
  private Integer id;
  private String email;
  private String nombre;
  private String telefono;
  private String perfil;
  private String perfilMostrar;
  private String rol;
  private List<String> areaUsuario;

  public UsuarioDTO(Usuario usuario) {
    this.email = usuario.getEmail();
    this.nombre = usuario.getNombre();
    this.telefono = usuario.getTelefono();
    if (usuario.getPerfil() != null){
      this.perfil = usuario.getPerfil().getNombre();
      Locale loc = LocaleContextHolder.getLocale();
      String defaultLocal = loc.getLanguage();
      if (defaultLocal == null || defaultLocal.equals("es"))
        this.perfilMostrar = usuario.getPerfil().getNombre();
      else if (defaultLocal.equals("en"))
        this.perfilMostrar = usuario.getPerfil().getNombreIngles();
    }
    if (usuario.getRolHaya() != null) this.rol = usuario.getRolHaya().getValor();
  }
}
