package com.haya.alaska.usuario.infrastructure.controller.dto.area;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class GrupoUsuarioInputDTO implements Serializable {
  Integer id; //Solo para updatear el GrupoUsuario
  String nombre;
}
