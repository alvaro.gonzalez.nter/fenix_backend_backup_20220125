package com.haya.alaska.usuario.infrastructure.repository;

import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
  Optional<Usuario> findByNombre(String username);
  Optional<Usuario> findByEmail(String email);
  Optional<Usuario> findByIdCliente(Integer idCliente);
  Optional<Usuario> findFirstByRolHayaCodigo(String admin);
  List<Usuario> findByPerfilNombre(String perfil);
  List<Usuario> findByPerfilId(Integer perfilId);
  List<Usuario> findByAreasId(Integer areaId);
  List<Usuario> findByAreasIdAndPerfilId(Integer areaId, Integer perfilId);
  List<Usuario> findByGrupoUsuariosId(Integer grupoId);

  List<Usuario> findByGrupoUsuariosIdAndPerfilId(Integer grupoId, Integer perfilId);

  List<Usuario> findByGrupoUsuariosIdAndAreasId(Integer grupoId, Integer areaId);

  List<Usuario> findByGrupoUsuariosIdAndAreasIdAndPerfilId(Integer grupoId, Integer areaId, Integer perfilId);

  List<Usuario> findByPerfilIdAndAreasId(Integer perfilId, Integer idArea);

  List<Usuario> findByPerfilIdAndAreasIdIsNull(Integer perfilId);

  List<Usuario> findByPerfilNombreAndAreas_NombreAndGrupoUsuarios_Nombre(String perfil, String area, String grupo);

  List<Usuario> findAllByPerfilNombreAndAsignacionExpedientesExpedienteId(String perfil, Integer idExpediente);

  List<Usuario> findAllByPerfilNombreAndAsignacionExpedientesPNExpedienteId(String perfil, Integer idExpediente);
  List<Usuario> findAllByActivoIsTrueAndPerfilNombre(String nombre);
  List<Usuario> findAllByPerfilNombre(String nombre);
  List<Usuario> findAllByAsignacionExpedientesExpedienteId(Integer id);

  List<Usuario> findAllByAsignacionExpedientesPNExpedienteId(Integer id);

  List<Usuario> findAllDistinctByAsignacionExpedientesExpedienteIdInAndPerfilIdAndAreasId(List<Integer> id,Integer perfil, Integer area);
  List<Usuario> findAllDistinctByAsignacionExpedientesPNExpedienteIdInAndPerfilIdAndAreasId(List<Integer> id,Integer perfil, Integer area);
  List<Usuario> findAllDistinctByAsignacionExpedientesExpedienteIdInAndPerfilId(List<Integer> id,Integer perfil);
  List<Usuario> findAllDistinctByAsignacionExpedientesPNExpedienteIdInAndPerfilId(List<Integer> id,Integer perfil);

  @Query(
    value =
      "SELECT HMU.ID AS id \n"
        + "FROM HIST_MSTR_USUARIO AS HMU \n"
        + "LEFT JOIN HIST_RELA_ASIGNACION_EXPEDIENTE AS HRAE ON HMU.ID = HRAE.ID_USUARIO \n"
        + "WHERE HRAE.ID_EXPEDIENTE = ?1 AND HMU.ID_PERFIL = ?2 \n"
        + "ORDER BY HMU.REV ;",
    nativeQuery = true)
  List<Map> getLastUsuarioAsignado(Integer idExpediente, Integer idPerfil);

  @Query(
      value =
          "SELECT HMU.ID AS id \n"
              + "FROM HIST_MSTR_USUARIO AS HMU \n"
              + "LEFT JOIN HIST_RELA_ASIGNACION_EXPEDIENTE_POSESION_NEGOCIADA AS HRAE ON HMU.ID = HRAE.ID_USUARIO \n"
              + "WHERE HRAE.ID_EXPEDIENTE = ?1 AND HMU.ID_PERFIL = ?2 \n"
              + "ORDER BY HMU.REV ;",
      nativeQuery = true)
  List<Map> getLastUsuarioAsignadoPN(Integer idExpediente, Integer idPerfil);
}
