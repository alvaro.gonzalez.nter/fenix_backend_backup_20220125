package com.haya.alaska.usuario.infrastructure.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.haya.alaska.expediente.infrastructure.controller.dto.ReasignacionGestorInputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.application.UsuarioUseCase;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioInputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioInputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.GrupoUsuarioOutputDTO;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/usuarios")
public class UsuarioController {
  @Autowired
  UsuarioUseCase usuarioUseCase;

  @ApiOperation(value = "Listado de todos los usuarios",
    notes = "Devuelve todos los usuarios registrados en la base de datos")
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioAgendaDto> getAllUsers() {
    return usuarioUseCase.getAll();
  }

  @ApiOperation(value = "Usuario por email.",
          notes = "Devuelve un usuario por el email enviado.")
  @GetMapping("/email")
  @ResponseStatus(HttpStatus.OK)
  public UsuarioAgendaDto getUsersByEmail(@RequestParam(value = "email") String email) throws NotFoundException {
      return usuarioUseCase.getUserDTOByEmail(email);
  }

  @ApiOperation(value = "Actualizar usuario", notes = "Actualizar el usuario con id enviado")
  @PutMapping("/{idUsuario}")
  @Transactional
  public UsuarioOutputDTO updateUsuario(@PathVariable("idUsuario") Integer idUsuario,
                                     @RequestBody @Valid UsuarioInputDTO usuarioInputDTO) throws Exception {
    return usuarioUseCase.updateUsuario(idUsuario, usuarioInputDTO);
  }

  @ApiOperation(value = "Obtener usuario", notes = "Obtener el usuario con id enviado")
  @GetMapping("/{idUsuario}")
  public UsuarioOutputDTO getUsuario(@PathVariable("idUsuario") Integer idUsuario) throws Exception {
    return usuarioUseCase.getUsuario(idUsuario);
  }

  @ApiOperation(value = "Obtener usuarios", notes = "Obtiene todos los usuarios")
  @GetMapping("/filtrados")
  public ListWithCountDTO<UsuarioOutputDTO> getFiltrados(@RequestParam(value = "nombre", required = false) String nombre,
                                                         @RequestParam(value = "perfil", required = false) String perfil,
                                                         @RequestParam(value = "orderField", required = false) String orderField,
                                                         @RequestParam(value = "orderDirection", required = false) String orderDirection,
                                                         @RequestParam(value = "size") Integer size,
                                                         @RequestParam(value = "page") Integer page) {
    return usuarioUseCase.getAllFilteredUsuarios(nombre, perfil, orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Get areas", notes = "Obtiene todas las areas")
  @GetMapping("/area")
  public List<AreaUsuarioOutputDTO> getArea(@ApiIgnore CustomUserDetails principal) throws NotFoundException {
    return usuarioUseCase.getAreas(false, false, null, principal);
  }

  @ApiOperation(value = "Get areas", notes = "Obtiene todas las areas")
  @PostMapping("/getAreas")
  public List<AreaUsuarioOutputDTO> getAreas(
    @RequestParam boolean todos,
    @RequestParam boolean todosPN,
    @RequestBody(required = false) ReasignacionGestorInputDTO busquedaDto,
    @ApiIgnore CustomUserDetails principal
  ) throws NotFoundException {
    return usuarioUseCase.getAreas(todos,todosPN,busquedaDto,principal);
  }

  @ApiOperation(value = "Get grupos", notes = "Obtiene todos los grupos")
  @GetMapping("/grupo")
  public List<GrupoUsuarioOutputDTO> getGrupo() {
    return usuarioUseCase.getGrupos();
  }


  @ApiOperation(
    value = "Usuarios de tipo gestor",
    notes = "Devuelve los usuarios de tipo gestor")
  @GetMapping("/gestores")
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioDTO> getGestores()
    throws NotFoundException {
    return usuarioUseCase.getGestores();
  }

  @ApiOperation(
    value = "Usuarios (de tipo gestor) por area",
    notes = "Devuelve los usuarios por el área enviada")
  @GetMapping("/gestoresArea")
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioDTO> getUsersByArea(
    @RequestParam(value = "idArea", required = false) Integer idArea)
    throws NotFoundException {
    return usuarioUseCase.getGestoresByArea(idArea);
  }

  @ApiOperation(
    value = "Usuarios (de tipo gestor) por grupo",
    notes = "Devuelve los usuarios por el grupo enviado")
  @GetMapping("/gestoresGrupo")
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioDTO> getUsersByGrupo(
    @RequestParam(value = "idGrupo", required = false) Integer idGrupo)
    throws NotFoundException {
    return usuarioUseCase.getGestoresByGrupo(idGrupo);
  }

  @ApiOperation(
    value = "Usuarios (de tipo gestor) por grupo y area",
    notes = "Devuelve los usuarios por el grupo y area enviados")
  @GetMapping("/gestoresGrupoArea")
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioDTO> getUsersByGrupoAndArea(
    @RequestParam(value = "idGrupo", required = false) Integer idGrupo,
    @RequestParam(value = "idArea", required = false) Integer idArea)
    throws NotFoundException {
    return usuarioUseCase.getGestoresByGrupoAndArea(idGrupo, idArea);
  }

  @ApiOperation(
    value = "Areas por grupo",
    notes = "Devuelve las areas por el grupo enviado")
  @GetMapping("/areasGrupo")
  @ResponseStatus(HttpStatus.OK)
  public List<AreaUsuarioOutputDTO> getAreasByGrupo(
    @RequestParam(value = "idGrupo", required = false) Integer idGrupo)
    throws NotFoundException {
    return usuarioUseCase.getAreasByGrupo(idGrupo);
  }

  @ApiOperation(
    value = "Areas por usuario",
    notes = "Devuelve las areas por el usuario enviado")
  @GetMapping("/areasUsuario")
  @ResponseStatus(HttpStatus.OK)
  public List<AreaUsuarioOutputDTO> getAreasByUsuario(
    @RequestParam(value = "idUsuario", required = false) Integer idUsuario)
    throws NotFoundException {
    return usuarioUseCase.getAreasByUsuario(idUsuario);
  }


  @ApiOperation(
    value = "Grupos por area",
    notes = "Devuelve los grupos por el area enviada")
  @GetMapping("/gruposArea")
  public List<GrupoUsuarioOutputDTO> getGruposByArea(
    @RequestParam(value = "idArea", required = false) Integer idArea)
    throws NotFoundException{

    return usuarioUseCase.getGruposByArea(idArea);
  }

  @ApiOperation(
    value = "Grupos por usuario",
    notes = "Devuelve los grupos por el usuario enviado")
  @GetMapping("/gruposUsuario")
  public List<GrupoUsuarioOutputDTO> getGruposByUsuario(
    @RequestParam(value = "idUsuario", required = false) Integer idUsuario)
    throws NotFoundException{

    return usuarioUseCase.getGruposByUsuario(idUsuario);
  }


  @ApiOperation(value = "Crear area", notes = "Añadir un nuevo area")
  @PostMapping("/area")
  @Transactional
  public AreaUsuarioOutputDTO createArea(@RequestParam(value = "nombre") String nombre,
                                         @RequestParam(value = "grupos") List<String> grupos)
    throws Exception {
    return usuarioUseCase.createArea(nombre, grupos);
  }

  @ApiOperation(value = "Update area", notes = "Updatear un area")
  @PutMapping("/area/{idArea}")
  @Transactional
  public AreaUsuarioOutputDTO updateArea(@PathVariable("idArea") Integer idArea,
                                         @RequestBody @Valid AreaUsuarioInputDTO input)
    throws Exception {
    return usuarioUseCase.updateArea(idArea, input);
  }

  @ApiOperation(value = "Borrar areas", notes = "Borrar una o varias areas. Fallara si las areas recibidas estan en uso")
  @DeleteMapping("/delete_list_areas")
  @Transactional
  public void deleteAreas(@RequestParam("idsArea") List<Integer> idsArea)
    throws Exception {
    usuarioUseCase.deleteArea(idsArea);
  }

  /*TODO qué tipo de gestores se quieren obtener, todos?
  @ApiOperation(value = "Usuarios gestores.",
    notes = "Devuelve los usuarios gestores en bbdd")
  @GetMapping("/gestores")
  @ResponseStatus(HttpStatus.OK)
  public UsuarioDTO getUsersGestores(@RequestParam(value = "email") String email) throws NotFoundException {
    return usuarioUseCase.getUserDTOByEmail(email);
  }*/

  @ApiOperation(value = "Usuarios Responsable de cartera.",
    notes = "Devuelve los usuarios Responsable de cartera en bbdd.")
  @GetMapping("/rc")
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioAgendaDto> getUsuariosRC() throws NotFoundException {
    return usuarioUseCase.findByPerfil("Responsable de cartera");
  }

  @ApiOperation(value = "Usuarios Gestor skip tracing.",
    notes = "Devuelve los usuarios Gestor skip tracing en bbdd.")
  @GetMapping("/st")
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioAgendaDto> getUsuariosST() throws NotFoundException {
    return usuarioUseCase.findByPerfil("Gestor skip tracing");
  }

  @ApiOperation(value = "Usuarios Gestor formalización.",
    notes = "Devuelve los usuarios Gestor formalización en bbdd.")
  @GetMapping("/form")
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioAgendaDto> getUsuariosForm() throws NotFoundException {
    return usuarioUseCase.findByPerfil("Gestor formalización");
  }

  @ApiOperation(value = "Usuarios con perfil soporte formalización.",
    notes = "Devuelve los usuarios con perfil soporte formalización en bbdd.")
  @GetMapping("/soporteFormalizacion")
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioAgendaDto> getUsuariosSoporteFormalizacion() throws NotFoundException {
    return usuarioUseCase.findByPerfil("Soporte formalización");
  }

  @ApiOperation(
    value = "Usuario por perfil.",
    notes = "Devuelve los usuarios por el perfil envidado.")
  @PostMapping("/perfil")
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioDTO> getUsersByPerfil(
    @RequestParam(value = "idPerfil", required = true) Integer idperfil,
    @RequestParam(value = "idArea", required = false) Integer idarea,
    @RequestParam boolean todos,
    @RequestParam boolean todosPN,
    @RequestBody(required = false) ReasignacionGestorInputDTO busquedaDto,
    @ApiIgnore CustomUserDetails principal)
    throws NotFoundException {
    return usuarioUseCase.getUserDTOByPerfil(idperfil, idarea, todos, todosPN, busquedaDto, principal);
  }

  @ApiOperation(
    value = "Usuario por perfil.",
    notes = "Devuelve los usuarios por el perfil envidado.")
  @GetMapping("/perfil")
  @ResponseStatus(HttpStatus.OK)
  public List<UsuarioDTO> getUsersByPerfil(
    @RequestParam(value = "idPerfil", required = true) Integer idperfil,
    @RequestParam(value = "idArea", required = false) Integer idarea,
    @ApiIgnore CustomUserDetails principal)
    throws NotFoundException {
    return usuarioUseCase.getUserDTOByPerfil(idperfil, idarea, false, false, null, principal);
  }


  // IdUsuario del usuarioDestino //Creacion de metadato en front esto en propuestas se hace en back
  @ApiOperation(value = "Añadir un documento a un usuario")
  @PostMapping("/{idUsuario}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoUsuario(
      @PathVariable("idUsuario") Integer idUsuario,
      @RequestPart("file") @NotNull @NotBlank MultipartFile file,
      @RequestParam("idMatricula") String idMatricula,
      @RequestParam("tipoDocumento") String tipoDocumento,
      @ApiIgnore CustomUserDetails principal)
      throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    usuarioUseCase.createDocumento(idUsuario, idMatricula,tipoDocumento, file, usuario);
    }

    //Cambiar y pasar el usuario conectado

  @ApiOperation(value = "Obtener documentación de usuario", notes = "Devuelve la documentación disponible del usuario con id enviado")
  @PostMapping("/documentosObt")
  @Transactional
  public ListWithCountDTO<DocumentoRepositorioDTO> getDocumentosByUsuario(
    @ApiIgnore CustomUserDetails principal,
    @RequestBody(required = false) BusquedaRepositorioDTO busquedaRepositorioDTO,
    @RequestParam(value="idDocumento",required = false) String idDocumento,
    @RequestParam(value="usuarioDestino",required = false) String usuarioDestino,
    @RequestParam(value="tipoDocumento",required = false) String tipoDocumento,
    @RequestParam(value="nombreDocumento",required = false) String nombreDocumento,
    @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
    @RequestParam(value = "fechaAlta", required = false) String fechaAlta,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page) throws IOException  {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return usuarioUseCase.findDocumentosByUsuario(busquedaRepositorioDTO,usuario.getId(),idDocumento,usuarioDestino,tipoDocumento,nombreDocumento,fechaActualizacion,fechaAlta,orderDirection,orderField,size,page);
  }

}
