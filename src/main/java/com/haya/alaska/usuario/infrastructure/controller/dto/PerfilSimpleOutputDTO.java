package com.haya.alaska.usuario.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.perfil.domain.Perfil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PerfilSimpleOutputDTO implements Serializable {
  Integer id;
  String nombre;
  CatalogoMinInfoDTO areaPerfil;

  public PerfilSimpleOutputDTO(Perfil perfil){
    this.id = perfil.getId();
    this.nombre = perfil.getNombre();
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage().equals("en"))
      this.nombre = perfil.getNombreIngles();
    this.areaPerfil = perfil.getAreaPerfil() != null ? new CatalogoMinInfoDTO(perfil.getAreaPerfil()) : null;
  }
}
