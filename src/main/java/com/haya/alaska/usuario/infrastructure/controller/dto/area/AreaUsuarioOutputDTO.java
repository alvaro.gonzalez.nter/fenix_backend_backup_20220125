package com.haya.alaska.usuario.infrastructure.controller.dto.area;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class AreaUsuarioOutputDTO implements Serializable {
  Integer id;
  String nombre;
  List<GrupoUsuarioOutputDTO> grupos;

  public AreaUsuarioOutputDTO(AreaUsuario au){
    BeanUtils.copyProperties(au, this);
    this.grupos = au.getGrupos().stream().map(GrupoUsuarioOutputDTO::new).collect(Collectors.toList());
  }
}
