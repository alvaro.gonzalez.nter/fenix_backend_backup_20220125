package com.haya.alaska.usuario.infrastructure.controller.dto;

import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraOutputDTO;
import com.haya.alaska.cartera.infrastructure.controller.dto.ClienteOutputDTO;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.GrupoUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.SubtipoGestorOutputDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UsuarioOutputDTO implements Serializable {
  Integer id;
  String nombre;
  PerfilSimpleOutputDTO perfil;
  SubtipoGestorOutputDTO subtipoGestor;
  List<CatalogoMinInfoDTO> provincia;
  List<AreaUsuarioOutputDTO> area;
  List<GrupoUsuarioOutputDTO> grupos;
  List<ClienteOutputDTO> clientes;
  List<CarteraOutputDTO> carteras;
  Integer numeroExpedientes;
}
