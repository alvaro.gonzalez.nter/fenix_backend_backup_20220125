package com.haya.alaska.usuario.infrastructure.mapper;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraOutputDTO;
import com.haya.alaska.cartera.infrastructure.controller.dto.ClienteOutputDTO;
import com.haya.alaska.cartera.infrastructure.mapper.CarteraMapper;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.GrupoUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.SubtipoGestorOutputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UsuarioMapper {

  @Autowired
  CarteraMapper carteraMapper;

  public UsuarioOutputDTO entityToDTO(Usuario usuario){
    UsuarioOutputDTO result = new UsuarioOutputDTO();
    if (usuario.getId() != null) {
      result.setId(usuario.getId());
    }
    result.setNombre(usuario.getNombre());
    result.setPerfil(usuario.getPerfil() != null ? new PerfilSimpleOutputDTO(usuario.getPerfil()) : null);
    if (usuario.getAreas() != null && !usuario.getAreas().isEmpty()){
      List<AreaUsuarioOutputDTO> areas = new ArrayList<>();
      for (AreaUsuario au : usuario.getAreas()){
        areas.add(new AreaUsuarioOutputDTO(au));
      }
      result.setArea(areas);
    }
    if (usuario.getPerfil() != null && !usuario.getPerfil().equals("")) {
      if (usuario.getPerfil().getNombre().equals("Gestor")) {
        SubtipoGestorOutputDTO output = new SubtipoGestorOutputDTO(usuario.getSubtipoGestor());
        result.setSubtipoGestor(output);
      }
    }

    List<CatalogoMinInfoDTO> provincias = usuario.getProvincias().stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
    result.setProvincia(provincias);

    List<GrupoUsuarioOutputDTO> grupos = new ArrayList<>();
    for (GrupoUsuario gu : usuario.getGrupoUsuarios()){
      grupos.add(new GrupoUsuarioOutputDTO(gu));
    }
    result.setGrupos(grupos);

    result.setNumeroExpedientes(usuario.getLimiteExpedientes());

    List<CarteraOutputDTO> carteras = new ArrayList<>();
    List<Cliente> clientes = new ArrayList<>();
    for(var asignacionUsuarioCartera: usuario.getAsignacionesUsuarioCartera()) {
      carteras.add(carteraMapper.entityToOutput(asignacionUsuarioCartera.getCartera()));
      if(!clientes.contains(asignacionUsuarioCartera.getCartera().getCliente())) {
        clientes.add(asignacionUsuarioCartera.getCartera().getCliente());
      }
    }
    result.setClientes(ClienteOutputDTO.newList(clientes));
    result.setCarteras(carteras);
    return result;
  }

  public UsuarioDTO parse(Usuario usuario) {
    UsuarioDTO usuarioDTO = new UsuarioDTO();
    usuarioDTO.setEmail(usuario.getEmail());
    usuarioDTO.setId(usuario.getId());
    usuarioDTO.setNombre(usuario.getNombre());
    usuarioDTO.setPerfil(usuario.getPerfil().getNombre());
    if(usuario.getAreas() != null) usuarioDTO.setAreaUsuario(
      usuario.getAreas().stream().map(AreaUsuario::getNombre).collect(Collectors.toList()));
    return usuarioDTO;
  }


}
