package com.haya.alaska.usuario.application;

import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;
import com.haya.alaska.expediente.infrastructure.controller.dto.ReasignacionGestorInputDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioInputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioInputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.GrupoUsuarioOutputDTO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface UsuarioUseCase {
  UsuarioAgendaDto getUserDTOByEmail(String email) throws NotFoundException;

  List<UsuarioAgendaDto> getAll();

  List<UsuarioAgendaDto> findByPerfil(String nombre);

  UsuarioOutputDTO updateUsuario(Integer idUsuario, UsuarioInputDTO input) throws Exception;

  List<AreaUsuarioOutputDTO> getAreas(boolean todos, boolean todosPN, ReasignacionGestorInputDTO busquedaDto, CustomUserDetails principal) throws NotFoundException;

  List<GrupoUsuarioOutputDTO> getGrupos();

  ListWithCountDTO<UsuarioOutputDTO> getAllFilteredUsuarios(
    String nombre, String perfil, String orderField, String orderDirection, Integer size, Integer page);

  AreaUsuarioOutputDTO createArea(String nombre, List<String> grupos);

  AreaUsuarioOutputDTO updateArea(Integer id, AreaUsuarioInputDTO input) throws NotFoundException, InvalidCodeException;

  List<GrupoUsuarioOutputDTO> getSeparados();

  UsuarioOutputDTO getUsuario(Integer idUsuario) throws NotFoundException;

  Usuario actualizarUsuarioConAccessToken(HashMap<String, String> accessToken, String nombrePerfilAD) throws ForbiddenException;

  List<UsuarioDTO> getUserDTOByPerfil(Integer idPerfil, Integer idArea, boolean todos, boolean todosPN,
                                      ReasignacionGestorInputDTO busquedaDto, CustomUserDetails principal) throws NotFoundException;

  List<UsuarioDTO> getGestores();

  List<UsuarioDTO> getGestoresByArea(Integer idArea) throws NotFoundException;

  List<UsuarioDTO> getGestoresByGrupo(Integer idGrupo) throws NotFoundException;

  List<UsuarioDTO> getGestoresByGrupoAndArea(Integer idGrupo, Integer idArea) throws NotFoundException;

  List<AreaUsuarioOutputDTO> getAreasByGrupo(Integer idGrupo) throws NotFoundException;

  List<AreaUsuarioOutputDTO> getAreasByUsuario(Integer idUsuario) throws NotFoundException;

  List<GrupoUsuarioOutputDTO> getGruposByArea(Integer idArea) throws NotFoundException;

  List<GrupoUsuarioOutputDTO> getGruposByUsuario(Integer idUsuario) throws NotFoundException;

  void deleteArea(List<Integer> idsArea) throws Exception;

   void createDocumento(Integer idUsuario,String idMatricula,String tipoDocumento, MultipartFile file, Usuario usuario) throws Exception;

  ListWithCountDTO<DocumentoRepositorioDTO> findDocumentosByUsuario(
      BusquedaRepositorioDTO busquedaRepositorioDTO,
      Integer usuarioConectado,
      String idDocumento,
      String usuarioDestino,
      String tipo,
      String nombreDocumento,
      String fechaActualizacion,
      String fechaAlta,
      String orderDirection,
      String orderField,
      Integer size,
      Integer page)
      throws IOException;
}
