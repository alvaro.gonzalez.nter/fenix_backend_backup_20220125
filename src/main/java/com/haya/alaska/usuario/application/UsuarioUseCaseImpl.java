package com.haya.alaska.usuario.application;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.area_usuario.domain.AreaUsuario_;
import com.haya.alaska.area_usuario.infrastructure.repository.AreaUsuarioRepository;
import com.haya.alaska.asignacion_expediente.application.AsignacionExpedienteUseCase;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.infrastructure.repository.AsignacionExpedienteRepository;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.asignacion_expediente_posesion_negociada.repository.AsignacionExpedientePosesionNegociadaRepository;
import com.haya.alaska.asignacion_usuario_cartera.domain.AsignacionUsuarioCartera;
import com.haya.alaska.asignacion_usuario_cartera.repository.AsignacionUsuarioCarteraRepository;
import com.haya.alaska.busqueda.application.BusquedaUseCase;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.ReasignacionGestorInputDTO;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCase;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario_;
import com.haya.alaska.grupo_usuario.infrastructure.repository.GrupoUsuarioRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.application.GestorDocumentalUseCase;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.domain.Perfil_;
import com.haya.alaska.perfil.infrastructure.repository.PerfilRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.domain.Provincia_;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.rol_haya.infrastructure.repository.RolHayaRepository;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoContenedorInputDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_gestor.domain.SubtipoGestor;
import com.haya.alaska.subtipo_gestor.infrastructure.repository.SubtipoGestorRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioInputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioInputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.GrupoUsuarioInputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.GrupoUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.mapper.UsuarioMapper;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class UsuarioUseCaseImpl implements UsuarioUseCase {
  @Autowired
  private UsuarioRepository usuarioRepository;
  @Autowired
  private UsuarioMapper usuarioMapper;
  @Autowired
  AsignacionExpedienteRepository asignacionExpedienteRepository;
  @Autowired
  AsignacionExpedientePosesionNegociadaRepository asignacionExpedientePosesionNegociadaRepository;
  @Autowired
  private RolHayaRepository rolHayaRepository;
  @Autowired
  private PerfilRepository perfilRepository;
  @Autowired
  private ProvinciaRepository provinciaRepository;
  @Autowired
  private AreaUsuarioRepository areaUsuarioRepository;
  @Autowired
  private GrupoUsuarioRepository grupoUsuarioRepository;
  @PersistenceContext
  private EntityManager entityManager;
  @Autowired
  private SubtipoGestorRepository subtipoGestorRepository;
  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;
  @Autowired
  private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired
  private GestorDocumentalUseCase gestorDocumentalUseCase;
  @Autowired
  private CarteraRepository carteraRepository;
  @Autowired
  private AsignacionUsuarioCarteraRepository asignacionUsuarioCarteraRepository;
  @Autowired
  private ExpedienteRepository expedienteRepository;
  @Autowired
  private AsignacionExpedienteUseCase asignacionExpedienteUseCase;
  @Autowired
  private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  private BusquedaUseCase busquedaUseCase;
  @Autowired
  private ExpedientePosesionNegociadaCase expedientePosesionNegociadaCase;


  @Override
  public List<UsuarioAgendaDto> getAll() {
    List<Usuario> usuarios = usuarioRepository.findAll();
    return usuarios.stream().map(UsuarioAgendaDto::new).collect(Collectors.toList());
  }

  @Override
  public UsuarioAgendaDto getUserDTOByEmail(String email) throws NotFoundException {
    Optional<Usuario> userOpt = usuarioRepository.findByEmail(email);
    Usuario user = userOpt.orElse(null);
    if (user == null) throw new NotFoundException("usuario", "email", email);
    return new UsuarioAgendaDto(user);
  }

  @Override
  public ListWithCountDTO<UsuarioOutputDTO> getAllFilteredUsuarios(
    String nombre, String perfil, String orderField, String orderDirection, Integer size, Integer page) {
    List<Usuario> usuariosSinPaginar = filterUsuariosInMemory(
      nombre, perfil, orderField, orderDirection);

    List<Usuario> contratos = usuariosSinPaginar.stream().skip(size * page).limit(size).collect(Collectors.toList());

    List<UsuarioOutputDTO> result = contratos.stream().map(usu -> usuarioMapper.entityToDTO(usu)).collect(Collectors.toList());
    return new ListWithCountDTO<>(result, usuariosSinPaginar.size());
  }

  private List<Usuario> filterUsuariosInMemory(
    String nombre,
    String perfil,
    String orderField,
    String orderDirection) {

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Usuario> query = cb.createQuery(Usuario.class);

    Root<Usuario> root = query.from(Usuario.class);
    List<Predicate> predicates = new ArrayList<>();

    if (perfil != null) {
      Join<Usuario, Perfil> join = root.join(Usuario_.PERFIL, JoinType.INNER);
      Predicate onCond = cb.like(join.get(Perfil_.nombre), "%" + perfil + "%");
      join.on(onCond);
    }
    if (nombre != null)
      predicates.add(cb.like(root.get(Usuario_.nombre), "%" + nombre + "%"));

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    if (orderField != null) {
      if (orderField.equals("perfil")) {
        Join<Usuario, Perfil> join1 = root.join(Usuario_.perfil, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(join1.get(Perfil_.nombre)));
        } else {
          rs.orderBy(cb.asc(join1.get(Perfil_.nombre)));
        }
      } else if (orderField.equals("area")) {
        Join<Usuario, AreaUsuario> join1 = root.join(Usuario_.areas, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(join1.get(AreaUsuario_.nombre)));
        } else {
          rs.orderBy(cb.asc(join1.get(AreaUsuario_.nombre)));
        }
      } else if (orderField.equals("provincia")) {
        Join<Usuario, Provincia> join1 = root.join(Usuario_.provincias, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(join1.get(Provincia_.VALOR)));
        } else {
          rs.orderBy(cb.asc(join1.get(Provincia_.VALOR)));
        }
      } else if (orderField.equals("grupos")) {
        Join<Usuario, GrupoUsuario> join1 = root.join(Usuario_.grupoUsuarios, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(join1.get(GrupoUsuario_.NOMBRE)));
        } else {
          rs.orderBy(cb.asc(join1.get(GrupoUsuario_.NOMBRE)));
        }
      } else if (orderField.equals("numeroExpedientes")) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(Usuario_.limiteExpedientes)));
        } else {
          rs.orderBy(cb.asc(root.get(Usuario_.limiteExpedientes)));
        }
      } else {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(orderField)));
        } else {
          rs.orderBy(cb.asc(root.get(orderField)));
        }
      }
    }
    Set<ParameterExpression<?>> q = query.getParameters();

    List<Usuario> listaUsuarios = entityManager.createQuery(query).getResultList();

    return listaUsuarios;
  }

  @Override
  public UsuarioOutputDTO getUsuario(Integer idUsuario) throws NotFoundException {
    Usuario usuario = usuarioRepository.findById(idUsuario).orElseThrow(() ->
      new NotFoundException("usuario", idUsuario));
    UsuarioOutputDTO result = usuarioMapper.entityToDTO(usuario);
    return result;
  }

  @Override
  public UsuarioOutputDTO updateUsuario(Integer idUsuario, UsuarioInputDTO input) throws Exception {
    Usuario usuario = usuarioRepository.findById(idUsuario).orElseThrow(() ->
      new NotFoundException("usuario", idUsuario));

    if (input.getNombre() == null) throw new RequiredValueException("nombre");
    if (input.getPerfil() == null) throw new RequiredValueException("rol");
    if (input.getProvincia() == null || input.getProvincia().size() == 0){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("The provinces field is required and must contain some value.");
      else throw new RequiredValueException("El campo provincias es necesario y debe contener algún valor.");
    }

    var cambiaPerfil = false;
    if (usuario.getPerfil() != null && !usuario.getPerfil().getId().equals(input.getPerfil()))
      cambiaPerfil = true;

    usuario.setNombre(input.getNombre());

    Perfil rol = perfilRepository.findById(input.getPerfil()).orElseThrow(() ->
      new NotFoundException("rol", input.getPerfil()));
    usuario.setPerfil(rol);

    SubtipoGestor subtipoGestor = null;
    if (rol.getNombre().equals("Gestor") && input.getSubtipoGestor() != null) {
      subtipoGestor = subtipoGestorRepository.findById(input.getSubtipoGestor()).get();
      usuario.setSubtipoGestor(subtipoGestor);
    }

    Set<Provincia> provincias = new HashSet<>();
    for (Integer provinciaId : input.getProvincia()) {
      provincias.add(provinciaRepository.findById(provinciaId).orElseThrow(() ->
        new NotFoundException("provincia", provinciaId)));
    }
    usuario.setProvincias(provincias);

    if (rol.getNombre().equals("Supervisor") || rol.getNombre().equals("Responsable de cartera")) {
      usuario.setLimiteExpedientes(null);
    } else {
      Integer n_asignados = asignacionExpedienteRepository.countByUsuarioId(usuario.getId());
      if(n_asignados>input.getNumeroExpedientes()){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("No se puede asignar el límite de expedientes seleccionado, el usuario con id " + usuario.getId() + " tiene asignados " + n_asignados + " expedientes");
        else throw new NotFoundException("No se puede asignar el límite de expedientes seleccionado, el usuario con id "+usuario.getId()+" tiene asignados " + n_asignados + " expedientes");

      }
      usuario.setLimiteExpedientes(input.getNumeroExpedientes());
    }

    if (input.getArea() == null) usuario.setAreas(new HashSet<>());
    else {
      Set<AreaUsuario> newAreas = new HashSet<>();
      for (Integer idAU : input.getArea()) {
        AreaUsuario au = areaUsuarioRepository.findById(idAU).orElseThrow(() ->
          new NotFoundException("areaUsuario", idAU));
        newAreas.add(au);
        usuario.setAreas(newAreas);
      }
    }

    if (input.getGrupos() == null) usuario.setGrupoUsuarios(new HashSet<>());
    else {
      Set<GrupoUsuario> grupos = new HashSet<>();
      for (Integer grupoId : input.getGrupos()) {
        grupos.add(grupoUsuarioRepository.findById(grupoId).orElseThrow(() ->
          new NotFoundException("grupoUsuario", grupoId)));
      }
      usuario.setGrupoUsuarios(grupos);
    }

    //si ha cambiado de perfil, borramos las asignaciones para que no haya 2 usuarios con el mismo perfil y las mismas asignaciones
    if (cambiaPerfil) {
      asignacionExpedienteRepository.deleteAllByUsuarioId(usuario.getId());
      asignacionExpedientePosesionNegociadaRepository.deleteAllByUsuarioId(usuario.getId());
    }


    List<AsignacionUsuarioCartera> asignaciones = new ArrayList<>();
    List<Integer> listExpediente = new ArrayList<>();
    List<Integer> listExpedientePN = new ArrayList<>();

    if(input.getCarteras() != null) {
      for(var asignacion: usuario.getAsignacionesUsuarioCartera()) {
        //el input no trae el id de la cartera que ya tiene el usuario
        if(!input.getCarteras().contains(asignacion.getCartera().getId())) {
          //desasignar la cartera (se hara fuera del bucle)
          asignaciones.add(asignacion);

          //desasignar los expedientes
          Set<AsignacionExpediente> asignacionesUsuarioExpediente = usuario.getAsignacionExpedientes();
          Iterator<AsignacionExpediente> it = asignacionesUsuarioExpediente.iterator();

          while(it.hasNext()) {
            AsignacionExpediente asig = it.next();
            Expediente expediente = asig.getExpediente();
            if(expediente.getCartera().getId().equals(asignacion.getCartera().getId())) {
              listExpediente.add(expediente.getId());
              it.remove();
            }
          }
          usuario.setAsignacionExpedientes(asignacionesUsuarioExpediente);


          //desasignar los expedientes de pn
          Set<AsignacionExpedientePosesionNegociada> asignacionesUsuarioExpedientePN = usuario.getAsignacionExpedientesPN();
          Iterator<AsignacionExpedientePosesionNegociada> itpn = asignacionesUsuarioExpedientePN.iterator();

          while(itpn.hasNext()) {
            AsignacionExpedientePosesionNegociada asig = itpn.next();
            ExpedientePosesionNegociada expediente = asig.getExpediente();
            if(expediente.getCartera().getId().equals(asignacion.getCartera().getId())) {
              listExpedientePN.add(expediente.getId());
              itpn.remove();
            }
          }
          usuario.setAsignacionExpedientesPN(asignacionesUsuarioExpedientePN);
        }
      }

      for(var idCartera: input.getCarteras()) {
        boolean tieneLaCartera = false;
        for(var asignacion: usuario.getAsignacionesUsuarioCartera()) {
          //el input trae un id de cartera que no tiene el usuario
          if(asignacion.getCartera().getId().equals(idCartera)) {
            tieneLaCartera = true;
          }
        }
        if(!tieneLaCartera) {
          //asignar la cartera
          Cartera cartera = carteraRepository.findById(idCartera).orElseThrow(()->new Exception("Cartera no encontrada"));
          AsignacionUsuarioCartera asignacionUsuarioCartera = new AsignacionUsuarioCartera(cartera,usuario);
          asignacionUsuarioCarteraRepository.save(asignacionUsuarioCartera);
          var listaUsuario = usuario.getAsignacionesUsuarioCartera();
          listaUsuario.add(asignacionUsuarioCartera);
          usuario.setAsignacionesUsuarioCartera(listaUsuario);
          usuarioRepository.save(usuario);
          var listaCartera = cartera.getAsignacionesUsuarioCartera();
          listaCartera.add(asignacionUsuarioCartera);
          cartera.setAsignacionesUsuarioCartera(listaCartera);
          carteraRepository.save(cartera);
        }
      }
    }
    var asignacionesUsuario = usuario.getAsignacionesUsuarioCartera();
    for(var asignacion: asignaciones) {
      asignacionesUsuario.remove(asignacion);
      asignacionUsuarioCarteraRepository.delete(asignacion);
    }
    usuario.setAsignacionesUsuarioCartera(asignacionesUsuario);


    Usuario usuarioSaved = usuarioRepository.save(usuario);
    asignacionExpedienteRepository.deleteAllByUsuarioIsNullAndExpedienteIdIn(listExpediente);
    asignacionExpedientePosesionNegociadaRepository.deleteAllByUsuarioIsNullAndExpedienteIdIn(listExpedientePN);
    UsuarioOutputDTO result = usuarioMapper.entityToDTO(usuarioSaved);
    return result;
  }

  public List<Usuario> getUsuarios(boolean todos, boolean todosPN, ReasignacionGestorInputDTO busquedaDto, CustomUserDetails principal, List<Usuario> usuarios) throws NotFoundException {

    List<Usuario> usuarios2 = new ArrayList<>();

    if(busquedaDto != null && busquedaDto.getExpedienteIdList() != null && busquedaDto.getExpedienteIdList().size() > 0) {
      for(var idExpediente: busquedaDto.getExpedienteIdList()) {
        Expediente expediente = expedienteRepository.findById(idExpediente).orElse(null);
        Cartera cartera = expediente.getCartera();
        for(var usuario: usuarios) {
          boolean meter = false;
          if(usuario.getAsignacionesUsuarioCartera() != null && usuario.getAsignacionesUsuarioCartera().size() > 0) {
            for(var asignacion: usuario.getAsignacionesUsuarioCartera()) {
              if(asignacion.getCartera().getId().equals(cartera.getId()))
                meter = true;
            }
          }
          if(meter && !usuarios2.contains(usuario)) usuarios2.add(usuario);
        }
      }
    }

    if(busquedaDto != null && busquedaDto.getExpedientePNIdList() != null && busquedaDto.getExpedientePNIdList().size() > 0) {
      for(var idExpediente: busquedaDto.getExpedientePNIdList()) {
        ExpedientePosesionNegociada expediente = expedientePosesionNegociadaRepository.findById(idExpediente).orElse(null);
        Cartera cartera = expediente.getCartera();
        for(var usuario: usuarios) {
          boolean meter = false;
          if(usuario.getAsignacionesUsuarioCartera() != null && usuario.getAsignacionesUsuarioCartera().size() > 0) {
            for(var asignacion: usuario.getAsignacionesUsuarioCartera()) {
              if(asignacion.getCartera().getId().equals(cartera.getId()))
                meter = true;
            }
          }
          if(meter && !usuarios2.contains(usuario)) usuarios2.add(usuario);
        }
      }
    }

    //si hay busqueda de RA
    if(busquedaDto != null && busquedaDto.getBusquedaDto() != null && todos) {
      List<Cartera> carteras = new ArrayList<>();
      List<Expediente> expedientesLista =
        busquedaUseCase.getDataExpedientesFilter(busquedaDto.getBusquedaDto(), (Usuario)principal.getPrincipal(), null, null);
      for(var expediente: expedientesLista) {
        if(!carteras.contains(expediente.getCartera())) {
          carteras.add(expediente.getCartera());
        }
      }
      for(var cartera: carteras) {
        for(var usuario: usuarios) {
          boolean meter = false;
          if(usuario.getAsignacionesUsuarioCartera() != null && usuario.getAsignacionesUsuarioCartera().size() > 0) {
            for(var asignacion: usuario.getAsignacionesUsuarioCartera()) {
              if(asignacion.getCartera().getId().equals(cartera.getId()))
                meter = true;
            }
          }
          if(meter && !usuarios2.contains(usuario)) usuarios2.add(usuario);
        }
      }
    }

    //si hay busqueda de PN
    if(busquedaDto != null && busquedaDto.getBusquedaDto() != null && todosPN) {
      List<Cartera> carteras = new ArrayList<>();
      List<ExpedientePosesionNegociada> expedientesLista = expedientePosesionNegociadaCase.filtrarExpedientesPosesionNegociadaBusqueda(
        busquedaDto.getBusquedaDto(), (Usuario)principal.getPrincipal(), null, null, null, null, null, null, null, null, null, null, null, null, null);
      for(var expediente: expedientesLista) {
        if(!carteras.contains(expediente.getCartera())) {
          carteras.add(expediente.getCartera());
        }
      }
      for(var cartera: carteras) {
        for(var usuario: usuarios) {
          boolean meter = false;
          if(usuario.getAsignacionesUsuarioCartera() != null && usuario.getAsignacionesUsuarioCartera().size() > 0) {
            for(var asignacion: usuario.getAsignacionesUsuarioCartera()) {
              if(asignacion.getCartera().getId().equals(cartera.getId()))
                meter = true;
            }
          }
          if(meter && !usuarios2.contains(usuario)) usuarios2.add(usuario);
        }
      }
    }

    return usuarios2;
  }

  @Override
  public List<AreaUsuarioOutputDTO> getAreas(boolean todos, boolean todosPN, ReasignacionGestorInputDTO busquedaDto, CustomUserDetails principal) throws NotFoundException {

    List<AreaUsuario> areas = new ArrayList<>();

    if(busquedaDto != null) {
      List<Usuario> usuarios = usuarioRepository.findAll();
      usuarios = getUsuarios(todos,todosPN,busquedaDto,principal,usuarios);
      for(var usuario: usuarios) {
        for(var area: usuario.getAreas()) {
          if(!areas.contains(area) && area.getActivo()) areas.add(area);
        }
      }
    } else {
      areas = areaUsuarioRepository.findAll();
    }


    List<AreaUsuarioOutputDTO> result = areas.stream().filter(AreaUsuario::getActivo).map(AreaUsuarioOutputDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public List<GrupoUsuarioOutputDTO> getGrupos() {
    List<GrupoUsuario> grupos = grupoUsuarioRepository.findAll();
    List<GrupoUsuarioOutputDTO> result = grupos.stream().map(GrupoUsuarioOutputDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public AreaUsuarioOutputDTO createArea(String nombre, List<String> grupos) {
    AreaUsuario au = new AreaUsuario();
    au.setNombre(nombre);
    AreaUsuario areaUsuario = areaUsuarioRepository.saveAndFlush(au);

    GrupoUsuario gu;
    for (String name : grupos) {
      gu = new GrupoUsuario();
      gu.setNombre(name);
      gu.setAreaUsuario(areaUsuario);
      areaUsuario.getGrupos().add(grupoUsuarioRepository.saveAndFlush(gu));
    }

    AreaUsuarioOutputDTO result = new AreaUsuarioOutputDTO(areaUsuario);
    return result;
  }

  @Override
  public AreaUsuarioOutputDTO updateArea(Integer id, AreaUsuarioInputDTO input) throws NotFoundException, InvalidCodeException {
    AreaUsuario areaUsuario = areaUsuarioRepository.findById(id).orElseThrow(() ->
      new NotFoundException("areaUsuario", id));
    areaUsuario.setNombre(input.getNombre());
    areaUsuarioRepository.saveAndFlush(areaUsuario);

    List<Integer> gruposABorrar = new ArrayList<>();

    for (GrupoUsuario gu : areaUsuario.getGrupos()) {
      boolean b = true;
      Integer guId = gu.getId();
      for (GrupoUsuarioInputDTO gui : input.getGrupos()) {
        Integer guiId = gui.getId();
        if (guiId != null && guiId.equals(guId)) {
          b = false;
          break;
        }
      }

      if (b) {
        if (!gu.getUsuarios().isEmpty()){
          Locale loc = LocaleContextHolder.getLocale();
          if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
            throw new InvalidCodeException("Cannot delete UserGroup named: " + gu.getNombre() + " as it is assigned to a User.");
          else throw new InvalidCodeException("No se puede borrar el GrupoUsuario de nombre: " + gu.getNombre() + " ya que está asignado a un Usuario.");
        }
        gruposABorrar.add(gu.getId());
      }
    }

    for (Integer guId : gruposABorrar) {
      grupoUsuarioRepository.deleteById(guId);
    }

    List<GrupoUsuario> grupos = new ArrayList<>();
    areaUsuario.setGrupos(grupos);
    areaUsuarioRepository.save(areaUsuario);

    for (GrupoUsuarioInputDTO gu : input.getGrupos()) {
      GrupoUsuario grupoUsuario;
      if (gu.getId() == null) {
        grupoUsuario = new GrupoUsuario();
      } else {
        grupoUsuario = grupoUsuarioRepository.findById(gu.getId()).orElseThrow(() ->
          new NotFoundException("grupoUsuario", gu.getId()));
      }
      grupoUsuario.setNombre(gu.getNombre());
      grupoUsuario.setAreaUsuario(areaUsuario);
      grupos.add(grupoUsuarioRepository.saveAndFlush(grupoUsuario));
    }
    areaUsuario.setGrupos(grupos);
    AreaUsuario areaFinal = areaUsuarioRepository.save(areaUsuario);

    AreaUsuarioOutputDTO result = new AreaUsuarioOutputDTO(areaFinal);
    return result;
  }


  @Override
  public List<GrupoUsuarioOutputDTO> getSeparados() {
    List<GrupoUsuario> grupos = grupoUsuarioRepository.findAllByAreaUsuarioIsNull();
    return grupos.stream().map(GrupoUsuarioOutputDTO::new).collect(Collectors.toList());
  }

  @Override
  public List<UsuarioAgendaDto> findByPerfil(String nombre) {
    List<Usuario> usuarios = usuarioRepository.findByPerfilNombre(nombre);
    List<UsuarioAgendaDto> result = usuarios.stream().map(UsuarioAgendaDto::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public Usuario actualizarUsuarioConAccessToken(HashMap<String, String> accessToken, String nombrePerfilAD) throws ForbiddenException {
    String email = accessToken.get("email");
    String perfilAD = accessToken.get(nombrePerfilAD);
    if (perfilAD == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new ForbiddenException("You do not have permission to access this application");
      else throw new ForbiddenException("No tienes permiso para acceder a esta aplicación");
    }
    String nombre = accessToken.get("commonname");
    if (nombre == null) {
      nombre = accessToken.get("given_name") + " " + accessToken.get("family_name");
    }

    Usuario usuario = this.usuarioRepository.findByEmail(email).orElse(new Usuario(email));
    if(usuario.getIdHaya() == null) usuario.setIdHaya(usuario.getEmail().split("@")[0]);
    usuario.setRolHaya(this.rolHayaRepository.findByCodigo(perfilAD).orElse(null));
    usuario.setNombre(nombre);

    String telefono = accessToken.get("telephoneNumber");
    if (telefono != null) {
	usuario.setTelefono(telefono);
    }

    return this.usuarioRepository.save(usuario);
  }

  @Override
  public List<UsuarioDTO> getUserDTOByPerfil(Integer idPerfil, Integer idArea, boolean todos, boolean todosPN, ReasignacionGestorInputDTO busquedaDto, CustomUserDetails principal)
    throws NotFoundException {
    List<Usuario> usuarios;

    if (idArea == null && idPerfil == null)
      usuarios = usuarioRepository.findAll(); //  entendemos que el front los quiere todos
    else if (idArea != null && idPerfil == null){
      // error, nunca deberiamos recibir el idperfil nulo
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The idPerfil cannot be null");
      else throw new NotFoundException("El idPerfil no puede ser nulo");
    }

    else if (idArea != null && idPerfil != null)
      usuarios = usuarioRepository.findByPerfilIdAndAreasId(idPerfil, idArea); // busca por id perfil y por id area
    else
      usuarios = usuarioRepository.findByPerfilIdAndAreasIdIsNull(idPerfil); // devuelve todos los usuarios con perfil && area con valor nulo

    if(busquedaDto != null){
      usuarios = getUsuarios(todos,todosPN,busquedaDto,principal,usuarios);
    }

    List<UsuarioDTO> usuariosDTO = new ArrayList<>();
    for (Usuario usuario : usuarios) {
      System.out.println(usuario);
      usuariosDTO.add(usuarioMapper.parse(usuario));
    }
    return usuariosDTO;
  }

  @Override
  public List<UsuarioDTO> getGestores(){
    List<Usuario> usuarios = usuarioRepository.findAll();
    List<UsuarioDTO> usuariosDTO = new ArrayList<>();
    for (Usuario usuario : usuarios) {
      if(usuario.getPerfil() != null && usuario.getPerfil().getNombre().equals("Gestor"))
        usuariosDTO.add(usuarioMapper.parse(usuario));
    }
    return usuariosDTO;
  }

  @Override
  public List<UsuarioDTO> getGestoresByGrupo(Integer idGrupo)
    throws NotFoundException {
    List<Usuario> usuarios;

    if (idGrupo != null)
      usuarios = usuarioRepository.findByGrupoUsuariosId(idGrupo); // busca por id perfil y por id area
    else
      usuarios = usuarioRepository.findAll(); //  entendemos que el front los quiere todos

    List<UsuarioDTO> usuariosDTO = new ArrayList<>();
    for (Usuario usuario : usuarios) {
      if(usuario.getPerfil() != null && usuario.getPerfil().getNombre().equals("Gestor"))
        usuariosDTO.add(usuarioMapper.parse(usuario));
    }
    return usuariosDTO;
  }

  @Override
  public List<UsuarioDTO> getGestoresByGrupoAndArea(Integer idGrupo, Integer idArea) throws NotFoundException {
      List<Usuario> usuarios;

      if (idGrupo != null && idArea != null)
        usuarios = usuarioRepository.findByGrupoUsuariosIdAndAreasId(idGrupo,idArea); // busca por id perfil y por id area
      else
        usuarios = usuarioRepository.findAll(); //  entendemos que el front los quiere todos

      List<UsuarioDTO> usuariosDTO = new ArrayList<>();
      for (Usuario usuario : usuarios) {
        if(usuario.getPerfil() != null && usuario.getPerfil().getNombre().equals("Gestor"))
          usuariosDTO.add(usuarioMapper.parse(usuario));
      }
      return usuariosDTO;
  }

  @Override
  public List<AreaUsuarioOutputDTO> getAreasByGrupo(Integer idGrupo) throws NotFoundException {
    List<AreaUsuario> areas = areaUsuarioRepository.findByGruposId(idGrupo);
    List<AreaUsuarioOutputDTO> result = areas.stream().filter(AreaUsuario::getActivo).map(AreaUsuarioOutputDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public List<AreaUsuarioOutputDTO> getAreasByUsuario(Integer idUsuario) throws NotFoundException {
    List<AreaUsuario> areas = areaUsuarioRepository.findByUsuarioId(idUsuario);
    List<AreaUsuarioOutputDTO> result = areas.stream().filter(AreaUsuario::getActivo).map(AreaUsuarioOutputDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public List<UsuarioDTO> getGestoresByArea(Integer idArea)
    throws NotFoundException {
    List<Usuario> usuarios;

    if (idArea != null)
      usuarios = usuarioRepository.findByAreasId(idArea); // busca por id perfil y por id area
    else
      usuarios = usuarioRepository.findAll(); //  entendemos que el front los quiere todos

    List<UsuarioDTO> usuariosDTO = new ArrayList<>();
    for (Usuario usuario : usuarios) {
      if(usuario.getPerfil() != null && usuario.getPerfil().getNombre().equals("Gestor"))
        usuariosDTO.add(usuarioMapper.parse(usuario));
    }
    return usuariosDTO;
  }

  @Override
  public List<GrupoUsuarioOutputDTO> getGruposByArea(Integer idArea) throws NotFoundException {
    List<GrupoUsuario> grupos;
    if(idArea != null)
      grupos = grupoUsuarioRepository.findByAreaUsuarioId(idArea);
    else
      grupos = grupoUsuarioRepository.findAll();

    List<GrupoUsuarioOutputDTO> result = grupos.stream().map(GrupoUsuarioOutputDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public List<GrupoUsuarioOutputDTO> getGruposByUsuario(Integer idUsuario) throws NotFoundException {
    List<GrupoUsuario> grupos;
    if(idUsuario!= null)
      grupos = grupoUsuarioRepository.findByUsuariosId(idUsuario);
    else
      grupos = grupoUsuarioRepository.findAll();

    List<GrupoUsuarioOutputDTO> result = grupos.stream().map(GrupoUsuarioOutputDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public void deleteArea(List<Integer> idsArea) throws Exception {
    for (Integer idarea : idsArea) {
      if (!usuarioRepository.findByAreasId(idarea).isEmpty()) {
        throw new Exception("No se puede eliminar el Area mientras este asignada a algun usuario");
      }
      AreaUsuario areaUs = areaUsuarioRepository.findById(idarea).orElseThrow();
      areaUs.setActivo(Boolean.FALSE);
      areaUsuarioRepository.save(areaUs);
    }
  }
//Pasar
  public void createDocumento(
      Integer idUsuario, String idMatricula,String tipoDocumento, MultipartFile file, Usuario usuario) throws Exception {

    Usuario usuarioDestinatario = usuarioRepository.findById(idUsuario).orElse(null);

    String nombreFichero = file.getOriginalFilename();
    // Sin el arroba y la parte decliente en usuario parametro no necesario
    MetadatoDocumentoInputDTO metadatoDocumentoInputDTO =
        new MetadatoDocumentoInputDTO(nombreFichero, idMatricula);
    try {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
          new MetadatoContenedorInputDTO(usuarioDestinatario.getIdHaya(),"");
      this.servicioGestorDocumental.procesarContenedorUsuario(
          CodigoEntidad.USUARIOS, metadatoContenedorInputDTO);
      long id =
          servicioGestorDocumental
              .procesarDocumento(
                  CodigoEntidad.USUARIOS,
                  usuarioDestinatario.getIdHaya(),
                  file,
                  metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;

      //Usuario usuario es que hace la peticion
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental, usuario, usuarioDestinatario, new Date(), null, nombreFichero, MatriculasEnum.obtenerPorMatricula(idMatricula), null);
      gestorDocumentalRepository.save(gestorDocumental);

    } catch (Exception e) {
      long id =
          servicioGestorDocumental
              .procesarDocumento(
                  CodigoEntidad.USUARIOS,
                  usuarioDestinatario.getIdHaya(),
                  file,
                  metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental, usuario, usuarioDestinatario, new Date(), null, nombreFichero, MatriculasEnum.obtenerPorMatricula(idMatricula), null);
      gestorDocumentalRepository.save(gestorDocumental);
    }
  }

  @Override
  public ListWithCountDTO<DocumentoRepositorioDTO> findDocumentosByUsuario(
    BusquedaRepositorioDTO busquedaRepositorioDTO,
    Integer idUsuarioCreador,
    String idDocumento,
    String usuarioDestino,
    String tipo,
    String nombreDocumento,
    String fechaActualizacion,
    String fechaAlta,
    String orderDirection,
    String orderField,
    Integer size,
    Integer page)
    throws IOException {
    Usuario usuario = usuarioRepository.findById(idUsuarioCreador).orElse(null);
    // String cartera1=cartera.getIdHaya();
    List<DocumentoRepositorioDTO>listadoIdGestor=new ArrayList<>();
    // Obtengo todos los que están registrados con mi usuario como destino

   try{

    if (usuario.getIdHaya() != null) {
       listadoIdGestor =
          this.servicioGestorDocumental
              .listarDocumentos(CodigoEntidad.USUARIOS, usuario.getIdHaya())
              .stream()
              .map(DocumentoRepositorioDTO::new)
              .collect(Collectors.toList());
      }
    //Cuando es usuario emisor
    List<GestorDocumental>caseUserEmisorList=gestorDocumentalRepository.findAllByUsuarioAndUsuarioDestinoIsNotNull(usuario);
    List<DocumentoRepositorioDTO>documentoRepositorioDTOSList=new ArrayList<>();

    for (GestorDocumental gestor:caseUserEmisorList) {

      DocumentoRepositorioDTO doc1=new DocumentoRepositorioDTO();
      doc1.setId(gestor.getId().longValue());
      doc1.setUsuarioCreador(gestor.getUsuario().getNombre());
      doc1.setTipoDocumento(gestor.getTipoDocumento().getTexto());
      doc1.setFechaAlta(gestor.getFechaGeneracion());
      if (gestor.getFechaActualizacion() != null) {
        doc1.setFechaActualizacion(gestor.getFechaActualizacion().toString());
      }
      doc1.setNombreArchivo(gestor.getNombreDocumento());
      documentoRepositorioDTOSList.add(doc1);
    }
    listadoIdGestor.addAll(documentoRepositorioDTOSList);

    List<DocumentoRepositorioDTO>list=gestorDocumentalUseCase.listarDocumentosRepositorio(listadoIdGestor);
    List<DocumentoRepositorioDTO>listbusqueda=gestorDocumentalUseCase.busquedaDocumentoRepositorio(busquedaRepositorioDTO,list);
    List<DocumentoRepositorioDTO>listfiltrada=gestorDocumentalUseCase.filterDocumentoRepositorioDTO(listbusqueda,
      idDocumento,
      null,
      tipo,
      null,
      nombreDocumento,
      fechaActualizacion,
      fechaAlta,
      orderDirection,
      orderField);
    List<DocumentoRepositorioDTO>listFiltradaPaginada=gestorDocumentalUseCase.listarDocumentosRepositorio(listfiltrada).stream().skip(size * page)
      .limit(size)
      .collect(Collectors.toList());

    return new ListWithCountDTO<>(listFiltradaPaginada, listfiltrada.size());
  }catch (Exception ex){
     return new ListWithCountDTO<>(listadoIdGestor,listadoIdGestor.size());
   }


   }
}
