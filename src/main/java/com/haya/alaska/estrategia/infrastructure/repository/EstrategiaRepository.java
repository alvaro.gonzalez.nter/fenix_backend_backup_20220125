package com.haya.alaska.estrategia.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estrategia.domain.Estrategia;
import org.springframework.stereotype.Repository;

@Repository
public interface EstrategiaRepository extends CatalogoRepository<Estrategia, Integer> {}
