package com.haya.alaska.business_plan.domain;

import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class BusinessPlanExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Business Plan Id");
      cabeceras.add("Type");
      cabeceras.add("Customer");
      cabeceras.add("Portfolio");
      cabeceras.add("Asset");
      cabeceras.add("Manager");
      cabeceras.add("Area");
      cabeceras.add("Group");
      cabeceras.add("Start Date");
      cabeceras.add("End Date");
      cabeceras.add("Strategy");
      cabeceras.add("Purchase Price");
      cabeceras.add("Recovered Amount");
      cabeceras.add("Outgoing Amount");
      cabeceras.add("Number of Operations");
      cabeceras.add("Success Rate (%)");
      cabeceras.add("Direct Expenses");
      cabeceras.add("Indirect Expenses");
      cabeceras.add("Net Flow");
      cabeceras.add("Amortization");
      cabeceras.add("Investment multiple");
      cabeceras.add("VAN");
      cabeceras.add("TIR");
      cabeceras.add("BP Date / Targets");
      cabeceras.add("Activity");
    } else {
      cabeceras.add("Id Business Plan");
      cabeceras.add("Tipo");
      cabeceras.add("Cliente");
      cabeceras.add("Cartera");
      cabeceras.add("Activo");
      cabeceras.add("Gestor");
      cabeceras.add("Area");
      cabeceras.add("Grupo");
      cabeceras.add("Fecha Inicio");
      cabeceras.add("Fecha Fin");
      cabeceras.add("Estrategia");
      cabeceras.add("Precio Compra");
      cabeceras.add("Importe Recuperado");
      cabeceras.add("Importe Salida");
      cabeceras.add("Número de Operaciones");
      cabeceras.add("Tasa de Éxito (%)");
      cabeceras.add("Gastos Directos");
      cabeceras.add("Gastos Indirectos");
      cabeceras.add("Flujo Neto");
      cabeceras.add("Amortizacion");
      cabeceras.add("Multiplo Inversion");
      cabeceras.add("VAN");
      cabeceras.add("TIR");
      cabeceras.add("Fecha BP / Objetivos");
      cabeceras.add("Actividad");
    }
  }

  public BusinessPlanExcel(BusinessPlan businessPlan) {
    if (businessPlan == null) return;
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Business Plan Id", businessPlan.getId());
      this.add("Type", businessPlan.getTipoBP());
      this.add(
          "Customer", businessPlan.getCliente() != null ? businessPlan.getCliente().getId() : null);
      this.add(
          "Portfolio",
          businessPlan.getCartera() != null ? businessPlan.getCartera().getId() : null);
      this.add("Asset", null);
      this.add("Manager", null);
      this.add("Area", null);
      this.add("Group", null);
      this.add("Start Date", businessPlan.getFechaInicio());
      this.add("End Date", businessPlan.getFechaFin());
      this.add("Strategy", null);
      this.add("Purchase Price", businessPlan.getPrecioCompra());
      this.add("Recovered Amount", businessPlan.getImporteRecuperado());
      this.add("Outgoing Amount", businessPlan.getImporteSalida());
      this.add("Number of Operations", businessPlan.getNumeroOperacion());
      this.add("Success Rate (%)", businessPlan.getPorcentajeCumplimiento());
      this.add("Direct Expenses", businessPlan.getGastosDirectos());
      this.add("Indirect Expenses", businessPlan.getGastosIndirectos());
      this.add("Net Flow", businessPlan.getFlujoNeto());
      this.add("Amortization", businessPlan.getAmortizacion());
      this.add("Investment multiple", businessPlan.getMultiploInversion());
      this.add("VAN", businessPlan.getVan());
      this.add("TIR", businessPlan.getTir());
      this.add("BP Date / Targets", businessPlan.getFechaObjetivo());
      this.add("Activity", businessPlan.getActividad());
    } else {
      this.add("Id Business Plan", businessPlan.getId());
      this.add("Tipo", businessPlan.getTipoBP());
      this.add(
          "Cliente", businessPlan.getCliente() != null ? businessPlan.getCliente().getId() : null);
      this.add(
          "Cartera", businessPlan.getCartera() != null ? businessPlan.getCartera().getId() : null);
      this.add("Activo", null);
      this.add("Gestor", null);
      this.add("Area", null);
      this.add("Grupo", null);
      this.add("Fecha Inicio", businessPlan.getFechaInicio());
      this.add("Fecha Fin", businessPlan.getFechaFin());
      this.add("Estrategia", null);
      this.add("Precio Compra", businessPlan.getPrecioCompra());
      this.add("Importe Recuperado", businessPlan.getImporteRecuperado());
      this.add("Importe Salida", businessPlan.getImporteSalida());
      this.add("Número de Operaciones", businessPlan.getNumeroOperacion());
      this.add("Tasa de Éxito (%)", businessPlan.getPorcentajeCumplimiento());
      this.add("Gastos Directos", businessPlan.getGastosDirectos());
      this.add("Gastos Indirectos", businessPlan.getGastosIndirectos());
      this.add("Flujo Neto", businessPlan.getFlujoNeto());
      this.add("Amortizacion", businessPlan.getAmortizacion());
      this.add("Multiplo Inversion", businessPlan.getMultiploInversion());
      this.add("VAN", businessPlan.getVan());
      this.add("TIR", businessPlan.getTir());
      this.add("Fecha BP / Objetivos", businessPlan.getFechaObjetivo());
      this.add("Actividad", businessPlan.getActividad());
    }
  }

  public BusinessPlanExcel(
      BusinessPlan businessPlan, BusinessPlanEstrategia businessPlanEstrategia) {
    if (businessPlanEstrategia == null || businessPlan == null) return;
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Business Plan Id", businessPlan.getId());
      this.add("Type", businessPlan.getTipoBP());
      this.add(
          "Customer", businessPlan.getCliente() != null ? businessPlan.getCliente().getId() : null);
      this.add(
          "Portfolio",
          businessPlan.getCartera() != null ? businessPlan.getCartera().getId() : null);
      this.add(
          "Asset",
          businessPlanEstrategia.getBien() != null
              ? businessPlanEstrategia.getBien().getIdCarga()
              : null);
      this.add(
          "Manager",
          businessPlanEstrategia.getGestor() != null
              ? businessPlanEstrategia.getGestor().getEmail()
              : null);
      this.add(
          "Area",
          businessPlanEstrategia.getArea() != null
              ? businessPlanEstrategia.getArea().getId()
              : null);
      this.add(
          "Group",
          businessPlanEstrategia.getGrupo() != null
              ? businessPlanEstrategia.getGrupo().getId()
              : null);
      this.add("Start Date", businessPlanEstrategia.getFechaInicio());
      this.add("End Date", businessPlanEstrategia.getFechaFin());
      this.add(
          "Strategy",
          businessPlanEstrategia.getEstrategia() != null
              ? businessPlanEstrategia.getEstrategia().getValorIngles()
              : null);
      this.add("Purchase Price", businessPlanEstrategia.getPrecioCompra());
      this.add("Recovered Amount", businessPlanEstrategia.getImporteRecuperado());
      this.add("Outgoing Amount", businessPlanEstrategia.getImporteSalida());
      this.add("Number of Operations", businessPlanEstrategia.getNumeroOperacion());
      this.add("Success Rate (%)", businessPlanEstrategia.getPorcentajeCumplimiento());
      this.add("Direct Expenses", businessPlanEstrategia.getGastosDirectos());
      this.add("Indirect Expenses", businessPlanEstrategia.getGastosIndirectos());
      this.add("Net Flow", businessPlanEstrategia.getFlujoNeto());
      this.add("Amortization", businessPlanEstrategia.getAmortizacion());
      this.add("Investment multiple", businessPlanEstrategia.getMultiploInversion());
      this.add("VAN", businessPlanEstrategia.getVan());
      this.add("TIR", businessPlanEstrategia.getTir());
      this.add("BP Date / Targets", businessPlanEstrategia.getFechaObjetivo());
      this.add("Activity", businessPlanEstrategia.getActividad());
    } else {
      this.add("Id Business Plan", businessPlan.getId());
      this.add("Tipo", businessPlan.getTipoBP());
      this.add(
          "Cliente", businessPlan.getCliente() != null ? businessPlan.getCliente().getId() : null);
      this.add(
          "Cartera", businessPlan.getCartera() != null ? businessPlan.getCartera().getId() : null);
      this.add(
          "Activo",
          businessPlanEstrategia.getBien() != null
              ? businessPlanEstrategia.getBien().getIdCarga()
              : null);
      this.add(
          "Gestor",
          businessPlanEstrategia.getGestor() != null
              ? businessPlanEstrategia.getGestor().getEmail()
              : null);
      this.add(
          "Area",
          businessPlanEstrategia.getArea() != null
              ? businessPlanEstrategia.getArea().getId()
              : null);
      this.add(
          "Grupo",
          businessPlanEstrategia.getGrupo() != null
              ? businessPlanEstrategia.getGrupo().getId()
              : null);
      this.add("Fecha Inicio", businessPlanEstrategia.getFechaInicio());
      this.add("Fecha Fin", businessPlanEstrategia.getFechaFin());
      this.add(
          "Estrategia",
          businessPlanEstrategia.getEstrategia() != null
              ? businessPlanEstrategia.getEstrategia().getValor()
              : null);
      this.add("Precio Compra", businessPlanEstrategia.getPrecioCompra());
      this.add("Importe Recuperado", businessPlanEstrategia.getImporteRecuperado());
      this.add("Importe Salida", businessPlanEstrategia.getImporteSalida());
      this.add("Número de Operaciones", businessPlanEstrategia.getNumeroOperacion());
      this.add("Tasa de Éxito (%)", businessPlanEstrategia.getPorcentajeCumplimiento());
      this.add("Gastos Directos", businessPlanEstrategia.getGastosDirectos());
      this.add("Gastos Indirectos", businessPlanEstrategia.getGastosIndirectos());
      this.add("Flujo Neto", businessPlanEstrategia.getFlujoNeto());
      this.add("Amortizacion", businessPlanEstrategia.getAmortizacion());
      this.add("Multiplo Inversion", businessPlanEstrategia.getMultiploInversion());
      this.add("VAN", businessPlanEstrategia.getVan());
      this.add("TIR", businessPlanEstrategia.getTir());
      this.add("Fecha BP / Objetivos", businessPlanEstrategia.getFechaObjetivo());
      this.add("Actividad", businessPlanEstrategia.getActividad());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
