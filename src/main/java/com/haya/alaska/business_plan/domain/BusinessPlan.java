package com.haya.alaska.business_plan.domain;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_BUSINESS_PLAN")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_BUSINESS_PLAN")
public class BusinessPlan implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "NUM_IMPORTE_RECUPERADO")
  private Double importeRecuperado;

  @Column(name = "NUM_IMPORTE_SALIDA")
  private Double importeSalida;

  @Column(name = "NUM_FLUJO_NETO")
  private Double flujoNeto;

  @Column(name = "NUM_AMORTIZACION")
  private Double amortizacion;

  @Column(name = "NUM_MULTIPLO_INVERSION")
  private Double multiploInversion;

  @Column(name = "DES_TIPO_BP")
  private String tipoBP;

  @Column(name = "NUM_PORCENTAJE_CUMPLIMIENTO")
  private Double porcentajeCumplimiento;

  @Column(name = "NUM_NUMERO_OPERACION")
  private Integer numeroOperacion;

  @Column(name = "NUM_TIR")
  private Double tir;

  @Column(name = "FCH_FECHA_OBJETIVO")
  private Date fechaObjetivo;

  @Column(name = "NUM_PRECIO_COMPRA")
  private Double precioCompra;

  @Column(name = "NUM_GASTOS_DIRECTOS")
  private Double gastosDirectos;

  @Column(name = "NUM_GASTOS_INDIRECTOS")
  private Double gastosIndirectos;

  @Column(name = "NUM_VAN")
  private Double van;

  @Column(name = "FCH_FECHA_INICIO")
  private Date fechaInicio;

  @Column(name = "FCH_FECHA_FIN")
  private Date fechaFin;

  @Column(name = "DES_ACTIVIDAD")
  private String actividad;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLIENTE")
  private Cliente cliente;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "ID_BUSINESS_PLAN")
  @NotAudited
  private Set<BusinessPlanEstrategia> businessPlanEstrategias = new HashSet<>();

  public void update(BusinessPlan businessPlan) {
    if (businessPlan.getImporteRecuperado() != null)
      this.setImporteRecuperado(businessPlan.getImporteRecuperado());
    if (businessPlan.getImporteSalida() != null)
      this.setImporteSalida(businessPlan.getImporteSalida());
    if (businessPlan.getFlujoNeto() != null) this.setFlujoNeto(businessPlan.getFlujoNeto());
    if (businessPlan.getAmortizacion() != null)
      this.setAmortizacion(businessPlan.getAmortizacion());
    if (businessPlan.getMultiploInversion() != null)
      this.setMultiploInversion(businessPlan.getMultiploInversion());
    if (businessPlan.getTipoBP() != null) this.setTipoBP(businessPlan.getTipoBP());
    if (businessPlan.getTir() != null) this.setTir(businessPlan.getTir());
    if (businessPlan.getPorcentajeCumplimiento() != null)
      this.setPorcentajeCumplimiento(businessPlan.getPorcentajeCumplimiento());
    if (businessPlan.getFechaInicio() != null) this.setFechaInicio(businessPlan.getFechaInicio());
    if (businessPlan.getFechaFin() != null) this.setFechaFin(businessPlan.getFechaFin());
    if (businessPlan.getPrecioCompra() != null)
      this.setPrecioCompra(businessPlan.getPrecioCompra());
    if (businessPlan.getFechaObjetivo() != null)
      this.setFechaObjetivo(businessPlan.getFechaObjetivo());
    if (businessPlan.getGastosDirectos() != null)
      this.setGastosDirectos(businessPlan.getGastosDirectos());
    if (businessPlan.getGastosIndirectos() != null)
      this.setGastosIndirectos(businessPlan.getGastosIndirectos());
    if (businessPlan.getVan() != null) this.setVan(businessPlan.getVan());
    if (businessPlan.getActividad() != null) this.setActividad(businessPlan.getActividad());
    if (businessPlan.getNumeroOperacion() != null)
      this.setNumeroOperacion(businessPlan.getNumeroOperacion());
    if (businessPlan.getCartera() != null) this.setCartera(businessPlan.getCartera());
    if (businessPlan.getCliente() != null) this.setCliente(businessPlan.getCliente());
    if (businessPlan.getBusinessPlanEstrategias() != null
        && !businessPlan.getBusinessPlanEstrategias().isEmpty())
      this.setBusinessPlanEstrategias(businessPlan.getBusinessPlanEstrategias());
  }

  public void addBusinessPlanEstrategia(BusinessPlanEstrategia businessPlanEstrategia) {
    if (this.businessPlanEstrategias == null) this.businessPlanEstrategias = new HashSet<>();
    this.businessPlanEstrategias.add(businessPlanEstrategia);
  }

  public boolean comprobarBusinessPlan(BusinessPlan businessPlan) {
    if (businessPlan.getCartera().equals(this.getCartera())
        && businessPlan.getCliente().equals(this.getCliente())
        && businessPlan.getTipoBP().equals(this.getTipoBP())
        && businessPlan.getFechaObjetivo().compareTo(this.getFechaObjetivo()) == 0) return true;
    return false;
  }

  public void addEstrategia(
      Estrategia estrategia, Bien bien, AreaUsuario area, GrupoUsuario grupo, Usuario usuario) {
    BusinessPlanEstrategia businessPlanEstrategia = new BusinessPlanEstrategia();
    businessPlanEstrategia.setEstrategia(estrategia);
    businessPlanEstrategia.setImporteRecuperado(this.getImporteRecuperado());
    businessPlanEstrategia.setImporteSalida(this.getImporteSalida());
    businessPlanEstrategia.setFlujoNeto(this.getFlujoNeto());
    businessPlanEstrategia.setAmortizacion(this.getAmortizacion());
    businessPlanEstrategia.setMultiploInversion(this.getMultiploInversion());
    businessPlanEstrategia.setPorcentajeCumplimiento(this.getPorcentajeCumplimiento());
    businessPlanEstrategia.setNumeroOperacion(this.getNumeroOperacion());
    businessPlanEstrategia.setTir(this.getTir());
    businessPlanEstrategia.setFechaObjetivo(this.getFechaObjetivo());
    businessPlanEstrategia.setPrecioCompra(this.getPrecioCompra());
    businessPlanEstrategia.setGastosDirectos(this.getGastosDirectos());
    businessPlanEstrategia.setGastosIndirectos(this.getGastosIndirectos());
    businessPlanEstrategia.setVan(this.getVan());
    businessPlanEstrategia.setFechaInicio(this.getFechaInicio());
    businessPlanEstrategia.setFechaFin(this.getFechaFin());
    businessPlanEstrategia.setActividad(this.getActividad());
    businessPlanEstrategia.setBien(bien);
    businessPlanEstrategia.setArea(area);
    businessPlanEstrategia.setGrupo(grupo);
    businessPlanEstrategia.setGestor(usuario);
    this.addBusinessPlanEstrategia(businessPlanEstrategia);
  }

  public void calcularSumatorios() {
    this.numeroOperacion = 0;
    this.importeSalida = 0.0;
    this.importeRecuperado = 0.0;
    this.flujoNeto = 0.0;
    this.amortizacion = 0.0;
    this.porcentajeCumplimiento = 0.0;
    this.tir = 0.0;
    this.precioCompra = 0.0;
    this.gastosIndirectos = 0.0;
    this.gastosDirectos = 0.0;
    this.van = 0.0;
    this.multiploInversion = 0.0;
    for (BusinessPlanEstrategia businessPlanEstrategia : this.businessPlanEstrategias) {
      if (businessPlanEstrategia.getNumeroOperacion() != null)
        this.numeroOperacion += businessPlanEstrategia.getNumeroOperacion();
      if (businessPlanEstrategia.getImporteSalida() != null)
        this.importeSalida += businessPlanEstrategia.getImporteSalida();
      if (businessPlanEstrategia.getImporteRecuperado() != null)
        this.importeRecuperado += businessPlanEstrategia.getImporteRecuperado();
      if (businessPlanEstrategia.getFlujoNeto() != null)
        this.flujoNeto += businessPlanEstrategia.getFlujoNeto();
      if (businessPlanEstrategia.getAmortizacion() != null)
        this.amortizacion += businessPlanEstrategia.getAmortizacion();
      if (businessPlanEstrategia.getPorcentajeCumplimiento() != null)
        this.porcentajeCumplimiento += businessPlanEstrategia.getPorcentajeCumplimiento();
      if (businessPlanEstrategia.getTir() != null) this.tir += businessPlanEstrategia.getTir();
      if (businessPlanEstrategia.getPrecioCompra() != null)
        this.precioCompra += businessPlanEstrategia.getPrecioCompra();
      if (businessPlanEstrategia.getGastosIndirectos() != null)
        this.gastosIndirectos += businessPlanEstrategia.getGastosIndirectos();
      if (businessPlanEstrategia.getGastosDirectos() != null)
        this.gastosDirectos += businessPlanEstrategia.getGastosDirectos();
      if (businessPlanEstrategia.getVan() != null) this.van += businessPlanEstrategia.getVan();
      if (businessPlanEstrategia.getMultiploInversion() != null)
        this.multiploInversion += businessPlanEstrategia.getMultiploInversion();
    }
    this.multiploInversion = this.multiploInversion / businessPlanEstrategias.size();
    this.porcentajeCumplimiento = this.porcentajeCumplimiento / businessPlanEstrategias.size();
    this.tir = this.tir / businessPlanEstrategias.size();
  }
}
