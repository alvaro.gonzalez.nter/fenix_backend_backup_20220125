package com.haya.alaska.business_plan.application;


import com.haya.alaska.business_plan.infrastructure.controller.dto.BusinessPlanInputDTO;
import com.haya.alaska.business_plan.infrastructure.controller.dto.BusinessPlanOutputDTO;
import com.haya.alaska.business_plan.infrastructure.controller.dto.FilterBusinessPlanDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface BusinessPlanUseCase {


  LinkedHashMap<String, List<Collection<?>>> descarga(FilterBusinessPlanDTO filterBusinessPlanDTO) throws Exception;

  boolean cargar(MultipartFile file, boolean guardar) throws Exception;

  void delete(Integer id);

  ResponseEntity<BusinessPlanOutputDTO> update(int id, BusinessPlanInputDTO businessPlanInputDTO) throws Exception;

  ListWithCountDTO<BusinessPlanOutputDTO> findAllBusinessPlan(FilterBusinessPlanDTO filterBusinessPlanDTO) throws NotFoundException;

  List<String> getTipos(
    Integer clienteId, Integer carteraId, String fechaInicio, String fechaFin, Boolean area, Boolean gestor, Boolean grupo, Boolean bien);

}
