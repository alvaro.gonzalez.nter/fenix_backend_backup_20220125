package com.haya.alaska.business_plan.application;

import com.haya.alaska.business_plan.infrastructure.controller.grafica.dto.GraficaOutputDTO;

public interface BusinessPlanGraficaUseCase {
  GraficaOutputDTO getGrafica(
    String tipo, String periodo, Integer estrategia,
    Integer clienteId, Integer carteraId, String fechaInicio, String fechaFin, Boolean area, Boolean gestor, Boolean grupo, String nombreArea,
    String nombreGestor,
    String nombreGrupo, Boolean bien);
}
