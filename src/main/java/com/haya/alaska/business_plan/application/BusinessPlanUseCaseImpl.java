package com.haya.alaska.business_plan.application;

import com.haya.alaska.business_plan.domain.BusinessPlan;
import com.haya.alaska.business_plan.domain.BusinessPlanExcel;
import com.haya.alaska.business_plan.infrastructure.controller.dto.BusinessPlanInputDTO;
import com.haya.alaska.business_plan.infrastructure.controller.dto.BusinessPlanOutputDTO;
import com.haya.alaska.business_plan.infrastructure.controller.dto.FilterBusinessPlanDTO;
import com.haya.alaska.business_plan.infrastructure.mapper.BusinessPlanMapper;
import com.haya.alaska.business_plan.infrastructure.repository.BusinessPlanRepository;
import com.haya.alaska.business_plan.infrastructure.util.BusinessPlanUtil;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.business_plan_estrategia.infrastructure.repository.BusinessPlanEstrategiaRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class BusinessPlanUseCaseImpl implements BusinessPlanUseCase {

  @Autowired BusinessPlanRepository businessPlanRepository;

  @Autowired BusinessPlanUtil businessPlanUtil;

  @Autowired BusinessPlanMapper businessPlanMapper;

  @Autowired BusinessPlanEstrategiaRepository businessPlanEstrategiaRepository;

  @PersistenceContext private EntityManager entityManager;

  @Override
  public LinkedHashMap<String, List<Collection<?>>> descarga(
      FilterBusinessPlanDTO filterBusinessPlanDTO) throws Exception {
    List<BusinessPlan> data = businessPlanUtil.listadoBusinessPlan(filterBusinessPlanDTO);
    if (data.size() > 5000)
      throw new Exception(
          "La exportación seleccionada supera el límite de expedientes permitido (5000)");
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();
    List<Collection<?>> businessPlans = new ArrayList<>();
    businessPlans.add(new ArrayList<>());
    businessPlans.add(
        new ArrayList<>()); // Como quieren updatear hay que hacer dos lineas vacias para que sea
                            // igual que los excels de carga
    businessPlans.add(BusinessPlanExcel.cabeceras);
    for (BusinessPlan businessPlan : data) {
      if (businessPlan.getBusinessPlanEstrategias() != null
          && businessPlan.getBusinessPlanEstrategias().size() > 1) {
        businessPlans.add(new BusinessPlanExcel(businessPlan).getValuesList());
      }
      for (BusinessPlanEstrategia businessPlanEstrategia :
          businessPlan.getBusinessPlanEstrategias()) {
        businessPlans.add(
            new BusinessPlanExcel(businessPlan, businessPlanEstrategia).getValuesList());
      }
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      datos.put("sheet 1", businessPlans);

    } else {

      datos.put("Hoja1", businessPlans);
    }
    return datos;
  }

  @Override
  @Transactional
  public boolean cargar(MultipartFile file, boolean guardar) throws Exception {
    XSSFWorkbook fichero = new XSSFWorkbook(file.getInputStream());
    XSSFSheet bp = fichero.getSheetAt(0);
    List<BusinessPlan> businessPlans = businessPlanUtil.procesar(bp);
    if (guardar) {
      busquedaUpdates(businessPlans, guardar);
      businessPlanRepository.saveAll(businessPlans);
      return true;
    } else {
      boolean respuesta = busquedaUpdates(businessPlans, guardar);
      if (!respuesta) {
        busquedaUpdates(businessPlans, true);
        businessPlanRepository.saveAll(businessPlans);
      }
      return respuesta;
    }
  }

  // Rellena los id de los businessplan que ya existan, devuelve true si algun businessPlan ya
  // existe en base de datos
  @Transactional
  private boolean busquedaUpdates(List<BusinessPlan> businessPlans, boolean guardar) {
    boolean existe = false;
    for (BusinessPlan businessPlan : businessPlans) {
      if (businessPlan.getId() != null) continue;
      List<BusinessPlan> businessPlansBD =
          businessPlanRepository.findByCarteraIdAndClienteId(
              businessPlan.getCartera().getId(), businessPlan.getCliente().getId());
      for (BusinessPlan businessPlanBD : businessPlansBD) {
        if (businessPlan.comprobarBusinessPlan(businessPlanBD)) {
          existe = true;
          businessPlan.setId(businessPlanBD.getId());
          if (guardar)
            businessPlanEstrategiaRepository.deleteAllByBusinessPlanId(businessPlanBD.getId());
        }
      }
    }
    return existe;
  }

  @Transactional
  @Override
  public void delete(Integer id) {
    businessPlanRepository.deleteById(id);
  }

  @Override
  @Transactional
  public ResponseEntity<BusinessPlanOutputDTO> update(
      int id, BusinessPlanInputDTO businessPlanInputDTO) throws Exception {
    BusinessPlan businessPlanUpdate =
        businessPlanRepository
            .findById(id)
            .orElseThrow(() -> new Exception("Business Plan no encontrado con el id:" + id));
    BusinessPlan businessPlanInput = businessPlanMapper.inputDTOtoEntity(businessPlanInputDTO);
    businessPlanUpdate.update(businessPlanInput);
    businessPlanRepository.save(businessPlanUpdate);
    BusinessPlanOutputDTO businessPlanOutputDTO =
        businessPlanMapper.entityToOutputDTO(businessPlanUpdate);
    return new ResponseEntity<>(businessPlanOutputDTO, HttpStatus.OK);
  }

  @Override
  public ListWithCountDTO<BusinessPlanOutputDTO> findAllBusinessPlan(
      FilterBusinessPlanDTO filterBusinessPlanDTO) {
    List<BusinessPlan> businessPlansSinPaginar =
        businessPlanUtil.listadoBusinessPlan(filterBusinessPlanDTO);
    List<BusinessPlan> businessPlans =
        businessPlansSinPaginar.stream()
            .skip(filterBusinessPlanDTO.getSize() * filterBusinessPlanDTO.getPage())
            .limit(filterBusinessPlanDTO.getSize())
            .collect(Collectors.toList());
    List<BusinessPlanOutputDTO> businessPlanOutputDTOList = new ArrayList<>();
    for (BusinessPlan businessPlan : businessPlans) {
      BusinessPlanOutputDTO businessPlanOutputDTO =
          businessPlanMapper.entityToOutputDTO(businessPlan);
      businessPlanOutputDTOList.add(businessPlanOutputDTO);
    }
    return new ListWithCountDTO<>(businessPlanOutputDTOList, businessPlansSinPaginar.size());
  }

  @Override
  public List<String> getTipos(
      Integer clienteId,
      Integer carteraId,
      String fechaInicio,
      String fechaFin,
      Boolean area,
      Boolean gestor,
      Boolean grupo,
      Boolean bien) {
    List<BusinessPlan> businessPlanList =
        businessPlanUtil.listadoBusinessPlan(
            clienteId,
            carteraId,
            fechaInicio,
            fechaFin,
            area,
            gestor,
            grupo,
            null,
            null,
            null,
            bien);
    List<String> tipos = new ArrayList<>();
    for (BusinessPlan businessPlan : businessPlanList)
      if (!tipos.contains(businessPlan.getTipoBP())) tipos.add(businessPlan.getTipoBP());
    return tipos;
  }
}
