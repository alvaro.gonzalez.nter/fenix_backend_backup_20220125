package com.haya.alaska.business_plan.application;

import com.haya.alaska.business_plan.domain.BusinessPlan;
import com.haya.alaska.business_plan.infrastructure.controller.grafica.dto.*;
import com.haya.alaska.business_plan.infrastructure.mapper.BusinessPlanMapper;
import com.haya.alaska.business_plan.infrastructure.repository.BusinessPlanRepository;
import com.haya.alaska.business_plan.infrastructure.util.BusinessPlanUtil;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.business_plan_estrategia.infrastructure.repository.BusinessPlanEstrategiaRepository;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BusinessPlanGraficaUseCaseImpl implements BusinessPlanGraficaUseCase {

  @Autowired
  BusinessPlanRepository businessPlanRepository;

  @Autowired
  BusinessPlanEstrategiaRepository businessPlanEstrategiaRepository;

  @Autowired
  BusinessPlanMapper businessPlanMapper;
  @Autowired
  BusinessPlanUtil businessPlanUtil;
  @Autowired
  ExpedienteUtil expedienteUtil;


  public GraficaCarteraDTO getGraficaCartera(Integer clienteId, Integer carteraId, Integer periodo, Date fechaInicio, Date fechaFin, Integer estrategiaId, Usuario usuario) {

    //TODO
    /**
     * Periodo inicial a Mensual
     * En caso de no chequear fecha inicio y ficha fin se muestra todos los datos de la base de datos
     *
     * El metodo devuelve:
     * Un listado de labels que determina cada punto
     * Listado de datos que en este servicio son dos - Importe Salida e Importe Recuperado
     */

    //TODO
    // Faltaria filtrar por la estrategia del selector de la gráfica

    List<BusinessPlan> listBusinessPlanCartera = new ArrayList<>();

    /*if(fechaInicio != null && fechaFin != null) {
    // Obtenemos el listado de Business Plan de Cartera y Cliente de un rango de fechas entre la Fecha de Inicio y la Fecha Fin
    listBusinessPlanCartera = businessPlanRepository.findByBetweenFechaInicioFechaFin(fechaInicio, fechaFin)
      .stream()
      .filter(cartera -> cartera.getCartera().getId() == carteraId)
      .filter(cliente -> cliente.getCliente().getId() == clienteId)
      //.filter(estrategia -> estrategia.getBusinessPlanEstrategias().stream().allMatch(id -> id.getEstrategia().getId() == estrategiaId))
      .collect(Collectors.toList());
    } else {
	listBusinessPlanCartera = businessPlanRepository.findAll();
    }*/

    /*
    for (BusinessPlan businessPlan : listBusinessPlanCartera) {
	businessPlan.getBusinessPlanEstrategias().stream().
	map(i -> i.getEstrategia().getId() == estrategiaId)
	.collect(Collectors.toList());
    }
    */

    List<Double> importeSalida = new ArrayList<>();
    List<Double> importeRecuperado = new ArrayList<>();

    Double sumImporteSalida = 0.0;
    Double sumImporteRecuperado = 0.0;
    for (BusinessPlan businessPlan : listBusinessPlanCartera) {
      Set<BusinessPlanEstrategia> listBusinessPlanEstrategia = businessPlan.getBusinessPlanEstrategias();
      for (BusinessPlanEstrategia businessPlanEstrategia : listBusinessPlanEstrategia) {
        sumImporteSalida += businessPlanEstrategia.getImporteSalida();
        sumImporteRecuperado += businessPlanEstrategia.getImporteRecuperado();
      }
    }

    //TODO
    // Faltaria desglosar la salida en virtud del periodo.
    // Tenemos el rango de fecha inicio-fin pero habría que coger el periodo para darle el formato final

    sumImporteSalida = sumImporteSalida / periodo;
    sumImporteRecuperado = sumImporteRecuperado / periodo;

    importeSalida.add(sumImporteSalida);
    importeRecuperado.add(sumImporteRecuperado);

    DataGraficaCarteraDTO data = new DataGraficaCarteraDTO();
    data.setListImporteSalida(importeSalida);
    data.setListImporteRecuperado(importeRecuperado);

    // Falta añadir el listado de labels
    GraficaCarteraDTO graficaCarteraDTO = new GraficaCarteraDTO();
    graficaCarteraDTO.setLabel(null);
    graficaCarteraDTO.setData(data);

    return graficaCarteraDTO;
  }

  public GraficaActivoDTO getGraficaActivo(
    Integer clienteId, Integer bienId, Integer periodo, Date fechaInicio, Date fechaFin, Integer estrategia, Usuario usuario) {

    //TODO
    // Faltaria filtrar por la estrategia del selector de la gráfica

    // Obtenemos el listado de Business Plan de Activo y Cliente de un rango de fechas entre la Fecha de Inicio y la Fecha Fin
    List<BusinessPlan> listBusinessPlanCartera = new ArrayList<>();

    /*if(fechaInicio != null && fechaFin != null) {
    listBusinessPlanCartera = businessPlanRepository.findByBetweenFechaInicioFechaFin(fechaInicio, fechaFin)
      .stream()
      .filter(cartera -> cartera.getBien().getId() == bienId)
      .filter(cliente -> cliente.getCliente().getId() == clienteId)
      .collect(Collectors.toList());
    } else {
	listBusinessPlanCartera = businessPlanRepository.findAll();
    }*/

    /*
    for (BusinessPlan businessPlan : listBusinessPlanCartera) {
	businessPlan.getBusinessPlanEstrategias().stream().
	map(i -> i.getEstrategia().getId() == estrategiaId)
	.collect(Collectors.toList());
    }
    */

    List<Double> listImporteSalida = new ArrayList<>();
    List<Double> listImporteRecuperado = new ArrayList<>();
    List<Double> listImporteCompra = new ArrayList<>();
    List<Double> listGastos = new ArrayList<>();
    List<Double> listVan = new ArrayList<>();

    Double sumImporteSalida = 0.0;
    Double sumImporteRecuperado = 0.0;
    Double sumImporteCompra = 0.0;
    Double sumGastos = 0.0;
    Double sumVan = 0.0;

    for (BusinessPlan businessPlan : listBusinessPlanCartera) {
      Set<BusinessPlanEstrategia> listBusinessPlanEstrategia = businessPlan.getBusinessPlanEstrategias();
      for (BusinessPlanEstrategia businessPlanEstrategia : listBusinessPlanEstrategia) {
        sumImporteSalida += businessPlanEstrategia.getImporteSalida();
        sumImporteRecuperado += businessPlanEstrategia.getImporteRecuperado();
        sumImporteCompra += businessPlanEstrategia.getPrecioCompra();
        sumGastos += businessPlanEstrategia.getGastosDirectos() + businessPlanEstrategia.getGastosIndirectos();
        sumVan += businessPlanEstrategia.getVan();
      }
    }

    // Añadimos las sumatorias
    listImporteSalida.add(sumImporteSalida);
    listImporteRecuperado.add(sumImporteRecuperado);
    listImporteCompra.add(sumImporteCompra);
    listGastos.add(sumGastos);
    listVan.add(sumVan);

    DataGraficaOutputDTO data = new DataGraficaOutputDTO();
    data.setListImporteSalida(listImporteSalida);
    data.setListImporteRecuperado(listImporteRecuperado);
    data.setListPrecioCompra(listImporteCompra);
    data.setListGastos(listGastos);
    data.setListVan(listVan);

    //TODO
    // Faltaria desglosar la salida en virtud del periodo.
    // Tenemos el rango de fecha inicio-fin pero habría que coger el periodo para darle el formato final
    // Agrupar la salida de los datos en virtud del periodo (Ejemplo segun Raul, hacer un groupby de los datos)

    GraficaActivoDTO graficaActivoDTO = new GraficaActivoDTO();
    graficaActivoDTO.setLabel(null);
    graficaActivoDTO.setData(data);

    return graficaActivoDTO;
  }

  @Override
  public GraficaOutputDTO getGrafica(
    String tipo, String periodo, Integer estrategia,
    Integer clienteId, Integer carteraId, String fechaInicioS, String fechaFinS, Boolean area, Boolean gestor, Boolean grupo, String nombreArea, String nombreGestor,
    String nombreGrupo, Boolean bien
  ) {

    List<BusinessPlan> listBP = businessPlanUtil.listadoBusinessPlan(clienteId, carteraId, fechaInicioS,
      fechaFinS, area, gestor, grupo, nombreArea, nombreGestor, nombreGrupo, bien);

    Date fechaInicio = null;
    Date fechaFin = null;
    if (fechaInicioS != null) fechaInicio = new Date(fechaInicioS);
    if (fechaFinS != null) fechaFin = new Date(fechaFinS);


    List<BusinessPlanEstrategia> listBPE = new ArrayList<>();
    if (tipo != null)
      listBP = listBP.stream().filter(bp -> bp.getTipoBP().equals(tipo)).collect(Collectors.toList());
    for (BusinessPlan bp : listBP) {
      if (estrategia != null)
        listBPE.addAll(bp.getBusinessPlanEstrategias().stream().filter(bpe -> {
          return bpe.getEstrategia() != null && bpe.getEstrategia().getId().equals(estrategia);
        }).collect(Collectors.toList()));
      else if (bp.getBusinessPlanEstrategias() != null && !bp.getBusinessPlanEstrategias().isEmpty())
        listBPE.addAll(new ArrayList<>(bp.getBusinessPlanEstrategias()));
      else
        listBPE.add(getSinEstrategia(bp));
    }

    if (fechaInicio == null) {
      BusinessPlanEstrategia bpeIni = businessPlanEstrategiaRepository.findFirstByOrderByFechaInicioAsc();
      fechaInicio = bpeIni.getFechaInicio();
    }
    if (fechaFin == null) {
      BusinessPlanEstrategia bpeFin = businessPlanEstrategiaRepository.findFirstByOrderByFechaFinDesc();
      fechaFin = bpeFin.getFechaFin();
    }
    Map<Calendar, List<BusinessPlanEstrategia>> porPeriodo = new HashMap<>();
    switch (periodo.toUpperCase()) {
      case ("MENSUAL"):
        porPeriodo = obtenerMensual(listBPE, fechaInicio, fechaFin);
        break;
      case ("TRIMESTRAL"):
        porPeriodo = obtenerTrimestral(listBPE, fechaInicio, fechaFin);
        break;
      case ("ANUAL"):
        porPeriodo = obtenerAnual(listBPE, fechaInicio, fechaFin);
        break;
      default:
        break;
    }

    List<String> label = new ArrayList<>();
    List<Double> listImporteSalida = new ArrayList<>();
    List<Double> listImporteRecuperado = new ArrayList<>();
    List<Double> listPrecioCompra = new ArrayList<>();
    List<Double> listGastos = new ArrayList<>();
    List<Double> listVan = new ArrayList<>();
    List<Double> listNOperaciones = new ArrayList<>();
    List<Double> listPorcentaje = new ArrayList<>();

    GraficaOutputDTO output = new GraficaOutputDTO();
    output.setTipo(tipo);
    output.setPeriodo(periodo);
    DataGraficaOutputDTO dataOutput = new DataGraficaOutputDTO();


    for (Map.Entry<Calendar, List<BusinessPlanEstrategia>> entry : porPeriodo.entrySet()) {
      Calendar c = entry.getKey();
      List<BusinessPlanEstrategia> bpeS = entry.getValue();
      switch (periodo.toUpperCase()) {
        case ("MENSUAL"):
          label.add(obtenerLaberMensual(c));
          break;
        case ("TRIMESTRAL"):
          label.add(obtenerLaberTrimestral(c));
          break;
        case ("ANUAL"):
          label.add(obtenerLaberAnual(c));
          break;
        default:
          break;
      }
      listImporteSalida.add(obtenerImporteSalida(bpeS));
      listImporteRecuperado.add(obtenerImporteRecuperado(bpeS));
      listVan.add(obtenerVan(bpeS));
      listGastos.add(obtenerGastos(bpeS));
      listPrecioCompra.add(obtenerPrecioCompra(bpeS));
      listNOperaciones.add(obtenerNumeroOperaciones(bpeS));
      listPorcentaje.add(obtenerPorcentaje(bpeS));
    }

    output.setLabels(label);
    dataOutput.setListImporteSalida(listImporteSalida);
    dataOutput.setListImporteRecuperado(listImporteRecuperado);
    dataOutput.setListGastos(listGastos);
    dataOutput.setListPrecioCompra(listPrecioCompra);
    dataOutput.setListVan(listVan);
    dataOutput.setListNOperaciones(listNOperaciones);
    dataOutput.setListPorcentaje(listPorcentaje);
    output.setData(dataOutput);

    return output;
  }

  private BusinessPlanEstrategia getSinEstrategia(BusinessPlan bp) {
    BusinessPlanEstrategia bpe = new BusinessPlanEstrategia();
    BeanUtils.copyProperties(bp, bpe, "id");

    return bpe;
  }

  private Double obtenerImporteSalida(List<BusinessPlanEstrategia> listBPE) {
    Double result = 0.0;

    for (BusinessPlanEstrategia bpe : listBPE) {
      if (bpe.getImporteSalida() != null) result += bpe.getImporteSalida();
    }

    return result;
  }

  private Double obtenerImporteRecuperado(List<BusinessPlanEstrategia> listBPE) {
    Double result = 0.0;

    for (BusinessPlanEstrategia bpe : listBPE) {
      if (bpe.getImporteRecuperado() != null) result += bpe.getImporteRecuperado();
    }

    return result;
  }

  private Double obtenerGastos(List<BusinessPlanEstrategia> listBPE) {
    Double result = 0.0;

    for (BusinessPlanEstrategia bpe : listBPE) {
      if (bpe.getGastosDirectos() != null) result += bpe.getGastosDirectos();
    }

    return result;
  } //Gastos Directos o Indirectos

  private Double obtenerPrecioCompra(List<BusinessPlanEstrategia> listBPE) {
    Double result = 0.0;

    for (BusinessPlanEstrategia bpe : listBPE) {
      if (bpe.getPrecioCompra() != null) result += bpe.getPrecioCompra();
    }

    return result;
  }

  private Double obtenerVan(List<BusinessPlanEstrategia> listBPE) {
    Double result = 0.0;

    for (BusinessPlanEstrategia bpe : listBPE) {
      if (bpe.getVan() != null) result += bpe.getVan();
    }

    return result;
  }

  private Double obtenerNumeroOperaciones(List<BusinessPlanEstrategia> listBPE) {
    Double result = 0.0;

    for (BusinessPlanEstrategia bpe : listBPE) {
      if (bpe.getNumeroOperacion() != null) result += bpe.getNumeroOperacion();
    }

    return result;
  }

  private Double obtenerPorcentaje(List<BusinessPlanEstrategia> listBPE) {
    Double result = 0.0;

    for (BusinessPlanEstrategia bpe : listBPE) {
      if (bpe.getPorcentajeCumplimiento() != null) result += bpe.getPorcentajeCumplimiento();
    }

    return result;
  }

  private Map<Calendar, List<BusinessPlanEstrategia>> obtenerMensual(List<BusinessPlanEstrategia> listBPE, Date fechaInicio, Date fechaFin) {
    List<Calendar> calendars = obtenerCalendarsMensual(fechaInicio, fechaFin);
    if (calendars.size() == 1) anadirAUno(calendars);

    Map<Calendar, List<BusinessPlanEstrategia>> map = new LinkedHashMap<>();

    for (Calendar c : calendars) {
      Calendar cIni = Calendar.getInstance();
      cIni.setTime(c.getTime());
      cIni.set(Calendar.DATE, 1);
      Date fIni = cIni.getTime();
      if (fIni.before(fechaInicio)) fIni = fechaInicio;

      Calendar cFin = Calendar.getInstance();
      cFin.setTime(c.getTime());
      cFin.add(Calendar.MONTH, 1);
      cFin.set(Calendar.DATE, 1);
      cFin.add(Calendar.DATE, -1);
      Date fFin = cFin.getTime();
      if (fFin.after(fechaFin)) fFin = fechaFin;
      List<BusinessPlanEstrategia> lista = filtratFechas(listBPE, fIni, fFin);
      map.put(c, lista);
    }

    return map;
  }

  private Map<Calendar, List<BusinessPlanEstrategia>> obtenerTrimestral(List<BusinessPlanEstrategia> listBPE, Date fechaInicio, Date fechaFin) {
    List<Calendar> calendars = obtenerCalendarsTrimestral(fechaInicio, fechaFin);
    if (calendars.size() == 1) anadirAUno(calendars);

    Map<Calendar, List<BusinessPlanEstrategia>> map = new LinkedHashMap<>();

    for (Calendar c : calendars) {
      Calendar cIni = Calendar.getInstance();
      cIni.setTime(c.getTime());
      Date fIni = cIni.getTime();
      if (fIni.before(fechaInicio)) fIni = fechaInicio;

      Calendar cFin = Calendar.getInstance();
      cFin.setTime(c.getTime());
      cFin.add(Calendar.MONTH, 3);
      cFin.add(Calendar.DATE, -1);
      Date fFin = cIni.getTime();
      if (fFin.after(fechaFin)) fFin = fechaFin;
      List<BusinessPlanEstrategia> lista = filtratFechas(listBPE, fIni, fFin);
      map.put(c, lista);
    }

    return map;
  }

  private Map<Calendar, List<BusinessPlanEstrategia>> obtenerAnual(List<BusinessPlanEstrategia> listBPE, Date fechaInicio, Date fechaFin) {
    List<Calendar> calendars = obtenerCalendarsAnual(fechaInicio, fechaFin);
    if (calendars.size() == 1) anadirAUno(calendars);

    Map<Calendar, List<BusinessPlanEstrategia>> map = new LinkedHashMap<>();

    for (Calendar c : calendars) {
      Calendar cIni = Calendar.getInstance();
      cIni.setTime(c.getTime());
      cIni.set(Calendar.DATE, 1);
      cIni.set(Calendar.MONTH, 1);
      Date fIni = cIni.getTime();
      if (fIni.before(fechaInicio)) fIni = fechaInicio;

      Calendar cFin = Calendar.getInstance();
      cFin.setTime(c.getTime());
      cFin.add(Calendar.YEAR, 1);
      cFin.set(Calendar.DATE, 1);
      cFin.set(Calendar.MONTH, 1);
      cFin.add(Calendar.DATE, -1);
      Date fFin = cIni.getTime();
      if (fFin.after(fechaFin)) fFin = fechaFin;
      List<BusinessPlanEstrategia> lista = filtratFechas(listBPE, fIni, fFin);
      map.put(c, lista);
    }

    return map;
  }

  private void anadirAUno(List<Calendar> calendars){
    Calendar c = Calendar.getInstance();
    c.setTime(calendars.get(0).getTime());
    int num;
    if (c.get(Calendar.DATE) > 15) num = -1;
    else num = 1;
    c.add(Calendar.DATE, num);
    calendars.add(c);
  }

  private List<BusinessPlanEstrategia> filtratFechas(List<BusinessPlanEstrategia> listBPE, Date fechaInicio, Date fechaFin) {
    List<BusinessPlanEstrategia> result = listBPE.stream().filter(bpe -> {
      Date fIni = bpe.getFechaInicio();
      Date fFin = bpe.getFechaFin();
      return expedienteUtil.comprobarEntre(fechaInicio, fIni, fFin) || expedienteUtil.comprobarEntre(fechaFin, fIni, fFin);
    }).collect(Collectors.toList());

    return result;
  }

  private String obtenerLaberMensual(Calendar c) {
    String[] meses = {
      "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    };
    String[] mesesEng = {
      "January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    };

    String result = "";

    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();

    String mes = "";
    if (defaultLocal.equals("es"))
      mes = meses[c.get(Calendar.MONTH)];
    else if (defaultLocal.equals("en"))
      mes = mesesEng[c.get(Calendar.MONTH)];
    String year = c.get(Calendar.YEAR) + "";
    result = mes + " " + year;

    return result;
  }

  private String obtenerLaberTrimestral(Calendar c) {
    String result = "";

    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();

    Integer mes = c.get(Calendar.MONTH);

    if (defaultLocal.equals("es")){
      if (mes <= 2) {
        result += "Enero-Marzo";
      } else if (mes <= 5) {
        result += "Abril-Junio";
      } else if (mes <= 8) {
        result += "Julio-Septiembre";
      } else if (mes <= 11) {
        result += "Octubre-Diciembre";
      } else {

      }
    }
    else if (defaultLocal.equals("en")){
      if (mes <= 2) {
        result += "January-March";
      } else if (mes <= 5) {
        result += "April-June";
      } else if (mes <= 8) {
        result += "July-September";
      } else if (mes <= 11) {
        result += "October-December";
      } else {

      }
    }

    result += " " + c.get(Calendar.YEAR);

    return result;
  }

  private String obtenerLaberAnual(Calendar c) {
    String result = "";
    result += c.get(Calendar.YEAR);
    return result;
  }

  private void setTimeToZero(Calendar c){
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
  }

  private List<Calendar> obtenerCalendarsMensual(Date fIni, Date fFin) {
    Calendar cFin = Calendar.getInstance();
    cFin.setTime(fFin);
    setTimeToZero(cFin);

    List<Calendar> result = new ArrayList<>();

    for (int i = 0; i < 10000; i++) {
      Calendar cLoop = Calendar.getInstance();
      cLoop.setTime(fIni);
      setTimeToZero(cLoop);
      cLoop.add(Calendar.MONTH, i);
      result.add(cLoop);

      cLoop.add(Calendar.MONTH, 1);
      cLoop.add(Calendar.DATE, -1);

      if (cLoop.after(cFin) || cLoop.compareTo(cFin) == 0) break;
    }

    return result;
  }

  private List<Calendar> obtenerCalendarsTrimestral(Date fIni, Date fFin) {
    Calendar cFin = Calendar.getInstance();
    cFin.setTime(fFin);
    setTimeToZero(cFin);

    List<Calendar> result = new ArrayList<>();

    for (int i = 0; i < 10000; i += 3) {
      Calendar cLoop = Calendar.getInstance();
      cLoop.setTime(fIni);
      Integer mes = cLoop.get(Calendar.MONTH);
      if (mes <= 2) cLoop.set(Calendar.MONTH, 0);
      else if (mes <= 5) cLoop.set(Calendar.MONTH, 3);
      else if (mes <= 8) cLoop.set(Calendar.MONTH, 6);
      else if (mes <= 11) cLoop.set(Calendar.MONTH, 8);
      else {

      }
      setTimeToZero(cLoop);
      cLoop.add(Calendar.MONTH, i);
      result.add(cLoop);

      cLoop.add(Calendar.MONTH, 3);
      cLoop.add(Calendar.DATE, -1);

      if (cLoop.after(cFin) || cLoop.compareTo(cFin) == 0) break;
    }

    return result;
  }

  private List<Calendar> obtenerCalendarsAnual(Date fIni, Date fFin) {
    Calendar cFin = Calendar.getInstance();
    cFin.setTime(fFin);
    setTimeToZero(cFin);

    List<Calendar> result = new ArrayList<>();

    for (int i = 0; i < 10000; i++) {
      Calendar cLoop = Calendar.getInstance();
      cLoop.setTime(fIni);
      setTimeToZero(cLoop);
      cLoop.add(Calendar.YEAR, i);
      result.add(cLoop);

      cLoop.add(Calendar.YEAR, 1);
      cLoop.add(Calendar.DATE, -1);

      if (cLoop.after(cFin) || cLoop.compareTo(cFin) == 0) break;
    }

    return result;
  }
}
