package com.haya.alaska.business_plan.infrastructure.controller.grafica.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class GraficaCarteraDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String periodo;
  private Date fechaInicio;
  private Date fechaFin;
  static final String IMPORTE_SALIDA = "IMPORTE SALIDA";
  static final String IMPORTE_RECUPERADO = "IMPORTE RECUPERADO";
  private List<String> label;
  private DataGraficaCarteraDTO data;

}
