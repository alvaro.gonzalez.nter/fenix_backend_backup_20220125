package com.haya.alaska.business_plan.infrastructure.controller.grafica.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class DataGraficaCarteraDTO implements Serializable {
    
  private static final long serialVersionUID = 1L;
  
  private List<Double> listImporteSalida;
  private List<Double> listImporteRecuperado;
  
}
