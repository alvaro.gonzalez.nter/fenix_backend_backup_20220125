package com.haya.alaska.business_plan.infrastructure.repository;

import com.haya.alaska.business_plan.domain.BusinessPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BusinessPlanRepository extends JpaRepository<BusinessPlan, Integer> {
  List<BusinessPlan> findByCarteraIdAndClienteId(Integer cartera, Integer cliente);
  void deleteByIdIn(List<Integer> listIds);
}

