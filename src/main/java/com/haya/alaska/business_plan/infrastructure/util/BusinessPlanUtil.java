package com.haya.alaska.business_plan.infrastructure.util;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.area_usuario.domain.AreaUsuario_;
import com.haya.alaska.area_usuario.infrastructure.repository.AreaUsuarioRepository;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.Bien_;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.business_plan.domain.BusinessPlan;
import com.haya.alaska.business_plan.domain.BusinessPlan_;
import com.haya.alaska.business_plan.infrastructure.controller.dto.FilterBusinessPlanDTO;
import com.haya.alaska.business_plan.infrastructure.repository.BusinessPlanRepository;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia_;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.cliente.domain.Cliente_;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario_;
import com.haya.alaska.grupo_usuario.infrastructure.repository.GrupoUsuarioRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Component
public class BusinessPlanUtil {

  @PersistenceContext
  private EntityManager entityManager;

  @Autowired
  EstrategiaRepository estrategiaRepository;
  @Autowired
  BienRepository bienRepository;
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  AreaUsuarioRepository areaUsuarioRepository;
  @Autowired
  UsuarioRepository usuarioRepository;
  @Autowired
  GrupoUsuarioRepository grupoUsuarioRepository;
  @Autowired
  ClienteRepository clienteRepository;
  @Autowired
  BusinessPlanRepository businessPlanRepository;

  public List<BusinessPlan> procesar(XSSFSheet hoja) throws Exception {
    List<BusinessPlan> dev = new ArrayList<>();
    HashMap<String, Integer> map = new HashMap<>();
    fillMap(map);
    XSSFRow filaActual;
    BusinessPlan businessPlan = null;
    boolean sumatorio = false;
    for (int i = 3; i <= hoja.getPhysicalNumberOfRows(); i++) {
      filaActual = hoja.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValueConGuion(filaActual, map.get("Cliente"));
      }
      if (null == hasContent || "".equals(hasContent)) {
        if (sumatorio) businessPlan.calcularSumatorios();
        break;
      }

      Integer id = getIntegerValueConGuion(filaActual, map.get("Id Business Plan"));
      Integer idCliente = getIntegerValueConGuion(filaActual, map.get("Cliente"));
      Integer idCartera = getIntegerValueConGuion(filaActual, map.get("Cartera"));
      String emailGestor = getRawStringValueConGuion(filaActual, map.get("Gestor"));
      Integer idArea = getIntegerValueConGuion(filaActual, map.get("Area"));
      Integer idGrupo = getIntegerValueConGuion(filaActual, map.get("Grupo"));
      String idBien = getRawStringValueConGuion(filaActual, map.get("Activo"));
      Date fechaInicio = getDateValue(filaActual, map.get("Fecha Inicio"));
      Date fechaFin = getDateValue(filaActual, map.get("Fecha Fin"));
      String estrategia = getRawStringValueConGuion(filaActual, map.get("Estrategia"));
      Double precioCompra = getNumericValueConGuion(filaActual, map.get("Precio Compra"));
      Double importeRecuperado = getNumericValueConGuion(filaActual, map.get("Importe Recuperado"));
      Double importeSalida = getNumericValueConGuion(filaActual, map.get("Importe Salida"));
      Integer numeroOperaciones = getIntegerValueConGuion(filaActual, map.get("Número de Operaciones"));
      Double porcentajeCumplimiento = getNumericValueConGuion(filaActual, map.get("Tasa de Éxito (%)"));
      Double gastosDirectos = getNumericValueConGuion(filaActual, map.get("Gastos Directos"));
      Double gastosIndirectos = getNumericValueConGuion(filaActual, map.get("Gastos Indirectos"));
      Double flujoNeto = getNumericValueConGuion(filaActual, map.get("Flujo Neto"));
      Double amortizacion = getNumericValueConGuion(filaActual, map.get("Amortizacion"));
      Double multiploInversion = getNumericValueConGuion(filaActual, map.get("Multiplo Inversion"));
      Double van = getNumericValueConGuion(filaActual, map.get("VAN"));
      Double tir = getNumericValueConGuion(filaActual, map.get("TIR"));
      Date fechaBP = getDateValue(filaActual, map.get("Fecha BP / Objetivos"));
      String actividad = getRawStringValueConGuion(filaActual, map.get("Actividad"));
      String tipoBP = getRawStringValueConGuion(filaActual, map.get("Tipo"));

      if (fechaBP == null || idCartera == null || fechaInicio == null || fechaFin == null || tipoBP == null || idCliente == null || importeRecuperado == null || importeSalida == null || fechaBP == null)
        throw new Exception("Deben rellenarse todos los campos obligatorios.");

      if (!comprobarBusiness(businessPlan, filaActual, map)) {
        if (sumatorio) businessPlan.calcularSumatorios();
        sumatorio = false;
        if (id != null)
          businessPlan = businessPlanRepository.findById(id).orElseThrow(() -> new Exception("Business Plan no encontrado con el id: " + id));
        else
          businessPlan = new BusinessPlan();

        businessPlan.setFechaInicio(fechaInicio);
        businessPlan.setFechaFin(fechaFin);
        businessPlan.setImporteRecuperado(importeRecuperado);
        businessPlan.setImporteSalida(importeSalida);
        businessPlan.setPorcentajeCumplimiento(porcentajeCumplimiento);
        businessPlan.setGastosDirectos(gastosDirectos);
        businessPlan.setGastosIndirectos(gastosIndirectos);
        businessPlan.setFlujoNeto(flujoNeto);
        businessPlan.setAmortizacion(amortizacion);
        businessPlan.setMultiploInversion(multiploInversion);
        businessPlan.setVan(van);
        businessPlan.setTir(tir);
        businessPlan.setFechaObjetivo(fechaBP);
        businessPlan.setActividad(actividad);
        businessPlan.setPrecioCompra(precioCompra);
        businessPlan.setNumeroOperacion(numeroOperaciones);
        businessPlan.setTipoBP(tipoBP);
        businessPlan.setCliente(clienteRepository.findById(idCliente).orElseThrow(() -> new Exception("Cliente no encontrado con el id: " + idCliente)));
        businessPlan.setCartera(carteraRepository.findById(idCartera).orElseThrow(() -> new Exception("Cartera no encontrado con el id: " + idCartera)));


        //Si no pertenece al anterior registro y tiene estrategia, se crea un BusinessPlan con el businessPlan estrategia dentro.
        if (estrategia != null) {
          sumatorio = true;
          Estrategia estrategiaBP = estrategiaRepository.findByCodigo(estrategia).orElseThrow(() -> new Exception("Estrategia no encontrada con el codigo: " + estrategia));
          Usuario usuario = null;
          AreaUsuario area = null;
          GrupoUsuario grupo = null;
          Bien bien = null;
          if (emailGestor != null)
            usuario = usuarioRepository.findByEmail(emailGestor).orElseThrow(() -> new Exception("Gestor no encontrado con el email: " + emailGestor));
          if (idArea != null)
            area = areaUsuarioRepository.findById(idArea).orElseThrow(() -> new Exception("Area no encontrado con el id: " + idArea));
          if (idGrupo != null)
            grupo = grupoUsuarioRepository.findById(idGrupo).orElseThrow(() -> new Exception("Grupo no encontrado con el id: " + idGrupo));
          if (idBien != null)
            bien = bienRepository.findByIdCarga(idBien).orElseThrow(() -> new Exception("Activo no encontrado con el id: " + idBien));

          businessPlan.addEstrategia(estrategiaBP, bien, area, grupo, usuario);
        }
        dev.add(businessPlan);
      } else {
        BusinessPlanEstrategia businessPlanEstrategia = new BusinessPlanEstrategia();
        if (id != null)
          businessPlanEstrategia.setBusinessPlan(businessPlanRepository.findById(id).orElseThrow(() -> new Exception("Business Plan no encontrado con el id: " + id)));

        businessPlanEstrategia.setFechaInicio(fechaInicio);
        businessPlanEstrategia.setFechaFin(fechaFin);
        businessPlanEstrategia.setImporteRecuperado(importeRecuperado);
        businessPlanEstrategia.setImporteSalida(importeSalida);
        businessPlanEstrategia.setPorcentajeCumplimiento(porcentajeCumplimiento);
        businessPlanEstrategia.setGastosDirectos(gastosDirectos);
        businessPlanEstrategia.setGastosIndirectos(gastosIndirectos);
        businessPlanEstrategia.setFlujoNeto(flujoNeto);
        businessPlanEstrategia.setAmortizacion(amortizacion);
        businessPlanEstrategia.setMultiploInversion(multiploInversion);
        businessPlanEstrategia.setVan(van);
        businessPlanEstrategia.setTir(tir);
        businessPlanEstrategia.setFechaObjetivo(fechaBP);
        businessPlanEstrategia.setActividad(actividad);
        businessPlanEstrategia.setPrecioCompra(precioCompra);
        businessPlanEstrategia.setNumeroOperacion(numeroOperaciones);
        if (emailGestor != null)
          businessPlanEstrategia.setGestor(usuarioRepository.findByEmail(emailGestor).orElseThrow(() -> new Exception("Gestor no encontrado con el email: " + emailGestor)));
        if (idArea != null)
          businessPlanEstrategia.setArea(areaUsuarioRepository.findById(idArea).orElseThrow(() -> new Exception("Area no encontrado con el id: " + idArea)));
        if (idGrupo != null)
          businessPlanEstrategia.setGrupo(grupoUsuarioRepository.findById(idGrupo).orElseThrow(() -> new Exception("Grupo no encontrado con el id: " + idGrupo)));
        if (idBien != null)
          businessPlanEstrategia.setBien(bienRepository.findByIdCarga(idBien).orElseThrow(() -> new Exception("Activo no encontrado con el id: " + idBien)));
        businessPlanEstrategia.setEstrategia(estrategiaRepository.findByCodigo(estrategia).orElseThrow(() -> new Exception("Estrategia no encontrada con el codigo: " + estrategia)));

        businessPlan.addBusinessPlanEstrategia(businessPlanEstrategia);
        businessPlanEstrategia.setBusinessPlan(businessPlan);
      }
    }
    return dev;
  }

  //Comprueba si la lineaActual es un nuevo businessPlan o es la estrategia de otro businessPlan
  private boolean comprobarBusiness(BusinessPlan businessPlan, XSSFRow filaActual, HashMap<String, Integer> map) throws NotFoundException {
    if (businessPlan == null) return false;
    if (businessPlan.getId() != null && businessPlan.getId() == getIntegerValueConGuion(filaActual, map.get("Id Business Plan")))
      return true;
    Integer idCliente = getIntegerValueConGuion(filaActual, map.get("Cliente"));
    Integer idCartera = getIntegerValueConGuion(filaActual, map.get("Cartera"));
    String tipo = getRawStringValueConGuion(filaActual, map.get("Tipo"));
    Date fechaBP = getDateValue(filaActual, map.get("Fecha BP / Objetivos"));
    if (businessPlan.getCliente().getId() == idCliente && businessPlan.getCartera().getId() == idCartera && businessPlan.getTipoBP().equals(tipo) && businessPlan.getFechaObjetivo().equals(fechaBP)) {
      return true;
    }
    return false;
  }

  private void fillMap(HashMap<String, Integer> map) {
    int i = 0;
    map.put("Id Business Plan", i++);
    map.put("Tipo", i++);
    map.put("Cliente", i++);
    map.put("Cartera", i++);
    map.put("Activo", i++);
    map.put("Gestor", i++);
    map.put("Area", i++);
    map.put("Grupo", i++);
    map.put("Fecha Inicio", i++);
    map.put("Fecha Fin", i++);
    map.put("Estrategia", i++);
    map.put("Precio Compra", i++);
    map.put("Importe Recuperado", i++);
    map.put("Importe Salida", i++);
    map.put("Número de Operaciones", i++);
    map.put("Tasa de Éxito (%)", i++);
    map.put("Gastos Directos", i++);
    map.put("Gastos Indirectos", i++);
    map.put("Flujo Neto", i++);
    map.put("Amortizacion", i++);
    map.put("Multiplo Inversion", i++);
    map.put("VAN", i++);
    map.put("TIR", i++);
    map.put("Fecha BP / Objetivos", i++);
    map.put("Actividad", i++);
  }

  public List<BusinessPlan> listadoBusinessPlan(Integer clienteId, Integer carteraId, String fechaInicioS,
                                                String fechaFinS, boolean area, boolean gestor, boolean grupo, String nombreArea, String nombreGestor, String nombreGrupo, boolean bien) {

    FilterBusinessPlanDTO filterBusinessPlanDTO = new FilterBusinessPlanDTO();
    filterBusinessPlanDTO.setClienteId(clienteId);
    filterBusinessPlanDTO.setCarteraId(carteraId);
    filterBusinessPlanDTO.setFechaInicio(fechaInicioS != null ? new Date(fechaInicioS) : null);
    filterBusinessPlanDTO.setFechaFin(fechaFinS != null ? new Date(fechaFinS) : null);
    filterBusinessPlanDTO.setArea(area);
    filterBusinessPlanDTO.setGestor(gestor);
    filterBusinessPlanDTO.setGrupo(grupo);
    filterBusinessPlanDTO.setNombreArea(nombreArea);
    filterBusinessPlanDTO.setNombreGestor(nombreGestor);
    filterBusinessPlanDTO.setNombreGrupo(nombreGrupo);
    filterBusinessPlanDTO.setBien(bien);
    return listadoBusinessPlan(filterBusinessPlanDTO);
  }

  public List<BusinessPlan> listadoPortalCliente(Integer carteraId, Date fechaIni, Date fechaFin){
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<BusinessPlan> query = cb.createQuery(BusinessPlan.class);

    Root<BusinessPlan> root = query.from(BusinessPlan.class);
    List<Predicate> predicates = new ArrayList<>();

    if (fechaIni != null) {
      predicates.add(cb.greaterThanOrEqualTo(root.get(BusinessPlan_.fechaInicio), fechaIni));
    }

    if (fechaFin != null) {
      predicates.add(cb.lessThanOrEqualTo(root.get(BusinessPlan_.fechaFin), fechaFin));
    }

    if (carteraId != null) {
      predicates.add(cb.equal(root.get(BusinessPlan_.cartera).get(Cartera_.id), carteraId));
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    List<BusinessPlan> businessPlanList = entityManager.createQuery(rs.distinct(true)).getResultList();
    return businessPlanList;
  }

  public List<BusinessPlan> listadoBusinessPlan(
    FilterBusinessPlanDTO filterBusinessPlanDTO) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<BusinessPlan> query = cb.createQuery(BusinessPlan.class);

    Root<BusinessPlan> root = query.from(BusinessPlan.class);
    Join<BusinessPlan, BusinessPlanEstrategia> joinEstrategias = root.join(BusinessPlan_.businessPlanEstrategias, JoinType.LEFT);
    List<Predicate> predicates = new ArrayList<>();

    if (filterBusinessPlanDTO.getClienteId() != null) {
      predicates.add(
        cb.like(
          root.get(BusinessPlan_.cliente).get(Cliente_.id).as(String.class),
          "%" + filterBusinessPlanDTO.getClienteId() + "%"));
    }

    if (filterBusinessPlanDTO.getCarteraId() != null) {
      predicates.add(
        cb.like(
          root.get(BusinessPlan_.cartera).get(Cartera_.id).as(String.class),
          "%" + filterBusinessPlanDTO.getCarteraId() + "%"));
    }
    if (filterBusinessPlanDTO.getBien() != null && filterBusinessPlanDTO.getBien()) {
      predicates.add(joinEstrategias.get(BusinessPlanEstrategia_.bien).isNotNull());
    } else {
      predicates.add(joinEstrategias.get(BusinessPlanEstrategia_.bien).isNull());
    }

    if (filterBusinessPlanDTO.getArea() != null && filterBusinessPlanDTO.getArea()) {
      predicates.add(joinEstrategias.get(BusinessPlanEstrategia_.area).isNotNull());
    } else {
      predicates.add(joinEstrategias.get(BusinessPlanEstrategia_.area).isNull());
    }

    if (filterBusinessPlanDTO.getGestor() != null && filterBusinessPlanDTO.getGestor()) {
      predicates.add(joinEstrategias.get(BusinessPlanEstrategia_.gestor).isNotNull());
    } else {
      predicates.add(joinEstrategias.get(BusinessPlanEstrategia_.gestor).isNull());
    }

    if (filterBusinessPlanDTO.getGrupo() != null && filterBusinessPlanDTO.getGrupo()) {
      predicates.add(joinEstrategias.get(BusinessPlanEstrategia_.grupo).isNotNull());
    } else {
      predicates.add(joinEstrategias.get(BusinessPlanEstrategia_.grupo).isNull());
    }

    if (filterBusinessPlanDTO.getFechaInicio() != null) {
      predicates.add(cb.greaterThanOrEqualTo(root.get(BusinessPlan_.fechaInicio), filterBusinessPlanDTO.getFechaInicio()));
    }

    if (filterBusinessPlanDTO.getFechaFin() != null) {
      predicates.add(cb.lessThanOrEqualTo(root.get(BusinessPlan_.fechaFin), filterBusinessPlanDTO.getFechaFin()));
    }

    if (filterBusinessPlanDTO.getNombreArea() != null && filterBusinessPlanDTO.getArea()) {
      predicates.add(cb.like(joinEstrategias.get(BusinessPlanEstrategia_.area).get(AreaUsuario_.nombre).as(String.class),
        "%" + filterBusinessPlanDTO.getArea() + "%"));
    }

    if (filterBusinessPlanDTO.getNombreGestor() != null && filterBusinessPlanDTO.getGestor()) {
      predicates.add(cb.like(joinEstrategias.get(BusinessPlanEstrategia_.gestor).get(Usuario_.nombre).as(String.class),
        "%" + filterBusinessPlanDTO.getGestor() + "%"));
    }

    if (filterBusinessPlanDTO.getNombreGrupo() != null && filterBusinessPlanDTO.getGrupo()) {
      predicates.add(cb.like(joinEstrategias.get(BusinessPlanEstrategia_.grupo).get(GrupoUsuario_.nombre).as(String.class),
        "%" + filterBusinessPlanDTO.getNombreGrupo() + "%"));
    }

    if (filterBusinessPlanDTO.getFechaObjetivo() != null) {
      predicates.add(cb.equal(root.get(BusinessPlan_.fechaObjetivo), filterBusinessPlanDTO.getFechaObjetivo()));
    }

    if (filterBusinessPlanDTO.getTipoBP() != null) {
      predicates.add(cb.like(root.get(BusinessPlan_.tipoBP).as(String.class), "%" + filterBusinessPlanDTO.getTipoBP() + "%"));
    }

    if (filterBusinessPlanDTO.getGastosDirectos() != null) {
      predicates.add(cb.equal(root.get(BusinessPlan_.gastosDirectos), filterBusinessPlanDTO.getGastosDirectos()));
    }

    if (filterBusinessPlanDTO.getGastosIndirectos() != null) {
      predicates.add(cb.equal(root.get(BusinessPlan_.gastosIndirectos), filterBusinessPlanDTO.getGastosIndirectos()));
    }

    if (filterBusinessPlanDTO.getImporteRecuperado() != null) {
      predicates.add(cb.equal(root.get(BusinessPlan_.importeRecuperado), filterBusinessPlanDTO.getImporteRecuperado()));
    }

    if (filterBusinessPlanDTO.getImporteSalida() != null) {
      predicates.add(cb.equal(root.get(BusinessPlan_.importeSalida), filterBusinessPlanDTO.getImporteSalida()));
    }

    if (filterBusinessPlanDTO.getPrecioCompra() != null) {
      predicates.add(cb.equal(root.get(BusinessPlan_.precioCompra), filterBusinessPlanDTO.getPrecioCompra()));
    }

    if (filterBusinessPlanDTO.getVan() != null) {
      predicates.add(cb.equal(root.get(BusinessPlan_.van), filterBusinessPlanDTO.getVan()));
    }

    if (filterBusinessPlanDTO.getTir() != null) {
      predicates.add(cb.equal(root.get(BusinessPlan_.tir), filterBusinessPlanDTO.getTir()));
    }

    if (filterBusinessPlanDTO.getMultiploInversion() != null) {
      predicates.add(cb.equal(root.get(BusinessPlan_.multiploInversion), filterBusinessPlanDTO.getMultiploInversion()));
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    if (filterBusinessPlanDTO.getOrderField() != null) {
      switch (filterBusinessPlanDTO.getOrderField()) {
        case "cartera":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.cartera).get(Cartera_.id), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "bien":
          rs.orderBy(getOrden(cb, joinEstrategias.get(BusinessPlanEstrategia_.bien).get(Bien_.id), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "fechaObjetivo":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.fechaObjetivo), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "actividad":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.actividad), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "importeRecuperado":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.importeRecuperado), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "importeSalida":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.importeSalida), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "gastosDirectos":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.gastosDirectos), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "gastosIndirectos":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.gastosIndirectos), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "precioCompra":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.precioCompra), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "van":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.van), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "tir":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.tir), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "multiploInversion":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.multiploInversion), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "fechaInicio":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.fechaInicio), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "fechaFin":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.fechaFin), filterBusinessPlanDTO.getOrderDirection()));
          break;
        case "tipoBP":
          rs.orderBy(getOrden(cb, root.get(BusinessPlan_.tipoBP), filterBusinessPlanDTO.getOrderDirection()));
          break;
      }
    }
    List<BusinessPlan> businessPlanList = entityManager.createQuery(rs.distinct(true)).getResultList();
    return businessPlanList;
  }

  private Order getOrden(CriteriaBuilder cb, Expression expresion, String orderDirection) {
    if (orderDirection == null) {
      orderDirection = "desc";
    }
    return orderDirection.toLowerCase().equalsIgnoreCase("desc") ? cb.desc(expresion) : cb.asc(expresion);
  }
}

