package com.haya.alaska.business_plan.infrastructure.mapper;

import com.haya.alaska.business_plan.domain.BusinessPlan;
import com.haya.alaska.business_plan.infrastructure.controller.dto.BusinessPlanInputDTO;
import com.haya.alaska.business_plan.infrastructure.controller.dto.BusinessPlanOutputDTO;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.business_plan_estrategia.infrastructure.mapper.BusinessPlanEstrategiaMapper;
import com.haya.alaska.cartera.infrastructure.controller.dto.ClienteOutputDTO;
import com.haya.alaska.cartera.infrastructure.mapper.CarteraMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
public class BusinessPlanMapper {

  @Autowired
  CarteraMapper carteraMapper;
  @Autowired
  BusinessPlanEstrategiaMapper businessPlanEstrategiaMapper;

  public BusinessPlan inputDTOtoEntity(BusinessPlanInputDTO businessPlanInputDTO) {
    BusinessPlan businessPlan = new BusinessPlan();

    businessPlan.setImporteRecuperado(businessPlanInputDTO.getImporteRecuperado());
    businessPlan.setImporteSalida(businessPlanInputDTO.getImporteSalida());
    businessPlan.setFlujoNeto(businessPlanInputDTO.getFlujoNeto());
    businessPlan.setAmortizacion(businessPlanInputDTO.getAmortizacion());
    businessPlan.setMultiploInversion(businessPlanInputDTO.getMultiploInversion());
    businessPlan.setTipoBP(businessPlanInputDTO.getTipoBP());
    businessPlan.setTir(businessPlanInputDTO.getTir());
    businessPlan.setPorcentajeCumplimiento(businessPlanInputDTO.getPorcentajeCumplimiento());
    businessPlan.setFechaInicio(businessPlanInputDTO.getFechaInicio());
    businessPlan.setFechaFin(businessPlanInputDTO.getFechaFin());
    businessPlan.setPrecioCompra(businessPlanInputDTO.getPrecioCompra());
    businessPlan.setFechaObjetivo(businessPlanInputDTO.getFechaObjetivo());
    businessPlan.setGastosDirectos(businessPlanInputDTO.getGastosDirectos());
    businessPlan.setGastosIndirectos(businessPlanInputDTO.getGastosIndirectos());
    businessPlan.setVan(businessPlanInputDTO.getVan());
    businessPlan.setActividad(businessPlanInputDTO.getActividad());
    businessPlan.setNumeroOperacion(businessPlanInputDTO.getNumeroOperacion());
    return businessPlan;
  }

  public BusinessPlanOutputDTO entityToOutputDTO(BusinessPlan businessPlan) {
    BusinessPlanOutputDTO businessPlanOutputDTO = new BusinessPlanOutputDTO();
    businessPlanOutputDTO.setId(businessPlan.getId());
    businessPlanOutputDTO.setImporteRecuperado(businessPlan.getImporteRecuperado());
    businessPlanOutputDTO.setImporteSalida(businessPlan.getImporteSalida());
    businessPlanOutputDTO.setFlujoNeto(businessPlan.getFlujoNeto());
    businessPlanOutputDTO.setAmortizacion(businessPlan.getAmortizacion());
    businessPlanOutputDTO.setMultiploInversion(businessPlan.getMultiploInversion());
    businessPlanOutputDTO.setPorcentajeCumplimiento(businessPlan.getPorcentajeCumplimiento());
    businessPlanOutputDTO.setNumeroOperacion(businessPlan.getNumeroOperacion());
    businessPlanOutputDTO.setTir(businessPlan.getTir());
    businessPlanOutputDTO.setFechaObjetivo(businessPlan.getFechaObjetivo());
    businessPlanOutputDTO.setPrecioCompra(businessPlan.getPrecioCompra());
    businessPlanOutputDTO.setGastosDirectos(businessPlan.getGastosDirectos());
    businessPlanOutputDTO.setGastosIndirectos(businessPlan.getGastosIndirectos());
    businessPlanOutputDTO.setVan(businessPlan.getVan());
    businessPlanOutputDTO.setFechaInicio(businessPlan.getFechaInicio());
    businessPlanOutputDTO.setFechaFin(businessPlan.getFechaFin());
    businessPlanOutputDTO.setActividad(businessPlan.getActividad());
    businessPlanOutputDTO.setTipoBP(businessPlan.getTipoBP());
    businessPlanOutputDTO.setCartera(businessPlan.getCartera() != null ? carteraMapper.entityToOutput(businessPlan.getCartera()) : null);
    businessPlanOutputDTO.setCliente(businessPlan.getCliente() != null ? new ClienteOutputDTO(businessPlan.getCliente()) : null);
    businessPlan.getBusinessPlanEstrategias().stream().sorted(Comparator.comparing(BusinessPlanEstrategia::getFechaInicio));
    for (BusinessPlanEstrategia businessPlanEstrategia : businessPlan.getBusinessPlanEstrategias()) {
      businessPlanOutputDTO.addBusinessPlanEstrategiaOutputDTO(businessPlanEstrategiaMapper.entityToOutputDTO(businessPlanEstrategia));
    }
    return businessPlanOutputDTO;
  }

}
