package com.haya.alaska.business_plan.infrastructure.controller;

import com.haya.alaska.business_plan.application.BusinessPlanGraficaUseCase;
import com.haya.alaska.business_plan.application.BusinessPlanUseCase;
import com.haya.alaska.business_plan.infrastructure.controller.dto.BusinessPlanInputDTO;
import com.haya.alaska.business_plan.infrastructure.controller.dto.BusinessPlanOutputDTO;
import com.haya.alaska.business_plan.infrastructure.controller.dto.FilterBusinessPlanDTO;
import com.haya.alaska.business_plan.infrastructure.controller.grafica.dto.GraficaOutputDTO;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/businessPlan")
public class BusinessPlanController {

  @Autowired
  private BusinessPlanUseCase businessPlanUseCase;
  @Autowired
  private BusinessPlanGraficaUseCase businessPlanGraficaUseCase;

  @ApiOperation(value = "Descargar excel", notes = "")
  @GetMapping("/descargarExcel")
  public ResponseEntity<InputStreamResource> descargar(@RequestParam(value = "clienteId") Integer clienteId,
                                                       @RequestParam(value = "carteraId") Integer carteraId,
                                                       @RequestParam(value = "fechaInicio", required = false) String fechaInicio,
                                                       @RequestParam(value = "fechaFin", required = false) String fechaFin,
                                                       @RequestParam(value = "area", required = false, defaultValue = "false") Boolean area,
                                                       @RequestParam(value = "gestor", required = false, defaultValue = "false") Boolean gestor,
                                                       @RequestParam(value = "grupo", required = false, defaultValue = "false") Boolean grupo,
                                                       @RequestParam(value = "nombreArea", required = false) String nombreArea,
                                                       @RequestParam(value = "nombreGestor", required = false) String nombreGestor,
                                                       @RequestParam(value = "nombreGrupo", required = false) String nombreGrupo,
                                                       @RequestParam(value = "fechaObjetivo", required = false) String fechaObjetivo,
                                                       @RequestParam(value = "tipoBP", required = false) String tipoBP,
                                                       @RequestParam(value = "bien", required = false, defaultValue = "false") Boolean bien,
                                                       @RequestParam(value = "gastosDirectos", required = false) Double gastosDirectos,
                                                       @RequestParam(value = "gastosIndirectos", required = false) Double gastosIndirectos,
                                                       @RequestParam(value = "importeRecuperado", required = false) Double importeRecuperado,
                                                       @RequestParam(value = "importeSalida", required = false) Double importeSalida,
                                                       @RequestParam(value = "precioCompra", required = false) Double precioCompra,
                                                       @RequestParam(value = "van", required = false) Double van,
                                                       @RequestParam(value = "tir", required = false) Double tir,
                                                       @RequestParam(value = "multiploInversion", required = false) Double multiploInversion) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    FilterBusinessPlanDTO filterBusinessPlanDTO = new FilterBusinessPlanDTO(null, null, clienteId, carteraId, fechaInicio, fechaFin, area, gestor, grupo, nombreArea, nombreGestor, nombreGrupo,
      fechaObjetivo, tipoBP, bien, gastosDirectos, gastosIndirectos, importeRecuperado, importeSalida, precioCompra, van, tir, multiploInversion, null, null);

    ByteArrayInputStream excel = excelExport.create(businessPlanUseCase.descarga(filterBusinessPlanDTO));
    return excelExport.download(excel, "informe-BP-" + new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
  }

  @ApiOperation(value = "Comprueba Excel", notes = "")
  @PostMapping("/comprobarExcel")
  public boolean comprobar(@RequestParam("file") MultipartFile file) throws Exception {
    return businessPlanUseCase.cargar(file, false);
  }

  @ApiOperation(value = "Cargar Excel", notes = "")
  @PostMapping("/cargarExcel")
  public boolean cargar(@RequestParam("file") MultipartFile file) throws Exception {
    return businessPlanUseCase.cargar(file, true);
  }

  @ApiOperation(value = "Actualizar un Business Plan", notes = "")
  @PutMapping("/{id}")
  public ResponseEntity<BusinessPlanOutputDTO> updateBusinessPlan(@PathVariable("id") int id, @RequestBody BusinessPlanInputDTO businessPlanInputDTO) throws Exception {
    return businessPlanUseCase.update(id, businessPlanInputDTO);
  }

  @ApiOperation(value = "Borrar uno o varios Business Plan", notes = "")
  @DeleteMapping()
  public void delete(@RequestParam Integer id) {
    businessPlanUseCase.delete(id);
  }

  @ApiOperation(value = "Filtrar BusinessPlan", notes = "")
  @GetMapping("/filterBusinessPlan")
  public ListWithCountDTO<BusinessPlanOutputDTO> filter(@RequestParam(value = "clienteId") Integer clienteId,
                                                        @RequestParam(value = "carteraId") Integer carteraId,
                                                        @RequestParam(value = "fechaInicio", required = false) String fechaInicio,
                                                        @RequestParam(value = "fechaFin", required = false) String fechaFin,
                                                        @RequestParam(value = "area", required = false, defaultValue = "false") Boolean area,
                                                        @RequestParam(value = "gestor", required = false, defaultValue = "false") Boolean gestor,
                                                        @RequestParam(value = "grupo", required = false, defaultValue = "false") Boolean grupo,
                                                        @RequestParam(value = "nombreArea", required = false) String nombreArea,
                                                        @RequestParam(value = "nombreGestor", required = false) String nombreGestor,
                                                        @RequestParam(value = "nombreGrupo", required = false) String nombreGrupo,
                                                        @RequestParam(value = "fechaObjetivo", required = false) String fechaObjetivo,
                                                        @RequestParam(value = "tipoBP", required = false) String tipoBP,
                                                        @RequestParam(value = "bien", required = false, defaultValue = "false") Boolean bien,
                                                        @RequestParam(value = "gastosDirectos", required = false) Double gastosDirectos,
                                                        @RequestParam(value = "gastosIndirectos", required = false) Double gastosIndirectos,
                                                        @RequestParam(value = "importeRecuperado", required = false) Double importeRecuperado,
                                                        @RequestParam(value = "importeSalida", required = false) Double importeSalida,
                                                        @RequestParam(value = "precioCompra", required = false) Double precioCompra,
                                                        @RequestParam(value = "van", required = false) Double van,
                                                        @RequestParam(value = "tir", required = false) Double tir,
                                                        @RequestParam(value = "multiploInversion", required = false) Double multiploInversion,
                                                        @RequestParam(value = "size") Integer size,
                                                        @RequestParam(value = "page") Integer page,
                                                        @RequestParam(value = "orderDirection", required = false) String orderDirection,
                                                        @RequestParam(value = "orderField", required = false) String orderField) throws Exception {
    FilterBusinessPlanDTO filterBusinessPlanDTO = new FilterBusinessPlanDTO(size, page, clienteId, carteraId, fechaInicio, fechaFin, area, gestor, grupo, nombreArea, nombreGestor, nombreGrupo,
      fechaObjetivo, tipoBP, bien, gastosDirectos, gastosIndirectos, importeRecuperado, importeSalida, precioCompra, van, tir, multiploInversion, orderDirection, orderField);
    return businessPlanUseCase.findAllBusinessPlan(filterBusinessPlanDTO);
  }

  @ApiOperation(value = "Generar gráfica de Carteras", notes = "")
  @GetMapping("/grafica")
  public GraficaOutputDTO generateGraficaCartera(@RequestParam(value = "tipo", required = false) String tipo,
                                                 @RequestParam(value = "periodo") String periodo,
                                                 @RequestParam(value = "estrategia", required = false) Integer estrategia,
                                                 @RequestParam(value = "clienteId", required = false) Integer clienteId,
                                                 @RequestParam(value = "carteraId") Integer carteraId,
                                                 @RequestParam(value = "fechaInicio", required = false) String fechaInicio,
                                                 @RequestParam(value = "fechaFin", required = false) String fechaFin,
                                                 @RequestParam(value = "area", required = false, defaultValue = "false") Boolean area,
                                                 @RequestParam(value = "gestor", required = false, defaultValue = "false") Boolean gestor,
                                                 @RequestParam(value = "grupo", required = false, defaultValue = "false") Boolean grupo,
                                                 @RequestParam(value = "bien", required = false, defaultValue = "false") Boolean bien,
                                                 @RequestParam(value = "nombreArea", required = false) String nombreArea,
                                                 @RequestParam(value = "nombreGestor", required = false) String nombreGestor,
                                                 @RequestParam(value = "nombreGrupo", required = false) String nombreGrupo) throws Exception {
    return businessPlanGraficaUseCase.getGrafica(
      tipo,
      periodo, estrategia, clienteId, carteraId, fechaInicio, fechaFin, area, gestor, grupo, nombreArea, nombreGestor, nombreGrupo, bien);
  }

  @ApiOperation(value = "Listado de Tipos", notes = "")
  @GetMapping("/tipos")
  public List<String> listarTipos(@RequestParam(value = "clienteId") Integer clienteId,
                                  @RequestParam(value = "carteraId") Integer carteraId,
                                  @RequestParam(value = "fechaInicio", required = false) String fechaInicio,
                                  @RequestParam(value = "fechaFin", required = false) String fechaFin,
                                  @RequestParam(value = "area", required = false, defaultValue = "false") Boolean area,
                                  @RequestParam(value = "gestor", required = false, defaultValue = "false") Boolean gestor,
                                  @RequestParam(value = "grupo", required = false, defaultValue = "false") Boolean grupo,
                                  @RequestParam(value = "bien", required = false, defaultValue = "false") Boolean bien) {
    return businessPlanUseCase.getTipos(clienteId, carteraId, fechaInicio, fechaFin, area, gestor, grupo, bien);
  }

}
