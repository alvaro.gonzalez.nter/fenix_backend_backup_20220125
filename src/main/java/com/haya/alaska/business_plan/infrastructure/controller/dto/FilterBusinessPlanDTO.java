package com.haya.alaska.business_plan.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FilterBusinessPlanDTO {

  private Integer size;
  private Integer page;
  private Integer clienteId;
  private Integer carteraId;
  private Date fechaInicio;
  private Date fechaFin;
  private Boolean area;
  private Boolean gestor;
  private Boolean grupo;
  private String nombreArea;
  private String nombreGestor;
  private String nombreGrupo;
  private Date fechaObjetivo;
  private String tipoBP;
  private Boolean bien;
  private Double gastosDirectos;
  private Double gastosIndirectos;
  private Double importeRecuperado;
  private Double importeSalida;
  private Double precioCompra;
  private Double van;
  private Double tir;
  private Double multiploInversion;
  private String orderDirection;
  private String orderField;

  public FilterBusinessPlanDTO(Integer size, Integer page, Integer clienteId, Integer carteraId, String fechaInicio,
                               String fechaFin, Boolean area, Boolean gestor, Boolean grupo, String nombreArea, String nombreGestor, String nombreGrupo,
                               String fechaObjetivo, String tipoBP, Boolean bien, Double gastosDirectos, Double gastosIndirectos, Double importeRecuperado,
                               Double importeSalida, Double precioCompra, Double van, Double tir, Double multiploInversion, String orderDirection,
                               String orderField) {
    this.size = size;
    this.page = page;
    this.clienteId = clienteId;
    this.carteraId = carteraId;
    try {
      this.fechaInicio = new Date(fechaInicio);
    } catch (Exception ex) {
      this.fechaInicio = null;
    }
    try {
      this.fechaFin = new Date(fechaFin);
    } catch (Exception ex) {
      this.fechaFin = null;
    }
    try {
      this.fechaObjetivo = new Date(fechaObjetivo);
    } catch (Exception ex) {
      this.fechaObjetivo = null;
    }

    this.area = area;
    this.gestor = gestor;
    this.grupo = grupo;
    this.nombreArea = nombreArea;
    this.nombreGestor = nombreGestor;
    this.nombreGrupo = nombreGrupo;
    this.tipoBP = tipoBP;
    this.bien = bien;
    this.gastosDirectos = gastosDirectos;
    this.gastosIndirectos = gastosIndirectos;
    this.importeRecuperado = importeRecuperado;
    this.orderField = orderField;
    this.importeSalida = importeSalida;
    this.precioCompra = precioCompra;
    this.van = van;
    this.tir = tir;
    this.multiploInversion = multiploInversion;
    this.orderDirection = orderDirection;
  }
}
