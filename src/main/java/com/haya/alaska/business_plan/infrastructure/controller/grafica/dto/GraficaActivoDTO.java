package com.haya.alaska.business_plan.infrastructure.controller.grafica.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class GraficaActivoDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  static final String IMPORTE_SALIDA = "IMPORTE SALIDA";
  static final String IMPORTE_RECUPERADO = "IMPORTE RECUPERADO";
  static final String PRECIO_COMPRA = "PRECIO COMPRA";
  static final String GASTOS = "GASTOS";
  static final String VAN = "VAN";
  private List<String> label;
  private DataGraficaOutputDTO data;

}
