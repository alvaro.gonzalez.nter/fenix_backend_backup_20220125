package com.haya.alaska.business_plan.infrastructure.controller.grafica.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GraficaOutputDTO implements Serializable {
  private String tipo;
  private String periodo;
  private Date fechaInicio;
  private Date fechaFin;
  private List<String> labels;
  private DataGraficaOutputDTO data;
}
