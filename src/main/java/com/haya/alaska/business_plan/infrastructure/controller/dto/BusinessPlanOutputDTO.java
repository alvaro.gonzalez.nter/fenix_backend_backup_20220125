package com.haya.alaska.business_plan.infrastructure.controller.dto;

import com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto.BusinessPlanEstrategiaOutputDTO;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraOutputDTO;
import com.haya.alaska.cartera.infrastructure.controller.dto.ClienteOutputDTO;
import lombok.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusinessPlanOutputDTO {

  private Integer id;
  private Double flujoNeto;
  private Double amortizacion;
  private Double multiploInversion;
  private String tipoBP;
  private Double porcentajeCumplimiento;
  private Integer numeroOperacion;
  private Double tir;
  private Date fechaObjetivo;
  private String actividad;
  private Double precioCompra;
  private Double importeRecuperado;
  private Double importeSalida;
  private Double gastosDirectos;
  private Double gastosIndirectos;
  private Double van;
  private Date fechaInicio;
  private Date fechaFin;
  private ClienteOutputDTO cliente;
  private CarteraOutputDTO cartera;
  private List<BusinessPlanEstrategiaOutputDTO> businessPlanEstrategiaOutputDTOList;

  public void addBusinessPlanEstrategiaOutputDTO(BusinessPlanEstrategiaOutputDTO businessPlanEstrategiaOutputDTO) {
    if (businessPlanEstrategiaOutputDTOList == null) businessPlanEstrategiaOutputDTOList = new ArrayList<>();
    businessPlanEstrategiaOutputDTOList.add(businessPlanEstrategiaOutputDTO);
  }
}
