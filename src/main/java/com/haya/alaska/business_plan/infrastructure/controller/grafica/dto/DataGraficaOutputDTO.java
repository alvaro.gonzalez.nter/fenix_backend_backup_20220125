package com.haya.alaska.business_plan.infrastructure.controller.grafica.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class DataGraficaOutputDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private List<Double> listImporteSalida;
  private List<Double> listImporteRecuperado;
  private List<Double> listPrecioCompra;
  private List<Double> listGastos;
  private List<Double> listVan;
  private List<Double> listNOperaciones;
  private List<Double> listPorcentaje;

}
