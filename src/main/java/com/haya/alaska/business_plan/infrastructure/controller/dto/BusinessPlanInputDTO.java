package com.haya.alaska.business_plan.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusinessPlanInputDTO {

  private Double precioCompra;
  private Double importeRecuperado;
  private Double importeSalida;
  private Double gastosDirectos;
  private Double gastosIndirectos;
  private Double flujoNeto;
  private Double amortizacion;
  private Double multiploInversion;
  private Double van;
  private Double porcentajeCumplimiento;
  private Integer numeroOperacion;
  private Double tir;
  private Date fechaInicio;
  private Date fechaFin;
  private Date fechaObjetivo;
  private String actividad;
  private String tipoBP;
  private String estrategia;
  private Integer idGestor;
  private Integer idArea;
  private Integer idGrupo;
  private Integer idCartera;
  private Integer idBien;
  private Integer idCliente;

}
