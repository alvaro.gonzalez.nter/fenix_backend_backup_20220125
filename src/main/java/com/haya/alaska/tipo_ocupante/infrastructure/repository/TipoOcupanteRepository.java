package com.haya.alaska.tipo_ocupante.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_ocupante.domain.TipoOcupante;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoOcupanteRepository extends CatalogoRepository<TipoOcupante, Integer> {

}
