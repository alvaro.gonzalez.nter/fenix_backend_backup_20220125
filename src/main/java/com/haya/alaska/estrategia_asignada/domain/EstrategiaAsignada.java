package com.haya.alaska.estrategia_asignada.domain;

import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.origen_estrategia.domain.OrigenEstrategia;
import com.haya.alaska.periodo_estrategia.domain.PeriodoEstrategia;
import com.haya.alaska.tipo_estrategia.domain.TipoEstrategia;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_RELA_ESTRATEGIA_ASIGNADA")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "RELA_ESTRATEGIA_ASIGNADA")
public class EstrategiaAsignada implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;
  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  /*@ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private Contrato contrato;*/

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_ESTRATEGIA")
  private Estrategia estrategia;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_TIPO_ESTRATEGIA")
  private TipoEstrategia tipoEstrategia;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_ORIGEN_ESTRATEGIA")
  private OrigenEstrategia origenEstrategia;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_PERIODO_ESTRATEGIA")
  private PeriodoEstrategia periodoEstrategia;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA")
  private Date fecha;
}
