package com.haya.alaska.estrategia_asignada.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstrategiaAsignadaRepository extends JpaRepository<EstrategiaAsignada, Integer> {

}
