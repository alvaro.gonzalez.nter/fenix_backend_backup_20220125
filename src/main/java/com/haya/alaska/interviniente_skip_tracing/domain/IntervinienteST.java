package com.haya.alaska.interviniente_skip_tracing.domain;

import com.haya.alaska.dato_contacto_skip_tracing.domain.DatoContactoST;
import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import com.haya.alaska.estado_civil.domain.EstadoCivil;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.sexo.domain.Sexo;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.tipo_contacto.domain.TipoContacto;
import com.haya.alaska.tipo_documento.domain.TipoDocumento;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_INTERVINIENTE_SKIP_TRACING")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_INTERVINIENTE_SKIP_TRACING")
public class IntervinienteST {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;


  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_DOCUMENTO")
  private TipoDocumento tipoDocumento;

  @Column(name = "DES_NUM_DOCUMENTO")
  private String numeroDocumento;

  @Column(name = "IND_ESTADO")
  private Boolean estado;

  //Añadir todos los cmpos de interviniente a IntervinienteST
  @Column(name = "IND_PERSONA_JURIDICA")
  private Boolean personaJuridica;


  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_INTERVENCION")
  private TipoIntervencion tipoIntervencion;

  @Column(name = "NUM_ORDEN_INTERVENCION")
  private Integer ordenIntervencion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_CIVIL")
  private EstadoCivil estadoCivil;

  //Residente??
  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESIDENCIA")
  private Pais residencia;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_NACIONALIDAD")
  private Pais nacionalidad;

//TipoGestor??

//Pais pais nacimiento??
@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
@JoinColumn(name = "ID_PAIS_NACIMIENTO")
  private Pais paisNacimiento;

  @Column(name = "DES_NOMBRE")
  private String nombre;
  @Column(name = "DES_APELLIDOS")
  private String apellidos;

  @Column(name = "DES_DATOS_ADICIONALES_1")
  private String datosAdicionales1;
  @Column(name = "DES_DATOS_ADICIONALES_2")
  private String datosAdicionales2;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEXO")
  private Sexo sexo;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_NACIMIENTO")
  private Date fechaNacimiento;

  @Column(name = "IND_CONTACTADO")
  private Boolean contactado;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_CONTACTO")
  private TipoContacto tipoContacto;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_INTERVINIENTE")
  private Interviniente interviniente;


  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
    name = "RELA_SKIP_TRACING_INTERVINIENTE_SKIP_TRACING",
    joinColumns = @JoinColumn(name = "ID_INTERVINIENTE_SKIP_TRACING"),
    inverseJoinColumns = @JoinColumn(name = "ID_SKIP_TRACING"))
  @AuditJoinTable(name = "HIST_RELA_SKIP_TRACING_INTERVINIENTE_SKIP_TRACING")
  private Set<SkipTracing> skipTracingSet = new HashSet<>();


  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "ID_INTERVINIENTE_SKIP_TRACING")
  @NotAudited
  private Set<DatoContactoST> datosContactoST = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "ID_INTERVINIENTE_SKIP_TRACING")
  @NotAudited
  private Set<DatoDireccionST> datosDireccionST = new HashSet<>();

//Añadir estos campos a los input y output
  @Column(name = "NUM_INCIDENCIAS_JUDICIALES")
  private Double numeroIncidenciasJudiciales;

  @Column(name = "NUM_INCIDENCIAS_ADMINISTRATIVAS")
  private Double numeroIncidenciasAdministrativas;

  @Column(name = "NUM_INCIDENCIAS_CONCURSO")
  private Double numeroIncidenciasConcurso;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FALLECIDO")
  private Date fechaFallecido;

  @Column(name = "IND_AUTONOMO")
  private Boolean autonomo;

  @Column(name = "IND_ORGANO")
  private Boolean organo;

  @Column(name = "IND_AUSENTE")
  private Boolean ausente;

  @Column(name = "NUM_INVESTIGACIONES_PREVIAS")
  private Double numeroInvestigacionesPrevias;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ULTIMA_INVESTIGACION")
  private Date fechaUltimaInvestigacion;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;

  @Column(name="DES_RAZON_SOCIAL")
  private String razonSocial;

  @Temporal(TemporalType.DATE)
  @Column(name="FCH_CONSTITUCION")
  private Date fechaConstitucion;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_GESTOR")
  private Perfil tipoGestor;



  public IntervinienteST(Interviniente interviniente) {
    BeanUtils.copyProperties(interviniente,this);
  }


  public void addDatosContactosST(List<DatoContactoST> datosContactosST) {
    datosContactosST.forEach(datoContacto -> this.datosContactoST.add(datoContacto));
  }

  public void addDireccionesST(List<DatoDireccionST> direccionesST) {
    this.datosDireccionST.addAll(direccionesST);
  }

}
