package com.haya.alaska.interviniente_skip_tracing.infraestructure.repository;

import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IntervinienteSTRepository extends JpaRepository<IntervinienteST, Integer> {

  Optional<IntervinienteST> findByInterviniente(Interviniente interviniente);

  Optional<IntervinienteST> findByIntervinienteId(Integer intervinienteId);
}
