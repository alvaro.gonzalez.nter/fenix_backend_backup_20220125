package com.haya.alaska.modelo_whatsappweb.infraestructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.modelo_whatsappweb.domain.ModeloWhatsappWeb;

public interface ModeloWhatsappWebRepository extends CatalogoRepository<ModeloWhatsappWeb, Integer>{}
