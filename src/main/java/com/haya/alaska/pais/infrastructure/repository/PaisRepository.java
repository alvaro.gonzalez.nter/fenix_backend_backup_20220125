package com.haya.alaska.pais.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.pais.domain.Pais;

@Repository
public interface PaisRepository extends CatalogoRepository<Pais, Integer> {
	
}
