package com.haya.alaska.periodo_estrategia.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.periodo_estrategia.domain.PeriodoEstrategia;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriodoEstrategiaRepository extends CatalogoRepository<PeriodoEstrategia, Integer> {

}
