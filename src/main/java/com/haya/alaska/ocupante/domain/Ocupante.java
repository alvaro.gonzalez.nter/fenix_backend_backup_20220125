package com.haya.alaska.ocupante.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.vulnerabilidad.domain.Vulnerabilidad;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Audited
@Setter
@Getter
@AuditTable(value = "HIST_MSTR_OCUPANTE")
@Table(name = "MSTR_OCUPANTE")
public class Ocupante {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;

  @Column(name = "ID_OCUPANTE_ORIGEN")
  private String idOcupanteOrigen;

  @Column(name = "DES_NOMBRE")
  private String nombre;

  @Column(name = "DES_APELLIDOS")
  private String apellidos;

  @Column(name = "DES_DNI")
  private String dni;

  @Column(name = "IND_MENOR_EDAD")
  private Boolean menorEdad;

  @Column(name = "IND_DISCAPACITADO")
  private Boolean discapacitado;

  @Column(name = "IND_DEPENDENCIA")
  private Boolean dependencia;

  @Column(name = "IND_RESIDENCIA_HABITUAL")
  private Boolean residenciaHabitual;

  @Column(name = "IMP_INGRESOS_NETOS_MENSUALES")
  private Double ingresosNetosMensuales;

  @Column(name = "IND_DEUDOR_ANTERIOR")
  private Boolean deudorAnterior;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PAIS")
  private Pais nacionalidad;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_VULNERABILIDAD")
  private Vulnerabilidad otrasSituacionesVulnerabilidad;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_OCUPANTE")
  @NotAudited
  private Set<OcupanteBien> bienes = new HashSet<>();

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
    name = "RELA_PROCEDIMIENTO_POSESION_NEGOCIADA_OCUPANTE",
    joinColumns = @JoinColumn(name = "ID_DEMANDADO"),
    inverseJoinColumns = @JoinColumn(name = "ID_PROCEDIMIENTO_POSESION_NEGOCIADA"))
  @AuditJoinTable(name = "HIST_RELA_PROCEDIMIENTO_POSESION_NEGOCIADA_OCUPANTE")
  private Set<ProcedimientoPosesionNegociada> procedimientos = new HashSet<>();

  @ManyToMany
  @JoinTable(name = "RELA_OCUPANTES_PROPUESTA",
    joinColumns = {@JoinColumn(name = "ID_OCUPANTES")},
    inverseJoinColumns = {@JoinColumn(name = "ID_PROPUESTA")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_OCUPANTES_PROPUESTA")
  private Set<Propuesta> propuestas = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_OCUPANTE")
  @NotAudited
  private Set<Direccion> direcciones = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "ID_OCUPANTE")
  @NotAudited
  private Set<DatoContacto> datosContacto = new HashSet<>();


  public void addDatosContactos(List<DatoContacto> datosContactos) {
    datosContactos.forEach(datoContacto -> this.datosContacto.add(datoContacto));
  }

  public void addDireccion(Direccion direccion) {
    this.direcciones.add(direccion);
  }

  public void addDirecciones(List<Direccion> direcciones) {
    this.direcciones.addAll(direcciones);
  }

  public void addDatoContacto(DatoContacto datoContacto) {
    this.datosContacto.add(datoContacto);
  }

  public String getTelefonoPrincipal() {
    List<DatoContacto> datosContacto =
      this.datosContacto
        .stream()
        .sorted(Comparator.comparing(DatoContacto::getOrden))
        .collect(Collectors.toList());
    if (datosContacto.size() > 0) {
      if (datosContacto.get(0).getMovil() != null) {
        return datosContacto.get(0).getMovil();
      } /*else if (datosContacto.get(0).getFijo() != null) {
        return datosContacto.get(0).getFijo();
      }*/
    }
    return "";
  }
  public String getNombreCompleto(){
    String nombre = "";
    String nom = this.getNombre() != null ? this.getNombre() : "";
    String ape = this.getApellidos() != null ? this.getApellidos() : "";
    nombre = nom + " " + ape;
    return nombre;
  }

}

