package com.haya.alaska.ocupante.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.shared.util.ExcelUtils;
import net.bytebuddy.dynamic.scaffold.MethodGraph;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

public class OcupanteInformeExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Occupant ID");
      cabeceras.add("Active ID");
      cabeceras.add("Origin ID");
      cabeceras.add("Intervention type");
      cabeceras.add("Status");
      cabeceras.add("Intervention Order");
      cabeceras.add("Document type");
      cabeceras.add("Document Number");
      cabeceras.add("Name");
      cabeceras.add("Surname");
      cabeceras.add("Phone");
      cabeceras.add("Nationality");
      cabeceras.add("Minor");
      cabeceras.add("Disabled");
      cabeceras.add("Dependent");
      cabeceras.add("Usual residence");
      cabeceras.add("Net monthly income");
      cabeceras.add("Previous debt mark");
      cabeceras.add("Other vulnerability situations");

    } else {
      cabeceras.add("Id Ocupante");
      cabeceras.add("Id Activo");
      cabeceras.add("Id Origen");
      cabeceras.add("Tipo Intervencion");
      cabeceras.add("Estado");
      cabeceras.add("Orden Intervencion");
      cabeceras.add("Tipo Documento");
      cabeceras.add("Numero Documento");
      cabeceras.add("Nombre");
      cabeceras.add("Apellidos");
      cabeceras.add("Teléfono");
      cabeceras.add("Nacionalidad");
      cabeceras.add("Menor De Edad");
      cabeceras.add("Discapacitado");
      cabeceras.add("Dependiente");
      cabeceras.add("Residencia Habitual");
      cabeceras.add("Ingresos Netos Mensuales");
      cabeceras.add("Marca Deuda Anterior");
      cabeceras.add("Otras Situaciones Vulnerabilidad");
    }
  }

  public OcupanteInformeExcel(OcupanteBien ocupanteBien) {
    super();

    Ocupante ocupante = ocupanteBien.getOcupante();
    Bien bien = null;
    if(ocupanteBien.getBienPosesionNegociada() != null){
      if(ocupanteBien.getBienPosesionNegociada().getBien() != null) {
        bien = ocupanteBien.getBienPosesionNegociada().getBien();
      }
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Occupant ID", ocupante.getId());
      this.add("Active ID", bien != null ? bien.getId() : null);
      this.add("Origin ID", ocupante.getIdOcupanteOrigen());
      this.add(
        "Intervention type",
        ocupanteBien.getTipoOcupante() != null
          ? ocupanteBien.getTipoOcupante().getValorIngles()
          : null);
      this.add("Status", ocupante.getActivo() ? "ACTIVE" : "INACTIVE");
      this.add("Intervention Order", ocupanteBien.getOrden());
      // Revisar esto de que el valor siempre sea DNI
      this.add("Document type", "D.N.I");
      this.add("Document Number", ocupante.getDni());
      this.add("Name", ocupante.getNombre());
      this.add("Surname", ocupante.getApellidos());
      var datosContacto = ocupante.getDatosContacto();
      for (var datoContacto : datosContacto) {
        if (datoContacto.getMovil() != null && datoContacto.getMovil() != "") {
          this.add("Phone", datoContacto.getMovil());
          break;
        }
      }
      this.add(
        "Nationality",
        ocupante.getNacionalidad() != null ? ocupante.getNacionalidad().getValorIngles() : null);
      this.add("Minor", ocupante.getMenorEdad());
      this.add("Disabled", ocupante.getDiscapacitado());
      this.add("Dependent", ocupante.getDependencia());
      this.add("Usual residence", ocupante.getResidenciaHabitual() != null ? "Si" : null);
      this.add("Net monthly income", ocupante.getIngresosNetosMensuales());
      this.add("Previous debt mark", ocupante.getDeudorAnterior());
      this.add(
        "Other vulnerability situations",
        ocupante.getOtrasSituacionesVulnerabilidad() != null
          ? ocupante.getOtrasSituacionesVulnerabilidad().getValorIngles()
          : null);

    } else {

      this.add("Id Ocupante", ocupante.getId());
      this.add("Id Activo", bien != null ? bien.getId() : null);
      this.add("Id Origen", ocupante.getIdOcupanteOrigen());
      this.add(
          "Tipo Intervención",
          ocupanteBien.getTipoOcupante() != null
              ? ocupanteBien.getTipoOcupante().getValor()
              : null);
      this.add("Estado", ocupante.getActivo() ? "ACTIVO" : "INACTIVO");
      this.add("Orden Intervención", ocupanteBien.getOrden());
      // Revisar esto de que el valor siempre sea DNI
      this.add("Tipo Documento", "D.N.I");
      this.add("Numero Documento", ocupante.getDni());
      this.add("Nombre", ocupante.getNombre());
      this.add("Apellidos", ocupante.getApellidos());
      var datosContacto = ocupante.getDatosContacto();
      for (var datoContacto : datosContacto) {
        if (datoContacto.getMovil() != null && datoContacto.getMovil() != "") {
          this.add("Teléfono", datoContacto.getMovil());
          break;
        }
      }
      this.add(
          "Nacionalidad",
          ocupante.getNacionalidad() != null ? ocupante.getNacionalidad().getValor() : null);
      this.add("Menor De Edad", ocupante.getMenorEdad());
      this.add("Discapacitado", ocupante.getDiscapacitado());
      this.add("Dependiente", ocupante.getDependencia());
      this.add("Residencia Habitual", ocupante.getResidenciaHabitual() != null ? "Si" : null);
      this.add("Ingresos Netos Mensuales", ocupante.getIngresosNetosMensuales());
      this.add("Marca Deuda Anterior", ocupante.getDeudorAnterior());
      this.add(
          "Otras Situaciones Vulnerabilidad",
          ocupante.getOtrasSituacionesVulnerabilidad() != null
              ? ocupante.getOtrasSituacionesVulnerabilidad().getValor()
              : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}

