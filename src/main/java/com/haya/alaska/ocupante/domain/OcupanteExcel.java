package com.haya.alaska.ocupante.domain;

import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

public class OcupanteExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Active ID");
      cabeceras.add("Occupant ID");
      cabeceras.add("Origin ID");
      cabeceras.add("Occupant type");
      cabeceras.add("Status");
      cabeceras.add("Order");
      cabeceras.add("Document type");
      cabeceras.add("Document number");
      cabeceras.add("Name");
      cabeceras.add("Surname");
      cabeceras.add("Phone");
      cabeceras.add("Nationality");
      cabeceras.add("Minor");
      cabeceras.add("Disabled");
      cabeceras.add("Dependent");
      cabeceras.add("Usual residence");
      cabeceras.add("Net monthly income");
      cabeceras.add("Previous debt mark");
      cabeceras.add("Other vulnerability situations");
    }

    else{

    cabeceras.add("Id Expediente");
    cabeceras.add("Id Activo");
    cabeceras.add("Id Ocupante");
    cabeceras.add("Id Origen");
    cabeceras.add("Tipo Ocupante");
    cabeceras.add("Estado");
    cabeceras.add("Orden");
    cabeceras.add("Tipo Documento");
    cabeceras.add("Numero Documento");
    cabeceras.add("Nombre");
    cabeceras.add("Apellidos");
    cabeceras.add("Telefono");
    cabeceras.add("Nacionalidad");
    cabeceras.add("Menor de edad");
    cabeceras.add("Discapacitado");
    cabeceras.add("Dependiente");
    cabeceras.add("Residencia habitual");
    cabeceras.add("Ingresos netos mensuales");
    cabeceras.add("Marca deuda anterior");
    cabeceras.add("Otras situaciones vulnerabilidad");
    }
  }

  public OcupanteExcel(OcupanteBien ocupanteBien) {
    super();

    Ocupante ocupante = ocupanteBien.getOcupante();
    List<DatoContacto> contactos = new ArrayList<>(ocupante.getDatosContacto());
    DatoContacto dc = null;
    if (!contactos.isEmpty()){
      dc = contactos.get(contactos.size() -1);
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add(
        "Connection ID",
        ocupanteBien
          .getBienPosesionNegociada()
          .getExpedientePosesionNegociada()
          .getIdConcatenado());
      this.add("Active ID", ocupante.getActivo());
      this.add("Occupant ID", ocupante.getId());
      this.add("Origin ID", null);
      this.add(
        "Occupant type",
        ocupanteBien.getTipoOcupante() != null
          ? ocupanteBien.getTipoOcupante().getValorIngles()
          : null);
      this.add("Status", ocupante.getActivo() ? "ACTIVE" : "INACTIVE");
      this.add("Order", ocupanteBien.getOrden());
      // Revisar esto de que el valor siempre sea DNI
      this.add("Document type ", "D.N.I");
      this.add("Document number", ocupante.getDni());
      this.add("Name", ocupante.getNombre());
      this.add("Surname", ocupante.getApellidos());
      String numero = "";
      if (dc != null) {
        if (dc.getMovil() != null) numero = dc.getMovil();
        else if (dc.getFijo() != null) numero = dc.getFijo();
      }
      this.add("Phone", numero);
      this.add(
        "Nationality",
        ocupante.getNacionalidad() != null ? ocupante.getNacionalidad().getValorIngles() : null);
      if (ocupante.getMenorEdad() != null)
        this.add("Minor", ocupante.getMenorEdad() ? "YES" : "No");
      if (ocupante.getDiscapacitado() != null)
        this.add("Disabled", ocupante.getDiscapacitado() ? "YES" : "No");
      if (ocupante.getDependencia() != null)
        this.add("Dependent", ocupante.getDependencia() ? "YES" : "No");
      if (ocupante.getResidenciaHabitual() != null)
        this.add("Usual residence", ocupante.getResidenciaHabitual() ? "YES" : "No");
      if (ocupante.getDeudorAnterior() != null)
        this.add("Net monthly income", ocupante.getDeudorAnterior() ? "YES" : "No");
      this.add("Previous debt mark", ocupante.getIngresosNetosMensuales());
      this.add(
        "Other vulnerability situations",
        ocupante.getOtrasSituacionesVulnerabilidad() != null
          ? ocupante.getOtrasSituacionesVulnerabilidad().getValorIngles()
          : null);

    } else {

      this.add(
          "Id Expediente",
          ocupanteBien
              .getBienPosesionNegociada()
              .getExpedientePosesionNegociada()
              .getIdConcatenado());
      this.add("Id Activo", ocupante.getActivo());
      this.add("Id Ocupante", ocupante.getId());
      this.add("Id Origen", null);
      this.add(
          "Tipo Ocupante",
          ocupanteBien.getTipoOcupante() != null
              ? ocupanteBien.getTipoOcupante().getValor()
              : null);
      this.add("Estado", ocupante.getActivo() ? "Activo" : "Inactivo");
      this.add("Orden", ocupanteBien.getOrden());
      // Revisar esto de que el valor siempre sea DNI
      this.add("Tipo Documento", "D.N.I");
      this.add("Numero Documento", ocupante.getDni());
      this.add("Nombre", ocupante.getNombre());
      this.add("Apellidos", ocupante.getApellidos());
      String numero = "";
      if (dc != null) {
        if (dc.getMovil() != null) numero = dc.getMovil();
        else if (dc.getFijo() != null) numero = dc.getFijo();
      }
      this.add("Telefono", numero);
      this.add(
          "Nacionalidad",
          ocupante.getNacionalidad() != null ? ocupante.getNacionalidad().getValor() : null);
      if (ocupante.getMenorEdad() != null)
        this.add("Menor de edad", ocupante.getMenorEdad() ? "Si" : "No");
      if (ocupante.getDiscapacitado() != null)
        this.add("Discapacitado", ocupante.getDiscapacitado() ? "Si" : "No");
      if (ocupante.getDependencia() != null)
        this.add("Dependiente", ocupante.getDependencia() ? "Si" : "No");
      if (ocupante.getResidenciaHabitual() != null)
        this.add("Residencia habitual", ocupante.getResidenciaHabitual() ? "Si" : "No");
      if (ocupante.getDeudorAnterior() != null)
        this.add("Marca deuda anterior", ocupante.getDeudorAnterior() ? "Si" : "No");
      this.add("Ingresos netos mensuales", ocupante.getIngresosNetosMensuales());
      this.add(
          "Otras situaciones vulnerabilidad",
          ocupante.getOtrasSituacionesVulnerabilidad() != null
              ? ocupante.getOtrasSituacionesVulnerabilidad().getValor()
              : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
