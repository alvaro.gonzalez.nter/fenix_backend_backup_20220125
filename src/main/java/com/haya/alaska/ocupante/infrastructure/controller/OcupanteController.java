package com.haya.alaska.ocupante.infrastructure.controller;

import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteOcupanteDatosContactoDTO;
import com.haya.alaska.ocupante.application.OcupanteUseCase;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteInputDto;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteOutputDto;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientePosesionNegociada/{idExpediente}/bien/{idBienPosesionNegociada}/ocupante")
public class OcupanteController {

  @Autowired
  private OcupanteUseCase ocupanteUseCase;

  @ApiOperation(value = "Listado ocupantes", notes = "Devuelve todas los ocupantes registradas relacionados con el bien enviado")
  @GetMapping
  public ListWithCountDTO<OcupanteDTOToList> getAllOcupantes(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBien, @RequestParam(value = "id", required = false) Integer id, @RequestParam(value = "nombre", required = false) String nombre, @RequestParam(value = "apellidos", required = false) String apellidos, @RequestParam(value = "nacionalidad", required = false) String nacionalidad, @RequestParam(value = "menorEdad", required = false) Boolean menorEdad, @RequestParam(value = "discapacitado", required = false) Boolean discapacitado, @RequestParam(value = "dependencia", required = false) Boolean dependencia, @RequestParam(value = "residenciaHabitual", required = false) Boolean residenciaHabitual, @RequestParam(value = "tipoOcupante", required = false) String tipoOcupante, @RequestParam(value = "deudorAnterior", required = false) Boolean deudorAnterior, @RequestParam(value = "orderField", required = false) String orderField, @RequestParam(value = "orderDirection", required = false) String orderDirection, @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) {

    return ocupanteUseCase.getAllFilteredOcupantes(idBien, id, nombre, apellidos, nacionalidad, menorEdad, discapacitado, dependencia, residenciaHabitual, tipoOcupante, deudorAnterior, orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Obtener ocupante por dni", notes = "Devuelve el ocupante con dni enviado")
  @GetMapping("/dni/{dni}")
  public List<OcupanteOutputDto> getOcupanteByDni(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBien, @PathVariable("dni") String dni) throws IllegalAccessException, NotFoundException {
    return ocupanteUseCase.findOcupanteByDniDto(dni);
  }


  @ApiOperation(value = "Obtener ocupante", notes = "Devuelve el ocupante con id enviado")
  @GetMapping("/{idOcupante}")
  public OcupanteOutputDto getOcupanteById(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBien, @PathVariable("idOcupante") Integer idOcupante) throws IllegalAccessException, NotFoundException {
    return ocupanteUseCase.findOcupanteByIdDto(idBien, idOcupante);
  }

  @ApiOperation(value = "Actualizar ocupante", notes = "Actualizar el ocupante con id enviado")
  @PutMapping("/{idOcupante}")
  @ResponseStatus(HttpStatus.OK)
  @Transactional(rollbackFor = Exception.class)
  public OcupanteOutputDto updateOcupante(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBien, @PathVariable("idOcupante") Integer idOcupante, @RequestBody @Valid OcupanteInputDto ocupanteInputDTO) throws Exception {
    return ocupanteUseCase.update(idBien, idOcupante, ocupanteInputDTO);
  }

  @ApiOperation(value = "Crear ocupante", notes = "Añadir un nuevo ocupante al bien con el id enviado")
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @Transactional(rollbackFor = Exception.class)
  public OcupanteOutputDto createOcupante(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBien, @RequestBody @Valid OcupanteInputDto ocupanteInputDTO) throws Exception {
    return ocupanteUseCase.create(idBien, ocupanteInputDTO);
  }

  @ApiOperation(value = "Listado ocupantes", notes = "Devuelve todas los ocupantes registradas relacionados con el expedientePN enviado")
  @GetMapping("/all")
  public List<OcupanteOutputDto> getOcupantesByExpedienteID(@PathVariable("idExpediente") Integer idExpediente) {
    return ocupanteUseCase.findOcupantesByExpedientePN(idExpediente);
  }

  @ApiOperation(value = "Añadir un documento a un ocupante")
  @PostMapping("/{idOcupante}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoOcupante(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBienPosesionNegociada, @PathVariable("idOcupante") Integer idOcupante, @RequestPart("file") @NotNull @NotBlank MultipartFile file, @RequestPart("metadatos") MetadatoDocumentoInputDTO metadatoDocumento, @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    ocupanteUseCase.createDocumentoOcupante(idOcupante, file, metadatoDocumento, usuario);
  }

  @ApiOperation(value = "Obtener documentación de los ocupantes de un ExpedientePN", notes = "Devuelve la documentación disponible de los ocupantes del ExpedientePN con id enviado")
  @GetMapping("/documentos")
  @Transactional
  public List<DocumentoDTO> getDocumentosByExpedientePN(@PathVariable("idExpediente") Integer idExpediente) throws NotFoundException, IOException {
    return ocupanteUseCase.findDocumentosByExpedienteId(idExpediente);
  }

  @ApiOperation(value = "Obtener los intervinientes con sus datos de contacto", notes = "Devuelve el interviniente")
  @GetMapping("/{idExpedientePN}/datoscontacto")
  public List<IntervinienteOcupanteDatosContactoDTO> getOcupantesDatosContacto(@PathVariable("idExpedientePN") int idExpedientePN) throws Exception {
    return ocupanteUseCase.getAllOcupantesContacto(idExpedientePN);
  }
}
