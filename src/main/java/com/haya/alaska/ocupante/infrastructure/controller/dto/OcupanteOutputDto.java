package com.haya.alaska.ocupante.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionDTO;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OcupanteOutputDto extends OcupanteDto {
  
  private static final long serialVersionUID = 1L;
  
  private CatalogoMinInfoDTO tipoOcupante;
  private CatalogoMinInfoDTO nacionalidad;
  private CatalogoMinInfoDTO otrasSituacionesVulnerabilidad;

  private List<DatosContactoDTOToList> datosContacto;
  private List<DireccionDTO> direcciones;
}
