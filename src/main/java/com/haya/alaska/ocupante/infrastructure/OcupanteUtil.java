package com.haya.alaska.ocupante.infrastructure;

import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OcupanteUtil {
  @Autowired
  OcupanteRepository ocupanteRepository;
  @Autowired
  DireccionRepository direccionRepository;




  public Ocupante addDireccionOcupante(Integer idOcupante, Integer idDireccion) throws Exception {
    Ocupante ocupante =
      ocupanteRepository
        .findById(idOcupante)
        .orElseThrow(
          () -> new NotFoundException("objeto", idOcupante));
    Direccion direccion =
      direccionRepository
        .findById(idDireccion)
        .orElseThrow(
          () -> new NotFoundException("objeto", idDireccion));
    ocupante.addDireccion(direccion);
    return ocupanteRepository.save(ocupante);
  }
}
