package com.haya.alaska.ocupante.infrastructure.controller.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OcupanteDatosContactoDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  
  private Integer id;
  private String nombre;
  private String apellidos;
  private String numeroDocumento;
  private String movil;
  private boolean movilvalidado;
  private String email;
  private boolean emailvalidado;
  private String direccion;
  private String telefono;
  private boolean telefonovalidado;
    
}
