package com.haya.alaska.ocupante.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaDTOToList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class OcupanteBusquedaDTOToList {

  private Integer id;
  private String nombre;
  private CatalogoMinInfoDTO tipoOcupante;
  private Integer orden;
  private Boolean activo;
  private String telefonoFijo;
  private String telefonoMovil;
  private String numeroDocumento;
  private String tipoDireccion;
  private String direccion;
  private String codigoPostal;
  private String municipio;
  private String email;
  private String idActivo;
  private String idExpediente;
  private String cliente;
  //Validados
  private Boolean telefonoValidado;
  private Boolean emailValidado;
  private Boolean direccionValidada;
  //Procedimientos
  private List<ProcedimientoPosesionNegociadaDTOToList> procedimientos;
}
