package com.haya.alaska.ocupante.infrastructure.repository;

import com.haya.alaska.ocupante.domain.Ocupante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface OcupanteRepository extends JpaRepository<Ocupante, Integer> {

  Optional<Ocupante> findByDni(String dni);
  Optional<Ocupante> findByidOcupanteOrigen(String idOcupanteOrigen);

  @Query(
    value =
      "SELECT * FROM (SELECT ID_OCUPANTE, NUM_ORDEN ,ID_TIPO_OCUPANTE, ROW_NUMBER ()\n"
        + "OVER(PARTITION BY NUM_ORDEN,ID_TIPO_OCUPANTE,ID_OCUPANTE)\n"
        + "AS CONTEO FROM RELA_OCUPANTE_BIEN rci ORDER BY ID_OCUPANTE) T WHERE ID_OCUPANTE IN ?1",
    nativeQuery = true)
  List<Map> findAllOcupantesOrden(List<Integer> idsOcupantes);

  @Query(
    value = "SELECT o.* FROM MSTR_OCUPANTE o\r\n"
      + "INNER JOIN RELA_OCUPANTE_BIEN obi on o.id = obi.ID_OCUPANTE\r\n"
      + "INNER JOIN MSTR_BIEN_POSESION_NEGOCIADA bpn ON bpn.id = obi.ID_BIEN_POSESION_NEGOCIADA\r\n"
      + "INNER JOIN MSTR_BIEN bi on bi.ID = bpn.ID_BIEN\r\n"
      + "WHERE bpn.id = :idBienPosesionNegociada",
    nativeQuery = true)
  List<Ocupante> findAllOcupantesByBienPosesionNegociada(Integer idBienPosesionNegociada);

  List<Ocupante> findAllByIdNotIn(List<Integer>lista);


  List<Ocupante>findDistinctAllByBienesBienPosesionNegociadaExpedientePosesionNegociadaIdIn(List<Integer>lista);

}
