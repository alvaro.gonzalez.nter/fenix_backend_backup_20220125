package com.haya.alaska.ocupante.infrastructure.controller.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OcupanteDto implements Serializable {
  
  private static final long serialVersionUID = 1L;

  private Integer id;
  private String nombre;
  private String apellidos;
  @NotNull
  private String dni;
  private Boolean menorEdad;
  private Boolean discapacitado;
  private Boolean dependencia;
  private Boolean residenciaHabitual;
  private Double ingresosNetosMensuales;
  private Boolean deudorAnterior;
  private Boolean activo;
  private Integer orden;
}
