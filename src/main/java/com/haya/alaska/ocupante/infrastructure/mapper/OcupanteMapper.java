package com.haya.alaska.ocupante.infrastructure.mapper;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.dato_contacto.infrastructure.mapper.DatoContactoMapper;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionDTO;
import com.haya.alaska.direccion.infrastructure.mapper.DireccionMapper;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteBusquedaDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteOutputDto;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.ocupante_bien.infrastructure.repository.OcupanteBienRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_ocupante.domain.TipoOcupante;
import com.haya.alaska.tipo_ocupante.infrastructure.repository.TipoOcupanteRepository;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class OcupanteMapper {

  @Autowired private CatalogoMinInfoMapper catalogoMinInfoMapper;
  @Autowired private OcupanteRepository ocupanteRepository;
  @Autowired private OcupanteBienRepository ocupanteBienRepository;
  @Autowired private TipoOcupanteRepository tipoOcupanteRepository;

  @SneakyThrows
  public List<OcupanteDTOToList> listOcupanteToListOcupanteDTOToList(
      List<Ocupante> ocupantes, Integer idBien) {
    List<OcupanteDTOToList> ocupanteDTO = new ArrayList<>();
    for (Ocupante ocupante : ocupantes) {
      var ob =
          ocupanteBienRepository
              .findByBienPosesionNegociadaIdAndOcupanteId(idBien, ocupante.getId())
              .orElseThrow(
                  () -> new NotFoundException("ocupante", ocupante.getId(), "activo", idBien));
      ocupanteDTO.add(OcupanteToOcupanteDTOToList(ocupante, ob));
    }
    return ocupanteDTO;
  }

  public OcupanteDTOToList OcupanteToOcupanteDTOToList(
      Ocupante ocupante, OcupanteBien ocupanteBien) {

    OcupanteDTOToList ocupanteDTOToList = new OcupanteDTOToList();
    BeanUtils.copyProperties(ocupante, ocupanteDTOToList);

    ocupanteDTOToList.setNacionalidad(
        ocupante.getNacionalidad() != null ? ocupante.getNacionalidad().getValor() : null);
    ocupanteDTOToList.setOtrasSituacionesVulnerabilidad(
        ocupante.getOtrasSituacionesVulnerabilidad() != null
            ? ocupante.getOtrasSituacionesVulnerabilidad().getValor()
            : null);
    ocupanteDTOToList.setTipoOcupante(
        ocupanteBien.getTipoOcupante() != null ? ocupanteBien.getTipoOcupante().getValor() : null);
    ocupanteDTOToList.setOrden(ocupanteBien.getOrden());
    List<DatoContacto> datosContacto = new ArrayList<>(ocupante.getDatosContacto());
    List<DatosContactoDTOToList> datosContactoDTO =
        datosContacto.stream()
            .map(DatoContactoMapper::parseEntityToListDTO)
            .collect(Collectors.toList());
    List<Direccion> direcciones = new ArrayList<>(ocupante.getDirecciones());
    List<DireccionDTO> direccionesDTO =
        direcciones.stream().map(DireccionMapper::parseEntityToDTO).collect(Collectors.toList());

    ocupanteDTOToList.setDatosContacto(datosContactoDTO);
    ocupanteDTOToList.setDirecciones(direccionesDTO);

    // meterle al dto un telefono, sacado de la lista de los datos de contacto
    var todosDatosContacto = ocupanteDTOToList.getDatosContacto();
    todosDatosContacto =
        todosDatosContacto.stream()
            .sorted(
                (DatosContactoDTOToList h1, DatosContactoDTOToList h2) ->
                    h1.getId().compareTo(h2.getId()))
            .collect(Collectors.toList());
    for (int z = 0; z < todosDatosContacto.size(); z++) {
      if (ocupanteDTOToList.getTelefono() == null
          && ocupanteDTOToList.getDatosContacto().get(z).getMovil() != null
          && ocupanteDTOToList.getDatosContacto().get(z).getMovil() != "") {
        ocupanteDTOToList.setTelefono(ocupanteDTOToList.getDatosContacto().get(z).getMovil());
      }
    }
    return ocupanteDTOToList;
  }

  public OcupanteOutputDto entityOutputDto(Ocupante ocupante, OcupanteBien ob) {

    OcupanteOutputDto ocupanteOutputDto = new OcupanteOutputDto();
    BeanUtils.copyProperties(ocupante, ocupanteOutputDto, "nombre");
    ocupanteOutputDto.setNombre(ocupante.getNombre());
    if (ob.getTipoOcupante() != null)
      ocupanteOutputDto.setTipoOcupante(catalogoMinInfoMapper.entityToDto(ob.getTipoOcupante()));
    if (ocupante.getNacionalidad() != null)
      ocupanteOutputDto.setNacionalidad(
          catalogoMinInfoMapper.entityToDto(ocupante.getNacionalidad()));
    if (ocupante.getOtrasSituacionesVulnerabilidad() != null)
      ocupanteOutputDto.setOtrasSituacionesVulnerabilidad(
          catalogoMinInfoMapper.entityToDto(ocupante.getOtrasSituacionesVulnerabilidad()));
    ocupanteOutputDto.setOrden(ob.getOrden());
    List<DatoContacto> datosContacto = new ArrayList<>(ocupante.getDatosContacto());
    List<DatosContactoDTOToList> datosContactoDTO =
        datosContacto.stream()
            .map(DatoContactoMapper::parseEntityToListDTO)
            .collect(Collectors.toList());
    List<Direccion> direcciones = new ArrayList<>(ocupante.getDirecciones());
    List<DireccionDTO> direccionesDTO =
        direcciones.stream().map(DireccionMapper::parseEntityToDTO).collect(Collectors.toList());
    ocupanteOutputDto.setDatosContacto(datosContactoDTO);
    ocupanteOutputDto.setDirecciones(direccionesDTO);

    return ocupanteOutputDto;
  }

  public OcupanteOutputDto OcupanteToOcupanteOutputDto(Ocupante ocupante) {
    OcupanteOutputDto ocupanteOutputDto = new OcupanteOutputDto();
    BeanUtils.copyProperties(ocupante, ocupanteOutputDto);
    return ocupanteOutputDto;
  }

  /*public Ocupante inputDtoToEntity(Integer idOcupante, OcupanteInputDto ocupanteInputDto) throws NotFoundException {
    Ocupante ocupante = new Ocupante();
    if (idOcupante != null) {
      ocupante = ocupanteRepository.findById(idOcupante).orElseThrow(()-> new NotFoundException("No se ha encontrado el ocupante con el id "+idOcupante));
      bienUtil.updateBien(bien, bienInputDto, contratoBien);
    }else{
      bienUtil.createBien(bien, bienInputDto, idContrato);
    }
    return bien;
  }*/

  public List<OcupanteBusquedaDTOToList> parseOcupanteBusqueda(Map map) {
    Object idOcupante = map.get("ID_OCUPANTE");
    Object numOrdenOcupante = map.get("NUM_ORDEN");
    Object idTipoOcupante = map.get("ID_TIPO_OCUPANTE");
    Integer idOcupanteParse = Integer.parseInt(idOcupante.toString());
    Integer numOrdenOcupanteParse = Integer.parseInt(numOrdenOcupante.toString());
    Integer idTipoOcupanteParse = null;
    if (idTipoOcupante != null) {
      idTipoOcupanteParse = Integer.parseInt(idTipoOcupante.toString());
    }
    Ocupante ocupante = ocupanteRepository.findById(idOcupanteParse).orElse(null);
    List<OcupanteBusquedaDTOToList> ocupanteBusquedaList = new ArrayList<>();
    List<DatoContacto> datoContactoList =
        ocupante.getDatosContacto().stream().collect(Collectors.toList());

    // if (ocupante.getDirecciones() != null ) {
    for (var ocupanteBien : ocupante.getBienes()) {
      for (DatoContacto datoContacto : datoContactoList) {
        var bien = ocupanteBien.getBienPosesionNegociada();
        OcupanteBusquedaDTOToList ocupanteBusquedaDTOToList = new OcupanteBusquedaDTOToList();
        ocupanteBusquedaDTOToList.setId(idOcupanteParse);
        ocupanteBusquedaDTOToList.setIdActivo(bien.getIdHaya());
        ocupanteBusquedaDTOToList.setIdExpediente(
            bien.getExpedientePosesionNegociada() != null
                ? bien.getExpedientePosesionNegociada().getIdConcatenado()
                : null);
        ExpedientePosesionNegociada epn = bien.getExpedientePosesionNegociada();
        if (epn != null) {
          Cliente cl = epn.getCartera().getCliente();
          if (cl != null) ocupanteBusquedaDTOToList.setCliente(cl.getNombre());
        }
        if (datoContacto.getEmail() != null) {
          ocupanteBusquedaDTOToList.setEmail(datoContacto.getEmail());
        }
        if (datoContacto.getEmailValidado() != null && datoContacto.getEmailValidado()) {
          ocupanteBusquedaDTOToList.setEmailValidado(datoContacto.getEmailValidado());
        }
        if (datoContacto.getFijoValidado() != null && datoContacto.getFijoValidado()) {
          ocupanteBusquedaDTOToList.setTelefonoValidado(datoContacto.getFijoValidado());
        }
        if (ocupante.getNombre() != null) {
          ocupanteBusquedaDTOToList.setNombre(ocupante.getNombre());
        }
        if (numOrdenOcupanteParse != null) {
          ocupanteBusquedaDTOToList.setOrden(numOrdenOcupanteParse);
        }
        if (idTipoOcupanteParse != null) {
          TipoOcupante tipoOcupante =
              tipoOcupanteRepository.findById(idTipoOcupanteParse).orElse(null);
          if (tipoOcupante != null) {
            ocupanteBusquedaDTOToList.setTipoOcupante(new CatalogoMinInfoDTO(tipoOcupante));
          }
        }
        ocupanteBusquedaDTOToList.setActivo(ocupante.getActivo());
        ocupanteBusquedaDTOToList.setTelefonoMovil(
            datoContacto.getMovil() != null ? datoContacto.getMovil() : null);
        ocupanteBusquedaDTOToList.setTelefonoFijo(
            datoContacto.getFijo() != null ? datoContacto.getFijo() : null);
        ocupanteBusquedaDTOToList.setNumeroDocumento(
            ocupante.getDni() != null ? ocupante.getDni() : null);
        /*ocupanteBusquedaDTOToList.setTipoDireccion(
            direccion.getTipoVia() != null ? direccion.getTipoVia().getValor() : null);

        ocupanteBusquedaDTOToList.setDireccion(
            direccion.getNombre() != null
                ? direccion.getNombre()
                : "" + "" + direccion.getNumero() != null
                    ? direccion.getNumero()
                    : "" + "" + direccion.getPortal() != null
                        ? direccion.getPortal()
                        : "" + "" + direccion.getBloque() != null
                            ? direccion.getBloque()
                            : "" + "" + direccion.getEscalera() != null
                                ? direccion.getEscalera()
                                : "" + "" + direccion.getPiso() != null
                                    ? direccion.getPiso()
                                    : "");
        ocupanteBusquedaDTOToList.setCodigoPostal(direccion.getCodigoPostal());
        ocupanteBusquedaDTOToList.setMunicipio(
            direccion.getMunicipio() != null ? direccion.getMunicipio().getValor() : null);*/

        /*List<DatoContacto> datosContactosList =
        ocupante.getDatosContacto().stream().collect(Collectors.toList());*/
        /*for (DatoContacto datocontacto :datosContactosList) {
          if (datocontacto.getEmailValidado()!=null && datocontacto.getEmailValidado()){
            ocupanteBusquedaDTOToList.setEmailValidado(datocontacto.getEmailValidado());
          }
          if (datocontacto.getFijoValidado()!=null && datocontacto.getFijoValidado()){
            ocupanteBusquedaDTOToList.setTelefonoValidado(datocontacto.getFijoValidado());
          }
        }*/
        // ocupanteBusquedaDTOToList.setDireccionValidada(direccion.getValidada());
        ocupanteBusquedaDTOToList.setNumeroDocumento(
            ocupante.getDni() != null ? ocupante.getDni() : null);
        // ocupanteBusquedaDTOToList.setIdActivo(idActivoParse != null ? idActivoParse : null);
        ocupanteBusquedaList.add(ocupanteBusquedaDTOToList);
      }
    }
    return ocupanteBusquedaList;
  }

  public OcupanteBusquedaDTOToList parseToBusqueda(Ocupante ocupante) {
    OcupanteBusquedaDTOToList ocupanteBusquedaDTOToList = new OcupanteBusquedaDTOToList();
    if (ocupante.getNombre() != null && ocupante.getApellidos() != null)
      ocupanteBusquedaDTOToList.setNombre(ocupante.getNombre() + " " + ocupante.getApellidos());
    // ocupanteBusquedaDTOToList.setTipoOcupante(ocupante.getti);

    return ocupanteBusquedaDTOToList;
  }
}
