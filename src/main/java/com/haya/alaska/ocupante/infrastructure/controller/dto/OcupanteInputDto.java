package com.haya.alaska.ocupante.infrastructure.controller.dto;

import com.haya.alaska.bien.infrastructure.controller.dto.BienDto;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionDTO;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionInputDTO;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OcupanteInputDto extends OcupanteDto {
  @NotNull
  private Integer tipoOcupante;
  private Integer nacionalidad;
  private Integer otrasSituacionesVulnerabilidad;

  private List<DatosContactoDTOToList> datosContacto;
  private List<DireccionInputDTO> direcciones;
}
