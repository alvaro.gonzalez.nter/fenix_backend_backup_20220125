package com.haya.alaska.ocupante.infrastructure.controller.dto;

import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionDTO;
import com.haya.alaska.ocupante.domain.Ocupante;
import lombok.*;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OcupanteDTOToList {

  private Integer id;
  private Integer orden;
  private String nombre;
  private String apellidos;
  private String nacionalidad;
  private String tipoOcupante;
  private Boolean menorEdad;
  private Boolean discapacitado;
  private Boolean dependencia;
  private Boolean residenciaHabitual;
  private Boolean deudorAnterior;
  private String telefono;
  private String dni;
  private String otrasSituacionesVulnerabilidad;

  private List<DatosContactoDTOToList> datosContacto;
  private List<DireccionDTO> direcciones;


  public OcupanteDTOToList(Ocupante ocupante) {

    BeanUtils.copyProperties(ocupante, this);

  }

  public static List<OcupanteDTOToList> newList(List<Ocupante> ocupantes) {
    return ocupantes.stream().map(ocupante -> new OcupanteDTOToList(ocupante)).collect(Collectors.toList());
  }
}
