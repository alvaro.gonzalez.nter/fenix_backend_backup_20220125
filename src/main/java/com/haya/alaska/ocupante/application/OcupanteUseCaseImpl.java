package com.haya.alaska.ocupante.application;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente_;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada_;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.Bien_;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada_;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.busqueda.domain.BusquedaBuilderOcupante;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.BusquedaAgendaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.IntervaloFecha;
import com.haya.alaska.carga.domain.Carga_;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable_;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.domain.Cliente_;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera_;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.domain.ContratoBien_;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.domain.DatoContacto_;
import com.haya.alaska.dato_contacto.infrastructure.DatoContactoUtil;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.dato_contacto.infrastructure.mapper.DatoContactoMapper;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionInputDTO;
import com.haya.alaska.direccion.infrastructure.mapper.DireccionMapper;
import com.haya.alaska.direccion.infrastructure.util.DireccionUtil;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.estado_checklist.domain.EstadoChecklist_;
import com.haya.alaska.estado_expediente_posesion_negociada.domain.EstadoExpedientePosesionNegociada_;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion.domain.Estimacion_;
import com.haya.alaska.estrategia.domain.Estrategia_;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada_;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCase;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada_;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.formalizacion.domain.Formalizacion_;
import com.haya.alaska.grupo_cliente.domain.GrupoCliente_;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta_;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.application.GestorDocumentalUseCase;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteOcupanteDatosContactoDTO;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.interviniente.infrastructure.util.IntervinienteUtil;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.domain.Movimiento_;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.domain.Ocupante_;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteInputDto;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteOutputDto;
import com.haya.alaska.ocupante.infrastructure.mapper.OcupanteMapper;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien_;
import com.haya.alaska.ocupante_bien.infrastructure.repository.OcupanteBienRepository;
import com.haya.alaska.origen_expediente_posesion_negociada.domain.OrigenExpedientePosesionNegociada_;
import com.haya.alaska.pais.domain.Pais_;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.domain.Perfil_;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.domain.Procedimiento_;
import com.haya.alaska.procedimiento.infrastructure.repository.ProcedimientoRepository;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada_;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.domain.Propuesta_;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.saldo.domain.Saldo_;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.situacion.domain.Situacion_;
import com.haya.alaska.tasacion.domain.Tasacion_;
import com.haya.alaska.tipo_ocupante.domain.TipoOcupante_;
import com.haya.alaska.tipo_ocupante.infrastructure.repository.TipoOcupanteRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import com.haya.alaska.valoracion.domain.Valoracion_;
import com.haya.alaska.vulnerabilidad.infrastructure.repository.VulnerabilidadRepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class OcupanteUseCaseImpl implements OcupanteUseCase {

  @PersistenceContext
  private EntityManager entityManager;

  @Autowired
  private OcupanteMapper ocupanteMapper;

  @Autowired
  private OcupanteRepository ocupanteRepository;
  @Autowired
  private OcupanteBienRepository ocupanteBienRepository;
  @Autowired
  private TipoOcupanteRepository tipoOcupanteRepository;
  @Autowired
  private PaisRepository paisRepository;
  @Autowired
  private VulnerabilidadRepository vulnerabilidadRepository;
  @Autowired
  private ContratoRepository contratoRepository;
  @Autowired
  private BienRepository bienRepository;
  @Autowired
  private BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  private DatoContactoUtil datoContactoUtil;
  @Autowired
  private DireccionUtil direccionUtil;
  @Autowired
  private DatoContactoMapper datoContactoMapper;
  @Autowired
  private DireccionMapper direccionMapper;
  @Autowired
  ServicioGestorDocumental servicioGestorDocumental;
  @Autowired
  private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired
  private GestorDocumentalUseCase gestorDocumentalUseCase;
  @Autowired
  private EventoUtil eventoUtil;
  @Autowired
  private ExpedienteRepository expedienteRepository;
  @Autowired
  private IntervinienteRepository intervinienteRepository;
  @Autowired
  private ProcedimientoRepository procedimientoRepository;
  @Autowired
  private PropuestaRepository propuestaRepository;
  @Autowired
  private IntervinienteUtil intervinienteUtil;

  @Override
  public ListWithCountDTO<OcupanteDTOToList> getAllFilteredOcupantes(Integer idBien, Integer id, String nombre, String apellidos, String nacionalidad, Boolean menorEdad, Boolean discapacitado, Boolean dependiente, Boolean residenciaHabitual, String tipoOcupante, Boolean marcaDeudorAnterior, String orderField, String orderDirection, Integer size, Integer page) {
    List<Ocupante> ocupantesSinPaginar = filterOcupantesInMemory(idBien, id, nombre, apellidos, nacionalidad, menorEdad, discapacitado, dependiente, residenciaHabitual, tipoOcupante, marcaDeudorAnterior, orderField, orderDirection);

    List<Ocupante> ocupantes = ocupantesSinPaginar.stream().skip(size * page).limit(size).collect(Collectors.toList());

    List<OcupanteDTOToList> ocupantesDTOS = ocupanteMapper.listOcupanteToListOcupanteDTOToList(ocupantes, idBien);

    for (int i = 0; i < ocupantesDTOS.size(); i++) {
      var a = ocupantesDTOS.get(i).getDatosContacto();
      a = a.stream().sorted((DatosContactoDTOToList h1, DatosContactoDTOToList h2) -> h1.getId().compareTo(h2.getId())).collect(Collectors.toList());
      for (int z = 0; z < a.size(); z++) {

        if (ocupantesDTOS.get(i).getTelefono() == null && ocupantesDTOS.get(i).getDatosContacto().get(z).getMovil() != null && ocupantesDTOS.get(i).getDatosContacto().get(z).getMovil() != "") {
          ocupantesDTOS.get(i).setTelefono(ocupantesDTOS.get(i).getDatosContacto().get(z).getMovil());

        }
      }
    }
    return new ListWithCountDTO<>(ocupantesDTOS, ocupantesSinPaginar.size());
  }

  @Override
  public OcupanteOutputDto findOcupanteByIdDto(Integer idBien, Integer idOcupante) throws NotFoundException {
    Ocupante ocupante = ocupanteRepository.findById(idOcupante).orElseThrow(() -> new NotFoundException("ocupante", idOcupante));
    OcupanteBien ob = ocupanteBienRepository.findByBienPosesionNegociadaIdAndOcupanteId(idBien, idOcupante).orElseThrow(() -> new NotFoundException("ocupante", idOcupante));
    OcupanteOutputDto result = ocupanteMapper.entityOutputDto(ocupante, ob);

    return result;
  }

  @Override
  public List<OcupanteOutputDto> findOcupanteByDniDto(String dni) throws NotFoundException {
    Ocupante ocupante = ocupanteRepository.findByDni(dni).orElse(null);
    List<OcupanteOutputDto> result = new ArrayList<>();
    if (ocupante == null) return result;
    else {
      List<OcupanteBien> obs = ocupanteBienRepository.findAllByOcupanteId(ocupante.getId());
      for (OcupanteBien ob : obs) {
        result.add(ocupanteMapper.entityOutputDto(ocupante, ob));
      }
    }
    return result;
  }

  private Boolean existOcupanteWithOrden(Integer idBien, Integer orden) {
    Optional<OcupanteBien> ob = ocupanteBienRepository.findByBienPosesionNegociadaIdAndOrden(idBien, orden);
    if (ob.isEmpty()) return false;
    return true;
  }

  private Boolean isRelatedWithBien(Integer idBien, Integer idOcupante) {
    Optional<OcupanteBien> ob = ocupanteBienRepository.findByBienPosesionNegociadaIdAndOcupanteId(idBien, idOcupante);
    if (ob.isEmpty()) return false;
    return true;
  }

  @Override
  public OcupanteOutputDto update(Integer idBien, Integer idOcupante, OcupanteInputDto ocupanteDTO) throws Exception {
    Ocupante ocupante = ocupanteRepository.findById(idOcupante).orElseThrow(() -> new NotFoundException("ocupante", idOcupante));
    OcupanteBien ob = ocupanteBienRepository.findByBienPosesionNegociadaIdAndOcupanteId(idBien, idOcupante).orElseThrow(() -> new NotFoundException("ocupante", idOcupante));

    ob.setOrden(ocupanteDTO.getOrden());
    if (ocupanteDTO.getTipoOcupante() != null)
      ob.setTipoOcupante(tipoOcupanteRepository.findById(ocupanteDTO.getTipoOcupante()).orElseThrow(() -> new NotFoundException("tipo de ocupante", ocupanteDTO.getTipoOcupante())));

    OcupanteBien obSaved = ocupanteBienRepository.save(ob);

    if (ocupante.getDni() != null && ocupanteDTO.getDni() != null) {
      if (!ocupante.getDni().equals(ocupanteDTO.getDni()) && checkOcupanteByDni(ocupanteDTO.getDni()) != null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCodeException("There is already an occupant with the DNI entered");
        else throw new InvalidCodeException("Ya existe un ocupante con el DNI introducido");
      }
    }


    BeanUtils.copyProperties(ocupanteDTO, ocupante, "id");

    if (ocupanteDTO.getNacionalidad() != null)
      ocupante.setNacionalidad(paisRepository.findById(ocupanteDTO.getNacionalidad()).orElseThrow(() -> new NotFoundException("tipo de ocupante", ocupanteDTO.getNacionalidad())));
    if (ocupanteDTO.getOtrasSituacionesVulnerabilidad() != null)
      ocupante.setOtrasSituacionesVulnerabilidad(vulnerabilidadRepository.findById(ocupanteDTO.getOtrasSituacionesVulnerabilidad()).orElseThrow(() -> new NotFoundException("tipo de situación de vulnerabilidad", ocupanteDTO.getNacionalidad())));

    if (ocupanteDTO.getDatosContacto() != null && !ocupanteDTO.getDatosContacto().isEmpty()) {
      List<DatosContactoDTOToList> datosContactoDto = ocupanteDTO.getDatosContacto();
      List<DatoContacto> datosContactoSaved = new ArrayList<>();
      for (DatosContactoDTOToList datoContactoDto : datosContactoDto) {
        if (!datoContactoUtil.checkDatoContactoOrderOcupante(datoContactoDto, ocupante.getId()))
          throw new RequiredValueException("Ya existe un dato contacto para el orden: " + datoContactoDto.getOrden());
        // caso en el que se ha modificado un dato contacto existente
        if (datoContactoDto.getId() != null) {
          //AsignacionExpedienteMapper datoContactoMapper;
          datoContactoUtil.updateFieldsOcupante(datoContactoMapper.parse(datoContactoDto), ocupante);
        } else {
          // caso en el que se ha añadido un nuevo dato contacto
          datoContactoUtil.createDatoContactoToOcupante(datoContactoDto, ocupante);
        }
      }
    }

    if (ocupanteDTO.getDirecciones() != null && !ocupanteDTO.getDirecciones().isEmpty()) {
      List<DireccionInputDTO> direccionesDto = ocupanteDTO.getDirecciones();
      List<Direccion> direccionesSaved = new ArrayList<>();
      for (DireccionInputDTO direccionDto : direccionesDto) {
        // comprobamos que no haya una dirección para el interviniente marcada como principal
        if (!direccionUtil.checkDireccionAsPrincipalOcupante(direccionDto, ocupante.getId()))
          throw new RequiredValueException("Ya existe una dirección marcada como principal para el ocupante con el id: " + ocupante.getId());
        // caso en el que se ha modificado una direccion existente
        if (direccionDto.getId() != null) {
          direccionUtil.updateFieldsOcupante(direccionMapper.parse(direccionDto), direccionDto, ocupante);
        } else {
          // caso en el que se ha añadido una nueva direccion
          direccionUtil.createDireccionToOcupante(direccionDto, ocupante);
        }
      }
    }

    var ocupanteSaved = ocupanteRepository.saveAndFlush(ocupante);
    return ocupanteMapper.entityOutputDto(ocupanteSaved, obSaved);
  }

  private Ocupante checkOcupanteByDni(String dni) {
    var result = ocupanteRepository.findByDni(dni).orElse(null);
    return result;
  }

  @Override
  public OcupanteOutputDto create(Integer idBien, OcupanteInputDto ocupanteDTO) throws Exception {
    Ocupante ocupante = new Ocupante();

    // controlamos que no exista un ocupante con el mismo orden
    if (existOcupanteWithOrden(idBien, ocupanteDTO.getOrden())) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCodeException("The order entered cannot be assigned, there is already an occupant with this order for the selected asset");
      else
        throw new InvalidCodeException("No se puede asignar el orden introducido, ya existe un ocupante con este orden para el activo seleccionado");
    }

    // controlamos que el ocupante no esté relacionado con el activo
    if (ocupanteDTO.getId() != null && isRelatedWithBien(idBien, ocupanteDTO.getId())) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCodeException("This occupant is already related to the selected asset");
      else throw new InvalidCodeException("Este ocupante ya está relacionado con el activo seleccionado");
    }

    if (ocupanteDTO.getId() == null) {

      BeanUtils.copyProperties(ocupanteDTO, ocupante);
      ocupante.setActivo(true);

      if (ocupanteDTO.getNacionalidad() != null)
        ocupante.setNacionalidad(paisRepository.findById(ocupanteDTO.getNacionalidad()).orElseThrow(() -> new NotFoundException("país", ocupanteDTO.getNacionalidad())));
      if (ocupanteDTO.getOtrasSituacionesVulnerabilidad() != null)
        ocupante.setOtrasSituacionesVulnerabilidad(vulnerabilidadRepository.findById(ocupanteDTO.getOtrasSituacionesVulnerabilidad()).orElseThrow(() -> new NotFoundException("tipo de situación de vulnerabilidad", ocupanteDTO.getNacionalidad())));

      ocupante = ocupanteRepository.saveAndFlush(ocupante);
      // crear datos contacto
      List<DatosContactoDTOToList> datosContactoDto = ocupanteDTO.getDatosContacto();
      if (datosContactoDto != null && !datosContactoDto.isEmpty()) {
        datoContactoUtil.createDatosContactoToOcupante(datosContactoDto, ocupante);
      }

      List<DireccionInputDTO> direccionesDTO = ocupanteDTO.getDirecciones();
      if (direccionesDTO != null && !direccionesDTO.isEmpty()) {
        direccionUtil.createDireccionesToOcupante(direccionesDTO, ocupante);
      }

      ocupante = ocupanteRepository.saveAndFlush(ocupante);
    } else {
      ocupante = ocupanteRepository.findById(ocupanteDTO.getId()).orElseThrow(() -> new NotFoundException("ocupante", ocupanteDTO.getId()));
    }

    OcupanteBien ob = new OcupanteBien();
    if (ocupanteDTO.getTipoOcupante() != null)
      ob.setTipoOcupante(tipoOcupanteRepository.findById(ocupanteDTO.getTipoOcupante()).orElseThrow(() -> new NotFoundException("tipo de ocupante", ocupanteDTO.getTipoOcupante())));
    var bien = bienPosesionNegociadaRepository.findById(idBien).orElseThrow(() -> new NotFoundException("posesión negociada", idBien));
    ob.setBienPosesionNegociada(bien);
    ob.setOcupante(ocupante);
    ob.setOrden(ocupanteDTO.getOrden());
    var obSaved = ocupanteBienRepository.saveAndFlush(ob);

    return ocupanteMapper.entityOutputDto(ocupante, obSaved);
  }

  public List<Ocupante> filterOcupantesInMemory(Integer idBien, Integer id, String nombre, String apellidos, String nacionalidad, Boolean menorEdad, Boolean discapacitado, Boolean dependiente, Boolean residenciaHabitual, String tipoOcupante, Boolean marcaDeudorAnterior, String orderField, String orderDirection) {

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Ocupante> query = cb.createQuery(Ocupante.class);

    Root<Ocupante> root = query.from(Ocupante.class);
    List<Predicate> predicates = new ArrayList<>();

    Join<Ocupante, OcupanteBien> cbJoin = root.join(Ocupante_.bienes, JoinType.INNER);
    Predicate onOcupanteBien = cb.equal(cbJoin.get(OcupanteBien_.bienPosesionNegociada).get(BienPosesionNegociada_.id), idBien);
    cbJoin.on(onOcupanteBien);

    if (id != null) predicates.add(cb.like(root.get(Ocupante_.id).as(String.class), "%" + id + "%"));

    predicates.add(cb.equal(cbJoin.get(OcupanteBien_.activo), true));

    if (tipoOcupante != null)
      predicates.add(cb.like(cbJoin.get(OcupanteBien_.tipoOcupante).get(TipoOcupante_.valor), "%" + tipoOcupante + "%"));

    if (nombre != null) predicates.add(cb.like(root.get(Ocupante_.nombre), "%" + nombre + "%"));
    if (apellidos != null) predicates.add(cb.like(root.get(Ocupante_.apellidos), "%" + apellidos + "%"));
    if (nacionalidad != null)
      predicates.add(cb.like(root.get(Ocupante_.nacionalidad).get(Pais_.valor), "%" + nacionalidad + "%"));
    if (menorEdad != null)

      predicates.add(cb.equal(root.get(Ocupante_.menorEdad), menorEdad));
    if (discapacitado != null) predicates.add(cb.equal(root.get(Ocupante_.discapacitado), discapacitado));
    if (dependiente != null) predicates.add(cb.equal(root.get(Ocupante_.dependencia), dependiente));
    if (residenciaHabitual != null)
      predicates.add(cb.equal(root.get(Ocupante_.residenciaHabitual), residenciaHabitual));
    if (marcaDeudorAnterior != null) predicates.add(cb.equal(root.get(Ocupante_.deudorAnterior), marcaDeudorAnterior));

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
    if (orderField != null) {
      switch (orderField) {
      /*  case "orden":
          rs.orderBy(getOrden(cb,cbJoin.get(OcupanteBien_.orden),orderDirection));
          break;*/
        case "id":
          rs.orderBy(getOrden(cb, root.get(Ocupante_.id), orderDirection));
          break;

        case "nombre":
          rs.orderBy(getOrden(cb, root.get(Ocupante_.nombre), orderDirection));
          break;
        case "apellidos":
          rs.orderBy(getOrden(cb, root.get(Ocupante_.apellidos), orderDirection));
          break;
        case "nacionalidad":
          rs.orderBy(getOrden(cb, root.get(Ocupante_.nacionalidad), orderDirection));
          break;
        case "menorEdad":
          rs.orderBy(getOrden(cb, root.get(Ocupante_.menorEdad), orderDirection));
          break;
        case "discapacitado":
          rs.orderBy(getOrden(cb, root.get(Ocupante_.discapacitado), orderDirection));
          break;
        case "dependencia":
          rs.orderBy(getOrden(cb, root.get(Ocupante_.dependencia), orderDirection));
          break;
        case "residenciaHabitual":
          rs.orderBy(getOrden(cb, root.get(Ocupante_.residenciaHabitual), orderDirection));
          break;
        case "tipoOcupante":
          rs.orderBy(getOrden(cb, cbJoin.get(OcupanteBien_.tipoOcupante).get(TipoOcupante_.valor), orderDirection));
          break;
        case "deudorAnterior":
          rs.orderBy(getOrden(cb, root.get(Ocupante_.deudorAnterior), orderDirection));
          break;
      }
    }
    return entityManager.createQuery(query).getResultList();
  }

  private Order getOrden(CriteriaBuilder cb, Expression expresion, String orderDirection) {
    if (orderDirection == null) {
      orderDirection = "desc";
    }
    return orderDirection.toLowerCase().equalsIgnoreCase("desc") ? cb.desc(expresion) : cb.asc(expresion);
  }

  public List<OcupanteOutputDto> findOcupantesByExpedientePN(Integer idExpedientePN) {

    ArrayList<Ocupante> listaOcupantes = new ArrayList<>();
    ArrayList<OcupanteOutputDto> listaOcupantesOutputDto = new ArrayList<>();
    ExpedientePosesionNegociada expedientePosesionNegociada = expedientePosesionNegociadaRepository.findById(idExpedientePN).orElse(null);
    Set<BienPosesionNegociada> listaBienesPosesionNegociada = expedientePosesionNegociada.getBienesPosesionNegociada();
    for (BienPosesionNegociada bienPosesionNegociada : listaBienesPosesionNegociada) {
      for (OcupanteBien ocupanteBien : bienPosesionNegociada.getOcupantes()) {
        OcupanteOutputDto oc = new OcupanteOutputDto();
        BeanUtils.copyProperties(ocupanteBien.getOcupante(), oc);
        // oc = ocupanteMapper.OcupanteToOcupanteOutputDto(ocupanteBien.getOcupante());
        listaOcupantesOutputDto.add(oc);
      }
    }
    return listaOcupantesOutputDto;
  }

  public List<OcupanteDTOToList> getOcupantesByBienes(List<Integer> bienes) {
    List<OcupanteBien> obs = ocupanteBienRepository.findDistinctByBienPosesionNegociadaIdIn(bienes);
    List<OcupanteDTOToList> result = obs.stream().map(ob -> ocupanteMapper.OcupanteToOcupanteDTOToList(ob.getOcupante(), ob)).collect(Collectors.toList());
    return result;
  }

  @Override
  public void createDocumentoOcupante(Integer idOcupante, MultipartFile file, MetadatoDocumentoInputDTO metadatoDocumento, Usuario usuario) throws NotFoundException, IOException {
    Ocupante ocupante = ocupanteRepository.findById(idOcupante).orElseThrow(() -> new NotFoundException("Ocupante", idOcupante));
    if (ocupante.getIdHaya() == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("This occupant does not have an identifier in the Master");
      else throw new NotFoundException("Este ocupante no tiene un identificador en el Maestro");
    }
    JSONObject metadatos = new JSONObject();
    JSONObject archivoFisico = new JSONObject();
    archivoFisico.put("contenedor", "CONT");
    metadatos.put("General documento", metadatoDocumento.createDescripcionGeneralDocumento());
    metadatos.put("Archivo físico", archivoFisico);
    long id = this.servicioGestorDocumental.crearDocumento(CodigoEntidad.INTERVINIENTE, ocupante.getIdHaya(), file, metadatos).getIdDocumento();
    Integer idgestorDocumental = (int) id;
    GestorDocumental gestorDocumental = new GestorDocumental(idgestorDocumental, usuario, null, new Date(), null, metadatoDocumento.getNombreFichero(), MatriculasEnum.obtenerPorMatricula(metadatoDocumento.getCatalogoDocumental()), null);
    gestorDocumentalRepository.save(gestorDocumental);
  }

  @Override
  public List<DocumentoDTO> findDocumentosByExpedienteId(Integer id) throws NotFoundException, IOException {
    ExpedientePosesionNegociada expedientePosesionNegociada = expedientePosesionNegociadaRepository.findById(id).orElse(null);
    List<BienPosesionNegociada> listaBienesPosesionNegociada = expedientePosesionNegociada.getBienesPosesionNegociada().stream().collect(Collectors.toList());
    List<String> listadoActivosOcupante = new ArrayList<>();
    for (BienPosesionNegociada bienposesionNegociada : listaBienesPosesionNegociada) {
      for (OcupanteBien ci : bienposesionNegociada.getOcupantes()) {
        Ocupante ocupante = ci.getOcupante();
        String idHaya = ocupante.getIdHaya();
        if (idHaya != null) {
          listadoActivosOcupante.add(idHaya);
        }
      }
      if (listadoActivosOcupante.size() == 0) {
        if (bienposesionNegociada.getOcupantes().size() != 0) {
          log.warn("Ningún Ocupante del ExpedientePN " + id + " tiene idHaya");
        }
        return new ArrayList<>();
      }
    }
    List<DocumentoDTO> listadoIdGestor = this.servicioGestorDocumental.listarDocumentosMulti(CodigoEntidad.INTERVINIENTE, listadoActivosOcupante).stream().map(DocumentoDTO::new).collect(Collectors.toList());

    List<DocumentoDTO> listFinal = gestorDocumentalUseCase.listarDocumentosGestor(listadoIdGestor);
    return listFinal;
  }


  public List<Predicate> getDataExpedientesPN(List<Predicate> predicates, BusquedaDto busquedaDto, Usuario usuario, BusquedaBuilderOcupante busquedaBuilderPN) throws NotFoundException {

    CriteriaBuilder cb = busquedaBuilderPN.getCb();
    var root = busquedaBuilderPN.getRoot();


    Join<Ocupante, OcupanteBien> joinOcupanteBien;
    Join<OcupanteBien, BienPosesionNegociada> joinOcupanteBienPN;
    Join<BienPosesionNegociada, ExpedientePosesionNegociada> joinBienExpedientePN;
    Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> joinAsignacionesExpediente;

    if (busquedaDto == null) {
      busquedaDto = new BusquedaDto();
    }
    var expedienteDto = busquedaDto.getExpediente() != null ? busquedaDto.getExpediente() : null;
    var intervinienteDto = busquedaDto.getInterviniente();
    var contratoDto = busquedaDto.getContrato();
    var bienDto = busquedaDto.getBien();
    var movimientoDto = busquedaDto.getMovimientos();
    var estimacionesDto = busquedaDto.getEstimaciones();
    var propuestaDto = busquedaDto.getPropuestas();
    var agendaDto = busquedaDto.getAgenda();
    var procedimientoDto = busquedaDto.getProcedimiento();

    // COMPRUEBA USUARIO LOGGEADO
    if (usuario.getRolHaya() == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("No Haya role found for this user");
      else throw new NotFoundException("No se ha encontrado un rol de Haya para este usuario");
    }
    if (!usuario.esAdministrador()) {
      var join = busquedaBuilderPN.getJoinAsignacionesExpediente();// root.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
      Predicate asignaciones = cb.equal(join.get(AsignacionExpedientePosesionNegociada_.usuario).get(Usuario_.id), usuario.getId());
      Predicate asignacionesSuplente;
      Predicate predicateFinal = asignaciones;
      if (usuario.getPerfil().getNombre().compareTo(Perfil.GESTOR) == 0) {
        Subquery<Integer> subquerySuplente = cb.createQuery().subquery(Integer.class);
        subquerySuplente.distinct(true);
        Root<ExpedientePosesionNegociada> rootSuplentes = subquerySuplente.from(ExpedientePosesionNegociada.class);
        Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> joinSuplente = rootSuplentes.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
        Predicate predicateSuplente = cb.equal(joinSuplente.join(AsignacionExpedientePosesionNegociada_.SUPLENTE, JoinType.INNER).get(Usuario_.ID), usuario.getId());
        Subquery<Integer> subquerySuplente2 = subquerySuplente.select(rootSuplentes.get(ExpedientePosesionNegociada_.id)).distinct(true).where(predicateSuplente);
        subquerySuplente2.distinct(true);

        predicateFinal = cb.or(asignaciones, root.get(ExpedientePosesionNegociada_.ID).in(subquerySuplente2.distinct(true)));
      }
      predicates.add(predicateFinal);
    }

    //SECCIÓN EXPEDIENTES
    if (expedienteDto != null) {
      if (expedienteDto.getId() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.ID_CONCATENADO), expedienteDto.getId()));
      }
      // SALDO EN GESTION >> Se hace despues de ejecutar la query ya que es un campo calculado
      if (expedienteDto.getGestor() != null) {
        Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> join = busquedaBuilderPN.getJoinBienExpedientePN().join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
        Predicate onCond = cb.equal(join.get(AsignacionExpedientePosesionNegociada_.usuario).get(Usuario_.perfil).get(Perfil_.NOMBRE), "Gestor");
        Join<AsignacionExpedientePosesionNegociada, Usuario> join2 = join.join(AsignacionExpedientePosesionNegociada_.usuario, JoinType.INNER);
        Predicate onCond2 = cb.like(join2.get(Usuario_.NOMBRE), "%" + expedienteDto.getGestor() + "%");
        predicates.add(cb.and(onCond, onCond2));
      }
      if (expedienteDto.getCartera() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.cartera).get(Cartera_.id), expedienteDto.getCartera()));
      }
      if (expedienteDto.getResponsableCartera() != null) {
        predicates.add(cb.like(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.cartera).get(Cartera_.responsableCartera).get(Usuario_.nombre), "%" + expedienteDto.getResponsableCartera() + "%"));
      }
      if (expedienteDto.getEstado() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().join(ExpedientePosesionNegociada_.estadoExpedienteRecuperacionAmistosa).get(EstadoExpedientePosesionNegociada_.id), expedienteDto.getEstado()));
      }
      if (expedienteDto.getSubestado() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }
      if (expedienteDto.getSupervisor() != null) {
        Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> ExAe = busquedaBuilderPN.getJoinBienExpedientePN().join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
        Join<AsignacionExpedientePosesionNegociada, Usuario> AeUs = ExAe.join(AsignacionExpedientePosesionNegociada_.usuario, JoinType.INNER);
        Predicate p1 = cb.like(AeUs.get(Usuario_.nombre), "%" + expedienteDto.getSupervisor() + "%");
        Predicate p2 = cb.equal(AeUs.get(Usuario_.perfil).get(Perfil_.nombre), "Supervisor");
        predicates.add(cb.and(p1, p2));
      }
      if (expedienteDto.getEstrategia() != null) {
        Join<ExpedientePosesionNegociada, Contrato> join = busquedaBuilderPN.getJoinBienExpedientePN().join(ExpedientePosesionNegociada_.contrato, JoinType.INNER);
        Predicate p1 = cb.isTrue(join.get(Contrato_.esRepresentante));
        Join<Contrato, EstrategiaAsignada> join1 = join.join(Contrato_.ESTRATEGIA, JoinType.INNER);
        Predicate p2 = cb.equal(join1.get(EstrategiaAsignada_.ESTRATEGIA).get(Estrategia_.ID), expedienteDto.getEstrategia());
        predicates.add(cb.and(p1, p2));
      }
      if (expedienteDto.getSituacion() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinContratoRepresentante().get(Contrato_.situacion).get(Situacion_.id), expedienteDto.getSituacion()));
      }
      if (expedienteDto.getCliente() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().join(ExpedientePosesionNegociada_.cartera).join(Cartera_.clientes).join(ClienteCartera_.cliente).get(Cliente_.id), expedienteDto.getCliente()));
      }
      if (expedienteDto.getGrupoCliente() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().join(ExpedientePosesionNegociada_.cartera).join(Cartera_.clientes).join(ClienteCartera_.cliente).join(Cliente_.grupo).get(GrupoCliente_.id), expedienteDto.getGrupoCliente()));
      }
    }

    // SECCIÓN DE INTERVINIENTES / OCUPANTES
    if (intervinienteDto != null) {
      Join<OcupanteBien, Ocupante> joinsOcupante;
      if (intervinienteDto.getDocId() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.equal(joinsOcupante.get(Ocupante_.dni), intervinienteDto.getDocId()));
      }
      if (intervinienteDto.getId() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.equal(joinsOcupante.get(Ocupante_.id), intervinienteDto.getId()));
      }
      if (intervinienteDto.getApellidos() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.equal(joinsOcupante.get(Ocupante_.apellidos), intervinienteDto.getApellidos()));
      }
      if (intervinienteDto.getNombre() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.equal(joinsOcupante.get(Ocupante_.nombre), intervinienteDto.getNombre()));
      }
      if (intervinienteDto.getNacionalidad() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.equal(joinsOcupante.get(Ocupante_.nacionalidad).get(Pais_.id), intervinienteDto.getNacionalidad()));
      }
      if (intervinienteDto.getEmail() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.like(joinsOcupante.join(Ocupante_.datosContacto,JoinType.INNER).get(DatoContacto_.email), intervinienteDto.getEmail()));
      }
      if (intervinienteDto.getTelefono() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.like(joinsOcupante.join(Ocupante_.datosContacto,JoinType.INNER).get(DatoContacto_.movil), intervinienteDto.getTelefono()));
      }
      if (intervinienteDto.getIdIntervinienteOrigen() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getResidencia() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }

      if (intervinienteDto.getProvincia() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getLocalidad() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getVulnerabilidad() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getTipoIntervencion() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getIntervaloFecha() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }
    }


    //SECCIÓN DE CONTRATOS
    if (contratoDto != null) {
      Join<ExpedientePosesionNegociadaCase, Contrato> joinContrato;
      if (contratoDto.getId() != null) {
        joinContrato = busquedaBuilderPN.getJoinContrato();
        predicates.add(cb.equal(joinContrato.get(Contrato_.ID_CARGA), contratoDto.getId()));
      }
      if (contratoDto.getEstado() != null) {
        joinContrato = busquedaBuilderPN.getJoinContrato();
        predicates.add(cb.equal(joinContrato.get(Contrato_.estadoContrato), contratoDto.getEstado()));
      }
      if (contratoDto.getProducto() != null) {
        joinContrato = busquedaBuilderPN.getJoinContrato();
        predicates.add(cb.equal(joinContrato.get(Contrato_.producto.getName()), contratoDto.getProducto()));
      }
      if (contratoDto.getClasificacionContable() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinContratos().get(Contrato_.clasificacionContable).get(ClasificacionContable_.ID), contratoDto.getClasificacionContable()));
      }
      if (contratoDto.getContratoOrigen() != null) {
        predicates.add(cb.like(busquedaBuilderPN.getJoinContratos().get(Contrato_.idCarga), contratoDto.getContratoOrigen()));
      }
      if (contratoDto.getReestructurado() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinContratos().get(Contrato_.reestructurado), contratoDto.getReestructurado()));
      }
      if (contratoDto.getNumCuotasImpago() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinContratos().get(Contrato_.numeroCuotasImpagadas), contratoDto.getNumCuotasImpago()));
      }
      if (contratoDto.getTipoEstimacion() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }
      if (contratoDto.getSubtipoEstimacion() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }
      if (contratoDto.getIntervaloImporte() != null) {
        switch (contratoDto.getIntervaloImporte().getDescripcion()) {
          case "capitalPendiente":
            predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinSaldos().get(Saldo_.capitalPendiente), contratoDto.getIntervaloImporte());
            break;
          case "importeInicial":
            predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.importeInicial), contratoDto.getIntervaloImporte());
            break;
          case "saldoGestion":
            predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinSaldos().get(Saldo_.saldoGestion), contratoDto.getIntervaloImporte());
            break;
          case "deudaImpagada":
            predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinSaldos().get(Saldo_.deudaImpagada), contratoDto.getIntervaloImporte());
            break;
          default:
            break;
        }
      }
      if (contratoDto.getIntervaloFechas() != null && contratoDto.getIntervaloFechas().size() > 0) {
        for (IntervaloFecha busqueda : contratoDto.getIntervaloFechas()) {
          switch (busqueda.getDescripcion()) {
            case "formalizacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaFormalizacion), busqueda);
              break;
            case "vencimientoInicial":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaVencimientoInicial), busqueda);
              break;
            case "vencimientoAnticipado":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaVencimientoAnticipado), busqueda);
              break;
            case "paseRegular":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaPaseRegular), busqueda);
              break;
            case "primerImpago":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaPrimerImpago), busqueda);
              break;
            case "primeraAsignacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaPrimeraAsignacion), busqueda);
              break;
            case "ultimaAsignacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaUltimaAsignacion), busqueda);
              break;
            default:
              break;
          }
        }
      }
    }

    // Si expedientePosesionNegociada no se encuentra relacionado a un bien no aparecerá en la
    // respuesta
    // SECCIÓN DE BIENES
    if (bienDto != null) {
      Join<BienPosesionNegociada, Bien> joinBien2;
      if (bienDto.getId() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.idHaya), bienDto.getId()));
      }
      if (bienDto.getIdOrigen() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.idCarga), bienDto.getIdOrigen()));
      }
      if (bienDto.getDireccion() != null) {
        predicates.add(cb.like(busquedaBuilderPN.getJoinBien().get(Bien_.nombreVia), bienDto.getDireccion()));
      }
      if (bienDto.getProvincia() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.provincia), bienDto.getProvincia()));
      }
      if (bienDto.getMunicipio() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.municipio), bienDto.getMunicipio()));
      }
      if (bienDto.getLocalidad() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.localidad), bienDto.getLocalidad()));
      }
      if (bienDto.getRegistro() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.registro), bienDto.getRegistro()));
      }
      if (bienDto.getLocalidadRegistro() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.localidadRegistro), bienDto.getLocalidadRegistro()));
      }
      if (bienDto.getFincaRegistral() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.finca), bienDto.getFincaRegistral()));
      }
      if (bienDto.getReferenciaCatastral() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.referenciaCatastral), bienDto.getReferenciaCatastral()));
      }
      if (bienDto.getIdufir() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.idufir), bienDto.getIdufir()));
      }

      if (bienDto.getIntervaloImporte() != null) {
        if (bienDto.getIntervaloImporte().getDescripcion() != null) {
          switch (bienDto.getIntervaloImporte().getDescripcion()) {
            case "tasacion":
              predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinTasacionBien().get(Tasacion_.importe), bienDto.getIntervaloImporte());
              break;
            case "valoracion":
              predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinValoracionBien().get(Valoracion_.importe), bienDto.getIntervaloImporte());
              break;
            case "adjudicacion":
              // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
              predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
              break;
            default:
              break;
          }
        }
      }
      if (bienDto.getIntervaloFechas() != null && bienDto.getIntervaloFechas().size() > 0) {
        for (IntervaloFecha busqueda : bienDto.getIntervaloFechas()) {
          switch (busqueda.getDescripcion()) {
            case "inscripcion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.contratos, JoinType.INNER).get(ContratoBien_.bien).get(Bien_.fechaInscripcion), busqueda);
              break;
            case "primeraValoracion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.valoraciones, JoinType.INNER).get(Valoracion_.fecha), busqueda);
              break;
            case "primeraTasacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.tasaciones, JoinType.INNER).get(Tasacion_.fecha), busqueda);
              break;
            case "reoConversion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.contratos, JoinType.INNER).get(ContratoBien_.bien).get(Bien_.fechaReoConversion), busqueda);
              break;
            case "anotacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.cargas, JoinType.INNER).get(Carga_.fechaAnotacion), busqueda);
              break;
            case "vencimientoCarga":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.cargas, JoinType.INNER).get(Carga_.fechaVencimiento), busqueda);
              break;
            case "asignacionPN":
              // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
              predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
              break;
            default:
              break;
          }
        }
      }
    }

    // SECCIÓN MOVIMIENTOS
    if (movimientoDto != null) {
      if (movimientoDto.getTipoMovimiento() != null) {
        Join<ExpedientePosesionNegociada, Contrato> joinContrato = root.join(ExpedientePosesionNegociada_.CONTRATO, JoinType.INNER);
        Join<Contrato, Movimiento> joinMovimiento = joinContrato.join(Contrato_.movimientos, JoinType.INNER);
        predicates.add(cb.equal(joinMovimiento.get(Movimiento_.tipo), movimientoDto.getTipoMovimiento()));
      }
      if (movimientoDto.getIntervaloImporte() != null) {
        predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinContrato().join(Contrato_.movimientos, JoinType.INNER).get(Movimiento_.importe), movimientoDto.getIntervaloImporte());
      }

      if (movimientoDto.getIntervaloFechas() != null && movimientoDto.getIntervaloFechas().size() > 0) {
        for (IntervaloFecha busqueda : movimientoDto.getIntervaloFechas()) {
          switch (busqueda.getDescripcion()) {
            case "valor":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContrato().join(Contrato_.movimientos, JoinType.INNER).get(Movimiento_.fechaValor), busqueda);
              break;
            case "contable":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContrato().join(Contrato_.movimientos, JoinType.INNER).get(Movimiento_.fechaContable), busqueda);
              break;
            default:
              break;
          }
        }
      }
    }

    // SECCIÓN ESTIMACIONES
    if (estimacionesDto != null) {
      Join<Expediente, Estimacion> joinEstimacion;
      if (estimacionesDto.getIntervaloFechas() != null && estimacionesDto.getIntervaloFechas().size() > 0) {
        joinEstimacion = busquedaBuilderPN.getJoinEstimacion();
        for (IntervaloFecha busqueda : estimacionesDto.getIntervaloFechas()) {
          switch (busqueda.getDescripcion()) {
            case "creacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, joinEstimacion.get(Estimacion_.fecha), busqueda);
              break;
            case "resolucion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, joinEstimacion.get(Estimacion_.fechaEstimada), busqueda);
              break;
            default:
              break;
          }
        }
      }
      if (estimacionesDto.getTipoPropuesta() != null) {
        joinEstimacion = busquedaBuilderPN.getJoinEstimacion();
        predicates.add(cb.equal(joinEstimacion.get(Estimacion_.tipo), estimacionesDto.getTipoPropuesta()));
      }
      if (estimacionesDto.getSubtipoPropuesta() != null) {
        joinEstimacion = busquedaBuilderPN.getJoinEstimacion();
        predicates.add(cb.equal(joinEstimacion.get(Estimacion_.subtipo), estimacionesDto.getSubtipoPropuesta()));
      }
      if (estimacionesDto.getProbalididadResolucion() != null) {
        joinEstimacion = busquedaBuilderPN.getJoinEstimacion();
        predicates.add(cb.equal(joinEstimacion.get(Estimacion_.probabilidadResolucion), estimacionesDto.getProbalididadResolucion()));
      }
    }

    // SECCION PROCEDIMIENTO
    if (procedimientoDto != null) {
      Join<BienPosesionNegociada, ProcedimientoPosesionNegociada> joinProcedimiento;
      //Revisar intervalofechas
      if (procedimientoDto.getLetrado() != null) {
        joinProcedimiento = busquedaBuilderPN.getJoinProcedimiento();
        predicates.add(cb.equal(joinProcedimiento.get(ProcedimientoPosesionNegociada_.letrado), procedimientoDto.getLetrado()));
      }
      if (procedimientoDto.getProcurador() != null) {
        joinProcedimiento = busquedaBuilderPN.getJoinProcedimiento();
        predicates.add(cb.equal(joinProcedimiento.get(ProcedimientoPosesionNegociada_.procurador), procedimientoDto.getProcurador()));
      }
      if (procedimientoDto.getJuzgado() != null) {
        joinProcedimiento = busquedaBuilderPN.getJoinProcedimiento();
        predicates.add(cb.equal(joinProcedimiento.get(ProcedimientoPosesionNegociada_.juzgado), procedimientoDto.getJuzgado()));
      }
      if (procedimientoDto.getAdministradorConcursal() != null) {
        var joinProcedimientoGestionAmistosa = busquedaBuilderPN.getJoinProcedimientoGestionAmistosa();
        predicates.add(cb.equal(joinProcedimientoGestionAmistosa.get(Procedimiento_.nombreAdminConcursal), procedimientoDto.getAdministradorConcursal()));
      }
      if (procedimientoDto.getTipoProcedimiento() != null) {
        joinProcedimiento = busquedaBuilderPN.getJoinProcedimiento();
        predicates.add(cb.equal(joinProcedimiento.get(ProcedimientoPosesionNegociada_.tipo), procedimientoDto.getTipoProcedimiento()));
      }
      if (procedimientoDto.getHitoProcedimiento() != null) {
        var joinProcedimientoGestionAmistosa = busquedaBuilderPN.getJoinProcedimientoGestionAmistosa();
        predicates.add(cb.equal(joinProcedimientoGestionAmistosa.get(Procedimiento_.hitoProcedimiento), procedimientoDto.getHitoProcedimiento()));
      }
      if (procedimientoDto.getIntervaloFecha() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
      }
      if (procedimientoDto.getNumeroAutos() != null && !procedimientoDto.getNumeroAutos().equals("")) {
        joinProcedimiento = busquedaBuilderPN.getJoinProcedimiento();
        predicates.add(cb.like(joinProcedimiento.get(ProcedimientoPosesionNegociada_.AUTOS), '%' + procedimientoDto.getNumeroAutos() + '%'));
      }
    }

    // SECCION PROPUESTAS
    if (propuestaDto != null) {
      Join<ExpedientePosesionNegociadaCase, Propuesta> joinPropuesta;
      //seleccionamos sólo las propuestas activas
      var filtrarPropuestasActivas = false;
      if (propuestaDto.getFormalizacion() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.formalizaciones), propuestaDto.getFormalizacion()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getClase() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.clasePropuesta), propuestaDto.getClase()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getTipo() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.tipoPropuesta), propuestaDto.getTipo()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getSubtipo() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.subtipoPropuesta), propuestaDto.getSubtipo()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getEstado() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.estadoPropuesta), propuestaDto.getEstado()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getSancion() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.sancionPropuesta), propuestaDto.getSancion()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getResolucion() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.resolucionPropuesta), propuestaDto.getResolucion()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getGestor() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.like(joinPropuesta.get(Propuesta_.usuario).get(Usuario_.nombre), propuestaDto.getGestor()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getAnalista() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.like(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.analistaCliente), propuestaDto.getAnalista()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getGestorRecuperaciones() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.like(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.gestorRecuperacionCliente), propuestaDto.getGestorRecuperaciones()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getNotaria() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.like(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.direccionNotaria), propuestaDto.getNotaria()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }

      if (propuestaDto.getFormalizacion() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        if (propuestaDto.getFormalizacion().getGestor() != null)
          predicates.add(cb.like(busquedaBuilderPN.getJoinAsignacionesExpediente().get(AsignacionExpediente_.USUARIO).get(Usuario_.NOMBRE), propuestaDto.getFormalizacion().getGestor()));
        if (propuestaDto.getFormalizacion().getGestorCliente() != null)
          predicates.add(cb.like(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.gestorFormalizacionCliente), propuestaDto.getFormalizacion().getGestorCliente()));
        if (propuestaDto.getFormalizacion().getDocumentacionCumplimentada() != null){
          if (propuestaDto.getFormalizacion().getDocumentacionCumplimentada())
            predicates.add(cb.equal(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.ESTADO_CHECKLIST).get(EstadoChecklist_.CODIGO), "ok"));
          else
            predicates.add(cb.notEqual(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.ESTADO_CHECKLIST).get(EstadoChecklist_.CODIGO), "ok"));
        }
        if (propuestaDto.getFormalizacion().getPbc() != null)
          predicates.add(cb.equal(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.pbc), propuestaDto.getFormalizacion().getPbc()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }

      // TODO hay que treminar el refactor
      if (propuestaDto.getIntervaloImporte() != null) {
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
        switch (propuestaDto.getIntervaloImporte().getDescripcion()) {
          case "deuda":
            Subquery<Double> subqueryDeudaImpagada = cb.createQuery().subquery(Double.class);
            Root<Contrato> importesContratos = subqueryDeudaImpagada.from(Contrato.class);
            predicates = busquedaBuilderPN.addIntervaloImporte(predicates, subqueryDeudaImpagada.select(cb.sum(importesContratos.get(Contrato_.deudaImpagada))).distinct(true).where(cb.equal(busquedaBuilderPN.getJoinContratosPropuestas().get(Propuesta_.ID), importesContratos.join(Contrato_.propuestas).get(Propuesta_.id))), propuestaDto.getIntervaloImporte());
            break;
          case "propuesta":
            busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinPropuestas().get(Propuesta_.importeTotal), propuestaDto.getIntervaloImporte());
            break;
          case "quita":
            Subquery<Double> subqueryQuita = cb.createQuery().subquery(Double.class);
            Root<ImportePropuesta> importes = subqueryQuita.from(ImportePropuesta.class);
            busquedaBuilderPN.addIntervaloImporte(predicates, subqueryQuita.select(cb.sum(importes.get(ImportePropuesta_.condonacionDeuda))).distinct(true).where(cb.equal(busquedaBuilderPN.getJoinImportesPropuestas().get(Propuesta_.ID), importes.join(ImportePropuesta_.propuestas).get(Propuesta_.id))), propuestaDto.getIntervaloImporte());
            break;
          case "prestamoInicial":
            Subquery<Double> subqueryPrestamoInicial = cb.createQuery().subquery(Double.class);
            Root<Contrato> importesContratos2 = subqueryPrestamoInicial.from(Contrato.class);
            busquedaBuilderPN.addIntervaloImporte(predicates, subqueryPrestamoInicial.select(cb.sum(importesContratos2.get(Contrato_.importeDispuesto))).distinct(true).where(cb.equal(busquedaBuilderPN.getJoinContratosPropuestas().get(Propuesta_.ID), importesContratos2.join(Contrato_.propuestas).get(Propuesta_.id))), propuestaDto.getIntervaloImporte());
            break;
          case "valoracion":
            // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
            predicates.add(cb.equal(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id), -1));
            break;
          default:
            break;
        }
      }

      if (propuestaDto.getIntervaloFechas() != null && propuestaDto.getIntervaloFechas().size() > 0) {
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
        for (IntervaloFecha busqueda : propuestaDto.getIntervaloFechas()) {
          switch (busqueda.getDescripcion()) {
            case "alta":
              busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinPropuestas().get(Propuesta_.fechaAlta), busqueda);
              break;
            case "sancion":
              busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinFormalizacionPropuesta().get(Formalizacion_.fechaSancion), busqueda);
              break;
            case "firma":
              busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinFormalizacionPropuesta().get(Formalizacion_.fechaFirma), busqueda);
              break;
            case "ultimoCambio":
              busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinPropuestas().get(Propuesta_.fechaUltimoCambio), busqueda);
              break;
            case "posesion":
              busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinFormalizacionPropuesta().get(Formalizacion_.fechaPosesion), busqueda);
              break;
            default:
              break;
          }
        }
      }
      if (filtrarPropuestasActivas) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.activo), true));
      }
    }

    // SECCIÓN AGENDA sería a partir de UsuarioAgenda
    if (agendaDto != null) {
      if (agendaDto.getEmisor() != null || agendaDto.getDestinatario() != null || agendaDto.getEstado() != null || agendaDto.getIntervaloFechas() != null || agendaDto.getClaseEvento() != null) {
        List<Integer> expedientesIds = getExpedienteFilteredByAgenda(usuario, agendaDto);
        predicates.add(busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.id).in(expedientesIds));
      }
    }
    return predicates;
  }


  private List<Integer> getExpedienteFilteredByAgenda(Usuario loggedUser, BusquedaAgendaDto busquedaAgendaDto) throws NotFoundException {
    List<Evento> eventos = eventoUtil.getListFilteredEventos(loggedUser.getId(), busquedaAgendaDto);
    List<Integer> expedientes = null;
    if (eventos != null) {
      expedientes = new ArrayList<>();
      for (Evento evento : eventos) {
        switch (evento.getNivel().getCodigo()) {
          case "EXP":
            Expediente expediente = expedienteRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("expediente", evento.getIdNivel()));
            if (!expedientes.contains(expediente.getId())) expedientes.add(expediente.getId());
            break;
          case "CON":
            Contrato contrato = contratoRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("contrato", evento.getIdNivel()));
            if (contrato.getExpediente() != null && !expedientes.contains(contrato.getExpediente().getId()))
              expedientes.add(contrato.getExpediente().getId());
            break;
          case "BIEN":
            Bien bien = bienRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("bien", evento.getIdNivel()));
            List<ContratoBien> contratosBien = new ArrayList<>(bien.getContratos());
            if (contratosBien != null) {
              for (ContratoBien cb : contratosBien) {
                if (bien.getId().equals(cb.getBien().getId()) && cb.getContrato().getExpediente() != null && !expedientes.contains(cb.getContrato().getExpediente().getId()))
                  expedientes.add(cb.getContrato().getExpediente().getId());
              }
            }
            break;
          case "INTE":
            Interviniente interviniente = intervinienteRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("interviniente", evento.getIdNivel()));
            List<ContratoInterviniente> contratosInterviniente = new ArrayList<>(interviniente.getContratos());
            if (contratosInterviniente != null) {
              for (ContratoInterviniente ci : contratosInterviniente) {
                if (interviniente.getId().equals(ci.getInterviniente().getId()) && ci.getContrato().getExpediente() != null && !expedientes.contains(ci.getContrato().getExpediente().getId()))
                  expedientes.add(ci.getContrato().getExpediente().getId());
              }
            }
            break;
          case "JUD":
            Procedimiento procedimiento = procedimientoRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("procedimiento", evento.getIdNivel()));
            List<Contrato> contratosProcedimiento = new ArrayList<>(procedimiento.getContratos());
            if (contratosProcedimiento != null) {
              for (Contrato c : contratosProcedimiento) {
                if (c.getExpediente() != null && !expedientes.contains(c.getExpediente().getId()))
                  expedientes.add(c.getExpediente().getId());
              }
            }
            break;
          case "PRO":
            Propuesta propuesta = propuestaRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("propuesta", evento.getIdNivel()));
            List<Contrato> contratosPropuesta = new ArrayList<>(propuesta.getContratos());
            if (propuesta.getExpediente() != null) {
              if (!expedientes.contains(propuesta.getExpediente().getId()))
                expedientes.add(propuesta.getExpediente().getId());
            } else if (contratosPropuesta != null) {
              for (Contrato c : contratosPropuesta) {
                if (c.getExpediente() != null && !expedientes.contains(c.getExpediente().getId()))
                  expedientes.add(c.getExpediente().getId());
              }
            }
            break;
          default:
            break;

        }
      }
    }
    return expedientes;
  }

  public List<Ocupante> filtrarOcupantesBusqueda(BusquedaDto busquedaDto, Usuario usuario, String orderField, String orderDirection) throws NotFoundException {
    var expedienteDto = busquedaDto.getExpediente();
    var tempCriteriaBuilder = entityManager.getCriteriaBuilder();
    var query = tempCriteriaBuilder.createQuery(Ocupante.class);

    BusquedaBuilderOcupante busquedaBuilderPN = new BusquedaBuilderOcupante(query.from(Ocupante.class), tempCriteriaBuilder);
    List<Predicate> predicates = new ArrayList<>();


    predicates = getDataExpedientesPN(predicates, busquedaDto, usuario, busquedaBuilderPN);
    var cb = busquedaBuilderPN.getCb();
    var root = busquedaBuilderPN.getRoot();

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()])).distinct(true);

    // ORDENACION
    if (orderField != null) {
      switch (orderField) {
        case "origen":
          rs.orderBy(getOrden(cb, busquedaBuilderPN.getJoinBienExpedientePN().get(ExpedientePosesionNegociada_.origen).get(OrigenExpedientePosesionNegociada_.valor), orderDirection));
          break;
        case "tipo":
          rs.orderBy(getOrden(cb, busquedaBuilderPN.getJoinBienExpedientePN().join(ExpedientePosesionNegociada_.cartera, JoinType.INNER).get(Cartera_.TIPO_ACTIVOS), orderDirection));
          break;
        case "gestor":
          Join<ExpedientePosesionNegociada_, AsignacionExpedientePosesionNegociada> join41 = root.join(ExpedientePosesionNegociada_.ASIGNACIONES, JoinType.INNER);
          Join<AsignacionExpedientePosesionNegociada, Usuario> join42 = join41.join(AsignacionExpedientePosesionNegociada_.USUARIO, JoinType.INNER);
          rs.orderBy(getOrden(cb, join42.get(Usuario_.nombre), orderDirection));
          break;
        case "cliente":
          Join<ClienteCartera, Cliente> join22 = busquedaBuilderPN.getJoinBienExpedientePN().join(ExpedientePosesionNegociada_.cartera, JoinType.INNER).join(Cartera_.clientes, JoinType.INNER).join(ClienteCartera_.cliente, JoinType.INNER);
          rs.orderBy(getOrden(cb, join22.get(Cliente_.NOMBRE), orderDirection));
          break;
        case "cartera":
          rs.orderBy(getOrden(cb, busquedaBuilderPN.getJoinBienExpedientePN().join(ExpedientePosesionNegociada_.cartera, JoinType.INNER).get(Cartera_.NOMBRE), orderDirection));
          break;
        case "usuarioId":
          var join = root.join(ExpedientePosesionNegociada_.ASIGNACIONES, JoinType.INNER).join(AsignacionExpediente_.USUARIO, JoinType.INNER);
          rs.orderBy(getOrden(cb, join.get(Usuario_.ID), orderDirection));
          break;
        case "estado":
          rs.orderBy(getOrden(cb, busquedaBuilderPN.getJoinBienExpedientePN().join(ExpedientePosesionNegociada_.estadoExpedienteRecuperacionAmistosa).get(EstadoExpedientePosesionNegociada_.codigo), orderDirection));
          break;
      }


    }
    var listaResultados = entityManager.createQuery(rs).getResultList();

    // FILTRADO POST QUERY
    {
      // FILTRADO EXPEDIENTE :: SALDO EN GESTION
      if (expedienteDto != null && expedienteDto.getSaldoGestion() != null) {
        var saldoBusqueda = expedienteDto.getSaldoGestion();
      /*  listaResultados = listaResultados
          .stream()
          .filter((ExpedientePosesionNegociada exp)
            -> {
            boolean mayorQue = true;
            boolean menorQue = true;
            if (saldoBusqueda.getIgual() != null)
              return saldoBusqueda.getIgual().equals(exp.calculaImporteBien());
            if (saldoBusqueda.getMayor() != null)
              menorQue = Double.compare(exp.calculaImporteBien(), saldoBusqueda.getMayor()) < 0;
            if (saldoBusqueda.getMenor() != null)
              mayorQue = Double.compare(exp.calculaImporteBien(), saldoBusqueda.getMenor()) > 0;
            boolean mayorQueAndMenorQue = mayorQue && menorQue;
            return mayorQueAndMenorQue;
          }).collect(Collectors.toList());*/
      }

      // NUMERO ACTIVOS

    /*  if (numeroBienes != null)
        listaResultados = listaResultados.stream().filter(exp -> exp.getBienesPosesionNegociada().size() == numeroBienes).collect(Collectors.toList());
      else {
        if ("numeroActivos".equals(orderField)) {
          if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
            listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.getBienesPosesionNegociada().size()).reversed()).
              collect(Collectors.toList());
          else
            listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.getBienesPosesionNegociada().size())).
              collect(Collectors.toList());
        } else if ("saldo".equals(orderField)) {
          if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
            listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.calculaImporteBien()).reversed()).
              collect(Collectors.toList());
          else
            listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.calculaImporteBien())).
              collect(Collectors.toList());
        }
      }*/
    }
    return listaResultados;
  }

  @Override
  public List<IntervinienteOcupanteDatosContactoDTO> findOcupantesDatosContacto(String tipo, Integer idBienPN) {

    List<Ocupante> listOcupantes = ocupanteRepository.findAllOcupantesByBienPosesionNegociada(idBienPN);
    List<IntervinienteOcupanteDatosContactoDTO> listOcupanteDatosContactoDTO = new ArrayList<IntervinienteOcupanteDatosContactoDTO>();

    for (Ocupante ocupante : listOcupantes) {

      IntervinienteOcupanteDatosContactoDTO ocupanteDatosContactoDTO = new IntervinienteOcupanteDatosContactoDTO();
      BeanUtils.copyProperties(ocupante, ocupanteDatosContactoDTO);
      ocupanteDatosContactoDTO.setNumeroDocumento(ocupante.getDni());

      // Recogemos el id del Interviniente
      Integer idOcupante = ocupante.getId();

      // Recorremos el listado de datos de contacto para asociar los datos con el interviniente
      List<DatoContacto> listDatoContacto = ocupante.getDatosContacto().stream().collect(Collectors.toCollection(ArrayList::new));

      if (listDatoContacto != null && listDatoContacto.size() > 0) {
        for (DatoContacto datoContacto : listDatoContacto) {
          if (datoContacto.getOcupante().getId() == idOcupante) {
            //    intervinienteUtil.parteDatosContactoToIntervinienteDatosContactoDTO(ocupanteDatosContactoDTO, datoContacto);
          }
        }
      }

      // Si el tipo es carta, además deberemos añadir los datos de dirección al interviniente
      if ("carta".equals(tipo)) {
        List<Direccion> listDireccion = ocupante.getDirecciones().stream().collect(Collectors.toCollection(ArrayList::new));
        if (listDireccion != null && listDireccion.size() > 0) {
          for (Direccion direccion : listDireccion) {
            if (direccion.getOcupante().getId() == idOcupante) {
              intervinienteUtil.parseDireccionToString(ocupanteDatosContactoDTO, direccion);
            }
          }
        }
      }

      listOcupanteDatosContactoDTO.add(ocupanteDatosContactoDTO);
    }

    return listOcupanteDatosContactoDTO;
  }

  @Override
  public List<IntervinienteOcupanteDatosContactoDTO> getAllOcupantesContacto(Integer IdExpediente) throws NotFoundException {

    ExpedientePosesionNegociada expedientePosesionNegociada = expedientePosesionNegociadaRepository.findById(IdExpediente).orElseThrow(() -> new NotFoundException("expedientePosesionNegociada", IdExpediente));

    List<BienPosesionNegociada> bienPosesionNegociadaList = expedientePosesionNegociada.getBienesPosesionNegociada().stream().collect(Collectors.toList());
    List<Ocupante> ocupanteList = new ArrayList<>();
    List<IntervinienteOcupanteDatosContactoDTO> intervinienteOcupanteDatosContactoDTOList = new ArrayList<>();

    for (BienPosesionNegociada bienPN : bienPosesionNegociadaList) {
      List<OcupanteBien> ocupanteBienList = bienPN.getOcupantes().stream().collect(Collectors.toList());
      for (OcupanteBien ocupanteBien : ocupanteBienList) {
        Ocupante ocupante = ocupanteBien.getOcupante();
        ocupanteList.add(ocupante);
      }
    }
    ocupanteList = ocupanteList.stream().distinct().collect(Collectors.toList());
    for (Ocupante ocupante : ocupanteList) {
      /*   IntervinienteOcupanteDatosContactoDTO intervinienteOcupanteDatosContactoDTO =
      new IntervinienteOcupanteDatosContactoDTO();*/
      List<DatoContacto> datoContactoList = ocupante.getDatosContacto().stream().collect(Collectors.toList());
      List<String> movilList = new ArrayList<>();
      List<String> emailList = new ArrayList<>();
      List<String> fijoList = new ArrayList<>();
      if (!datoContactoList.isEmpty()) {
        IntervinienteOcupanteDatosContactoDTO intervinienteOcupanteDatosContactoDTO = new IntervinienteOcupanteDatosContactoDTO();
        for (DatoContacto datoContacto : datoContactoList) {
          /*     IntervinienteOcupanteDatosContactoDTO intervinienteOcupanteDatosContactoDTO =
          new IntervinienteOcupanteDatosContactoDTO();*/
          intervinienteOcupanteDatosContactoDTO.setId(ocupante.getId());
          intervinienteOcupanteDatosContactoDTO.setNombre(ocupante.getNombre() != null ? ocupante.getNombre() : null);
          intervinienteOcupanteDatosContactoDTO.setApellidos(ocupante.getApellidos() != null ? ocupante.getApellidos() : null);

          if (datoContacto.getMovil() != null && !datoContacto.getMovil().equals("")) {
            movilList.add(datoContacto.getMovil());
          }
          if (datoContacto.getEmail() != null && !datoContacto.getEmail().equals("")) {
            emailList.add(datoContacto.getEmail());
          }
          if (datoContacto.getFijo() != null && !datoContacto.getFijo().equals("")) {
            fijoList.add(datoContacto.getFijo());
          }
          intervinienteOcupanteDatosContactoDTO.setMovilList(movilList);
          intervinienteOcupanteDatosContactoDTO.setEmailList(emailList);
          intervinienteOcupanteDatosContactoDTO.setTelefonoList(fijoList);

          /* intervinienteOcupanteDatosContactoDTO.setNumeroDocumento(
          ocupante.getNumeroDocumento() != null ? ocupante.getNumeroDocumento() : null);*/

          /*  intervinienteOcupanteDatosContactoDTO.setMovil(
          datoContacto.getMovil() != null && !datoContacto.getMovil().equals("")
            ? datoContacto.getMovil()
            : null);*/
          intervinienteOcupanteDatosContactoDTO.setMovilvalidado(Boolean.TRUE.equals(datoContacto.getMovilValidado()) ? true : false);

          /*intervinienteOcupanteDatosContactoDTO.setEmail(
          datoContacto.getEmail() != null && !datoContacto.getEmail().equals("")
            ? datoContacto.getEmail()
            : null);*/
          intervinienteOcupanteDatosContactoDTO.setEmailvalidado(Boolean.TRUE.equals(datoContacto.getEmailValidado()) ? true : false);

          /*   intervinienteOcupanteDatosContactoDTO.setTelefono(
          datoContacto.getFijo() != null && !datoContacto.getFijo().equals("")
            ? datoContacto.getFijo()
            : null);*/
          intervinienteOcupanteDatosContactoDTO.setTelefonovalidado(Boolean.TRUE.equals(datoContacto.getFijoValidado()) ? true : false);

          // intervinienteOcupanteDatosContactoDTO.setDireccion();
        }
        intervinienteOcupanteDatosContactoDTOList.add(intervinienteOcupanteDatosContactoDTO);
      }
    }
  /*  if (tipo != null) {
      switch (tipo) {
        case "sms":
          intervinienteOcupanteDatosContactoDTOList =
            intervinienteOcupanteDatosContactoDTOList.stream()
              .filter(
                intervinienteOcupanteDatosContactoDTO -> {
                  if (intervinienteOcupanteDatosContactoDTO.getMovil() != null) {
                    return true;
                  }
                  return false;
                })
              .collect(Collectors.toList());
          break;
        case "email":
          intervinienteOcupanteDatosContactoDTOList =
            intervinienteOcupanteDatosContactoDTOList.stream()
              .filter(
                intervinienteOcupanteDatosContactoDTO -> {
                  if (intervinienteOcupanteDatosContactoDTO.getEmail() != null) {
                    return true;
                  }
                  return false;
                })
              .collect(Collectors.toList());
          break;
      }
    }*/
    return intervinienteOcupanteDatosContactoDTOList;
  }
}
