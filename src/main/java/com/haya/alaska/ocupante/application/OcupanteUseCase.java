package com.haya.alaska.ocupante.application;

import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteOcupanteDatosContactoDTO;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteInputDto;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteOutputDto;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface OcupanteUseCase {

  ListWithCountDTO<OcupanteDTOToList> getAllFilteredOcupantes(Integer idBien, Integer id, String nombre, String apellidos, String nacionalidad, Boolean menorEdad, Boolean discapacitado, Boolean dependiente, Boolean residenciaHabitual, String tipoOcupante, Boolean marcaDeudorAnterior, String orderField, String orderDirection, Integer size, Integer page);

  void createDocumentoOcupante(Integer idOcupante, MultipartFile file, MetadatoDocumentoInputDTO metadatoDocumento, Usuario usuario) throws NotFoundException, IOException;

  OcupanteOutputDto findOcupanteByIdDto(Integer idBien, Integer idOcupante) throws IllegalAccessException, NotFoundException;

  OcupanteOutputDto update(Integer idBien, Integer idOcupante, OcupanteInputDto ocupanteDTO) throws Exception;

  OcupanteOutputDto create(Integer idBien, OcupanteInputDto ocupanteDTO) throws Exception;

  List<OcupanteOutputDto> findOcupantesByExpedientePN(Integer idExpedientePN);

  List<OcupanteOutputDto> findOcupanteByDniDto(String dni) throws IllegalAccessException, NotFoundException;

  List<IntervinienteOcupanteDatosContactoDTO> findOcupantesDatosContacto(String tipo, Integer idBienPN);

  List<OcupanteDTOToList> getOcupantesByBienes(List<Integer> bienes);

  List<DocumentoDTO> findDocumentosByExpedienteId(Integer id) throws NotFoundException, IOException;

  List<IntervinienteOcupanteDatosContactoDTO> getAllOcupantesContacto(Integer IdExpediente) throws NotFoundException;


}
