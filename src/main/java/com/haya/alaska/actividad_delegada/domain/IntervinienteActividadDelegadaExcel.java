package com.haya.alaska.actividad_delegada.domain;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;
import java.util.stream.Collectors;

public class IntervinienteActividadDelegadaExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Customer");
      cabeceras.add("Portfolio");
      cabeceras.add("Management Area");
      cabeceras.add("Borrower Id");
      cabeceras.add("NIF");
      cabeceras.add("Name");
      cabeceras.add("Surnames");
      cabeceras.add("Connection Id");
      cabeceras.add("Loan Id");
      cabeceras.add("Origin Id");
      cabeceras.add("Product");
      cabeceras.add("type Intervention");
      cabeceras.add("Status");
      cabeceras.add("Substate");
      cabeceras.add("Date of birth/Constitution");
      cabeceras.add("Nationality");
      cabeceras.add("Residence");

      cabeceras.add("Phone 1");
      cabeceras.add("Phone 2");
      cabeceras.add("Phone 3");
      cabeceras.add("Phone 4");
      cabeceras.add("Phone 5");

      cabeceras.add("Address 1");
      cabeceras.add("Address 2");
      cabeceras.add("Address 3");
      cabeceras.add("Address 4");
      cabeceras.add("Address 5");

      cabeceras.add("Email 1");
      cabeceras.add("Email 2");

      cabeceras.add("Occupation");
      cabeceras.add("Company");
      cabeceras.add("Num Children");
      cabeceras.add("Vulnerability");
      cabeceras.add("Judicialized");
      cabeceras.add("Balance in process");
      cabeceras.add("Manager");
      cabeceras.add("Manager Tlf"); // TODO
      cabeceras.add("Manager Email");

      // vacias
      cabeceras.add("Call/Visit Date (Required)");
      cabeceras.add("Call/Visit Time (Required)");
      cabeceras.add("Status/Closure (Required)");
      cabeceras.add("Action Type (Required)");
      cabeceras.add("Contact Person");
      cabeceras.add("Relationship with the Borrower");
      cabeceras.add("New Phone");
      cabeceras.add("New Address");
      cabeceras.add("New Email");
      cabeceras.add("Best Contact/Visit Time");
      cabeceras.add("Observations");
    } else {

      cabeceras.add("Cliente");
      cabeceras.add("Cartera");
      cabeceras.add("Area de gestion");
      cabeceras.add("Id Interviniente");
      cabeceras.add("NIF");
      cabeceras.add("Nombre");
      cabeceras.add("Apellidos");
      cabeceras.add("Id expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Origen");
      cabeceras.add("Producto");
      cabeceras.add("Tipo Intervencion");
      cabeceras.add("Estado");
      cabeceras.add("Subestado");
      cabeceras.add("Fecha Nacimiento/Constitucion");
      cabeceras.add("Nacionalidad");
      cabeceras.add("Residencia");

      cabeceras.add("Telefono 1");
      cabeceras.add("Telefono 2");
      cabeceras.add("Telefono 3");
      cabeceras.add("Telefono 4");
      cabeceras.add("Telefono 5");

      cabeceras.add("Direccion 1");
      cabeceras.add("Direccion 2");
      cabeceras.add("Direccion 3");
      cabeceras.add("Direccion 4");
      cabeceras.add("Direccion 5");

      cabeceras.add("Email 1");
      cabeceras.add("Email 2");

      cabeceras.add("Ocupacion");
      cabeceras.add("Empresa");
      cabeceras.add("Num Hijos");
      cabeceras.add("Vulnerabilidad");
      cabeceras.add("Judicializado");
      cabeceras.add("Saldo en gestión");
      cabeceras.add("Gestor");
      cabeceras.add("Tlf gestor"); // TODO
      cabeceras.add("Email gestor");

      // vacias
      cabeceras.add("Fecha llamada/visita (Obligatorio)");
      cabeceras.add("Hora llamada/visita (Obligatorio)");
      cabeceras.add("Estatus/cierre (Obligatorio)");
      cabeceras.add("Tipo de acción (Obligatorio)");
      cabeceras.add("Persona de contacto");
      cabeceras.add("Relación con el interviniente");
      cabeceras.add("Teléfono nuevo");
      cabeceras.add("Domicilio nuevo");
      cabeceras.add("Email nuevo");
      cabeceras.add("Mejor hora contacto/visita");
      cabeceras.add("Observaciones");
    }
  }

  public IntervinienteActividadDelegadaExcel(
      Interviniente interviniente,
      Contrato contrato,
      TipoIntervencion tipoIntervencion,
      Boolean mostrarTelefonos,
      Boolean mostrarDirecciones) {
    super();

    Expediente expediente = contrato.getExpediente();
    Cartera cartera = expediente.getCartera();
    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      this.add("Connection Id", expediente.getIdConcatenado());
      this.add(
          "Customer",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Portfolio", cartera != null ? expediente.getCartera().getNombre() : null);
      String areas = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          areas += au.getNombre() + ", ";
        }
      }
      this.add("Management Area", areas);
      this.add("Borrower Id", interviniente.getId());
      this.add("Loan Id", contrato.getIdCarga() != null ? contrato.getIdCarga() : null);
      this.add("Origin Id", interviniente.getIdCarga() != null ? interviniente.getIdCarga() : null);
      this.add(
          "Product", contrato.getProducto() != null ? contrato.getProducto().getValorIngles() : null);
      this.add(
          "Substate",
          expediente.getSubEstado() != null ? expediente.getSubEstado().getValorIngles() : null);
      this.add(
          "NIF",
          interviniente.getNumeroDocumento() != null ? interviniente.getNumeroDocumento() : null);
      if (interviniente.getPersonaJuridica() != null && interviniente.getPersonaJuridica()) {
        this.add(
            "Name", interviniente.getRazonSocial() != null ? interviniente.getRazonSocial() : null);
        this.add("Date of birth/Constitution", interviniente.getFechaConstitucion());
      } else {
        this.add("Name", interviniente.getNombre());
        this.add("Surnames", interviniente.getApellidos());
        this.add("Date of birth/Constitution", interviniente.getFechaNacimiento());
      }
      this.add(
          "Residence",
          interviniente.getResidencia() != null ? interviniente.getResidencia() : null);
      this.add(
          "Nationality",
          interviniente.getNacionalidad() != null
              ? interviniente.getNacionalidad().getValorIngles()
              : null);
      this.add(
          "Occupation",
          interviniente.getTipoOcupacion() != null
              ? interviniente.getTipoOcupacion().getValorIngles()
              : null);
      this.add("Company", interviniente.getEmpresa() != null ? interviniente.getEmpresa() : null);
      this.add(
          "Num Children",
          interviniente.getNumHijosDependientes() != null
              ? interviniente.getNumHijosDependientes()
              : null);
      this.add(
          "Vulnerability", Boolean.TRUE.equals(interviniente.getVulnerabilidad()) ? "Yes" : "NO");
      this.add("Judicialized", contrato.getProcedimientos() != null ? "Yes" : "NO");
      this.add("Balance in process", expediente.getSaldoGestion());
      this.add("Manager", gestor != null ? gestor.getNombre() : null);
      this.add("Manager Email", gestor != null ? gestor.getEmail() : null);

      if (interviniente.getDirecciones() != null && mostrarDirecciones == true) {
        var contador = 0;
        Iterator<Direccion> it = interviniente.getDirecciones().iterator();
        while (it.hasNext() && contador < 5) {
          Direccion direccion = it.next();
          String direccionString = new String();
          direccionString +=
              direccion.getTipoVia() != null ? direccion.getTipoVia().getValorIngles() + " " : "";
          direccionString += direccion.getNombre() != null ? direccion.getNombre() + " " : "";
          direccionString += direccion.getNumero() != null ? direccion.getNumero() + " " : "";
          direccionString += direccion.getPortal() != null ? direccion.getPortal() + " " : "";
          direccionString += direccion.getBloque() != null ? direccion.getBloque() + " " : "";
          direccionString += direccion.getEscalera() != null ? direccion.getEscalera() + " " : "";
          direccionString += direccion.getPiso() != null ? direccion.getPiso() + " " : "";
          direccionString += direccion.getPuerta() != null ? direccion.getPuerta() + " " : "";
          direccionString += direccion.getEntorno() != null ? direccion.getEntorno() + " " : "";
          direccionString +=
              direccion.getCodigoPostal() != null ? direccion.getCodigoPostal() + " " : "";
          direccionString += direccion.getMunicipio() != null ? direccion.getMunicipio() + " " : "";
          direccionString += direccion.getLocalidad() != null ? direccion.getLocalidad() + " " : "";
          direccionString += direccion.getProvincia() != null ? direccion.getProvincia() + " " : "";

          if (contador == 0) {
            this.add("Address 1", direccionString);
          } else if (contador == 1) {
            this.add("Address 2", direccionString);
          } else if (contador == 2) {
            this.add("Address 3", direccionString);
          } else if (contador == 3) {
            this.add("Address 4", direccionString);
          } else if (contador == 4) {
            this.add("Address 5", direccionString);
          }
          contador++;
        }
      }
      if (mostrarTelefonos == true) {
        List<String> emails = new ArrayList<>();
        List<String> telefonos = new ArrayList<>();

        for (var datoContacto : interviniente.getDatosContacto()) {
          if (datoContacto.getEmail() != null && !datoContacto.getEmail().equals(""))
            emails.add(datoContacto.getEmail());
          if (datoContacto.getFijo() != null && !datoContacto.getFijo().equals(""))
            telefonos.add(datoContacto.getFijo());
          if (datoContacto.getMovil() != null && !datoContacto.getMovil().equals(""))
            telefonos.add(datoContacto.getMovil());
        }

        if (emails.size() > 0) {
          var contador = 0;
          Iterator<String> it = emails.iterator();
          while (it.hasNext() && contador < 2) {
            switch (contador) {
              case 0:
                this.add("Email 1", it.next());
                break;
              case 1:
                this.add("Email 2", it.next());
                break;
            }
            contador++;
          }
        }
        if (telefonos.size() > 0) {
          var contador = 0;
          Iterator<String> it = telefonos.iterator();
          while (it.hasNext() && contador < 5) {
            switch (contador) {
              case 0:
                this.add("Phone 1", it.next());
                break;
              case 1:
                this.add("Phone 2", it.next());
                break;
              case 2:
                this.add("Phone 3", it.next());
                break;
              case 3:
                this.add("Phone 4", it.next());
                break;
              case 4:
                this.add("Phone 5", it.next());
                break;
            }
            contador++;
          }
        }
      }

    } else {

      this.add("Id expediente", expediente.getIdConcatenado());
      this.add(
          "Cliente",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Cartera", cartera != null ? expediente.getCartera().getNombre() : null);

      String areas = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          areas += au.getNombre() + ", ";
        }
      }
      this.add("Area de gestion", areas);

      this.add("Id Interviniente", interviniente.getId());
      this.add("Id Contrato", contrato.getIdCarga() != null ? contrato.getIdCarga() : null);
      this.add("Id Origen", interviniente.getIdCarga() != null ? interviniente.getIdCarga() : null);
      this.add(
          "Producto", contrato.getProducto() != null ? contrato.getProducto().getValor() : null);
      this.add("Tipo Intervencion", tipoIntervencion != null ? tipoIntervencion.getValor() : null);
      this.add(
          "Estado",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
      this.add(
          "Subestado",
          expediente.getSubEstado() != null ? expediente.getSubEstado().getValor() : null);
      this.add(
          "NIF",
          interviniente.getNumeroDocumento() != null ? interviniente.getNumeroDocumento() : null);

      if (interviniente.getPersonaJuridica() != null && interviniente.getPersonaJuridica()) {
        this.add(
            "Nombre",
            interviniente.getRazonSocial() != null ? interviniente.getRazonSocial() : null);
        this.add("Fecha Nacimiento/Constitucion", interviniente.getFechaConstitucion());
      } else {
        this.add("Nombre", interviniente.getNombre());
        this.add("Apellidos", interviniente.getApellidos());
        this.add("Fecha Nacimiento/Constitucion", interviniente.getFechaNacimiento());
      }

      this.add(
          "Residencia",
          interviniente.getResidencia() != null ? interviniente.getResidencia() : null);
      this.add(
          "Nacionalidad",
          interviniente.getNacionalidad() != null
              ? interviniente.getNacionalidad().getValor()
              : null);
      this.add(
          "Ocupacion",
          interviniente.getTipoOcupacion() != null
              ? interviniente.getTipoOcupacion().getValor()
              : null);
      this.add("Empresa", interviniente.getEmpresa() != null ? interviniente.getEmpresa() : null);
      this.add(
          "Num Hijos",
          interviniente.getNumHijosDependientes() != null
              ? interviniente.getNumHijosDependientes()
              : null);
      this.add(
          "Vulnerabilidad", Boolean.TRUE.equals(interviniente.getVulnerabilidad()) ? "SÍ" : "NO");
      this.add("Judicializado", contrato.getProcedimientos() != null ? "SI" : "NO");
      this.add("Saldo en gestión", expediente.getSaldoGestion());
      this.add("Gestor", gestor != null ? gestor.getNombre() : null);
      this.add("Email gestor", gestor != null ? gestor.getEmail() : null);

      if (interviniente.getDirecciones() != null && mostrarDirecciones == true) {
        var contador = 0;
        Iterator<Direccion> it = interviniente.getDirecciones().iterator();
        while (it.hasNext() && contador < 5) {
          Direccion direccion = it.next();
          String direccionString = new String();
          direccionString +=
              direccion.getTipoVia() != null ? direccion.getTipoVia().getValor() + " " : "";
          direccionString += direccion.getNombre() != null ? direccion.getNombre() + " " : "";
          direccionString += direccion.getNumero() != null ? direccion.getNumero() + " " : "";
          direccionString += direccion.getPortal() != null ? direccion.getPortal() + " " : "";
          direccionString += direccion.getBloque() != null ? direccion.getBloque() + " " : "";
          direccionString += direccion.getEscalera() != null ? direccion.getEscalera() + " " : "";
          direccionString += direccion.getPiso() != null ? direccion.getPiso() + " " : "";
          direccionString += direccion.getPuerta() != null ? direccion.getPuerta() + " " : "";
          direccionString += direccion.getEntorno() != null ? direccion.getEntorno() + " " : "";
          direccionString +=
              direccion.getCodigoPostal() != null ? direccion.getCodigoPostal() + " " : "";
          direccionString += direccion.getMunicipio() != null ? direccion.getMunicipio() + " " : "";
          direccionString += direccion.getLocalidad() != null ? direccion.getLocalidad() + " " : "";
          direccionString += direccion.getProvincia() != null ? direccion.getProvincia() + " " : "";

          if (contador == 0) {
            this.add("Direccion 1", direccionString);
          } else if (contador == 1) {
            this.add("Direccion 2", direccionString);
          } else if (contador == 2) {
            this.add("Direccion 3", direccionString);
          } else if (contador == 3) {
            this.add("Direccion 4", direccionString);
          } else if (contador == 4) {
            this.add("Direccion 5", direccionString);
          }
          contador++;
        }
      }
      if (mostrarTelefonos == true) {
        List<String> emails = new ArrayList<>();
        List<String> telefonos = new ArrayList<>();

        for (var datoContacto : interviniente.getDatosContacto()) {
          if (datoContacto.getEmail() != null && !datoContacto.getEmail().equals(""))
            emails.add(datoContacto.getEmail());
          if (datoContacto.getFijo() != null && !datoContacto.getFijo().equals(""))
            telefonos.add(datoContacto.getFijo());
          if (datoContacto.getMovil() != null && !datoContacto.getMovil().equals(""))
            telefonos.add(datoContacto.getMovil());
        }

        if (emails.size() > 0) {
          var contador = 0;
          Iterator<String> it = emails.iterator();
          while (it.hasNext() && contador < 2) {
            switch (contador) {
              case 0:
                this.add("Email 1", it.next());
                break;
              case 1:
                this.add("Email 2", it.next());
                break;
            }
            contador++;
          }
        }

        if (telefonos.size() > 0) {
          var contador = 0;
          Iterator<String> it = telefonos.iterator();
          while (it.hasNext() && contador < 5) {
            switch (contador) {
              case 0:
                this.add("Telefono 1", it.next());
                break;
              case 1:
                this.add("Telefono 2", it.next());
                break;
              case 2:
                this.add("Telefono 3", it.next());
                break;
              case 3:
                this.add("Telefono 4", it.next());
                break;
              case 4:
                this.add("Telefono 5", it.next());
                break;
            }
            contador++;
          }
        }
      }
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }

  public static Collection<String> getSobreCabecera() {
    LinkedHashSet<String> sobre = new LinkedHashSet<>();
    sobre.add("");
    sobre.add(" ");
    sobre.add("  ");
    sobre.add("   ");
    sobre.add("    ");
    sobre.add("     ");
    sobre.add("      ");
    sobre.add("       ");
    sobre.add("        ");
    sobre.add("         ");
    sobre.add("          ");
    sobre.add("           ");
    sobre.add("            ");
    sobre.add("             ");
    sobre.add("              ");
    sobre.add("               ");
    sobre.add("                ");

    sobre.add("                 ");
    sobre.add("                  ");
    sobre.add("                   ");
    sobre.add("                    ");
    sobre.add("                     ");

    sobre.add("                      ");
    sobre.add("                       ");
    sobre.add("                        ");
    sobre.add("                         ");
    sobre.add("                          ");

    sobre.add("                           ");
    sobre.add("                            ");

    sobre.add("                             ");
    sobre.add("                              ");
    sobre.add("                               ");
    sobre.add("                                ");
    sobre.add("                                 ");
    sobre.add("                                  ");
    sobre.add("                                   ");
    sobre.add("                                    ");
    sobre.add("                                     ");

    // vacias
    sobre.add("dd/mm/aaaa");
    sobre.add("hh:mm:ss");

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      sobre.add(
        "If Called (complete only with one of the following types):\n"
          + "1 : With Agreement\n"
          + "2 : Without Agreement\n"
          + "3 : Without Contact\n"
          + "4 : Wrong Contact\n"
          + "5 : Contact with Third Party\n"
          + "6 : Customer Incident\n"
          + "7 : Payment Agreement\n"
          + "\n"
          + "If Visit (complete only with one of the following types):\n"
          + "1 : With Agreement\n"
          + "2 : Without Agreement\n"
          + "3 : Without Contact\n"
          + "8 : Incorrect Address\n"
          + "9 : Contact with Titled Third Party\n"
          + "10: Contact with Untitled Third Party\n"
          + "6 : Customer Incident\n"
          + "7 : Payment Agreement");
      sobre.add("1: Call / 2: Visit");
      sobre.add("Free Text");
      sobre.add("Free Text ");
      sobre.add("Free Text  ");
      sobre.add("Free Text   ");
      sobre.add("Free Text    ");
      sobre.add("Free Text     ");
      sobre.add("Free Text      ");

    } else {
      sobre.add(
          "Si es Llamada (completar solo con uno de los siguientes tipos):\n"
              + "1 : Con Acuerdo\n"
              + "2 : Sin Acuerdo\n"
              + "3 : Sin Contacto\n"
              + "4 : Contacto Incorrecto\n"
              + "5 : Contacto con Tercero\n"
              + "6 : Incidencia Cliente\n"
              + "7 : Acuerdo de Pago\n"
              + "\n"
              + "Si es Visita (completar solo con uno de los siguientes tipos):\n"
              + "1 : Con Acuerdo\n"
              + "2 : Sin Acuerdo\n"
              + "3 : Sin Contacto\n"
              + "8 : Dirección Incorrecta\n"
              + "9 : Contacto con Tercero Con Título\n"
              + "10: Contacto con Tercero Sin Título\n"
              + "6 : Incidencia Cliente\n"
              + "7 : Acuerdo de Pago");
      sobre.add("1: Llamada / 2: Visita");
      sobre.add("Texto Libre");
      sobre.add("Texto Libre ");
      sobre.add("Texto Libre  ");
      sobre.add("Texto Libre   ");
      sobre.add("Texto Libre    ");
      sobre.add("Texto Libre     ");
      sobre.add("Texto Libre      ");
    }
    return sobre;
  }
}
