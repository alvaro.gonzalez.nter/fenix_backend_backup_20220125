package com.haya.alaska.actividad_delegada.domain;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class BienActividadDelegadaExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Customer");
      cabeceras.add("Portfolio");
      cabeceras.add("Management Area");
      cabeceras.add("Asset Id");
      cabeceras.add("Loan Id");
      cabeceras.add("Connection Id");
      cabeceras.add("Haya Id");
      cabeceras.add("Origin Id");
      cabeceras.add("Origin 2 Id");

      cabeceras.add("Asset Type");
      cabeceras.add("Asset Subtype");
      cabeceras.add("Warranty Type");
      cabeceras.add("Status");
      cabeceras.add("Cadastral Reference");
      cabeceras.add("Finca");
      cabeceras.add("Registration");
      cabeceras.add("Location Registration");
      cabeceras.add("Negotiated Possession");
      cabeceras.add("Via Type");
      cabeceras.add("Via Name");
      cabeceras.add("Number");
      cabeceras.add("Portal");
      cabeceras.add("Block");
      cabeceras.add("Stairs");
      cabeceras.add("Floor");
      cabeceras.add("Door");
      cabeceras.add("Environment");
      cabeceras.add("Municipality");
      cabeceras.add("Location");
      cabeceras.add("Province");
      cabeceras.add("Comment Address");
      cabeceras.add("Postal Code");
      cabeceras.add("Country");
      cabeceras.add("Active Type");
      cabeceras.add("Use");
      cabeceras.add("Garage Annex");
      cabeceras.add("Storage Room Annex");
      cabeceras.add("Other Annex");

      cabeceras.add("Decree Date");
      cabeceras.add("Testimonial Date");
      cabeceras.add("Possession/Launch Date");
      cabeceras.add("Occupants Num");

      cabeceras.add("Haya Manager");
      cabeceras.add("Haya Manager Tlf"); // TODO
      cabeceras.add("Haya Manager Email");

      // vacias
      cabeceras.add("Call/Visit Date (Required)");
      cabeceras.add("Call/Visit Time (Required)");
      cabeceras.add("Status/Closure (Required)");
      cabeceras.add("Action Type (Required)");
      cabeceras.add("Contact Person");
      cabeceras.add("Relationship with the Borrower");
      cabeceras.add("New Phone");
      cabeceras.add("New Address");
      cabeceras.add("New Email");
      cabeceras.add("Best Contact/Visit Time");
      cabeceras.add("Observations");
    } else {
      cabeceras.add("Cliente");
      cabeceras.add("Cartera");
      cabeceras.add("Area de gestion");
      cabeceras.add("Id Bien");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Haya");
      cabeceras.add("Id Origen");
      cabeceras.add("Id Origen 2");

      cabeceras.add("Tipo Bien");
      cabeceras.add("Subtipo Bien");
      cabeceras.add("Tipo Garantia");
      cabeceras.add("Estado");
      cabeceras.add("Referencia Catastral");
      cabeceras.add("Finca");
      cabeceras.add("Registro");
      cabeceras.add("Localidad Registro");
      cabeceras.add("Posesion Negociada");
      cabeceras.add("Tipo Via");
      cabeceras.add("Nombre Via");
      cabeceras.add("Numero");
      cabeceras.add("Portal");
      cabeceras.add("Bloque");
      cabeceras.add("Escalera");
      cabeceras.add("Piso");
      cabeceras.add("Puerta");
      cabeceras.add("Entorno");
      cabeceras.add("Municipio");
      cabeceras.add("Localidad");
      cabeceras.add("Provincia");
      cabeceras.add("Comentario Direccion");
      cabeceras.add("Codigo Postal");
      cabeceras.add("Pais");
      cabeceras.add("Tipo Activo");
      cabeceras.add("Uso");
      cabeceras.add("Anejo Garaje");
      cabeceras.add("Anejo Trastero");
      cabeceras.add("Anejo Otros");

      cabeceras.add("Fecha decreto");
      cabeceras.add("Fecha testimonio");
      cabeceras.add("Fecha posesion/Lanzamiento");
      cabeceras.add("Num Ocupantes");

      cabeceras.add("Gestor Haya");
      cabeceras.add("Tlf gestor Haya"); // TODO
      cabeceras.add("Email gestor Haya");

      // vacias
      cabeceras.add("Fecha llamada/visita (Obligatorio)");
      cabeceras.add("Hora llamada/visita (Obligatorio)");
      cabeceras.add("Estatus/cierre (Obligatorio)");
      cabeceras.add("Tipo de acción (Obligatorio)");
      cabeceras.add("Persona de contacto");
      cabeceras.add("Relación con el interviniente");
      cabeceras.add("Teléfono nuevo");
      cabeceras.add("Domicilio nuevo");
      cabeceras.add("Email nuevo");
      cabeceras.add("Mejor hora contacto/visita");
      cabeceras.add("Observaciones");
    }
  }

  public BienActividadDelegadaExcel(Bien bien, Contrato contrato, BienPosesionNegociada bienPN) {
    super();

    Expediente expediente = contrato.getExpediente();
    Cartera cartera = expediente.getCartera();
    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");
    ContratoBien cb = bien.getContratoBien(contrato.getId());

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add(
          "Customer",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Portfolio", cartera != null ? expediente.getCartera().getNombre() : null);

      String areas = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          areas += au.getNombre() + ", ";
        }
      }
      this.add("Management Area", areas);

      this.add("Asset Id", bien.getId());
      if (cb != null)
        this.add("Loan Id", cb.getContrato() != null ? cb.getContrato().getIdCarga() : null);

      this.add("Connection Id", expediente.getIdConcatenado());
      this.add("Haya Id", bien.getIdHaya() != null ? bien.getIdHaya() : null);
      // this.add("Id Origen", bien.getIdCarga() !=null ? bien.getIdCarga():null);
      this.add("Origin Id", bien.getIdOrigen() != null ? bien.getIdOrigen() : null);
      this.add("Origin 2 Id", bien.getIdOrigen2() != null ? bien.getIdOrigen2() : null);
      this.add(
          "Asset Type", bien.getTipoBien() != null ? bien.getTipoBien().getValorIngles() : null);
      this.add(
          "Asset Subtype",
          bien.getSubTipoBien() != null ? bien.getSubTipoBien().getValorIngles() : null);
      this.add("Status", Boolean.TRUE.equals(bien.getActivo()) ? "Activated" : "Released");
      if (cb != null) {
        this.add(
            "Warranty Type",
            cb.getTipoGarantia() != null ? cb.getTipoGarantia().getValorIngles() : null);
      } else if (bienPN != null) {
        this.add(
            "Warranty Type",
            bienPN.getTipoGarantia() != null ? bienPN.getTipoGarantia().getValorIngles() : null);
      }
      this.add(
          "Cadastral Reference",
          bien.getReferenciaCatastral() != null ? bien.getReferenciaCatastral() : null);
      this.add("Finca", bien.getFinca() != null ? bien.getFinca() : null);
      this.add(
          "Location Registration",
          bien.getLocalidadRegistro() != null ? bien.getLocalidadRegistro() : null);
      this.add("Registration", bien.getRegistro() != null ? bien.getRegistro() : null);

      this.add(
          "Negotiated Possession", Boolean.TRUE.equals(bien.getPosesionNegociada()) ? "YES" : "NO");
      this.add("Via Type", bien.getTipoVia() != null ? bien.getTipoVia().getValorIngles() : null);
      this.add("Via Name", bien.getNombreVia() != null ? bien.getNombreVia() : null);
      this.add("Number", bien.getNumero() != null ? bien.getNumero() : null);
      this.add("Portal", bien.getPortal() != null ? bien.getPortal() : null);
      this.add("Block", bien.getBloque() != null ? bien.getBloque() : null);
      this.add("Stairs", bien.getEscalera() != null ? bien.getEscalera() : null);
      this.add("Floor", bien.getPiso() != null ? bien.getPiso() : null);
      this.add("Door", bien.getPuerta() != null ? bien.getPuerta() : null);
      this.add(
          "Environment", bien.getEntorno() != null ? bien.getEntorno().getValorIngles() : null);
      this.add(
          "Municipality",
          bien.getMunicipio() != null ? bien.getMunicipio().getValorIngles() : null);
      this.add(
          "Location", bien.getLocalidad() != null ? bien.getLocalidad().getValorIngles() : null);
      this.add(
          "Province", bien.getProvincia() != null ? bien.getProvincia().getValorIngles() : null);
      this.add(
          "Comment Address",
          bien.getComentarioDireccion() != null ? bien.getComentarioDireccion() : null);
      this.add("Postal Code", bien.getCodigoPostal() != null ? bien.getCodigoPostal() : null);
      this.add("Country", bien.getPais() != null ? bien.getPais().getValorIngles() : null);
      this.add(
          "Active Type",
          bien.getTipoActivo() != null ? bien.getTipoActivo().getValorIngles() : null);
      this.add("Use", bien.getUso() != null ? bien.getUso().getValorIngles() : null);
      this.add("Garage Annex", bien.getAnejoGaraje() != null ? "YES" : "-");
      this.add("Storage Room Annex", bien.getAnejoTrastero() != null ? "YES" : "-");
      this.add("Other Annex", bien.getAnejoOtros() != null ? "YES" : "-");

      BienSubastado bienSubastado = null;
      Iterator<BienSubastado> it = bien.getBienesSubastados().iterator();
      while (it.hasNext()) {
        BienSubastado bienSubastadoProv = it.next();
        if (!it.hasNext()) {
          bienSubastado = bienSubastadoProv;
        }
      }
      if (bienSubastado != null) {
        this.add(
            "Decree Date",
            bienSubastado.getFechaDecretoAdjudicacion() != null
                ? bienSubastado.getFechaDecretoAdjudicacion()
                : null);
        this.add(
            "Testimonial Date",
            bienSubastado.getFechaTestimonio() != null ? bienSubastado.getFechaTestimonio() : null);
        this.add(
            "Possession/Launch Date",
            bienSubastado.getFechaPosteriorLanzamiento() != null
                ? bienSubastado.getFechaPosteriorLanzamiento()
                : null);
      }
      if (bienPN != null) {
        if (bienPN.getOcupantes() != null) {
          this.add("Occupants Num", bienPN.getOcupantes().size());
        }
      }
      this.add("Haya Manager", gestor != null ? gestor.getNombre() : null);
      this.add("Haya Manager Email", gestor != null ? gestor.getEmail() : null);
    } else {
      this.add(
          "Cliente",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Cartera", cartera != null ? expediente.getCartera().getNombre() : null);

      String areas = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          areas += au.getNombre() + ", ";
        }
      }
      this.add("Area de gestion", areas);

      this.add("Id Bien", bien.getId());
      if (cb != null)
        this.add("Id Contrato", cb.getContrato() != null ? cb.getContrato().getIdCarga() : null);

      this.add("Id Expediente", expediente.getIdConcatenado());
      this.add("Id Haya", bien.getIdHaya() != null ? bien.getIdHaya() : null);
      // this.add("Id Origen", bien.getIdCarga() !=null ? bien.getIdCarga():null);
      this.add("Id Origen", bien.getIdOrigen() != null ? bien.getIdOrigen() : null);
      this.add("Id Origen 2", bien.getIdOrigen2() != null ? bien.getIdOrigen2() : null);
      this.add("Tipo Bien", bien.getTipoBien() != null ? bien.getTipoBien().getValor() : null);
      this.add(
          "Subtipo Bien", bien.getSubTipoBien() != null ? bien.getSubTipoBien().getValor() : null);
      this.add("Estado", Boolean.TRUE.equals(bien.getActivo()) ? "Activa" : "Liberada");
      if (cb != null) {
        this.add(
            "Tipo Garantia", cb.getTipoGarantia() != null ? cb.getTipoGarantia().getValor() : null);
      } else if (bienPN != null) {
        this.add(
            "Tipo Garantia",
            bienPN.getTipoGarantia() != null ? bienPN.getTipoGarantia().getValor() : null);
      }
      this.add(
          "Refenrencia Catastral",
          bien.getReferenciaCatastral() != null ? bien.getReferenciaCatastral() : null);
      this.add("Finca", bien.getFinca() != null ? bien.getFinca() : null);
      this.add(
          "Localidad Registro",
          bien.getLocalidadRegistro() != null ? bien.getLocalidadRegistro() : null);
      this.add("Registro", bien.getRegistro() != null ? bien.getRegistro() : null);

      this.add(
          "Posesion Negociada", Boolean.TRUE.equals(bien.getPosesionNegociada()) ? "SÍ" : "NO");
      this.add("Tipo Via", bien.getTipoVia() != null ? bien.getTipoVia().getValor() : null);
      this.add("Nombre Via", bien.getNombreVia() != null ? bien.getNombreVia() : null);
      this.add("Numero", bien.getNumero() != null ? bien.getNumero() : null);
      this.add("Portal", bien.getPortal() != null ? bien.getPortal() : null);
      this.add("Bloque", bien.getBloque() != null ? bien.getBloque() : null);
      this.add("Escalera", bien.getEscalera() != null ? bien.getEscalera() : null);
      this.add("Piso", bien.getPiso() != null ? bien.getPiso() : null);
      this.add("Puerta", bien.getPuerta() != null ? bien.getPuerta() : null);
      this.add("Entorno", bien.getEntorno() != null ? bien.getEntorno().getValor() : null);
      this.add("Municipio", bien.getMunicipio() != null ? bien.getMunicipio().getValor() : null);
      this.add("Localidad", bien.getLocalidad() != null ? bien.getLocalidad().getValor() : null);
      this.add("Provincia", bien.getProvincia() != null ? bien.getProvincia().getValor() : null);
      this.add(
          "Comentario Direccion",
          bien.getComentarioDireccion() != null ? bien.getComentarioDireccion() : null);
      this.add("Codigo Postal", bien.getCodigoPostal() != null ? bien.getCodigoPostal() : null);
      this.add("Pais", bien.getPais() != null ? bien.getPais().getValor() : null);
      this.add(
          "Tipo Activo", bien.getTipoActivo() != null ? bien.getTipoActivo().getValor() : null);
      this.add("Uso", bien.getUso() != null ? bien.getUso().getValor() : null);
      this.add("Anejo Garaje", bien.getAnejoGaraje() != null ? "SÍ" : "-");
      this.add("Anejo Trastero", bien.getAnejoTrastero() != null ? "SÍ" : "-");
      this.add("Anejo Otros", bien.getAnejoOtros() != null ? "SÍ" : "-");

      BienSubastado bienSubastado = null;
      Iterator<BienSubastado> it = bien.getBienesSubastados().iterator();
      while (it.hasNext()) {
        BienSubastado bienSubastadoProv = it.next();
        if (!it.hasNext()) {
          bienSubastado = bienSubastadoProv;
        }
      }
      if (bienSubastado != null) {
        this.add(
            "Fecha decreto",
            bienSubastado.getFechaDecretoAdjudicacion() != null
                ? bienSubastado.getFechaDecretoAdjudicacion()
                : null);
        this.add(
            "Fecha testimonio",
            bienSubastado.getFechaTestimonio() != null ? bienSubastado.getFechaTestimonio() : null);
        this.add(
            "Fecha posesion/Lanzamiento",
            bienSubastado.getFechaPosteriorLanzamiento() != null
                ? bienSubastado.getFechaPosteriorLanzamiento()
                : null);
      }
      if (bienPN != null) {
        if (bienPN.getOcupantes() != null) {
          this.add("Num Ocupantes", bienPN.getOcupantes().size());
        }
      }
      this.add("Gestor Haya", gestor != null ? gestor.getNombre() : null);
      this.add("Email gestor Haya", gestor != null ? gestor.getEmail() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }

  public static Collection<String> getSobreCabecera() {
    LinkedHashSet<String> sobre = new LinkedHashSet<>();
    sobre.add("");
    sobre.add(" ");
    sobre.add("  ");
    sobre.add("   ");
    sobre.add("    ");
    sobre.add("     ");
    sobre.add("      ");
    sobre.add("       ");
    sobre.add("        ");

    sobre.add("         ");
    sobre.add("          ");
    sobre.add("           ");
    sobre.add("            ");
    sobre.add("             ");
    sobre.add("              ");
    sobre.add("               ");
    sobre.add("                ");
    sobre.add("                 ");
    sobre.add("                  ");
    sobre.add("                   ");
    sobre.add("                    ");
    sobre.add("                     ");
    sobre.add("                      ");
    sobre.add("                       ");
    sobre.add("                        ");
    sobre.add("                         ");
    sobre.add("                          ");
    sobre.add("                           ");
    sobre.add("                            ");
    sobre.add("                             ");
    sobre.add("                              ");
    sobre.add("                               ");
    sobre.add("                                ");
    sobre.add("                                 ");
    sobre.add("                                  ");
    sobre.add("                                   ");
    sobre.add("                                    ");
    sobre.add("                                     ");

    sobre.add("                                      ");
    sobre.add("                                       ");
    sobre.add("                                        ");
    sobre.add("                                         ");

    sobre.add("                                          ");
    sobre.add("                                           ");
    sobre.add("                                            ");

    // vacias
    sobre.add("dd/mm/aaaa");
    sobre.add("hh:mm:ss");
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      sobre.add(
          "If Called (complete only with one of the following types):\n"
              + "1 : With Agreement\n"
              + "2 : Without Agreement\n"
              + "3 : Without Contact\n"
              + "4 : Wrong Contact\n"
              + "5 : Contact with Third Party\n"
              + "6 : Customer Incident\n"
              + "7 : Payment Agreement\n"
              + "\n"
              + "If Visit (complete only with one of the following types):\n"
              + "1 : With Agreement\n"
              + "2 : Without Agreement\n"
              + "3 : Without Contact\n"
              + "8 : Incorrect Address\n"
              + "9 : Contact with Titled Third Party\n"
              + "10: Contact with Untitled Third Party\n"
              + "6 : Customer Incident\n"
              + "7 : Payment Agreement");
      sobre.add("1: Call / 2: Visit");
      sobre.add("Free Text");
      sobre.add("Free Text ");
      sobre.add("Free Text  ");
      sobre.add("Free Text   ");
      sobre.add("Free Text    ");
      sobre.add("Free Text     ");
      sobre.add("Free Text      ");
    } else {
      sobre.add(
          "Si es Llamada (completar solo con uno de los siguientes tipos):\n"
              + "1 : Con Acuerdo\n"
              + "2 : Sin Acuerdo\n"
              + "3 : Sin Contacto\n"
              + "4 : Contacto Incorrecto\n"
              + "5 : Contacto con Tercero\n"
              + "6 : Incidencia Cliente\n"
              + "7 : Acuerdo de Pago\n"
              + "\n"
              + "Si es Visita (completar solo con uno de los siguientes tipos):\n"
              + "1 : Con Acuerdo\n"
              + "2 : Sin Acuerdo\n"
              + "3 : Sin Contacto\n"
              + "8 : Dirección Incorrecta\n"
              + "9 : Contacto con Tercero Con Título\n"
              + "10: Contacto con Tercero Sin Título\n"
              + "6 : Incidencia Cliente\n"
              + "7 : Acuerdo de Pago");
      sobre.add("1: Llamada / 2: Visita");
      sobre.add("Texto Libre");
      sobre.add("Texto Libre ");
      sobre.add("Texto Libre  ");
      sobre.add("Texto Libre   ");
      sobre.add("Texto Libre    ");
      sobre.add("Texto Libre     ");
      sobre.add("Texto Libre      ");
    }

    return sobre;
  }
}
