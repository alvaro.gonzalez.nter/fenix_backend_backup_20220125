package com.haya.alaska.actividad_delegada.domain;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;
import java.util.stream.Collectors;

public class ContratoActividadDelegadaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Customer");
      cabeceras.add("Portfolio");
      cabeceras.add("Management area");
      cabeceras.add("Representative Loan");
      cabeceras.add("Product");
      cabeceras.add("Status");
      cabeceras.add("Substate");
      cabeceras.add("Name First holder");
      cabeceras.add("Last Name First Holder");
      cabeceras.add("NIF");
      cabeceras.add("Relationship type");
      cabeceras.add("Date of birth/Constitution");
      cabeceras.add("Num Loans");
      cabeceras.add("Num Borrowers");
      cabeceras.add("Num Warranties");
      cabeceras.add("Num Solvencies");
      cabeceras.add("Connection manager");
      cabeceras.add("Judicialized");
      cabeceras.add("Judicialization phase");
      cabeceras.add("Proposal");
      cabeceras.add("Proposed status");
      cabeceras.add("Balance in process");
      cabeceras.add("Recovered amount");
      cabeceras.add("Principal pending");
      cabeceras.add("Unpaid debt");
      cabeceras.add("Retrieved");
    } else {
      cabeceras.add("Id expediente");
      cabeceras.add("Cliente");
      cabeceras.add("Cartera");
      cabeceras.add("Area de gestion");
      cabeceras.add("Contrato representante");
      cabeceras.add("Producto");
      cabeceras.add("Estado");
      cabeceras.add("Subestado");
      cabeceras.add("Nombre Primer titular");
      cabeceras.add("Apellidos Primer Titular");
      cabeceras.add("NIF");
      cabeceras.add("Tipo de relación");
      cabeceras.add("Fecha de nacimiento/Constitución");
      cabeceras.add("Num Contratos");
      cabeceras.add("Num Intervinientes");
      cabeceras.add("Num Garantias");
      cabeceras.add("Num Solvencias");
      cabeceras.add("Gestor del expediente");
      cabeceras.add("Judicializado");
      cabeceras.add("Fase judicialización");
      cabeceras.add("Propuesta");
      cabeceras.add("Propuesta estado");
      cabeceras.add("Saldo en gestión");
      cabeceras.add("Importe recuperado");
      cabeceras.add("Principal pendiente");
      cabeceras.add("Deuda impagada");
      cabeceras.add("Recuperado");
    }
  }

  public ContratoActividadDelegadaExcel(Contrato contrato) {
    super();

    Expediente expediente = contrato.getExpediente();
    Cartera cartera = expediente.getCartera();
    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");
    Usuario responsableCartera = expediente.getUsuarioByPerfil("Responsable de cartera");
    Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Connection Id", expediente.getIdConcatenado());
      this.add(
          "Customer",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Portfolio", cartera != null ? expediente.getCartera().getNombre() : null);

      String areas = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          areas += au.getNombre() + ", ";
        }
      }
      this.add("Management area", areas);

      this.add("Representative Loan", contrato.getIdCarga());
      this.add(
          "Product",
          contrato.getProducto() != null ? contrato.getProducto().getValorIngles() : null);
      this.add(
          "Status",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValorIngles() : null);
      this.add(
          "Substate",
          expediente.getSubEstado() != null ? expediente.getSubEstado().getValorIngles() : null);

      if (contrato.getPrimerInterviniente() != null) {
        this.add(
            "NIF",
            contrato.getPrimerInterviniente().getNumeroDocumento() != null
                ? contrato.getPrimerInterviniente().getNumeroDocumento()
                : null);
        if (contrato.getPrimerInterviniente().getPersonaJuridica() != null
            && contrato.getPrimerInterviniente().getPersonaJuridica()) {
          this.add(
              "Name First holder",
              contrato.getPrimerInterviniente().getRazonSocial() != null
                  ? contrato.getPrimerInterviniente().getRazonSocial()
                  : null);
          this.add(
              "Date of birth/Constitution",
              contrato.getPrimerInterviniente().getFechaConstitucion());
        } else {
          this.add("Name First holder", contrato.getPrimerInterviniente().getNombre());
          this.add("Last Name First Holder", contrato.getPrimerInterviniente().getApellidos());
          this.add(
              "Date of birth/Constitution", contrato.getPrimerInterviniente().getFechaNacimiento());
        }
      }
      this.add(
          "Relationship type",
          contrato.getPrimerInterviniente().getContratoInterviniente(contrato.getId()) != null
                  && contrato
                          .getPrimerInterviniente()
                          .getContratoInterviniente(contrato.getId())
                          .getTipoIntervencion()
                      != null
              ? contrato
                  .getPrimerInterviniente()
                  .getContratoInterviniente(contrato.getId())
                  .getTipoIntervencion()
                  .getValorIngles()
              : null);

      this.add(
          "Num Loans",
          expediente.getContratos() != null ? expediente.getContratos().size() : null);
      this.add(
          "Num Borrowers",
          contrato.getIntervinientes() != null ? contrato.getIntervinientes().size() : null);

      int numGarantias = 0;
      int numSolvencias = 0;
      for (var contratoBien : contrato.getBienes()) {
        if (contratoBien.getResponsabilidadHipotecaria() != null) {
          numGarantias++;
        } else {
          numSolvencias++;
        }
      }
      this.add("Num Warranties", numGarantias);
      this.add("Num Solvencies", numSolvencias);

      this.add(
          "Portfolio Manager", responsableCartera != null ? responsableCartera.getNombre() : null);
      this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
      String nombres = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          nombres += au.getNombre() + ", ";
        }
      }
      this.add("Management area", nombres);
      this.add("Connection manager", gestor != null ? gestor.getNombre() : null);

      String procedimientojudicial = "";
      List<Procedimiento> procedimientos = new ArrayList<>();
      for (Contrato c : expediente.getContratos()) {
        procedimientos.addAll(c.getProcedimientos());
      }
      if (!procedimientos.isEmpty()) {
        procedimientojudicial = "YES";
      } else {
        procedimientojudicial = "NO";
      }

      HitoProcedimiento hitoMax = null;

      for (Procedimiento procedimiento : procedimientos) {
        HitoProcedimiento hp = procedimiento.getHitoProcedimiento();
        if (hp != null) {
          String[] partes = hp.getCodigo().split("_");
          Integer hitoActual = Integer.parseInt(partes[1]);
          if (hitoMax == null || hitoMax.getNumero() < hitoActual) {
            hitoMax = hp;
          }
        }
      }

      this.add("Judicialized", procedimientojudicial);
      this.add("Judicialization phase", hitoMax != null ? hitoMax.getValorIngles() : null);

      List<Propuesta> propuestas = new ArrayList<>(expediente.getPropuestas());
      String propuestasB = "NO";
      EstadoPropuesta estadoPropuesta = new EstadoPropuesta();
      if (!propuestas.isEmpty()) {
        propuestasB = "YES";
        Date fecha = propuestas.get(0).getFechaAlta();
        for (Propuesta propuesta : propuestas) {
          estadoPropuesta = propuesta.getEstadoPropuesta();
          if (propuesta.getFechaAlta().after(fecha)) {
            estadoPropuesta = propuesta.getEstadoPropuesta();
            fecha = propuesta.getFechaAlta();
          }
        }
      }

      this.add(
          "Proposal",
          propuestasB); // Comprobar si el expediente Posesión negociada tiene propuestas SI/NO
      this.add(
          "Proposed status",
          estadoPropuesta
              .getValorIngles()); // De todas las propuestas el estado más avanzado a partir de la
                                  // fecha
      EstrategiaAsignada ea = contrato.getEstrategia();
      this.add("Balance in process", expediente.getSaldoGestion());
      this.add("Recovered amount", expediente.getImporteRecuperado());
      this.add("Principal pending", expediente.getPrincipalPendiente());
      this.add("Unpaid debt", contrato.getDeudaImpagada());
      this.add(
          "Balance in process",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getSaldoGestion()
              : null);
      this.add("Retrieved", expediente.getImporteRecuperado());
    } else {
      this.add("Id expediente", expediente.getIdConcatenado());
      this.add(
          "Cliente",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Cartera", cartera != null ? expediente.getCartera().getNombre() : null);

      String areas = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          areas += au.getNombre() + ", ";
        }
      }
      this.add("Area de gestion", areas);

      this.add("Contrato representante", contrato.getIdCarga());
      this.add(
          "Producto", contrato.getProducto() != null ? contrato.getProducto().getValor() : null);
      this.add(
          "Estado",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
      this.add(
          "Subestado",
          expediente.getSubEstado() != null ? expediente.getSubEstado().getValor() : null);

      if (contrato.getPrimerInterviniente() != null) {
        this.add(
            "NIF",
            contrato.getPrimerInterviniente().getNumeroDocumento() != null
                ? contrato.getPrimerInterviniente().getNumeroDocumento()
                : null);
        if (contrato.getPrimerInterviniente().getPersonaJuridica() != null
            && contrato.getPrimerInterviniente().getPersonaJuridica()) {
          this.add(
              "Nombre Primer titular",
              contrato.getPrimerInterviniente().getRazonSocial() != null
                  ? contrato.getPrimerInterviniente().getRazonSocial()
                  : null);
          this.add(
              "Fecha de nacimiento/Constitución",
              contrato.getPrimerInterviniente().getFechaConstitucion());
        } else {
          this.add("Nombre Primer titular", contrato.getPrimerInterviniente().getNombre());
          this.add("Apellidos Primer Titular", contrato.getPrimerInterviniente().getApellidos());
          this.add(
              "Fecha de nacimiento/Constitución",
              contrato.getPrimerInterviniente().getFechaNacimiento());
        }
      }
      this.add(
          "Tipo de relación",
          contrato.getPrimerInterviniente().getContratoInterviniente(contrato.getId()) != null
                  && contrato
                          .getPrimerInterviniente()
                          .getContratoInterviniente(contrato.getId())
                          .getTipoIntervencion()
                      != null
              ? contrato
                  .getPrimerInterviniente()
                  .getContratoInterviniente(contrato.getId())
                  .getTipoIntervencion()
                  .getValor()
              : null);

      this.add(
          "Num Contratos",
          expediente.getContratos() != null ? expediente.getContratos().size() : null);
      this.add(
          "Num Intervinientes",
          contrato.getIntervinientes() != null ? contrato.getIntervinientes().size() : null);

      int numGarantias = 0;
      int numSolvencias = 0;
      for (var contratoBien : contrato.getBienes()) {
        if (contratoBien.getResponsabilidadHipotecaria() != null) {
          numGarantias++;
        } else {
          numSolvencias++;
        }
      }
      this.add("Num Garantias", numGarantias);
      this.add("Num Solvencias", numSolvencias);

      this.add(
          "Responsable de cartera",
          responsableCartera != null ? responsableCartera.getNombre() : null);
      this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
      String nombres = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          nombres += au.getNombre() + ", ";
        }
      }
      this.add("Área de gestión", nombres);
      this.add("Gestor del expediente", gestor != null ? gestor.getNombre() : null);

      String procedimientojudicial = "";
      List<Procedimiento> procedimientos = new ArrayList<>();
      for (Contrato c : expediente.getContratos()) {
        procedimientos.addAll(c.getProcedimientos());
      }
      if (!procedimientos.isEmpty()) {
        procedimientojudicial = "SI";
      } else {
        procedimientojudicial = "NO";
      }

      HitoProcedimiento hitoMax = null;

      for (Procedimiento procedimiento : procedimientos) {
        HitoProcedimiento hp = procedimiento.getHitoProcedimiento();
        if (hp != null) {
          String[] partes = hp.getCodigo().split("_");
          Integer hitoActual = Integer.parseInt(partes[1]);
          if (hitoMax == null || hitoMax.getNumero() < hitoActual) {
            hitoMax = hp;
          }
        }
      }

      this.add("Judicializado", procedimientojudicial);
      this.add("Fase judicialización", hitoMax != null ? hitoMax.getValor() : null);

      List<Propuesta> propuestas = new ArrayList<>(expediente.getPropuestas());
      String propuestasB = "NO";
      EstadoPropuesta estadoPropuesta = new EstadoPropuesta();
      if (!propuestas.isEmpty()) {
        propuestasB = "SI";
        Date fecha = propuestas.get(0).getFechaAlta();
        for (Propuesta propuesta : propuestas) {
          estadoPropuesta = propuesta.getEstadoPropuesta();
          if (propuesta.getFechaAlta().after(fecha)) {
            estadoPropuesta = propuesta.getEstadoPropuesta();
            fecha = propuesta.getFechaAlta();
          }
        }
      }

      this.add(
          "Propuesta",
          propuestasB); // Comprobar si el expediente Posesión negociada tiene propuestas SI/NO
      this.add(
          "Propuesta estado",
          estadoPropuesta
              .getValor()); // De todas las propuestas el estado más avanzado a partir de la fecha
      EstrategiaAsignada ea = contrato.getEstrategia();
      this.add("Saldo en gestión", expediente.getSaldoGestion());
      this.add("Importe recuperado", expediente.getImporteRecuperado());
      this.add("Principal pendiente", expediente.getPrincipalPendiente());
      this.add("Deuda impagada", contrato.getDeudaImpagada());
      this.add(
          "Saldo en gestión",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getSaldoGestion()
              : null);
      this.add("Recuperado", expediente.getImporteRecuperado());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
