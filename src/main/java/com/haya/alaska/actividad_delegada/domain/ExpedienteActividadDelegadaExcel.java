package com.haya.alaska.actividad_delegada.domain;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;
import java.util.stream.Collectors;

public class ExpedienteActividadDelegadaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Customer");
      cabeceras.add("Portfolio");
      cabeceras.add("Management area");
      cabeceras.add("Representative Loan");
      cabeceras.add("Product");
      cabeceras.add("Status");
      cabeceras.add("Substate");
      cabeceras.add("First holder Name");
      cabeceras.add("First holder Surnames");
      cabeceras.add("NIF");
      cabeceras.add("Relation type");
      cabeceras.add("Birth/Constitution Date");
      cabeceras.add("Num Loans");
      cabeceras.add("Num Borrowers");
      cabeceras.add("Num Warranties");
      cabeceras.add("Num Solvencies");
      cabeceras.add("Connection manager");
      cabeceras.add("Judicialized");
      cabeceras.add("Judicialization phase");
      cabeceras.add("Proposal");
      cabeceras.add("Proposal Status");
      cabeceras.add("Balance under management");
      cabeceras.add("Recovered amount");
      cabeceras.add("Principal pending");
      cabeceras.add("Unpaid debt");
      cabeceras.add("Retrieved");
    } else {
      cabeceras.add("Id expediente");
      cabeceras.add("Cliente");
      cabeceras.add("Cartera");
      cabeceras.add("Area de gestion");
      cabeceras.add("Contrato representante");
      cabeceras.add("Producto");
      cabeceras.add("Estado");
      cabeceras.add("Subestado");
      cabeceras.add("Nombre Primer titular");
      cabeceras.add("Apellidos Primer Titular");
      cabeceras.add("NIF");
      cabeceras.add("Tipo de relación");
      cabeceras.add("Fecha de nacimiento/Constitución");
      cabeceras.add("Num Contratos");
      cabeceras.add("Num Intervinientes");
      cabeceras.add("Num Garantías");
      cabeceras.add("Num Solvencias");
      cabeceras.add("Gestor del expediente");
      cabeceras.add("Judicializado");
      cabeceras.add("Fase judicialización");
      cabeceras.add("Propuesta");
      cabeceras.add("Propuesta estado");
      cabeceras.add("Saldo en gestión");
      cabeceras.add("Importe recuperado");
      cabeceras.add("Principal pendiente");
      cabeceras.add("Deuda impagada");
      cabeceras.add("Recuperado");
    }
  }

  public ExpedienteActividadDelegadaExcel(Expediente expediente) {
    super();

    Contrato contratoRepresentante = expediente.getContratoRepresentante();
    Cartera cartera = expediente.getCartera();
    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");
    Usuario gestorFormalizacion = expediente.getUsuarioByPerfil("Gestor formalización");
    Usuario gestorSkipTracing = expediente.getUsuarioByPerfil("Gestor skip tracing");
    Usuario responsableCartera = expediente.getUsuarioByPerfil("Responsable de cartera");
    Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Connection Id", expediente.getIdConcatenado());
      this.add(
          "Customer",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Portfolio", cartera != null ? expediente.getCartera().getNombre() : null);

      String areas = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          areas += au.getNombre() + ", ";
        }
      }
      this.add("Management area", areas);

      this.add("Representative Loan", contratoRepresentante.getIdCarga());
      this.add(
          "Product",
          contratoRepresentante.getProducto() != null
              ? contratoRepresentante.getProducto().getValorIngles()
              : null);
      this.add(
          "Status",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValorIngles() : null);
      this.add(
          "Substate",
          expediente.getSubEstado() != null ? expediente.getSubEstado().getValorIngles() : null);

      if (contratoRepresentante.getPrimerInterviniente() != null) {
        this.add(
            "NIF",
            contratoRepresentante.getPrimerInterviniente().getNumeroDocumento() != null
                ? contratoRepresentante.getPrimerInterviniente().getNumeroDocumento()
                : null);
        if (contratoRepresentante.getPrimerInterviniente().getPersonaJuridica() != null
            && contratoRepresentante.getPrimerInterviniente().getPersonaJuridica()) {
          this.add(
              "First holder Name",
              contratoRepresentante.getPrimerInterviniente().getRazonSocial() != null
                  ? contratoRepresentante.getPrimerInterviniente().getRazonSocial()
                  : null);
          this.add(
              "Birth/Constitution Date",
              contratoRepresentante.getPrimerInterviniente().getFechaConstitucion());
        } else {
          this.add("First holder Name", contratoRepresentante.getPrimerInterviniente().getNombre());
          this.add(
              "First holder Surnames",
              contratoRepresentante.getPrimerInterviniente().getApellidos());
          this.add(
              "Birth/Constitution Date",
              contratoRepresentante.getPrimerInterviniente().getFechaNacimiento());
        }
      }
      this.add(
          "Relation type",
          contratoRepresentante
                          .getPrimerInterviniente()
                          .getContratoInterviniente(contratoRepresentante.getId())
                      != null
                  && contratoRepresentante
                          .getPrimerInterviniente()
                          .getContratoInterviniente(contratoRepresentante.getId())
                          .getTipoIntervencion()
                      != null
              ? contratoRepresentante
                  .getPrimerInterviniente()
                  .getContratoInterviniente(contratoRepresentante.getId())
                  .getTipoIntervencion()
                  .getValorIngles()
              : null);

      Integer contadorIntervinientes = 0;
      int numGarantias = 0;
      int numSolvencias = 0;
      if (expediente.getContratos() != null) {
        for (var contrato : expediente.getContratos()) {
          contadorIntervinientes += contrato.getIntervinientes().size();
          for (var contratoBien : contrato.getBienes()) {
            if (contratoBien.getResponsabilidadHipotecaria() != null) {
              numGarantias++;
            } else {
              numSolvencias++;
            }
          }
        }
      }
      this.add(
          "Num Loans",
          expediente.getContratos() != null ? expediente.getContratos().size() : null);
      this.add("Num Borrowers", contadorIntervinientes);
      this.add("Num Warranties", numGarantias);
      this.add("Num Solvencies", numSolvencias);

      this.add(
          "Portfolio Manager", responsableCartera != null ? responsableCartera.getNombre() : null);
      this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
      String nombres = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          nombres += au.getNombre() + ", ";
        }
      }
      this.add("Management area", nombres);
      this.add("Connection manager", gestor != null ? gestor.getNombre() : null);

      String procedimientojudicial = "";
      List<Procedimiento> procedimientos = new ArrayList<>();
      for (Contrato c : expediente.getContratos()) {
        procedimientos.addAll(c.getProcedimientos());
      }
      if (!procedimientos.isEmpty()) {
        procedimientojudicial = "YES";
      } else {
        procedimientojudicial = "NO";
      }

      HitoProcedimiento hitoMax = null;

      for (Procedimiento procedimiento : procedimientos) {
        HitoProcedimiento hp = procedimiento.getHitoProcedimiento();
        if (hp != null) {
          String[] partes = hp.getCodigo().split("_");
          Integer hitoActual = Integer.parseInt(partes[1]);
          if (hitoMax == null || hitoMax.getNumero() < hitoActual) {
            hitoMax = hp;
          }
        }
      }

      this.add("Judicialized", procedimientojudicial);
      this.add("Judicialization phase", hitoMax != null ? hitoMax.getValorIngles() : null);

      List<Propuesta> propuestas = new ArrayList<>(expediente.getPropuestas());
      String propuestasB = "NO";
      EstadoPropuesta estadoPropuesta = new EstadoPropuesta();
      if (!propuestas.isEmpty()) {
        propuestasB = "YES";
        Date fecha = propuestas.get(0).getFechaAlta();
        for (Propuesta propuesta : propuestas) {
          estadoPropuesta = propuesta.getEstadoPropuesta();
          if (propuesta.getFechaAlta().after(fecha)) {
            estadoPropuesta = propuesta.getEstadoPropuesta();
            fecha = propuesta.getFechaAlta();
          }
        }
      }

      this.add(
          "Proposal",
          propuestasB); // Comprobar si el expediente Posesión negociada tiene propuestas SI/NO
      this.add(
          "Proposal Status",
          estadoPropuesta
              .getValorIngles()); // De todas las propuestas el estado más avanzado a partir de la
                                  // fecha
      EstrategiaAsignada ea = contratoRepresentante.getEstrategia();
      this.add("Balance under management", expediente.getSaldoGestion());
      this.add("Recovered amount", expediente.getImporteRecuperado());
      this.add("Principal pending", expediente.getPrincipalPendiente());
      this.add("Unpaid debt", contratoRepresentante.getDeudaImpagada());
      this.add(
          "Balance under management",
          contratoRepresentante.getSaldoContrato() != null
              ? contratoRepresentante.getSaldoContrato().getSaldoGestion()
              : null);
      this.add("Retrieved", expediente.getImporteRecuperado());
    } else {
      this.add("Id expediente", expediente.getIdConcatenado());
      this.add(
          "Cliente",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Cartera", cartera != null ? expediente.getCartera().getNombre() : null);

      String areas = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          areas += au.getNombre() + ", ";
        }
      }
      this.add("Area de gestion", areas);

      this.add("Contrato representante", contratoRepresentante.getIdCarga());
      this.add(
          "Producto",
          contratoRepresentante.getProducto() != null
              ? contratoRepresentante.getProducto().getValor()
              : null);
      this.add(
          "Estado",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
      this.add(
          "Subestado",
          expediente.getSubEstado() != null ? expediente.getSubEstado().getValor() : null);

      if (contratoRepresentante.getPrimerInterviniente() != null) {
        this.add(
            "NIF",
            contratoRepresentante.getPrimerInterviniente().getNumeroDocumento() != null
                ? contratoRepresentante.getPrimerInterviniente().getNumeroDocumento()
                : null);
        if (contratoRepresentante.getPrimerInterviniente().getPersonaJuridica() != null
            && contratoRepresentante.getPrimerInterviniente().getPersonaJuridica()) {
          this.add(
              "Nombre Primer titular",
              contratoRepresentante.getPrimerInterviniente().getRazonSocial() != null
                  ? contratoRepresentante.getPrimerInterviniente().getRazonSocial()
                  : null);
          this.add(
              "Fecha de nacimiento/Constitución",
              contratoRepresentante.getPrimerInterviniente().getFechaConstitucion());
        } else {
          this.add(
              "Nombre Primer titular", contratoRepresentante.getPrimerInterviniente().getNombre());
          this.add(
              "Apellidos Primer Titular",
              contratoRepresentante.getPrimerInterviniente().getApellidos());
          this.add(
              "Fecha de nacimiento/Constitución",
              contratoRepresentante.getPrimerInterviniente().getFechaNacimiento());
        }
      }
      this.add(
          "Tipo de relación",
          contratoRepresentante
                          .getPrimerInterviniente()
                          .getContratoInterviniente(contratoRepresentante.getId())
                      != null
                  && contratoRepresentante
                          .getPrimerInterviniente()
                          .getContratoInterviniente(contratoRepresentante.getId())
                          .getTipoIntervencion()
                      != null
              ? contratoRepresentante
                  .getPrimerInterviniente()
                  .getContratoInterviniente(contratoRepresentante.getId())
                  .getTipoIntervencion()
                  .getValor()
              : null);

      Integer contadorIntervinientes = 0;
      int numGarantias = 0;
      int numSolvencias = 0;
      if (expediente.getContratos() != null) {
        for (var contrato : expediente.getContratos()) {
          contadorIntervinientes += contrato.getIntervinientes().size();
          for (var contratoBien : contrato.getBienes()) {
            if (contratoBien.getResponsabilidadHipotecaria() != null) {
              numGarantias++;
            } else {
              numSolvencias++;
            }
          }
        }
      }
      this.add(
          "Num Contratos",
          expediente.getContratos() != null ? expediente.getContratos().size() : null);
      this.add("Num Intervinientes", contadorIntervinientes);
      this.add("Num Garantias", numGarantias);
      this.add("Num Solvencias", numSolvencias);

      this.add(
          "Responsable de cartera",
          responsableCartera != null ? responsableCartera.getNombre() : null);
      this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
      String nombres = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          nombres += au.getNombre() + ", ";
        }
      }
      this.add("Área de gestión", nombres);
      this.add("Gestor del expediente", gestor != null ? gestor.getNombre() : null);

      String procedimientojudicial = "";
      List<Procedimiento> procedimientos = new ArrayList<>();
      for (Contrato c : expediente.getContratos()) {
        procedimientos.addAll(c.getProcedimientos());
      }
      if (!procedimientos.isEmpty()) {
        procedimientojudicial = "SI";
      } else {
        procedimientojudicial = "NO";
      }

      HitoProcedimiento hitoMax = null;

      for (Procedimiento procedimiento : procedimientos) {
        HitoProcedimiento hp = procedimiento.getHitoProcedimiento();
        if (hp != null) {
          String[] partes = hp.getCodigo().split("_");
          Integer hitoActual = Integer.parseInt(partes[1]);
          if (hitoMax == null || hitoMax.getNumero() < hitoActual) {
            hitoMax = hp;
          }
        }
      }

      this.add("Judicializado", procedimientojudicial);
      this.add("Fase judicialización", hitoMax != null ? hitoMax.getValor() : null);

      List<Propuesta> propuestas = new ArrayList<>(expediente.getPropuestas());
      String propuestasB = "NO";
      EstadoPropuesta estadoPropuesta = new EstadoPropuesta();
      if (!propuestas.isEmpty()) {
        propuestasB = "SI";
        Date fecha = propuestas.get(0).getFechaAlta();
        for (Propuesta propuesta : propuestas) {
          estadoPropuesta = propuesta.getEstadoPropuesta();
          if (propuesta.getFechaAlta().after(fecha)) {
            estadoPropuesta = propuesta.getEstadoPropuesta();
            fecha = propuesta.getFechaAlta();
          }
        }
      }

      this.add(
          "Propuesta",
          propuestasB); // Comprobar si el expediente Posesión negociada tiene propuestas SI/NO
      this.add(
          "Propuesta estado",
          estadoPropuesta
              .getValor()); // De todas las propuestas el estado más avanzado a partir de la fecha
      EstrategiaAsignada ea = contratoRepresentante.getEstrategia();
      this.add("Saldo en gestión", expediente.getSaldoGestion());
      this.add("Importe recuperado", expediente.getImporteRecuperado());
      this.add("Principal pendiente", expediente.getPrincipalPendiente());
      this.add("Deuda impagada", contratoRepresentante.getDeudaImpagada());
      this.add(
          "Saldo en gestión",
          contratoRepresentante.getSaldoContrato() != null
              ? contratoRepresentante.getSaldoContrato().getSaldoGestion()
              : null);
      this.add("Recuperado", expediente.getImporteRecuperado());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
