package com.haya.alaska.actividad_delegada.aplication;

import com.haya.alaska.actividad_delegada.infrastructure.controller.dto.ExpedienteActividadDelegadaOutputDTO;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface ActividadDelegadaUseCase {
  LinkedHashMap<String, List<Collection<?>>> generateExcel(
    BusquedaDto busqueda, Usuario user, Boolean telefon1erTitular, Boolean telefonRestoIntervinientes, Boolean direccion1erTitular,
    Boolean direccionRestoIntervinientes, List<Integer> ids, Boolean todos) throws Exception;
  void carga(MultipartFile file, Usuario user) throws Exception;
  ListWithCountDTO<ExpedienteActividadDelegadaOutputDTO> getAllExpedientes(
    BusquedaDto busqueda, Usuario user, String idConcatenado, String estado, String cliente, String cartera,
    String primerTitular, Integer nContratos, Integer nIntervinientes, Integer nBienes, Double saldoGestion,
    String estrategia, Boolean telefon1erTitular, Boolean telefonRestoIntervinientes, Boolean direccion1erTitular,
    Boolean direccionRestoIntervinientes, String orderField, String orderDirection, Integer page, Integer size
  ) throws com.haya.alaska.shared.exceptions.NotFoundException;

  ByteArrayInputStream create(LinkedHashMap<String, List<Collection<?>>> datos) throws IOException;
}
