package com.haya.alaska.actividad_delegada.aplication;

import com.haya.alaska.actividad_delegada.domain.BienActividadDelegadaExcel;
import com.haya.alaska.actividad_delegada.domain.ContratoActividadDelegadaExcel;
import com.haya.alaska.actividad_delegada.domain.ExpedienteActividadDelegadaExcel;
import com.haya.alaska.actividad_delegada.domain.IntervinienteActividadDelegadaExcel;
import com.haya.alaska.actividad_delegada.infrastructure.controller.dto.ExpedienteActividadDelegadaOutputDTO;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.busqueda.application.BusquedaUseCase;
import com.haya.alaska.busqueda.domain.BusquedaBuilder;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.BusquedaExpedienteDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.IntervaloImporte;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.clase_evento.infrastructure.repository.ClaseEventoRepository;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.domain.Cliente_;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente_;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.estado_evento.infrastructure.repository.EstadoEventoRepository;
import com.haya.alaska.estrategia.domain.Estrategia_;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada_;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.importancia_evento.infrastructure.repository.ImportanciaEventoRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.domain.Interviniente_;
import com.haya.alaska.nivel_evento.infrastructure.repository.NivelEventoRepository;
import com.haya.alaska.periodicidad_evento.infrastructure.repository.PeriodicidadEventoRepository;
import com.haya.alaska.resultado_evento.domain.ResultadoEvento;
import com.haya.alaska.resultado_evento.infrastructure.repository.ResultadoEventoRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.tipo_evento.infrastructure.repository.TipoEventoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.procedimiento.domain.Procedimiento;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Component
@Slf4j
public class ActividadDelegadaUseCaseImpl implements ActividadDelegadaUseCase{
  @PersistenceContext
  private EntityManager entityManager;
  @Autowired
  private EventoRepository eventoRepository;
  @Autowired
  private ClaseEventoRepository claseEventoRepository;
  @Autowired
  private CarteraRepository carteraRepository;
  @Autowired
  private TipoEventoRepository tipoEventoRepository;
  @Autowired
  private SubtipoEventoRepository subtipoEventoRepository;
  @Autowired
  private PeriodicidadEventoRepository periodicidadEventoRepository;
  @Autowired
  private EstadoEventoRepository estadoEventoRepository;
  @Autowired
  private ImportanciaEventoRepository importanciaEventoRepository;
  @Autowired
  private ResultadoEventoRepository resultadoEventoRepository;
  @Autowired
  private UsuarioRepository usuarioRepository;
  @Autowired
  private NivelEventoRepository nivelEventoRepository;
  @Autowired
  private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  private ExpedienteRepository expedienteRepository;
  @Autowired
  private BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  private ExpedienteUtil expedienteUtil;
  @Autowired
  private ExpedienteMapper expedienteMapper;
  @Autowired
  private BusquedaUseCase busquedaUseCase;

  public LinkedHashMap<String, List<Collection<?>>> generateExcel(
    BusquedaDto busqueda, Usuario user, Boolean telefon1erTitular, Boolean telefonRestoIntervinientes, Boolean direccion1erTitular,
    Boolean direccionRestoIntervinientes, List<Integer> ids, Boolean todos) throws Exception {

    List<Expediente> expedientes = new ArrayList<>();
    if(todos && busqueda != null) {
      expedientes = filtrados(
        busqueda, user, null,null,null,null,null,null,null,null,null,null,
        telefon1erTitular, telefonRestoIntervinientes, direccion1erTitular, direccionRestoIntervinientes);
    } else if(ids != null){
      expedientes = expedienteRepository.findByIdIn(ids);
    } else {
      throw new Exception("No se ha introducido ningún expediente ni criterios de búsqueda");
    }
    var datos = this.createActividadDelegadaDataForExcel();
    //for (var expediente: expedientes) {
      this.addActividadDelegadaDataForExcel(expedientes, datos, telefon1erTitular, telefonRestoIntervinientes, direccion1erTitular, direccionRestoIntervinientes);
      //si es pn sera otro metodo que hay que copiar tambien
    //}
    return datos;

  }

  private LinkedHashMap<String, List<Collection<?>>> createActividadDelegadaDataForExcel() {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();
    //LA PARTE DE PN ESTA SIN HACER. DA ERROR SI SE USA.
    /*if (posesionNegociada == true) {
      List<Collection<?>> expediente = new ArrayList<>();
      expediente.add(ExpedientePosesionNegociadaExcel.cabeceras);
      datos.put("Expediente", expediente);

      List<Collection<?>> ocupantes = new ArrayList<>();
      ocupantes.add(OcupanteInformeExcel.cabeceras);
      datos.put("Ocupantes", ocupantes);

      List<Collection<?>> bienes = new ArrayList<>();
      bienes.add(BienActividadDelegadaExcel.cabeceras);
      datos.put("Colaterales", bienes);

    } else if (posesionNegociada == false) {*/
    List<Collection<?>> expediente = new ArrayList<>();
    expediente.add(ExpedienteActividadDelegadaExcel.cabeceras);

    List<Collection<?>> contrato = new ArrayList<>();
    contrato.add(ContratoActividadDelegadaExcel.cabeceras);

    List<Collection<?>> intervinientes = new ArrayList<>();
    intervinientes.add(IntervinienteActividadDelegadaExcel.getSobreCabecera());
    intervinientes.add(IntervinienteActividadDelegadaExcel.cabeceras);

    List<Collection<?>> bienes = new ArrayList<>();
    bienes.add(BienActividadDelegadaExcel.getSobreCabecera());
    bienes.add(BienActividadDelegadaExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      datos.put("Connection", expediente);
      datos.put("Loan", contrato);
      datos.put("Borrowers", intervinientes);
      datos.put("Collaterals", bienes);

    } else {

      datos.put("Expediente", expediente);
      datos.put("Contrato", contrato);
      datos.put("Intervinientes", intervinientes);
      datos.put("Colaterales", bienes);
    }
    //}

    return datos;
  }

  private void addActividadDelegadaDataForExcel(List<Expediente> expedientesFilter, LinkedHashMap<String, List<Collection<?>>> datos,  Boolean telefon1erTitular, Boolean telefonRestoIntervinientes, Boolean direccion1erTitular,
                                                Boolean direccionRestoIntervinientes) throws ParseException, NotFoundException{

    List<Collection<?>> expedientes;
    List<Collection<?>> contratos;
    List<Collection<?>> intervinientes;
    List<Collection<?>> bienes;

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      expedientes = datos.get("Connection");
      contratos = datos.get("Loan");
      intervinientes = datos.get("Borrowers");
      bienes = datos.get("Collaterals");

    } else  {
      expedientes = datos.get("Expediente");
      contratos = datos.get("Contrato");
      intervinientes = datos.get("Intervinientes");
      bienes = datos.get("Colaterales");
    }

    /*List<Expediente> expedientesFilter = null;
    if(usuario.esAdministrador() == true) {
      expedientesFilter = expedienteRepository.findAllByCarteraId(cartera.getId());
    } else {
      expedientesFilter = expedienteRepository.findAllByCarteraIdAndAsignacionesUsuarioId(cartera.getId(), usuario.getId());
    }*/

    if(expedientesFilter == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("There is no data for the selected user");
      else throw new NotFoundException("No existe ningún dato para el usuario seleccionado");
    }

    /*expedientesFilter.stream().filter(
      expediente -> {
        Date fCE = expediente.getFechaCreacion();
        Date fCI = informeCarteraInputDTO.getFechaCreacionInicial();
        Date fCF = informeCarteraInputDTO.getFechaCreacionFinal();
        if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;

        Date fRE = expediente.getFechaCierre();
        Date fRI = informeCarteraInputDTO.getFechaCierreInicial();
        Date fRF = informeCarteraInputDTO.getFechaCierreFinal();
        if (!expedienteUtil.comprobarEntre(fRE, fRI, fRF)) return false;

        return usuario.contieneExpediente(expediente.getId(), false);
      }).collect(Collectors.toList());*/

    for (Expediente expediente : expedientesFilter) {
      var estadoExpediente = expedienteUtil.getEstado(expediente);
      //if (informeCarteraInputDTO.getEstado() == null || estadoExpediente.getId().equals(informeCarteraInputDTO.getEstado())) {
      //estas tres no van a hacer falta:
        //Evento accion = carteraExcelUseCase.getLastEvento(expediente.getId(), 1);
        //Evento alerta = carteraExcelUseCase.getLastEvento(expediente.getId(), 2);
        //Evento actividad = carteraExcelUseCase.getLastEvento(expediente.getId(), 3);
        Procedimiento p = expedienteUtil.getUltimoProcedimiento(expediente);
        expedientes.add(new ExpedienteActividadDelegadaExcel(expediente).getValuesList());
        for (Contrato contrato : expediente.getContratos()) {
          contratos.add(new ContratoActividadDelegadaExcel(contrato).getValuesList());
          if (contrato.getIntervinientes() != null) {
            Contrato contratoRepresentante = expediente.getContratoRepresentante();
            Interviniente primerInterviniente = contratoRepresentante.getPrimerInterviniente();
            for (ContratoInterviniente interviniente : contrato.getIntervinientes()) {
              if(interviniente.getInterviniente().getId().equals(primerInterviniente.getId()) && !direccion1erTitular && !telefon1erTitular)
                continue;
              else if(!interviniente.getInterviniente().getId().equals(primerInterviniente.getId()) && !direccionRestoIntervinientes && !telefonRestoIntervinientes)
                continue;

              Boolean telefonos = true;
              Boolean direcciones = true;
              if(interviniente.getInterviniente().getId() == primerInterviniente.getId() && telefon1erTitular == false)
                telefonos=false;
              else if(interviniente.getInterviniente().getId() != primerInterviniente.getId() && telefonRestoIntervinientes == false)
                telefonos=false;
              if(interviniente.getInterviniente().getId() == primerInterviniente.getId() && direccion1erTitular == false)
                direcciones=false;
              else if(interviniente.getInterviniente().getId() != primerInterviniente.getId() && direccionRestoIntervinientes == false)
                direcciones=false;

              intervinientes.add(new IntervinienteActividadDelegadaExcel(interviniente.getInterviniente(), contrato, interviniente.getTipoIntervencion(), telefonos, direcciones).getValuesList());
            }
          }
          if (contrato.getBienes() != null) {
            for (ContratoBien bien : contrato.getBienes()) {
              BienPosesionNegociada bienPN = bienPosesionNegociadaRepository.findByBien(bien.getBien()).orElse(null);
              bienes.add(new BienActividadDelegadaExcel(bien.getBien(), contrato, bienPN).getValuesList());
            }
          }
        }
      //}
    }
  }

  public void carga(MultipartFile file, Usuario user) throws Exception {
    List<EventoInputDTO> eventos = new ArrayList<>();
    XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
    XSSFSheet worksheetIntervinientes = workbook.getSheetAt(2);
    eventos.addAll(obtenerIntervinientes(worksheetIntervinientes));
    XSSFSheet worksheetBienes = workbook.getSheetAt(3);
    eventos.addAll(obtenerBienes(worksheetBienes));

    for (EventoInputDTO input : eventos){
      Evento e = createEvento(input, user);
      eventoRepository.save(e);
    }
  }

  public List<EventoInputDTO> obtenerIntervinientes(XSSFSheet sheet) throws NotFoundException {
    List<EventoInputDTO> eventos = new ArrayList<>();
    for (int i = 2; i < sheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = sheet.getRow(i);

      if (row != null){
        EventoInputDTO input = obtenerRowInterviniente(row);
        if (input != null) eventos.add(input);
      }
    }

    return eventos;
  }

  public List<EventoInputDTO> obtenerBienes(XSSFSheet sheet) throws NotFoundException {
    List<EventoInputDTO> eventos = new ArrayList<>();
    for (int i = 2; i < sheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = sheet.getRow(i);

      if (row != null){
        EventoInputDTO input = obtenerRowBien(row);
        if (input != null) eventos.add(input);
      }
    }

    return eventos;
  }

  private String getSubtipos(Integer id) throws NotFoundException {
    String result = null;
    switch (id){
      case 1:
        result = "LLAM01";
        break;
      case 2:
        result = "VIS01";
        break;
      default:
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Allowed values are '1' for calls and '2' for visits.");
        else throw new NotFoundException("Los valores permitidos son '1' para llamadas y '2' para visitas.");
    }
    return result;
  }

  private String getResultados(Integer id) throws NotFoundException {
    String result = null;
    switch (id){
      case 1:
        result = "C_ACU";
        break;
      case 2:
        result = "S_ACU";
        break;
      case 3:
        result = "S_CON";
        break;
      case 4:
        result = "C_INC";
        break;
      case 5:
        result = "C_TER";
        break;
      case 6:
        result = "I_CLI";
        break;
      case 7:
        result = "A_PAG";
        break;
      case 8:
        result = "D_INC";
        break;
      case 9:
        result = "CTCT";
        break;
      case 10:
        result = "CTST";
        break;
      default:
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Los valores permitidos son '1' para 'Con Acuerdo', '2' para 'Sin Acuerdo', '3' para 'Sin Contacto', " +
                  "'4' para 'Contacto Incorrecto', '5' para 'Contacto con Tercero', '6' para 'Incidencia Cliente', '7' para 'Acuerdo de Pago', " +
                  "'8' para 'Direccion Incorrecta', '9' para 'Contacto con Tercero Con Título' o '10' para 'Contacto con Tercero Sin Título'.");
        else throw new NotFoundException("Allowed values are '1' for 'With Agreement', '2' for 'No Agreement', '3' for 'No Contact'," +
                "'4' for 'Incorrect Contact', '5' for 'Contact with Third Party', '6' for 'Customer Incident', '7' for 'Payment Agreement'," +
                "'8' for 'Incorrect Address', '9' for 'Contact with a Third Party with Title' or '10' for 'Contact with a Third Party with No Title'.");
    }
    return result;
  }

  public EventoInputDTO obtenerRowInterviniente(XSSFRow row) throws NotFoundException {
    int i = 37; // ultimo antiguo: Email GESTOR, AL
    EventoInputDTO result = new EventoInputDTO();
    setElementosComunes(result);
    result.setNivel(4); //Nivel interviniente
    String descripcion = "";

    Integer idCartera;

    if (row.getCell(3) != null) {
      Integer idInterviniente = getIntegerValue(row, 3);
      result.setNivelId(idInterviniente);
    } else {
      throw new NotFoundException("interviniente", row.getRowNum(), "id");
    }

    if (row.getCell(7) != null) {
      String idC = getRawStringValueConGuion(row, 7);
      String idConcat = limpiarConcatenado(idC);
      Expediente expediente = expedienteRepository.findByIdConcatenado(idConcat).orElseThrow(() ->
        new NotFoundException("Expediente", "IdConcatenado", idConcat));
      result.setIdExpediente(expediente.getId());
      idCartera = expediente.getCartera().getId();
      result.setCartera(idCartera);
      Usuario gestor = expediente.getGestor();
      if (gestor != null) result.setDestinatario(gestor.getId());
      else {
        Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");
        if (supervisor != null) result.setDestinatario(supervisor.getId());
        else throw new NotFoundException("expediente", row.getRowNum(), "ni gestor ni supervisor.");
      }
    } else {
      throw new NotFoundException("interviniente", row.getRowNum(), " un expediente asociado.");
    }

    if (row.getCell(i + 1) != null) {
      Date fechaLlamada = getDateValueConGuion(row, i + 1);
      if (fechaLlamada == null) return null;
      Calendar c = Calendar.getInstance();
      c.setTime(fechaLlamada);

      if (row.getCell(i + 2) != null){
        Integer[] lista = getTimeValueConGuion(row, i+2);
        if (lista != null){
          c.set(Calendar.HOUR, lista[0]);
          c.set(Calendar.MINUTE, lista[1]);
          c.set(Calendar.SECOND, lista[2]);
          c.set(Calendar.MILLISECOND, lista[3]);
        }
      }
      Date d = c.getTime();

      result.setFechaCreacion(d);
      result.setFechaAlerta(d);
      result.setFechaLimite(d);
    } else {
      return null;
    }

    if (row.getCell(i + 3) != null) {
      Integer resultadoId = getIntegerValueConGuion(row, i + 3);
      if (resultadoId != null){
        String resultado = getResultados(resultadoId);
        ResultadoEvento re = resultadoEventoRepository.findByCodigo(resultado).orElseThrow(() ->
          new NotFoundException("ResultadoEvento", "Código", resultado));
        result.setResultado(re.getId());
      }
    }

    if (row.getCell(i + 4) != null) {
      Integer subtipoId = getIntegerValueConGuion(row, i + 4);
      if (subtipoId == null) throw new NotFoundException("SubtipoEvento", row.getRowNum());
      String subtipo = getSubtipos(subtipoId);
      SubtipoEvento se = subtipoEventoRepository.findByCodigoAndCarteraId(subtipo, idCartera).orElseThrow(() ->
        new NotFoundException("SubtipoEvento", subtipo, "Cartera" + idCartera));
      TipoEvento te = se.getTipoEvento();
      ClaseEvento ce = te.getClaseEvento();

      result.setClaseEvento(ce.getId());
      result.setTipoEvento(te.getId());
      result.setSubtipoEvento(se.getId());
    } else {
      throw new NotFoundException("interviniente", row.getRowNum(), "un tipo de Acción");
    }

    if (row.getCell(i + 5) != null) {
      String string = getRawStringValueConGuion(row, i + 5);
      if (string != null)
        descripcion += "Persona de Contacto: " + string + "\n";
    }
    if (row.getCell(i + 6) != null) {
      String string = getRawStringValueConGuion(row, i + 6);
      if (string != null)
        descripcion += "Relación con el Interviniente: " + string + "\n";
    }
    if (row.getCell(i + 7) != null) {
      String string = getRawStringValueConGuion(row, i + 7);
      if (string != null)
        descripcion += "Teléfono nuevo: " + string + "\n";
    }
    if (row.getCell(i + 8) != null) {
      String string = getRawStringValueConGuion(row, i + 8);
      if (string != null)
        descripcion += "Domicilio nuevo: " + string + "\n";
    }
    if (row.getCell(i + 9) != null) {
      String string = getRawStringValueConGuion(row, i + 9);
      if (string != null)
        descripcion += "Email nuevo: " + string + "\n";
    }
    if (row.getCell(i + 10) != null) {
      String string = getRawStringValueConGuion(row, i + 10);
      if (string != null)
        descripcion += "Mejor hora Contacto/Visita: " + string + "\n";
    }
    if (row.getCell(i + 11) != null) {
      String string = getRawStringValueConGuion(row, i + 11);
      if (string != null)
        descripcion += "Observaciones: " + string + "\n";
    }

    result.setComentariosDestinatario(descripcion);

    return result;
  }

  public EventoInputDTO obtenerRowBien(XSSFRow row) throws NotFoundException {
    int i = 44; // ultimo antiguo: Email GESTOR, AS
    EventoInputDTO result = new EventoInputDTO();
    setElementosComunes(result);
    result.setNivel(3); // Nivel bien
    String descripcion = "";

    Integer idCartera;

    if (row.getCell(3) != null) {
      Integer idBien = getIntegerValue(row, 3);
      result.setNivelId(idBien);
    } else {
      throw new NotFoundException("bien", row.getRowNum(), "id");
    }

    if (row.getCell(5) != null) {
      String idC = getRawStringValueConGuion(row, 5);
      String idConcat = limpiarConcatenado(idC);
      Expediente expediente = expedienteRepository.findByIdConcatenado(idConcat).orElseThrow(() ->
        new NotFoundException("Expediente", "IdConcatenado", idConcat));
      result.setIdExpediente(expediente.getId());
      idCartera = expediente.getCartera().getId();
      result.setCartera(idCartera);
      Usuario gestor = expediente.getGestor();
      if (gestor != null) result.setDestinatario(gestor.getId());
      else {
        Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");
        if (supervisor != null) result.setDestinatario(supervisor.getId());
        else throw new NotFoundException("expediente del bien", row.getRowNum(), "ni gestor ni supervisor.");
      }
    } else {
      throw new NotFoundException("bien", row.getRowNum(), "un expediente asociado.");
    }

    if (row.getCell(i + 1) != null) {
      Date fechaLlamada = getDateValueConGuion(row, i + 1);
      if (fechaLlamada == null) return null;
      Calendar c = Calendar.getInstance();
      c.setTime(fechaLlamada);

      if (row.getCell(i + 2) != null){
        Integer[] lista = getTimeValueConGuion(row, i+2);
        if (lista != null){
          c.set(Calendar.HOUR, lista[0]);
          c.set(Calendar.MINUTE, lista[1]);
          c.set(Calendar.SECOND, lista[2]);
          c.set(Calendar.MILLISECOND, lista[3]);
        }
      }
      Date d = c.getTime();

      result.setFechaCreacion(d);
      result.setFechaAlerta(d);
      result.setFechaLimite(d);
    } else {
      return null;
    }

    if (row.getCell(i + 3) != null) {
      Integer resultadoId = getIntegerValueConGuion(row, i + 3);
      if (resultadoId != null){
        String resultado = getResultados(resultadoId);
        ResultadoEvento re = resultadoEventoRepository.findByCodigo(resultado).orElseThrow(() ->
          new NotFoundException("ResultadoEvento", "Código", resultado));
        result.setResultado(re.getId());
      }
    }

    if (row.getCell(i + 4) != null) {
      Integer subtipoId = getIntegerValueConGuion(row, i + 4);
      if (subtipoId == null){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("No subtype found in row " + row.getRowNum());
        else throw new NotFoundException("No encontrado subtipo en la fila " + row.getRowNum());
      }
      String subtipo = getSubtipos(subtipoId);
      SubtipoEvento se = subtipoEventoRepository.findByCodigoAndCarteraId(subtipo, idCartera).orElseThrow(() ->
        new NotFoundException("SubtipoEvento", subtipo, "Cartera", idCartera));
      TipoEvento te = se.getTipoEvento();
      ClaseEvento ce = te.getClaseEvento();

      result.setClaseEvento(ce.getId());
      result.setTipoEvento(te.getId());
      result.setSubtipoEvento(se.getId());
    } else {
      throw new NotFoundException("interviniente", row.getRowNum(), "un tipo de Acción");
    }

    if (row.getCell(i + 5) != null) {
      String string = getRawStringValueConGuion(row, i + 5);
      if (string != null)
        descripcion += "Persona de Contacto: " + string + "\n";
    }
    if (row.getCell(i + 6) != null) {
      String string = getRawStringValueConGuion(row, i + 6);
      if (string != null)
        descripcion += "Relación con el Interviniente: " + string + "\n";
    }
    if (row.getCell(i + 7) != null) {
      String string = getRawStringValueConGuion(row, i + 7);
      if (string != null)
        descripcion += "Teléfono nuevo: " + string + "\n";
    }
    if (row.getCell(i + 8) != null) {
      String string = getRawStringValueConGuion(row, i + 8);
      if (string != null)
        descripcion += "Domicilio nuevo: " + string + "\n";
    }
    if (row.getCell(i + 9) != null) {
      String string = getRawStringValueConGuion(row, i + 9);
      if (string != null)
        descripcion += "Email nuevo: " + string + "\n";
    }
    if (row.getCell(i + 10) != null) {
      String string = getRawStringValueConGuion(row, i + 10);
      if (string != null)
        descripcion += "Mejor hora Contacto/Visita: " + string + "\n";
    }
    if (row.getCell(i + 11) != null) {
      String string = getRawStringValueConGuion(row, i + 11);
      if (string != null)
        descripcion += "Observaciones: " + string + "\n";
    }

    result.setComentariosDestinatario(descripcion);

    return result;
  }

  private void setElementosComunes(EventoInputDTO input){
    input.setTitulo("Actividad Delegada");
    input.setEstado(3); //Realizado
    input.setImportancia(2); //Media

    input.setProgramacion(false);
    input.setCorreo(false);
    input.setAutogenerado(true);
    input.setRevisado(false);
  }

  private Evento createEvento(EventoInputDTO input, Usuario emisor) throws RequiredValueException, com.haya.alaska.shared.exceptions.NotFoundException {
    Evento evento = new Evento();
    BeanUtils.copyProperties(input, evento, "revisado");

    if (input.getFechaCreacion() == null)
      evento.setFechaCreacion(Calendar.getInstance().getTime());
    else
      evento.setFechaCreacion(input.getFechaCreacion());

    evento.setComentariosDestinatario(input.getComentariosDestinatario());

    Integer idObjeto;
    idObjeto = input.getIdEventoPadre();
    evento.setEventoPadre(idObjeto != null ? eventoRepository.findById(idObjeto).orElse(null) : null);

    idObjeto = input.getClaseEvento();
    if (idObjeto == null)
      throw new RequiredValueException("Clase");
    evento.setClaseEvento(claseEventoRepository.findById(idObjeto).
      orElseThrow(() -> new NotFoundException("claseEvento", "id", String.valueOf(input.getClaseEvento()))));

    idObjeto = input.getCartera();
    if (idObjeto != null) evento.setCartera(carteraRepository.findById(idObjeto).orElse(null));

    idObjeto = input.getImportancia();
    if (idObjeto == null)
      throw new RequiredValueException("Importancia");
    evento.setImportancia(importanciaEventoRepository.findById(idObjeto).
      orElseThrow(() -> new NotFoundException("importanciaEvento", "id", String.valueOf(input.getImportancia()))));

    if (input.getEstado() == null) {
      evento.setEstado(estadoEventoRepository.findByCodigo("PEN").
        orElseThrow(() -> new NotFoundException("estadoEvento", "código", "PEN")));
    } else {
      idObjeto = input.getEstado();
      evento.setEstado(estadoEventoRepository.findById(idObjeto).
        orElseThrow(() -> new NotFoundException("estadoEvento",  "id", String.valueOf(input.getEstado()))));
    }

    if (!evento.getClaseEvento().getCodigo().equals("COM")) {
      idObjeto = input.getTipoEvento();
      if (idObjeto == null)
        throw new RequiredValueException("Tipo");
      evento.setTipoEvento(tipoEventoRepository.findById(idObjeto).
        orElseThrow(() -> new NotFoundException("tipoEvento", "id", String.valueOf(input.getTipoEvento()))));

      idObjeto = input.getSubtipoEvento();
      if (idObjeto == null)
        throw new RequiredValueException("Subtipo");
      SubtipoEvento subtipoEvento = subtipoEventoRepository.findById(idObjeto).
        orElseThrow(() -> new NotFoundException("subtipoEvento", "id", String.valueOf(input.getSubtipoEvento())));
      evento.setSubtipoEvento(subtipoEvento);

      if (input.getFechaLimite() != null) evento.setFechaLimite(input.getFechaLimite());
      if (input.getFechaCreacion() != null) evento.setFechaCreacion(input.getFechaCreacion());
      if (input.getFechaAlerta() != null) evento.setFechaAlerta(input.getFechaAlerta());

      switch (evento.getClaseEvento().getCodigo()) {
        case "ACC":
          if (input.getDestinatario() != null)
            evento.setDestinatario(usuarioRepository.findById(input.getDestinatario()).orElse(null));
          else evento.setDestinatario(emisor);

          if (input.getFechaLimite() == null) {
            Integer dias = subtipoEvento.getFechaLimite();
            if (dias != null) {
              Calendar c = Calendar.getInstance();
              c.setTime(evento.getFechaCreacion());
              c.add(Calendar.DATE, dias);
              evento.setFechaLimite(c.getTime());
            } else {
              evento.setFechaLimite(evento.getFechaCreacion());
              if (input.getFechaAlerta() == null){
                evento.setEstado(estadoEventoRepository.findByCodigo("REA").orElse(null));
              }
            }
          }
          if (input.getFechaAlerta() == null){
            evento.setFechaAlerta(evento.getFechaCreacion());
          }
          break;
        case "ALE":
          if (input.getDestinatario() == null)
            throw new RequiredValueException("Destinatario");
          evento.setDestinatario(usuarioRepository.findById(input.getDestinatario()).orElse(null));

          if (input.getFechaLimite() == null) {
            if (!evento.getImportancia().getCodigo().equals("ALT")){
              Integer dias = subtipoEvento.getFechaLimite();
              if (dias != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(evento.getFechaCreacion());
                c.add(Calendar.DATE, dias);
                evento.setFechaLimite(c.getTime());
              } else {
                Locale loc = LocaleContextHolder.getLocale();
                if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                  throw new RequiredValueException("The field 'LimitDate' is required for alerts with low or medium importance.");
                else throw new RequiredValueException("El campo 'FechaLimite' es obligatorio para alertas con importancia baja o media.");
              }
            }
          }
          if (input.getFechaAlerta() == null) throw new RequiredValueException("FechaAlerta");
          break;
        case "ACT":
          if (input.getDestinatario() != null)
            evento.setDestinatario(usuarioRepository.findById(input.getDestinatario()).orElse(null));
          else evento.setDestinatario(emisor);

          if (input.getFechaCreacion() != null) {
            evento.setFechaCreacion(input.getFechaCreacion());
            if (input.getFechaCreacion().compareTo(Calendar.getInstance().getTime()) < 0)
              evento.setEstado(estadoEventoRepository.findByCodigo("REA").orElse(null));
          }
          if (input.getFechaLimite() == null) {
            Integer dias = subtipoEvento.getFechaLimite();
            if (dias != null) {
              Calendar c = Calendar.getInstance();
              c.setTime(evento.getFechaCreacion());
              c.add(Calendar.DATE, dias);
              evento.setFechaLimite(c.getTime());
            } else {
              evento.setFechaLimite(evento.getFechaCreacion());
              if (input.getFechaAlerta() == null){
                evento.setEstado(estadoEventoRepository.findByCodigo("REA").orElse(null));
              }
            }
          }
          if (input.getFechaAlerta() == null){
            evento.setFechaAlerta(evento.getFechaCreacion());
          }
          break;
      }
    } else {
      evento.setTipoEvento(tipoEventoRepository.findByCodigo("COM_01").
        orElseThrow(() -> new NotFoundException("tipoEvento", "código", "COM_01")));

      evento.setSubtipoEvento(subtipoEventoRepository.findByCodigoAndCarteraId("COMU01", input.getCartera()).
        orElseThrow(() -> new NotFoundException("subtipoEvento", "código", "COMU01")));

      if (input.getDestinatario() == null)
        throw new RequiredValueException("Destinatario");
      evento.setDestinatario(usuarioRepository.findById(input.getDestinatario()).orElse(null));

      evento.setFechaAlerta(new Date());
      evento.setFechaCreacion(new Date());
      evento.setFechaLimite(new Date());
    }

    if (input.getCorreo() == null)
      evento.setCorreo(evento.getSubtipoEvento().getCorreo());
    if (input.getProgramacion() == null)
      evento.setProgramacion(evento.getSubtipoEvento().getProgramacion());

    idObjeto = input.getPeriodicidad();
    evento.setPeriodicidad(idObjeto != null ? periodicidadEventoRepository.findById(idObjeto).
      orElseThrow(() -> new NotFoundException("periodicidadEvento", "id",String.valueOf(input.getPeriodicidad()))) : evento.getSubtipoEvento().getPeriodicidad());

    if (input.getRevisado() != null && input.getRevisado()) {
      evento.setEstado(estadoEventoRepository.findByCodigo("PRO").orElse(null));
    }
    idObjeto = input.getResultado();
    evento.setResultado(idObjeto != null ? resultadoEventoRepository.findById(idObjeto).
      orElseThrow(() -> new NotFoundException("resultadoEvento", "id", String.valueOf(input.getResultado()))) : null);
    if (input.getResultado() != null) {
      evento.setEstado(estadoEventoRepository.findByCodigo("REA").orElse(null));
    }

    evento.setEmisor(emisor);

    if (input.getNivel() == null && input.getNivelId() == null){
      evento.setNivel(nivelEventoRepository.findByCodigo("GEN").
        orElseThrow(() -> new NotFoundException("nivelEvento", "código", "'GEN'")));
      evento.setIdNivel(0);
    } else {
      idObjeto = input.getNivel();
      if (idObjeto == null)
        throw new RequiredValueException("Nivel");
      evento.setNivel(nivelEventoRepository.findById(idObjeto).
        orElseThrow(() -> new NotFoundException("nivelEvento", "id", String.valueOf(input.getImportancia()))));

      idObjeto = input.getNivelId();
      if (idObjeto == null)
        throw new RequiredValueException("Nivel Id");
      evento.setIdNivel(idObjeto);
    }

    return evento;
  }

  private String limpiarConcatenado(String s){
    String result = s;

    if (result.contains("E")){
      String[] separados = result.split("E");
      result = separados[0];
    }
    if (result.contains(".")){
      result = result.replace(".", "");
    }

    return result;
  }

  public ListWithCountDTO<ExpedienteActividadDelegadaOutputDTO> getAllExpedientes(
    BusquedaDto busqueda, Usuario user, String idConcatenado, String estado, String cliente, String cartera,
    String primerTitular, Integer nContratos, Integer nIntervinientes, Integer nBienes, Double saldoGestion,
    String estrategia, Boolean telefon1erTitular, Boolean telefonRestoIntervinientes, Boolean direccion1erTitular,
    Boolean direccionRestoIntervinientes, String orderField, String orderDirection, Integer page, Integer size
  ) throws com.haya.alaska.shared.exceptions.NotFoundException {
    List<Expediente> expedientes = filtrados(
      busqueda, user, idConcatenado, estado, cliente, cartera, primerTitular, nContratos, nIntervinientes, nBienes,
      saldoGestion, estrategia, telefon1erTitular, telefonRestoIntervinientes, direccion1erTitular,
      direccionRestoIntervinientes);
    if (orderField != null && getNoFiltrados().contains(orderField))
      expedientes = ordenado(expedientes, orderField, orderDirection);
    List<Expediente> expedientesPaginados = expedientes.stream().skip(size * page).limit(size).collect(Collectors.toList());
    List<ExpedienteActividadDelegadaOutputDTO> result = expedientesPaginados.stream().map(
      expedienteMapper::parseActivDeleg).collect(Collectors.toList());
    return new ListWithCountDTO<ExpedienteActividadDelegadaOutputDTO>(result, expedientes.size());
  }

  private List<Expediente> filtrados(
    BusquedaDto busqueda, Usuario user, String idConcatenado, String estado, String cliente, String cartera,
    String primerTitular, Integer nContratos, Integer nIntervinientes, Integer nBienes, Double saldoGestion,
    String estrategia, Boolean telefon1erTitular, Boolean telefonRestoIntervinientes, Boolean direccion1erTitular,
    Boolean direccionRestoIntervinientes) throws com.haya.alaska.shared.exceptions.NotFoundException {
    List<Expediente> expedientes = busqueda(busqueda, user, idConcatenado, cliente, cartera, primerTitular, estrategia, null, null);
    List<Expediente> expedientesFiltados = expedientes.stream().filter(ex -> {
      if (estado != null) if(!expedienteUtil.getEstado(ex).getValor().contains(estado)) return false;
      if (nContratos != null){if(!(ex.getContratos().size() == nContratos)) return false;}
      if (nIntervinientes != null){
        Integer i = 0;
        for (Contrato c : ex.getContratos()){
          i += c.getIntervinientes().size();
        }
        if(!(i.equals(nIntervinientes))) return false;
      }
      if (nBienes != null){
        Integer i = 0;
        for (Contrato c : ex.getContratos()){
          i += c.getBienes().size();
        }
        if(!(i.equals(nBienes))) return false;
      }
      if (saldoGestion != null) if (!(ex.getSaldoGestion() == saldoGestion)) return false;
      /*if (telefon1erTitular != null || telefonRestoIntervinientes != null || direccion1erTitular != null || direccionRestoIntervinientes != null) {
        Boolean b = filtrarBooleans(ex, telefon1erTitular, telefonRestoIntervinientes, direccion1erTitular, direccionRestoIntervinientes);
        if (!b) return false;
      }*/

      return true;
    }).collect(Collectors.toList());
    return expedientesFiltados;
  }

  private List<Expediente> busqueda(
    BusquedaDto busqueda, Usuario user, String idConcatenado, String cliente, String cartera,
    String primerTitular, String estrategia, String orderField, String orderDirection) throws com.haya.alaska.shared.exceptions.NotFoundException {
    BusquedaExpedienteDto busquedaExpedienteDto = busqueda.getExpediente();
    BusquedaBuilder builder = busquedaUseCase.generateBuilder(busqueda, user, null, null);

    CriteriaBuilder cb = builder.getCb();
    Root<Expediente> root = builder.getRoot();
    List<Predicate> predicates = builder.getPredicates();

    if (idConcatenado != null)
      predicates.add(cb.like(root.get(Expediente_.ID_CONCATENADO), "%" + idConcatenado + "%"));

    if (cliente != null){
      Join<ClienteCartera, Cliente> joinCliente = builder.getJoinCliente();
      predicates.add(cb.like(joinCliente.get(Cliente_.nombre), "%" + cliente + "%"));
    }
    if (cartera != null){
      Join<Expediente, Cartera> joinCartera = builder.getJoinCartera();
      predicates.add(cb.like(joinCartera.get(Cartera_.nombre), "%" + cartera + "%"));
    }
    if (primerTitular != null){
      Join<Contrato, ContratoInterviniente> joinContratoInterviniente = builder.getJoinPrimerInterviniente();
      Join<ContratoInterviniente, Interviniente> joinInterviniente = joinContratoInterviniente.join(ContratoInterviniente_.INTERVINIENTE, JoinType.INNER);
      predicates.add(cb.like(joinInterviniente.get(Interviniente_.nombre), "%" + primerTitular + "%"));
    }
    if (estrategia != null) {
      Join<Expediente, Contrato> joinContratoRepresentante = builder.getJoinContratoRepresentante();
      Join<Contrato, EstrategiaAsignada> join1 = joinContratoRepresentante.join(Contrato_.ESTRATEGIA, JoinType.INNER);
      predicates.add(cb.like(join1.get(EstrategiaAsignada_.ESTRATEGIA).get(Estrategia_.VALOR), "%" + estrategia + "%"));
    }
    if (orderField != null){
      if (!getNoFiltrados().contains(orderField)){
        ordenado(builder, orderField, orderDirection);
      }
    }

    CriteriaQuery<Expediente> query = builder.createQuery().distinct(true);
    List<Expediente> result = entityManager.createQuery(query).getResultList();
    if (busquedaExpedienteDto != null && busquedaExpedienteDto.getSaldoGestion() != null) {
      IntervaloImporte ii = busquedaExpedienteDto.getSaldoGestion();
      result = result.stream().filter(expediente -> {
        var saldo = expediente.getSaldoGestion();
        if (ii.getIgual() != null && saldo == ii.getIgual()) {
          return true;
        } else if (ii.getMayor() != null && ii.getMenor() != null) {
          if (saldo > ii.getMayor() && saldo < ii.getMenor())
            return true;
        } else {
          if (ii.getMayor() != null && saldo > ii.getMayor())
            return true;
          if (ii.getMenor() != null && saldo < ii.getMenor())
            return true;
        }
        return false;
      }).collect(Collectors.toList());
    }
    return result;
  }

  private List<String> getNoFiltrados(){
    List<String> result = new ArrayList<>();

    result.add("estado");
    result.add("nContratos");
    result.add("nIntervinientes");
    result.add("nBienes");
    result.add("saldoGestion");
    result.add("telefon1erTitular");
    result.add("telefonRestoIntervinientes");
    result.add("direccion1erTitular");
    result.add("direccionRestoIntervinientes");

    return result;
  }

  private Boolean filtrarBooleans(Expediente e, Boolean telefon1erTitular, Boolean telefonRestoIntervinientes,
                                  Boolean direccion1erTitular, Boolean direccionRestoIntervinientes){
    Contrato contratoRepresentante = e.getContratoRepresentante();
    Interviniente primerInterviniente = contratoRepresentante.getPrimerInterviniente();
    if (telefon1erTitular != null){
      if (telefon1erTitular){
        Boolean comprobar = false;
        for (DatoContacto dc : primerInterviniente.getDatosContacto()){
          if (dc.getMovil() != null || dc.getFijo() != null){
            comprobar = true;
            break;
          }
        }
        if (!comprobar) return false;
      } else {
        for (DatoContacto dc : primerInterviniente.getDatosContacto()){
          if (dc.getMovil() != null || dc.getFijo() != null){
            return false;
          }
        }
      }
    }
    if (direccion1erTitular != null){
      if (direccion1erTitular){
        if (primerInterviniente.getDirecciones().isEmpty()) return false;
      } else {
        if (!primerInterviniente.getDirecciones().isEmpty()) return false;
      }
    }

    if (telefonRestoIntervinientes != null || direccionRestoIntervinientes != null){
      Boolean t = false;
      Boolean d = false;
      for (Contrato c : e.getContratos()){
        for (ContratoInterviniente ci : c.getIntervinientes()){
          Interviniente i = ci.getInterviniente();
          if (telefonRestoIntervinientes != null){
            for (DatoContacto dc : i.getDatosContacto()){
              if (dc.getMovil() != null || dc.getFijo() != null){
                t = true;
                break;
              }
            }
          }
          if (direccionRestoIntervinientes != null){
            if (!primerInterviniente.getDirecciones().isEmpty()){
              d = true;
              break;
            }
          }
        }
        if (telefonRestoIntervinientes != null && direccionRestoIntervinientes == null && t) break;
        if (telefonRestoIntervinientes == null && direccionRestoIntervinientes != null && d) break;
        if (telefonRestoIntervinientes != null && direccionRestoIntervinientes != null && t && d) break;
      }
      if (telefonRestoIntervinientes != null){
        if (telefonRestoIntervinientes != t) return false;
      }
      if (direccionRestoIntervinientes != null){
        if (direccionRestoIntervinientes != d) return false;
      }
    }

    return true;
  }

  private void ordenado(BusquedaBuilder builder, String orderField, String orderDirection){
    CriteriaBuilder cb = builder.getCb();
    CriteriaQuery<Expediente> query = builder.getQuery();
    Root<Expediente> root = builder.getRoot();
    List<Predicate> predicates = builder.getPredicates();
    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    if (orderField != null) {
      switch (orderField) {
        case "cliente":
          Join<ClienteCartera, Cliente> clJoin = builder.getJoinCliente();
          rs.orderBy(getOrden(cb, clJoin.get(Cliente_.nombre), orderDirection));
          break;
        case "cartera":
          Join<Expediente, Cartera> caJoin = builder.getJoinCartera();
          rs.orderBy(getOrden(cb, caJoin.get(Cartera_.nombre), orderDirection));
          break;
        case "idConcatenado":
          rs.orderBy(getOrden(cb, root.get(Expediente_.idConcatenado), orderDirection));
          break;
        case "estrategia":
          Join<Expediente, Contrato> joinContratoRepresentante = builder.getJoinContratoRepresentante();
          Join<Contrato, EstrategiaAsignada> join1 = joinContratoRepresentante.join(Contrato_.ESTRATEGIA, JoinType.INNER);
          rs.orderBy(getOrden(cb, join1.get(EstrategiaAsignada_.ESTRATEGIA).get(Estrategia_.VALOR), orderDirection));
          break;
        case "primerTitular":
          Join<Contrato, ContratoInterviniente> joinContratoInterviniente = builder.getJoinPrimerInterviniente();
          Join<ContratoInterviniente, Interviniente> joinInterviniente = joinContratoInterviniente.join(ContratoInterviniente_.INTERVINIENTE, JoinType.INNER);
          rs.orderBy(getOrden(cb, joinInterviniente.get(Interviniente_.NOMBRE), orderDirection));
          break;
      }
    }
  }

  private List<Expediente> ordenado(List<Expediente> expedientes, String orderField, String orderDirection){
    List expedientesFiltered = expedientes;
    Comparator<Expediente> comparator = (var o1, var o2) -> StringUtils.compareIgnoreCase(o1.getIdConcatenado(), o2.getIdConcatenado());
    switch (orderField){
      case "estado":
        comparator = (var o1, var o2) -> StringUtils.compareIgnoreCase(expedienteUtil.getEstado(o1).getValor(), expedienteUtil.getEstado(o2).getValor());
        break;
      case "nContratos":
        comparator = Comparator.comparingInt((Expediente o) -> o.getContratos().size());
        break;
      case "nBienes":
        comparator = Comparator.comparingInt((Expediente o) -> expedienteUtil.nBienes(o));
        break;
      case "nIntervinientes":
        comparator = Comparator.comparingInt((Expediente o) -> expedienteUtil.nIntervinientes(o));
        break;
    }
    if (orderDirection != null && orderDirection.equals("desc") && orderField.equals("contratoExpediente"))
      expedientesFiltered = expedientes.stream().sorted(comparator).collect(Collectors.toList());

    return expedientesFiltered;
  }

  private Order getOrden(CriteriaBuilder cb, Expression expresion, String orderDirection) {
    if (orderDirection == null) {
      orderDirection = "desc";
    }
    return orderDirection.toLowerCase().equalsIgnoreCase("desc") ? cb.desc(expresion) : cb.asc(expresion);
  }

  public ByteArrayInputStream create(LinkedHashMap<String, List<Collection<?>>> datos) throws IOException {
    Workbook workbook = new XSSFWorkbook();

    CellStyle cellStyleDate = workbook.createCellStyle();
    cellStyleDate.setDataFormat((short)14);


    CellStyle styleHeader = workbook.createCellStyle();
    Font font = workbook.createFont();
    font.setBold(true);
    font.setColor(IndexedColors.WHITE.getIndex());
    styleHeader.setFont(font);
    //styleHeader.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
    byte[] rgb = new byte[3];
    rgb[0] = (byte) 10; // red
    rgb[1] = (byte) 148; // green
    rgb[2] = (byte) 214; // blue
    //create XSSFColor
    XSSFColor color = new XSSFColor(rgb, new DefaultIndexedColorMap());
    XSSFCellStyle xssfcellcolorstyle = (XSSFCellStyle) styleHeader;
    xssfcellcolorstyle.setFillForegroundColor(color);
    xssfcellcolorstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

    for (var datosHoja : datos.entrySet()) {
      Sheet sheet = workbook.createSheet(datosHoja.getKey());
      int indiceFila = 0;
      for (Collection<?> datosFila : datosHoja.getValue()) {
        Row filaExcel = sheet.createRow(indiceFila);
        int indiceColumna = 0;
        for (Object dato : datosFila) {
          Cell celdaExcel = filaExcel.createCell(indiceColumna);
          if (indiceFila == 0 || (indiceFila == 1 && (sheet.getSheetName().equals("Intervinientes") || sheet.getSheetName().equals("Colaterales")))) {
            celdaExcel.setCellStyle(xssfcellcolorstyle);
            sheet.setColumnWidth(indiceColumna, 15 * 256);
          }
          if (dato != null) {
            if (dato instanceof Date) {
              celdaExcel.setCellStyle(cellStyleDate);
              celdaExcel.setCellValue((Date) dato);
            } else if (dato instanceof Boolean) {
              if (dato == Boolean.TRUE) {
                celdaExcel.setCellValue("Sí");
              } else if (dato == Boolean.FALSE) {
                celdaExcel.setCellValue("No");
              }
            } else if (dato instanceof Number) {
              celdaExcel.setCellValue(((Number) dato).doubleValue());
            } else if (dato instanceof Catalogo) {
              celdaExcel.setCellValue(((Catalogo) dato).getValor());
            } else if (dato.equals("")) {
              celdaExcel.setCellValue("-");
            } else {
              celdaExcel.setCellValue(dato.toString());
            }
          } else {
            celdaExcel.setCellValue("-");
          }
          indiceColumna++;
        }
        indiceFila++;
      }
    }

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      workbook.write(outputStream);
      return new ByteArrayInputStream(outputStream.toByteArray());
    }
  }
}
