package com.haya.alaska.actividad_delegada.infrastructure.controller;

import com.haya.alaska.actividad_delegada.aplication.ActividadDelegadaUseCase;
import com.haya.alaska.actividad_delegada.infrastructure.controller.dto.ExpedienteActividadDelegadaOutputDTO;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.skip_tracing.infrastructure.controller.dto.CargaSTInputDTO;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/actividadDelegada")
public class ActividadDelegadaController {

  @Autowired
  ActividadDelegadaUseCase actividadDelegadaUseCase;

  @ApiOperation(value = "Descargar Informe de actividad delegada en Excel",
    notes = "Descargar información en Excel de loos expedientes indicados")
  @PostMapping("/descarga")
  public ResponseEntity<InputStreamResource> descarga(
    @RequestBody(required = false) BusquedaDto busquedaDto,
    @RequestParam(value = "telefon1erTitular") Boolean telefon1erTitular,
    @RequestParam(value = "telefonRestoIntervinientes") Boolean telefonRestoIntervinientes,
    @RequestParam(value = "direccion1erTitular") Boolean direccion1erTitular,
    @RequestParam(value = "direccionRestoIntervinientes") Boolean direccionRestoIntervinientes,
    @RequestParam(required = false) List<Integer> expedientes,
    @RequestParam Boolean todos,
    @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = actividadDelegadaUseCase.create(actividadDelegadaUseCase.generateExcel(
      busquedaDto, usuario, telefon1erTitular, telefonRestoIntervinientes, direccion1erTitular,
      direccionRestoIntervinientes, expedientes, todos));

    return excelExport.download(excel, "Actividad_Delegada_" + new SimpleDateFormat("ddMMyyyy").format(new Date()));
  }

  @ApiOperation(value = "Importa los datos",
    notes = "Descargar información en Excel de loos expedientes indicados")
  @PostMapping("/carga")
  public void carga(@RequestParam("file") MultipartFile file,
                    @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    actividadDelegadaUseCase.carga(file, usuario);
  }

  @ApiOperation(value = "Get expedientes",
    notes = "Consigue los expedientes para las Actividades Delegadas")
  @PostMapping
  public ListWithCountDTO<ExpedienteActividadDelegadaOutputDTO> get(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "id", required = false) String id,
    @RequestParam(value = "estado", required = false) String estado,
    @RequestParam(value = "cliente", required = false) String cliente,
    @RequestParam(value = "cartera", required = false) String cartera,
    @RequestParam(value = "primerTitular", required = false) String primerTitular,
    @RequestParam(value = "nContratos", required = false) Integer nContratos,
    @RequestParam(value = "nIntervinientes", required = false) Integer nIntervinientes,
    @RequestParam(value = "nBienes", required = false) Integer nBienes,
    @RequestParam(value = "saldoGestion", required = false) Double saldoGestion,
    @RequestParam(value = "estrategia", required = false) String estrategia,
    @RequestParam(value = "telefon1erTitular", required = false) Boolean telefon1erTitular,
    @RequestParam(value = "telefonRestoIntervinientes", required = false) Boolean telefonRestoIntervinientes,
    @RequestParam(value = "direccion1erTitular", required = false) Boolean direccion1erTitular,
    @RequestParam(value = "direccionRestoIntervinientes", required = false) Boolean direccionRestoIntervinientes,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return actividadDelegadaUseCase.getAllExpedientes(
      busquedaDto, usuario, id, estado, cliente, cartera, primerTitular, nContratos, nIntervinientes, nBienes,
      saldoGestion, estrategia, telefon1erTitular, telefonRestoIntervinientes, direccion1erTitular,
      direccionRestoIntervinientes, orderField, orderDirection, page, size);
  }
}
