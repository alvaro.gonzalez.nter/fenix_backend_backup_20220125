package com.haya.alaska.actividad_delegada.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ExpedienteActividadDelegadaOutputDTO {
  private Integer id;
  private String idConcatenado;
  private CatalogoMinInfoDTO estado;
  private String cliente;
  private String cartera;
  private String primerTitular;
  private Integer nContratos;
  private Integer nIntervinientes;
  private Integer nBienes;
  private Double saldoGestion;
  private CatalogoMinInfoDTO estrategia;
  private Boolean telefon1erTitular;
  private Boolean telefonRestoIntervinientes;
  private Boolean direccion1erTitular;
  private Boolean direccionRestoIntervinientes;
}
