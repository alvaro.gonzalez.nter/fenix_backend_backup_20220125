package com.haya.alaska.importe_estimacion_bien_pos_negociada.infrastructure.repository;

import com.haya.alaska.importe_estimacion_bien_pos_negociada.domain.ImporteEstimacionBienPosesionNegociada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ImporteEstimacionBienPosesionNegociadaRepository extends JpaRepository<ImporteEstimacionBienPosesionNegociada, Integer> {
  @Modifying
  @Query("delete from ImporteEstimacionBienPosesionNegociada t where t.id = ?1")
  void deleteById(Integer entityId);
}
