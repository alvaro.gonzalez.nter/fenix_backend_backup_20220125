package com.haya.alaska.importe_estimacion_bien_pos_negociada.domain;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.estimacion.domain.Estimacion;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_RELA_IMPORTE_ESTIMACION_BIEN_POS_NEGOCIADA")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "RELA_IMPORTE_ESTIMACION_BIEN_POS_NEGOCIADA")
public class ImporteEstimacionBienPosesionNegociada implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "ID_ESTIMACION", nullable = false)
  private Estimacion estimacion;

  @ManyToOne
  @JoinColumn(name = "ID_BIEN_POS_NEGOCIADA", nullable = false)
  private BienPosesionNegociada bienPosesionNegociada;

  @Column(name = "NUM_IMPORTE_TOTAL", columnDefinition = "decimal(15,2)")
  private Double importeTotal;

  @Column(name = "NUM_IMPORTE_TOTAL_SALIDA", columnDefinition = "decimal(15,2)")
  private Double importeTotalSalida;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

}
