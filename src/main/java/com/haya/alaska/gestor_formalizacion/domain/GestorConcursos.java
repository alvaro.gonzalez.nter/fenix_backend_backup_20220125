package com.haya.alaska.gestor_formalizacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@AuditTable(value = "HIST_LKUP_GESTOR_CONCURSOS")
@NoArgsConstructor
@Entity
@Table(name = "LKUP_GESTOR_CONCURSOS")
public class GestorConcursos extends Catalogo {
}
