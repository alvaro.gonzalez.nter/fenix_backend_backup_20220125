package com.haya.alaska.gestor_formalizacion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.gestor_formalizacion.domain.GestorConcursos;
import org.springframework.stereotype.Repository;

@Repository
public interface GestorConcursosRepository extends CatalogoRepository<GestorConcursos, Integer> {}
