package com.haya.alaska.gestor_formalizacion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.gestor_formalizacion.domain.GestorConcursos;
import com.haya.alaska.gestor_formalizacion.domain.GestorGestoria;
import org.springframework.stereotype.Repository;

@Repository
public interface GestorGestoriaRepository extends CatalogoRepository<GestorGestoria, Integer> {}
