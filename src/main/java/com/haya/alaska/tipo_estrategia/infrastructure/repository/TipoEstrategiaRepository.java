package com.haya.alaska.tipo_estrategia.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_estrategia.domain.TipoEstrategia;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoEstrategiaRepository extends CatalogoRepository<TipoEstrategia, Integer> {
}
