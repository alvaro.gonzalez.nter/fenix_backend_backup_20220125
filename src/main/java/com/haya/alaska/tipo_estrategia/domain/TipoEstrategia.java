package com.haya.alaska.tipo_estrategia.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_ESTRATEGIA")
@Entity
@Getter
@Table(name = "LKUP_TIPO_ESTRATEGIA")
public class TipoEstrategia extends Catalogo {
  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ESTRATEGIA")
  @NotAudited
  private Set<EstrategiaAsignada> estrategiasAsignadas = new HashSet<>();
}
