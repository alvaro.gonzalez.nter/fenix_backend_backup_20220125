package com.haya.alaska.busqueda.application;

import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDto;
import com.haya.alaska.busqueda.domain.BusquedaBuilder;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaGuardadaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaGuardadaInputDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaRecienteDto;
import com.haya.alaska.campania.infraestructure.controller.dto.BusquedaCampaniaOutputDTO;
import com.haya.alaska.campania.infraestructure.controller.dto.BusquedaCampaniaPNOutputDTO;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoBusquedaDto;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTOToList;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output.FormalizacionBusquedaOutputDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteBusquedaDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteBusquedaDTOToList;
import com.haya.alaska.shared.dto.BienExtendedDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.skip_tracing.infrastructure.controller.dto.SkipTracingOutputDTO;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface BusquedaUseCase {

  ListWithCountDTO<ExpedienteDTOToList> getAllFilteredExpedientes(
    BusquedaDto busquedaDto, Usuario loggedUser, String id, String estado, String modoGestion, String situacion,
    String cliente, String cartera, String titular, String saldoEnGestion, String estrategia, String supervisor,
    String areaDeGestion, String gestor, String gestorSuplente, String orderField, String orderDirection, Integer size,
    Integer page);

  ListWithCountDTO<FormalizacionBusquedaOutputDTO> getBusquedaFormalizacion(
    BusquedaDto busquedaDto, Usuario loggedUser, String id, String titular, String nif, String estado,
    String detalleEstado, String fechaFirma, String horaFirma, String formalizacionHaya, String gestorHaya,
    String formalizacionCliente, String fechaSancion, String importeDacion, String importeDeuda, String provincia,
    String alquiler, String orderField, String orderDirection, Integer size, Integer page);

  LinkedHashMap<String, List<Collection<?>>> getAllFilteredExpedientesExcel(
      BusquedaDto busquedaDto, Usuario usuario) throws Exception;

  List<Expediente> getDataExpedientesFilter(
      BusquedaDto busquedaDto, Usuario loggedUser, String orderField, String orderDirection)
      throws NotFoundException;

  BusquedaBuilder generateBuilder(
      BusquedaDto busquedaDto, Usuario loggedUser, String orderField, String orderDirection)
      throws NotFoundException;

  List<BusquedaRecienteDto> getRecientes(Usuario usuario);

  List<BusquedaGuardadaDto> getGuardadas(Usuario usuario);

  ListWithCountDTO<ExpedienteDTOToList> getGuardada(
      Usuario usuario, int id, Integer size, Integer page) throws NotFoundException;

  BusquedaGuardadaDto crearGuardada(
      Usuario usuario, BusquedaGuardadaInputDto busquedaGuardadaInputDto) throws NotFoundException;

  void eliminarGuardada(Usuario usuario, int id) throws NotFoundException;

  ListWithCountDTO<ContratoBusquedaDto> getAllFilteredContratos(
      BusquedaDto busquedaDto,
      Usuario usuario,
      Integer size,
      Integer page,
      String id,
      String idOrigen,
      String cartera,
      String activo,
      String origenEstrategia,
      String fecha,
      String periodo,
      String tipo,
      String estrategia,
      String orderField,
      String orderDirection);

  ListWithCountDTO<BienPosesionNegociadaDto> getAllFilteredBienesPN(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      Integer size,
      Integer page,
      String orderField,
      String orderDirection)
      throws NotFoundException;

  ListWithCountDTO<BienExtendedDTOToList> getAllFilteredBienesRA(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page);

  ListWithCountDTO<BienExtendedDTOToList> getAllFilteredBienesPN(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page);

  ListWithCountDTO<ExpedienteDTOToList> buscarExpedientesPorExcel(
      MultipartFile file, Usuario principal) throws IOException;

  ListWithCountDTO<ExpedientePosesionNegociadaDTO> buscarExpedientesPNPorExcel(
      MultipartFile file, Usuario principal) throws IOException;

  ListWithCountDTO<SkipTracingOutputDTO> buscarSkipTracingsPorExcel(
      MultipartFile file, Usuario usuario)
      throws IOException, IllegalAccessException, NotFoundException, InvocationTargetException;

  List<IntervinienteBusquedaDTOToList> buscarIntervinientesPorExcel(
      MultipartFile file,  String tipo, Usuario principal) throws IOException, NotFoundException;

  List<OcupanteBusquedaDTOToList> buscarOcupantesPorExcel(MultipartFile file, String tipo, Usuario principal)
      throws IOException, NotFoundException;

  ListWithCountDTO<BusquedaCampaniaOutputDTO> getAllFilteredCampania(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      Integer size,
      Integer page,
      String idExpediente,
      String idContrato,
      String producto,
      String estado,
      String cliente,
      String cartera,
      String titular,
      String saldoGestion,
      String estrategia,
      String supervisor,
      String area,
      String gestor,
      String campania,
      String idCarga,
      String orderField,
      String orderDirection);

  ListWithCountDTO<IntervinienteBusquedaDTOToList> getAllFilteredIntervinientes(
      String tipo,
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      Boolean sacarProcedimientos,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page,
      boolean firma)
      throws NotFoundException;

  ListWithCountDTO<OcupanteBusquedaDTOToList> getAllFilteredOcupantesGestionMasiva(
      String tipo,
      BusquedaDto busquedaDto,
      Usuario usuario,
      Boolean sacarProcedimientos,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page)
      throws NotFoundException;

  ListWithCountDTO<BusquedaCampaniaPNOutputDTO> getAllFilteredCampaniaPN(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      Integer size,
      Integer page,
      String idConcatenado,
      String idBienPN,
      String estado,
      String cliente,
      String cartera,
      String saldoGestion,
      String estrategia,
      String supervisor,
      String area,
      String gestor,
      String campania,
      String orderField,
      String orderDirection)
      throws NotFoundException;
}
