package com.haya.alaska.busqueda.application;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDto;
import com.haya.alaska.bien_posesion_negociada.infrastructure.mapper.BienPosesionNegociadaMapper;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.busqueda.domain.BusquedaBuilder;
import com.haya.alaska.busqueda.domain.BusquedaBuilderInterviniente;
import com.haya.alaska.busqueda.domain.BusquedaGuardada;
import com.haya.alaska.busqueda.domain.BusquedaReciente;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaGuardadaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaGuardadaInputDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaRecienteDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.*;
import com.haya.alaska.busqueda.infrastructure.mapper.BusquedaMapper;
import com.haya.alaska.busqueda.infrastructure.repository.BusquedaGuardadaRepository;
import com.haya.alaska.busqueda.infrastructure.repository.BusquedaRecienteRepository;
import com.haya.alaska.campania.infraestructure.controller.dto.BusquedaCampaniaOutputDTO;
import com.haya.alaska.campania.infraestructure.controller.dto.BusquedaCampaniaPNOutputDTO;
import com.haya.alaska.campania.infraestructure.repository.CampaniaRepository;
import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.campania_asignada.infraestructure.repository.CampaniaAsignadaRepository;
import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoBusquedaDto;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.application.ExpedienteExcelUseCase;
import com.haya.alaska.expediente.application.ExpedienteUseCase;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteCSVInputDTO;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTOToList;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteExcelExportDTO;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCase;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.mapper.ExpedientePosesionNegociadaMapper;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output.FormalizacionBusquedaOutputDTO;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.hito_procedimiento.infrastructure.repository.HitoProcedimientoRepository;
import com.haya.alaska.interviniente.application.IntervinienteUseCaseImpl;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteBusquedaDTOToList;
import com.haya.alaska.interviniente.infrastructure.mapper.IntervinienteMapper;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.ocupante.application.OcupanteUseCaseImpl;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteBusquedaDTOToList;
import com.haya.alaska.ocupante.infrastructure.mapper.OcupanteMapper;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.ProcedimientoDTOToList;
import com.haya.alaska.procedimiento.infrastructure.mapper.ProcedimientoMapper;
import com.haya.alaska.procedimiento.infrastructure.repository.ProcedimientoRepository;
import com.haya.alaska.procedimiento_posesion_negociada.application.ProcedimientoPosesionNegociadaUseCase;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaDTOToList;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.mapper.ProcedimientoPosesionNegociadaMapper;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.repository.ProcedimientoPosesionNegociadaRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.shared.dto.BienExtendedDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.skip_tracing.infrastructure.controller.dto.SkipTracingOutputDTO;
import com.haya.alaska.skip_tracing.infrastructure.mapper.SkipTracingMapper;
import com.haya.alaska.skip_tracing.infrastructure.repository.SkipTracingRepository;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.DateUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.NoSuchFileException;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class BusquedaUseCaseImpl implements BusquedaUseCase {

  @Autowired ExpedienteUtil expedienteUtil;
  @Autowired private ExpedienteMapper expedienteMapper;
  @Autowired private ExpedientePosesionNegociadaMapper expedientePNMapper;
  @Autowired private TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired private HitoProcedimientoRepository hitoProcedimientoRepository;
  @PersistenceContext private EntityManager entityManager;
  @Autowired private BusquedaMapper busquedaMapper;
  @Autowired private BusquedaRecienteRepository busquedaRecienteRepository;
  @Autowired private BusquedaGuardadaRepository busquedaGuardadaRepository;
  @Autowired private ExpedienteUseCase expedienteUseCase;
  @Autowired private ExpedienteExcelUseCase expedienteExcelUseCase;
  @Autowired private EventoUtil eventoUtil;
  @Autowired private ExpedienteRepository expedienteRepository;
  @Autowired private ExpedientePosesionNegociadaRepository expedientePNRepository;
  @Autowired private SkipTracingRepository skipTracingRepository;
  @Autowired private SkipTracingMapper skipTracingMapper;
  @Autowired private ContratoRepository contratoRepository;
  @Autowired private BienRepository bienRepository;
  @Autowired private IntervinienteRepository intervinienteRepository;
  @Autowired private ProcedimientoRepository procedimientoRepository;
  @Autowired private PropuestaRepository propuestaRepository;
  @Autowired private BienPosesionNegociadaMapper bienPosesionNegociadaMapper;
  @Autowired private ExpedientePosesionNegociadaMapper expedientePosesionNegociadaMapper;
  @Autowired @Lazy private ExpedientePosesionNegociadaCase expedientePosesionNegociadaCase;
  @Autowired private CampaniaAsignadaRepository campaniaAsignadaRepository;
  @Autowired private IntervinienteMapper intervinienteMapper;
  @Autowired private IntervinienteUseCaseImpl intervinienteUseCaseimpl;
  @Autowired private TipoIntervencionRepository tipoIntervencionRepository;
  @Autowired private OcupanteUseCaseImpl ocupanteUseCaseImpl;
  @Autowired private OcupanteMapper ocupanteMapper;
  @Autowired private OcupanteRepository ocupanteRepository;
  @Autowired private BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired private CampaniaRepository campaniaRepository;
  @Autowired private ProcedimientoMapper procedimientoMapper;
  @Autowired private ProcedimientoPosesionNegociadaUseCase procedimientoPosesionNegociadaUseCase;

  @Autowired
  private ProcedimientoPosesionNegociadaRepository procedimientoPosesionNegociadaRepository;

  @Autowired private ProcedimientoPosesionNegociadaMapper procedimientoPosesionNegociadaMapper;

  @Override
  @SneakyThrows
  public ListWithCountDTO<ExpedienteDTOToList> getAllFilteredExpedientes(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      String id,
      String estado,
      String modoDeGestion,
      String situacion,
      String cliente,
      String cartera,
      String primerTitular,
      String saldoGestion,
      String estrategia,
      String supervisor,
      String areaGestion,
      String gestor,
      String gestor_suplente,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page) {
    List<Expediente> expedientesSinPaginar =
        getDataExpedientesFilter(busquedaDto, loggedUser, orderField, orderDirection);

    if (page == 0) {
      BusquedaReciente busquedaReciente =
          new BusquedaReciente(loggedUser, busquedaDto, expedientesSinPaginar.size());
      busquedaRecienteRepository.save(busquedaReciente);
    }

    List<ExpedienteDTOToList> expedientesDTOs =
        expedientesSinPaginar.stream()
            .map(expedienteMapper::parseDTOToList)
            .collect(Collectors.toList());

    // Primero filtra la lista y después la ordena, añadiendo la paginación con el skip
    expedientesDTOs =
        filtrarListaExpedientes(
            expedientesDTOs,
            id,
            estado,
            modoDeGestion,
            situacion,
            cliente,
            cartera,
            primerTitular,
            saldoGestion,
            estrategia,
            supervisor,
            areaGestion,
            gestor,
            gestor_suplente);
    Integer i = expedientesDTOs.size();
    expedientesDTOs =
        ordenarListaExpedientes(expedientesDTOs, orderField, orderDirection).stream()
            .skip(size * page)
            .limit(size)
            .collect(Collectors.toList());
    return new ListWithCountDTO<>(expedientesDTOs, i);
  }

  @Override
  @SneakyThrows
  public ListWithCountDTO<FormalizacionBusquedaOutputDTO> getBusquedaFormalizacion(BusquedaDto busquedaDto, Usuario loggedUser,
                                                                                   String id,
                                                                                   String titular,
                                                                                   String nif,
                                                                                   String estado,
                                                                                   String detalleEstado,
                                                                                   String fechaFirma,
                                                                                   String horaFirma,
                                                                                   String formalizacionHaya,
                                                                                   String gestorHaya,
                                                                                   String formalizacionCliente,
                                                                                   String fechaSancion,
                                                                                   String importeDacion,
                                                                                   String importeDeuda,
                                                                                   String provincia,
                                                                                   String alquiler,
                                                                         String orderField, String orderDirection, Integer size, Integer page) {
    List<Expediente> expedientesSinPaginar = getDataExpedientesFilter(busquedaDto, loggedUser, orderField, orderDirection);

    if (page == 0) {
      BusquedaReciente busquedaReciente = new BusquedaReciente(loggedUser, busquedaDto, expedientesSinPaginar.size());
      busquedaRecienteRepository.save(busquedaReciente);
    }

    List<FormalizacionBusquedaOutputDTO> expedientesDTOs = expedientesSinPaginar
      .stream()
      .map(FormalizacionBusquedaOutputDTO::new)
      .collect(Collectors.toList());

    // Primero filtra la lista y después la ordena, añadiendo la paginación con el skip
    expedientesDTOs = filtrarListaExpedientesFormalizacion(expedientesDTOs,
      id, titular, nif, estado, detalleEstado, fechaFirma, horaFirma, formalizacionHaya, gestorHaya, formalizacionCliente, fechaSancion, importeDacion, importeDeuda, provincia, alquiler);
    Integer i = expedientesDTOs.size();
    if (size != null)
      expedientesDTOs =
          ordenarListaFormalizacion(expedientesDTOs, orderField, orderDirection).stream()
              .skip(size * page)
              .limit(size)
              .collect(Collectors.toList());
    return new ListWithCountDTO<>(expedientesDTOs, i);
  }

  public LinkedHashMap<String, List<Collection<?>>> getAllFilteredExpedientesExcel(
      BusquedaDto busquedaDto, Usuario usuario) throws NotFoundException {
    List<Expediente> expedientesSinPaginar =
        getDataExpedientesFilter(busquedaDto, usuario, null, null);
    return this.expedienteExcelUseCase.toExcel(expedientesSinPaginar, new ExpedienteExcelExportDTO(true));
  }

  public List<Expediente> getDataExpedientesFilter(
      BusquedaDto busquedaDto, Usuario loggedUser, String orderField, String orderDirection)
      throws NotFoundException {
    BusquedaExpedienteDto busquedaExpedienteDto = busquedaDto.getExpediente();

    BusquedaBuilder builder = generateBuilder(busquedaDto, loggedUser, orderField, orderDirection);

    // builder.setOrdenación(orderField, orderDirection);

    var query = builder.createQuery().distinct(true);
    List<Expediente> result = entityManager.createQuery(query).getResultList();
    if (busquedaExpedienteDto != null && busquedaExpedienteDto.getSaldoGestion() != null) {
      IntervaloImporte ii = busquedaExpedienteDto.getSaldoGestion();
      result =
          result.stream()
              .filter(
                  expediente -> {
                    var saldo = expediente.getSaldoGestion();
                    if (ii.getIgual() != null && saldo == ii.getIgual()) {
                      return true;
                    } else if (ii.getMayor() != null && ii.getMenor() != null) {
                      if (saldo > ii.getMayor() && saldo < ii.getMenor()) return true;
                    } else {
                      if (ii.getMayor() != null && saldo > ii.getMayor()) return true;
                      if (ii.getMenor() != null && saldo < ii.getMenor()) return true;
                    }
                    return false;
                  })
              .collect(Collectors.toList());
    }
    return result;
  }

  @SneakyThrows
  public BusquedaBuilder generateBuilder(
      BusquedaDto busquedaDto, Usuario loggedUser, String orderField, String orderDirection)
      throws NotFoundException {
    BusquedaExpedienteDto busquedaExpedienteDto = busquedaDto.getExpediente();
    BusquedaContratoDto busquedaContratoDto = busquedaDto.getContrato();
    BusquedaIntervinienteDto busquedaIntervinienteDto = busquedaDto.getInterviniente();
    BusquedaBienDto busquedaBienDto = busquedaDto.getBien();
    BusquedaMovimientoDto busquedaMovimientoDto = busquedaDto.getMovimientos();
    BusquedaEstimacionDto busquedaEstimacionDto = busquedaDto.getEstimaciones();
    BusquedaProcedimientoDto busquedaProcedimientoDto = busquedaDto.getProcedimiento();
    BusquedaPropuestaDto busquedaPropuestaDto = busquedaDto.getPropuestas();
    BusquedaAgendaDto busquedaAgendaDto = busquedaDto.getAgenda();

    BusquedaBuilder builder = new BusquedaBuilder(entityManager.getCriteriaBuilder());
    if (busquedaExpedienteDto != null)
      builder.addCondicionesExpediente(busquedaExpedienteDto, loggedUser);
    if (busquedaContratoDto != null) builder.addCondicionesContrato(busquedaContratoDto);
    if (busquedaIntervinienteDto != null)
      builder.addCondicionesInterviniente(busquedaIntervinienteDto);
    if (busquedaBienDto != null) builder.addCondicionesBien(busquedaBienDto);
    if (busquedaMovimientoDto != null) builder.addCondicionesMovimiento(busquedaMovimientoDto);
    if (busquedaEstimacionDto != null) builder.addCondicionesEstimacion(busquedaEstimacionDto);
    if (busquedaProcedimientoDto != null) {
      TipoProcedimiento tipo = null;
      HitoProcedimiento fechaHito = null;
      if (busquedaProcedimientoDto.getTipoProcedimiento() != null) {
        tipo =
            tipoProcedimientoRepository
                .findById(busquedaProcedimientoDto.getTipoProcedimiento())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "Tipo de Procedimiento",
                            busquedaProcedimientoDto.getTipoProcedimiento()));
      }
      if (busquedaProcedimientoDto.getHitoProcedimiento() != null) {
        fechaHito =
            hitoProcedimientoRepository
                .findById(busquedaProcedimientoDto.getHitoProcedimiento())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "Hito de Procedimiento",
                            busquedaProcedimientoDto.getHitoProcedimiento()));
      }
      builder.addCondicionesProcedimiento(busquedaProcedimientoDto, tipo, fechaHito);
    }
    if (busquedaPropuestaDto != null) {
      List<Integer> expedientes = null;
      if (busquedaPropuestaDto.getIntervaloImporte() != null) {
        if (busquedaPropuestaDto.getIntervaloImporte().getDescripcion().equals("valoracion")) {
          expedientes = getExpedienteFilteredByImporteUltimaValoracion(busquedaPropuestaDto);
        }
      }
      builder.addCondicionesPropuesta(busquedaPropuestaDto, expedientes);
    }
    if (busquedaAgendaDto != null) {
      if (busquedaAgendaDto.getIntervaloFechas() != null
          || busquedaAgendaDto.getDestinatario() != null
          || busquedaAgendaDto.getEmisor() != null
          || busquedaAgendaDto.getEstado() != null
          || busquedaAgendaDto.getClaseEvento() != null) {
        List<Integer> expedientesIds = getExpedienteFilteredByAgenda(loggedUser, busquedaAgendaDto);
        builder.addCondicionesAgenda(expedientesIds);
      }
    }

    return builder;
  }

  public List<Interviniente> getDataIntervinientesFilter(
      BusquedaDto busquedaDto, Usuario loggedUser) throws NotFoundException {
    BusquedaExpedienteDto busquedaExpedienteDto = busquedaDto.getExpediente();
    BusquedaContratoDto busquedaContratoDto = busquedaDto.getContrato();
    BusquedaIntervinienteDto busquedaIntervinienteDto = busquedaDto.getInterviniente();
    BusquedaBienDto busquedaBienDto = busquedaDto.getBien();
    BusquedaMovimientoDto busquedaMovimientoDto = busquedaDto.getMovimientos();
    BusquedaEstimacionDto busquedaEstimacionDto = busquedaDto.getEstimaciones();
    BusquedaProcedimientoDto busquedaProcedimientoDto = busquedaDto.getProcedimiento();
    BusquedaPropuestaDto busquedaPropuestaDto = busquedaDto.getPropuestas();
    BusquedaAgendaDto busquedaAgendaDto = busquedaDto.getAgenda();

    BusquedaBuilderInterviniente builder =
        new BusquedaBuilderInterviniente(entityManager.getCriteriaBuilder());
    if (busquedaExpedienteDto != null)
      builder.addCondicionesExpediente(busquedaExpedienteDto, loggedUser);
    if (busquedaContratoDto != null) builder.addCondicionesContrato(busquedaContratoDto);
    if (busquedaIntervinienteDto != null)
      builder.addCondicionesInterviniente(busquedaIntervinienteDto);

    if (busquedaBienDto != null) builder.addCondicionesBien(busquedaBienDto);
    if (busquedaMovimientoDto != null) builder.addCondicionesMovimiento(busquedaMovimientoDto);
    if (busquedaEstimacionDto != null) builder.addCondicionesEstimacion(busquedaEstimacionDto);
    if (busquedaProcedimientoDto != null) {
      TipoProcedimiento tipo = null;
      HitoProcedimiento fechaHito = null;
      if (busquedaProcedimientoDto.getTipoProcedimiento() != null) {
        tipo =
            tipoProcedimientoRepository
                .findById(busquedaProcedimientoDto.getTipoProcedimiento())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "Tipo de Procedimiento",
                            busquedaProcedimientoDto.getTipoProcedimiento()));
      }
      if (busquedaProcedimientoDto.getHitoProcedimiento() != null) {
        fechaHito =
            hitoProcedimientoRepository
                .findById(busquedaProcedimientoDto.getHitoProcedimiento())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "Hipo de Procedimiento",
                            busquedaProcedimientoDto.getHitoProcedimiento()));
      }
      builder.addCondicionesProcedimiento(busquedaProcedimientoDto, tipo, fechaHito);
    }
    if (busquedaPropuestaDto != null) {
      List<Integer> expedientes = null;
      if (busquedaPropuestaDto.getIntervaloImporte() != null) {
        if (busquedaPropuestaDto.getIntervaloImporte().getDescripcion().equals("valoracion")) {
          expedientes = getExpedienteFilteredByImporteUltimaValoracion(busquedaPropuestaDto);
        }
      }
      builder.addCondicionesPropuesta(busquedaPropuestaDto, expedientes);
    }
    if (busquedaAgendaDto != null) {
      if (busquedaAgendaDto.getIntervaloFechas() != null
          || busquedaAgendaDto.getDestinatario() != null
          || busquedaAgendaDto.getEmisor() != null
          || busquedaAgendaDto.getEstado() != null
          || busquedaAgendaDto.getClaseEvento() != null) {
        List<Integer> expedientesIds = getExpedienteFilteredByAgenda(loggedUser, busquedaAgendaDto);
        builder.addCondicionesAgenda(expedientesIds);
      }
    }

    // builder.setOrdenación(orderField, orderDirection);

    var query = builder.createQuery().distinct(true);
    List<Interviniente> result = entityManager.createQuery(query).getResultList();
    /*Query queryResult = entityManager.createQuery(query);

    if (page != null && size != null){
      queryResult.setFirstResult(page * size);
      queryResult.setMaxResults(size);
    }

    List<Interviniente> result = queryResult.getResultList();*/

    if (busquedaExpedienteDto != null && busquedaExpedienteDto.getSaldoGestion() != null) {
      IntervaloImporte ii = busquedaExpedienteDto.getSaldoGestion();
      /*   result = result.stream().filter(expediente -> {
        var saldo = expediente.getSaldoGestion();
        if (ii.getIgual() != null && saldo == ii.getIgual()) {
          return true;
        } else if (ii.getMayor() != null && ii.getMenor() != null) {
          if (saldo > ii.getMayor() && saldo < ii.getMenor())
            return true;
        } else {
          if (ii.getMayor() != null && saldo > ii.getMayor())
            return true;
          if (ii.getMenor() != null && saldo < ii.getMenor())
            return true;
        }
        return false;
      }).collect(Collectors.toList());*/
    }
    return result;
  }

  private List<Integer> getExpedienteFilteredByImporteUltimaValoracion(
      BusquedaPropuestaDto busquedaPropuestaDto) {
    List<Map> infoValoraciones = expedienteUseCase.getExpedientesByUltimaValoracion();
    List<Integer> expedientes = new ArrayList<>();
    for (Map map : infoValoraciones) {
      Object importeMap = map.get("importe");
      Double importe = importeMap != null ? Double.parseDouble(String.valueOf(importeMap)) : null;
      if (importe != null) {
        Object idExpedienteMap = map.get("idExpediente");
        Integer idExpediente =
            idExpedienteMap != null ? Integer.parseInt(String.valueOf(idExpedienteMap)) : null;
        if (idExpediente != null) {
          if (busquedaPropuestaDto.getIntervaloImporte().getIgual() != null) {
            if (busquedaPropuestaDto.getIntervaloImporte().getIgual().equals(importe)) {
              if (!expedientes.contains(idExpediente)) expedientes.add(idExpediente);
            }
          } else if (busquedaPropuestaDto.getIntervaloImporte().getMenor() != null
              && busquedaPropuestaDto.getIntervaloImporte().getMayor() != null) {
            if (busquedaPropuestaDto.getIntervaloImporte().getMenor() >= importe
                && busquedaPropuestaDto.getIntervaloImporte().getMayor() <= importe) {
              if (!expedientes.contains(idExpediente)) expedientes.add(idExpediente);
            }
          } else if (busquedaPropuestaDto.getIntervaloImporte().getMenor() != null) {
            if (busquedaPropuestaDto.getIntervaloImporte().getMenor() >= importe) {
              if (!expedientes.contains(idExpediente)) expedientes.add(idExpediente);
            }
          } else {
            if (busquedaPropuestaDto.getIntervaloImporte().getMayor() <= importe) {
              if (!expedientes.contains(idExpediente)) expedientes.add(idExpediente);
            }
          }
        }
      }
    }
    return expedientes;
  }

  private List<Integer> getExpedienteFilteredByAgenda(
      Usuario loggedUser, BusquedaAgendaDto busquedaAgendaDto) throws NotFoundException {
    List<Evento> eventos = eventoUtil.getListFilteredEventos(loggedUser.getId(), busquedaAgendaDto);
    List<Integer> expedientes = null;
    if (eventos != null) {
      expedientes = new ArrayList<>();
      for (Evento evento : eventos) {
        switch (evento.getNivel().getCodigo()) {
          case "EXP":
            Expediente expediente =
                expedienteRepository
                    .findById(evento.getIdNivel())
                    .orElseThrow(() -> new NotFoundException("Expediente", evento.getIdNivel()));
            if (!expedientes.contains(expediente.getId())) expedientes.add(expediente.getId());
            break;
          case "CON":
            Contrato contrato =
                contratoRepository
                    .findById(evento.getIdNivel())
                    .orElseThrow(() -> new NotFoundException("Contrato", evento.getIdNivel()));
            if (contrato.getExpediente() != null
                && !expedientes.contains(contrato.getExpediente().getId()))
              expedientes.add(contrato.getExpediente().getId());
            break;
          case "BIEN":
            Bien bien =
                bienRepository
                    .findById(evento.getIdNivel())
                    .orElseThrow(() -> new NotFoundException("Bien", evento.getIdNivel()));
            List<ContratoBien> contratosBien = new ArrayList<>(bien.getContratos());
            if (contratosBien != null) {
              for (ContratoBien cb : contratosBien) {
                if (bien.getId().equals(cb.getBien().getId())
                    && cb.getContrato().getExpediente() != null
                    && !expedientes.contains(cb.getContrato().getExpediente().getId()))
                  expedientes.add(cb.getContrato().getExpediente().getId());
              }
            }
            break;
          case "INTE":
            Interviniente interviniente =
                intervinienteRepository
                    .findById(evento.getIdNivel())
                    .orElseThrow(() -> new NotFoundException("Interviniente", evento.getIdNivel()));
            List<ContratoInterviniente> contratosInterviniente =
                new ArrayList<>(interviniente.getContratos());
            if (contratosInterviniente != null) {
              for (ContratoInterviniente ci : contratosInterviniente) {
                if (interviniente.getId().equals(ci.getInterviniente().getId())
                    && ci.getContrato().getExpediente() != null
                    && !expedientes.contains(ci.getContrato().getExpediente().getId()))
                  expedientes.add(ci.getContrato().getExpediente().getId());
              }
            }
            break;
          case "JUD":
            Procedimiento procedimiento =
                procedimientoRepository
                    .findById(evento.getIdNivel())
                    .orElseThrow(() -> new NotFoundException("Procedimiento", evento.getIdNivel()));
            List<Contrato> contratosProcedimiento = new ArrayList<>(procedimiento.getContratos());
            if (contratosProcedimiento != null) {
              for (Contrato c : contratosProcedimiento) {
                if (c.getExpediente() != null && !expedientes.contains(c.getExpediente().getId()))
                  expedientes.add(c.getExpediente().getId());
              }
            }
            break;
          case "PRO":
            Propuesta propuesta =
                propuestaRepository
                    .findById(evento.getIdNivel())
                    .orElseThrow(() -> new NotFoundException("Propuesta", evento.getIdNivel()));
            List<Contrato> contratosPropuesta = new ArrayList<>(propuesta.getContratos());
            if (propuesta.getExpediente() != null) {
              if (!expedientes.contains(propuesta.getExpediente().getId()))
                expedientes.add(propuesta.getExpediente().getId());
            } else if (contratosPropuesta != null) {
              for (Contrato c : contratosPropuesta) {
                if (c.getExpediente() != null && !expedientes.contains(c.getExpediente().getId()))
                  expedientes.add(c.getExpediente().getId());
              }
            }
            break;
          default:
            break;
        }
      }
    }
    return expedientes;
  }

  public List<BusquedaRecienteDto> getRecientes(Usuario usuario) {
    return this.busquedaRecienteRepository.findTop10ByUsuarioOrderByFechaDesc(usuario).stream()
        .map(this.busquedaMapper::busquedaRecienteToDto)
        .collect(Collectors.toList());
  }

  public List<BusquedaGuardadaDto> getGuardadas(Usuario usuario) {
    return this.busquedaGuardadaRepository.findAllByUsuario(usuario).stream()
        .map(this.busquedaMapper::busquedaGuardadaToDto)
        .collect(Collectors.toList());
  }

  public ListWithCountDTO<ExpedienteDTOToList> getGuardada(
      Usuario usuario, int id, Integer size, Integer page) throws NotFoundException {
    BusquedaGuardada busquedaGuardada = this.getBusquedaGuardadaOrThrow(id, usuario);
    ListWithCountDTO<ExpedienteDTOToList> resultados =
        this.getAllFilteredExpedientes(
            busquedaGuardada.parseJsonConsulta(),
            usuario,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            size,
            page);

    // actualizar número de resultados de búsqueda
    busquedaGuardada.setNumeroResultados(resultados.getCount());
    busquedaGuardada.setFecha(new Date());
    this.busquedaGuardadaRepository.save(busquedaGuardada);

    return resultados;
  }

  public BusquedaGuardadaDto crearGuardada(
      Usuario usuario, BusquedaGuardadaInputDto busquedaGuardadaInputDto) throws NotFoundException {
    BusquedaGuardada busquedaGuardada =
        this.busquedaMapper.parseBusquedaGuardadaDto(usuario, busquedaGuardadaInputDto);

    // actualizar número de resultados de búsqueda
    int numeroResultados =
        this.getDataExpedientesFilter(busquedaGuardadaInputDto.getConsulta(), usuario, null, null)
            .size();
    busquedaGuardada.setNumeroResultados(numeroResultados);
    busquedaGuardada.setFecha(new Date());

    this.busquedaGuardadaRepository.save(busquedaGuardada);
    return this.busquedaMapper.busquedaGuardadaToDto(busquedaGuardada);
  }

  public void eliminarGuardada(Usuario usuario, int id) throws NotFoundException {
    BusquedaGuardada busquedaGuardada = this.getBusquedaGuardadaOrThrow(id, usuario);
    this.busquedaGuardadaRepository.delete(busquedaGuardada);
  }

  private BusquedaGuardada getBusquedaGuardadaOrThrow(int id, Usuario usuario)
      throws NotFoundException {
    return this.busquedaGuardadaRepository
        .findByIdAndUsuario(id, usuario)
        .orElseThrow(() -> new NotFoundException("Búsqueda", id));
  }

  @Override
  @SneakyThrows
  public ListWithCountDTO<ContratoBusquedaDto> getAllFilteredContratos(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      Integer size,
      Integer page,
      String id,
      String idOrigen,
      String cartera,
      String activo,
      String origenEstrategia,
      String fecha,
      String periodo,
      String tipo,
      String estrategia,
      String orderField,
      String orderDirection) {
    List<Expediente> expedientesSinPaginar =
        getDataExpedientesFilter(busquedaDto, loggedUser, null, null);
    if (page == 0) {
      BusquedaReciente busquedaReciente =
          new BusquedaReciente(loggedUser, busquedaDto, expedientesSinPaginar.size());
      busquedaRecienteRepository.save(busquedaReciente);
    }

    List<ContratoBusquedaDto> contratosDtos = new ArrayList<>();
    List<List<ContratoBusquedaDto>> tempList =
        expedientesSinPaginar.stream()
            .map(expedienteMapper::getContratoDTO)
            .collect(Collectors.toList());

    // Estos bucles transforman la list<list<>> en una unica list<>
    for (var l : tempList) {
      for (var contrato : l) {
        contratosDtos.add(contrato);
      }
    }
    int listTotalSize = contratosDtos.size();

    // Primero filtra la lista y después la ordena, añadiendo la paginación con el skip
    contratosDtos =
        filtrarListaContratos(
            contratosDtos,
            id,
            idOrigen,
            cartera,
            activo,
            origenEstrategia,
            fecha,
            periodo,
            tipo,
            estrategia);
    contratosDtos =
        ordenarListaContratos(contratosDtos, orderField, orderDirection).stream()
            .skip(size * page)
            .limit(size)
            .collect(Collectors.toList());
    return new ListWithCountDTO<>(contratosDtos, listTotalSize);
  }

  public ListWithCountDTO<BienPosesionNegociadaDto> getAllFilteredBienesPN(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      Integer size,
      Integer page,
      String orderField,
      String orderDirection)
      throws NotFoundException {
    return expedientePosesionNegociadaCase.getAllFilteredBienesPN(
        busquedaDto, loggedUser, page, size, orderField, orderDirection);
  }

  @Override
  @SneakyThrows
  public ListWithCountDTO<BienExtendedDTOToList> getAllFilteredBienesPN(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page) {
    List<ExpedientePosesionNegociada> expedientesSinPaginar =
        expedientePosesionNegociadaCase.filtrarExpedientesPosesionNegociadaBusqueda(
            busquedaDto,
            loggedUser,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            orderField,
            orderDirection);

    if (page == 0) {
      BusquedaReciente busquedaReciente =
          new BusquedaReciente(loggedUser, busquedaDto, expedientesSinPaginar.size());
      busquedaRecienteRepository.save(busquedaReciente);
    }

    List<BienExtendedDTOToList> bienesDTOs = new ArrayList<>();
    var tempList =
        expedientesSinPaginar.stream()
            .map(expedienteMapper::getBienPNDTO)
            .collect(Collectors.toList());

    // Estos bucles transforman la list<list<>> en una unica list<>
    for (var l : tempList) {
      for (var bienPN : l) {
        bienesDTOs.add(bienPN);
      }
    }
    int listTotalSize = bienesDTOs.size();

    bienesDTOs = bienesDTOs.stream().skip(size * page).limit(size).collect(Collectors.toList());
    return new ListWithCountDTO<>(bienesDTOs, listTotalSize);
  }

  @Override
  @SneakyThrows
  public ListWithCountDTO<BienExtendedDTOToList> getAllFilteredBienesRA(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page) {
    List<Expediente> expedientesSinPaginar =
        getDataExpedientesFilter(busquedaDto, loggedUser, orderField, orderDirection);

    if (page == 0) {
      BusquedaReciente busquedaReciente =
          new BusquedaReciente(loggedUser, busquedaDto, expedientesSinPaginar.size());
      busquedaRecienteRepository.save(busquedaReciente);
    }

    List<BienExtendedDTOToList> bienesDTOs = new ArrayList<>();
    List<List<BienExtendedDTOToList>> tempList =
        expedientesSinPaginar.stream()
            .map(expedienteMapper::getBienDTO)
            .collect(Collectors.toList());

    // Estos bucles transforman la list<list<>> en una unica list<>
    for (var l : tempList) {
      for (var bienPN : l) {
        bienesDTOs.add(bienPN);
      }
    }
    int listTotalSize = bienesDTOs.size();

    bienesDTOs = bienesDTOs.stream().skip(size * page).limit(size).collect(Collectors.toList());
    return new ListWithCountDTO<>(bienesDTOs, listTotalSize);
  }

  private List<ContratoBusquedaDto> ordenarListaContratos(
      List<ContratoBusquedaDto> lista, String orderField, String orderDirection) {
    // En caso de que el orderfield no este contemplado se ordena por ID por defecto
    Comparator<ContratoBusquedaDto> comparator = Comparator.comparing(ContratoBusquedaDto::getId);
    lista.sort(comparator);
    if (orderField == null || orderDirection == null) return lista;

    if (orderField.equals("cartera")) {
      comparator =
          (ContratoBusquedaDto o1, ContratoBusquedaDto o2) ->
              StringUtils.compare(o1.getCartera(), o2.getCartera());
    } else if (orderField.equals("activo")) {
      comparator =
          (ContratoBusquedaDto o1, ContratoBusquedaDto o2) ->
              StringUtils.compare(o1.getActivo(), o2.getActivo());
    } else if (orderField.equals("origenEstrategia")) {
      comparator =
          (ContratoBusquedaDto o1, ContratoBusquedaDto o2) ->
              StringUtils.compare(o1.getOrigenEstrategia(), o2.getOrigenEstrategia());
    } else if (orderField.equals("fecha")) {
      comparator =
          (ContratoBusquedaDto o1, ContratoBusquedaDto o2) ->
              StringUtils.compare(o1.getFecha(), o2.getFecha());
    } else if (orderField.equals("periodo")) {
      comparator =
          (ContratoBusquedaDto o1, ContratoBusquedaDto o2) ->
              StringUtils.compare(o1.getPeriodo(), o2.getPeriodo());
    } else if (orderField.equals("tipo")) {
      comparator =
          (ContratoBusquedaDto o1, ContratoBusquedaDto o2) ->
              StringUtils.compare(o1.getTipo(), o2.getTipo());
    } else if (orderField.equals("estrategia")) {
      comparator =
          (ContratoBusquedaDto o1, ContratoBusquedaDto o2) ->
              StringUtils.compare(o1.getEstrategia(), o2.getEstrategia());
    }

    if (orderDirection.equals("asc")) lista.sort(comparator);
    else if (orderDirection.equals("desc")) lista.sort(comparator.reversed());
    return lista;
  }

  private List<ContratoBusquedaDto> filtrarListaContratos(
      List<ContratoBusquedaDto> lista,
      String id,
      String idOrigen,
      String cartera,
      String activo,
      String origenEstrategia,
      String fecha,
      String periodo,
      String tipo,
      String estrategia) {
    if (id != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getId().toString()), id))
              .collect(Collectors.toList());
    if (idOrigen != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getIdOrigen()), idOrigen))
              .collect(Collectors.toList());
    if (cartera != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getCartera()), cartera))
              .collect(Collectors.toList());
    if (activo != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getActivo()), activo))
              .collect(Collectors.toList());
    if (origenEstrategia != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getOrigenEstrategia()), origenEstrategia))
              .collect(Collectors.toList());
    if (fecha != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getFecha()), fecha))
              .collect(Collectors.toList());
    if (periodo != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getPeriodo()), periodo))
              .collect(Collectors.toList());
    if (tipo != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getTipo()), tipo))
              .collect(Collectors.toList());
    if (estrategia != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getEstrategia()), estrategia))
              .collect(Collectors.toList());
    return lista;
  }

  public List<ExpedienteDTOToList> ordenarListaExpedientes(
      List<ExpedienteDTOToList> lista, String orderField, String orderDirection) {

    // En caso de que el orderfield no este contemplado se ordena por ID por defecto
    Comparator<ExpedienteDTOToList> comparator =
        (ExpedienteDTOToList o1, ExpedienteDTOToList o2) -> Integer.compare(o1.getId(), o2.getId());
    lista.sort(comparator);
    if (orderField == null || orderDirection == null) return lista;

    if (orderField.equals("id")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              Integer.compare(o1.getId(), o2.getId());
    } else if (orderField.equals("estado")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(
                  o1.getEstado() != null ? o1.getEstado().getValor() : null,
                  o2.getEstado() != null ? o2.getEstado().getValor() : null);
    } else if (orderField.equals("modoDeGestion")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(o1.getModoDeGestion(), o2.getModoDeGestion());
    } else if (orderField.equals("situacion")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(o1.getSituacion(), o2.getSituacion());
    } else if (orderField.equals("cliente")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(o1.getCliente(), o2.getCliente());
    } else if (orderField.equals("cartera")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(o1.getCartera(), o2.getCartera());
    } else if (orderField.equals("primerTitular")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(o1.getPrimerTitular(), o2.getPrimerTitular());
    } else if (orderField.equals("saldoEnGestion") || orderField.equals("saldoGestion")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              Double.compare(o1.getSaldoGestion(), o2.getSaldoGestion());
    } else if (orderField.equals("estrategia")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(
                  o1.getEstrategia() != null ? o1.getEstrategia().getValor() : null,
                  o2.getEstrategia() != null ? o2.getEstrategia().getValor() : null);
    } else if (orderField.equals("supervisor")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(o1.getSupervisor(), o2.getSupervisor());
    } else if (orderField.equals("areaDeGestion")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(o1.getAreaGestion().get(0), o2.getAreaGestion().get(0));
    } else if (orderField.equals("gestor")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(o1.getGestor(), o2.getGestor());
    } else if (orderField.equals("gestor_suplente")) {
      comparator =
          (ExpedienteDTOToList o1, ExpedienteDTOToList o2) ->
              StringUtils.compare(o1.getGestor_suplente(), o2.getGestor_suplente());
    }

    if (orderDirection.equals("asc")) lista.sort(comparator);
    else if (orderDirection.equals("desc")) lista.sort(comparator.reversed());
    return lista;
  }

  private List<FormalizacionBusquedaOutputDTO> ordenarListaFormalizacion(List<FormalizacionBusquedaOutputDTO> lista, String orderField, String orderDirection) {

    // En caso de que el orderfield no este contemplado se ordena por ID por defecto
    Comparator<FormalizacionBusquedaOutputDTO> comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> Integer.compare(o1.getIdExpediente(), o2.getIdExpediente());
    lista.sort(comparator);
    if (orderField == null || orderDirection == null) return lista;

    if (orderField.equals("idConcatenado")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getIdConcatenado(), o2.getIdConcatenado());
    } else if (orderField.equals("titular")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getTitular(), o2.getTitular());
    } else if (orderField.equals("nif")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getNif(), o2.getNif());
    } else if (orderField.equals("estado")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getEstado() != null ? o1.getEstado().getValor() : null, o2.getEstado() != null ? o2.getEstado().getValor() : null);
    } else if (orderField.equals("detalleEstado")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getDetalleEstado() != null ? o1.getDetalleEstado().getValor() : null, o2.getDetalleEstado() != null ? o2.getDetalleEstado().getValor() : null);
    } else if (orderField.equals("fechaFirma")) {
      comparator = Comparator.comparing(FormalizacionBusquedaOutputDTO::getFechaFirma);
    } else if (orderField.equals("horaFirma")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getHoraFirma(), o2.getHoraFirma());
    } else if (orderField.equals("formalizacionHaya")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getFormalizacionHaya(), o2.getFormalizacionHaya());
    } else if (orderField.equals("gestorHaya")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getGestorHaya() != null ? o1.getGestorHaya().getNombre() : null, o2.getGestorHaya() != null ? o2.getGestorHaya().getNombre() : null);
    } else if (orderField.equals("formalizacionCliente")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getFormalizacionCliente(), o2.getFormalizacionCliente());
    } else if (orderField.equals("fechaSancion")) {
      comparator = Comparator.comparing(FormalizacionBusquedaOutputDTO::getFechaSancion);
    } else if (orderField.equals("importeDacion")) {
      comparator = Comparator.comparingDouble(FormalizacionBusquedaOutputDTO::getImporteDacion);
    } else if (orderField.equals("importeDeuda")) {
      comparator = Comparator.comparingDouble(FormalizacionBusquedaOutputDTO::getImporteDeuda);
    } else if (orderField.equals("provincia")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getProvincia() != null ? o1.getProvincia().getValor() : null, o2.getProvincia() != null ? o2.getProvincia().getValor() : null);
    } else if (orderField.equals("alquiler")) {
      comparator = (FormalizacionBusquedaOutputDTO o1, FormalizacionBusquedaOutputDTO o2) -> StringUtils.compare(o1.getAlquiler() != null ? o1.getAlquiler().getValor() : null, o2.getAlquiler() != null ? o2.getAlquiler().getValor() : null);
    }

    if (orderDirection.equals("asc")) lista.sort(comparator);
    else if (orderDirection.equals("desc")) lista.sort(comparator.reversed());
    return lista;
  }

  public List<ExpedienteDTOToList> filtrarListaExpedientes(
      List<ExpedienteDTOToList> lista,
      String id,
      String estado,
      String modoDeGestion,
      String situacion,
      String cliente,
      String cartera,
      String primerTitular,
      String saldoGestion,
      String estrategia,
      String supervisor,
      String areaGestion,
      String gestor,
      String gestor_suplente) {
    if (id != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      c.getIdConcatenado() != null
                          && StringUtils.startsWithIgnoreCase(
                              StringUtils.stripAccents(c.getIdConcatenado().toString()), id))
              .collect(Collectors.toList());
    if (estado != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getEstado().getValor()), estado))
              .collect(Collectors.toList());
    if (modoDeGestion != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getModoDeGestion()), modoDeGestion))
              .collect(Collectors.toList());
    if (situacion != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getSituacion()), situacion))
              .collect(Collectors.toList());
    if (cliente != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getCliente()), cliente))
              .collect(Collectors.toList());
    if (cartera != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getCartera()), cartera))
              .collect(Collectors.toList());
    if (primerTitular != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getPrimerTitular()), primerTitular))
              .collect(Collectors.toList());
    if (saldoGestion != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getSaldoGestion().toString()), saldoGestion))
              .collect(Collectors.toList());
    if (estrategia != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(
                              c.getEstrategia() != null ? c.getEstrategia().getValor() : null),
                          estrategia))
              .collect(Collectors.toList());
    if (supervisor != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getSupervisor()), supervisor))
              .collect(Collectors.toList());
    if (gestor != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getGestor()), gestor))
              .collect(Collectors.toList());
    if (gestor_suplente != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      StringUtils.startsWithIgnoreCase(
                          StringUtils.stripAccents(c.getGestor_suplente()), gestor_suplente))
              .collect(Collectors.toList());
    if (areaGestion != null)
      lista =
          lista.stream()
              .filter(
                  c ->
                      c.getAreaGestion().stream()
                          .anyMatch(
                              s ->
                                  StringUtils.startsWithIgnoreCase(
                                      StringUtils.stripAccents(s), areaGestion)))
              .collect(Collectors.toList());
    return lista;
  }

  private List<FormalizacionBusquedaOutputDTO> filtrarListaExpedientesFormalizacion(
      List<FormalizacionBusquedaOutputDTO> lista,
      String id,
      String titular,
      String nif,
      String estado,
      String detalleEstado,
      String fechaFirma,
      String horaFirma,
      String formalizacionHaya,
      String gestorHaya,
      String formalizacionCliente,
      String fechaSancion,
      String importeDacion,
      String importeDeuda,
      String provincia,
      String alquiler) {
    if (id != null)
      lista = lista.stream().filter(c -> c.getIdConcatenado() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getIdConcatenado().toString()), id)).collect(Collectors.toList());
    if (titular != null)
      lista = lista.stream().filter(c -> c.getTitular() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getTitular()), titular)).collect(Collectors.toList());
    if (nif != null)
      lista = lista.stream().filter(c -> c.getNif() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getNif()), nif)).collect(Collectors.toList());
    if (estado != null)
      lista = lista.stream().filter(c -> c.getEstado() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getEstado().getValor()), estado)).collect(Collectors.toList());
    if (detalleEstado != null)
      lista = lista.stream().filter(c -> c.getDetalleEstado() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getDetalleEstado().getValor()), detalleEstado)).collect(Collectors.toList());
    if (fechaFirma != null)
      lista = lista.stream().filter(c -> c.getFechaFirma() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents( c.getFechaFirma().toString()), fechaFirma)).collect(Collectors.toList());
    if (horaFirma != null)
      lista = lista.stream().filter(c -> c.getHoraFirma() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getHoraFirma()), horaFirma)).collect(Collectors.toList());
    if (formalizacionHaya != null)
      lista = lista.stream().filter(c -> c.getFormalizacionHaya() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getFormalizacionHaya()), formalizacionHaya)).collect(Collectors.toList());
    if (gestorHaya != null)
      lista = lista.stream().filter(c -> c.getGestorHaya() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getGestorHaya().getNombre()), formalizacionHaya)).collect(Collectors.toList());
    if (formalizacionCliente != null)
      lista = lista.stream().filter(c -> c.getFormalizacionCliente() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getFormalizacionCliente()), formalizacionCliente)).collect(Collectors.toList());
    if (fechaSancion != null)
      lista = lista.stream().filter(c -> c.getFechaSancion() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getFechaSancion().toString()), fechaSancion)).collect(Collectors.toList());
    if (importeDacion != null)
      lista = lista.stream().filter(c -> c.getImporteDacion() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getImporteDacion().toString()), importeDacion)).collect(Collectors.toList());
    if (importeDeuda != null)
      lista = lista.stream().filter(c -> c.getImporteDeuda() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getImporteDeuda().toString()), importeDeuda)).collect(Collectors.toList());
    if (provincia != null)
      lista = lista.stream().filter(c -> c.getProvincia() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getProvincia().getValor()), provincia)).collect(Collectors.toList());
    if (alquiler != null)
      lista = lista.stream().filter(c -> c.getAlquiler() != null && StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getAlquiler().getValor()), alquiler)).collect(Collectors.toList());

    return lista;
  }

  @Override
  public ListWithCountDTO<ExpedienteDTOToList> buscarExpedientesPorExcel(
      MultipartFile file, Usuario usuario) throws IOException {
    if (!file.getOriginalFilename().endsWith(".csv")
        && !file.getOriginalFilename().endsWith(".xlsx"))
      throw new NoSuchFileException("No se reconoce la extensión del archivo");

    List<String> listaIdRawValue = new ArrayList<>();

    if (file.getOriginalFilename().endsWith(".csv")) {
      if (file.isEmpty()) throw new FileNotFoundException("El fichero esta vacío");
      else {
        try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
          listaIdRawValue =
              new CsvToBeanBuilder(reader)
                  .withType(ExpedienteCSVInputDTO.class)
                  .withIgnoreLeadingWhiteSpace(true)
                  .build()
                  .parse();
        }
      }
    } else if (file.getOriginalFilename().endsWith(".xlsx")) {

      XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
      XSSFSheet worksheet = workbook.getSheetAt(0);
      for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
        XSSFRow row = worksheet.getRow(i);
        listaIdRawValue.add(row != null ? row.getCell(0).getStringCellValue() : null);
      }
    }

    List<Expediente> lista = expedienteRepository.findByIdConcatenadoIn(listaIdRawValue);
    if (!usuario.getRolHaya().getCodigo().equals("admin")) {
      List<Expediente> listaAux = new ArrayList<>();
      for (Expediente expediente : lista) {
        if (usuario.contieneExpediente(expediente.getId(), false)) listaAux.add(expediente);
      }
      lista = listaAux;
    }
    List<ExpedienteDTOToList> expedientesDTOs =
        lista.stream().map(expedienteMapper::parseDTOToList).collect(Collectors.toList());

    return new ListWithCountDTO<>(expedientesDTOs, expedientesDTOs.size());
  }

  public ListWithCountDTO<ExpedientePosesionNegociadaDTO> buscarExpedientesPNPorExcel(
      MultipartFile file, Usuario usuario) throws IOException {
    if (!file.getOriginalFilename().endsWith(".csv")
        && !file.getOriginalFilename().endsWith(".xlsx"))
      throw new NoSuchFileException("No se reconoce la extensión del archivo");

    List<String> listaIdRawValue = new ArrayList<>();

    if (file.getOriginalFilename().endsWith(".csv")) {
      if (file.isEmpty()) throw new FileNotFoundException("El fichero esta vacío");
      else {
        try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
          listaIdRawValue =
              new CsvToBeanBuilder(reader)
                  .withType(ExpedienteCSVInputDTO.class)
                  .withIgnoreLeadingWhiteSpace(true)
                  .build()
                  .parse();
        }
      }
    } else if (file.getOriginalFilename().endsWith(".xlsx")) {

      XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
      XSSFSheet worksheet = workbook.getSheetAt(0);
      for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
        XSSFRow row = worksheet.getRow(i);
        listaIdRawValue.add(row != null ? row.getCell(0).getStringCellValue() : null);
      }
    }

    List<ExpedientePosesionNegociada> lista =
        expedientePNRepository.findByIdConcatenadoIn(listaIdRawValue);

    if (!usuario.getRolHaya().getCodigo().equals("admin")) {
      List<ExpedientePosesionNegociada> listaAux = new ArrayList<>();
      for (ExpedientePosesionNegociada expedientePosesionNegociada : lista) {
        if (usuario.contieneExpediente(expedientePosesionNegociada.getId(), true))
          listaAux.add(expedientePosesionNegociada);
      }
      lista = listaAux;
    }

    List<ExpedientePosesionNegociadaDTO> expedientesDTOs = new ArrayList<>();

    for (ExpedientePosesionNegociada expediente : lista) {
      expedientesDTOs.add(expedientePNMapper.parse(expediente));
    }

    return new ListWithCountDTO<>(expedientesDTOs, expedientesDTOs.size());
  }

  public ListWithCountDTO<SkipTracingOutputDTO> buscarSkipTracingsPorExcel(
      MultipartFile file, Usuario usuario)
      throws IOException, IllegalAccessException, NotFoundException, InvocationTargetException {
    if (!file.getOriginalFilename().endsWith(".csv")
        && !file.getOriginalFilename().endsWith(".xlsx"))
      throw new NoSuchFileException("No se reconoce la extensión del archivo");

    List<String> listaIdRawValue = new ArrayList<>();

    if (file.getOriginalFilename().endsWith(".csv")) {
      if (file.isEmpty()) throw new FileNotFoundException("El fichero esta vacío");
      else {
        try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
          listaIdRawValue =
              new CsvToBeanBuilder(reader)
                  .withType(ExpedienteCSVInputDTO.class)
                  .withIgnoreLeadingWhiteSpace(true)
                  .build()
                  .parse();
        }
      }
    } else if (file.getOriginalFilename().endsWith(".xlsx")) {

      XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
      XSSFSheet worksheet = workbook.getSheetAt(0);
      for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
        XSSFRow row = worksheet.getRow(i);
        listaIdRawValue.add(row != null ? row.getCell(0).getStringCellValue() : null);
      }
    }

    List<Integer> ids =
        listaIdRawValue.stream().map(Integer::parseInt).collect(Collectors.toList());

    List<SkipTracing> lista = skipTracingRepository.findAllById(ids);

    if (!usuario.getRolHaya().getCodigo().equals("admin")) {
      List<SkipTracing> listaAux = new ArrayList<>();
      for (SkipTracing expedientePosesionNegociada : lista) {
        if (usuario.contieneExpediente(expedientePosesionNegociada.getId(), true))
          listaAux.add(expedientePosesionNegociada);
      }
      lista = listaAux;
    }

    List<SkipTracingOutputDTO> expedientesDTOs = new ArrayList<>();

    for (SkipTracing expediente : lista) {
      expedientesDTOs.add(skipTracingMapper.parseToOutput(expediente));
    }

    return new ListWithCountDTO<>(expedientesDTOs, expedientesDTOs.size());
  }

  @Override
  public List<IntervinienteBusquedaDTOToList> buscarIntervinientesPorExcel(
      MultipartFile file, String tipo, Usuario principal) throws IOException, NotFoundException {
    if (!file.getOriginalFilename().endsWith(".xlsx"))
      throw new NoSuchFileException("No se reconoce la extensión del archivo");

    List<BusquedaDto> lista = new ArrayList<>();
    List<String> contratos = new ArrayList<>();

    if (file.getOriginalFilename().endsWith(".xlsx")) {
      XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
      XSSFSheet worksheet = workbook.getSheetAt(0);
      for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
        XSSFRow row = worksheet.getRow(i);
        BusquedaDto busquedaDto = new BusquedaDto();

        BusquedaContratoDto contrato = new BusquedaContratoDto();
        DataFormatter formatter = new DataFormatter();
        if (row.getCell(0) != null
            && formatter.formatCellValue(row.getCell(0)) != null
            && !formatter.formatCellValue(row.getCell(0)).equals("")) {
          contrato.setContratoOrigen(formatter.formatCellValue(row.getCell(0)));
          contratos.add(formatter.formatCellValue(row.getCell(0)));
        }
        busquedaDto.setContrato(contrato);

        BusquedaIntervinienteDto interviniente = new BusquedaIntervinienteDto();
        if (row.getCell(1) != null
            && formatter.formatCellValue(row.getCell(1)) != null
            && !formatter.formatCellValue(row.getCell(1)).equals("")) {
          interviniente.setIdIntervinienteOrigen(formatter.formatCellValue(row.getCell(1)));
        }
        if (row.getCell(2) != null
            && formatter.formatCellValue(row.getCell(2)) != null
            && !formatter.formatCellValue(row.getCell(2)).equals("")) {
          interviniente.setTelefono(formatter.formatCellValue(row.getCell(2)));
        }
        if (row.getCell(3) != null
            && formatter.formatCellValue(row.getCell(3)) != null
            && !formatter.formatCellValue(row.getCell(3)).equals("")) {
          interviniente.setEmail(formatter.formatCellValue(row.getCell(3)));
        }
        if (row.getCell(4) != null
            && formatter.formatCellValue(row.getCell(4)) != null
            && !formatter.formatCellValue(row.getCell(4)).equals("")) {
          interviniente.setDireccion(formatter.formatCellValue(row.getCell(4)));
        }
        busquedaDto.setInterviniente(interviniente);
        lista.add(busquedaDto);
      }
    }

    List<IntervinienteBusquedaDTOToList> resultado = new ArrayList<>();

    for (int i = 0; i < lista.size(); i++) {
      var busqueda = lista.get(i);
      if (busqueda != null
          && busqueda.getContrato() != null
          && busqueda.getInterviniente() != null
          && busqueda.getInterviniente().getIdIntervinienteOrigen() != null
          && busqueda.getContrato().getContratoOrigen() != null) {
        if (tipo.equals("firma")) {
          List<IntervinienteBusquedaDTOToList> intervinientesAux =
              getAllFilteredIntervinientes(
                      "email", busqueda, principal, null, null, null, null, null, true)
                  .getList();
          resultado.addAll(
              intervinientesAux.stream()
                  .filter(
                      c ->
                          c.getIdContrato().equals(busqueda.getContrato().getContratoOrigen())
                              && c.getEmail().equals(busqueda.getInterviniente().getEmail()))
                  .collect(Collectors.toList()));
        } else {
          List<IntervinienteBusquedaDTOToList> intervinientesAux =
              getAllFilteredIntervinientes(
                      tipo, busqueda, principal, null, null, null, null, null, false)
                  .getList();
          if (tipo.equals("sms"))
            resultado.addAll(
                intervinientesAux.stream()
                    .filter(
                        c ->
                            c.getIdContrato().equals(busqueda.getContrato().getContratoOrigen())
                                && c.getTelefonoMovil()
                                    .equals(busqueda.getInterviniente().getTelefono()))
                    .collect(Collectors.toList()));
          else if (tipo.equals("email"))
            resultado.addAll(
                intervinientesAux.stream()
                    .filter(
                        c ->
                            c.getIdContrato().equals(busqueda.getContrato().getContratoOrigen())
                                && c.getEmail().equals(busqueda.getInterviniente().getEmail()))
                    .collect(Collectors.toList()));
          else
            resultado.addAll(
                intervinientesAux.stream()
                    .filter(
                        c -> c.getIdContrato().equals(busqueda.getContrato().getContratoOrigen()))
                    .collect(Collectors.toList()));
        }
      }
    }
    return resultado;
  }

  public List<OcupanteBusquedaDTOToList> buscarOcupantesPorExcel(
      MultipartFile file, String tipo, Usuario principal) throws IOException, NotFoundException {
    if (!file.getOriginalFilename().endsWith(".xlsx"))
      throw new NoSuchFileException("No se reconoce la extensión del archivo");

    List<BusquedaDto> lista = new ArrayList<>();
    List<String> bienesPN = new ArrayList<>();

    if (file.getOriginalFilename().endsWith(".xlsx")) {
      XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
      XSSFSheet worksheet = workbook.getSheetAt(0);
      for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
        XSSFRow row = worksheet.getRow(i);
        BusquedaDto busquedaDto = new BusquedaDto();
        BusquedaBienDto busquedaBienDto = new BusquedaBienDto();
        DataFormatter formatter = new DataFormatter();
        if (row.getCell(0) != null
            && formatter.formatCellValue(row.getCell(0)) != null
            && !formatter.formatCellValue(row.getCell(0)).equals("")) {
          busquedaBienDto.setId(formatter.formatCellValue(row.getCell(0)));
          bienesPN.add(formatter.formatCellValue(row.getCell(0)));
        }
        busquedaDto.setBien(busquedaBienDto);

        BusquedaIntervinienteDto interviniente = new BusquedaIntervinienteDto();
        if (row.getCell(1) != null
            && formatter.formatCellValue(row.getCell(1)) != null
            && !formatter.formatCellValue(row.getCell(1)).equals("")) {
          interviniente.setId(Integer.valueOf(formatter.formatCellValue(row.getCell(1))));
        }
        if (row.getCell(2) != null
            && formatter.formatCellValue(row.getCell(2)) != null
            && !formatter.formatCellValue(row.getCell(2)).equals("")) {
          interviniente.setTelefono(formatter.formatCellValue(row.getCell(2)));
        }
        if (row.getCell(3) != null
            && formatter.formatCellValue(row.getCell(3)) != null
            && !formatter.formatCellValue(row.getCell(3)).equals("")) {
          interviniente.setEmail(formatter.formatCellValue(row.getCell(3)));
        }
        if (row.getCell(4) != null
            && formatter.formatCellValue(row.getCell(4)) != null
            && !formatter.formatCellValue(row.getCell(4)).equals("")) {
          interviniente.setDireccion(formatter.formatCellValue(row.getCell(4)));
        }
        busquedaDto.setInterviniente(interviniente);
        lista.add(busquedaDto);
      }
    }

    List<OcupanteBusquedaDTOToList> resultado = new ArrayList<>();

    for (int i = 0; i < lista.size(); i++) {
      var busqueda = lista.get(i);
      if (busqueda.getBien() != null
          && busqueda.getInterviniente() != null
          && busqueda.getInterviniente().getId() != null
          && busqueda.getBien().getId() != null) {
        List<OcupanteBusquedaDTOToList> intervinientesAux =
            getAllFilteredOcupantesGestionMasiva(
                    tipo, busqueda, principal, null, null, null, null, null)
                .getList();
        if (tipo.equals("sms"))
          resultado.addAll(
              intervinientesAux.stream()
                  .filter(
                      c ->
                          c.getIdActivo().equals(busqueda.getBien().getId())
                              && c.getTelefonoMovil()
                                  .equals(busqueda.getInterviniente().getTelefono()))
                  .collect(Collectors.toList()));
        else if (tipo.equals("email"))
          resultado.addAll(
              intervinientesAux.stream()
                  .filter(
                      c ->
                          c.getIdActivo().equals(busqueda.getBien().getId())
                              && c.getEmail().equals(busqueda.getInterviniente().getEmail()))
                  .collect(Collectors.toList()));
        else
          resultado.addAll(
              intervinientesAux.stream()
                  .filter(c -> c.getIdActivo().equals(busqueda.getBien().getId()))
                  .collect(Collectors.toList()));
      }
    }
    return resultado;
  }

  @Override
  @SneakyThrows
  public ListWithCountDTO<BusquedaCampaniaOutputDTO> getAllFilteredCampania(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      Integer size,
      Integer page,
      String idExpediente,
      String idContrato,
      String producto,
      String estado,
      String cliente,
      String cartera,
      String titular,
      String saldoGestion,
      String estrategia,
      String supervisor,
      String area,
      String gestor,
      String campania,
      String idCarga,
      String orderField,
      String orderDirection) {
    List<Expediente> expedientesSinPaginar =
        getDataExpedientesFilter(busquedaDto, loggedUser, null, null);
    if (page == 0) {
      BusquedaReciente busquedaReciente =
          new BusquedaReciente(loggedUser, busquedaDto, expedientesSinPaginar.size());
      busquedaRecienteRepository.save(busquedaReciente);
    }

    /*  List<Integer> intExpedientesId = new ArrayList<>();
    for (Expediente expediente : expedientesSinPaginar) {
      intExpedientesId.add(expediente.getId());
    }*/

    List<BusquedaCampaniaOutputDTO> resultList = new ArrayList<>();
    // ContratosInsertados de la relacion Campaña Asignada
    List<Integer> contratosIdInsertados = new ArrayList<>();

    // Insertamos primero los contratos que tienen relación con campañas
    for (Expediente expediente : expedientesSinPaginar) {
      Contrato contratoRepresentante = expediente.getContratoRepresentante();
      Interviniente primerInterviniente = contratoRepresentante.getPrimerInterviniente();
      Usuario supervisorObt = expediente.getUsuarioByPerfil("Supervisor");
      Usuario gestorObt = expediente.getUsuarioByPerfil("Gestor");

      for (Contrato contrato : expediente.getContratos()) {
        List<CampaniaAsignada> campaniaAsignadaList =
            contrato.getCampaniasAsignadas().stream().collect(Collectors.toList());
        if (!campaniaAsignadaList.isEmpty()) {
          if (busquedaDto.getContrato() != null
              && busquedaDto.getContrato().getCampania() != null) {
            campaniaAsignadaList =
                campaniaAsignadaList.stream()
                    .filter(
                        campaniaAsignada -> {
                          if (campaniaAsignada
                              .getCampania()
                              .getId()
                              .equals(busquedaDto.getContrato().getCampania())) {
                            return true;
                          }
                          return false;
                        })
                    .collect(Collectors.toList());
          }

          for (CampaniaAsignada campaniaAsignada : campaniaAsignadaList) {
            Integer contratoID = campaniaAsignada.getContrato().getId();
            contratosIdInsertados.add(contratoID);

            BusquedaCampaniaOutputDTO busquedaCampaniaOutputDTO =
                busquedaMapper.parseCampaniaBusqueda(campaniaAsignada);
            if (primerInterviniente != null) {
              if (primerInterviniente.getPersonaJuridica() != null
                  && primerInterviniente.getPersonaJuridica()) {
                String raz =
                    primerInterviniente.getRazonSocial() != null
                        ? primerInterviniente.getRazonSocial()
                        : "";
                busquedaCampaniaOutputDTO.setTitular(raz);
              } else {
                String nom =
                    primerInterviniente.getNombre() != null ? primerInterviniente.getNombre() : "";
                String ape =
                    primerInterviniente.getApellidos() != null
                        ? primerInterviniente.getApellidos()
                        : "";
                busquedaCampaniaOutputDTO.setTitular(nom + "" + ape);
              }
            }
            busquedaCampaniaOutputDTO.setSupervisor(
                supervisorObt != null ? supervisorObt.getNombre() : "");
            // Area-A partir del gestor
            // Gestor
            busquedaCampaniaOutputDTO.setArea(
                gestorObt != null && gestorObt.getAreas() != null
                    ? gestorObt.getAreas().stream()
                        .map(AreaUsuario::getNombre)
                        .collect(Collectors.toList())
                    : null);
            busquedaCampaniaOutputDTO.setGestor(gestorObt != null ? gestorObt.getNombre() : "");
            busquedaCampaniaOutputDTO.setCampania(
                campaniaAsignada.getCampania() != null
                    ? campaniaAsignada.getCampania().getNombre()
                    : "");
            resultList.add(busquedaCampaniaOutputDTO);
          }
        } else {
          BusquedaCampaniaOutputDTO busquedaCampaniaOutputDTO = new BusquedaCampaniaOutputDTO();
          busquedaCampaniaOutputDTO.setIdContrato(contrato.getId());
          busquedaCampaniaOutputDTO.setIdExpediente(contrato.getExpediente().getId());
          busquedaCampaniaOutputDTO.setIdConcatenado(
              contrato.getExpediente().getIdConcatenado() != null
                  ? contrato.getExpediente().getIdConcatenado()
                  : "");
          busquedaCampaniaOutputDTO.setProducto(
              contrato.getProducto() != null ? contrato.getProducto().getValor() : "");
          busquedaCampaniaOutputDTO.setEstado(
              contrato.getEstadoContrato() != null ? contrato.getEstadoContrato().getValor() : "");
          busquedaCampaniaOutputDTO.setCliente(
              contrato.getExpediente().getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", ")));
          busquedaCampaniaOutputDTO.setCartera(
              contrato.getExpediente().getCartera().getNombre() != null
                  ? contrato.getExpediente().getCartera().getNombre()
                  : "");
          busquedaCampaniaOutputDTO.setSaldoGestion(contrato.getExpediente().getSaldoGestion());
          busquedaCampaniaOutputDTO.setIdCarga(contrato.getIdCarga());
          busquedaCampaniaOutputDTO.setEstrategia(
              contrato.getEstrategia() != null && contrato.getEstrategia().getEstrategia() != null
                  ? new CatalogoMinInfoDTO(contrato.getEstrategia().getEstrategia())
                  : null);
          if (primerInterviniente != null) {
            if (primerInterviniente.getPersonaJuridica() != null
                && primerInterviniente.getPersonaJuridica()) {
              String raz =
                  primerInterviniente.getRazonSocial() != null
                      ? primerInterviniente.getRazonSocial()
                      : "";
              busquedaCampaniaOutputDTO.setTitular(raz);
            } else {
              String nom =
                  primerInterviniente.getNombre() != null ? primerInterviniente.getNombre() : "";
              String ape =
                  primerInterviniente.getApellidos() != null
                      ? primerInterviniente.getApellidos()
                      : "";
              busquedaCampaniaOutputDTO.setTitular(nom + "" + ape);
            }
          }
          busquedaCampaniaOutputDTO.setSupervisor(
              supervisorObt != null ? supervisorObt.getNombre() : "");
          busquedaCampaniaOutputDTO.setArea(
              gestorObt != null && gestorObt.getAreas() != null
                  ? gestorObt.getAreas().stream()
                      .map(AreaUsuario::getNombre)
                      .collect(Collectors.toList())
                  : null);
          busquedaCampaniaOutputDTO.setGestor(gestorObt != null ? gestorObt.getNombre() : "");
          busquedaCampaniaOutputDTO.setCampania("");
          resultList.add(busquedaCampaniaOutputDTO);
        }
      }
    }
    int listTotalSize = resultList.size();

    // Primero filtra la lista y después la ordena, añadiendo la paginación con el skip
    resultList =
        filtrarListaCampanias(
            resultList,
            idExpediente,
            idContrato,
            producto,
            estado,
            cliente,
            cartera,
            titular,
            saldoGestion,
            estrategia,
            supervisor,
            area,
            gestor,
            campania,
            idCarga);
    resultList =
        ordenarListaCampanias(resultList, orderField, orderDirection).stream()
            .skip(size * page)
            .limit(size)
            .collect(Collectors.toList());
    return new ListWithCountDTO<>(resultList, listTotalSize);
  }

  private List<BusquedaCampaniaOutputDTO> filtrarListaCampanias(
      List<BusquedaCampaniaOutputDTO> lista,
      String idConcatenado,
      String idContrato,
      String producto,
      String estado,
      String cliente,
      String cartera,
      String titular,
      String saldoGestion,
      String estrategia,
      String supervisor,
      String area,
      String gestor,
      String campania,
      String idCarga) {
    if (idConcatenado != null)
      lista =
          lista.stream()
              .filter(c -> c.getIdConcatenado().toLowerCase().contains(idConcatenado))
              .collect(Collectors.toList());
    if (idContrato != null)
      lista =
          lista.stream()
              .filter(c -> c.getIdContrato().toString().toLowerCase().contains(idContrato))
              .collect(Collectors.toList());
    if (producto != null)
      lista =
          lista.stream()
              .filter(c -> c.getProducto().toLowerCase().contains(producto))
              .collect(Collectors.toList());
    if (estado != null)
      lista =
          lista.stream()
              .filter(c -> c.getEstado().toLowerCase().contains(estado))
              .collect(Collectors.toList());
    if (cartera != null)
      lista =
          lista.stream()
              .filter(c -> c.getCartera().toLowerCase().contains(cartera))
              .collect(Collectors.toList());
    if (titular != null)
      lista =
          lista.stream()
              .filter(c -> c.getTitular().toLowerCase().contains(titular))
              .collect(Collectors.toList());
    if (saldoGestion != null)
      lista =
          lista.stream()
              .filter(c -> c.getSaldoGestion().toString().toLowerCase().contains(saldoGestion))
              .collect(Collectors.toList());
    if (estrategia != null)
      lista =
          lista.stream()
              .filter(c -> c.getEstrategia().getValor().toLowerCase().contains(estrategia))
              .collect(Collectors.toList());
    if (supervisor != null)
      lista =
          lista.stream()
              .filter(c -> c.getSupervisor().toLowerCase().contains(supervisor))
              .collect(Collectors.toList());
    if (area != null)
      lista =
          lista.stream()
              .filter(c -> c.getArea().toString().toLowerCase().contains(area))
              .collect(Collectors.toList());
    if (gestor != null)
      lista =
          lista.stream()
              .filter(c -> c.getGestor().toLowerCase().contains(gestor))
              .collect(Collectors.toList());
    if (campania != null)
      lista =
          lista.stream()
              .filter(c -> c.getCampania().toLowerCase().contains(campania))
              .collect(Collectors.toList());
    if (idCarga != null)
      lista =
          lista.stream()
              .filter(c -> c.getIdCarga().toLowerCase().contains(idCarga))
              .collect(Collectors.toList());
    return lista;
  }

  private List<BusquedaCampaniaOutputDTO> ordenarListaCampanias(
      List<BusquedaCampaniaOutputDTO> lista, String orderField, String orderDirection) {
    // En caso de que el orderfield no este contemplado se ordena por ID por defecto
    Comparator<BusquedaCampaniaOutputDTO> comparator =
        Comparator.comparing(BusquedaCampaniaOutputDTO::getIdExpediente);
    // Comparator<BusquedaCampaniaOutputDTO> comparator = (BusquedaCampaniaOutputDTO o1,
    // BusquedaCampaniaOutputDTO o2) -> StringUtils.compare(o1.getIdExpediente(),
    // o2.getIdExpediente());
    lista.sort(comparator);
    if (orderField == null || orderDirection == null) return lista;

    if (orderField.equals("idConcatenado")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getIdConcatenado(), o2.getIdConcatenado());
    } else if (orderField.equals("idContrato")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getIdContrato().toString(), o2.getIdContrato().toString());
    } else if (orderField.equals("producto")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getProducto(), o2.getProducto());
    } else if (orderField.equals("estado")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getEstado(), o2.getEstado());
    } else if (orderField.equals("cliente")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getCliente(), o2.getCliente());
    } else if (orderField.equals("cartera")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getCartera(), o2.getCartera());
    } else if (orderField.equals("titular")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getTitular(), o2.getTitular());
    } else if (orderField.equals("saldoGestion")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getSaldoGestion().toString(), o2.getSaldoGestion().toString());
    } else if (orderField.equals("estrategia")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getEstrategia().getValor(), o2.getEstrategia().getValor());
    } else if (orderField.equals("supervisor")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getSupervisor(), o2.getSupervisor());
    } else if (orderField.equals("areaGestion")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getArea().get(0), o2.getArea().get(0));
    } else if (orderField.equals("gestor")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getGestor(), o2.getGestor());
    } else if (orderField.equals("campania")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getCampania(), o2.getCampania());
    } else if (orderField.equals("idCarga")) {
      comparator =
          (BusquedaCampaniaOutputDTO o1, BusquedaCampaniaOutputDTO o2) ->
              StringUtils.compare(o1.getIdCarga(), o2.getIdCarga());
    }
    if (orderDirection.equals("asc")) lista.sort(comparator);
    else if (orderDirection.equals("desc")) lista.sort(comparator.reversed());
    return lista;
  }

  private boolean like(String str, String expr) {
    expr = expr.toLowerCase();
    expr = expr.replace(".", "\\.");
    expr = expr.replace("?", ".");
    expr = expr.replace("%", ".*");
    str = str.toLowerCase();
    return str.matches(expr);
  }

  @Override
  public ListWithCountDTO<IntervinienteBusquedaDTOToList> getAllFilteredIntervinientes(
      String tipo,
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      Boolean sacarProcedimientos,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page,
      boolean firma)
      throws NotFoundException {

    List<Interviniente> intervinientesSinPaginar =
        getDataIntervinientesFilter(busquedaDto, loggedUser);

    /* TODO FIX TEMPORAL PARA QUE FILTRE BIEN TELEFONOS Y EMAILS, CORREGIR BUSQUEDA */
    if (busquedaDto.getInterviniente() != null) {
      List<Interviniente> intervinienteAux = new ArrayList<>();
      if (busquedaDto.getInterviniente().getTelefono() != null) {
        for (Interviniente interviniente : intervinientesSinPaginar) {
          for (DatoContacto datoContacto : interviniente.getDatosContacto()) {
            if (datoContacto.getMovil() != null
                && datoContacto.getMovil().contains(busquedaDto.getInterviniente().getTelefono()))
              if (!intervinienteAux.contains(interviniente)) intervinienteAux.add(interviniente);
          }
        }
      }
      if (busquedaDto.getInterviniente().getEmail() != null) {
        for (Interviniente interviniente : intervinientesSinPaginar) {
          for (DatoContacto datoContacto : interviniente.getDatosContacto()) {
            if (datoContacto.getEmail() != null
                && datoContacto.getEmail().contains(busquedaDto.getInterviniente().getEmail()))
              if (!intervinienteAux.contains(interviniente)) intervinienteAux.add(interviniente);
          }
        }
      }
      if(!intervinienteAux.isEmpty())
      intervinientesSinPaginar = intervinienteAux;
    }
    if (page != null && page == 0) {
      BusquedaReciente busquedaReciente =
          new BusquedaReciente(loggedUser, busquedaDto, intervinientesSinPaginar.size());
      busquedaRecienteRepository.save(busquedaReciente);
    }
    List<Integer> intervinientesID = new ArrayList<>();
    for (Interviniente interviniente : intervinientesSinPaginar) {
      intervinientesID.add(interviniente.getId());
    }
    List<Integer> idIntervinientesRelacionOrdenTipo = new ArrayList<>();
    List<Map> mapList = intervinienteRepository.findAllIntervinientesOrden(intervinientesID);
    List<IntervinienteBusquedaDTOToList> intervinienteDTOs = new ArrayList<>();

    mapList = mapList.stream().distinct().collect(Collectors.toList());
    // En primer lugar obtenemos los intervinientes que presentan orden y tipoIntervencion //
    // Podemos obtener un listado de intervinientesDTOtoList porque cada interviniente ccuenta con
    // varios datosContacto
    for (Map map : mapList) {
      List<IntervinienteBusquedaDTOToList> intervinienteDTOToList = new ArrayList<>();
      intervinienteDTOToList = intervinienteMapper.parseIntervinienteBusqueda(map, firma);
      intervinienteDTOs.addAll(intervinienteDTOToList);
    }

    if (tipo != null) {
      switch (tipo) {
        case "sms":
          intervinienteDTOs =
              intervinienteDTOs.stream()
                  .filter(
                      intervinienteBusquedaDTOToList -> {
                        if (intervinienteBusquedaDTOToList.getTelefonoMovil() != null
                            && !intervinienteBusquedaDTOToList.getTelefonoMovil().equals("")) {
                          return true;
                        }
                        return false;
                      })
                  .collect(Collectors.toList());
          break;
        case "email":
          intervinienteDTOs =
              intervinienteDTOs.stream()
                  .filter(
                      intervinienteBusquedaDTOToList -> {
                        if (intervinienteBusquedaDTOToList.getEmail() != null
                            && !intervinienteBusquedaDTOToList.getEmail().equals("")) {
                          return true;
                        }
                        return false;
                      })
                  .collect(Collectors.toList());
          break;
      }
    }

    int resultList = intervinienteDTOs.size();
    // Obtenermos los intervinientes que no tienen OrdenIntervencion y TipoIntervencion

    // Filtramos y ordenamos
    /* intervinienteDTOs = filtrarListaIntervinientes(intervinienteDTOs);  */
    intervinienteDTOs = ordenarListaIntervinientes(intervinienteDTOs, orderField, orderDirection);

    if (size != null && page != null && intervinienteDTOs.size() > size) {
      intervinienteDTOs =
          intervinienteDTOs.stream().skip(size * page).limit(size).collect(Collectors.toList());
    } else {
      intervinienteDTOs = ordenarListaIntervinientes(intervinienteDTOs, orderField, orderDirection);
    }

    var listWithCount = new ListWithCountDTO<>(intervinienteDTOs, resultList);
    if (Boolean.TRUE.equals(sacarProcedimientos)) {
      for (var interviniente : listWithCount.getList()) {
        List<Procedimiento> procedimientos =
            procedimientoRepository.findAllByDemandadosId(interviniente.getId());
        List<ProcedimientoDTOToList> procedimientosDto = new ArrayList<>();
        for (var procedimiento : procedimientos) {
          for (var contrato : procedimiento.getContratos()) {
            if (interviniente.getIdContrato().equals(contrato.getIdCarga())) {
              procedimientosDto.add(procedimientoMapper.procedimientoDTOToList(procedimiento));
            }
          }
        }
        interviniente.setProcedimientos(procedimientosDto);
      }
    }
    return listWithCount;
  }

  /**
   * private List<IntervinienteBusquedaDTOToList> filtrarListaIntervinientes(
   * List<IntervinienteBusquedaDTOToList> lista, String nombre, String tipoIntervencion, String
   * ordenIntervencion, String estado, String fechaNacimiento, String documento, String telefono,
   * String tipoDireccion, String direccion, String codigoPostal, String municipio, String
   * telefonoValidado, String emailValidado, String direccionValidado, String email) { if (nombre !=
   * null) lista = lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getNombre()),
   * nombre)).collect(Collectors.toList()); if (tipoIntervencion != null) lista =
   * lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getTipoIntervencion().getValor()),
   * tipoIntervencion)).collect(Collectors.toList()); if (ordenIntervencion != null) lista =
   * lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getOrdenIntervencion().toString()),
   * ordenIntervencion)).collect(Collectors.toList()); // estado revisar lo de vigente y no vigente
   * if (fechaNacimiento != null) { lista = lista.stream().filter(c -> c.getFechaNacimiento() !=
   * null).filter(c ->
   * c.getFechaNacimiento().toString().contains(fechaNacimiento)).collect(Collectors.toList()); } if
   * (documento != null) lista = lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getTipoDocumento().getValor()),
   * documento)).collect(Collectors.toList()); if (telefono != null) lista = lista.stream().filter(c
   * -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getTelefonoPrincipal()),
   * telefono)).collect(Collectors.toList()); if (tipoDireccion != null) lista =
   * lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getTipoDireccion()),
   * tipoDireccion)).collect(Collectors.toList()); if (direccion != null) lista =
   * lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getDireccion()),
   * direccion)).collect(Collectors.toList()); if (codigoPostal != null) lista =
   * lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getCodigoPostal()),
   * codigoPostal)).collect(Collectors.toList()); if (municipio != null) lista =
   * lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getMunicipio()),
   * municipio)).collect(Collectors.toList()); if (estado != null) lista = lista.stream().filter(c
   * -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getActivo().toString()),
   * estado)).collect(Collectors.toList()); if (telefonoValidado != null) lista =
   * lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getTelefonoValidado().toString()),
   * telefonoValidado)).collect(Collectors.toList()); if (emailValidado != null) lista =
   * lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getEmailValidado().toString()),
   * emailValidado)).collect(Collectors.toList()); if (direccionValidado != null) lista =
   * lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getDireccionValidada().toString()),
   * direccionValidado)).collect(Collectors.toList()); if (email != null) lista =
   * lista.stream().filter(c ->
   * StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getEmail()),
   * email)).collect(Collectors.toList()); return lista; }*
   */
  private List<IntervinienteBusquedaDTOToList> ordenarListaIntervinientes(
      List<IntervinienteBusquedaDTOToList> lista, String orderField, String orderDirection) {
    // En caso de que el orderfield no este contemplado se ordena por ID por defecto
    Comparator<IntervinienteBusquedaDTOToList> comparator =
        Comparator.comparing(IntervinienteBusquedaDTOToList::getId);
    // Comparator<BusquedaCampaniaOutputDTO> comparator = (BusquedaCampaniaOutputDTO o1,
    // BusquedaCampaniaOutputDTO o2) -> StringUtils.compare(o1.getIdExpediente(),
    // o2.getIdExpediente());
    lista.sort(comparator);
    if (orderField == null || orderDirection == null) return lista;

    if (orderField.equals("nombre")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getNombre(), o2.getNombre());
    } else if (orderField.equals("tipoIntervencion")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(
                  o1.getTipoIntervencion().getValor(), o2.getTipoIntervencion().getValor());
    } else if (orderField.equals("ordenIntervencion")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(
                  o1.getOrdenIntervencion().toString(), o2.getOrdenIntervencion().toString());
    } else if (orderField.equals("fechaNacimiento")) {
      lista =
          lista.stream()
              .sorted(Comparator.comparing(IntervinienteBusquedaDTOToList::getFechaNacimiento))
              .collect(Collectors.toList());
    } else if (orderField.equals("documento")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getNumeroDocumento(), o2.getNumeroDocumento());
    } else if (orderField.equals("telefono")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getTelefonoPrincipal(), o2.getTelefonoPrincipal());
    } else if (orderField.equals("tipoDireccion")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getTipoDireccion(), o2.getTipoDireccion());
    } else if (orderField.equals("direccion")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getDireccion(), o2.getDireccion());
    } else if (orderField.equals("codigoPostal")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getCodigoPostal(), o2.getCodigoPostal());
    } else if (orderField.equals("municipio")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getMunicipio(), o2.getMunicipio());
    } else if (orderField.equals("telefonoValidado")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(
                  o1.getTelefonoValidado().toString(), o2.getTelefonoValidado().toString());
    } else if (orderField.equals("emailValidado")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(
                  o1.getEmailValidado().toString(), o2.getEmailValidado().toString());
    } else if (orderField.equals("direccionValidado")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(
                  o1.getDireccionValidada().toString(), o2.getDireccionValidada().toString());
    } else if (orderField.equals("email")) {
      comparator =
          (IntervinienteBusquedaDTOToList o1, IntervinienteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getEmail(), o2.getEmail());
    }

    /*  } else if (orderField.equals("estado")) {
      comparator = (IntervinienteDTOToList o1, IntervinienteDTOToList o2) -> StringUtils.compare(o1.getCartera(), o2.getCartera());
    }*/
    if (orderDirection.equals("asc")) lista.sort(comparator);
    else if (orderDirection.equals("desc")) lista.sort(comparator.reversed());
    return lista;
  }

  @Override
  public ListWithCountDTO<OcupanteBusquedaDTOToList> getAllFilteredOcupantesGestionMasiva(
      String tipo,
      BusquedaDto busquedaDto,
      Usuario usuario,
      Boolean sacarProcedimientos,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page)
      throws NotFoundException {

    List<Ocupante> ocupantesSinPaginar =
        ocupanteUseCaseImpl.filtrarOcupantesBusqueda(
            busquedaDto, usuario, orderField, orderDirection);

    /* TODO FIX TEMPORAL PARA QUE FILTRE BIEN TELEFONOS Y EMAILS, CORREGIR BUSQUEDA */
    if (busquedaDto.getInterviniente() != null) {
      List<Ocupante> ocupanteAux = new ArrayList<>();
      if (busquedaDto.getInterviniente().getTelefono() != null) {
        for (Ocupante ocupante : ocupantesSinPaginar) {
          for (DatoContacto datoContacto : ocupante.getDatosContacto()) {
            if (datoContacto.getMovil() != null
              && datoContacto.getMovil().contains(busquedaDto.getInterviniente().getTelefono()))
              if (!ocupanteAux.contains(ocupante)) ocupanteAux.add(ocupante);
          }
        }
      }
      if (busquedaDto.getInterviniente().getEmail() != null) {
        for (Ocupante ocupante : ocupantesSinPaginar) {
          for (DatoContacto datoContacto : ocupante.getDatosContacto()) {
            if (datoContacto.getEmail() != null
              && datoContacto.getEmail().contains(busquedaDto.getInterviniente().getEmail()))
              if (!ocupanteAux.contains(ocupante)) ocupanteAux.add(ocupante);
          }
        }
      }
      if(!ocupanteAux.isEmpty())
        ocupantesSinPaginar = ocupanteAux;
    }
    if (page != null && page == 0) {
      BusquedaReciente busquedaReciente =
          new BusquedaReciente(usuario, busquedaDto, ocupantesSinPaginar.size());
      busquedaRecienteRepository.save(busquedaReciente);
    }
    List<Integer> ocupantesID = new ArrayList<>();
    for (Ocupante ocupante : ocupantesSinPaginar) {
      ocupantesID.add(ocupante.getId());
    }

    List<OcupanteBusquedaDTOToList> ocupanteBusquedaDTOToLists = new ArrayList<>();

    List<Map> mapList = ocupanteRepository.findAllOcupantesOrden(ocupantesID);
    for (Map map : mapList) {
      List<OcupanteBusquedaDTOToList> ocupanteBusquedaDTOToList = new ArrayList<>();
      ocupanteBusquedaDTOToList = ocupanteMapper.parseOcupanteBusqueda(map);
      //  idOcupantesRelacionOrdenTipo.add(ocupanteBusquedaDTOToList.getId());
      ocupanteBusquedaDTOToLists.addAll(ocupanteBusquedaDTOToList);
    }
    if (tipo != null) {
      switch (tipo) {
        case "sms":
          ocupanteBusquedaDTOToLists =
              ocupanteBusquedaDTOToLists.stream()
                  .filter(
                      ocupante -> {
                        if (ocupante.getTelefonoMovil() != null
                            && !ocupante.getTelefonoMovil().equals("")
                            && ocupante
                                .getTelefonoMovil()
                                .equals(busquedaDto.getInterviniente().getTelefono())) {
                          return true;
                        }
                        return false;
                      })
                  .collect(Collectors.toList());
          break;
        case "email":
          ocupanteBusquedaDTOToLists =
              ocupanteBusquedaDTOToLists.stream()
                  .filter(
                      ocupante -> {
                        if (ocupante.getEmail() != null
                            && !ocupante.getEmail().equals("")
                            && ocupante
                                .getEmail()
                                .equals(busquedaDto.getInterviniente().getEmail())) {
                          return true;
                        }
                        return false;
                      })
                  .collect(Collectors.toList());
          break;
      }
    }

    Integer resultsize = ocupanteBusquedaDTOToLists.size();

    /** ocupanteBusquedaDTOToLists = filtrarListaOcupantes(ocupanteBusquedaDTOToLists);* */
    if (size != null && page != null)
      ocupanteBusquedaDTOToLists =
          ordenarListaOcupantes(ocupanteBusquedaDTOToLists, orderField, orderDirection).stream()
              .skip(size * page)
              .limit(size)
              .collect(Collectors.toList());
    else
      ocupanteBusquedaDTOToLists =
          ordenarListaOcupantes(ocupanteBusquedaDTOToLists, orderField, orderDirection);

    if (Boolean.TRUE.equals(sacarProcedimientos)) {
      for (var ocupante : ocupanteBusquedaDTOToLists) {
        List<ProcedimientoPosesionNegociada> procedimientos =
            procedimientoPosesionNegociadaRepository.findAllByDemandadosId(ocupante.getId());
        for (var procedimiento : procedimientos) {
          if (procedimiento.getBien().getId().equals(ocupante.getIdActivo())) {
            List<ProcedimientoPosesionNegociadaDTOToList> procedimientosOcupante =
                new ArrayList<>();
            if (ocupante.getProcedimientos() != null)
              procedimientosOcupante = ocupante.getProcedimientos();
            procedimientosOcupante.add(new ProcedimientoPosesionNegociadaDTOToList(procedimiento));
            ocupante.setProcedimientos(procedimientosOcupante);
          }
        }
        if (ocupante.getProcedimientos() == null) {
          ocupante.setProcedimientos(new ArrayList<>());
        }
      }
    }
    var devolver = new ListWithCountDTO<>(ocupanteBusquedaDTOToLists, resultsize);
    return devolver;
  }

  /**
   * private List<OcupanteBusquedaDTOToList> filtrarListaOcupantes( List<OcupanteBusquedaDTOToList>
   * lista, String idOcupante, String nombre, String tipoOcupante, String orden, String activo,
   * String telefonoMovil, String telefonoFijo, String numeroDocumento, String tipoDireccion, String
   * direccion, String codigoPostal, String municipio, String email, String telefonoValidado, String
   * emailValidado, String direccionValidada) { if (idOcupante != null) lista = lista.stream()
   * .filter( c -> StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getId().toString()),
   * idOcupante)) .collect(Collectors.toList()); if (nombre != null) lista = lista.stream() .filter(
   * c -> StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getNombre()), nombre))
   * .collect(Collectors.toList()); if (tipoOcupante != null) lista = lista.stream() .filter( c ->
   * StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getTipoOcupante().getValor()),
   * tipoOcupante)) .collect(Collectors.toList()); if (orden != null) lista = lista.stream()
   * .filter( c -> StringUtils.startsWithIgnoreCase(
   * StringUtils.stripAccents(c.getOrden().toString()), orden)) .collect(Collectors.toList());
   *
   * <p>// Activo vigente o no vigente?? if (activo != null) lista = lista.stream() .filter( c ->
   * StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getActivo().toString()), activo))
   * .collect(Collectors.toList()); if (telefonoMovil != null) lista = lista.stream() .filter( c ->
   * StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getTelefonoMovil()),
   * telefonoMovil)) .collect(Collectors.toList()); if (telefonoFijo != null) lista = lista.stream()
   * .filter( c -> StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getTelefonoFijo()),
   * telefonoFijo)) .collect(Collectors.toList()); if (numeroDocumento != null) lista =
   * lista.stream() .filter( c -> StringUtils.startsWithIgnoreCase(
   * StringUtils.stripAccents(c.getNumeroDocumento()), numeroDocumento))
   * .collect(Collectors.toList()); if (tipoDireccion != null) lista = lista.stream() .filter( c ->
   * StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getTipoDireccion()),
   * tipoDireccion)) .collect(Collectors.toList()); if (direccion != null) lista = lista.stream()
   * .filter( c -> StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getDireccion()),
   * direccion)) .collect(Collectors.toList()); if (codigoPostal != null) lista = lista.stream()
   * .filter( c -> StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getCodigoPostal()),
   * codigoPostal)) .collect(Collectors.toList()); if (municipio != null) lista = lista.stream()
   * .filter( c -> StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getMunicipio()),
   * municipio)) .collect(Collectors.toList()); if (email != null) lista = lista.stream() .filter( c
   * -> StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getEmail()), email))
   * .collect(Collectors.toList()); if (telefonoValidado != null) lista = lista.stream() .filter( c
   * -> StringUtils.startsWithIgnoreCase(
   * StringUtils.stripAccents(c.getTelefonoValidado().toString()), telefonoValidado))
   * .collect(Collectors.toList()); if (emailValidado != null) lista = lista.stream() .filter( c ->
   * StringUtils.startsWithIgnoreCase( StringUtils.stripAccents(c.getEmailValidado().toString()),
   * emailValidado)) .collect(Collectors.toList()); if (direccionValidada != null) lista =
   * lista.stream() .filter( c -> StringUtils.startsWithIgnoreCase(
   * StringUtils.stripAccents(c.getDireccionValidada().toString()), direccionValidada))
   * .collect(Collectors.toList()); return lista; }*
   */
  private List<OcupanteBusquedaDTOToList> ordenarListaOcupantes(
      List<OcupanteBusquedaDTOToList> lista, String orderField, String orderDirection) {
    // En caso de que el orderfield no este contemplado se ordena por ID por defecto
    Comparator<OcupanteBusquedaDTOToList> comparator =
        Comparator.comparing(OcupanteBusquedaDTOToList::getId);
    // Comparator<BusquedaCampaniaOutputDTO> comparator = (BusquedaCampaniaOutputDTO o1,
    // BusquedaCampaniaOutputDTO o2) -> StringUtils.compare(o1.getIdExpediente(),
    // o2.getIdExpediente());
    lista.sort(comparator);
    if (orderField == null || orderDirection == null) return lista;

    if (orderField.equals("nombre")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getNombre(), o2.getNombre());
    } else if (orderField.equals("tipoOcupante")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getTipoOcupante().getValor(), o2.getTipoOcupante().getValor());
    } else if (orderField.equals("orden")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getOrden().toString(), o2.getOrden().toString());
    } else if (orderField.equals("estado")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getActivo().toString(), o2.getActivo().toString());
    } else if (orderField.equals("telefono")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getTelefonoMovil(), o2.getTelefonoMovil());
    } else if (orderField.equals("telefonoFijo")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getTelefonoFijo(), o2.getTelefonoFijo());
    } else if (orderField.equals("telefonoMovil")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getTelefonoMovil(), o2.getTelefonoMovil());
    } else if (orderField.equals("numeroDocumento")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getNumeroDocumento(), o2.getNumeroDocumento());
    } else if (orderField.equals("tipoDireccion")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getTipoDireccion(), o2.getTipoDireccion());
    } else if (orderField.equals("direccion")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getDireccion(), o2.getDireccion());
    } else if (orderField.equals("codigoPostal")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getCodigoPostal(), o2.getCodigoPostal());
    } else if (orderField.equals("municipio")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getMunicipio(), o2.getMunicipio());
    } else if (orderField.equals("email")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(o1.getActivo().toString(), o2.getActivo().toString());
    } else if (orderField.equals("telefonoValidado")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(
                  o1.getTelefonoValidado().toString(), o2.getTelefonoValidado().toString());
    } else if (orderField.equals("emailValidado")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(
                  o1.getEmailValidado().toString(), o2.getEmailValidado().toString());
    } else if (orderField.equals("direccionValidada")) {
      comparator =
          (OcupanteBusquedaDTOToList o1, OcupanteBusquedaDTOToList o2) ->
              StringUtils.compare(
                  o1.getDireccionValidada().toString(), o2.getDireccionValidada().toString());
    }

    if (orderDirection.equals("asc")) lista.sort(comparator);
    else if (orderDirection.equals("desc")) lista.sort(comparator.reversed());
    return lista;
  }

  @Override
  public ListWithCountDTO<BusquedaCampaniaPNOutputDTO> getAllFilteredCampaniaPN(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      Integer size,
      Integer page,
      String idConcatenado,
      String idBienPN,
      String estado,
      String cliente,
      String cartera,
      String saldoGestion,
      String estrategia,
      String supervisor,
      String area,
      String gestor,
      String campania,
      String orderField,
      String orderDirection)
      throws NotFoundException {
    List<ExpedientePosesionNegociada> expedientesSinPaginar =
        expedientePosesionNegociadaCase.filtrarExpedientesPosesionNegociadaBusqueda(
            busquedaDto,
            loggedUser,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            orderField,
            orderDirection);
    if (page == 0) {
      BusquedaReciente busquedaReciente =
          new BusquedaReciente(loggedUser, busquedaDto, expedientesSinPaginar.size());
      busquedaRecienteRepository.save(busquedaReciente);
    }

    List<Integer> intExpedientesId = new ArrayList<>();
    for (ExpedientePosesionNegociada expedientePosesionNegociada : expedientesSinPaginar) {
      intExpedientesId.add(expedientePosesionNegociada.getId());
    }
    List<BusquedaCampaniaPNOutputDTO> resultList = new ArrayList<>();
    // ContratosInsertados de la relacion Campaña Asignada
    List<Integer> bienesIdInsertados = new ArrayList<>();

    // Insertamos primero los contratos que tienen relación con campañas
    for (ExpedientePosesionNegociada expedientePN : expedientesSinPaginar) {

      for (BienPosesionNegociada bienPN : expedientePN.getBienesPosesionNegociada()) {
        List<CampaniaAsignadaBienPN> campaniaAsignadaBienPNList =
            bienPN.getCampaniasAsignadasPN().stream().collect(Collectors.toList());
        if (!campaniaAsignadaBienPNList.isEmpty()) {

          for (CampaniaAsignadaBienPN campaniaAsignadaPN : campaniaAsignadaBienPNList) {
            Integer bienPNID = campaniaAsignadaPN.getBienPosesionNegociada().getId();
            bienesIdInsertados.add(bienPNID);
            BusquedaCampaniaPNOutputDTO busquedaCampaniaPNOutputDTO =
                busquedaMapper.parseCampaniaBusquedaPN(campaniaAsignadaPN);
            resultList.add(busquedaCampaniaPNOutputDTO);
          }
        } else {
          BusquedaCampaniaPNOutputDTO busquedaCampaniaPNOutputDTO =
              busquedaMapper.parseCampaniaSinRelacionPN(bienPN);
          resultList.add(busquedaCampaniaPNOutputDTO);
        }
      }
    }

    if (busquedaDto.getContrato() != null && busquedaDto.getContrato().getCampania() != null) {
      resultList =
          resultList.stream()
              .filter(
                  busquedaCampaniaPNOutputDTO -> {
                    if (busquedaDto
                        .getContrato()
                        .getCampania()
                        .equals(busquedaCampaniaPNOutputDTO.getIdCampania())) {
                      return true;
                    }
                    return false;
                  })
              .collect(Collectors.toList());
    }

    int listTotalSize = resultList.size();

    // Primero filtra la lista y después la ordena, añadiendo la paginación con el skip
    resultList =
        filtrarListaCampaniasPN(
            resultList,
            idConcatenado,
            idBienPN,
            estado,
            cliente,
            cartera,
            saldoGestion,
            estrategia,
            supervisor,
            area,
            gestor,
            campania);
    resultList =
        ordenarListaCampaniasPN(resultList, orderField, orderDirection).stream()
            .skip(size * page)
            .limit(size)
            .collect(Collectors.toList());

    return new ListWithCountDTO<>(resultList, listTotalSize);
  }

  private List<BusquedaCampaniaPNOutputDTO> filtrarListaCampaniasPN(
      List<BusquedaCampaniaPNOutputDTO> lista,
      String idConcatenado,
      String idBienPN,
      String estado,
      String cliente,
      String cartera,
      String saldoGestion,
      String estrategia,
      String supervisor,
      String area,
      String gestor,
      String campania) {

    if (idConcatenado != null)
      lista =
          lista.stream()
              .filter(c -> c.getIdConcatenado().toLowerCase().contains(idConcatenado))
              .collect(Collectors.toList());
    if (idBienPN != null)
      lista =
          lista.stream()
              .filter(c -> c.getIdBienPN().toString().toLowerCase().contains(idBienPN))
              .collect(Collectors.toList());
    if (estado != null)
      lista =
          lista.stream()
              .filter(c -> c.getEstado().toLowerCase().contains(estado))
              .collect(Collectors.toList());
    if (cartera != null)
      lista =
          lista.stream()
              .filter(c -> c.getCartera().toLowerCase().contains(cartera))
              .collect(Collectors.toList());
    if (cliente != null)
      lista =
          lista.stream()
              .filter(c -> c.getCliente().toLowerCase().contains(cliente))
              .collect(Collectors.toList());
    if (saldoGestion != null)
      lista =
          lista.stream()
              .filter(c -> c.getSaldoGestion().toString().toLowerCase().contains(saldoGestion))
              .collect(Collectors.toList());
    if (estrategia != null)
      lista =
          lista.stream()
              .filter(c -> c.getEstrategia().toString().toLowerCase().contains(estrategia))
              .collect(Collectors.toList());
    if (supervisor != null)
      lista =
          lista.stream()
              .filter(c -> c.getSupervisor().toLowerCase().contains(supervisor))
              .collect(Collectors.toList());
    if (area != null)
      lista =
          lista.stream()
              .filter(c -> c.getArea().toString().toLowerCase().contains(area))
              .collect(Collectors.toList());
    if (gestor != null)
      lista =
          lista.stream()
              .filter(c -> c.getGestor().toLowerCase().contains(gestor))
              .collect(Collectors.toList());
    if (campania != null)
      lista =
          lista.stream()
              .filter(c -> c.getCampania().toLowerCase().contains(campania))
              .collect(Collectors.toList());
    return lista;
  }

  private List<BusquedaCampaniaPNOutputDTO> ordenarListaCampaniasPN(
      List<BusquedaCampaniaPNOutputDTO> lista, String orderField, String orderDirection) {
    // En caso de que el orderfield no este contemplado se ordena por ID por defecto
    Comparator<BusquedaCampaniaPNOutputDTO> comparator =
        Comparator.comparing(BusquedaCampaniaPNOutputDTO::getIdExpediente);
    // Comparator<BusquedaCampaniaOutputDTO> comparator = (BusquedaCampaniaOutputDTO o1,
    // BusquedaCampaniaOutputDTO o2) -> StringUtils.compare(o1.getIdExpediente(),
    // o2.getIdExpediente());
    lista.sort(comparator);
    if (orderField == null || orderDirection == null) return lista;

    if (orderField.equals("idConcatenado")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getIdConcatenado(), o2.getIdConcatenado());
    } else if (orderField.equals("idBienPN")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getIdBienPN().toString(), o2.getIdBienPN().toString());
    } else if (orderField.equals("estado")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getEstado(), o2.getEstado());
    } else if (orderField.equals("cliente")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getCliente(), o2.getCliente());
    } else if (orderField.equals("cartera")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getCartera(), o2.getCartera());
    } else if (orderField.equals("saldoGestion")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getSaldoGestion().toString(), o2.getSaldoGestion().toString());
    } else if (orderField.equals("estrategia")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getEstrategia().getValor(), o2.getEstrategia().getValor());
    } else if (orderField.equals("supervisor")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getSupervisor(), o2.getSupervisor());
    } else if (orderField.equals("areaGestion")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getArea().get(0), o2.getArea().get(0));
    } else if (orderField.equals("gestor")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getGestor(), o2.getGestor());
    } else if (orderField.equals("campania")) {
      comparator =
          (BusquedaCampaniaPNOutputDTO o1, BusquedaCampaniaPNOutputDTO o2) ->
              StringUtils.compare(o1.getCampania(), o2.getCampania());
    }
    if (orderDirection.equals("asc")) lista.sort(comparator);
    else if (orderDirection.equals("desc")) lista.sort(comparator.reversed());
    return lista;
  }
}
