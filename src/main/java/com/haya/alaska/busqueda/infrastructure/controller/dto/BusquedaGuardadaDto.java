package com.haya.alaska.busqueda.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaGuardadaDto extends BusquedaRecienteDto {
  private String nombre;
}
