package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaAgendaDto {

    Integer tipoEvento;
    List<Integer> tiposEvento;
    List<Integer> subtiposEvento;
    List<Integer> claseEvento;
    String emisor;
    String destinatario;
    List<Integer> estado;
    List<IntervaloFecha> intervaloFechas;
}
