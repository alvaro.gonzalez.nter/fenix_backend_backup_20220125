package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaEstimacionDto {

    Integer tipoPropuesta;
    Integer subtipoPropuesta;
    Integer probalididadResolucion;
    List<IntervaloFecha> intervaloFechas;
}
