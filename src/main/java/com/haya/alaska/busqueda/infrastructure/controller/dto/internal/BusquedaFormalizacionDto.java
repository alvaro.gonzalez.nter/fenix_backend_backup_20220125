package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaFormalizacionDto {

    String gestor;
    String gestorCliente;
    Boolean documentacionCumplimentada;
    Boolean pbc;

}
