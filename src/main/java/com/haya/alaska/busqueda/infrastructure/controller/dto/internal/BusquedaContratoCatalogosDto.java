package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaContratoCatalogosDto {

  String id;
  CatalogoMinInfoDTO estado;
  CatalogoMinInfoDTO producto;
  Boolean reestructurado;
  CatalogoMinInfoDTO clasificacionContable;
  String contratoOrigen;
  CatalogoMinInfoDTO tipoEstimacion;
  CatalogoMinInfoDTO subtipoEstimacion;
  Integer numCuotasImpago;
  IntervaloImporteDescripcion intervaloImporte;
  List<IntervaloFecha> intervaloFechas;
}
