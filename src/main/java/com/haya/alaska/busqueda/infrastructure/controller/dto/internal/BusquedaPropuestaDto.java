package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaPropuestaDto {

    Integer clase;
    Integer tipo;
    Integer subtipo;
    Integer estado;
    Integer sancion;
    Integer resolucion;
    String gestor;
    String analista;
    String gestorRecuperaciones;
    String notaria;
    BusquedaFormalizacionDto formalizacion;
    BusquedaFormalizacionCompletaDto formalizacionCompleta;
    IntervaloImporteDescripcion intervaloImporte;
    List<IntervaloFecha> intervaloFechas;
}
