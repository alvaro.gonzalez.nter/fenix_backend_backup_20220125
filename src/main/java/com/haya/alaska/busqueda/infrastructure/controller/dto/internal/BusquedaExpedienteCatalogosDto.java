package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaExpedienteCatalogosDto {

  String id;
  CatalogoMinInfoDTO grupoCliente;
  CatalogoMinInfoDTO cliente;
  CatalogoMinInfoDTO cartera;
  String responsableCartera;
  String gestor;
  String supervisor;
  CatalogoMinInfoDTO estado;
  CatalogoMinInfoDTO subestado;
  CatalogoMinInfoDTO situacion;
  CatalogoMinInfoDTO estrategia;
  String campania;
  IntervaloImporte saldoGestion;
  List<IntervaloFecha> intervaloFechas;
}
