package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaBienDto {

  String id;
  String idOrigen;
  String idMixto;
  String direccion;
  Integer localidad;
  Integer municipio;
  Integer provincia;
  String localidadRegistro;
  String registro;
  String fincaRegistral;
  String referenciaCatastral;
  String idufir;
  String uso;
  String tipoGarantia;
  IntervaloImporteDescripcion intervaloImporte;
  List<IntervaloFecha> intervaloFechas;
}
