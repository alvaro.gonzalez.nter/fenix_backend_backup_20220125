package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaFormalizacionCompletaDto {
  Integer provinciaActivos;
  Integer subestadoFormalizacion;
  Integer resultadoCheck;
  Date firmaEstimada;
  String propuestaOrigen;
  String gestoria;
  String despachoLegal;
  String despachoTecnico;

  String gestorFormalizacionLegal;
  String gestorFormalizacionPrincipal;
  Integer gestorConcursos;
  Integer gestorGestoria;
  String gestorRecuperacionesCliente;
  String territorial;
  String gestorFormalizacionLetradoCliente;
  Boolean concurso;
  Double precioDeCompra;
  Double importeEntregaDeEfectivo;
  Boolean entregaDeBienesLibres;
  Integer tipologia;
  Integer subtipoActivo;
  String direccionFormalizacion;
  Boolean riesgoConsignacion;
  Integer tipoCargaFormalizacion;
  Boolean cargaPropia;
  Integer tipologiaCarga;
  Integer subtipoCarga;
}
