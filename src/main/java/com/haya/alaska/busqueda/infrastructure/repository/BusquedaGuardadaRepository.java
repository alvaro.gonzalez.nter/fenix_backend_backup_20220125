package com.haya.alaska.busqueda.infrastructure.repository;

import com.haya.alaska.busqueda.domain.BusquedaGuardada;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BusquedaGuardadaRepository extends JpaRepository<BusquedaGuardada, Integer> {

  List<BusquedaGuardada> findAllByUsuario(Usuario usuario);
  Optional<BusquedaGuardada> findByIdAndUsuario(int id, Usuario usuario);
}
