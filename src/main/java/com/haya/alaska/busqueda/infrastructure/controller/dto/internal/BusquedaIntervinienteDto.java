package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaIntervinienteDto {

    Integer id;
    String idIntervinienteOrigen;
    String docId;
    String apellidos;
    String nombre;
    String direccion;
    Integer localidad;
    Integer provincia;
    String telefono;
    String email;
    Boolean vulnerabilidad;
    Integer tipoIntervencion;
    Integer residencia;
    Integer nacionalidad;
    List<IntervaloFecha> intervaloFecha;
    String nombreCompleto;
}
