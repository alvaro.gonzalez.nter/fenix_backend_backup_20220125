package com.haya.alaska.busqueda.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaGuardadaInputDto {
  private String nombre;
  private BusquedaDto consulta;
}
