package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class IntervaloFecha {

    @NotNull
    String descripcion;
    Date fechaInicio;
    Date fechaFin;

}
