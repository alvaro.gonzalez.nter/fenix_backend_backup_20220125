package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaContratoDto {

    String id;
    Integer estado;
    Integer producto;
    Boolean reestructurado;
    Integer clasificacionContable;
    String contratoOrigen;
    Integer tipoEstimacion;
    Integer subtipoEstimacion;
    Integer numCuotasImpago;
    IntervaloImporteDescripcion intervaloImporte;
    List<IntervaloFecha> intervaloFechas;
    Integer campania;
}
