package com.haya.alaska.busqueda.infrastructure.repository;

import com.haya.alaska.busqueda.domain.BusquedaReciente;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BusquedaRecienteRepository extends JpaRepository<BusquedaReciente, Integer> {
  List<BusquedaReciente> findTop10ByUsuarioOrderByFechaDesc(Usuario usuario);
}
