package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaMovimientoDto {

    Integer tipoMovimiento;
    IntervaloImporte intervaloImporte;
    List<IntervaloFecha> intervaloFechas;
}
