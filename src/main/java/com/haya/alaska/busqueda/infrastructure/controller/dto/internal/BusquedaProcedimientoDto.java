package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaProcedimientoDto {

    String letrado;
    String procurador;
    String juzgado;
    String administradorConcursal;
    Integer tipoProcedimiento;
    Integer hitoProcedimiento;
    IntervaloFecha intervaloFecha;
    String numeroAutos;
}
