package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaEstimacionCatalogosDto {

  CatalogoMinInfoDTO tipoPropuesta;
  CatalogoMinInfoDTO subtipoPropuesta;
  Integer probalididadResolucion;
  List<IntervaloFecha> intervaloFechas;
}
