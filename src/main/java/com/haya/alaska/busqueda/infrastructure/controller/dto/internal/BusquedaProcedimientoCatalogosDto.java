package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaProcedimientoCatalogosDto {

  String letrado;
  String procurador;
  String juzgado;
  String administradorConcursal;
  CatalogoMinInfoDTO tipoProcedimiento;
  CatalogoMinInfoDTO hitoProcedimiento;
  IntervaloFecha intervaloFecha;
}
