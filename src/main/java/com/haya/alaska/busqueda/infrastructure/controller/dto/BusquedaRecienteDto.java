package com.haya.alaska.busqueda.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaRecienteDto {
  private int id;
  private int numeroResultados;
  private Date fecha;
  private BusquedaCatalogosDto consulta;
}
