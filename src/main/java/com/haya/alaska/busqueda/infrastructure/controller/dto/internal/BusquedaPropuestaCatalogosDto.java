package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaPropuestaCatalogosDto {

  CatalogoMinInfoDTO clase;
  CatalogoMinInfoDTO tipo;
  CatalogoMinInfoDTO subtipo;
  CatalogoMinInfoDTO estado;
  CatalogoMinInfoDTO sancion;
  CatalogoMinInfoDTO resolucion;
  String gestor;
  String analista;
  String gestorRecuperaciones;
  String notaria;
  BusquedaFormalizacionDto formalizacion;
  IntervaloImporteDescripcion intervaloImporte;
  List<IntervaloFecha> intervaloFechas;
}
