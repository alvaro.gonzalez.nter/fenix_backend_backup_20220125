package com.haya.alaska.busqueda.infrastructure.controller.dto;

import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaDto {

    BusquedaExpedienteDto expediente;
    BusquedaContratoDto contrato;
    BusquedaIntervinienteDto interviniente;
    BusquedaBienDto bien;
    BusquedaMovimientoDto movimientos;
    BusquedaEstimacionDto estimaciones;
    BusquedaProcedimientoDto procedimiento;
    BusquedaPropuestaDto propuestas;
    BusquedaAgendaDto agenda;

}
