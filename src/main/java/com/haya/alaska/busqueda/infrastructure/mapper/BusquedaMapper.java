package com.haya.alaska.busqueda.infrastructure.mapper;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.busqueda.domain.BusquedaGuardada;
import com.haya.alaska.busqueda.domain.BusquedaReciente;
import com.haya.alaska.busqueda.infrastructure.controller.dto.*;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.*;
import com.haya.alaska.campania.infraestructure.controller.dto.BusquedaCampaniaOutputDTO;
import com.haya.alaska.campania.infraestructure.controller.dto.BusquedaCampaniaPNOutputDTO;
import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.grupo_cliente.domain.GrupoCliente;
import com.haya.alaska.grupo_cliente.infrastructure.repository.GrupoClienteRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class BusquedaMapper {
  @Autowired
  private Map<String, CatalogoRepository<? extends Catalogo, Integer>> catalogoRepositoryMap;

  @Autowired
  private GrupoClienteRepository grupoClienteRepository;

  @Autowired
  private ClienteRepository clienteRepository;

  @Autowired
  private CarteraRepository carteraRepository;

  @Autowired
  private CatalogoMinInfoMapper catalogoMinInfoMapper;

  public BusquedaRecienteDto busquedaRecienteToDto(BusquedaReciente busquedaReciente) {
    BusquedaRecienteDto dto = new BusquedaRecienteDto();
    dto.setId(busquedaReciente.getId());
    dto.setNumeroResultados(busquedaReciente.getNumeroResultados());
    dto.setFecha(busquedaReciente.getFecha());
    dto.setConsulta(rellenarCatalogos(busquedaReciente.parseJsonConsulta()));
    return dto;
  }

  public BusquedaGuardadaDto busquedaGuardadaToDto(BusquedaGuardada busquedaGuardada) {
    BusquedaGuardadaDto dto = new BusquedaGuardadaDto();
    dto.setId(busquedaGuardada.getId());
    dto.setNumeroResultados(busquedaGuardada.getNumeroResultados());
    dto.setFecha(busquedaGuardada.getFecha());
    dto.setConsulta(rellenarCatalogos(busquedaGuardada.parseJsonConsulta()));
    dto.setNombre(busquedaGuardada.getNombre());
    return dto;
  }

  public BusquedaGuardada parseBusquedaGuardadaDto(Usuario usuario, BusquedaGuardadaInputDto busquedaGuardadaDto) {
    return new BusquedaGuardada(usuario, busquedaGuardadaDto.getNombre(), busquedaGuardadaDto.getConsulta());
  }

  public BusquedaCatalogosDto rellenarCatalogos(BusquedaDto origen) {
    BusquedaCatalogosDto destino = new BusquedaCatalogosDto();
    if (origen.getExpediente() != null) {
      var dto = new BusquedaExpedienteCatalogosDto();
      destino.setExpediente(dto);
      BeanUtils.copyProperties(origen.getExpediente(), dto);
      if (origen.getExpediente().getGrupoCliente() != null) {
        GrupoCliente entidad = this.grupoClienteRepository.findById(origen.getExpediente().getGrupoCliente()).orElse(null);
        if (entidad != null) {
          dto.setGrupoCliente(crearCatalogoMinInfo(entidad.getId(), entidad.getNombre()));
        }
      }
      if (origen.getExpediente().getCliente() != null) {
        Cliente entidad = this.clienteRepository.findById(origen.getExpediente().getCliente()).orElse(null);
        if (entidad != null) {
          dto.setCliente(crearCatalogoMinInfo(entidad.getId(), entidad.getNombre()));
        }
      }
      if (origen.getExpediente().getCartera() != null) {
        Cartera entidad = this.carteraRepository.findById(origen.getExpediente().getCartera()).orElse(null);
        if (entidad != null) {
          dto.setCartera(crearCatalogoMinInfo(entidad.getId(), entidad.getNombre()));
        }
      }
      dto.setEstado(buscar("tipoEstado", origen.getExpediente().getEstado()));
      dto.setSubestado(buscar("tipoEstado", origen.getExpediente().getSubestado()));
      dto.setSituacion(buscar("situacion", origen.getExpediente().getSituacion()));
      dto.setEstrategia(buscar("estrategia", origen.getExpediente().getEstrategia()));
    }
    if (origen.getContrato() != null) {
      var dto = new BusquedaContratoCatalogosDto();
      destino.setContrato(dto);
      BeanUtils.copyProperties(origen.getContrato(), dto);
      dto.setEstado(buscar("estadoContrato", origen.getContrato().getEstado()));
      dto.setProducto(buscar("producto", origen.getContrato().getProducto()));
      dto.setClasificacionContable(buscar("clasificacionContable", origen.getContrato().getClasificacionContable()));
      dto.setTipoEstimacion(buscar("tipoPropuesta", origen.getContrato().getTipoEstimacion()));
      dto.setSubtipoEstimacion(buscar("subtipoPropuesta", origen.getContrato().getSubtipoEstimacion()));
    }
    if (origen.getInterviniente() != null) {
      var dto = new BusquedaIntervinienteCatalogosDto();
      destino.setInterviniente(dto);
      BeanUtils.copyProperties(origen.getInterviniente(), dto);
      dto.setLocalidad(buscar("localidad", origen.getInterviniente().getLocalidad()));
      dto.setProvincia(buscar("provincia", origen.getInterviniente().getProvincia()));
      dto.setTipoIntervencion(buscar("tipoIntervencion", origen.getInterviniente().getTipoIntervencion()));
      dto.setResidencia(buscar("pais", origen.getInterviniente().getResidencia()));
      dto.setNacionalidad(buscar("pais", origen.getInterviniente().getNacionalidad()));
    }
    if (origen.getBien() != null) {
      var dto = new BusquedaBienCatalogosDto();
      destino.setBien(dto);
      BeanUtils.copyProperties(origen.getBien(), dto);
      dto.setLocalidad(buscar("localidad", origen.getBien().getLocalidad()));
      dto.setMunicipio(buscar("municipio", origen.getBien().getMunicipio()));
      dto.setProvincia(buscar("provincia", origen.getBien().getProvincia()));
    }
    if (origen.getMovimientos() != null) {
      var dto = new BusquedaMovimientoCatalogosDto();
      destino.setMovimientos(dto);
      BeanUtils.copyProperties(origen.getMovimientos(), dto);
      dto.setTipoMovimiento(buscar("tipoMovimiento", origen.getMovimientos().getTipoMovimiento()));
    }
    if (origen.getEstimaciones() != null) {
      var dto = new BusquedaEstimacionCatalogosDto();
      destino.setEstimaciones(dto);
      BeanUtils.copyProperties(origen.getEstimaciones(), dto);
      dto.setTipoPropuesta(buscar("tipoPropuesta", origen.getEstimaciones().getTipoPropuesta()));
      dto.setSubtipoPropuesta(buscar("subtipoPropuesta", origen.getEstimaciones().getSubtipoPropuesta()));
    }
    if (origen.getProcedimiento() != null) {
      var dto = new BusquedaProcedimientoCatalogosDto();
      destino.setProcedimiento(dto);
      BeanUtils.copyProperties(origen.getProcedimiento(), dto);
      dto.setTipoProcedimiento(buscar("tipoProcedimiento", origen.getProcedimiento().getTipoProcedimiento()));
      dto.setHitoProcedimiento(buscar("hitoProcedimiento", origen.getProcedimiento().getHitoProcedimiento()));
    }
    if (origen.getPropuestas() != null) {
      var dto = new BusquedaPropuestaCatalogosDto();
      destino.setPropuestas(dto);
      BeanUtils.copyProperties(origen.getPropuestas(), dto);
      dto.setClase(buscar("clasePropuesta", origen.getPropuestas().getClase()));
      dto.setTipo(buscar("tipoPropuesta", origen.getPropuestas().getTipo()));
      dto.setSubtipo(buscar("subtipoPropuesta", origen.getPropuestas().getSubtipo()));
      dto.setEstado(buscar("estadoPropuesta", origen.getPropuestas().getEstado()));
      dto.setSancion(buscar("sancionPropuesta", origen.getPropuestas().getSancion()));
      dto.setResolucion(buscar("resolucionPropuesta", origen.getPropuestas().getResolucion()));
    }
    if (origen.getAgenda() != null) {
      var dto = new BusquedaAgendaCatalogosDto();
      destino.setAgenda(dto);
      BeanUtils.copyProperties(origen.getAgenda(), dto);
      dto.setClaseEvento(buscar("claseEvento", origen.getAgenda().getClaseEvento()));
      dto.setTipoEvento(buscar("tipoEvento", origen.getAgenda().getTipoEvento()));
      dto.setEstado(buscar("estadoEvento", origen.getAgenda().getEstado()));
    }
    return destino;
  }

  private CatalogoMinInfoDTO buscar(String nombreCatalogo, Integer id) {
    if (id == null) {
      return null;
    }
    CatalogoRepository<? extends Catalogo, Integer> repository = this.catalogoRepositoryMap.get(nombreCatalogo + "Repository");
    if (repository == null) {
      throw new IllegalArgumentException("Catálogo no válido: " + nombreCatalogo);
    }
    Catalogo catalogo = repository.findById(id).orElse(null);
    return this.catalogoMinInfoMapper.entityToDto(catalogo);
  }

  private List<CatalogoMinInfoDTO> buscar(String nombreCatalogo, List<Integer> ids) {
    if (ids == null) {
      return null;
    }
    CatalogoRepository<? extends Catalogo, Integer> repository = this.catalogoRepositoryMap.get(nombreCatalogo + "Repository");
    if (repository == null) {
      throw new IllegalArgumentException("Catálogo no válido: " + nombreCatalogo);
    }
    List<CatalogoMinInfoDTO> result = new ArrayList<>();
    for (Integer id : ids){
      Catalogo catalogo = repository.findById(id).orElse(null);
      result.add(this.catalogoMinInfoMapper.entityToDto(catalogo));
    }
    return result;
  }

  private CatalogoMinInfoDTO crearCatalogoMinInfo(Integer id, String nombre) {
    CatalogoMinInfoDTO c = new CatalogoMinInfoDTO();
    c.setId(id);
    c.setValor(nombre);
    return c;
  }

  //Cuando existe una relacion entre campañas y contratos
  public BusquedaCampaniaOutputDTO parseCampaniaBusqueda(CampaniaAsignada campaniaAsignada){
    BusquedaCampaniaOutputDTO busquedaCampaniaOutputDTO=new BusquedaCampaniaOutputDTO();
    busquedaCampaniaOutputDTO.setIdExpediente(
      campaniaAsignada.getContrato().getExpediente().getId());
    busquedaCampaniaOutputDTO.setIdConcatenado(campaniaAsignada.getContrato().getExpediente().getIdConcatenado()!=null?campaniaAsignada.getContrato().getExpediente().getIdConcatenado():"");
    busquedaCampaniaOutputDTO.setIdContrato(campaniaAsignada.getContrato().getId());
    busquedaCampaniaOutputDTO.setProducto(
      campaniaAsignada.getContrato().getProducto()!=null ? campaniaAsignada.getContrato().getProducto().getValor():"");
    busquedaCampaniaOutputDTO.setEstado(campaniaAsignada.getContrato().getEstadoContrato()!=null ? campaniaAsignada.getContrato().getEstadoContrato().getValor():"");
    busquedaCampaniaOutputDTO.setCliente(campaniaAsignada.getContrato().getExpediente().getCartera().getClientes().stream().map(clienteCartera -> clienteCartera.getCliente().getNombre()).collect(Collectors.joining(", ")));
    busquedaCampaniaOutputDTO.setCartera(campaniaAsignada.getContrato().getExpediente().getCartera().getNombre()!=null?campaniaAsignada.getContrato().getExpediente().getCartera().getNombre():"");
    busquedaCampaniaOutputDTO.setSaldoGestion(campaniaAsignada.getContrato().getExpediente().getSaldoGestion());
    busquedaCampaniaOutputDTO.setEstrategia(campaniaAsignada.getContrato().getEstrategia()!=null? new CatalogoMinInfoDTO(campaniaAsignada.getContrato().getEstrategia().getEstrategia()) : null);
    busquedaCampaniaOutputDTO.setIdCampania(campaniaAsignada.getCampania().getId());
    busquedaCampaniaOutputDTO.setIdCarga(campaniaAsignada.getContrato().getIdCarga());
    return busquedaCampaniaOutputDTO;
  }

  //Cuando no existe una relación con ninguna campaña debe devolvernos todos los contratos
  public BusquedaCampaniaOutputDTO parseCampaniaSinRelacion(Contrato contrato){
    Contrato contratoRepresentante=contrato.getExpediente().getContratoRepresentante();
    Interviniente primerInterviniente=contratoRepresentante.getPrimerInterviniente();
    Usuario supervisorObt = contrato.getExpediente().getUsuarioByPerfil("Supervisor");
    Usuario gestorObt = contrato.getExpediente().getUsuarioByPerfil("Gestor");
    BusquedaCampaniaOutputDTO busquedaCampaniaOutputDTO=new BusquedaCampaniaOutputDTO();
    busquedaCampaniaOutputDTO.setIdExpediente(contrato.getExpediente().getId());
    busquedaCampaniaOutputDTO.setIdConcatenado(contrato.getExpediente().getIdConcatenado());
    busquedaCampaniaOutputDTO.setIdContrato(contrato.getId());
    busquedaCampaniaOutputDTO.setProducto(contrato.getProducto().getValor());
    busquedaCampaniaOutputDTO.setEstado(contrato.getEstadoContrato().getValor());
    busquedaCampaniaOutputDTO.setCliente(contrato.getExpediente().getCartera().getClientes().stream().map(clienteCartera -> clienteCartera.getCliente().getNombre()).collect(Collectors.joining(", ")));
    busquedaCampaniaOutputDTO.setCartera(contrato.getExpediente().getCartera().getNombre()!=null?contrato.getExpediente().getCartera().getNombre():"");
    busquedaCampaniaOutputDTO.setSaldoGestion(contrato.getExpediente().getSaldoGestion());
    busquedaCampaniaOutputDTO.setEstrategia(contrato.getEstrategia()!=null? new CatalogoMinInfoDTO(contrato.getEstrategia().getEstrategia()) : null);
    if (primerInterviniente!=null){
      if (primerInterviniente.getPersonaJuridica()!=null&& primerInterviniente.getPersonaJuridica()){
        String raz=primerInterviniente.getRazonSocial()!=null ? primerInterviniente.getRazonSocial():"";
        busquedaCampaniaOutputDTO.setTitular(raz);
      }else{
        String nom=primerInterviniente.getNombre()!=null?primerInterviniente.getNombre():"";
        String ape=primerInterviniente.getApellidos()!=null?primerInterviniente.getApellidos():"";
        busquedaCampaniaOutputDTO.setTitular(nom+""+ape);
      }
    }
    busquedaCampaniaOutputDTO.setSupervisor(supervisorObt!=null?supervisorObt.getNombre():"");
    busquedaCampaniaOutputDTO.setArea(gestorObt!=null && gestorObt.getAreas()!=null ? gestorObt.getAreas().stream().map(AreaUsuario::getNombre).collect(Collectors.toList()) : null);
    busquedaCampaniaOutputDTO.setGestor(gestorObt!=null ? gestorObt.getNombre():"");
    return busquedaCampaniaOutputDTO;
  }


  public BusquedaCampaniaPNOutputDTO parseCampaniaBusquedaPN(CampaniaAsignadaBienPN campaniaAsignadaBienPN){
    Usuario gestorObt = campaniaAsignadaBienPN.getBienPosesionNegociada().getExpedientePosesionNegociada().getUsuarioByPerfil("Gestor");
    Usuario supervisorObt = campaniaAsignadaBienPN.getBienPosesionNegociada().getExpedientePosesionNegociada().getUsuarioByPerfil("Supervisor");

    BusquedaCampaniaPNOutputDTO busquedaCampaniaPNOutputDTO=new BusquedaCampaniaPNOutputDTO();
    busquedaCampaniaPNOutputDTO.setIdExpediente(campaniaAsignadaBienPN.getBienPosesionNegociada().getExpedientePosesionNegociada().getId());
    busquedaCampaniaPNOutputDTO.setIdBienPN(campaniaAsignadaBienPN.getBienPosesionNegociada().getId());
    busquedaCampaniaPNOutputDTO.setCampania(campaniaAsignadaBienPN.getCampania().getNombre()!=null?campaniaAsignadaBienPN.getCampania().getNombre():"");
    busquedaCampaniaPNOutputDTO.setIdConcatenado(campaniaAsignadaBienPN.getBienPosesionNegociada().getExpedientePosesionNegociada().getIdConcatenado());
    busquedaCampaniaPNOutputDTO.setEstrategia(campaniaAsignadaBienPN.getBienPosesionNegociada().getEstrategia()!=null? new CatalogoMinInfoDTO(campaniaAsignadaBienPN.getBienPosesionNegociada().getEstrategia().getEstrategia()) : null);
    busquedaCampaniaPNOutputDTO.setIdCampania(campaniaAsignadaBienPN.getCampania().getId());
    busquedaCampaniaPNOutputDTO.setGestor(gestorObt!=null ? gestorObt.getNombre():"");
    busquedaCampaniaPNOutputDTO.setCliente(campaniaAsignadaBienPN.getBienPosesionNegociada().getExpedientePosesionNegociada().getCartera().getClientes().stream().map(clienteCartera -> clienteCartera.getCliente().getNombre()).collect(Collectors.joining(", ")));
    busquedaCampaniaPNOutputDTO.setCartera(campaniaAsignadaBienPN.getBienPosesionNegociada().getExpedientePosesionNegociada().getCartera().getNombre()!=null ? campaniaAsignadaBienPN.getBienPosesionNegociada().getExpedientePosesionNegociada().getCartera().getNombre():"");
    busquedaCampaniaPNOutputDTO.setSaldoGestion(campaniaAsignadaBienPN.getBienPosesionNegociada().getExpedientePosesionNegociada().getSaldoGestion());
    busquedaCampaniaPNOutputDTO.setSupervisor(supervisorObt!=null?supervisorObt.getNombre():"");
    busquedaCampaniaPNOutputDTO.setArea(gestorObt!=null && gestorObt.getAreas()!=null ? gestorObt.getAreas().stream().map(AreaUsuario::getNombre).collect(Collectors.toList()) : null);
    busquedaCampaniaPNOutputDTO.setEstado(campaniaAsignadaBienPN.getBienPosesionNegociada().getEstadoOcupacion()!=null ?campaniaAsignadaBienPN.getBienPosesionNegociada().getEstadoOcupacion().getValor():"");
    //busquedaCampaniaPNOutputDTO-se
    return busquedaCampaniaPNOutputDTO;
  }

  public BusquedaCampaniaPNOutputDTO parseCampaniaSinRelacionPN(BienPosesionNegociada bienPosesionNegociada){
    Usuario gestorObt = bienPosesionNegociada.getExpedientePosesionNegociada().getUsuarioByPerfil("Gestor");
    Usuario supervisorObt = bienPosesionNegociada.getExpedientePosesionNegociada().getUsuarioByPerfil("Supervisor");
    BusquedaCampaniaPNOutputDTO busquedaCampaniaPNOutputDTO=new BusquedaCampaniaPNOutputDTO();
    busquedaCampaniaPNOutputDTO.setIdExpediente(bienPosesionNegociada.getExpedientePosesionNegociada().getId());
    busquedaCampaniaPNOutputDTO.setIdBienPN(bienPosesionNegociada.getId());
    busquedaCampaniaPNOutputDTO.setIdConcatenado(bienPosesionNegociada.getExpedientePosesionNegociada().getIdConcatenado()!=null?bienPosesionNegociada.getExpedientePosesionNegociada().getIdConcatenado():"");
    busquedaCampaniaPNOutputDTO.setEstrategia(bienPosesionNegociada.getEstrategia()!=null? new CatalogoMinInfoDTO(bienPosesionNegociada.getEstrategia().getEstrategia()) : null);
    busquedaCampaniaPNOutputDTO.setIdCampania(bienPosesionNegociada.getId());
    busquedaCampaniaPNOutputDTO.setGestor(gestorObt!=null ? gestorObt.getNombre():"");
    busquedaCampaniaPNOutputDTO.setCliente(bienPosesionNegociada.getExpedientePosesionNegociada().getCartera().getClientes().stream().map(clienteCartera -> clienteCartera.getCliente().getNombre()).collect(Collectors.joining(", ")));
    busquedaCampaniaPNOutputDTO.setCartera(bienPosesionNegociada.getExpedientePosesionNegociada().getCartera().getNombre()!=null?bienPosesionNegociada.getExpedientePosesionNegociada().getCartera().getNombre():"");
    busquedaCampaniaPNOutputDTO.setSaldoGestion(bienPosesionNegociada.getExpedientePosesionNegociada().getSaldoGestion());
    busquedaCampaniaPNOutputDTO.setSupervisor(supervisorObt!=null?supervisorObt.getNombre():"");
    busquedaCampaniaPNOutputDTO.setArea(gestorObt!=null && gestorObt.getAreas()!=null ? gestorObt.getAreas().stream().map(AreaUsuario::getNombre).collect(Collectors.toList()) : null);
    busquedaCampaniaPNOutputDTO.setEstado(bienPosesionNegociada.getEstadoOcupacion()!=null ? bienPosesionNegociada.getEstadoOcupacion().getValor():"");
    busquedaCampaniaPNOutputDTO.setCampania("");
    return busquedaCampaniaPNOutputDTO;
  }

}
