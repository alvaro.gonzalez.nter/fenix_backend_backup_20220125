package com.haya.alaska.busqueda.infrastructure.controller.dto;

import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaCatalogosDto {

  BusquedaExpedienteCatalogosDto expediente;
  BusquedaContratoCatalogosDto contrato;
  BusquedaIntervinienteCatalogosDto interviniente;
  BusquedaBienCatalogosDto bien;
  BusquedaMovimientoCatalogosDto movimientos;
  BusquedaEstimacionCatalogosDto estimaciones;
  BusquedaProcedimientoCatalogosDto procedimiento;
  BusquedaPropuestaCatalogosDto propuestas;
  BusquedaAgendaCatalogosDto agenda;

}
