package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaIntervinienteCatalogosDto {

  Integer id;
  String idIntervinienteOrigen;
  String docId;
  String apellidos;
  String nombre;
  CatalogoMinInfoDTO localidad;
  CatalogoMinInfoDTO provincia;
  String telefono;
  String email;
  Boolean vulnerabilidad;
  CatalogoMinInfoDTO tipoIntervencion;
  CatalogoMinInfoDTO residencia;
  CatalogoMinInfoDTO nacionalidad;
  List<IntervaloFecha> intervaloFechas;
}
