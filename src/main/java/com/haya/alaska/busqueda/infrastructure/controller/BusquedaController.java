package com.haya.alaska.busqueda.infrastructure.controller;

import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDto;
import com.haya.alaska.busqueda.application.BusquedaUseCase;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaGuardadaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaGuardadaInputDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaRecienteDto;
import com.haya.alaska.campania.infraestructure.controller.dto.BusquedaCampaniaOutputDTO;
import com.haya.alaska.campania.infraestructure.controller.dto.BusquedaCampaniaPNOutputDTO;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoBusquedaDto;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTOToList;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteExcelExportDTO;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCase;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output.FormalizacionBusquedaOutputDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteBusquedaDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteBusquedaDTOToList;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.BienExtendedDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.skip_tracing.aplication.SkipTracingUseCase;
import com.haya.alaska.skip_tracing.infrastructure.controller.dto.SkipTracingOutputDTO;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.apache.tomcat.websocket.AuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/busqueda")
public class BusquedaController {
  @Autowired
  private BusquedaUseCase busquedaUseCase;

  @Autowired  private ExpedientePosesionNegociadaCase expedientePosesionNegociadaCase;
  @Autowired  private SkipTracingUseCase skipTracingUseCase;

  @ApiOperation(value = "Listado de expedientes filtrados",
    notes = "Filtra los expedientes por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping
  public ListWithCountDTO<ExpedienteDTOToList> getFilteredExpedientes(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "id", required = false) String id,
    @RequestParam(value = "idConcatenado", required = false) String idConcatenado,
    @RequestParam(value = "estado", required = false) String estado,
    @RequestParam(value = "modoDeGestion", required = false) String modoDeGestion,
    @RequestParam(value = "situacion", required = false) String situacion,
    @RequestParam(value = "cliente", required = false) String cliente,
    @RequestParam(value = "cartera", required = false) String cartera,
    @RequestParam(value = "primerTitular", required = false) String primerTitular,
    @RequestParam(value = "saldoGestion", required = false) String saldoGestion,
    @RequestParam(value = "estrategia", required = false) String estrategia,
    @RequestParam(value = "supervisor", required = false) String supervisor,
    @RequestParam(value = "areaGestion", required = false) String areaGestion,
    @RequestParam(value = "gestor", required = false) String gestor,
    @RequestParam(value = "gestor_suplente", required = false) String gestor_suplente,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal
  ) {
    Usuario usuario = (Usuario) principal.getPrincipal();
    String idEnviar = idConcatenado != null ? idConcatenado : id;
    return busquedaUseCase.getAllFilteredExpedientes(busquedaDto, usuario,
      idEnviar,
      estado,
      modoDeGestion,
      situacion,
      cliente,
      cartera,
      primerTitular,
      saldoGestion,
      estrategia,
      supervisor,
      areaGestion,
      gestor,
      gestor_suplente,
      orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Listado de expedientes formalizacion",
    notes = "Filtra los expedientes por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/formalizacion")
  public ListWithCountDTO<FormalizacionBusquedaOutputDTO> getFilteredFormalizacion(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "idConcatenado", required = false) String idConcatenado,
    @RequestParam(value = "titular", required = false) String titular,
    @RequestParam(value = "nif", required = false) String nif,
    @RequestParam(value = "estado", required = false) String estado,
    @RequestParam(value = "detalleEstado", required = false) String detalleEstado,
    @RequestParam(value = "fechaFirma", required = false) String fechaFirma,
    @RequestParam(value = "horaFirma", required = false) String horaFirma,
    @RequestParam(value = "formalizacionHaya", required = false) String formalizacionHaya,
    @RequestParam(value = "gestorHaya", required = false) String gestorHaya,
    @RequestParam(value = "formalizacionCliente", required = false) String formalizacionCliente,
    @RequestParam(value = "fechaSancion", required = false) String fechaSancion,
    @RequestParam(value = "importeDacion", required = false) String importeDacion,
    @RequestParam(value = "importeDeuda", required = false) String importeDeuda,
    @RequestParam(value = "provincia", required = false) String provincia,
    @RequestParam(value = "alquiler", required = false) String alquiler,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal
  ) {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return busquedaUseCase.getBusquedaFormalizacion(busquedaDto, usuario,
      idConcatenado,
      titular,
      nif,
      estado,
      detalleEstado,
      fechaFirma,
      horaFirma,
      formalizacionHaya,
      gestorHaya,
      formalizacionCliente,
      fechaSancion,
      importeDacion,
      importeDeuda,
      provincia,
      alquiler,
      orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Listado de expedientes filtrados",
    notes = "Filtra los expedientes por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/expedientesPosesionNegociada")
  public ListWithCountDTO<ExpedientePosesionNegociadaDTO> getFilteredExpedientesPN(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "id", required = false) String id,
    @RequestParam(value = "idConcatenado", required = false) String idConcatenado,
    @RequestParam(value = "origen", required = false) String origen,
    @RequestParam(value = "carteraTipo", required = false) String carteraTipo,
    @RequestParam(value = "gestor", required = false) String gestor,
    @RequestParam(value = "titular", required = false) String titular,
    @RequestParam(value = "cliente", required = false) String cliente,
    @RequestParam(value = "cartera", required = false) String cartera,
    @RequestParam(value = "saldo", required = false) String saldo,
    @RequestParam(value = "numeroBienes", required = false) Integer numeroBienes,
    @RequestParam(value = "estado", required = false) String estado,
    @RequestParam(value = "estrategia", required = false) String estrategia,
    @RequestParam(value = "supervisor", required = false) String supervisor,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal
  ) throws NotFoundException, AuthenticationException, javax.naming.AuthenticationException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    String idEnviar = idConcatenado != null ? idConcatenado : id;
    return expedientePosesionNegociadaCase.getAllFilteredExpedientesGestionMasiva(
      busquedaDto,
      (Usuario) principal.getPrincipal(),
      idEnviar,
      origen,
      carteraTipo,
      gestor,
      titular,
      cliente,
      cartera,
      saldo,
      numeroBienes,
      estado,
      estrategia,
      supervisor,
      orderField,
      orderDirection,
      size,
      page );
  }

  @ApiOperation(
    value = "Listado de skipTracing filtrados",
    notes = "Filtra los skipTracing por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @GetMapping("/skipTracing")
  public ListWithCountDTO<SkipTracingOutputDTO> findAll(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "cliente", required = false) String cliente,
    @RequestParam(value = "cartera", required = false) String cartera,
    @RequestParam(value = "idExpediente", required = false) String idExpediente,
    @RequestParam(value = "idContrato", required = false) Integer idContrato,
    @RequestParam(value = "idCarga", required = false) Integer idCarga,

    @RequestParam(value = "producto", required = false) String producto,

    @RequestParam(value = "nIntervinientes", required = false) Integer nIntervinientes,
    @RequestParam(value = "nGarantias", required = false) Integer nGarantias,
    @RequestParam(value = "estado", required = false) String estado,
    @RequestParam(value = "nivel", required = false) String nivel,
    @RequestParam(value = "intervinientePrincipal", required = false) String intervinientePrincipal,
    @RequestParam(value = "tipoIntervencion", required = false) String tipoIntervencion,
    @RequestParam(value = "judicial", required = false) Boolean judicial,
    @RequestParam(value = "provincia", required = false) String provincia,
    @RequestParam(value = "analistaST", required = false) String analistaST,
    @RequestParam(value = "gestorExpediente", required = false) String gestorExpediente,
    @RequestParam(value = "saldoGestion", required = false) Double saldoGestion,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,

    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException, InvocationTargetException, IllegalAccessException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return skipTracingUseCase.getAllFilteredExpedientes(busquedaDto, usuario, cliente, cartera, idExpediente, idContrato, idCarga, producto,
      nIntervinientes, nGarantias, estado, nivel, intervinientePrincipal, tipoIntervencion, judicial, provincia,
      analistaST, gestorExpediente, saldoGestion, orderField, orderDirection, size,page);
  }

  @ApiOperation(value = "Listado de contratos filtrados",
    notes = "Filtra los contratos por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/contratos")
  public ListWithCountDTO<ContratoBusquedaDto> getFilteredContratos(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "id", required = false) String id,
    @RequestParam(value = "idOrigen", required = false) String idOrigen,
    @RequestParam(value = "cartera", required = false) String cartera,
    @RequestParam(value = "activo", required = false) String activo,
    @RequestParam(value = "origenEstrategia", required = false) String origenEstrategia,
    @RequestParam(value = "fecha", required = false) String fecha,
    @RequestParam(value = "periodo", required = false) String periodo,
    @RequestParam(value = "tipo", required = false) String tipo,
    @RequestParam(value = "estrategia", required = false) String estrategia,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal
  ) {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return busquedaUseCase.getAllFilteredContratos(busquedaDto, usuario, size, page,id, idOrigen, cartera, activo, origenEstrategia, fecha, periodo, tipo, estrategia, orderField, orderDirection);
  }

  @ApiOperation(value = "Listado de bienes filtrados",
    notes = "Filtra los bienes por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/bienesPosesionNegociada")
  public ListWithCountDTO<BienPosesionNegociadaDto> getFilteredBienes(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal
  ) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return busquedaUseCase.getAllFilteredBienesPN(busquedaDto, usuario, size, page, orderField, orderDirection);
  }

  @ApiOperation(value = "Listado de bienes filtrados PN",
    notes = "Filtra los bienes por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/movil/bienesPosesionNegociada")
  public ListWithCountDTO<BienExtendedDTOToList> getFilteredBienesPN(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal
  ) {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return busquedaUseCase.getAllFilteredBienesPN(busquedaDto, usuario,
      orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Listado de bienes filtrados",
    notes = "Filtra los bienes por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/movil/bienes")
  public ListWithCountDTO<BienExtendedDTOToList> getFilteredBienesRA(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal
  ) {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return busquedaUseCase.getAllFilteredBienesRA(busquedaDto, usuario,
      orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Listado de expedientes filtrados en Excel")
  @PostMapping("excel")
  public ResponseEntity<InputStreamResource> getFilteredExpedientesExcel(
    @RequestBody BusquedaDto busquedaDto,
    @ApiIgnore CustomUserDetails principal
  ) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(busquedaUseCase.getAllFilteredExpedientesExcel(busquedaDto, usuario));
    return excelExport.download(excel, "expedientes_busqueda_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(value = "Listado de búsquedas recientes del usuario")
  @GetMapping("recientes")
  public List<BusquedaRecienteDto> getRecientes(@ApiIgnore CustomUserDetails principal) {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return this.busquedaUseCase.getRecientes(usuario);
  }

  @ApiOperation(value = "Listado de búsquedas guardadas del usuario")
  @GetMapping("guardadas")
  public List<BusquedaGuardadaDto> getGuardadas(@ApiIgnore CustomUserDetails principal) {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return this.busquedaUseCase.getGuardadas(usuario);
  }

  @ApiOperation(value = "Obtiene los resultados de una búsqueda guardada por el usuario")
  @GetMapping("guardadas/{id}")
  public ListWithCountDTO<ExpedienteDTOToList> getGuardada(
    @ApiIgnore CustomUserDetails principal,
    @PathVariable(value = "id") Integer id,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page
  ) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return this.busquedaUseCase.getGuardada(usuario, id, size, page);
  }

  @ApiOperation(value = "Creación de una búsqueda guardada")
  @PostMapping("guardadas")
  @ResponseStatus(code = HttpStatus.CREATED)
  public BusquedaGuardadaDto crearGuardada(
    @ApiIgnore CustomUserDetails principal,
    @RequestBody BusquedaGuardadaInputDto busquedaGuardadaInputDto
  ) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return this.busquedaUseCase.crearGuardada(usuario, busquedaGuardadaInputDto);
  }

  @ApiOperation(value = "Eliminación de una búsqueda guardada del usuario")
  @DeleteMapping("guardadas/{id}")
  public void eliminarGuardada(
    @ApiIgnore CustomUserDetails principal,
    @PathVariable(value = "id") Integer id
  ) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    this.busquedaUseCase.eliminarGuardada(usuario, id);
  }

  @ApiOperation(
    value = "Insertar expediente mediante excel")
  @PostMapping("/ExpedientePorExcel")
  public ListWithCountDTO<ExpedienteDTOToList> insertarExpedientePorExcel(@RequestParam("file") MultipartFile file, @ApiIgnore CustomUserDetails principal) throws IOException {
    return busquedaUseCase.buscarExpedientesPorExcel(file, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(
    value = "Insertar expediente de Posesión Negociada mediante excel")
  @PostMapping("/ExpedientePNPorExcel")
  public ListWithCountDTO<ExpedientePosesionNegociadaDTO> insertarExpedientePNPorExcel(@RequestParam("file") MultipartFile file, @ApiIgnore CustomUserDetails principal) throws IOException {
    return busquedaUseCase.buscarExpedientesPNPorExcel(file, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(
    value = "Insertar Skip Tracing mediante excel")
  @PostMapping("/SkipTracingPorExcel")
  public ListWithCountDTO<SkipTracingOutputDTO> insertarSkipTracingPorExcel(@RequestParam("file") MultipartFile file, @ApiIgnore CustomUserDetails principal) throws IOException, IllegalAccessException, NotFoundException, InvocationTargetException {
    return busquedaUseCase.buscarSkipTracingsPorExcel(file, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Listado de expedientesPosesionNegociada filtrados en Excel")
  @PostMapping("excelPN")
  public ResponseEntity<InputStreamResource> getFilteredExpedientesPNExcel(
    @RequestBody BusquedaDto busquedaDto,
    @ApiIgnore CustomUserDetails principal
  ) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(expedientePosesionNegociadaCase.getAllFilteredExpedientesPNExcel(busquedaDto, usuario));
    return excelExport.download(excel, "expedientesPosesionNegociada_busqueda_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(
      value = "Listado de contratos filtrados",
      notes =
          "Filtra los contratos por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/campania")
  public ListWithCountDTO<BusquedaCampaniaOutputDTO> getFilteredCampania(
      @RequestBody BusquedaDto busquedaDto,
      @RequestParam(value = "idConcatenado", required = false) String idExpediente,
      @RequestParam(value = "idContrato", required = false) String idContrato,
      @RequestParam(value = "producto", required = false) String producto,
      @RequestParam(value = "estado", required = false) String estado,
      @RequestParam(value = "cliente", required = false) String cliente,
      @RequestParam(value = "cartera", required = false) String cartera,
      @RequestParam(value = "titular", required = false) String titular,
      @RequestParam(value = "saldoGestion", required = false) String saldoGestion,
      @RequestParam(value = "estrategia", required = false) String estrategia,
      @RequestParam(value = "supervisor", required = false) String supervisor,
      @RequestParam(value = "area", required = false) String area,
      @RequestParam(value = "gestor", required = false) String gestor,
      @RequestParam(value = "campania", required = false) String campania,
      @RequestParam(value = "idCarga", required = false) String idCarga,
      @RequestParam(value = "orderField", required = false) String orderField,
      @RequestParam(value = "orderDirection", required = false) String orderDirection,
      @RequestParam(value = "size") Integer size,
      @RequestParam(value = "page") Integer page,
      @ApiIgnore CustomUserDetails principal) {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return busquedaUseCase.getAllFilteredCampania(
        busquedaDto,
        usuario,
        size,
        page,
        idExpediente,
        idContrato,
        producto,
        estado,
        cliente,
        cartera,
        titular,
        saldoGestion,
        estrategia,
        supervisor,
        area,
        gestor,
        campania,
        idCarga,
        orderField,
        orderDirection);
  }


  @ApiOperation(value = "Listado de intervinientes filtrados",
    notes = "Filtra los intervinientes por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/intervinientes")
  public ListWithCountDTO<IntervinienteBusquedaDTOToList> getFilteredIntervinientes(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "tipo") String tipo,
    @RequestParam(value = "sacarProcedimientos", required = false) Boolean sacarProcedimientos,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal
  ) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return busquedaUseCase.getAllFilteredIntervinientes(tipo,busquedaDto,usuario,sacarProcedimientos,orderField,orderDirection,size,page,false);
  }

  @ApiOperation(value = "Listado de intervinientes filtrados",
    notes = "Filtra los intervinientes por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/intervinientes/firma")
  public ListWithCountDTO<IntervinienteBusquedaDTOToList> getFilteredIntervinientesFirma(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "tipo") String tipo,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal
  ) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return busquedaUseCase.getAllFilteredIntervinientes(tipo,busquedaDto,usuario, null, orderField, orderDirection, size, page, true);
  }

  @ApiOperation(
    value = "Obtener intervinientes mediante excel")
  @PostMapping("/intervinientesPorExcel")
  public List<IntervinienteBusquedaDTOToList> intervinientesPorExcel(@RequestParam("file") MultipartFile file, @ApiIgnore CustomUserDetails principal, @RequestParam("tipo") String tipo) throws IOException, NotFoundException {
    return busquedaUseCase.buscarIntervinientesPorExcel(file, tipo, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(
    value = "Obtener ocupantes mediante excel")
  @PostMapping("/ocupantesPorExcel")
  public List<OcupanteBusquedaDTOToList> ocupantesPorExcel(@RequestParam("file") MultipartFile file, @ApiIgnore CustomUserDetails principal, @RequestParam("tipo") String tipo) throws IOException, NotFoundException {
    return busquedaUseCase.buscarOcupantesPorExcel(file, tipo, (Usuario) principal.getPrincipal());
  }


  @ApiOperation(value = "Listado de ocupantes filtrados",
    notes = "Filtra los ocupantes por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/ocupantes")
  public ListWithCountDTO<OcupanteBusquedaDTOToList> getFilteredOcupantes(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "tipo") String tipo,
    @RequestParam(value = "sacarProcedimientos", required = false) Boolean sacarProcedimientos,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal
  ) throws NotFoundException{
    return busquedaUseCase.getAllFilteredOcupantesGestionMasiva(
      tipo,
      busquedaDto,
      (Usuario) principal.getPrincipal(),
      sacarProcedimientos,
      orderField,
      orderDirection,
      size,
      page);
  }


  @ApiOperation(
    value = "Listado de contratos filtrados",
    notes =
      "Filtra las campañas de los bienesPN por los campos indicados en cada una de las secciones disponibles en el módulo de búsqueda")
  @PostMapping("/campaniaPN")
  public ListWithCountDTO<BusquedaCampaniaPNOutputDTO> getFilteredCampaniaPN(
    @RequestBody BusquedaDto busquedaDto,
    @RequestParam(value = "idConcatenado", required = false) String idExpediente,
    @RequestParam(value = "idBienPN", required = false) String idBienPN,
    @RequestParam(value = "estado", required = false) String estado,
    @RequestParam(value = "cliente", required = false) String cliente,
    @RequestParam(value = "cartera", required = false) String cartera,
    @RequestParam(value = "saldoGestion", required = false) String saldoGestion,
    @RequestParam(value = "estrategia", required = false) String estrategia,
    @RequestParam(value = "supervisor", required = false) String supervisor,
    @RequestParam(value = "area", required = false) String area,
    @RequestParam(value = "gestor", required = false) String gestor,
    @RequestParam(value = "campania", required = false) String campania,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return busquedaUseCase.getAllFilteredCampaniaPN(
      busquedaDto,
      usuario,
      size,
      page,
      idExpediente,
      idBienPN,
      estado,
      cliente,
      cartera,
      saldoGestion,
      estrategia,
      supervisor,
      area,
      gestor,
      campania,
      orderField,
      orderDirection);
  }



}
