package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaExpedienteDto {

    String id;
    Integer grupoCliente;
    Integer cliente;
    Integer cartera;
    String responsableCartera;
    String gestor;
    String supervisor;
    Integer estado;
    Integer subestado;
    Integer situacion;
    Integer estrategia;
    String campania;
    IntervaloImporte saldoGestion;
    List<IntervaloFecha> intervaloFechas;
}
