package com.haya.alaska.busqueda.infrastructure.controller.dto.internal;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BusquedaBienCatalogosDto {

  Integer id;
  String idOrigen;
  String direccion;
  CatalogoMinInfoDTO localidad;
  CatalogoMinInfoDTO municipio;
  CatalogoMinInfoDTO provincia;
  String localidadRegistro;
  String registro;
  String fincaRegistral;
  String referenciaCatastral;
  String idufir;
  IntervaloImporteDescripcion intervaloImporte;
  List<IntervaloFecha> intervaloFechas;
}
