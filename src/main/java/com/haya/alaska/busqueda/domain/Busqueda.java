package com.haya.alaska.busqueda.domain;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@Audited
public abstract class Busqueda implements Serializable {
  private static final long serialVersionUID = 1L;
  private static final ObjectMapper objectMapper = new ObjectMapper();

  static {
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID", nullable = false)
  private Usuario usuario;

  @Column(name = "DES_JSON_CONSULTA", nullable = false)
  @Lob
  private String jsonConsulta;

  @Column(name = "NUM_RESULTADOS", nullable = false)
  private Integer numeroResultados;

  @Column(name = "FCH_FECHA", nullable = false)
  private Date fecha;

  public Busqueda(Usuario usuario, BusquedaDto consulta) {
    this(usuario, consulta, 0);
  }

  public Busqueda(Usuario usuario, BusquedaDto consulta, int numeroResultados) {
    this(usuario, consulta, numeroResultados, new Date());
  }

  public Busqueda(Usuario usuario, BusquedaDto consulta, int numeroResultados, Date fecha) {
    this.setUsuario(usuario);
    this.setJsonConsultaWithDto(consulta);
    this.setNumeroResultados(numeroResultados);
    this.setFecha(fecha);
  }

  @SneakyThrows
  public BusquedaDto parseJsonConsulta() {
    return objectMapper.readValue(this.jsonConsulta, BusquedaDto.class);
  }

  @SneakyThrows
  protected void setJsonConsultaWithDto(BusquedaDto consulta) {
    this.setJsonConsulta(objectMapper.writeValueAsString(consulta));
  }
}
