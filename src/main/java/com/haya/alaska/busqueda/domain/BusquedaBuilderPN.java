package com.haya.alaska.busqueda.domain;

import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.Bien_;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada_;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.IntervaloFecha;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.IntervaloImporte;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCase;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada_;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien_;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.domain.Propuesta_;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.valoracion.domain.Valoracion;
import lombok.Getter;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BusquedaBuilderPN {

  @Getter
  private Root<ExpedientePosesionNegociada> root;
  @Getter
  private CriteriaBuilder cb;

  private Join<OcupanteBien, Ocupante> joinsOcupante;
  private Join<ExpedientePosesionNegociadaCase, Contrato> joinContrato;
  private Join<BienPosesionNegociada, Bien> joinBien;
  private Join<Expediente, Estimacion> joinEstimacion;
  private Join<BienPosesionNegociada, ProcedimientoPosesionNegociada> joinProcedimiento;
  private Join<ExpedientePosesionNegociadaCase, Propuesta> joinPropuesta;
  private Join<ExpedientePosesionNegociada, Contrato> joinContratos;
  private Join<Contrato, Saldo> joinSaldos;
  private Join<ExpedientePosesionNegociada, Contrato> joinContratoRepresentante;
  private Join<Bien, Tasacion>joinTasacionBien;
  private Join<Bien, Valoracion> joinValoracionBien;
  private Join<Contrato, Procedimiento> joinProcedimientoGestionAmistosa;
  private Join<ExpedientePosesionNegociada, Propuesta> joinPropuestas;
  private Join<Propuesta, ImportePropuesta> joinImportesPropuestas;
  private Join<Propuesta, Contrato> joinContratosPropuestas;
  private Join<Propuesta, Formalizacion> joinFormalizacionPropuesta;
  private Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> joinAsignacionesExpediente;


  public BusquedaBuilderPN(Root<ExpedientePosesionNegociada> root, CriteriaBuilder cb)
  {
    this.root = root;
    this.cb = cb;
  }

  public Join<OcupanteBien, Ocupante> getJoinsOcupante()
  {
    if(joinsOcupante != null) return joinsOcupante;
    Join<ExpedientePosesionNegociada, BienPosesionNegociada> join = root.join(ExpedientePosesionNegociada_.bienesPosesionNegociada, JoinType.INNER);
    Join<BienPosesionNegociada, OcupanteBien> join2 = join.join(BienPosesionNegociada_.ocupantes, JoinType.INNER);
    this.joinsOcupante = join2.join(OcupanteBien_.ocupante, JoinType.INNER);
    return joinsOcupante;
  }

  public Join<ExpedientePosesionNegociadaCase, Contrato> getJoinContrato()
  {
    if(joinContrato != null) return joinContrato;
    this.joinContrato = root.join(ExpedientePosesionNegociada_.CONTRATO, JoinType.INNER);
    return joinContrato;
  }

  public Join<BienPosesionNegociada, Bien> getJoinBien()
  {
    if(joinBien != null) return joinBien;
    Join<ExpedientePosesionNegociadaCase, BienPosesionNegociada> joinBienPosesion = root.join(ExpedientePosesionNegociada_.BIENES_POSESION_NEGOCIADA, JoinType.INNER);
    joinBien = joinBienPosesion.join(BienPosesionNegociada_.BIEN, JoinType.INNER);
    return joinBien;
  }


  public Join<Bien, Tasacion> getJoinTasacionBien()
  {

    if (this.joinTasacionBien == null) {
      this.joinTasacionBien = this.getJoinBien().join(Bien_.tasaciones, JoinType.INNER);
    }
    return this.joinTasacionBien;
  }

  public Join<Bien, Valoracion> getJoinValoracionBien()
  {

    if (this.joinValoracionBien == null) {
      this.joinValoracionBien = this.getJoinBien().join(Bien_.valoraciones, JoinType.INNER);
    }
    return this.joinValoracionBien;
  }

  public Join<Expediente, Estimacion> getJoinEstimacion()
  {
    if(joinEstimacion != null) return joinEstimacion;
    Join<ExpedientePosesionNegociada,Expediente>joinExpediente = root.join(ExpedientePosesionNegociada_.EXPEDIENTE,JoinType.INNER);
    joinEstimacion = joinExpediente.join(Expediente_.estimaciones,JoinType.INNER);
    return joinEstimacion;
  }

  public Join<BienPosesionNegociada, ProcedimientoPosesionNegociada> getJoinProcedimiento()
  {
    if(joinProcedimiento != null) return joinProcedimiento;
    Join<ExpedientePosesionNegociadaCase,BienPosesionNegociada>joinBienPosesion2=root.join(ExpedientePosesionNegociada_.BIENES_POSESION_NEGOCIADA,JoinType.INNER);
    joinProcedimiento=joinBienPosesion2.join(BienPosesionNegociada_.procedimientos,JoinType.INNER);
    return joinProcedimiento;
  }

  public Join<Contrato, Procedimiento> getJoinProcedimientoGestionAmistosa()
  {
    if(joinProcedimientoGestionAmistosa != null) return joinProcedimientoGestionAmistosa;
    joinProcedimientoGestionAmistosa = getJoinContrato().join(Contrato_.procedimientos);
    return joinProcedimientoGestionAmistosa;
  }

  public Join<ExpedientePosesionNegociadaCase, Propuesta> getJoinPropuesta()
  {
    if(joinPropuesta != null) return joinPropuesta;
    joinPropuesta = root.join(ExpedientePosesionNegociada_.PROPUESTAS, JoinType.INNER);
    return joinPropuesta;
  }

  public Join<ExpedientePosesionNegociada, Contrato> getJoinContratoRepresentante() {
    if (this.joinContratoRepresentante == null) {
      this.joinContratoRepresentante = root.join(ExpedientePosesionNegociada_.contrato, JoinType.INNER);
      this.joinContratoRepresentante.on(cb.equal(this.joinContratoRepresentante.get(Contrato_.esRepresentante), true));
    }
    return this.joinContratoRepresentante;
  }

  public Join<ExpedientePosesionNegociada, Contrato> getJoinContratos() {
    if (this.joinContratos == null) {
      this.joinContratos = root.join(ExpedientePosesionNegociada_.contrato, JoinType.INNER);
    }
    return this.joinContratos;
  }

  public Join<Contrato, Saldo> getJoinSaldos() {
    if (this.joinSaldos == null) {
      this.joinSaldos = this.getJoinContratoRepresentante().join(Contrato_.saldos, JoinType.INNER);
    }
    return this.joinSaldos;
  }

  public Join<ExpedientePosesionNegociada, Propuesta> getJoinPropuestas() {
    if (this.joinPropuestas == null) {
      this.joinPropuestas = root.join(ExpedientePosesionNegociada_.propuestas, JoinType.INNER);
    }
    return this.joinPropuestas;
  }

  public Join<Propuesta, ImportePropuesta> getJoinImportesPropuestas() {
    if (this.joinImportesPropuestas == null) {
      this.joinImportesPropuestas = this.getJoinPropuestas().join(Propuesta_.importesPropuestas, JoinType.INNER);
    }
    return this.joinImportesPropuestas;
  }

  public Join<Propuesta, Contrato> getJoinContratosPropuestas() {
    if (this.joinContratosPropuestas == null) {
      this.joinContratosPropuestas = this.getJoinPropuestas().join(Propuesta_.contratos, JoinType.INNER);
    }
    return this.joinContratosPropuestas;
  }

  public Join<Propuesta, Formalizacion> getJoinFormalizacionPropuesta() {
    if (this.joinFormalizacionPropuesta == null) {
      this.joinFormalizacionPropuesta = this.getJoinPropuestas().join(Propuesta_.formalizaciones);
    }
    return this.joinFormalizacionPropuesta;
  }

  public Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> getJoinAsignacionesExpediente() {
    if (this.joinAsignacionesExpediente == null) {
      this.joinAsignacionesExpediente =
        root.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
    }
    return joinAsignacionesExpediente;
  }

  public List<Predicate> addIntervaloImporte(List<Predicate> predicates ,Expression<Double> atributo, IntervaloImporte intervaloImporte) {
    if (intervaloImporte.getIgual() != null) {
      predicates.add(cb.equal(atributo, intervaloImporte.getIgual()));
    } else {
      if (intervaloImporte.getMayor() != null) {
        predicates.add(cb.greaterThanOrEqualTo(atributo, intervaloImporte.getMayor()));
      }
      if (intervaloImporte.getMenor() != null) {
        predicates.add(cb.lessThanOrEqualTo(atributo, intervaloImporte.getMenor()));
      }
    }
    if (predicates.size() != 0) {
      predicates.add(cb.and(predicates.toArray(new Predicate[0])));
    }
    return predicates;
  }

  public List<Predicate> addIntervaloFechas(List<Predicate> predicates ,Expression<Date> atributo, IntervaloFecha intervaloFecha) {
    if (intervaloFecha.getFechaInicio() != null) {
      predicates.add(cb.greaterThanOrEqualTo(atributo, intervaloFecha.getFechaInicio()));
    }
    if (intervaloFecha.getFechaFin() != null) {
      predicates.add(cb.lessThanOrEqualTo(atributo, intervaloFecha.getFechaFin()));
    }
    if (predicates.size() != 0) {
      predicates.add(cb.and(predicates.toArray(new Predicate[0])));
    }
    return predicates;
  }

}
