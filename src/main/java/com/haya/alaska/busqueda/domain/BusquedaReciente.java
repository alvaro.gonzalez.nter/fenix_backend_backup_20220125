package com.haya.alaska.busqueda.domain;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@AuditTable(value = "HIST_MSTR_BUSQUEDA_RECIENTE")
@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "MSTR_BUSQUEDA_RECIENTE")
public class BusquedaReciente extends Busqueda {
  public BusquedaReciente(Usuario usuario, BusquedaDto consulta, int numeroResultados) {
    super(usuario, consulta, numeroResultados);
  }
}
