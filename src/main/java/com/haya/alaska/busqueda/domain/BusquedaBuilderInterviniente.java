package com.haya.alaska.busqueda.domain;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente_;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.Bien_;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.*;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.domain.Carga_;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable_;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.domain.Cliente_;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera_;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.domain.ContratoBien_;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente_;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.domain.DatoContacto_;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.domain.Direccion_;
import com.haya.alaska.estado_checklist.domain.EstadoChecklist_;
import com.haya.alaska.estado_contrato.domain.EstadoContrato_;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion.domain.Estimacion_;
import com.haya.alaska.estrategia.domain.Estrategia_;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada_;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.formalizacion.domain.*;
import com.haya.alaska.gestor_formalizacion.domain.GestorConcursos_;
import com.haya.alaska.gestor_formalizacion.domain.GestorGestoria_;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta_;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.domain.Interviniente_;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.domain.Localidad_;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.domain.Movimiento_;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.domain.Municipio_;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.domain.Pais_;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.domain.Perfil_;
import com.haya.alaska.procedimiento.domain.*;
import com.haya.alaska.producto.domain.Producto_;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.domain.Propuesta_;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien_;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.domain.Provincia_;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.domain.Saldo_;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.situacion.domain.Situacion_;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien_;
import com.haya.alaska.subestado_formalizacion.domain.SubestadoFormalizacion_;
import com.haya.alaska.subtipo_carga_formalizacion.domain.SubtipoCargaFormalizacion_;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado_;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta_;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.domain.Tasacion_;
import com.haya.alaska.tipo_bien.domain.TipoBien_;
import com.haya.alaska.tipo_carga.domain.TipoCarga_;
import com.haya.alaska.tipo_carga_formalizacion.domain.TipoCargaFormalizacion_;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.tipo_estado.domain.TipoEstado_;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion_;
import com.haya.alaska.tipo_movimiento.domain.TipoMovimiento;
import com.haya.alaska.tipo_movimiento.domain.TipoMovimiento_;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta_;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.valoracion.domain.Valoracion_;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.lang.Nullable;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BusquedaBuilderInterviniente {


  private final CriteriaBuilder cb;
  private final CriteriaQuery<Interviniente> query;
  private final Root<Interviniente> root;
  private final List<Predicate> predicates;
  private Expression<?> orderField;
  private boolean ordenAscendente;

  @Nullable
  private Join<Expediente, Contrato> joinContratos;
  @Nullable
  private Join<Expediente, Cartera> joinCarteraExpediente;
  @Nullable
  private Join<Expediente, TipoEstado> joinEstadoExpediente;
  @Nullable
  private Join<Expediente, Contrato> joinContratoRepresentante;
  @Nullable
  private Join<Contrato, Movimiento> joinMovimientos;
  @Nullable
  private Join<Movimiento, TipoMovimiento> joinTipoMovimientos;
  @Nullable
  private Join<Contrato, ContratoInterviniente> joinContratoIntervinientes;
  @Nullable
  private Join<ContratoInterviniente, Interviniente> joinIntervinientes;
  @Nullable
  private Join<Contrato, ContratoBien> joinContratoBienes;
  @Nullable
  private Join<ContratoBien, Bien> joinBienes;
  @Nullable
  private Join<Contrato, Saldo> joinSaldos;
  @Nullable
  private Join<Interviniente, Pais> joinPaisInterviniente;
  @Nullable
  private Join<Interviniente, Direccion> joinDireccionInterviniente;
  @Nullable
  private Join<Interviniente, DatoContacto> joinDatosContactoInterviniente;
  @Nullable
  private Join<Bien, Localidad> joinLocalidadBien;
  @Nullable
  private Join<Bien, Municipio> joinMunicipioBien;
  @Nullable
  private Join<Bien, Provincia> joinProvinciaBien;
  @Nullable
  private Join<Bien, Tasacion> joinTasacionBien;
  @Nullable
  private Join<Bien, Valoracion> joinValoracionBien;
  @Nullable
  private Join<Bien, Carga> joinCargaBien;
  @Nullable
  private Join<Expediente, Estimacion> joinEstimaciones;
  @Nullable
  private Join<Contrato, Procedimiento> joinProcedimientos;
  @Nullable
  private Join<Expediente, Propuesta> joinPropuestas;
  @Nullable
  private Join<Propuesta, ImportePropuesta> joinImportesPropuestas;
  @Nullable
  private Join<Propuesta, Contrato> joinContratosPropuestas;
  @Nullable
  private Join<Propuesta, PropuestaBien> joinPropuestaBien;
  @Nullable
  private Join<PropuestaBien, Bien> joinBienPropuestaBien;
  @Nullable
  private Join<Propuesta, Formalizacion> joinFormalizacionPropuesta;
  @Nullable
  private Join<Expediente, AsignacionExpediente> joinAsignacionExpediente;
  @Nullable
  private Join<Expediente, Cartera> joinCartera;
  @Nullable
  private Join<ClienteCartera, Cliente> joinCliente;
  @Nullable
  private Join<Contrato, ContratoInterviniente> joinContratoInterviniente;
  @Nullable
  private Join<Expediente, Usuario> joinSupervisor;
  @Nullable
  private Join<Expediente, Usuario> joinGestor;
  @Nullable
  private Join<Expediente, Usuario> joinGestorFormalizacion;
  @Nullable
  private Join<Interviniente,ContratoInterviniente>joinIntervinienteContratoInterviniente;
  @Nullable
  private Join<ContratoInterviniente,Contrato>joinContratoIntervinienteContrato;
  @Nullable
  private Join<Contrato,Expediente>joinContratoExpediente;

  public BusquedaBuilderInterviniente(CriteriaBuilder criteriaBuilder) {
    this.cb = criteriaBuilder;
    this.query = criteriaBuilder.createQuery(Interviniente.class);
    this.root = query.from(Interviniente.class);
    this.predicates = new ArrayList<>();
  }

  public CriteriaQuery<Interviniente> createQuery() {
    var q = query.select(root).where(predicates.toArray(new Predicate[0])).distinct(true);
    if (orderField != null) {
      q = q.orderBy(this.ordenAscendente ? this.cb.asc(orderField) : this.cb.desc(orderField));
    }
    return q;
  }

  private Join<Interviniente,ContratoInterviniente>getJoinIntervinienteContratoInterviniente(){
    if (this.joinIntervinienteContratoInterviniente==null){
      this.joinIntervinienteContratoInterviniente=root.join(Interviniente_.contratos,JoinType.INNER);
    }
    return this.joinIntervinienteContratoInterviniente;
  }


  private Join<ContratoInterviniente,Contrato>getJoinContratoIntervinienteContrato(){
    if (this.joinContratoIntervinienteContrato==null){
      this.joinContratoIntervinienteContrato=getJoinIntervinienteContratoInterviniente().join(ContratoInterviniente_.contrato,JoinType.INNER);
    }
    return this.joinContratoIntervinienteContrato;
  }


  private Join<Contrato,Expediente>getJoinContratoExpediente(){
    if(this.joinContratoExpediente==null){
      this.joinContratoExpediente=getJoinContratoIntervinienteContrato().join(Contrato_.expediente,JoinType.INNER);
    }
    return this.joinContratoExpediente;
  }

  private Join<Expediente, AsignacionExpediente> getJoinAsignacionExpediente() {
    if (this.joinAsignacionExpediente == null) {
      this.joinAsignacionExpediente = getJoinContratoExpediente().join(Expediente_.asignaciones, JoinType.LEFT);
    }
    return this.joinAsignacionExpediente;
  }

  private Join<Expediente, TipoEstado> getJoinEstadoExpediente() {
    if (this.joinEstadoExpediente == null) {
      this.joinEstadoExpediente = getJoinContratoExpediente().join(Expediente_.tipoEstado, JoinType.INNER);
    }
    return this.joinEstadoExpediente;
  }

  private Join<Expediente, Contrato> getJoinContratos() {
    if (this.joinContratos == null) {
      this.joinContratos = getJoinContratoExpediente().join(Expediente_.contratos, JoinType.INNER);
    }
    return this.joinContratos;
  }

  private Join<Expediente, Cartera> getJoinCarteraExpediente() {
    if (this.joinCarteraExpediente == null) {
      this.joinCarteraExpediente = getJoinContratoExpediente().join(Expediente_.cartera, JoinType.INNER);
    }
    return this.joinCarteraExpediente;
  }

  private Join<Expediente, Contrato> getJoinContratoRepresentante() {
    if (this.joinContratoRepresentante == null) {
      this.joinContratoRepresentante = getJoinContratoExpediente().join(Expediente_.contratos, JoinType.INNER);
      this.joinContratoRepresentante.on(cb.equal(this.joinContratoRepresentante.get(Contrato_.esRepresentante), true));
    }
    return this.joinContratoRepresentante;
  }

  private Join<Contrato, Movimiento> getJoinMovimientos() {
    if (this.joinMovimientos == null) {
      this.joinMovimientos = this.getJoinContratos().join(Contrato_.movimientos, JoinType.INNER);
    }
    return this.joinMovimientos;
  }

  private Join<Movimiento, TipoMovimiento> getJoinTipoMovimientos() {
    if (this.joinTipoMovimientos == null) {
      this.joinTipoMovimientos = this.getJoinMovimientos().join(Movimiento_.tipo, JoinType.INNER);
    }
    return this.joinTipoMovimientos;
  }

  private Join<Contrato, ContratoInterviniente> getJoinContratoIntervinientes() {
    if (this.joinContratoIntervinientes == null) {
      this.joinContratoIntervinientes = this.getJoinContratos().join(Contrato_.intervinientes, JoinType.INNER);
    }
    return this.joinContratoIntervinientes;
  }

  private Join<ContratoInterviniente, Interviniente> getJoinIntervinientes() {
    if (this.joinIntervinientes == null) {
      this.joinIntervinientes = this.getJoinContratoIntervinientes().join(ContratoInterviniente_.interviniente, JoinType.INNER);
    }
    return this.joinIntervinientes;
  }

  private Join<Contrato, ContratoBien> getJoinContratoBienes() {
    if (this.joinContratoBienes == null) {
      this.joinContratoBienes = this.getJoinContratos().join(Contrato_.bienes, JoinType.INNER);
    }
    return this.joinContratoBienes;
  }

  private Join<ContratoBien, Bien> getJoinBienes() {
    if (this.joinBienes == null) {
      this.joinBienes = this.getJoinContratoBienes().join(ContratoBien_.bien, JoinType.INNER);
    }
    return this.joinBienes;
  }

  private Join<Contrato, Saldo> getJoinSaldos() {
    if (this.joinSaldos == null) {
      this.joinSaldos = this.getJoinContratoRepresentante().join(Contrato_.saldos, JoinType.INNER);
    }
    return this.joinSaldos;
  }

  private Join<Interviniente, Pais> getJoinResidenciaInterviniente() {
    if (this.joinPaisInterviniente == null) {
      this.joinPaisInterviniente = this.getJoinIntervinientes().join(Interviniente_.residencia, JoinType.INNER);
    }
    return this.joinPaisInterviniente;
  }

  private Join<Interviniente, Pais> getJoinNacionalidadInterviniente() {
    if (this.joinPaisInterviniente == null) {
      this.joinPaisInterviniente = this.getJoinIntervinientes().join(Interviniente_.nacionalidad, JoinType.INNER);
    }
    return this.joinPaisInterviniente;
  }

  private Join<Interviniente, DatoContacto> getJoinDatosContactoInterviniente() {
    if (this.joinDatosContactoInterviniente == null) {
      this.joinDatosContactoInterviniente = this.getJoinIntervinientes().join(Interviniente_.datosContacto, JoinType.INNER);
    }
    return this.joinDatosContactoInterviniente;
  }

  private Join<Interviniente, Direccion> getJoinDireccionInterviniente() {
    if (this.joinDireccionInterviniente == null) {
      this.joinDireccionInterviniente = this.getJoinIntervinientes().join(Interviniente_.direcciones, JoinType.INNER);
    }
    return this.joinDireccionInterviniente;
  }

  private Join<Bien, Localidad> getJoinLocalidadBien() {
    if (this.joinLocalidadBien == null) {
      this.joinLocalidadBien = this.getJoinBienes().join(Bien_.localidad, JoinType.INNER);
    }
    return this.joinLocalidadBien;
  }

  private Join<Bien, Municipio> getJoinMunicipioBien() {
    if (this.joinMunicipioBien == null) {
      this.joinMunicipioBien = this.getJoinBienes().join(Bien_.municipio);
    }
    return this.joinMunicipioBien;
  }

  private Join<Bien, Provincia> getJoinProvinciaBien() {
    if (this.joinProvinciaBien == null) {
      this.joinProvinciaBien = this.getJoinBienes().join(Bien_.provincia, JoinType.INNER);
    }
    return this.joinProvinciaBien;
  }

  private Join<Bien, Tasacion> getJoinTasacionBien() {
    if (this.joinTasacionBien == null) {
      this.joinTasacionBien = this.getJoinBienes().join(Bien_.tasaciones, JoinType.INNER);
    }
    return this.joinTasacionBien;
  }

  private Join<Bien, Valoracion> getJoinValoracionBien() {
    if (this.joinValoracionBien == null) {
      this.joinValoracionBien = this.getJoinBienes().join(Bien_.valoraciones, JoinType.INNER);
    }
    return this.joinValoracionBien;
  }

  private Join<Bien, Carga> getJoinCargaBien() {
    if (this.joinCargaBien == null) {
      this.joinCargaBien = this.getJoinBienes().join(Bien_.cargas, JoinType.INNER);
    }
    return this.joinCargaBien;
  }

  private Join<Expediente, Estimacion> getJoinEstimaciones() {
    if (this.joinEstimaciones == null) {
      this.joinEstimaciones = getJoinContratoExpediente().join(Expediente_.estimaciones, JoinType.INNER);
    }
    return this.joinEstimaciones;
  }

  private Join<Contrato, Procedimiento> getJoinProcedimientos() {
    if (this.joinProcedimientos == null) {
      this.joinProcedimientos = this.getJoinContratos().join(Contrato_.procedimientos, JoinType.INNER);
    }
    return this.joinProcedimientos;
  }

  private Join<Expediente, Propuesta> getJoinPropuestas() {
    if (this.joinPropuestas == null) {
      this.joinPropuestas = getJoinContratoExpediente().join(Expediente_.propuestas, JoinType.INNER);
      this.joinPropuestas.on(cb.equal(this.joinPropuestas.get(Propuesta_.activo), true));
    }
    return this.joinPropuestas;
  }

  private Join<Propuesta, ImportePropuesta> getJoinImportesPropuestas() {
    if (this.joinImportesPropuestas == null) {
      this.joinImportesPropuestas = this.getJoinPropuestas().join(Propuesta_.importesPropuestas, JoinType.INNER);
    }
    return this.joinImportesPropuestas;
  }

  private Join<Propuesta, Contrato> getJoinContratosPropuestas() {
    if (this.joinContratosPropuestas == null) {
      this.joinContratosPropuestas = this.getJoinPropuestas().join(Propuesta_.contratos, JoinType.INNER);
    }
    return this.joinContratosPropuestas;
  }

  private Join<Propuesta, PropuestaBien> getJoinPropuestaBien() {
    if (this.joinPropuestaBien == null) {
      this.joinPropuestaBien = this.getJoinPropuestas().join(Propuesta_.bienes, JoinType.INNER);
    }
    return this.joinPropuestaBien;
  }

  private Join<PropuestaBien, Bien> getJoinBienPropuestaBien() {
    if (this.joinBienPropuestaBien == null) {
      this.joinBienPropuestaBien = this.getJoinPropuestaBien().join(PropuestaBien_.bien, JoinType.INNER);
    }
    return this.joinBienPropuestaBien;
  }

  private Join<Propuesta, Formalizacion> getJoinFormalizacionPropuesta() {
    if (this.joinFormalizacionPropuesta == null) {
      this.joinFormalizacionPropuesta = this.getJoinPropuestas().join(Propuesta_.formalizaciones);
    }
    return this.joinFormalizacionPropuesta;
  }

  private Join<Expediente, Cartera> getJoinCartera() {
    if (this.joinCartera == null) {
      this.joinCartera = this.getJoinContratoExpediente().join(Expediente_.cartera);
    }
    return this.joinCartera;
  }

  private Join<ClienteCartera, Cliente> getJoinCliente() {
    if (this.joinCliente == null) {
      this.joinCliente = this.getJoinCartera().join(Cartera_.clientes).join(ClienteCartera_.cliente);
    }
    return this.joinCliente;
  }

  private Join<Contrato, ContratoInterviniente> getJoinPrimerInterviniente() {
    joinContratoInterviniente = this.getJoinContratoRepresentante().join(Contrato_.intervinientes);
    joinContratoInterviniente.on(cb.equal(joinContratoInterviniente.get(ContratoInterviniente_.ordenIntervencion), 1));
    return this.joinContratoInterviniente;
  }

  private Join<Expediente, Usuario> getJoinSupervisor() {
    var joinAsignacionExpediente = this.getJoinAsignacionExpediente().get(AsignacionExpediente_.usuario).get(Usuario_.perfil);
    joinSupervisor.on(cb.equal(joinAsignacionExpediente.get(Perfil_.nombre), Perfil.SUPERVISOR));
    return this.joinSupervisor;
  }

  private Join<Expediente, Usuario> getJoinGestor() {
    if (this.joinGestor == null){
      this.joinGestor = this.getJoinAsignacionExpediente().join(AsignacionExpediente_.USUARIO);
      joinGestor.on(cb.equal(joinGestor.get(Usuario_.perfil).get(Perfil_.nombre), Perfil.GESTOR));
    }
    return this.joinGestor;
  }

  public Join<Expediente, Usuario> getJoinGestorFormalizacion() {
    if (this.joinGestorFormalizacion == null){
      this.joinGestorFormalizacion = this.getJoinAsignacionExpediente().join(AsignacionExpediente_.USUARIO);
      joinGestorFormalizacion.on(cb.equal(joinGestorFormalizacion.get(Usuario_.perfil).get(Perfil_.nombre), Perfil.GESTOR_FORMALIZACION));
    }
    return this.joinGestorFormalizacion;
  }

  public Join<Expediente, Usuario> getJoinGestorFormalizacionLegal() {
    if (this.joinGestorFormalizacion == null){
      this.joinGestorFormalizacion = this.getJoinAsignacionExpediente().join(AsignacionExpediente_.USUARIO);
      joinGestorFormalizacion.on(cb.equal(joinGestorFormalizacion.get(Usuario_.perfil).get(Perfil_.nombre), Perfil.SOPORTE_FORMALIZACION));
    }
    return this.joinGestorFormalizacion;
  }

  public void addCondicionesExpediente(BusquedaExpedienteDto busquedaExpedienteDto, Usuario loggedUser) throws NotFoundException {
    //filtramos primero las asignaciones del usuario logado
    if(loggedUser.getRolHaya() == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("No Haya role found for this user");
      else throw new NotFoundException("No se ha encontrado un rol de Haya para este usuario");
    }
    if(!loggedUser.esAdministrador()){
      Predicate asignaciones = cb.equal(this.getJoinAsignacionExpediente().get(AsignacionExpediente_.usuario).get(Usuario_.id),loggedUser.getId());
      Predicate asignacionesSuplente;
      Predicate predicateFinal = asignaciones;
      if(loggedUser.getPerfil().getNombre().compareTo(Perfil.GESTOR) == 0) {

        Subquery<Integer> subquerySuplente = cb.createQuery().subquery(Integer.class);
        subquerySuplente.distinct(true);
        Root<Expediente> rootSuplentes = subquerySuplente.from(Expediente.class);
        Join<Expediente, AsignacionExpediente> joinSuplente = rootSuplentes.join(Expediente_.asignaciones, JoinType.INNER);
        Predicate predicateSuplente = cb.equal( joinSuplente.join(AsignacionExpediente_.SUPLENTE, JoinType.INNER).get(Usuario_.ID),loggedUser.getId());
        Subquery<Integer> subquerySuplente2 = subquerySuplente.select(rootSuplentes.get(Expediente_.id)).distinct(true).where(predicateSuplente);
        subquerySuplente2.distinct(true);

        predicateFinal = cb.or(asignaciones,root.get(Expediente_.ID).in(subquerySuplente2.distinct(true)));
      }
      this.predicates.add(predicateFinal);
    }

    if (busquedaExpedienteDto.getId() != null)
      equal(this.getJoinContratoExpediente().get(Expediente_.ID_CONCATENADO), busquedaExpedienteDto.getId());
    if (busquedaExpedienteDto.getEstado() != null) {
      equal(this.getJoinEstadoExpediente().get(TipoEstado_.id), busquedaExpedienteDto.getEstado());
    }
    if (busquedaExpedienteDto.getSituacion() != null) {
      equal(this.getJoinContratoRepresentante().get(Contrato_.situacion).get(Situacion_.id), busquedaExpedienteDto.getSituacion());
    }
    if (busquedaExpedienteDto.getSubestado() != null) {
      equal(getJoinContratoExpediente().get(Expediente_.subEstado).get(SubtipoEstado_.id), busquedaExpedienteDto.getSubestado());
    }
    if (busquedaExpedienteDto.getCliente() != null){
      Join<Cartera, ClienteCartera> CaCc = getJoinCarteraExpediente().join(Cartera_.clientes, JoinType.INNER);
      Join<ClienteCartera, Cliente> CcCl = CaCc.join(ClienteCartera_.cliente, JoinType.INNER);

      this.predicates.add(cb.equal(CcCl.get(Cliente_.ID), busquedaExpedienteDto.getCliente()));
    }
    if (hayValor(busquedaExpedienteDto.getResponsableCartera())){
      Join<Cartera, Usuario> CaUs = getJoinCarteraExpediente().join(Cartera_.responsableCartera, JoinType.INNER);
      this.predicates.add(cb.like(CaUs.get(Usuario_.nombre),
        "%" + busquedaExpedienteDto.getResponsableCartera() + "%"));
    }
    if (hayValor(busquedaExpedienteDto.getSupervisor())){
      Join<Expediente, AsignacionExpediente> ExAe = getJoinContratoExpediente().join(Expediente_.asignaciones, JoinType.INNER);
      Join<AsignacionExpediente, Usuario> AeUs = ExAe.join(AsignacionExpediente_.usuario, JoinType.INNER);
      Predicate p1 = cb.like(AeUs.get(Usuario_.nombre), "%" + busquedaExpedienteDto.getSupervisor() + "%");
      Predicate p2 = cb.equal(AeUs.get(Usuario_.perfil).get(Perfil_.nombre), "Supervisor");

      this.predicates.add(cb.and(p1, p2));
    }
    if (hayValor(busquedaExpedienteDto.getGestor())){
      Join<Expediente, AsignacionExpediente> ExAe = getJoinContratoExpediente().join(Expediente_.asignaciones, JoinType.INNER);
      Join<AsignacionExpediente, Usuario> AeUs = ExAe.join(AsignacionExpediente_.usuario, JoinType.INNER);
      Predicate p1 = cb.like(AeUs.get(Usuario_.nombre), "%" + busquedaExpedienteDto.getGestor() + "%");
      Predicate p2 = cb.equal(AeUs.get(Usuario_.perfil).get(Perfil_.nombre), "Gestor");

      this.predicates.add(cb.and(p1, p2));
    }

    if (busquedaExpedienteDto.getCartera() != null)
      equal(getJoinContratoExpediente().get(Expediente_.cartera).get(Cartera_.id), busquedaExpedienteDto.getCartera());

    if (busquedaExpedienteDto.getEstrategia() != null) {
      //equal(root.get(Expediente_.estrategia).get(Estrategia_.id), busquedaExpedienteDto.getEstrategia());
      Join<Expediente, Contrato> join =
        getJoinContratoExpediente().join(Expediente_.contratos, JoinType.INNER);
      Predicate p1 = cb.isTrue(join.get(Contrato_.esRepresentante));
      Join<Contrato, EstrategiaAsignada> join1 =
        join.join(Contrato_.ESTRATEGIA, JoinType.INNER);
      Predicate p2 = cb.equal(
        join1.get(EstrategiaAsignada_.ESTRATEGIA).get(Estrategia_.ID), busquedaExpedienteDto.getEstrategia());
      predicates.add(cb.and(p1, p2));
    }

    if (busquedaExpedienteDto.getIntervaloFechas() != null && busquedaExpedienteDto.getIntervaloFechas().size() > 0) {
      for (IntervaloFecha busqueda : busquedaExpedienteDto.getIntervaloFechas()) {
        switch (busqueda.getDescripcion()) {
          case "creacion":
            //TODO pte añadir
            addIntervaloFechas(this.getJoinContratoExpediente().get(Expediente_.fechaCreacion), busqueda);
            break;
          case "cierre":
            addIntervaloFechas(this.getJoinContratoExpediente().get(Expediente_.fechaCierre), busqueda);
            break;
          default:
            break;
        }
      }
    }
  }

  public void addCondicionesContrato(BusquedaContratoDto busquedaContratoDto) {

    if (hayValor(busquedaContratoDto.getId()))
      equal(this.getJoinContratos().get(Contrato_.ID_CARGA), busquedaContratoDto.getId());
    if (busquedaContratoDto.getEstado() != null) {
      equal(this.getJoinContratos().get(Contrato_.estadoContrato).get(EstadoContrato_.ID), busquedaContratoDto.getEstado());
    }
    if (busquedaContratoDto.getProducto() != null) {
      equal(this.getJoinContratos().get(Contrato_.producto).get(Producto_.ID), busquedaContratoDto.getProducto());
    }
    if (busquedaContratoDto.getClasificacionContable() != null) {
      equal(this.getJoinContratos().get(Contrato_.clasificacionContable).get(ClasificacionContable_.ID), busquedaContratoDto.getClasificacionContable());
    }
    if (hayValor(busquedaContratoDto.getContratoOrigen())) {
      like(this.getJoinContratos().get(Contrato_.idCarga), busquedaContratoDto.getContratoOrigen());
    }
    if (busquedaContratoDto.getReestructurado() != null) {
      equal(this.getJoinContratos().get(Contrato_.reestructurado), busquedaContratoDto.getReestructurado());
    }
    if (busquedaContratoDto.getNumCuotasImpago() != null) {
      equal(this.getJoinContratos().get(Contrato_.numeroCuotasImpagadas), busquedaContratoDto.getNumCuotasImpago());
    }
    if (busquedaContratoDto.getTipoEstimacion() != null) {
      equal(this.getJoinEstimaciones().get(Estimacion_.tipo).get(TipoPropuesta_.id), busquedaContratoDto.getTipoEstimacion());
    }
    if (busquedaContratoDto.getSubtipoEstimacion() != null) {
      equal(this.getJoinEstimaciones().get(Estimacion_.subtipo).get(SubtipoPropuesta_.id), busquedaContratoDto.getSubtipoEstimacion());
    }
    if (busquedaContratoDto.getIntervaloImporte() != null) {
      switch (busquedaContratoDto.getIntervaloImporte().getDescripcion()) {
        case "capitalPendiente":
          addIntervaloImporte(this.getJoinSaldos().get(Saldo_.capitalPendiente), busquedaContratoDto.getIntervaloImporte());
          break;
        case "importeInicial":
          addIntervaloImporte(this.getJoinContratos().get(Contrato_.importeInicial), busquedaContratoDto.getIntervaloImporte());
          break;
        case "saldoGestion":
          addIntervaloImporte(this.getJoinSaldos().get(Saldo_.saldoGestion), busquedaContratoDto.getIntervaloImporte());
          break;
        case "deudaImpagada":
          addIntervaloImporte(this.getJoinSaldos().get(Saldo_.deudaImpagada), busquedaContratoDto.getIntervaloImporte());
          break;
        default:
          break;
      }
    }

    if (busquedaContratoDto.getIntervaloFechas() != null && busquedaContratoDto.getIntervaloFechas().size() > 0) {
      for (IntervaloFecha busqueda : busquedaContratoDto.getIntervaloFechas()) {
        switch (busqueda.getDescripcion()) {
          case "formalizacion":
            addIntervaloFechas(this.getJoinContratos().get(Contrato_.fechaFormalizacion), busqueda);
            break;
          case "vencimientoInicial":
            addIntervaloFechas(this.getJoinContratos().get(Contrato_.fechaVencimientoInicial), busqueda);
            break;
          case "vencimientoAnticipado":
            addIntervaloFechas(this.getJoinContratos().get(Contrato_.fechaVencimientoAnticipado), busqueda);
            break;
          case "paseRegular":
            addIntervaloFechas(this.getJoinContratos().get(Contrato_.fechaPaseRegular), busqueda);
            break;
          case "primerImpago":
            addIntervaloFechas(this.getJoinContratos().get(Contrato_.fechaPrimerImpago), busqueda);
            break;
          case "primeraAsignacion":
            addIntervaloFechas(this.getJoinContratos().get(Contrato_.fechaPrimeraAsignacion), busqueda);
            break;
          case "ultimaAsignacion":
            addIntervaloFechas(this.getJoinContratos().get(Contrato_.fechaUltimaAsignacion), busqueda);
            break;
          default:
            break;
        }
      }
    }


  }

  public void addCondicionesInterviniente(BusquedaIntervinienteDto busquedaIntervinienteDto) {

    if (busquedaIntervinienteDto.getId() != null)
      equal(root.get(Interviniente_.id), busquedaIntervinienteDto.getId());
    if (hayValor(busquedaIntervinienteDto.getIdIntervinienteOrigen()))
      like(root.get(Interviniente_.idCarga), busquedaIntervinienteDto.getIdIntervinienteOrigen());
    if (hayValor(busquedaIntervinienteDto.getDocId()))
      like(root.get(Interviniente_.numeroDocumento), busquedaIntervinienteDto.getDocId());
    if (hayValor(busquedaIntervinienteDto.getApellidos()))
      like(root.get(Interviniente_.apellidos), busquedaIntervinienteDto.getApellidos());
    if (hayValor(busquedaIntervinienteDto.getNombre()))
      like(root.get(Interviniente_.nombre), busquedaIntervinienteDto.getNombre());
    if (busquedaIntervinienteDto.getResidencia() != null) {
      equal(this.getJoinResidenciaInterviniente().get(Pais_.id), busquedaIntervinienteDto.getResidencia());
    }
    if (busquedaIntervinienteDto.getNacionalidad() != null) {
      equal(this.getJoinNacionalidadInterviniente().get(Pais_.id), busquedaIntervinienteDto.getNacionalidad());
    }
    if (busquedaIntervinienteDto.getTipoIntervencion() != null) {
      equal(this.getJoinContratoIntervinientes().get(ContratoInterviniente_.tipoIntervencion).get(TipoIntervencion_.id), busquedaIntervinienteDto.getTipoIntervencion());
    }
    if (busquedaIntervinienteDto.getVulnerabilidad() != null)
      equal(this.getJoinContratoIntervinientes().get(ContratoInterviniente_.interviniente).get(Interviniente_.vulnerabilidad), busquedaIntervinienteDto.getVulnerabilidad());

    if (hayValor(busquedaIntervinienteDto.getTelefono())) {
      like(this.getJoinDatosContactoInterviniente().get(DatoContacto_.movil), busquedaIntervinienteDto.getTelefono());
    }
    if (hayValor(busquedaIntervinienteDto.getEmail())) {
      like(this.getJoinDatosContactoInterviniente().get(DatoContacto_.email), busquedaIntervinienteDto.getEmail());
    }
    if (busquedaIntervinienteDto.getLocalidad() != null) {
      equal(this.getJoinDireccionInterviniente().get(Direccion_.localidad).get(Localidad_.id), busquedaIntervinienteDto.getLocalidad());
    }
    if (busquedaIntervinienteDto.getProvincia() != null) {
      equal(this.getJoinDireccionInterviniente().get(Direccion_.provincia).get(Provincia_.id), busquedaIntervinienteDto.getProvincia());
    }
    if (busquedaIntervinienteDto.getDireccion() != null) {
      like(this.getJoinDireccionInterviniente().get(Direccion_.nombre), "%"+busquedaIntervinienteDto.getDireccion()+"%");
    }

    if (busquedaIntervinienteDto.getIntervaloFecha() != null && busquedaIntervinienteDto.getIntervaloFecha().size() > 0) {
      for (IntervaloFecha busqueda : busquedaIntervinienteDto.getIntervaloFecha()) {
        switch (busqueda.getDescripcion()) {
          case "nacimiento":
            addIntervaloFechas(this.getJoinContratoIntervinientes().join(ContratoInterviniente_.interviniente).get(Interviniente_.fechaNacimiento), busqueda);
            break;
          case "constitucion":
            addIntervaloFechas(this.getJoinContratoIntervinientes().join(ContratoInterviniente_.interviniente).get(Interviniente_.fechaConstitucion), busqueda);
            break;
          default:
            break;
        }
      }
    }
  }

  public void addCondicionesBien(BusquedaBienDto busquedaBienDto) {

    if (busquedaBienDto.getId() != null)
      like(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.idHaya), busquedaBienDto.getId());
    if (hayValor(busquedaBienDto.getIdOrigen()))
      like(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.idCarga), busquedaBienDto.getIdOrigen());
    if (busquedaBienDto.getLocalidad() != null)
      equal(this.getJoinLocalidadBien().get(Localidad_.id), busquedaBienDto.getLocalidad());
    if (busquedaBienDto.getProvincia() != null)
      equal(this.getJoinProvinciaBien().get(Provincia_.id), busquedaBienDto.getProvincia());
    if (busquedaBienDto.getMunicipio() != null)
      equal(this.getJoinMunicipioBien().get(Municipio_.id), busquedaBienDto.getMunicipio());
    if (hayValor(busquedaBienDto.getRegistro()))
      like(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.registro), busquedaBienDto.getRegistro());
    if (hayValor(busquedaBienDto.getLocalidadRegistro()))
      like(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.localidadRegistro), busquedaBienDto.getLocalidadRegistro());
    if (hayValor(busquedaBienDto.getFincaRegistral()))
      like(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.finca), busquedaBienDto.getFincaRegistral());
    if (hayValor(busquedaBienDto.getIdufir()))
      like(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.idufir), busquedaBienDto.getIdufir());
    if (busquedaBienDto.getDireccion() != null) {
      like(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.nombreVia), busquedaBienDto.getDireccion());
    }
    if (busquedaBienDto.getReferenciaCatastral() != null) {
      like(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.referenciaCatastral), busquedaBienDto.getReferenciaCatastral());
    }
    if (busquedaBienDto.getIntervaloImporte() != null) {
      switch (busquedaBienDto.getIntervaloImporte().getDescripcion()) {
        case "tasacion":
          addIntervaloImporte(this.getJoinTasacionBien().get(Tasacion_.importe), busquedaBienDto.getIntervaloImporte());
          break;
        case "valoracion":
          addIntervaloImporte(this.getJoinValoracionBien().get(Valoracion_.importe), busquedaBienDto.getIntervaloImporte());
          break;
        case "adjudicacion":
          //TODO
          //addIntervalorImporte(this.getJoinAdjudicacionBien().get(), busquedaBienDto.getIntervaloImporte());
          break;
        default:
          break;
      }
    }
    if (busquedaBienDto.getIntervaloFechas() != null && busquedaBienDto.getIntervaloFechas().size() > 0) {
      for (IntervaloFecha busqueda : busquedaBienDto.getIntervaloFechas()) {
        switch (busqueda.getDescripcion()) {
          case "inscripcion":
            addIntervaloFechas(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.fechaInscripcion), busqueda);
            break;
          case "primeraValoracion":
            addIntervaloFechas(this.getJoinValoracionBien().get(Valoracion_.fecha), busqueda);
            break;
          case "primeraTasacion":
            addIntervaloFechas(this.getJoinTasacionBien().get(Tasacion_.fecha), busqueda);
            break;
          case "reoConversion":
            addIntervaloFechas(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.fechaReoConversion), busqueda);
            break;
          case "anotacion":
            addIntervaloFechas(this.getJoinCargaBien().get(Carga_.fechaAnotacion), busqueda);
            break;
          case "vencimientoCarga":
            addIntervaloFechas(this.getJoinCargaBien().get(Carga_.fechaVencimiento), busqueda);
            break;
          case "asignacionPN":
            //TODO
            //addIntervaloFechas(this.getJoinContratoBienes().get(ContratoBien_.bien).get(Bien_.fecha), busqueda);
            break;
          default:
            break;
        }
      }
    }
  }

  public void addCondicionesMovimiento(BusquedaMovimientoDto busquedaMovimientoDto) {

    if (busquedaMovimientoDto.getTipoMovimiento() != null) {
      equal(this.getJoinTipoMovimientos().get(TipoMovimiento_.id), busquedaMovimientoDto.getTipoMovimiento());
    }
    if (busquedaMovimientoDto.getIntervaloImporte() != null)
      addIntervaloImporte(this.getJoinMovimientos().get(Movimiento_.importe), busquedaMovimientoDto.getIntervaloImporte());

    if (busquedaMovimientoDto.getIntervaloFechas() != null && busquedaMovimientoDto.getIntervaloFechas().size() > 0) {
      for (IntervaloFecha busqueda : busquedaMovimientoDto.getIntervaloFechas()) {
        switch (busqueda.getDescripcion()) {
          case "valor":
            addIntervaloFechas(this.getJoinMovimientos().get(Movimiento_.fechaValor), busqueda);
            break;
          case "contable":
            addIntervaloFechas(this.getJoinMovimientos().get(Movimiento_.fechaContable), busqueda);
            break;
          default:
            break;
        }
      }
    }
  }

  public void addCondicionesEstimacion(BusquedaEstimacionDto busquedaEstimacionDto) {
    if (busquedaEstimacionDto.getTipoPropuesta() != null) {
      equal(this.getJoinEstimaciones().get(Estimacion_.tipo), busquedaEstimacionDto.getTipoPropuesta());
    }
    if (busquedaEstimacionDto.getSubtipoPropuesta() != null) {
      equal(this.getJoinEstimaciones().get(Estimacion_.subtipo), busquedaEstimacionDto.getSubtipoPropuesta());
    }
    if (busquedaEstimacionDto.getProbalididadResolucion() != null) {
      equal(this.getJoinEstimaciones().get(Estimacion_.probabilidadResolucion), busquedaEstimacionDto.getProbalididadResolucion());
    }
    if (busquedaEstimacionDto.getIntervaloFechas() != null && busquedaEstimacionDto.getIntervaloFechas().size() > 0) {
      for (IntervaloFecha busqueda : busquedaEstimacionDto.getIntervaloFechas()) {
        switch (busqueda.getDescripcion()) {
          case "creacion":
            addIntervaloFechas(this.getJoinEstimaciones().get(Estimacion_.fecha), busqueda);
            break;
          case "resolucion":
            addIntervaloFechas(this.getJoinEstimaciones().get(Estimacion_.fechaEstimada), busqueda);
            break;
          default:
            break;
        }
      }
    }
  }

  public void addCondicionesProcedimiento(BusquedaProcedimientoDto busquedaProcedimientoDto, TipoProcedimiento tipo, HitoProcedimiento fechaHito) throws NotFoundException {
    if (hayValor(busquedaProcedimientoDto.getLetrado())) {
      like(this.getJoinProcedimientos().get(Procedimiento_.letrado), busquedaProcedimientoDto.getLetrado());
    }
    if (hayValor(busquedaProcedimientoDto.getProcurador())) {
      like(this.getJoinProcedimientos().get(Procedimiento_.procurador), busquedaProcedimientoDto.getProcurador());
    }
    if (hayValor(busquedaProcedimientoDto.getJuzgado())) {
      like(this.getJoinProcedimientos().get(Procedimiento_.juzgado), busquedaProcedimientoDto.getJuzgado());
    }
    if (hayValor(busquedaProcedimientoDto.getNumeroAutos())) {
      like(this.getJoinProcedimientos().get(Procedimiento_.numeroAutos), busquedaProcedimientoDto.getNumeroAutos());
    }
    if (busquedaProcedimientoDto.getAdministradorConcursal() != null) {
      like(this.getJoinProcedimientos().get(Procedimiento_.nombreAdminConcursal), busquedaProcedimientoDto.getAdministradorConcursal());
    }
    if (busquedaProcedimientoDto.getTipoProcedimiento() != null) {
      equal(this.getJoinProcedimientos().get(Procedimiento_.tipo), busquedaProcedimientoDto.getTipoProcedimiento());
    }
    if (busquedaProcedimientoDto.getHitoProcedimiento() != null) {
      equal(this.getJoinProcedimientos().get(Procedimiento_.hitoProcedimiento), busquedaProcedimientoDto.getHitoProcedimiento());
    }
    if (busquedaProcedimientoDto.getIntervaloFecha() != null && tipo != null && fechaHito != null) {
      addFechasProcedimiento(busquedaProcedimientoDto, tipo, fechaHito);
    }
  }

  public void addCondicionesPropuesta(BusquedaPropuestaDto busquedaPropuestaDto, List<Integer> expedientes) {
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();

    if (busquedaPropuestaDto.getClase() != null) {
      equal(this.getJoinPropuestas().get(Propuesta_.clasePropuesta), busquedaPropuestaDto.getClase());
    }
    if (busquedaPropuestaDto.getTipo() != null) {
      equal(this.getJoinPropuestas().get(Propuesta_.tipoPropuesta), busquedaPropuestaDto.getTipo());
    }
    if (busquedaPropuestaDto.getSubtipo() != null) {
      equal(this.getJoinPropuestas().get(Propuesta_.subtipoPropuesta), busquedaPropuestaDto.getSubtipo());
    }
    if (busquedaPropuestaDto.getEstado() != null) {
      equal(this.getJoinPropuestas().get(Propuesta_.estadoPropuesta), busquedaPropuestaDto.getEstado());
    }
    if (busquedaPropuestaDto.getSancion() != null) {
      equal(this.getJoinPropuestas().get(Propuesta_.sancionPropuesta), busquedaPropuestaDto.getSancion());
    }
    if (busquedaPropuestaDto.getResolucion() != null) {
      equal(this.getJoinPropuestas().get(Propuesta_.resolucionPropuesta), busquedaPropuestaDto.getResolucion());
    }
    if (hayValor(busquedaPropuestaDto.getGestor())) {
      like(this.getJoinPropuestas().get(Propuesta_.usuario).get(Usuario_.nombre), busquedaPropuestaDto.getGestor());
    }
    if (hayValor(busquedaPropuestaDto.getAnalista())) {
      like(this.getJoinFormalizacionPropuesta().get(Formalizacion_.analistaCliente), busquedaPropuestaDto.getAnalista());
    }
    if (hayValor(busquedaPropuestaDto.getGestorRecuperaciones())) {
      like(this.getJoinFormalizacionPropuesta().get(Formalizacion_.gestorRecuperacionCliente), busquedaPropuestaDto.getGestorRecuperaciones());
    }
    if (hayValor(busquedaPropuestaDto.getNotaria())) {
      like(this.getJoinFormalizacionPropuesta().get(Formalizacion_.direccionNotaria), busquedaPropuestaDto.getNotaria());
    }

    if (busquedaPropuestaDto.getFormalizacionCompleta() != null){
      BusquedaFormalizacionCompletaDto fC = busquedaPropuestaDto.getFormalizacionCompleta();
      if (fC.getProvinciaActivos() != null){
        equal(this.getJoinBienPropuestaBien().get(Bien_.provincia).get(Provincia_.ID), fC.getProvinciaActivos());
      }
      if (fC.getSubestadoFormalizacion()!= null){
        equal(this.getJoinFormalizacionPropuesta().get(Formalizacion_.SUBESTADO_FORMALIZACION).get(SubestadoFormalizacion_.ID), fC.getSubestadoFormalizacion());
      }
      if (fC.getResultadoCheck()!= null){
        equal(this.getJoinFormalizacionPropuesta().get(Formalizacion_.ESTADO_CHECKLIST).get(EstadoChecklist_.ID), fC.getResultadoCheck());
      }
      if (fC.getFirmaEstimada()!= null){
        predicates.add(cb.equal(
          this.getJoinFormalizacionPropuesta().get(Formalizacion_.FECHA_FIRMA_ESTIMADA),
          fC.getFirmaEstimada()));
      }
      if (fC.getPropuestaOrigen()!= null){
        like(
          this.getJoinPropuestas().get(Propuesta_.ID_CARGA),
          fC.getPropuestaOrigen());
      }
      if (fC.getGestoria()!= null){
        like(
          this.getJoinFormalizacionPropuesta().get(Formalizacion_.GESTORIA_TRAMITADORA),
          fC.getGestoria());
      }
      if (fC.getDespachoLegal()!= null){
        like(
          this.getJoinFormalizacionPropuesta().get(Formalizacion_.DESPACHO_LEGAL),
          fC.getDespachoLegal());
      }
      if (fC.getDespachoTecnico()!= null){
        like(
          this.getJoinFormalizacionPropuesta().get(Formalizacion_.DESPACHO_TECNICO),
          fC.getDespachoTecnico());
      }

      if (fC.getGestorFormalizacionLegal()!= null){
        like(
          this.getJoinGestorFormalizacionLegal().get(Usuario_.NOMBRE),
          fC.getGestorFormalizacionLegal());
      }
      if (fC.getGestorFormalizacionPrincipal()!= null){
        like(
          this.getJoinGestorFormalizacion().get(Usuario_.NOMBRE),
          fC.getGestorFormalizacionPrincipal());
      }
      if (fC.getGestorConcursos()!= null){
        equal(this.getJoinFormalizacionPropuesta().get(Formalizacion_.GESTOR_CONCURSOS).get(GestorConcursos_.ID), fC.getGestorConcursos());
      }
      if (fC.getGestorGestoria()!= null){
        equal(this.getJoinFormalizacionPropuesta().get(Formalizacion_.GESTOR_GESTORIA).get(GestorGestoria_.ID), fC.getGestorGestoria());
      }
      if (fC.getGestorRecuperacionesCliente()!= null){
        like(this.getJoinFormalizacionPropuesta().get(Formalizacion_.GESTOR_RECUPERACION_CLIENTE), fC.getGestorRecuperacionesCliente());
      }
      if (fC.getTerritorial()!= null){
        like(
          this.getJoinFormalizacionPropuesta().get(Formalizacion_.TERRITORIAL),
          fC.getTerritorial());
      }
      if (fC.getGestorFormalizacionLetradoCliente()!= null){
        like(
          this.getJoinFormalizacionPropuesta().get(Formalizacion_.GESTOR_FORMALIZACION_CLIENTE),
          fC.getGestorFormalizacionLetradoCliente());
      }
      if (fC.getConcurso()!= null){
        Join<Contrato, Procedimiento> joinProce = this.getJoinContratosPropuestas().join(Contrato_.PROCEDIMIENTOS, JoinType.INNER);
        Join<Procedimiento, FormalizacionProcedimiento> joinFormProce = joinProce.join(Procedimiento_.FORMALIZACION_PROCEDIMIENTO, JoinType.INNER);
        equal(
          joinFormProce.get(FormalizacionProcedimiento_.CONCURSO),
          fC.getConcurso());
      }
      if (fC.getPrecioDeCompra()!= null){
        Join<Contrato, FormalizacionContrato> joinFormCont = this.getJoinContratosPropuestas().join(Contrato_.FORMALIZACION_CONTRATO, JoinType.INNER);
        equal(
          joinFormCont.get(FormalizacionContrato_.PRECIO_COMPRA),
          fC.getPrecioDeCompra());
      }
      if (fC.getImporteEntregaDeEfectivo()!= null){
        equal(
          this.getJoinFormalizacionPropuesta().get(Formalizacion_.IMPORTE_ENTREGA_EFECTIVO),
          fC.getImporteEntregaDeEfectivo());
      }
      if (fC.getEntregaDeBienesLibres()!= null){
        equal(
          this.getJoinFormalizacionPropuesta().get(Formalizacion_.ENTREGA_DE_BIENES_LIBRES),
          fC.getEntregaDeBienesLibres());
      }
      if (fC.getTipologia()!= null){
        equal(this.getJoinBienPropuestaBien().get(Bien_.TIPO_BIEN).get(TipoBien_.ID), fC.getTipologia());
      }
      if (fC.getSubtipoActivo()!= null){
        equal(this.getJoinBienPropuestaBien().get(Bien_.SUB_TIPO_BIEN).get(SubTipoBien_.ID), fC.getSubtipoActivo());
      }
      if (fC.getDireccionFormalizacion()!= null){
        like(this.getJoinBienPropuestaBien().get(Bien_.NOMBRE_VIA), fC.getDireccionFormalizacion());
      }
      if (fC.getRiesgoConsignacion()!= null){
        Join<Bien, FormalizacionBien> joinFormBien = this.getJoinBienPropuestaBien().join(Bien_.FORMALIZACION_BIEN, JoinType.INNER);
        equal(joinFormBien.get(FormalizacionBien_.RIESGO_CONSIGNACION), fC.getRiesgoConsignacion());
      }
      if (fC.getTipoCargaFormalizacion()!= null){
        Join<Bien, Carga> joinBienCarg = this.getJoinBienPropuestaBien().join(Bien_.CARGAS, JoinType.INNER);
        equal(joinBienCarg.get(Carga_.TIPO_CARGA).get(TipoCarga_.ID), fC.getTipoCargaFormalizacion());
      }
      if (fC.getCargaPropia()!= null){
        Join<Bien, Carga> joinBienCarg = this.getJoinBienPropuestaBien().join(Bien_.CARGAS, JoinType.INNER);
        equal(joinBienCarg.get(Carga_.CARGA_PROPIA), fC.getCargaPropia());
      }
      if (fC.getTipologiaCarga()!= null){
        Join<Bien, Carga> joinBienCarg = this.getJoinBienPropuestaBien().join(Bien_.CARGAS, JoinType.INNER);
        equal(joinBienCarg.get(Carga_.TIPO_CARGA_FORMALIZACION).get(TipoCargaFormalizacion_.ID), fC.getTipologiaCarga());
      }
      if (fC.getSubtipoCarga()!= null){
        Join<Bien, Carga> joinBienCarg = this.getJoinBienPropuestaBien().join(Bien_.CARGAS, JoinType.INNER);
        equal(joinBienCarg.get(Carga_.SUBTIPO_CARGA_FORMALIZACION).get(SubtipoCargaFormalizacion_.ID), fC.getSubtipoCarga());
      }
    }

    if (busquedaPropuestaDto.getFormalizacion() != null) {
      if (busquedaPropuestaDto.getFormalizacion().getGestor() != null)
        like(this.getJoinGestorFormalizacion().get(Usuario_.nombre), busquedaPropuestaDto.getFormalizacion().getGestor());
      if (busquedaPropuestaDto.getFormalizacion().getGestorCliente() != null)
        like(this.getJoinFormalizacionPropuesta().get(Formalizacion_.gestorFormalizacionCliente), busquedaPropuestaDto.getFormalizacion().getGestorCliente());
      if (busquedaPropuestaDto.getFormalizacion().getDocumentacionCumplimentada() != null){
        if (busquedaPropuestaDto.getFormalizacion().getDocumentacionCumplimentada())
          predicates.add(cb.equal(this.getJoinFormalizacionPropuesta().get(Formalizacion_.ESTADO_CHECKLIST).get(EstadoChecklist_.CODIGO), "ok"));
        else
          predicates.add(cb.notEqual(this.getJoinFormalizacionPropuesta().get(Formalizacion_.ESTADO_CHECKLIST).get(EstadoChecklist_.CODIGO), "ok"));
      }
      if (busquedaPropuestaDto.getFormalizacion().getPbc() != null)
        equal(this.getJoinFormalizacionPropuesta().get(Formalizacion_.pbc), busquedaPropuestaDto.getFormalizacion().getPbc());
    }

    if (busquedaPropuestaDto.getIntervaloImporte() != null) {
      switch (busquedaPropuestaDto.getIntervaloImporte().getDescripcion()) {
        case "deuda":
          Subquery<Double> subqueryDeudaImpagada = cb.createQuery().subquery(Double.class);
          Root<Contrato> importesContratos = subqueryDeudaImpagada.from(Contrato.class);
          addIntervaloImporte(
            subqueryDeudaImpagada.select(cb.sum(importesContratos.get(Contrato_.deudaImpagada))).distinct(true).where(cb.equal(this.getJoinContratosPropuestas().get(Propuesta_.ID), importesContratos.join(Contrato_.propuestas).get(Propuesta_.id))),
            busquedaPropuestaDto.getIntervaloImporte());
          break;
        case "propuesta":
          addIntervaloImporte(this.getJoinPropuestas().get(Propuesta_.importeTotal), busquedaPropuestaDto.getIntervaloImporte());
          break;
        case "quita":
          Subquery<Double> subqueryQuita = cb.createQuery().subquery(Double.class);
          Root<ImportePropuesta> importes = subqueryQuita.from(ImportePropuesta.class);
          addIntervaloImporte(
            subqueryQuita.select(cb.sum(importes.get(ImportePropuesta_.condonacionDeuda))).distinct(true).where(cb.equal(this.getJoinImportesPropuestas().get(Propuesta_.ID), importes.join(ImportePropuesta_.propuestas).get(Propuesta_.id))),
            busquedaPropuestaDto.getIntervaloImporte());
          break;
        case "prestamoInicial":
          Subquery<Double> subqueryPrestamoInicial = cb.createQuery().subquery(Double.class);
          Root<Contrato> importesContratos2 = subqueryPrestamoInicial.from(Contrato.class);
          addIntervaloImporte(
            subqueryPrestamoInicial.select(cb.sum(importesContratos2.get(Contrato_.importeDispuesto))).distinct(true).where(cb.equal(this.getJoinContratosPropuestas().get(Propuesta_.ID), importesContratos2.join(Contrato_.propuestas).get(Propuesta_.id))),
            busquedaPropuestaDto.getIntervaloImporte());
          break;
        case "valoracion":
          if(expedientes != null){
            this.predicates.add(this.getJoinContratoExpediente().get(Expediente_.id).in(expedientes));
          }
          break;
        default:
          break;
      }
    }

    if (busquedaPropuestaDto.getIntervaloFechas() != null && busquedaPropuestaDto.getIntervaloFechas().size() > 0) {
      for (IntervaloFecha busqueda : busquedaPropuestaDto.getIntervaloFechas()) {
        switch (busqueda.getDescripcion()) {
          case "alta":
            addIntervaloFechas(this.getJoinPropuestas().get(Propuesta_.fechaAlta), busqueda);
            break;
          case "sancion":
            addIntervaloFechas(this.getJoinFormalizacionPropuesta().get(Formalizacion_.fechaSancion), busqueda);
            break;
          case "firma":
            addIntervaloFechas(this.getJoinFormalizacionPropuesta().get(Formalizacion_.fechaFirma), busqueda);
            break;
          case "ultimoCambio":
            addIntervaloFechas(this.getJoinPropuestas().get(Propuesta_.fechaUltimoCambio), busqueda);
            break;
          case "posesion":
            addIntervaloFechas(this.getJoinFormalizacionPropuesta().get(Formalizacion_.fechaPosesion), busqueda);
            break;
          default:
            break;
        }
      }
    }
  }

  private void filterImportes(Expression<Double> atributo, IntervaloImporte ii){
    Predicate predicate = null;
    if (ii.getIgual() != null) {
      predicate = cb.equal(atributo, ii.getIgual());
    } else if (ii.getMayor() != null && ii.getMenor() != null) {
      predicate = cb.between(atributo, ii.getMayor(), ii.getMenor());
    } else {
      if (ii.getMayor() != null)
        predicate = cb.greaterThan(atributo, ii.getMayor());
      if (ii.getMenor() != null)
        predicate = cb.lessThan(atributo, ii.getMenor());
    }
    if (predicate != null)
      this.predicates.add(predicate);
  }

  public void addCondicionesAgenda(List<Integer> expedientesIds) {
    if(expedientesIds != null){
      this.predicates.add(this.getJoinContratoExpediente().get(Expediente_.id).in(expedientesIds));
    }
  }

  private void addFechasProcedimiento(BusquedaProcedimientoDto busquedaProcedimientoDto, TipoProcedimiento tipo, HitoProcedimiento fechaHito) throws NotFoundException {

    switch (tipo.getCodigo()) {
      case "HIP":
        switch (fechaHito.getCodigo()) {
          case "HIP_02":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoHipotecario.class).get(ProcedimientoHipotecario_.fechaOposicion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "HIP_05":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoHipotecario.class).get(ProcedimientoHipotecario_.fechaNotificacionAuto), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "HIP_06":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoHipotecario.class).get(ProcedimientoHipotecario_.fechaCertificacion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "HIP_09":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoHipotecario.class).get(ProcedimientoHipotecario_.fechaPresentacionDemanda), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          default:
            break;
        }
        break;
      case "ETNJ":
        switch (fechaHito.getCodigo()) {
          case "ETNJ_02":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEtnj.class).get(ProcedimientoEtnj_.fechaOposicion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "ETNJ_04":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEtnj.class).get(ProcedimientoEtnj_.fechaNotificacionReqPago), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "ETNJ_05":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEtnj.class).get(ProcedimientoEtnj_.fechaAutoDespachando), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "ETNJ_07":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEtnj.class).get(ProcedimientoEtnj_.fechaPresentacionDemanda), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          default:
            break;
        }
        break;
      case "EN":
        switch (fechaHito.getCodigo()) {
          case "EN_01":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEjecucionNotarial.class).get(ProcedimientoEjecucionNotarial_.fechaPosesion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "EN_02":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEjecucionNotarial.class).get(ProcedimientoEjecucionNotarial_.fechaFirmaEscritura), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "EN_06":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEjecucionNotarial.class).get(ProcedimientoEjecucionNotarial_.fechaSubasta), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "EN_07":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEjecucionNotarial.class).get(ProcedimientoEjecucionNotarial_.fechaAuto), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "EN_08":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEjecucionNotarial.class).get(ProcedimientoEjecucionNotarial_.fechaReqPagoDeudor), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "EN_09":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEjecucionNotarial.class).get(ProcedimientoEjecucionNotarial_.fechaRecepcionCertificacionRegistral), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "EN_10":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEjecucionNotarial.class).get(ProcedimientoEjecucionNotarial_.fechaFirmaActa), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          default:
            break;
        }
        break;
      case "MON":
        switch (fechaHito.getCodigo()) {
          case "MON_02":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoMonitorio.class).get(ProcedimientoMonitorio_.fechaOposicion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "MON_03":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoMonitorio.class).get(ProcedimientoMonitorio_.fecha), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "MON_04":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoMonitorio.class).get(ProcedimientoMonitorio_.fechaAdmisionDemanda), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "MON_06":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoMonitorio.class).get(ProcedimientoMonitorio_.fechaPresentacionDemanda), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          default:
            break;
        }
        break;
      case "VERB":
        switch (fechaHito.getCodigo()) {
          case "VERB_02":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoVerbal.class).get(ProcedimientoVerbal_.fechaCelebracionJuicio), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "VERB_04":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoVerbal.class).get(ProcedimientoVerbal_.fechaNotificacion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "VERB_05":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoVerbal.class).get(ProcedimientoVerbal_.fechaAdmisionDemanda), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "VERB_07":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoVerbal.class).get(ProcedimientoVerbal_.fechaPresentacionDemanda), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          default:
            break;
        }
        break;
      case "ORD":
        switch (fechaHito.getCodigo()) {
          case "ORD_02":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoOrdinario.class).get(ProcedimientoOrdinario_.fechaOposicion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "ORD_03":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoOrdinario.class).get(ProcedimientoOrdinario_.fechaReqPagoDeudor), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "ORD_06":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoOrdinario.class).get(ProcedimientoOrdinario_.fechaAdmisionDemanda), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "ORD_08":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoOrdinario.class).get(ProcedimientoOrdinario_.fechaPresentacionDemanda), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          default:
            break;
        }
        break;
      case "ETJ":
        switch (fechaHito.getCodigo()) {
          case "ETJ_02":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEtj.class).get(ProcedimientoEtj_.fechaOposicion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "ETJ_03":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEtj.class).get(ProcedimientoEtj_.fechaNotificacionReqPago), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "ETJ_04":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEtj.class).get(ProcedimientoEtj_.fechaAutoDespachandoEjecucion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "ETJ_06":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoEtj.class).get(ProcedimientoEtj_.fechaPresentacionDemanda), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          default:
            break;
        }
        break;
      case "CON":
        switch (fechaHito.getCodigo()) {
          case "CON_01":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoConcursal.class).get(ProcedimientoConcursal_.fechaDecreto), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "CON_02":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoConcursal.class).get(ProcedimientoConcursal_.fechaResolucion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "CON_03":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoConcursal.class).get(ProcedimientoConcursal_.fechaApertura), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "CON_04":
            //TODO en el modelo de datos es un booleano, cómo hacemos para esta fecha?
            //addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoConcursal.class).get(ProcedimientoConcursal_.fecha), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "CON_05":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoConcursal.class).get(ProcedimientoConcursal_.fechaAutoFaseConvenio), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "CON_06":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoConcursal.class).get(ProcedimientoConcursal_.fechaAutoDeclarandoConcurso), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          case "CON_07":
            addIntervaloFechas(cb.treat(this.getJoinProcedimientos(), ProcedimientoConcursal.class).get(ProcedimientoConcursal_.fechaPublicacion), busquedaProcedimientoDto.getIntervaloFecha());
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }
  }

  private <Y> void equal(Expression<Y> atributo, Integer valor) {
    Predicate predicate = cb.equal(atributo, valor);
    this.predicates.add(predicate);
  }

  private <Y> void equal(Expression<Y> atributo, Double valor) {
    Predicate predicate = cb.equal(atributo, valor);
    this.predicates.add(predicate);
  }

  private <Y> void equal(Expression<Y> atributo, String valor) {
    Predicate predicate = cb.equal(atributo, valor);
    this.predicates.add(predicate);
  }

  private <Y> void equal(Expression<Y> atributo, Boolean valor) {
    Predicate predicate = cb.equal(atributo, valor);
    this.predicates.add(predicate);
  }

  private void like(Expression<String> atributo, String valor) {
    valor = valor.replaceAll("%", "\\%");
    valor = valor.replaceAll("_", "\\_");
    valor = valor.replaceAll("!", "\\!");
    Predicate predicate = cb.like(atributo, "%" + valor + "%");
    this.predicates.add(predicate);
  }

  private void addIntervaloImporte(Expression<Double> atributo, IntervaloImporte intervaloImporte) {
    List<Predicate> predicates = new ArrayList<>();
    if (intervaloImporte.getIgual() != null) {
      predicates.add(cb.equal(atributo, intervaloImporte.getIgual()));
    } else {
      if (intervaloImporte.getMayor() != null) {
        predicates.add(cb.greaterThanOrEqualTo(atributo, intervaloImporte.getMayor()));
      }
      if (intervaloImporte.getMenor() != null) {
        predicates.add(cb.lessThanOrEqualTo(atributo, intervaloImporte.getMenor()));
      }
    }
    if (predicates.size() != 0) {
      this.predicates.add(cb.and(predicates.toArray(new Predicate[0])));
    }
  }

  private void addIntervaloFechas(Expression<Date> atributo, IntervaloFecha intervaloFecha) {
    List<Predicate> predicates = new ArrayList<>();
    if (intervaloFecha.getFechaInicio() != null) {
      predicates.add(cb.greaterThanOrEqualTo(atributo, intervaloFecha.getFechaInicio()));
    }
    if (intervaloFecha.getFechaFin() != null) {
      predicates.add(cb.lessThanOrEqualTo(atributo, intervaloFecha.getFechaFin()));
    }
    if (predicates.size() != 0) {
      this.predicates.add(cb.and(predicates.toArray(new Predicate[0])));
    }
  }

  private static boolean hayValor(String valor) {
    return valor != null && valor.length() != 0;
  }

  public void setOrdenación(String campo, String direccion)
  {
    return;
    /*
    if (campo == null || direccion == null) return;

    this.ordenAscendente = direccion.equalsIgnoreCase("asc");

    if (campo.equals("id")) {
      orderField = this.root.get(Expediente_.id);
    } else if(campo.equals("estado")) {
      orderField = this.getJoinContratos().get(Contrato_.estadoContrato);
    } else if(campo.equals("modoDeGestion")) {
      orderField = this.root.get(Expediente_.tipoEstado);
    } else if(campo.equals("situacion")) {
      orderField = this.getJoinContratoRepresentante().get(Contrato_.situacion);
    } else if (campo.equals("cliente")) {
      orderField = this.getJoinCliente().get(Cliente_.nombre);
    } else if (campo.equals("cartera")) {
      orderField = this.root.get(Expediente_.cartera);
    } else if (campo.equals("titular")) {
      orderField = getJoinPrimerInterviniente().get(ContratoInterviniente_.id);
    } else if (campo.equals("saldoEnGestion")) {
      // Se realiza una vez terminada la query, ya que es un campo calculado
    } else if (campo.equals("estrategia")) {
      orderField = this.getJoinContratos().get(Contrato_.estrategia);
    } else if (campo.equals("supervisor")) {
      orderField = this.getJoinSupervisor().get(Usuario_.id);
    } else if (campo.equals("areaDeGestion")) {
      orderField = this.getJoinGestor().get(Usuario_.areaUsuario);
    } else if (campo.equals("gestor")) {
      orderField = this.getJoinGestor().get(Usuario_.id);
    } else if (campo.equals("suplente")) {
      orderField = this.joinAsignacionExpediente.get(AsignacionExpediente_.suplente);
    }
    */
  }
}
