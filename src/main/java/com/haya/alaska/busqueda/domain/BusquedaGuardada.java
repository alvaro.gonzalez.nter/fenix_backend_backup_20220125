package com.haya.alaska.busqueda.domain;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@AuditTable(value = "HIST_MSTR_BUSQUEDA_GUARDADA")
@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "MSTR_BUSQUEDA_GUARDADA")
public class BusquedaGuardada extends Busqueda {
  @Column(name = "DES_NOMBRE", nullable = false)
  private String nombre;

  public BusquedaGuardada(Usuario usuario, String nombre, BusquedaDto busquedaDto) {
    super(usuario, busquedaDto);
    this.setNombre(nombre);
  }
}
