package com.haya.alaska.resultado_visita.intraestructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.resultado_visita.domain.ResultadoVisita;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultadoVisitaRepository extends CatalogoRepository<ResultadoVisita, Integer> {}
