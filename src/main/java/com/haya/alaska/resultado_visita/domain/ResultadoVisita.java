package com.haya.alaska.resultado_visita.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.visita.domain.Visita;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_RESULTADO_VISITA")
@Entity
@Table(name = "LKUP_RESULTADO_VISITA")
public class ResultadoVisita extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESULTADO_VISITA")
  @NotAudited
  private Set<Visita> visitas = new HashSet<>();
}
