package com.haya.alaska.tipo_prestacion.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_prestacion.domain.TipoPrestacion;

@Repository
public interface TipoPrestacionRepository extends CatalogoRepository<TipoPrestacion, Integer> {
	  
}
