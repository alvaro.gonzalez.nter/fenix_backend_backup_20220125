package com.haya.alaska.tipo_prestacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.interviniente.domain.Interviniente;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_PRESTACION")
@Entity
@Table(name = "LKUP_TIPO_PRESTACION")
public class TipoPrestacion extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_PRESTACION")
  @NotAudited
  private Set<Interviniente> intervinientes = new HashSet<>();
}
