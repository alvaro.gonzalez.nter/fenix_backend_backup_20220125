package com.haya.alaska.visita.domain;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.modalidad.domain.Modalidad;
import com.haya.alaska.resultado_visita.domain.ResultadoVisita;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Audited
@AuditTable(value = "HIST_MSTR_VISITA")
@Table(name = "MSTR_VISITA")
public class Visita {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN_POSESION_NEGOCIADA", nullable = false)
  private BienPosesionNegociada bienPosesionNegociada;

  @Column(name = "FCH_FECHA", nullable = false)
  private Date fecha;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESULTADO_VISITA")
  private ResultadoVisita resultado;

  @Column(name = "NUM_LATITUD")
  private Double latitud; // actualizar en Bien

  @Column(name = "NUM_LONGITUD")
  private Double longitud;

  @ElementCollection()
  @CollectionTable(name = "RELA_VISITA_FOTOS", joinColumns = @JoinColumn(name = "ID_VISITA"))
  @Column(name = "ID_FOTO_GESTOR_DOCUMENTAL")
  @NotAudited
  private Set<Long> fotos = new HashSet<>();

  @Lob
  @Column(name = "DES_NOTAS")
  private String notas;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MODALIDAD")
  private Modalidad modalidad;

  public boolean hayGeolocalizacion() {
    return this.latitud != null && this.longitud != null;
  }

  public boolean hayFotos() {
    return !this.fotos.isEmpty();
  }

  public void addFoto(long idDocumento) {
    this.fotos.add(idDocumento);
  }

  public void setFechaHora(String fecha, String hora) throws ParseException {
    if (fecha == null || hora == null) {
      return;
    }
    SimpleDateFormat format;
    Date temp = null;
    format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    try {
      temp = format.parse(fecha + " " + hora);
    } catch (ParseException ex) {
    }

    format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    try {
      temp = format.parse(fecha + " " + hora);
    } catch (ParseException ex) {
    }

    if (temp == null) throw new ParseException("No se reconoce el formato de la fecha", 0);
    this.setFecha(temp);
  }
}
