package com.haya.alaska.visita.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.LinkedHashSet;

public class VisitaExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection Id");
      cabeceras.add("Collateral Id");
      cabeceras.add("Date");
      cabeceras.add("Time");
      cabeceras.add("Result");
      cabeceras.add("Geolocation");
      cabeceras.add("Photos");
      cabeceras.add("Notes");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Activo");
      cabeceras.add("Fecha");
      cabeceras.add("Hora");
      cabeceras.add("Resultado");
      cabeceras.add("Geolocalización");
      cabeceras.add("Fotos");
      cabeceras.add("Notas");
    }
  }

  public VisitaExcel(Visita visita) {
    super();

    SimpleDateFormat formatohora = new SimpleDateFormat("HH:mm:ss");
    String hora = formatohora.format(visita.getFecha());

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Collateral Id", visita.getBienPosesionNegociada().getId());
      this.add("Connection Id", visita.getBienPosesionNegociada().getExpedientePosesionNegociada().getIdConcatenado());
      this.add("Date", visita.getFecha());
      this.add("Time", hora);
      this.add("Result", visita.getResultado() != null ? visita.getResultado().getValorIngles() : null);
      this.add("Geolocation", visita.hayGeolocalizacion() ? "YES" : "NO");
      this.add("Photos", visita.getFotos());
      this.add("Notes", visita.getNotas());

    } else {

      this.add("Id Activo", visita.getBienPosesionNegociada().getId());
      this.add("Id Expediente", visita.getBienPosesionNegociada().getExpedientePosesionNegociada().getIdConcatenado());
      this.add("Fecha", visita.getFecha());
      this.add("Hora", hora);
      this.add("Resultado", visita.getResultado() != null ? visita.getResultado().getValor() : null);
      this.add("Geolocalización", visita.hayGeolocalizacion() ? "SI" : "NO");
      this.add("Fotos", visita.getFotos());
      this.add("Notas", visita.getNotas());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
