package com.haya.alaska.visita.infraestructure.repository;

import com.haya.alaska.visita.domain.Visita;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitaRepository extends JpaRepository<Visita, Integer> {
}
