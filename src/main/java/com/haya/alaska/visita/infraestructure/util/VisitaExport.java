package com.haya.alaska.visita.infraestructure.util;

import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.*;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class VisitaExport {


  private static final SimpleDateFormat dateFormatSave = new SimpleDateFormat("ddMMyyyy");


  public ResponseEntity<InputStreamResource> wordExport(Map<String, String> mapaVisita) throws IOException {
    File file = new File("src/main/java/com/haya/alaska/visita/infraestructure/documents/PlantillaInformeVisitas.docx");
    FileInputStream fileInputStream = new FileInputStream(file);
    XWPFDocument plantilla = new XWPFDocument(fileInputStream);


    for (XWPFParagraph p : plantilla.getParagraphs()) {
      replace2(p, mapaVisita);
    }
    for (XWPFTable tbl : plantilla.getTables()) {
      for (XWPFTableRow row : tbl.getRows()) {
        for (XWPFTableCell cell : row.getTableCells()) {
          for (XWPFParagraph p : cell.getParagraphs()) {
            replace2(p, mapaVisita);
          }
        }
      }
    }

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      plantilla.write(outputStream);
      ByteArrayInputStream result = new ByteArrayInputStream(outputStream.toByteArray());
      HttpHeaders headers = new HttpHeaders();
      headers.add("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      headers.add("Content-Disposition", "attachment; filename=" + "informe-visitas-" + mapaVisita.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".docx");
      return ResponseEntity
        .ok()
        .headers(headers)
        .body(new InputStreamResource(result));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public ResponseEntity<byte[]> pdfExport(Map<String, String> mapaVisita) throws IOException {

    File file = new File("src/main/java/com/haya/alaska/visita/infraestructure/documents/PlantillaInformeVisitas.docx");
    FileInputStream fileInputStream = new FileInputStream(file);
    XWPFDocument plantilla = new XWPFDocument(fileInputStream);

    for (XWPFParagraph p : plantilla.getParagraphs()) {
      replace2(p, mapaVisita);
    }
    for (XWPFTable tbl : plantilla.getTables()) {
      for (XWPFTableRow row : tbl.getRows()) {
        for (XWPFTableCell cell : row.getTableCells()) {
          for (XWPFParagraph p : cell.getParagraphs()) {
            replace2(p, mapaVisita);
          }
        }
      }
    }

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      plantilla.write(outputStream);

      plantilla = new XWPFDocument(new ByteArrayInputStream(outputStream.toByteArray()));
      PdfOptions options = PdfOptions.create();
      PdfConverter converter = (PdfConverter) PdfConverter.getInstance();
      converter.convert(plantilla, new FileOutputStream("pdf/informe-visitas-" + mapaVisita.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".pdf"), options);


      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_PDF);
      headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
      headers.add("Content-Disposition", "attachment; filename=" + "informe-visitas-" + mapaVisita.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".pdf");
      Path pdfPath = Paths.get("pdf/informe-visitas-" + mapaVisita.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".pdf");
      byte[] pdf = Files.readAllBytes(pdfPath);

      ResponseEntity<byte[]> response = new ResponseEntity<>(pdf, headers, HttpStatus.OK);
      return response;

    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  private void replace2(XWPFParagraph p, Map<String, String> data) {
    String pText = p.getText(); // complete paragraph as string
    if (pText.contains("${")) { // if paragraph does not include our pattern, ignore
      TreeMap<Integer, XWPFRun> posRuns = getPosToRuns(p);
      Pattern pat = Pattern.compile("\\$\\{(.+?)\\}");
      Matcher m = pat.matcher(pText);
      while (m.find()) { // for all patterns in the paragraph
        String g = m.group(1);  // extract key start and end pos
        int s = m.start(1);
        int e = m.end(1);
        String key = g;
        String x = data.get(key);
        if (x == null)
          x = "";
        SortedMap<Integer, XWPFRun> range = posRuns.subMap(s - 2, true, e + 1, true); // get runs which contain the pattern
        boolean found1 = false; // found $
        boolean found2 = false; // found {
        boolean found3 = false; // found }
        XWPFRun prevRun = null; // previous run handled in the loop
        XWPFRun found2Run = null; // run in which { was found
        int found2Pos = -1; // pos of { within above run
        for (XWPFRun r : range.values())
        {
          if (r == prevRun)
            continue; // this run has already been handled
          if (found3)
            break; // done working on current key pattern
          prevRun = r;
          for (int k = 0;; k++) { // iterate over texts of run r
            if (found3)
              break;
            String txt = null;
            try {
              txt = r.getText(k); // note: should return null, but throws exception if the text does not exist
            } catch (Exception ex) {

            }
            if (txt == null)
              break; // no more texts in the run, exit loop
            if (txt.contains("$") && !found1) {  // found $, replace it with value from data map
              txt = txt.replaceFirst("\\$", x);
              found1 = true;
            }
            if (txt.contains("{") && !found2 && found1) {
              found2Run = r; // found { replace it with empty string and remember location
              found2Pos = txt.indexOf('{');
              txt = txt.replaceFirst("\\{", "");
              found2 = true;
            }
            if (found1 && found2 && !found3) { // find } and set all chars between { and } to blank
              if (txt.contains("}"))
              {
                if (r == found2Run)
                { // complete pattern was within a single run
                  txt = txt.substring(0, found2Pos)+txt.substring(txt.indexOf('}'));
                }
                else // pattern spread across multiple runs
                  txt = txt.substring(txt.indexOf('}'));
              }
              else if (r == found2Run) // same run as { but no }, remove all text starting at {
                txt = txt.substring(0,  found2Pos);
              else
                txt = ""; // run between { and }, set text to blank
            }
            if (txt.contains("}") && !found3) {
              txt = txt.replaceFirst("\\}", "");
              found3 = true;
            }
            r.setText(txt, k);
          }
        }
      }
      System.out.println(p.getText());
    }
  }


  private TreeMap<Integer, XWPFRun> getPosToRuns(XWPFParagraph paragraph) {
    int pos = 0;
    TreeMap<Integer, XWPFRun> map = new TreeMap<Integer, XWPFRun>();
    for (XWPFRun run : paragraph.getRuns()) {
      String runText = run.text();
      if (runText != null && runText.length() > 0) {
        for (int i = 0; i < runText.length(); i++) {
          map.put(pos + i, run);
        }
        pos += runText.length();
      }

    }
    return map;
  }
}
