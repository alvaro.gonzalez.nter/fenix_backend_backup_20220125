package com.haya.alaska.visita.infraestructure.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VisitaInputDto {
  private String fecha;
  private String hora;
  private Integer resultado;
  private Double latitud;
  private Double longitud;
  private String notas;
}
