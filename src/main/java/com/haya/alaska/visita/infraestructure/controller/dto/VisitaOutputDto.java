package com.haya.alaska.visita.infraestructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class VisitaOutputDto {
  private Integer id;
  private String fecha;
  private String hora;
  private CatalogoMinInfoDTO resultado;
  private Double latitud;
  private Double longitud;
  private List<Long> fotos;
  private String notas;
}
