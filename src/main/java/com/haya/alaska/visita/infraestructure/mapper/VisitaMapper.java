package com.haya.alaska.visita.infraestructure.mapper;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.resultado_visita.intraestructure.repository.ResultadoVisitaRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.visita.domain.Visita;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaDTOToList;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaInputDto;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaOutputDto;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class VisitaMapper {
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
  private static final SimpleDateFormat dateFormatRaw = new SimpleDateFormat("yyyy-MM-dd");
  private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");



  @Autowired
  private CatalogoMinInfoMapper catalogoMinInfoMapper;

  @Autowired
  private ResultadoVisitaRepository resultadoVisitaRepository;

  public List<VisitaDTOToList> listVisitaToListVisitaDTOToList(List<Visita> visitas) {
    List<VisitaDTOToList> dto = new ArrayList<>();
    for (Visita visita : visitas) {
      dto.add(visitaToVisitaDTOToList(visita));
    }
    return dto;
  }

  public VisitaDTOToList visitaToVisitaDTOToList(Visita visita) {
    VisitaDTOToList dto = new VisitaDTOToList();
    BeanUtils.copyProperties(visita, dto);
    dto.setFecha(dateFormat.format(visita.getFecha()));
    dto.setHora(timeFormat.format(visita.getFecha()));
    dto.setResultado(visita.getResultado() != null ? visita.getResultado().getValor() : null);
    dto.setFotos(visita.hayFotos());
    dto.setGeolocalizacion(visita.getLongitud() != null && visita.getLatitud() != null);
    return dto;
  }

  public VisitaOutputDto entityOutputDto(Visita visita) {
    VisitaOutputDto dto = new VisitaOutputDto();
    BeanUtils.copyProperties(visita, dto);
    if (visita.getResultado() != null) {
      dto.setResultado(this.catalogoMinInfoMapper.entityToDto(visita.getResultado()));
    }
    dto.setFotos(new ArrayList<>(visita.getFotos()));
    dto.setFecha(dateFormatRaw.format(visita.getFecha()));
    dto.setHora(timeFormat.format(visita.getFecha()));
    return dto;
  }

  public void inputDtoToEntity(VisitaInputDto dto, Visita visita) throws NotFoundException, ParseException {
    BeanUtils.copyProperties(dto, visita);
    visita.setFechaHora(dto.getFecha(), dto.getHora());
    if (dto.getResultado() != null) {
      visita.setResultado(this.resultadoVisitaRepository.findById(dto.getResultado())
        .orElseThrow(() -> new NotFoundException("Resultado de visita", dto.getResultado())));
    } else {
      visita.setResultado(null);
    }
  }




}
