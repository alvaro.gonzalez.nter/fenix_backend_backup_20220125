package com.haya.alaska.visita.application;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada_;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.informes.infrastructure.controller.dto.VisitaDTO;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.modalidad.domain.Modalidad;
import com.haya.alaska.modalidad.infrastructure.repository.ModalidadRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.resultado_visita.domain.ResultadoVisita_;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.visita.domain.Visita;
import com.haya.alaska.visita.domain.Visita_;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaDTOToList;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaInputDto;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaOutputDto;
import com.haya.alaska.visita.infraestructure.mapper.VisitaMapper;
import com.haya.alaska.visita.infraestructure.repository.VisitaRepository;
import com.haya.alaska.visita.infraestructure.util.VisitaExport;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class VisitaUseCaseImpl implements VisitaUseCase {
  @PersistenceContext
  private EntityManager entityManager;

  @Autowired
  private VisitaRepository visitaRepository;

  @Autowired
  private BienRepository bienRepository;

  @Autowired
  private ExpedienteRepository expedienteRepository;

  @Autowired
  private PropuestaRepository propuestaRepository;

  @Autowired
  private ModalidadRepository modalidadRepository;

  @Autowired
  private BienPosesionNegociadaRepository bienPosesionNegociadaRepository;

  @Autowired
  private VisitaMapper visitaMapper;

  @Autowired
  private EventoUseCase eventoUseCase;

  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;

  @Autowired
  private VisitaExport visitaExport;
  @Autowired private GestorDocumentalRepository gestorDocumentalRepository;

  public ListWithCountDTO<VisitaDTOToList> getAllFilteredVisitas(
    Integer idBien,
    String fecha,
    String hora,
    String resultado,
    Boolean geolocalizacion,
    Boolean fotos,
    String notas,
    String orderField,
    String orderDirection,
    Integer size,
    Integer page) {
    List<Visita> visitasSinPaginar =
      this.filterVisitas(
        idBien,
        fecha,
        hora,
        resultado,
        geolocalizacion,
        fotos,
        notas,
        orderField,
        orderDirection);
    List<Visita> visitas =
      visitasSinPaginar.stream().skip(size * page).limit(size).collect(Collectors.toList());

    List<VisitaDTOToList> visitasDTOS =
      visitaMapper.listVisitaToListVisitaDTOToList(visitasSinPaginar);
    Comparator<VisitaDTOToList> comparator =
      (var o1, var o2) -> Boolean.compare(o1.isFotos(), o2.isFotos());
    if (orderDirection != null && orderDirection.equals("desc") && orderField.equals("fotos"))
      visitasDTOS = visitasDTOS.stream().sorted(comparator).collect(Collectors.toList());
    else if (orderDirection != null && orderDirection.equals("asc") && orderField.equals("fotos"))
      visitasDTOS = visitasDTOS.stream().sorted(comparator.reversed()).collect(Collectors.toList());

    visitasDTOS = visitasDTOS.stream().skip(size * page).limit(size).collect(Collectors.toList());

    return new ListWithCountDTO<>(visitasDTOS, visitasSinPaginar.size());
  }

  private List<Visita> filterVisitas(
    Integer idBien,
    String fecha,
    String hora,
    String resultado,
    Boolean geolocalizacion,
    Boolean fotos,
    String notas,
    String orderField,
    String orderDirection) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Visita> query = cb.createQuery(Visita.class);
    Root<Visita> root = query.from(Visita.class);
    List<Predicate> predicates = new ArrayList<>();

    if (idBien != null) {
      predicates.add(
        cb.equal(root.get(Visita_.bienPosesionNegociada).get(BienPosesionNegociada_.id), idBien));
    }
    if (fecha != null) {
      predicates.add(
        cb.like(
          cb.function("date", Date.class, root.get(Visita_.fecha)).as(String.class),
          "%" + fecha + "%"));
    }
    if (hora != null) {
      predicates.add(
        cb.like(
          cb.function("time", Date.class, root.get(Visita_.fecha)).as(String.class),
          "%" + hora + "%"));
    }
    if (resultado != null) {
      predicates.add(
        cb.like(root.get(Visita_.resultado).get(ResultadoVisita_.valor), "%" + resultado + "%"));
    }
    if (geolocalizacion != null) {
      Predicate predicate =
        cb.and(cb.isNotNull(root.get(Visita_.latitud)), cb.isNotNull(root.get(Visita_.longitud)));
      if (!geolocalizacion) {
        predicate = cb.not(predicate);
      }
      predicates.add(predicate);
    }
    if (fotos != null) {
      Predicate predicate = cb.isNotEmpty(root.get(Visita_.fotos));
      if (!fotos) {
        predicate = cb.not(predicate);
      }
      predicates.add(predicate);
    }
    if (notas != null) {
      predicates.add(cb.like(root.get(Visita_.notas), "%" + notas + "%"));
    }

    CriteriaQuery<Visita> rs =
      query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
    if (orderField != null) {
      switch (orderField) {
        case "fecha":
          rs.orderBy(
            getOrden(
              cb,
              cb.function("date", Date.class, root.get(Visita_.fecha)).as(String.class),
              orderDirection));
          break;
        case "hora":
          rs.orderBy(
            getOrden(
              cb,
              cb.function("time", Date.class, root.get(Visita_.fecha)).as(String.class),
              orderDirection));
          break;
        case "resultado":
          rs.orderBy(
            getOrden(
              cb, (root.get(Visita_.resultado).get(ResultadoVisita_.valor)), orderDirection));
          break;
        case "geolocalizacion":
          rs.orderBy(getOrden(cb, (root.get(Visita_.latitud)), orderDirection));
          break;
          /*   case "fotos":
          rs.orderBy(
            getOrden(
              cb, root.get(Visita_.fotos).get(Fotos), orderDirection));
          break;*/
        case "notas":
          rs.orderBy(getOrden(cb, root.get(Visita_.notas), orderDirection));
          break;
      }
    }
    return entityManager.createQuery(query).getResultList();
  }

  private Order getOrden(CriteriaBuilder cb, Expression expresion, String orderDirection) {
    if (orderDirection == null) {
      orderDirection = "desc";
    }
    return orderDirection.toLowerCase().equalsIgnoreCase("desc")
      ? cb.desc(expresion)
      : cb.asc(expresion);
  }

  @Override
  public VisitaOutputDto findVisitaByIdDto(Integer idVisita) throws NotFoundException {
    Visita visita =
      this.visitaRepository
        .findById(idVisita)
        .orElseThrow(
          () ->
            new NotFoundException("visita", idVisita));
    return this.visitaMapper.entityOutputDto(visita);
  }

  @Override
  public VisitaOutputDto createVisita(
    Integer idBien, VisitaInputDto visitaInputDto, List<MultipartFile> fotos,Usuario usuario) throws Exception {
    Visita visita = new Visita();
    visitaMapper.inputDtoToEntity(visitaInputDto, visita);
    if (isVisitaFutura(visitaInputDto)) {
      throw new Exception("No se pueden crear visitas con fechas futuras");
    }
    visita.setBienPosesionNegociada(
      this.bienPosesionNegociadaRepository
        .findById(idBien)
        .orElseThrow(
          () ->
            new NotFoundException(
              "Bien de posesión negociada", idBien)));

    this.actualizarGeolocalizacionBien(visita);
    this.procesarFotos(visita, fotos,usuario);
    this.visitaRepository.save(visita);

    eventoUseCase.crearEventoVisita(visita, usuario);

    return this.visitaMapper.entityOutputDto(visita);
  }

  private boolean isVisitaFutura(VisitaInputDto visitaInputDto) {
    DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyy HH:mm");
    LocalDateTime dtRecibido =
      LocalDateTime.parse(visitaInputDto.getFecha() + " " + visitaInputDto.getHora(), fmt);
    LocalDateTime dtactual = LocalDateTime.now();
    Instant ahora = dtactual.toInstant(ZoneOffset.UTC);
    LocalDateTime now = LocalDateTime.now();
    Instant recibida = dtRecibido.toInstant(ZoneId.of("Europe/Madrid").getRules().getOffset(now));
    return recibida.isAfter(ahora);
  }

  @Override
  public VisitaOutputDto updateVisita(
    Integer idBien, Integer idVisita, VisitaInputDto visitaInputDto, List<MultipartFile> fotos,Usuario usuario)
    throws Exception {
    Visita visita =
      this.visitaRepository
        .findById(idVisita)
        .orElseThrow(
          () -> new NotFoundException("Visita", idVisita));

    this.visitaMapper.inputDtoToEntity(visitaInputDto, visita);
    this.visitaRepository.save(visita);
    this.actualizarGeolocalizacionBien(visita);
    this.procesarFotos(visita, fotos,usuario);
    if (isVisitaFutura(visitaInputDto)) {
      throw new Exception("No se pueden crear visitas con fechas futuras");
    }
    this.visitaRepository.save(visita);

    return this.visitaMapper.entityOutputDto(visita);
  }

  private void actualizarGeolocalizacionBien(Visita visita) {
    if (visita.hayGeolocalizacion()) {
      Bien bienActualizado = visita.getBienPosesionNegociada().getBien();
      bienActualizado.setLatitud(visita.getLatitud());
      bienActualizado.setLongitud(visita.getLongitud());
      visita.getBienPosesionNegociada().setBien(bienActualizado);
      this.bienRepository.save(bienActualizado);
    }
  }

  private void procesarFotos(Visita visita, List<MultipartFile> fotos, Usuario usuario) throws IOException {
    if (fotos.isEmpty()) {
      return;
    }
    for (MultipartFile foto : fotos) {
      BienPosesionNegociada bien = visita.getBienPosesionNegociada();
      String valor = visita.getBienPosesionNegociada().getBien().getTipoActivo().getValor();

      if (visita
        .getBienPosesionNegociada()
        .getBien()
        .getTipoActivo()
        .getValor()
        .equals("Mobiliario(MUEBLE, DERECHOS DE CREDITO, OTROS BIENES,VALORES, etc)")) {
        JSONObject metadatos = new JSONObject();
        JSONObject archivoFisico = new JSONObject();
        archivoFisico.put("contenedor", "CONT");
        metadatos.put(
          "General documento",
          new MetadatoDocumentoInputDTO(foto.getOriginalFilename(), "GA-01-FOTO-04")
            .createDescripcionGeneralDocumento());
        metadatos.put("Archivo físico", archivoFisico);
        long idDocumento =
          this.servicioGestorDocumental
            .crearDocumento(
              CodigoEntidad.GARANTIA,
              visita.getBienPosesionNegociada().getBien().getIdHaya(),
              foto,
              metadatos)
            .getIdDocumento();

        Integer idgestorDocumental=(int)idDocumento;
        GestorDocumental gestorDocumental =
          new GestorDocumental(
            idgestorDocumental,
            usuario,
            null,
            new Date(),
            null,
            foto.getOriginalFilename(),
            null,
            null);
        gestorDocumentalRepository.save(gestorDocumental);
        visita.addFoto(idDocumento);
      } else {
        JSONObject metadatos = new JSONObject();
        JSONObject archivoFisico = new JSONObject();
        archivoFisico.put("contenedor", "CONT");
        metadatos.put(
          "General documento",
          new MetadatoDocumentoInputDTO(foto.getOriginalFilename(), "AI-01-FOTO-04")
            .createDescripcionGeneralDocumento());
        metadatos.put("Archivo físico", archivoFisico);
        long idDocumento =
          this.servicioGestorDocumental
            .crearDocumento(
              CodigoEntidad.ACTIVO_INMOBILIARIO,
              visita.getBienPosesionNegociada().getBien().getIdHaya(),
              foto,
              metadatos)
            .getIdDocumento();
        Integer idgestorDocumental=(int)idDocumento;
        GestorDocumental gestorDocumental =
          new GestorDocumental(
            idgestorDocumental,
            usuario,
            null,
            new Date(),
            null,
            foto.getOriginalFilename(),
            null,
            null);
        gestorDocumentalRepository.save(gestorDocumental);
        visita.addFoto(idDocumento);
      }
    }
  }

  @Override
  public ResponseEntity<InputStreamResource> informeSolicitudVisitaPlantillaWord(
    VisitaDTO visitaDTO, boolean posesionNegociada) throws Exception {
    return visitaExport.wordExport(getMapVisita(visitaDTO, posesionNegociada));
  }

  @Override
  public ResponseEntity<byte[]> informeSolicitudVisitaPlantillaPdf(VisitaDTO visitaDTO, boolean posesionNegociada)
    throws Exception {
    return visitaExport.pdfExport(getMapVisita(visitaDTO, posesionNegociada));
  }

  private Map<String, String> getMapVisita(VisitaDTO visitaDTO, boolean posesionNegociada) throws Exception {
    Bien bien;
    Map<String, String> mapaVisita = new HashMap<>();
    Propuesta propuesta = propuestaRepository.findById(visitaDTO.getIdPropuesta()).orElseThrow(() -> new Exception("Propuesta no encontrada con el id:" + visitaDTO.getIdPropuesta()));
    if (posesionNegociada) {
      BienPosesionNegociada bienPosesionNegociada = bienPosesionNegociadaRepository.findById(visitaDTO.getIdBien()).orElseThrow(() -> new Exception("Bien posesion negociada no encontrado con el id:" + visitaDTO.getIdBien()));
      bien = bienPosesionNegociada.getBien();
      mapaVisita.put("idBien", String.valueOf(bienPosesionNegociada.getId()));
      mapaVisita.put("operacion", bien.getIdOrigen());
    } else {
      bien = bienRepository.findById(visitaDTO.getIdBien()).orElseThrow(() -> new Exception("Bien no encontrado con el id:" + visitaDTO.getIdBien()));
      mapaVisita.put("idBien", String.valueOf(bien.getId()));
      mapaVisita.put("operacion", getOperacion(bien));
    }
    Formalizacion formalizacion = getFormalizacion(propuesta);
    mapaVisita.put("cliente", visitaDTO.getCliente() != null ? visitaDTO.getCliente() : null);
    mapaVisita.put("contactoA", visitaDTO.getContacto() != null ? visitaDTO.getContacto() : null);
    mapaVisita.put("alquiler", visitaDTO.getAlquiler() != null ? "Si" : "No");
    if (visitaDTO.getModalidad() != null) {
      Modalidad modalidad = modalidadRepository.findById(Integer.parseInt(visitaDTO.getModalidad())).orElse(null);
      if (modalidad == null){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("The selected mode was not found");
        else throw new NotFoundException("No se ha encontrado la modalidad seleccionada");
      }

      mapaVisita.put("modalidad", modalidad.getValor());
    } else {
      mapaVisita.put("modalidad", null);
    }

    mapaVisita.put("fecha", visitaDTO.getFecha() != null ? visitaDTO.getFecha() : null);
    mapaVisita.put("hora", visitaDTO.getHora() != null ? visitaDTO.getHora() : null);
    mapaVisita.put("tipologia", bien.getTipoBien() != null ? bien.getTipoBien().getValor() : "");
    mapaVisita.put("municipio", bien.getMunicipio() != null ? bien.getMunicipio().getValor() : "");
    mapaVisita.put("provincia", bien.getProvincia() != null ? bien.getProvincia().getValor() : "");
    mapaVisita.put("catastral", bien.getReferenciaCatastral() != null ? bien.getReferenciaCatastral() : "");
    mapaVisita.put("finca", bien.getFinca() != null ? bien.getFinca() : "");
    mapaVisita.put("registro", bien.getLocalidadRegistro() != null ? bien.getLocalidadRegistro() : "");

    if (formalizacion != null) {
      mapaVisita.put("lugar", formalizacion.getDireccionNotaria() != null ? formalizacion.getDireccionNotaria() : null);
      mapaVisita.put("notario", formalizacion.getNotario());
      String contactoB = "";
      if (formalizacion.getOficinaNotaria() != null) contactoB += formalizacion.getOficinaNotaria();
      if (formalizacion.getTelefonoNotaria() != null) contactoB += " " + formalizacion.getTelefonoNotaria();
      if (formalizacion.getEmailOficialNotaria() != null) contactoB += " " + formalizacion.getEmailOficialNotaria();
      mapaVisita.put("contactoB", contactoB);
    }

    String direccion = "";
    if (bien.getNombreVia() != null) direccion += bien.getNombreVia();
    if (bien.getNumero() != null) direccion += ": " + bien.getNumero();
    if (bien.getPortal() != null) direccion += " " + bien.getPortal();
    if (bien.getBloque() != null) direccion += " " + bien.getBloque();
    if (bien.getEscalera() != null) direccion += " " + bien.getEscalera();
    if (bien.getPiso() != null) direccion += " " + bien.getPiso();
    if (bien.getPuerta() != null) direccion += " " + bien.getPuerta();
    mapaVisita.put("direccion", direccion);
    return mapaVisita;
  }

  private Formalizacion getFormalizacion(Propuesta propuesta) {
    return propuesta.getFormalizaciones().stream().findFirst().orElse(null);
  }

  private String getOperacion(Bien bien) {
    Set<ContratoBien> contratos = bien.getContratos();
    String des_id_origen = "";
    for (ContratoBien contrato : contratos) {
      if (contrato.getContrato().getIdOrigen() != null && !des_id_origen.contains(contrato.getContrato().getIdOrigen()))
        des_id_origen += contrato.getContrato().getIdOrigen() + " ";
    }
    return des_id_origen;
  }

  // Listo lo de garantías y tienen que coincidir con lo que tenemos registrados en la rela
  // fotos_visita
  @Override
  public List<Integer> infoFotos(List<Integer> idVisitas) throws IOException, NotFoundException {

    List<Integer> listIdFotos = new ArrayList<>();

    for (Integer idvisita : idVisitas) {
      Visita visita =
        visitaRepository
          .findById(idvisita)
          .orElseThrow(() -> new NotFoundException("Visita", idvisita));
      List<DocumentoDTO> listadoIdGestor = new ArrayList<>();

      if (visita
        .getBienPosesionNegociada()
        .getBien()
        .getTipoActivo()
        .getValor()
        .equals("Mobiliario(MUEBLE, DERECHOS DE CREDITO, OTROS BIENES,VALORES, etc)")) {
        listadoIdGestor =
          this.servicioGestorDocumental
            .listarDocumentos(
              CodigoEntidad.GARANTIA, visita.getBienPosesionNegociada().getBien().getIdHaya())
            .stream()
            .map(DocumentoDTO::new)
            .collect(Collectors.toList());
      } else {
        listadoIdGestor =
          this.servicioGestorDocumental
            .listarDocumentos(
              CodigoEntidad.ACTIVO_INMOBILIARIO,
              visita.getBienPosesionNegociada().getBien().getIdHaya())
            .stream()
            .map(DocumentoDTO::new)
            .collect(Collectors.toList());
      }
      // Conseguimos documentos de las fotos
      List<DocumentoDTO> listFinal =
        listadoIdGestor.stream()
          .filter(documentoDTO -> visita.getFotos().contains(documentoDTO.getId()))
          .collect(Collectors.toList());

      // Pasamos a id y las añadimos al listado
      for (DocumentoDTO documento : listFinal) {
        Integer idfoto = documento.getId().intValue();
        listIdFotos.add(idfoto);
      }
    }
    return listIdFotos;
  }
}
