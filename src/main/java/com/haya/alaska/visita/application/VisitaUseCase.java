package com.haya.alaska.visita.application;

import com.haya.alaska.informes.infrastructure.controller.dto.VisitaDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaDTOToList;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaInputDto;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaOutputDto;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface VisitaUseCase {
  ListWithCountDTO<VisitaDTOToList> getAllFilteredVisitas(Integer idBien, String fecha, String hora, String resultado, Boolean geolocalizacion, Boolean fotos, String notas, String orderField, String orderDirection, Integer size, Integer page);

  VisitaOutputDto findVisitaByIdDto(Integer idVisita) throws NotFoundException;

  VisitaOutputDto createVisita(Integer idBien, VisitaInputDto visitaInputDto, List<MultipartFile> fotos, Usuario usuario) throws Exception;

  VisitaOutputDto updateVisita(Integer idBien, Integer idVisita, VisitaInputDto visitaInputDto, List<MultipartFile> fotos,Usuario usuario) throws Exception;

  ResponseEntity<InputStreamResource> informeSolicitudVisitaPlantillaWord(VisitaDTO visitaDTO, boolean posesionNegociada) throws Exception;

  ResponseEntity<byte[]> informeSolicitudVisitaPlantillaPdf(VisitaDTO visitaDTO, boolean posesionNegociada) throws Exception;

  List<Integer> infoFotos(List<Integer> idVisitas) throws IOException, NotFoundException;
}

