package com.haya.alaska.subtipo_propuesta.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;

/**
 * SubtipoPropuesta entity
 *
 * @author agonzalez
 */
@Audited
@AuditTable(value = "HIST_LKUP_SUBTIPO_PROPUESTA")
@NoArgsConstructor
@Entity
@Data
@Getter
@Table(name = "LKUP_SUBTIPO_PROPUESTA")
public class SubtipoPropuesta extends Catalogo {
    @ManyToOne(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_TIPO_PROPUESTA", nullable = false)
    private TipoPropuesta tipoPropuesta;

    @Column(name = "IND_PBC", nullable = false, columnDefinition = "boolean default false")
    private Boolean pbc = false;

    @Column(name = "IND_FORMALIZACION", nullable = false, columnDefinition = "boolean default true")
    private Boolean formalizacion = true;
}
