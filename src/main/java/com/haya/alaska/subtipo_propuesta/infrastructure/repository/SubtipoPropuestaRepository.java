package com.haya.alaska.subtipo_propuesta.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubtipoPropuestaRepository extends CatalogoRepository<SubtipoPropuesta, Integer> {
    List<SubtipoPropuesta> findByTipoPropuestaIdAndActivoIsTrue(Integer tipoPropuestaId);
    List<SubtipoPropuesta> findByTipoPropuestaIdAndActivoIsTrueAndFormalizacionIsTrue(Integer tipoPropuestaId);
}
