package com.haya.alaska.estimacion.domain;

import com.haya.alaska.importe_estimacion_contrato.domain.ImporteEstimacionContrato;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class EstimacionExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("CONNECTION NUMBER");
      cabeceras.add("DATE");
      cabeceras.add("LOAN");
      cabeceras.add("STRATEGY");
      cabeceras.add("RECOVERED AMOUNT");
      cabeceras.add("OUTPUT AMOUNT");
      cabeceras.add("ESTIMATED DECISION DATE");
      cabeceras.add("RESOLUTION PROBABILITY");
      cabeceras.add("USER");
      cabeceras.add("STATUS");
      cabeceras.add("JUDICIAL");
      cabeceras.add("LEVEL");
    } else {
      cabeceras.add("NUMERO EXPEDIENTE");
      cabeceras.add("FECHA");
      cabeceras.add("CONTRATO");
      cabeceras.add("ESTRATEGIA");
      cabeceras.add("IMPORTE RECUPERADO");
      cabeceras.add("IMPORTE SALIDA");
      cabeceras.add("FECHA ESTIMACION RESOLUCION");
      cabeceras.add("PROBABILIDAD RESOLUCION");
      cabeceras.add("USUARIO");
      cabeceras.add("ESTADO");
      cabeceras.add("JUDICIAL");
      cabeceras.add("NIVEL");
    }
  }

  public EstimacionExcel(
      Estimacion estimacion,
      String idExpediente,
      ImporteEstimacionContrato importeEstimacionContrato,
      boolean hasPermissions,
      boolean posesionNegociada) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      var nivel = " ";
      if (importeEstimacionContrato != null) {
        if (posesionNegociada) {
          nivel = "ASSET";
        } else {
          nivel = "LOAN";
        }
      } else {
        nivel = "CONNECTION";
      }
      this.add("CONNECTION NUMBER", idExpediente);
      this.add("DATE", estimacion.getFecha());
      this.add(
          "LOAN",
          importeEstimacionContrato != null
              ? importeEstimacionContrato.getContrato().getIdCarga()
              : null);
      this.add("STRATEGY", estimacion.getEstrategia());
      this.add(
          "RECOVERED AMOUNT",
          importeEstimacionContrato != null
              ? importeEstimacionContrato.getImporteTotal()
              : estimacion.getImporteRecuperado());
      this.add("OUTPUT AMOUNT", estimacion.getImporteSalida());
      this.add("ESTIMATED DECISION DATE", estimacion.getFechaEstimada());
      this.add("RESOLUTION PROBABILITY", estimacion.getProbabilidadResolucion());
      this.add("USER", estimacion.getUsuario().getNombre());
      if (hasPermissions) {
        this.add(
            "STATUS",
            estimacion.getCumplida() != null ? estimacion.getCumplida().getValorIngles() : null);
      }
      this.add("JUDICIAL", estimacion.getProcedimientoJudicial());
      this.add("LEVEL", nivel);
    } else {
      var nivel = " ";
      if (importeEstimacionContrato != null) {
        if (posesionNegociada) {
          nivel = "Bien";
        } else {
          nivel = "Contrato";
        }
      } else {
        nivel = "Expediente";
      }
      this.add("NUMERO EXPEDIENTE", idExpediente);
      this.add("FECHA", estimacion.getFecha());
      this.add(
          "CONTRATO",
          importeEstimacionContrato != null
              ? importeEstimacionContrato.getContrato().getIdCarga()
              : null);
      this.add("ESTRATEGIA", estimacion.getEstrategia());
      this.add(
          "IMPORTE RECUPERADO",
          importeEstimacionContrato != null
              ? importeEstimacionContrato.getImporteTotal()
              : estimacion.getImporteRecuperado());
      this.add("IMPORTE SALIDA", estimacion.getImporteSalida());
      this.add("FECHA ESTIMACION RESOLUCION", estimacion.getFechaEstimada());
      this.add("PROBABILIDAD RESOLUCION", estimacion.getProbabilidadResolucion());
      this.add("USUARIO", estimacion.getUsuario().getNombre());
      if (hasPermissions) {
        this.add(
            "ESTADO",
            estimacion.getCumplida() != null ? estimacion.getCumplida().getValor() : null);
      }
      this.add("JUDICIAL", estimacion.getProcedimientoJudicial());
      this.add("NIVEL", nivel);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
