package com.haya.alaska.estimacion.domain;

import com.haya.alaska.estimacion_cumplida.domain.EstimacionCumplida;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.importe_estimacion_bien_pos_negociada.domain.ImporteEstimacionBienPosesionNegociada;
import com.haya.alaska.importe_estimacion_contrato.domain.ImporteEstimacionContrato;
import com.haya.alaska.intervalo_estimacion.domain.IntervaloEstimacion;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.usuario.domain.Usuario;
import com.sun.istack.Nullable;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

@Audited
@AuditTable(value = "HIST_MSTR_ESTIMACION")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_ESTIMACION")
public class Estimacion implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "id")
  private Expediente expediente;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXP_POS_NEGOCIADA")
  private ExpedientePosesionNegociada expedientePosesionNegociada;

  @Column(name = "FCH_FECHA", nullable = false)
  @Temporal(TemporalType.DATE)
  private Date fecha; // obligatorio

  @OneToMany(
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH}, fetch = FetchType.EAGER, orphanRemoval = true)
  @JoinColumn(name = "ID_ESTIMACION")
  @NotAudited
  private Set<ImporteEstimacionContrato> importesContratos = new HashSet<>();

  @OneToMany(
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH}, fetch = FetchType.EAGER, orphanRemoval = true)
  @JoinColumn(name = "ID_ESTIMACION_BIEN_POS_NEGOCIADA")
  @NotAudited
  private Set<ImporteEstimacionBienPosesionNegociada> importesBienesPosesionNegociada = new HashSet<>();

  @Column(name = "IND_PROCEDIMIENTO_JUDICIAL", nullable = false)
  private Boolean procedimientoJudicial;

  @Column(name = "NUM_IMPORTE_RECUPERADO", columnDefinition = "decimal(15,2)")
  private Double importeRecuperado;

  @Column(name = "NUM_IMPORTE_SALIDA", columnDefinition = "decimal(15,2)")
  private Double importeSalida;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA_ESTIMADA")
  private Date fechaEstimada;

  @Column(name = "DES_FECHA_ESTIMADA_FILTRO", columnDefinition = "varchar(32)")
  private String fechaEstimadaFiltro;

  @ManyToOne
  @JoinColumn(name = "ID_INTERVALO_ESTIMACION")
  private IntervaloEstimacion intervalo;

  @Column(name = "NUM_PROBABILIDAD_RESOLUCION")
  private Integer probabilidadResolucion;

  @ManyToOne
  @JoinColumn(name = "ID_TIPO_PROPUESTA", referencedColumnName = "ID")
  private TipoPropuesta tipo;

  @ManyToOne
  @JoinColumn(name = "ID_SUBTIPO_PROPUESTA", referencedColumnName = "ID")
  private SubtipoPropuesta subtipo;

  @ManyToOne
  @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID", nullable = false)
  private Usuario usuario;

  @Column(name = "DES_TIPO_SOLUCION")
  private String tipoSolucion;

  @Column(name = "DES_COMENTARIOS")
  private String comentarios;

  @Column(name = "IND_CUMPLIMIENTO", columnDefinition = "boolean")
  private Boolean cumplimiento = false;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @Nullable
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTIMACION_CUMPLIDA")
  private EstimacionCumplida cumplida;

  public String getIntervaloFechasEstimado() {
    if (this.intervalo == null || this.fechaEstimada == null) {
      return null;
    }
    switch (this.intervalo.getCodigo()) {
      case "1":
        return new SimpleDateFormat("dd-MM-yyyy").format(this.fechaEstimada);
      case "30":
        return new SimpleDateFormat("MMMM-yyyy").format(this.fechaEstimada);
      case "90": {
        int mes = Integer.parseInt(new SimpleDateFormat("M").format(this.fechaEstimada));
        int trimestre = (mes - 1) / 3 + 1;
        return trimestre + " TRIM-" + new SimpleDateFormat("yyyy").format(this.fechaEstimada);
      }
      case "180": {
        int mes = Integer.parseInt(new SimpleDateFormat("M").format(this.fechaEstimada));
        int semestre = (mes - 1) / 6 + 1;
        return semestre + " SEM-" + new SimpleDateFormat("yyyy").format(this.fechaEstimada);
      }
      case "365":
        return new SimpleDateFormat("yyyy").format(this.fechaEstimada);
    }
    return "";
  }

  public void setIntervalo(IntervaloEstimacion intervalo) {
    this.intervalo = intervalo;

    if (this.intervalo == null || this.fechaEstimada == null) {
      return;
    }

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(this.fechaEstimada);
    switch (intervalo.getCodigo()) {
      case "1":
        break; // la fecha es correcta
      case "30":
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        break;
      case "90": {
        int mes = Integer.parseInt(new SimpleDateFormat("M").format(this.fechaEstimada));
        int trimestre = (mes - 1) / 3 + 1;
        calendar.set(Calendar.MONTH, trimestre * 3 - 1);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        break;
      }
      case "180": {
        int mes = Integer.parseInt(new SimpleDateFormat("M").format(this.fechaEstimada));
        int semestre = (mes - 1) / 6 + 1;
        calendar.set(Calendar.MONTH, semestre * 6 - 1);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        break;
      }
      case "365":
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        break;
    }
    this.fechaEstimada = calendar.getTime();
  }

  public void addImportesEstimacionContrato(List<ImporteEstimacionContrato> importes) {
    this.importesContratos.addAll(importes);
  }

  public void addImportesEstimacionBienPosesionNegociada(List<ImporteEstimacionBienPosesionNegociada> importes) {
    this.importesBienesPosesionNegociada.addAll(importes);
  }

  public String getProbabilidadResolucionString() {
    if (this.probabilidadResolucion == null) {
      return "";
    }
    switch (this.probabilidadResolucion) {
      case 1:
        return "alta";
      case 2:
        return "media";
      case 3:
        return "baja";
    }
    return null;
  }

  public String getEstrategia() {
    return this.tipo != null
      ? this.tipo.getValor() + (this.subtipo != null ? " " + this.subtipo.getValor() : "")
      : "";
  }

  public void clearContratos() {
    this.importesContratos.clear();
  }

  public void clearBienesPosesionNegociada() {
    this.importesBienesPosesionNegociada.clear();
  }
}
