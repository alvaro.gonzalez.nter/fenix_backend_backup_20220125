package com.haya.alaska.estimacion.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.importe_estimacion_contrato.domain.ImporteEstimacionContrato;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;
import java.util.stream.Collectors;

public class EstimacionInformeExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Loan Id");
      cabeceras.add("Strategy");
      cabeceras.add("Recovered Amount");
      cabeceras.add("Outgoing Amount");
      cabeceras.add("Estimated Recovery Date");
      cabeceras.add("Probability Resolution");
      cabeceras.add("User");
      cabeceras.add("Status");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Estrategia");
      cabeceras.add("Importe Recuperado");
      cabeceras.add("Importe Salida");
      cabeceras.add("Fecha Estimada Recuperacion");
      cabeceras.add("Probabilidad Resolucion");
      cabeceras.add("Usuario");
      cabeceras.add("Estado");
    }
  }

  public EstimacionInformeExcel(Estimacion estimacion, Boolean posesionNegociada) {

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      if (posesionNegociada) {
        this.add("Connection Id", estimacion.getExpedientePosesionNegociada().getIdConcatenado());
        this.add("Status", estimacion != null ? estimacion.getCumplida() : null);
      } else {
        this.add("Connection Id", estimacion.getExpediente().getIdConcatenado());
        this.add(
            "Status",
            estimacion.getExpediente() != null ? estimacion.getExpediente().getTipoEstado() : null);
      }
      Set<ImporteEstimacionContrato> impEstConS = estimacion.getImportesContratos();
      List<Contrato> contratos =
          impEstConS.stream()
              .map(ImporteEstimacionContrato::getContrato)
              .distinct()
              .filter(Objects::nonNull)
              .collect(Collectors.toList());
      this.add(
          "Loan Id",
          contratos.stream()
              .map(c -> c.getIdCarga() + "")
              .collect(Collectors.joining(", "))); // contratos del importe
      this.add("Strategy", estimacion.getEstrategia());
      this.add("Recovered Amount", estimacion.getImporteRecuperado());
      this.add("Outgoing Amount", estimacion.getImporteSalida());
      this.add("Estimated Recovery Date", estimacion.getFechaEstimada());
      this.add("Probability Resolution", estimacion.getProbabilidadResolucionString());
      this.add("User", estimacion.getUsuario().getNombre());
    } else {
      if (posesionNegociada) {
        this.add("Id Expediente", estimacion.getExpedientePosesionNegociada().getIdConcatenado());
        this.add("Estado", estimacion != null ? estimacion.getCumplida() : null);
      } else {
        this.add("Id Expediente", estimacion.getExpediente().getIdConcatenado());
        this.add(
            "Estado",
            estimacion.getExpediente() != null ? estimacion.getExpediente().getTipoEstado() : null);
      }
      Set<ImporteEstimacionContrato> impEstConS = estimacion.getImportesContratos();
      List<Contrato> contratos =
          impEstConS.stream()
              .map(ImporteEstimacionContrato::getContrato)
              .distinct()
              .filter(Objects::nonNull)
              .collect(Collectors.toList());
      this.add(
          "Id Contrato",
          contratos.stream()
              .map(c -> c.getIdCarga() + "")
              .collect(Collectors.joining(", "))); // contratos del importe
      this.add("Estrategia", estimacion.getEstrategia());
      this.add("Importe Recuperado", estimacion.getImporteRecuperado());
      this.add("Importe Salida", estimacion.getImporteSalida());
      this.add("Fecha Estimada Recuperacion", estimacion.getFechaEstimada());
      this.add("Probabilidad Resolucion", estimacion.getProbabilidadResolucionString());
      this.add("Usuario", estimacion.getUsuario().getNombre());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
