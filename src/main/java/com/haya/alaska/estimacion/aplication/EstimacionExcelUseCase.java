package com.haya.alaska.estimacion.aplication;

import com.haya.alaska.estimacion.infrastructure.controller.dto.InformeEstimacionDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;

import java.text.ParseException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface EstimacionExcelUseCase {
  LinkedHashMap<String, List<Collection<?>>> findExcelEstimacionesById(Integer carteraId, Integer mes, InformeEstimacionDTO informeEstimacionDTO,
                                                                       Usuario usuario, Boolean posesionNegociada) throws NotFoundException, ParseException;
}
