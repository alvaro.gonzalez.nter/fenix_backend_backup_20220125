package com.haya.alaska.estimacion.aplication;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import com.haya.alaska.estado_evento.infrastructure.repository.EstadoEventoRepository;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion.domain.EstimacionExcel;
import com.haya.alaska.estimacion.domain.Estimacion_;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionDTO;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionInputDTO;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionTablaDTO;
import com.haya.alaska.estimacion.infrastructure.mapper.EstimacionMapper;
import com.haya.alaska.estimacion.infrastructure.repository.EstimacionRepository;
import com.haya.alaska.estimacion_cumplida.domain.EstimacionCumplida_;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada_;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.importancia_evento.domain.ImportanciaEvento;
import com.haya.alaska.importancia_evento.infrastructure.repository.ImportanciaEventoRepository;
import com.haya.alaska.importe_estimacion_contrato.domain.ImporteEstimacionContrato;
import com.haya.alaska.importe_estimacion_contrato.domain.ImporteEstimacionContrato_;
import com.haya.alaska.nivel_evento.domain.NivelEvento;
import com.haya.alaska.nivel_evento.infrastructure.repository.NivelEventoRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta_;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta_;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class EstimacionUseCaseImpl implements EstimacionUseCase {
  @PersistenceContext private EntityManager entityManager;

  @Autowired private EstimacionRepository estimacionRepository;
  @Autowired private EstimacionMapper estimacionMapper;
  @Autowired private SubtipoEventoRepository subtipoEventoRepository;
  @Autowired private EstadoEventoRepository estadoEventoRepository;
  @Autowired private ImportanciaEventoRepository importanciaEventoRepository;
  @Autowired private NivelEventoRepository nivelEventoRepository;
  @Autowired private EventoRepository eventoRepository;
  @Autowired private ExpedienteRepository expedienteRepository;
  @Autowired private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;

  public EstimacionDTO createEstimacion(
      EstimacionInputDTO estimacionInputDTO,
      Integer idExpediente,
      Usuario usuario,
      boolean posesionNegociada)
      throws NotFoundException, RequiredValueException {
    var estimacionSaved =
        estimacionMapper.createEstimacion(
            estimacionInputDTO,
            idExpediente,
            usuario,
            hasPermission(usuario, idExpediente),
            posesionNegociada);
    return new EstimacionDTO(estimacionSaved);
  }

  public EstimacionDTO updateEstimacion(
      EstimacionInputDTO estimacionInputDTO,
      Integer idEstimacion,
      Usuario loggedUser,
      boolean posesionNegociada)
      throws Exception {
    var estimacion =
        estimacionRepository
            .findById(idEstimacion)
            .orElseThrow(() -> new Exception("Estimacion no encontrada con el id:" + idEstimacion));
    Integer idExpediente;
    if (posesionNegociada) idExpediente = estimacion.getExpedientePosesionNegociada().getId();
    else idExpediente = estimacion.getExpediente().getId();
    var estimacionUpdated =
        estimacionMapper.actualizarCamposEstimacion(
            estimacionInputDTO,
            estimacion,
            hasPermission(loggedUser, idExpediente),
            posesionNegociada);
    return new EstimacionDTO(estimacionUpdated);
  }

  public ListWithCountDTO<EstimacionTablaDTO> getAll(
      Integer idExpediente,
      Boolean posesionNegociada,
      Usuario loggedUser,
      String contrato,
      String estrategia,
      String fechaEstimada,
      String importeRecuperado,
      String importeSalida,
      String probabilidadResolucion,
      String intervaloFechasEstimado,
      String usuario,
      boolean historico,
      String orderField,
      String orderDirection,
      String cumplida,
      int size,
      int page) {
    List<Estimacion> estimaciones =
        getDataFiltered(
            orderField,
            posesionNegociada,
            contrato,
            estrategia,
            fechaEstimada,
            importeRecuperado,
            importeSalida,
            probabilidadResolucion,
            intervaloFechasEstimado,
            usuario,
            historico,
            orderDirection,
            idExpediente,
            cumplida);

    System.out.println(estimaciones);

    if (!hasPermission(loggedUser, idExpediente)) {
      estimaciones.forEach(est -> est.setCumplida(null));
    }
    int count = estimaciones.size();
    List<EstimacionTablaDTO> estimacionesDTO =
        estimaciones.stream().map(EstimacionTablaDTO::new).collect(Collectors.toList());

    Comparator<EstimacionTablaDTO> comparator =
        (var o1, var o2) ->
            StringUtils.compareIgnoreCase(o1.getContratoExpediente(), o2.getContratoExpediente());
    if (orderDirection != null
        && orderDirection.equals("desc")
        && orderField.equals("contratoExpediente"))
      estimacionesDTO = estimacionesDTO.stream().sorted(comparator).collect(Collectors.toList());
    else if (orderDirection != null
        && orderDirection.equals("asc")
        && orderField.equals("contratoExpediente"))
      estimacionesDTO =
          estimacionesDTO.stream().sorted(comparator.reversed()).collect(Collectors.toList());
    estimacionesDTO =
        estimacionesDTO.stream().skip(size * page).limit(size).collect(Collectors.toList());

    return new ListWithCountDTO<>(estimacionesDTO, count);
  }

  public EstimacionDTO getEstimacion(
      Integer idExpediente, Integer idEstimacion, Boolean posesionNegociada, Usuario loggedUser)
      throws NotFoundException {
    EstimacionDTO estimacionDto = new EstimacionDTO();
    var estimacion =
        estimacionRepository
            .findById(idEstimacion)
            .orElseThrow(() -> new NotFoundException("estimacion", idEstimacion));

    // Optional<Estimacion> estimacion = estimacionRepository.findById(idEstimacion);
    /*  if (estimacion.isEmpty())
    throw new NotFoundException("No encontrado estimación con ID: " + idEstimacion);*/
    if (!posesionNegociada) {
      if (estimacion.getExpediente() == null
          || !estimacion.getExpediente().getId().equals(idExpediente)) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException(
              "The estimate " + idEstimacion + " does not belong to the file; " + idExpediente);
        else
          throw new NotFoundException(
              "La estimación " + idEstimacion + " no pertenece al expediente; " + idExpediente);
      }

    } else {
      if (estimacion.getExpedientePosesionNegociada() == null
          || !estimacion.getExpedientePosesionNegociada().getId().equals(idExpediente)) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException(
              "The estimate " + idEstimacion + " does not belong to the file; " + idExpediente);
        else
          throw new NotFoundException(
              "La estimación "
                  + idEstimacion
                  + " no pertenece al expediente Posesión negociada; "
                  + idExpediente);
      }
    }
    if (!hasPermission(loggedUser, idExpediente)) {
      // estimacion.get().setCumplida(null);
      estimacion.setCumplida(null);
    }
    EstimacionDTO nuevaestimacionDto = new EstimacionDTO(estimacion);

    return nuevaestimacionDto;
    // return new EstimacionDTO(estimacion);
  }

  private boolean hasPermission(Usuario loggedUser, int idExpediente) {
    // Comprobar que el usuario tiene los permisos adecuados y si no los tiene no mostrarle datos
    // que no puede ver
    Expediente exp = null;
    ExpedientePosesionNegociada epn = null;
    try {
      exp = expedienteRepository.findById(idExpediente).orElseThrow();
      boolean isAdmin = loggedUser.esAdministrador();
      boolean estaAsignadoAlExpediente =
          exp.getAsignaciones().stream()
              .filter(AsignacionExpediente::getActivo)
              .map(AsignacionExpediente::getUsuario)
              .anyMatch(us -> us.getId() == loggedUser.getId());
      boolean isResponsable =
          loggedUser.getPerfil() != null
              && "Responsable de cartera".equals(loggedUser.getPerfil().getNombre());
      boolean isSupervisor =
          loggedUser.getPerfil() != null && "Supervisor".equals(loggedUser.getPerfil().getNombre());
      return isAdmin || (estaAsignadoAlExpediente && (isResponsable || isSupervisor));
    } catch (Exception e) {
      epn = expedientePosesionNegociadaRepository.findById(idExpediente).orElseThrow();
      boolean isAdmin = loggedUser.esAdministrador();
      boolean estaAsignadoAlExpediente =
          epn.getAsignaciones().stream()
              .filter(AsignacionExpedientePosesionNegociada::getActivo)
              .map(AsignacionExpedientePosesionNegociada::getUsuario)
              .anyMatch(us -> us.getId() == loggedUser.getId());
      boolean isResponsable =
          loggedUser.getPerfil() != null
              && "Responsable de cartera".equals(loggedUser.getPerfil().getNombre());
      boolean isSupervisor =
          loggedUser.getPerfil() != null && "Supervisor".equals(loggedUser.getPerfil().getNombre());
      return isAdmin || (estaAsignadoAlExpediente && (isResponsable || isSupervisor));
    }
  }

  public LinkedHashMap<String, List<Collection<?>>> findExcelEstimacionById(
      Integer idExpediente, Usuario loggedUser, boolean posesionNegociada) throws Exception {
    String idConcatenado;
    if (posesionNegociada) {
      ExpedientePosesionNegociada exPN =
          expedientePosesionNegociadaRepository
              .findById(idExpediente)
              .orElseThrow(() -> new NotFoundException("expedientePN", idExpediente));
      idConcatenado = exPN.getIdConcatenado();
    } else {
      Expediente expediente =
          expedienteRepository
              .findById(idExpediente)
              .orElseThrow(() -> new NotFoundException("expediente", idExpediente));
      idConcatenado = expediente.getIdConcatenado();
    }
    List<Estimacion> estimacionesList = estimacionRepository.findAllByExpedienteId(idExpediente);
    if (posesionNegociada) {
      estimacionesList = estimacionRepository.findAllByExpedientePosesionNegociadaId(idExpediente);
    }
    if (estimacionesList.isEmpty())
      throw new NotFoundException("estimaciones asociadas al expediente", idExpediente);

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> estimaciones = new ArrayList<>();
    estimaciones.add(EstimacionExcel.cabeceras);

    for (Estimacion estimacion : estimacionesList) {
      if (estimacion.getImportesContratos().isEmpty()) {
        estimaciones.add(
            new EstimacionExcel(
                    estimacion,
                    idConcatenado,
                    null,
                    hasPermission(loggedUser, idExpediente),
                    posesionNegociada)
                .getValuesList());
      } else {
        for (ImporteEstimacionContrato iec : estimacion.getImportesContratos()) {
          estimaciones.add(
              new EstimacionExcel(
                      estimacion,
                      idConcatenado,
                      iec,
                      hasPermission(loggedUser, idExpediente),
                      posesionNegociada)
                  .getValuesList());
        }
      }
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      datos.put("Estimates", estimaciones);

    } else {
      datos.put("Estimaciones", estimaciones);
    }

    return datos;
  }

  @Transactional
  public void ponerEstimacionInactivo(Integer isEstimacion) throws NotFoundException {
    Estimacion estimacion =
        estimacionRepository
            .findByIdAndActivoIsTrue(isEstimacion)
            .orElseThrow(() -> new NotFoundException("estimación", isEstimacion));
    estimacion.setActivo(false);
    estimacionRepository.save(estimacion);
  }

  @Scheduled(cron = "0 0 8 * * *")
  @Transactional
  public void generarAlertasCaducidadCercana() {
    log.info("Iniciando tarea programada: EstimacionUseCaseImpl::generarAlertasCaducidadCercana");

    SubtipoEvento subtipoEvento = this.subtipoEventoRepository.findByCodigo("VAL01").get();
    EstadoEvento estadoEvento = this.estadoEventoRepository.findByCodigo("PEN").get();
    ImportanciaEvento importanciaEvento =
        this.importanciaEventoRepository.findByCodigo("MED").get();
    NivelEvento nivelEvento = this.nivelEventoRepository.findByCodigo("EXP").get();

    Calendar calDentroDeUnaSemana = Calendar.getInstance();
    calDentroDeUnaSemana.add(Calendar.DATE, 7);
    Date diaDentroDeUnaSemana = calDentroDeUnaSemana.getTime();

    List<Estimacion> estimaciones =
        this.estimacionRepository.findAllByFechaEstimadaEquals(diaDentroDeUnaSemana);
    log.info(
        "Se han encontrado "
            + estimaciones.size()
            + " estimaciones con una fecha estimada "
            + diaDentroDeUnaSemana);
    for (Estimacion estimacion : estimaciones) {
      String descripcion =
          "Queda menos de una semana para la fecha límite de la estimación " + estimacion.getId();
      if (estimacion.getImportesContratos().size() == 0) {
        descripcion +=
            " ("
                + estimacion.getImportesContratos().stream()
                    .map(ic -> "C" + ic.getContrato().getIdCarga())
                    .collect(Collectors.joining(", "))
                + ")";
      } else {
        descripcion += " (E" + estimacion.getExpediente().getId() + ")";
      }
      Evento evento = new Evento();
      evento.setCartera(estimacion.getExpediente().getCartera());
      evento.setTitulo("Fecha límite estimación");
      evento.setComentariosDestinatario(descripcion);
      evento.setFechaLimite(estimacion.getFechaEstimada());
      evento.setFechaCreacion(new Date());
      evento.setFechaAlerta(new Date());
      evento.setClaseEvento(subtipoEvento.getTipoEvento().getClaseEvento());
      evento.setTipoEvento(subtipoEvento.getTipoEvento());
      evento.setSubtipoEvento(subtipoEvento);
      evento.setPeriodicidad(null);
      evento.setEstado(estadoEvento);
      evento.setImportancia(importanciaEvento);
      evento.setResultado(null);
      evento.setEmisor(null);
      evento.setDestinatario(estimacion.getExpediente().getGestor());
      evento.setProgramacion(true);
      evento.setCorreo(false);
      evento.setRevisado(false);
      evento.setAutogenerado(true);
      evento.setNivel(nivelEvento);
      evento.setIdNivel(1);
      this.eventoRepository.saveAndFlush(evento);
    }
  }

  public List<Estimacion> getDataFiltered(
      String orderField,
      Boolean posesionNegociada,
      String contrato,
      String estrategia,
      String fechaEstimada,
      String importeRecuperado,
      String importeSalida,
      String probabilidadResolucion,
      String intervaloFechasEstimado,
      String usuario,
      boolean historico,
      String orderDirection,
      Integer idExpediente,
      String cumplida) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    var query = cb.createQuery(Estimacion.class);

    Root<Estimacion> root = query.from(Estimacion.class);
    List<Predicate> predicates = new ArrayList<>();

    if (!historico) {
      predicates.add(cb.equal(root.get(Estimacion_.activo), true));
    }

    /*   Join<Estimacion, Expediente>joinExp=root.join(Estimacion_.EXPEDIENTE,JoinType.INNER);
    Predicate onExp=cb.equal(cb.upper(joinExp.get(Expediente_.ID)),idExpediente);
    joinExp.on(onExp);*/
    List<Estimacion> listanueva = new ArrayList<>();

    if (!posesionNegociada) {
      Join<Estimacion, Expediente> joinExp = root.join(Estimacion_.EXPEDIENTE, JoinType.INNER);
      Predicate onExp = cb.equal(cb.upper(joinExp.get(Expediente_.ID)), idExpediente);
      joinExp.on(onExp);
    } else {
      Join<Estimacion, ExpedientePosesionNegociada> joinExp =
          root.join(Estimacion_.expedientePosesionNegociada, JoinType.INNER);
      Predicate onExp =
          cb.equal(cb.upper(joinExp.get(ExpedientePosesionNegociada_.ID)), idExpediente);
      joinExp.on(onExp);
    }

    // ESTE PREDICATE ES INCORRECTO
    /*   if (!historico) {
      predicates.add(cb.or(
          cb.isNull(root.get(Estimacion_.fechaEstimada)),
          cb.greaterThanOrEqualTo(root.get(Estimacion_.fechaEstimada), new Date())
      ));
    }*/

    if (contrato != null && contrato.length() != 0) {
      if (contrato.toUpperCase().charAt(0)
          == 'E') { // devolver solo los que estén asociados al expediente
        contrato = contrato.substring(1);
        if (!idExpediente
            .toString()
            .contains(
                contrato)) { // filtrando por el expediente equivocado: no devolver ningún resultado
          return new ArrayList<>();
        }
        Join<Estimacion, ImporteEstimacionContrato> join =
            root.join(Estimacion_.importesContratos, JoinType.LEFT);
        predicates.add(cb.isNull(join.get(ImporteEstimacionContrato_.id)));
      } else {
        Join<Estimacion, ImporteEstimacionContrato> join =
            root.join(Estimacion_.importesContratos, JoinType.INNER);
        Join<ImporteEstimacionContrato, Contrato> joinContrato =
            join.join(ImporteEstimacionContrato_.CONTRATO, JoinType.INNER);
        if (contrato.toUpperCase().charAt(0) == 'C') {
          contrato = contrato.substring(1);
        }
        Predicate onCond =
            cb.like(joinContrato.get(Contrato_.id).as(String.class), "%" + contrato + "%");
        joinContrato.on(onCond);
      }
    }

    Join<Estimacion, TipoPropuesta> tipoPropuestaJoin = null;
    if (estrategia != null || "estrategia".equals(orderField)) {
      tipoPropuestaJoin =
          root.join(
              Estimacion_.tipo,
              estrategia == null ? JoinType.LEFT : JoinType.INNER); // para el order-by

      if (estrategia != null) {
        Join<Estimacion, SubtipoPropuesta> subtipoPropuestaJoin =
            root.join(Estimacion_.subtipo, JoinType.INNER);
        Predicate onCond =
            cb.or(
                cb.like(
                    (Expression<String>)
                        cb.upper(tipoPropuestaJoin.get(TipoPropuesta_.valor))
                            .alias("tipoPropuesta"),
                    "%" + estrategia.toUpperCase() + "%"),
                cb.like(
                    (Expression<String>)
                        cb.upper(subtipoPropuestaJoin.get(SubtipoPropuesta_.valor))
                            .alias("subtipoPropuesta"),
                    "%" + estrategia.toUpperCase() + "%"));
        predicates.add(onCond);
      }
    }

    if (importeRecuperado != null) {
      predicates.add(
          cb.like(
              root.get(Estimacion_.importeRecuperado).as(String.class),
              "%" + importeRecuperado + "%"));
    }

    if (importeSalida != null) {
      predicates.add(
          cb.like(root.get(Estimacion_.importeSalida).as(String.class), "%" + importeSalida + "%"));
    }

    if (intervaloFechasEstimado != null) {
      predicates.add(
          cb.like(
              root.get(Estimacion_.fechaEstimadaFiltro).as(String.class),
              "%" + intervaloFechasEstimado + "%"));
    }

    if (fechaEstimada != null) {
      predicates.add(
          cb.like(
              root.get(Estimacion_.FECHA_ESTIMADA).as(String.class), "%" + fechaEstimada + "%"));
    }

    if (cumplida != null) {
      predicates.add(
          cb.like(
              root.get(Estimacion_.CUMPLIDA).get(EstimacionCumplida_.VALOR), "%" + cumplida + "%"));
    }
    if (probabilidadResolucion != null) {
      int probabilidadResolucionNumero = 0;
      probabilidadResolucion = probabilidadResolucion.toLowerCase();
      if (probabilidadResolucion.equals("1") || "alta".startsWith(probabilidadResolucion)) {
        probabilidadResolucionNumero = 1;
      } else if (probabilidadResolucion.equals("2") || "media".startsWith(probabilidadResolucion)) {
        probabilidadResolucionNumero = 2;
      } else if (probabilidadResolucion.equals("3") || "baja".startsWith(probabilidadResolucion)) {
        probabilidadResolucionNumero = 3;
      }
      if (probabilidadResolucionNumero != 0) {
        predicates.add(
            cb.equal(root.get(Estimacion_.probabilidadResolucion), probabilidadResolucionNumero));
      }
    }

    if (usuario != null) {
      Join<Estimacion, Usuario> usuarioJoin = root.join(Estimacion_.usuario, JoinType.INNER);
      Predicate onCond =
          cb.like(cb.upper(usuarioJoin.get(Usuario_.nombre)), "%" + usuario.toUpperCase() + "%");
      usuarioJoin.on(onCond);
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    // listanueva=entityManager.createQuery(query).getResultList();
    // System.out.println(listanueva);

    if (orderField != null) {
      Expression<?> orderBy = null;
      switch (orderField) {
        case "intervaloFechasEstimado":
          orderBy = root.get(Estimacion_.intervalo);
          break;
        case "estrategia":
          if (tipoPropuestaJoin != null) {
            orderBy = tipoPropuestaJoin.get(TipoPropuesta_.valor);
          }
          break;
          /*     case "fechaEstimada":
          if (tipoPropuestaJoin != null) {
            orderBy = root.get(Estimacion_.fechaEstimada).as(String.class);
          }
          break;*/

        case "estado":
          // TODO
          break;
        case "contratoExpediente":
          //   orderBy=root.get(Estimacion_.importesContratos);
          break;

        case "cumplida":
          orderBy = root.get(Estimacion_.cumplida);
          break;
        default:
          orderBy = root.get(orderField);
          break;
      }
      if (orderBy != null) {
        if (orderDirection.equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(orderBy));
        } else {
          rs.orderBy(cb.asc(orderBy));
        }
      }
    }
    // listanueva=entityManager.createQuery(query).getResultList();
    // System.out.println(listanueva);
    // List<Estimacion>listaEstimacion=entityManager.createQuery(query).getResultList();

    return entityManager.createQuery(query).getResultList();
  }

  private Order getOrden(CriteriaBuilder cb, Expression expresion, String orderDirection) {
    if (orderDirection == null) {
      orderDirection = "desc";
    }
    return orderDirection.toLowerCase().equalsIgnoreCase("desc")
        ? cb.desc(expresion)
        : cb.asc(expresion);
  }
}
