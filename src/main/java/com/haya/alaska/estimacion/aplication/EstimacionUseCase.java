package com.haya.alaska.estimacion.aplication;

import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionDTO;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionInputDTO;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionTablaDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface EstimacionUseCase {
  EstimacionDTO createEstimacion(EstimacionInputDTO estimacionInputDTO, Integer idExpediente, Usuario usuario, boolean posesionNegociada) throws NotFoundException, RequiredValueException;

  EstimacionDTO updateEstimacion(EstimacionInputDTO estimacionInputDTO, Integer idEstimacion, Usuario usuario, boolean posesionNegociada) throws Exception;

  ListWithCountDTO<EstimacionTablaDTO> getAll(
    Integer idExpediente, Boolean posesionNegociada, Usuario loggedUser, String contrato, String estrategia, String fechaEstimada, String importeRecuperado, String importeSalida,
    String probabilidadResolucion, String intervaloFechasEstimado, String usuario, boolean historico,
    String orderField, String orderDirection, String cumplida, int size, int page);

  EstimacionDTO getEstimacion(Integer idExpediente, Integer idEstimacion, Boolean posesionNegociada, Usuario loggedUser) throws NotFoundException;

  LinkedHashMap<String, List<Collection<?>>> findExcelEstimacionById(Integer idExpediente, Usuario loggedUser, boolean posesionNegociada) throws Exception;

  void ponerEstimacionInactivo(Integer isEstimacion) throws NotFoundException;
}
