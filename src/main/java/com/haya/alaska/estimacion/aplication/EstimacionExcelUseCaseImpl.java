package com.haya.alaska.estimacion.aplication;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion.domain.EstimacionInformeExcel;
import com.haya.alaska.estimacion.infrastructure.controller.dto.InformeEstimacionDTO;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.application.ExpedienteUseCase;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.ExpedienteCarteraExcel;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class EstimacionExcelUseCaseImpl implements EstimacionExcelUseCase {

  @Autowired
  private CarteraRepository carteraRepository;

  @Autowired
  private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;

  @Autowired
  private ExpedienteRepository expedienteRepository;

  @Autowired
  private EventoUtil eventoUtil;

  @Autowired
  ExpedienteUtil expedienteUtil;
  @Autowired
  ExpedienteUseCase expedienteUseCase;

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelEstimacionesById(Integer carteraId, Integer mes, InformeEstimacionDTO informeEstimacionDTO,
                                                                              Usuario usuario, Boolean posesionNegociada) throws NotFoundException, ParseException {

    Perfil perfilUsu = usuario.getPerfil();

    if(perfilUsu.getNombre().equals("Responsable de cartera") || perfilUsu.getNombre().equals("Supervisor") || usuario.esAdministrador() ) {
      var datos = this.createEstimacionDataForExcel();
      if (carteraId != null) {
        Cartera cartera = carteraRepository.findById(carteraId)
          .orElseThrow(() -> new NotFoundException("Cartera", carteraId));
        this.addEstimacionDataForExcel(cartera, datos, mes, informeEstimacionDTO, posesionNegociada, usuario);
      } else {
        for(Cartera cartera : carteraRepository.findAll()) {
          this.addEstimacionDataForExcel(cartera, datos, mes, informeEstimacionDTO, posesionNegociada, usuario);
        }
      }
      return datos;
    }
    else {
      throw new IllegalArgumentException("El tipo del usuario debe ser Responsable de cartera o Supervisor para poder generar el Informe");
    }
  }


  private LinkedHashMap<String, List<Collection<?>>> createEstimacionDataForExcel() {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expedientes = new ArrayList<>();
    expedientes.add(ExpedienteCarteraExcel.cabeceras);

    List<Collection<?>> estimaciones = new ArrayList<>();
    estimaciones.add(EstimacionInformeExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("Connection ", expedientes);
      datos.put("Estimates", estimaciones);

    } else  {
      datos.put("Expediente", expedientes);
      datos.put("Estimaciones", estimaciones);
    }

    return datos;
  }



  private void addEstimacionDataForExcel(Cartera cartera, LinkedHashMap<String, List<Collection<?>>> datos, Integer mes,
                                         InformeEstimacionDTO informeEstimacionDTO, Boolean posesionNegociada, Usuario usuario) throws ParseException, NotFoundException {

    List<Collection<?>> expedientes;
    List<Collection<?>> estimaciones;

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      expedientes = datos.get("Connection");
      estimaciones = datos.get("Estimates");

    } else{
      expedientes = datos.get("Expediente");
      estimaciones = datos.get("Estimaciones");
    }

    Integer mesActual = Calendar.getInstance().get(Calendar.MONTH);
    Integer mesParametro = mes-1;

    Calendar hoy = Calendar.getInstance();
    Calendar calVariableInicio = Calendar.getInstance();
    Calendar calVariableFin = Calendar.getInstance();
    if (mesActual >= mesParametro){
      calVariableInicio.set(hoy.get(Calendar.YEAR),mesParametro,1,0,0,0);
      calVariableFin.set(hoy.get(Calendar.YEAR),mesParametro,1,0,0,0);
      calVariableFin.add(Calendar.MONTH, 1);
    }else{
      calVariableInicio.set(hoy.get(Calendar.YEAR)-1,mesParametro,1,0,0,0);
      calVariableFin.set(hoy.get(Calendar.YEAR)-1,mesParametro,1,0,0,0);
      calVariableFin.add(Calendar.MONTH, 1);
    }

      Date variableInicio=calVariableInicio.getTime();
      Date variableFinal=calVariableFin.getTime();
      Calendar calEx=Calendar.getInstance();
      calEx.set(Calendar.YEAR,3900);

    if(posesionNegociada){
      List<ExpedientePosesionNegociada> expedientesFiltrar = new ArrayList<>();
      if (usuario.esAdministrador()){
        expedientesFiltrar = expedientePosesionNegociadaRepository.findAllByCarteraId(cartera.getId());
      } else {
        expedientesFiltrar = expedientePosesionNegociadaRepository.findAllByCarteraIdAndAsignacionesUsuarioId(cartera.getId(), usuario.getId());
      }

      for (ExpedientePosesionNegociada expedientePN : expedientesFiltrar) {

        if(expedientePN.getEstimaciones().size() == 0) {
          continue;
        }
        Evento accion = getLastEvento(expedientePN.getId(), 1);
        Evento alerta = getLastEvento(expedientePN.getId(), 2);
        Evento actividad = getLastEvento(expedientePN.getId(), 3);
        var estadoExpediente = expedienteUtil.getEstado(expedientePN);
        Boolean comprobacion = expedienteUseCase.filtroMesInformesPosesionNegociada(expedientePN, mes);

        if (comprobacion && usuario.contieneExpediente(expedientePN.getId(), true)) {
          if (informeEstimacionDTO.getEstadoExpediente() == null || estadoExpediente == null || estadoExpediente.getId().equals(informeEstimacionDTO.getEstadoExpediente())) {
            Procedimiento p = expedienteUtil.getUltimoProcedimiento(expedientePN);
            expedientes.add(new ExpedienteCarteraExcel(expedientePN, accion, alerta, actividad, p).getValuesList());
          }
          List<Estimacion> estimacionesFilter=expedientePN.getEstimaciones().stream().filter(
            estimacion->{
              Date fCE = estimacion.getFechaEstimada();
              Date fCI = informeEstimacionDTO.getFechaResolucionInicial();
              Date fCF = informeEstimacionDTO.getFechaResolucionFinal();
              if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;
              return true;
            }).collect(Collectors.toList());

          for (Estimacion estimacion : estimacionesFilter) {
            String estrategia = estimacion.getEstrategia();
            Date fechaEstimadaAUX =
              estimacion.getFechaEstimada() != null ? estimacion.getFechaEstimada() : null;

            if (informeEstimacionDTO.getTipoEstrategia() == null
              || (estimacion.getTipo() != null && estimacion.getTipo().getId().equals(informeEstimacionDTO.getTipoEstrategia()))) {
              estimaciones.add(new EstimacionInformeExcel(estimacion, posesionNegociada).getValuesList());
            }
          }
        }
      }
    }
    else{
      List<Expediente> expedientesFiltrar = new ArrayList<>();
      if (usuario.esAdministrador()){
        expedientesFiltrar = expedienteRepository.findAllByCarteraId(cartera.getId());
      } else {
        expedientesFiltrar = expedienteRepository.findAllByCarteraIdAndAsignacionesUsuarioId(cartera.getId(), usuario.getId());
      }

      for (Expediente expediente : expedientesFiltrar) {
        if(expediente.getEstimaciones().size() == 0) {
          continue;
        }
        Evento accion = getLastEvento(expediente.getId(), 1);
        Evento alerta = getLastEvento(expediente.getId(), 2);
        Evento actividad = getLastEvento(expediente.getId(), 3);
        var estadoExpediente= expedienteUtil.getEstado(expediente);
        Boolean comprobacion=expedienteUseCase.filtroMesInformes(expediente,mes);

        if (comprobacion && usuario.contieneExpediente(expediente.getId(), false)){
          if (informeEstimacionDTO.getEstadoExpediente() == null || estadoExpediente.getId().equals(informeEstimacionDTO.getEstadoExpediente())) {
            Procedimiento p = expedienteUtil.getUltimoProcedimiento(expediente);
            expedientes.add(new ExpedienteCarteraExcel(expediente, accion, alerta, actividad, p).getValuesList());
          }
          List<Estimacion> estimacionesFilter=expediente.getEstimaciones().stream().filter(
            estimacion->{
              Date fCE = estimacion.getFechaEstimada();
              Date fCI = informeEstimacionDTO.getFechaResolucionInicial();
              Date fCF = informeEstimacionDTO.getFechaResolucionFinal();
              if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;
              return true;
            }).collect(Collectors.toList());

          for (Estimacion estimacion : estimacionesFilter) {
            if (informeEstimacionDTO.getTipoEstrategia() == null
              || (estimacion.getTipo() != null && estimacion.getTipo().getId().equals(informeEstimacionDTO.getTipoEstrategia()))) {
              estimaciones.add(new EstimacionInformeExcel(estimacion, posesionNegociada).getValuesList());
            }
          }
        }
      }
    }
  }


  public Evento getLastEvento(Integer idExpediente, Integer clase) {
    try {
      List<Integer> clases = new ArrayList<>();
      clases.add(clase);
      List<Evento> eventos = eventoUtil.getFilteredEventos(null, null, null, 1,
        idExpediente, null, null, null, null,
        null, null, clases, null, null, null, null, null, null, null, null,
        null,null, "fechaCreacion", "desc", 0, 100);

      Evento result = null;
      if (!eventos.isEmpty()) result = eventos.get(0);
      return result;
    }catch (Exception e) {
      return null;
    }
  }

}
