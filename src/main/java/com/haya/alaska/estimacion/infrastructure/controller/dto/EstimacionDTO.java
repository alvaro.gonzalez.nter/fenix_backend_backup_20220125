package com.haya.alaska.estimacion.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoDTO;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion_cumplida.domain.EstimacionCumplida;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class EstimacionDTO implements Serializable {
  private Integer id;
  private Date fecha;
  private Integer expediente;
  private String idConcatenado;
  // private Integer expedientePosesionNegociada;
  private List<ImporteEstimacionContratoDTO> importesContratos;
  private List<ImporteEstimacionBienPosesionNegociadaDTO> importesBienes;
  private boolean procedimientoJudicial;
  private Double importeRecuperado;
  private Double importeSalida;
  private CatalogoDTO intervalo;
  private Date fechaEstimada;
  private Integer probabilidadResolucion;
  private CatalogoDTO tipo;
  private CatalogoDTO subtipo;
  private String tipoSolucion;
  private String comentarios;
  private Boolean cumplimiento;
  private EstimacionCumplida cumplida;
  private Boolean activo;

  public EstimacionDTO(Estimacion estimacion) {
    BeanUtils.copyProperties(estimacion, this, "importesContratos", "activo");
    this.activo = estimacion.getActivo() && (estimacion.getFechaEstimada() == null || estimacion.getFechaEstimada().after(new Date()));
    this.importesContratos =
      estimacion.getImportesContratos().stream()
        .map(ImporteEstimacionContratoDTO::new)
        .collect(Collectors.toList());
    this.importesBienes =
      estimacion.getImportesBienesPosesionNegociada().stream()
        .map(ImporteEstimacionBienPosesionNegociadaDTO::new)
        .collect(Collectors.toList());

    this.setCumplida(estimacion.getCumplida());
    if (estimacion.getIntervalo() != null) {
      this.intervalo = new CatalogoDTO(estimacion.getIntervalo());
    }
    if (estimacion.getTipo() != null) {
      this.tipo = new CatalogoDTO(estimacion.getTipo());
    }
    if (estimacion.getSubtipo() != null) {
      this.subtipo = new CatalogoDTO(estimacion.getSubtipo());
    }
  }

  public static List<EstimacionDTO> newList(List<Estimacion> estimaciones) {
    return estimaciones.stream().map(EstimacionDTO::new).collect(Collectors.toList());
  }
}
