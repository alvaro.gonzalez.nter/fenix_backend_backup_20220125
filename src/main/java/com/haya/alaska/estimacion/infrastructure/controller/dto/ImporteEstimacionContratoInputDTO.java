package com.haya.alaska.estimacion.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/** @author agonzalez */

@Getter
@ToString
@NoArgsConstructor
public class ImporteEstimacionContratoInputDTO implements Serializable {
    private Integer id;
    private Double importeTotal;
    private Double importeTotalSalida;
    private String contrato;

    private static final long serialVersionUID = 1L;
}
