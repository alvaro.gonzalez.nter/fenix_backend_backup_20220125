package com.haya.alaska.estimacion.infrastructure.controller.dto;

import com.haya.alaska.importe_estimacion_contrato.domain.ImporteEstimacionContrato;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ImporteEstimacionContratoDTO implements Serializable {
  private Integer id;
  private Double importeTotal;
  private Double importeTotalSalida;
  private ContratoDTOToList contrato;

  public ImporteEstimacionContratoDTO(ImporteEstimacionContrato importeEstimacionContrato) {
    this.id = importeEstimacionContrato.getId();
    this.importeTotal = importeEstimacionContrato.getImporteTotal();
    this.importeTotalSalida = importeEstimacionContrato.getImporteTotalSalida();
    this.contrato = new ContratoDTOToList(importeEstimacionContrato.getContrato());
  }
}
