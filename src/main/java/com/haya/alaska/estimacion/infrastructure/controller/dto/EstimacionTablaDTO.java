package com.haya.alaska.estimacion.infrastructure.controller.dto;

import com.haya.alaska.estimacion.domain.Estimacion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class EstimacionTablaDTO implements Serializable {
  private Integer id;
  private Date fecha;
  private String contratoExpediente;
  private String estrategia;
  private Double importeRecuperado;
  private Double importeSalida;
  private String intervaloFechasEstimado;
  private String probabilidadResolucion;
  private String usuario;
  private String cumplida;
  private Date fechaEstimada;


  public EstimacionTablaDTO(Estimacion estimacion) {
    this.id = estimacion.getId();
    this.fecha = estimacion.getFecha();
    this.fechaEstimada = estimacion.getFechaEstimada();
    this.importeRecuperado = estimacion.getImporteRecuperado();
    this.importeSalida = estimacion.getImporteSalida();
    this.intervaloFechasEstimado = estimacion.getIntervaloFechasEstimado();
    this.usuario = estimacion.getUsuario() != null ? estimacion.getUsuario().getNombre() : null;

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      this.estrategia = estimacion.getEstrategia();
      this.probabilidadResolucion = estimacion.getProbabilidadResolucionString();
      this.cumplida = estimacion.getCumplida() != null ? estimacion.getCumplida().getValorIngles() : null;

    } else{
      this.estrategia = estimacion.getEstrategia();
      this.probabilidadResolucion = estimacion.getProbabilidadResolucionString();
      this.cumplida = estimacion.getCumplida() != null ? estimacion.getCumplida().getValor() : null;
    }

    if (estimacion.getImportesContratos().isEmpty() && estimacion.getImportesBienesPosesionNegociada().isEmpty()) {
      if (estimacion.getExpediente() != null) {
        this.contratoExpediente = "E" + estimacion.getExpediente().getIdConcatenado();
      }
      if (estimacion.getExpedientePosesionNegociada() != null) {
        this.contratoExpediente = "E" + estimacion.getExpedientePosesionNegociada().getIdConcatenado();
      }
    } else {
      if (!estimacion.getImportesContratos().isEmpty()) {
        this.contratoExpediente =
          estimacion.getImportesContratos().stream()
            .map(ic -> "C" + ic.getContrato().getIdCarga())
            .collect(Collectors.joining(", "));
      }
      if (!estimacion.getImportesBienesPosesionNegociada().isEmpty()) {
        this.contratoExpediente =
          estimacion.getImportesBienesPosesionNegociada().stream()
            .map(ic -> "B" + ic.getBienPosesionNegociada().getId())
            .collect(Collectors.joining(", "));
      }
    }

  }

  public static List<EstimacionTablaDTO> newList(List<Estimacion> estimaciones) {
    return estimaciones.stream().map(EstimacionTablaDTO::new).collect(Collectors.toList());
  }
}
