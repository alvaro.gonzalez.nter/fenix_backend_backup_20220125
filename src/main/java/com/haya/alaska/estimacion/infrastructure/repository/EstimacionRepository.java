package com.haya.alaska.estimacion.infrastructure.repository;

import com.haya.alaska.estimacion.domain.Estimacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface EstimacionRepository extends JpaRepository<Estimacion, Integer> {
  Optional<Estimacion> findByIdAndActivoIsTrue(Integer id);

  List<Estimacion> findAllByExpedienteId(Integer idExpediente);

  List<Estimacion> findAllByFechaEstimadaEquals(Date fecha);

  List<Estimacion> findAllByExpedientePosesionNegociadaId(Integer idExpediente);

  List<Estimacion> findAllByExpedienteCarteraId(Integer idCartera);

  List<Estimacion> findAllByExpedientePosesionNegociadaCarteraId(Integer idCartera);

  List<Estimacion> findAllByExpedienteCarteraIdAndTipoId(
    Integer idCartera, Integer idTipo);

  List<Estimacion> findAllByExpedientePosesionNegociadaCarteraIdAndTipoId(
    Integer idCartera, Integer idTipo);
}
