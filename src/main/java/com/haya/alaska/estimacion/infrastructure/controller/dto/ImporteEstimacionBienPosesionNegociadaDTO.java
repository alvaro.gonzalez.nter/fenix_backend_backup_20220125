package com.haya.alaska.estimacion.infrastructure.controller.dto;

import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDTOToList;
import com.haya.alaska.importe_estimacion_bien_pos_negociada.domain.ImporteEstimacionBienPosesionNegociada;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ImporteEstimacionBienPosesionNegociadaDTO implements Serializable {
  private Integer id;
  private Double importeTotal;
  private Double importeTotalSalida;
  private BienPosesionNegociadaDTOToList bien;


  public ImporteEstimacionBienPosesionNegociadaDTO(ImporteEstimacionBienPosesionNegociada importeEstimacionBienPosesionNegociada) {
    this.id = importeEstimacionBienPosesionNegociada.getId();
    this.importeTotal = importeEstimacionBienPosesionNegociada.getImporteTotal();
    this.importeTotalSalida = importeEstimacionBienPosesionNegociada.getImporteTotalSalida();
    this.bien = new BienPosesionNegociadaDTOToList(importeEstimacionBienPosesionNegociada.getBienPosesionNegociada());
  }
}
