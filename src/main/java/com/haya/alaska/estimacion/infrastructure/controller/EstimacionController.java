package com.haya.alaska.estimacion.infrastructure.controller;

import com.haya.alaska.estimacion.aplication.EstimacionExcelUseCase;
import com.haya.alaska.estimacion.aplication.EstimacionUseCase;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionDTO;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionInputDTO;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionTablaDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientes/{idExpediente}/estimaciones")
public class EstimacionController {

  @Autowired
  private EstimacionUseCase estimacionUseCase;
  @Autowired
  private UsuarioRepository usuarioRepository;

  @Autowired
  private EstimacionExcelUseCase estimacionExcelUseCase;

  @ApiOperation(
      value = "Listado de estimaciones de un expediente",
      notes = "Devuelve todas las estimaciones de un expediente")
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public ListWithCountDTO<EstimacionTablaDTO> getAllEstimaciones(
      @PathVariable("idExpediente") Integer idExpediente,
      @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
      @RequestParam(value = "contratoExpediente", required = false) String contrato,
      @RequestParam(value = "estrategia", required = false) String estrategia,
      @RequestParam(value = "fechaEstimada", required = false) String fechaEstimada,
      @RequestParam(value = "importeRecuperado", required = false) String importeRecuperado,
      @RequestParam(value = "importeSalida", required = false) String importeSalida,
      @RequestParam(value = "probabilidadResolucion", required = false)
          String probabilidadResolucion,
      @RequestParam(value = "intervaloFechasEstimado", required = false) String intervaloFechasEstimado,
      @RequestParam(value = "usuario", required = false) String usuario,
      @RequestParam(value = "historico", required = false, defaultValue = "false") boolean historico,
      @RequestParam(value = "orderField", required = false) String orderField,
      @RequestParam(value = "orderDirection", required = false) String orderDirection,
      @RequestParam(value = "cumplida", required = false) String cumplida,
      @RequestParam(value = "size", required = true) Integer size,
      @RequestParam(value = "page", required = true) Integer page,
      @ApiIgnore CustomUserDetails principal) {
    Usuario loggedUser = (Usuario) principal.getPrincipal();
    return estimacionUseCase.getAll(
        idExpediente,
        posesionNegociada,
        loggedUser,
        contrato,
        estrategia,
        fechaEstimada,
        importeRecuperado,
        importeSalida,
        probabilidadResolucion,
        intervaloFechasEstimado,
        usuario,
        historico,
        orderField,
        orderDirection,
        cumplida,
        size,
        page);
  }

  @ApiOperation(value = "Devuelve los datos de una estimación")
  @GetMapping("/{idEstimacion}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<EstimacionDTO> getAcuerdo(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idEstimacion") Integer idEstimacion,
      @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
      @ApiIgnore CustomUserDetails principal) {
    try {
      var estimacionDTO = estimacionUseCase.getEstimacion(idExpediente, idEstimacion,posesionNegociada,(Usuario) principal.getPrincipal());
      return ResponseEntity.ok(estimacionDTO);
    } catch (NotFoundException e) {
      return ResponseEntity.notFound().build();
    }
  }

  @ApiOperation(value = "Borrar una estimación de un expediente")
  @DeleteMapping("/{idEstimacion}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void deleteDocumentoContrato(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idEstimacion") Integer idEstimacion)
      throws NotFoundException {
    estimacionUseCase.ponerEstimacionInactivo(idEstimacion);
  }

  @ApiOperation(value = "Añadir una estimación a un expediente o contrato")
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @Transactional(rollbackFor = Exception.class)
  public EstimacionDTO createEstimacion(
    @PathVariable("idExpediente") Integer idExpediente,
    @RequestParam(value = "idContrato", required = false) Integer idContrato,
    @RequestBody EstimacionInputDTO estimacionInputDTO,
    @RequestParam(value = "posesionNegociada") boolean posesionNegociada,
    @ApiIgnore CustomUserDetails principal)
      throws Exception {
      return estimacionUseCase.createEstimacion(estimacionInputDTO, idExpediente, (Usuario) principal.getPrincipal(),posesionNegociada);
  }

  @ApiOperation(value = "Editar una estimación")
  @PutMapping("/{idEstimacion}")
  @ResponseStatus(HttpStatus.CREATED)
  @Transactional(rollbackFor = Exception.class)
  public EstimacionDTO updateEstimacion(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idEstimacion") Integer idEstimacion,
    @RequestBody EstimacionInputDTO estimacionInputDTO,
    @RequestParam(value = "posesionNegociada") boolean posesionNegociada,
    @ApiIgnore CustomUserDetails principal)
          throws Exception {
    return estimacionUseCase.updateEstimacion(estimacionInputDTO, idEstimacion, (Usuario) principal.getPrincipal(), posesionNegociada);
  }

  @ApiOperation(value = "Descargar información de Estimaciones en Excel", notes = "Descargar información en Excel de las estimaciones por id de expediente")
  @GetMapping("/excel")
  public ResponseEntity<InputStreamResource> getExcelBienById(@PathVariable Integer idExpediente,
                                                              @RequestParam(value = "posesionNegociada" , required = false) Boolean posesionNegociada,
                                                              @ApiIgnore CustomUserDetails principal  )
          throws Exception {
    ExcelExport excelExport = new ExcelExport();
    if (posesionNegociada==null){ //para evitar tener que cambiar las llamadas en front desde gestion amistosa
      posesionNegociada = Boolean.FALSE;
    }
    ByteArrayInputStream excel = excelExport
            .create(this.estimacionUseCase.findExcelEstimacionById(idExpediente,(Usuario) principal.getPrincipal(), posesionNegociada));

    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    return excelExport.download(excel, idExpediente + "-estimacion-" + date);
  }













}
