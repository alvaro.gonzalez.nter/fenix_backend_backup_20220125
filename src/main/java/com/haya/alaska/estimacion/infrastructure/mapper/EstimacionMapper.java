package com.haya.alaska.estimacion.infrastructure.mapper;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoDTO;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.PlazoAcuerdoPagoDTO;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionDTO;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionInputDTO;
import com.haya.alaska.estimacion.infrastructure.controller.dto.ImporteEstimacionContratoInputDTO;
import com.haya.alaska.estimacion.infrastructure.repository.EstimacionRepository;
import com.haya.alaska.estimacion_cumplida.domain.EstimacionCumplida;
import com.haya.alaska.estimacion_cumplida.infraestructure.repository.EstimacionCumplidaRepository;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.importe_estimacion_bien_pos_negociada.domain.ImporteEstimacionBienPosesionNegociada;
import com.haya.alaska.importe_estimacion_bien_pos_negociada.infrastructure.repository.ImporteEstimacionBienPosesionNegociadaRepository;
import com.haya.alaska.importe_estimacion_contrato.domain.ImporteEstimacionContrato;
import com.haya.alaska.importe_estimacion_contrato.infrastructure.repository.ImporteEstimacionContratoRepository;
import com.haya.alaska.intervalo_estimacion.infrastructure.repository.IntervaloEstimacionRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_propuesta.infrastructure.repository.SubtipoPropuestaRepository;
import com.haya.alaska.tipo_propuesta.infrastructure.repository.TipoPropuestaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class EstimacionMapper {
  @Autowired
  ExpedienteRepository expedienteRepository;
  @Autowired
  ContratoRepository contratoRepository;
  @Autowired
  IntervaloEstimacionRepository intervaloEstimacionRepository;
  @Autowired
  TipoPropuestaRepository tipoPropuestaRepository;
  @Autowired
  SubtipoPropuestaRepository subtipoPropuestaRepository;
  @Autowired
  EstimacionRepository estimacionRepository;
  @Autowired
  ImporteEstimacionContratoRepository importeEstimacionContratoRepository;
  @Autowired
  EstimacionCumplidaRepository estimacionCumplidaRepository;
  @Autowired
  ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  ImporteEstimacionBienPosesionNegociadaRepository importeEstimacionBienPosesionNegociadaRepository;

  /**
   * Convierte un objeto EstimacionInputDTO a Estimacion comprobando que las dependencias con otras
   * tablas son correctas.
   *
   * @param estimacionInputDTO //  * @param estimacion
   * @param idExpediente
   * @return
   * @throws NotFoundException
   */
  public Estimacion createEstimacion(
    EstimacionInputDTO estimacionInputDTO,
    Integer idExpediente,
    Usuario usuario,
    boolean hasPermissions,
    boolean posesionNegociada)
    throws NotFoundException, RequiredValueException {

    var estimacion = new Estimacion();
    BeanUtils.copyProperties(estimacionInputDTO, estimacion, "id", "contratos", "intervinientes", "bienes", "acuerdosPago", "importes");


    // activamos la ESTIMACION
    estimacion.setActivo(true);
    estimacion.setFecha(new Date());
    estimacion.setUsuario(usuario);

    if (posesionNegociada) {
      var expedientePN = expedientePosesionNegociadaRepository.findById(idExpediente).orElseThrow(() -> new NotFoundException("Expediente de poesión negociada", idExpediente));
      estimacion.setExpedientePosesionNegociada(expedientePN);
    } else {
      var expediente = expedienteRepository.findById(idExpediente).orElseThrow(() -> new NotFoundException("Expediente", idExpediente));
      estimacion.setExpediente(expediente);
    }


    EstimacionCumplida cumplida = null;
    if (estimacionInputDTO.getCumplida() != null) {
      cumplida =
        estimacionCumplidaRepository.findById(estimacionInputDTO.getCumplida()).orElse(null);
    }
    if (hasPermissions) {
      estimacion.setCumplida(cumplida);
    }

  // if (!estimacion.getProcedimientoJudicial()) {
      // Intervalo
      if (estimacionInputDTO.getIntervalo() == null)
        throw new RequiredValueException("intervalo");
      var intervalo =
        intervaloEstimacionRepository
          .findById(estimacionInputDTO.getIntervalo())
          .orElseThrow(
            () ->
              new NotFoundException("intervalo", estimacionInputDTO.getIntervalo()));
      estimacion.setIntervalo(intervalo);
      // añadimos en bbdd la columna calculada sobre el intervalo y la fecha estimada de
      // recuperacion para poder usar después el filtro
      String filtroFecha = estimacion.getIntervaloFechasEstimado();
      estimacion.setFechaEstimadaFiltro(filtroFecha);

      // Tipo
      if (estimacionInputDTO.getTipo() == null)
        throw new RequiredValueException("tipo");
      var tipo = tipoPropuestaRepository.findById(estimacionInputDTO.getTipo()).orElse(null);
      estimacion.setTipo(tipo);

      // Subtipo
      if (estimacionInputDTO.getSubtipo() == null)
        throw new RequiredValueException("subtipo");
      var subtipo =
        subtipoPropuestaRepository
          .findById(estimacionInputDTO.getSubtipo())
          .orElseThrow(
            () ->
              new NotFoundException(
                "subtipo propuesta", estimacionInputDTO.getSubtipo()));
      if (!subtipo.getTipoPropuesta().getId().equals(estimacionInputDTO.getTipo())){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Proposal Subtype: " + estimacionInputDTO.getSubtipo() + " DOES NOT belong to the type sent: " + estimacionInputDTO.getTipo());
        else throw new NotFoundException("Subtipo Propuesta: " + estimacionInputDTO.getSubtipo() + " NO pertenece al tipo mandado: " + estimacionInputDTO.getTipo());
      }

      estimacion.setSubtipo(subtipo);
  //  }
    Estimacion estimacionSaved = estimacionRepository.saveAndFlush(estimacion);
    // comprobamos si la estimación está asignada a un expediente o a contratos
    if (estimacionInputDTO.getImportesContratosNuevos() != null
      || !estimacionInputDTO.getImportesContratosActualizados().isEmpty()) {
      // inicializar contratos e importes asociados
      if (posesionNegociada) {
        List<ImporteEstimacionBienPosesionNegociada> importesSaved =
          createImporteEstimacionBienPosesionNegociada(estimacionInputDTO, estimacionSaved);
        estimacionSaved.addImportesEstimacionBienPosesionNegociada(importesSaved);
      } else {
        List<ImporteEstimacionContrato> importesSaved =
          createImporteEstimacionContrato(estimacionInputDTO, idExpediente, estimacionSaved);
        estimacionSaved.addImportesEstimacionContrato(importesSaved);
      }
    }

    return estimacionRepository.saveAndFlush(estimacionSaved);
  }

  public Estimacion actualizarCamposEstimacion(
    EstimacionInputDTO estimacionInputDTO, Estimacion estimacion, boolean hasPermission, boolean posesionNegociada)
    throws NotFoundException, RequiredValueException {

    BeanUtils.copyProperties(estimacionInputDTO, estimacion);

    EstimacionCumplida cumplida = null;
    if (estimacionInputDTO.getCumplida() != null)
      cumplida =
        estimacionCumplidaRepository.findById(estimacionInputDTO.getCumplida()).orElse(null);
    if (hasPermission) {
      estimacion.setCumplida(cumplida);
    }

  //  if (!estimacion.getProcedimientoJudicial()) {
      // Intervalo
      if (estimacionInputDTO.getIntervalo() == null)
        throw new RequiredValueException("intervalo");
      var intervalo =
        intervaloEstimacionRepository
          .findById(estimacionInputDTO.getIntervalo())
          .orElseThrow(
            () ->
              new NotFoundException(
                "intervalo", estimacionInputDTO.getIntervalo()));
      estimacion.setIntervalo(intervalo);
      // añadimos en bbdd la columna calculada sobre el intervalo y la fecha estimada de
      // recuperacion para poder usar después el filtro
      String filtroFecha = estimacion.getIntervaloFechasEstimado();
      estimacion.setFechaEstimadaFiltro(filtroFecha);

      // Tipo
      if (estimacionInputDTO.getTipo() == null)
        throw new RequiredValueException("tipo");
      var tipo =
        tipoPropuestaRepository
          .findById(estimacionInputDTO.getTipo())
          .orElseThrow(
            () ->
              new NotFoundException(
                "tipo", estimacionInputDTO.getTipo()));
      estimacion.setTipo(tipo);

      // Subtipo
      if (estimacionInputDTO.getSubtipo() == null)
        throw new RequiredValueException("subtipo");
      var subtipo =
        subtipoPropuestaRepository
          .findById(estimacionInputDTO.getSubtipo())
          .orElseThrow(
            () ->
              new NotFoundException(
                "subtipo propuesta", estimacionInputDTO.getSubtipo()));
      if (!subtipo.getTipoPropuesta().getId().equals(estimacionInputDTO.getTipo())){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Proposal Subtype: " + estimacionInputDTO.getSubtipo() + " DOES NOT belong to the type sent: " + estimacionInputDTO.getTipo());
        else throw new NotFoundException("Subtipo Propuesta: " + estimacionInputDTO.getSubtipo() + " NO pertenece al tipo mandado: " + estimacionInputDTO.getTipo());
      }
      estimacion.setSubtipo(subtipo);
  //  }

    // actualizamos importes existentes
    if (estimacionInputDTO.getImportesContratosActualizados() != null
      && !estimacionInputDTO.getImportesContratosActualizados().isEmpty()) {
      if (posesionNegociada) {
        deleteImportesEstimacionBienPosesionNegociada(
          estimacionInputDTO.getImportesContratosActualizados(),
          estimacion.getImportesBienesPosesionNegociada(),
          estimacion);
        estimacion.addImportesEstimacionBienPosesionNegociada(updateImporteEstimacionBienPosesionNegociada(estimacionInputDTO.getImportesContratosActualizados()));
      } else {
        deleteImportesEstimacionContrato(
          estimacionInputDTO.getImportesContratosActualizados(),
          estimacion.getImportesContratos(),
          estimacion);
        estimacion.clearContratos();
        estimacion.addImportesEstimacionContrato(updateImporteEstimacionContrato(estimacionInputDTO.getImportesContratosActualizados()));
      }
    }

    Estimacion estimacionSaved = estimacionRepository.saveAndFlush(estimacion);
    Integer idExpediente;
    if (estimacion.getExpediente() != null)
      idExpediente = estimacionSaved.getExpediente().getId();
    else
      idExpediente = estimacionSaved.getExpedientePosesionNegociada().getId();


    // creamos importes nuevos
    if (estimacionInputDTO.getImportesContratosNuevos() != null
      && !estimacionInputDTO.getImportesContratosActualizados().isEmpty()) {
      if (posesionNegociada) {
        List<ImporteEstimacionBienPosesionNegociada> importesCreated =
          createImporteEstimacionBienPosesionNegociada(
            estimacionInputDTO, estimacionSaved);
        estimacionSaved.addImportesEstimacionBienPosesionNegociada(importesCreated);
      } else {
        List<ImporteEstimacionContrato> importesCreated =
          createImporteEstimacionContrato(
            estimacionInputDTO, idExpediente, estimacionSaved);
        estimacionSaved.addImportesEstimacionContrato(importesCreated);
      }
    }
    return estimacionRepository.saveAndFlush(estimacionSaved);
  }

  private void deleteImportesEstimacionContrato(
    List<ImporteEstimacionContratoInputDTO> importesContratosActualizados,
    Set<ImporteEstimacionContrato> importesContratos,
    Estimacion estimacion) {
    for (var x : importesContratos) {
      var found =
        importesContratosActualizados.stream()
          .filter((ImporteEstimacionContratoInputDTO i) -> i.getId() == x.getId())
          .findAny();
      if (found.isEmpty()) {
        importeEstimacionContratoRepository.deleteById(x.getId());
      }
    }
    estimacion.setImportesContratos(importesContratos);
  }

  private void deleteImportesEstimacionBienPosesionNegociada(
    List<ImporteEstimacionContratoInputDTO> importesContratosActualizados,
    Set<ImporteEstimacionBienPosesionNegociada> importeEstimacionBienPosesionNegociadas,
    Estimacion estimacion) {
    for (var x : importeEstimacionBienPosesionNegociadas) {
      var found =
        importesContratosActualizados.stream()
          .filter((ImporteEstimacionContratoInputDTO i) -> i.getId() == x.getId())
          .findAny();
      if (found.isEmpty()) {
        importeEstimacionBienPosesionNegociadaRepository.deleteById(x.getId());
      }
    }
    estimacion.setImportesBienesPosesionNegociada(importeEstimacionBienPosesionNegociadas);
  }

  private List<ImporteEstimacionContrato> updateImporteEstimacionContrato(
    List<ImporteEstimacionContratoInputDTO> importes) {
    List<ImporteEstimacionContrato> result = new ArrayList<>();
    for (ImporteEstimacionContratoInputDTO importe : importes) {
      var importeEstimacionContrato =
        importeEstimacionContratoRepository.findById(importe.getId()).get();
      importeEstimacionContrato.setImporteTotal(importe.getImporteTotal());
      importeEstimacionContrato.setImporteTotalSalida(importe.getImporteTotalSalida());
      var importeEstimacionContratoUpdated =
        importeEstimacionContratoRepository.save(importeEstimacionContrato);
      result.add(importeEstimacionContratoUpdated);
    }
    return result;
  }

  private List<ImporteEstimacionBienPosesionNegociada> updateImporteEstimacionBienPosesionNegociada(
    List<ImporteEstimacionContratoInputDTO> importes) {
    List<ImporteEstimacionBienPosesionNegociada> result = new ArrayList<>();
    for (ImporteEstimacionContratoInputDTO importe : importes) {
      var importeEstimacionBienPosesionNegociada =
        importeEstimacionBienPosesionNegociadaRepository.findById(importe.getId()).get();
      importeEstimacionBienPosesionNegociada.setImporteTotal(importe.getImporteTotal());
      importeEstimacionBienPosesionNegociada.setImporteTotalSalida(importe.getImporteTotalSalida());
      var importeEstimacionContratoUpdated =
        importeEstimacionBienPosesionNegociadaRepository.save(importeEstimacionBienPosesionNegociada);
      result.add(importeEstimacionContratoUpdated);
    }
    return result;
  }


  private List<ImporteEstimacionBienPosesionNegociada> createImporteEstimacionBienPosesionNegociada(EstimacionInputDTO estimacionInputDTO, Estimacion estimacion) throws NotFoundException {
    if (estimacionInputDTO.getContratosNuevos() == null) return new ArrayList<>();
    var contPosContrato = 0;
    List<ImporteEstimacionBienPosesionNegociada> result = new ArrayList<>();
    for (Integer idBienPN : estimacionInputDTO.getContratosNuevos()) {
      var bienPN = bienPosesionNegociadaRepository.findById(idBienPN).orElse(null);
      if (bienPN == null)
        throw new NotFoundException(
          "Bien PN", idBienPN);
      ImporteEstimacionBienPosesionNegociada importeEstimacion = new ImporteEstimacionBienPosesionNegociada();
      importeEstimacion.setActivo(true);
      importeEstimacion.setEstimacion(estimacion);
      importeEstimacion.setBienPosesionNegociada(bienPN);
      importeEstimacion.setImporteTotal(
        estimacionInputDTO.getImportesContratosNuevos().get(contPosContrato));
      importeEstimacion.setImporteTotalSalida(
        estimacionInputDTO.getImportesSalidaContratosNuevos().get(contPosContrato));
      ImporteEstimacionBienPosesionNegociada importeEstimacionContratoSaved =
        importeEstimacionBienPosesionNegociadaRepository.saveAndFlush(importeEstimacion);
      result.add(importeEstimacionContratoSaved);
      contPosContrato++;
    }
    return result;
  }

  private List<ImporteEstimacionContrato> createImporteEstimacionContrato(
    EstimacionInputDTO estimacionInputDTO, Integer idExpediente, Estimacion estimacion)
    throws NotFoundException {
    if (estimacionInputDTO.getContratosNuevos() == null) return new ArrayList<>();
    var contPosContrato = 0;
    List<ImporteEstimacionContrato> result = new ArrayList<>();
    for (Integer contratoId : estimacionInputDTO.getContratosNuevos()) {
      var contrato = contratoRepository.findById(contratoId).orElse(null);
      if (contrato == null)
        throw new NotFoundException(
          "Contrato", contratoId);
      ImporteEstimacionContrato importeEstimacionContrato = new ImporteEstimacionContrato();
      importeEstimacionContrato.setActivo(true);
      importeEstimacionContrato.setEstimacion(estimacion);
      importeEstimacionContrato.setContrato(contrato);
      importeEstimacionContrato.setImporteTotal(
        estimacionInputDTO.getImportesContratosNuevos().get(contPosContrato));
      importeEstimacionContrato.setImporteTotalSalida(
        estimacionInputDTO.getImportesSalidaContratosNuevos().get(contPosContrato));
      ImporteEstimacionContrato importeEstimacionContratoSaved =
        importeEstimacionContratoRepository.saveAndFlush(importeEstimacionContrato);
      result.add(importeEstimacionContratoSaved);
      contPosContrato++;
    }
    return result;
  }

  public AcuerdoPagoDTO acuerdoPagoToAcuerdoPagoDto(
    AcuerdoPago acuerdoPago, AcuerdoPagoDTO acuerdoPagoDto) {
    BeanUtils.copyProperties(acuerdoPago, acuerdoPagoDto, "plazosAcuerdoPago");

    acuerdoPagoDto.setUsuario(new UsuarioDTO(acuerdoPago.getUsuario()));
    acuerdoPagoDto.setOrigenAcuerdo(new CatalogoDTO(acuerdoPago.getOrigenAcuerdo()));
    acuerdoPagoDto.setPeriodicidadAcuerdoPago(
      new CatalogoDTO(acuerdoPago.getPeriodicidadAcuerdoPago()));
    if (acuerdoPago.getPlazosAcuerdoPago() != null) {
      List<PlazoAcuerdoPagoDTO> plazosAcuerdoPagoDto = new ArrayList<>();
      for (var plazo : acuerdoPago.getPlazosAcuerdoPago()) {
        plazosAcuerdoPagoDto.add(
          PlazoAcuerdoPagoDTO.builder()
            .cantidad(plazo.getCantidad())
            .fecha(plazo.getFecha())
            .build());
      }
      acuerdoPagoDto.setPlazosAcuerdoPago(plazosAcuerdoPagoDto);
    }
    if (acuerdoPago.getContratos() != null) {
      acuerdoPagoDto.setContratos(
        acuerdoPago.getContratos().stream().map(Contrato::getId).collect(Collectors.toList()));
    }
    return acuerdoPagoDto;
  }

  public EstimacionDTO createEstimacionDTO(
    Optional<Estimacion> estimacion, EstimacionDTO estimacionDTO, Boolean posesionNegociada) {
    if (estimacion == null || estimacionDTO == null) return null;

    BeanUtils.copyProperties(estimacion, estimacionDTO, "contratos", "intervinientes", "bienes");

   /* if (!posesionNegociada) {
      if (estimacion.get().getExpediente() != null)
        estimacionDTO.setExpediente(estimacion.get().getExpediente().getId());
    } else {
      if (estimacion.get().getExpediente() != null)
        estimacionDTO.setExpedientePosesionNegociada(estimacion.get().getExpediente().getId());
}*/
    if (estimacion.get().getId() != null) estimacionDTO.setId(estimacion.get().getId());
    if (estimacion.get().getFecha() != null) estimacionDTO.setFecha(estimacion.get().getFecha());

    /* List<ImporteEstimacionContratoDTO> listaestimacionescontrato = new ArrayList(estimacion.get().getImportesContratos());
    if (estimacion.get().getImportesContratos()!=null){

      estimacionDTO.setImportesContratos(listaestimacionescontrato);
    }*/

    if (estimacion.get().getProcedimientoJudicial() != null)
      estimacionDTO.setProcedimientoJudicial(estimacion.get().getProcedimientoJudicial());
    if (estimacion.get().getImporteRecuperado() != null)
      estimacionDTO.setImporteRecuperado(estimacion.get().getImporteRecuperado());
    if (estimacion.get().getImporteSalida() != null)
      estimacionDTO.setImporteSalida(estimacion.get().getImporteSalida());
    if (estimacion.get().getIntervalo() != null)
      estimacionDTO.setIntervalo(new CatalogoDTO(estimacion.get().getIntervalo()));
    if (estimacion.get().getFechaEstimada() != null)
      estimacionDTO.setFechaEstimada(estimacion.get().getFechaEstimada());
    if (estimacion.get().getProbabilidadResolucion() != null)
      estimacionDTO.setProbabilidadResolucion(estimacion.get().getProbabilidadResolucion());
    if (estimacion.get().getTipo() != null) estimacionDTO.setTipo(new CatalogoDTO(estimacion.get().getTipo()));
    if (estimacion.get().getSubtipo() != null) estimacionDTO.setSubtipo(new CatalogoDTO(estimacion.get().getSubtipo()));
    if (estimacion.get().getTipoSolucion() != null) estimacionDTO.setTipoSolucion(estimacion.get().getTipoSolucion());


    if (estimacion.get().getComentarios() != null) estimacionDTO.setComentarios(estimacion.get().getComentarios());
    if (estimacion.get().getCumplimiento() != null) estimacionDTO.setCumplimiento(estimacion.get().getCumplimiento());
    if (estimacion.get().getCumplida() != null) estimacionDTO.setCumplida(estimacion.get().getCumplida());
    if (estimacion.get().getActivo() != null) estimacionDTO.setActivo(estimacion.get().getActivo());

    return estimacionDTO;
  }
}
