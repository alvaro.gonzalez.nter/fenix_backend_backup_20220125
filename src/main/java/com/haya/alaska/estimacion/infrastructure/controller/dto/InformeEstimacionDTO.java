package com.haya.alaska.estimacion.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InformeEstimacionDTO {

  private Integer estadoExpediente;
  private Integer tipoEstrategia;
  private Date fechaResolucionInicial;
  private Date fechaResolucionFinal;

}
