package com.haya.alaska.estimacion.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/** @author agonzalez */

@Getter
@ToString
@NoArgsConstructor
public class EstimacionInputDTO implements Serializable {
    List<Integer> contratosNuevos;
    List<Double> importesContratosNuevos;
    List<Double> importesSalidaContratosNuevos;
    List<ImporteEstimacionContratoInputDTO> importesContratosActualizados;
    @NotNull
    Boolean procedimientoJudicial; // obligatorio
    Double importeRecuperado; // obligatorio (si no judicial)
    Double importeSalida;
    Date fechaEstimada;
    Integer intervalo;
    Integer probabilidadResolucion; // obligatorio (si no judicial)
    Integer tipo; // obligatorio (si no judicial)
    Integer subtipo; // obligatorio (si no judicial)
    String tipoSolucion;
    String comentarios;
    Boolean cumplimiento; // obligatorio (si no judicial)
    Integer cumplida;
    private static final long serialVersionUID = 1L;
}
