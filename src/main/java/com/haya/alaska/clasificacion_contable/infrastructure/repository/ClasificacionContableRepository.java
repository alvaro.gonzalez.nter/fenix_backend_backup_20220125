package com.haya.alaska.clasificacion_contable.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable;

@Repository
public interface ClasificacionContableRepository
        extends CatalogoRepository<ClasificacionContable, Integer> {

}
