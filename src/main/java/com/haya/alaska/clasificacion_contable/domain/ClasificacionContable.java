package com.haya.alaska.clasificacion_contable.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_CLASIFICACION_CONTABLE")
@Entity
@Table(name = "LKUP_CLASIFICACION_CONTABLE")
public class ClasificacionContable extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLASIFICACION_CONTABLE")
  @NotAudited
  private Set<Contrato> contratos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLASIFICACION_CONTABLE")
  @NotAudited
  private Set<Segmentacion> segmentaciones = new HashSet<>();
}
