package com.haya.alaska.localidad.infrastructure.controller;

import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.controller.dto.LocalidadDTO;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/localidades")
public class LocalidadController {
  @Autowired
  private LocalidadRepository localidadRepository;
  @Autowired
  private MunicipioRepository municipioRepository;

  @ApiOperation(
          value = "Listado de localidades",
          notes = "Devuelve todas las localidades, opcionalmente filtrando por municipio")
  @GetMapping()
  public List<LocalidadDTO> getAll(@RequestParam(value = "municipio", required = false) Integer municipio,
                                   @RequestParam(value = "provincia", required = false) Integer provincia) {
    List<Localidad> lista;
    if (provincia != null) {
      List<Municipio> municipios = municipioRepository.findByProvinciaIdAndActivoIsTrueOrderByValor(provincia);
      if(municipios != null && municipios.size() > 0) {
        lista = localidadRepository.findAllByActivoIsTrueAndMunicipioInOrderByValor(municipios);
      } else {
        lista = new ArrayList<>();
      }
    } else if (municipio != null) {
      lista = localidadRepository.findByMunicipioIdAndActivoIsTrueOrderByValor(municipio);
    } else {
      lista = localidadRepository.findAll();
    }
    return lista.stream().map(LocalidadDTO::new).collect(Collectors.toList());
  }
}
