package com.haya.alaska.localidad.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.municipio.domain.Municipio;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocalidadRepository extends CatalogoRepository<Localidad, Integer> {
  List<Localidad> findByMunicipioIdAndActivoIsTrueOrderByValor(Integer municipio);
  List<Localidad> findAllByActivoIsTrueAndMunicipioInOrderByValor(List<Municipio> municipios);
  List<Localidad> findByValorAndMunicipioProvinciaValor(String valorLocalidad, String valorProvincia);
}
