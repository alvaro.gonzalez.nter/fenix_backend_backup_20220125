package com.haya.alaska.localidad.infrastructure.controller.dto;

import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.municipio.infrastructure.controller.dto.MunicipioDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;

@Getter
@NoArgsConstructor
public class LocalidadDTO implements Serializable {
  private Integer id;
  private String codigo;
  private String valor;
  private Boolean activo;
  private MunicipioDTO municipio;

  public LocalidadDTO(Localidad localidad) {
    this.id = localidad.getId();
    this.codigo = localidad.getCodigo();
    String valor = null;
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();
    if (defaultLocal == null || defaultLocal.equals("es")) valor = localidad.getValor();
    else if (defaultLocal.equals("en")) valor = localidad.getValorIngles();
    this.valor = valor;
    this.activo = localidad.getActivo();
    this.municipio = localidad.getMunicipio() != null ? new MunicipioDTO(localidad.getMunicipio()) : null;
  }
}
