package com.haya.alaska.localidad.domain;

import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class DatosLocalizacionExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Dossier Id");
      cabeceras.add("Intervener Id");
      cabeceras.add("Contract Id");
      cabeceras.add("Validated");
      cabeceras.add("Via Type");
      cabeceras.add("Street Name");
      cabeceras.add("Nº");
      cabeceras.add("Block");
      cabeceras.add("Stairs");
      cabeceras.add("Floor");
      cabeceras.add("Door");
      cabeceras.add("Environment");
      cabeceras.add("Location");
      cabeceras.add("Municipality");
      cabeceras.add("Province");
      cabeceras.add("Comment Address");
      cabeceras.add("Postal Code");
      cabeceras.add("Country");
      cabeceras.add("Length");
      cabeceras.add("Latitude");
      cabeceras.add("View on Google");
      cabeceras.add("Origin");
      cabeceras.add("Update Date");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Interviniente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Validada");
      cabeceras.add("Tipo de Via");
      cabeceras.add("Nombre de la Calle");
      cabeceras.add("Nº");
      cabeceras.add("Bloque");
      cabeceras.add("Escalera");
      cabeceras.add("Piso");
      cabeceras.add("Puerta");
      cabeceras.add("Entorno");
      cabeceras.add("Localidad");
      cabeceras.add("Municipio");
      cabeceras.add("Provincia");
      cabeceras.add("Comentario Direccion");
      cabeceras.add("Cp");
      cabeceras.add("Pais");
      cabeceras.add("Longitud");
      cabeceras.add("Latitud");
      cabeceras.add("Ver en Google");
      cabeceras.add("Origen");
      cabeceras.add("Fecha Actualizacion");
    }
  }

  public DatosLocalizacionExcel(Direccion direccion, String idContrato, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Dossier Id Expediente", idExpediente);
      this.add("Intervener Id Interviniente", direccion.getInterviniente().getId());
      this.add("Contract Id Contrato", idContrato);
      this.add("Validated", direccion.getValidada());
      this.add(
          "Via Type",
          direccion.getTipoVia() != null ? direccion.getTipoVia().getValorIngles() : null);
      this.add("Street Name", direccion.getNombre());
      this.add("Nº", direccion.getNumero());
      this.add("Block", direccion.getBloque());
      this.add("Stairs", direccion.getEscalera());
      this.add("Floor", direccion.getPiso());
      this.add("Door", direccion.getPuerta());
      this.add(
          "Environment",
          direccion.getEntorno() != null ? direccion.getEntorno().getValorIngles() : null);
      this.add(
          "Location",
          direccion.getLocalidad() != null ? direccion.getLocalidad().getValorIngles() : null);
      this.add(
          "Municipality",
          direccion.getMunicipio() != null ? direccion.getMunicipio().getValorIngles() : null);
      this.add(
          "Province",
          direccion.getProvincia() != null ? direccion.getProvincia().getValorIngles() : null);
      this.add("Comment Address", direccion.getComentario());
      this.add("Postal Code", direccion.getCodigoPostal());
      this.add(
          "Country", direccion.getPais() != null ? direccion.getPais().getValorIngles() : null);
      this.add("Length", direccion.getLongitud());
      this.add("Latitude", direccion.getLatitud());
      this.add("View on Google", direccion.verEnGoogle());
      this.add("Origin", direccion.getOrigen());
      this.add("Update Date", direccion.getFechaActualizacion());
    } else {
      this.add("Id Expediente", idExpediente);
      this.add("Id Interviniente", direccion.getInterviniente().getId());
      this.add("Id Contrato", idContrato);
      this.add("Validada", direccion.getValidada());
      this.add(
          "Tipo de Via", direccion.getTipoVia() != null ? direccion.getTipoVia().getValor() : null);
      this.add("Nombre de la Calle", direccion.getNombre());
      this.add("Nº", direccion.getNumero());
      this.add("Bloque", direccion.getBloque());
      this.add("Escalera", direccion.getEscalera());
      this.add("Piso", direccion.getPiso());
      this.add("Puerta", direccion.getPuerta());
      this.add(
          "Entorno", direccion.getEntorno() != null ? direccion.getEntorno().getValor() : null);
      this.add(
          "Localidad",
          direccion.getLocalidad() != null ? direccion.getLocalidad().getValor() : null);
      this.add(
          "Municipio",
          direccion.getMunicipio() != null ? direccion.getMunicipio().getValor() : null);
      this.add(
          "Provincia",
          direccion.getProvincia() != null ? direccion.getProvincia().getValor() : null);
      this.add("Comentario Direccion", direccion.getComentario());
      this.add("Cp", direccion.getCodigoPostal());
      this.add("Pais", direccion.getPais() != null ? direccion.getPais().getValor() : null);
      this.add("Longitud", direccion.getLongitud());
      this.add("Latitud", direccion.getLatitud());
      this.add("Ver en Google", direccion.verEnGoogle());
      this.add("Origen", direccion.getOrigen());
      this.add("Fecha Actualizacion", direccion.getFechaActualizacion());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
