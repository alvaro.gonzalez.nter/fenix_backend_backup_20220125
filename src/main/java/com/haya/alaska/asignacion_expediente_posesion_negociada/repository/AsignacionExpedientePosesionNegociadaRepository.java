package com.haya.alaska.asignacion_expediente_posesion_negociada.repository;

import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AsignacionExpedientePosesionNegociadaRepository   extends JpaRepository<AsignacionExpedientePosesionNegociada, Integer> {
  List<AsignacionExpedientePosesionNegociada> findAllByUsuarioId(Integer usuarioId);

  List<AsignacionExpedientePosesionNegociada> findAllByExpedienteId(Integer expedienteId);

  Integer countAllByUsuarioId(Integer usuarioId);

  List<AsignacionExpedientePosesionNegociada> findAllByExpedienteIdAndUsuarioPerfilId(
    Integer expedienteId, Integer perfilId);

  void deleteAllByUsuarioId(Integer usuarioId);

  List<AsignacionExpedientePosesionNegociada> findAllByExpedienteIdAndUsuarioId(Integer expedienteId, Integer usuarioId);

  void deleteAllByUsuarioIdAndExpedienteIdIn(Integer usuarioId, List<Integer> expedientesId);

  void deleteAllByUsuarioIsNullAndExpedienteIdIn(List<Integer> expedientesId);
}
