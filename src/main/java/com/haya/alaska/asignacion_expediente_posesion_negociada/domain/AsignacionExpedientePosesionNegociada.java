package com.haya.alaska.asignacion_expediente_posesion_negociada.domain;

import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_RELA_ASIGNACION_EXPEDIENTE_POSESION_NEGOCIADA")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "RELA_ASIGNACION_EXPEDIENTE_POSESION_NEGOCIADA")
public class AsignacionExpedientePosesionNegociada implements  Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_EXPEDIENTE")
  private ExpedientePosesionNegociada expediente;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_USUARIO")
  @NotNull
  private Usuario usuario;

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
    name = "RELA_ASIGNACION_PN_SUPLENTE",
    joinColumns = @JoinColumn(name = "ID_ASIGNACION_EXPEDIENTE_POSESION_NEGOCIADA"),
    inverseJoinColumns = @JoinColumn(name = "ID_SUPLENTE"))
  @AuditJoinTable(name = "HIST_RELA_ASIGNACION_PN_SUPLENTE")
  private Set<Usuario> suplente;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_USUARIO_ASIGNACION")
  private Usuario usuarioAsignacion;
}
