package com.haya.alaska.skip_tracing.aplication;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.skip_tracing.infrastructure.controller.dto.*;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface SkipTracingUseCase {

  SkipTracingOutputDTO nuevo(SkipTracingInputDTO skipTracingInputDTO) throws NotFoundException, InvocationTargetException, IllegalAccessException;
  IntervinienteSkipTracingDTO updateIntervinienteST(Integer idIntervinienteST, IntervinienteSkipTracingInputDTO intervinienteSTDTO) throws Exception;
  SkipTracingOutputDTO asignarUsuario(Integer skipTracingId, Integer idUsuario) throws NotFoundException, InvocationTargetException, IllegalAccessException;

  ListWithCountDTO<SkipTracingOutputDTO> findAll(
      BusquedaDto busqueda,
      Usuario logged,
      String cliente,
      String cartera,
      String idExpediente,
      Integer idContrato,
      Integer idCarga,
      String producto,
      Integer nIntervinientes,
      Integer nGarantias,
      String estado,
      String nivel,
      String intervinientePrincipal,
      String tipoIntervencion,
      Boolean judicial,
      String provincia,
      String analistaST,
      String gestorExpediente,
      Double saldoGestion,
      String fechaSolicitud,
      String fechaAsignacion,
      String fechaInicioBusqueda,
      String fechaFinBusqueda,
      String primerTitular,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page)
      throws NotFoundException, InvocationTargetException, IllegalAccessException;

  ListWithCountDTO<SkipTracingOutputDTO> findByExpediente(Integer expedienteId,Integer size,Integer page) throws NotFoundException, InvocationTargetException, IllegalAccessException;
  SkipTracingOutputDTO editar(Integer skipTracingId, SkipTracingInputDTO skipTracingInputDTO, Usuario logged) throws NotFoundException, RequiredValueException, InvocationTargetException, IllegalAccessException;
  void comprobarCreados(Interviniente interviniente) throws ForbiddenException;
  IntervinienteSkipTracingDTO getInterviniente(Integer id) throws InvocationTargetException, IllegalAccessException, NotFoundException;
  void generarSkipTracing(Evento e) throws NotFoundException;
  LinkedHashMap<String, List<Collection<?>>> asignarNivel(List<Integer> idSTs, Integer idNivel, Integer idUsuario, Boolean todos, Integer empresa, Usuario loggedUser, BusquedaDto busqueda) throws Exception;
  IntervinienteSkipTracingDTO createIntervinienteST(Integer idSkipTracing, IntervinienteSkipTracingInputDTO intervinienteSkipTracingInputDTO, Usuario gestor) throws Exception;
  ListWithCountDTO<HistoricoSkipTracingDTO> getHistorico(Integer skipTracingId) throws Exception;

  DatoContactoSkipTracingDTO getDatoContactoST(Integer datoContactoSkipTracingId);
  DatoDireccionSkipTracingDTO getDatoDireccionST(Integer datoDireccionSkipTracingId);
  DatoContactoSkipTracingDTO updateDatoContactoST(Integer datoContactoSkipTracingId, DatoContactoSTInputDTO datoContactoSTInputDTO,List<MultipartFile>fotos,Usuario gestor) throws NotFoundException, IOException;
  DatoDireccionSkipTracingDTO updateDatoDireccionST(Integer datoDireccionSkipTracingId, DatoDireccionSTInputDTO datoDireccionSTInputDTO,List<MultipartFile>fotos, Usuario usuario) throws NotFoundException, IOException;

  DatoContactoSkipTracingDTO createDatoContactoST(Integer intervinienteSkiptracingId, DatoContactoSTInputDTO datoContactoSTInputDTO,List<MultipartFile> fotos, Usuario usuario) throws NotFoundException, IOException;
  DatoDireccionSkipTracingDTO createDatoDireccionST(Integer intervinienteSkiptracingId, DatoDireccionSTInputDTO datoDireccionSTInputDTO,List<MultipartFile> fotos, Usuario usuario) throws InvocationTargetException, IllegalAccessException, NotFoundException, IOException;

  void carga(CargaSTInputDTO input, MultipartFile file, Usuario user) throws Exception;
  LinkedHashMap<String, List<Collection<?>>> export(
    List<Integer> idSTs, Boolean todos, String empresa, Usuario loggedUser, BusquedaDto busqueda) throws Exception;
  Integer findCarga(List<Integer> sts);

  SkipTracingOutputDTO getById(Integer id) throws NotFoundException, InvocationTargetException, IllegalAccessException;

  DatoContactoSkipTracingDTO validarDC (Integer id, Boolean validado) throws NotFoundException;
  DatoDireccionSkipTracingDTO validarDD (Integer id, Boolean validado) throws NotFoundException;

  ListWithCountDTO<SkipTracingOutputDTO> getAllFilteredExpedientes(
    BusquedaDto busquedaDto, Usuario loggedUser, String cliente, String cartera, String idExpediente,
    Integer idContrato, Integer idCarga, String producto, Integer nIntervinientes, Integer nGarantias, String estado, String nivel,
    String intervinientePrincipal, String tipoIntervencion, Boolean judicial, String provincia, String analistaST,
    String gestorExpediente, Double saldoGestion, String orderField, String orderDirection, Integer size, Integer page)
    throws NotFoundException, InvocationTargetException, IllegalAccessException;
}
