package com.haya.alaska.skip_tracing.aplication;

import com.haya.alaska.asignacion_expediente.application.AsignacionExpedienteUseCase;
import com.haya.alaska.busqueda.domain.BusquedaReciente;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.repository.BusquedaGuardadaRepository;
import com.haya.alaska.busqueda.infrastructure.repository.BusquedaRecienteRepository;
import com.haya.alaska.carga_skip_tracing.domain.CargaST;
import com.haya.alaska.carga_skip_tracing.infrastructure.repository.CargaSTRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.dato_contacto_skip_tracing.domain.DatoContactoST;
import com.haya.alaska.dato_contacto_skip_tracing.infraestructure.repository.DatoContactoSTRepository;
import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import com.haya.alaska.dato_direccion_skip_tracing.infraestructure.repository.DatoDireccionSTRepository;
import com.haya.alaska.datos_origen.domain.DatosOrigen;
import com.haya.alaska.datos_origen.infrastructure.repository.DatosOrigenRepository;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.estado_skip_tracing.domain.EstadoSkipTracing;
import com.haya.alaska.estado_skip_tracing.infraestructure.repository.EstadoSkipTracingRepository;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.mapper.IntervinienteMapper;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.interviniente.infrastructure.util.IntervinienteUtil;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import com.haya.alaska.interviniente_skip_tracing.infraestructure.repository.IntervinienteSTRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.nivel_skip_tracing.domain.NivelSkipTracing;
import com.haya.alaska.nivel_skip_tracing.infraestructure.repository.NivelSkipTracingRepository;
import com.haya.alaska.origen_dato.domain.OrigenDato;
import com.haya.alaska.origen_dato.infrastructure.repository.OrigenDatoRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.*;
import com.haya.alaska.skip_tracing.domain.DetectysExcel;
import com.haya.alaska.skip_tracing.domain.IncofisaExcel;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.skip_tracing.infrastructure.controller.dto.*;
import com.haya.alaska.skip_tracing.infrastructure.mapper.SkipTracingMapper;
import com.haya.alaska.skip_tracing.infrastructure.repository.SkipTracingRepository;
import com.haya.alaska.skip_tracing.infrastructure.util.SkipTracingUtil;
import com.haya.alaska.tipo_busqueda.domain.TipoBusqueda;
import com.haya.alaska.tipo_busqueda.infraestructure.repository.TipoBusquedaRepository;
import com.haya.alaska.tipo_contacto.domain.TipoContacto;
import com.haya.alaska.tipo_contacto.infraestructure.repository.TipoContactoRepository;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ws.client.WebServiceIOException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SkipTracingUseCaseImpl implements SkipTracingUseCase {

  @Autowired
  private ServicioMaestro servicioMaestro;
  @Autowired ContratoRepository contratoRepository;
  @Autowired EstadoSkipTracingRepository estadoSkipTracingRepository;
  @Autowired NivelSkipTracingRepository nivelSkipTracingRepository;
  @Autowired UsuarioRepository usuarioRepository;
  @Autowired ProvinciaRepository provinciaRepository;
  @Autowired SkipTracingMapper skipTracingMapper;
  @Autowired SkipTracingRepository skipTracingRepository;
  @Autowired ContratoIntervinienteRepository contratoIntervinienteRepository;
  @Autowired IntervinienteMapper intervinienteMapper;
  @Autowired OrigenDatoRepository origenDatoRepository;
  @Autowired DatosOrigenRepository datosOrigenRepository;
  @Autowired IntervinienteSTRepository intervinienteSTRepository;
  @Autowired TipoBusquedaRepository tipoBusquedaRepository;
  @Autowired EventoRepository eventoRepository;
  @Autowired SkipTracingUtil skipTracingUtil;
  @Autowired DatoContactoSTRepository datoContactoSTRepository;
  @Autowired private BusquedaRecienteRepository busquedaRecienteRepository;
  @Autowired private BusquedaGuardadaRepository busquedaGuardadaRepository;
  /* @Autowired
  DatoContactoSTMapper datoContactoSTMapper;*/
  @Autowired DatoDireccionSTRepository datoDireccionSTRepository;
  /* @Autowired
  DatoDireccionSTMapper datoDireccionSTMapper;*/
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired DireccionRepository direccionRepository;
  @Autowired DatoContactoRepository datoContactoRepository;
  @Autowired TipoViaRepository tipoViaRepository;
  @Autowired CargaSTRepository cargaSTRepository;
  @Autowired TipoIntervencionRepository tipoIntervencionRepository;
  @Autowired
  AsignacionExpedienteUseCase asignacionExpedienteUseCase;
  @Autowired
  EventoUseCase eventoUseCase;
  @Autowired
  LocalidadRepository localidadRepository;
  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;
  @Autowired
  TipoContactoRepository tipoContactoRepository;
  @Autowired
  IntervinienteUtil intervinienteUtil;

  // TODO revisar de como realizar con la nueva relación contratoInterviniente
  @Override
  public SkipTracingOutputDTO nuevo(SkipTracingInputDTO skipTracingInputDTO) throws NotFoundException, InvocationTargetException, IllegalAccessException {

    // Este proceso llevarlo a SkiptracingMapper

    SkipTracing skipTracing = new SkipTracing();

    skipTracingMapper.parseToEntity(skipTracing, skipTracingInputDTO);

    ContratoInterviniente contratoInterviniente =
        contratoIntervinienteRepository
            .findById(skipTracingInputDTO.getContratoInterviniente())
            .orElse(null);

    // 1ºComprobar que para ese contratoInterviniente hay alguno de los 3 niveles skiptracing libre
    List<SkipTracing> skipTracingList =
        contratoInterviniente.getSkipTracing().stream().collect(Collectors.toList());
    // 2º Si solo existen 3 niveles de Skiptracing entonces podemos revisar que si hay menos de 3
    // activos
    skipTracingList.stream()
        .filter(
            skipTracing1 -> {
              if (skipTracingList.size() < 3 && skipTracing1.getActivo()) {
                skipTracingList.add(skipTracing1);
              }
              return false;
            })
        .collect(Collectors.toList());

    // 3º En caso de que sean menor de 3 los skiptracings activos añadiremos el nuevo que estamos
    // creando
    if (skipTracingList.size() < 3) {
      skipTracingRepository.save(skipTracing);
    }
    // 4ºGenerar el interviniente Skiptracing a partir del contratoInterviniente seleccionado
    // Sacar el contrato y a partir de el crear intervinienteST
    IntervinienteST intervinienteST =
        skipTracingMapper.parseToST(contratoInterviniente);
    intervinienteSTRepository.save(intervinienteST);

    // Proceso de generar evento
    SkipTracingOutputDTO skipTracingOutputDTO = skipTracingMapper.parseToOutput(skipTracing);
    return skipTracingOutputDTO;
  }

  @Override
  @Transactional
  public LinkedHashMap<String, List<Collection<?>>> asignarNivel(
      List<Integer> idSTs,
      Integer idNivel,
      Integer idUsuario,
      Boolean todos,
      Integer empresa,
      Usuario loggedUser,
      BusquedaDto busqueda)
      throws Exception {
    Integer usuarioLoggedId=loggedUser.getId();
    Usuario usuariologged=
      usuarioRepository
        .findById(usuarioLoggedId)
        .orElseThrow(
          () -> new NotFoundException("Usuario", usuarioLoggedId));
    NivelSkipTracing nivelST =
        nivelSkipTracingRepository
            .findById(idNivel)
            .orElseThrow(
                () -> new NotFoundException("NivelSkipTracing", idNivel));
    List<SkipTracing> skipTracings = new ArrayList<>();
    if (todos != null && todos) {
      skipTracings = skipTracingUtil.getDataExpedientesFilter(
        busqueda, loggedUser);
    } else {
      skipTracings = skipTracingRepository.findAllById(idSTs);
    }

    List<SkipTracing> stList = new ArrayList<>();
    LinkedHashMap<String, List<Collection<?>>> result = null;
    switch (nivelST.getCodigo()) {
      case ("01"):
        /*if (idUsuario == null)
          throw new RequiredValueException(
              "El idUsuario es obligatorio para SkipTracing de nivel 02");*/
      /*  Usuario gestor =
            usuarioRepository
                .findById(idUsuario)
                .orElseThrow(
                    () -> new NotFoundException("No encontrado Usuario con id: " + idUsuario));*/
        EstadoSkipTracing eST3 =
            estadoSkipTracingRepository
                .findByCodigo("03")
                .orElseThrow(
                    () ->
                        new NotFoundException("EstadoSkipTracing", "código", "'03'"));

        TipoBusqueda tbE = tipoBusquedaRepository.findByCodigo("EMP").orElseThrow(() ->
          new NotFoundException("TipoBusqueda", "código", "'EMP'"));

        CargaST carga = new CargaST();
        //carga.setSkipTracings(new HashSet<>(stList));
        carga.setFechaDescarga(new Date());
        if (empresa.equals(1)) {
          carga.setEncargado("Detectys");
        }else {
          carga.setEncargado("Incofisa");
         }
        carga.setValidado(true);
        carga.setFechaInput(new Date());
        carga.setFechaActualizacion(new Date());
        CargaST cargaSaved = cargaSTRepository.save(carga);
        for (SkipTracing st : skipTracings) {

          st.setTipoBusqueda(tbE);
          st.setFechaAsignacion(new Date());
          st.setEstadoST(eST3);
          st.setResponsableCartera(usuariologged);
          //st.setAnalistaST(gestor);
          SkipTracing stSaved;
          if (st.getNivelST() == null || !st.getNivelST().getId().equals(idNivel)){
            Integer id1 = st.getContratoInterviniente().getInterviniente().getId();
            List<SkipTracing> st1 = skipTracingRepository.
              findAllByContratoIntervinienteIntervinienteIdAndNivelSTIdAndActivoIsTrue(id1, nivelST.getId());
            if (!st1.isEmpty()){
              Locale loc = LocaleContextHolder.getLocale();
              if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                throw new InvalidCodeException("The Intervener of id: " + id1 + " is already the main participant of an open SkipTracing level 1.");
              else throw new InvalidCodeException("El Interviniente de id: " + id1 + " ya es el interviniente principal de un SkipTracing de nivel 1 abierto.");
            }
            st.setNivelST(nivelST);
            st.setCargaST(cargaSaved);
            stSaved = st;
          } else {
            st.setNivelST(nivelST);
            st.setCargaST(cargaSaved);
            stSaved = skipTracingRepository.save(st);
          }
          stList.add(stSaved);
        }
        result = crearExcel(empresa, stList);
        break;
      case ("02"):
        if (idUsuario == null){
          Locale loc = LocaleContextHolder.getLocale();
          if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
            throw new RequiredValueException("The userid is mandatory for SkipTracing level 02");
          else throw new RequiredValueException("El idUsuario es obligatorio para SkipTracing de nivel 02");
        }
        Usuario user =
          usuarioRepository
            .findById(idUsuario)
            .orElseThrow(
              () -> new NotFoundException("Usuario", idUsuario));
        EstadoSkipTracing eST8 =
          estadoSkipTracingRepository
            .findByCodigo("02")
            .orElseThrow(
              () ->
                new NotFoundException("EstadoSkipTracing", "código", "'02'"));
        TipoBusqueda tb = tipoBusquedaRepository.findByCodigo("GES").orElseThrow(() ->
          new NotFoundException("TipoBusqueda", "código", "'GES'"));
        for (SkipTracing st : skipTracings) {
          st.setTipoBusqueda(tb);
          st.setFechaAsignacion(new Date());
          st.setAnalistaST(user);
          st.setEstadoST(eST8);
          st.setResponsableCartera(usuariologged);
          SkipTracing stSaved;
          asignarGestorST(st, user, loggedUser);
          if (st.getNivelST() == null || !st.getNivelST().getId().equals(idNivel)){
            Integer id2 = st.getContratoInterviniente().getInterviniente().getId();
            List<SkipTracing> st2 = skipTracingRepository.
              findAllByContratoIntervinienteIntervinienteIdAndNivelSTIdAndActivoIsTrue(id2, nivelST.getId());
            if (!st2.isEmpty()){
              Locale loc = LocaleContextHolder.getLocale();
              if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                throw new InvalidCodeException("The Intervener of id: " + id2 + " is already the main participant of an open SkipTracing level 2.");
              else throw new InvalidCodeException("El Interviniente de id: " + id2 + " ya es el interviniente principal de un SkipTracing de nivel 2 abierto.");
            }
            st.setNivelST(nivelST);
            stSaved = st;
          } else {
            st.setNivelST(nivelST);
            stSaved = skipTracingRepository.save(st);
          }
          stList.add(stSaved);
          eventoUseCase.eventoCierreST(stSaved, loggedUser, user);
        }

        result = crearExcel(1, stList);
        break;
      case ("03"):
        if (idUsuario == null){
          Locale loc = LocaleContextHolder.getLocale();
          if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
            throw new RequiredValueException("The userid is mandatory for SkipTracing level 03");
          else throw new RequiredValueException("El idUsuario es obligatorio para SkipTracing de nivel 03");
        }
        Usuario user2 =
            usuarioRepository
                .findById(idUsuario)
                .orElseThrow(
                    () -> new NotFoundException("Usuario", idUsuario));
        EstadoSkipTracing eST2 =
            estadoSkipTracingRepository
                .findByCodigo("08")
                .orElseThrow(
                    () ->
                        new NotFoundException("EstadoSkipTracing", "código", "'08'"));
        TipoBusqueda tb2 = tipoBusquedaRepository.findByCodigo("GES").orElseThrow(() ->
              new NotFoundException("TipoBusqueda", "código", "'GES'"));
        for (SkipTracing st : skipTracings) {
          st.setTipoBusqueda(tb2);
          st.setFechaAsignacion(new Date());
          st.setAnalistaST(user2);
          st.setEstadoST(eST2);
          st.setResponsableCartera(usuariologged);
          SkipTracing stSaved;
          if (st.getNivelST() == null || !st.getNivelST().getId().equals(idNivel)){
            Integer id3 = st.getContratoInterviniente().getInterviniente().getId();
            List<SkipTracing> st3 = skipTracingRepository.
              findAllByContratoIntervinienteIntervinienteIdAndNivelSTIdAndActivoIsTrue(id3, nivelST.getId());
            if (!st3.isEmpty()){
              Locale loc = LocaleContextHolder.getLocale();
              if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                throw new InvalidCodeException("The Intervener of id: " + id3 + " is already the main participant of an open SkipTracing level 3.");
              else throw new InvalidCodeException("El Interviniente de id: " + id3 + " ya es el interviniente principal de un SkipTracing de nivel 3 abierto.");
            }
            st.setNivelST(nivelST);
            stSaved = st;
          } else {
            st.setNivelST(nivelST);
            stSaved = skipTracingRepository.save(st);
          }
          stList.add(stSaved);
          eventoUseCase.eventoCierreST(stSaved, loggedUser, user2);
        }

        result = crearExcel(1, stList);
        break;
    }

    return result;
  }

  private void asignarGestorST(SkipTracing st, Usuario gestor, Usuario logged) throws Exception {
    if (st != null){
      ContratoInterviniente ci = st.getContratoInterviniente();
      if (ci != null){
        Contrato c = ci.getContrato();
        if (c != null){
          Expediente e = c.getExpediente();
          if (e != null){
            List<Integer> lista = new ArrayList<>();
            lista.add(e.getId());
            asignacionExpedienteUseCase.modificarGestor(lista, new ArrayList<>(), gestor.getId(), false, false, false, new BusquedaDto(), logged);
          }
        }
      }
    }
  }

  @Override
  public IntervinienteSkipTracingDTO createIntervinienteST(Integer idSkipTracing, IntervinienteSkipTracingInputDTO input, Usuario logged) throws Exception {
    Usuario gestor = usuarioRepository.findById(logged.getId()).orElseThrow(
      () -> new NotFoundException("Usuario", logged.getId()));
    //crear IntervinienteSkipTracing
    SkipTracing skipTracing = skipTracingRepository.findById(idSkipTracing).orElseThrow(
        () -> new NotFoundException("SkipTracing", idSkipTracing));

    if (input.getTipoIntervencion() == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("When creating a new participant, the Type of Intervention is mandatory.");
      else throw new RequiredValueException("Al crear un nuevo interviniente, el Tipo de Intervención es obligatorio.");
    }
    TipoIntervencion ti = tipoIntervencionRepository.findById(input.getTipoIntervencion()).orElseThrow(
      ()->new NotFoundException("tipo intervención", input.getTipoIntervencion()));
    if (input.getOrdenIntervencion() == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("When creating a new participant, the Type of Intervention is mandatory.");
      else throw new RequiredValueException("Al crear un nuevo interviniente, el Tipo de Intervención es obligatorio.");
    }

    Contrato c = skipTracing.getContratoInterviniente().getContrato();
    ContratoInterviniente ciOld = contratoIntervinienteRepository.findByContratoIdAndOrdenIntervencionAndTipoIntervencionId(
      c.getId(), input.getOrdenIntervencion(), input.getTipoIntervencion()).orElse(null);

    if (ciOld != null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("There is already a participant with the intervention type:" + ti.getValor () + ", and intervention order:" + input.getOrdenIntervencion () + ", for the contract:" + c.getId ());
      else throw new RequiredValueException("Ya existe un interviniente con el tipo intervención: " + ti.getValor() + ", y orden intervención: " + input.getOrdenIntervencion() + ", para el contrato: " + c.getId());
    }

    IntervinienteST intervinienteSTgenerado = skipTracingMapper.parseToEntity(new IntervinienteST(), input);
    intervinienteSTgenerado.getSkipTracingSet().add(skipTracing);
    IntervinienteST iSaved = intervinienteSTRepository.save(intervinienteSTgenerado);
    // Crear datosContactoST
    if (input.getDatosContactoST() != null) {
      List<DatoContactoSTInputDTO> datosContactoSkipTracingDTOToLists =
        new ArrayList<>(input.getDatosContactoST());

      if (!datosContactoSkipTracingDTOToLists.isEmpty()) {
        for (DatoContactoSTInputDTO dcInput : datosContactoSkipTracingDTOToLists){
          DatoContactoST dc = new DatoContactoST();
          dc = skipTracingMapper.parseToEntity(dc, dcInput, gestor,intervinienteSTgenerado.getId());
          dc.setIntervinienteST(iSaved);
          datoContactoSTRepository.save(dc);
        }
      }
    }
    // CrearDireccionesST
    if (input.getDireccionesST() != null) {
      List<DatoDireccionSTInputDTO> direccionesSTDTO =
        new ArrayList<>(input.getDireccionesST());
      if (direccionesSTDTO != null && !direccionesSTDTO.isEmpty()) {
        for (DatoDireccionSTInputDTO dcInput : direccionesSTDTO){
          DatoDireccionST dc = new DatoDireccionST();
          dc.setIntervinienteST(iSaved);
          dc = skipTracingMapper.parseToEntity(dc, dcInput, gestor);
          datoDireccionSTRepository.save(dc);
        }
      }
    }

    IntervinienteST result = intervinienteSTRepository.findById(iSaved.getId()).orElseThrow(
      () -> new NotFoundException("IntervinienteST", iSaved.getId()));
    //Mapper para cambiar el intervinienteST a la respuesta
    IntervinienteSkipTracingDTO intervinienteSkipTracingDTO=
      skipTracingMapper.parseToOutput(result);

    return intervinienteSkipTracingDTO;
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public IntervinienteSkipTracingDTO updateIntervinienteST(
    Integer idIntervinienteST, IntervinienteSkipTracingInputDTO intervinienteSTDTO)
    throws Exception {

    IntervinienteST intervinienteST =
      intervinienteSTRepository
        .findById(idIntervinienteST)
        .orElseThrow(
          () ->
            new NotFoundException(
              "Interviniente", idIntervinienteST));
    intervinienteST = skipTracingUtil.actualizarCampos(intervinienteST, intervinienteSTDTO);

    IntervinienteST intervinienteSTActualizado = intervinienteSTRepository.saveAndFlush(intervinienteST);

    return skipTracingMapper.parseToOutput(
      intervinienteSTRepository
        .findById(intervinienteSTActualizado.getId())
        .orElseThrow(
          () ->
            new NotFoundException(
              "objeto", intervinienteSTActualizado.getId())));

  }

  public ListWithCountDTO<SkipTracingOutputDTO> getAllFilteredExpedientes(
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      String cliente,
      String cartera,
      String idExpediente,
      Integer idContrato,
      Integer idCarga,
      String producto,
      Integer nIntervinientes,
      Integer nGarantias,
      String estado,
      String nivel,
      String intervinientePrincipal,
      String tipoIntervencion,
      Boolean judicial,
      String provincia,
      String analistaST,
      String gestorExpediente,
      Double saldoGestion,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page)
    throws NotFoundException, InvocationTargetException, IllegalAccessException {

    List<SkipTracing> expedientesSinPaginar =
        skipTracingUtil.getDataExpedientesFilter(
            busquedaDto, loggedUser);

    if (page == 0) {
      BusquedaReciente busquedaReciente =
          new BusquedaReciente(loggedUser, busquedaDto, expedientesSinPaginar.size());
      busquedaRecienteRepository.save(busquedaReciente);
    }

    List<SkipTracingOutputDTO> expedientesDTOs = new ArrayList<>();
    for (SkipTracing st : expedientesSinPaginar){
      expedientesDTOs.add(skipTracingMapper.parseToOutput(st));
    }

    // Primero filtra la lista y después la ordena, añadiendo la paginación con el skip
    expedientesDTOs =
        skipTracingUtil.filtrarListaST(
            expedientesDTOs,
            cliente,
            cartera,
            idExpediente,
            idContrato,
            idCarga,
            producto,
            nIntervinientes,
            nGarantias,
            estado,
            nivel,
            intervinientePrincipal,
            tipoIntervencion,
            judicial,
            provincia,
            analistaST,
            gestorExpediente);
    expedientesDTOs =
        skipTracingUtil.ordenarListaST(expedientesDTOs, orderField, orderDirection).stream()
            .skip(size * page)
            .limit(size)
            .collect(Collectors.toList());
    return new ListWithCountDTO<>(expedientesDTOs, expedientesSinPaginar.size());
  }

  private SkipTracing RAtoST(SkipTracing st) throws NotFoundException {
    // sacar los intervinientes del contrato
    ContratoInterviniente contratoInterviniente = st.getContratoInterviniente();
    Contrato contrato = contratoInterviniente.getContrato();
    var contratosIntervinientes = contrato.getIntervinientes();

    List<Interviniente> intervinientes = new ArrayList<>();
    for (var contratoInterv : contratosIntervinientes) {
      intervinientes.add(contratoInterv.getInterviniente());
    }

    List<IntervinienteST> enST = new ArrayList<>();

    for (ContratoInterviniente ci : contratosIntervinientes) {
      Interviniente interviniente = ci.getInterviniente();
      IntervinienteST intervinienteST = skipTracingMapper.parseToST(ci);

      Set<DatoContacto> datosContacto = interviniente.getDatosContacto();
      Set<Direccion> direcciones = interviniente.getDirecciones();

      Set<DatoContactoST> datosContactoST = new HashSet<>();
      Set<DatoDireccionST> datosDireccionST = new HashSet<>();

      for (var datoContacto : datosContacto) {
        List<DatoContactoST> datoST = skipTracingMapper.parseToST(datoContacto);
        datosContactoST.addAll(datoST);
      }
      for (var datoDireccion : direcciones) {
        DatoDireccionST datoST = skipTracingMapper.parseToST(datoDireccion,contrato);
        datosDireccionST.add(datoST);
      }

      List<DatoContactoST> reaultCont = datoContactoSTRepository.saveAll(datosContactoST);
      List<DatoDireccionST> reaultDire = datoDireccionSTRepository.saveAll(datosDireccionST);

      intervinienteST.setDatosContactoST(new HashSet<>(reaultCont));
      intervinienteST.setDatosDireccionST(new HashSet<>(reaultDire));

      enST.add(intervinienteSTRepository.save(intervinienteST));
    }
    st.setIntervinientesST(new HashSet<>(enST));

    return skipTracingRepository.save(st);
  }

  @Override
  public SkipTracingOutputDTO asignarUsuario(Integer skipTracingId, Integer idUsuario)
    throws NotFoundException, InvocationTargetException, IllegalAccessException {

    // -el front pasa skiptracing y el usuario y tenemos que coger al usuario y asignarlo como
    // analista skiptracing
    Usuario usuario =
        usuarioRepository
            .findById(idUsuario)
            .orElseThrow(() -> new NotFoundException("Usuario", idUsuario));
    SkipTracing skipTracing =
        skipTracingRepository
            .findById(skipTracingId)
            .orElseThrow(
                () -> new NotFoundException("SkipTracing", skipTracingId));

    skipTracing.setAnalistaST(usuario);

    SkipTracing stSaved = skipTracingRepository.save(skipTracing);

    SkipTracingOutputDTO skipTracingOutputDTO = skipTracingMapper.parseToOutput(stSaved);
    return skipTracingOutputDTO;
  }

  @Override
  public ListWithCountDTO<SkipTracingOutputDTO> findAll(
    BusquedaDto busqueda,
      Usuario looged,
      String cliente,
      String cartera,
      String idExpediente,
      Integer idContrato,
      Integer idCarga,
      String producto,
      Integer nIntervinientes,
      Integer nGarantias,
      String estado,
      String nivel,
      String intervinientePrincipal,
      String tipoIntervencion,
      Boolean judicial,
      String provincia,
      String analistaST,
      String gestorExpediente,
      Double saldoGestion,
      String fechaSolicitud,
      String fechaAsignacion,
      String fechaInicioBusqueda,
      String primerTitular,
      String fechaFinBusqueda,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page) throws NotFoundException, InvocationTargetException, IllegalAccessException {
    List<SkipTracing> listaBusqueda = new ArrayList<>();

    List<SkipTracing> skipTracingList;

    List<SkipTracing>  listaFiltro = skipTracingUtil.filter(
      looged, cliente, cartera, idExpediente, idContrato, idCarga, producto, nIntervinientes, nGarantias, estado, nivel,
      intervinientePrincipal, tipoIntervencion, judicial, provincia, analistaST, gestorExpediente, saldoGestion,fechaSolicitud,fechaAsignacion,
      fechaInicioBusqueda,fechaFinBusqueda, orderField, orderDirection);

    if(analistaST != null) {
      listaFiltro = skipTracingUtil.filterUserAndAgency(listaFiltro, analistaST);
    }


    if (busqueda != null){
      listaBusqueda = skipTracingUtil.getDataExpedientesFilter(busqueda, looged);
      skipTracingList = filtrarEnMemoria(listaBusqueda, listaFiltro);
    } else {
      skipTracingList = new ArrayList<>(listaFiltro);
    }

    List<SkipTracingOutputDTO> skipTracingOutputDTOList = new ArrayList<>();
    for (SkipTracing skiptracing : skipTracingList) {
      SkipTracingOutputDTO skipTracingOutputDTO = skipTracingMapper.parseToOutput(skiptracing);
      skipTracingOutputDTOList.add(skipTracingOutputDTO);
    }

    if (primerTitular!=null){
      skipTracingOutputDTOList = skipTracingOutputDTOList.stream().filter(c -> c.getIntervinientePrincipal().getPrimerTitular().toLowerCase().contains(primerTitular)).collect(Collectors.toList());
    }


    skipTracingOutputDTOList =
      skipTracingUtil.ordenarListaST(skipTracingOutputDTOList, orderField, orderDirection).stream()
        .skip(size * page)
        .limit(size)
        .collect(Collectors.toList());

    return new ListWithCountDTO<>(skipTracingOutputDTOList, skipTracingList.size());
  }

  public LinkedHashMap<String, List<Collection<?>>> export(
    List<Integer> idSTs, Boolean todos, String empresa, Usuario loggedUser, BusquedaDto busqueda) throws Exception {
    List<SkipTracing> skipTracings = new ArrayList<>();
    if (todos != null && todos) {
      skipTracings = skipTracingUtil.getDataExpedientesFilter(
        busqueda, loggedUser);
    } else {
      skipTracings = skipTracingRepository.findAllById(idSTs);
    }

    /*CargaST carga = new CargaST();
    carga.setSkipTracings(new HashSet<>(skipTracings));
    carga.setFechaDescarga(new Date());
    cargaSTRepository.save(carga);*/

    Integer modelo;
    if (empresa.equals("Detectys")) modelo = 1;
    else modelo = 2;

    return crearExcel(modelo, skipTracings);
  }

  private List<SkipTracing> filtrarEnMemoria(List<SkipTracing> listaBusqueda, List<SkipTracing> listaFiltro){
    List<SkipTracing> result = new ArrayList<>();
    for (SkipTracing st : listaBusqueda){
      if (listaFiltro.contains(st)) result.add(st);
    }
    return result;
  }

  private List<CargaST> filtrarCargasEnMemoria(List<CargaST> listaBusqueda, List<CargaST> listaFiltro){
    List<CargaST> result = new ArrayList<>();
    for (CargaST st : listaBusqueda){
      if (listaFiltro.contains(st)) result.add(st);
    }
    return result;
  }

  // TODO

  @Override
  public ListWithCountDTO<SkipTracingOutputDTO> findByExpediente(
      Integer expedienteId, Integer size, Integer page) throws NotFoundException, InvocationTargetException, IllegalAccessException {

    List<SkipTracing> skipTracingList =
        skipTracingRepository.findAllByContratoIntervinienteContratoExpedienteId(expedienteId);
    List<SkipTracingOutputDTO> skipTracingOutputDTOList = new ArrayList<>();
    for (SkipTracing skiptracing : skipTracingList) {
      SkipTracingOutputDTO skipTracingOutputDTO = skipTracingMapper.parseToOutput(skiptracing);
      skipTracingOutputDTOList.add(skipTracingOutputDTO);
    }

    skipTracingOutputDTOList =
        skipTracingOutputDTOList.stream()
            .skip(size * page)
            .limit(size)
            .collect(Collectors.toList());
    return new ListWithCountDTO<>(skipTracingOutputDTOList, skipTracingList.size());
  }

  @Override
  public SkipTracingOutputDTO editar(Integer skipTracingId, SkipTracingInputDTO skipTracingInputDTO, Usuario logged)
    throws NotFoundException, InvocationTargetException, IllegalAccessException, RequiredValueException {

    SkipTracing st = skipTracingRepository.findById(skipTracingId).orElseThrow(() ->
      new NotFoundException("SkipTracing", skipTracingId));

    SkipTracing skipTracing =
        skipTracingMapper.parseToEntity(st, skipTracingInputDTO);

    skipTracing = skipTracingRepository.save(skipTracing);

    EstadoSkipTracing eST = skipTracing.getEstadoST();
    if (eST != null){
      String cod = eST.getCodigo();
      if (getCierres().contains(cod))
        cerrar(skipTracing, logged);
    }
    SkipTracingOutputDTO skipTracingOutputDTO = skipTracingMapper.parseToOutput(skipTracing);

    return skipTracingOutputDTO;
  }

  public void comprobarCreados(Interviniente interviniente) throws ForbiddenException {
    List<SkipTracing> activos =
        skipTracingRepository.findAllByContratoIntervinienteIntervinienteIdAndActivoIsTrue(
            interviniente.getId());
    if (activos.size() == 3){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new ForbiddenException("This participant already has 3 active SkipTracings");
      else throw new ForbiddenException("Este interviniente ya tiene 3 SkipTracings activos");
    }
    for (ContratoInterviniente ci : interviniente.getContratos()) {
      List<Evento> eventos =
          eventoRepository.findAllByNivelCodigoAndResultadoIsNullAndIdNivel("CON_INT", ci.getId());
      if (!eventos.isEmpty()){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new ForbiddenException("There is already a SkipTracing request for the caller of id: " + interviniente.getId());
        else throw new ForbiddenException("Ya existe una solicitud de SkipTracing para el interviniente de id: " + interviniente.getId());
      }
    }
  }

  public IntervinienteSkipTracingDTO getInterviniente(Integer id) throws InvocationTargetException, IllegalAccessException, NotFoundException {
    IntervinienteST i = intervinienteSTRepository.findById(id).orElseThrow(() ->
        new NotFoundException("IntervinienteSkipTracing", id));
    IntervinienteSkipTracingDTO result = skipTracingMapper.parseToOutput(i);
    return result;
  }

  public void generarSkipTracing(Evento e) throws NotFoundException {
    ContratoInterviniente ci =
        contratoIntervinienteRepository
            .findById(e.getIdNivel())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "ContratoInterviniente", e.getIdNivel()));
    EstadoSkipTracing eST =
        estadoSkipTracingRepository
            .findByCodigo("01")
            .orElseThrow(
                () -> new NotFoundException("EstadoSkipTracing", "código", "'01'"));

    SkipTracing sk = new SkipTracing();
    sk.setContratoInterviniente(ci);
    sk.setFechaSolicitud(e.getFechaCreacion());
    sk.setEstadoST(eST);
    SkipTracing stSaved = skipTracingRepository.save(sk);
    RAtoST(stSaved);
  }

  private List<IntervinienteST> cerrar(SkipTracing skipTracing, Usuario loggedUser) throws NotFoundException, RequiredValueException{
    skipTracing.setFechaFinBusqueda(new Date());
    if (skipTracing.getNivelST() != null && skipTracing.getNivelST().getCodigo().equals("01")){
      Calendar c = Calendar.getInstance();
      c.setTime(new Date());
      c.add(Calendar.DATE, -3);
      skipTracing.setFechaInicioBusqueda(c.getTime());
    }
    skipTracing.setActivo(false);

    eventoUseCase.eventoCierreST(skipTracing, loggedUser, null);
    Contrato contrato = skipTracing.getContratoInterviniente().getContrato();

    // En función del estado se cierra el skiptracing
    List<IntervinienteST> intervinientesSTlist = new ArrayList<>(skipTracing.getIntervinientesST());

    // Pasamos los intervinientes de skiptracing y los guardamos en los de recuperacion amistosa
    // cuando se cierre el skiptracing
    // Por cada datoContactoSkipTracing del intervinienteST debo generar un nuevo datocontacto

    for (IntervinienteST intervinienteST : intervinientesSTlist) {
      Interviniente interviniente = intervinienteST.getInterviniente();
      if (interviniente == null){
        if (intervinienteST.getPersonaJuridica()){
          if (intervinienteST.getRazonSocial() != null)
            interviniente = intervinienteRepository.findByRazonSocial(intervinienteST.getRazonSocial()).orElse(null);
        } else {
          if (intervinienteST.getNumeroDocumento() != null)
            interviniente = intervinienteRepository.findByNumeroDocumento(intervinienteST.getNumeroDocumento()).orElse(null);
        }
      }

      if (interviniente == null){
        interviniente = new Interviniente();
        //llamar maestro
        cerrarInterviniente(interviniente, intervinienteST);
        interviniente = intervinienteRepository.save(interviniente);
        intervinienteST.setInterviniente(interviniente);
        intervinienteSTRepository.save(intervinienteST);
        ClienteCartera cc = getCC(contrato);
      //  if (cc != null) registrarMasterIntervinienteST(interviniente, cc.getCliente().getNombre(), intervinienteST.getTipoIntervencion());
        ContratoInterviniente ci = new ContratoInterviniente();
        ci.setTipoIntervencion(intervinienteST.getTipoIntervencion());
        ci.setOrdenIntervencion(intervinienteST.getOrdenIntervencion());
        ci.setInterviniente(interviniente);
        ci.setContrato(contrato);
        ci.getSkipTracing().add(skipTracing);
        contratoIntervinienteRepository.save(ci);
      } else {
        cerrarInterviniente(interviniente, intervinienteST);
        interviniente = intervinienteRepository.save(interviniente);
      }

      Set<DatoDireccionST> direccionesST = intervinienteST.getDatosDireccionST();
      for (DatoDireccionST dirST : direccionesST){
        Direccion direccion = dirST.getDatoDireccion();
        if (direccion == null){
          direccion = new Direccion();
          cerrarDireccion(direccion, dirST);
          intervinienteUtil.direccionGoogle(direccion);
          direccion = direccionRepository.save(direccion);
          interviniente.getDirecciones().add(direccion);
          dirST.setDatoDireccion(direccion);
          datoDireccionSTRepository.save(dirST);
        } else {
          cerrarDireccion(direccion, dirST);
          intervinienteUtil.direccionGoogle(direccion);
          direccion = direccionRepository.save(direccion);
        }
      }

      Set<DatoContactoST> contactosST = intervinienteST.getDatosContactoST();
      for (DatoContactoST dcST : contactosST){
        DatoContacto dc = dcST.getDatoContacto();
        if (dc == null){
          dc = new DatoContacto();
          cerrarContacto(dc, dcST);
          dc = datoContactoRepository.save(dc);
        //Al datoContactoST que se va a volcar sobre RA
          dcST.setDatoContacto(dc);
          datoContactoSTRepository.save(dcST);
          interviniente.getDatosContacto().add(dc);
        } else {
          cerrarContacto(dc, dcST);
          dc = datoContactoRepository.save(dc);
        }
      }
      intervinienteRepository.save(interviniente);
    }
    return intervinientesSTlist;
  }

  private ClienteCartera getCC(Contrato contrato){
    ClienteCartera cc = null;

    if (contrato != null){
      Expediente e = contrato.getExpediente();
      if (e != null){
        Cartera c = e.getCartera();
        if (c != null){
          List<ClienteCartera> ccList = new ArrayList<>(c.getClientes());
          if (!ccList.isEmpty()) cc = ccList.get(0);
        }
      }
    }

    return cc;
  }

  private List<String> getCierres(){
    List<String> result = new ArrayList<>();

    result.add("04");
    result.add("05");
    result.add("06");
    result.add("07");

    return result;
  }

  private void cerrarInterviniente(Interviniente interviniente, IntervinienteST intervinienteST){
    if (interviniente.getTipoDocumento() == null)
      interviniente.setTipoDocumento(intervinienteST.getTipoDocumento());
    if (interviniente.getNumeroDocumento() == null)
      interviniente.setNumeroDocumento(intervinienteST.getNumeroDocumento());
    if (interviniente.getPersonaJuridica() == null)
      interviniente.setPersonaJuridica(intervinienteST.getPersonaJuridica());
    if (interviniente.getEstadoCivil() == null)
      interviniente.setEstadoCivil(intervinienteST.getEstadoCivil());
    if (interviniente.getResidencia() == null)
      interviniente.setResidencia(intervinienteST.getResidencia());
    if (interviniente.getNacionalidad() == null)
      interviniente.setNacionalidad(intervinienteST.getNacionalidad());
    if (interviniente.getPaisNacimiento() == null)
      interviniente.setPaisNacimiento(intervinienteST.getPaisNacimiento());
    if (interviniente.getNombre() == null)
      interviniente.setNombre(intervinienteST.getNombre());
    if (interviniente.getApellidos() == null)
      interviniente.setApellidos(intervinienteST.getApellidos());
    if (interviniente.getSexo() == null)
      interviniente.setSexo(intervinienteST.getSexo());
    if (interviniente.getFechaNacimiento() == null)
      interviniente.setFechaNacimiento(intervinienteST.getFechaNacimiento());
    if (interviniente.getContactado() == null)
      interviniente.setContactado(intervinienteST.getContactado());
    //TipoContacto
  }

  private Interviniente registrarMasterIntervinienteST(Interviniente interviniente, String cliente, TipoIntervencion tipoIntervencion) {
    // Llamada al servicio de maestros para obtener el idHaya del contrato
    try {
      servicioMaestro.registrarInterviniente(interviniente, cliente, tipoIntervencion);
    } catch (NotFoundException | WebServiceIOException | LockedChangeException e) {
      log.warn(e.getMessage());
      interviniente.setIdHaya(null);
    }
    log.debug(String.format("Interviniente.idHaya -> %s", interviniente.getId()));
    return interviniente;
  }

  private void cerrarDireccion(Direccion direccion, DatoDireccionST direccionST){
    direccion.setFechaActualizacion(new Date());
    if (direccion.getValidada() == null)
      direccion.setValidada(direccionST.getValidado());
    if (direccion.getTipoVia() == null)
      direccion.setTipoVia(direccionST.getTipoVia());
    if (direccion.getBloque() == null)
      direccion.setBloque(direccionST.getBloque());
    if (direccion.getEscalera() == null)
      direccion.setEscalera(direccionST.getEscalera());
    if (direccion.getPiso() == null)
      direccion.setPiso(direccionST.getPiso());
    if (direccion.getPuerta() == null)
      direccion.setPuerta(direccionST.getPuerta());
    if (direccion.getEntorno() == null)
      direccion.setEntorno(direccionST.getEntorno());
    if (direccion.getMunicipio() == null)
      direccion.setMunicipio(direccionST.getMunicipio());
    if (direccion.getLocalidad() == null)
      direccion.setLocalidad(direccionST.getLocalidad());
    if (direccion.getProvincia() == null)
      direccion.setProvincia(direccionST.getProvincia());
    if (direccion.getComentario() == null)
      direccion.setComentario(direccionST.getComentario());
    if (direccion.getCodigoPostal() == null)
      direccion.setCodigoPostal(direccionST.getCodigoPostal());
    if (direccion.getPais() == null)
      direccion.setPais(direccionST.getPais());
    if (direccion.getLongitud() == null)
      direccion.setLongitud(direccionST.getLongitud());
    if (direccion.getLatitud() == null)
      direccion.setLatitud(direccionST.getLatitud());
    if (direccion.getNombre()==null)
      direccion.setNombre(direccionST.getNombre());
    if (direccion.getOrigen() == null){
      direccion.setOrigen("Skip Tracing");
      /*if (direccionST.getDatosOrigen() != null && direccionST.getDatosOrigen().getOrigenDato() != null)
        direccion.setOrigen(direccionST.getDatosOrigen().getOrigenDato().getValor());*/
    }
    if (direccion.getFechaAlta() == null){
      if (direccionST.getDatosOrigen() != null)
        direccion.setFechaAlta(direccionST.getDatosOrigen().getFecha());
      else
        direccion.setFechaAlta(new Date());
    }

    direccion.setFechaActualizacion(new Date());
  }

  private void cerrarContacto(DatoContacto dc, DatoContactoST dcST){
    TipoContacto tc = dcST.getTipoContacto();
    if (dc.getOrden() == null){
      if (dcST.getPrincipal()!=null && dcST.getPrincipal()) dc.setOrden(1);
      else dc.setOrden(2);
    }
    dc.setFechaActualizacion(dcST.getDatosOrigen().getFechaActualizacion());
    dc.setFechaAlta(dcST.getDatosOrigen().getFecha());
    if (dc.getId() == null) dc.setOrigen("Skip Tracing");

    if (tc != null){
      switch (tc.getCodigo()){
        case ("TEL_FIJ"):
          if (dc.getFijo() == null)
            dc.setFijo(dcST.getValor());
          if (dcST.getValidado() != null)
            dc.setFijoValidado(dcST.getValidado());
          break;
        case ("TEL_MOV"):
          if (dc.getMovil() == null)
            dc.setMovil(dcST.getValor());
          if (dcST.getValidado() != null)
            dc.setMovilValidado(dcST.getValidado());
          break;
        case ("MAIL"):
          if (dc.getEmail() == null)
            dc.setEmail(dcST.getValor());
          if (dcST.getValidado() != null)
            dc.setEmailValidado(dcST.getValidado());
          break;
      }
    }
  }

  private LinkedHashMap<String, List<Collection<?>>> crearExcel(
      Integer modelo, List<SkipTracing> skipTracings) throws Exception {
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();
    if (modelo.equals(1)) {
      List<Collection<?>> detectys = new ArrayList<>();
      detectys.add(DetectysExcel.cabeceras);
      for (SkipTracing st : skipTracings) {
        Interviniente i = st.getContratoInterviniente().getInterviniente();
        IntervinienteST iST = intervinienteSTRepository.findByIntervinienteId(i.getId()).orElse(null);
        if (iST != null)
          detectys.add(new DetectysExcel(iST).getValuesList());
      }
      datos.put("Detectys", detectys);
    } else if (modelo.equals(2)) {
      List<Collection<?>> incofisa = new ArrayList<>();
      incofisa.add(IncofisaExcel.cabeceras);
      for (SkipTracing st : skipTracings) {
        Interviniente i = st.getContratoInterviniente().getInterviniente();
        IntervinienteST iST = intervinienteSTRepository.findByIntervinienteId(i.getId()).orElse(null);
        if (iST != null)
          incofisa.add(new IncofisaExcel(st, iST).getValuesList());
      }
      datos.put("Incofisa", incofisa);
    } else {
      throw new Exception("No se encuentra el modelo seleccionado");
    }
    return datos;
  }

  @Override
  public ListWithCountDTO<HistoricoSkipTracingDTO> getHistorico(Integer skipTracingId)
      throws Exception {
    skipTracingRepository
        .findById(skipTracingId)
        .orElseThrow(() -> new Exception("Skip Tracing no encontrado con el id: " + skipTracingId));
    List<Map> maps = skipTracingRepository.historico(skipTracingId);
    List<HistoricoSkipTracingDTO> historicoSkipTracingDTOS = new ArrayList<>();
    maps.forEach(map -> historicoSkipTracingDTOS.add(skipTracingMapper.parseMap(map)));
    return new ListWithCountDTO<>(historicoSkipTracingDTOS, historicoSkipTracingDTOS.size());
  }

  @Override
  public DatoContactoSkipTracingDTO getDatoContactoST(Integer datoContactoSkipTracingId) {
    var datoContacto = datoContactoSTRepository.findById(datoContactoSkipTracingId).orElse(null);
    return skipTracingMapper.parseToOutput(datoContacto);
  }

  @Override
  public DatoDireccionSkipTracingDTO getDatoDireccionST(Integer datoDireccionSkipTracingId) {
    var datoDireccion = datoDireccionSTRepository.findById(datoDireccionSkipTracingId).orElse(null);
    return skipTracingMapper.parseToOutput(datoDireccion);
  }

  @Override
  public DatoContactoSkipTracingDTO updateDatoContactoST(
      Integer datoContactoSkipTracingId, DatoContactoSTInputDTO datoContactoSTInputDTO,List<MultipartFile>fotos, Usuario logged) throws NotFoundException, IOException {
    Usuario gestor = usuarioRepository.findById(logged.getId()).orElseThrow(
      () -> new NotFoundException("Usuario", logged.getId()));
    var datoContacto = datoContactoSTRepository.findById(datoContactoSkipTracingId).orElse(null);
    if (datoContacto != null) {
      skipTracingUtil.actualizarCamposDatoContacto(datoContacto, datoContactoSTInputDTO, gestor);
    }
    this.procesarFotos(datoContacto,fotos);
    datoContactoSTRepository.save(datoContacto);
    return skipTracingMapper.parseToOutput(datoContacto);
  }

  @Override
  public DatoDireccionSkipTracingDTO updateDatoDireccionST(
      Integer datoDireccionSkipTracingId, DatoDireccionSTInputDTO datoDireccionSTInputDTO,List<MultipartFile>fotos, Usuario logged) throws NotFoundException, IOException {
    Usuario usuario = usuarioRepository.findById(logged.getId()).orElseThrow(
      () -> new NotFoundException("Usuario", logged.getId()));
    var datoDireccion = datoDireccionSTRepository.findById(datoDireccionSkipTracingId).orElse(null);
    if (datoDireccion != null) {
      skipTracingUtil.actualizarCamposDatoDireccion(datoDireccion, datoDireccionSTInputDTO, usuario);
    }
    this.procesarFotosDireccionST(datoDireccion,fotos);
    datoDireccionSTRepository.save(datoDireccion);
    return skipTracingMapper.parseToOutput(datoDireccion);
  }

  @Override
  public DatoContactoSkipTracingDTO createDatoContactoST(
      Integer intervinienteSkiptracingId, DatoContactoSTInputDTO datoContactoSTInputDTO,List<MultipartFile> fotos, Usuario logged) throws NotFoundException, IOException {
    Usuario usuario = usuarioRepository.findById(logged.getId()).orElseThrow(
      () -> new NotFoundException("Usuario", logged.getId()));
    IntervinienteST intervinienteST =
        intervinienteSTRepository.findById(intervinienteSkiptracingId).orElse(null);
    DatoContactoST datoContactoST = new DatoContactoST();

    datoContactoST =
        skipTracingMapper.parseToEntity(datoContactoST, datoContactoSTInputDTO, usuario,intervinienteSkiptracingId);
    datoContactoST.setIntervinienteST(intervinienteST);

    this.procesarFotos(datoContactoST,fotos);
    DatoContactoST datoContactoSTsaved = datoContactoSTRepository.save(datoContactoST);
    return skipTracingMapper.parseToOutput(datoContactoSTsaved);
  }

  @Override
  public DatoDireccionSkipTracingDTO createDatoDireccionST(
      Integer intervinienteSkiptracingId, DatoDireccionSTInputDTO datoDireccionSTInputDTO,List<MultipartFile> fotos, Usuario logged)
    throws InvocationTargetException, IllegalAccessException, NotFoundException, IOException {
    Usuario usuario = usuarioRepository.findById(logged.getId()).orElseThrow(
      () -> new NotFoundException("Usuario", logged.getId()));
    IntervinienteST intervinienteST =
        intervinienteSTRepository.findById(intervinienteSkiptracingId).orElse(null);
    DatoDireccionST datoDireccionST = new DatoDireccionST();
    datoDireccionST.setIntervinienteST(intervinienteST);
    datoDireccionST =
        skipTracingMapper.parseToEntity(datoDireccionST, datoDireccionSTInputDTO, usuario);
   // datoDireccionST.setIntervinienteST(intervinienteST);
    this.procesarFotosDireccionST(datoDireccionST,fotos);
    DatoDireccionST datoDireccionSTsaved = datoDireccionSTRepository.save(datoDireccionST);
    return skipTracingMapper.parseToOutput(datoDireccionSTsaved);
  }

  public SkipTracingOutputDTO getById(Integer id) throws NotFoundException, InvocationTargetException, IllegalAccessException {
    SkipTracing st =
        skipTracingRepository
            .findById(id)
            .orElseThrow(() -> new NotFoundException("SkipTracing", id));
    SkipTracingOutputDTO result = skipTracingMapper.parseToOutput(st);
    return result;
  }

  public DatoContactoSkipTracingDTO validarDC(Integer id, Boolean validado)
      throws NotFoundException {
    DatoContactoST dc =
        datoContactoSTRepository
            .findById(id)
            .orElseThrow(
                () -> new NotFoundException("DatoContacto SkipTracing", id));
    dc.setValidado(validado);
    if (dc.getValidado()){
      dc.setFechaValidacion(new Date());
    } else {
      dc.setFechaValidacion(null);
    }
    datoContactoSTRepository.save(dc);
    DatoContactoSkipTracingDTO result = skipTracingMapper.parseToOutput(dc);
    return result;
  }

  public DatoDireccionSkipTracingDTO validarDD(Integer id, Boolean validado)
      throws NotFoundException {
    DatoDireccionST dd =
        datoDireccionSTRepository
            .findById(id)
            .orElseThrow(
                () -> new NotFoundException("Direccion SkipTracing", id));
    dd.setValidado(validado);
    if (dd.getValidado()){
      dd.setFechaValidacion(new Date());
    } else {
      dd.setFechaValidacion(null);
    }
    datoDireccionSTRepository.save(dd);
    DatoDireccionSkipTracingDTO result = skipTracingMapper.parseToOutput(dd);
    return result;
  }

  // Este listado debe ser de IntervinientesST
  private List<IntervinienteST> skiptracingsFromExcel(MultipartFile file, String empresa, Usuario user)
    throws Exception {
    List<IntervinienteST> intervinienteSTList = new ArrayList<>();
    List<DatoContactoST> datoContactoSTList = new ArrayList<>();
    List<DatoDireccionST> datoDireccionSTList = new ArrayList<>();
    XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
    XSSFSheet worksheet = workbook.getSheetAt(0);
    TipoContacto telefonoFijo= tipoContactoRepository.findByCodigo("TEL_FIJ").orElse(null);
    TipoContacto telefonoMovil= tipoContactoRepository.findByCodigo("TEL_MOV").orElse(null);

    for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
      IntervinienteST intervinienteST = new IntervinienteST();
      // Posicion 27 del telefono titular 1
      XSSFRow row = worksheet.getRow(i);

      if (row != null) {
        // TODO id  1
        if (row != null) {
          // POR CADA TELEFONO DEL TITULAR
          if (row.getCell(55) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST != null) {
              DatoContactoST datoContactoST = new DatoContactoST();
              try {
                //Esto debido a que si al exportar viene como "-" y luego hay que importarlo cuando falle es porque viene un número
                var telefono1=row.getCell(55).getStringCellValue();
                if (!telefono1.equals("-")){
                  datoContactoST.setValor(telefono1);
                  datoContactoST.setIntervinienteST(intervinienteST);
                  DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                  datoContactoST.setTipoContacto(telefonoMovil);
                  datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                  datoContactoSTRepository.save(datoContactoST);
                  datoContactoSTList.add(datoContactoST);
                }
              } catch (Exception e) {
                Double telefono1=row.getCell(55).getNumericCellValue();
                DecimalFormat format = new DecimalFormat("0.#");
                String telefonoToString=format.format(telefono1);
              //  String telefonoToString=Double.toString(telefono1);
               // var valorceldaStringRemove=valorceldaString.replace(".","");
                datoContactoST.setValor(telefonoToString);
                datoContactoST.setIntervinienteST(intervinienteST);
                DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                datoContactoST.setTipoContacto(telefonoMovil);
                datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                datoContactoSTRepository.save(datoContactoST);
                datoContactoSTList.add(datoContactoST);
              }
              }
          }
          if (row.getCell(56) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST != null) {
              DatoContactoST datoContactoST = new DatoContactoST();
              try {
                //Esto debido a que si al exportar viene como "-" y luego hay que importarlo cuando falle es porque viene un número
                var telefono1=row.getCell(56).getStringCellValue();
                if (!telefono1.equals("-")){
                  datoContactoST.setValor(telefono1);
                  datoContactoST.setIntervinienteST(intervinienteST);
                  DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                  datoContactoST.setTipoContacto(telefonoMovil);
                  datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                  datoContactoSTRepository.save(datoContactoST);
                  datoContactoSTList.add(datoContactoST);
                }
              } catch (Exception e) {
                Double telefono1=row.getCell(56).getNumericCellValue();
                DecimalFormat format = new DecimalFormat("0.#");
                String telefonoToString=format.format(telefono1);
                //  String telefonoToString=Double.toString(telefono1);
                // var valorceldaStringRemove=valorceldaString.replace(".","");
                datoContactoST.setValor(telefonoToString);
                datoContactoST.setIntervinienteST(intervinienteST);
                DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                datoContactoST.setTipoContacto(telefonoMovil);
                datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                datoContactoSTRepository.save(datoContactoST);
                datoContactoSTList.add(datoContactoST);
              }
            }
           }
          if (row.getCell(57) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST != null) {
              DatoContactoST datoContactoST = new DatoContactoST();
              try {
                //Esto debido a que si al exportar viene como "-" y luego hay que importarlo cuando falle es porque viene un número
                var telefono1=row.getCell(57).getStringCellValue();
                if (!telefono1.equals("-")){
                  datoContactoST.setValor(telefono1);
                  datoContactoST.setIntervinienteST(intervinienteST);
                  DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                  datoContactoST.setTipoContacto(telefonoMovil);
                  datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                  datoContactoSTRepository.save(datoContactoST);
                  datoContactoSTList.add(datoContactoST);
                }
              } catch (Exception e) {
                Double telefono1=row.getCell(57).getNumericCellValue();
                DecimalFormat format = new DecimalFormat("0.#");
                String telefonoToString=format.format(telefono1);
                //  String telefonoToString=Double.toString(telefono1);
                // var valorceldaStringRemove=valorceldaString.replace(".","");
                datoContactoST.setValor(telefonoToString);
                datoContactoST.setIntervinienteST(intervinienteST);
                DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                datoContactoST.setTipoContacto(telefonoMovil);
                datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                datoContactoSTRepository.save(datoContactoST);
                datoContactoSTList.add(datoContactoST);
              }
            }
            }
          }
          if (row.getCell(58) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST!=null){
              DatoContactoST datoContactoST = new DatoContactoST();
              try {
                //Esto debido a que si al exportar viene como "-" y luego hay que importarlo cuando falle es porque viene un número
                var telefono1=row.getCell(58).getStringCellValue();
                if (!telefono1.equals("-")){
                  datoContactoST.setValor(telefono1);
                  datoContactoST.setIntervinienteST(intervinienteST);
                  DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                  datoContactoST.setTipoContacto(telefonoMovil);
                  datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                  datoContactoSTRepository.save(datoContactoST);
                  datoContactoSTList.add(datoContactoST);
                }
              } catch (Exception e) {
                Double telefono1=row.getCell(58).getNumericCellValue();
                DecimalFormat format = new DecimalFormat("0.#");
                String telefonoToString=format.format(telefono1);
                //  String telefonoToString=Double.toString(telefono1);
                // var valorceldaStringRemove=valorceldaString.replace(".","");
                datoContactoST.setValor(telefonoToString);
                datoContactoST.setIntervinienteST(intervinienteST);
                DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                datoContactoST.setTipoContacto(telefonoMovil);
                datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                datoContactoSTRepository.save(datoContactoST);
                datoContactoSTList.add(datoContactoST);
              }
            }
          }
          if (row.getCell(59) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST!=null){
              DatoContactoST datoContactoST = new DatoContactoST();
              try {
                //Esto debido a que si al exportar viene como "-" y luego hay que importarlo cuando falle es porque viene un número
                var telefono1=row.getCell(59).getStringCellValue();
                if (!telefono1.equals("-")){
                  datoContactoST.setValor(telefono1);
                  datoContactoST.setIntervinienteST(intervinienteST);
                  DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                  datoContactoST.setTipoContacto(telefonoMovil);
                  datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                  datoContactoSTRepository.save(datoContactoST);
                  datoContactoSTList.add(datoContactoST);
                }
              } catch (Exception e) {
                Double telefono1=row.getCell(59).getNumericCellValue();
                DecimalFormat format = new DecimalFormat("0.#");
                String telefonoToString=format.format(telefono1);
                //  String telefonoToString=Double.toString(telefono1);
                // var valorceldaStringRemove=valorceldaString.replace(".","");
                datoContactoST.setValor(telefonoToString);
                datoContactoST.setIntervinienteST(intervinienteST);
                DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                datoContactoST.setTipoContacto(telefonoMovil);
                datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                datoContactoSTRepository.save(datoContactoST);
                datoContactoSTList.add(datoContactoST);
              }
            }
          }
          if (row.getCell(60) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST!=null){
              DatoContactoST datoContactoST = new DatoContactoST();
              try {
                //Esto debido a que si al exportar viene como "-" y luego hay que importarlo cuando falle es porque viene un número
                var telefono1=row.getCell(60).getStringCellValue();
                if (!telefono1.equals("-")){
                  datoContactoST.setValor(telefono1);
                  datoContactoST.setIntervinienteST(intervinienteST);
                  DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                  datoContactoST.setTipoContacto(telefonoMovil);
                  datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                  datoContactoSTRepository.save(datoContactoST);
                  datoContactoSTList.add(datoContactoST);
                }
              } catch (Exception e) {
                Double telefono1=row.getCell(60).getNumericCellValue();
                DecimalFormat format = new DecimalFormat("0.#");
                String telefonoToString=format.format(telefono1);
                //  String telefonoToString=Double.toString(telefono1);
                // var valorceldaStringRemove=valorceldaString.replace(".","");
                datoContactoST.setValor(telefonoToString);
                datoContactoST.setIntervinienteST(intervinienteST);
                DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                datoContactoST.setTipoContacto(telefonoMovil);
                datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                datoContactoSTRepository.save(datoContactoST);
                datoContactoSTList.add(datoContactoST);
              }
            }
          }
          if (row.getCell(61) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST!=null){
              DatoContactoST datoContactoST = new DatoContactoST();
              try {
                //Esto debido a que si al exportar viene como "-" y luego hay que importarlo cuando falle es porque viene un número
                var telefono1=row.getCell(61).getStringCellValue();
                if (!telefono1.equals("-")){
                  datoContactoST.setValor(telefono1);
                  datoContactoST.setIntervinienteST(intervinienteST);
                  DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                  datoContactoST.setTipoContacto(telefonoMovil);
                  datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                  datoContactoSTRepository.save(datoContactoST);
                  datoContactoSTList.add(datoContactoST);
                }
              } catch (Exception e) {
                Double telefono1=row.getCell(61).getNumericCellValue();
                DecimalFormat format = new DecimalFormat("0.#");
                String telefonoToString=format.format(telefono1);
                //  String telefonoToString=Double.toString(telefono1);
                // var valorceldaStringRemove=valorceldaString.replace(".","");
                datoContactoST.setValor(telefonoToString);
                datoContactoST.setIntervinienteST(intervinienteST);
                DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                datoContactoST.setTipoContacto(telefonoMovil);
                datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                datoContactoSTRepository.save(datoContactoST);
                datoContactoSTList.add(datoContactoST);
              }
            }
          }
          if (row.getCell(62) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
           if (intervinienteST!=null){
             DatoContactoST datoContactoST = new DatoContactoST();
             try {
               //Esto debido a que si al exportar viene como "-" y luego hay que importarlo cuando falle es porque viene un número
               var telefono1=row.getCell(62).getStringCellValue();
               if (!telefono1.equals("-")){
                 datoContactoST.setValor(telefono1);
                 datoContactoST.setIntervinienteST(intervinienteST);
                 DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                 datoContactoST.setTipoContacto(telefonoMovil);
                 datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                 datoContactoSTRepository.save(datoContactoST);
                 datoContactoSTList.add(datoContactoST);
               }
             } catch (Exception e) {
               Double telefono1=row.getCell(62).getNumericCellValue();
               DecimalFormat format = new DecimalFormat("0.#");
               String telefonoToString=format.format(telefono1);
               //  String telefonoToString=Double.toString(telefono1);
               // var valorceldaStringRemove=valorceldaString.replace(".","");
               datoContactoST.setValor(telefonoToString);
               datoContactoST.setIntervinienteST(intervinienteST);
               DatosOrigen dor = getDatosOrigenCarga(empresa, user);
               datoContactoST.setTipoContacto(telefonoMovil);
               datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
               datoContactoSTRepository.save(datoContactoST);
               datoContactoSTList.add(datoContactoST);
             }
           }
          }
          // DATO DIRECCION SKIPTRACING
          if (row.getCell(63) != null) {
            //   DatoDireccionST datoDireccionST = new DatoDireccionST();
            // El domicilio hay que separarlo //TODO
            DatoDireccionST datoDireccionST = new DatoDireccionST();
            datoDireccionST.setNombre(row.getCell(63).getStringCellValue());

            DatosOrigen dor = getDatosOrigenCarga(empresa, user);
            datoDireccionST.setDatosOrigen(datosOrigenRepository.save(dor));

            var valorLocalidad = row.getCell(64).getStringCellValue();
            var valorProvincia = row.getCell(66).getStringCellValue();

            // Y si hay domicilios que no se encuentran en la base de datos es decir en el catálogo
          // localidad?¿
          if (!valorLocalidad.equals("-") && !valorProvincia.equals("-")) {
            List<Localidad> localidad = localidadRepository.findByValorAndMunicipioProvinciaValor(valorLocalidad,valorProvincia);
            // .orElseThrow(() -> new NotFoundException("No se ha encontrado
            // la"+valorLocalidad+"para la provincia"+valorProvincia));
            if (!localidad.isEmpty()) {
              datoDireccionST.setLocalidad(localidad.get(0));
              // Provincia
              Provincia provincia = provinciaRepository.findByValor(valorProvincia).orElse(null);
              datoDireccionST.setProvincia(provincia);
              // NumeroIncidenciasJudiciales
              //   datoDireccionSTRepository.save(datoDireccionST);
              // datoDireccionST
              datoDireccionSTList.add(datoDireccionST);
            }
            }
          if ((valorLocalidad.equals("-")&& !valorProvincia.equals("-")) || (!valorLocalidad.equals("-")&&valorProvincia.equals("-"))){
            throw new Exception("Para añadir un dato direccion con localidad hay que añadir su provincia");
          }
            // Código Postal
            try{
              var codigopostal=row.getCell(65).getStringCellValue();
              if (!codigopostal.equals("-")){
                datoDireccionST.setCodigoPostal(codigopostal);
              }
            }catch (Exception e){
              Double codigopostal=row.getCell(65).getNumericCellValue();
              DecimalFormat format = new DecimalFormat("0.#");
              String codigopostalToString=format.format(codigopostal);
              datoDireccionST.setCodigoPostal(codigopostalToString);
            }
            //Fecha alta
            //datoDireccionST.setFechaAlta(new Date());
            datoDireccionST.setIntervinienteST(intervinienteST);
            datoDireccionSTRepository.save(datoDireccionST);

          }
          //Actualización de campos IntervinienteST QUE NOS PASAN ELLOS

          if (row.getCell(67)!=null){
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST != null) {
            try {
              String numeroIncidenciasJudiciales = row.getCell(67).getStringCellValue();
              if (!numeroIncidenciasJudiciales.equals("-")) {
                intervinienteST.setNumeroIncidenciasJudiciales(
                    Double.parseDouble(numeroIncidenciasJudiciales));
                intervinienteSTRepository.save(intervinienteST);
              }
            }catch(Exception e){
              Double numeroIncidenciasJudiciales = row.getCell(67).getNumericCellValue();
              intervinienteST.setNumeroIncidenciasJudiciales(numeroIncidenciasJudiciales);
              intervinienteSTRepository.save(intervinienteST);
             }
            }
          }
          if (row.getCell(68)!=null){
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST != null) {
              try{
              String numeroIncidenciasAdministrativas = row.getCell(68).getStringCellValue();
            if (!numeroIncidenciasAdministrativas.equals("-")) {
              intervinienteST.setNumeroIncidenciasAdministrativas(
                  Double.parseDouble(numeroIncidenciasAdministrativas));
              intervinienteSTRepository.save(intervinienteST);
              }
                }catch (Exception e){
                Double numeroIncidenciasAdministrativas = row.getCell(68).getNumericCellValue();
                intervinienteST.setNumeroIncidenciasAdministrativas(numeroIncidenciasAdministrativas);
                intervinienteSTRepository.save(intervinienteST);
              }
          }
            }
          if (row.getCell(69)!=null){
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST != null) {
            try {
              String numeroIncidenciasConcurso = row.getCell(69).getStringCellValue();
              if (!numeroIncidenciasConcurso.equals("-")) {
                intervinienteST.setNumeroIncidenciasConcurso(
                    Double.parseDouble(numeroIncidenciasConcurso));
                intervinienteSTRepository.save(intervinienteST);
              }
            }catch (Exception e){
              Double numeroIncidenciasConcurso = row.getCell(69).getNumericCellValue();
              intervinienteST.setNumeroIncidenciasConcurso(numeroIncidenciasConcurso);
              intervinienteSTRepository.save(intervinienteST);
            }
            }
          }
          //Fecha fallecido
          if (row.getCell(70)!=null){
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
          if (intervinienteST != null) {
          try{
            String fechafallecido = row.getCell(70).getStringCellValue().replace(".", "/");
            if (!fechafallecido.equals("-")) {
              Date fechafalle = new SimpleDateFormat("dd/MM/yyyy").parse(fechafallecido);
              intervinienteST.setFechaFallecido(fechafalle);
              intervinienteSTRepository.save(intervinienteST);
            }
            }catch(Exception e){
            Date fechafallecido=row.getCell(70).getDateCellValue();
            intervinienteST.setFechaFallecido(fechafallecido);
            intervinienteSTRepository.save(intervinienteST);
          }
            }
          }
          // Autónomo 51
          if (row.getCell(71) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST != null) {
              String autonomo = row.getCell(71).getStringCellValue();
            if (!autonomo.equals("-")) {
              intervinienteST.setAutonomo(autonomo != null && autonomo.equals("AUT"));
              intervinienteSTRepository.save(intervinienteST);
              }
            }
          }
          //Organo 52
          if (row.getCell(72) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST != null) {
              String organo = row.getCell(72).getStringCellValue();
            if (!organo.equals("-")) {
              intervinienteST.setOrgano(organo != null && organo.equals("ORG"));
              intervinienteSTRepository.save(intervinienteST);
              }
            }
          }
          //Ausente 53
          if (row.getCell(73) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
              intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
            if (intervinienteST != null) {
              String ausente = row.getCell(73).getStringCellValue();
            if (!ausente.equals("-")) {
              intervinienteST.setAusente(ausente != null && ausente.equals("AUS"));
              intervinienteSTRepository.save(intervinienteST);
              }
            }
          }
          if (row.getCell(74)!=null){
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
              intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
          if (intervinienteST != null) {
            try{
            String numeroInvestigacionesPrevias = row.getCell(74).getStringCellValue();
            if (!numeroInvestigacionesPrevias.equals("-")) {
              intervinienteST.setNumeroInvestigacionesPrevias(
                  Double.parseDouble(numeroInvestigacionesPrevias));
              intervinienteSTRepository.save(intervinienteST);
            }
            }catch (Exception e){
              Double numeroInvestigacionesPrevias = row.getCell(74).getNumericCellValue();
              intervinienteST.setNumeroInvestigacionesPrevias(numeroInvestigacionesPrevias);
              intervinienteSTRepository.save(intervinienteST);
            }
             }
          }
        //FechaUltimaInvestigacion 55
          if (row.getCell(75)!=null){
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);
          if (intervinienteST != null) {
            try{
            String fechaUltimaInvestigacion =
                row.getCell(75).getStringCellValue().replace(".", "/");
            if (!fechaUltimaInvestigacion.equals("-")) {
              Date fechaInvestigacion =
                  new SimpleDateFormat("dd/MM/yyyy").parse(fechaUltimaInvestigacion);
              intervinienteST.setFechaUltimaInvestigacion(fechaInvestigacion);
              intervinienteSTRepository.save(intervinienteST);
            }
            }catch(Exception e){
              Date fechaUltimaInvestigacion=row.getCell(75).getDateCellValue();
              intervinienteST.setFechaUltimaInvestigacion(fechaUltimaInvestigacion);
              intervinienteSTRepository.save(intervinienteST);
            }
             }
          }
        }
      if (intervinienteST!=null)
        intervinienteSTList.add(intervinienteST);
    }
    for (IntervinienteST iST : intervinienteSTList){
      List<SkipTracing>skipTracingList=iST.getSkipTracingSet().stream().collect(Collectors.toList());
      for (SkipTracing st : skipTracingList){
        EstadoSkipTracing estado = estadoSkipTracingRepository.findByCodigo("02").orElseThrow(
          () -> new NotFoundException("EstadoSkipTracing", "código", "'02'"));
        if (st.getNivelST().equals("01"))
          st.setEstadoST(estado);
          skipTracingRepository.save(st);
          //cerrar(st);
      }
    }
    return intervinienteSTList;
  }

  private List<IntervinienteST> skiptracingsFromExcelIncofisa(MultipartFile file, String empresa, Usuario user)
    throws IOException, NotFoundException, ParseException {
    List<IntervinienteST> intervinienteSTList = new ArrayList<>();
    List<DatoContactoST> datoContactoSTList = new ArrayList<>();
    List<DatoDireccionST> datoDireccionSTList = new ArrayList<>();
    XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
    XSSFSheet worksheet = workbook.getSheetAt(0);
    for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
      IntervinienteST intervinienteST = new IntervinienteST();
      // Posicion 27 del telefono titular 1
      XSSFRow row = worksheet.getRow(i);

      if (row != null) {
        // TODO id  1
        if (row != null) {
          TipoContacto telefonoFijo= tipoContactoRepository.findByCodigo("TEL_FIJ").orElse(null);
          TipoContacto telefonoMovil= tipoContactoRepository.findByCodigo("TEL_MOV").orElse(null);
          // Teléfonos Nuevos
          if (row.getCell(12) != null) {
            var intervinienteSTid = row.getCell(0).getNumericCellValue();
            Integer intId = (int) intervinienteSTid;
            intervinienteST = intervinienteSTRepository.findById(intId).orElse(null);

            try{
            var valorcelda=row.getCell(12).getNumericCellValue();

              NumberFormat nf = NumberFormat.getNumberInstance();
              nf.setMaximumFractionDigits(0);
              var valorceldaString=nf.format(valorcelda);
              var valorceldaStringRemove=valorceldaString.replace(".","");
             // Double.toString(valorcelda);
              DatoContactoST datoContactoST = new DatoContactoST();
              datoContactoST.setValor(valorceldaStringRemove);
              datoContactoST.setIntervinienteST(intervinienteST);
              DatosOrigen dor = getDatosOrigenCarga(empresa, user);
              datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
              datoContactoST.setTipoContacto(telefonoFijo);

              datoContactoSTRepository.save(datoContactoST);
              datoContactoSTList.add(datoContactoST);
            } catch (Exception e) {
              var valorcelda = row.getCell(12).getStringCellValue();
              if (!valorcelda.equals("-")) {
                List<String> sepaTodo = Arrays.asList(valorcelda.split("-"));
                for (String valoresSeparados : sepaTodo) {
                  DatoContactoST datoContactoST = new DatoContactoST();
                  datoContactoST.setValor(valoresSeparados);
                  datoContactoST.setIntervinienteST(intervinienteST);
                  DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                  datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                  datoContactoST.setTipoContacto(telefonoFijo);
                  datoContactoSTRepository.save(datoContactoST);
                  datoContactoSTList.add(datoContactoST);
                }
              }
            }
          }
          //Telefonos afines
          if (row.getCell(13)!=null){
            var intervinienteSTid=row.getCell(0).getNumericCellValue();
            Integer intId=(int)intervinienteSTid;
            intervinienteST=intervinienteSTRepository.findById(intId).orElse(null);
         try{
            var valorcelda=row.getCell(13).getNumericCellValue();
           NumberFormat nf = NumberFormat.getNumberInstance();
           nf.setMaximumFractionDigits(0);
           var valorceldaString=nf.format(valorcelda);
           var valorceldaStringRemove=valorceldaString.replace(".","");
           DatoContactoST datoContactoST = new DatoContactoST();
           datoContactoST.setValor(valorceldaStringRemove);
           datoContactoST.setIntervinienteST(intervinienteST);
           DatosOrigen dor = getDatosOrigenCarga(empresa, user);
           datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
           datoContactoST.setTipoContacto(telefonoFijo);

           datoContactoSTRepository.save(datoContactoST);
           datoContactoSTList.add(datoContactoST);
            }catch (Exception e){
           var valorcelda=row.getCell(13).getStringCellValue();
              if (!valorcelda.equals("-")) {
                List<String> sepaTodo = Arrays.asList(valorcelda.split("-"));
                for (String valoresSeparados : sepaTodo) {
                  DatoContactoST datoContactoST = new DatoContactoST();
                  datoContactoST.setValor(valoresSeparados);
                  datoContactoST.setIntervinienteST(intervinienteST);
                  DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                  datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                  datoContactoST.setTipoContacto(telefonoFijo);

                  datoContactoSTRepository.save(datoContactoST);
                  datoContactoSTList.add(datoContactoST);
                }
           }
         }
          }
          if (row.getCell(14)!=null){
            var intervinienteSTid=row.getCell(0).getNumericCellValue();
            Integer intId=(int)intervinienteSTid;
            intervinienteST=intervinienteSTRepository.findById(intId).orElse(null);
            try{
            var valorcelda=row.getCell(14).getNumericCellValue();
              NumberFormat nf = NumberFormat.getNumberInstance();
              nf.setMaximumFractionDigits(0);
              var valorceldaString=nf.format(valorcelda);
              var valorceldaStringRemove=valorceldaString.replace(".","");
              DatoContactoST datoContactoST = new DatoContactoST();
              datoContactoST.setValor(valorceldaStringRemove);
              datoContactoST.setIntervinienteST(intervinienteST);
              DatosOrigen dor = getDatosOrigenCarga(empresa, user);
              datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
              datoContactoST.setTipoContacto(telefonoFijo);

              datoContactoSTRepository.save(datoContactoST);
              datoContactoSTList.add(datoContactoST);

            }catch(Exception e){
              var valorcelda=row.getCell(14).getStringCellValue();
              if (!valorcelda.equals("-")) {
                List<String> sepaTodo = Arrays.asList(valorcelda.split("-"));
                for (String valoresSeparados : sepaTodo) {
                  DatoContactoST datoContactoST = new DatoContactoST();
                  datoContactoST.setValor(valoresSeparados);
                  datoContactoST.setIntervinienteST(intervinienteST);
                  DatosOrigen dor = getDatosOrigenCarga(empresa, user);
                  datoContactoST.setDatosOrigen(datosOrigenRepository.save(dor));
                  datoContactoST.setTipoContacto(telefonoFijo);

                  datoContactoSTRepository.save(datoContactoST);
                  datoContactoSTList.add(datoContactoST);
                }
              }
            }
          }
        }
      }
      intervinienteSTList.add(intervinienteST);
    }
    for (IntervinienteST iST : intervinienteSTList){
      for (SkipTracing st : iST.getSkipTracingSet()){
        EstadoSkipTracing estado = estadoSkipTracingRepository.findByCodigo("02").orElseThrow(
          () -> new NotFoundException("EstadoSkipTracing", "código", "'02'"));
        if (st.getNivelST().equals("01"))
          st.setEstadoST(estado);
        skipTracingRepository.save(st);
        //cerrar(st);
      }
    }
    return intervinienteSTList;
  }

  private DatosOrigen getDatosOrigenCarga(String plantilla, Usuario user) throws NotFoundException {
    DatosOrigen dor = new DatosOrigen();

    dor.setGestor(user);
    dor.setFechaActualizacion(new Date());
    dor.setFecha(new Date());

    dor.setOtraFuente(plantilla);

    OrigenDato od = null;
    if (plantilla.equals("Detectys") || plantilla.equals("Gestor skip tracing")){
      od = origenDatoRepository.findByCodigo("DETE").orElseThrow(() ->
        new NotFoundException("OrigenDato", "código", "'DETE'"));
    } else if (plantilla.equals("Incofisa")) {
      od = origenDatoRepository.findByCodigo("INCO").orElseThrow(() ->
        new NotFoundException("OrigenDato", "código", "'INCO'"));
    } else {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The template code does not match 'Detectys' or 'Incofisa'.");
      else throw new NotFoundException("El código de la plantilla no coincide con 'Detectys' ni con 'Incofisa'.");
    }
    dor.setOrigenDato(od);

    return dor;
  }

  // Pasar usuario????
  @Override
  @Transactional
  public void carga(CargaSTInputDTO input, MultipartFile file, Usuario user) throws Exception {
    //TODO: cambiar las cosas
    String nombreplantilla = input.getEncargado();
    String name = file.getOriginalFilename();
    String[] names = name.split("_");
    if (names.length != 3){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("The file name must be 'Carga_XXX _...', where 'XXX' must be an Integer.");
      else throw new RequiredValueException("El nombre del fichero debe ser 'Carga_XXX_...', donde 'XXX' debe ser un Integer.");
    }
    Integer idCarga = null;
    try {
      if (!names[1].contains("U"))
        idCarga = Integer.parseInt(names[1]);
    } catch (Exception e){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("The file name must be 'Carga_XXX _...', where 'XXX' must be an Integer.");
      else throw new RequiredValueException("El nombre del fichero debe ser 'Carga_XXX_...', donde 'XXX' debe ser un Integer.");
    }

    if (idCarga != null) {
      Integer finalIdCarga = idCarga;
      CargaST cargaST = cargaSTRepository.findById(idCarga).orElseThrow(() -> new NotFoundException("carga", finalIdCarga));
      skipTracingMapper.parseToEntity(cargaST, input);
      cargaSTRepository.save(cargaST);

      EstadoSkipTracing eST = estadoSkipTracingRepository.findByCodigo("09").orElseThrow(() ->
        new NotFoundException("EstadoSkipTracing", "código", "'09'"));

      List<SkipTracing>cargaSkipTracingList=cargaST.getSkipTracings().stream().collect(Collectors.toList());

      for (SkipTracing st : cargaSkipTracingList){
        st.setActivo(false);
        st.setEstadoST(eST);
        SkipTracing stSaved = skipTracingRepository.save(st);
        if (input.getValidado()){
          cerrar(stSaved, user);
        }
      }
    }

    List<IntervinienteST> intervinienteSTList = new ArrayList<>();
    if (nombreplantilla.equals("Detectys") || nombreplantilla.equals("Gestor skip tracing")) {
      intervinienteSTList = skiptracingsFromExcel(file, nombreplantilla, user);
    } else if (nombreplantilla.equals("Incofisa")) {
      intervinienteSTList = skiptracingsFromExcelIncofisa(file, nombreplantilla, user);
    } else {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("Error, the file does not contain data informationContactosST");
      else throw new RequiredValueException("Error, el fichero no contienes información de datosContactosST");
    }
    if (intervinienteSTList.isEmpty()){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("The template code does not match 'Detectys' or 'Incofisa'.");
      else throw new RequiredValueException("El código de la plantilla no coincide con 'Detectys' ni con 'Incofisa'.");
    }

    cargaSkipTracings(intervinienteSTList);
  }

  public Integer findCarga(List<Integer> sts){
    List<SkipTracing> skipTracings = skipTracingRepository.findAllById(sts);
    CargaST cargaST = null;
    for (SkipTracing st : skipTracings){
      if (st.getCargaST() == null) return null;
      if (cargaST != null && !st.getCargaST().getId().equals(cargaST.getId())) return null;
      cargaST = st.getCargaST();
    }
    if (cargaST == null) return null;
    return cargaST.getId();
  }

  private void cargaSkipTracings(List<IntervinienteST> list) throws NotFoundException {
    TipoBusqueda tb = tipoBusquedaRepository.findByCodigo("GES").orElseThrow(() ->
      new NotFoundException("TipoBusqueda", "código", "'GES'"));
    EstadoSkipTracing eST = estadoSkipTracingRepository.findByCodigo("02").orElseThrow(() ->
      new NotFoundException("EstadoSkipTracing", "código", "'02'"));

    for (IntervinienteST iST : list){
      Interviniente i = iST.getInterviniente();
      for (SkipTracing st : iST.getSkipTracingSet()){
        if (i != null && st.getContratoInterviniente().getInterviniente().getId().equals(i.getId()) && st.getEstadoST().getCodigo().equals("03")){
          st.setTipoBusqueda(tb);
          st.setEstadoST(eST);
          skipTracingRepository.save(st);
        }
      }
    }
  }

  private void procesarFotos(DatoContactoST datoContactoST, List<MultipartFile> fotos)
      throws IOException {
    if (fotos.isEmpty()) {
      return;
    }
    for (MultipartFile foto : fotos) {
      JSONObject metadatos = new JSONObject();
      JSONObject archivoFisico = new JSONObject();
      archivoFisico.put("contenedor", "CONT");
      metadatos.put(
          "General documento",
          new MetadatoDocumentoInputDTO(foto.getOriginalFilename(), "EN-02-DOCA-01")
              .createDescripcionGeneralDocumento());
      metadatos.put("Archivo físico", archivoFisico);
      long idDocumento =
          this.servicioGestorDocumental
              .crearDocumento(
                  CodigoEntidad.INTERVINIENTE,
                  datoContactoST.getIntervinienteST().getIdHaya(),
                  foto,
                  metadatos)
              .getIdDocumento();
      datoContactoST.addFoto(idDocumento);
    }
}


  private void procesarFotosDireccionST(DatoDireccionST datoDireccionST, List<MultipartFile> fotos)
    throws IOException {
    if (fotos.isEmpty()) {
      return;
    }
    for (MultipartFile foto : fotos) {
      JSONObject metadatos = new JSONObject();
      JSONObject archivoFisico = new JSONObject();
      archivoFisico.put("contenedor", "CONT");
      metadatos.put(
        "General documento",
        new MetadatoDocumentoInputDTO(foto.getOriginalFilename(), "EN-02-DOCA-01")
          .createDescripcionGeneralDocumento());
      metadatos.put("Archivo físico", archivoFisico);
      long idDocumento =
        this.servicioGestorDocumental
          .crearDocumento(
            CodigoEntidad.INTERVINIENTE,
            datoDireccionST.getIntervinienteST().getIdHaya(),
            foto,
            metadatos)
          .getIdDocumento();
      datoDireccionST.addFoto(idDocumento);
    }
  }
}
