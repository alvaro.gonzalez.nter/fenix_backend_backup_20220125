package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DatosOrigenOutputDTO implements Serializable {
  private Integer id;
  private String link;
  private CatalogoMinInfoDTO origenDato;
  private UsuarioAgendaDto gestor;
  private Date fecha;
  private Date fechaActualizacion;
  private String fuenteFinal;

  private CatalogoMinInfoDTO paginaPublica;
  private CatalogoMinInfoDTO redSocial;
  private CatalogoMinInfoDTO perfilProfesional;
  private String otraFuente;
  private String observaciones;
}
