package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SkipTracingInputDTO implements Serializable {

  Integer contratoInterviniente;
  Integer estadoST;
  Integer nivelST;
  Integer perfilBusqueda;
  Integer tipoBusqueda;
  Date fechaInicioBusqueda;
  Integer provincia;
  Integer analistaST;
  String comentario;
  List<Integer> busquedas;
}
