package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import com.haya.alaska.skip_tracing.infrastructure.controller.dto.DatosOrigenInputDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class DatoContactoSTInputDTO implements Serializable {

  //private Integer id;
  private Integer tipoContacto;
  private Boolean validado;
  private Boolean principal;
  private String valor; //Esto en función del tipoContacto
  private DatosOrigenInputDTO datosOrigen;
}
