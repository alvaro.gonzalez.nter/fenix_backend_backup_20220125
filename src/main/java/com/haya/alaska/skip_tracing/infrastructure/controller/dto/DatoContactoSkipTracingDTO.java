package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DatoContactoSkipTracingDTO implements Serializable {

  private Integer id;
  private CatalogoMinInfoDTO tipoContacto;
  private Boolean validado;
  private Boolean principal;
  private String valor;
  //private String email;
  private Date fechaValidacion;
  private DatosOrigenOutputDTO datosOrigen;
  private Integer idIntervinienteST;
  private List<Long> fotos;
  private Date fechaAlta;

}
