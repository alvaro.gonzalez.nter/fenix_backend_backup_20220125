package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class DatoDireccionSTInputDTO implements Serializable {

  private Boolean principal;
  private Boolean validado;
  //private Date fechaAlta;

  private String nombre;
  private String bloque;
  private String escalera;
  private String piso;
  private String puerta;
  private String comentario;
  private String codigoPostal;
  private Double longitud;
  private Double latitud;
  private String origen;
  private String portal;
  private String numero;

  private Integer tipoVia;
  private Integer municipio;
  private Integer localidad;
  private Integer entorno;
  private Integer provincia;
  private Integer pais;


  private DatosOrigenInputDTO datosOrigen;
  private String observaciones;
}
