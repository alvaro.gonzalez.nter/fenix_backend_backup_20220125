package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SkipTracingOutputDTO implements Serializable {

  private Integer id;
  private String cliente;
  private String cartera;
  private Integer idExpediente;
  private String idConcatenado;
  private Integer idContrato;
  private CatalogoMinInfoDTO producto;
  private Integer nIntervinientes;
  private Integer nBienes;
  private CatalogoMinInfoDTO estadoST;
  private CatalogoMinInfoDTO nivelST;
  private CatalogoMinInfoDTO tipoBusqueda;
  private PerfilSimpleOutputDTO perfilBusqueda;
  private IntervinienteSkipTracingDTO intervinientePrincipal;
  private Boolean judicial;
  private CatalogoMinInfoDTO provincia;
  private UsuarioOutputDTO analistaST;
  private UsuarioOutputDTO gestor;
  private Double saldo;
  private Date fechaSolicitud;
  private Date fechaAsignacion;
  private Date fechaInicioBusqueda;
  private Date fechaFinBusqueda;
  private List<IntervinienteSTOutputDTOToList> intervinientes = new ArrayList<>();
  private List<CatalogoMinInfoDTO> busquedas = new ArrayList<>();

  private List<DatoDireccionSkipTracingDTO>datoDireccionSkipTracingDTOList;
  //DatosContacto,Emails y Redes y en función del tipoContacto va a una u otra
  private List<DatoContactoSkipTracingDTO>datoContactoSkipTracingDTOList;
  private List<DatoContactoSkipTracingDTO>emails;
  private List<DatoContactoSkipTracingDTO>redes;

  private CargaSTOutputDTO carga;

  private String comentario;
}
