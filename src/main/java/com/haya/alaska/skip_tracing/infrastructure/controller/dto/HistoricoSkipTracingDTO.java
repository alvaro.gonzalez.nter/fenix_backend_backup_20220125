package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class HistoricoSkipTracingDTO {
  private Date fecha;
  private String nivelBusqueda;
  private String solicitud;
  private String resultado;
  private String agencia;
  private Date fechaGeneracion;
  private Date fechaCierre;
}
