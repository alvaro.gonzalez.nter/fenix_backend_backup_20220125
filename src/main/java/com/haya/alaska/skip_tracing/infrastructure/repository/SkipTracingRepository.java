package com.haya.alaska.skip_tracing.infrastructure.repository;

import com.haya.alaska.skip_tracing.domain.SkipTracing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SkipTracingRepository extends JpaRepository<SkipTracing, Integer> {

  List<SkipTracing> findAllByContratoIntervinienteContratoExpedienteId(Integer expedienteId);

  List<SkipTracing> findAllByContratoIntervinienteIntervinienteIdAndActivoIsTrue(Integer idCI);
  List<SkipTracing> findAllByContratoIntervinienteIntervinienteIdAndNivelSTIdAndActivoIsTrue(Integer idCI, Integer nivelId);

  @Query(
      value =
          "   SELECT rh.timestamp AS fecha, nivel.DES_VALOR AS nivel, usuario.DES_NOMBRE as solicitud , carga.DES_ENCARGADO as agencia,"
              + "  estado.DES_VALOR as estado, skip.FCH_INICIO_BUSQUEDA as fechaInicio ,skip.FCH_FIN_BUSQUEDA as fechaFin,"
              + "  skip.ID_RESPONSABLE_CARTERA as responsableCarteraId"
              + "  FROM HIST_MSTR_SKIP_TRACING skip"
              + "   left join REGISTRO_HISTORICO rh ON skip.REV = rh.id"
              + "   left join MSTR_USUARIO usuario ON skip.ID_USUARIO = usuario.ID"
              + "   left join MSTR_CARGA_SKIP_TRACING carga ON skip.ID_CARGA_SKIP_TRACING = carga.ID"
              + "   left join LKUP_NIVEL_SKIP_TRACING nivel ON nivel.id = skip.ID_NIVEL_SKIP_TRACING"
              + "   left join LKUP_ESTADO_SKIP_TRACING estado ON estado.id = skip.ID_ESTADO_SKIP_TRACING"
              + "   WHERE skip.id = ?1"
              + "   ORDER BY rh.timestamp desc;",
      nativeQuery = true)
  List<Map> historico(Integer skipTracingId);
}
