package com.haya.alaska.skip_tracing.infrastructure.mapper;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.busqueda_skip_tracing.domain.BusquedaSkipTracing;
import com.haya.alaska.busqueda_skip_tracing.infrastructure.repository.BusquedaSkipTracingRepository;
import com.haya.alaska.carga_skip_tracing.domain.CargaST;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto_skip_tracing.domain.DatoContactoST;
import com.haya.alaska.dato_contacto_skip_tracing.infraestructure.repository.DatoContactoSTRepository;
import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import com.haya.alaska.dato_direccion_skip_tracing.infraestructure.repository.DatoDireccionSTRepository;
import com.haya.alaska.datos_origen.domain.DatosOrigen;
import com.haya.alaska.datos_origen.infrastructure.repository.DatosOrigenRepository;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.estado_civil.infrastructure.repository.EstadoCivilRepository;
import com.haya.alaska.estado_skip_tracing.infraestructure.repository.EstadoSkipTracingRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import com.haya.alaska.interviniente_skip_tracing.infraestructure.repository.IntervinienteSTRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.nivel_skip_tracing.infraestructure.repository.NivelSkipTracingRepository;
import com.haya.alaska.origen_dato.domain.OrigenDato;
import com.haya.alaska.origen_dato.infrastructure.repository.OrigenDatoRepository;
import com.haya.alaska.pagina_publica.infrastructure.repository.PaginaPublicaRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.perfil.infrastructure.repository.PerfilRepository;
import com.haya.alaska.perfil_profesional.infrastructure.repository.PerfilProfesionalRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.red_social.infrastructure.repository.RedSocialRepository;
import com.haya.alaska.sexo.infrastructure.repository.SexoRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.skip_tracing.infrastructure.controller.dto.*;
import com.haya.alaska.skip_tracing.infrastructure.repository.SkipTracingRepository;
import com.haya.alaska.skip_tracing.infrastructure.util.SkipTracingUtil;
import com.haya.alaska.tipo_busqueda.infraestructure.repository.TipoBusquedaRepository;
import com.haya.alaska.tipo_contacto.domain.TipoContacto;
import com.haya.alaska.tipo_contacto.infraestructure.repository.TipoContactoRepository;
import com.haya.alaska.tipo_documento.infrastructure.repository.TipoDocumentoRepository;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.mapper.UsuarioMapper;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class SkipTracingMapper {
  @Autowired
  UsuarioMapper usuarioMapper;

  @Autowired
  EstadoSkipTracingRepository estadoSkipTracingRepository;
  @Autowired
  DatosOrigenRepository datosOrigenRepository;
  @Autowired
  TipoBusquedaRepository tipoBusquedaRepository;
  @Autowired
  NivelSkipTracingRepository nivelSkipTracingRepository;
  @Autowired
  UsuarioRepository usuarioRepository;
  @Autowired
  PerfilRepository perfilRepository;
  @Autowired
  ProvinciaRepository provinciaRepository;
  @Autowired
  DatoContactoSTRepository datoContactoSTRepository;
  @Autowired
  IntervinienteSTRepository intervinienteSTRepository;
  @Autowired
  SkipTracingRepository skipTracingRepository;
  @Autowired
  ContratoIntervinienteRepository contratoIntervinienteRepository;
  @Autowired
  BusquedaSkipTracingRepository busquedaSkipTracingRepository;
  @Autowired
  TipoContactoRepository tipoContactoRepository;
  @Autowired
  DatoDireccionSTRepository datoDireccionSTRepository;
  @Autowired
  OrigenDatoRepository origenDatoRepository;
  @Autowired
  PaginaPublicaRepository paginaPublicaRepository;
  @Autowired
  RedSocialRepository redSocialRepository;
  @Autowired
  PerfilProfesionalRepository perfilProfesionalRepository;
  @Autowired
  TipoDocumentoRepository tipoDocumentoRepository;
  @Autowired
  TipoIntervencionRepository tipoIntervencionRepository;
  @Autowired
  SexoRepository sexoRepository;
  @Autowired
  EstadoCivilRepository estadoCivilRepository;
  @Autowired
  MunicipioRepository municipioRepository;
  @Autowired
  LocalidadRepository localidadRepository;
  @Autowired
  EntornoRepository entornoRepository;
  @Autowired
  PaisRepository paisRepository;
  @Autowired
  TipoViaRepository tipoViaRepository;
  @Autowired
  SkipTracingUtil skipTracingUtil;
  @Autowired
  BienRepository bienRepository;

  //SkipTracing
  public SkipTracingOutputDTO parseToOutput(SkipTracing sk) {
    SkipTracingOutputDTO result = new SkipTracingOutputDTO();
    if (sk == null) return result;

    result.setId(sk.getId());
    result.setEstadoST(sk.getEstadoST() != null ? new CatalogoMinInfoDTO(sk.getEstadoST()) : null);
    result.setNivelST(sk.getNivelST() != null ? new CatalogoMinInfoDTO(sk.getNivelST()) : null);
    result.setTipoBusqueda(sk.getTipoBusqueda() != null ? new CatalogoMinInfoDTO(sk.getTipoBusqueda()) : null);
    result.setPerfilBusqueda(sk.getPerfilBusqueda() != null ? new PerfilSimpleOutputDTO(sk.getPerfilBusqueda()) : null);

    if(sk.getNivelST() != null && sk.getCargaST()!= null && sk.getCargaST().getEncargado() != null && sk.getNivelST().getCodigo().equals("01")) {
      UsuarioOutputDTO usuarioOutputDTO = new UsuarioOutputDTO();
      usuarioOutputDTO.setNombre(sk.getCargaST().getEncargado());
      result.setAnalistaST(usuarioOutputDTO);
    } else if(sk.getNivelST() != null && !sk.getNivelST().getCodigo().equals("01")){
      result.setAnalistaST(sk.getAnalistaST() != null ? usuarioMapper.entityToDTO(sk.getAnalistaST()) : null);
    }

    result.setFechaSolicitud(sk.getFechaSolicitud());
    result.setFechaAsignacion(sk.getFechaAsignacion());
    result.setFechaInicioBusqueda(sk.getFechaInicioBusqueda());
    result.setFechaFinBusqueda(sk.getFechaFinBusqueda());
    result.setBusquedas(sk.getBusquedas().stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList()));
    result.setProvincia(sk.getProvincia() != null ? new CatalogoMinInfoDTO(sk.getProvincia()) : null);
    result.setComentario(sk.getComentario());

    //Revisar el mapeo con la nueva relación contratoInterviniente
    ContratoInterviniente ci = sk.getContratoInterviniente();
    if (ci != null){
      Contrato c = ci.getContrato();
      if (c != null) {
        result.setIdContrato(c.getId());
        Expediente e = c.getExpediente();
        if (e != null) {
          result.setIdExpediente(e.getId());
          result.setIdConcatenado(e.getIdConcatenado());
          if (e.getGestor() != null) {
            result.setGestor(usuarioMapper.entityToDTO(e.getGestor()));
          }
          Cartera ca = e.getCartera();
          if (ca != null) {
            result.setCartera(ca.getNombre());
            ClienteCartera cc = ca.getClientes().stream().findFirst().orElse(null);
            if (cc != null) {
              Cliente cl = cc.getCliente();
              if (cl != null) result.setCliente(cl.getNombre());
            }
          }
        }

        result.setProducto(c.getProducto() != null ? new CatalogoMinInfoDTO(c.getProducto()) : null);
        result.setNIntervinientes(c.getIntervinientes().size());
        result.setNBienes(c.getBienes().size());

        result.setJudicial(c.getProcedimientos().isEmpty() ? false : true);
        // Provincia?
        result.setSaldo(c.getSaldoGestion());
      }
      Interviniente i = ci.getInterviniente();
      if (i != null){
        IntervinienteST iST = intervinienteSTRepository.findByIntervinienteId(i.getId()).orElse(null);
        if (iST != null){
          result.setIntervinientePrincipal(parseToOutput(iST));
          anadirDatos(result, iST);
        }
      }
    }

    List<IntervinienteSTOutputDTOToList> intervinientes = new ArrayList<>();
    for (IntervinienteST iST : sk.getIntervinientesST()) {
      if (iST != null){
        IntervinienteSTOutputDTOToList iList = parseToOutputList(iST);
        intervinientes.add(iList);
      }
    }
    intervinientes = intervinientes.stream().sorted(Comparator.comparing((IntervinienteSTOutputDTOToList intST) -> intST.getId())).collect(Collectors.toList());
    result.setIntervinientes(intervinientes);
    result.setCarga(parseToOutput(sk.getCargaST()));

    return result;
  }
  public Integer calcularEdad(Date fNacimianto) {
    if (fNacimianto == null) return null;
    LocalDate fechaNac = LocalDate.parse(fNacimianto.toString());
    LocalDate ahora = LocalDate.now();
    Period periodo = Period.between(fechaNac, ahora);
    return periodo.getYears();
  }
  public SkipTracing parseToEntity(SkipTracing skipTracing, SkipTracingInputDTO skipTracingInputDTO) {
    if (skipTracingInputDTO.getContratoInterviniente() != null)
      skipTracing.setContratoInterviniente(contratoIntervinienteRepository.findById(skipTracingInputDTO.getContratoInterviniente()).orElse(null));
    if (skipTracingInputDTO.getEstadoST() != null)
      skipTracing.setEstadoST(estadoSkipTracingRepository.findById(skipTracingInputDTO.getEstadoST()).orElse(null));
    if (skipTracingInputDTO.getNivelST() != null)
      skipTracing.setNivelST(nivelSkipTracingRepository.findById(skipTracingInputDTO.getNivelST()).orElse(null));
    if (skipTracingInputDTO.getPerfilBusqueda() != null)
      skipTracing.setPerfilBusqueda(perfilRepository.findById(skipTracingInputDTO.getPerfilBusqueda()).orElse(null));
    if (skipTracingInputDTO.getTipoBusqueda() != null)
      skipTracing.setTipoBusqueda(tipoBusquedaRepository.findById(skipTracingInputDTO.getTipoBusqueda()).orElse(null));
    if (skipTracingInputDTO.getFechaInicioBusqueda() != null)
      skipTracing.setFechaInicioBusqueda(skipTracingInputDTO.getFechaInicioBusqueda());
    if (skipTracingInputDTO.getProvincia() != null)
      skipTracing.setProvincia(provinciaRepository.findById(skipTracingInputDTO.getProvincia()).orElse(null));
    if (skipTracingInputDTO.getComentario() != null)
      skipTracing.setComentario(skipTracingInputDTO.getComentario());
    if (skipTracingInputDTO.getAnalistaST() != null)
      skipTracing.setAnalistaST(usuarioRepository.findById(skipTracingInputDTO.getAnalistaST()).orElse(null));

    if (skipTracingInputDTO.getBusquedas() != null){
      List<BusquedaSkipTracing> busquedas = busquedaSkipTracingRepository.findAllById(skipTracingInputDTO.getBusquedas());
      skipTracing.setBusquedas(new HashSet<>(busquedas));
    }


    return skipTracing;
  }
  public HistoricoSkipTracingDTO parseMap(Map map) {
    HistoricoSkipTracingDTO historicoSkipTracingDTO = new HistoricoSkipTracingDTO();
    Object timestamp = map.get("fecha");
    Object nivel = map.get("nivel");
    Object solicitud = map.get("solicitud");
    Object estado = map.get("estado");
    Object fechaInicio = map.get("fechaInicio");
    Object fechaFin = map.get("fechaFin");
    Object agencia = map.get("agencia");
    Object responsableCarteraId=map.get("responsableCarteraId");
  //  Object analistaST = map.get("analistaST");

    if (timestamp != null) {
      Timestamp valueFecha = new Timestamp(Long.parseLong(String.valueOf(timestamp)));
      historicoSkipTracingDTO.setFecha(new Date(valueFecha.getTime()));
    }
    if (fechaInicio != null)
      historicoSkipTracingDTO.setFechaGeneracion((Date) fechaInicio);
    if (fechaFin != null)
      historicoSkipTracingDTO.setFechaCierre((Date) fechaFin);
    if (estado != null)
      historicoSkipTracingDTO.setResultado(String.valueOf(estado));
    if (nivel != null)
      historicoSkipTracingDTO.setNivelBusqueda(String.valueOf(nivel));
    /*if (nivel != null && agencia != null) {
      // historicoSkipTracingDTO.setAgencia(String.valueOf(agencia));
    }*/
    if (nivel!=null && nivel.equals("Nivel bajo")){
      historicoSkipTracingDTO.setAgencia(String.valueOf(agencia));
    }
    if (nivel!=null && !nivel.equals("Nivel bajo")){
      historicoSkipTracingDTO.setAgencia(String.valueOf(solicitud));
    }

    if (responsableCarteraId!=null) {
      String responsable=responsableCarteraId.toString();
      Usuario usuario=usuarioRepository.findById(Integer.parseInt(responsable)).orElse(null);
      historicoSkipTracingDTO.setSolicitud(usuario.getNombre());
    //historicoSkipTracingDTO.setSolicitud(String.valueOf(solicitud));
    }

    return historicoSkipTracingDTO;
  }

  //Interviniente
  public IntervinienteST parseToST(ContratoInterviniente ci) {
    Interviniente inte = ci.getInterviniente();
    IntervinienteST result = intervinienteSTRepository.findByIntervinienteId(inte.getId()).orElse(null);
    if (result == null){
      result = new IntervinienteST();
      BeanUtils.copyProperties(inte, result, "id");

      result.setInterviniente(inte);
      result.setEstado(ci.getActivo());
      result.setTipoIntervencion(ci.getTipoIntervencion());
      result.setOrdenIntervencion(ci.getOrdenIntervencion());
    }


    return result;
  }
  public IntervinienteSTOutputDTOToList parseToOutputList(IntervinienteST i) {
    IntervinienteSTOutputDTOToList result = new IntervinienteSTOutputDTOToList();
    if (i == null) return result;

    result.setTipoIntervencion(i.getTipoIntervencion() != null ? new CatalogoMinInfoDTO(i.getTipoIntervencion()) : null);
    result.setOrdenIntervencion(i.getOrdenIntervencion());

    result.setId(i.getId());
    String nombre = i.getNombre() != null ? i.getNombre() : "";
    String apellidos = i.getApellidos() != null ? i.getApellidos() : "";

    if (i.getPersonaJuridica()!=null && i.getPersonaJuridica()){
      result.setNombre(i.getRazonSocial()!=null ? i.getRazonSocial():"");
    }else{
      result.setNombre(nombre + " " + apellidos);
    }

    if(i.getFechaNacimiento() != null) {
      LocalDate birthDate = Instant.ofEpochMilli(i.getFechaNacimiento().getTime())
        .atZone(ZoneId.systemDefault())
        .toLocalDate();

      LocalDate now = LocalDate.now();
      Period diff = Period.between(birthDate, now);
      result.setEdad(diff.getYears());
    }


    result.setNif(i.getNumeroDocumento());
    //result.setRazonSocial(i.getRazonSocial()!=null ? i.getRazonSocial():"");

    //result.setEdad(calcularEdad(i.getFechaNacimiento()));
    result.setNacionalidad(i.getNacionalidad() != null ? new CatalogoMinInfoDTO(i.getNacionalidad()) : null);

    result.setEstado(i.getEstado());

    for (DatoContactoST dc : i.getDatosContactoST()){
      TipoContacto tc = dc.getTipoContacto();
      if (tc != null){
        switch (tc.getCodigo()){
          case ("TEL_MOV"):
            if (result.getTValidado() == null){
              result.setTelefono(dc.getValor());
              result.setTValidado(dc.getValidado() != null ? dc.getValidado() ? "Si" : "No" : "Pendiente");
            }
            break;
          case ("MAIL"):
            if (result.getEValidado() == null){
              result.setEmail(dc.getValor());
              result.setEValidado(dc.getValidado() != null ? dc.getValidado() ? "Si" : "No" : "Pendiente");
            }
            break;
        }
      }
    }

    return result;
  }
  public IntervinienteST parseToEntity(IntervinienteST intervinienteST, IntervinienteSkipTracingInputDTO input) throws NotFoundException {

    intervinienteST.setDatosAdicionales1(input.getDatosAdicionales1());
    intervinienteST.setDatosAdicionales2(input.getDatosAdicionales2());

    intervinienteST.setNumeroDocumento(input.getNumeroDocumento()
      != null ? input.getNumeroDocumento() : null);
   /* if (input.getPersonaJuridica() != null) {
      intervinienteST.setPersonaJuridica(
        Boolean.TRUE.equals(input.getPersonaJuridica() ? "SÍ" : "NO"));
    }*/
    intervinienteST.setPersonaJuridica(input.getPersonaJuridica()!=null ? input.getPersonaJuridica():null);
    intervinienteST.setNombre(input.getNombre()!=null ? input.getNombre():null);
    intervinienteST.setApellidos(input.getApellidos()!=null ? input.getApellidos():null);
    intervinienteST.setFechaNacimiento(input.getFechaNacimiento()!=null ? input.getFechaNacimiento():null);
    intervinienteST.setOrdenIntervencion(input.getOrdenIntervencion()!=null ? input.getOrdenIntervencion():null);
    intervinienteST.setEstado(input.getEstado()!=null ? input.getEstado():null);
    if (input.getTipoContacto()!=null){
      intervinienteST.setTipoContacto(tipoContactoRepository.findById(input.getTipoContacto()).orElseThrow(
        ()->new NotFoundException("tipo contacto", input.getTipoContacto())));
    }
    if (input.getTipoDocumento()!=null){
      intervinienteST.setTipoDocumento(tipoDocumentoRepository.findById(input.getTipoDocumento()).orElseThrow(
        ()->new NotFoundException("tipo documento", input.getTipoDocumento())));
    }
    if (input.getTipoIntervencion()!=null){
      intervinienteST.setTipoIntervencion(tipoIntervencionRepository.findById(input.getTipoIntervencion()).orElseThrow(
        ()->new NotFoundException("tipo intervención", input.getTipoIntervencion())));
    }
    if (input.getResidencia()!=null){
      intervinienteST.setResidencia(paisRepository.findById(input.getResidencia()).orElseThrow(
        ()->new NotFoundException("país", input.getResidencia())));
    }
    if (input.getSexo()!=null){
      intervinienteST.setSexo(sexoRepository.findById(input.getSexo()).orElseThrow(
        ()-> new NotFoundException("género", input.getSexo())));
    }
    if (input.getEstadoCivil() != null){
      intervinienteST.setEstadoCivil(estadoCivilRepository.findById(input.getEstadoCivil()).orElseThrow(
        ()-> new NotFoundException("estado civil", input.getEstadoCivil())));
    }
    if (input.getNacionalidad() != null){
      intervinienteST.setNacionalidad(
        paisRepository.findById(input.getNacionalidad()).orElse(null));
    }
    if (input.getPaisNacimiento() != null){
      intervinienteST.setPaisNacimiento(
        paisRepository.findById(input.getPaisNacimiento()).orElse(null));
    }
    if (input.getTipoGestor() != null){
      intervinienteST.setTipoGestor(perfilRepository.findById(input.getTipoGestor()).orElseThrow(
        ()-> new NotFoundException("tipo gestor", input.getTipoGestor())));
    }
    intervinienteST.setContactado(input.getContactado()!=null ? input.getContactado():null);
    intervinienteST.setRazonSocial(input.getRazonSocial()!=null ? input.getRazonSocial():null);
    intervinienteST.setFechaConstitucion(input.getFechaConstitucion()!=null ? input.getFechaConstitucion():null);

    return intervinienteSTRepository.save(intervinienteST);
  }
  public void anadirDatos(SkipTracingOutputDTO out, IntervinienteST iST){
    List<DatoContactoST>datoContactoSTList = new ArrayList<>(iST.getDatosContactoST());
    List<DatoDireccionST>datoDireccionSTList = new ArrayList<>(iST.getDatosDireccionST());


    List<DatoDireccionSkipTracingDTO>datoDireccionSkipTracingDTOList = new ArrayList<>();
    List<DatoContactoSkipTracingDTO>datoContactoSkipTracingDTOList = new ArrayList<>();
    List<DatoContactoSkipTracingDTO>emails = new ArrayList<>();
    List<DatoContactoSkipTracingDTO>redes = new ArrayList<>();

    for (DatoDireccionST datoDireccionST:datoDireccionSTList) {
      DatoDireccionSkipTracingDTO datoDireccionSkipTracingDTO = parseToOutput(datoDireccionST);
      datoDireccionSkipTracingDTOList.add(datoDireccionSkipTracingDTO);
    }
    for (DatoContactoST datoContactoST:datoContactoSTList) {
      TipoContacto tipoContacto=datoContactoST.getTipoContacto();
      if (tipoContacto != null) {
        if (tipoContacto.getCodigo().equals("TEL_FIJ")
          || tipoContacto.getCodigo().equals("TEL_MOV")) {
          DatoContactoSkipTracingDTO datoContactoSkipTracingDTO =
            parseToOutput(datoContactoST);
          datoContactoSkipTracingDTOList.add(datoContactoSkipTracingDTO);
        }
        if (tipoContacto.getCodigo().equals("MAIL")) {
          DatoContactoSkipTracingDTO datoContactoSkipTracingDTO =
            parseToOutput(datoContactoST);
          emails.add(datoContactoSkipTracingDTO);
        }
        if (tipoContacto.getCodigo().equals("SOC")) {
          DatoContactoSkipTracingDTO datoContactoSkipTracingDTO =
            parseToOutput(datoContactoST);
          redes.add(datoContactoSkipTracingDTO);
        }
      }
    }
    out.setRedes(redes);
    out.setEmails(emails);
    out.setDatoContactoSkipTracingDTOList(datoContactoSkipTracingDTOList);
    out.setDatoDireccionSkipTracingDTOList(datoDireccionSkipTracingDTOList);
  }
  public IntervinienteSkipTracingDTO parseToOutput(IntervinienteST intervinienteST) {
    IntervinienteSkipTracingDTO intervinienteSTDTO=new IntervinienteSkipTracingDTO();
    intervinienteSTDTO.setId(intervinienteST.getId());
    intervinienteSTDTO.setNombre(intervinienteST.getNombre()!=null ? intervinienteST.getNombre():null);
    intervinienteSTDTO.setApellidos(intervinienteST.getApellidos()!=null ?intervinienteST.getApellidos():null);
    intervinienteSTDTO.setNumeroDocumento(intervinienteST.getNumeroDocumento()!=null ? intervinienteST.getNumeroDocumento():null);
    intervinienteSTDTO.setPersonaJuridica(intervinienteST.getPersonaJuridica()!=null ? intervinienteST.getPersonaJuridica():null);

    if (intervinienteST.getPersonaJuridica()){
      intervinienteSTDTO.setPrimerTitular(intervinienteST.getRazonSocial());
    }else{
      intervinienteSTDTO.setPrimerTitular(intervinienteST.getNombre()!=null && intervinienteST.getApellidos()!=null ? intervinienteST.getNombre()+" "+intervinienteST.getApellidos():null);
    }



    intervinienteSTDTO.setOrdenIntervencion(intervinienteST.getOrdenIntervencion());
    intervinienteSTDTO.setFechaNacimiento(intervinienteST.getFechaNacimiento());
    intervinienteSTDTO.setContactado(intervinienteST.getContactado());

    intervinienteSTDTO.setDatosAdicionales1(intervinienteST.getDatosAdicionales1());
    intervinienteSTDTO.setDatosAdicionales2(intervinienteST.getDatosAdicionales2());

    intervinienteSTDTO.setEstado(Boolean.TRUE.equals(intervinienteST.getEstado())?true:false);

    if (intervinienteST.getInterviniente() != null)
      intervinienteSTDTO.setIntervinienteOrigen(new IntervinienteDTOToList(intervinienteST.getInterviniente(), null));

    intervinienteSTDTO.setTipoDocumento(intervinienteST.getTipoDocumento()!=null? new CatalogoMinInfoDTO(intervinienteST.getTipoDocumento()):null);
    intervinienteSTDTO.setTipoIntervencion(intervinienteST.getTipoIntervencion()!=null ? new CatalogoMinInfoDTO(intervinienteST.getTipoIntervencion()):null);
    intervinienteSTDTO.setTipoContacto(intervinienteST.getTipoContacto()!=null ? new CatalogoMinInfoDTO(intervinienteST.getTipoContacto()):null);

    intervinienteSTDTO.setSexo(intervinienteST.getSexo()!=null ? new CatalogoMinInfoDTO(intervinienteST.getSexo()):null);
    intervinienteSTDTO.setEstadoCivil(intervinienteST.getEstadoCivil()!=null ? new CatalogoMinInfoDTO(intervinienteST.getEstadoCivil()):null);
    intervinienteSTDTO.setResidencia(intervinienteST.getResidencia()!=null ? new CatalogoMinInfoDTO(intervinienteST.getResidencia()):null);

    intervinienteSTDTO.setNacionalidad(intervinienteST.getNacionalidad()!=null?new CatalogoMinInfoDTO(intervinienteST.getNacionalidad()):null);
    intervinienteSTDTO.setPaisNacimiento(intervinienteST.getPaisNacimiento()!=null?new CatalogoMinInfoDTO(intervinienteST.getPaisNacimiento()):null);

    List<DatoContactoST>datoContactoSTList= new ArrayList<>(intervinienteST.getDatosContactoST());
    List<DatoDireccionST>datoDireccionSTList= new ArrayList<>(intervinienteST.getDatosDireccionST());


    List<DatoDireccionSkipTracingDTO>datoDireccionSkipTracingDTOList=new ArrayList<>();
    List<DatoContactoSkipTracingDTO>datoContactoSkipTracingDTOList=new ArrayList<>();
    List<DatoContactoSkipTracingDTO>emails=new ArrayList<>();
    List<DatoContactoSkipTracingDTO>redes=new ArrayList<>();


    for (DatoDireccionST datoDireccionST:datoDireccionSTList) {
      DatoDireccionSkipTracingDTO datoDireccionSkipTracingDTO = parseToOutput(datoDireccionST);
      datoDireccionSkipTracingDTOList.add(datoDireccionSkipTracingDTO);
    }
    for (DatoContactoST datoContactoST:datoContactoSTList) {
      TipoContacto tipoContacto=datoContactoST.getTipoContacto();
      if (tipoContacto != null) {
        if (tipoContacto.getCodigo().equals("TEL_FIJ")
          || tipoContacto.getCodigo().equals("TEL_MOV")) {
          DatoContactoSkipTracingDTO datoContactoSkipTracingDTO =
            parseToOutput(datoContactoST);
          datoContactoSkipTracingDTOList.add(datoContactoSkipTracingDTO);
        }
        if (tipoContacto.getCodigo().equals("MAIL")) {
          DatoContactoSkipTracingDTO datoContactoSkipTracingDTO =
            parseToOutput(datoContactoST);
          emails.add(datoContactoSkipTracingDTO);
        }
        if (tipoContacto.getCodigo().equals("SOC")) {
          DatoContactoSkipTracingDTO datoContactoSkipTracingDTO =
            parseToOutput(datoContactoST);
          redes.add(datoContactoSkipTracingDTO);
        }
      }
    }
    intervinienteSTDTO.setRedes(redes);
    intervinienteSTDTO.setEmails(emails);
    intervinienteSTDTO.setDatoContactoSkipTracingDTOList(datoContactoSkipTracingDTOList);
    intervinienteSTDTO.setDatoDireccionSkipTracingDTOList(datoDireccionSkipTracingDTOList);
    intervinienteSTDTO.setEstado(intervinienteST.getEstado());
    intervinienteSTDTO.setRazonSocial(intervinienteST.getRazonSocial());
    intervinienteSTDTO.setFechaConstitucion(intervinienteST.getFechaConstitucion());
    intervinienteSTDTO.setTipoGestor(intervinienteST.getTipoGestor()!=null?intervinienteST.getTipoGestor().getId():null);

    //   intervinienteSTDTO.setDatoContactoSkipTracingDTOList(datoContactoSkipTracingDTOList);
    // this.estado = subasta.getEstado() != null ? new CatalogoMinInfoDTO(subasta.getEstado()) : null;
    //  BeanUtils.copyProperties(intervinienteST, intervinienteSTDTO);
    return intervinienteSTDTO;
  }

  //DatoDireccion
  public DatoDireccionST parseToST(Direccion direccion, Contrato contrato) throws NotFoundException {
    DatoDireccionST result = datoDireccionSTRepository.findByDatoDireccionId(direccion.getId()).orElse(null);
    if (result == null){
      result = new DatoDireccionST();
      BeanUtils.copyProperties(direccion, result, "id");
      result.setDatoDireccion(direccion);
      result.setPrincipal(direccion.getPrincipal());
      OrigenDato origenDato = origenDatoRepository.findByCodigo("INTE").orElseThrow(() ->
        new NotFoundException("OrigenDato", "código", "'INTE'"));
      DatosOrigen od = result.getDatosOrigen();
      if (od == null){
        od = new DatosOrigen();
        od.setOrigenDato(origenDato);
      }
      od.setFechaActualizacion(new Date());

      result.setDatosOrigen(od);
    }
    //Pillo todos los bienes activos (una llamada al bienRepository) //Cuando salga primera finca hipotecada vale se acabó
//Buscas por el id de los contratos es un objetoContratoiBien findByContratosContratoId(contrato.getid)
  //  List<Bien>bienList=bienRepository.findByContratosId(contrato.getId());
    List<ContratoBien>contratoBienList=contrato.getBienes().stream().collect(Collectors.toList());
    for (ContratoBien contratoBien:contratoBienList) {
   Bien bien=contratoBien.getBien();
   // for (Bien bien:bienList) {
      if (bien.isMismaDireccion(direccion)){
        result.setFincaHipotecada(true);
        break;
      }
    }
    //if (result.getId() == null) result.setFechaAlta(new Date());
    return result;
  }
  public DatoDireccionST parseToEntity(DatoDireccionST datoDireccionST, DatoDireccionSTInputDTO datoDireccionSTInputDTO, Usuario gestor) throws NotFoundException {
    //datoDireccionST.setPrincipal(Boolean.TRUE.equals(datoDireccionSTInputDTO.getPrincipal()) ? true:false);
    datoDireccionST.setValidado(Boolean.TRUE.equals(datoDireccionSTInputDTO.getValidado())?true:false);
    if (datoDireccionST.getValidado() != null && datoDireccionST.getValidado()){
      datoDireccionST.setFechaValidacion(new Date());
    } else {
      datoDireccionST.setFechaValidacion(null);
    }
    if(datoDireccionSTInputDTO.getPrincipal()){
      IntervinienteST intervinienteST=datoDireccionST.getIntervinienteST();
      List<DatoDireccionST>datoDireccionSTList=intervinienteST.getDatosDireccionST().stream().collect(Collectors.toList());
      for (DatoDireccionST dsT:datoDireccionSTList) {
        dsT.setPrincipal(false);
        datoDireccionSTRepository.save(dsT);
      }
    }
    datoDireccionST.setPrincipal(datoDireccionSTInputDTO.getPrincipal());

    datoDireccionST.setNombre(datoDireccionSTInputDTO.getNombre());
    datoDireccionST.setBloque(datoDireccionSTInputDTO.getBloque());
    datoDireccionST.setEscalera(datoDireccionSTInputDTO.getEscalera());
    datoDireccionST.setPiso(datoDireccionSTInputDTO.getPiso());
    datoDireccionST.setPuerta(datoDireccionSTInputDTO.getPuerta());
    datoDireccionST.setComentario(datoDireccionSTInputDTO.getComentario());
    datoDireccionST.setCodigoPostal(datoDireccionSTInputDTO.getCodigoPostal());
    datoDireccionST.setLongitud(datoDireccionSTInputDTO.getLongitud());
    datoDireccionST.setLatitud(datoDireccionSTInputDTO.getLatitud());
    datoDireccionST.setPortal(datoDireccionSTInputDTO.getPortal());
    datoDireccionST.setNumero(datoDireccionSTInputDTO.getNumero());
    datoDireccionST.setObservaciones(datoDireccionSTInputDTO.getObservaciones());

    DatosOrigen dO = datoDireccionST.getDatosOrigen();
    if (dO == null) dO = new DatosOrigen();
    parseToEntity(dO, datoDireccionSTInputDTO.getDatosOrigen(), gestor);
    DatosOrigen doS = datosOrigenRepository.save(dO);
    datoDireccionST.setDatosOrigen(doS);
    /*if (datoDireccionST.getFechaAlta() == null){
      if (datoDireccionSTInputDTO.getFechaAlta() == null){
        datoDireccionST.setFechaAlta(new Date());
      } else {
        datoDireccionST.setFechaAlta(datoDireccionSTInputDTO.getFechaAlta());
      }
    }*/

    if (datoDireccionSTInputDTO.getMunicipio() != null) {
      Municipio municipio =
        municipioRepository.findById(datoDireccionSTInputDTO.getMunicipio()).orElse(null);
      if (municipio != null) datoDireccionST.setMunicipio(municipio);
    }
    if (datoDireccionSTInputDTO.getLocalidad()!=null){
      Localidad localidad=
        localidadRepository.findById(datoDireccionSTInputDTO.getLocalidad()).orElse(null);
      if (localidad!=null)datoDireccionST.setLocalidad(localidad);
    }
    if (datoDireccionSTInputDTO.getEntorno()!=null){
      Entorno entorno=entornoRepository.findById(datoDireccionSTInputDTO.getEntorno()).orElse(null);
      if (entorno!=null) datoDireccionST.setEntorno(entorno);
    }
    if (datoDireccionSTInputDTO.getProvincia()!=null){
      Provincia provincia=provinciaRepository.findById(datoDireccionSTInputDTO.getProvincia()).orElse(null);
      if (provincia!=null) datoDireccionST.setProvincia(provincia);
    }
    if (datoDireccionSTInputDTO.getPais()!=null){
      Pais pais=paisRepository.findById(datoDireccionSTInputDTO.getPais()).orElse(null);
      if (pais!=null) datoDireccionST.setPais(pais);
    }
    if (datoDireccionSTInputDTO.getTipoVia()!=null){
      TipoVia tipovia=tipoViaRepository.findById(datoDireccionSTInputDTO.getTipoVia()).orElse(null);
      if (tipovia!=null)datoDireccionST.setTipoVia(tipovia);
    }
    datoDireccionST.setObservaciones(datoDireccionSTInputDTO.getObservaciones());
    return datoDireccionST;
  }
  public DatoDireccionSkipTracingDTO parseToOutput(DatoDireccionST datoDireccionST) {
    DatoDireccionSkipTracingDTO devolver = new DatoDireccionSkipTracingDTO();
    BeanUtils.copyProperties(datoDireccionST, devolver);

    devolver.setPrincipal(datoDireccionST.getPrincipal());
    devolver.setValidado(datoDireccionST.getValidado());
    devolver.setFechaValidacion(datoDireccionST.getFechaValidacion());
    //devolver.setFechaAlta(datoDireccionST.getFechaAlta());

    devolver.setNombre(datoDireccionST.getNombre());
    devolver.setBloque(datoDireccionST.getBloque());
    devolver.setEscalera(datoDireccionST.getEscalera());
    devolver.setPiso(datoDireccionST.getPiso());
    devolver.setPuerta(datoDireccionST.getPuerta());
    devolver.setComentario(datoDireccionST.getComentario());
    devolver.setCodigoPostal(datoDireccionST.getCodigoPostal());
    devolver.setLongitud(datoDireccionST.getLongitud());
    devolver.setLatitud(datoDireccionST.getLatitud());
    devolver.setPortal(datoDireccionST.getPortal());
    devolver.setNumero(datoDireccionST.getNumero());

    devolver.setMunicipio(datoDireccionST.getMunicipio()!=null ? new CatalogoMinInfoDTO(datoDireccionST.getMunicipio()):null);
    devolver.setTipoVia(datoDireccionST.getTipoVia()!=null ? new CatalogoMinInfoDTO(datoDireccionST.getTipoVia()):null);
    devolver.setLocalidad(datoDireccionST.getLocalidad()!=null ? new CatalogoMinInfoDTO(datoDireccionST.getLocalidad()):null);
    devolver.setEntorno(datoDireccionST.getEntorno()!=null ? new CatalogoMinInfoDTO(datoDireccionST.getEntorno()):null);
    devolver.setProvincia(datoDireccionST.getProvincia()!=null ? new CatalogoMinInfoDTO(datoDireccionST.getProvincia()):null);
    devolver.setPais(datoDireccionST.getPais()!=null ? new CatalogoMinInfoDTO(datoDireccionST.getPais()):null);

    devolver.setIdIntervinienteST(datoDireccionST.getIntervinienteST()!=null ? datoDireccionST.getIntervinienteST().getId():null);
    devolver.setDatosOrigen(parseToOutput(datoDireccionST.getDatosOrigen()));

    devolver.setFincaHipotecada(datoDireccionST.getFincaHipotecada()!=null ? datoDireccionST.getFincaHipotecada():null);


    devolver.setFotos(new ArrayList<>(datoDireccionST.getFotos()));
    return devolver;
  }

  //DatoContacto
  public List<DatoContactoST> parseToST(DatoContacto datoContacto) throws NotFoundException {
    List<DatoContactoST> olds = datoContactoSTRepository.findAllByDatoContactoId(datoContacto.getId());
    OrigenDato origenDato = origenDatoRepository.findByCodigo("INTE").orElseThrow(() ->
      new NotFoundException("OrigenDato", "código", "'INTE'"));

    Boolean fijo = false;
    Boolean movil = false;
    Boolean email = false;
    Boolean social = false;

    for (DatoContactoST dcSTold : olds) {
      TipoContacto tc = dcSTold.getTipoContacto();
      switch (tc.getCodigo()) {
        case ("TEL_FIJ"):
          //dcSTold.setValor(datoContacto.getFijo());
          //dcSTold.setValidado(datoContacto.getFijoValidado());
          fijo = true;
          break;
        case ("TEL_MOV"):
          //dcSTold.setValor(datoContacto.getMovil());
          //dcSTold.setValidado(datoContacto.getMovilValidado());
          movil = true;
          break;
        case ("MAIL"):
          //dcSTold.setValor(datoContacto.getEmail());
          //dcSTold.setValidado(datoContacto.getEmailValidado());
          email = true;
          break;
        case ("SOC"):
          social = true;
          break;
        default:
          break;
      }

      DatosOrigen od = dcSTold.getDatosOrigen();
      if (od == null){
        od = new DatosOrigen();
        od.setOrigenDato(origenDato);
      }
      od.setFechaActualizacion(new Date());

      dcSTold.setDatosOrigen(od);
      if (datoContacto.getOrden() == 1) dcSTold.setPrincipal(true);

      dcSTold.setDatoContacto(datoContacto);
    }

    if (!fijo && datoContacto.getFijo() != null) {
      DatoContactoST dcSTfijo = new DatoContactoST();
      dcSTfijo.setValor(datoContacto.getFijo());
      dcSTfijo.setValidado(datoContacto.getFijoValidado());

      DatosOrigen od = new DatosOrigen();
      od.setFechaActualizacion(new Date());
      od.setOrigenDato(origenDato);

      dcSTfijo.setDatosOrigen(od);
      if (datoContacto.getOrden() == 1) dcSTfijo.setPrincipal(true);
      dcSTfijo.setDatoContacto(datoContacto);
      dcSTfijo.setTipoContacto(tipoContactoRepository.findByCodigo("TEL_FIJ").orElse(null));
      olds.add(dcSTfijo);
    }
    if (!movil && datoContacto.getMovil() != null) {
      DatoContactoST dcSTmovil = new DatoContactoST();
      dcSTmovil.setValor(datoContacto.getMovil());

      DatosOrigen od = new DatosOrigen();
      od.setFechaActualizacion(new Date());
      od.setOrigenDato(origenDato);

      dcSTmovil.setDatosOrigen(od);
      if (datoContacto.getOrden() == 1) dcSTmovil.setPrincipal(true);
      dcSTmovil.setDatoContacto(datoContacto);
      dcSTmovil.setTipoContacto(tipoContactoRepository.findByCodigo("TEL_MOV").orElse(null));
      olds.add(dcSTmovil);
    }
    if (!email && datoContacto.getEmail() != null) {
      DatoContactoST dcSTmail = new DatoContactoST();
      dcSTmail.setValor(datoContacto.getEmail());
      dcSTmail.setValidado(datoContacto.getEmailValidado());

      DatosOrigen od = new DatosOrigen();
      od.setFechaActualizacion(new Date());
      od.setOrigenDato(origenDato);

      dcSTmail.setDatosOrigen(od);
      if (datoContacto.getOrden() == 1) dcSTmail.setPrincipal(true);
      dcSTmail.setDatoContacto(datoContacto);
      dcSTmail.setTipoContacto(tipoContactoRepository.findByCodigo("MAIL").orElse(null));
      olds.add(dcSTmail);
    }

    return olds;
  }
  public DatoContactoST parseToEntity(DatoContactoST dc, DatoContactoSTInputDTO dcIDTO, Usuario gestor,Integer intervinienteSTid) throws NotFoundException {
    if (dcIDTO.getTipoContacto() != null){
      TipoContacto tipoContacto = tipoContactoRepository.findById(dcIDTO.getTipoContacto()).orElse(null);
      if (tipoContacto != null) dc.setTipoContacto(tipoContacto);
    }
    dc.setValidado(dcIDTO.getValidado());
    //Revisar si esta a null el principal del dc (Dato COntacto) si lo tiene a null si no tiene datoscontactos para x tipo entonces lo pongo como principal si no
    //Busco los datoContactoST (tipoContacto) y luego reviso si tienen el campo principal a null

    DatoContactoST telefonosFijos=datoContactoSTRepository.findByTipoContactoIdAndIntervinienteSTIdAndPrincipalIsTrue(1,intervinienteSTid);
    DatoContactoST telefonosMoviles=datoContactoSTRepository.findByTipoContactoIdAndIntervinienteSTIdAndPrincipalIsTrue(2,intervinienteSTid);
    DatoContactoST email=datoContactoSTRepository.findByTipoContactoIdAndIntervinienteSTIdAndPrincipalIsTrue(3,intervinienteSTid);
    DatoContactoST redesSociales=datoContactoSTRepository.findByTipoContactoIdAndIntervinienteSTIdAndPrincipalIsTrue(4,intervinienteSTid);
    //Esto separarlo
    if (dcIDTO.getTipoContacto() == 1){
      if ((telefonosFijos!=null && telefonosFijos.getPrincipal())){
        dc.setPrincipal(false);
      }else{
        dc.setPrincipal(true);
      }
    }
    if (dcIDTO.getTipoContacto()==2){
     if (telefonosMoviles!=null && telefonosMoviles.getPrincipal()){
       dc.setPrincipal(false);
     }else{
       dc.setPrincipal(true);
     }
    }
    if (dcIDTO.getTipoContacto() == 3) {
      if (email != null && email.getPrincipal()) {
        dc.setPrincipal(false);
      } else {
        dc.setPrincipal(true);
      }
      }
    if (dcIDTO.getTipoContacto() == 4) {
      if (redesSociales != null && redesSociales.getPrincipal()) {
        dc.setPrincipal(false);
      } else {
        dc.setPrincipal(true);
      }
      }
    //dc.setPrincipal(dcIDTO.getPrincipal());
    if (dc.getValidado() != null && dc.getValidado()){
      dc.setFechaValidacion(new Date());
    } else {
      dc.setFechaValidacion(null);
    }
    dc.setValor(dcIDTO.getValor());

    DatosOrigen dO = dc.getDatosOrigen();
    if (dO == null) dO = new DatosOrigen();
    parseToEntity(dO, dcIDTO.getDatosOrigen(), gestor);
    DatosOrigen doS = datosOrigenRepository.save(dO);
    dc.setDatosOrigen(doS);

    return dc;
  }
  public DatoContactoSkipTracingDTO parseToOutput(DatoContactoST datoContactoST) {

    DatoContactoSkipTracingDTO devolver = new DatoContactoSkipTracingDTO();
    BeanUtils.copyProperties(datoContactoST, devolver);

    devolver.setTipoContacto(datoContactoST.getTipoContacto()!=null ? new CatalogoMinInfoDTO(datoContactoST.getTipoContacto()):null);
    devolver.setValidado(datoContactoST.getValidado()!=null ? datoContactoST.getValidado() ? true:false:null);

    devolver.setPrincipal(Boolean.TRUE.equals(datoContactoST.getPrincipal())?true:false);
    devolver.setValor(datoContactoST.getValor());
    devolver.setFechaValidacion(datoContactoST.getFechaValidacion());
    devolver.setFechaAlta(datoContactoST.getDatosOrigen().getFecha());
    devolver.setIdIntervinienteST(datoContactoST.getIntervinienteST()!=null ? datoContactoST.getIntervinienteST().getId():null);
    devolver.setDatosOrigen(parseToOutput(datoContactoST.getDatosOrigen()));
    devolver .setFotos(new ArrayList<>(datoContactoST.getFotos()));
    return devolver;
  }

  //Datos Origen
  public void parseToEntity(DatosOrigen dor, DatosOrigenInputDTO doIDTO, Usuario usuario) throws NotFoundException {
    dor.setGestor(usuario);
    if (doIDTO.getFechaActualizacion()!=null) {
      dor.setFechaActualizacion(doIDTO.getFechaActualizacion());
    }else{
      dor.setFechaActualizacion(new Date());
    }
    dor.setLink(doIDTO.getLink());
    if (doIDTO.getFecha() != null) dor.setFecha(doIDTO.getFecha());
    else dor.setFecha(new Date());
    dor.setObservaciones(doIDTO.getObservaciones());
    dor.setOtraFuente(doIDTO.getOtraFuente());

    if (doIDTO.getOrigenDato() != null)
      dor.setOrigenDato(origenDatoRepository.findById(doIDTO.getOrigenDato()).orElseThrow(() ->
        new NotFoundException("OrigenDato", doIDTO.getOrigenDato())));
    else dor.setOrigenDato(origenDatoRepository.findByCodigo("A_ST").orElseThrow(() ->
      new NotFoundException("OrigenDato", "código", "'A_ST'")));
    if (doIDTO.getPaginaPublica() != null)
      dor.setPaginaPublica(paginaPublicaRepository.findById(doIDTO.getPaginaPublica()).orElseThrow(() ->
        new NotFoundException("PaginaPublica", doIDTO.getPaginaPublica())));
    else dor.setPaginaPublica(null);
    if (doIDTO.getRedSocial() != null)
      dor.setRedSocial(redSocialRepository.findById(doIDTO.getRedSocial()).orElseThrow(() ->
        new NotFoundException("RedSocial", doIDTO.getRedSocial())));
    else dor.setRedSocial(null);
    if (doIDTO.getPerfilProfesional() != null)
      dor.setPerfilProfesional(perfilProfesionalRepository.findById(doIDTO.getPerfilProfesional()).orElseThrow(() ->
        new NotFoundException("PerfilProfesional", doIDTO.getOrigenDato())));
    else dor.setPerfilProfesional(null);

    if (dor.getPaginaPublica() == null && dor.getRedSocial() == null && dor.getPerfilProfesional() == null && dor.getOtraFuente() == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("At least one origin is required: Public Page, Social Network, Professional Profile, or other Origin");
      else throw new NotFoundException("Se requiere al menos un origen: PaginaPublica, RedSocial, PerfilProfesional, u otroOrigen");
    }
  }
  public DatosOrigenOutputDTO parseToOutput(DatosOrigen dor){
    DatosOrigenOutputDTO doODTO = new DatosOrigenOutputDTO();
    if (dor == null) return doODTO;
    doODTO.setId(dor.getId());
    doODTO.setLink(dor.getLink());
    doODTO.setFecha(dor.getFecha());
    doODTO.setFechaActualizacion(dor.getFechaActualizacion());
    doODTO.setObservaciones(dor.getObservaciones());
    doODTO.setOtraFuente(dor.getOtraFuente());
    doODTO.setGestor(dor.getGestor() != null ? new UsuarioAgendaDto(dor.getGestor()) : null);
    doODTO.setOrigenDato(dor.getOrigenDato() != null ? new CatalogoMinInfoDTO(dor.getOrigenDato()) : null);
    doODTO.setPaginaPublica(dor.getPaginaPublica() != null ? new CatalogoMinInfoDTO(dor.getPaginaPublica()) : null);
    doODTO.setPerfilProfesional(dor.getPerfilProfesional() != null ? new CatalogoMinInfoDTO(dor.getPerfilProfesional()) : null);
    doODTO.setRedSocial(dor.getRedSocial() != null ? new CatalogoMinInfoDTO(dor.getRedSocial()) : null);
    doODTO.setOtraFuente(dor.getOtraFuente());

    if (doODTO.getPaginaPublica() != null) doODTO.setFuenteFinal(doODTO.getPaginaPublica().getValor());
    else if (doODTO.getRedSocial() != null) doODTO.setFuenteFinal(doODTO.getRedSocial().getValor());
    else if (doODTO.getPerfilProfesional() != null) doODTO.setFuenteFinal(doODTO.getPerfilProfesional().getValor());
    else doODTO.setFuenteFinal(doODTO.getOtraFuente());

    return doODTO;
  }

  //CargaST
  public void parseToEntity(CargaST c, CargaSTInputDTO input){
    c.setFechaInput(input.getFecha());
    c.setFechaActualizacion(new Date());
    c.setEncargado(input.getEncargado());
    c.setValidado(input.getValidado());

    if (input.getBusquedas() != null){
      List<BusquedaSkipTracing> busquedas = busquedaSkipTracingRepository.findAllById(input.getBusquedas());
      c.setBusquedas(new HashSet<>(busquedas));
    }
    c.setComentario(input.getComentario());
  }
  public CargaSTOutputDTO parseToOutput(CargaST c){
    CargaSTOutputDTO output = new CargaSTOutputDTO();
    if (c == null) return null;

    BeanUtils.copyProperties(c, output);

    output.setBusquedas(c.getBusquedas().stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList()));

    return output;
  }
}
