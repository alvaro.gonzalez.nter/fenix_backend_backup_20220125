package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class IntervinienteSTOutputDTOToList implements Serializable {

  private Integer id;
  private String nombre;
  private String nif;
  private CatalogoMinInfoDTO tipoIntervencion;
  private Integer ordenIntervencion;
  private String telefono;
  private String tValidado; //Boolean -> true "Si", false "No", null "Pendiente"
  private String email;
  private String eValidado; //Boolean -> true "Si", false "No", null "Pendiente"
  private Integer edad;
  private Boolean estado;
  private CatalogoMinInfoDTO nacionalidad;
  private String razonSocial;
}
