package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class IntervinienteSkipTracingInputDTO implements Serializable {
  private String numeroDocumento;
  private Boolean personaJuridica;
  private String nombre; //Nombre
  private String apellidos;//Apellidos

  private Integer sexo;//Sexo
  private Integer estadoCivil;//Estado Civil
  private Integer nacionalidad;//Nacionalidad
  private Integer paisNacimiento;//Pais nacimiento
  private Boolean contactado;  //Contactado???
  private Integer tipoContacto;//TipoContacto nuevo catalogo
  private Integer residencia; //Residente tipoPais catalogo
  private Boolean estado; //Activo del interviniente en la relacion ContratoInterviniente
  private Date fechaNacimiento;

  private Integer ordenIntervencion;


  private Set<DatoContactoSTInputDTO> datosContactoST;
  private Set<DatoDireccionSTInputDTO> direccionesST;

  private Integer tipoDocumento;
  private Integer tipoIntervencion;

  private String datosAdicionales1;
  private String datosAdicionales2;
  private String razonSocial;
  private Date fechaConstitucion;
  private Integer tipoGestor;
}

