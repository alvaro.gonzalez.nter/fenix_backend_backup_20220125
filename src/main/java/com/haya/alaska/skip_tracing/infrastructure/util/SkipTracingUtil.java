package com.haya.alaska.skip_tracing.infrastructure.util;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente_;
import com.haya.alaska.busqueda.domain.BusquedaBuilderST;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.*;
import com.haya.alaska.carga_skip_tracing.domain.CargaST;
import com.haya.alaska.carga_skip_tracing.domain.CargaST_;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.catalogo.domain.Catalogo_;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.domain.Cliente_;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera_;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.domain.ContratoBien_;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente_;
import com.haya.alaska.dato_contacto_skip_tracing.domain.DatoContactoST;
import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import com.haya.alaska.dato_direccion_skip_tracing.infraestructure.repository.DatoDireccionSTRepository;
import com.haya.alaska.datos_origen.domain.DatosOrigen;
import com.haya.alaska.datos_origen.infrastructure.repository.DatosOrigenRepository;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.estado_civil.domain.EstadoCivil;
import com.haya.alaska.estado_civil.infrastructure.repository.EstadoCivilRepository;
import com.haya.alaska.estado_skip_tracing.domain.EstadoSkipTracing_;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.hito_procedimiento.infrastructure.repository.HitoProcedimientoRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.domain.Interviniente_;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import com.haya.alaska.interviniente_skip_tracing.infraestructure.repository.IntervinienteSTRepository;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.nivel_skip_tracing.domain.NivelSkipTracing_;
import com.haya.alaska.origen_dato.infrastructure.repository.OrigenDatoRepository;
import com.haya.alaska.pagina_publica.infrastructure.repository.PaginaPublicaRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.domain.Perfil_;
import com.haya.alaska.perfil.infrastructure.repository.PerfilRepository;
import com.haya.alaska.perfil_profesional.infrastructure.repository.PerfilProfesionalRepository;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.red_social.infrastructure.repository.RedSocialRepository;
import com.haya.alaska.sexo.domain.Sexo;
import com.haya.alaska.sexo.infrastructure.repository.SexoRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.skip_tracing.domain.SkipTracing_;
import com.haya.alaska.skip_tracing.infrastructure.controller.dto.*;
import com.haya.alaska.skip_tracing.infrastructure.repository.SkipTracingRepository;
import com.haya.alaska.tipo_busqueda.domain.TipoBusqueda_;
import com.haya.alaska.tipo_contacto.domain.TipoContacto;
import com.haya.alaska.tipo_contacto.infraestructure.repository.TipoContactoRepository;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class SkipTracingUtil {
  @PersistenceContext
  private EntityManager entityManager;
  @Autowired
  SkipTracingRepository skipTracingRepository;
  @Autowired
  IntervinienteSTRepository intervinienteSTRepository;
  @Autowired
  TipoContactoRepository tipoContactoRepository;
  @Autowired
  TipoViaRepository tipoViaRepository;
  @Autowired
  EntornoRepository entornoRepository;
  @Autowired
  MunicipioRepository municipioRepository;
  @Autowired
  LocalidadRepository localidadRepository;
  @Autowired
  ProvinciaRepository provinciaRepository;
  @Autowired
  PaisRepository paisRepository;
  @Autowired
  private EventoUtil eventoUtil;
  @Autowired
  private EstadoCivilRepository estadoCivilRepository;
  @Autowired
  private SexoRepository sexoRepository;
  @Autowired
  private TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired
  private HitoProcedimientoRepository hitoProcedimientoRepository;
  @Autowired
  OrigenDatoRepository origenDatoRepository;
  @Autowired
  PaginaPublicaRepository paginaPublicaRepository;
  @Autowired
  RedSocialRepository redSocialRepository;
  @Autowired
  PerfilProfesionalRepository perfilProfesionalRepository;
  @Autowired
  DatosOrigenRepository datosOrigenRepository;
  @Autowired
  PerfilRepository perfilRepository;
  @Autowired
  DatoDireccionSTRepository datoDireccionSTRepository;

  public List<SkipTracing> filter(
    Usuario logger, String cliente, String cartera, String idExpediente, Integer idContrato, Integer idCarga, String producto, Integer nIntervinientes,
    Integer nGarantias, String estado, String nivel, String intervinientePrincipal, String tipoIntervencion,
    Boolean judicial, String provincia, String analistaST, String gestorExpediente, Double saldoGestion, String fechaSolicitud, String fechaAsignacion, String fechaInicioBusqueda, String fechaFinBusqueda,
    String orderField, String orderDirection)
  {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<SkipTracing> query = cb.createQuery(SkipTracing.class);

    Root<SkipTracing> root = query.from(SkipTracing.class);
    List<Predicate> predicates = new ArrayList<>();

    Join<SkipTracing, ContratoInterviniente> ciJoin;
    Join<SkipTracing, CargaST> carJoin;
    Join<SkipTracing, Usuario> uJoin;
    Join<ContratoInterviniente, Contrato> cJoin;
    Join<ContratoInterviniente, Interviniente> iJoin;
    Join<Contrato, Expediente> eJoin;
    Join<Expediente, AsignacionExpediente> aeJoin;
    Join<AsignacionExpediente, Usuario> aeUJoin;
    Join<Expediente, Cartera> caJoin;
    Join<Cartera, ClienteCartera> ccJoin;
    Join<ClienteCartera, Cliente> clJoin;

    /*Join<SkipTracing, ContratoInterviniente> ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
    Join<SkipTracing, Usuario> uJoin = root.join(SkipTracing_.analistaST, JoinType.INNER);
    Join<ContratoInterviniente, Contrato> cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
    Join<ContratoInterviniente, Interviniente> iJoin = ciJoin.join(ContratoInterviniente_.interviniente, JoinType.INNER);
    Join<Contrato, Expediente> eJoin = cJoin.join(Contrato_.expediente, JoinType.INNER);
    Join<Expediente, AsignacionExpediente> aeJoin = eJoin.join(Expediente_.asignaciones, JoinType.INNER);
    Join<AsignacionExpediente, Usuario> aeUJoin = aeJoin.join(AsignacionExpediente_.usuario, JoinType.INNER);
    Join<Expediente, Cartera> caJoin = eJoin.join(Expediente_.cartera, JoinType.INNER);
    Join<Cartera, ClienteCartera> ccJoin = caJoin.join(Cartera_.clientes, JoinType.INNER);
    Join<ClienteCartera, Cliente> clJoin = ccJoin.join(ClienteCartera_.cliente, JoinType.INNER);*/

    if (cliente != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      eJoin = cJoin.join(Contrato_.expediente, JoinType.INNER);
      caJoin = eJoin.join(Expediente_.cartera, JoinType.INNER);
      ccJoin = caJoin.join(Cartera_.clientes, JoinType.INNER);
      clJoin = ccJoin.join(ClienteCartera_.cliente, JoinType.INNER);
      predicates.add(cb.like(clJoin.get(Cliente_.NOMBRE), "%" + cliente + "%"));
    }
    if (cartera != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      eJoin = cJoin.join(Contrato_.expediente, JoinType.INNER);
      caJoin = eJoin.join(Expediente_.cartera, JoinType.INNER);
      predicates.add(cb.like(caJoin.get(Cartera_.NOMBRE), "%" + cartera + "%"));
    }
    if (idExpediente != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      eJoin = cJoin.join(Contrato_.expediente, JoinType.INNER);
      predicates.add(cb.like(eJoin.get(Expediente_.idConcatenado), "%" + idExpediente + "%"));
    }
    if (idContrato != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      predicates.add(cb.like(cJoin.get(Contrato_.id).as(String.class), "%" + idContrato + "%"));
    }
    if (idCarga != null){
      carJoin = root.join(SkipTracing_.cargaST, JoinType.INNER);
      predicates.add(cb.like(carJoin.get(CargaST_.id).as(String.class), "%" + idCarga + "%"));
    }
    if (producto != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      Join<Contrato, Producto> cpJoin = cJoin.join(Contrato_.PRODUCTO, JoinType.INNER);
      Predicate onCond = cb.like(cpJoin.get(Catalogo_.VALOR), "%" + producto + "%");
      cpJoin.on(onCond);
    }
    if (nIntervinientes != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      Subquery<Long> subqueryI = cb.createQuery().subquery(Long.class);
      Root<ContratoInterviniente> fromCI = subqueryI.from(ContratoInterviniente.class);
      subqueryI.select(cb.count(fromCI)).distinct(true).where(cb.equal(cJoin.get(Contrato_.ID), fromCI.get(ContratoInterviniente_.CONTRATO).get(Contrato_.ID)));
      predicates.add(cb.like(subqueryI.as(String.class), "%" + nIntervinientes + "%"));
    }
    if (nGarantias != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      Subquery<Long> subqueryG = cb.createQuery().subquery(Long.class);
      Root<ContratoBien> fromCB = subqueryG.from(ContratoBien.class);
      subqueryG.select(cb.count(fromCB)).distinct(true).where(cb.equal(cJoin.get(Contrato_.ID), fromCB.get(ContratoBien_.CONTRATO).get(Contrato_.ID)));
      predicates.add(cb.like(subqueryG.as(String.class), "%" + nGarantias + "%"));
    }
   // addOnCond(cb, root, SkipTracing_.estadoST, estado);
    addOnCond(cb, root, SkipTracing_.nivelST, nivel);

    if (estado!=null){
      Predicate estPr = cb.or(
        cb.like(root.get(SkipTracing_.estadoST).get(EstadoSkipTracing_.valor),"%" + estado + "%"));
      predicates.add(estPr);
    }


    if (intervinientePrincipal != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      iJoin = ciJoin.join(ContratoInterviniente_.interviniente, JoinType.INNER);
      Predicate intPri = cb.or(
        cb.like(iJoin.get(Interviniente_.NOMBRE), "%" + intervinientePrincipal + "%"),
        cb.like(iJoin.get(Interviniente_.APELLIDOS), "%" + intervinientePrincipal + "%"));
      predicates.add(intPri);
    }
    if (tipoIntervencion != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      Join<ContratoInterviniente, TipoIntervencion> tiJoin = ciJoin.join(ContratoInterviniente_.TIPO_INTERVENCION, JoinType.INNER);
      Predicate onCond = cb.like(tiJoin.get(Catalogo_.VALOR), "%" + tipoIntervencion + "%");
      tiJoin.on(onCond);
    }
    if (judicial != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      if (!judicial) predicates.add(cb.isEmpty(cJoin.get(Contrato_.procedimientos)));
      else predicates.add(cb.isNotEmpty(cJoin.get(Contrato_.procedimientos)));
    }
    addOnCond(cb, root, SkipTracing_.provincia, provincia);
    /*if (analistaST != null){
      uJoin = root.join(SkipTracing_.analistaST, JoinType.INNER);
      predicates.add(cb.like(uJoin.get(Usuario_.nombre), "%" + analistaST + "%"));
    }*/
    if (logger != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      eJoin = cJoin.join(Contrato_.expediente, JoinType.INNER);
      caJoin = eJoin.join(Expediente_.cartera, JoinType.INNER);
      Predicate logPre = cb.or(
        cb.equal(root.get(SkipTracing_.analistaST).get(Usuario_.id), logger.getId()),
        cb.equal(caJoin.get(Cartera_.responsableSkipTracing).get(Usuario_.id), logger.getId()));
      predicates.add(logPre);
    }
    if (gestorExpediente != null){
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      eJoin = cJoin.join(Contrato_.expediente, JoinType.INNER);
      aeJoin = eJoin.join(Expediente_.asignaciones, JoinType.INNER);
      aeUJoin = aeJoin.join(AsignacionExpediente_.usuario, JoinType.INNER);


      Predicate onCond = cb.like(
        aeJoin.get(AsignacionExpediente_.usuario).get(Usuario_.perfil).get(Perfil_.NOMBRE),
        "Gestor");

      Predicate onCond2=cb.like(aeUJoin.get(Usuario_.NOMBRE),"%" + gestorExpediente + "%");


    /*  Predicate gesExp = cb.and(
        cb.like(aeUJoin.get(Usuario_.NOMBRE), "%" + intervinientePrincipal + "%"),
        cb.equal(aeUJoin.get(Usuario_.PERFIL).get(Perfil_.NOMBRE), "Gestor"));*/
      predicates.add(cb.and(onCond,onCond2));
    }
    if (saldoGestion != null) {
      ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
      cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
      predicates.add(cb.equal(cJoin.get(Contrato_.saldoGestion), saldoGestion));
    }
    if (fechaSolicitud!=null){
      predicates.add(cb.like(root.get(SkipTracing_.fechaSolicitud).as(String.class), "%"+ fechaSolicitud + "%"));
    }
    if (fechaAsignacion!=null){
      predicates.add(cb.like(root.get(SkipTracing_.fechaAsignacion).as(String.class), "%"+ fechaAsignacion + "%"));
    }
    if (fechaInicioBusqueda!=null){
      predicates.add(cb.like(root.get(SkipTracing_.fechaInicioBusqueda).as(String.class), "%"+ fechaInicioBusqueda + "%"));
    }
    if (fechaFinBusqueda!=null){
      predicates.add(cb.like(root.get(SkipTracing_.fechaFinBusqueda).as(String.class), "%"+ fechaFinBusqueda + "%"));
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    if (orderField != null) {
      switch (orderField) {
        case "cliente":
          ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          eJoin = cJoin.join(Contrato_.expediente, JoinType.INNER);
          caJoin = eJoin.join(Expediente_.cartera, JoinType.INNER);
          ccJoin = caJoin.join(Cartera_.clientes, JoinType.INNER);
          clJoin = ccJoin.join(ClienteCartera_.cliente, JoinType.INNER);
          rs.orderBy(getOrden(cb, clJoin.get(Cliente_.nombre), orderDirection));
          break;
        case "cartera":
          ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          eJoin = cJoin.join(Contrato_.expediente, JoinType.INNER);
          caJoin = eJoin.join(Expediente_.cartera, JoinType.INNER);
          rs.orderBy(getOrden(cb, caJoin.get(Cartera_.nombre), orderDirection));
          break;
        case "idConcatenado":
          ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          eJoin = cJoin.join(Contrato_.expediente, JoinType.INNER);
          rs.orderBy(getOrden(cb, eJoin.get(Expediente_.idConcatenado), orderDirection));
          break;
        case "idContrato":
          ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          rs.orderBy(getOrden(cb, cJoin.get(Contrato_.id), orderDirection));
          break;
        case "producto":
          ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          Join<Contrato, Producto> cpJoin = cJoin.join(Contrato_.PRODUCTO, JoinType.INNER);
          rs.orderBy(getOrden(cb, cpJoin.get(Catalogo_.VALOR), orderDirection));
          break;
        case "nintervinientes":
          /*ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          Subquery<Long> subqueryI = cb.createQuery().subquery(Long.class);
          Root<ContratoInterviniente> fromCI = subqueryI.from(ContratoInterviniente.class);
          subqueryI.select(cb.count(fromCI)).distinct(true).where(cb.equal(cJoin.get(Contrato_.ID), fromCI.get(ContratoInterviniente_.CONTRATO).get(Contrato_.ID)));
          rs.orderBy(getOrden(cb, subqueryI, orderDirection));*/
          break;
        case "nbienes":
          /*ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          Subquery<Long> subqueryG = cb.createQuery().subquery(Long.class);
          Root<ContratoBien> fromCB = subqueryG.from(ContratoBien.class);
          subqueryG.select(cb.count(fromCB)).distinct(true).where(cb.equal(cJoin.get(Contrato_.ID), fromCB.get(ContratoBien_.CONTRATO).get(Contrato_.ID)));
          rs.orderBy(getOrden(cb, subqueryG, orderDirection));*/
          break;
        case "estadoST":
          rs.orderBy(getOrden(cb, root.get(SkipTracing_.estadoST).get(EstadoSkipTracing_.valor), orderDirection));
          break;
        case "nivelST":
          rs.orderBy(getOrden(cb, root.get(SkipTracing_.nivelST).get(NivelSkipTracing_.valor), orderDirection));
          break;
        case "tipoBusqueda":
          rs.orderBy(getOrden(cb, root.get(SkipTracing_.tipoBusqueda).get(TipoBusqueda_.valor), orderDirection));
          break;
        case "intervinientePrincipal":
        case "primerTitular":
          ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          iJoin = ciJoin.join(ContratoInterviniente_.interviniente, JoinType.INNER);
          rs.orderBy(getOrden(cb, iJoin.get(Interviniente_.NOMBRE), orderDirection));
          break;
        case "tipoIntervencion":
          ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          Join<ContratoInterviniente, TipoIntervencion> tiJoin = ciJoin.join(ContratoInterviniente_.TIPO_INTERVENCION, JoinType.INNER);
          rs.orderBy(getOrden(cb, tiJoin.get(Catalogo_.VALOR), orderDirection));
          break;
        case "judicial":
          /*ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          rs.orderBy(getOrden(cb, cJoin.get(Catalogo_.VALOR), orderDirection));*/
          break;
        case "analistaST":
          /*ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          rs.orderBy(getOrden(cb, cJoin.get(Catalogo_.VALOR), orderDirection));*/
          break;
        case "gestor":
          /*ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          rs.orderBy(getOrden(cb, cJoin.get(Catalogo_.VALOR), orderDirection));*/
          break;
        case "saldo":
          ciJoin = root.join(SkipTracing_.contratoInterviniente, JoinType.INNER);
          cJoin = ciJoin.join(ContratoInterviniente_.contrato, JoinType.INNER);
          rs.orderBy(getOrden(cb, cJoin.get(Contrato_.saldoGestion), orderDirection));
          break;
        case "carga":
          /*carJoin = root.join(SkipTracing_.cargaST, JoinType.INNER);
          rs.orderBy(getOrden(cb, carJoin.get(CargaST_.id), orderDirection));*/
          break;
        default:
          if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
            rs.orderBy(cb.desc(root.get(orderField)));
          else
            rs.orderBy(cb.asc(root.get(orderField)));
          break;
      }
    }
    Set<ParameterExpression<?>> q = query.getParameters();

    List<SkipTracing> lista = entityManager.createQuery(query).getResultList();

    return lista;
  }

  private Order getOrden(CriteriaBuilder cb, Expression expresion, String orderDirection) {
    if (orderDirection == null) {
      orderDirection = "desc";
    }
    return orderDirection.toLowerCase().equalsIgnoreCase("desc") ? cb.desc(expresion) : cb.asc(expresion);
  }

  private void addOnCond(CriteriaBuilder cb, Root<SkipTracing> root, SingularAttribute<SkipTracing, ?> singularAtrribute, String campo) {
    if (campo == null)
      return;
    Join<SkipTracing, ?> estadoJoin = root.join(singularAtrribute, JoinType.INNER);
    Predicate onCond = cb.like(estadoJoin.get(Catalogo_.VALOR), "%" + campo + "%");
    estadoJoin.on(onCond);
  }

  public DatoDireccionST actualizarCamposDatoDireccion(DatoDireccionST datoDireccionST, DatoDireccionSTInputDTO datoDireccionSTDTO, Usuario gestor) throws NotFoundException {

    if(datoDireccionSTDTO.getTipoVia() != null) {
      var tipoVia = tipoViaRepository.findById(datoDireccionSTDTO.getTipoVia()).orElse(null);
      if(tipoVia != null) {
        datoDireccionST.setTipoVia(tipoVia);
      }
    }
    if(datoDireccionSTDTO.getEntorno() != null) {
      var entorno = entornoRepository.findById(datoDireccionSTDTO.getEntorno()).orElse(null);
      if(entorno != null) {
        datoDireccionST.setEntorno(entorno);
      }
    }
    if(datoDireccionSTDTO.getMunicipio() != null) {
      var municipio = municipioRepository.findById(datoDireccionSTDTO.getMunicipio()).orElse(null);
      if(municipio != null) {
        datoDireccionST.setMunicipio(municipio);
      }
    }
    if(datoDireccionSTDTO.getLocalidad() != null) {
      var localidad = localidadRepository.findById(datoDireccionSTDTO.getLocalidad()).orElse(null);
      if(localidad != null) {
        datoDireccionST.setLocalidad(localidad);
      }
    }
    if(datoDireccionSTDTO.getProvincia() != null) {
      var provincia = provinciaRepository.findById(datoDireccionSTDTO.getProvincia()).orElse(null);
      if(provincia != null) {
        datoDireccionST.setProvincia(provincia);
      }
    }
    if(datoDireccionSTDTO.getPais() != null) {
      var pais = paisRepository.findById(datoDireccionSTDTO.getPais()).orElse(null);
      if(pais != null) {
        datoDireccionST.setPais(pais);
      }
    }
    if (datoDireccionST.getValidado() != null && datoDireccionST.getValidado()){
      datoDireccionST.setFechaValidacion(new Date());
    } else {
      datoDireccionST.setFechaValidacion(null);
    }
    /*if(datoDireccionSTDTO.getFechaAlta() != null) {
      datoDireccionST.setFechaAlta(datoDireccionSTDTO.getFechaAlta());
    }*/
   // if(datoDireccionSTDTO.getValidado() != null) {
     // datoDireccionST.setValidado(datoDireccionSTDTO.getValidado());
    if(datoDireccionSTDTO.getPrincipal()){
      IntervinienteST intervinienteST=datoDireccionST.getIntervinienteST();
      List<DatoDireccionST>datoDireccionSTList=intervinienteST.getDatosDireccionST().stream().collect(Collectors.toList());
      for (DatoDireccionST dsT:datoDireccionSTList) {
        dsT.setPrincipal(false);
        datoDireccionSTRepository.save(dsT);
      }
    }
    datoDireccionST.setPrincipal(datoDireccionSTDTO.getPrincipal());




      datoDireccionST.setValidado(datoDireccionSTDTO.getValidado());


    //}



  /*  if(datoDireccionSTDTO.getPrincipal() != null) {
      datoDireccionST.setPrincipal(datoDireccionSTDTO.getPrincipal());
    }*/
    if(datoDireccionSTDTO.getLatitud() != null) {
      datoDireccionST.setLatitud(datoDireccionSTDTO.getLatitud());
    }
    if(datoDireccionSTDTO.getLongitud() != null) {
      datoDireccionST.setLongitud(datoDireccionSTDTO.getLongitud());
    }
    if(datoDireccionSTDTO.getBloque() != null &&  datoDireccionSTDTO.getBloque() != "") {
      datoDireccionST.setBloque(datoDireccionSTDTO.getBloque());
    }
    if(datoDireccionSTDTO.getEscalera() != null &&  datoDireccionSTDTO.getEscalera() != "") {
      datoDireccionST.setEscalera(datoDireccionSTDTO.getEscalera());
    }
    if(datoDireccionSTDTO.getPiso() != null &&  datoDireccionSTDTO.getPiso() != "") {
      datoDireccionST.setPiso(datoDireccionSTDTO.getPiso());
    }
    if(datoDireccionSTDTO.getPuerta() != null &&  datoDireccionSTDTO.getPuerta() != "") {
      datoDireccionST.setPuerta(datoDireccionSTDTO.getPuerta());
    }
    if (datoDireccionSTDTO.getNombre()!=null && datoDireccionSTDTO.getNombre()!=""){
      datoDireccionST.setNombre(datoDireccionSTDTO.getNombre());
    }
    if (datoDireccionSTDTO.getNumero() != null && datoDireccionSTDTO.getNumero() != "") {
      datoDireccionST.setNumero(datoDireccionSTDTO.getNumero());
    }
    if (datoDireccionSTDTO.getPortal() !=null && datoDireccionSTDTO.getPortal()!=""){
      datoDireccionST.setPortal(datoDireccionSTDTO.getPortal());
    }

    if(datoDireccionSTDTO.getComentario() != null &&  datoDireccionSTDTO.getComentario() != "") {
      datoDireccionST.setComentario(datoDireccionSTDTO.getComentario());
    }
    if(datoDireccionSTDTO.getCodigoPostal() != null &&  datoDireccionSTDTO.getCodigoPostal() != "") {
      datoDireccionST.setCodigoPostal(datoDireccionSTDTO.getCodigoPostal());
    }
    if (datoDireccionSTDTO.getDatosOrigen() != null) {
      DatosOrigen dor = datoDireccionST.getDatosOrigen();
      if (dor == null) dor = new DatosOrigen();
      dor = actualizarCamposDatosOrigen(dor, datoDireccionSTDTO.getDatosOrigen(), gestor);
      DatosOrigen dorSaved = datosOrigenRepository.save(dor);
      datoDireccionST.setDatosOrigen(dorSaved);
    }
    //imagen falta, ver la clase DatoDireccionST
    if(datoDireccionSTDTO.getObservaciones() != null &&  datoDireccionSTDTO.getObservaciones() != "") {
      datoDireccionST.setObservaciones(datoDireccionSTDTO.getObservaciones());
    }
    return datoDireccionST;
  }

  public DatoContactoST actualizarCamposDatoContacto(DatoContactoST datoContactoST, DatoContactoSTInputDTO datoContactoSTDTO, Usuario gestor) throws NotFoundException {

   // if(datoContactoSTDTO.getTipoContacto() != null) {
    //  var tipoContacto = tipoContactoRepository.findById(datoContactoSTDTO.getTipoContacto()).orElse(null);
      //if(!datoContactoST.getTipoContacto().equals(tipoContacto) && tipoContacto != null) {
       //

    //PSAR EL TIPOCONTACTO TAMBIEN
        /* if(tipoContacto.getValor().contains("Teléfono") && datoContactoSTDTO.getTelefono() != null) {
          datoContactoST.setValor(datoContactoSTDTO.getTelefono());
          datoContactoST.setTipoContacto(tipoContacto);
        } else if(tipoContacto.getValor().contains("Email") && datoContactoSTDTO.getEmail() != null) {
          datoContactoST.setValor(datoContactoSTDTO.getEmail());
          datoContactoST.setTipoContacto(tipoContacto);
        }*/
   //   }
    //}

    if(datoContactoSTDTO.getValor()!=null){
      datoContactoST.setValor(datoContactoSTDTO.getValor());
    }
    if (datoContactoSTDTO.getTipoContacto()!=null){
      TipoContacto tipoContacto=tipoContactoRepository.findById(datoContactoSTDTO.getTipoContacto()).orElse(null);
      if (tipoContacto!=null){
        datoContactoST.setTipoContacto(tipoContacto);
      }
    }

    if(datoContactoSTDTO.getValidado() != null) {
     datoContactoST.setValidado(datoContactoSTDTO.getValidado());
    }else{
      datoContactoST.setValidado(null);
    }

    if (datoContactoST.getValidado() != null && datoContactoST.getValidado()){
      datoContactoST.setFechaValidacion(new Date());
    } else {
      datoContactoST.setFechaValidacion(null);
    }
    if(datoContactoSTDTO.getPrincipal() != null) {
      datoContactoST.setPrincipal(datoContactoSTDTO.getPrincipal());
    }
    if (datoContactoSTDTO.getDatosOrigen() != null) {
      DatosOrigen dor = datoContactoST.getDatosOrigen();
      if (dor == null) dor = new DatosOrigen();
      dor = actualizarCamposDatosOrigen(dor, datoContactoSTDTO.getDatosOrigen(), gestor);
      DatosOrigen dorSaved = datosOrigenRepository.save(dor);
      datoContactoST.setDatosOrigen(dorSaved);
    }
    return datoContactoST;
  }

  public DatosOrigen actualizarCamposDatosOrigen(DatosOrigen dor, DatosOrigenInputDTO doIDTO, Usuario usuario) throws NotFoundException {
    dor.setGestor(usuario);
    dor.setFechaActualizacion(new Date());

    if (doIDTO.getLink() != null)
      dor.setLink(doIDTO.getLink());
    if (doIDTO.getFecha() != null)
      dor.setFecha(doIDTO.getFecha());
    if (doIDTO.getObservaciones() != null)
      dor.setObservaciones(doIDTO.getObservaciones());
    if (doIDTO.getOtraFuente() != null)
      dor.setOtraFuente(doIDTO.getOtraFuente());
    if (doIDTO.getFechaActualizacion()!=null)
      dor.setFechaActualizacion(doIDTO.getFechaActualizacion());

    if (doIDTO.getOrigenDato() != null)
      dor.setOrigenDato(origenDatoRepository.findById(doIDTO.getOrigenDato()).orElseThrow(() ->
        new NotFoundException("OrigenDato", doIDTO.getOrigenDato())));
    if (doIDTO.getPaginaPublica() != null)
      dor.setPaginaPublica(paginaPublicaRepository.findById(doIDTO.getPaginaPublica()).orElseThrow(() ->
        new NotFoundException("PaginaPublica", doIDTO.getPaginaPublica())));
    if (doIDTO.getRedSocial() != null)
      dor.setRedSocial(redSocialRepository.findById(doIDTO.getRedSocial()).orElseThrow(() ->
        new NotFoundException("RedSocial", doIDTO.getRedSocial())));
    if (doIDTO.getPerfilProfesional() != null)
      dor.setPerfilProfesional(perfilProfesionalRepository.findById(doIDTO.getPerfilProfesional()).orElseThrow(() ->
        new NotFoundException("PerfilProfesional", doIDTO.getOrigenDato())));

    return dor;
  }

  public List<SkipTracing> getDataExpedientesFilter(BusquedaDto busquedaDto, Usuario loggedUser) throws NotFoundException {
    BusquedaExpedienteDto busquedaExpedienteDto = busquedaDto.getExpediente();
    BusquedaContratoDto busquedaContratoDto = busquedaDto.getContrato();
    BusquedaIntervinienteDto busquedaIntervinienteDto = busquedaDto.getInterviniente();
    BusquedaBienDto busquedaBienDto = busquedaDto.getBien();
    BusquedaMovimientoDto busquedaMovimientoDto = busquedaDto.getMovimientos();
    BusquedaEstimacionDto busquedaEstimacionDto = busquedaDto.getEstimaciones();
    BusquedaProcedimientoDto busquedaProcedimientoDto = busquedaDto.getProcedimiento();
    BusquedaPropuestaDto busquedaPropuestaDto = busquedaDto.getPropuestas();
    BusquedaAgendaDto busquedaAgendaDto = busquedaDto.getAgenda();

    BusquedaBuilderST builder = new BusquedaBuilderST(entityManager.getCriteriaBuilder());
    if (busquedaExpedienteDto != null) builder.addCondicionesExpediente(busquedaExpedienteDto, loggedUser);
    if (busquedaContratoDto != null) builder.addCondicionesContrato(busquedaContratoDto);
    if (busquedaIntervinienteDto != null) builder.addCondicionesInterviniente(busquedaIntervinienteDto);
    if (busquedaBienDto != null) builder.addCondicionesBien(busquedaBienDto);
    if (busquedaMovimientoDto != null) builder.addCondicionesMovimiento(busquedaMovimientoDto);
    if (busquedaEstimacionDto != null) builder.addCondicionesEstimacion(busquedaEstimacionDto);
    if (busquedaProcedimientoDto != null) {
      TipoProcedimiento tipo = null;
      HitoProcedimiento fechaHito = null;
      if (busquedaProcedimientoDto.getTipoProcedimiento() != null) {
        tipo = tipoProcedimientoRepository.findById(busquedaProcedimientoDto.getTipoProcedimiento()).orElseThrow(() -> new NotFoundException("tipo de procedimiento", busquedaProcedimientoDto.getTipoProcedimiento()));
      }
      if (busquedaProcedimientoDto.getHitoProcedimiento() != null) {
        fechaHito = hitoProcedimientoRepository.findById(busquedaProcedimientoDto.getHitoProcedimiento()).orElseThrow(() -> new NotFoundException("hito de procedimiento", busquedaProcedimientoDto.getHitoProcedimiento()));
      }
      builder.addCondicionesProcedimiento(busquedaProcedimientoDto, tipo, fechaHito);
    }
    /*if (busquedaPropuestaDto != null) {
      List<Integer> expedientes = null;
      if (busquedaPropuestaDto.getIntervaloImporte() != null) {
        if (busquedaPropuestaDto.getIntervaloImporte().getDescripcion().equals("valoracion")) {
          expedientes = getExpedienteFilteredByImporteUltimaValoracion(busquedaPropuestaDto);
        }
      }
      builder.addCondicionesPropuesta(busquedaPropuestaDto, expedientes);
    }*/
    if (busquedaAgendaDto != null) {
      if (busquedaAgendaDto.getIntervaloFechas() != null
        || busquedaAgendaDto.getDestinatario() != null
        || busquedaAgendaDto.getEmisor() != null
        || busquedaAgendaDto.getEstado() != null
        || busquedaAgendaDto.getClaseEvento() != null) {
        List<Integer> expedientesIds = getExpedienteFilteredByAgenda(loggedUser, busquedaAgendaDto);
        builder.addCondicionesAgenda(expedientesIds);
      }
    }

    //builder.setOrdenación(orderField, orderDirection);

    var query = builder.createQuery().distinct(true);
    List<SkipTracing> result = entityManager.createQuery(query).getResultList();
    return result;
  }

  public List<SkipTracingOutputDTO> filtrarListaST(
    List<SkipTracingOutputDTO> lista, String cliente, String cartera, String idExpediente, Integer idContrato, Integer idCarga, String producto,
    Integer nIntervinientes, Integer nGarantias, String estado, String nivel, String intervinientePrincipal,
    String tipoIntervencion, Boolean judicial, String provincia, String analistaST, String gestorExpediente) {
 /*   if (estado != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getEstadoST() != null ? c.getEstadoST().getValor() : "-1"), estado)).collect(Collectors.toList());*/
    if (idExpediente != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getIdConcatenado()), idExpediente)).collect(Collectors.toList());
    if (idContrato != null)
      lista = lista.stream().filter(c -> c.getIdContrato() != null && c.getIdContrato().equals(idContrato)).collect(Collectors.toList());
    if (idContrato != null)
      lista = lista.stream().filter(c -> c.getCarga() != null && c.getCarga().getId().equals(idCarga)).collect(Collectors.toList());
    if (cliente != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getCliente()), cliente)).collect(Collectors.toList());
    if (cartera != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getCartera()), cartera)).collect(Collectors.toList());
    if (producto != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getProducto() != null ? c.getProducto().getValor() : "-1"), producto)).collect(Collectors.toList());
    if (nIntervinientes != null)
      lista = lista.stream().filter(c -> c.getNIntervinientes() != null && c.getNIntervinientes().equals(nIntervinientes)).collect(Collectors.toList());
    if (nGarantias != null)
      lista = lista.stream().filter(c -> c.getNBienes() != null && c.getNBienes().equals(nGarantias)).collect(Collectors.toList());
    if (nivel != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getNivelST() != null ? c.getNivelST().getValor() : "-1"), nivel)).collect(Collectors.toList());
    if (intervinientePrincipal != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getIntervinientePrincipal() != null ? c.getIntervinientePrincipal().getNombre() : "-1"), intervinientePrincipal)).collect(Collectors.toList());
    if (tipoIntervencion != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getIntervinientePrincipal() != null ? c.getIntervinientePrincipal().getTipoIntervencion() != null ?
        c.getIntervinientePrincipal().getTipoIntervencion().getValor() : "-1" : "-1"), tipoIntervencion)).collect(Collectors.toList());
    if (provincia != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getProvincia() != null ? c.getProvincia().getValor() : "-1"), provincia)).collect(Collectors.toList());
    if (analistaST != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getAnalistaST() != null ? c.getAnalistaST().getNombre() : "-1"), analistaST)).collect(Collectors.toList());
    if (gestorExpediente != null)
      lista = lista.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getGestor() != null ? c.getGestor().getNombre() : "-1"), gestorExpediente)).collect(Collectors.toList());
    if (judicial != null)
      lista = lista.stream().filter(c -> c.getJudicial() != null && (judicial ? c.getJudicial().equals("SI") : c.getJudicial().equals("No"))).collect(Collectors.toList());
    return lista;
  }

  public List<SkipTracingOutputDTO> ordenarListaST(List<SkipTracingOutputDTO> lista, String orderField, String orderDirection) {

    // En caso de que el orderfield no este contemplado se ordena por ID por defecto
    Comparator<SkipTracingOutputDTO> comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> Integer.compare(o1.getId(), o2.getId());
    lista.sort(comparator);
    if (orderField == null || orderDirection == null) return lista;

    switch (orderField) {
      case "saldo":
        comparator = Comparator.comparingDouble(SkipTracingOutputDTO::getSaldo);
        break;
      case "judicial":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> Boolean.compare(o1.getJudicial(), o2.getJudicial());
        break;
      case "cliente":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(o1.getCliente(), o2.getCliente());
        break;
      case "cartera":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(o1.getCartera(), o2.getCartera());
        break;
      case "idExpediente":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(o1.getIdConcatenado(), o2.getIdConcatenado());
        break;
      case "idContrato":
        comparator = Comparator.comparingInt(SkipTracingOutputDTO::getIdContrato);
        break;
      case "carga":
        comparator = Comparator.comparingInt((SkipTracingOutputDTO o) -> o.getCarga().getId());
        break;
      case "nIntervinientes":
      case "nintervinientes":
        comparator = Comparator.comparingInt(SkipTracingOutputDTO::getNIntervinientes);
        break;
      case "nGarantias":
      case "nbienes":
        comparator = Comparator.comparingInt(SkipTracingOutputDTO::getNBienes);
        break;
      case "provincia":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(
          o1.getProvincia() != null ? o1.getProvincia().getValor() : null, o2.getProvincia() != null ? o2.getProvincia().getValor() : null);
        break;
      case "producto":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(
          o1.getProducto() != null ? o1.getProducto().getValor() : null, o2.getProducto() != null ? o2.getProducto().getValor() : null);
        break;
     /* case "estado":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(
          o1.getEstadoST() != null ? o1.getEstadoST().getValor() : null, o2.getEstadoST() != null ? o2.getEstadoST().getValor() : null);
        break;*/
      case "nivel":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(
          o1.getNivelST() != null ? o1.getNivelST().getValor() : null, o2.getNivelST() != null ? o2.getNivelST().getValor() : null);
        break;
      case "intervinientePrincipal":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(
          o1.getIntervinientePrincipal() != null ? o1.getIntervinientePrincipal().getNombre() : null,
          o2.getIntervinientePrincipal() != null ? o2.getIntervinientePrincipal().getNombre() : null);
        break;
      case "analistaST":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(
          o1.getAnalistaST() != null ? o1.getAnalistaST().getNombre() : null,
          o2.getAnalistaST() != null ? o2.getAnalistaST().getNombre() : null);
        break;
      case "gestor":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(
          o1.getGestor() != null ? o1.getGestor().getNombre() : null,
          o2.getGestor() != null ? o2.getGestor().getNombre() : null);
        break;
      case "tipoIntervencion":
        comparator = (SkipTracingOutputDTO o1, SkipTracingOutputDTO o2) -> StringUtils.compare(
          o1.getIntervinientePrincipal() != null ? o1.getIntervinientePrincipal().getTipoIntervencion() != null ? o1.getIntervinientePrincipal().getTipoIntervencion().getValor() : null : null,
          o2.getIntervinientePrincipal() != null ? o2.getIntervinientePrincipal().getTipoIntervencion() != null ? o2.getIntervinientePrincipal().getTipoIntervencion().getValor() : null : null);
        break;
    }

    if (orderDirection.equals("asc")) lista.sort(comparator);
    else if (orderDirection.equals("desc")) lista.sort(comparator.reversed());
    return lista;
  }

  private List<Integer> getExpedienteFilteredByAgenda(Usuario loggedUser, BusquedaAgendaDto busquedaAgendaDto) throws NotFoundException {
    List<Evento> eventos = eventoUtil.getListFilteredEventos(loggedUser.getId(), busquedaAgendaDto);
    List<Integer> expedientes = null;
    if (eventos != null) {
      expedientes = new ArrayList<>();
      for (Evento evento : eventos) {
        if ("SK_TR".equals(evento.getNivel().getCodigo())) {
          SkipTracing st = skipTracingRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("SkipTracing", evento.getIdNivel()));
          if (!expedientes.contains(st.getId())) expedientes.add(st.getId());
        }
      }
    }
    return expedientes;
  }
  public IntervinienteST actualizarCampos(IntervinienteST intervinienteST, IntervinienteSkipTracingInputDTO intervinienteSTDTO) {

    if(intervinienteSTDTO.getNumeroDocumento() != null &&  intervinienteSTDTO.getNumeroDocumento() != "") {
      intervinienteST.setNumeroDocumento(intervinienteSTDTO.getNumeroDocumento());
    }
    if(intervinienteSTDTO.getPersonaJuridica() != null) {
      intervinienteST.setPersonaJuridica(intervinienteSTDTO.getPersonaJuridica());
    }
    if(intervinienteSTDTO.getNombre() != null &&  intervinienteSTDTO.getNombre() != "") {
      intervinienteST.setNombre(intervinienteSTDTO.getNombre());
    }
    if(intervinienteSTDTO.getApellidos() != null &&  intervinienteSTDTO.getApellidos() != "") {
      intervinienteST.setApellidos(intervinienteSTDTO.getApellidos());
    }
    if(intervinienteSTDTO.getSexo() != null) {
      Sexo sexo = sexoRepository.findById(intervinienteSTDTO.getSexo()).orElse(null);
      intervinienteST.setSexo(sexo);
    }
    if(intervinienteSTDTO.getEstadoCivil() != null) {
      EstadoCivil estadoCivil = estadoCivilRepository.findById(intervinienteSTDTO.getEstadoCivil()).orElse(null);
      intervinienteST.setEstadoCivil(estadoCivil);
    }
    if(intervinienteSTDTO.getNacionalidad() != null) {
      Pais nacionalidad = paisRepository.findById(intervinienteSTDTO.getNacionalidad()).orElse(null);
      intervinienteST.setNacionalidad(nacionalidad);
    }
    if(intervinienteSTDTO.getPaisNacimiento() != null) {
      Pais pais = paisRepository.findById(intervinienteSTDTO.getPaisNacimiento()).orElse(null);
      intervinienteST.setPaisNacimiento(pais);
    }

    if (intervinienteSTDTO.getContactado() != null) {
      intervinienteST.setContactado(intervinienteSTDTO.getContactado());
    }
    if (intervinienteSTDTO.getEstado()!=null){
      intervinienteST.setEstado(intervinienteSTDTO.getEstado());
    }

    if (intervinienteSTDTO.getOrdenIntervencion()!=null){
      intervinienteST.setOrdenIntervencion(intervinienteSTDTO.getOrdenIntervencion());
    }
    if (intervinienteSTDTO.getResidencia()!=null){
      Pais residencia=paisRepository.findById(intervinienteSTDTO.getResidencia()).orElse(null);
      intervinienteST.setResidencia(residencia);
    }
    if (intervinienteSTDTO.getTipoContacto()!=null){
      TipoContacto tipoContacto=tipoContactoRepository.findById(intervinienteSTDTO.getTipoContacto()).orElse(null);
      intervinienteST.setTipoContacto(tipoContacto);
    }
    if (intervinienteSTDTO.getFechaConstitucion()!=null){
      intervinienteST.setFechaConstitucion(intervinienteSTDTO.getFechaConstitucion());
    }
    if (intervinienteSTDTO.getRazonSocial()!=null){
      intervinienteST.setRazonSocial(intervinienteSTDTO.getRazonSocial());
    }
    if (intervinienteSTDTO.getTipoGestor()!=null){
      Perfil perfil=perfilRepository.findById(intervinienteSTDTO.getTipoGestor()).orElse(null);
      intervinienteST.setTipoGestor(perfil);

      //intervinienteST.setTipoGestor(intervinienteSTDTO.getTipoGestor());
    }


    if (intervinienteSTDTO.getDatosAdicionales1() != null)
      intervinienteST.setDatosAdicionales1(intervinienteSTDTO.getDatosAdicionales1());
    if (intervinienteSTDTO.getDatosAdicionales2() != null)
      intervinienteST.setDatosAdicionales2(intervinienteSTDTO.getDatosAdicionales2());
    return intervinienteST;
  }

  public List<SkipTracing> filterUserAndAgency(List<SkipTracing> initialList, String analistaST) {
    List<SkipTracing> finalList = new ArrayList<>();
    for(var current: initialList) {
      if(current.getNivelST() != null) {
        if(current.getNivelST().getValor().equals("Nivel bajo")) {
          if(current.getCargaST() != null && current.getCargaST().getEncargado() != null) {
            if(Pattern.compile(Pattern.quote(analistaST), Pattern.CASE_INSENSITIVE).matcher(current.getCargaST().getEncargado()).find()) {
              finalList.add(current);
            }
          }
        } else if(current.getNivelST().getValor().equals("Nivel medio") || current.getNivelST().getValor().equals("Nivel alto")) {
          if(current.getAnalistaST() != null && current.getAnalistaST().getNombre() != null) {
            if(Pattern.compile(Pattern.quote(analistaST), Pattern.CASE_INSENSITIVE).matcher(current.getAnalistaST().getNombre()).find()) {
              finalList.add(current);
            }
          }
        }
      }
    }
    return finalList;
  }
}
