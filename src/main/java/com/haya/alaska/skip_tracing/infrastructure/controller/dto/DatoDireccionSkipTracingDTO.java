package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DatoDireccionSkipTracingDTO implements Serializable {

  private Integer id;
  private Boolean principal;
  private Boolean validado;
  private Date fechaValidacion;
  private Date fechaAlta;

  private String nombre;
  private String bloque;
  private String escalera;
  private String piso;
  private String puerta;
  private String comentario;
  private String codigoPostal;
  private Double longitud;
  private Double latitud;
  private String portal;
  private String numero;

  private CatalogoMinInfoDTO tipoVia;
  private CatalogoMinInfoDTO municipio;
  private CatalogoMinInfoDTO localidad;
  private CatalogoMinInfoDTO entorno;
  private CatalogoMinInfoDTO provincia;
  private CatalogoMinInfoDTO pais;

  private DatosOrigenOutputDTO datosOrigen;

  private Integer idIntervinienteST;
  private List<Long> fotos;
  private Boolean fincaHipotecada;
}
