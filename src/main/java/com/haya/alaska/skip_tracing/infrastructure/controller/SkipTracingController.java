package com.haya.alaska.skip_tracing.infrastructure.controller;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.model.respuesta.RespuestaDescargaDocumento;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.EmptyFileException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.skip_tracing.aplication.SkipTracingUseCase;
import com.haya.alaska.skip_tracing.infrastructure.controller.dto.*;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/skipTracing")
public class SkipTracingController {
  @Autowired
  SkipTracingUseCase skipTracingUseCase;

  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;

  @Autowired
  UsuarioRepository usuarioRepository;
  //SkipTracing
  @ApiOperation(value = "Asignar un nivel un SkipTracing",
    notes = "Asignar un nivel un SkipTracing, y pasar sus datos desde Recuperación Amistosa, a SkipTracing.")
  @PutMapping("/asignar-nivel")
  public ResponseEntity<InputStreamResource> asignarNivel(@RequestParam(required = false) List<Integer> skipTracingId,
                                                 @RequestParam Integer idNivel,
                                                 @RequestParam(required = false) Integer idUsuario,
                                                 @RequestParam(required = false) Boolean todos,
                                                 @RequestParam(required = false) Integer empresa,
                                                 @ApiIgnore CustomUserDetails principal,
                                                 @RequestBody BusquedaDto busqueda) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();

    Integer usuarioId=usuario.getId();
    Usuario usuarionuevo = usuarioRepository.findById(usuarioId).orElse(null);
    ExcelExport excelExport = new ExcelExport();
    LinkedHashMap<String, List<Collection<?>>> result = skipTracingUseCase.asignarNivel(skipTracingId, idNivel, idUsuario, todos, empresa, usuarionuevo, busqueda);
    if (result != null){
      ByteArrayInputStream excel = excelExport.create(result);
      Integer idCarga = skipTracingUseCase.findCarga(skipTracingId);
      String nombre = "U" + usuarionuevo.getId();
      if (idCarga != null) nombre = idCarga + "";
      return excelExport.download(excel, "ST_" + nombre + "_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
    }
    return null;
  }

  @ApiOperation(value = "Find SkipTracing by Id", notes = "Devuelve el SkipTracing con el id enviado")
  @GetMapping("/{skipTracingId}")
  public SkipTracingOutputDTO findById(
    @PathVariable Integer skipTracingId) throws NotFoundException, InvocationTargetException, IllegalAccessException {
    return skipTracingUseCase.getById(skipTracingId);
  }

  @ApiOperation(
    value = "Listar todos los skiptracing",
    notes = "Devuelve todos los skiptracing")
  @PostMapping("/findAll")
  public ListWithCountDTO<SkipTracingOutputDTO> findAll(
    @RequestParam(value = "cliente", required = false) String cliente,
    @RequestParam(value = "cartera", required = false) String cartera,
    @RequestParam(value = "idConcatenado", required = false) String idExpediente,
    @RequestParam(value = "idContrato", required = false) Integer idContrato,
    @RequestParam(value = "carga", required = false) Integer idCarga,

    @RequestParam(value = "producto", required = false) String producto,

    @RequestParam(value = "nintervinientes", required = false) Integer nIntervinientes,
    @RequestParam(value = "nbienes", required = false) Integer nGarantias,
    @RequestParam(value = "estadoST", required = false) String estado,
    @RequestParam(value = "nivelST", required = false) String nivelST,
    @RequestParam(value = "intervinientePrincipal", required = false) String intervinientePrincipal,
    @RequestParam(value = "tipoIntervencion", required = false) String tipoIntervencion,
    @RequestParam(value = "judicial", required = false) Boolean judicial,
    @RequestParam(value = "provincia", required = false) String provincia,
    @RequestParam(value = "analistaST", required = false) String analistaST,
    @RequestParam(value = "gestor", required = false) String gestorExpediente,
    @RequestParam(value = "saldo", required = false) Double saldoGestion,
    @RequestParam(value = "primerTitular", required = false) String primerTitular,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "fechaSolicitud", required = false) String fechaSolicitud,
    @RequestParam(value = "fechaAsignacion", required = false) String fechaAsignacion,
    @RequestParam(value = "fechaInicioBusqueda", required = false) String fechaInicioBusqueda,
    @RequestParam(value = "fechaFinBusqueda", required = false) String fechaFinBusqueda,

    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @RequestBody(required = false) BusquedaDto busqueda,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException, InvocationTargetException, IllegalAccessException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return skipTracingUseCase.findAll(busqueda, usuario, cliente, cartera, idExpediente, idContrato,idCarga, producto,
      nIntervinientes, nGarantias, estado, nivelST, intervinientePrincipal, tipoIntervencion, judicial, provincia,
      analistaST, gestorExpediente, saldoGestion,fechaSolicitud,fechaAsignacion,fechaInicioBusqueda,primerTitular,fechaFinBusqueda, orderField, orderDirection, size,page);
  }

  @ApiOperation(
    value = "Listar todos los skiptracing por expediente",
    notes = "Devuelve todos los skiptracing por expediente")
  @GetMapping("/findByExpediente")
  public ListWithCountDTO<SkipTracingOutputDTO> findByExpediente(@RequestParam(value = "expedienteId") Integer expedienteId, @RequestParam(value = "size") Integer size,
                                                                 @RequestParam(value = "page") Integer page) throws NotFoundException, InvocationTargetException, IllegalAccessException {
    return skipTracingUseCase.findByExpediente(expedienteId, size, page);
  }

  @ApiOperation(
    value = "Actualizar skipTracing",
    notes = "Actualizar skipTracing")
  @PutMapping("/{skipTracingId}")
  public SkipTracingOutputDTO editar(@PathVariable Integer skipTracingId,
                                     @RequestBody SkipTracingInputDTO skipTracingInputDTO,
                                     @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();

    Integer usuarioId=usuario.getId();
    Usuario usuarionuevo = usuarioRepository.findById(usuarioId).orElse(null);

    return skipTracingUseCase.editar(skipTracingId, skipTracingInputDTO, usuarionuevo);
  }

  @ApiOperation(
    value = "", notes = "Devuelve todos los skip tracing filtrados")
  @GetMapping("/{skipTracingId}/historico")
  public ListWithCountDTO<HistoricoSkipTracingDTO> getHistorico(
    @PathVariable Integer skipTracingId) throws Exception {
    return skipTracingUseCase.getHistorico(skipTracingId);
  }

  //Interviniente ST
  @ApiOperation(value = "Find IntervinienteSkipTracing by Id", notes = "Devuelve el IntervinienteSkipTracing con el id enviado")
  @GetMapping("/interviniente/{intervinienteId}")
  public IntervinienteSkipTracingDTO findIntervinienteSTById(
    @PathVariable Integer intervinienteId) throws NotFoundException, InvocationTargetException, IllegalAccessException {
    return skipTracingUseCase.getInterviniente(intervinienteId);
  }

  @ApiOperation(
    value = "Crea intervinienteST",
    notes = "Añade un nuevo intervinienteST al skiptracing con el id enviado")
  @PostMapping("/interviniente/{skipTracingId}")
  public IntervinienteSkipTracingDTO createInterviniente(
    @PathVariable("skipTracingId") Integer skipTracingId,
    @RequestBody IntervinienteSkipTracingInputDTO intervinienteSkipTracingInputDTO,
    @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return skipTracingUseCase.createIntervinienteST(skipTracingId, intervinienteSkipTracingInputDTO, usuario);
  }

  @ApiOperation(
    value = "Actualizar interviniente de skipTracing",
    notes = "Actualizar interviniente de skipTracing")
  @PutMapping("/interviniente/{intervinienteId}")
  public IntervinienteSkipTracingDTO editarInterviniente(@PathVariable Integer intervinienteId,
                                                         @RequestBody IntervinienteSkipTracingInputDTO intervinienteSkipTracingInputDTO,
                                                         @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return skipTracingUseCase.updateIntervinienteST(intervinienteId, intervinienteSkipTracingInputDTO);
  }

  //DatoDireccion
  @ApiOperation(
    value = "Obtener dirección de skip tracing por id",
    notes = "Obtener dirección de skip tracing por id")
  @GetMapping("/datoDireccion/{datoDireccionId}")
  public DatoDireccionSkipTracingDTO getDatoDireccion(
    @PathVariable Integer datoDireccionId) throws Exception {
    return skipTracingUseCase.getDatoDireccionST(datoDireccionId);
  }

  @ApiOperation(
    value = "Actualizar dirección de skipTracing",
    notes = "Actualizar dirección de skipTracing")
  @PostMapping("/datoDireccion/{datoDireccionId}")
  public DatoDireccionSkipTracingDTO editarDatoDireccion(@PathVariable Integer datoDireccionId,
                                                         @RequestPart(value="datoDireccionST")@Valid DatoDireccionSTInputDTO datoDireccionSTInputDTO,
                                                         @RequestParam(value="fotos",required = false)List<MultipartFile>fotos,
                                                         @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return skipTracingUseCase.updateDatoDireccionST(datoDireccionId, datoDireccionSTInputDTO,fotos, usuario);
  }

  @ApiOperation(value = "Crear un datoDireccionST a un IntervinienteSkipTracing",
    notes = "Crear un datodireccionST a un intervinienteSkiptracing")
  @PostMapping("/datoDireccion/Nuevo/{intervinienteId}")
  public DatoDireccionSkipTracingDTO addDatoDireccionSTtoIntervinieneST(@RequestPart(value="datoDireccionST")@Valid DatoDireccionSTInputDTO datoDireccionSTInputDTO,
                                                                        @PathVariable Integer intervinienteId,
                                                                        @RequestParam(value = "fotos", required = false) List<MultipartFile> fotos,
                                                                        @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return skipTracingUseCase.createDatoDireccionST(intervinienteId,datoDireccionSTInputDTO,fotos, usuario);
  }

  @ApiOperation(
    value = "Validar DatoDireccion",
    notes = "Valida a positivo o negativo el Dato de Direccion obtenido por id.")
  @PutMapping("/datoDireccion/{datoDireccionId}/validar")
  public DatoDireccionSkipTracingDTO validarDD(@PathVariable Integer datoDireccionId,
                                               @RequestParam Boolean validado) throws NotFoundException {
    return skipTracingUseCase.validarDD(datoDireccionId, validado);
  }

  //DatoContacto
  @ApiOperation(
    value = "Obtener dato de contacto de skip tracing por id",
    notes = "Obtener dato de contacto de skip tracing por id")
  @GetMapping("/datoContacto/{datoContactoId}")
  public DatoContactoSkipTracingDTO getDatoContacto(
    @PathVariable Integer datoContactoId) throws Exception {
    return skipTracingUseCase.getDatoContactoST(datoContactoId);
  }

  @ApiOperation(value = "Crear un datoContactoST a un IntervinienteSkipTracing",
    notes = "Crear un datoContactoST a un intervinienteSkiptracing")
  @PostMapping("/datoContacto/Nuevo/{intervinienteId}")
  public DatoContactoSkipTracingDTO addDatoContactoSTtoIntervinienteST(@RequestPart(value="datoContactoST")@Valid DatoContactoSTInputDTO datoContactoSTInputDTO,
                                                                       @PathVariable Integer intervinienteId,
                                                                       @RequestParam(value = "fotos", required = false) List<MultipartFile> fotos,
                                                                       @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return skipTracingUseCase.createDatoContactoST(intervinienteId,datoContactoSTInputDTO,fotos, usuario);
  }

  @ApiOperation(
    value = "Actualizar dato de contacto de skipTracing",
    notes = "Actualizar dato de contacto de skipTracing")
  @PostMapping("/datoContacto/{datoContactoId}")
  public DatoContactoSkipTracingDTO editarDatoContacto(@PathVariable Integer datoContactoId,
                                                       @RequestPart(value="datoContactoST")@Valid DatoContactoSTInputDTO datoContactoSTInputDTO,
                                                       @RequestParam(value="fotos",required = false)List<MultipartFile>fotos,
                                                       @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return skipTracingUseCase.updateDatoContactoST(datoContactoId, datoContactoSTInputDTO,fotos, usuario);
  }

  @ApiOperation(
    value = "Validar DatoContacto",
    notes = "Valida a positivo o negativo el Dato de Contacto obtenido por id.")
  @PutMapping("/datoContacto/{datoContactoId}/validar")
  public DatoContactoSkipTracingDTO validarDC(@PathVariable Integer datoContactoId,
                                     @RequestParam Boolean validado) throws NotFoundException {
    return skipTracingUseCase.validarDC(datoContactoId, validado);
  }

  //Otros
  @PostMapping("/carga")
  public void carga(@RequestPart CargaSTInputDTO input,
                    @RequestParam("file")MultipartFile file,
                    @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    Integer usuarioId=usuario.getId();
    Usuario usuarionuevo=usuarioRepository.findById(usuarioId).orElse(null);
    skipTracingUseCase.carga(input, file, usuarionuevo);
  }

  @PostMapping("/export")
  public ResponseEntity<InputStreamResource> export(@RequestParam List<Integer> skipTracingId,
                                                    @RequestParam(required = false) Boolean todos,
                                                    @RequestParam(required = false) String empresa,
                                                    @ApiIgnore CustomUserDetails principal,
                                                    @RequestBody BusquedaDto busqueda) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    Integer usuarioId=usuario.getId();
    Usuario usuarionuevo=usuarioRepository.findById(usuarioId).orElse(null);
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(skipTracingUseCase.export(skipTracingId, todos, empresa, usuarionuevo, busqueda));
    Integer idCarga = skipTracingUseCase.findCarga(skipTracingId);
    String nombre = "U" + usuarionuevo.getId();
    if (idCarga != null) nombre = idCarga + "";
    return excelExport.download(excel, "ST_" + nombre + "_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }



  @ApiOperation(
    value = "Descargar imagenes en zip de un datoContactoST",
    notes = "Obtener imágenes del gestor Documental")
  @PostMapping("/descargarDocumentos")
  public ResponseEntity<byte[]> allDocumentos(
    @RequestParam(value = "listaIdGestor") List<Integer> listaIdGestor) throws IOException, EmptyFileException {

    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ZipOutputStream zos = new ZipOutputStream(bos);

    if (!listaIdGestor.isEmpty()) {
      for (Integer idGestor : listaIdGestor) {
        RespuestaDescargaDocumento respuestaDescargaDocumento =
          servicioGestorDocumental.descargarDocumento(idGestor);
        // InputStreamResource se crea a partir de un input
        InputStreamResource inputStreamResource =
          new InputStreamResource(respuestaDescargaDocumento.getBody());
        String filename = respuestaDescargaDocumento.getContentDisposition();
        String[] parts = filename.split("filename=");
        String nombrearchivo2 = parts[1];
        String nombrearchivofinal = nombrearchivo2.replace('"', ' ');
        ZipEntry zip = new ZipEntry(nombrearchivofinal);
        zos.putNextEntry(zip);
        int count;
        byte data[] = new byte[2048];
        BufferedInputStream entrysteam =
          new BufferedInputStream(respuestaDescargaDocumento.getBody());
        while ((count = entrysteam.read(data, 0, 2048)) != -1) {
          zos.write(data, 0, count);
        }
        entrysteam.close();
        zos.closeEntry();
      }
      zos.closeEntry();
      zos.close();
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
      return ResponseEntity.ok().headers(headers).body(bos.toByteArray());
    }else{
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new EmptyFileException("Does not present files");
      else throw new EmptyFileException("No presenta ficheros");
      }
  }
}
