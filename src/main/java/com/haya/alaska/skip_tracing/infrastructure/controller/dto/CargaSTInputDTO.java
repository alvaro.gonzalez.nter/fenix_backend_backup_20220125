package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CargaSTInputDTO implements Serializable {
  private Date fecha;
  private Boolean validado;
  private String encargado;
  private String comentario;
  private List<Integer> busquedas;
}
