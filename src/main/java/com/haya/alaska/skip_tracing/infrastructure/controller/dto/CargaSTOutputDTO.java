package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CargaSTOutputDTO implements Serializable {
  private Integer id;

  private Date fechaDescarga;
  private Date fechaActualizacion;
  private Date fechaInput;

  private Boolean validado;
  private String encargado;

  private List<CatalogoMinInfoDTO> busquedas = new ArrayList<>();
  private String comentario;
}
