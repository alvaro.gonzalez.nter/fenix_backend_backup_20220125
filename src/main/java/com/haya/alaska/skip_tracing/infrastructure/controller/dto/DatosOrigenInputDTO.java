package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DatosOrigenInputDTO implements Serializable {
  private String link;
  private Integer origenDato;
  private Date fecha;
  private Integer paginaPublica;
  private Integer redSocial;
  private Integer perfilProfesional;
  private String otraFuente;
  private String observaciones;
  private Date fechaActualizacion;
}
