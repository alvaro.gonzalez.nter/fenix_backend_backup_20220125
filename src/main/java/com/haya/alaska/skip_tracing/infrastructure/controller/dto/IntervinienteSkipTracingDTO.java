package com.haya.alaska.skip_tracing.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class IntervinienteSkipTracingDTO {

  private Integer id;
  private CatalogoMinInfoDTO tipoDocumento;
  private String numeroDocumento; // numeroDocumento
  private Boolean personaJuridica; // persona jurídica
  private CatalogoMinInfoDTO tipoIntervencion;
  private Integer ordenIntervencion;
  private String nombre; // nombre
  private String apellidos; // apellidos
  private CatalogoMinInfoDTO sexo; // sexo
  private CatalogoMinInfoDTO estadoCivil;
  private CatalogoMinInfoDTO residencia;
  private CatalogoMinInfoDTO nacionalidad;
  private CatalogoMinInfoDTO paisNacimiento;
  private CatalogoMinInfoDTO tipoContacto;
  private IntervinienteDTOToList intervinienteOrigen;
  private Date fechaNacimiento;
  private Boolean contactado;
  private Boolean estado;
  private String datosAdicionales1;
  private String datosAdicionales2;

  private String razonSocial;
  private Date fechaConstitucion;
  private Integer tipoGestor;

  private List<DatoDireccionSkipTracingDTO> datoDireccionSkipTracingDTOList;
  // DatosContacto,Emails y Redes y en función del tipoContacto va a una u otra
  private List<DatoContactoSkipTracingDTO> datoContactoSkipTracingDTOList;
  private List<DatoContactoSkipTracingDTO> emails;
  private List<DatoContactoSkipTracingDTO> redes;

  private String primerTitular;
}
