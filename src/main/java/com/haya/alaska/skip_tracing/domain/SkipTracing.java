package com.haya.alaska.skip_tracing.domain;

import com.haya.alaska.busqueda_skip_tracing.domain.BusquedaSkipTracing;
import com.haya.alaska.carga_skip_tracing.domain.CargaST;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.estado_skip_tracing.domain.EstadoSkipTracing;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import com.haya.alaska.nivel_skip_tracing.domain.NivelSkipTracing;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.tipo_busqueda.domain.TipoBusqueda;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_SKIP_TRACING")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_SKIP_TRACING")
public class SkipTracing {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO_INTERVINIENTE")
  private ContratoInterviniente contratoInterviniente;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_SKIP_TRACING")
  private EstadoSkipTracing estadoST;


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_NIVEL_SKIP_TRACING")
  private NivelSkipTracing nivelST;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERFIL_BUSQUEDA")
  private Perfil perfilBusqueda;


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA")
  private Provincia provincia;


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  private Usuario analistaST;


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_BUSQUEDA")
  private TipoBusqueda tipoBusqueda;


  /*@ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO_GESTOR")
  @NotNull
  private Usuario gestor;*/


  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_SOLICITUD")
  private Date fechaSolicitud;


  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ASIGNACION")
  private Date fechaAsignacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_INICIO_BUSQUEDA")
  private Date fechaInicioBusqueda;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FIN_BUSQUEDA")
  private Date fechaFinBusqueda;

  @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
  @JoinTable(
    name = "RELA_SKIP_TRACING_BUSQUEDA_SKIP_TRACING",
    joinColumns = @JoinColumn(name = "ID_SKIP_TRACING"),
    inverseJoinColumns = @JoinColumn(name = "ID_BUSQUEDA"))
  @AuditJoinTable(name = "HIST_RELA_SKIP_TRACING_BUSQUEDA_SKIP_TRACING")
  private Set<BusquedaSkipTracing> busquedas = new HashSet<>();


  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
    name = "RELA_SKIP_TRACING_INTERVINIENTE_SKIP_TRACING",
    joinColumns = @JoinColumn(name = "ID_SKIP_TRACING"),
    inverseJoinColumns = @JoinColumn(name = "ID_INTERVINIENTE_SKIP_TRACING"))
  @AuditJoinTable(name = "HIST_RELA_SKIP_TRACING_INTERVINIENTE_SKIP_TRACING")
  private Set<IntervinienteST> intervinientesST = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARGA_SKIP_TRACING")
  @NotNull
  private CargaST cargaST;

  //TODO LISTA DE CORREOS ELECTRÓNICOS
 /* @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SKIP_TRACING")
  @NotAudited
  private Set<CorreoElectronico> correoElectronicos = new HashSet<>();*/

  public void addIntervinientesST(List<IntervinienteST> intervinientesST) {
    this.intervinientesST.addAll(intervinientesST);
  }

  @Column(name = "DES_COMENTARIO")
  @Lob
  private String comentario;


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESPONSABLE_CARTERA")
  private Usuario responsableCartera;
}
