package com.haya.alaska.skip_tracing.domain;

import com.haya.alaska.dato_contacto_skip_tracing.domain.DatoContactoST;
import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.tipo_contacto.domain.TipoContacto;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;
import java.util.stream.Collectors;

public class DetectysExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("S/Reference");
      cabeceras.add("Person Type");
      cabeceras.add("NIF/CIF");
      cabeceras.add("Full Name");
      cabeceras.add("Last name1");
      cabeceras.add("Last name2");
      cabeceras.add("Name");

      cabeceras.add("Via Type 1");
      cabeceras.add("Via Name 1");
      cabeceras.add("Via Number 1");
      cabeceras.add("Rest Via 1");
      cabeceras.add("Location 1");
      cabeceras.add("Postal Code 1");
      cabeceras.add("Province Code 1");
      cabeceras.add("Via Type 2");
      cabeceras.add("Via Name 2");
      cabeceras.add("Via Number 2");
      cabeceras.add("Rest Via 2");
      cabeceras.add("Location 2");
      cabeceras.add("Postal Code 2");
      cabeceras.add("Province Code 2");
      cabeceras.add("Via Type 3");
      cabeceras.add("Via Name 3");
      cabeceras.add("Via Number 3");
      cabeceras.add("Rest Via 3");
      cabeceras.add("Location 3");
      cabeceras.add("Postal Code 3");
      cabeceras.add("Province Code 3");
      cabeceras.add("Via Type 4");
      cabeceras.add("Via Name 4");
      cabeceras.add("Via Number 4");
      cabeceras.add("Rest Via 4");
      cabeceras.add("Location 4");
      cabeceras.add("Postal Code 4");
      cabeceras.add("Province Code 4");
      cabeceras.add("Via Type 5");
      cabeceras.add("Via Name 5");
      cabeceras.add("Via Number 5");
      cabeceras.add("Rest Via 5");
      cabeceras.add("Location 5");
      cabeceras.add("Postal Code 5");
      cabeceras.add("Province Code 5");

      cabeceras.add("Birth Date");
      cabeceras.add("Phone1");
      cabeceras.add("Phone2");
      cabeceras.add("Phone3");
      cabeceras.add("Phone4");
      cabeceras.add("Phone5");
      cabeceras.add("Phone6");
      cabeceras.add("Phone7");
      cabeceras.add("Phone8");
      cabeceras.add("Phone9");
      cabeceras.add("Phone10");
      cabeceras.add("Phone11");
      cabeceras.add("Phone12");

      cabeceras.add("Holder's Phone1");
      cabeceras.add("Holder's Phone2");
      cabeceras.add("Holder's Phone3");
      cabeceras.add("Holder's Phone4");
      cabeceras.add("Holder's Phone5");
      cabeceras.add("Holder's Phone6");
      cabeceras.add("Holder's Phone7");
      cabeceras.add("Holder's Phone8");
      cabeceras.add("Address");
      cabeceras.add("Location");
      cabeceras.add("Postal Code");
      cabeceras.add("Province");
      cabeceras.add("Number of Judicial Incidents");
      cabeceras.add("Number of Administrative Incidents");
      cabeceras.add("Number of Competition Incidents");
      cabeceras.add("Date of death");
      cabeceras.add("Freelance");
      cabeceras.add("Organ");
      cabeceras.add("Absent");
      cabeceras.add("Number of Preliminary Investigations");
      cabeceras.add("Date of Last Investigation");
    } else {
      cabeceras.add("S/Referencia");
      cabeceras.add("Tipo Persona");
      cabeceras.add("NIF/CIF");
      cabeceras.add("Nombre Completo");
      cabeceras.add("Apellido1");
      cabeceras.add("Apellido2");
      cabeceras.add("Nombre");

      cabeceras.add("Tipo Via 1");
      cabeceras.add("Nombre Via 1");
      cabeceras.add("Numero Via 1");
      cabeceras.add("Resto Via 1");
      cabeceras.add("Localidad 1");
      cabeceras.add("Codigo Postal 1");
      cabeceras.add("Codigo Provincia 1");
      cabeceras.add("Tipo Via 2");
      cabeceras.add("Nombre Via 2");
      cabeceras.add("Numero Via 2");
      cabeceras.add("Resto Via 2");
      cabeceras.add("Localidad 2");
      cabeceras.add("Codigo Postal 2");
      cabeceras.add("Codigo Provincia 2");
      cabeceras.add("Tipo Via 3");
      cabeceras.add("Nombre Via 3");
      cabeceras.add("Numero Via 3");
      cabeceras.add("Resto Via 3");
      cabeceras.add("Localidad 3");
      cabeceras.add("Codigo Postal 3");
      cabeceras.add("Codigo Provincia 3");
      cabeceras.add("Tipo Via 4");
      cabeceras.add("Nombre Via 4");
      cabeceras.add("Numero Via 4");
      cabeceras.add("Resto Via 4");
      cabeceras.add("Localidad 4");
      cabeceras.add("Codigo Postal 4");
      cabeceras.add("Codigo Provincia 4");
      cabeceras.add("Tipo Via 5");
      cabeceras.add("Nombre Via 5");
      cabeceras.add("Numero Via 5");
      cabeceras.add("Resto Via 5");
      cabeceras.add("Localidad 5");
      cabeceras.add("Codigo Postal 5");
      cabeceras.add("Codigo Provincia 5");

      cabeceras.add("Fecha Nacimiento");
      cabeceras.add("Telefono1");
      cabeceras.add("Telefono2");
      cabeceras.add("Telefono3");
      cabeceras.add("Telefono4");
      cabeceras.add("Telefono5");
      cabeceras.add("Telefono6");
      cabeceras.add("Telefono7");
      cabeceras.add("Telefono8");
      cabeceras.add("Telefono9");
      cabeceras.add("Telefono10");
      cabeceras.add("Telefono11");
      cabeceras.add("Telefono12");

      cabeceras.add("Telefono del Titular1");
      cabeceras.add("Telefono del Titular2");
      cabeceras.add("Telefono del Titular3");
      cabeceras.add("Telefono del Titular4");
      cabeceras.add("Telefono del Titular5");
      cabeceras.add("Telefono del Titular6");
      cabeceras.add("Telefono del Titular7");
      cabeceras.add("Telefono del Titular8");
      cabeceras.add("Domicilio");
      cabeceras.add("Localidad");
      cabeceras.add("Codigo Postal");
      cabeceras.add("Provincia");
      cabeceras.add("Numero de Incidencias Judiciales");
      cabeceras.add("Numero de Incidencias Administrativas");
      cabeceras.add("Numero de Incidencias de Concurso");
      cabeceras.add("Fecha Fallecido");
      cabeceras.add("Autonomo");
      cabeceras.add("Organo");
      cabeceras.add("Ausente");
      cabeceras.add("Numero de Investigaciones Previas");
      cabeceras.add("Fecha Ultima Investigacion");
    }
  }

  public DetectysExcel(IntervinienteST iST) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      // Interviniente interviniente = skipTracing.getContratoInterviniente().getInterviniente();
      List<DatoDireccionST> direcciones =
          iST.getDatosDireccionST().stream()
              .sorted(Comparator.comparing(DatoDireccionST::getPrincipal))
              .collect(Collectors.toList());
      Set<DatoContactoST> datosContacto = iST.getDatosContactoST();
      this.add("S/Reference", iST.getId());
      this.add("Person Type", iST.getPersonaJuridica() ? "J" : "F");
      this.add("NIF/CIF", iST.getNumeroDocumento());
      this.add("Full Name", iST.getNombre() + " " + iST.getApellidos());
      this.add("Last name1", iST.getApellidos());
      this.add("Last name2", iST.getApellidos()); // FIXME No hay 2 apellidos
      this.add("Name", iST.getNombre());
      int i = 1;
      for (DatoDireccionST direccion : direcciones) {
        Direccion d = direccion.getDatoDireccion();
        if (d != null) {
          this.add("Via Name " + i, d.getNombre() != null ? d.getNombre() : null);
          this.add("Via Number " + i, d.getNumero() != null ? d.getNumero() : null);
        }
        this.add("Via Type " + i, direccion.getTipoVia() != null ? direccion.getTipoVia() : null);
        this.add(
            "Rest Via " + i, direccion.getComentario() != null ? direccion.getComentario() : null);
        this.add(
            "Location " + i, direccion.getLocalidad() != null ? direccion.getLocalidad() : null);
        this.add(
            "Postal Code " + i,
            direccion.getCodigoPostal() != null ? direccion.getCodigoPostal() : null);
        this.add(
            "Province Code " + i,
            direccion.getProvincia() != null ? direccion.getProvincia().getCodigo() : null);
        i++;
        if (i > 5) break;
      }

      this.add("Birth Date", iST.getFechaNacimiento());
      i = 1;
      for (DatoContactoST datoContacto : datosContacto) {
        TipoContacto tc = datoContacto.getTipoContacto();
        if (tc == null) continue;
        if (tc.getCodigo().equals("TEL_FIJ") || tc.getCodigo().equals("TEL_MOV")) {
          this.add("Phone" + i, datoContacto.getValor());
          i++;
        }
        if (i == 12) break;
      }
    } else {
      // Interviniente interviniente = skipTracing.getContratoInterviniente().getInterviniente();
      List<DatoDireccionST> direcciones =
          iST.getDatosDireccionST().stream()
              .sorted(Comparator.comparing(DatoDireccionST::getPrincipal))
              .collect(Collectors.toList());
      Set<DatoContactoST> datosContacto = iST.getDatosContactoST();
      this.add("S/Referencia", iST.getId());
      this.add("Tipo Persona", iST.getPersonaJuridica() ? "J" : "F");
      this.add("NIF/CIF", iST.getNumeroDocumento());
      this.add("Nombre Completo", iST.getNombre() + " " + iST.getApellidos());
      this.add("Apellido1", iST.getApellidos());
      this.add("Apellido2", iST.getApellidos()); // FIXME No hay 2 apellidos
      this.add("Nombre", iST.getNombre());
      int i = 1;
      for (DatoDireccionST direccion : direcciones) {
        Direccion d = direccion.getDatoDireccion();
        if (d != null) {
          this.add("Nombre Via " + i, d.getNombre() != null ? d.getNombre() : null);
          this.add("Numero Via " + i, d.getNumero() != null ? d.getNumero() : null);
        }
        this.add("Tipo Via " + i, direccion.getTipoVia() != null ? direccion.getTipoVia() : null);
        this.add(
            "Resto Via " + i, direccion.getComentario() != null ? direccion.getComentario() : null);
        this.add(
            "Localidad " + i, direccion.getLocalidad() != null ? direccion.getLocalidad() : null);
        this.add(
            "Codigo Postal " + i,
            direccion.getCodigoPostal() != null ? direccion.getCodigoPostal() : null);
        this.add(
            "Codigo Provincia " + i,
            direccion.getProvincia() != null ? direccion.getProvincia().getCodigo() : null);
        i++;
        if (i > 5) break;
      }

      this.add("Fecha Nacimiento", iST.getFechaNacimiento());
      i = 1;
      for (DatoContactoST datoContacto : datosContacto) {
        TipoContacto tc = datoContacto.getTipoContacto();
        if (tc == null) continue;
        if (tc.getCodigo().equals("TEL_FIJ") || tc.getCodigo().equals("TEL_MOV")) {
          this.add("Telefono" + i, datoContacto.getValor());
          i++;
        }
        if (i == 12) break;
      }
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
