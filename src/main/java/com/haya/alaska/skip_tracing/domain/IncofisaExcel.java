package com.haya.alaska.skip_tracing.domain;

import com.haya.alaska.dato_contacto_skip_tracing.domain.DatoContactoST;
import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.tipo_contacto.domain.TipoContacto;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;
import java.util.stream.Collectors;

public class IncofisaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      cabeceras.add("S/Reference");
      cabeceras.add("Id Debt");
      cabeceras.add("Name");
      cabeceras.add("Surnames");
      cabeceras.add("Customer NIF");
      cabeceras.add("Customer FNA");
      cabeceras.add("Customer POB");
      cabeceras.add("Customer TEL");
      cabeceras.add("Customer DIR");
      cabeceras.add("Customer MAIL");
      cabeceras.add("Customer PROV");
      cabeceras.add("Customer CPOSTAL");

      cabeceras.add("New Phones");
      cabeceras.add("Related Phones");
      cabeceras.add("Matching Phones");
    } else {
      cabeceras.add("S/Referencia");
      cabeceras.add("Id Deuda");
      cabeceras.add("Nombre");
      cabeceras.add("Apellidos");
      cabeceras.add("NIF Cliente");
      cabeceras.add("FNA Cliente");
      cabeceras.add("POB Cliente");
      cabeceras.add("TEL Cliente");
      cabeceras.add("DIR Cliente");
      cabeceras.add("MAIL Cliente");
      cabeceras.add("PROV Cliente");
      cabeceras.add("CPOSTAL Cliente");

      cabeceras.add("Telefonos Nuevos");
      cabeceras.add("Telefonos Afines");
      cabeceras.add("Telefonos Coincidentes");
    }
  }

  public IncofisaExcel(SkipTracing st,
    IntervinienteST iST) {
    super();
    DatoDireccionST direccion = null;
    List<DatoDireccionST> direcciones =
      iST.getDatosDireccionST()
        .stream()
        .sorted(Comparator.comparing(DatoDireccionST::getPrincipal))
        .collect(Collectors.toList());
    if (direcciones.size() > 0) {
      direccion = direcciones.get(0);
    }
    Set<DatoContactoST> datosContacto = iST.getDatosContactoST();
    String email = "";
    for (DatoContactoST dcST : datosContacto) {
      TipoContacto tc = dcST.getTipoContacto();
      if (tc == null) continue;
      if (tc.getCodigo().equals("MAIL")){
        email += dcST.getValor() + " ";
      }
    }
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      this.add("S/Reference", iST.getId());
      this.add("Id Debt", st.getContratoInterviniente().getContrato().getId());
      this.add("Name", iST.getNombre());
      this.add("Surnames", iST.getApellidos());
      this.add("Customer NIF", iST.getNumeroDocumento());
      this.add("Customer FNA", iST.getFechaNacimiento());
      this.add("Customer POB", direccion != null ? direccion.getLocalidad() : null);
      this.add("Customer TEL", iST.getInterviniente().getTelefonoPrincipal());
      this.add("Customer DIR", direccion != null ? direccion.getDatoDireccion()!=null ?direccion.getDatoDireccion().getNombre() : null:null);
      this.add("Customer MAIL", email);
      this.add("Customer PROV", direccion != null ? direccion.getProvincia() != null ? direccion.getProvincia().getValor() : null : null);
      this.add("Customer CPOSTAL", direccion != null ? direccion.getCodigoPostal() : null);
    } else {
      this.add("S/Referencia", iST.getId());
      this.add("Id Deuda", st.getContratoInterviniente().getContrato().getId());
      this.add("Nombre", iST.getNombre());
      this.add("Apellidos", iST.getApellidos());
      this.add("NIF Cliente", iST.getNumeroDocumento());
      this.add("FNA Cliente", iST.getFechaNacimiento());
      this.add("POB Cliente", direccion != null ? direccion.getLocalidad() : null);
      this.add("TEL Cliente", iST.getInterviniente().getTelefonoPrincipal());
      this.add("DIR Cliente", direccion != null ? direccion.getDatoDireccion()!=null ?direccion.getDatoDireccion().getNombre() : null:null);
      this.add("MAIL Cliente", email);
      this.add("PROV Cliente", direccion != null ? direccion.getProvincia() != null ? direccion.getProvincia().getValor() : null : null);
      this.add("CPOSTAL Cliente", direccion != null ? direccion.getCodigoPostal() : null);
    }
  }


  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
