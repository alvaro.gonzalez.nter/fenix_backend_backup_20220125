package com.haya.alaska.IntervaloImportesSegmentacion.domain;


import com.haya.alaska.segmentacion.domain.Segmentacion;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_INTERVALO_IMPORTES_SEGMENTACION")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_INTERVALO_IMPORTES_SEGMENTACION")
public class IntervaloImportesSegmentacion {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name="DES_DESCRIPION")
  private String descripcion;

  @Column(name="NUM_MAYOR")
  private Double mayor;

  @Column(name="NUM_MENOR")
  private Double menor;

  @Column(name="NUM_IGUAL")
  private Double igual;

  /*@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_EXPEDIENTE")
  private Segmentacion segmentacionExpediente;*/

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_CONTRATO")
  private Segmentacion segmentacionContrato;

  /*@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_GARANTIA")
  private Segmentacion segmentacionGarantia;*/

  /*@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_BIEN_PN")
  private Segmentacion segmentacionBienPN;*/

}
