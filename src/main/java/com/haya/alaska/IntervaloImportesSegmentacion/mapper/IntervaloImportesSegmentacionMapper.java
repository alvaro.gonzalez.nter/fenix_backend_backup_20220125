package com.haya.alaska.IntervaloImportesSegmentacion.mapper;

import com.haya.alaska.IntervaloImportesSegmentacion.controller.dto.IntervaloImportesSegmentacionDTO;
import com.haya.alaska.IntervaloImportesSegmentacion.domain.IntervaloImportesSegmentacion;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class IntervaloImportesSegmentacionMapper {

  public IntervaloImportesSegmentacion dtoAEntidad(IntervaloImportesSegmentacionDTO dto) {

    if(dto == null) return null;

    IntervaloImportesSegmentacion intervaloImportesSegmentacion = new IntervaloImportesSegmentacion();
    intervaloImportesSegmentacion.setDescripcion(dto.getDescripcion() != null ? dto.getDescripcion() : null);
    intervaloImportesSegmentacion.setIgual(dto.getIgual() != null ? dto.getIgual().doubleValue() : null);
    intervaloImportesSegmentacion.setMayor(dto.getMayor() != null ? dto.getMayor().doubleValue() : null);
    intervaloImportesSegmentacion.setMenor(dto.getMenor() != null ? dto.getMenor().doubleValue() : null);
    return intervaloImportesSegmentacion;
  }

  public IntervaloImportesSegmentacionDTO entidadADto(IntervaloImportesSegmentacion intervaloImportesSegmentacion) {

    if(intervaloImportesSegmentacion == null) return null;

    IntervaloImportesSegmentacionDTO dto = new IntervaloImportesSegmentacionDTO();
    dto.setDescripcion(intervaloImportesSegmentacion.getDescripcion() != null ? intervaloImportesSegmentacion.getDescripcion() : null);
    dto.setIgual(intervaloImportesSegmentacion.getIgual() != null ? intervaloImportesSegmentacion.getIgual().intValue() : null);
    dto.setMayor(intervaloImportesSegmentacion.getMayor() != null ? intervaloImportesSegmentacion.getMayor().intValue() : null);
    dto.setMenor(intervaloImportesSegmentacion.getMenor() != null ? intervaloImportesSegmentacion.getMenor().intValue() : null);
    return dto;
  }

  public Set<IntervaloImportesSegmentacion> dtoListAEntidadList(List<IntervaloImportesSegmentacionDTO> listaDto) {

    if(listaDto == null)
      return null;

    Set<IntervaloImportesSegmentacion> lista = new HashSet<>();
    for(IntervaloImportesSegmentacionDTO dto: listaDto) {
      lista.add(dtoAEntidad(dto));
    }
    return lista;
  }

  public List<IntervaloImportesSegmentacionDTO> entidadListADtoList(Set<IntervaloImportesSegmentacion> lista) {

    if(lista == null)
      return null;

    List<IntervaloImportesSegmentacionDTO> listaDto = new ArrayList<>();
    for(IntervaloImportesSegmentacion entidad: lista) {
      listaDto.add(entidadADto(entidad));
    }
    return listaDto;
  }
}
