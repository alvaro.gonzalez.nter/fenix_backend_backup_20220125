package com.haya.alaska.IntervaloImportesSegmentacion.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class IntervaloImportesSegmentacionDTO {

  private String descripcion;
  private Integer igual;
  private Integer menor;
  private Integer mayor;


}
