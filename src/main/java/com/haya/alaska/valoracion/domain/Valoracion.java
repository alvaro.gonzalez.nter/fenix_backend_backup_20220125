package com.haya.alaska.valoracion.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.tipo_valoracion.domain.TipoValoracion;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_VALORACION")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_VALORACION")
public class Valoracion implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  private Bien bien;

  @Column(name = "DES_VALORADOR")
  private String valorador;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LIQUIDEZ")
  private Liquidez liquidez;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_VALORACION")
  private TipoValoracion tipoValoracion;

  @Column(name = "NUM_IMPORTE")
  private Double importe;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA")
  private Date fecha;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  public long getFechaNull() {
    if (this.fecha == null) return 0;
    return this.fecha.getTime();
  }

  @Override
  public boolean equals(Object o) {
    Valoracion valoracion = (Valoracion) o;
    if (this.getBien().getIdCarga().equals(valoracion.getBien().getIdCarga())
      && this.getValorador().equals(valoracion.getValorador())
      && Double.compare(this.getImporte(), valoracion.getImporte()) == 0
      && this.getFechaNull() == valoracion.getFechaNull()) {
      return true;
    }
    return false;
  }
}
