package com.haya.alaska.valoracion.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ValoracionGarantiaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Warranty ID");
      cabeceras.add("Valuation amount");
      cabeceras.add("Valuation date");
      cabeceras.add("Valuator");
      cabeceras.add("Valuation type");
      cabeceras.add("Liquidity");

    } else {

      cabeceras.add("Id Garantia");
      cabeceras.add("Importe Valoracion");
      cabeceras.add("Fecha Valoracion");
      cabeceras.add("Valorador");
      cabeceras.add("Tipo Valoracion");
      cabeceras.add("Liquidez");
    }
  }

  public ValoracionGarantiaExcel(Valoracion valoracion, Integer idBien) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      this.add("Warranty ID", idBien);
      this.add("Valuation amount", valoracion.getImporte());
      this.add("Valuation date", valoracion.getFecha());
      this.add("Valuator", valoracion.getValorador());
      this.add("Valuation type", valoracion.getTipoValoracion());
      this.add("Liquidity", valoracion.getLiquidez());

    } else {

      this.add("Id Garantia", idBien);
      this.add("Importe Valoracion", valoracion.getImporte());
      this.add("Fecha Valoracion", valoracion.getFecha());
      this.add("Valorador", valoracion.getValorador());
      this.add("Tipo Valoracion", valoracion.getTipoValoracion());
      this.add("Liquidez", valoracion.getLiquidez());
      }

  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
