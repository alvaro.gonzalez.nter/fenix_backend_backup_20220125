package com.haya.alaska.valoracion.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ValoracionExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Warranty ID/Rating");
      cabeceras.add("Valuation amount");
      cabeceras.add("Valuation date");
      cabeceras.add("Appraiser");
      cabeceras.add("Valuation type");
      cabeceras.add("Liquidity");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Garantía/Valoración");
      cabeceras.add("Importe Valoración");
      cabeceras.add("Fecha Valoración");
      cabeceras.add("Tasadora");
      cabeceras.add("Tipo Valoración");
      cabeceras.add("Liquidez");

    }

  }

  public ValoracionExcel(
      Valoracion valoracion, String idContrato, Integer idBien, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Warranty ID/Rating", idBien);
      this.add("Valuation amount", valoracion.getImporte());
      this.add("Appraiser", valoracion.getFecha());
      this.add("Tasadora", valoracion.getValorador());
      this.add(
        "Valuation type",
        valoracion.getTipoValoracion() != null ? valoracion.getTipoValoracion().getValorIngles() : null);
      this.add(
        "Liquidity", valoracion.getLiquidez() != null ? valoracion.getLiquidez().getValorIngles() : null);


    } else {

    this.add("Id Expediente", idExpediente);
    this.add("Id Contrato", idContrato);
    this.add("Id Garantía/Valoración", idBien);
    this.add("Importe Valoración", valoracion.getImporte());
    this.add("Fecha Valoración", valoracion.getFecha());
    this.add("Tasadora", valoracion.getValorador());
    this.add(
        "Tipo Valoración",
        valoracion.getTipoValoracion() != null ? valoracion.getTipoValoracion().getValor() : null);
    this.add(
        "Liquidez", valoracion.getLiquidez() != null ? valoracion.getLiquidez().getValor() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
