package com.haya.alaska.valoracion.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class ValoracionDto {

  private Integer id;
  private String valorador;
  private String tipoValoracion;
  private Double importe;
  private Date fecha;
  private String liquidez;
}
