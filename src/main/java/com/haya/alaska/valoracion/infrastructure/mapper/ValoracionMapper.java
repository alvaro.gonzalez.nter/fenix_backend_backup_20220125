package com.haya.alaska.valoracion.infrastructure.mapper;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.shared.dto.ValoracionDTOToList;
import com.haya.alaska.tipo_valoracion.domain.TipoValoracion;
import com.haya.alaska.tipo_valoracion.infrastructure.repository.TipoValoracionRepository;
import com.haya.alaska.valoracion.controller.dto.ValoracionDto;
import com.haya.alaska.valoracion.domain.Valoracion;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class ValoracionMapper {

  @Autowired
  private CatalogoMinInfoMapper catalogoMinInfoMapper;
  @Autowired TipoValoracionRepository tipoValoracionRepository;
  @Autowired LiquidezRepository liquidezRepository;

  public ValoracionDTOToList valoracionToValoracionDTOToList(Valoracion valoracion) {
    ValoracionDTOToList valoracionDTOToList = new ValoracionDTOToList();
    valoracionDTOToList.setId(valoracion.getId());
    valoracionDTOToList.setValorador(valoracion.getValorador());
    valoracionDTOToList.setTipoValoracion(catalogoMinInfoMapper.entityToDto(valoracion.getTipoValoracion()));
    valoracionDTOToList.setImporte(valoracion.getImporte());
    valoracionDTOToList.setFecha(valoracion.getFecha());
    valoracionDTOToList.setLiquidez(catalogoMinInfoMapper.entityToDto(valoracion.getLiquidez()));

    return valoracionDTOToList;
  }

  public List<ValoracionDTOToList> listValoracionToListValoracionDTOToList(Set<Valoracion> valoraciones) {
    List<ValoracionDTOToList> valoracionDTOToLists = new ArrayList<>();
    for (Valoracion valoracion: valoraciones) {
      valoracionDTOToLists.add(valoracionToValoracionDTOToList(valoracion));
    }
    return valoracionDTOToLists;
  }

  public Valoracion updateFields(ValoracionDto valoracionDto, Valoracion valoracion, TipoValoracion tipoValoracion, Liquidez liquidez) {

    BeanUtils.copyProperties(valoracionDto, valoracion, "id");
    valoracion.setTipoValoracion(tipoValoracion);
    valoracion.setLiquidez(liquidez);

    return valoracion;
  }

  public Valoracion parseToEntity(ValoracionDto valoracionDTO, Bien bien, TipoValoracion tipoValoracion, Liquidez liquidez){
    Valoracion valoracion = new Valoracion();
    BeanUtils.copyProperties(valoracionDTO, valoracion);
    valoracion.setActivo(true);
    valoracion.setBien(bien);
    if(tipoValoracion != null) valoracion.setTipoValoracion(tipoValoracion);
    if(liquidez != null) valoracion.setLiquidez(liquidez);

    return valoracion;
  }

  public Valoracion dtoToEntity(ValoracionDto valoracionDto, boolean rem){
    Valoracion valoracion = new Valoracion();

    valoracion.setActivo(true);
    valoracion.setId(valoracionDto.getId());
    valoracion.setValorador(valoracionDto.getValorador());
    valoracion.setImporte(valoracionDto.getImporte());
    valoracion.setFecha(valoracionDto.getFecha());
    if(!rem) {
      valoracion.setTipoValoracion(valoracionDto.getTipoValoracion() != null ? tipoValoracionRepository.findById(Integer.valueOf(valoracionDto.getTipoValoracion())).orElse(null) : null);
      valoracion.setLiquidez(valoracionDto.getLiquidez() != null ? liquidezRepository.findById(Integer.valueOf(valoracionDto.getLiquidez())).orElse(null) : null);
    }else{
      valoracion.setTipoValoracion(valoracionDto.getTipoValoracion() != null ? tipoValoracionRepository.findByCodigo(valoracionDto.getTipoValoracion()).orElse(null) : null);
      valoracion.setLiquidez(valoracionDto.getLiquidez() != null ? liquidezRepository.findByCodigo(valoracionDto.getLiquidez()).orElse(null) : null);
    }
    return valoracion;
  }

  public ValoracionDto entityToDto(Valoracion valoracion){
    ValoracionDto valoracionDto = new ValoracionDto();
    if (valoracion != null) {
      valoracionDto.setId(valoracion.getId());
      valoracionDto.setValorador(valoracion.getValorador());
      if (valoracion.getTipoValoracion() != null)
        valoracionDto.setTipoValoracion(valoracion.getTipoValoracion().getId().toString());
      valoracionDto.setImporte(valoracion.getImporte());
      valoracionDto.setFecha(valoracion.getFecha());
      if (valoracion.getLiquidez() != null)
        valoracionDto.setLiquidez(valoracion.getLiquidez().getId().toString());
      }
    return valoracionDto;
  }
}
