package com.haya.alaska.valoracion.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.valoracion.domain.Valoracion;

@Repository
public interface ValoracionRepository extends JpaRepository<Valoracion, Integer> {}
