package com.haya.alaska.campania.domain;

import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_CAMPANIA")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_CAMPANIA")
public class Campania {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "DES_NOMBRE_CAMPANIA", nullable = false)
  private String nombre;

  @Column(name = "DES_DESCRIPCION")
  private String descripcion;

  @Column(name = "DES_FECHA_INICIO", nullable = false)
  private Date fechaInicio;

  @Column(name = "DES_FECHA_FIN", nullable = false)
  private Date fechaFin;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CAMPANIA")
  @NotAudited
  private Set<CampaniaAsignada> campaniasAsignadas = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CAMPANIA")
  @NotAudited
  private Set<CampaniaAsignadaBienPN> campaniasAsignadasPN = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID")
  private Usuario usuario;

}
