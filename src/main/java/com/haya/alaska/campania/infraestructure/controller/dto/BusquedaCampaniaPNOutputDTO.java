package com.haya.alaska.campania.infraestructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Data;

import java.util.List;

@Data
public class BusquedaCampaniaPNOutputDTO {
  private Integer idExpediente;//done
  private String idConcatenado;//done
  private Integer idBienPN;//done
  private String campania;//done
  private Integer idCampania;//done
  private String gestor;//done
  private String cliente;//done
  private String cartera;//done
  private Double saldoGestion;//done
  private CatalogoMinInfoDTO estrategia;//done
  private String supervisor;//
  private List<String> area;
  private String estado;
}
