package com.haya.alaska.campania.infraestructure.mapper;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDto;
import com.haya.alaska.bien_posesion_negociada.infrastructure.mapper.BienPosesionNegociadaMapper;
import com.haya.alaska.campania.infraestructure.controller.dto.CampaniasDesasignadas;
import com.haya.alaska.campania.infraestructure.controller.dto.CampaniasRelacionadas;
import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDto;
import com.haya.alaska.contrato.infrastructure.mapper.ContratoMapper;

@Component
public class CampaniaMapper {

  @Autowired
  private ContratoMapper contratoMapper;

  @Autowired
  private BienPosesionNegociadaMapper bienPosesionNegociadaMapper;


  /**
   * Convertir una entidad de Contrato a ContratoDTO mediante su Mapper de EntityToDTO
   *
   * @param contrato
   * @return ContratoDTO
   */
  public ContratoDto inputContratoToDTO(Contrato contrato) {
    return contratoMapper.entityToOutputDto(contrato);
  }

  /**
   * Obtener un listado de ContratoDTO desde un listado de Entidades de Contrato mediante su Mapper de EntityToDTO
   *
   * @param listadoContratos
   * @return Listado de ContratoDto
   */
  public List<ContratoDto> inputAllContratoToDTO(List<Contrato> listadoContratos) {
    return listadoContratos.stream()
      .map(contrato -> inputContratoToDTO(contrato))
      .collect(Collectors.toList());
  }

  /**
   * Convertir una entidad de BienPosesionNegociada a BienPosesionNegociadaDTO mediante su Mapper de EntityToDTO
   *
   * @param bienPosesionNegociada
   * @return BienPosesionNegociadaDTO
   */
  public BienPosesionNegociadaDto inputBienPosesionNegociadaToDto(BienPosesionNegociada bienPosesionNegociada) {
    return bienPosesionNegociadaMapper.entityOutputDto(bienPosesionNegociada);
  }

  /**
   * Obtener un listado de BienPosesionNegociadaDto desde un listado de Entidades de BienPosesionNegociada mediante su Mapper de EntityToDTO
   *
   * @param listadoBienPosesionNegociada
   * @return Listado de BienPosesionNegociadaDto
   */
  public List<BienPosesionNegociadaDto> inputAllBienPosesionNegociadaToDTO(List<BienPosesionNegociada> listadoBienPosesionNegociada) {
    return listadoBienPosesionNegociada.stream()
      .map(bpn -> inputBienPosesionNegociadaToDto(bpn))
      .collect(Collectors.toList());
  }

  /**
   * Obtener un listado de ContratoDTO desde un Listado de CampaniaAsignada
   *
   * @param listCampaniaAsignada
   * @return Listado de ContratoDto
   */
  public List<ContratoDto> convertAsignadasToContratoDTO(List<CampaniaAsignada> listCampaniaAsignada) {
    return listCampaniaAsignada.stream()
      .map(asignada -> inputContratoToDTO(asignada.getContrato()))
      .collect(Collectors.toList());
  }

  /**
   * Obtener un listado de BienPosesionNegociadaDto desde un Listado de CampaniaAsignadaPN
   *
   * @param listCampaniaAsignadaPN
   * @return Listado de BienPosesionNegociadaDto
   */
  public List<BienPosesionNegociadaDto> convertAsignadasToBienPosesionNegociadaDTO(List<CampaniaAsignadaBienPN> listCampaniaAsignadaPN) {
    return listCampaniaAsignadaPN.stream()
      .map(asignada -> inputBienPosesionNegociadaToDto(asignada.getBienPosesionNegociada()))
      .collect(Collectors.toList());
  }

  /**
   * Mapeo de Campañas con relación de Ids de Contratos / Bienes de Posesion Negociada
   * 
   * @param campaniaDesasignada
   * @param contrato
   * @return
   */
  public CampaniasRelacionadas mapToCampaniasRelacionadas(CampaniasDesasignadas campaniaDesasignada, boolean contrato) {
    CampaniasRelacionadas cbpnD = new CampaniasRelacionadas();
    cbpnD.setIdCampania(campaniaDesasignada.getIdCampania());
    
    if (contrato) {
      cbpnD.setIdsRelacion(campaniaDesasignada.getIdsContrato());
    } else {
      cbpnD.setIdsRelacion(campaniaDesasignada.getIdsBienPN());
    }
    
    return cbpnD;
  }
  
  /**
   * Mapeo de un listado de ContratoDTO desde un Listado de CampaniaAsignada
   *
   * @param campaniasAsignadas
   * @return List<ContratoDto>
   */
  public List<ContratoDto> getListContratoDTO(Set<CampaniaAsignada> campaniasAsignadas) {
	return this.convertAsignadasToContratoDTO(Lists.newArrayList(campaniasAsignadas));
  }

  /**
   * Mapeo de un listado de ContratoDTO desde un Listado de CampaniaAsignada
   *
   * @param campaniasAsignadasPN
   * @return List<BienPosesionNegociadaDto>
   */
  public List<BienPosesionNegociadaDto> getListBienPosesionesNegociadosDTO(Set<CampaniaAsignadaBienPN> campaniasAsignadasPN) {
	return this.convertAsignadasToBienPosesionNegociadaDTO(Lists.newArrayList(campaniasAsignadasPN));
  }

}
