package com.haya.alaska.campania.infraestructure.controller.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CampaniasDesasignadas implements Serializable {
  
  private static final long serialVersionUID = 1L;

  private Integer idCampania;
    
  private List<Integer> idsContrato;

  private List<Integer> idsBienPN;
  
}
