package com.haya.alaska.campania.infraestructure.controller.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDto;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDto;

import lombok.Data;

@Data
public class CampaniaOutputDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer id;

  private String nombre;

  private String descripcion;

  private Date fechaInicio;

  private Date fechaFin;

  private List<ContratoDto> contratos;

  private List<BienPosesionNegociadaDto> bienPosesionNegociada;

  private Integer idUsuario;

  private String usuario;

}
