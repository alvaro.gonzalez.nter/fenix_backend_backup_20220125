package com.haya.alaska.campania.infraestructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Data;

import java.util.List;

@Data
public class BusquedaCampaniaOutputDTO {
  private Integer idExpediente;
  private String idConcatenado;
  private Integer idContrato;
  private String producto;
  private String estado;
  private String cliente;
  private String cartera;
  private String titular;
  private Double saldoGestion;
  private CatalogoMinInfoDTO estrategia;
  private String supervisor;
  private List<String> area;
  private String gestor;
  private String campania;
  private Integer idCampania;
  private String idCarga;

}
