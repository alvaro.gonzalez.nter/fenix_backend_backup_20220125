package com.haya.alaska.campania.infraestructure.repository;

import com.haya.alaska.campania.domain.Campania;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CampaniaRepository extends JpaRepository<Campania, Integer> {

  List<Campania> findByIdIn(List<Integer> ids);
  List<Campania>findAllByfechaFinLessThanAndActivoIsTrue(Date fechaActual);
  Optional<Campania> findByNombre(String nombre);
  List<Campania> findAllByActivoIsTrue();
}
