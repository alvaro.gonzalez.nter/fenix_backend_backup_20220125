package com.haya.alaska.campania.infraestructure.controller.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Component
public class CampaniaInputDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String nombre;

  private String descripcion;

  private Date fechaInicio;

  private Date fechaFin;

}
