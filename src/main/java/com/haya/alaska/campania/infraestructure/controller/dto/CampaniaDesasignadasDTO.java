package com.haya.alaska.campania.infraestructure.controller.dto;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CampaniaDesasignadasDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private BusquedaDto busquedaDto;

  private Integer idCampania;

  private List<Integer> idsContrato;

  private List<Integer> idsBienPN;

}
