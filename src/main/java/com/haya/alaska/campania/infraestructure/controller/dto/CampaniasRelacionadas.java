package com.haya.alaska.campania.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaniasRelacionadas {

  private Integer idCampania;

  private List<Integer> idsRelacion;
}
