package com.haya.alaska.campania.infraestructure.controller.dto;

import java.io.Serializable;
import java.util.List;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DesasignarCampaniasDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private BusquedaDto busquedaDto;

  List<CampaniasDesasignadas> campanias;

}
