package com.haya.alaska.campania.infraestructure.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.campania.application.CampaniaUseCase;
import com.haya.alaska.campania.infraestructure.controller.dto.CampaniaDTO;
import com.haya.alaska.campania.infraestructure.controller.dto.CampaniaInputDTO;
import com.haya.alaska.campania.infraestructure.controller.dto.CampaniaOutputDTO;
import com.haya.alaska.campania.infraestructure.controller.dto.DesasignarCampaniasDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.usuario.domain.Usuario;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/campania")
public class CampaniaController {

  @Autowired
  private CampaniaUseCase campaniaUseCase;

  @ApiOperation(value = "Obtener una Campaña por su ID")
  @GetMapping("/{id}")
  public ResponseEntity<CampaniaOutputDTO> obtenerCampania(@PathVariable("id") int idCampania) {
    return campaniaUseCase.getCampania(idCampania) == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(campaniaUseCase.getCampania(idCampania));
  }

  @ApiOperation(value = "Obtener todas las Campañas")
  @GetMapping("/campanias")
  public List<CampaniaOutputDTO> listadoCampanias() {
    return campaniaUseCase.getAllCampanias();
  }

  @ApiOperation(value = "Crear una Campaña")
  @PostMapping("/nueva")
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<CampaniaDTO> createNuevaCampania(@RequestBody CampaniaInputDTO campaniaDTO, @ApiIgnore CustomUserDetails principal) throws Exception {
    return ResponseEntity.ok(campaniaUseCase.crearNuevaCampania((Usuario) principal.getPrincipal(), campaniaDTO));
  }

  @ApiOperation(value = "Editado de una campaña")
  @PutMapping("/{id}")
  public ResponseEntity<CampaniaDTO> editarCampania(@PathVariable("id") int idCampania, @RequestBody CampaniaInputDTO campaniaDTO, @ApiIgnore CustomUserDetails principal) {
    return ResponseEntity.ok(campaniaUseCase.editarCampania((Usuario) principal.getPrincipal(), idCampania, campaniaDTO));
  }

  @ApiOperation(value = "Borrado lógico de una campaña")
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteCampania(@PathVariable("id") int idCampania, @ApiIgnore CustomUserDetails principal) {
    return ResponseEntity.ok(campaniaUseCase.borrarCampania((Usuario) principal.getPrincipal(), idCampania));
  }

  @ApiOperation(value = "Asignar una campaña a un Contrato o Bien de Posesión Negociada")
  @PostMapping("/asignar/{id}")
  public ResponseEntity<CampaniaDTO> asignarCampania(@PathVariable("id") int idCampania,
                                                     @ApiIgnore CustomUserDetails principal,
                                                     @RequestBody(required = false) BusquedaDto busquedaDto,
                                                     @RequestParam(value = "idsContratos", required = false) List<Integer> idsContratos,
                                                     @RequestParam(value = "idsBienesPN", required = false) List<Integer> idsBienesPN,
                                                     @RequestParam(required = false) boolean todos,
                                                     @RequestParam(required = false) boolean todosPN) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return ResponseEntity.ok(campaniaUseCase.asignarCampania(usuario, busquedaDto, idCampania, idsContratos, idsBienesPN, todos, todosPN));
  }

  @ApiOperation(value = "Desasignar una o varias campañas a un Contrato o Bien de Posesión Negociada")
  @PostMapping("/desasignar")
  public ResponseEntity<Void> desasignarCampania(@RequestBody(required = false) DesasignarCampaniasDTO campaniaDesasignadas,
                                                 @RequestParam(required = false) boolean todos,
                                                 @RequestParam(required = false) boolean todosPN,
                                                 @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return ResponseEntity.ok(campaniaUseCase.desasignarCampania(usuario, campaniaDesasignadas, todos, todosPN));
  }

  /*@ApiOperation(value = "Comprobar las campañas que están expiradas")
  @GetMapping("/expired")
  public ResponseEntity<List<CampaniaDTO>> checkCampaniaExpirada() throws Exception {
    return ResponseEntity.ok(campaniaUseCase.checkCampaniaExpirada());
  }*/

}
