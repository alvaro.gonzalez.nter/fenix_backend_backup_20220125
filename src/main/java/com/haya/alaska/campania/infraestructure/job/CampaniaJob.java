package com.haya.alaska.campania.infraestructure.job;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.campania.application.CampaniaUseCaseImpl;
import com.haya.alaska.campania.domain.Campania;
import com.haya.alaska.campania.infraestructure.repository.CampaniaRepository;
import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.campania_asignada.infraestructure.repository.CampaniaAsignadaRepository;
import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.campania_asignada_pn.infraestructure.repository.CampaniaAsignadaBienPNRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CampaniaJob {

    @Autowired
    private CampaniaRepository campaniaRepository;

    @Autowired
    private CampaniaAsignadaRepository campaniaAsignadaRepository;

    @Autowired
    private CampaniaAsignadaBienPNRepository campaniaAsignadaBienPNRepository;

    @Autowired
    private CampaniaUseCaseImpl campaniaUseCaseImpl;

    @Autowired
    private ContratoRepository contratoRepository;

  @Autowired
  private BienPosesionNegociadaRepository bienPosesionNegociadaRepository;


  @Scheduled(cron = "0 30 1 * * *")
  public void checkCampaniaExpired() {
	// Obtenemos la fecha actual
	Date fechaactual = new Date(System.currentTimeMillis());

    List<Campania>listCampaniasExpiradas=campaniaRepository.findAllByfechaFinLessThanAndActivoIsTrue(fechaactual);

    // Se recorre las campañas asociadas a contratos y bienes para eliminar sus relaciones del repositorio
	for (Campania campania : listCampaniasExpiradas) {
            // Comprobamos todos los Contratos asociados a esta Campaña
    List<CampaniaAsignada>campaniaAsignadaList=campania.getCampaniasAsignadas().stream().collect(Collectors.toList());
    List<CampaniaAsignadaBienPN>campaniaAsignadaBienPNList=campania.getCampaniasAsignadasPN().stream().collect(Collectors.toList());
   // System.out.println(campaniaAsignadaList.get(0).getCampania().getNombre());
            if(campaniaAsignadaList != null) {
		// Obtenemos los ids de los Contratos Asignados

              for (CampaniaAsignada campaniaAsignada:campaniaAsignadaList) {
                Contrato contrato = contratoRepository.findById(campaniaAsignada.getContrato().getId()).get();
                contrato.getCampaniasAsignadas().remove(contrato);
                contratoRepository.save(contrato);
                campaniaAsignadaRepository.delete(campaniaAsignada);
                Campania campania1=campaniaAsignada.getCampania();
                campania1.setActivo(false);
                campaniaRepository.save(campania1);
              }
             }

            // Comprobamos todos los Bienes de Posesion Negociada asociados a esta Campaña
            if(campaniaAsignadaBienPNList != null) {
		// Obtenemos los ids de los Bienes Asignados
              for (CampaniaAsignadaBienPN campaniaAsignadabienPN:campaniaAsignadaBienPNList) {
                BienPosesionNegociada bienPN = bienPosesionNegociadaRepository.findById(campaniaAsignadabienPN.getBienPosesionNegociada().getId()).get();
                bienPN.getCampaniasAsignadasPN().remove(bienPN);
                bienPosesionNegociadaRepository.save(bienPN);
                campaniaAsignadaBienPNRepository.delete(campaniaAsignadabienPN);
                Campania campania1=campaniaAsignadabienPN.getCampania();
                campania1.setActivo(false);
                campaniaRepository.save(campania1);
              }
	    }
	}
    }

}
