package com.haya.alaska.campania.application;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.campania.infraestructure.controller.dto.*;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.List;

public interface CampaniaUseCase {

  CampaniaOutputDTO getCampania(int idCampania);

  List<CampaniaOutputDTO> getAllCampanias();

  CampaniaDTO crearNuevaCampania(Usuario usuario, CampaniaInputDTO campaniaDTO) throws Exception;

  CampaniaDTO editarCampania(Usuario usuario, int idCampania, CampaniaInputDTO campaniaInputDTO);

  Void borrarCampania(Usuario usuario, int idCampania);

  CampaniaDTO asignarCampania(Usuario usuario, BusquedaDto busquedaDto, int idCampania, List<Integer> idsContratos, List<Integer> idsBienesPN, boolean todos, boolean todosPN) throws Exception;

  Void desasignarCampania(Usuario usuario, DesasignarCampaniasDTO campaniaDesasignadas, boolean todos, boolean todosPN) throws Exception;

  List<BusquedaCampaniaOutputDTO> buscador();

//  List<CampaniaDTO> checkCampaniaExpirada();

}
