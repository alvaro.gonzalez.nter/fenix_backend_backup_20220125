package com.haya.alaska.campania.application;

import com.google.common.collect.Lists;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDto;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.busqueda.application.BusquedaUseCaseImpl;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.campania.domain.Campania;
import com.haya.alaska.campania.infraestructure.controller.dto.*;
import com.haya.alaska.campania.infraestructure.mapper.CampaniaMapper;
import com.haya.alaska.campania.infraestructure.repository.CampaniaRepository;
import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.campania_asignada.infraestructure.repository.CampaniaAsignadaRepository;
import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.campania_asignada_pn.infraestructure.repository.CampaniaAsignadaBienPNRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDto;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCaseImpl;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class CampaniaUseCaseImpl implements CampaniaUseCase {

  @Autowired
  private CampaniaRepository campaniaRepository;

  @Autowired
  private ContratoRepository contratoRepository;

  @Autowired
  private BienPosesionNegociadaRepository bienPosesionNegociadaRepository;

  @Autowired
  private CampaniaAsignadaRepository campaniaAsignadaRepository;

  @Autowired
  private CampaniaAsignadaBienPNRepository campaniaAsignadaBienPNRepository;

  @Autowired
  private BusquedaUseCaseImpl busquedaUseCase;

  @Autowired
  private ExpedientePosesionNegociadaCaseImpl expedientePosesionNegociadaCaseImpl;

  @Autowired
  CampaniaMapper campaniaMapper;

  @Autowired
  ModelMapper modelMapper;

  @Bean
  public ModelMapper modelMapper() {
    ModelMapper modelMapper = new ModelMapper();
    return modelMapper;
  }


  @Override
  public CampaniaOutputDTO getCampania(int idCampania) {
    Campania campania = null;

    try {
      campania = campaniaRepository.findById(idCampania).filter(activo -> activo.getActivo())
        .orElseThrow(() -> new NotFoundException("campaña", idCampania));
    } catch (NotFoundException e) {
      e.getMessage();
      return null;
    }

    log.debug("Campaña obtenida con el id:%", idCampania);
    CampaniaOutputDTO campaniaOutputDTO = modelMapper.map(campania, CampaniaOutputDTO.class);

    // Mapeo de las Entidades a DTO para añadirlos a los List del DTO de CampaniaOutputDTO
    campaniaOutputDTO.setContratos(getListContratoDTO(campania.getCampaniasAsignadas()));
    campaniaOutputDTO.setBienPosesionNegociada(getListBienPosesionesNegociadosDTO(campania.getCampaniasAsignadasPN()));

    return campaniaOutputDTO;
  }

  @Override
  public List<CampaniaOutputDTO> getAllCampanias() {
    List<CampaniaOutputDTO> listCampaniasDTO = new ArrayList<CampaniaOutputDTO>();
    CampaniaOutputDTO campaniaOutputDTO = new CampaniaOutputDTO();

    List<Campania> listCampania = campaniaRepository.findAll().stream()
      .filter(activa -> activa.getActivo())
      .collect(Collectors.toList());

    for (Campania campania : listCampania) {
      campaniaOutputDTO = modelMapper.map(campania, CampaniaOutputDTO.class);

      //Mapeo de las Entidades a DTO para añadirlos a los List del DTO de CampaniaOutputDTO
      campaniaOutputDTO.setContratos(getListContratoDTO(campania.getCampaniasAsignadas()));
      campaniaOutputDTO.setBienPosesionNegociada(getListBienPosesionesNegociadosDTO(campania.getCampaniasAsignadasPN()));
      if(campania.getUsuario() != null) {
        campaniaOutputDTO.setIdUsuario(campania.getUsuario().getId() != null ? campania.getUsuario().getId() : null);
        campaniaOutputDTO.setUsuario(campania.getUsuario().getNombre() != null ? campania.getUsuario().getNombre() : null);
      }

      listCampaniasDTO.add(campaniaOutputDTO);
    }

    return listCampaniasDTO;
  }

  @Override
  public CampaniaDTO crearNuevaCampania(Usuario usuario, CampaniaInputDTO campaniaInputDTO) throws Exception {
    log.debug("Campaña a crear %", campaniaInputDTO);

    Boolean checkNameCampania = campaniaRepository.findAll().stream()
    .anyMatch(nombre -> nombre.getNombre().equals(campaniaInputDTO.getNombre()));

    if(checkNameCampania) {
	throw new Exception(String.format("Ya existe una campaña con ese nombre: %s", campaniaInputDTO.getNombre()));
    }

    Campania campania = modelMapper.map(campaniaInputDTO, Campania.class);
    campania.setUsuario(usuario);
    campaniaRepository.save(campania);

    CampaniaDTO campaniaDTO = modelMapper.map(campania, CampaniaDTO.class);
    log.debug("Campaña creada");

    return campaniaDTO;
  }

  @Override
  public CampaniaDTO editarCampania(Usuario usuario, int idCampania, CampaniaInputDTO campaniaInputDTO) {

    Campania campania = null;

    try {
      campania = campaniaRepository.findById(idCampania).orElseThrow(() -> new NotFoundException(null));
    } catch (NotFoundException e) {
      e.getMessage();
    }

    BeanUtils.copyProperties(campaniaInputDTO, campania, "id", "activo", "usuario", "contratos", "bienPosesionNegociada");
    campania.setUsuario(usuario);
    campaniaRepository.save(campania);

    CampaniaDTO campaniaDTO = modelMapper.map(campania, CampaniaDTO.class);
    log.debug("Campaña modificada");

    return campaniaDTO;
  }

  @Override
  public Void borrarCampania(Usuario usuario, int idCampania) {

    Campania campania = null;

    try {
      campania = campaniaRepository.findById(idCampania).orElseThrow(() -> new NotFoundException(null));
    } catch (NotFoundException e) {
      e.getMessage();
    }

    // Borrado logico de la campaña de la tabla de Campania
    campania.setUsuario(usuario);
    campania.setActivo(false);
    campaniaRepository.save(campania);

    // Borrado fisico de las tablas de relacion de Campania con Contrato y Bien Posesion Negociada
    List<CampaniaAsignada> listAsignada = campaniaAsignadaRepository.findAllByCampaniaId(idCampania);

    if (!listAsignada.isEmpty()) {
      for (CampaniaAsignada campaniaAsignada : listAsignada) {
        campaniaAsignadaRepository.delete(campaniaAsignada);
      }
    }

    List<CampaniaAsignadaBienPN> listAsignadaPN = campaniaAsignadaBienPNRepository.findAllByCampaniaId(idCampania);

    if (!listAsignadaPN.isEmpty()) {
      for (CampaniaAsignadaBienPN campaniaAsignadaPN : listAsignadaPN) {
        campaniaAsignadaBienPNRepository.delete(campaniaAsignadaPN);
      }
    }

    return null;
  }

  @Override
  public CampaniaDTO asignarCampania(Usuario usuario, BusquedaDto busquedaDto, int idCampania, List<Integer> idsContratos, List<Integer> idsBienesPN,
                                     boolean todos, boolean todosPN) throws Exception {
    List<Integer> listadoIdsContrato = new ArrayList<>();
    List<Integer> listadoIdsPN = new ArrayList<>();

    Campania campania = campaniaRepository.findById(idCampania).orElseThrow(
      () -> new NotFoundException("campaña", idCampania));

    // Compprobamos si se ha seleccionado el check de Seleccionar todos los Contratos
    // Para activar la busqueda de los contratos de todos los ids mediante el filtro de busqueda.
    // Si no, se usa el listado de Ids seleccionados por el usuario
    if (todos == true) {

      if (busquedaDto == null) {
        throw new Exception("Hace falta enviar el objeto BusquedaDTO si se hace una petición para cambiar todas las estrategias");
      }
      // Obtenemos todos los ids de los Contratos
      List<Expediente> listadoIdsExpediente = busquedaUseCase.getDataExpedientesFilter(busquedaDto, usuario, null, null).stream().collect(Collectors.toList());

      for (Expediente expediente :listadoIdsExpediente ) {
        for (Contrato contrato : expediente.getContratos()) {
          listadoIdsContrato.add(contrato.getId());
        }
      }

    } else {
      listadoIdsContrato = idsContratos;
    }

    // Compprobamos si se ha seleccionado el check de Seleccionar todos los Bienes de Posesion Negociada
    // Para activar la busqueda de los contratos de todos los ids mediante el filtro de busqueda.
    // Si no, se usa el listado de Ids seleccionados por el usuario
    if (todosPN == true) {

      if (busquedaDto == null) {
        throw new Exception("Hace falta enviar el objeto BusquedaDTO si se hace una petición para cambiar todas las estrategias");
      }

      // Obtenemos todos los ids de los Contratos
      List<ExpedientePosesionNegociada> listadoIdsExpediente = expedientePosesionNegociadaCaseImpl.filtrarExpedientesPosesionNegociadaBusqueda(busquedaDto, usuario, null, null, null, null, null, null, null, null, null, null, null,null,null);


        for (ExpedientePosesionNegociada expedientePosesionNegociada : listadoIdsExpediente) {
          for (BienPosesionNegociada bienPosesionNegociada:expedientePosesionNegociada.getBienesPosesionNegociada()){
            listadoIdsPN.add(bienPosesionNegociada.getId());
          }
        }
    } else {
      listadoIdsPN = idsBienesPN;
    }


    // Comprobamos si se ha hecho seleccion de asignacion para la campaña de algun Contrato
    if (listadoIdsContrato != null) {
      List<Contrato> todosContratos = contratoRepository.findAll();
      List<CampaniaAsignada> listCheckCampaniaAsignadas = campaniaAsignadaRepository.findAll();
      for (Integer idContrato : listadoIdsContrato) {
        for (Contrato contrato : todosContratos) {
          if (contrato.getId().equals(idContrato)) {
            // Comprobamos si ya existe relación de asignacion entre el contrato y la campaña, para no volver a guardarla
            Boolean exist = listCheckCampaniaAsignadas.stream()
              .filter(campaniaAsignada -> campaniaAsignada.getCampania().getId() == idCampania)
              .anyMatch(campaniaContrato -> campaniaContrato.getContrato().getId() == contrato.getId());

            if (!exist) {
              CampaniaAsignada campaniaAsignada = new CampaniaAsignada();
              campaniaAsignada.setCampania(campania);
              campaniaAsignada.setContrato(contrato);
              campaniaAsignada.setFechaAsignacion(new Date());
              campaniaAsignadaRepository.save(campaniaAsignada);

              Set<CampaniaAsignada> campaniasAsignadasCampania = campania.getCampaniasAsignadas();
              campaniasAsignadasCampania.add(campaniaAsignada);

              campania.setCampaniasAsignadas(campaniasAsignadasCampania);

              Set<CampaniaAsignada> campaniasAsignadas = contrato.getCampaniasAsignadas();
              campaniasAsignadas.add(campaniaAsignada);

              contrato.setCampaniasAsignadas(campaniasAsignadas);
              contratoRepository.save(contrato);
              campaniaRepository.save(campania);
            }
          }
        }
      }
    }

    // Comprobamos si se ha hecho seleccion de asignacion para la campaña de algun Bien de Posesion Negociada
    if (listadoIdsPN != null) {

      List<BienPosesionNegociada> todosBienesPN = new ArrayList<>();
      todosBienesPN.addAll(bienPosesionNegociadaRepository.findAllByIdIn(listadoIdsPN));

      List<CampaniaAsignadaBienPN> listCheckCampaniaAsignada = campaniaAsignadaBienPNRepository.findAll();
      for (Integer idBienPN : listadoIdsPN) {
        for (BienPosesionNegociada bienPosesionNegociada : todosBienesPN) {
          if (bienPosesionNegociada.getId() == idBienPN.intValue()) {
            // Comprobamos si ya existe relación de asignacion entre el Bien PN y la campaña, para no volver a guardarla
            Boolean exist = listCheckCampaniaAsignada.stream()
              .filter(campaniaAsignada -> campaniaAsignada.getCampania().getId() == idCampania)
              .anyMatch(campaniaBienPN -> campaniaBienPN.getBienPosesionNegociada().getId() == bienPosesionNegociada.getId());

            if (!exist) {
              CampaniaAsignadaBienPN campaniaAsignadaPN = new CampaniaAsignadaBienPN();

              campaniaAsignadaPN.setCampania(campania);
              campaniaAsignadaPN.setBienPosesionNegociada(bienPosesionNegociada);
              campaniaAsignadaPN.setFechaAsignacion(new Date());
              campaniaAsignadaBienPNRepository.save(campaniaAsignadaPN);

              Set<CampaniaAsignadaBienPN> campaniasAsignadasCampania = campania.getCampaniasAsignadasPN();
              campaniasAsignadasCampania.add(campaniaAsignadaPN);

              campania.setCampaniasAsignadasPN(campaniasAsignadasCampania);

              Set<CampaniaAsignadaBienPN> campaniasAsignadas = bienPosesionNegociada.getCampaniasAsignadasPN();
              campaniasAsignadas.add(campaniaAsignadaPN);
              bienPosesionNegociada.setCampaniasAsignadasPN(campaniasAsignadas);
              bienPosesionNegociadaRepository.save(bienPosesionNegociada);
              campaniaRepository.save(campania);
            }
          }
        }
      }
    }


    CampaniaDTO campaniaDTO = modelMapper.map(campania, CampaniaDTO.class);

    // Mapeo de las Entidades a DTO para añadirlos a los List del DTO de CampaniaOutputDTO
    campaniaDTO.setContratos(getListContratoDTO(campania.getCampaniasAsignadas()));
    campaniaDTO.setBienPosesionNegociada(getListBienPosesionesNegociadosDTO(campania.getCampaniasAsignadasPN()));

    return campaniaDTO;
  }

  @Override
  public Void desasignarCampania(Usuario usuario, DesasignarCampaniasDTO campaniaDesasignadas, boolean todos, boolean todosPN) throws Exception {

    // Compprobamos si se ha seleccionado el check de Seleccionar todos los Contratos
    if (todos == true) {

      if (campaniaDesasignadas.getBusquedaDto() == null) {
        throw new Exception("Hace falta enviar el objeto BusquedaDTO si se hace una petición para cambiar todas las estrategias");
      }
      List<Integer>idContratosList=new ArrayList<>();
      // Obtenemos todos los ids de los Contratos
      List<Expediente> listadoIdsExpediente = busquedaUseCase.getDataExpedientesFilter(campaniaDesasignadas.getBusquedaDto(), usuario, null, null).stream().collect(Collectors.toList());

      for (Expediente expediente :listadoIdsExpediente ) {
      for (Contrato contrato:expediente.getContratos()) {
        idContratosList.add(contrato.getId());
      }
      }
   /*     .map(exp -> exp.getContratos())
        .filter(f -> f.stream().iterator().hasNext())
        .map(id -> id.stream().iterator().next().getId())
        .collect(Collectors.toList());*/

      // Obtenemos todas las Campañas Asignadas de los contratos seleccionados
      List<CampaniaAsignada> listAsignada = campaniaAsignadaRepository.findAllByContratoIdIn(idContratosList);

      // Eliminados las Campañas Asignadas de los Contratos y la relación de Contrato-Campaña
      if (listAsignada.size() > 0) {
        this.desasignacionCampaniaContrato(listAsignada);
      }

    } else {

      // Devuelve un listado de campanias que quieres desasignar
      List<CampaniasRelacionadas> listExitsContrato = campaniaDesasignadas.getCampanias().stream()
        .filter(idContrato -> idContrato.getIdsContrato() != null && idContrato.getIdsContrato().size() > 0)
        .map(campaniaDesasignada -> campaniaMapper.mapToCampaniasRelacionadas(campaniaDesasignada, true))
        .collect(Collectors.toList());

      // Si el listado de campañas que vas a desasignar es mayor a 0
      if (listExitsContrato != null && listExitsContrato.size() > 0) {
        for (int i = 0; i < listExitsContrato.size(); i++) {

          //Obtenemos el idCampaña de la campaña que queremos desasignar
          int idCampania = listExitsContrato.get(i).getIdCampania();

          // Obtenemos todos los ids de Contratos de las campañasasignadas que queremos desasignar
          List<Integer> listadoContratos = listExitsContrato.get(i).getIdsRelacion().stream()
            .collect(Collectors.toList());

          // Obtenemos todas las Campañas Asignadas de los contratos seleccionados
          if (listadoContratos.size() > 0) {
            List<CampaniaAsignada> listAsignada = campaniaAsignadaRepository.findAllByContratoIdIn(listadoContratos).stream()
    	      .filter(campania -> idCampania == campania.getCampania().getId())
              .filter(activo -> activo.getCampania().getActivo()== true)
              .collect(Collectors.toList());

            // Eliminados las Campañas Asignadas de los Contratos y la relación de la Campaña Asignada
            // Contrato-Campaña
            if (listAsignada.size() > 0) {
              this.desasignacionCampaniaContrato(listAsignada);
            }
          }
        }
      }
    }

    // Compprobamos si se ha seleccionado el check de Seleccionar todos los Bienes de Posesion Negociada
    if (todosPN == true) {

      if (campaniaDesasignadas.getBusquedaDto() == null) {
        throw new Exception(
          "Hace falta enviar el objeto BusquedaDTO si se hace una petición para cambiar todas las estrategias");
      }

      List<Integer> idPosesionNegociada = new ArrayList<>();

      // Obtenemos todos los ids de los Contratos
      List<ExpedientePosesionNegociada> listadoIdsExpediente = expedientePosesionNegociadaCaseImpl.filtrarExpedientesPosesionNegociadaBusqueda(campaniaDesasignadas.getBusquedaDto(), usuario, null, null, null, null, null, null, null, null, null, null,null,null, null);
      List<Integer>listadoIdsPN=new ArrayList<>();

      for (ExpedientePosesionNegociada expedientePosesionNegociada : listadoIdsExpediente) {
        for (BienPosesionNegociada bienPosesionNegociada:expedientePosesionNegociada.getBienesPosesionNegociada()){
          listadoIdsPN.add(bienPosesionNegociada.getId());
        }
      }

      // Se necesitaria pasar todos los ids de las campañas
      // Obtenemos todas las Campañas Asignadas de los Bienes de Posesion Negociada seleccionados
      List<CampaniaAsignadaBienPN> listAsignadaPN = campaniaAsignadaBienPNRepository.findAllByBienPosesionNegociadaIdIn(listadoIdsPN);

      // Eliminados las Campañas Asignadas de los Bienes de Posesion Negociada y la relación BienPN-Campaña
      if (listAsignadaPN.size() > 0) {
        this.desasignacionCampaniaBienPN(listAsignadaPN);
      }

    } else {

      // Obtenemos solo los datos relaciones de los Bienes de PN
      List<CampaniasRelacionadas> listExitsBienPN = campaniaDesasignadas.getCampanias().stream()
	.filter(idBien -> idBien.getIdsBienPN() != null && idBien.getIdsBienPN().size() > 0)
        .map(campaniaDesasignada -> campaniaMapper.mapToCampaniasRelacionadas(campaniaDesasignada, false))
        .collect(Collectors.toList());

      // Comprobamos que el listado de ids de BienPN tenga datos
      if (listExitsBienPN != null && listExitsBienPN.size() > 0) {
        for (int i = 0; i < listExitsBienPN.size(); i++) {

          int idCampania = listExitsBienPN.get(i).getIdCampania();

          List<Integer> listadoBienPN = listExitsBienPN.get(i).getIdsRelacion().stream()
            .collect(Collectors.toList());

          // Obtenemos todas las Campañas Asignadas de los Bienes de Posesion Negociada seleccionados
          if (listadoBienPN.size() > 0) {
            List<CampaniaAsignadaBienPN> listAsignadaPN = campaniaAsignadaBienPNRepository.findAllByBienPosesionNegociadaIdIn(listadoBienPN)
              .stream()
              .filter(campania -> idCampania == campania.getCampania().getId())
              .filter(activo -> activo.getCampania().getActivo().booleanValue() == true)
              .collect(Collectors.toList());

            // Se eliminan las Campañas Asignadas de los Bienes PN y la relación de BienPN-Campaña
            if (listAsignadaPN.size() > 0) {
              this.desasignacionCampaniaBienPN(listAsignadaPN);
            }
          }
        }
      }
    }

    return null;
  }

  @Override
  public List<BusquedaCampaniaOutputDTO> buscador() {
    List<Map> busqueda = contratoRepository.findContratosRelCampania2();
    return null;
  }

  /*@Override
  public List<CampaniaDTO> checkCampaniaExpirada() {
    return null;
  }*/

  /**
   * Borrado de la Campaña Asignada a Contrato y de la Tabla de Relación Contrato-Campaña
   *
   * @param listAsignada
   */
  public void desasignacionCampaniaContrato(List<CampaniaAsignada> listAsignada) {
    for (CampaniaAsignada campaniaAsignada : listAsignada) {
      Contrato contrato = contratoRepository.findById(campaniaAsignada.getContrato().getId()).get();
      contrato.getCampaniasAsignadas().remove(campaniaAsignada);
      contratoRepository.save(contrato);
      campaniaAsignadaRepository.delete(campaniaAsignada);
    }
  }

  /**
   * Borrado de la Campaña Asignada a Bien de PN y de la Tabla de Relación BienPN-Campaña
   *
   * @param listAsignadaPN
   */
  public void desasignacionCampaniaBienPN(List<CampaniaAsignadaBienPN> listAsignadaPN) {
    for (CampaniaAsignadaBienPN campaniaAsignadaPN : listAsignadaPN) {
      BienPosesionNegociada bienPN = bienPosesionNegociadaRepository.findById(campaniaAsignadaPN.getBienPosesionNegociada().getId()).get();
      bienPN.getCampaniasAsignadasPN().remove(campaniaAsignadaPN);
      bienPosesionNegociadaRepository.save(bienPN);
      campaniaAsignadaBienPNRepository.delete(campaniaAsignadaPN);
    }
  }

  /**
   * Mapeo de un listado de ContratoDTO desde un Listado de CampaniaAsignada
   *
   * @param campaniasAsignadas
   * @return List<ContratoDto>
   */
  private List<ContratoDto> getListContratoDTO(Set<CampaniaAsignada> campaniasAsignadas) {
    return campaniaMapper.convertAsignadasToContratoDTO(Lists.newArrayList(campaniasAsignadas));
  }

  /**
   * Mapeo de un listado de ContratoDTO desde un Listado de CampaniaAsignada
   *
   * @param campaniasAsignadasPN
   * @return List<BienPosesionNegociadaDto>
   */
  private List<BienPosesionNegociadaDto> getListBienPosesionesNegociadosDTO(Set<CampaniaAsignadaBienPN> campaniasAsignadasPN) {
    return campaniaMapper.convertAsignadasToBienPosesionNegociadaDTO(Lists.newArrayList(campaniasAsignadasPN));
  }

}
