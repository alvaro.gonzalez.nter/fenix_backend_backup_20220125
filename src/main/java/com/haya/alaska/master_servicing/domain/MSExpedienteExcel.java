package com.haya.alaska.master_servicing.domain;

import com.haya.alaska.agencia_master_servicing.domain.AgenciaMasterServicing;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;

public class MSExpedienteExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection Id");
      cabeceras.add("Status");
      cabeceras.add("Customer");
      cabeceras.add("Portfolio");
      cabeceras.add("Manager");
      cabeceras.add("First holder");
      cabeceras.add("Strategy");
      cabeceras.add("Management Balance");
      cabeceras.add("Agency");

    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Estado");
      cabeceras.add("Cliente");
      cabeceras.add("Cartera");
      cabeceras.add("Gestor");
      cabeceras.add("Primer Titular");
      cabeceras.add("Estrategia");
      cabeceras.add("Saldo en Gestión");
      cabeceras.add("Agencia");
    }
  }

  public MSExpedienteExcel(Expediente ex){

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Loan Id", "'" + ex.getIdConcatenado());
      this.add("Management Balance", ex.getSaldoGestion());
      this.add("Status", ex.getTipoEstado() != null ? ex.getTipoEstado().getValorIngles() : null);
      Cartera ca = ex.getCartera();
      if (ca != null) {
        this.add("Portfolio", ca.getNombre());
        ClienteCartera cc =
          ca.getClientes().isEmpty() ? null : new ArrayList<>(ca.getClientes()).get(0);
        if (cc != null) {
          Cliente cl = cc.getCliente();
          if (cl != null) this.add("Customer", cl.getNombre());
        }
      }
      this.add("Manager", ex.getGestor() != null ? ex.getGestor().getNombre() : null);
      Contrato representante = ex.getContratoRepresentante();
      if (representante != null) {
        EstrategiaAsignada ea = representante.getEstrategia();
        if (ea != null)
          this.add("Strategy", ea.getEstrategia() != null ? ea.getEstrategia().getValorIngles() : null);
        Interviniente in = representante.getPrimerInterviniente();
        if (in != null) {
          String nombre = "";
          if (in.getPersonaJuridica()) {
            nombre += in.getRazonSocial() != null ? in.getRazonSocial() : "";
          } else {
            nombre += in.getNombre() != null ? in.getNombre() + " " : "";
            nombre += in.getApellidos() != null ? in.getApellidos() : "";
          }
          this.add("First holder", nombre);
        }
      }
      AgenciaMasterServicing agencia = ex.getAgencia();
      if (agencia != null) this.add("Agency", agencia.getValorIngles());

    } else {

      this.add("ID expediente", "'" + ex.getIdConcatenado());
      this.add("Saldo en Gestión", ex.getSaldoGestion());
      this.add("Estado", ex.getTipoEstado() != null ? ex.getTipoEstado().getValor() : null);
      Cartera ca = ex.getCartera();
      if (ca != null) {
        this.add("Cartera", ca.getNombre());
        ClienteCartera cc =
            ca.getClientes().isEmpty() ? null : new ArrayList<>(ca.getClientes()).get(0);
        if (cc != null) {
          Cliente cl = cc.getCliente();
          if (cl != null) this.add("Cliente", cl.getNombre());
        }
      }
      this.add("Gestor", ex.getGestor() != null ? ex.getGestor().getNombre() : null);
      Contrato representante = ex.getContratoRepresentante();
      if (representante != null) {
        EstrategiaAsignada ea = representante.getEstrategia();
        if (ea != null)
          this.add("Estrategia", ea.getEstrategia() != null ? ea.getEstrategia().getValor() : null);
        Interviniente in = representante.getPrimerInterviniente();
        if (in != null) {
          String nombre = "";
          if (in.getPersonaJuridica()) {
            nombre += in.getRazonSocial() != null ? in.getRazonSocial() : "";
          } else {
            nombre += in.getNombre() != null ? in.getNombre() + " " : "";
            nombre += in.getApellidos() != null ? in.getApellidos() : "";
          }
          this.add("Primer Titular", nombre);
        }
      }
      AgenciaMasterServicing agencia = ex.getAgencia();
      if (agencia != null) this.add("Agencia", agencia.getValor());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
