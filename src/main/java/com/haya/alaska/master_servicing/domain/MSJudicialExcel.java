package com.haya.alaska.master_servicing.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MSJudicialExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Origin Loan");
      cabeceras.add("Loan ID");
      cabeceras.add("Name");
      cabeceras.add("Procedure ID");
      cabeceras.add("Procedure Type");
      cabeceras.add("Num Orders");
      cabeceras.add("Place");
      cabeceras.add("Court");
      cabeceras.add("Province");
      cabeceras.add("Amount demanded");
      cabeceras.add("Hito");
      cabeceras.add("Status procedure");
      cabeceras.add("Reason");

    } else {
      cabeceras.add("ID Expediente");
      cabeceras.add("Contrato origen");
      cabeceras.add("ID Contrato");
      cabeceras.add("Nombre");
      cabeceras.add("ID Procedimiento");
      cabeceras.add("Tipo Procedimiento");
      cabeceras.add("Num Autos");
      cabeceras.add("Plaza");
      cabeceras.add("Juzgado");
      cabeceras.add("Provincia");
      cabeceras.add("Importe demanda");
      cabeceras.add("Hito");
      cabeceras.add("Estado procedimiento");
      cabeceras.add("Motivo");
    }
  }

  public MSJudicialExcel(Procedimiento pr, Contrato co){
    Expediente ex = co.getExpediente();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", ex.getIdConcatenado());
      this.add("Origin Loan", co.getIdCarga());
      this.add("Loan ID", co.getId());
      this.add("Name", null);
      this.add("Procedure ID", pr.getId());
      this.add("Procedure Type", pr.getTipo() != null ? pr.getTipo().getValorIngles() : null);
      this.add("Num Orders", pr.getNumeroAutos());
      this.add("Place", pr.getPlaza());
      this.add("Court", pr.getJuzgado());
      this.add("Province", pr.getProvincia() != null ? pr.getProvincia().getValorIngles() : null);
      this.add("Amount demanded", pr.getImporteDemanda());
      this.add("Amount", pr.getHito());
      this.add("Status procedure", pr.getEstado() != null ? pr.getEstado().getValorIngles() : null);
      this.add("Reason", pr.getMotivo());

    } else {

      this.add("ID Expediente", ex.getIdConcatenado());
      this.add("Contrato origen", co.getIdCarga());
      this.add("ID Contrato", co.getId());
      this.add("Nombre", null);
      this.add("ID Procedimiento", pr.getId());
      this.add("Tipo Procedimiento", pr.getTipo() != null ? pr.getTipo().getValorIngles() : null);
      this.add("Num Autos", pr.getNumeroAutos());
      this.add("Plaza", pr.getPlaza());
      this.add("Juzgado", pr.getJuzgado());
      this.add("Provincia", pr.getProvincia() != null ? pr.getProvincia().getValorIngles() : null);
      this.add("Importe demanda", pr.getImporteDemanda());
      this.add("Importe", pr.getHito());
      this.add("Estado procedimiento", pr.getEstado() != null ? pr.getEstado().getValorIngles() : null);
      this.add("Motivo", pr.getMotivo());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
