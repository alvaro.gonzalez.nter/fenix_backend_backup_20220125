package com.haya.alaska.master_servicing.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MSSolvenciaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Solvency ID");
      cabeceras.add("Origin Loan");
      cabeceras.add("Loan ID");
      cabeceras.add("Solvency type");
      cabeceras.add("Status");
      cabeceras.add("Registered Finca");
      cabeceras.add("Location");
      cabeceras.add("Province");
      cabeceras.add("Value");
      cabeceras.add("Liquidity");
      cabeceras.add("Description");
      cabeceras.add("Responsibility");
      cabeceras.add("Customer ID");
      cabeceras.add("Negotiated possession");


    } else {
      cabeceras.add("ID Solvencia");
      cabeceras.add("Contrato origen");
      cabeceras.add("ID Contrato");
      cabeceras.add("Tipo de Solvencia");
      cabeceras.add("Estado");
      cabeceras.add("Finca Registral");
      cabeceras.add("Localidad");
      cabeceras.add("Provincia");
      cabeceras.add("Valor");
      cabeceras.add("Liquidez");
      cabeceras.add("Descripción");
      cabeceras.add("Responsabilidad");
      cabeceras.add("ID Cliente");
      cabeceras.add("Posesión Negociada");
    }
  }

  public MSSolvenciaExcel(ContratoBien cb){
    Bien bi = cb.getBien();
    Contrato co = cb.getContrato();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Solvency ID", bi.getId());
      this.add("Origin Loan", co.getIdCarga());
      this.add("ILoan ID", co.getId());
      this.add("Solvency type", bi.getTipoBien() != null ? bi.getTipoBien().getValorIngles() : null);
      this.add("Status", Boolean.TRUE.equals(cb.getEstado()) ? "ACTIVE" : "RELEASED");
      this.add("Registered Finca", bi.getFinca());
      this.add("Location", bi.getLocalidad() != null ? bi.getLocalidad().getValorIngles() : null);
      this.add("Province", bi.getProvincia() != null ? bi.getProvincia().getValorIngles() : null);
      this.add("Value", bi.getSaldoValoracion());
      this.add("Liquidity", null);
      this.add("Description", bi.getDescripcion());
      this.add("Responsibility", cb.getResponsabilidadHipotecaria());
      this.add("Customer ID", null);
      this.add("Negotiated possession", null);

    } else {

      this.add("ID Solvencia", bi.getId());
      this.add("Contrato origen", co.getIdCarga());
      this.add("ID Contrato", co.getId());
      this.add("Tipo de Solvencia", bi.getTipoBien() != null ? bi.getTipoBien().getValor() : null);
      this.add("Estado", Boolean.TRUE.equals(cb.getEstado()) ? "ACTIVO" : "LIBERADO");
      this.add("Finca Registral", bi.getFinca());
      this.add("Localidad", bi.getLocalidad() != null ? bi.getLocalidad().getValor() : null);
      this.add("Provincia", bi.getProvincia() != null ? bi.getProvincia().getValor() : null);
      this.add("Valor", bi.getSaldoValoracion());
      this.add("Liquidez", null);
      this.add("Descripción", bi.getDescripcion());
      this.add("Responsabilidad", cb.getResponsabilidadHipotecaria());
      this.add("ID Cliente", null);
      this.add("Posesión Negociada", null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
