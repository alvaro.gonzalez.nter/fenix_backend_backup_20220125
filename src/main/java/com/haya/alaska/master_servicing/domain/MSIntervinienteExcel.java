package com.haya.alaska.master_servicing.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MSIntervinienteExcel  extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  /*Número de documento	Teléfono principal	Contactado
   */
  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Origin Loan");
      cabeceras.add("Loan ID");
      cabeceras.add("Name");
      cabeceras.add("Intervention type");
      cabeceras.add("Order");
      cabeceras.add("Status");
      cabeceras.add("Date of birth");
      cabeceras.add("Document number");
      cabeceras.add("Main telephone");
      cabeceras.add("Contacted");

    } else {

      cabeceras.add("Contrato origen");
      cabeceras.add("ID Contrato");
      cabeceras.add("Nombre");
      cabeceras.add("Tipo de Intervención");
      cabeceras.add("Orden");
      cabeceras.add("Estado");
      cabeceras.add("Fecha nacimiento");
      cabeceras.add("Número de documento");
      cabeceras.add("Teléfono principal");
      cabeceras.add("Contactado");
    }
  }

  public MSIntervinienteExcel(ContratoInterviniente ci){
    Contrato co = ci.getContrato();
    Interviniente in = ci.getInterviniente();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Origin Loan", co.getIdCarga());
      this.add("Loan ID", co.getId());
      String nombre = "";
      if (in.getPersonaJuridica()) {
        nombre += in.getRazonSocial() != null ? in.getRazonSocial() : "";
      } else {
        nombre += in.getNombre() != null ? in.getNombre() + " " : "";
        nombre += in.getApellidos() != null ? in.getApellidos() : "";
      }
      this.add("Name", nombre);
      this.add(
        "Intervention type",
        ci.getTipoIntervencion() != null ? ci.getTipoIntervencion().getValorIngles() : null);
      this.add("Order", ci.getOrdenIntervencion());
      this.add("Status", in.getEstadoCivil() != null ? in.getEstadoCivil().getValorIngles() : null);
      this.add("Date of birth", in.getFechaNacimiento());
      this.add("Document number", in.getNumeroDocumento());
      this.add("Main telephone", in.getTelefonoPrincipal());
      this.add(
        "Contacted", Boolean.TRUE.equals(in.getContactado()) ? "CONTACTED" : "NOT CONTACTED");

    } else {

      this.add("Contrato origen", co.getIdCarga());
      this.add("ID Contrato", co.getId());
      String nombre = "";
      if (in.getPersonaJuridica()) {
        nombre += in.getRazonSocial() != null ? in.getRazonSocial() : "";
      } else {
        nombre += in.getNombre() != null ? in.getNombre() + " " : "";
        nombre += in.getApellidos() != null ? in.getApellidos() : "";
      }
      this.add("Nombre", nombre);
      this.add(
          "Tipo de Intervención",
          ci.getTipoIntervencion() != null ? ci.getTipoIntervencion().getValor() : null);
      this.add("Orden", ci.getOrdenIntervencion());
      this.add("Estado", in.getEstadoCivil() != null ? in.getEstadoCivil().getValor() : null);
      this.add("Fecha nacimiento", in.getFechaNacimiento());
      this.add("Número de documento", in.getNumeroDocumento());
      this.add("Teléfono principal", in.getTelefonoPrincipal());
      this.add(
          "Contactado", Boolean.TRUE.equals(in.getContactado()) ? "CONTACTADO" : "NO CONTACTADO");
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
