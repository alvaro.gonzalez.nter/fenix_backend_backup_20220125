package com.haya.alaska.master_servicing.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MSContratoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Main Loan");
      cabeceras.add("Origin Loan");
      cabeceras.add("Loan Id");
      cabeceras.add("Connection Id");
      cabeceras.add("Product");
      cabeceras.add("First Holder");
      cabeceras.add("Management Balance");
      cabeceras.add("Borrowers");
      cabeceras.add("Warranties");
      cabeceras.add("Judicialized");
      cabeceras.add("Status");
      cabeceras.add("Situation");
      cabeceras.add("Strategy");

    } else {

      cabeceras.add("Contrato Principal");
      cabeceras.add("Contrato Origen");
      cabeceras.add("ID Contrato");
      cabeceras.add("ID Expediente");
      cabeceras.add("Producto");
      cabeceras.add("Primer Titular");
      cabeceras.add("Saldo en gestión");
      cabeceras.add("Intervinientes");
      cabeceras.add("Garantías");
      cabeceras.add("Judicializado");
      cabeceras.add("Estado");
      cabeceras.add("Situación");
      cabeceras.add("Estrategia");
    }
  }

  public MSContratoExcel(Contrato co){

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Main Loan", co.isRepresentante() ? "YES" : "NO");
      this.add("Origin Loan", co.getIdCarga());
      this.add("Loan Id", co.getId());
      this.add(
        "Connection Id",
        co.getExpediente() != null ? co.getExpediente().getIdConcatenado() : null);
      this.add("Product", co.getProducto() != null ? co.getProducto().getValorIngles() : null);
      Interviniente in = co.getPrimerInterviniente();
      if (in != null) {
        String nombre = "";
        if (in.getPersonaJuridica()) {
          nombre += in.getRazonSocial() != null ? in.getRazonSocial() : "";
        } else {
          nombre += in.getNombre() != null ? in.getNombre() + " " : "";
          nombre += in.getApellidos() != null ? in.getApellidos() : "";
        }
        this.add("First Holder", nombre);
      }
      this.add("Management Balance", co.getSaldoGestion());
      this.add("Borrowers", co.getIntervinientes().size());
      this.add("Warranties", co.getBienes().size());
      this.add("Judicialized", co.getProcedimientos().size());
      this.add("Status", co.getEstadoContrato() != null ? co.getEstadoContrato().getValorIngles() : null);
      EstrategiaAsignada ea = co.getEstrategia();
      if (ea != null)
        this.add("Strategy", ea.getEstrategia() != null ? ea.getEstrategia().getValorIngles() : null);

    } else {

      this.add("Contrato Principal", co.isRepresentante() ? "Si" : "NO");
      this.add("Contrato Origen", co.getIdCarga());
      this.add("ID Contrato", co.getId());
      this.add(
          "ID Expediente",
          co.getExpediente() != null ? co.getExpediente().getIdConcatenado() : null);
      this.add("Producto", co.getProducto() != null ? co.getProducto().getValor() : null);
      Interviniente in = co.getPrimerInterviniente();
      if (in != null) {
        String nombre = "";
        if (in.getPersonaJuridica()) {
          nombre += in.getRazonSocial() != null ? in.getRazonSocial() : "";
        } else {
          nombre += in.getNombre() != null ? in.getNombre() + " " : "";
          nombre += in.getApellidos() != null ? in.getApellidos() : "";
        }
        this.add("Primer Titular", nombre);
      }
      this.add("Saldo en gestión", co.getSaldoGestion());
      this.add("Intervinientes", co.getIntervinientes().size());
      this.add("Garantías", co.getBienes().size());
      this.add("Judicializado", co.getProcedimientos().size());
      this.add("Estado", co.getEstadoContrato() != null ? co.getEstadoContrato().getValor() : null);
      EstrategiaAsignada ea = co.getEstrategia();
      if (ea != null)
        this.add("Estrategia", ea.getEstrategia() != null ? ea.getEstrategia().getValor() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
