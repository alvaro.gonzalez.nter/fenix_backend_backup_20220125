package com.haya.alaska.master_servicing.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MSCatalogosExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Level (Required)");
      cabeceras.add(" ");
      cabeceras.add("Class (Required)");
      cabeceras.add("  ");
      cabeceras.add("Importance (Required)");

    } else {

      cabeceras.add("Nivel (Obligatorio)");
      cabeceras.add(" ");
      cabeceras.add("Clase (Obligatorio)");
      cabeceras.add("  ");
      cabeceras.add("Importancia (Obligatorio)");
    }
  }

  public MSCatalogosExcel(String nivel, String clase, String importancia){

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Level (Required)", nivel);
      this.add("Class (Required)", clase);
      this.add("Importance (Required)", importancia);

    } else {

      this.add("Nivel (Obligatorio)", nivel);
      this.add("Clase (Obligatorio)", clase);
      this.add("Importancia (Obligatorio)", importancia);
    }
  }

  public MSCatalogosExcel(SubtipoEvento se, String resultado){

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      if (se != null) {
        this.add("Level (Required)", se.getTipoEvento().getValorIngles());
        this.add(" ", se.getValor());
      }
      this.add("Importance (Required)", resultado);

    } else {
      if (se != null) {
        this.add("Nivel (Obligatorio)", se.getTipoEvento().getValor());
        this.add(" ", se.getValor());
      }
      this.add("Importancia (Obligatorio)", resultado);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }

  public static Collection<String> getSubCabecera(){
    LinkedHashSet<String> sobre = new LinkedHashSet<>();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      sobre.add("Event type (Required)");
      sobre.add("Event subtype (Required)");
      sobre.add(" ");
      sobre.add("  ");
      sobre.add("Event result");
      return sobre;

    } else {

      sobre.add("Tipo Evento (Obligatorio)");
      sobre.add("Subtipo Evento (Obligatorio)");
      sobre.add(" ");
      sobre.add("  ");
      sobre.add("Resultado Evento");
      return sobre;
    }
  }
}
