package com.haya.alaska.master_servicing.domain;

import com.haya.alaska.agencia_master_servicing.domain.AgenciaMasterServicing;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MasterServicingExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  //private Evento alerta

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      cabeceras.add("Connection Id");
      cabeceras.add("Loans");
      cabeceras.add("Borrowers");
      cabeceras.add("Warranties");
      cabeceras.add("Management balance");
      cabeceras.add("Type of product");
      cabeceras.add("Age debt");
      cabeceras.add("Accounting status");
      cabeceras.add("Remaining");
      cabeceras.add("Agency");

    } else {
      cabeceras.add("Id expediente");
      cabeceras.add("Contratos");
      cabeceras.add("Intervinientes");
      cabeceras.add("Garantías");
      cabeceras.add("Saldo en Gestión");
      cabeceras.add("Tipología de producto");
      cabeceras.add("Antigüedad de la Deuda");
      cabeceras.add("Situación Contable");
      cabeceras.add("Remanente");
      cabeceras.add("Agencia");
    }
  }

  public MasterServicingExcel(Expediente ex){
    Integer c = 0;
    Integer i = 0;
    Integer g = 0;
    c = ex.getContratos().size();
    for (Contrato con : ex.getContratos()){
      i += con.getIntervinientes().size();
      g += con.getBienes().size();
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection Id", "'" + ex.getIdConcatenado());
      this.add("Loans", c);
      this.add("Borrowers", i);
      this.add("Warranties", c);
      this.add("Management balance", ex.getSaldoGestion());
      Contrato representante = ex.getContratoRepresentante();
      if (representante != null) {
        Producto producto = representante.getProducto();
        if (producto != null) this.add("Type of product", producto.getValorIngles());
        this.add("Age debt", representante.getFechaPrimerImpago());
      }

      this.add("Accounting status", null);
      this.add("Remaining", null);
      AgenciaMasterServicing agencia = ex.getAgencia();
      if (agencia != null) this.add("Agency", agencia.getValorIngles());

    } else {

      this.add("Id expediente", "'" + ex.getIdConcatenado());
      this.add("Contratos", c);
      this.add("Intervinientes", i);
      this.add("Garantías", c);
      this.add("Saldo en Gestión", ex.getSaldoGestion());
      Contrato representante = ex.getContratoRepresentante();
      if (representante != null) {
        Producto producto = representante.getProducto();
        if (producto != null) this.add("Tipología de producto", producto.getValor());
        this.add("Antigüedad de la Deuda", representante.getFechaPrimerImpago());
      }

      this.add("Situación Contable", null);
      this.add("Remanente", null);
      AgenciaMasterServicing agencia = ex.getAgencia();
      if (agencia != null) this.add("Agencia", agencia.getValor());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
