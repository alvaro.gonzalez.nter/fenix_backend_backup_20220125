package com.haya.alaska.master_servicing.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MSMovimientosExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      cabeceras.add("Origin Loan");
      cabeceras.add("Loan ID");
      cabeceras.add("Accounting date");
      cabeceras.add("Value date");
      cabeceras.add("Movement type");
      cabeceras.add("Description");
      cabeceras.add("Import");
      cabeceras.add("Principal");
      cabeceras.add("Ordinary interests");
      cabeceras.add("Interest on late payment");
      cabeceras.add("Commissions");
      cabeceras.add("Expenses");
      cabeceras.add("Affects");
      cabeceras.add("Status");
    }

    else{

      cabeceras.add("Contrato origen");
      cabeceras.add("ID Contrato");
      cabeceras.add("Fecha contable");
      cabeceras.add("Fecha valor");
      cabeceras.add("Tipo Movimiento");
      cabeceras.add("Descripción");
      cabeceras.add("Importe");
      cabeceras.add("Principal");
      cabeceras.add("Intereses Ordinarios");
      cabeceras.add("Intereses de Demora");
      cabeceras.add("Comisiones");
      cabeceras.add("Gastos");
      cabeceras.add("Afecta");
      cabeceras.add("Estado");

    }
  }

  public MSMovimientosExcel(Movimiento mo, Contrato co){

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Origin Loan", co.getIdCarga());
      this.add("Loan ID", co.getId());
      this.add("Accounting date", mo.getFechaContable());
      this.add("Value date", mo.getFechaValor());
      this.add("Movement type", mo.getTipo() != null ? mo.getTipo().getValorIngles() : null);
      this.add("Description", mo.getDescripcion() != null ? mo.getDescripcion().getValorIngles() : null);
      this.add("Import", mo.getImporte());
      this.add("Principal", mo.getPrincipalCapital());
      this.add("Ordinary interests", mo.getInteresesOrdinarios());
      this.add("Interest on late payment", mo.getInteresesDemora());
      this.add("Commissions", mo.getComisiones());
      this.add("Expenses", mo.getGastos());
      this.add("Affects", Boolean.TRUE.equals(mo.getAfecta()) ? "YES" : "No");
      this.add("Status", null);

    } else {

      this.add("Contrato origen", co.getIdCarga());
      this.add("ID Contrato", co.getId());
      this.add("Fecha contable", mo.getFechaContable());
      this.add("Fecha valor", mo.getFechaValor());
      this.add("Tipo Movimiento", mo.getTipo() != null ? mo.getTipo().getValorIngles() : null);
      this.add("Descripción", mo.getDescripcion() != null ? mo.getDescripcion().getValorIngles() : null);
      this.add("Importe", mo.getImporte());
      this.add("Principal", mo.getPrincipalCapital());
      this.add("Intereses Ordinarios", mo.getInteresesOrdinarios());
      this.add("Intereses de Demora", mo.getInteresesDemora());
      this.add("Comisiones", mo.getComisiones());
      this.add("Gastos", mo.getGastos());
      this.add("Afecta", Boolean.TRUE.equals(mo.getAfecta()) ? "SI" : "No");
      this.add("Estado", null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
