package com.haya.alaska.master_servicing.domain;

import com.haya.alaska.agencia_master_servicing.domain.AgenciaMasterServicing;
import com.haya.alaska.expediente.domain.Expediente;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class MasterServicingSeparado implements Serializable {
  List<Expediente> exN;
  Map<AgenciaMasterServicing, List<Expediente>> agencias;
  List<Expediente> all;
}
