package com.haya.alaska.master_servicing.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MSAgendaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Level (Required)");
      cabeceras.add("Level Id (Required)");
      cabeceras.add("Title (Required)");
      cabeceras.add("Class (Required)");
      cabeceras.add("Event Type (Required)");
      cabeceras.add("Event Subtype (Required)");
      cabeceras.add("Event Result");
      cabeceras.add("Event date (Required)");
      cabeceras.add("Event time");
      cabeceras.add("Importance (Required)");
      cabeceras.add("Comments");

    } else {
      cabeceras.add("Nivel (Obligatorio)");
      cabeceras.add("Nivel ID (Obligatorio)");
      cabeceras.add("Titulo (Obligatorio)");
      cabeceras.add("Clase (Obligatorio)");
      cabeceras.add("Tipo Evento (Obligatorio)");
      cabeceras.add("Subtipo Evento (Obligatorio)");
      cabeceras.add("Resultado Evento");
      cabeceras.add("Fecha Evento (Obligatorio)");
      cabeceras.add("Hora Evento");
      cabeceras.add("Importancia (Obligatorio)");
      cabeceras.add("Comentarios");
    }
  }

  public MSAgendaExcel(){
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
