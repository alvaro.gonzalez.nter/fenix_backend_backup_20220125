package com.haya.alaska.master_servicing.infrastructure.controller.dto;

import com.haya.alaska.criterio_ms_antiguedad_deuda.domain.CriterioMSAntiguedadDeuda;
import com.haya.alaska.criterio_ms_deuda_impagada.domain.CriterioMSDeudaImpagada;
import com.haya.alaska.criterio_ms_remanente.domain.CriterioMSRemanente;
import com.haya.alaska.criterio_ms_riesgo_total.domain.CriterioMSRiesgoTotal;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CatalogoMSOutputDTO implements Serializable {
  private Integer id;
  private Integer idCartera;
  private Boolean activo;
  private String codigo;
  private String valor;

  public CatalogoMSOutputDTO (CriterioMSAntiguedadDeuda cMS){
    this.id = cMS.getId();
    this.idCartera = cMS.getCartera().getId();
    this.activo = cMS.getActivo();
    this.codigo = cMS.getCodigo();
    this.valor = cMS.getValor();
  }

  public CatalogoMSOutputDTO (CriterioMSDeudaImpagada cMS){
    this.id = cMS.getId();
    this.idCartera = cMS.getCartera().getId();
    this.activo = cMS.getActivo();
    this.codigo = cMS.getCodigo();
    this.valor = cMS.getValor();
  }

  public CatalogoMSOutputDTO (CriterioMSRemanente cMS){
    this.id = cMS.getId();
    this.idCartera = cMS.getCartera().getId();
    this.activo = cMS.getActivo();
    this.codigo = cMS.getCodigo();
    this.valor = cMS.getValor();
  }

  public CatalogoMSOutputDTO (CriterioMSRiesgoTotal cMS){
    this.id = cMS.getId();
    this.idCartera = cMS.getCartera().getId();
    this.activo = cMS.getActivo();
    this.codigo = cMS.getCodigo();
    this.valor = cMS.getValor();
  }
}
