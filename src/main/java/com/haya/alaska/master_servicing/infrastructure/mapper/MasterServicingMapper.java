package com.haya.alaska.master_servicing.infrastructure.mapper;

import com.haya.alaska.agencia_master_servicing.domain.AgenciaMasterServicing;
import com.haya.alaska.agencia_master_servicing.infrastructure.repository.AgenciaMasterServicingRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTOToList;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.master_servicing.infrastructure.controller.dto.ExpedientesMSOutputDTO;
import com.haya.alaska.master_servicing.infrastructure.controller.dto.MasterServicingOutputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MasterServicingMapper {
  @Autowired
  AgenciaMasterServicingRepository agenciaMasterServicingRepository;
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  ExpedienteRepository expedienteRepository;
  @Autowired
  ExpedienteMapper expedienteMapper;

  public MasterServicingOutputDTO getOutput(Integer idCartera) throws NotFoundException {
    Cartera c = carteraRepository.findById(idCartera).orElseThrow(() ->
      new NotFoundException("Cartera", idCartera));

    MasterServicingOutputDTO output = new MasterServicingOutputDTO();

    output.setCartera(new CarteraDTOToList(c));

    Integer total = 0;
    List<Expediente> exps1 = expedienteRepository.findDistinctAllByCarteraIdAndAsignacionesUsuarioAreasNombre(idCartera, "Master Servicing");
    List<Expediente> exps2 = expedienteRepository.findDistinctAllByCarteraIdAndAsignacionesUsuarioAreasNombreNotAndAgenciaIsNotNull(idCartera, "Master Servicing");
    exps1.addAll(exps2);
    List<Expediente> finalExp = exps1.stream().distinct().collect(Collectors.toList());

    total = finalExp.size();
    Double porcentaje = 0.0;

    List<Expediente> exN = expedienteRepository.findAllByCarteraIdAndAsignacionesUsuarioAreasNombreAndAgenciaIsNull(idCartera, "Master Servicing");
    porcentaje = exN.size() * 100.0 / total;
    output.setSinAsignar(getOutput(exN, null, porcentaje));

    List<ExpedientesMSOutputDTO> listaAgencias = new ArrayList<>();

    List<AgenciaMasterServicing> agencias = agenciaMasterServicingRepository.findAll();

    for (AgenciaMasterServicing ams : agencias){
      List<Expediente> expList = expedienteRepository.findAllByCarteraIdAndAgenciaId(idCartera, ams.getId());
      porcentaje = expList.size() * 100.0 / total;
      ExpedientesMSOutputDTO emsOutput = getOutput(expList, ams, porcentaje);
      listaAgencias.add(emsOutput);
    }

    output.setOtrasAgencias(listaAgencias);

    return output;
  }

  public ExpedientesMSOutputDTO getOutput (List<Expediente> expedientes, AgenciaMasterServicing agencia, Double porcentaje){
    ExpedientesMSOutputDTO output = new ExpedientesMSOutputDTO();
    Integer c = 0;
    Integer i = 0;
    Integer g = 0;
    Double s = 0.0;
    List<ExpedienteDTOToList> dtoList = new ArrayList<>();

    for (Expediente ex : expedientes){
      s += ex.getSaldoGestion();
      c += ex.getContratos().size();
      for (Contrato con : ex.getContratos()){
        i += con.getIntervinientes().size();
        g += con.getBienes().size();
      }
      //dtoList.add(expedienteMapper.parseDTOToList(ex));
    }

    if (agencia != null)
      output.setAgencia(new CatalogoMinInfoDTO(agencia));
    output.setNExpedientes(expedientes.size());
    output.setNContratos(c);
    output.setNIntervinientes(i);
    output.setNBienes(g);
    output.setSaldoTotal(s);
    output.setPorcentaje(porcentaje);
    //output.setExpedientes(dtoList);

    return output;
  }
}
