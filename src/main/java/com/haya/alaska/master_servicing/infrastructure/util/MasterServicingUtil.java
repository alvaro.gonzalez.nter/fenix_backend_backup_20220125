package com.haya.alaska.master_servicing.infrastructure.util;

import com.haya.alaska.agencia_master_servicing.domain.AgenciaMasterServicing;
import com.haya.alaska.agencia_master_servicing.domain.AgenciaMasterServicing_;
import com.haya.alaska.agencia_master_servicing.infrastructure.repository.AgenciaMasterServicingRepository;
import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.area_usuario.domain.AreaUsuario_;
import com.haya.alaska.area_usuario.infrastructure.repository.AreaUsuarioRepository;
import com.haya.alaska.asignacion_expediente.application.AsignacionExpedienteUseCase;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente_;
import com.haya.alaska.asignacion_expediente.infrastructure.repository.AsignacionExpedienteRepository;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.IntervaloImporte;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.clase_evento.infrastructure.repository.ClaseEventoRepository;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable_;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion.infrastructure.repository.EstimacionRepository;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.importancia_evento.domain.ImportanciaEvento;
import com.haya.alaska.importancia_evento.infrastructure.repository.ImportanciaEventoRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.master_servicing.domain.*;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.infrastructure.repository.ProcedimientoRepository;
import com.haya.alaska.producto.domain.Producto_;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.resultado_evento.domain.ResultadoEvento;
import com.haya.alaska.resultado_evento.infrastructure.repository.ResultadoEventoRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.skip_tracing.infrastructure.repository.SkipTracingRepository;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Component
public class MasterServicingUtil {
  @PersistenceContext
  private EntityManager entityManager;
  @Autowired
  ExpedienteRepository expedienteRepository;
  @Autowired
  ContratoRepository contratoRepository;
  @Autowired
  BienRepository bienRepository;
  @Autowired
  IntervinienteRepository intervinienteRepository;
  @Autowired
  ProcedimientoRepository procedimientoRepository;
  @Autowired
  PropuestaRepository propuestaRepository;
  @Autowired
  ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  EstimacionRepository estimacionRepository;
  @Autowired
  SkipTracingRepository skipTracingRepository;
  @Autowired
  AgenciaMasterServicingRepository agenciaMasterServicingRepository;
  @Autowired
  ClaseEventoRepository claseEventoRepository;
  @Autowired
  SubtipoEventoRepository subtipoEventoRepository;
  @Autowired
  ResultadoEventoRepository resultadoEventoRepository;
  @Autowired
  ImportanciaEventoRepository importanciaEventoRepository;
  @Autowired
  UsuarioRepository usuarioRepository;
  @Autowired
  AsignacionExpedienteUseCase asignacionExpedienteUseCase;
  @Autowired
  AsignacionExpedienteRepository asignacionExpedienteRepository;
  @Autowired
  AreaUsuarioRepository areaUsuarioRepository;

  @Transactional
  public MasterServicingSeparado obtenerExpedientes(MultipartFile file) throws Exception {
    List<Expediente> expedientes = new ArrayList<>();
    XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
    XSSFSheet worksheet = workbook.getSheetAt(0);
    Map<AgenciaMasterServicing, List<Expediente>> agencias = new HashMap<>();
    List<Expediente> exN = new ArrayList<>();

    for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      String idConcatPre = getRawStringValueConGuion(row, 0);
      if (idConcatPre == null){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("There is no idExpediente in the line: " + i);
        else throw new NotFoundException("No hay un idExpediente en la línea: " + i);
      }
      String idConcat = limpiarConcatenado(idConcatPre);
      Expediente ex = expedienteRepository.findByIdConcatenado(idConcat).orElseThrow(() ->
        new NotFoundException("Expediente", "IdConcatenado", idConcat));
      String agenciaString = getRawStringValueConGuion(row, 9);
      AgenciaMasterServicing agencia = null;
      if (agenciaString != null){
        agencia = agenciaMasterServicingRepository.findByValor(agenciaString).orElse(null);
        List<Expediente> porAgencia = agencias.get(agencia);
        if (porAgencia == null){
          porAgencia = new ArrayList<>();
          agencias.put(agencia, porAgencia);
        }
        porAgencia.add(ex);
      } else {
        exN.add(ex);
      }
      ex.setAgencia(agencia);
      expedientes.add(ex);
    }

    MasterServicingSeparado result = new MasterServicingSeparado();
    result.setAll(expedientes);
    result.setAgencias(agencias);
    result.setExN(exN);

    return result;
  }

  @Transactional
  public void asignarUsuario(MasterServicingSeparado separado, Usuario logg) throws Exception {
    List<Expediente> exN = separado.getExN();

    if (!exN.isEmpty()){
      for (Expediente ex : exN){
        Usuario user = ex.getUsuarioByPerfil("Perfil consulta externo");
        if (user != null){
          List<AreaUsuario> areas = new ArrayList<>(user.getAreas());
          if (!areas.isEmpty()){
            AreaUsuario au = areaUsuarioRepository.findByNombre("Master Servicing").orElseThrow(() ->
              new NotFoundException("AreaUsuario", "Nombre", "Master Servicing"));
            if (areas.contains(au)){
              asignacionExpedienteRepository.deleteAllByExpedienteIdAndUsuarioId(ex.getId(), user.getId());
            }
          }
        }
      }
    }
    Map<AgenciaMasterServicing, List<Expediente>> agencias = separado.getAgencias();
    for (AgenciaMasterServicing aMS : agencias.keySet()){
      List<Expediente> expedientes = agencias.get(aMS);
      if (!expedientes.isEmpty()){
        List<Usuario> usuarios =
          usuarioRepository.findByPerfilNombreAndAreas_NombreAndGrupoUsuarios_Nombre(
            "Perfil consulta externo", "Master Servicing", aMS.getValor());
        if (!usuarios.isEmpty()){
          List<Integer> exPIds = expedientes.stream().map(Expediente::getId).collect(Collectors.toList());
          Usuario usuario = usuarios.get(0);
          asignacionExpedienteUseCase.modificarGestor(exPIds, null, usuario.getId(), false, false, false, new BusquedaDto(), logg);
        }
      }
    }
  }

  private String limpiarConcatenado(String concat){
    String result = concat;

    if (concat.contains("'")){
      result = concat.replace("'", "");
    }

    return result;
  }

  public List<EventoInputDTO> obtenerEventos(MultipartFile file) throws IOException, NotFoundException {
    List<EventoInputDTO> eventos = new ArrayList<>();
    XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
    XSSFSheet worksheet = workbook.getSheetAt(7);

    for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);

      EventoInputDTO evento = getRowEvento(row);
      eventos.add(evento);
    }

    return eventos;
  }

  private void setElementosComunes(EventoInputDTO input){
    input.setProgramacion(false);
    input.setCorreo(false);
    input.setAutogenerado(true);
    input.setRevisado(false);
  }

  private EventoInputDTO getRowEvento(XSSFRow row) throws NotFoundException {
    EventoInputDTO evento = new EventoInputDTO();
    setElementosComunes(evento);

    String nivel = getRawStringValueConGuion(row, 0);
    if (nivel == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("Row '" + row.getRowNum () + "' does not have an associated level.");
      else throw new NotFoundException("La fila '" + row.getRowNum() + "' no tiene un nivel asociado.");
    }
    Integer nivelId = getNivel(nivel);
    evento.setNivel(nivelId);

    String idONivel = getRawStringValue(row, 1);
    if (idONivel == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("Row '" + row.getRowNum () + "' does not have an associated idlevel.");
      else throw new NotFoundException("La fila '" + row.getRowNum() + "' no tiene un nivelId asociado.");
    }

    Integer idObjectNivel;
    if (nivelId == 1 || nivelId == 2 || nivelId == 7){
      String concat = limpiarConcatenado(idONivel);
      if (nivelId == 1){
        Expediente ex = expedienteRepository.findByIdConcatenado(concat).orElseThrow(() ->
          new NotFoundException("Expediente", "IdConcatenado", concat));
        idObjectNivel = ex.getId();
      } else if (nivelId == 2){
        Contrato co = contratoRepository.findByIdCarga(concat).orElseThrow(() ->
          new NotFoundException("Contrato", "IdCarga", concat));
        idObjectNivel = co.getId();
      } else {
        ExpedientePosesionNegociada exPN = expedientePosesionNegociadaRepository.findByIdConcatenado(concat).orElseThrow(() ->
          new NotFoundException("ExpedientePosesiónNegociada", "IdConcatenado", concat));
        idObjectNivel = exPN.getId();
      }
    }
    else idObjectNivel = Integer.parseInt(idONivel);
    if (idObjectNivel == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("Row '" + row.getRowNum () + "' does not have an associated idNivel.");
      else throw new NotFoundException("La fila '" + row.getRowNum() + "' no tiene un idNivel asociado.");
    }
    evento.setNivelId(idObjectNivel);

    String titulo = getRawStringValueConGuion(row, 2);
    if (titulo == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("Row '" + row.getRowNum () + "' does not have an associated title.");
      else throw new NotFoundException("La fila '" + row.getRowNum() + "' no tiene un titulo asociado.");
    }
    evento.setTitulo(titulo);

    String subtipo = getRawStringValueConGuion(row, 5);
    if (subtipo == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("Row '" + row.getRowNum () + "' does not have an associated subtipo.");
      else throw new NotFoundException("La fila '" + row.getRowNum() + "' no tiene un subtipo asociado.");
    }

    Cartera cartera = getCartera(nivelId, idObjectNivel);
    SubtipoEvento se;
    if (cartera == null) se = subtipoEventoRepository.findByValorAndCarteraIsNull(subtipo).orElseThrow(() ->
      new NotFoundException("SubtipoEvento", subtipo, "Cartera", "null"));
    else se = subtipoEventoRepository.findByValorAndCarteraId(subtipo, cartera.getId()).orElseThrow(() ->
      new NotFoundException("SubtipoEvento", subtipo, "Cartera", cartera.getNombre()));
    evento.setSubtipoEvento(se.getId());
    evento.setTipoEvento(se.getTipoEvento().getId());
    evento.setClaseEvento(se.getTipoEvento().getClaseEvento().getId());

    Usuario gestor = getGestor(nivelId, idObjectNivel);
    if (gestor == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The '" + nivel + "' of the row '" + row.getRowNum () + "' does not have a manager in its associated File.");
      else throw new NotFoundException("El '" + nivel  + "' de la fila '" + row.getRowNum() + "' no tiene un gestor en su Expediente asociado.");
    }
    else evento.setDestinatario(gestor.getId());

    String resultado = getRawStringValueConGuion(row, 6);
    ResultadoEvento re = null;
    if (resultado != null){
      re = resultadoEventoRepository.findByValor(resultado).orElseThrow(() ->
        new NotFoundException("ResultadoEvento", resultado));
      evento.setResultado(re.getId());
    }

    Date fechaEvento = getDateValueConGuion(row, 7);
    if (fechaEvento == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("Row '" + row.getRowNum () + "' does not have a date associated with it.");
      else throw new NotFoundException("La fila '" + row.getRowNum() + "' no tiene una fecha asociada.");
    }
    Calendar c = Calendar.getInstance();
    c.setTime(fechaEvento);

    if (row.getCell(8) != null){
      Integer[] lista = getTimeValueConGuion(row, 8);
      if (lista != null){
        c.set(Calendar.HOUR, lista[0]);
        c.set(Calendar.MINUTE, lista[1]);
        c.set(Calendar.SECOND, lista[2]);
        c.set(Calendar.MILLISECOND, lista[3]);
      }
    }
    Date d = c.getTime();

    evento.setFechaCreacion(d);
    evento.setFechaAlerta(d);
    evento.setFechaLimite(d);

    if (re == null){
      if (fechaEvento.before(new Date())) evento.setEstado(1);
      else evento.setEstado(4);
    } else {
      evento.setEstado(3);
    }

    String importancia = getRawStringValueConGuion(row, 9);
    if (importancia != null) {
      ImportanciaEvento ie = importanciaEventoRepository.findByValor(importancia).orElseThrow(() ->
        new NotFoundException("ImportanciaEvento", importancia));
      evento.setImportancia(ie.getId());
    } else evento.setImportancia(2);

    String comentarios = getRawStringValue(row, 10);
    evento.setComentariosDestinatario(comentarios);

    return evento;
  }

  private Integer getNivel(String nivel) throws NotFoundException {
    Integer i = 0;

    switch (nivel){
      case ("Expediente"):
        i = 1;
        break;
      case ("Contrato"):
        i = 2;
        break;
      case ("Bien"):
        i = 3;
        break;
      case ("Interviniente"):
        i = 4;
        break;
      case ("Judicial"):
        i = 5;
        break;
      case ("Propuesta"):
        i = 6;
        break;
      case ("Expediente Posesión Negociada"):
        i = 7;
        break;
      case ("Bien Posesión Negociada"):
        i = 8;
        break;
      case ("Estimaciones"):
        i = 9;
        break;
      case ("Skip Tracing"):
        i = 11;
        break;
      default:
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("The level presented is: '" + nivel + "', the options are:" +
                  "'Expediente', 'Contrato', 'Bien', 'Interviniente', 'Judicial', 'Propuesta', 'Expediente Posesión Negociada', " +
                  "'Bien Posesión Negociada', 'Estimaciones' or 'Skip Tracing'.");
        else throw new NotFoundException("El nivel presentado es: '" + nivel + "', las opciones son:" +
                "'Expediente', 'Contrato', 'Bien', 'Interviniente', 'Judicial', 'Propuesta', 'Expediente Posesión Negociada', " +
                "'Bien Posesión Negociada', 'Estimaciones' o 'Skip Tracing'.");
    }

    return i;
  }

  private Cartera getCartera(Integer nivel, Integer id) throws NotFoundException {
    Cartera cartera = null;
    switch (nivel){
      case (1):
        cartera = getExpediente(id);
        break;
      case (2):
        cartera = getContrato(id);
        break;
      case (3):
        cartera = getBien(id);
        break;
      case (4):
        cartera = getInterviniente(id);
        break;
      case (5):
        cartera = getJudicial(id);
        break;
      case (6):
        cartera = getPropuesta(id);
        break;
      case (7):
        cartera = getExpedientePN(id);
        break;
      case (8):
        cartera = getBienPN(id);
        break;
      case (9):
        cartera = getEstimacion(id);
        break;
      case (11):
        cartera = getSkipTracing(id);
        break;
    }
    return cartera;
  }

  private Cartera getExpediente(Integer id) throws NotFoundException {
    Expediente ex = expedienteRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Expediente", id));
    Cartera ca = ex.getCartera();
    return ca;
  }
  private Cartera getContrato(Integer id) throws NotFoundException {
    Contrato co = contratoRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Contrato", id));
    Expediente ex = co.getExpediente();
    Cartera ca = ex.getCartera();
    return ca;
  }
  private Cartera getBien(Integer id) throws NotFoundException {
    Bien bi = bienRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Bien", id));
    List<ContratoBien> cbList = new ArrayList<>(bi.getContratos());
    if (cbList.isEmpty()) throw new NotFoundException("Contrato", "Bien", id, false);
    Contrato co = cbList.get(0).getContrato();
    Expediente ex = co.getExpediente();
    Cartera ca = ex.getCartera();
    return ca;
  }
  private Cartera getInterviniente(Integer id) throws NotFoundException {
    Interviniente in = intervinienteRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Interviniente", id));
    List<ContratoInterviniente> ciList = new ArrayList<>(in.getContratos());
    if (ciList.isEmpty()) throw new NotFoundException("Contrato", "Interviniente", id, false);
    Contrato co = ciList.get(0).getContrato();
    Expediente ex = co.getExpediente();
    Cartera ca = ex.getCartera();
    return ca;
  }
  private Cartera getJudicial(Integer id) throws NotFoundException {
    Procedimiento pr = procedimientoRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Procedimiento", id));
    List<Contrato> coList = new ArrayList<>(pr.getContratos());
    if (coList.isEmpty()) throw new NotFoundException("Contrato", "Procedimiento", id, false);
    Contrato co = coList.get(0);
    Expediente ex = co.getExpediente();
    Cartera ca = ex.getCartera();
    return ca;
  }
  private Cartera getPropuesta(Integer id)throws NotFoundException {
    Propuesta pr = propuestaRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Propuesta", id));
    Cartera ca;
    if (pr.getExpediente() == null){
      ExpedientePosesionNegociada exPN = pr.getExpedientePosesionNegociada();
      ca = exPN.getCartera();
    } else {
      Expediente ex = pr.getExpediente();
      ca = ex.getCartera();
    }
    return ca;
  }
  private Cartera getExpedientePN(Integer id) throws NotFoundException {
    ExpedientePosesionNegociada exPN = expedientePosesionNegociadaRepository.findById(id).orElseThrow(() ->
      new NotFoundException("ExpedientePosesionNegociada", id));
    Cartera ca = exPN.getCartera();
    return ca;
  }
  private Cartera getBienPN(Integer id) throws NotFoundException {
    BienPosesionNegociada biPN = bienPosesionNegociadaRepository.findById(id).orElseThrow(() ->
      new NotFoundException("BienPosesionNegociada", id));
    ExpedientePosesionNegociada exPN = biPN.getExpedientePosesionNegociada();
    Cartera ca = exPN.getCartera();
    return ca;
  }
  private Cartera getEstimacion(Integer id) throws NotFoundException {
    Estimacion es = estimacionRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Estimacion", id));
    Cartera ca;
    if (es.getExpediente() == null){
      ExpedientePosesionNegociada exPN = es.getExpedientePosesionNegociada();
      ca = exPN.getCartera();
    } else {
      Expediente ex = es.getExpediente();
      ca = ex.getCartera();
    }
    return ca;
  }
  private Cartera getSkipTracing(Integer id) throws NotFoundException {
    SkipTracing st = skipTracingRepository.findById(id).orElseThrow(() ->
      new NotFoundException("SkipTracing", id));
    ContratoInterviniente ci = st.getContratoInterviniente();
    Contrato co = ci.getContrato();
    Expediente ex = co.getExpediente();
    Cartera ca = ex.getCartera();
    return ca;
  }

  private Usuario getGestor(Integer nivel, Integer id) throws NotFoundException {
    Usuario usuario = null;
    switch (nivel){
      case (1):
        usuario = getExpedienteGestor(id);
        break;
      case (2):
        usuario = getContratoGestor(id);
        break;
      case (3):
        usuario = getBienGestor(id);
        break;
      case (4):
        usuario = getIntervinienteGestor(id);
        break;
      case (5):
        usuario = getJudicialGestor(id);
        break;
      case (6):
        usuario = getPropuestaGestor(id);
        break;
      case (7):
        usuario = getExpedientePNGestor(id);
        break;
      case (8):
        usuario = getBienPNGestor(id);
        break;
      case (9):
        usuario = getEstimacionGestor(id);
        break;
      case (11):
        usuario = getSkipTracingGestor(id);
        break;
    }
    return usuario;
  }

  private Usuario getExpedienteGestor(Integer id) throws NotFoundException {
    Expediente ex = expedienteRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Expediente", id));
    Usuario ca = ex.getGestor();
    return ca;
  }
  private Usuario getContratoGestor(Integer id) throws NotFoundException {
    Contrato co = contratoRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Contrato", id));
    Expediente ex = co.getExpediente();
    Usuario ca = ex.getGestor();
    return ca;
  }
  private Usuario getBienGestor(Integer id) throws NotFoundException {
    Bien bi = bienRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Bien", id));
    List<ContratoBien> cbList = new ArrayList<>(bi.getContratos());
    if (cbList.isEmpty()) throw new NotFoundException("Contrato", "Bien", id, false);
    Contrato co = cbList.get(0).getContrato();
    Expediente ex = co.getExpediente();
    Usuario ca = ex.getGestor();
    return ca;
  }
  private Usuario getIntervinienteGestor(Integer id) throws NotFoundException {
    Interviniente in = intervinienteRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Interviniente", id));
    List<ContratoInterviniente> ciList = new ArrayList<>(in.getContratos());
    if (ciList.isEmpty()) throw new NotFoundException("Contrato", "Interviniente", id, false);
    Contrato co = ciList.get(0).getContrato();
    Expediente ex = co.getExpediente();
    Usuario ca = ex.getGestor();
    return ca;
  }
  private Usuario getJudicialGestor(Integer id) throws NotFoundException {
    Procedimiento pr = procedimientoRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Procedimiento", id));
    List<Contrato> coList = new ArrayList<>(pr.getContratos());
    if (coList.isEmpty()) throw new NotFoundException("Contrato", "Procedimiento", id, false);
    Contrato co = coList.get(0);
    Expediente ex = co.getExpediente();
    Usuario ca = ex.getGestor();
    return ca;
  }
  private Usuario getPropuestaGestor(Integer id)throws NotFoundException {
    Propuesta pr = propuestaRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Propuesta", id));
    Usuario ca;
    if (pr.getExpediente() == null){
      ExpedientePosesionNegociada exPN = pr.getExpedientePosesionNegociada();
      ca = exPN.getGestor();
    } else {
      Expediente ex = pr.getExpediente();
      ca = ex.getGestor();
    }
    return ca;
  }
  private Usuario getExpedientePNGestor(Integer id) throws NotFoundException {
    ExpedientePosesionNegociada exPN = expedientePosesionNegociadaRepository.findById(id).orElseThrow(() ->
      new NotFoundException("ExpedientePosesionNegociada", id));
    Usuario ca = exPN.getGestor();
    return ca;
  }
  private Usuario getBienPNGestor(Integer id) throws NotFoundException {
    BienPosesionNegociada biPN = bienPosesionNegociadaRepository.findById(id).orElseThrow(() ->
      new NotFoundException("BienPosesionNegociada", id));
    ExpedientePosesionNegociada exPN = biPN.getExpedientePosesionNegociada();
    Usuario ca = exPN.getGestor();
    return ca;
  }
  private Usuario getEstimacionGestor(Integer id) throws NotFoundException {
    Estimacion es = estimacionRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Estimacion", id));
    Usuario ca;
    if (es.getExpediente() == null){
      ExpedientePosesionNegociada exPN = es.getExpedientePosesionNegociada();
      ca = exPN.getGestor();
    } else {
      Expediente ex = es.getExpediente();
      ca = ex.getGestor();
    }
    return ca;
  }
  private Usuario getSkipTracingGestor(Integer id) throws NotFoundException {
    SkipTracing st = skipTracingRepository.findById(id).orElseThrow(() ->
      new NotFoundException("SkipTracing", id));
    ContratoInterviniente ci = st.getContratoInterviniente();
    Contrato co = ci.getContrato();
    Expediente ex = co.getExpediente();
    Usuario ca = ex.getGestor();
    return ca;
  }

  public List<Expediente> filtrarExpedientes(
    Boolean listar, Integer idCartera, Integer idAgencia, Integer idProducto, Integer idClasificacionContable,
    Integer adMin, Integer adMax, Double deudaIni, Double deudaFin, Double remaIni, Double remaFin,
    Double riesgoIni, Double riesgoFin, Boolean reasignacion){

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Expediente> query = cb.createQuery(Expediente.class);

    Root<Expediente> root = query.from(Expediente.class);
    List<Predicate> predicates = new ArrayList<>();

    if (reasignacion){
      Join<Expediente, AsignacionExpediente> asigJoin = root.join(Expediente_.asignaciones, JoinType.INNER);
      Join<AsignacionExpediente, Usuario> asigUsuJoin = asigJoin.join(AsignacionExpediente_.usuario, JoinType.INNER);
      predicates.add(cb.equal(asigUsuJoin.get(Usuario_.EMAIL), "masterServicing@ms.com"));
      predicates.add(cb.isNull(root.get(Expediente_.AGENCIA)));
    } else {
      if (idAgencia == null){
        Join<Expediente, AsignacionExpediente> asigJoin = root.join(Expediente_.asignaciones, JoinType.INNER);
        Join<AsignacionExpediente, Usuario> asigUsuJoin = asigJoin.join(AsignacionExpediente_.usuario, JoinType.INNER);
        Join<Usuario, AreaUsuario> usuAreJoin = asigUsuJoin.join(Usuario_.areas, JoinType.INNER);
        predicates.add(cb.equal(usuAreJoin.get(AreaUsuario_.nombre), "Master Servicing"));
      }
    }

    if (idCartera != null){
      Join<Expediente, Cartera> carteraJoin = root.join(Expediente_.CARTERA, JoinType.INNER);
      predicates.add(cb.equal(carteraJoin.get(Cartera_.ID), idCartera));
    }
    if (idAgencia != null){
      Join<Expediente, AgenciaMasterServicing> agenciaJoin = root.join(Expediente_.AGENCIA, JoinType.INNER);
      predicates.add(cb.equal(agenciaJoin.get(AgenciaMasterServicing_.ID), idAgencia));
    } else {
      if (listar)
        predicates.add(cb.isNull(root.get(Expediente_.AGENCIA)));
    }

    if (idProducto != null || idClasificacionContable != null || adMin != null || adMax != null ||
      deudaIni != null || deudaFin != null || remaIni != null || remaFin != null || riesgoIni != null || riesgoFin != null){
      Join<Expediente, Contrato> contratoJoin = root.join(Expediente_.contratos, JoinType.INNER);
      contratoJoin.on(cb.equal(contratoJoin.get(Contrato_.esRepresentante), true));
      if (idProducto != null)
        predicates.add(cb.equal(contratoJoin.get(Contrato_.PRODUCTO).get(Producto_.ID), idProducto));
      if (idClasificacionContable != null)
        predicates.add(cb.equal(contratoJoin.get(Contrato_.CLASIFICACION_CONTABLE).get(ClasificacionContable_.ID), idClasificacionContable));
      if (adMin != null || adMax != null){
        Date hoy = new Date();
        Date fechaIni = null;
        Date fechaFin = null;

        if (adMin != null){
          Calendar c = Calendar.getInstance();
          c.setTime(hoy);
          c.add(Calendar.DATE, -adMin);
          fechaFin = c.getTime();
        }
        if (adMax != null){
          Calendar c = Calendar.getInstance();
          c.setTime(hoy);
          c.add(Calendar.DATE, -adMax);
          fechaIni = c.getTime();
        }


        if (fechaIni != null && fechaFin != null) {
          predicates.add(cb.between(contratoJoin.get(Contrato_.fechaPrimerImpago), fechaIni, fechaFin));
        } else {
          if (fechaIni != null) predicates.add(cb.greaterThanOrEqualTo(contratoJoin.get(Contrato_.fechaPrimerImpago), fechaIni));
          if (fechaFin != null) predicates.add(cb.lessThanOrEqualTo(contratoJoin.get(Contrato_.fechaPrimerImpago), fechaFin));
        }
      }
    }

    if (deudaIni != null || deudaFin != null){
      IntervaloImporte ii = new IntervaloImporte();
      ii.setMenor(deudaFin);
      ii.setMayor(deudaIni);
      Subquery<Double> subqueryDeudaImpagada = cb.createQuery().subquery(Double.class);
      Root<Contrato> importesContratos = subqueryDeudaImpagada.from(Contrato.class);
      predicates.addAll(addIntervaloImporte(cb,
        subqueryDeudaImpagada.select(cb.sum(importesContratos.get(Contrato_.DEUDA_IMPAGADA))).distinct(true),
        ii));
    }

    if (remaIni != null || remaFin != null){
      IntervaloImporte ii = new IntervaloImporte();
      ii.setMenor(remaFin);
      ii.setMayor(remaIni);
      Subquery<Double> subqueryDeudaImpagada = cb.createQuery().subquery(Double.class);
      Root<Contrato> importesContratos = subqueryDeudaImpagada.from(Contrato.class);
      predicates.addAll(addIntervaloImporte(cb,
        subqueryDeudaImpagada.select(cb.sum(importesContratos.get(Contrato_.RESTO_HIPOTECARIO))).distinct(true),
        ii));
    }

    if (riesgoIni != null || riesgoFin != null){
      IntervaloImporte ii = new IntervaloImporte();
      ii.setMenor(riesgoFin);
      ii.setMayor(riesgoIni);
      Subquery<Double> subqueryDeudaImpagada = cb.createQuery().subquery(Double.class);
      Root<Contrato> importesContratos = subqueryDeudaImpagada.from(Contrato.class);
      predicates.addAll(addIntervaloImporte(cb,
        subqueryDeudaImpagada.select(cb.sum(importesContratos.get(Contrato_.SALDO_GESTION))).distinct(true),
        ii));
    }


    var rs = query.select(root).distinct(true).where(predicates.toArray(new Predicate[predicates.size()]));

    Set<ParameterExpression<?>> q = query.getParameters();

    List<Expediente> listaPropuesta = entityManager.createQuery(query).getResultList();
    return listaPropuesta;
  }

  public List<Predicate> addIntervaloImporte(CriteriaBuilder cb, Expression<Double> atributo, IntervaloImporte intervaloImporte) {
    List<Predicate> predicates = new ArrayList<>();
    if (intervaloImporte.getIgual() != null) {
      predicates.add(cb.equal(atributo, intervaloImporte.getIgual()));
    } else {
      if (intervaloImporte.getMayor() != null) {
        predicates.add(cb.greaterThanOrEqualTo(atributo, intervaloImporte.getMayor()));
      }
      if (intervaloImporte.getMenor() != null) {
        predicates.add(cb.lessThanOrEqualTo(atributo, intervaloImporte.getMenor()));
      }
    }
    return predicates;
  }

  public LinkedHashMap<String, List<Collection<?>>> obtenerCabeceras(){
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expedientes = new ArrayList<>();
    expedientes.add(MSExpedienteExcel.cabeceras);
    List<Collection<?>> contratos = new ArrayList<>();
    contratos.add(MSContratoExcel.cabeceras);
    List<Collection<?>> garantias = new ArrayList<>();
    garantias.add(MSGarantiaExcel.cabeceras);
    List<Collection<?>> solvencia = new ArrayList<>();
    solvencia.add(MSSolvenciaExcel.cabeceras);
    List<Collection<?>> interviniente = new ArrayList<>();
    interviniente.add(MSIntervinienteExcel.cabeceras);
    List<Collection<?>> judicial = new ArrayList<>();
    judicial.add(MSJudicialExcel.cabeceras);
    List<Collection<?>> movimiento = new ArrayList<>();
    movimiento.add(MSMovimientosExcel.cabeceras);
    List<Collection<?>> agenda = new ArrayList<>();
    agenda.add(MSAgendaExcel.cabeceras);
    List<Collection<?>> catalogos = new ArrayList<>();
    catalogos.add(MSCatalogosExcel.cabeceras);

    datos.put("Expedientes", expedientes);
    datos.put("Contratos", contratos);
    datos.put("Garantías", garantias);
    datos.put("Solvencias", solvencia);
    datos.put("Intervinientes", interviniente);
    datos.put("Judicial", judicial);
    datos.put("Movimientos", movimiento);
    datos.put("Agenda", agenda);
    datos.put("Catálogos", catalogos);

    return datos;
  }

  public void addForExcel(
      List<Expediente> exList, LinkedHashMap<String, List<Collection<?>>> datos) throws NotFoundException {
    var expedientes = datos.get("Expedientes");
    var contratos = datos.get("Contratos");
    var garantias = datos.get("Garantías");
    var solvencias = datos.get("Solvencias");
    var intervinientes=datos.get("Intervinientes");
    var judicial = datos.get("Judicial");
    var movimientos = datos.get("Movimientos");
    var catalogos = datos.get("Catálogos");

    for (Expediente ex : exList){
      expedientes.add(new MSExpedienteExcel(ex).getValuesList());
      for (Contrato co : ex.getContratos()){
        contratos.add(new MSContratoExcel(co).getValuesList());
        for (ContratoBien cb : co.getBienes()){
          if (cb.getResponsabilidadHipotecaria() != null) {
            garantias.add(new MSGarantiaExcel(cb).getValuesList());
          } else {
            solvencias.add(new MSSolvenciaExcel(cb).getValuesList());
          }
        }
        for (ContratoInterviniente ci : co.getIntervinientes()){
          intervinientes.add(new MSIntervinienteExcel(ci).getValuesList());
        }
        for (Procedimiento pr : co.getProcedimientos()){
          judicial.add(new MSJudicialExcel(pr, co).getValuesList());
        }
        for (Movimiento mo : co.getMovimientos()){
          movimientos.add(new MSMovimientosExcel(mo, co).getValuesList());
        }
      }
    }

    List<String> niveles = getNiveles();
    List<ClaseEvento> clases = claseEventoRepository.findAll();
    List<ImportanciaEvento> importancias = importanciaEventoRepository.findAll();
    List<SubtipoEvento> subtipos;
    if (exList.isEmpty()){
      subtipos = subtipoEventoRepository.findByCarteraIsNull();
    } else {
      Expediente ex = exList.get(0);
      subtipos = subtipoEventoRepository.findByCarteraId(ex.getCartera().getId());
    }
    List<ResultadoEvento> resultados = resultadoEventoRepository.findAll();

    for (int i = 0; i < niveles.size(); i++){
      String nivel = niveles.get(i);

      String clase = null;
      if (i < clases.size()) clase = clases.get(i).getValor();

      String importancia = null;
      if (i < importancias.size()) importancia = importancias.get(i).getValor();

      catalogos.add(new MSCatalogosExcel(nivel, clase, importancia).getValuesList());
    }

    catalogos.add(new MSCatalogosExcel(null, null, null).getValuesList());

    catalogos.add(MSCatalogosExcel.getSubCabecera());

    for (int i = 0; i < subtipos.size(); i++){
      SubtipoEvento se = subtipos.get(i);

      String result = null;
      if (i < resultados.size()) result = resultados.get(i).getValor();

      catalogos.add(new MSCatalogosExcel(se, result).getValuesList());
    }
  }

  private List<String> getNiveles(){
    List<String> result = new ArrayList<>();

    result.add ("Expediente");
    result.add ("Contrato");
    result.add ("Bien");
    result.add ("Interviniente");
    result.add ("Judicial");
    result.add ("Propuesta");
    result.add ("Expediente Posesión Negociada");
    result.add ("Bien Posesión Negociada");
    result.add ("Estimaciones");
    result.add ("Skip Tracing");

    return result;
  }
}
