package com.haya.alaska.master_servicing.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ExpedientesMSOutputDTO implements Serializable {
  CatalogoMinInfoDTO agencia;
  Integer nExpedientes;
  Integer nContratos;
  Integer nIntervinientes;
  Integer nBienes;
  Double saldoTotal;
  Double porcentaje;
  //List<ExpedienteDTOToList> expedientes;
}
