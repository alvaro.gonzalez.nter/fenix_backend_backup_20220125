package com.haya.alaska.master_servicing.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class MasterServicingInputDTO implements Serializable {
  Integer cartera;
  CriteriosMS criteriosMS;
  List<PorcentageAgenciaInputDTO> porcentajes;
}
