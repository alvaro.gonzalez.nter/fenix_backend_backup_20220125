package com.haya.alaska.master_servicing.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CriteriosMS implements Serializable {
  Boolean tipoProducto;
  Boolean antiguedadDeuda;
  Boolean situacionContable;
  Boolean riesgoTotal;
  Boolean deudaImpagada;
  Boolean remanente;
}
