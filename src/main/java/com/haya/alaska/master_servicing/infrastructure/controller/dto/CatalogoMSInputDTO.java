package com.haya.alaska.master_servicing.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CatalogoMSInputDTO implements Serializable {
  private Integer id;
  private Integer idCartera;
  private Boolean activo;
  private String codigo;
  private String valor;
}
