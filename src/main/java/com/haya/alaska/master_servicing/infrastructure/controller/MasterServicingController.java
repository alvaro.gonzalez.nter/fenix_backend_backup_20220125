package com.haya.alaska.master_servicing.infrastructure.controller;

import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTOToList;
import com.haya.alaska.master_servicing.application.MasterServicingUseCase;
import com.haya.alaska.master_servicing.infrastructure.controller.dto.*;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/masterServicing")
public class MasterServicingController {
  @Autowired
  MasterServicingUseCase masterServicingUseCase;

  @ApiOperation(
    value = "Asignación Automática",
    notes = "Asigna automáticamente los expedientes a las agencias según los criterios y porcentajes pasados.")
  @PostMapping()
  @Transactional(rollbackFor = Exception.class)
  public Boolean asignarAgencias(
    @RequestBody @Valid MasterServicingInputDTO input,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return masterServicingUseCase.asignacionManual(input, usuario);
  }

  @ApiOperation(value = "Descargar información de Master Servicing de la cartera",
    notes = "Descargar información de Master Servicing de los expedientes de la cartera")
  @GetMapping("/excel")
  public ResponseEntity<InputStreamResource> findExcelContratosByExpediente(
    @RequestParam(value = "idCartera") Integer idCartera,
    @RequestParam(value = "agencia", required = false) Integer agencia,
    @RequestParam(value = "todos", required = false) Boolean todos) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(masterServicingUseCase.findSimpleExcel(idCartera, agencia, todos));

    return excelExport.download(excel, "MasterServicing_" + idCartera + "_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(value = "Obtener la distribución de los expedientes de la cartera por Agencia",
    notes = "Obtener la distribución de los expedientes de la cartera por Agencia")
  @GetMapping("/output")
  public MasterServicingOutputDTO getDTO(@RequestParam Integer idCartera) throws NotFoundException {
    MasterServicingOutputDTO result = masterServicingUseCase.getDTO(idCartera);
    return result;
  }

  @ApiOperation(value = "Obtener la distribución de todos los expedientes de la cartera",
    notes = "Obtener la distribución de todos los expedientes de la cartera")
  @GetMapping()
  public ExpedientesMSOutputDTO getOutput(@RequestParam Integer idCartera){
    ExpedientesMSOutputDTO result = masterServicingUseCase.getOutput(idCartera);
    return result;
  }

  @ApiOperation(value = "Obtener la distribución de todos los expedientes de la cartera",
    notes = "Obtener la distribución de todos los expedientes de la cartera")
  @GetMapping("/lista")
  public ListWithCountDTO<ExpedienteDTOToList> getAll(
    @RequestParam(value = "idCartera") Integer idCartera,
    @RequestParam(value = "idAgencia", required = false) Integer idAgencia,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page
  ){
    return masterServicingUseCase.getLista(idCartera, idAgencia, size, page);
  }

  @ApiOperation(value = "Carga los datos de MasterServicing",
    notes = "Carga los datos de MasterServicing y asocia las agencias a los expedientes.")
  @PostMapping("/carga")
  public CarteraDTOToList carga(
    @RequestParam("file") MultipartFile file,
    @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return masterServicingUseCase.cargaSimple(file, usuario);
  }

  @ApiOperation(value = "Carga los datos para hacer eventos",
    notes = "Carga los datos de MasterServicing para crear los eventos de la pestaña de Eventos.")
  @PostMapping("/cargaComunicacion")
  public Boolean cargaComunicacion(
    @RequestParam("file") MultipartFile file,
    @ApiIgnore CustomUserDetails principal) throws Exception {

    Usuario usuario = (Usuario) principal.getPrincipal();
    return masterServicingUseCase.cargaCompleja(file, usuario);
  }

  @ApiOperation(value = "Descargar información de Master Servicing de la cartera",
    notes = "Descargar información de Master Servicing de los expedientes de la cartera")
  @GetMapping("/excelComunicacion")
  public ResponseEntity<InputStreamResource> excelComunicacion(
    @RequestParam(value = "idCartera") Integer idCartera,
    @RequestParam(value = "agencia", required = false) Integer agencia,
    @RequestParam(value = "todos", required = false) Boolean todos) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = masterServicingUseCase.create(masterServicingUseCase.findComplexExcel(idCartera, agencia, todos));

    return excelExport.download(excel, "Comunicación_MasterServicing_" + idCartera + "_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(value = "Obtiene los Catálogos de Master Servicing")
  @GetMapping("/catalogo/{nombreCatalogo}")
  public ListWithCountDTO<CatalogoMSOutputDTO> getCatalogos(
      @PathVariable(value = "nombreCatalogo") String nombre,
      @RequestParam(value = "cartera") Integer cartera,
      @RequestParam(value = "activo") Boolean activo,
      @RequestParam(value = "size") Integer size,
      @RequestParam(value = "page") Integer page)
      throws Exception {
    return masterServicingUseCase.getCatalogos(nombre, cartera, activo, size, page);
  }

  @ApiOperation(value = "Actualiza los Catálogos de Master Servicing")
  @PutMapping("/catalogo/{nombreCatalogo}")
  public void updateCatalogos(
      @PathVariable(value = "nombreCatalogo") String nombre,
      @RequestBody List<CatalogoMSInputDTO> input)
      throws Exception {
    masterServicingUseCase.actualizarCatalogos(nombre, input);
  }
}
