package com.haya.alaska.master_servicing.infrastructure.controller.dto;

import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class MasterServicingOutputDTO implements Serializable {
  CarteraDTOToList cartera;
  ExpedientesMSOutputDTO sinAsignar;
  List<ExpedientesMSOutputDTO> otrasAgencias;
}
