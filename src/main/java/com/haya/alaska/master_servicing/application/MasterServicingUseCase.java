package com.haya.alaska.master_servicing.application;

import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTOToList;
import com.haya.alaska.master_servicing.infrastructure.controller.dto.*;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface MasterServicingUseCase {
  Boolean asignacionManual(MasterServicingInputDTO input, Usuario logged) throws Exception;
  LinkedHashMap<String, List<Collection<?>>> findSimpleExcel(Integer id, Integer agencia, Boolean todos) throws Exception;
  CarteraDTOToList cargaSimple(MultipartFile file, Usuario user) throws Exception;
  MasterServicingOutputDTO getDTO(Integer idCartera) throws NotFoundException;
  ExpedientesMSOutputDTO getOutput(Integer idCartera);
  ListWithCountDTO<ExpedienteDTOToList> getLista(Integer idCartera, Integer idAgencia, Integer size, Integer page);
  LinkedHashMap<String, List<Collection<?>>> findComplexExcel(Integer id, Integer agencia, Boolean todos) throws Exception;
  Boolean cargaCompleja(MultipartFile file, Usuario user) throws Exception;

  ListWithCountDTO<CatalogoMSOutputDTO> getCatalogos(String nombre, Integer cartera, Boolean activo, Integer size, Integer page);
  void actualizarCatalogos(String nombre, List<CatalogoMSInputDTO> inputs) throws NotFoundException, RequiredValueException;

  ByteArrayInputStream create(LinkedHashMap<String, List<Collection<?>>> datos) throws IOException;
}
