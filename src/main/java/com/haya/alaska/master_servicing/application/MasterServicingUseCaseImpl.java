package com.haya.alaska.master_servicing.application;

import com.haya.alaska.agencia_master_servicing.domain.AgenciaMasterServicing;
import com.haya.alaska.agencia_master_servicing.infrastructure.repository.AgenciaMasterServicingRepository;
import com.haya.alaska.asignacion_expediente.application.AsignacionExpedienteUseCase;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable;
import com.haya.alaska.clasificacion_contable.infrastructure.repository.ClasificacionContableRepository;
import com.haya.alaska.criterio_ms_antiguedad_deuda.domain.CriterioMSAntiguedadDeuda;
import com.haya.alaska.criterio_ms_antiguedad_deuda.infrastructure.repository.CriterioMSAntiguedadDeudaRepository;
import com.haya.alaska.criterio_ms_deuda_impagada.domain.CriterioMSDeudaImpagada;
import com.haya.alaska.criterio_ms_deuda_impagada.infrastructure.repository.CriterioMSDeudaImpagadaRepository;
import com.haya.alaska.criterio_ms_remanente.domain.CriterioMSRemanente;
import com.haya.alaska.criterio_ms_remanente.infrastructure.repository.CriterioMSRemanenteRepository;
import com.haya.alaska.criterio_ms_riesgo_total.domain.CriterioMSRiesgoTotal;
import com.haya.alaska.criterio_ms_riesgo_total.infrastructure.repository.CriterioMSRiesgoTotalRepository;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTOToList;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.master_servicing.domain.MasterServicingExcel;
import com.haya.alaska.master_servicing.domain.MasterServicingSeparado;
import com.haya.alaska.master_servicing.infrastructure.controller.dto.*;
import com.haya.alaska.master_servicing.infrastructure.mapper.MasterServicingMapper;
import com.haya.alaska.master_servicing.infrastructure.util.MasterServicingUtil;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.producto.infrastructure.repository.ProductoRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class MasterServicingUseCaseImpl implements MasterServicingUseCase {
  @Autowired
  AgenciaMasterServicingRepository agenciaMasterServicingRepository;
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  ExpedienteRepository expedienteRepository;
  @Autowired
  ExpedienteMapper expedienteMapper;
  @Autowired
  MasterServicingUtil masterServicingUtil;
  @Autowired
  MasterServicingMapper masterServicingMapper;
  @Autowired
  EventoUseCase eventoUseCase;
  @Autowired
  UsuarioRepository usuarioRepository;

  @Autowired
  ProductoRepository productoRepository;
  @Autowired
  ClasificacionContableRepository clasificacionContableRepository;
  @Autowired
  CriterioMSAntiguedadDeudaRepository criterioMSAntiguedadDeudaRepository;
  @Autowired
  CriterioMSDeudaImpagadaRepository criterioMSDeudaImpagadaRepository;
  @Autowired
  CriterioMSRemanenteRepository criterioMSRemanenteRepository;
  @Autowired
  CriterioMSRiesgoTotalRepository criterioMSRiesgoTotalRepository;
  @Autowired
  AsignacionExpedienteUseCase asignacionExpedienteUseCase;


  @Override
  public Boolean asignacionManual(MasterServicingInputDTO input, Usuario logged) throws Exception {
    Usuario logg = usuarioRepository.findById(logged.getId()).orElseThrow(() ->
      new NotFoundException("Usuario", logged.getId()));
    Integer porcentage = 0;
    for (PorcentageAgenciaInputDTO pa : input.getPorcentajes()){
      porcentage += pa.getPorcentaje();
    }
    if (porcentage != 100){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("The sum of the percentages of the agencies adds up to a" + porcentage + "%. It should be 100%");
      else throw new RequiredValueException("La suma de porcentajes de las agencias suma un " + porcentage + "%. Deberia ser 100%");
    }
    CriteriosMS criterios = input.getCriteriosMS();

    Integer idC = input.getCartera();

    List<Producto> productos = new ArrayList<>();
    if (criterios.getTipoProducto() == null || !criterios.getTipoProducto()) productos.add(null);
    else productos = productoRepository.findAll();

    List<ClasificacionContable> clasificaciones = new ArrayList<>();
    if (criterios.getSituacionContable() == null || !criterios.getSituacionContable()) clasificaciones.add(null);
    else clasificaciones = clasificacionContableRepository.findAll();

    List<CriterioMSAntiguedadDeuda> fechas = new ArrayList<>();
    if (criterios.getAntiguedadDeuda() == null || !criterios.getAntiguedadDeuda()) fechas.add(null);
    else fechas = criterioMSAntiguedadDeudaRepository.findAllByCarteraId(idC);

    List<CriterioMSDeudaImpagada> deudas = new ArrayList<>();
    if (criterios.getDeudaImpagada() == null || !criterios.getDeudaImpagada()) deudas.add(null);
    else deudas = criterioMSDeudaImpagadaRepository.findAllByCarteraId(idC);

    List<CriterioMSRemanente> remanentes = new ArrayList<>();
    if (criterios.getRemanente() == null || !criterios.getRemanente()) remanentes.add(null);
    else remanentes = criterioMSRemanenteRepository.findAllByCarteraId(idC);

    List<CriterioMSRiesgoTotal> riesgos = new ArrayList<>();
    if (criterios.getRiesgoTotal() == null || !criterios.getRiesgoTotal()) riesgos.add(null);
    else riesgos = criterioMSRiesgoTotalRepository.findAllByCarteraId(idC);

    List<Expediente> expedientes = expedienteRepository.findAllByCarteraId(input.getCartera());

    for (Producto p : productos){
      for (ClasificacionContable cc : clasificaciones){
        for (int adId = 1; adId <= fechas.size(); adId ++){
          CriterioMSAntiguedadDeuda cmsADMin = null;
          CriterioMSAntiguedadDeuda cmsADMax = null;
          Integer adMin = null;
          Integer adMax = null;

          if (adId == 0){
            cmsADMax = fechas.get(adId);
          } else if (adId == fechas.size()){
            cmsADMin = fechas.get(adId - 1);
          } else {
            cmsADMax = fechas.get(adId);
            cmsADMin = fechas.get(adId - 1);
          }

          if (cmsADMin != null){
            adMin = Integer.parseInt(cmsADMin.getValor());
          }
          if (cmsADMax != null){
            adMax = Integer.parseInt(cmsADMax.getValor());
          }

          for (int diId = 1; diId <= deudas.size(); diId ++){
            CriterioMSDeudaImpagada cmsDIMin = null;
            CriterioMSDeudaImpagada cmsDIMax = null;
            Double diMin = null;
            Double diMax = null;

            if (diId == 0){
              cmsDIMax = deudas.get(diId);
            } else if (diId == deudas.size()){
              cmsDIMin = deudas.get(diId - 1);
            } else {
              cmsDIMax = deudas.get(diId);
              cmsDIMin = deudas.get(diId - 1);
            }

            if (cmsDIMin != null){
              diMin = Double.parseDouble(cmsDIMin.getValor());
            }
            if (cmsDIMax != null){
              diMax = Double.parseDouble(cmsDIMax.getValor());
            }

            for (int rId = 1; rId <= remanentes.size(); rId ++){
              CriterioMSRemanente cmsRMin = null;
              CriterioMSRemanente cmsRMax = null;
              Double rMin = null;
              Double rMax = null;

              if (rId == 0){
                cmsRMax = remanentes.get(rId);
              } else if (rId == remanentes.size()){
                cmsRMin = remanentes.get(rId - 1);
              } else {
                cmsRMax = remanentes.get(rId);
                cmsRMin = remanentes.get(rId - 1);
              }

              if (cmsRMin != null){
                rMin = Double.parseDouble(cmsRMin.getValor());
              }
              if (cmsRMax != null){
                rMax = Double.parseDouble(cmsRMax.getValor());
              }

              for (int rtId = 1; rtId <= riesgos.size(); rtId ++){
                CriterioMSRiesgoTotal cmsRTMin = null;
                CriterioMSRiesgoTotal cmsRTMax = null;
                Double rtMin = null;
                Double rtMax = null;

                if (rtId == 0){
                  cmsRTMax = riesgos.get(rtId);
                } else if (rtId == riesgos.size()){
                  cmsRTMin = riesgos.get(rtId - 1);
                } else {
                  cmsRTMax = riesgos.get(rtId);
                  cmsRTMin = riesgos.get(rtId - 1);
                }

                if (cmsRTMin != null){
                  rtMin = Double.parseDouble(cmsRTMin.getValor());
                }
                if (cmsRTMax != null){
                  rtMax = Double.parseDouble(cmsRTMax.getValor());
                }
                expedientes = masterServicingUtil.filtrarExpedientes(
                  false,
                  idC, null,
                  p != null ? p.getId() : null,
                  cc != null ? cc.getId() : null,
                  adMin, adMax,
                  diMin, diMax,
                  rMin, rMax,
                  rtMin, rtMax,
                  true);

                if (!expedientes.isEmpty())
                  dividirYAsignar(expedientes, input.getPorcentajes(), logg);
              }
            }
          }
        }
      }
    }

    //return masterServicingMapper.getOutput(input.getCartera());
    return true;
  }

  private void dividirYAsignar(List<Expediente> expedientes, List<PorcentageAgenciaInputDTO> porcentages, Usuario logg) throws Exception {
    Integer suma = 0;
    for (int i = 0; i < porcentages.size(); i++){
      PorcentageAgenciaInputDTO pa = porcentages.get(i);
      AgenciaMasterServicing agencia = agenciaMasterServicingRepository.findById(pa.getAgencia()).orElseThrow(() ->
        new NotFoundException("AgenciaMasterServicing", pa.getAgencia()));

      Integer nExp = (int) Math.ceil(expedientes.size() * pa.getPorcentaje() / 100);
      List<Expediente> expList;
      if (i + 1 == porcentages.size()) expList = expedientes.stream().skip(suma).collect(Collectors.toList());
      else expList = expedientes.stream().skip(suma).limit(nExp).collect(Collectors.toList());
      suma += nExp;

      List<Usuario> usuarios = usuarioRepository.findByPerfilNombreAndAreas_NombreAndGrupoUsuarios_Nombre("Perfil consulta externo", "Master Servicing", agencia.getValor());
      if (!usuarios.isEmpty()){
        List<Integer> exPIds = expList.stream().map(Expediente::getId).collect(Collectors.toList());
        for (Usuario usu : usuarios){
          asignacionExpedienteUseCase.modificarGestor(exPIds, null, usu.getId(), false, false, false, new BusquedaDto(), logg);
        }
      }

      for (Expediente e : expList){
        e.setAgencia(agencia);
        expedienteRepository.save(e);
      }
    }
  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findSimpleExcel(Integer id, Integer agencia, Boolean todos) throws Exception {
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expedientes = new ArrayList<>();
    expedientes.add(MasterServicingExcel.cabeceras);

    List<Expediente> exList;
    if (todos != null && todos){
      exList = masterServicingUtil.filtrarExpedientes(false, id, null,
        null, null, null, null, null, null, null,
        null, null, null, false);
    } else {
      exList = masterServicingUtil.filtrarExpedientes(true, id, agencia,
        null, null, null, null, null, null, null,
        null, null, null, false);
    }

    for (Expediente ex : exList){
      expedientes.add(new MasterServicingExcel(ex).getValuesList());
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("Connection", expedientes);
    } else {
      datos.put("Expediente", expedientes);
    }

    return datos;
  }

  @Override
  public CarteraDTOToList cargaSimple(MultipartFile file, Usuario user) throws Exception {
    Usuario logg = usuarioRepository.findById(user.getId()).orElseThrow(() ->
      new NotFoundException("Usuario", user.getId()));
    CarteraDTOToList result = null;
    MasterServicingSeparado separado = masterServicingUtil.obtenerExpedientes(file);
    List<Expediente> expedientes = separado.getAll();
    masterServicingUtil.asignarUsuario(separado, logg);
    if (!expedientes.isEmpty()) result = new CarteraDTOToList(expedientes.get(0).getCartera());
    expedienteRepository.saveAll(expedientes);
    return result;
  }

  @Override
  public MasterServicingOutputDTO getDTO(Integer idCartera) throws NotFoundException {
    MasterServicingOutputDTO result = masterServicingMapper.getOutput(idCartera);
    return result;
  }

  @Override
  public ExpedientesMSOutputDTO getOutput(Integer idCartera){
    ExpedientesMSOutputDTO result = null;
    List<Expediente> expedientes = expedienteRepository.findAllByCarteraIdAndAsignacionesUsuarioEmailAndAgenciaIsNull(idCartera, "masterServicing@ms.com");
    result = masterServicingMapper.getOutput(expedientes, null, 100.0);
    return result;
  }

  @Override
  public ListWithCountDTO<ExpedienteDTOToList> getLista(
      Integer idCartera, Integer idAgencia, Integer size, Integer page) {

    List<Expediente> expedientes =
        masterServicingUtil.filtrarExpedientes(
            true, idCartera, idAgencia, null, null, null, null,
          null,null, null,null, null, null, false);
    List<Expediente> paged =
        expedientes.stream().skip(size * page).limit(size).collect(Collectors.toList());
    List<ExpedienteDTOToList> result =
        paged.stream().map(expedienteMapper::parseDTOToList).collect(Collectors.toList());
    return new ListWithCountDTO<>(result, expedientes.size());
  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findComplexExcel(Integer id, Integer agencia, Boolean todos) throws Exception {
    var datos = masterServicingUtil.obtenerCabeceras();
    List<Expediente> expedientes;
    if (todos != null && todos){
      expedientes = masterServicingUtil.filtrarExpedientes(false, id, null,
        null, null, null, null, null, null, null,
        null, null, null, false);
    } else {
      expedientes = masterServicingUtil.filtrarExpedientes(true, id, agencia,
        null, null, null, null, null, null, null,
        null, null, null, false);
    }

    masterServicingUtil.addForExcel(expedientes, datos);
    return datos;
  }

  @Override
  public Boolean cargaCompleja(MultipartFile file, Usuario user) throws Exception {
    List<EventoInputDTO> eventos = masterServicingUtil.obtenerEventos(file);
    Usuario emisor = usuarioRepository.findById(user.getId()).orElseThrow(() ->
      new NotFoundException("Usuario", user.getId()));
    for (EventoInputDTO input : eventos){
      eventoUseCase.create(input, emisor);
    }
    return true;
  }

  @Override
  public ListWithCountDTO<CatalogoMSOutputDTO> getCatalogos(String nombre, Integer cartera, Boolean activo, Integer size, Integer page) {
    List<CatalogoMSOutputDTO> result = new ArrayList<>();
    Integer tamano = 0;
    switch (nombre){
      case "Criterio Master Servicing Antiguedad Deuda":
        List<CriterioMSAntiguedadDeuda> cAD;
        if (activo) cAD = criterioMSAntiguedadDeudaRepository.findAllByCarteraIdAndActivoIsTrue(cartera);
        else  cAD = criterioMSAntiguedadDeudaRepository.findAllByCarteraId(cartera);
        tamano = cAD.size();
        result = cAD.stream()
            .skip(size * page)
            .limit(size)
            .map(CatalogoMSOutputDTO::new)
            .collect(Collectors.toList());
        break;
      case "Criterio Master Servicing Deuda Impagada":
        List<CriterioMSDeudaImpagada> cDI;
        if (activo) cDI = criterioMSDeudaImpagadaRepository.findAllByCarteraIdAndActivoIsTrue(cartera);
        else  cDI = criterioMSDeudaImpagadaRepository.findAllByCarteraId(cartera);
        tamano = cDI.size();
        result = cDI.stream()
            .skip(size * page)
            .limit(size)
            .map(CatalogoMSOutputDTO::new)
            .collect(Collectors.toList());
        break;
      case "Criterio Master Servicing Remanente":
        List<CriterioMSRemanente> cR;
        if (activo) cR = criterioMSRemanenteRepository.findAllByCarteraIdAndActivoIsTrue(cartera);
        else  cR = criterioMSRemanenteRepository.findAllByCarteraId(cartera);
        tamano = cR.size();
        result = cR.stream()
            .skip(size * page)
            .limit(size)
            .map(CatalogoMSOutputDTO::new)
            .collect(Collectors.toList());
        break;
      case "Criterio Master Servicing Riesgo Total":
        List<CriterioMSRiesgoTotal> cRT;
        if (activo) cRT = criterioMSRiesgoTotalRepository.findAllByCarteraIdAndActivoIsTrue(cartera);
        else  cRT = criterioMSRiesgoTotalRepository.findAllByCarteraId(cartera);
        tamano = cRT.size();
        result = cRT.stream()
            .skip(size * page)
            .limit(size)
            .map(CatalogoMSOutputDTO::new)
            .collect(Collectors.toList());
        break;
    }

    return  new ListWithCountDTO<CatalogoMSOutputDTO>(result, tamano);
  }

  @Override
  public void actualizarCatalogos(String nombre, List<CatalogoMSInputDTO> inputs) throws NotFoundException, RequiredValueException {
    switch (nombre){
      case "Criterio Master Servicing Antiguedad Deuda":
        actualizarAD(inputs);
        break;
      case "Criterio Master Servicing Deuda Impagada":
        actualizarDI(inputs);
        break;
      case "Criterio Master Servicing Remanente":
        actualizarR(inputs);
        break;
      case "Criterio Master Servicing Riesgo Total":
        actualizarRT(inputs);
        break;
    }
  }

  private void actualizarAD(List<CatalogoMSInputDTO> inputs) throws NotFoundException, RequiredValueException {
    for (CatalogoMSInputDTO in : inputs){
      CriterioMSAntiguedadDeuda cMSAD;
      if (in.getId() == null)
        cMSAD = new CriterioMSAntiguedadDeuda();
      else cMSAD = criterioMSAntiguedadDeudaRepository.findById(in.getId()).orElseThrow(() ->
        new NotFoundException("CriterioMSAntiguedadDeuda", in.getId()));

      cMSAD.setActivo(in.getActivo());
      cMSAD.setCodigo(in.getCodigo());
      cMSAD.setValor(in.getValor());

      if (in.getIdCartera() == null) throw new RequiredValueException("idCartera");
      else {
        Cartera c = carteraRepository.findById(in.getIdCartera()).orElseThrow(() ->
          new NotFoundException("Cartera", in.getIdCartera()));
        cMSAD.setCartera(c);
      }
      criterioMSAntiguedadDeudaRepository.save(cMSAD);
    }
  }

  private void actualizarDI(List<CatalogoMSInputDTO> inputs) throws NotFoundException, RequiredValueException {
    for (CatalogoMSInputDTO in : inputs){
      CriterioMSDeudaImpagada cMSDI;
      if (in.getId() == null)
        cMSDI = new CriterioMSDeudaImpagada();
      else cMSDI = criterioMSDeudaImpagadaRepository.findById(in.getId()).orElseThrow(() ->
        new NotFoundException("CriterioMSDeudaImpagada", in.getId()));

      cMSDI.setActivo(in.getActivo());
      cMSDI.setCodigo(in.getCodigo());
      cMSDI.setValor(in.getValor());

      if (in.getIdCartera() == null) throw new RequiredValueException("idCartera");
      else {
        Cartera c = carteraRepository.findById(in.getIdCartera()).orElseThrow(() ->
          new NotFoundException("Cartera", in.getIdCartera()));
        cMSDI.setCartera(c);
      }
      criterioMSDeudaImpagadaRepository.save(cMSDI);
    }
  }

  private void actualizarR(List<CatalogoMSInputDTO> inputs) throws NotFoundException, RequiredValueException {
    for (CatalogoMSInputDTO in : inputs){
      CriterioMSRemanente cMSR;
      if (in.getId() == null)
        cMSR = new CriterioMSRemanente();
      else cMSR = criterioMSRemanenteRepository.findById(in.getId()).orElseThrow(() ->
        new NotFoundException("CriterioMSRemanente", in.getId()));

      cMSR.setActivo(in.getActivo());
      cMSR.setCodigo(in.getCodigo());
      cMSR.setValor(in.getValor());

      if (in.getIdCartera() == null) throw new RequiredValueException("idCartera");
      else {
        Cartera c = carteraRepository.findById(in.getIdCartera()).orElseThrow(() ->
          new NotFoundException("Cartera", in.getIdCartera()));
        cMSR.setCartera(c);
      }
      criterioMSRemanenteRepository.save(cMSR);
    }
  }

  private void actualizarRT(List<CatalogoMSInputDTO> inputs) throws NotFoundException, RequiredValueException {
    for (CatalogoMSInputDTO in : inputs){
      CriterioMSRiesgoTotal cMSR;
      if (in.getId() == null)
        cMSR = new CriterioMSRiesgoTotal();
      else cMSR = criterioMSRiesgoTotalRepository.findById(in.getId()).orElseThrow(() ->
        new NotFoundException("CriterioMSRemanente", in.getId()));

      cMSR.setActivo(in.getActivo());
      cMSR.setCodigo(in.getCodigo());
      cMSR.setValor(in.getValor());

      if (in.getIdCartera() == null) throw new RequiredValueException("idCartera");
      else {
        Cartera c = carteraRepository.findById(in.getIdCartera()).orElseThrow(() ->
          new NotFoundException("Cartera", in.getIdCartera()));
        cMSR.setCartera(c);
      }
      criterioMSRiesgoTotalRepository.save(cMSR);
    }
  }

  public ByteArrayInputStream create(LinkedHashMap<String, List<Collection<?>>> datos) throws IOException {
    Workbook workbook = new XSSFWorkbook();

    CellStyle cellStyleDate = workbook.createCellStyle();
    cellStyleDate.setDataFormat((short)14);


    CellStyle styleHeader = workbook.createCellStyle();
    Font font = workbook.createFont();
    font.setBold(true);
    font.setColor(IndexedColors.WHITE.getIndex());
    styleHeader.setFont(font);
    //styleHeader.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
    byte[] rgb = new byte[3];
    rgb[0] = (byte) 10; // red
    rgb[1] = (byte) 148; // green
    rgb[2] = (byte) 214; // blue
    //create XSSFColor
    XSSFColor color = new XSSFColor(rgb, new DefaultIndexedColorMap());
    XSSFCellStyle xssfcellcolorstyle = (XSSFCellStyle) styleHeader;
    xssfcellcolorstyle.setFillForegroundColor(color);
    xssfcellcolorstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

    Integer filaTipo = null;

    for (var datosHoja : datos.entrySet()) {
      Sheet sheet = workbook.createSheet(datosHoja.getKey());
      int indiceFila = 0;
      for (Collection<?> datosFila : datosHoja.getValue()) {
        Row filaExcel = sheet.createRow(indiceFila);
        int indiceColumna = 0;
        for (Object dato : datosFila) {
          Cell celdaExcel = filaExcel.createCell(indiceColumna);
          if (datosHoja.getKey().equals("Catálogos") && filaTipo == null && dato instanceof String &&
            ((String) dato).contains("Tipo Evento")){
            filaTipo = indiceFila;
          }
          if (indiceFila == 0 || (filaTipo != null && indiceFila == filaTipo)) {
            celdaExcel.setCellStyle(xssfcellcolorstyle);
            sheet.setColumnWidth(indiceColumna, 15 * 256);
          }

          if (dato != null) {
            if (dato instanceof Date) {
              celdaExcel.setCellStyle(cellStyleDate);
              celdaExcel.setCellValue((Date) dato);
            } else if (dato instanceof Boolean) {
              if (dato == Boolean.TRUE) {
                celdaExcel.setCellValue("Sí");
              } else if (dato == Boolean.FALSE) {
                celdaExcel.setCellValue("No");
              }
            } else if (dato instanceof Number) {
              celdaExcel.setCellValue(((Number) dato).doubleValue());
            } else if (dato instanceof Catalogo) {
              celdaExcel.setCellValue(((Catalogo) dato).getValor());
            } else if (dato.equals("")) {
              celdaExcel.setCellValue("-");
            } else {
              celdaExcel.setCellValue(dato.toString());
            }
          } else {
            celdaExcel.setCellValue("-");
          }
          indiceColumna++;
        }
        indiceFila++;
      }
    }

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      workbook.write(outputStream);
      return new ByteArrayInputStream(outputStream.toByteArray());
    }
  }
}
