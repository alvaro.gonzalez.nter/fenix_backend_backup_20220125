package com.haya.alaska.grupo_usuario.infrastructure.repository;

import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GrupoUsuarioRepository extends JpaRepository<GrupoUsuario, Integer> {
  List<GrupoUsuario> findByAreaUsuarioId(Integer idArea);
  List<GrupoUsuario> findAllByAreaUsuarioIsNull();
  List<GrupoUsuario> findByUsuariosId(Integer idUsuario);
  void deleteById(Integer id);
}
