package com.haya.alaska.grupo_usuario.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_GRUPO_USUARIO")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_GRUPO_USUARIO")
public class GrupoUsuario {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "DES_NOMBRE")
  private String nombre;

  @ManyToMany
  @JoinTable(name = "RELA_USUARIO_GRUPO_USUARIO",
    joinColumns = { @JoinColumn(name = "ID_GRUPO_USUARIO") },
    inverseJoinColumns = { @JoinColumn(name = "ID_USUARIO") }
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_USUARIO_GRUPO_USUARIO")
  private Set<Usuario> usuarios = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AREA_USUARIO")
  private AreaUsuario areaUsuario;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_GRUPO_USUARIO")
  @NotAudited
  private Set<BusinessPlanEstrategia> businessPlanEstrategias = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_GRUPO")
  @NotAudited
  private Set<Segmentacion> segmentacion = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_GRUPO")
  @NotAudited
  private Set<SegmentacionPN> segmentacionPN = new HashSet<>();
}
