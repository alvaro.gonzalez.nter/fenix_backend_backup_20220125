package com.haya.alaska.fecha_reo_conversion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.fecha_reo_conversion.domain.FechaReoConversion;
import org.springframework.stereotype.Repository;

@Repository
public interface FechaReoConversionRepository extends CatalogoRepository<FechaReoConversion, Integer> {}
