package com.haya.alaska.subtipo_carga_formalizacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Audited
@AuditTable(value = "HIST_LKUP_SUBTIPO_CARGA_FORMALIZACION")
@Entity
@Getter
@Setter
@Table(name = "LKUP_SUBTIPO_CARGA_FORMALIZACION")
public class SubtipoCargaFormalizacion extends Catalogo {

}
