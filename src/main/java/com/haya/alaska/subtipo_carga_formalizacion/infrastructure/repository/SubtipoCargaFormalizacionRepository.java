package com.haya.alaska.subtipo_carga_formalizacion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.subtipo_carga_formalizacion.domain.SubtipoCargaFormalizacion;
import org.springframework.stereotype.Repository;

@Repository
public interface SubtipoCargaFormalizacionRepository extends CatalogoRepository<SubtipoCargaFormalizacion, Integer> {
}
