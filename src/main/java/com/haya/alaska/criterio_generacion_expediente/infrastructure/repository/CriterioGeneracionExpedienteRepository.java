package com.haya.alaska.criterio_generacion_expediente.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.criterio_generacion_expediente.domain.CriterioGeneracionExpediente;

@Repository
public interface CriterioGeneracionExpedienteRepository
    extends CatalogoRepository<CriterioGeneracionExpediente, Integer> {}
