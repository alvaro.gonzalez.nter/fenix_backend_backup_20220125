package com.haya.alaska.criterio_generacion_expediente.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.domain.Catalogo;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_CRITERIO_GENERACION_EXPEDIENTE")
@Entity
@Table(name = "LKUP_CRITERIO_GENERACION_EXPEDIENTE")
public class CriterioGeneracionExpediente extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CRITERIO_GENERACION_EXPEDIENTE")
  @NotAudited
  private Set<Cartera> carteras = new HashSet<>();
}
