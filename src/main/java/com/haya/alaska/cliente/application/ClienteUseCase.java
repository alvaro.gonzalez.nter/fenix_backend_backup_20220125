package com.haya.alaska.cliente.application;

import com.haya.alaska.cartera.infrastructure.controller.dto.ClienteOutputDTO;
import com.haya.alaska.cliente.infrastructure.controller.dto.ClienteCarteraDTO;
import com.haya.alaska.grupo_cliente.domain.GrupoCliente;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.List;


public interface ClienteUseCase {

  public List<ClienteOutputDTO> getAll(Usuario user, Boolean asignados) throws NotFoundException;

  public List<ClienteCarteraDTO> findClientesByIdInterviniente(Integer idInterviniente) throws NotFoundException;

  List<ClienteOutputDTO> getClientesByUser(Usuario user);

  List<ClienteOutputDTO> getClientesByUserAlways(Usuario user);

  List<ClienteOutputDTO> getClientesByGrupoAndUserAlways(GrupoCliente grupo, Usuario user);

  List<ClienteOutputDTO> getClientesFormalizacion();
}
