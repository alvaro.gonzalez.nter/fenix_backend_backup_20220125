package com.haya.alaska.cliente.application;

import com.haya.alaska.asignacion_usuario_cartera.domain.AsignacionUsuarioCartera;
import com.haya.alaska.cartera.application.CarteraUseCase;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraOutputDTO;
import com.haya.alaska.cartera.infrastructure.controller.dto.ClienteOutputDTO;
import com.haya.alaska.cartera.infrastructure.mapper.CarteraMapper;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.infrastructure.controller.dto.ClienteCarteraDTO;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.grupo_cliente.domain.GrupoCliente;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClienteUseCaseImpl implements ClienteUseCase{

  @Autowired ClienteRepository clienteRepository;
  @Autowired
  CarteraUseCase carteraUseCase;
  @Autowired
  IntervinienteRepository intervinienteRepository;
  @Autowired
  CarteraMapper carteraMapper;

  @Override
  public List<ClienteOutputDTO> getAll(Usuario user, Boolean asignados) throws NotFoundException {
    List<ClienteOutputDTO> result;
    if (user.esAdministrador() || user.esResponsableFormalizacion())
      result = ClienteOutputDTO.newList(clienteRepository.findAll());
    else if (asignados) {
      List<Cartera> ccs = user.getAsignacionesUsuarioCartera().stream().map(AsignacionUsuarioCartera::getCartera).collect(Collectors.toList());
      List<Cliente> clientes = new ArrayList<>();
      for (Cartera ca : ccs) {
        List<ClienteCartera> clienteCarteras = new ArrayList<>(ca.getClientes());
        for (ClienteCartera cc : clienteCarteras)
          clientes.add(cc.getCliente());
      }
      result = ClienteOutputDTO.newList(clientes.stream().distinct().collect(Collectors.toList()));
    } else {
      List<CarteraDTOToList> carteras = carteraUseCase.getCarteras(user);
      List<Integer> clientes =
          carteras.stream()
              .map(CarteraDTOToList::getCliente)
              .distinct()
              .collect(Collectors.toList());
      result = ClienteOutputDTO.newList(clienteRepository.findAllById(clientes));
    }
    return result;
  }

  @Override
  public List<ClienteCarteraDTO> findClientesByIdInterviniente(Integer idInterviniente) throws NotFoundException {
    Interviniente interviniente =
      intervinienteRepository
        .findById(idInterviniente)
        .orElseThrow(
          () ->
            new NotFoundException(
              "interviniente", idInterviniente));

   List<ContratoInterviniente>contratoIntervinienteList=interviniente.getContratos().stream().collect(Collectors.toList());
   List<Contrato>contratoList=new ArrayList<>();
   List<Cartera>carteraList=new ArrayList<>();
   List<ClienteCartera>clienteCarteraList=new ArrayList<>();

   for (ContratoInterviniente contratoInterviniente:contratoIntervinienteList) {
      contratoList.add(contratoInterviniente.getContrato());
    }
    List<Expediente>expedienteList = contratoList.stream().filter(contrato -> {if (contrato!=null){
    return true;}
    return false;
    }).map(Contrato::getExpediente).distinct().collect(Collectors.toList());
    for (Expediente expediente :expedienteList) {
    carteraList.add(expediente.getCartera());
    }
    for(Cartera cartera:carteraList){
      clienteCarteraList.addAll(cartera.getClientes());
    }
 //   clienteCarteraList=clienteCarteraList.stream().distinct().collect(Collectors.toList());
    return ClienteCarteraDTO.newList(clienteCarteraList);
  }

  @Override
  public List<ClienteOutputDTO> getClientesByUser(Usuario user){
    List<ClienteOutputDTO> result;
    List<CarteraDTOToList> carteras = carteraUseCase.getCarteraByUser(user);
    List<Integer> clientes = carteras.stream().map(CarteraDTOToList::getCliente).distinct().collect(Collectors.toList());
    result = ClienteOutputDTO.newList(clienteRepository.findAllById(clientes));
    return result;
  }

  @Override
  public List<ClienteOutputDTO> getClientesByUserAlways(Usuario user){
    List<ClienteOutputDTO> result;
    List<CarteraDTOToList> carteras = carteraUseCase.getCarterasByUserAlways(user);
    List<Integer> clientes = carteras.stream().map(CarteraDTOToList::getCliente).distinct().collect(Collectors.toList());
    result = ClienteOutputDTO.newList(clienteRepository.findAllById(clientes));
    return result;
  }

  public List<ClienteOutputDTO> getClientesByGrupoAndUserAlways(GrupoCliente grupo, Usuario user) {
    List<Cliente> clientes = clienteRepository.findAllByGrupo(grupo);
    List<Integer> idClientes = clientes.stream().map(cliente -> cliente.getId()).collect(Collectors.toList());
    List<ClienteOutputDTO> result;
    List<CarteraDTOToList> carteras = carteraUseCase.getCarterasByUserAlways(user);
    List<Integer> clientes2 = carteras.stream().filter(cartera -> idClientes.contains(cartera.getCliente())).map(CarteraDTOToList::getCliente).distinct().collect(Collectors.toList());
    result = ClienteOutputDTO.newList(clienteRepository.findAllById(clientes2));
    return result;
  }

  @Override
  public List<ClienteOutputDTO> getClientesFormalizacion(){
    List<Cliente> clientes = clienteRepository.findAllByCarterasCarteraFormalizacionIsTrue();
    List<ClienteOutputDTO> result = ClienteOutputDTO.newList(clientes);
    return result;
  }
}
