package com.haya.alaska.cliente.infrastructure.repository;

import com.haya.alaska.grupo_cliente.domain.GrupoCliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.cliente.domain.Cliente;

import java.util.List;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
  Cliente findByNombre(String nombre);
  List<Cliente> findAllByGrupo(GrupoCliente grupo);
  List<Cliente> findAllByCarterasCarteraFormalizacionIsTrue();
}
