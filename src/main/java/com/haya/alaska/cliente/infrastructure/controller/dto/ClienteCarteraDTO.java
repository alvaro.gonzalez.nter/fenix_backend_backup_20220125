package com.haya.alaska.cliente.infrastructure.controller.dto;

import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ClienteCarteraDTO {
  //El id hace referencia al idCliente y no al de la relación
  Integer id;
  String nombre;

  public ClienteCarteraDTO(ClienteCartera clienteCartera) {
    this.id = clienteCartera.getCliente().getId();
    this.nombre = clienteCartera.getCiaPropiedadCartera();
  }

  public static List<ClienteCarteraDTO> newList(List<ClienteCartera> clienteCarteraList) {
    return clienteCarteraList.stream().map(ClienteCarteraDTO::new).collect(Collectors.toList());
  }

}
