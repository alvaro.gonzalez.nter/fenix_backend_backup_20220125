package com.haya.alaska.cliente.infrastructure.controller;

import com.haya.alaska.cartera.infrastructure.controller.dto.ClienteOutputDTO;
import com.haya.alaska.cliente.application.ClienteUseCase;
import com.haya.alaska.cliente.infrastructure.controller.dto.ClienteCarteraDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/clientes")
public class ClienteController {

  @Autowired
  private ClienteUseCase clienteUseCase;

  @ApiOperation(value = "Listado de clientes",
    notes = "Devuelve todos los clientes registrados en bbdd")
  @GetMapping
  public List<ClienteOutputDTO> getAll(@ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return clienteUseCase.getAll(usuario,false);
  }

  @ApiOperation(value = "Listado de clientes cuyas carteras tiene asignacion en el usuario",
    notes = "Devuelve todos los clientes registrados en bbdd")
  @GetMapping("/asignados")
  public List<ClienteOutputDTO> getAllAsignados(@ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return clienteUseCase.getAll(usuario,true);
  }

  @ApiOperation(value = "Listado de clientes de formalizacion",
    notes = "Devuelve los clientes registrados en bbdd con carteras de formalización")
  @GetMapping("/formalizacion")
  public List<ClienteOutputDTO> getFormalizacion(@ApiIgnore CustomUserDetails principal) {
    return clienteUseCase.getClientesFormalizacion();
  }

  @ApiOperation(value = "Listado de clientes por usuario",
    notes = "Devuelve todos los clientes registrados en función del id del usuario.")
  @GetMapping("/usuario")
  public List<ClienteOutputDTO> getClientesByUsuario(
    @ApiIgnore CustomUserDetails principal){
    Usuario usuario = (Usuario) principal.getPrincipal();
    return clienteUseCase.getClientesByUserAlways(usuario);
  }

  @ApiOperation(value = "Listado de clientes por idInterviniente",
    notes = "Devuelve todos los clientes registrados en bbdd por idInterviniente")
  @GetMapping("{intervinienteId}")
  public List<ClienteCarteraDTO> findClientesByIdInterviniente(@PathVariable Integer intervinienteId) throws NotFoundException {
    return clienteUseCase.findClientesByIdInterviniente(intervinienteId);
  }
}
