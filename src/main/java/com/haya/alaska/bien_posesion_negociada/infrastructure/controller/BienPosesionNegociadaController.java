package com.haya.alaska.bien_posesion_negociada.infrastructure.controller;

import com.haya.alaska.bien_posesion_negociada.application.BienPosesionNegociadaUseCase;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.*;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientePosesionNegociada/{idExpediente}/bien")
public class BienPosesionNegociadaController {

  @Autowired
  private BienPosesionNegociadaUseCase bienPosesionNegociadaUseCase;

  @ApiOperation(value = "Listado bienes de posesión negociada", notes = "Devuelve todas los bienes registradas relacionados con el expediente de posesión negociada enviado")
  @GetMapping
  public ListWithCountDTO<BienPosesionNegociadaDTOToList> getAllOcupantes(@PathVariable("idExpediente") Integer idExpediente, @RequestParam(value = "id", required = false) Integer id, @RequestParam(value = "estadoOcupacion", required = false) String estadoOcupacion, @RequestParam(value = "finca", required = false) String fincaRegistral, @RequestParam(value = "localidad", required = false) String localidad, @RequestParam(value = "provincia", required = false) String provincia, @RequestParam(value = "direccion", required = false) String direccion, @RequestParam(value = "valor", required = false) String valor, @RequestParam(value = "modoGestion", required = false) String modoGestion, @RequestParam(value = "orderField", required = false) String orderField, @RequestParam(value = "orderDirection", required = false) String orderDirection, @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) {

    return bienPosesionNegociadaUseCase.getAllFilteredBienes(idExpediente, id, estadoOcupacion, fincaRegistral, localidad, provincia, direccion, valor, modoGestion, orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Obtener bien", notes = "Devuelve el bien con id enviado")
  @GetMapping("/{idBienPosesionNegociada}")
  public BienPosesionNegociadaOutputDto getBienById(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBien) throws IllegalAccessException, NotFoundException {
    return bienPosesionNegociadaUseCase.findBienPosesionNegociadaByIdDto(idExpediente, idBien);
  }

  @ApiOperation(value = "Obtener bien", notes = "Devuelve el bien con id enviado")
  @GetMapping("/movil/{idBienPosesionNegociada}")
  public BienPosesionNegociadaExtendedOutputDto getBienByIdMovil(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBien) throws IllegalAccessException, NotFoundException {
    return bienPosesionNegociadaUseCase.findBienPosesionNegociadaByIdMovilDto(idExpediente, idBien);
  }

  @ApiOperation(value = "Actualizar bien", notes = "Actualizar el bien con id enviado")
  @PutMapping("/{idBienPosesionNegociada}")
  @Transactional(rollbackFor = Exception.class)
  public BienPosesionNegociadaOutputDto updateBien(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBienPosesionNegociada, @RequestBody @Valid BienPosesionNegociadaInputDto bienInputDTO) throws Exception {
    return bienPosesionNegociadaUseCase.update(idExpediente, idBienPosesionNegociada, bienInputDTO);
  }

  @ApiOperation(value = "Obtener resumen de ocupacion", notes = "Devuelve el resumen de los datos de ocupacion")
  @GetMapping("/{idBienPosesionNegociada}/resumen_ocupacion")
  public ResumenOcupacionOutputDTO getResumenOcupacion(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBienPN, @RequestParam Set<Integer> ocupantesSeleccionados) throws IllegalAccessException, NotFoundException {
    return bienPosesionNegociadaUseCase.getResumenOcupacion(idBienPN, ocupantesSeleccionados);
  }


  @ApiOperation(value = "Obtener resumen de informacion judicial", notes = "Devuelve el resumen de los datos judiciales")
  @GetMapping("/{idBienPosesionNegociada}/resumen_judicial")
  public ResumenInformacionJudicialOutputDTO getResumenJudicial(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBienPN) throws IllegalAccessException, NotFoundException {
    return bienPosesionNegociadaUseCase.getResumenInformacionJudicial(idExpediente, idBienPN);
  }

  @ApiOperation(value = "Todos los bienes posesion negociada de los Expedientes", notes = "Obtiene todos los bienes posesion negociada de cada expediente.")
  @GetMapping("/many")
  public List<BienPosesionNegociadaDTOToList> getManyBienesPN(@RequestParam List<Integer> expedientesPN) throws IOException {
    return bienPosesionNegociadaUseCase.getAllBienesPosesionNegociada(expedientesPN);
  }

  @ApiOperation(value = "Añadir un documento a un bienPN")
  @PostMapping("/{idBienPN}/documentos")
  public void createDocumentoBienPN(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPN") Integer idBienPN, @RequestPart("file") @NotNull @NotBlank MultipartFile file, @RequestPart("metadatos") MetadatoDocumentoInputDTO metadatoDocumento, @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    bienPosesionNegociadaUseCase.createDocumento(idBienPN, file, metadatoDocumento, usuario);
  }

  @ApiOperation(value = "Obtener documentación de los bienes de un contrato", notes = "Devuelve la documentación disponible de los bienes del contrato con id enviado")
  @GetMapping("/{idBienPosesionNegociada}/documentos")
  @Transactional
  public ListWithCountDTO<DocumentoDTO> getDocumentosByExpediente(@PathVariable("idExpediente") Integer idExpediente, @RequestParam(value = "id", required = false) String idDocumento, @PathVariable("idBienPosesionNegociada") Integer idBienPN, @RequestParam(value = "nombreArchivo", required = false) String nombreArchivo, @RequestParam(value = "idHaya", required = false) String idHaya, @RequestParam(value = "usuarioCreador", required = false) String usuarioCreador, @RequestParam(value = "tipo", required = false) String tipo, @RequestParam(value = "idEntidad", required = false) String idEntidad, @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion, @RequestParam(value = "orderField", required = false) String orderField, @RequestParam(value = "orderDirection", required = false) String orderDirection, @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) throws NotFoundException, IOException {
    return bienPosesionNegociadaUseCase.findDocumentosByBienPNId(idBienPN, idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, nombreArchivo, idHaya, orderDirection, orderField, size, page);
  }

  @ApiOperation(value = "Devuelve los bienesPN asociados a un expediente")
  @GetMapping("/all")
  @ResponseStatus(HttpStatus.OK)
  public List<BienPosesionNegociadaDTOToList> getAllBienes(@PathVariable("idExpediente") Integer idExpediente) {
    List<BienPosesionNegociadaDTOToList> bienes = bienPosesionNegociadaUseCase.getAllByExpedientePNId(idExpediente);
    return bienes;
  }

  @PostMapping("/{idBienPosesionNegociada}/desasignarOcupantes")
  public void deleteByIds(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBienPosesionNegociada") Integer idBien, @RequestBody List<Integer> ids, @ApiIgnore CustomUserDetails principal) throws Exception {
    bienPosesionNegociadaUseCase.deleteByIds(idExpediente, idBien, ids, (Usuario) principal.getPrincipal());
  }
}
