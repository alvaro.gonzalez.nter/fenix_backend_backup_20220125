package com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ResumenInformacionJudicialOutputDTO {
  String tipoProcedimiento;
  String juzgado;
  String autos;
  Date fechaSubasta;
  Date decreto;
  Boolean testimonio;
  Boolean oposicion;
  Boolean lanzamiento;
}
