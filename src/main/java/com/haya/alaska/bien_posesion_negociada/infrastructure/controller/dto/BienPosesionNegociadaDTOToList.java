package com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto;

import org.springframework.beans.BeanUtils;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.i18n.LocaleContextHolder;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienPosesionNegociadaDTOToList {
  private Integer id;
  private String idHaya;
  private Integer idBien;
  private Boolean estadoTramitacion;
  private String estadoOcupacion;
  private String finca;
  private String referenciaCatastral;
  private Double responsabilidadHipotecaria;
  private String localidad;
  private String provincia;
  private String direccion;
  private String descripcion;
  private String tipoActivo;
  private Boolean posesionNegociada;
  private Double valor;
  private String situacion;
  private String campania;
  private Boolean solicitudAlquiler;
  private Double latitud;
  private Double longitud;
  private String modoGestion;

  public BienPosesionNegociadaDTOToList(BienPosesionNegociada bienPosesionNegociada) {
    BeanUtils.copyProperties(bienPosesionNegociada, this);
    this.setId(bienPosesionNegociada.getId());
    this.setIdHaya(bienPosesionNegociada.getIdHaya());
    this.setSituacion(bienPosesionNegociada.getSituacion() != null ? bienPosesionNegociada.getSituacion().getValor() : null);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      this.setEstadoOcupacion(bienPosesionNegociada.getEstadoOcupacion() != null ? bienPosesionNegociada.getEstadoOcupacion().getValorIngles() : null);
    } else{
      this.setEstadoOcupacion(bienPosesionNegociada.getEstadoOcupacion() != null ? bienPosesionNegociada.getEstadoOcupacion().getValor() : null);
    }


    if (bienPosesionNegociada.getBien() != null){
      Bien bien = bienPosesionNegociada.getBien();
      this.setEstadoTramitacion(bien.getTramitado());
      this.setFinca(bien.getFinca());
      this.setIdBien(bien.getId());
      this.setDireccion(bien.getNombreVia());
      this.setValor(bien.getImporteBien());
      this.setReferenciaCatastral(bien.getReferenciaCatastral());
      this.setPosesionNegociada(bien.getPosesionNegociada());
      this.setResponsabilidadHipotecaria(bien.getResponsabilidadHipotecaria());
      this.setLocalidad(bien.getLocalidad() != null ? bien.getLocalidad().getValor() : null);
      this.setProvincia(bien.getProvincia() != null ? bien.getProvincia().getValor() : null);

      if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")){
        this.setTipoActivo(bien.getTipoActivo() != null ? bien.getTipoActivo().getValorIngles(): null);
        this.setDescripcion(bien.getDescripcion());
      } else{
        this.setTipoActivo(bien.getTipoActivo() != null ? bien.getTipoActivo().getValor() : null);
        this.setDescripcion(bien.getDescripcion());

      }
    }
    String nombreCampania= bienPosesionNegociada.getCampaniasAsignadasPN().size() > 0 ? bienPosesionNegociada.getCampaniasAsignadasPN().stream().findFirst().get().getCampania().getNombre() : null;
    this.setCampania(nombreCampania);
    this.latitud = bienPosesionNegociada.getBien() != null ? bienPosesionNegociada.getBien().getLatitud() : null;
    this.longitud = bienPosesionNegociada.getBien() != null ? bienPosesionNegociada.getBien().getLongitud() : null;
    String modoGestionEstado = bienPosesionNegociada.getTipoEstado() != null ? bienPosesionNegociada.getTipoEstado().getValor() : null;
    String modoGestionSubEstado = bienPosesionNegociada.getSubEstado() != null ? bienPosesionNegociada.getSubEstado().getValor() : null;

    if (modoGestionSubEstado != null) {
      this.setModoGestion(modoGestionEstado + "/" + modoGestionSubEstado);
    } else {
      this.setModoGestion(modoGestionEstado);
    }
  }

}
