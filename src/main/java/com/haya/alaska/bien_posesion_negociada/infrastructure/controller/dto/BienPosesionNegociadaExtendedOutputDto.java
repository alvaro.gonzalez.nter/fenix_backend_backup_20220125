package com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_subastado.infrastructure.controller.dto.BienSubastadoDTOToList;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienPosesionNegociadaExtendedOutputDto extends BienPosesionNegociadaOutputDto {
  private Double latitud;
  private Double longitud;
  private List<Long> fotos;

  public void setFotos(BienPosesionNegociada bienPN)
  {
    List<Long> tempFotos = new ArrayList<>();
    if(bienPN == null) return;
    for (var visita : bienPN.getVisitas()) {
      for (var foto : visita.getFotos()) {
        tempFotos.add(foto);
      }
    }
    this.fotos = tempFotos;
  }
}
