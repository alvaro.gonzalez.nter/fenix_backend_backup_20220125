package com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto;

import com.haya.alaska.bien.infrastructure.controller.dto.BienOutputDto;
import lombok.*;

import java.io.Serializable;
@Getter
@Setter
@ToString
public class BienPosesionNegociadaDto extends BienOutputDto implements Serializable {
  private Integer idBienPosesionNegociada;
  private String direccion;
  private Double valor;
}
