package com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto;

import com.haya.alaska.bien_subastado.infrastructure.controller.dto.BienSubastadoDTOToList;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienPosesionNegociadaOutputDto extends BienPosesionNegociadaDto {

  private static final long serialVersionUID = 1L;

  private CatalogoMinInfoDTO estadoOcupacion;
  private CatalogoMinInfoDTO localidad;
  private CatalogoMinInfoDTO municipio;
  private List<BienSubastadoDTOToList> saneamiento;
  private Integer estrategiaAsignada;
  private CatalogoMinInfoDTO situacion;
  private Integer idCampania;
  private CatalogoMinInfoDTO tipoEstado;
  private CatalogoMinInfoDTO subtipoEstado;
}
