package com.haya.alaska.bien_posesion_negociada.infrastructure.repository;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface BienPosesionNegociadaRepository extends JpaRepository<BienPosesionNegociada, Integer> {
  public Optional<BienPosesionNegociada> findByBien(Bien bien);
  public Optional<BienPosesionNegociada> findByBienId(Integer idBien);
  Optional<BienPosesionNegociada> findByIdHaya(String idHaya);
  public List<BienPosesionNegociada>findAllByExpedientePosesionNegociadaId(Integer idexpedientePN);
  public List<BienPosesionNegociada>findAllByExpedientePosesionNegociadaIdIn(List<Integer> ids);
  List<BienPosesionNegociada> findAllByBienIdIn(List<Integer> ids);
  List<BienPosesionNegociada> findAllByIdIn(List<Integer> ids);

  @Query(
    value =
      "SELECT DISTINCT ch.ID as idContrato, rh.timestamp as fecha, rea.ID_ESTRATEGIA as idEstrategia "
        + "FROM HIST_MSTR_BIEN_POSESION_NEGOCIADA ch "
        + "INNER JOIN HIST_RELA_ESTRATEGIA_ASIGNADA rea ON ch.ID_ESTRATEGIA_ASIGNADA = rea.ID "
        + "INNER JOIN REGISTRO_HISTORICO rh ON rea.REV = rh.id "
        + "WHERE ch.ID_EXPEDIENTE_POSESION_NEGOCIADA = ?1 "
        + "AND ch.ID = ?3 "
        + "AND rh.timestamp <= ?2 "
        + "ORDER BY rh.timestamp DESC",
    nativeQuery = true)
  List<Map> findAllForEstrategiaLess(Integer idExpediente, Timestamp fecha, Integer idBien);

  @Query(
    value =
      "SELECT DISTINCT ch.ID as idContrato, rh.timestamp as fecha, rea.ID_ESTRATEGIA as idEstrategia "
        + "FROM HIST_MSTR_BIEN_POSESION_NEGOCIADA ch "
        + "INNER JOIN HIST_RELA_ESTRATEGIA_ASIGNADA rea ON ch.ID_ESTRATEGIA_ASIGNADA = rea.ID "
        + "INNER JOIN REGISTRO_HISTORICO rh ON rea.REV = rh.id "
        + "WHERE ch.ID_EXPEDIENTE_POSESION_NEGOCIADA = ?1 "
        + "AND ch.ID = ?3 "
        + "AND rh.timestamp > ?2 "
        + "ORDER BY rh.timestamp ASC",
    nativeQuery = true)
  List<Map> findAllForEstrategiaBigg(Integer idExpediente, Timestamp fecha, Integer idBien);
  List<BienPosesionNegociada>findAllByIdNotIn(List<Integer>ids);
  Optional<BienPosesionNegociada>findAllByIdHaya(String idHaya);

  //Métodos para segmentacion
  //Situacion
  Integer countAllBySituacionIdAndExpedientePosesionNegociadaIdIn(Integer isSituacion, List<Integer> idsExp);
  @Query(
      value =
          "SELECT SUM(B.IMPORTE_BIEN) "
              + "FROM MSTR_BIEN AS B "
              + "LEFT JOIN MSTR_BIEN_POSESION_NEGOCIADA AS BP ON BP.ID_BIEN = B.ID "
              + "WHERE BP.ID_SITUACION = ?1 "
              + "AND BP.ID_EXPEDIENTE_POSESION_NEGOCIADA IN ?2 ;",
      nativeQuery = true)
  Double sumAllBySituacion(Integer isSituacion, List<Integer> idsExp);
  //Modo
  Integer countAllByExpedientePosesionNegociadaIdIn(List<Integer> idsExp);
  @Query(
    value =
      "SELECT SUM(B.IMPORTE_BIEN) "
        + "FROM MSTR_BIEN AS B "
        + "LEFT JOIN MSTR_BIEN_POSESION_NEGOCIADA AS BP ON BP.ID_BIEN = B.ID "
        + "WHERE BP.ID_EXPEDIENTE_POSESION_NEGOCIADA IN ?1 ;",
    nativeQuery = true)
  Double sumAllByModo(List<Integer> idsExp);
  //Producto No Tiene
  //Judicial
  @Query(
    value =
      "    SELECT COUNT(C1.ID) " +
        "    FROM MSTR_BIEN_POSESION_NEGOCIADA AS C1 " +
        "    LEFT JOIN MSTR_PROCEDIMIENTO_POSESION_NEGOCIADA AS R ON C1.ID = R.ID_BIEN " +
        "    WHERE C1.ID_EXPEDIENTE_POSESION_NEGOCIADA IN ?1" +
        "    HAVING COUNT(R.ID) = 0 ",
    nativeQuery = true
  )
  Integer countAllByCountProcedimientosZero(List<Integer> idsExp);
  @Query(
    value =
      "    SELECT SUM(B.IMPORTE_BIEN) " +
        "    FROM MSTR_BIEN AS B " +
        "    LEFT JOIN MSTR_BIEN_POSESION_NEGOCIADA AS C1 ON C1.ID_BIEN = B.ID" +
        "    LEFT JOIN MSTR_PROCEDIMIENTO_POSESION_NEGOCIADA AS R ON C1.ID = R.ID_BIEN " +
        "    WHERE C1.ID_EXPEDIENTE_POSESION_NEGOCIADA IN ?1" +
        "    HAVING COUNT(R.ID) = 0 ",
    nativeQuery = true
  )
  Double sumAllByCountProcedimientosZero(List<Integer> idsExp);
  @Query(
    value =
      "    SELECT COUNT(C1.ID) " +
        "    FROM MSTR_BIEN_POSESION_NEGOCIADA AS C1 " +
        "    LEFT JOIN MSTR_PROCEDIMIENTO_POSESION_NEGOCIADA AS R ON C1.ID = R.ID_BIEN " +
        "    WHERE C1.ID_EXPEDIENTE_POSESION_NEGOCIADA IN ?1" +
        "    HAVING COUNT(R.ID) > 0 ",
    nativeQuery = true
  )
  Integer countAllByCountProcedimientosMore(List<Integer> idsExp);
  @Query(
    value =
      "    SELECT SUM(B.IMPORTE_BIEN) " +
        "    FROM MSTR_BIEN AS B " +
        "    LEFT JOIN MSTR_BIEN_POSESION_NEGOCIADA AS C1 ON C1.ID_BIEN = B.ID" +
        "    LEFT JOIN MSTR_PROCEDIMIENTO_POSESION_NEGOCIADA AS R ON C1.ID = R.ID_BIEN " +
        "    WHERE C1.ID_EXPEDIENTE_POSESION_NEGOCIADA IN ?1" +
        "    HAVING COUNT(R.ID) > 0 ",
    nativeQuery = true
  )
  Double sumAllByCountProcedimientosMore(List<Integer> idsExp);
  //Clasificacion NO TIENE
}
