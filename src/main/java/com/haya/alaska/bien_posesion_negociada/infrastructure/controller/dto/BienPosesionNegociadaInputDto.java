package com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto;

import com.haya.alaska.bien.infrastructure.controller.dto.BienInputDto;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienPosesionNegociadaInputDto extends BienInputDto {
  private Integer estrategiaAsignada;
  private String situacion;
  private Integer tipoEstado;
  private Integer subtipoEstado;
}
