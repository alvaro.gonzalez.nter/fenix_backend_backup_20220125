package com.haya.alaska.bien_posesion_negociada.infrastructure.mapper;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDTOToList;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaExtendedOutputDto;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaOutputDto;
import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.bien_subastado.infrastructure.controller.dto.BienSubastadoDTOToList;
import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.infrastructure.mapper.CargaMapper;
import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.shared.dto.CargaDTOToList;
import com.haya.alaska.tasacion.infrastructure.mapper.TasacionMapper;
import com.haya.alaska.valoracion.infrastructure.mapper.ValoracionMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class BienPosesionNegociadaMapper {

  @Autowired
  private CatalogoMinInfoMapper catalogoMinInfoMapper;

  @Autowired
  private TasacionMapper tasacionMapper;

  @Autowired
  private ValoracionMapper valoracionMapper;

  @Autowired
  private CargaMapper cargaMapper;

  public List<BienPosesionNegociadaDTOToList> listBienPosesionNegociadaToListBienPosesionNegociadaDTOToList(List<BienPosesionNegociada> bienes) {
    List<BienPosesionNegociadaDTOToList> bienPosesionNegociadaDTO = new ArrayList<>();
    for (BienPosesionNegociada bienPosesionNegociada : bienes) {
      bienPosesionNegociadaDTO.add(new BienPosesionNegociadaDTOToList(bienPosesionNegociada));
    }
    return bienPosesionNegociadaDTO;
  }

  public BienPosesionNegociadaOutputDto entityOutputDto(BienPosesionNegociada bienPosesionNegociada) {

    BienPosesionNegociadaOutputDto bienPosesionNegociadaOutputDto = new BienPosesionNegociadaOutputDto();
    BeanUtils.copyProperties(bienPosesionNegociada.getBien(), bienPosesionNegociadaOutputDto);
    bienPosesionNegociadaOutputDto.setIdBienPosesionNegociada(bienPosesionNegociada.getId());
    if (bienPosesionNegociada.getEstadoOcupacion() != null)
      bienPosesionNegociadaOutputDto.setEstadoOcupacion(catalogoMinInfoMapper.entityToDto(bienPosesionNegociada.getEstadoOcupacion()));
    if (bienPosesionNegociada.getBien().getLocalidad() != null)
      bienPosesionNegociadaOutputDto.setLocalidad(catalogoMinInfoMapper.entityToDto(bienPosesionNegociada.getBien().getLocalidad()));
    if (bienPosesionNegociada.getBien().getMunicipio() != null)
      bienPosesionNegociadaOutputDto.setMunicipio(catalogoMinInfoMapper.entityToDto(bienPosesionNegociada.getBien().getMunicipio()));
    if (bienPosesionNegociada.getBien().getTipoActivo() != null)
      bienPosesionNegociadaOutputDto.setTipoActivo(catalogoMinInfoMapper.entityToDto(bienPosesionNegociada.getBien().getTipoActivo()));
    if (bienPosesionNegociada.getSituacion() != null)
      bienPosesionNegociadaOutputDto.setSituacion(catalogoMinInfoMapper.entityToDto(bienPosesionNegociada.getSituacion()));
    if (bienPosesionNegociada.getBien().getNombreVia() != null)
      bienPosesionNegociadaOutputDto.setDireccion(bienPosesionNegociada.getBien().getNombreVia());
    if (bienPosesionNegociada.getBien().getImporteBien() != null)
      bienPosesionNegociadaOutputDto.setValor(bienPosesionNegociada.getBien().getImporteBien());
    if (bienPosesionNegociada.getEstrategia() != null) {
      bienPosesionNegociadaOutputDto.setEstrategiaAsignada(bienPosesionNegociada.getEstrategia().getEstrategia().getId());
    }
    if (bienPosesionNegociada.getTipoEstado() !=null){
      bienPosesionNegociadaOutputDto.setTipoEstado(
          catalogoMinInfoMapper.entityToDto(bienPosesionNegociada.getTipoEstado()));
    }
    if (bienPosesionNegociada.getSubEstado()!=null){
      bienPosesionNegociadaOutputDto.setSubtipoEstado(catalogoMinInfoMapper.entityToDto(bienPosesionNegociada.getSubEstado()));
    }
    //aqu
    Set<CampaniaAsignadaBienPN> campaniasAsignadas = bienPosesionNegociada.getCampaniasAsignadasPN();
    if (campaniasAsignadas != null && campaniasAsignadas.size() > 0) {
      var campaniaAsignada = bienPosesionNegociada.getCampaniasAsignadasPN().stream().sorted(Comparator.comparing(CampaniaAsignadaBienPN::getFechaAsignacion).reversed()).findFirst().orElse(null);
      if (campaniaAsignada != null) {
        bienPosesionNegociadaOutputDto.setIdCampania(campaniaAsignada.getCampania().getId());
        }
    }
    Bien bien = bienPosesionNegociada.getBien();

    if (bien.getTipoVia() != null)
      bienPosesionNegociadaOutputDto.setTipoVia(catalogoMinInfoMapper.entityToDto(bien.getTipoVia()));
    if (bien.getEntorno() != null)
      bienPosesionNegociadaOutputDto.setEntorno(catalogoMinInfoMapper.entityToDto(bien.getEntorno()));
    if (bien.getMunicipio() != null)
      bienPosesionNegociadaOutputDto.setMunicipio(catalogoMinInfoMapper.entityToDto(bien.getMunicipio()));
    if (bien.getLocalidad() != null)
      bienPosesionNegociadaOutputDto.setLocalidad(catalogoMinInfoMapper.entityToDto(bien.getLocalidad()));
    if (bien.getProvincia() != null)
      bienPosesionNegociadaOutputDto.setProvincia(catalogoMinInfoMapper.entityToDto(bien.getProvincia()));
    if (bien.getPais() != null)
      bienPosesionNegociadaOutputDto.setPais(catalogoMinInfoMapper.entityToDto(bien.getPais()));
    if (bien.getTipoActivo() != null)
      bienPosesionNegociadaOutputDto.setTipoActivo(catalogoMinInfoMapper.entityToDto(bien.getTipoActivo()));
    if (bien.getUso() != null)
      bienPosesionNegociadaOutputDto.setUso(catalogoMinInfoMapper.entityToDto(bien.getUso()));
    if (bien.getTipoBien() != null)
      bienPosesionNegociadaOutputDto.setTipoBien(catalogoMinInfoMapper.entityToDto(bien.getTipoBien()));
    if (bien.getSubTipoBien() != null)
      bienPosesionNegociadaOutputDto.setSubTipoBien(catalogoMinInfoMapper.entityToDto(bien.getSubTipoBien()));

    if (bien.getTasaciones() != null)
      bienPosesionNegociadaOutputDto.setTasaciones(tasacionMapper.listTasacionToListTasacionDTOToList(bien.getTasaciones()));

    if (bien.getValoraciones() != null)
      bienPosesionNegociadaOutputDto.setValoraciones(valoracionMapper.listValoracionToListValoracionDTOToList(bien.getValoraciones()));

    if (bien.getCargas() != null) {
      Set<Carga> a = bien.getCargas();
      List<CargaDTOToList> b = cargaMapper.listCargaToListCargaDTOToList(a, null);
      bienPosesionNegociadaOutputDto.setCargas(b);
    }

    List<BienSubastado> subastas = new ArrayList<>(bien.getBienesSubastados());
    if (!subastas.isEmpty())
      bienPosesionNegociadaOutputDto.setSaneamiento(subastas.stream().map(BienSubastadoDTOToList::new).collect(Collectors.toList()));
    else bienPosesionNegociadaOutputDto.setSaneamiento(new ArrayList<>());

    return bienPosesionNegociadaOutputDto;
  }

  public BienPosesionNegociadaExtendedOutputDto entityExtendedOutputDto(BienPosesionNegociada bien) {
    BienPosesionNegociadaOutputDto temp = entityOutputDto(bien);
    BienPosesionNegociadaExtendedOutputDto result = new BienPosesionNegociadaExtendedOutputDto();
    BeanUtils.copyProperties(temp, result);
    result.setFotos(bien);
    result.setLatitud(bien.getBien() != null? bien.getBien().getLatitud() : null);
    result.setLongitud(bien.getBien() != null? bien.getBien().getLongitud() : null);
    return result;
  }

}
