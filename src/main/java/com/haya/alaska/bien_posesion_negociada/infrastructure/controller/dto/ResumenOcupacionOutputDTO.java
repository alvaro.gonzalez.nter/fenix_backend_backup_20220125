package com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ResumenOcupacionOutputDTO {
  private Integer numeroTotalOcupantes;
  private Long numeroMenores;
  private Long numeroDiscapacitados;
  private Boolean residenciaHabitual;
  private Boolean vulnerabilidad;
  private Double ingresosUnidadFamiliar;

}
