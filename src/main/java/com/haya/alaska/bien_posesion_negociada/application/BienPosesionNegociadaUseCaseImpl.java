package com.haya.alaska.bien_posesion_negociada.application;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.Bien_;
import com.haya.alaska.bien.infrastructure.controller.dto.BienInputDto;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien.infrastructure.util.BienUtil;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada_;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.*;
import com.haya.alaska.bien_posesion_negociada.infrastructure.mapper.BienPosesionNegociadaMapper;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.bien_subastado.infrastructure.repository.BienSubastadoRepository;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.estado_expediente_posesion_negociada.domain.EstadoExpedientePosesionNegociada;
import com.haya.alaska.estado_expediente_posesion_negociada.infastructure.repository.EstadoExpedientePosesionNegociadaRepository;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion_;
import com.haya.alaska.estado_ocupacion.infrastructure.repository.EstadoOcupacionRepository;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.estrategia_asignada.infrastructure.repository.EstrategiaAsignadaRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada_;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.mapper.ExpedientePosesionNegociadaMapper;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.application.GestorDocumentalUseCase;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.domain.enums.TipoEntidadEnum;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.localidad.domain.Localidad_;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.ocupante_bien.infrastructure.repository.OcupanteBienRepository;
import com.haya.alaska.origen_estrategia.infrastructure.repository.OrigenEstrategiaRepository;
import com.haya.alaska.periodo_estrategia.infrastructure.repository.PeriodoEstrategiaRepository;
import com.haya.alaska.procedimiento.domain.ProcedimientoHipotecario;
import com.haya.alaska.provincia.domain.Provincia_;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.situacion.domain.Situacion;
import com.haya.alaska.situacion.infrastructure.repository.SituacionRepository;
import com.haya.alaska.subasta.domain.Subasta;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.subtipo_estado.infrastructure.repository.SubtipoEstadoRepository;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.tipo_estado.infrastructure.repository.TipoEstadoRepository;
import com.haya.alaska.tipo_estrategia.infrastructure.repository.TipoEstrategiaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class BienPosesionNegociadaUseCaseImpl implements BienPosesionNegociadaUseCase {

  @PersistenceContext
  private EntityManager entityManager;

  @Autowired
  private BienPosesionNegociadaMapper bienPosesionNegociadaMapper;
  @Autowired
  private BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  private EstadoOcupacionRepository estadoOcupacionRepository;
  @Autowired
  private BienRepository bienRepository;
  @Autowired
  private BienUtil bienUtil;
  @Autowired
  private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  EstadoExpedientePosesionNegociadaRepository estadoExpedientePosesionNegociadaRepository;
  @Autowired
  private BienSubastadoRepository bienSubastadoRepository;
  @Autowired
  private OcupanteBienRepository ocupantesRepository;
  @Autowired
  private EstrategiaAsignadaRepository estrategiaAsignadaRepository;
  @Autowired
  private EstrategiaRepository estrategiaRepository;
  @Autowired
  private SituacionRepository situacionRepository;
  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;
  @Autowired
  private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired
  private GestorDocumentalUseCase gestorDocumentalUseCase;
  @Autowired
  TipoEstrategiaRepository tipoEstrategiaRepository;
  @Autowired
  OrigenEstrategiaRepository origenEstrategiaRepository;
  @Autowired
  PeriodoEstrategiaRepository periodoEstrategiaRepository;
  @Autowired
  ExpedientePosesionNegociadaMapper expedientePosesionNegociadaMapper;
  @Autowired
  TipoEstadoRepository tipoEstadoRepository;
  @Autowired
  SubtipoEstadoRepository subtipoEstadoRepository;


  @Override
  public ListWithCountDTO<BienPosesionNegociadaDTOToList> getAllFilteredBienes(Integer idExpediente, Integer id, String estadoOcupacion, String fincaRegistral, String localidad, String provincia, String direccion, String valor, String modoGestion, String orderField, String orderDirection, Integer size, Integer page) {
    List<BienPosesionNegociada> bienesSinPaginar = filterBienesInMemory(idExpediente, id, estadoOcupacion, fincaRegistral, localidad, provincia, direccion, valor, orderField, orderDirection);

    List<BienPosesionNegociada> bienes = bienesSinPaginar.stream().skip(size * page).limit(size).collect(Collectors.toList());

    List<BienPosesionNegociadaDTOToList> ocupantesDTOS = bienPosesionNegociadaMapper.listBienPosesionNegociadaToListBienPosesionNegociadaDTOToList(bienes);
    // Si se quiere ordenar por Modo Gestion al ser formado no podemos usar Criteria
    if (modoGestion != null) {
      ocupantesDTOS = ocupantesDTOS.stream().filter(o -> {
        if (!o.getModoGestion().toLowerCase().contains(modoGestion)) return false;
        return true;
      }).collect(Collectors.toList());
    }
    if (orderField != null && orderField.equals("ModoGestion")) {
      Comparator<BienPosesionNegociadaDTOToList> comparator = Comparator.comparing(BienPosesionNegociadaDTOToList::getModoGestion);
      if (orderDirection.equals("asc")) {
        ocupantesDTOS = ocupantesDTOS.stream().sorted(comparator).collect(Collectors.toList());
      } else {
        ocupantesDTOS = ocupantesDTOS.stream().sorted(comparator.reversed()).collect(Collectors.toList());
      }
    }
    return new ListWithCountDTO<>(ocupantesDTOS, bienesSinPaginar.size());
  }

  @Override
  public BienPosesionNegociadaOutputDto findBienPosesionNegociadaByIdDto(Integer idExpediente, Integer idBien) throws NotFoundException {
    BienPosesionNegociada bienPosesionNegociada = bienPosesionNegociadaRepository.findById(idBien).orElseThrow(() -> new NotFoundException("bien de posesión negociada", idBien));
    BienPosesionNegociadaOutputDto result = bienPosesionNegociadaMapper.entityOutputDto(bienPosesionNegociada);

    return result;
  }

  @Override
  public BienPosesionNegociadaExtendedOutputDto findBienPosesionNegociadaByIdMovilDto(Integer idExpediente, Integer idBien) throws NotFoundException {
    BienPosesionNegociada bienPosesionNegociada = bienPosesionNegociadaRepository.findById(idBien).orElseThrow(() -> new NotFoundException("bien de posesión negociada", idBien));
    BienPosesionNegociadaExtendedOutputDto result = bienPosesionNegociadaMapper.entityExtendedOutputDto(bienPosesionNegociada);
    return result;
  }

  @Override
  public BienPosesionNegociadaOutputDto update(Integer idExpediente, Integer idBien, BienPosesionNegociadaInputDto bienPosesionNegociadaInputDTO) throws NotFoundException {

    BienPosesionNegociada bienPN = bienPosesionNegociadaRepository.findById(idBien).orElseThrow(() -> new NotFoundException("bien de posesión negociada", idBien));
    BienInputDto bienInputDto = new BienInputDto();
    BeanUtils.copyProperties(bienPosesionNegociadaInputDTO, bienInputDto);
    bienUtil.updateBien(bienPN.getBien(), bienInputDto, null);

    if (bienPosesionNegociadaInputDTO.getEstadoOcupacion() != null && (bienPN.getEstadoOcupacion() == null || !bienPN.getEstadoOcupacion().getId().equals(bienPosesionNegociadaInputDTO.getEstadoOcupacion()))) {
      EstadoOcupacion nuevoEstadoOcupacion = estadoOcupacionRepository.findById(Integer.valueOf(bienPosesionNegociadaInputDTO.getEstadoOcupacion())).orElseThrow(() -> new NotFoundException("No se ha encontrado el estado de ocupación con id " + bienPosesionNegociadaInputDTO.getEstadoOcupacion()));
      bienPN.setEstadoOcupacion(nuevoEstadoOcupacion);
      bienPN = bienPosesionNegociadaRepository.saveAndFlush(bienPN);
    }

    if (bienPosesionNegociadaInputDTO.getEstrategiaAsignada() != null && (bienPN.getEstrategia() == null || !bienPN.getEstrategia().getId().equals(bienPosesionNegociadaInputDTO.getEstrategiaAsignada()))) {
      //crear nueva estrategia
      var ea2 = new EstrategiaAsignada();
      ea2.setOrigenEstrategia(origenEstrategiaRepository.findByCodigo("GM").orElse(null));
      ea2.setTipoEstrategia(tipoEstrategiaRepository.findByCodigo("M").orElse(null));
      ea2.setPeriodoEstrategia(periodoEstrategiaRepository.findByCodigo("M").orElse(null));
      ea2.setFecha(new Date());
      ea2.setEstrategia(estrategiaRepository.findById(bienPosesionNegociadaInputDTO.getEstrategiaAsignada()).orElseThrow(() -> new NotFoundException("Estrategia", bienPosesionNegociadaInputDTO.getEstrategiaAsignada())));

      bienPN.setEstrategia(estrategiaAsignadaRepository.save(ea2));
      bienPN = bienPosesionNegociadaRepository.saveAndFlush(bienPN);
    }

    if (bienPosesionNegociadaInputDTO.getSituacion() != null) {
      Situacion situacion = situacionRepository.findById(Integer.valueOf(bienPosesionNegociadaInputDTO.getSituacion())).orElse(null);
      if (situacion != null) {
        bienPN.setSituacion(situacion);
        bienPN = bienPosesionNegociadaRepository.saveAndFlush(bienPN);
      }
    }

    if (bienPosesionNegociadaInputDTO.getTipoEstado() != null) {
      TipoEstado tipoEstado = tipoEstadoRepository.findById(bienPosesionNegociadaInputDTO.getTipoEstado()).orElse(null);
      bienPN.setTipoEstado(tipoEstado);
    }
    if (bienPosesionNegociadaInputDTO.getSubtipoEstado() != null) {
      SubtipoEstado subtipoEstado = subtipoEstadoRepository.findById(bienPosesionNegociadaInputDTO.getSubtipoEstado()).orElse(null);
      bienPN.setSubEstado(subtipoEstado);
    }
    bienPN = bienPosesionNegociadaRepository.save(bienPN);

    // Cuando ya se ha actualizado el modo de gestion hay que revisar si el activo es el principal
    // para actualizar el modoGestion del expediente
    ExpedientePosesionNegociada expedientePosesionNegociada = expedientePosesionNegociadaRepository.findById(idExpediente).orElseThrow(() -> new NotFoundException("Expediente posesion negociada no encontrada con el id: " + idExpediente));
    BienPosesionNegociada bienPosesionNegociadaPrincipal = expedientePosesionNegociadaMapper.getPrincipal(expedientePosesionNegociada);
    if (bienPosesionNegociadaPrincipal.getId().equals(bienPN.getId())) {
      if (bienPosesionNegociadaInputDTO.getTipoEstado() != null) {
        TipoEstado tipoEstado = tipoEstadoRepository.findById(bienPosesionNegociadaInputDTO.getTipoEstado()).orElse(null);
        expedientePosesionNegociada.setTipoEstado(tipoEstado);
      }
      if (bienPosesionNegociadaInputDTO.getSubtipoEstado() != null) {
        SubtipoEstado subtipoEstado = subtipoEstadoRepository.findById(bienPosesionNegociadaInputDTO.getSubtipoEstado()).orElse(null);
        bienPN.setSubEstado(subtipoEstado);
        expedientePosesionNegociada.setSubEstado(subtipoEstado);
      }
      expedientePosesionNegociadaRepository.save(expedientePosesionNegociada);
    }

    BienPosesionNegociadaOutputDto result = bienPosesionNegociadaMapper.entityOutputDto(bienPN);

    return result;
  }

  private void checkIfExistBienByIdOrigen(String idOrigen) throws InvalidCodeException {
    Bien bien = bienRepository.findByIdOrigenEquals(idOrigen).orElse(null);
    if (bien != null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCodeException("The asset could not be created with the Origin ID " + idOrigen + ", there is already an asset with the entered ID");
      else
        throw new InvalidCodeException("No se ha podido crear el bien con el ID Origen " + idOrigen + ", ya existe un activo con el ID introducido");
    }
  }

  private void checkIdCarga(String idCarga) throws InvalidCodeException {
    Bien bien = null;
    if (idCarga != null) {
      bien = bienRepository.findByIdCargaEquals(idCarga).orElse(null);
      if (bien != null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCodeException("The asset cannot be created, there is already a asset with the load id " + idCarga);
        else
          throw new InvalidCodeException("No se puede crear el bien, ya existe un bien con el id de carga " + idCarga);
      }
    }
  }

  private void checkIdHaya(String idHaya) throws InvalidCodeException {
    Bien bien = null;
    if (idHaya != null) {
      bien = bienRepository.findByIdHayaEquals(idHaya).orElse(null);
      if (bien != null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCodeException("The asset cannot be created, there is already a asset with the id of Haya " + idHaya);
        else throw new InvalidCodeException("No se puede crear el bien, ya existe un bien con el id de Haya " + idHaya);
      }
    }
  }

  private void checkIfExistBienByIdOrigen2(String idOrigen2) throws InvalidCodeException {
    Bien bien = bienRepository.findByIdOrigen2Equals(idOrigen2).orElse(null);
    if (bien != null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCodeException("The asset could not be created with the Origin ID " + idOrigen2 + ", an asset already exists with the ID entered");
      else
        throw new InvalidCodeException("No se ha podido crear el bien con el ID Origen " + idOrigen2 + ", ya existe un activo con el ID introducido");
    }
  }

  private List<BienPosesionNegociadaOutputDto> createBienes(ExpedientePosesionNegociada expediente, List<BienPosesionNegociadaInputDto> nuevosBienesDTO, boolean rem) throws NotFoundException, InvalidCodeException {
    List<BienPosesionNegociadaOutputDto> result = new ArrayList<>();
    for (BienPosesionNegociadaInputDto bienInput : nuevosBienesDTO) {

      //comprobamos que no exista un bien con el id de origen o origen 2 introducido
      /*Cambio pedido por Jesús el 05/03/2021 por una petición de BAU
       * Quieren que no se tengan en cuenta el idOrigen e idOrigen2*/
      /*if (bienInput.getIdOrigen() != null) {
        checkIfExistBienByIdOrigen(bienInput.getIdOrigen());
      } else if (bienInput.getIdOrigen2() != null) {
        checkIfExistBienByIdOrigen2(bienInput.getIdOrigen2());
      }*/

      if (bienInput.getIdCarga() != null) {
        checkIdCarga(bienInput.getIdCarga());
      }

      if (bienInput.getIdHaya() != null) {
        checkIdHaya(bienInput.getIdHaya());
      }

      //añadimos el bien
      var bienDTO = new BienInputDto();
      Bien bien = new Bien();
      BeanUtils.copyProperties(bienInput, bienDTO);
      var nuevoBien = bienUtil.createBien(bien, expediente.getCartera(), bienDTO, null, rem);

      //creamos la relación del bien con la posesión negociada
      var nuevoBienPosesionNegociada = new BienPosesionNegociada();
      nuevoBienPosesionNegociada.setExpedientePosesionNegociada(expediente);
      nuevoBienPosesionNegociada.setBien(nuevoBien);
      EstadoOcupacion estadoOcupacion = estadoOcupacionRepository.findByCodigo(EstadoOcupacion.PENDIENTE_DE_INFORMAR).orElseThrow(() -> new NotFoundException("estado de ocupación", "código", "PEN (Pendiente de informar)"));
      nuevoBienPosesionNegociada.setEstadoOcupacion(estadoOcupacion);

      Situacion situacion = null;
      if (!rem) {
        situacion = bienInput.getSituacion() != null ? situacionRepository.findById(Integer.valueOf(bienInput.getSituacion())).orElse(null) : null;
      } else {
        situacion = bienInput.getSituacion() != null ? situacionRepository.findByCodigo(bienInput.getSituacion()).orElse(null) : null;
      }
      if (situacion != null) {
        nuevoBienPosesionNegociada.setSituacion(situacion);
      }
      nuevoBienPosesionNegociada = bienPosesionNegociadaRepository.saveAndFlush(nuevoBienPosesionNegociada);
      expediente.getBienesPosesionNegociada().add(nuevoBienPosesionNegociada);
      expedientePosesionNegociadaRepository.save(expediente);

      var nuevoBienDTO = bienPosesionNegociadaMapper.entityOutputDto(nuevoBienPosesionNegociada);
      result.add(nuevoBienDTO);
    }
    return result;
  }

  private void checkIfBienAlreadyAssigned(Bien bien) throws InvalidCodeException {
    if (bienPosesionNegociadaRepository.findByBien(bien).orElse(null) != null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCodeException("The asset with origin id " + bien.getIdOrigen() + " entered is already associated with a negotiated possession file");
      else
        throw new InvalidCodeException("El bien con id de origen " + bien.getIdOrigen() + " introducido ya está asociado a un expediente de posesión negociada");
    }
  }

  private List<BienPosesionNegociadaOutputDto> createBienesExistentes(ExpedientePosesionNegociada expediente, List<Integer> nuevosBienes) throws NotFoundException, InvalidCodeException {
    List<BienPosesionNegociadaOutputDto> result = new ArrayList<>();
    for (Integer bienExistente : nuevosBienes) {

      //añadimos el bien
      var bien = bienRepository.findById(bienExistente).orElseThrow(() -> new NotFoundException("bien", bienExistente));

      //comprobamos si el bien ya está asociado con otro expediente de posesión negociada
      checkIfBienAlreadyAssigned(bien);

      //creamos la relación del bien con la posesión negociada
      var nuevoBienPosesionNegociada = new BienPosesionNegociada();
      nuevoBienPosesionNegociada.setExpedientePosesionNegociada(expediente);
      nuevoBienPosesionNegociada.setBien(bien);
      EstadoOcupacion estadoOcupacion = estadoOcupacionRepository.findByCodigo(EstadoOcupacion.PENDIENTE_DE_INFORMAR).orElseThrow(() -> new NotFoundException("estado de ocupación", "código", "PEN (Pendiente de informar)"));
      nuevoBienPosesionNegociada.setEstadoOcupacion(estadoOcupacion);

      nuevoBienPosesionNegociada = bienPosesionNegociadaRepository.saveAndFlush(nuevoBienPosesionNegociada);

      var nuevoBienDTO = bienPosesionNegociadaMapper.entityOutputDto(nuevoBienPosesionNegociada);
      result.add(nuevoBienDTO);
    }
    return result;
  }

  @Override
  public List<BienPosesionNegociadaOutputDto> create(ExpedientePosesionNegociada expedientePosesionNegociada, List<BienPosesionNegociadaInputDto> nuevosBienesDTO, List<Integer> bienesExistentes, boolean rem) throws NotFoundException, InvalidCodeException {
    List<BienPosesionNegociadaOutputDto> result = new ArrayList<>();

    if (nuevosBienesDTO != null) {
      result.addAll(createBienes(expedientePosesionNegociada, nuevosBienesDTO, rem));
    }

    if (bienesExistentes != null) {
      result.addAll(createBienesExistentes(expedientePosesionNegociada, bienesExistentes));
    }

    return result;
  }

  public List<BienPosesionNegociada> filterBienesInMemory(Integer idExpediente, Integer id, String estadoOcupacion, String fincaRegistral, String localidad, String provincia, String direccion, String valor, String orderField, String orderDirection) {

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<BienPosesionNegociada> query = cb.createQuery(BienPosesionNegociada.class);

    Root<BienPosesionNegociada> root = query.from(BienPosesionNegociada.class);
    List<Predicate> predicates = new ArrayList<>();

    Join<BienPosesionNegociada, ExpedientePosesionNegociada> cbJoin = root.join(BienPosesionNegociada_.expedientePosesionNegociada, JoinType.INNER);
    Predicate onExpediente = cb.equal(cbJoin.get(ExpedientePosesionNegociada_.id), idExpediente);
    cbJoin.on(onExpediente);

    if (id != null) predicates.add(cb.like(root.get(BienPosesionNegociada_.id).as(String.class), "%" + id + "%"));

    if (estadoOcupacion != null)
      predicates.add(cb.like(root.get(BienPosesionNegociada_.estadoOcupacion).get(EstadoOcupacion_.valor), "%" + estadoOcupacion + "%"));
    if (fincaRegistral != null)
      predicates.add(cb.like(root.get(BienPosesionNegociada_.bien).get(Bien_.finca), "%" + fincaRegistral + "%"));
    if (localidad != null)
      predicates.add(cb.like(cb.upper(root.get(BienPosesionNegociada_.bien).get(Bien_.localidad).get(Localidad_.valor)), "%" + localidad.toUpperCase() + "%"));
    if (provincia != null)
      predicates.add(cb.like(cb.upper(root.get(BienPosesionNegociada_.bien).get(Bien_.provincia).get(Provincia_.valor)), "%" + provincia.toUpperCase() + "%"));
    if (direccion != null)
      predicates.add(cb.like(root.get(BienPosesionNegociada_.bien).get(Bien_.nombreVia), "%" + direccion + "%"));
    if (valor != null)
      predicates.add(cb.like(root.get(BienPosesionNegociada_.bien).get(Bien_.importeBien).as(String.class), "%" + valor.toString() + "%"));


    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
    if (orderField != null) {
      switch (orderField) {

        case "id":
          rs.orderBy(getOrden(cb, root.get(BienPosesionNegociada_.id), orderDirection));
          break;
        case "estadoOcupacion":
          rs.orderBy(getOrden(cb, root.get(BienPosesionNegociada_.estadoOcupacion).get(EstadoOcupacion_.valor), orderDirection));
          break;
        case "finca":
          rs.orderBy(getOrden(cb, root.get(BienPosesionNegociada_.bien).get(Bien_.finca), orderDirection));
          break;

        case "localidad":
          rs.orderBy(getOrden(cb, root.get(BienPosesionNegociada_.bien).get(Bien_.localidad).get(Localidad_.valor), orderDirection));
          break;
        case "provincia":
          rs.orderBy(getOrden(cb, root.get(BienPosesionNegociada_.bien).get(Bien_.provincia).get(Provincia_.valor), orderDirection));
          break;
        case "direccion":
          rs.orderBy(getOrden(cb, root.get(BienPosesionNegociada_.bien).get(Bien_.nombreVia), orderDirection));
          break;
        case "valor":
          rs.orderBy(getOrden(cb, root.get(BienPosesionNegociada_.bien).get(Bien_.importeBien), orderDirection));
          break;
      }
    }
    return entityManager.createQuery(query).getResultList();
  }


  private Order getOrden(CriteriaBuilder cb, Expression expresion, String orderDirection) {
    if (orderDirection == null) {
      orderDirection = "desc";
    }
    return orderDirection.toLowerCase().equalsIgnoreCase("desc") ? cb.desc(expresion) : cb.asc(expresion);
  }


  public void tramitarBien(List<Bien> bienes) throws NotFoundException {
    for (Bien b : bienes) {
      if (b != null) {
        b.setTramitado(true);
      }
    }
    for (Bien b : bienes) {
      BienPosesionNegociada bpn = bienPosesionNegociadaRepository.findByBien(b).orElse(null);
      if (bpn != null) {
        this.closeExpediente(bpn.getExpedientePosesionNegociada().getId());
        break;
      }
    }
  }

  private void closeExpediente(Integer idExpediente) throws NotFoundException {
    ExpedientePosesionNegociada epn = expedientePosesionNegociadaRepository.findById(idExpediente).orElse(null);
    Boolean cerrar = true;
    if (epn != null) {
      Set<BienPosesionNegociada> bienesPN = epn.getBienesPosesionNegociada();
      if (bienesPN != null) {
        for (BienPosesionNegociada bpn : bienesPN) {
          Bien b = bpn.getBien();
          if (b != null && !b.getTramitado()) {
            cerrar = false;
            break;
          }
        }
      }

      if (cerrar) {
        EstadoExpedientePosesionNegociada eepn = estadoExpedientePosesionNegociadaRepository.findByCodigo("CER").orElseThrow(() -> new NotFoundException("Estado Expediente Posesion Negociada", "código", "'CER'"));
        epn.setEstadoExpedienteRecuperacionAmistosa(eepn);
        expedientePosesionNegociadaRepository.save(epn);
      }
    }
  }

  @Override
  public ResumenOcupacionOutputDTO getResumenOcupacion(Integer idBienPN, Set<Integer> idsOcupantesSeleccionados) throws NotFoundException {
    BienPosesionNegociada bienPN = bienPosesionNegociadaRepository.findById(idBienPN).orElseThrow(() -> new NotFoundException("bien de posesión negociada", idBienPN));
    ResumenOcupacionOutputDTO resumen = new ResumenOcupacionOutputDTO();
    var ocupantesBien = ocupantesRepository.findAllByBienPosesionNegociadaId(idBienPN);

    var ocupantesSeleccionados = ocupantesBien.stream().map(ob -> ob.getOcupante()).filter(o -> idsOcupantesSeleccionados.contains(o.getId())).collect(Collectors.toList());

    resumen.setNumeroTotalOcupantes(ocupantesSeleccionados.size());
    resumen.setNumeroMenores(ocupantesSeleccionados.stream().filter(o -> o.getMenorEdad() != null && o.getMenorEdad()).count());
    resumen.setNumeroDiscapacitados(ocupantesSeleccionados.stream().filter(o -> o.getDiscapacitado() != null && o.getDiscapacitado()).count());
    resumen.setResidenciaHabitual(ocupantesSeleccionados.stream().anyMatch(o -> o.getResidenciaHabitual() != null && o.getResidenciaHabitual()));
    resumen.setVulnerabilidad(ocupantesSeleccionados.stream().anyMatch(o -> o.getOtrasSituacionesVulnerabilidad() != null));
    Double ingresosUnidadFamiliar = 0.0;
    for (Ocupante ocupante : ocupantesSeleccionados.stream().collect(Collectors.toList())) {
      Double mas = 0.0;
      if (ocupante.getIngresosNetosMensuales() != null) {
        mas = ocupante.getIngresosNetosMensuales();
      }
      ingresosUnidadFamiliar += mas;
    }
    resumen.setIngresosUnidadFamiliar(ingresosUnidadFamiliar);

    return resumen;
  }

  @Override
  public ResumenInformacionJudicialOutputDTO getResumenInformacionJudicial(Integer idExpediente, Integer idBienPN) throws NotFoundException {
    ResumenInformacionJudicialOutputDTO infoJudicial = new ResumenInformacionJudicialOutputDTO();
    BienPosesionNegociada bienPN = bienPosesionNegociadaRepository.findById(idBienPN).orElseThrow(() -> new NotFoundException("bien de posesión negociada", idBienPN));
    BienSubastado bienSubastado = null;
    try {
      bienSubastado = bienSubastadoRepository.findAllByBien(bienPN.getBien()).stream().filter(bs -> bs.getSubasta().getProcedimiento() instanceof ProcedimientoHipotecario).findFirst().orElse(null);
    } catch (Exception exception) {

    }
    Subasta subasta = null;
    if (bienSubastado != null) {
      subasta = bienSubastado.getSubasta();
    }
    ProcedimientoHipotecario proc = null;
    if (subasta != null) {
      proc = (ProcedimientoHipotecario) subasta.getProcedimiento();
    }
    if (proc != null) {
      infoJudicial.setTipoProcedimiento(proc.getTipo().getValor());
      infoJudicial.setJuzgado(proc.getJuzgado());
      infoJudicial.setAutos(proc.getNumeroAutos());//ok
      infoJudicial.setFechaSubasta(subasta.getFechaInicioPujas());//ok
      infoJudicial.setDecreto(subasta.getFechaNotificacionDecreto()); //ok
      infoJudicial.setTestimonio(bienSubastado.getFechaTestimonio() != null); //ok
      infoJudicial.setOposicion(proc.getResultadoOposicion());
      infoJudicial.setLanzamiento(bienSubastado.getFechaPosteriorLanzamiento() != null); //BienSubastado.fechaPosteriorLanzamiento
    }


    bienPN.getBien();
    return infoJudicial;
  }

  @Override
  public List<BienPosesionNegociadaDTOToList> getAllBienesPosesionNegociada(List<Integer> expedientesPN) {
    List<BienPosesionNegociadaDTOToList> result = new ArrayList<>();
    for (Integer id : expedientesPN) {
      List<BienPosesionNegociada> bienes = filterBienesInMemory(id, null, null, null, null, null, null, null, null, null);
      result.addAll(bienPosesionNegociadaMapper.listBienPosesionNegociadaToListBienPosesionNegociadaDTOToList(bienes));
    }
    return result;
  }

  @Override
  public void createDocumento(Integer idBienPN, MultipartFile file, MetadatoDocumentoInputDTO metadatoDocumento, Usuario usuario) throws NotFoundException, IOException {
    BienPosesionNegociada bienPosesionNegociada = bienPosesionNegociadaRepository.findById(idBienPN).orElse(null);
    if (bienPosesionNegociada.getBien().getIdHaya() == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("This good PN does not have an identifier in the Master");
      else throw new NotFoundException("Este bienPN no tiene un identificador en el Maestro");
    }
    JSONObject metadatos = new JSONObject();
    JSONObject archivoFisico = new JSONObject();
    archivoFisico.put("contenedor", "CONT");
    metadatos.put("General documento", metadatoDocumento.createDescripcionGeneralDocumento());
    metadatos.put("Archivo físico", archivoFisico);

    long id = servicioGestorDocumental.crearDocumento(CodigoEntidad.ACTIVO_INMOBILIARIO, bienPosesionNegociada.getIdHaya(), file, metadatos).getIdDocumento();
    Integer idgestorDocumental = (int) id;
    GestorDocumental gestorDocumental = new GestorDocumental(idgestorDocumental, usuario, null, new Date(), null, metadatoDocumento.getNombreFichero(), MatriculasEnum.obtenerPorMatricula(metadatoDocumento.getCatalogoDocumental()), null, idBienPN.toString(), bienPosesionNegociada.getIdHaya(), TipoEntidadEnum.ACTIVOINMOBILIARIO);
    gestorDocumentalRepository.save(gestorDocumental);
  }

  private MetadatoDocumentoInputDTO crearMetadatoDocumento(MultipartFile file, String CatalogoDocumental) {
    return new MetadatoDocumentoInputDTO(file.getName(), CatalogoDocumental);
  }

  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosByBienPNId(Integer id, String idDocumento, String usuarioCreador, String tipo, String idEntidad, String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection, String orderField, Integer size, Integer page) throws NotFoundException, IOException {

    BienPosesionNegociada bien = bienPosesionNegociadaRepository.findById(id).orElse(null);
    if (bien.getIdHaya() != null) {
      List<DocumentoDTO> listadoIdGestor = this.servicioGestorDocumental.listarDocumentos(CodigoEntidad.ACTIVO_INMOBILIARIO, bien.getIdHaya()).stream().map(DocumentoDTO::new).collect(Collectors.toList());

      //Primero sin paginar
      List<DocumentoDTO> list = gestorDocumentalUseCase.listarDocumentosGestorBienesPosesionNegociada(listadoIdGestor, id).stream().collect(Collectors.toList());
      List<DocumentoDTO> listFinal = gestorDocumentalUseCase.filterOrderDocumentosDTO(list, idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, nombreArchivo, idHaya, orderDirection, orderField);
      List<DocumentoDTO> listaFiltradaPaginada = listFinal.stream().skip(size * page).limit(size).collect(Collectors.toList());
      return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
    } else {
      return new ListWithCountDTO<>(new ArrayList<>(), 0);
    }
  }

  @Override
  public List<BienPosesionNegociadaDTOToList> getAllByExpedientePNId(Integer idExpediente) {
    List<BienPosesionNegociada> bienesPosesionNegociada = bienPosesionNegociadaRepository.findAllByExpedientePosesionNegociadaId(idExpediente);
    List<BienPosesionNegociadaDTOToList> listaDTO = bienPosesionNegociadaMapper.listBienPosesionNegociadaToListBienPosesionNegociadaDTOToList(bienesPosesionNegociada);
    return listaDTO;
  }

  @Override
  public void deleteByIds(Integer idExpediente, Integer idBienPN, List<Integer> idsOcupantes, Usuario usuario) throws Exception {
    if (usuario.esAdministrador() || (usuario.getPerfil() != null && (usuario.getPerfil().getNombre().equals("Responsable de cartera") || usuario.getPerfil().getNombre().equals("Supervisor")))) {
      BienPosesionNegociada bienPosesionNegociada = bienPosesionNegociadaRepository.findById(idBienPN).orElseThrow();
      List<OcupanteBien> ocupanteBienes =  bienPosesionNegociada.getOcupantes().stream().filter(c-> !idsOcupantes.contains(c.getOcupante().getId())).collect(Collectors.toList());
      bienPosesionNegociada.setOcupantes(new HashSet<>(ocupanteBienes));
      bienPosesionNegociadaRepository.save(bienPosesionNegociada);
    }else{
      throw new Exception("El usuario debe ser Responsable de cartera o Supervisor para poder eliminar ocupantes");
    }
  }
}
