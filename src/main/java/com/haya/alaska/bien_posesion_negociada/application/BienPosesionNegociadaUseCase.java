package com.haya.alaska.bien_posesion_negociada.application;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.*;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface BienPosesionNegociadaUseCase {

  ListWithCountDTO<BienPosesionNegociadaDTOToList> getAllFilteredBienes(
    Integer idExpediente,
    Integer id,
    String estadoOcupacion,
    String fincaRegistral,
    String localidad,
    String provincia,
    String direccion,
    String valor,
    String modoGestion,
    String orderField,
    String orderDirection,
    Integer size,
    Integer page);

  BienPosesionNegociadaOutputDto findBienPosesionNegociadaByIdDto(Integer idExpediente, Integer idBien) throws IllegalAccessException, NotFoundException;

  BienPosesionNegociadaExtendedOutputDto findBienPosesionNegociadaByIdMovilDto(Integer idExpediente, Integer idBien) throws NotFoundException;

  BienPosesionNegociadaOutputDto update(Integer idExpediente, Integer idBien, BienPosesionNegociadaInputDto bienPosesionNegociadaInputDTO) throws NotFoundException;

  @Transactional(rollbackFor = Exception.class)
  List<BienPosesionNegociadaOutputDto> create(ExpedientePosesionNegociada expediente, List<BienPosesionNegociadaInputDto> nuevosBienesDTO, List<Integer> bienesExistentes, boolean rem) throws NotFoundException, InvalidCodeException;

  void tramitarBien(List<Bien> bienesPN) throws NotFoundException;

  ResumenOcupacionOutputDTO getResumenOcupacion( Integer idBienPN, Set<Integer> idsOcupantesSeleccionados) throws NotFoundException;

  ResumenInformacionJudicialOutputDTO getResumenInformacionJudicial(Integer idExpediente, Integer idBien) throws NotFoundException;

  List<BienPosesionNegociadaDTOToList>getAllBienesPosesionNegociada(List<Integer>expedientesPN);

  void createDocumento(Integer idBienPN, MultipartFile file, MetadatoDocumentoInputDTO metadatoDocumento, Usuario usuario) throws NotFoundException, IOException;

  ListWithCountDTO<DocumentoDTO> findDocumentosByBienPNId(Integer id,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion,String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size,Integer page) throws NotFoundException, IOException;

  List<BienPosesionNegociadaDTOToList>getAllByExpedientePNId(Integer idExpediente);

  void deleteByIds(Integer idExpediente, Integer idBienPN, List<Integer> idsOcupantes, Usuario usuario) throws Exception;
}
