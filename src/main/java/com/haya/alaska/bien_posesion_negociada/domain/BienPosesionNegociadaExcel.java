package com.haya.alaska.bien_posesion_negociada.domain;

import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class BienPosesionNegociadaExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Asset Id");
      cabeceras.add("Haya Id");
      cabeceras.add("Customer Id");
      cabeceras.add("Customer2 Id");
      cabeceras.add("Asset Type");
      cabeceras.add("Asset Subtype");
      cabeceras.add("Warranty Type");
      cabeceras.add("Status");
      cabeceras.add("Occupational Status");
      cabeceras.add("Cadastral Reference");
      cabeceras.add("Finca");
      cabeceras.add("Location Registration");
      cabeceras.add("Registration");
      cabeceras.add("Volume");
      cabeceras.add("Book");
      cabeceras.add("Folio");
      cabeceras.add("Registration Date");
      cabeceras.add("Idufir");
      cabeceras.add("Reo Origin");
      cabeceras.add("Reo Conversion Date");
      cabeceras.add("Guaranteed Amount");
      cabeceras.add("Mortgage Liability");
      cabeceras.add("Negotiated Possession");
      cabeceras.add("Auction");
      cabeceras.add("Via Type");
      cabeceras.add("Via Name");
      cabeceras.add("Number");
      cabeceras.add("Portal");
      cabeceras.add("Block");
      cabeceras.add("Stairs");
      cabeceras.add("Floor");
      cabeceras.add("Door");
      cabeceras.add("Environment");
      cabeceras.add("Municipality");
      cabeceras.add("Location");
      cabeceras.add("Province");
      cabeceras.add("Comment Address");
      cabeceras.add("Postal Code");
      cabeceras.add("Country");
      cabeceras.add("Length");
      cabeceras.add("Latitude");
      cabeceras.add("Active Type");
      cabeceras.add("Use");
      cabeceras.add("Year Construction");
      cabeceras.add("Floor area");
      cabeceras.add("Built-up area");
      cabeceras.add("Useful surface");
      cabeceras.add("Garage Annex");
      cabeceras.add("Storage Room Annex");
      cabeceras.add("Other Annex");
      cabeceras.add("Campaigns");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Activo");
      cabeceras.add("Id Haya");
      cabeceras.add("Id Cliente");
      cabeceras.add("Id Cliente2");
      cabeceras.add("Tipo Bien");
      cabeceras.add("Subtipo Bien");
      cabeceras.add("Tipo Garantia");
      cabeceras.add("Estado");
      cabeceras.add("Estado Ocupacional");
      cabeceras.add("Referencia Catastral");
      cabeceras.add("Finca");
      cabeceras.add("Localidad Registro");
      cabeceras.add("Registro");
      cabeceras.add("Tomo");
      cabeceras.add("Libro");
      cabeceras.add("Folio");
      cabeceras.add("Fecha Inscripcion");
      cabeceras.add("Idufir");
      cabeceras.add("Reo Origen");
      cabeceras.add("Fecha Reo Conversion");
      cabeceras.add("Importe Garantizado");
      cabeceras.add("Responsabilidad Hipotecaria");
      cabeceras.add("Posesion Negociada");
      cabeceras.add("Subasta");
      cabeceras.add("Tipo Via");
      cabeceras.add("Nombre Via");
      cabeceras.add("Numero");
      cabeceras.add("Portal");
      cabeceras.add("Bloque");
      cabeceras.add("Escalera");
      cabeceras.add("Piso");
      cabeceras.add("Puerta");
      cabeceras.add("Entorno");
      cabeceras.add("Municipio");
      cabeceras.add("Localidad");
      cabeceras.add("Provincia");
      cabeceras.add("Comentario Direccion");
      cabeceras.add("Codigo Postal");
      cabeceras.add("Pais");
      cabeceras.add("Longitud");
      cabeceras.add("Latitud");
      cabeceras.add("Tipo Activo");
      cabeceras.add("Uso");
      cabeceras.add("Año Construccion");
      cabeceras.add("Superficie Suelo");
      cabeceras.add("Superficie Construida");
      cabeceras.add("Superficie Util");
      cabeceras.add("Anejo Garaje");
      cabeceras.add("Anejo Trastero");
      cabeceras.add("Anejo Otros");
      cabeceras.add("Campañas");
    }
  }

  public BienPosesionNegociadaExcel(BienPosesionNegociada bienPosesionNegociada) {
    super();
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add(
          "Asset Id", bienPosesionNegociada.getId() != null ? bienPosesionNegociada.getId() : null);
      this.add(
          "Connection Id",
          bienPosesionNegociada.getExpedientePosesionNegociada() != null
              ? bienPosesionNegociada.getExpedientePosesionNegociada().getIdConcatenado()
              : null);
      this.add(
          "Haya Id",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getIdHaya()
              : null);
      this.add(
          "Customer Id",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getIdOrigen()
              : null); // FIXME Revisar
      this.add(
          "Customer2 Id",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getIdOrigen2()
              : null); // FIXME Revisar
      this.add(
          "Asset Type",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getTipoBien()
              : null);
      this.add(
          "Asset Subtype",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getSubTipoBien()
              : null);
      this.add(
          "Warranty Type",
          bienPosesionNegociada.getTipoGarantia() != null
              ? bienPosesionNegociada.getTipoGarantia()
              : null);
      if (bienPosesionNegociada.getBien() != null
          && bienPosesionNegociada.getBien().getActivo() != null)
        this.add("Status", bienPosesionNegociada.getBien().getActivo() ? "Activated" : "Released");
      this.add(
          "Occupational Status",
          bienPosesionNegociada.getEstadoOcupacion() != null
              ? bienPosesionNegociada.getEstadoOcupacion()
              : null);
      this.add(
          "Cadastral Reference",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getReferenciaCatastral()
              : null);
      this.add(
          "Finca",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getFinca()
              : null);
      this.add(
          "Location Registration",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getLocalidadRegistro()
              : null);
      this.add(
          "Registration",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getRegistro()
              : null);
      this.add(
          "Volume",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getTomo()
              : null);
      this.add(
          "Book",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getLibro()
              : null);
      this.add(
          "Folio",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getFolio()
              : null);
      this.add(
          "Registration Date",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getFechaInscripcion()
              : null);
      this.add(
          "Idufir",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getIdufir()
              : null);
      this.add(
          "Reo Origin",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getReoOrigen()
              : null);
      this.add(
          "Reo Conversion Date",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getFechaReoConversion()
              : null);
      this.add(
          "Guaranteed Amount",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getImporteGarantizado()
              : null);
      this.add(
          "Mortgage Liability",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getResponsabilidadHipotecaria()
              : null);
      this.add(
          "Negotiated Possession",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getPais()
              : null);
      if (bienPosesionNegociada.getBien() != null
          && bienPosesionNegociada.getBien().getSubasta() != null)
        this.add("Auction", bienPosesionNegociada.getBien().getSubasta() ? "YES" : "NO");
      this.add(
          "Via Type",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getTipoVia()
              : null);
      this.add(
          "Via Name",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getNombreVia()
              : null);
      this.add(
          "Number",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getNumero()
              : null);
      this.add(
          "Portal",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getPortal()
              : null);
      this.add(
          "Block",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getBloque()
              : null);
      this.add(
          "Stairs",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getEscalera()
              : null);
      this.add(
          "Floor",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getPiso()
              : null);
      this.add(
          "Door",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getPuerta()
              : null);
      this.add(
          "Environment",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getEntorno()
              : null);
      this.add(
          "Municipality",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getMunicipio()
              : null);
      this.add(
          "Location",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getLocalidad()
              : null);
      this.add(
          "Province",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getProvincia()
              : null);
      this.add(
          "Comment Address",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getComentarioDireccion()
              : null);
      this.add(
          "Postal Code",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getCodigoPostal()
              : null);
      this.add(
          "Country",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getPais()
              : null);
      this.add(
          "Length",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getLongitud()
              : null);
      this.add(
          "Latitude",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getLatitud()
              : null);
      this.add(
          "Active Type",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getTipoActivo()
              : null);
      this.add(
          "Use",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getUso()
              : null);
      this.add(
          "Year Construction",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getAnioConstruccion()
              : null);
      this.add(
          "Floor area",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getSuperficieSuelo()
              : null);
      this.add(
          "Built-up area",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getSuperficieConstruida()
              : null);
      this.add(
          "Useful surface",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getSuperficieUtil()
              : null);
      this.add(
          "Garage Annex",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getAnejoGaraje()
              : null);
      this.add(
          "Storage Room Annex",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getAnejoTrastero()
              : null);
      this.add(
          "Other Annex",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getAnejoOtros()
              : null);

      String campanias = "";
      for (CampaniaAsignadaBienPN campaniaAsignadaPN :
          bienPosesionNegociada.getCampaniasAsignadasPN()) {
        String nombre_campania = campaniaAsignadaPN.getCampania().getNombre();
        if (nombre_campania != null) {
          campanias += nombre_campania + ";";
        }
      }
      this.add("Campaigns", campanias != null ? campanias : null);
    } else {
      this.add(
          "Id Activo",
          bienPosesionNegociada.getId() != null ? bienPosesionNegociada.getId() : null);
      this.add(
          "Id Expediente",
          bienPosesionNegociada.getExpedientePosesionNegociada() != null
              ? bienPosesionNegociada.getExpedientePosesionNegociada().getIdConcatenado()
              : null);
      this.add(
          "Id Haya",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getIdHaya()
              : null);
      this.add(
          "Id Cliente",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getIdOrigen()
              : null); // FIXME Revisar
      this.add(
          "Id Cliente2",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getIdOrigen2()
              : null); // FIXME Revisar
      this.add(
          "Tipo Bien",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getTipoBien()
              : null);
      this.add(
          "Subtipo Bien",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getSubTipoBien()
              : null);
      this.add(
          "Tipo Garantia",
          bienPosesionNegociada.getTipoGarantia() != null
              ? bienPosesionNegociada.getTipoGarantia()
              : null);
      if (bienPosesionNegociada.getBien() != null
          && bienPosesionNegociada.getBien().getActivo() != null)
        this.add("Estado", bienPosesionNegociada.getBien().getActivo() ? "Activa" : "Liberada");
      this.add(
          "Estado Ocupacional",
          bienPosesionNegociada.getEstadoOcupacion() != null
              ? bienPosesionNegociada.getEstadoOcupacion()
              : null);
      this.add(
          "Referencia Catastral",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getReferenciaCatastral()
              : null);
      this.add(
          "Finca",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getFinca()
              : null);
      this.add(
          "Localidad Registro",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getLocalidadRegistro()
              : null);
      this.add(
          "Registro",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getRegistro()
              : null);
      this.add(
          "Tomo",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getTomo()
              : null);
      this.add(
          "Libro",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getLibro()
              : null);
      this.add(
          "Folio",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getFolio()
              : null);
      this.add(
          "Fecha Inscripcion",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getFechaInscripcion()
              : null);
      this.add(
          "Idufir",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getIdufir()
              : null);
      this.add(
          "Reo Origen",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getReoOrigen()
              : null);
      this.add(
          "Fecha Reo Conversion",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getFechaReoConversion()
              : null);
      this.add(
          "Importe Garantizado",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getImporteGarantizado()
              : null);
      this.add(
          "Responsabilidad Hipotecaria",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getResponsabilidadHipotecaria()
              : null);
      this.add(
          "Posesion Negociada",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getPais()
              : null);
      if (bienPosesionNegociada.getBien() != null
          && bienPosesionNegociada.getBien().getSubasta() != null)
        this.add("Subasta", bienPosesionNegociada.getBien().getSubasta() ? "Si" : "No");
      this.add(
          "Tipo Via",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getTipoVia()
              : null);
      this.add(
          "Nombre Via",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getNombreVia()
              : null);
      this.add(
          "Numero",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getNumero()
              : null);
      this.add(
          "Portal",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getPortal()
              : null);
      this.add(
          "Bloque",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getBloque()
              : null);
      this.add(
          "Escalera",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getEscalera()
              : null);
      this.add(
          "Piso",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getPiso()
              : null);
      this.add(
          "Puerta",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getPuerta()
              : null);
      this.add(
          "Entorno",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getEntorno()
              : null);
      this.add(
          "Municipio",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getMunicipio()
              : null);
      this.add(
          "Localidad",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getLocalidad()
              : null);
      this.add(
          "Provincia",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getProvincia()
              : null);
      this.add(
          "Comentario Direccion",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getComentarioDireccion()
              : null);
      this.add(
          "Codigo Postal",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getCodigoPostal()
              : null);
      this.add(
          "Pais",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getPais()
              : null);
      this.add(
          "Longitud",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getLongitud()
              : null);
      this.add(
          "Latitud",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getLatitud()
              : null);
      this.add(
          "Tipo Activo",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getTipoActivo()
              : null);
      this.add(
          "Uso",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getUso()
              : null);
      this.add(
          "Año Construccion",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getAnioConstruccion()
              : null);
      this.add(
          "Superficie Suelo",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getSuperficieSuelo()
              : null);
      this.add(
          "Superficie Construida",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getSuperficieConstruida()
              : null);
      this.add(
          "Superficie Util",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getSuperficieUtil()
              : null);
      this.add(
          "Anejo Garaje",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getAnejoGaraje()
              : null);
      this.add(
          "Anejo Trastero",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getAnejoTrastero()
              : null);
      this.add(
          "Anejo Otros",
          bienPosesionNegociada.getBien() != null
              ? bienPosesionNegociada.getBien().getAnejoOtros()
              : null);

      String campanias = "";
      for (CampaniaAsignadaBienPN campaniaAsignadaPN :
          bienPosesionNegociada.getCampaniasAsignadasPN()) {
        String nombre_campania = campaniaAsignadaPN.getCampania().getNombre();
        if (nombre_campania != null) {
          campanias += nombre_campania + ";";
        }
      }
      this.add("Campañas", campanias != null ? campanias : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
