package com.haya.alaska.bien_posesion_negociada.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.situacion.domain.Situacion;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;
import com.haya.alaska.visita.domain.Visita;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_BIEN_POSESION_NEGOCIADA")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_BIEN_POSESION_NEGOCIADA")
public class BienPosesionNegociada implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_EXPEDIENTE_POSESION_NEGOCIADA")
  ExpedientePosesionNegociada expedientePosesionNegociada;

  @OneToOne(
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN", nullable = false)
  private Bien bien;


  @Column(name = "ACTIVO")
  Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_OCUPACION")
  EstadoOcupacion estadoOcupacion;


  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN_POSESION_NEGOCIADA")
  @NotAudited
  private Set<OcupanteBien> ocupantes = new HashSet<>();

  @OneToMany
  @JoinColumn(name = "ID_BIEN_POSESION_NEGOCIADA")
  @NotAudited
  Set<Visita> visitas = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  Set<ProcedimientoPosesionNegociada> procedimientos = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_GARANTIA")
  private TipoGarantia tipoGarantia;

  @ManyToMany
  @JoinTable(name = "RELA_BIEN_POSESION_NEGOCIADA_IMPORTE_PROPUESTA",
    joinColumns = {@JoinColumn(name = "ID_BIEN_POSESION_NEGOCIADA")},
    inverseJoinColumns = {@JoinColumn(name = "ID_IMPORTE_PROPUESTA")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_BIEN_POSESION_NEGOCIADA_IMPORTE_PROPUESTA")
  private Set<ImportePropuesta> importes = new HashSet<>();

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(name = "RELA_BIEN_POSESION_NEGOCIADA_ACUERDO_PAGO",
    joinColumns = {@JoinColumn(name = "ID_BIEN_POSESION_NEGOCIADA")},
    inverseJoinColumns = {@JoinColumn(name = "ID_ACUERDO_PAGO")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_BIEN_POSESION_NEGOCIADA_ACUERDO_PAGO")
  private Set<AcuerdoPago> acuerdos;

  @OneToOne()
  @JoinColumn(name = "ID_ESTRATEGIA_ASIGNADA")
  private EstrategiaAsignada estrategia;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SITUACION")
  private Situacion situacion;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN_POSESION_NEGOCIADA")
  @NotAudited
  private Set<CampaniaAsignadaBienPN> campaniasAsignadasPN = new HashSet<>();
//Nuevos campos
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ESTADO")
  private TipoEstado tipoEstado;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_SUBESTADO")
  private SubtipoEstado subEstado;

  @Override
  public boolean equals(Object o) {
    BienPosesionNegociada bienPosesionNegociada = (BienPosesionNegociada) o;
    if (this.getBien().getReferenciaCatastralNull().equals(bienPosesionNegociada.getBien().getReferenciaCatastralNull())
      && (this.getBien().getIdufirNull().equals(bienPosesionNegociada.getBien().getIdufirNull())
      && this.getBien().getRegistroNull().equals(bienPosesionNegociada.getBien().getRegistroNull())
      && this.getBien().getFechaInscripcionNull() == bienPosesionNegociada.getBien().getFechaInscripcionNull()
      && this.getBien().getFincaNull().equals(bienPosesionNegociada.getBien().getFincaNull())
      && this.getBien().getTomoNull().equals(bienPosesionNegociada.getBien().getTomoNull())
      && this.getBien().getLibroNull().equals(bienPosesionNegociada.getBien().getLibroNull())
      && this.getBien().getFolioNull().equals(bienPosesionNegociada.getBien().getFolioNull()))) {
      return true;
    }
    return false;
  }
}
