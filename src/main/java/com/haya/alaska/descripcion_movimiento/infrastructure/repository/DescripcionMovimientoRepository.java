package com.haya.alaska.descripcion_movimiento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.descripcion_movimiento.domain.DescripcionMovimiento;

@Repository
public interface DescripcionMovimientoRepository  extends CatalogoRepository<DescripcionMovimiento, Integer> {}
