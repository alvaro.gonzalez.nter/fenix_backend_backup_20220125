package com.haya.alaska.descripcion_movimiento.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.movimiento.domain.Movimiento;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_DESCRIPCION_MOVIMIENTO")
@Entity
@Table(name = "LKUP_DESCRIPCION_MOVIMIENTO")
public class DescripcionMovimiento extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_DESCRIPCION_MOVIMIENTO")
  @NotAudited
  private Set<Movimiento> movimientos = new HashSet<>();
}
