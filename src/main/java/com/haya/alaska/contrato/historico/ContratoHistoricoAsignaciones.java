package com.haya.alaska.contrato.historico;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ContratoHistoricoAsignaciones {

  private Date fechaInicio;
  private Date fechaFin;
  private Integer revInfoInicio;
  private Integer revInfoFin;
  private String expedienteId;
  private String usuarioGestor;

  public ContratoHistoricoAsignaciones(Date fechaInicio, Date fechaFin, Integer revInfoInicio, Integer revInfoFin, String expedienteId, String usuarioGestor) {
    this.fechaInicio = fechaInicio;
    this.fechaFin = fechaFin;
    this.revInfoInicio = revInfoInicio;
    this.revInfoFin = revInfoFin;
    this.expedienteId = expedienteId;
    this.usuarioGestor = usuarioGestor;
  }
}
