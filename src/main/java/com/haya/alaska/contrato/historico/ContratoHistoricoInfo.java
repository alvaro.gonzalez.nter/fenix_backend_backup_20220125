package com.haya.alaska.contrato.historico;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ContratoHistoricoInfo {

  private Integer idContrato;
  private Integer revInfo;
  private Boolean activo;
  private BigInteger fecha;
  private String usuario;
  private String expedienteId;
  private Integer idEx;
  private String usuarioGestor;

  public ContratoHistoricoInfo(Integer idContrato, Integer revInfo, Boolean activo, BigInteger fecha, String usuario, String expedienteId, String usuarioGestor) {
    this.idContrato = idContrato;
    this.revInfo = revInfo;
    this.activo = activo;
    this.fecha = fecha;
    this.usuario = usuario;
    this.expedienteId = expedienteId;
    this.usuarioGestor = usuarioGestor;
  }

  public Date parseDate() {
    long longFecha = this.fecha.longValue();
    return new Date(longFecha);
  }
}
