package com.haya.alaska.contrato.domain;

import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ContratoMovimientoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Loan Id");
      cabeceras.add("Connection Id");
      cabeceras.add("Loan Type");
      cabeceras.add("Loan Status");
      cabeceras.add("Accounting Status");
      cabeceras.add("Date Value Balance");
      cabeceras.add("Principal Outstanding Due Balance");
      cabeceras.add("Unpaid Balance");
      cabeceras.add("Principal in Non-Payment");
      cabeceras.add("Ordinary interest on non-payment");
      cabeceras.add("Interest on delay");
      cabeceras.add("Commissions on non-payment");
      cabeceras.add("Unpaid Expenses");
      cabeceras.add("Judicial Expenses");
      cabeceras.add("Balance In Balance Management");
      cabeceras.add("Type"); // //^ saldo     v movimiento
      cabeceras.add("Description");

      cabeceras.add("Value Date");
      cabeceras.add("Accounting Date");
      cabeceras.add("Sense");
      cabeceras.add("Amount");
      cabeceras.add("Main Capital");
      cabeceras.add("Ordinary Interest");
      cabeceras.add("Interest Delay");
      cabeceras.add("Commissions");
      cabeceras.add("Expenses");
      cabeceras.add("Taxes");
      cabeceras.add("Principal Pending Overcoming");
      cabeceras.add("Unpaid Debt");
      cabeceras.add("Balance Under Management");
      cabeceras.add("User");
      cabeceras.add("Recovered Import");
      cabeceras.add("Output Doubtful");
      cabeceras.add("Invoiced Amount");
      cabeceras.add("Invoice Number");
      cabeceras.add("Expense detail");
      cabeceras.add("Movement Id");
      cabeceras.add("Affects");
      cabeceras.add("Status");
      cabeceras.add("Comments");
    } else {
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Expediente");
      cabeceras.add("Tipo Contrato");
      cabeceras.add("Estado Contrato");
      cabeceras.add("Estado Contable");
      cabeceras.add("Fecha Valor Saldo");
      cabeceras.add("Principal Pendiente Vencer Saldo");
      cabeceras.add("Saldo En Impago");
      cabeceras.add("Principal En Impago");
      cabeceras.add("Intereses Ordinarios En Impago");
      cabeceras.add("Intereses De Demora");
      cabeceras.add("Comisiones En Impago");
      cabeceras.add("Gastos En Impago");
      cabeceras.add("Gastos Judiciales");
      cabeceras.add("Saldo En Gestion Saldo");
      cabeceras.add("Tipo"); // //^ saldo     v movimiento
      cabeceras.add("Descripcion");

      cabeceras.add("Fecha Valor");
      cabeceras.add("Fecha Contable");
      cabeceras.add("Sentido");
      cabeceras.add("Importe");
      cabeceras.add("Principal Capital");
      cabeceras.add("Intereses Ordinarios");
      cabeceras.add("Intereses Demora");
      cabeceras.add("Comisiones");
      cabeceras.add("Gastos");
      cabeceras.add("Impuestos");
      cabeceras.add("Principal Pendiente Vencer");
      cabeceras.add("Deuda Impagada");
      cabeceras.add("Saldo En Gestion");
      cabeceras.add("Usuario");
      cabeceras.add("Importe Recuperado");
      cabeceras.add("Salida Dudoso");
      cabeceras.add("Importe Facturable");
      cabeceras.add("Numero Factura");
      cabeceras.add("Detalle gasto");
      cabeceras.add("Id Movimiento");
      cabeceras.add("Afecta");
      cabeceras.add("Estado");
      cabeceras.add("Comentarios");
    }
  }

  public ContratoMovimientoExcel(Contrato contrato, Movimiento movimiento, Saldo saldo) {

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Loan Id", contrato.getIdCarga());
      this.add("Connection Id", contrato.getExpediente().getIdConcatenado());
      this.add("Loan Type", contrato.getPrimerInterviniente().getTipoContrato());
      this.add("Loan Status", contrato.getEstadoContrato().getValorIngles());
      this.add("Accounting Status", contrato.getClasificacionContable().getValorIngles());
      this.add("Date Value Balance", saldo.getDia());
      this.add(
          "Principal Outstanding Due Balance",
          saldo.getCapitalPendiente() != null ? saldo.getCapitalPendiente() : "0.0");
      this.add(
          "Unpaid Balance", saldo.getDeudaImpagada() != null ? saldo.getDeudaImpagada() : "0.0");
      this.add(
          "Principal in Non-Payment",
          saldo.getPrincipalVencidoImpagado() != null
              ? saldo.getPrincipalVencidoImpagado()
              : "0.0");
      this.add(
          "Ordinary interest on non-payment",
          saldo.getInteresesOrdinariosImpagados() != null
              ? saldo.getInteresesOrdinariosImpagados()
              : "0.0");
      this.add(
          "Interest on delay",
          saldo.getInteresesDemora() != null ? saldo.getInteresesDemora() : "0.0");
      this.add(
          "Commissions on non-payment",
          saldo.getComisionesImpagados() != null ? saldo.getComisionesImpagados() : "0.0");
      this.add(
          "Unpaid Expenses",
          saldo.getGastosImpagados() != null ? saldo.getGastosImpagados() : "0.0");
      this.add(
          "Judicial Expenses",
          contrato.getGastosJudiciales() != null ? contrato.getGastosJudiciales() : "0.0");
      this.add(
          "Balance In Balance Management",
          saldo.getSaldoGestion() != null ? saldo.getSaldoGestion() : "0.0");
      this.add("Type", movimiento.getTipo() != null ? movimiento.getTipo().getValorIngles() : null);
      this.add("Description", contrato.getDescripcionProducto());

      this.add("Value Date", movimiento.getFechaValor());
      this.add("Accounting Date", movimiento.getFechaContable());
      this.add("Sense", movimiento.getSentido());
      this.add("Amount", movimiento.getImporte() != null ? movimiento.getImporte() : "0.0");
      this.add(
          "Main Capital",
          movimiento.getPrincipalCapital() != null ? movimiento.getPrincipalCapital() : "0.0");
      this.add(
          "Ordinary Interest",
          movimiento.getInteresesOrdinarios() != null
              ? movimiento.getInteresesOrdinarios()
              : "0.0");
      this.add(
          "Interest Delay",
          movimiento.getInteresesDemora() != null ? movimiento.getInteresesDemora() : "0.0");
      this.add(
          "Commissions", movimiento.getComisiones() != null ? movimiento.getComisiones() : "0.0");
      this.add("Expenses", movimiento.getGastos() != null ? movimiento.getGastos() : "0.0");
      this.add("Taxes", movimiento.getImpuestos() != null ? movimiento.getImpuestos() : "0.0");
      this.add(
          "Principal Pending Overcoming",
          movimiento.getPrincipalPendienteVencer() != null
              ? movimiento.getPrincipalPendienteVencer()
              : "0.0");
      this.add(
          "Unpaid Debt", contrato.getDeudaImpagada() != null ? contrato.getDeudaImpagada() : "0.0");
      this.add(
          "Balance Under Management",
          contrato.getSaldoGestion() != null ? contrato.getSaldoGestion() : "0.0");
      this.add("User", movimiento.getUsuario());
      this.add(
          "Recovered Import",
          movimiento.getImporteRecuperado() != null ? movimiento.getImporteRecuperado() : "0.0");
      this.add(
          "Output Doubtful",
          movimiento.getSalidaDudoso() != null ? movimiento.getSalidaDudoso() : "0.0");
      this.add(
          "Invoiced Amount",
          movimiento.getImporteFacturable() != null ? movimiento.getImporteFacturable() : "0.0");
      this.add("Invoice Number", movimiento.getNumeroFactura());
      this.add("Expense detail", movimiento.getDetalleGasto());
      this.add("Movement Id", movimiento.getId());
      this.add("Affects", movimiento.getAfecta());
      this.add("Status", contrato.getEstadoContrato());
      this.add("Comments", movimiento.getComentarios());
    } else {
      this.add("Id Contrato", contrato.getIdCarga());
      this.add("Id Expediente", contrato.getExpediente().getIdConcatenado());
      this.add("Tipo Contrato", contrato.getPrimerInterviniente().getTipoContrato());
      this.add("Estado Contrato", contrato.getEstadoContrato().getValor());
      this.add("Estado Contable", contrato.getClasificacionContable().getValor());
      this.add("Fecha Valor Saldo", saldo.getDia());
      this.add(
          "Principal Pendiente Vencer Saldo",
          saldo.getCapitalPendiente() != null ? saldo.getCapitalPendiente() : "0.0");
      this.add(
          "Saldo En Impago", saldo.getDeudaImpagada() != null ? saldo.getDeudaImpagada() : "0.0");
      this.add(
          "Principal En Impago",
          saldo.getPrincipalVencidoImpagado() != null
              ? saldo.getPrincipalVencidoImpagado()
              : "0.0");
      this.add(
          "Intereses Ordinarios En Impago",
          saldo.getInteresesOrdinariosImpagados() != null
              ? saldo.getInteresesOrdinariosImpagados()
              : "0.0");
      this.add(
          "Intereses De Demora",
          saldo.getInteresesDemora() != null ? saldo.getInteresesDemora() : "0.0");
      this.add(
          "Comisiones En Impago",
          saldo.getComisionesImpagados() != null ? saldo.getComisionesImpagados() : "0.0");
      this.add(
          "Gastos En Impago",
          saldo.getGastosImpagados() != null ? saldo.getGastosImpagados() : "0.0");
      this.add(
          "Gastos Judiciales",
          contrato.getGastosJudiciales() != null ? contrato.getGastosJudiciales() : "0.0");
      this.add(
          "Saldo En Gestion Saldo",
          saldo.getSaldoGestion() != null ? saldo.getSaldoGestion() : "0.0");
      this.add("Tipo", movimiento.getTipo() != null ? movimiento.getTipo().getValor() : null);
      this.add("Descripcion", contrato.getDescripcionProducto());

      this.add("Fecha Valor", movimiento.getFechaValor());
      this.add("Fecha Contable", movimiento.getFechaContable());
      this.add("Sentido", movimiento.getSentido());
      this.add("Importe", movimiento.getImporte() != null ? movimiento.getImporte() : "0.0");
      this.add(
          "Principal Capital",
          movimiento.getPrincipalCapital() != null ? movimiento.getPrincipalCapital() : "0.0");
      this.add(
          "Intereses Ordinarios",
          movimiento.getInteresesOrdinarios() != null
              ? movimiento.getInteresesOrdinarios()
              : "0.0");
      this.add(
          "Intereses Demora",
          movimiento.getInteresesDemora() != null ? movimiento.getInteresesDemora() : "0.0");
      this.add(
          "Comisiones", movimiento.getComisiones() != null ? movimiento.getComisiones() : "0.0");
      this.add("Gastos", movimiento.getGastos() != null ? movimiento.getGastos() : "0.0");
      this.add("Impuestos", movimiento.getImpuestos() != null ? movimiento.getImpuestos() : "0.0");
      this.add(
          "Principal Pendiente Vencer",
          movimiento.getPrincipalPendienteVencer() != null
              ? movimiento.getPrincipalPendienteVencer()
              : "0.0");
      this.add(
          "Deuda Impagada",
          contrato.getDeudaImpagada() != null ? contrato.getDeudaImpagada() : "0.0");
      this.add(
          "Saldo En Gestion",
          contrato.getSaldoGestion() != null ? contrato.getSaldoGestion() : "0.0");
      this.add("Usuario", movimiento.getUsuario());
      this.add(
          "Importe Recuperado",
          movimiento.getImporteRecuperado() != null ? movimiento.getImporteRecuperado() : "0.0");
      this.add(
          "Salida Dudoso",
          movimiento.getSalidaDudoso() != null ? movimiento.getSalidaDudoso() : "0.0");
      this.add(
          "Importe Facturable",
          movimiento.getImporteFacturable() != null ? movimiento.getImporteFacturable() : "0.0");
      this.add("Numero Factura", movimiento.getNumeroFactura());
      this.add("Detalle gasto", movimiento.getDetalleGasto());
      this.add("Id Movimiento", movimiento.getId());
      this.add("Afecta", movimiento.getAfecta());
      this.add("Estado", contrato.getEstadoContrato());
      this.add("Comentarios", movimiento.getComentarios());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
