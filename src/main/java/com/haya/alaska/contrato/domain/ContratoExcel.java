package com.haya.alaska.contrato.domain;

import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

public class ContratoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Loan Id");
      cabeceras.add("Borrower Id");
      cabeceras.add("Origin Id");
      cabeceras.add("Status");
      cabeceras.add("Product");
      cabeceras.add("Amortization System");
      cabeceras.add("Settlement Periodicity");
      cabeceras.add("Situation");
      cabeceras.add("Reference Index");
      cabeceras.add("Differential");
      cabeceras.add("Tin");
      cabeceras.add("Tae");
      cabeceras.add("Mortgage Range");
      cabeceras.add("Restructured");
      cabeceras.add("Restructured Type");
      cabeceras.add("Accounting Classification");
      cabeceras.add("Campaign");
      cabeceras.add("Formalization Date");
      cabeceras.add("Initial Due Date");
      cabeceras.add("Early Expiration Date");
      cabeceras.add("Regular Pass Date");
      cabeceras.add("Outstanding Installments");
      cabeceras.add("Quota Settlement Date");
      cabeceras.add("Initial Amount");
      cabeceras.add("Amount Drawn");
      cabeceras.add("Capital Pending");
      cabeceras.add("Management Balance");
      cabeceras.add("Total Fees");
      cabeceras.add("Last Installment Amount");
      cabeceras.add("Date of First Non-Payment");
      cabeceras.add("Number of Unpaid Installments");
      cabeceras.add("Unpaid Debt");
      cabeceras.add("Remaining Mortgage");
      cabeceras.add("Origin");
      cabeceras.add("Principal Due Unpaid");
      cabeceras.add("Unpaid Ordinary Interest");
      cabeceras.add("Interest Delayed");
      cabeceras.add("Unpaid Commissions");
      cabeceras.add("Unpaid Expenses");
      cabeceras.add("Judicial Expenses");
      cabeceras.add("Amount of Unpaid Fees");
      cabeceras.add("Additional Data");
      cabeceras.add("Additional Data 2");
    } else {
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Origen");
      cabeceras.add("Estado");
      cabeceras.add("Producto");
      cabeceras.add("Sistema Amortizacion");
      cabeceras.add("Periodicidad Liquidacion");
      cabeceras.add("Situacion");
      cabeceras.add("Indice Referencia");
      cabeceras.add("Diferencial");
      cabeceras.add("Tin");
      cabeceras.add("Tae");
      cabeceras.add("Rango Hipotecario");
      cabeceras.add("Reestructurado");
      cabeceras.add("Tipo Reestructuracion");
      cabeceras.add("Clasificacion Contable");
      cabeceras.add("Campaña");
      cabeceras.add("Fecha Formalizacion");
      cabeceras.add("Fecha Vencimiento Inicial");
      cabeceras.add("Fecha Vencimiento Anticipado");
      cabeceras.add("Fecha Pase Regular");
      cabeceras.add("Cuotas Pendientes");
      cabeceras.add("Fecha Liquidacion Cuota");
      cabeceras.add("Importe Inicial");
      cabeceras.add("Importe Dispuesto");
      cabeceras.add("Capital Pendiente");
      cabeceras.add("Saldo Gestión");
      cabeceras.add("Cuotas Totales");
      cabeceras.add("Importe Ultima Cuota");
      cabeceras.add("Fecha Primer Impago");
      cabeceras.add("Numero Cuotas Impagadas");
      cabeceras.add("Deuda Impagada");
      cabeceras.add("Resto Hipotecario");
      cabeceras.add("Origen");
      cabeceras.add("Principal Vencido Impagado");
      cabeceras.add("Intereses Ordinarios Impagados");
      cabeceras.add("Intereses Demora");
      cabeceras.add("Comisiones Impagados");
      cabeceras.add("Gastos Impagados");
      cabeceras.add("Gastos Judiciales");
      cabeceras.add("Importe Cuotas Impagadas");
      cabeceras.add("Datos adicionales");
      cabeceras.add("Datos adicionales 2");
    }
  }

  public ContratoExcel(Contrato contrato) {
    super();

    Saldo saldoContrato = contrato.getSaldoContrato();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Loan Ido", contrato.getId());
      this.add(
          "Borrower Id",
          contrato.getExpediente() != null ? contrato.getExpediente().getIdConcatenado() : null);
      this.add("Origin Id", contrato.getIdCarga());
      this.add("Status", contrato.getActivo() ? "Active" : "Closed");
      this.add("Product", contrato.getProducto());
      this.add("Amortization System", contrato.getSistemaAmortizacion());
      this.add("Settlement Periodicity", contrato.getPeriodicidadLiquidacion());
      this.add("Reference Index", contrato.getIndiceReferencia());
      this.add("Differential", contrato.getDiferencial());
      this.add("Tin", contrato.getTin());
      this.add("Tae", contrato.getTae());
      this.add("Mortgage Range", contrato.getRangoHipotecario());
      this.add("Restructured", contrato.getReestructurado());
      this.add("Restructured Type", contrato.getTipoReestructuracion());
      this.add("Accounting Classification", contrato.getClasificacionContable());
      String campanias = "";
      List<CampaniaAsignada> campaniaTemp = new ArrayList<>(contrato.getCampaniasAsignadas());
      for (int i = 0; i < campaniaTemp.size(); i++) {
        campanias += campaniaTemp.get(i).getCampania().getNombre();
        if (campaniaTemp.size() - 1 != i) {
          campanias += ", ";
        }
      }
      this.add("Campaign", campanias);
      this.add("Formalization Date", contrato.getFechaFormalizacion());
      this.add("Initial Due Date", contrato.getFechaVencimientoInicial());
      this.add("Early Expiration Date", contrato.getFechaVencimientoAnticipado());
      this.add("Regular Pass Date", contrato.getFechaPaseRegular());
      this.add("Outstanding Installments", contrato.getCuotasPendientes());
      this.add("Quota Settlement Date", contrato.getFechaLiquidacionCuota());
      this.add("Initial Amount", contrato.getImporteInicial());
      this.add("Amount Drawn", contrato.getImporteDispuesto());
      this.add("Capital Pending", saldoContrato.getCapitalPendiente());
      this.add("Management Balance", saldoContrato.getSaldoGestion());
      this.add("Total Fees", contrato.getCuotasTotales());
      this.add("Last Installment Amount", contrato.getImporteUltimaCuota());
      this.add("Date of First Non-Payment", contrato.getFechaPrimerImpago());
      this.add("Number of Unpaid Installments", contrato.getNumeroCuotasImpagadas());
      this.add("Unpaid Debt", contrato.getDeudaImpagada());
      this.add("Remaining Mortgage", contrato.getRestoHipotecario());
      this.add("Origin", contrato.getOrigen());
      this.add("Principal Due Unpaid", saldoContrato.getPrincipalVencidoImpagado());
      this.add("Unpaid Ordinary Interest", saldoContrato.getInteresesOrdinariosImpagados());
      this.add("Interest Delayed", saldoContrato.getInteresesDemora());
      this.add("Unpaid Commissions", saldoContrato.getComisionesImpagados());
      this.add("Unpaid Expenses", saldoContrato.getGastosImpagados());
      this.add("Judicial Expenses", contrato.getGastosJudiciales());
      this.add("Amount of Unpaid Fees", contrato.getImporteCuotasImpagadas());
      this.add("Additional Data", contrato.getNotas1());
      this.add("Additional Data 2", contrato.getNotas2());
    } else {
      this.add("Id Contrato", contrato.getId());
      this.add(
          "Id Expediente",
          contrato.getExpediente() != null ? contrato.getExpediente().getIdConcatenado() : null);
      this.add("Id Origen", contrato.getIdCarga());
      this.add("Estado", contrato.getActivo() ? "Activo" : "Cerrado");
      this.add("Producto", contrato.getProducto());
      this.add("Sistema Amortizacion", contrato.getSistemaAmortizacion());
      this.add("Periodicidad Liquidacion", contrato.getPeriodicidadLiquidacion());
      this.add("Indice Referencia", contrato.getIndiceReferencia());
      this.add("Diferencial", contrato.getDiferencial());
      this.add("Tin", contrato.getTin());
      this.add("Tae", contrato.getTae());
      this.add("Rango Hipotecario", contrato.getRangoHipotecario());
      this.add("Reestructurado", contrato.getReestructurado());
      this.add("Tipo Reestructuracion", contrato.getTipoReestructuracion());
      this.add("Clasificacion Contable", contrato.getClasificacionContable());
      String campanias = "";
      List<CampaniaAsignada> campaniaTemp = new ArrayList<>(contrato.getCampaniasAsignadas());
      for (int i = 0; i < campaniaTemp.size(); i++) {
        campanias += campaniaTemp.get(i).getCampania().getNombre();
        if (campaniaTemp.size() - 1 != i) {
          campanias += ", ";
        }
      }
      this.add("Campaña", campanias);
      this.add("Fecha Formalizacion", contrato.getFechaFormalizacion());
      this.add("Fecha Vencimiento Inicial", contrato.getFechaVencimientoInicial());
      this.add("Fecha Vencimiento Anticipado", contrato.getFechaVencimientoAnticipado());
      this.add("Fecha Pase Regular", contrato.getFechaPaseRegular());
      this.add("Cuotas Pendientes", contrato.getCuotasPendientes());
      this.add("Fecha Liquidacion Cuota", contrato.getFechaLiquidacionCuota());
      this.add("Importe Inicial", contrato.getImporteInicial());
      this.add("Importe Dispuesto", contrato.getImporteDispuesto());
      this.add("Capital Pendiente", saldoContrato.getCapitalPendiente());
      this.add("Saldo Gestión", saldoContrato.getSaldoGestion());
      this.add("Cuotas Totales", contrato.getCuotasTotales());
      this.add("Importe Ultima Cuota", contrato.getImporteUltimaCuota());
      this.add("Fecha Primer Impago", contrato.getFechaPrimerImpago());
      this.add("Numero Cuotas Impagadas", contrato.getNumeroCuotasImpagadas());
      this.add("Deuda Impagada", contrato.getDeudaImpagada());
      this.add("Resto Hipotecario", contrato.getRestoHipotecario());
      this.add("Origen", contrato.getOrigen());
      this.add("Principal Vencido Impagado", saldoContrato.getPrincipalVencidoImpagado());
      this.add("Intereses Ordinarios Impagados", saldoContrato.getInteresesOrdinariosImpagados());
      this.add("Intereses Demora", saldoContrato.getInteresesDemora());
      this.add("Comisiones Impagados", saldoContrato.getComisionesImpagados());
      this.add("Gastos Impagados", saldoContrato.getGastosImpagados());
      this.add("Gastos Judiciales", contrato.getGastosJudiciales());
      this.add("Importe Cuotas Impagadas", contrato.getImporteCuotasImpagadas());
      this.add("Datos adicionales", contrato.getNotas1());
      this.add("Datos adicionales 2", contrato.getNotas2());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
