package com.haya.alaska.contrato.domain;

import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ContratoCarteraExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Loan Id");
      cabeceras.add("Connection Id");
      cabeceras.add("Origin Id");
      cabeceras.add("Status");
      cabeceras.add("Product");
      cabeceras.add("Amortization System");
      cabeceras.add("Settlement Periodicity");
      cabeceras.add("Reference Index");
      cabeceras.add("Differential");
      cabeceras.add("Tin");
      cabeceras.add("Tae");
      cabeceras.add("Mortgage Range");
      cabeceras.add("Restructured");
      cabeceras.add("Restructured Type");
      cabeceras.add("Accounting Classification");
      cabeceras.add("Formalization Date");
      cabeceras.add("Initial Maturity Date");
      cabeceras.add("Anticipated Maturity Date");
      cabeceras.add("Regular Pass Date");
      cabeceras.add("Outstanding Installments");
      cabeceras.add("Quota Settlement Date");
      cabeceras.add("Initial Amount");
      cabeceras.add("Amount Drawn");
      cabeceras.add("Capital Pending");
      cabeceras.add("Management Balance");
      cabeceras.add("Total Fees");
      cabeceras.add("Last Installment Amount");
      cabeceras.add("Date of First Non-Payment");
      cabeceras.add("Number of Unpaid Installments");
      cabeceras.add("Unpaid Debt");
      cabeceras.add("Remaining Mortgage");
      cabeceras.add("Origin");
      cabeceras.add("Principal Due Unpaid");
      cabeceras.add("Unpaid Ordinary Interest");
      cabeceras.add("Interest Delayed");
      cabeceras.add("Unpaid Commissions");
      cabeceras.add("Unpaid Expenses");
      cabeceras.add("Judicial Expenses");
      cabeceras.add("Amount of Unpaid Fees");
      cabeceras.add("Connection Manager");
      cabeceras.add("Connection Status");
      cabeceras.add("Connection Sub-Status");
      cabeceras.add("Connection Situation");
      cabeceras.add("Proposal");
      cabeceras.add("Proposal Status");
      cabeceras.add("Action");
      cabeceras.add("Action Date");
      cabeceras.add("Alert");
      cabeceras.add("Date Alert");
      cabeceras.add("Activity");
      cabeceras.add("Date Activity");
      cabeceras.add("Date");
    } else {
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Origen");
      cabeceras.add("Estado");
      cabeceras.add("Producto");
      cabeceras.add("Sistema Amortizacion");
      cabeceras.add("Periodicidad Liquidacion");
      cabeceras.add("Indice Referencia");
      cabeceras.add("Diferencial");
      cabeceras.add("Tin");
      cabeceras.add("Tae");
      cabeceras.add("Rango Hipotecario");
      cabeceras.add("Reestructurado");
      cabeceras.add("Tipo Reestructuracion");
      cabeceras.add("Clasificacion Contable");
      cabeceras.add("Fecha Formalizacion");
      cabeceras.add("Fecha Vencimiento Inicial");
      cabeceras.add("Fecha Vencimiento Anticipado");
      cabeceras.add("Fecha Pase Regular");
      cabeceras.add("Cuotas Pendientes");
      cabeceras.add("Fecha Liquidacion Cuota");
      cabeceras.add("Importe Inicial");
      cabeceras.add("Importe Dispuesto");
      cabeceras.add("Capital Pendiente");
      cabeceras.add("Salgo Gestion");
      cabeceras.add("Cuotas Totales");
      cabeceras.add("Importe Ultima Cuota");
      cabeceras.add("Fecha Primer Impago");
      cabeceras.add("Numero Cuotas Impagadas");
      cabeceras.add("Deuda Impagada");
      cabeceras.add("Resto Hipotecario");
      cabeceras.add("Origen");
      cabeceras.add("Principal Vencido Impagado");
      cabeceras.add("Intereses Ordinarios Impagados");
      cabeceras.add("Intereses Demora");
      cabeceras.add("Comisiones Impagados");
      cabeceras.add("Gastos Impagados");
      cabeceras.add("Gastos Judiciales");
      cabeceras.add("Importe Cuotas Impagadas");
      cabeceras.add("Gestor Del Expediente");
      cabeceras.add("Estado Del Expediente");
      cabeceras.add("Subestado Del Expediente");
      cabeceras.add("Situación Del Expediente");
      cabeceras.add("Propuesta");
      cabeceras.add("Estado Propuesta");
      cabeceras.add("Acción");
      cabeceras.add("Fecha Acción");
      cabeceras.add("Alerta");
      cabeceras.add("Fecha Alerta");
      cabeceras.add("Actividad");
      cabeceras.add("Fecha Actividad");
      cabeceras.add("Fecha");
    }
  }

  public ContratoCarteraExcel(Contrato contrato, Evento accion, Evento alerta, Evento actividad) {
    super();

    Saldo saldoContrato = contrato.getSaldoContrato();

    var propuestas = contrato.getPropuestas();
    Propuesta propuestaBuena = null;
    for (Propuesta propuesta : propuestas) {
      propuestaBuena = propuesta;
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Loan Id", contrato.getId());
      this.add(
          "Connection Id",
          contrato.getExpediente() != null ? contrato.getExpediente().getIdConcatenado() : null);
      this.add("Origin Id", contrato.getIdCarga() != null ? contrato.getIdCarga() : null);
      this.add("Status", Boolean.TRUE.equals(contrato.getActivo()) ? "ACTIVE" : "NOT ACTIVE");
      this.add("Product", contrato.getProducto() != null ? contrato.getProducto() : null);
      this.add(
          "Amortization System",
          contrato.getSistemaAmortizacion() != null ? contrato.getSistemaAmortizacion() : null);
      this.add(
          "Settlement Periodicity",
          contrato.getPeriodicidadLiquidacion() != null
              ? contrato.getPeriodicidadLiquidacion()
              : null);
      this.add(
          "Reference Index",
          contrato.getIndiceReferencia() != null ? contrato.getIndiceReferencia() : null);
      this.add(
          "Differential", contrato.getDiferencial() != null ? contrato.getDiferencial() : null);
      this.add("Tin", contrato.getTin() != null ? contrato.getTin() : null);
      this.add("Tae", contrato.getTae() != null ? contrato.getTae() : null);
      this.add(
          "Mortgage Range",
          contrato.getRangoHipotecario() != null ? contrato.getRangoHipotecario() : null);
      this.add("Restructured", Boolean.TRUE.equals(contrato.getReestructurado()) ? "YES" : "NO");
      this.add(
          "Restructured Type",
          contrato.getTipoReestructuracion() != null
              ? contrato.getTipoReestructuracion().getValorIngles()
              : null);
      this.add(
          "Accounting Classification",
          contrato.getClasificacionContable() != null
              ? contrato.getClasificacionContable().getValorIngles()
              : null);
      this.add(
          "Formalization Date",
          contrato.getFechaFormalizacion() != null ? contrato.getFechaFormalizacion() : null);
      this.add(
          "Initial Maturity Date",
          contrato.getFechaVencimientoInicial() != null
              ? contrato.getFechaVencimientoInicial()
              : null);
      this.add(
          "Anticipated Maturity Date",
          contrato.getFechaVencimientoAnticipado() != null
              ? contrato.getFechaVencimientoAnticipado()
              : null);
      this.add(
          "Regular Pass Date",
          contrato.getFechaPaseRegular() != null ? contrato.getFechaPaseRegular() : null);
      this.add(
          "Outstanding Installments",
          contrato.getCuotasPendientes() != null ? contrato.getCuotasPendientes() : null);
      this.add(
          "Quota Settlement Date",
          contrato.getFechaLiquidacionCuota() != null ? contrato.getFechaLiquidacionCuota() : null);
      this.add(
          "Initial Amount",
          contrato.getImporteInicial() != null ? contrato.getImporteInicial() : null);
      this.add(
          "Amount Drawn",
          contrato.getImporteDispuesto() != null ? contrato.getImporteDispuesto() : null);
      this.add(
          "Capital Pending",
          saldoContrato.getCapitalPendiente() != null ? saldoContrato.getCapitalPendiente() : null);
      this.add(
          "Management Balance",
          saldoContrato.getSaldoGestion() != null ? saldoContrato.getSaldoGestion() : null);
      this.add(
          "Total Fees", contrato.getCuotasTotales() != null ? contrato.getCuotasTotales() : null);
      this.add(
          "Last Installment Amount",
          contrato.getImporteUltimaCuota() != null ? contrato.getImporteUltimaCuota() : null);
      this.add(
          "Date of First Non-Payment",
          contrato.getFechaPrimerImpago() != null ? contrato.getFechaPrimerImpago() : null);
      this.add(
          "Number of Unpaid Installments",
          contrato.getNumeroCuotasImpagadas() != null ? contrato.getNumeroCuotasImpagadas() : null);
      this.add(
          "Unpaid Debt", contrato.getDeudaImpagada() != null ? contrato.getDeudaImpagada() : null);
      this.add(
          "Remaining Mortgage",
          contrato.getRestoHipotecario() != null ? contrato.getRestoHipotecario() : null);
      this.add("Origin", contrato.getOrigen() != null ? contrato.getOrigen() : null);
      this.add(
          "Principal Due Unpaid",
          saldoContrato.getPrincipalVencidoImpagado() != null
              ? saldoContrato.getPrincipalVencidoImpagado()
              : null);
      this.add(
          "Unpaid Ordinary Interest",
          saldoContrato.getInteresesOrdinariosImpagados() != null
              ? saldoContrato.getInteresesOrdinariosImpagados()
              : null);
      this.add(
          "Interest Delayed",
          saldoContrato.getInteresesDemora() != null ? saldoContrato.getInteresesDemora() : null);
      this.add("Unpaid Commissions", saldoContrato.getComisionesImpagados());
      this.add(
          "Unpaid Expenses",
          saldoContrato.getGastosImpagados() != null ? saldoContrato.getGastosImpagados() : null);
      this.add(
          "Judicial Expenses",
          contrato.getGastosJudiciales() != null ? contrato.getGastosJudiciales() : null);
      this.add("Amount of Unpaid Fees", contrato.getImporteCuotasImpagadas());
      this.add(
          "Connection Manager",
          contrato.getExpediente().getGestor() != null
              ? contrato.getExpediente().getGestor().getNombre()
              : null);
      this.add(
          "Connection Status",
          contrato.getExpediente().getTipoEstado() != null
              ? contrato.getExpediente().getTipoEstado().getValorIngles()
              : null);
      this.add(
          "Connection Sub-Status",
          contrato.getExpediente().getSubEstado() != null
              ? contrato.getExpediente().getSubEstado().getValorIngles()
              : null);
      this.add(
          "Connection Situation",
          contrato.getSituacion() != null ? contrato.getSituacion().getValorIngles() : null);
      this.add(
          "Proposal",
          propuestaBuena != null ? propuestaBuena.getTipoPropuesta().getValorIngles() : null);
      this.add(
          "Proposal Status",
          propuestaBuena != null ? propuestaBuena.getEstadoPropuesta().getValorIngles() : null);
      this.add("Action", accion != null ? accion.getTitulo() : null);
      this.add("Action Date", accion != null ? accion.getFechaCreacion() : null);
      this.add("Alert", alerta != null ? alerta.getTitulo() : null);
      this.add("Date Alert", alerta != null ? alerta.getFechaCreacion() : null);
      this.add("Activity", actividad != null ? actividad.getTitulo() != null : null);
      this.add("Date Activity", actividad != null ? actividad.getFechaCreacion() : null);
      this.add("Date", contrato.getFechaCarga() != null ? contrato.getFechaCarga() : null);
    } else {
      this.add("Id Contrato", contrato.getId());
      this.add(
          "Id Expediente",
          contrato.getExpediente() != null ? contrato.getExpediente().getIdConcatenado() : null);
      this.add("Id Origen", contrato.getIdCarga() != null ? contrato.getIdCarga() : null);
      this.add("Estado", Boolean.TRUE.equals(contrato.getActivo()) ? "ACTIVO" : "NO ACTIVO");
      this.add("Producto", contrato.getProducto() != null ? contrato.getProducto() : null);
      this.add(
          "Sistema Amortizacion",
          contrato.getSistemaAmortizacion() != null ? contrato.getSistemaAmortizacion() : null);
      this.add(
          "Periodicidad Liquidacion",
          contrato.getPeriodicidadLiquidacion() != null
              ? contrato.getPeriodicidadLiquidacion()
              : null);
      this.add(
          "Indice Referencia",
          contrato.getIndiceReferencia() != null ? contrato.getIndiceReferencia() : null);
      this.add("Diferencial", contrato.getDiferencial() != null ? contrato.getDiferencial() : null);
      this.add("Tin", contrato.getTin() != null ? contrato.getTin() : null);
      this.add("Tae", contrato.getTae() != null ? contrato.getTae() : null);
      this.add(
          "Rango Hipotecario",
          contrato.getRangoHipotecario() != null ? contrato.getRangoHipotecario() : null);
      this.add("Reestructurado", Boolean.TRUE.equals(contrato.getReestructurado()) ? "SÍ" : "NO");
      this.add(
          "Tipo Reestructuracion",
          contrato.getTipoReestructuracion() != null
              ? contrato.getTipoReestructuracion().getValor()
              : null);
      this.add(
          "Clasificacion Contable",
          contrato.getClasificacionContable() != null
              ? contrato.getClasificacionContable().getValor()
              : null);
      this.add(
          "Fecha Formalizacion",
          contrato.getFechaFormalizacion() != null ? contrato.getFechaFormalizacion() : null);
      this.add(
          "Fecha Vencimiento Inicial",
          contrato.getFechaVencimientoInicial() != null
              ? contrato.getFechaVencimientoInicial()
              : null);
      this.add(
          "Fecha Vencimiento Anticipado",
          contrato.getFechaVencimientoAnticipado() != null
              ? contrato.getFechaVencimientoAnticipado()
              : null);
      this.add(
          "Fecha Pase Regular",
          contrato.getFechaPaseRegular() != null ? contrato.getFechaPaseRegular() : null);
      this.add(
          "Cuotas Pendientes",
          contrato.getCuotasPendientes() != null ? contrato.getCuotasPendientes() : null);
      this.add(
          "Fecha Liquidacion Cuota",
          contrato.getFechaLiquidacionCuota() != null ? contrato.getFechaLiquidacionCuota() : null);
      this.add(
          "Importe Inicial",
          contrato.getImporteInicial() != null ? contrato.getImporteInicial() : null);
      this.add(
          "Importe Dispuesto",
          contrato.getImporteDispuesto() != null ? contrato.getImporteDispuesto() : null);
      this.add(
          "Capital Pendiente",
          saldoContrato.getCapitalPendiente() != null ? saldoContrato.getCapitalPendiente() : null);
      this.add(
          "Salgo Gestion",
          saldoContrato.getSaldoGestion() != null ? saldoContrato.getSaldoGestion() : null);
      this.add(
          "Cuotas Totales",
          contrato.getCuotasTotales() != null ? contrato.getCuotasTotales() : null);
      this.add(
          "Importe Ultima Cuota",
          contrato.getImporteUltimaCuota() != null ? contrato.getImporteUltimaCuota() : null);
      this.add(
          "Fecha Primer Impago",
          contrato.getFechaPrimerImpago() != null ? contrato.getFechaPrimerImpago() : null);
      this.add(
          "Numero Cuotas Impagadas",
          contrato.getNumeroCuotasImpagadas() != null ? contrato.getNumeroCuotasImpagadas() : null);
      this.add(
          "Deuda Impagada",
          contrato.getDeudaImpagada() != null ? contrato.getDeudaImpagada() : null);
      this.add(
          "Resto Hipotecario",
          contrato.getRestoHipotecario() != null ? contrato.getRestoHipotecario() : null);
      this.add("Origen", contrato.getOrigen() != null ? contrato.getOrigen() : null);
      this.add(
          "Principal Vencido Impagado",
          saldoContrato.getPrincipalVencidoImpagado() != null
              ? saldoContrato.getPrincipalVencidoImpagado()
              : null);
      this.add(
          "Intereses Ordinarios Impagados",
          saldoContrato.getInteresesOrdinariosImpagados() != null
              ? saldoContrato.getInteresesOrdinariosImpagados()
              : null);
      this.add(
          "Intereses Demora",
          saldoContrato.getInteresesDemora() != null ? saldoContrato.getInteresesDemora() : null);
      this.add("Comisiones Impagados", saldoContrato.getComisionesImpagados());
      this.add(
          "Gastos Impagados",
          saldoContrato.getGastosImpagados() != null ? saldoContrato.getGastosImpagados() : null);
      this.add(
          "Gastos Judiciales",
          contrato.getGastosJudiciales() != null ? contrato.getGastosJudiciales() : null);
      this.add("Importe Cuotas Impagadas", contrato.getImporteCuotasImpagadas());
      this.add(
          "Gestor Del Expediente",
          contrato.getExpediente().getGestor() != null
              ? contrato.getExpediente().getGestor().getNombre()
              : null);
      this.add(
          "Estado Del Expediente",
          contrato.getExpediente().getTipoEstado() != null
              ? contrato.getExpediente().getTipoEstado().getValor()
              : null);
      this.add(
          "Subestado Del Expediente",
          contrato.getExpediente().getSubEstado() != null
              ? contrato.getExpediente().getSubEstado().getValor()
              : null);
      this.add(
          "Situación Del Expediente",
          contrato.getSituacion() != null ? contrato.getSituacion().getValor() : null);
      this.add(
          "Propuesta",
          propuestaBuena != null ? propuestaBuena.getTipoPropuesta().getValor() : null);
      this.add(
          "Estado Propuesta",
          propuestaBuena != null ? propuestaBuena.getEstadoPropuesta().getValor() : null);
      this.add("Acción", accion != null ? accion.getTitulo() : null);
      this.add("Fecha Acción", accion != null ? accion.getFechaCreacion() : null);
      this.add("Alerta", alerta != null ? alerta.getTitulo() : null);
      this.add("Fecha Alerta", alerta != null ? alerta.getFechaCreacion() : null);
      this.add("Actividad", actividad != null ? actividad.getTitulo() != null : null);
      this.add("Fecha Actividad", actividad != null ? actividad.getFechaCreacion() : null);
      this.add("Fecha", contrato.getFechaCarga() != null ? contrato.getFechaCarga() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
