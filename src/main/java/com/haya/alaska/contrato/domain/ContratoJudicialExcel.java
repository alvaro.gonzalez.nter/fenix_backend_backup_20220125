package com.haya.alaska.contrato.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ContratoJudicialExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Procedure Id");
      cabeceras.add("Principal Loan");
      cabeceras.add("Loan Id");
      cabeceras.add("Product");
      cabeceras.add("First Holder");
      cabeceras.add("Balance under management");
      cabeceras.add("Borrowers");
      cabeceras.add("Warranties");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Procedimeinto");
      cabeceras.add("Contrato Principal");
      cabeceras.add("Id Contrato");
      cabeceras.add("Producto");
      cabeceras.add("Primer Titular");
      cabeceras.add("Saldo en Gestion");
      cabeceras.add("Intervinientes");
      cabeceras.add("Garantias");
    }
  }

  public ContratoJudicialExcel(Contrato contrato, Integer idProcedimiento, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Connection Id", idExpediente);
      this.add("Procedure Id", idProcedimiento);
      this.add("Principal Loan", null);
      this.add("Loan Id", contrato.getIdCarga());
      this.add(
          "Product",
          contrato.getProducto() != null ? contrato.getProducto().getValorIngles() : null);
      this.add(
          "First Holder",
          contrato.getPrimerInterviniente() != null
              ? contrato.getPrimerInterviniente().getNombre()
                  + " "
                  + contrato.getPrimerInterviniente().getApellidos()
              : null);
      this.add("Balance under management", null);
      this.add("Borrowers", null);
      this.add("Warranties", contrato.getBienes() != null ? contrato.getBienes().size() : null);
    } else {
      this.add("Id Expediente", idExpediente);
      this.add("Id Procedimeinto", idProcedimiento);
      this.add("Contrato Principal", null);
      this.add("Id Contrato", contrato.getIdCarga());
      this.add(
          "Producto", contrato.getProducto() != null ? contrato.getProducto().getValor() : null);
      this.add(
          "Primer Titular",
          contrato.getPrimerInterviniente() != null
              ? contrato.getPrimerInterviniente().getNombre()
                  + " "
                  + contrato.getPrimerInterviniente().getApellidos()
              : null);
      this.add("Saldo en Gestion", null);
      this.add("Intervinientes", null);
      this.add("Garantias", contrato.getBienes() != null ? contrato.getBienes().size() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
