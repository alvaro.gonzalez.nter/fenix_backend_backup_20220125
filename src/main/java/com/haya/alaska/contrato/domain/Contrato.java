package com.haya.alaska.contrato.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.formalizacion.domain.FormalizacionBien;
import com.haya.alaska.formalizacion.domain.FormalizacionContrato;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.indice_referencia.domain.IndiceReferencia;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.periodicidad_liquidacion.domain.PeriodicidadLiquidacion;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.sistema_amortizacion.domain.SistemaAmortizacion;
import com.haya.alaska.situacion.domain.Situacion;
import com.haya.alaska.tipo_reestructuracion.domain.TipoReestructuracion;
import lombok.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Audited
@AuditTable(value = "HIST_MSTR_CONTRATO")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_CONTRATO")
public class Contrato implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToMany
  @JoinTable(
      name = "RELA_CONTRATO_ACUERDO_PAGO",
      joinColumns = {@JoinColumn(name = "ID_CONTRATO")},
      inverseJoinColumns = {@JoinColumn(name = "ID_ACUERDO_PAGO")})
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_CONTRATO_ACUERDO_PAGO")
  private Set<AcuerdoPago> acuerdosPago;

  @ManyToMany
  @JoinTable(
      name = "RELA_CONTRATO_PROPUESTA",
      joinColumns = {@JoinColumn(name = "ID_CONTRATO")},
      inverseJoinColumns = {@JoinColumn(name = "ID_PROPUESTA")})
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_CONTRATO_PROPUESTA")
  private Set<Propuesta> propuestas;

  @ManyToMany
  @JoinTable(
      name = "RELA_CONTRATO_IMPORTE_PROPUESTA",
      joinColumns = {@JoinColumn(name = "ID_CONTRATO")},
      inverseJoinColumns = {@JoinColumn(name = "ID_IMPORTE_PROPUESTA")})
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_CONTRATO_IMPORTE_PROPUESTA")
  private Set<ImportePropuesta> importesPropuestas;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE")
  private Expediente expediente;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_ESTRATEGIA_ASIGNADA")
  private EstrategiaAsignada estrategia;

  //  @ManyToMany(cascade = {CascadeType.ALL})
  //  @JoinTable(
  //      name = "RELA_CONTRATO_BIEN",
  //      joinColumns = @JoinColumn(name = "ID_CONTRATO"),
  //      inverseJoinColumns = @JoinColumn(name = "ID_BIEN"))
  //  private Set<Bien> bienes = new HashSet<>();

  @OneToMany(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  @NotAudited
  private Set<ContratoBien> bienes = new HashSet<>();

  @OneToMany(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  @NotAudited
  private Set<Saldo> saldos = new HashSet<>();

  @OneToMany(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  @NotAudited
  private Set<ContratoInterviniente> intervinientes = new HashSet<>();

  @OneToMany(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  @NotAudited
  private Set<Movimiento> movimientos = new HashSet<>();

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
      name = "RELA_CONTRATO_PROCEDIMIENTO",
      joinColumns = @JoinColumn(name = "ID_CONTRATO"),
    inverseJoinColumns = @JoinColumn(name = "ID_PROCEDIMIENTO"))
  @AuditJoinTable(name = "HIST_RELA_CONTRATO_PROCEDIMIENTO")
  private Set<Procedimiento> procedimientos = new HashSet<>();

  @Column(name = "IND_REPRESENTANTE")
  private Boolean esRepresentante;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;

  @Column(name = "DES_ID_ORIGEN")
  private String idOrigen;

  @Column(name = "DES_ID_ORIGEN_2")
  private String idOrigen2;

  @Column(name = "DES_ID_DATATAPE")
  private Integer idDatatape;

  @Column(name = "IND_EN_REVISION")
  private Boolean enRevision;

  @Column(name = "IND_VENCIMIENTO_ANTICIPADO")
  private Boolean vencimientoAnticipado;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PRODUCTO")
  private Producto producto;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SISTEMA_AMORTIZACION")
  private SistemaAmortizacion sistemaAmortizacion;

  @Column(name = "DES_PRODUCTO")
  private String descripcionProducto;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERIOCIDAD_LIQUIDACION")
  private PeriodicidadLiquidacion periodicidadLiquidacion;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_INDICE_REFERENCIA")
  private IndiceReferencia indiceReferencia;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SITUACION")
  private Situacion situacion;

  @Column(name = "NUM_DIFERENCIAL")
  private Double diferencial;

  @Column(name = "NUM_TIN")
  private Double tin;

  @Column(name = "NUM_TAE")
  private Double tae;

  @Column(name = "IND_REESTRUCTURADO")
  private Boolean reestructurado;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_REESTRUCTURACION")
  private TipoReestructuracion tipoReestructuracion;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLASIFICACION_CONTABLE")
  private ClasificacionContable clasificacionContable;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FORMALIZACION")
  private Date fechaFormalizacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_VENC_INICIAL")
  private Date fechaVencimientoInicial;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_VENC_ANTICIPADO")
  private Date fechaVencimientoAnticipado;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PASE_REGULAR")
  private Date fechaPaseRegular;

  @Column(name = "NUM_CUOTAS_PENDIENTES")
  private Integer cuotasPendientes;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_LIQUIDACION_CUOTA")
  private Date fechaLiquidacionCuota;

  @Column(name = "NUM_IMPORTE_INICIAL")
  private Double importeInicial;

  @Column(name = "NUM_IMPORTE_DISPUESTO")
  private Double importeDispuesto;

  @Column(name = "NUM_CUOTAS_TOTALES")
  private Integer cuotasTotales;

  @Column(name = "NUM_IMPORTE_ULTIMA_CUOTA")
  private Double importeUltimaCuota;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PRIMER_IMPAGO")
  private Date fechaPrimerImpago;

  @Column(name = "NUM_DIAS_IMPAGO")
  private Integer diasImpago;

  @Column(name = "NUM_CUOTAS_IMPAGADAS")
  private Integer numeroCuotasImpagadas;

  @Column(name = "NUM_RESTO_HIPOTECARIO")
  private Double restoHipotecario;

  @Column(name = "DES_ORIGEN")
  private String origen;

  @Column(name = "NUM_GASTOS_JUDICIALES")
  private Double gastosJudiciales;

  @Column(name = "NUM_IMPORTE_CUOTAS_IMPAGADAS")
  private Double importeCuotasImpagadas;

  @Column(name = "DES_NOTAS_1")
  private String notas1;

  @Column(name = "DES_NOTAS_2")
  private String notas2;

  @Column(name = "NUM_DEUDA_IMAGADA")
  private Double deudaImpagada;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @Column(name = "ID_CARGA")
  private String idCarga;

  @Column(name = "DES_CARTERA_REM")
  private String carteraREM;

  @Column(name = "DES_SUBCARTERA_REM")
  private String subcarteraREM;

  @Column(name = "DES_RANGO_HIPOTECARIO")
  private String rangoHipotecario;

  @Column(name = "DES_TERRITORIAL")
  private String territorial;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_CONTRATO")
  private EstadoContrato estadoContrato;

  // columnas de apoyo para la carga de datos
  @Column(name = "NUM_SALDO_GESTION")
  private Double saldoGestion;

  @Column(name = "IND_CALCULADO")
  private Boolean calculado;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CARGA")
  private Date fechaCarga;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PRIMERA_ASIG")
  private Date fechaPrimeraAsignacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ULTIMA_ASIG")
  private Date fechaUltimaAsignacion;

  @OneToMany(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  @NotAudited
  private Set<CampaniaAsignada> campaniasAsignadas = new HashSet<>();

  @OneToOne(mappedBy = "contrato")
  private FormalizacionContrato formalizacionContrato;

  public boolean isRepresentante() {
    if (this.esRepresentante == null) return false;
    return this.esRepresentante;
  }

  public Double getResponsabilidadHipotecaria() {
    if (this.getBienes().isEmpty()) {
      return null;
    }
    List<ContratoBien> bienes = new ArrayList<>(this.bienes);
    double result = 0.0;
    for (ContratoBien ci : bienes) {
      if (ci.getResponsabilidadHipotecaria() != null) result += ci.getResponsabilidadHipotecaria();
    }
    return result;
  }

  public Saldo getSaldoContrato() {
    if (this.getSaldos().isEmpty()) {
      return new Saldo();
    }
    List<Saldo> saldosOrdenados = new ArrayList<>(this.saldos);
    saldosOrdenados.sort((a, b) -> b.getDia().compareTo(a.getDia()));
    return !saldosOrdenados.isEmpty() ? saldosOrdenados.get(0) : new Saldo();
  }

  public Interviniente getPrimerInterviniente() {
    if (intervinientes == null || intervinientes.isEmpty()) {
      return new Interviniente();
    }
    ContratoInterviniente ci =
        this.getIntervinientes().stream()
            .filter(ContratoInterviniente::isPrimerInterviniente)
            .findFirst()
            .orElse(null);
    if (ci == null) {
      return new Interviniente();
    } else {
      return ci.getInterviniente();
    }
  }

  /**
   * Quita un procedimiento del contrato
   *
   * @param procedimiento Id del procedimiento que quieres quitar de la lista
   */
  public void removeProcedimiento(Procedimiento procedimiento) {
    if (procedimiento == null) {
      return;
    }
    this.procedimientos.remove(procedimiento);
  }

  public void addIntervinientes(List<ContratoInterviniente> intervinientes) {
    this.intervinientes.addAll(intervinientes);
  }

  public void removeIntervinientes(List<ContratoInterviniente> intervinientes) {
    this.intervinientes.removeAll(intervinientes);
  }

  public Saldo getUltimoSaldo() {
    return saldos.stream()
        .sorted(Comparator.comparing(Saldo::getDia).reversed())
        .findFirst()
        .orElse(null);
  }

  public void setExpediente(Expediente expediente) {
    if ((this.expediente == null && expediente != null)
      || (this.expediente != null
      && expediente != null
      /* && !this.expediente.getId().equals(expediente.getId()))**/)) {
      this.fechaUltimaAsignacion = new Date();
      if (this.fechaPrimeraAsignacion == null)
        this.fechaPrimeraAsignacion = new Date();
      this.expediente = expediente;
    }
  }

  public Cliente getCliente() {
    if (this.expediente != null
        && this.expediente.getCartera() != null
        && this.expediente.getCartera().getClientes().size() == 1)
      return new ArrayList<>(this.expediente.getCartera().getClientes()).get(0).getCliente();
    else return null;
  }
}
