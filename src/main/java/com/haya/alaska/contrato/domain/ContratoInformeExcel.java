package com.haya.alaska.contrato.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class ContratoInformeExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Loan Id");
      cabeceras.add("Proposal Id");
      cabeceras.add("Status");
      cabeceras.add("Product");
      cabeceras.add("Amortization System");
      cabeceras.add("Settlement Periodicity");
      cabeceras.add("Situation");
      cabeceras.add("Reference Index");
      cabeceras.add("Differential");
      cabeceras.add("Tin");
      cabeceras.add("Tae");
      cabeceras.add("Mortgage Range");
      cabeceras.add("Restructured");
      cabeceras.add("Restructured Type");
      cabeceras.add("Accounting Classification");
      cabeceras.add("Campaign");
      cabeceras.add("Formalization Date");
      cabeceras.add("Initial Due Date");
      cabeceras.add("Early Expiration Date");
      cabeceras.add("Regular Pass Date");
      cabeceras.add("Outstanding Installments");
      cabeceras.add("Quota Settlement Date");
      cabeceras.add("Initial Amount");
      cabeceras.add("Amount Drawn");
      cabeceras.add("Capital Pending");
      cabeceras.add("Management Balance");
      cabeceras.add("Total Fees");
      cabeceras.add("Last Installment Amount");
      cabeceras.add("Date of First Non-Payment");
      cabeceras.add("Number of Unpaid Installments");
      cabeceras.add("Unpaid Debt");
      cabeceras.add("Remaining Mortgage");
      cabeceras.add("Origin");
      cabeceras.add("Principal Due Unpaid");
      cabeceras.add("Unpaid Ordinary Interest");
      cabeceras.add("Interest Delayed");
      cabeceras.add("Unpaid Commissions");
      cabeceras.add("Unpaid Expenses");
      cabeceras.add("Judicial Expenses");
      cabeceras.add("Amount of Unpaid Fees");
    } else {
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Propuesta");
      cabeceras.add("Estado");
      cabeceras.add("Producto");
      cabeceras.add("Sistema Amortizacion");
      cabeceras.add("Periodicidad Liquidacion");
      cabeceras.add("Indice Referencia");
      cabeceras.add("Diferencial");
      cabeceras.add("Tin");
      cabeceras.add("Tae");
      cabeceras.add("Rango Hipotecario");
      cabeceras.add("Reestructurado");
      cabeceras.add("Tipo Reestructuracion");
      cabeceras.add("Clasificacion Contable");
      cabeceras.add("Fecha Formalizacion");
      cabeceras.add("Fecha Vencimiento Inicial");
      cabeceras.add("Fecha Vencimiento Anticipado");
      cabeceras.add("Fecha Pase Regular");
      cabeceras.add("Cuotas Pendientes");
      cabeceras.add("Fecha Liquidacion Cuota");
      cabeceras.add("Importe Inicial");
      cabeceras.add("Importe Dispuesto");
      cabeceras.add("Capital Pendiente");
      cabeceras.add("Saldo Gestion");
      cabeceras.add("Cuotas Totales");
      cabeceras.add("Importe Ultima Cuota");
      cabeceras.add("Fecha Primer Impago");
      cabeceras.add("Numero Cuotas Impagadoas");
      cabeceras.add("Deuda Impagada");
      cabeceras.add("Resto Hipotecario");
      cabeceras.add("Origen");
      cabeceras.add("Principal Vencido Impagado");
      cabeceras.add("Intereses Ordinarios Impagados");
      cabeceras.add("Intereses Demora");
      cabeceras.add("Comisiones Impagados");
      cabeceras.add("Gastos Impagados");
      cabeceras.add("Gastos Judiciales");
      cabeceras.add("Importe Cuotas Impagadas");
    }
  }

  public ContratoInformeExcel(Contrato contrato) {

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Freight Id", contrato.getIdCarga());
      this.add("Origin Id", contrato.getIdOrigen());
      this.add(
          "Accounting Classification Id", contrato.getClasificacionContable().getValorIngles());
      this.add("Loan Status", contrato.getEstadoContrato().getValorIngles());
      this.add("Loan Id", contrato.getId());
      this.add(
          "Proposal Id",
          contrato.getPropuestas() != null
              ? contrato.getPropuestas().stream()
                  .map(propuesta -> (propuesta.getId()).toString())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Status", contrato.getEstadoContrato().getValorIngles());
      this.add("Product", contrato.getProducto().getValorIngles());
      this.add(
          "Amortization System",
          contrato.getSistemaAmortizacion() != null
              ? contrato.getSistemaAmortizacion().getValorIngles()
              : null);
      this.add(
          "Settlement Periodicity",
          contrato.getPeriodicidadLiquidacion() != null
              ? contrato.getPeriodicidadLiquidacion().getValorIngles()
              : null);
      this.add(
          "Reference Index",
          contrato.getIndiceReferencia() != null
              ? contrato.getIndiceReferencia().getValorIngles()
              : null);
      this.add(
          "Differential", contrato.getDiferencial() != null ? contrato.getDiferencial() : null);
      this.add("Tin", contrato.getTin() != null ? contrato.getTin() : null);
      this.add("Tae", contrato.getTae() != null ? contrato.getTae() : null);
      this.add(
          "Mortgage Range",
          contrato.getRangoHipotecario() != null ? contrato.getRangoHipotecario() : null);
      this.add(
          "Restructured",
          contrato.getReestructurado() != null ? contrato.getReestructurado() : null);
      this.add(
          "Restructured Type",
          contrato.getTipoReestructuracion() != null
              ? contrato.getTipoReestructuracion().getValorIngles()
              : null);
      this.add(
          "Accounting Classification",
          contrato.getClasificacionContable() != null
              ? contrato.getClasificacionContable().getValorIngles()
              : null);
      this.add(
          "Formalization Date",
          contrato.getFechaFormalizacion() != null ? contrato.getFechaFormalizacion() : null);
      this.add(
          "Initial Due Date",
          contrato.getFechaVencimientoInicial() != null
              ? contrato.getFechaVencimientoInicial()
              : null);
      this.add(
          "Early Expiration Date",
          contrato.getFechaVencimientoAnticipado() != null
              ? contrato.getFechaVencimientoAnticipado()
              : null);
      this.add(
          "Regular Pass Date",
          contrato.getFechaPaseRegular() != null ? contrato.getFechaPaseRegular() : null);
      this.add(
          "Outstanding Installments",
          contrato.getCuotasPendientes() != null ? contrato.getCuotasPendientes() : null);
      this.add(
          "Quota Settlement Date",
          contrato.getFechaLiquidacionCuota() != null ? contrato.getFechaLiquidacionCuota() : null);
      this.add(
          "Initial Amount",
          contrato.getImporteInicial() != null ? contrato.getImporteInicial() : null);
      this.add(
          "Amount Drawn",
          contrato.getImporteDispuesto() != null ? contrato.getImporteDispuesto() : null);
      this.add("Capital Pending", contrato.getCuotasPendientes());
      this.add(
          "Management Balance",
          contrato.getSaldoGestion() != null ? contrato.getSaldoGestion() : null);
      this.add(
          "Total Fees", contrato.getCuotasTotales() != null ? contrato.getCuotasTotales() : null);
      this.add(
          "Last Installment Amount",
          contrato.getImporteUltimaCuota() != null ? contrato.getImporteUltimaCuota() : null);
      this.add(
          "Date of First Non-Payment",
          contrato.getFechaPrimerImpago() != null ? contrato.getFechaPrimerImpago() : null);
      this.add(
          "Number of Unpaid Installments",
          contrato.getNumeroCuotasImpagadas() != null ? contrato.getNumeroCuotasImpagadas() : null);
      this.add(
          "Unpaid Debt", contrato.getDeudaImpagada() != null ? contrato.getDeudaImpagada() : null);
      this.add(
          "Remaining Mortgage",
          contrato.getRestoHipotecario() != null ? contrato.getRestoHipotecario() : null);
      this.add("Origin", contrato.getOrigen() != null ? contrato.getOrigen() : null);
      this.add(
          "Principal Due Unpaid",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getPrincipalVencidoImpagado()
              : null);
      this.add(
          "Unpaid Ordinary Interest",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getInteresesOrdinariosImpagados()
              : null);
      this.add(
          "Interest Delayed",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getInteresesDemora()
              : null);
      this.add(
          "Unpaid Commissions",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getComisionesImpagados()
              : null);
      this.add(
          "Unpaid Expenses",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getGastosImpagados()
              : null);
      this.add(
          "Judicial Expenses",
          contrato.getGastosJudiciales() != null ? contrato.getGastosJudiciales() : null);
      this.add(
          "Amount of Unpaid Fees",
          contrato.getImporteCuotasImpagadas() != null
              ? contrato.getImporteCuotasImpagadas()
              : null);
    } else {
      this.add("Id Carga", contrato.getIdCarga());
      this.add("Id Origen", contrato.getIdOrigen());
      this.add("Id Clasificacion Contable", contrato.getClasificacionContable().getValor());
      this.add("Estado Contrato", contrato.getEstadoContrato().getValor());
      this.add("Id Contrato", contrato.getId());
      this.add(
          "Id Propuesta",
          contrato.getPropuestas() != null
              ? contrato.getPropuestas().stream()
                  .map(propuesta -> (propuesta.getId()).toString())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Estado", contrato.getEstadoContrato().getValor());
      this.add("Producto", contrato.getProducto().getValor());
      this.add(
          "Sistema Amortizacion",
          contrato.getSistemaAmortizacion() != null
              ? contrato.getSistemaAmortizacion().getValor()
              : null);
      this.add(
          "Periodicidad Liquidacion",
          contrato.getPeriodicidadLiquidacion() != null
              ? contrato.getPeriodicidadLiquidacion().getValor()
              : null);
      this.add(
          "Indice Referencia",
          contrato.getIndiceReferencia() != null
              ? contrato.getIndiceReferencia().getValor()
              : null);
      this.add("Diferencial", contrato.getDiferencial() != null ? contrato.getDiferencial() : null);
      this.add("Tin", contrato.getTin() != null ? contrato.getTin() : null);
      this.add("Tae", contrato.getTae() != null ? contrato.getTae() : null);
      this.add(
          "Rango Hipotecario",
          contrato.getRangoHipotecario() != null ? contrato.getRangoHipotecario() : null);
      this.add(
          "Reestructurado",
          contrato.getReestructurado() != null ? contrato.getReestructurado() : null);
      this.add(
          "Tipo Reestructuracion",
          contrato.getTipoReestructuracion() != null
              ? contrato.getTipoReestructuracion().getValor()
              : null);
      this.add(
          "Clasificacion Contable",
          contrato.getClasificacionContable() != null
              ? contrato.getClasificacionContable().getValor()
              : null);
      this.add(
          "Fecha Formalizacion",
          contrato.getFechaFormalizacion() != null ? contrato.getFechaFormalizacion() : null);
      this.add(
          "Fecha Vencimiento Inicial",
          contrato.getFechaVencimientoInicial() != null
              ? contrato.getFechaVencimientoInicial()
              : null);
      this.add(
          "Fecha Vencimiento Anticipado",
          contrato.getFechaVencimientoAnticipado() != null
              ? contrato.getFechaVencimientoAnticipado()
              : null);
      this.add(
          "Fecha Pase Regular",
          contrato.getFechaPaseRegular() != null ? contrato.getFechaPaseRegular() : null);
      this.add(
          "Cuotas Pendientes",
          contrato.getCuotasPendientes() != null ? contrato.getCuotasPendientes() : null);
      this.add(
          "Fecha Liquidacion Cuota",
          contrato.getFechaLiquidacionCuota() != null ? contrato.getFechaLiquidacionCuota() : null);
      this.add(
          "Importe Inicial",
          contrato.getImporteInicial() != null ? contrato.getImporteInicial() : null);
      this.add(
          "Importe Dispuesto",
          contrato.getImporteDispuesto() != null ? contrato.getImporteDispuesto() : null);
      this.add("Capital Pendiente", contrato.getCuotasPendientes());
      this.add(
          "Saldo Gestion", contrato.getSaldoGestion() != null ? contrato.getSaldoGestion() : null);
      this.add(
          "Cuotas Totales",
          contrato.getCuotasTotales() != null ? contrato.getCuotasTotales() : null);
      this.add(
          "Importe Ultima Cuota",
          contrato.getImporteUltimaCuota() != null ? contrato.getImporteUltimaCuota() : null);
      this.add(
          "Fecha Primer Impago",
          contrato.getFechaPrimerImpago() != null ? contrato.getFechaPrimerImpago() : null);
      this.add(
          "Numero Cuotas Impagadoas",
          contrato.getNumeroCuotasImpagadas() != null ? contrato.getNumeroCuotasImpagadas() : null);
      this.add(
          "Deuda Impagada",
          contrato.getDeudaImpagada() != null ? contrato.getDeudaImpagada() : null);
      this.add(
          "Resto Hipotecario",
          contrato.getRestoHipotecario() != null ? contrato.getRestoHipotecario() : null);
      this.add("Origen", contrato.getOrigen() != null ? contrato.getOrigen() : null);
      this.add(
          "Principal Vencido Impagado",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getPrincipalVencidoImpagado()
              : null);
      this.add(
          "Intereses Ordinarios Impagados",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getInteresesOrdinariosImpagados()
              : null);
      this.add(
          "Intereses Demora",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getInteresesDemora()
              : null);
      this.add(
          "Comisiones Impagados",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getComisionesImpagados()
              : null);
      this.add(
          "Gastos Impagados",
          contrato.getSaldoContrato() != null
              ? contrato.getSaldoContrato().getGastosImpagados()
              : null);
      this.add(
          "Gastos Judiciales",
          contrato.getGastosJudiciales() != null ? contrato.getGastosJudiciales() : null);
      this.add(
          "Importe Cuotas Impagadas",
          contrato.getImporteCuotasImpagadas() != null
              ? contrato.getImporteCuotasImpagadas()
              : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
