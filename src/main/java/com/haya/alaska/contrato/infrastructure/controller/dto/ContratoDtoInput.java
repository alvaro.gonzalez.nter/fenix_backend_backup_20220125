package com.haya.alaska.contrato.infrastructure.controller.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ContratoDtoInput extends ContratoDto{

  private Integer estado;
  private Integer producto;
  private Integer periodicidadLiquidacion;
  private Integer indiceReferencia;
  private Integer tipoReestructuracion;
  private Integer clasificacionContable;
  private Integer cuotasImpagadas;
  private Integer situacion;
  private Integer estrategia;
}
