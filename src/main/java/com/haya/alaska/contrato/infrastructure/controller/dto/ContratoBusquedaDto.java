package com.haya.alaska.contrato.infrastructure.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContratoBusquedaDto implements Comparable{
  private Integer id;
  private String idOrigen;
  private String cartera;
  private String activo;
  private String OrigenEstrategia;
  private String Fecha;
  private String Periodo;
  private String Tipo;
  private String Estrategia;

  @Override
  public int compareTo(Object o) {
    ContratoBusquedaDto otra = (ContratoBusquedaDto) o;

    return this.getId().compareTo(otra.getId());
  }
}
