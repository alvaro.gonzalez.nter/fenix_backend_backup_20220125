package com.haya.alaska.contrato.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.saldo.infrastructure.controller.dto.SaldoDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ContratoDtoOutput extends ContratoDto {

  private static final long serialVersionUID = 1L;
  
  private Boolean esRepresentante;
  private CatalogoMinInfoDTO producto;
  private String descripcionProducto;
  private CatalogoMinInfoDTO periodicidadLiquidacion;
  private CatalogoMinInfoDTO indiceReferencia;
  private CatalogoMinInfoDTO tipoReestructuracion;
  private Boolean vencimientoAnticipado;
  private CatalogoMinInfoDTO clasificacionContable;
  private String cancelacionParcial;
  private CatalogoMinInfoDTO estado;
  private EstrategiaAsignadaOutputDTO estrategia;
  
  /*Info extra cabecera detalle contrato*/
  private String primerTitular;
  private Integer intervinientes;
  private Integer garantias;
  private SaldoDto saldo;
  private ContratoDtoOutput historico;
  private Boolean judicial;
  private CatalogoMinInfoDTO situacion;

}
