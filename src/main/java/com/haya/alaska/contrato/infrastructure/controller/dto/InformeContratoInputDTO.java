package com.haya.alaska.contrato.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InformeContratoInputDTO {

  private Integer estadoExpediente;
  private Integer tipo;
  private Date fechaInicioInicial;
  private Date fechaInicioFinal;
  private Date fechaFinInicial;
  private Date fechaFinFinal;

}
