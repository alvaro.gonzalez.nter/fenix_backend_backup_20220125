package com.haya.alaska.contrato.infrastructure.mapper;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable;
import com.haya.alaska.clasificacion_contable.infrastructure.repository.ClasificacionContableRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDto;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDtoInput;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDtoOutput;
import com.haya.alaska.contrato.infrastructure.controller.dto.EstrategiaAsignadaOutputDTO;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.estado_contrato.infrastructure.repository.EstadoContratoRepository;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.estrategia_asignada.infrastructure.repository.EstrategiaAsignadaRepository;
import com.haya.alaska.indice_referencia.domain.IndiceReferencia;
import com.haya.alaska.indice_referencia.infrastructure.repository.IndiceReferenciaRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.origen_estrategia.infrastructure.repository.OrigenEstrategiaRepository;
import com.haya.alaska.periodicidad_liquidacion.infrastructure.repository.PeriodicidadLiquidacionRepository;
import com.haya.alaska.periodo_estrategia.infrastructure.repository.PeriodoEstrategiaRepository;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.producto.infrastructure.repository.ProductoRepository;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.infrastructure.controller.dto.SaldoDto;
import com.haya.alaska.saldo.infrastructure.repository.SaldoRepository;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.situacion.domain.Situacion;
import com.haya.alaska.situacion.infrastructure.repository.SituacionRepository;
import com.haya.alaska.tipo_estrategia.infrastructure.repository.TipoEstrategiaRepository;
import com.haya.alaska.tipo_reestructuracion.domain.TipoReestructuracion;
import com.haya.alaska.tipo_reestructuracion.infrastructure.repository.TipoReestructuracionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

@Component
public class ContratoMapper {

  @Autowired
  CatalogoMinInfoMapper catalogoMinInfoMapper;

  @Autowired
  ContratoRepository contratoRepository;
  @Autowired
  TipoReestructuracionRepository tipoReestructuracionRepository;
  @Autowired
  ProductoRepository productoRepository;
  @Autowired
  PeriodicidadLiquidacionRepository periodicidadLiquidacionRepository;
  @Autowired
  IndiceReferenciaRepository indiceReferenciaRepository;
  @Autowired
  ClasificacionContableRepository clasificacionContableRepository;
  @Autowired
  EstadoContratoRepository estadoContratoRepository;
  @Autowired
  SituacionRepository situacionRepository;
  @Autowired
  EstrategiaAsignadaRepository estrategiaAsignadaRepository;
  @Autowired
  EstrategiaRepository estrategiaRepository;
  @Autowired
  SaldoRepository saldoRepository;
  @Autowired
  TipoEstrategiaRepository tipoEstrategiaRepository;
  @Autowired
  OrigenEstrategiaRepository origenEstrategiaRepository;
  @Autowired
  PeriodoEstrategiaRepository periodoEstrategiaRepository;

  @Autowired
  Map<String, CatalogoRepository<? extends Catalogo, Integer>> catalogoRepository;

  public List<ContratoDTOToList> getContratoDTOToListToPropuesta(List<Contrato> contratos, ContratoInterviniente ci){
    List<ContratoDTOToList> contratosDTO = new ArrayList<>();
    for (Contrato contrato : contratos) {
      if(ci != null){
        var contratoDTO = new ContratoDTOToList(contrato);
        contratoDTO.setResponsabilidadHipotecaria(ci.getContrato().getResponsabilidadHipotecaria());
        contratoDTO.setRango(ci.getContrato().getRangoHipotecario());
        contratosDTO.add(contratoDTO);
      }
    }
    return contratosDTO;
  }

  public List<ContratoDTOToList> listContratoToListContratoDTOToList(Collection<Contrato> contratos, Interviniente interviniente) {
    List<ContratoDTOToList> contratosDTO = new ArrayList<>();
    for (Contrato contrato : contratos) {
      contratosDTO.add(new ContratoDTOToList(contrato, interviniente));
    }
    return contratosDTO;
  }

  public List<ContratoDTOToList> listContratoToListContratoDTOToList(Collection<Contrato> contratos, Bien bien) {
    List<ContratoDTOToList> contratosDTO = new ArrayList<>();
    for (Contrato contrato : contratos) {
      if (contrato != null)
        contratosDTO.add(new ContratoDTOToList(contrato, bien));
    }
    return contratosDTO;
  }

  public List<ContratoDTOToList> listContratoToListContratoDTOToList(Collection<Contrato> contratos) {
    List<ContratoDTOToList> contratosDTO = new ArrayList<>();
    for (Contrato contrato : contratos) {
      contratosDTO.add(new ContratoDTOToList(contrato));
    }
    return contratosDTO;
  }

  public ContratoDtoOutput entityToOutputDto(Contrato contrato) {
    ContratoDtoOutput contratoDtoOutput = new ContratoDtoOutput();
    BeanUtils.copyProperties(contrato, contratoDtoOutput);

    contratoDtoOutput.setEstado(contrato.getEstadoContrato() != null ? new CatalogoMinInfoDTO(contrato.getEstadoContrato()) : null);
    if(contrato.getPrimerInterviniente().getPersonaJuridica() != null &&contrato.getPrimerInterviniente().getPersonaJuridica() )
      contratoDtoOutput.setPrimerTitular(contrato.getPrimerInterviniente().getRazonSocial());
    else
      contratoDtoOutput.setPrimerTitular(contrato.getPrimerInterviniente().getNombre() + " " + contrato.getPrimerInterviniente().getApellidos());
    contratoDtoOutput.setIntervinientes(contrato.getIntervinientes().size());
    contratoDtoOutput.setGarantias(contrato.getBienes().size());
    contratoDtoOutput.setProducto(contrato.getProducto() != null ? catalogoMinInfoMapper.entityToDto(contrato.getProducto()) : null);
    contratoDtoOutput.setPeriodicidadLiquidacion(contrato.getPeriodicidadLiquidacion() != null ? new CatalogoMinInfoDTO(contrato.getPeriodicidadLiquidacion()) : null);
    contratoDtoOutput.setIndiceReferencia(contrato.getIndiceReferencia() != null ? new CatalogoMinInfoDTO(contrato.getIndiceReferencia()) : null);
    contratoDtoOutput.setTipoReestructuracion(contrato.getTipoReestructuracion() != null ? new CatalogoMinInfoDTO(contrato.getTipoReestructuracion()) : null);
    contratoDtoOutput.setClasificacionContable(contrato.getClasificacionContable() != null ? new CatalogoMinInfoDTO(contrato.getClasificacionContable()) : null);
    contratoDtoOutput.setJudicial(!contrato.getProcedimientos().isEmpty());
    contratoDtoOutput.setSituacion(contrato.getSituacion() != null ? new CatalogoMinInfoDTO(contrato.getSituacion()) : null);

    contratoDtoOutput.setEstrategia(contrato.getEstrategia() != null ? new EstrategiaAsignadaOutputDTO(contrato.getEstrategia()) : null);

    if (contrato.getCampaniasAsignadas() != null && contrato.getCampaniasAsignadas().size() > 0) {
      var campaniaAsignada = contrato.getCampaniasAsignadas().stream().sorted(Comparator.comparing(CampaniaAsignada::getFechaAsignacion).reversed()).findFirst().orElse(null);
      if (campaniaAsignada != null) {
        contratoDtoOutput.setCampania(campaniaAsignada.getCampania().getNombre());
        contratoDtoOutput.setIdCampania(campaniaAsignada.getCampania().getId());
      }
    }
    return contratoDtoOutput;
  }

  public Contrato inputDtoToEntity(Integer id, ContratoDtoInput contratoDtoInput) throws NotFoundException {
    Contrato contrato = new Contrato();
    if (id != null) {
      contrato = contratoRepository.findById(id).orElse(null);
    }

    BeanUtils.copyProperties(contratoDtoInput, contrato, "id", "idCarga");

    if (contratoDtoInput.getEstado() != null) {
      contrato.setEstadoContrato(estadoContratoRepository.findById(contratoDtoInput.getEstado()).orElse(null));
      contrato.setActivo(!contrato.getEstadoContrato().getCodigo().equals("CER"));
    }
    if (contratoDtoInput.getTipoReestructuracion() != null)
      contrato.setTipoReestructuracion(tipoReestructuracionRepository.findById(contratoDtoInput.getTipoReestructuracion()).orElse(null));
    if (contratoDtoInput.getProducto() != null)
      contrato.setProducto(productoRepository.findById(contratoDtoInput.getProducto()).orElse(null));
    if (contratoDtoInput.getPeriodicidadLiquidacion() != null)
      contrato.setPeriodicidadLiquidacion(periodicidadLiquidacionRepository.findById(contratoDtoInput.getPeriodicidadLiquidacion()).orElse(null));
    if (contratoDtoInput.getIndiceReferencia() != null)
      contrato.setIndiceReferencia(indiceReferenciaRepository.findById(contratoDtoInput.getIndiceReferencia()).orElse(null));
    if (contratoDtoInput.getClasificacionContable() != null)
      contrato.setClasificacionContable(clasificacionContableRepository.findById(contratoDtoInput.getClasificacionContable()).orElse(null));
    if (contratoDtoInput.getSituacion() != null)
      contrato.setSituacion(situacionRepository.findById(contratoDtoInput.getSituacion()).orElse(null));
    if (contratoDtoInput.getEstrategia() != null){
      //crear una nueva estrategia
      var ea2 = new EstrategiaAsignada();
      ea2.setOrigenEstrategia(origenEstrategiaRepository.findByCodigo("GM").orElse(null));
      ea2.setTipoEstrategia(tipoEstrategiaRepository.findByCodigo("M").orElse(null));
      ea2.setPeriodoEstrategia(periodoEstrategiaRepository.findByCodigo("M").orElse(null));
      ea2.setFecha(new Date());
      ea2.setEstrategia(estrategiaRepository.findById(contratoDtoInput.getEstrategia()).orElseThrow(
        ()->new NotFoundException("Estrategia", contratoDtoInput.getEstrategia())));
      contrato.setEstrategia(estrategiaAsignadaRepository.save(ea2));
    }

    Contrato contratoSaved = contratoRepository.save(contrato);
    Saldo saldo = contrato.getSaldoContrato();
    if (saldo != null) {
      Set<Saldo> saldos = new HashSet<>();
      saldos.add(saldo);
      contratoSaved.setSaldos(saldos);
    }
    return contratoSaved;
  }

  public ContratoDtoOutput mapToOutputDto(Map histContrato) throws IllegalAccessException{
    ContratoDtoOutput contratoOutputDTO = new ContratoDtoOutput();
    Field[] declaredFieldsOutputDTO = ContratoDtoOutput.class.getDeclaredFields();
    //ContratoDto contratoDTO = new ContratoDto();
    Field[] declaredFieldsDTO = ContratoDto.class.getDeclaredFields();
    for (Field declaredField : declaredFieldsOutputDTO) { // non entity fields
      declaredField.setAccessible(true);
      String name = declaredField.getName();
      if (histContrato.get(name) == null) continue;
      //IMPORTANTE esta comprobacion valida si es de tipo double en cuyo caso lo trata
      if(declaredField.getType() == Double.class) {
        declaredField.set(contratoOutputDTO, Double.valueOf(histContrato.get(name).toString()));
      } else if (histContrato.get(name).getClass().equals(Integer.class)){
        CatalogoRepository<? extends Catalogo, Integer> repo = catalogoRepository.get(name + "Repository");
        if (repo != null){
          Catalogo catalogo = repo.findById((Integer) histContrato.get(name)).orElse(null);
          if (catalogo != null && !name.equals("estrategia"))
            declaredField.set(contratoOutputDTO, new CatalogoMinInfoDTO(catalogo));
        } else if (name.equals("estrategia") ) {
          EstrategiaAsignada estrategiaAsignada = estrategiaAsignadaRepository.findById(
            (Integer) histContrato.get(name)).orElse(null);
          if (estrategiaAsignada != null)
            declaredField.set(contratoOutputDTO, new EstrategiaAsignadaOutputDTO(estrategiaAsignada));
        }  else if (name.equals("saldo") ) {
          Saldo saldo = saldoRepository.findTopByContratoIdOrderByDiaDesc((Integer) histContrato.get("id")).orElse(null);
          if (saldo != null)
            declaredField.set(contratoOutputDTO, new SaldoDto(saldo));
        } else if (name.equals("estado") ) {
          EstadoContrato estado = estadoContratoRepository.findById(Integer.parseInt(histContrato.get("estado").toString())).orElse(null);
          if (estado != null)
            declaredField.set(contratoOutputDTO, new CatalogoMinInfoDTO(estado));
        } else if (name.equals("situacion") ) {
          Situacion situacion = situacionRepository.findById(Integer.parseInt(histContrato.get("situacion").toString())).orElse(null);
          if (situacion != null)
            declaredField.set(contratoOutputDTO, new CatalogoMinInfoDTO(situacion));
        } else if (name.equals("producto") ) {
          Producto producto = productoRepository.findById(Integer.parseInt(histContrato.get("producto").toString())).orElse(null);
          if (producto != null)
            declaredField.set(contratoOutputDTO, new CatalogoMinInfoDTO(producto));
        } else if (name.equals("indiceReferencia") ) {
          IndiceReferencia ir = indiceReferenciaRepository.findById(Integer.parseInt(histContrato.get("indiceReferencia").toString())).orElse(null);
          if (ir != null)
            declaredField.set(contratoOutputDTO, new CatalogoMinInfoDTO(ir));
        } else if (name.equals("tipoReestructuracion") ) {
          TipoReestructuracion tipo = tipoReestructuracionRepository.findById(Integer.parseInt(histContrato.get("tipoReestructuracion").toString())).orElse(null);
          if (tipo != null)
            declaredField.set(contratoOutputDTO, new CatalogoMinInfoDTO(tipo));
        } else if (name.equals("clasificacionContable") ) {
          ClasificacionContable clasificacion = clasificacionContableRepository.findById(Integer.parseInt(histContrato.get("clasificacionContable").toString())).orElse(null);
          if (clasificacion != null)
            declaredField.set(contratoOutputDTO, new CatalogoMinInfoDTO(clasificacion));
        } else {
          declaredField.set(contratoOutputDTO, histContrato.get(name));
        }
      } else {
        declaredField.set(contratoOutputDTO, histContrato.get(name));
      }
    }

    for (Field declaredField : declaredFieldsDTO) { // non entity fields
      declaredField.setAccessible(true);
      String name = declaredField.getName();
      if (histContrato.get(name) == null)
        continue;
      if (histContrato.get(name).getClass().equals(BigDecimal.class)){
        declaredField.set(contratoOutputDTO,Double.parseDouble(histContrato.get(name).toString()));
      } else {
        declaredField.set(contratoOutputDTO, histContrato.get(name));
        //org.apache.commons.beanutils.BeanUtils.setProperty(contratoOutputDTO,name,histContrato.get(name));
      }
    }
    if(contratoOutputDTO.getHistorico()!=null)
      contratoOutputDTO.getHistorico().setHistorico(null);
    return contratoOutputDTO;
  }
}
