package com.haya.alaska.contrato.infrastructure.controller;

import com.haya.alaska.contrato.application.ContratoUseCase;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDtoInput;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDtoOutput;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input.FormalizacionContratoInputDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.dto.HistoricoAsignacionesDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientes/{idExpediente}/contratos")
public class ContratoControler {

  @Autowired
  private ContratoUseCase contratoUseCase;

  @Transactional(readOnly = true)
  @GetMapping
  public ListWithCountDTO<ContratoDTOToList> getAllFiltered(
    @PathVariable(value = "idExpediente") Integer idExpediente,
    @RequestParam(value = "id", required = false) Integer id,
    @RequestParam(value = "idOrigen", required = false) String idOrigen,
    @RequestParam(value = "esRepresentante", required = false) Boolean esRepresentante,
    @RequestParam(value = "producto", required = false) String producto,
    @RequestParam(value = "estadoContratos", required = false) String estadoContratos,
    @RequestParam(value = "primerTitular", required = false) String primerTitular,
    @RequestParam(value = "saldoEnGestion", required = false) String saldoEnGestion,
    @RequestParam(value = "intervinientes", required = false) String intervinientes,
    @RequestParam(value = "garantias", required = false) String garantias,
    @RequestParam(value = "judicial", required = false) Boolean judicial,
    @RequestParam(value = "estado", required = false) Boolean estado,
    @RequestParam(value = "situacion", required = false) String situacion,
    @RequestParam(value = "estrategia", required = false) String estrategia,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) {

    return contratoUseCase.getAllFilteredContratos(idExpediente, id, idOrigen, esRepresentante,
      producto, estadoContratos, primerTitular, saldoEnGestion, intervinientes, garantias,
      judicial, estado, situacion, estrategia, orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Obtener contrato", notes = "Devuelve el contrato con id enviado")
  @GetMapping("/{idContrato}")
  @Transactional
  public ContratoDtoOutput getContratoById(
    @PathVariable("idContrato") Integer idContrato) throws IllegalAccessException, NotFoundException {
    return contratoUseCase.findContratoByIdDto(idContrato);
  }

  @ApiOperation(value = "Obtener contrato", notes = "Devuelve el contrato con id Origen enviado")
  @GetMapping("/byIdOrigen/{idContratoOrigen}")
  @Transactional
  public ContratoDtoOutput getContratoByIdCarga(
    @PathVariable("idContratoOrigen") String idContratoOrigen) throws IllegalAccessException {
    return contratoUseCase.findContratoByIdCargaDto(idContratoOrigen);
  }

  @ApiOperation(value = "Editar contrato", notes = "Editar el contrato con id enviado")
  @PutMapping("/{idContrato}")
  public ContratoDtoOutput updateContratoById(@PathVariable("idExpediente") Integer idExpediente,
                                              @PathVariable("idContrato") Integer idContrato,
                                              @RequestBody @Valid ContratoDtoInput contratoInputDTO) throws IllegalAccessException, NotFoundException, com.haya.alaska.shared.exceptions.NotFoundException {
    return contratoUseCase.update(idContrato, contratoInputDTO);
  }

  @ApiOperation(value = "Obtener histórico", notes = "Devuelve el histórico de asignaciones contrato con id enviado. Muestra cuando ha cambiado de estado el contrato (de activo a inactivo)")
  @GetMapping("/{idContrato}/historico")
  public List<HistoricoAsignacionesDTOToList> getHistoricoAsignacionesContrato(@PathVariable("idExpediente") Integer idExpediente,
                                                                               @PathVariable("idContrato") Integer idContrato) throws IllegalAccessException {
    return contratoUseCase.getHistoricoAsignacionesByContrato(idContrato, idExpediente);
  }

  @ApiOperation(value = "Obtener documentación de contrato", notes = "Devuelve la documentación disponible del contrato con id enviado")
  @GetMapping("/{idContrato}/documentos")
  @Transactional
  public ListWithCountDTO<DocumentoDTO> getDocumentosByContrato(
    @PathVariable(value = "idContrato", required = false) Integer idContrato,
    @PathVariable("idExpediente") Integer idExpediente, @RequestParam(value = "id", required = false) String idDocumento,
    @RequestParam(value = "nombreArchivo", required = false) String nombreArchivo,
    @RequestParam(value = "idHaya", required = false) String idHaya,
    @RequestParam(value = "usuarioCreador", required = false) String usuarioCreador,
    @RequestParam(value = "tipo", required = false) String tipo,
    @RequestParam(value = "idEntidad", required = false) String idEntidad,
    @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) throws IOException {
    if (idContrato == -1) {
      return contratoUseCase.findDocumentosByExpedienteId(idExpediente, idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, nombreArchivo, idHaya, orderDirection, orderField, size, page);
    } else {
      return contratoUseCase.findDocumentosByContratoId(idContrato, idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, nombreArchivo, idHaya, orderDirection, orderField, size, page);
    }
  }

  @ApiOperation(value = "Cambia el Contrato Representante del expediente")
  @PostMapping("/{idContrato}/contratoRepresentante")
  public void cambiarCR(@PathVariable("idExpediente") Integer idExpediente,
                                      @PathVariable("idContrato") Integer idContrato,
                                      @ApiIgnore CustomUserDetails principal)
    throws NotFoundException, IOException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    contratoUseCase.cambiarCR(idContrato,usuario);
  }

  @ApiOperation(value = "Añadir un documento a un contrato")
  @PostMapping("/{idContrato}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoContrato(@PathVariable("idExpediente") Integer idExpediente,
                                      @PathVariable("idContrato") Integer idContrato,
                                      @RequestPart("file") @NotNull @NotBlank MultipartFile file,
                                      @RequestPart("metadatos") MetadatoDocumentoInputDTO metadatoDocumento,
                                      @RequestParam(value = "tipoDocumento", required = false) String tipoDocumento,
                                      @ApiIgnore CustomUserDetails principal)
    throws NotFoundException, IOException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    contratoUseCase.createDocumento(idContrato, file, metadatoDocumento, usuario,tipoDocumento);
  }

  @ApiOperation(value = "Descargar información de contratos de un expediente en Excel", notes = "Descargar información en Excel de los contratos del expediente con ID enviado")
  @GetMapping("/excel")
  public ResponseEntity<InputStreamResource> findExcelContratosByExpediente(@PathVariable Integer idExpediente) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(contratoUseCase.findExcelContratosByExpediente(idExpediente));

    return excelExport.download(excel, "contratos_" + idExpediente + "_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  /*TODO pendiente recibir plantilla excel de descarga*/
  @ApiOperation(value = "Descargar información de contrato en Excel", notes = "Descargar información en Excel del contrato con ID enviado")
  @GetMapping("/{idContrato}/excel")
  public ResponseEntity<InputStreamResource> getExcelContratoById(@PathVariable Integer idContrato) throws IOException {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(contratoUseCase.findExcelContratoById(idContrato));

    return excelExport.download(excel, idContrato + "_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(value = "Todos los contratos de los Expedientes", notes = "Obtiene todos los contratos de cada expediente.")
  @GetMapping("/many")
  public List<ContratoDTOToList> getManyContratos(@RequestParam List<Integer> expedientes) throws IOException {
    return contratoUseCase.getAllContratos(expedientes);
  }

  @ApiOperation(
    value = "Todos los contratos en función de IdExpediente",
    notes = "Obtiene todos los contratos de según expediente.")
  @GetMapping("/listado")
  public List<ContratoDTOToList> findAllContratosByExpedienteId(@PathVariable Integer idExpediente)
    throws IOException, NotFoundException {
    return contratoUseCase.findAllContratosByExpedienteId(idExpediente);
  }

  @ApiOperation(value = "Create contrato", notes = "Crea un contrato para el expediente")
  @PostMapping()
  public void createContrato(
      @PathVariable Integer idExpediente, @RequestBody @Valid FormalizacionContratoInputDTO input)
      throws NotFoundException, RequiredValueException {
    contratoUseCase.createContrato(idExpediente, input);
  }
}
