package com.haya.alaska.contrato.infrastructure.repository;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface ContratoRepository extends JpaRepository<Contrato, Integer> {

  Contrato getContratoByExpedienteIdAndEsRepresentanteIsTrue(Integer idExpediente);

  @Query(
          value =
                  "SELECT c FROM Contrato c WHERE c.expediente.id IN (:ids) AND c.esRepresentante = true")
  List<Contrato> getContratosRepresentanteByExpedientesIds(@Param("ids") List<Integer> idExpediente);
  //  List<Contrato> getContratosRepresentanteByExpedientesIdsAndEsRepresentanteIsTrue(List<String>
  // idExpediente);

  long countByExpedienteId(Integer idExpediente);

  long countByExpediente_CarteraAndActivoIsTrue(Cartera cartera);

  List<Contrato> findAllByIdIn(List<Integer> ids);

  Page<Contrato> findAllByExpedienteId(Integer idExpediente, Pageable pageable);

  List<Contrato> findAllByExpedienteId(Integer idExpediente);

  List<Contrato> findAllByExpedienteIdIn(List<Integer> idsExpedientes);

  Optional<Contrato> findByIdCarga(String idCarga);

  Optional<Contrato> findByIdCargaAndExpedienteIdCarga(String idCarga, String expedienteIdCarga);

  Optional<Contrato> findByIdCargaAndExpedienteCarteraAndActivoIsTrue(
          String idCarga, Cartera cartera);

  Optional<Contrato> findByIdCargaAndExpedienteCarteraIdCarga(
          String idCarga, String expCarteraIdCarga);

  Optional<Contrato> findByIdCargaAndExpedienteIdCargaAndExpedienteCarteraIdCarga(
          String idCarga, String expedienteIdCarga, String expCarteraIdCarga);

  List<Contrato> findAllByExpedienteCartera(Cartera cartera);

  List<Contrato> findAllByExpedienteCarteraIdCargaAndExpedienteCarteraIdCargaSubcarteraIsNotNull(String idCarga);

  @Modifying(clearAutomatically = true)
  @Query(value = "UPDATE MSTR_CONTRATO MC SET MC.IND_ACTIVO = 0, MC.ID_ESTADO_CONTRATO = :idEstado  " +
          "WHERE MC.ID_EXPEDIENTE IN (SELECT ME.ID FROM MSTR_EXPEDIENTE ME  " +
          "INNER JOIN MSTR_CARTERA MC2 on ME.ID_CARTERA = MC2.ID " +
          "WHERE MC2.DES_ID_CARGA = :idCarga  " +
          "AND MC2.DES_ID_CARGA_SUBCARTERA IS NOT NULL)", nativeQuery = true)
  void updateAllBorradoDeltasContrato(Integer idEstado, String idCarga);

  @Modifying(clearAutomatically = true)
  @Query(value = "UPDATE MSTR_BIEN MB  " +
          "INNER JOIN RELA_CONTRATO_BIEN RCB ON MB.ID = RCB.ID_BIEN  " +
          "INNER JOIN MSTR_CONTRATO MC ON RCB.ID_CONTRATO = MC.ID  " +
          "INNER JOIN MSTR_EXPEDIENTE ME ON MC.ID_EXPEDIENTE = ME.ID  " +
          "INNER JOIN MSTR_CARTERA MC2 ON ME.ID_CARTERA = MC2.ID  " +
          "SET MB.IND_ACTIVO = 0, MC.IND_ACTIVO = 0  " +
          "WHERE MC2.DES_ID_CARGA = :idCarga ", nativeQuery = true)
  void updateAllBorradoDeltasBien(String idCarga);

  @Modifying(clearAutomatically = true)
  @Query(value =
          "UPDATE MSTR_INTERVINIENTE MI  " +
                  " INNER JOIN RELA_CONTRATO_INTERVINIENTE RCI ON MI.ID = RCI.ID_INTERVINIENTE   " +
                  " INNER JOIN MSTR_CONTRATO MC ON RCI.ID_CONTRATO = MC.ID  " +
                  " INNER JOIN MSTR_EXPEDIENTE ME ON MC.ID_EXPEDIENTE = ME.ID  " +
                  " INNER JOIN MSTR_CARTERA MC2 ON ME.ID_CARTERA = MC2.ID  " +
                  " SET MI.IND_ACTIVO = 0, MC.IND_ACTIVO = 0  " +
                  " WHERE MC2.DES_ID_CARGA = :idCarga ",
          nativeQuery = true)
  void updateAllBorradoDeltasInterviniente(String idCarga);


  List<Contrato> findAllByExpedienteIdCargaAndExpedienteCartera(String expIdCarga, Cartera cartera);
  //  List<Contrato> findAllByExpedienteIdCargaAndExpedienteCarteraAndIntervinientes(String
  // expIdCarga, Cartera cartera, Interviniente interviniente);

  @Query(
          value =
                  "SELECT  COUNT(DISTINCT c.producto.id) "
                          + "FROM Contrato c "
                          + "INNER JOIN Expediente e ON c.expediente.id = e.id "
                          + "INNER JOIN Cartera c2 On e.cartera.id = c2.id "
                          + "WHERE c2.idCarga = :carteraIdCarga")
  Long countProductsByExpedienteCartera(@Param("carteraIdCarga") String carteraIdCartera);

  //  @Query(value ="SELECT  new es.haya.alaska.orm.vo.ctc.ClasificacionContableStats(cc, count(c))
  // "
  //          + "FROM Contrato c "
  //          + "INNER JOIN ClasificacionContable cc ON c.clasificacionContable.id = cc.id "
  //          + "INNER JOIN Expediente e ON c.expediente.id = e.id "
  //          + "INNER JOIN Cartera c2 On e.cartera.id = c2.id "
  //          + "WHERE c2.id = :id "
  //          + "AND c.activo = true "
  //          + "GROUP BY c.clasificacionContable")
  //  List<Contrato> countByClasificacionContableInCartera(@Param("id") String id);

  long countByExpediente_Cartera_IdAndActivoIsFalseOrActivoIsNull(Integer idCartera);

  long countByExpediente_Cartera_IdAndActivoIsTrueAndEnRevisionIsNullOrEnRevisionIsTrue(
          Integer idCartera);

  long countByExpediente_Cartera_IdAndActivoIsTrueAndEnRevisionIsNotNullAndEnRevisionIsFalse(
          Integer idCartera);

  List<Contrato> findAllByExpedienteIdCargaAndExpedienteCarteraIdCarga(
          String expedienteIdCarga, String expedienteCarteraIdCarga);

  List<Contrato> findAllByIdInOrderByDeudaImpagada(Iterable<Integer> ids);

  List<Contrato> findAllByIdInOrderByFechaPrimerImpago(Iterable<Integer> ids);

  List<Contrato> findAllByIdInOrderByIdAsc(Iterable<Integer> ids);

  @Query(
          value =
                  "SELECT ch.ID as idContrato, ch.IND_ACTIVO, ch.REV as revInfo, rh.timestamp as fecha, rh.ID_USUARIO, ex.DES_ID_CONCATENADO as expedienteId, ex.ID as idEx, usu.DES_NOMBRE as usuarioGestor "
                          + "FROM HIST_MSTR_CONTRATO ch INNER JOIN REGISTRO_HISTORICO rh ON ch.REV = rh.id "
                          + "INNER JOIN RELA_ASIGNACION_EXPEDIENTE asex ON ch.ID_EXPEDIENTE = asex.ID_EXPEDIENTE "
                          + "INNER JOIN MSTR_USUARIO usu ON asex.ID_USUARIO = usu.ID "
                          + "INNER JOIN MSTR_EXPEDIENTE ex ON ch.ID_EXPEDIENTE = ex.ID "
                          + "WHERE ch.ID = ?1 "
                          + "AND ch.ID_EXPEDIENTE = ?2 AND usu.ID_PERFIL = 1 "
                          + "ORDER BY rh.timestamp ASC",
          nativeQuery = true)
  List<Map> findAllRegistroHistoricoByContratoId(Integer id, Integer idExpediente);

  @Query(
          value =
                  "SELECT usu.DES_NOMBRE as nombre "
                          + "FROM HIST_RELA_ASIGNACION_EXPEDIENTE hrae INNER JOIN REGISTRO_HISTORICO rh ON hrae.REV = rh.id "
                          + "INNER JOIN MSTR_USUARIO usu ON hrae.ID_USUARIO = usu.ID "
                          + "WHERE hrae.ID_EXPEDIENTE = ?1 AND usu.ID_PERFIL = 1 "
                          + "AND rh.timestamp <= ?2 "
                          + "ORDER BY rh.timestamp DESC",
          nativeQuery = true)
  List<Map> findUsuHistGestorLess(Integer idExpediente, BigInteger fecha);

  @Query(
          value =
                  "SELECT usu.DES_NOMBRE as nombre "
                          + "FROM HIST_RELA_ASIGNACION_EXPEDIENTE hrae INNER JOIN REGISTRO_HISTORICO rh ON hrae.REV = rh.id "
                          + "INNER JOIN MSTR_USUARIO usu ON hrae.ID_USUARIO = usu.ID "
                          + "WHERE hrae.ID_EXPEDIENTE = ?1 AND usu.ID_PERFIL = 1 "
                          + "AND rh.timestamp > ?2 "
                          + "ORDER BY rh.timestamp ASC",
          nativeQuery = true)
  List<Map> findUsuHistGestorBig(Integer idExpediente, BigInteger fecha);

  @Query(
          value =
                  "SELECT DISTINCT ch.ID as idContrato, rh.timestamp as fecha, rea.ID_ESTRATEGIA as idEstrategia "
                          + "FROM HIST_MSTR_CONTRATO ch "
                          + "INNER JOIN HIST_RELA_ESTRATEGIA_ASIGNADA rea ON ch.ID_ESTRATEGIA_ASIGNADA = rea.ID "
                          + "INNER JOIN REGISTRO_HISTORICO rh ON rea.REV = rh.id "
                          + "WHERE ch.ID_EXPEDIENTE = ?1 "
                          + "AND ch.IND_REPRESENTANTE is TRUE "
                          + "AND rh.timestamp <= ?2 "
                          + "ORDER BY rh.timestamp DESC",
          nativeQuery = true)
  List<Map> findAllForEstrategiaLess(Integer idExpediente, Timestamp fecha);

  @Query(
          value =
                  "SELECT DISTINCT ch.ID as idContrato, rh.timestamp as fecha, rea.ID_ESTRATEGIA as idEstrategia "
                          + "FROM HIST_MSTR_CONTRATO ch "
                          + "INNER JOIN HIST_RELA_ESTRATEGIA_ASIGNADA rea ON ch.ID_ESTRATEGIA_ASIGNADA = rea.ID "
                          + "INNER JOIN REGISTRO_HISTORICO rh ON rea.REV = rh.id "
                          + "WHERE ch.ID_EXPEDIENTE = ?1 "
                          + "AND ch.IND_REPRESENTANTE is TRUE "
                          + "AND rh.timestamp > ?2 "
                          + "ORDER BY rh.timestamp ASC",
          nativeQuery = true)
  List<Map> findAllForEstrategiaBigg(Integer idExpediente, Timestamp fecha);

  @Query(value = "SELECT " +
          "ch.ID as id, " +
          //"ch.ID_EXPEDIENTE as expediente, " +
          "ch.ID_ESTRATEGIA_ASIGNADA as estrategia, " +
          "ch.IND_REPRESENTANTE as esRepresentante, " +
          "ch.DES_ID_HAYA as idHaya, " +
          "ch.DES_ID_ORIGEN as idOrigen, " +
          "ch.DES_ID_ORIGEN_2 as idOrigen2, " +
          "ch.DES_ID_DATATAPE as idDatatape, " +
          "ch.IND_EN_REVISION as enRevision, " +
          "ch.IND_VENCIMIENTO_ANTICIPADO as vencimientoAnticipado, " +
          "ch.ID_PRODUCTO as producto, " +
          "ch.ID_SISTEMA_AMORTIZACION as SistemaAmortizacion, " +
          "ch.DES_PRODUCTO as descripcionProducto, " +
          "ch.ID_INDICE_REFERENCIA as indiceReferencia, " +
          "ch.ID_SITUACION as situacion, " +
          "ch.NUM_DIFERENCIAL as diferencial, " +
          "ch.NUM_TIN as tin, " +
          "ch.NUM_TAE as tae, " +
          "ch.IND_REESTRUCTURADO as reestructurado, " +
          "ch.ID_TIPO_REESTRUCTURACION as tipoReestructuracion, " +
          "ch.ID_CLASIFICACION_CONTABLE as clasificacionContable, " +
          "ch.FCH_FORMALIZACION as fechaFormalizacion, " +
          "ch.FCH_VENC_INICIAL as fechaVencimientoInicial, " +
          "ch.FCH_VENC_ANTICIPADO as fechaVencimientoAnticipado, " +
          "ch.FCH_PASE_REGULAR as fechaPaseRegular, " +
          "ch.NUM_CUOTAS_PENDIENTES as cuotasPendientes, " +
          "ch.FCH_LIQUIDACION_CUOTA as fechaLiquidacionCuota, " +
          "ch.NUM_IMPORTE_INICIAL as importeInicial, " +
          "ch.NUM_IMPORTE_DISPUESTO as importeDispuesto, " +
          "ch.NUM_CUOTAS_TOTALES as cuotasTotales, " +
          "ch.NUM_IMPORTE_ULTIMA_CUOTA as importeUltimaCuota, " +
          "ch.FCH_PRIMER_IMPAGO as fechaPrimerImpago, " +
          "ch.NUM_DIAS_IMPAGO as diasImpago, " +
          "ch.NUM_CUOTAS_IMPAGADAS as numeroCuotasImpagadas, " +
          "ch.NUM_RESTO_HIPOTECARIO as restoHipotecario, " +
          "ch.DES_ORIGEN as origen, " +
          "ch.NUM_GASTOS_JUDICIALES as gastosJudiciales, " +
          "ch.NUM_IMPORTE_CUOTAS_IMPAGADAS as importeCuotasImpagadas, " +
          "ch.DES_NOTAS_1 as notas1, " +
          "ch.DES_NOTAS_2 as notas2, " +
          "ch.DES_COMPANIA as compania, " +
          "ch.NUM_DEUDA_IMAGADA as deudaImpagada, " +
          "ch.IND_ACTIVO as activo, " +
          "ch.ID_CARGA as idCarga, " +
          "ch.DES_CARTERA_REM as carteraREM, " +
          "ch.DES_SUBCARTERA_REM as subcarteraREM, " +
          "ch.DES_RANGO_HIPOTECARIO as rangoHipotecario, " +
          "ch.ID_ESTADO_CONTRATO as estado, " +
          "ch.NUM_SALDO_GESTION as saldoGestion, " +
          "ch.IND_CALCULADO as calculado, " +
          "ch.FCH_CARGA as fechaCarga, " +
          "ch.FCH_PRIMERA_ASIG as fechaPrimeraAsignacion, " +
          "ch.FCH_ULTIMA_ASIG as fechaUltimaAsignacione " +
          "FROM HIST_MSTR_CONTRATO ch JOIN REGISTRO_HISTORICO r ON ch.REV = r.id WHERE ch.ID = ?1 AND r.ID_USUARIO IS NULL ORDER BY timestamp DESC LIMIT 1", nativeQuery = true)
  Map findContratoHistoricoOldest(Integer idContrato);

  List<Contrato> findAllByBienesId(Integer idBien);

  List<Contrato> findAllByIntervinientesId(Integer idInterviniente);

  List<Contrato> findAllByCalculadoIsFalse();

  List<Contrato> findAllByExpedienteId(Integer idExpediente, Sort order);

  @Query(value =
          "select T3.ID 'idExpediente',T1.ID 'idContrato', T1.DES_PRODUCTO 'producto',T5.ID 'idCartera' ,T5.ID_RESPONSABLE  ,T1.NUM_SALDO_GESTION 'saldoGestion' ," +
                  " T1.ID_ESTRATEGIA_ASIGNADA 'estrategia' ,T8.id 'idSupervisor',T11.id 'idGestor' ,t2.ID_CAMPANIA 'idCampania'" +
                  "from mstr_contrato T1" +
                  "inner join mstr_expediente T3 on T1.ID_EXPEDIENTE =T3.ID" +
                  " inner join mstr_cartera T5 on T3.ID_CARTERA =T5.ID" +
                  " inner join rela_asignacion_expediente T7 on T3.ID = T7.ID_EXPEDIENTE" +
                  " inner join mstr_usuario T8 on t7.ID_USUARIO = t8.ID" +
                  " inner join mstr_perfil T9 on T8.ID_PERFIL = T9.ID" +
                  " inner join rela_asignacion_expediente T10 on T3.ID = T10.ID_EXPEDIENTE" +
                  " inner join mstr_usuario T11 on t10.ID_USUARIO = t11.ID" +
                  " inner join mstr_perfil T12 on T11.ID_PERFIL = T12.ID" +
                  " LEFT JOIN rela_campania_asignada T2 ON T1.id = T2.id" +
                  " inner join mstr_campania T4 on T2.ID_CAMPANIA = T4.ID" +
                  " where T7.IND_ACTIVO = true" +
                  " and T9.DES_NOMBRE = 'Supervisor'" +
                  " and T12.DES_NOMBRE = 'Gestor'", nativeQuery = true)
  List<Map> findContratosRelCampania2();

  List<Contrato> findAllByIdNotIn(List<Integer> lista);

  Optional<Contrato> findByIdHaya(String idHaya);

  Optional<Contrato> findAllByIdHaya(String idHaya);

  List<Contrato> findAllByFechaCargaAndIdHayaIsNull(Date fecha);

  List<Contrato> findAllByIdHayaIsNotNull();

  //Métodos para segmentacion
  //Situacion
  Integer countAllBySituacionIdAndExpedienteIdIn(Integer idCat, List<Integer> idsExp);
  @Query(
          value =
                  "SELECT SUM(NUM_SALDO_GESTION) "
                          + "FROM MSTR_CONTRATO "
                          + "WHERE ID_SITUACION = ?1 "
                          + "AND ID_EXPEDIENTE IN ?2 ;",
          nativeQuery = true
  )
  Double sumSaldoGestionBySituacionIdAndExpedienteIdIn(Integer idCat, List<Integer> idsExp);
  //Modo
  Integer countAllByExpedienteIdIn(List<Integer> idsExp);
  @Query(
          value =
                  "SELECT SUM(NUM_SALDO_GESTION) "
                          + "FROM MSTR_CONTRATO "
                          + "WHERE ID_EXPEDIENTE IN ?1 ;",
          nativeQuery = true
  )
  Double sumSaldoGestionByExpedienteIdIn(List<Integer> idsExp);
  //Producto
  Integer countAllByProductoIdAndExpedienteIdIn(Integer idCat, List<Integer> idsExp);
  @Query(
          value =
                  "SELECT SUM(NUM_SALDO_GESTION) "
                          + "FROM MSTR_CONTRATO "
                          + "WHERE ID_PRODUCTO = ?1 "
                          + "AND ID_EXPEDIENTE IN ?2 ;",
          nativeQuery = true
  )
  Double sumSaldoGestionByProductoIdAndExpedienteIdIn(Integer idCat, List<Integer> idsExp);
  //Judicial
  @Query(
          value =
                  "    SELECT COUNT(C1.ID) " +
                          "    FROM MSTR_CONTRATO AS C1 " +
                          "    LEFT JOIN RELA_CONTRATO_PROCEDIMIENTO AS R ON C1.ID = R.ID_CONTRATO " +
                          "    WHERE C1.ID_EXPEDIENTE IN ?1" +
                          "    HAVING COUNT(R.ID_PROCEDIMIENTO) = 0 ",
          nativeQuery = true
  )
  Integer countAllByCountProcedimientosZero(List<Integer> idsExp);
  @Query(
          value =
                  "    SELECT SUM(C1.NUM_SALDO_GESTION) " +
                          "    FROM MSTR_CONTRATO AS C1 " +
                          "    LEFT JOIN RELA_CONTRATO_PROCEDIMIENTO AS R ON C1.ID = R.ID_CONTRATO " +
                          "    WHERE C1.ID_EXPEDIENTE IN ?1" +
                          "    HAVING COUNT(R.ID_PROCEDIMIENTO) = 0 ",
          nativeQuery = true
  )
  Double sumAllByCountProcedimientosZero(List<Integer> idsExp);
  @Query(
          value =
                  "    SELECT COUNT(C1.ID) " +
                          "    FROM MSTR_CONTRATO AS C1 " +
                          "    LEFT JOIN RELA_CONTRATO_PROCEDIMIENTO AS R ON C1.ID = R.ID_CONTRATO " +
                          "    WHERE C1.ID_EXPEDIENTE IN ?1" +
                          "    HAVING COUNT(R.ID_PROCEDIMIENTO) > 0 ",
          nativeQuery = true
  )
  Integer countAllByCountProcedimientosMore(List<Integer> idsExp);
  @Query(
          value =
                  "    SELECT SUM(C1.NUM_SALDO_GESTION) " +
                          "    FROM MSTR_CONTRATO AS C1 " +
                          "    LEFT JOIN RELA_CONTRATO_PROCEDIMIENTO AS R ON C1.ID = R.ID_CONTRATO " +
                          "    WHERE C1.ID_EXPEDIENTE IN ?1" +
                          "    HAVING COUNT(R.ID_PROCEDIMIENTO) > 0 ",
          nativeQuery = true
  )
  Double sumAllByCountProcedimientosMore(List<Integer> idsExp);
  //Clasificacion
  Integer countAllByClasificacionContableIdAndExpedienteIdIn(Integer idCat, List<Integer> idsExp);
  @Query(
          value =
                  "SELECT SUM(NUM_SALDO_GESTION) "
                          + "FROM MSTR_CONTRATO "
                          + "WHERE ID_CLASIFICACION_CONTABLE = ?1 "
                          + "AND ID_EXPEDIENTE IN ?2 ;",
          nativeQuery = true
  )
  Double sumSaldoGestionByClasificacionContableIdAndExpedienteIdIn(Integer idCat, List<Integer> idsExp);
}
