package com.haya.alaska.contrato.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EstrategiaAsignadaOutputDTO implements Serializable {
  Integer id;
  Boolean activo;
  Date fecha;

  CatalogoMinInfoDTO estrategia;
  CatalogoMinInfoDTO tipoEstrategia;
  CatalogoMinInfoDTO origenEstrategia;
  CatalogoMinInfoDTO periodoEstrategia;

  public EstrategiaAsignadaOutputDTO(EstrategiaAsignada ea){
    BeanUtils.copyProperties(ea, this);
    this.estrategia = ea.getEstrategia() != null ? new CatalogoMinInfoDTO(ea.getEstrategia()) : null;
    this.tipoEstrategia = ea.getTipoEstrategia() != null ? new CatalogoMinInfoDTO(ea.getTipoEstrategia()) : null;
    this.origenEstrategia = ea.getOrigenEstrategia() != null ? new CatalogoMinInfoDTO(ea.getOrigenEstrategia()) : null;
    this.periodoEstrategia = ea.getPeriodoEstrategia() != null ? new CatalogoMinInfoDTO(ea.getPeriodoEstrategia()) : null;
  }
}
