package com.haya.alaska.contrato.infrastructure.controller;

import com.haya.alaska.shared.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.haya.alaska.contrato.application.ContratoUseCase;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDtoOutput;

import io.swagger.annotations.ApiOperation;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/contratodeudor/{idExpediente}")
public class ContratoDeudorController {

    @Autowired
    private ContratoUseCase contratoUseCase;

    @ApiOperation(value = "Obtener contrato", notes = "Devuelve el contrato con id enviado")
    @GetMapping("/{idContrato}")
    @Transactional
    public ContratoDtoOutput getContratoById(@PathVariable("idContrato") Integer idContrato)
	    throws IllegalAccessException, NotFoundException {
	return contratoUseCase.findContratoByIdDto(idContrato);
    }

}
