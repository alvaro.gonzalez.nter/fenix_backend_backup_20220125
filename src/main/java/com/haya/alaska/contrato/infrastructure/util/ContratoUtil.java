package com.haya.alaska.contrato.infrastructure.util;

import java.util.List;
import java.util.stream.Collectors;

import com.haya.alaska.procedimiento.domain.Procedimiento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;

@Component
public class ContratoUtil {

  @Autowired ContratoIntervinienteRepository contratoIntervinienteRepository;
  @Autowired ContratoRepository contratoRepository;

  public Contrato addIntervinientesContrato(Integer idContrato, List<Integer> idIntervinientes)
      throws Exception {
    Contrato contrato =
        contratoRepository
            .findById(idContrato)
            .orElseThrow(() -> new NotFoundException("contrato", idContrato));
    List<ContratoInterviniente> intervinientees =
        contratoIntervinienteRepository.findAllByIntervinienteIdIn(idIntervinientes);
    contrato.addIntervinientes(intervinientees);
    return contratoRepository.save(contrato);
  }

  public Procedimiento findInContrato(Contrato contrato, Integer idProcedimiento){
    if (idProcedimiento == null) return null;
    for (Procedimiento pr : contrato.getProcedimientos()){
      if (pr.getId().equals(idProcedimiento)) return pr;
    }
    return null;
  }
}
