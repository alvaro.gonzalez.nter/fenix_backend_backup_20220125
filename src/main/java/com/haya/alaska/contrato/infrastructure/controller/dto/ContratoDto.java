package com.haya.alaska.contrato.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ContratoDto implements Serializable {

  private static final long serialVersionUID = 1L;
  
  private Integer id;
  private Double diferencial;
  private Double tin;
  private Double tae;
  private Boolean reestructurado;
  private Integer idCampania;
  private String campania;
  private Date fechaFormalizacion;
  private Date fechaVencimientoInicial;
  private Date fechaVencimientoAnticipado;
  private Date fechaPaseRegular;
  private Integer cuotasPendientes;
  private Date fechaLiquidacionCuota;
  private Double importeInicial;
  private Double importeDispuesto;
  private Integer cuotasTotales;
  private Double importeUltimaCuota;
  private Date fechaPrimerImpago;
  private Integer diasImpago;
  private Double restoHipotecario;
  private String rangoHipotecario;
  private String origen;
  private Double gastosJudiciales;
  private Double importeCuotasImpagadas;
  private Integer numeroCuotasImpagadas;
  private String notas1;
  private String notas2;
  private String idCarga;
  private String territorial;
}
