package com.haya.alaska.contrato.application;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoInformeExcel;
import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.ContratoInformeExcel;
import com.haya.alaska.contrato.infrastructure.controller.dto.InformeContratoInputDTO;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.application.ExpedienteUseCase;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.ExpedienteCarteraExcel;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ContratoExcelUseCaseImpl implements ContratoExcelUseCase {

  @Autowired private CarteraRepository carteraRepository;

  @Autowired private EventoUtil eventoUtil;

  @Autowired ExpedienteUtil expedienteUtil;

  @Autowired ExpedienteUseCase expedienteUseCase;

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelPlanesPagoById(
      Integer carteraId,
      Integer mes,
      InformeContratoInputDTO informeContratoInputDTO,
      Usuario usuario)
      throws Exception {

    Perfil perfilUsu = usuario.getPerfil();

    if (perfilUsu.getNombre().equals("Responsable de cartera")
        || perfilUsu.getNombre().equals("Supervisor")) {
      var datos = this.createContratoDataForExcel();
      if (carteraId != null) {
        Cartera cartera =
            carteraRepository
                .findById(carteraId)
                .orElseThrow(() -> new NotFoundException("Cartera", carteraId));
        this.addContratoDataForExcel(cartera, datos, mes, informeContratoInputDTO);
      } else {
        for (Cartera cartera : carteraRepository.findAll()) {
          this.addContratoDataForExcel(cartera, datos, mes, informeContratoInputDTO);
        }
      }
      return datos;
    } else {
      throw new IllegalArgumentException(
          "El tipo del usuario debe ser Responsable de cartera o Supervisor para poder generar el Informe");
    }
  }

  private LinkedHashMap<String, List<Collection<?>>> createContratoDataForExcel() {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expedientes = new ArrayList<>();
    expedientes.add(ExpedienteCarteraExcel.cabeceras);

    List<Collection<?>> contratos = new ArrayList<>();
    contratos.add(ContratoInformeExcel.cabeceras);

    List<Collection<?>> planDePagos = new ArrayList<>();
    planDePagos.add(AcuerdoInformeExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      datos.put("Connection", expedientes);
      datos.put("Borrowers", contratos);
      datos.put("Payment plan", planDePagos);

    } else {

      datos.put("Expediente", expedientes);
      datos.put("Contratos", contratos);
      datos.put("Plan De Pagos", planDePagos);
    }

    return datos;
  }

  private void addContratoDataForExcel(
      Cartera cartera,
      LinkedHashMap<String, List<Collection<?>>> datos,
      Integer mes,
      InformeContratoInputDTO informeContratoInputDTO)
      throws ParseException, NotFoundException {

    List<Collection<?>> expedientes;
    List<Collection<?>> contratos;
    List<Collection<?>> planDePagos;

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      expedientes = datos.get("Connection");
      contratos = datos.get("Borrowers");
      planDePagos = datos.get("Payment plan");

    } else {

      expedientes = datos.get("Expediente");
      contratos = datos.get("Contratos");
      planDePagos = datos.get("Plan De Pagos");
    }

    /*  Date fechaInicioInicialD = informeContratoInputDTO.getFechaInicioInicial() != null ? new SimpleDateFormat("yyyy-MM-dd").parse(informeContratoInputDTO.getFechaInicioInicial()) : null;
    Date fechaInicioFinalD = informeContratoInputDTO.getFechaInicioFinal() != null ? new SimpleDateFormat("yyyy-MM-dd").parse(informeContratoInputDTO.getFechaInicioFinal()) : null;
    Date fechaFinInicialD = informeContratoInputDTO.getFechaFinInicial() != null ? new SimpleDateFormat("yyyy-MM-dd").parse(informeContratoInputDTO.getFechaFinInicial()) : null;
    Date fechaFinFinalD = informeContratoInputDTO.getFechaFinFinal() != null ? new SimpleDateFormat("yyyy-MM-dd").parse(informeContratoInputDTO.getFechaFinFinal()) : null;*/

    for (Expediente expediente : cartera.getExpedientes()) {

      boolean seguir = false;
      for (Contrato contrato : expediente.getContratos()) {
        if (contrato.getAcuerdosPago().size() > 0) seguir = true;
      }
      if (!seguir) continue;

      Boolean comprobacion = expedienteUseCase.filtroMesInformes(expediente, mes);
      var estadoExpediente = expedienteUtil.getEstado(expediente);

      if (comprobacion) {
        if (informeContratoInputDTO.getEstadoExpediente() == null
            || estadoExpediente.getId().equals(informeContratoInputDTO.getEstadoExpediente())) {

          Evento accion = getLastEvento(expediente.getId(), 1);
          Evento alerta = getLastEvento(expediente.getId(), 2);
          Evento actividad = getLastEvento(expediente.getId(), 3);
          Procedimiento p = expedienteUtil.getUltimoProcedimiento(expediente);
          expedientes.add(
              new ExpedienteCarteraExcel(expediente, accion, alerta, actividad, p).getValuesList());
          for (Contrato contrato : expediente.getContratos()) {

            if (contrato.getAcuerdosPago().size() == 0) continue;

            contratos.add(new ContratoInformeExcel(contrato).getValuesList());

            List<AcuerdoPago> acuerdosFilterFinal =
                contrato.getAcuerdosPago().stream()
                    .filter(
                        acuerdoPago -> {
                          Date fCE = acuerdoPago.getFechaInicio();
                          Date fCI = informeContratoInputDTO.getFechaInicioInicial();
                          Date fCF = informeContratoInputDTO.getFechaInicioFinal();
                          if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;

                          Date fRE = acuerdoPago.getFechaFinal();
                          Date fRI = informeContratoInputDTO.getFechaFinInicial();
                          Date fRF = informeContratoInputDTO.getFechaFinFinal();
                          if (!expedienteUtil.comprobarEntre(fRE, fRI, fRF)) return false;
                          return true;
                        })
                    .collect(Collectors.toList());

            for (AcuerdoPago acuerdoPago : acuerdosFilterFinal) {

              if (informeContratoInputDTO.getTipo() == null
                  || acuerdoPago
                      .getOrigenAcuerdo()
                      .getId()
                      .equals(informeContratoInputDTO.getTipo())) {
                planDePagos.add(new AcuerdoInformeExcel(acuerdoPago).getValuesList());
              }
            }
          }
        }
      }
    }
  }

  public Evento getLastEvento(Integer idExpediente, Integer clase) {
    try {
      List<Integer> clases = new ArrayList<>();
      clases.add(clase);
      List<Evento> eventos =
          eventoUtil
              .getFilteredEventos(
                  null,
                  null,
                  null,
                  1,
                  idExpediente,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  clases,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  "fechaCreacion",
                  "desc",
                  0, 100);

      Evento result = null;
      if (!eventos.isEmpty()) result = eventos.get(0);
      return result;
    } catch (Exception e) {
      return null;
    }
  }
}
