package com.haya.alaska.contrato.application;

import com.haya.alaska.contrato.infrastructure.controller.dto.InformeContratoInputDTO;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface ContratoExcelUseCase {
  LinkedHashMap<String, List<Collection<?>>> findExcelPlanesPagoById(Integer carteraId, Integer mes, InformeContratoInputDTO informeContratoInputDTO,
                                                                     Usuario usuario) throws Exception;
}
