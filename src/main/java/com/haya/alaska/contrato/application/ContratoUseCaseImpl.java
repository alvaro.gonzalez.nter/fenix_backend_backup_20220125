package com.haya.alaska.contrato.application;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.BienExcel;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.domain.CargaExcel;
import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.ContratoExcel;
import com.haya.alaska.contrato.domain.ContratoJudicialExcel;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato.historico.ContratoHistoricoAsignaciones;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDtoInput;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDtoOutput;
import com.haya.alaska.contrato.infrastructure.mapper.ContratoMapper;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.domain.ContratoBien_;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente_;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.domain.DatosContactoExcel;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.estado_contrato.domain.EstadoContrato_;
import com.haya.alaska.estrategia.domain.Estrategia_;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada_;
import com.haya.alaska.expediente.application.ExpedienteExcelUseCase;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input.FormalizacionContratoInputDTO;
import com.haya.alaska.formalizacion.infrastructure.mapper.FormalizacionMapper;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.application.GestorDocumentalUseCase;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.domain.enums.TipoEntidadEnum;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.domain.IntervinienteExcel;
import com.haya.alaska.interviniente.domain.Interviniente_;
import com.haya.alaska.localidad.domain.DatosLocalizacionExcel;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.domain.MovimientoExcel;
import com.haya.alaska.procedimiento.domain.*;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.producto.domain.Producto_;
import com.haya.alaska.registro_historico.application.RegistroHistoricoUseCase;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.domain.Saldo_;
import com.haya.alaska.saldo.infrastructure.controller.dto.SaldoDto;
import com.haya.alaska.saldo.infrastructure.repository.SaldoRepository;
import com.haya.alaska.shared.DemandadoJudicialExcel;
import com.haya.alaska.shared.JudicialExcel;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.dto.HistoricoAsignacionesDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.situacion.domain.Situacion;
import com.haya.alaska.situacion.domain.Situacion_;
import com.haya.alaska.subasta.domain.Subasta;
import com.haya.alaska.subasta.domain.SubastasExcel;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.domain.TasacionExcel;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion_;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.valoracion.domain.ValoracionExcel;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.StringUtil;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ContratoUseCaseImpl implements ContratoUseCase {

  @PersistenceContext private EntityManager entityManager;
  @Autowired private ContratoRepository contratoRepository;
  @Autowired private SaldoRepository saldoRepository;
  @Autowired private ExpedienteExcelUseCase expedienteExcelUseCase;
  @Autowired private RegistroHistoricoUseCase registroHistoricoUseCase;
  @Autowired private ContratoMapper contratoMapper;
  @Autowired private ServicioGestorDocumental servicioGestorDocumental;
  @Autowired private ExpedienteRepository expedienteRepository;
  @Autowired private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired private GestorDocumentalUseCase gestorDocumentalUseCase;
  @Autowired private FormalizacionMapper formalizacionMapper;


  public static final String DEUDA = "deuda";
  public static final String RIESGO = "riesgo";
  public static final String ANTIGUEDAD = "ant-deuda";
  public static final String NUMERO = "num";
  public static final String PRODUCTO = "producto";

  @Override
  public ListWithCountDTO<ContratoDTOToList> getAllFilteredContratos(
          Integer idExpediente, Integer id, String idOrigen, Boolean esRepresentante, String producto, String estadoContratos,
          String primerTitular, String saldoEnGestion, String intervinientes, String garantias, Boolean judicial,
         Boolean estado, String situacion, String estrategia, String orderField, String orderDirection, Integer size, Integer page) {
    List<Contrato> contratosSinPaginar = filterContratosInMemory(
            idExpediente, id,idOrigen,  esRepresentante, producto, estadoContratos, primerTitular, saldoEnGestion, intervinientes,
            garantias, judicial, estado, situacion, estrategia, orderField, orderDirection);


    List<ContratoDTOToList> contratosDTOS =contratoMapper.listContratoToListContratoDTOToList(contratosSinPaginar);
    Comparator<ContratoDTOToList> comparator = (var o1, var o2) -> Boolean.compare(o1.getJudicial(), o2.getJudicial());
    if(orderDirection != null && orderDirection.equals("desc") && orderField.equals("judicial")) contratosDTOS = contratosDTOS.stream().sorted(comparator).collect(Collectors.toList());
    else if(orderDirection != null && orderDirection.equals("asc")  && orderField.equals("judicial"))contratosDTOS = contratosDTOS.stream().sorted(comparator.reversed()).collect(Collectors.toList());

    if(saldoEnGestion != null) contratosDTOS = contratosDTOS.stream().filter((var o1) -> StringUtil.startsWithIgnoreCase(o1.getSaldoEnGestion() + "", saldoEnGestion)).collect(Collectors.toList());

    contratosDTOS = contratosDTOS.stream().skip(size * page).limit(size).collect(Collectors.toList());

    return new ListWithCountDTO<>(contratosDTOS, contratosSinPaginar.size());
  }

  public List<Contrato> filterContratosInMemory(
          Integer idExpediente, Integer id,String idOrigen, Boolean esRepresentante, String producto, String estadoContratos,
          String primerTitular, String saldoEnGestion, String intervinientes, String garantias, Boolean judicial,
          Boolean estado, String situacion, String estrategia, String orderField, String orderDirection) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Contrato> query = cb.createQuery(Contrato.class);


    Root<Contrato> root = query.from(Contrato.class);
    List<Predicate> predicates = new ArrayList<>();

    Join<Contrato, Expediente> destinatarioJoin = root.join(Contrato_.expediente, JoinType.INNER);
    Predicate onExpediente = cb.equal(destinatarioJoin.get(Expediente_.id), idExpediente);
    destinatarioJoin.on(onExpediente);

    if (id != null)
      predicates.add(cb.like(root.get(Contrato_.ID).as(String.class), "%" + id + "%"));
    if (esRepresentante != null)
      predicates.add(cb.equal(root.get(Contrato_.esRepresentante), esRepresentante));
    if (producto != null) {
      Join<Contrato, Producto> productoJoin = root.join(Contrato_.producto, JoinType.INNER);
      Predicate onProducto = cb.like(productoJoin.get(Producto_.VALOR), "%" + producto + "%");
      productoJoin.on(onProducto);
    }
    if (idOrigen != null)
      predicates.add(cb.like(root.get(Contrato_.idCarga), "%" + idOrigen + "%"));
    if (estadoContratos != null){
      Join<Contrato, EstadoContrato> estadoJoin = root.join(Contrato_.estadoContrato, JoinType.INNER);
      Predicate onEstado = cb.like(estadoJoin.get(EstadoContrato_.VALOR), "%" + estadoContratos + "%");
      estadoJoin.on(onEstado);
    }

    if (estadoContratos != null){
      Join<Contrato, EstadoContrato> estadoJoin = root.join(Contrato_.estadoContrato, JoinType.INNER);
      Predicate onEstado = cb.like(estadoJoin.get(EstadoContrato_.VALOR), "%" + estadoContratos + "%");
      estadoJoin.on(onEstado);
    }

    if (estrategia != null){
      Join<Contrato, EstrategiaAsignada> estadoJoin = root.join(Contrato_.estrategia, JoinType.INNER);
      Predicate onEstrategia = cb.like(estadoJoin.get(EstrategiaAsignada_.estrategia).get(Estrategia_.VALOR), "%" + estrategia + "%");
      estadoJoin.on(onEstrategia);
    }

    if (primerTitular != null) {
      Join<Contrato, ContratoInterviniente> join1 =
              root.join(Contrato_.INTERVINIENTES, JoinType.INNER);
      Join<ContratoInterviniente, Interviniente> join2 =
              join1.join(ContratoInterviniente_.INTERVINIENTE, JoinType.INNER);

      Predicate pInter = cb.conjunction();
      pInter.getExpressions().add(cb.isNotNull(join1.get(ContratoInterviniente_.ORDEN_INTERVENCION)));
      pInter.getExpressions().add(cb.equal(join1.get(ContratoInterviniente_.ORDEN_INTERVENCION), 1));
      pInter.getExpressions().add(cb.equal(join1.get(ContratoInterviniente_.TIPO_INTERVENCION)
              .get(TipoIntervencion_.VALOR), "TITULAR"));

      pInter.getExpressions().add(cb.or(
        cb.like(
          (Expression<String>) cb.upper(join1.get(ContratoInterviniente_.INTERVINIENTE)
            .get(Interviniente_.NOMBRE)).alias("nombre"),
          "%" + primerTitular.toUpperCase() + "%"),
        cb.like(
          (Expression<String>) cb.upper(join1.get(ContratoInterviniente_.INTERVINIENTE)
            .get(Interviniente_.APELLIDOS)).alias("apellidos"),
          "%" + primerTitular.toUpperCase() + "%")));


      predicates.add(pInter);
    }
    if (saldoEnGestion != null){/*
      Join<Contrato, Saldo> saldoJoin = root.join(Contrato_.SALDOS, JoinType.INNER);
      Expression<String> filterKeyExp = saldoJoin.get(Saldo_.SALDO_GESTION).as(String.class);
      String saldoString =  saldoEnGestion.toString().trim();
      Predicate onSaldo = cb.like(root.get(Contrato_.saldoGestion).as(String.class), "%" + saldoEnGestion + "%");
      Predicate isRepresentante = cb.equal(root.get(Contrato_.esRepresentante), true);
    //  predicates.add(cb.and(onSaldo, isRepresentante));
      predicates.add(cb.like(root.get(Contrato_.saldoGestion).as(String.class), "%" + saldoEnGestion + "%"));
      */
    }
    if (intervinientes != null){
      Subquery<Long> subqueryI = cb.createQuery().subquery(Long.class);
      Root<ContratoInterviniente> fromCI = subqueryI.from(ContratoInterviniente.class);
      subqueryI.select(cb.count(fromCI)).distinct(true).where(cb.equal(root.get(Contrato_.ID), fromCI.get(ContratoInterviniente_.CONTRATO).get(Contrato_.ID)));
      predicates.add(cb.like(subqueryI.as(String.class), intervinientes));
    }
    if (garantias != null){
      Subquery<Long> subqueryG = cb.createQuery().subquery(Long.class);
      Root<ContratoBien> fromCB = subqueryG.from(ContratoBien.class);
      subqueryG.select(cb.count(fromCB)).distinct(true).where(cb.equal(root.get(Contrato_.ID), fromCB.get(ContratoBien_.CONTRATO).get(Contrato_.ID)));
      predicates.add(cb.like(subqueryG.as(String.class), garantias));
    }
    if (judicial != null)
      if (!judicial) predicates.add(cb.isEmpty(root.get(Contrato_.procedimientos)));
      else predicates.add(cb.isNotEmpty(root.get(Contrato_.procedimientos)));
    if (situacion != null){
      Join<Contrato, Situacion> situacionJoin = root.join(Contrato_.situacion, JoinType.INNER);
      Predicate onSituacion = cb.like(situacionJoin.get(Situacion_.VALOR), "%" + situacion + "%");
      situacionJoin.on(onSituacion);
    }
    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
    if (orderField != null) {
      switch (orderField) {
        case "producto":
          Join<Contrato, Producto> join1 = root.join(Contrato_.producto, JoinType.INNER);
          rs.orderBy(getOrden(cb, join1.get(Producto_.valor), orderDirection));
          break;

        case "estadoContratos":
          Join<Contrato, EstadoContrato> join2 =
              root.join(Contrato_.estadoContrato, JoinType.INNER);
          rs.orderBy(getOrden(cb, join2.get(EstadoContrato_.VALOR), orderDirection));
          break;

        case "primerTitular":
          Join<Contrato, ContratoInterviniente> join31 =
            root.join(Contrato_.INTERVINIENTES, JoinType.INNER);
          Join<ContratoInterviniente, Interviniente> join32 =
            join31.join(ContratoInterviniente_.INTERVINIENTE, JoinType.INNER);
          rs.orderBy(getOrden(cb,join32.get(Interviniente_.nombre),orderDirection));
          break;
        case "saldo":
          Join<Contrato, Saldo> join4 = root.join(Contrato_.SALDOS, JoinType.INNER);
          rs.orderBy(getOrden(cb,join4.get(Saldo_.saldoGestion),orderDirection));
          break;
        case "garantias":
          Subquery<Long> subqueryG = cb.createQuery().subquery(Long.class);
          Root<ContratoBien> fromCB = subqueryG.from(ContratoBien.class);
      //(cb.count(fromCB))., fromCB.get(ContratoBien_.CONTRATO).get(Contrato_.ID)));
          subqueryG.select(cb.count(fromCB)).distinct(true).where(cb.equal(root.get(Contrato_.ID), fromCB.get(ContratoBien_.CONTRATO).get(Contrato_.ID)));
          rs.orderBy(getOrden(cb,subqueryG.as(String.class),orderDirection));
    break;

        case "intervinientes":
          Subquery<Long> subqueryI = cb.createQuery().subquery(Long.class);
          Root<ContratoInterviniente> fromCI = subqueryI.from(ContratoInterviniente.class);
          subqueryI.select(cb.count(fromCI)).distinct(true).where(cb.equal(root.get(Contrato_.ID), fromCI.get(ContratoInterviniente_.CONTRATO).get(Contrato_.ID)));
          rs.orderBy(getOrden(cb,subqueryI.as(String.class),orderDirection));
          break;

        case "situacion":
          Join<Contrato, Situacion> join5 = root.join(Contrato_.situacion, JoinType.INNER);
          rs.orderBy(getOrden(cb,join5.get(Situacion_.VALOR),orderDirection));
          break;

        case "saldoEnGestion":
          rs.orderBy(getOrden(cb, root.get(Contrato_.saldoGestion), orderDirection));
          break;

    /*    case "judicial":
        //  rs.orderBy(getOrden(cb,root.get(Contrato_.procedimientos)get(Procedimiento_.id), orderDirection));
          Join<Contrato,Procedimiento>joinPro=root.join(Contrato_.procedimientos, JoinType.INNER);
          rs.orderBy(getOrden(cb,joinPro.get((Procedimiento_.id)),orderDirection));
          break;*/
      }
    }
    return entityManager.createQuery(query).getResultList();
  }

  private Order getOrden(CriteriaBuilder cb,Expression expresion,String orderDirection)
  {
    if (orderDirection==null){
      orderDirection="desc";
    }
    return orderDirection.toLowerCase().equalsIgnoreCase("desc")?cb.desc(expresion):cb.asc(expresion);
  }


  @Override
  public ContratoDtoOutput findContratoByIdDto(Integer id) throws IllegalAccessException, NotFoundException {
    Contrato contrato = contratoRepository.findById(id).orElseThrow(() -> new NotFoundException("contrato", id));
    ContratoDtoOutput contratoDTO = contratoMapper.entityToOutputDto(contrato);

    Saldo saldo = saldoRepository.findTopByContratoIdOrderByDiaDesc(id).orElse(null);

    if (saldo != null) {
      SaldoDto saldoDto = new SaldoDto(saldo);

      Map historicoByIdContrato = contratoRepository.findContratoHistoricoOldest(contrato.getId());
      contratoDTO.setSaldo(saldoDto);
      contratoDTO.setHistorico(contratoMapper.mapToOutputDto(historicoByIdContrato));
    }
    return contratoDTO;
  }

  @Override
  public ContratoDtoOutput findContratoByIdCargaDto(String idCarga) throws IllegalAccessException {
    Contrato contrato = contratoRepository.findByIdCarga(idCarga).orElse(null);
    ContratoDtoOutput contratoDTO = null;
    if (contrato != null){
      contratoDTO = contratoMapper.entityToOutputDto(contrato);

      Saldo saldo = saldoRepository.findTopByContratoIdOrderByDiaDesc(contrato.getId()).orElse(null);

      if (saldo != null) {
        SaldoDto saldoDto = new SaldoDto(saldo);

        Map historicoByIdContrato = contratoRepository.findContratoHistoricoOldest(contrato.getId());
        contratoDTO.setSaldo(saldoDto);
        contratoDTO.setHistorico(contratoMapper.mapToOutputDto(historicoByIdContrato));
      }
    }

    return contratoDTO;
  }

  @Override
  public Contrato findContratoById(Integer id)
          throws NotFoundException {
    return contratoRepository.findById(id).get();
  }

  @Override
  public ContratoDtoOutput update(Integer idContrato, ContratoDtoInput contratoDTO) throws IllegalAccessException, NotFoundException, com.haya.alaska.shared.exceptions.NotFoundException {
    contratoMapper.inputDtoToEntity(idContrato, contratoDTO);
    return findContratoByIdDto(idContrato);
  }

  @Override
  public void cambiarCR(Integer idContrato,Usuario usuario) throws NotFoundException {
    Contrato contrato = contratoRepository.findById(idContrato).orElseThrow();
    Expediente expediente = contrato.getExpediente();
    for (Contrato contratoAux: expediente.getContratos()) {
      contratoAux.setEsRepresentante(false);
      if(contratoAux.getId() == contrato.getId())
        contratoAux.setEsRepresentante(true);
    }
    expedienteRepository.save(expediente);
  }

  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosByContratoId(Integer id,String idDocumento,String usuarioCreador,String tipo, String idEntidad,
  String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size, Integer page) throws IOException {
    Contrato contrato = contratoRepository.findById(id).orElse(null);
    List<DocumentoDTO>listadoIdGestor=this.servicioGestorDocumental
      .listarDocumentos(CodigoEntidad.CONTRATO, contrato.getIdHaya())
      .stream()
      .map(DocumentoDTO::new)
      .collect(Collectors.toList());


    List<DocumentoDTO>list=gestorDocumentalUseCase.listarDocumentosGestorContrato(listadoIdGestor).stream().collect(Collectors.toList());
    List<DocumentoDTO>listFinal=gestorDocumentalUseCase.filterOrderDocumentosDTO(list,idDocumento,usuarioCreador,tipo, idEntidad,fechaActualizacion,nombreArchivo,idHaya,orderDirection,orderField);
    List<DocumentoDTO>listaFiltradaPaginada=listFinal.stream().skip(size*page).limit(size).collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada,listFinal.size());
  }




  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosByExpedienteId(Integer id,String idDocumento,String usuarioCreador, String tipo, String idEntidad,
  String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection, String orderField,Integer size, Integer page) throws IOException {
    Expediente expediente = expedienteRepository.findById(id).orElse(null);
    Set<Contrato>listaContratos= expediente.getContratos();
    List<String>listaActivosContrato=new ArrayList<>();
    for (Contrato contrato : listaContratos) {
      if (contrato.getIdHaya()!=null){
        listaActivosContrato.add(contrato.getIdHaya());
      }
    }
    List<DocumentoDTO>listadoIdGestor= this.servicioGestorDocumental
      .listarDocumentosMulti(CodigoEntidad.CONTRATO, listaActivosContrato)
      .stream()
      .map(DocumentoDTO::new)
      .collect(Collectors.toList());

    List<DocumentoDTO>list=gestorDocumentalUseCase.listarDocumentosGestorContrato(listadoIdGestor).stream().collect(Collectors.toList());
    List<DocumentoDTO>listFinal=gestorDocumentalUseCase.filterOrderDocumentosDTO(list,idDocumento,usuarioCreador,tipo, idEntidad,fechaActualizacion, nombreArchivo, idHaya, orderDirection, orderField);
    List<DocumentoDTO>listaFiltradaPaginada=listFinal.stream().skip(size*page).limit(size).collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada,listFinal.size());
  }


  @Override
  public List<Contrato> getContratosRelaciondosWithBien(Integer idBien){
    return contratoRepository.findAllByBienesId(idBien);
  }

  public List<Contrato> getContratosRelaciondosWithInterviniente(Integer idInterviniente){
    return contratoRepository.findAllByIntervinientesId(idInterviniente);
  }

  @Override
  public List<ContratoDTOToList> getAllContratos(List<Integer> expedientes){
    List<ContratoDTOToList> result = new ArrayList<>();
    for (Integer id : expedientes){
      List<Contrato> contratos = filterContratosInMemory(id, null,null, null, null, null, null, null, null, null, null, null, null,null, null, null);
      result.addAll(contratoMapper.listContratoToListContratoDTOToList(contratos));
    }
    return result;
  }

  @Override
  public void createDocumento(
      Integer idContrato,
      MultipartFile file,
      MetadatoDocumentoInputDTO metadatoDocumento,
      Usuario usuario,
      String tipoDocumento)
      throws NotFoundException, IOException {
    Contrato contrato =
        contratoRepository
            .findById(idContrato)
            .orElseThrow(
                () -> new NotFoundException("contrato", idContrato));
    if (contrato.getIdHaya() == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("This contract does not have an identifier in the Master");
      else throw new NotFoundException("Este contrato no tiene un identificador en el Maestro");
    }
    JSONObject metadatos = new JSONObject();
    JSONObject archivoFisico = new JSONObject();
    archivoFisico.put("contenedor", "CONT");
    metadatos.put("General documento", metadatoDocumento.createDescripcionGeneralDocumento());
    metadatos.put("Archivo físico", archivoFisico);
    long id =
        servicioGestorDocumental
            .crearDocumento(CodigoEntidad.CONTRATO, contrato.getIdHaya(), file, metadatos)
            .getIdDocumento();
    Integer idgestorDocumental = (int) id;
    GestorDocumental gestorDocumental =
        new GestorDocumental(
            idgestorDocumental,
            usuario,
            null,
            new Date(),
            new Date(),
            metadatoDocumento.getNombreFichero(),
          MatriculasEnum.obtenerPorMatricula(metadatoDocumento.getCatalogoDocumental()),
            null,
            contrato.getIdCarga(),
            TipoEntidadEnum.CONTRATO,
            contrato.getIdHaya());
    gestorDocumentalRepository.save(gestorDocumental);
  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelContratosByExpediente(Integer id) throws Exception {
    var excel = this.expedienteExcelUseCase.findExcelExpedienteById(id);
    excel.remove("Expediente");
    return excel;
  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelContratoById(Integer id) {
    Contrato contrato = contratoRepository.findById(id).orElse(null);

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> contratos = new ArrayList<>();
    contratos.add(ContratoExcel.cabeceras);
    contratos.add(new ContratoExcel(contrato).getValuesList());


    List<Collection<?>> garantias = new ArrayList<>();
    garantias.add(BienExcel.cabeceras);
    List<Collection<?>> solvencias = new ArrayList<>();
    solvencias.add(BienExcel.cabeceras);
    List<Collection<?>> tasaciones = new ArrayList<>();
    tasaciones.add(TasacionExcel.cabeceras);
    List<Collection<?>> valoraciones = new ArrayList<>();
    valoraciones.add(ValoracionExcel.cabeceras);
    List<Collection<?>> cargas = new ArrayList<>();
    cargas.add(CargaExcel.cabeceras);


    List<Collection<?>>intervinientes=new ArrayList<>();
    intervinientes.add(IntervinienteExcel.cabeceras);


    List<Collection<?>> datosLocalizacion = new ArrayList<>();
    datosLocalizacion.add(DatosLocalizacionExcel.cabeceras);
    List<Collection<?>> datosDeContacto = new ArrayList<>();
    datosDeContacto.add(DatosContactoExcel.cabeceras);

    List<Collection<?>> movimientos = new ArrayList<>();
    movimientos.add(MovimientoExcel.cabeceras);
    List<Collection<?>> judicial = new ArrayList<>();
    judicial.add(JudicialExcel.cabeceras);

    List<Collection<?>> contratosJudiciales = new ArrayList<>();
    contratosJudiciales.add(ContratoJudicialExcel.cabeceras);
    List<Collection<?>> demandadosJudiciales = new ArrayList<>();
    demandadosJudiciales.add(DemandadoJudicialExcel.cabeceras);

    List<Collection<?>> procedimientosETJN = new ArrayList<>();
    procedimientosETJN.add(ProcedimientoETJNExcel.cabeceras);
    List<Collection<?>> procedimientosETJ = new ArrayList<>();
    procedimientosETJ.add(ProcedimientoETJExcel.cabeceras);
    List<Collection<?>> procedimientosHipotecario = new ArrayList<>();
    procedimientosHipotecario.add(ProcedimientoHipotecarioExcel.cabeceras);
    List<Collection<?>> procedimientosEjecucionNotarial = new ArrayList<>();
    procedimientosEjecucionNotarial.add(ProcedimientoEjecucionNotarialExcel.cabeceras);
    List<Collection<?>> procedimientosMonitorio = new ArrayList<>();
    procedimientosMonitorio.add(ProcedimientoMonitorioExcel.cabeceras);
    List<Collection<?>> procedimientosVerbal = new ArrayList<>();
    procedimientosVerbal.add(ProcedimientoVerbalExcel.cabeceras);
    List<Collection<?>> procedimientosOrdinario = new ArrayList<>();
    procedimientosOrdinario.add(ProcedimientoOrdinarioExcel.cabeceras);
    List<Collection<?>> procedimientosConcursal = new ArrayList<>();
    procedimientosConcursal.add(ProcedimientoConcursalExcel.cabeceras);
    List<Collection<?>> subastas = new ArrayList<>();
    subastas.add(SubastasExcel.cabeceras);


    var idContrato = contrato.getIdCarga();
    Integer idC = contrato.getId();
    var idExpediente=contrato.getExpediente().getIdConcatenado();
    for (ContratoBien cb : contrato.getBienes()) {
      Bien bien = cb.getBien();
      var idBien = bien.getId();
      for (Tasacion tasacion : bien.getTasaciones()) {
        tasaciones.add(new TasacionExcel(tasacion, idContrato, idBien,idExpediente).getValuesList());
      }
      for (Valoracion valoracion : bien.getValoraciones()) {
        valoraciones.add(new ValoracionExcel(valoracion, idContrato, idBien,idExpediente).getValuesList());
      }
      for (Carga carga : bien.getCargas()) {
        cargas.add(new CargaExcel(carga, idContrato, idBien,idExpediente).getValuesList());
      }
      if (cb.getResponsabilidadHipotecaria() != null) {
        garantias.add(new BienExcel(bien, idC).getValuesList());
      } else {
        solvencias.add(new BienExcel(bien, idC).getValuesList());
      }
    }

    for (ContratoInterviniente contratoInterviniente:contrato.getIntervinientes()){

      intervinientes.add(new IntervinienteExcel(contratoInterviniente.getInterviniente(),
              contrato.getIdCarga(),contratoInterviniente.getOrdenIntervencion(),contratoInterviniente.getTipoIntervencion(),idExpediente).getValuesList());


      for (Direccion direccion : contratoInterviniente.getInterviniente().getDirecciones()) {
        datosLocalizacion.add(new DatosLocalizacionExcel(direccion, idContrato,idExpediente).getValuesList());
      }
      for (DatoContacto datoContacto :
          contratoInterviniente.getInterviniente().getDatosContacto()) {
        datosDeContacto.add(new DatosContactoExcel(datoContacto, idContrato,idExpediente).getValuesList());
      }
    }
    for (Movimiento movimiento : contrato.getMovimientos()) {
      movimientos.add(new MovimientoExcel(movimiento).getValuesList());
    }
    for (Procedimiento procedimiento : contrato.getProcedimientos()) {
      if (procedimiento instanceof ProcedimientoHipotecario) {
        procedimientosHipotecario.add(
            new ProcedimientoHipotecarioExcel((ProcedimientoHipotecario) procedimiento, idContrato,idExpediente)
                .getValuesList());
      } else if (procedimiento instanceof ProcedimientoConcursal) {
        procedimientosConcursal.add(
            new ProcedimientoConcursalExcel((ProcedimientoConcursal) procedimiento, idContrato,idExpediente)
                .getValuesList());
      } else if (procedimiento instanceof ProcedimientoEjecucionNotarial) {
        procedimientosEjecucionNotarial.add(
            new ProcedimientoEjecucionNotarialExcel(
                    (ProcedimientoEjecucionNotarial) procedimiento, idContrato,idExpediente)
                .getValuesList());
      } else if (procedimiento instanceof ProcedimientoEtj) {
        procedimientosETJ.add(
            new ProcedimientoETJExcel((ProcedimientoEtj) procedimiento, idContrato,idExpediente)
                .getValuesList());
      } else if (procedimiento instanceof ProcedimientoEtnj) {
        procedimientosETJN.add(
            new ProcedimientoETJNExcel((ProcedimientoEtnj) procedimiento, idContrato,idExpediente)
                .getValuesList());
      } else if (procedimiento instanceof ProcedimientoMonitorio) {
        procedimientosMonitorio.add(
            new ProcedimientoMonitorioExcel((ProcedimientoMonitorio) procedimiento, idContrato,idExpediente)
                .getValuesList());
      } else if (procedimiento instanceof ProcedimientoOrdinario) {
        procedimientosOrdinario.add(
            new ProcedimientoOrdinarioExcel((ProcedimientoOrdinario) procedimiento, idContrato,idExpediente)
                .getValuesList());
      } else if (procedimiento instanceof ProcedimientoVerbal) {
        procedimientosVerbal.add(
            new ProcedimientoVerbalExcel((ProcedimientoVerbal) procedimiento, idContrato,idExpediente)
                .getValuesList());
      }

      var contratoexpediente = contrato.getIdCarga();


      judicial.add(
          new JudicialExcel(procedimiento, contratoexpediente,idExpediente).getValuesList());
      for (Contrato contratoJudicial : procedimiento.getContratos()) {
        contratosJudiciales.add(
            new ContratoJudicialExcel(contratoJudicial, procedimiento.getId(),idExpediente).getValuesList());
      }
      for (Interviniente demandadoJudicial : procedimiento.getDemandados()) {
        demandadosJudiciales.add(
            new DemandadoJudicialExcel(demandadoJudicial, procedimiento.getId(),idExpediente).getValuesList());
      }
      for (Subasta subasta : procedimiento.getSubastas()) {
        subastas.add(new SubastasExcel(subasta, procedimiento.getId(),idExpediente).getValuesList());
      }
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      datos.put("Loan", contratos);
      datos.put("Warranties", garantias);
      datos.put("Solvencies", solvencias);
      datos.put("Appraisals", tasaciones);
      datos.put("Ratings", valoraciones);
      datos.put("Charges", cargas);
      //datos.put("Intervinientes Fisicos", intervinientesFisicos);
      //datos.put("Intervinientes Juridicos", intervinientesJuridicos);
      datos.put("Borrowers",intervinientes);
      datos.put("Location Data", datosLocalizacion);
      datos.put("Contact Data", datosDeContacto);
      datos.put("Movements", movimientos);
      datos.put("Judicial", judicial);
      datos.put("Loans Judicial", contratosJudiciales);
      datos.put("Defendants in court", demandadosJudiciales);
      datos.put("Procedures ETJN", procedimientosETJN);
      datos.put("Procedures ETJ", procedimientosETJ);
      datos.put("Mortgage proceedings", procedimientosHipotecario);
      datos.put("Notarial Procedures", procedimientosEjecucionNotarial);
      datos.put("Monitoring Proceedings", procedimientosMonitorio);
      datos.put("Verbal procedures", procedimientosVerbal);
      datos.put("Ordinary procedures", procedimientosOrdinario);
      datos.put("Bankruptcy Proceedings", procedimientosConcursal);
      datos.put("Auctions", subastas);


    } else  {

      datos.put("Contrato", contratos);
      datos.put("Garantías", garantias);
      datos.put("Solvencias", solvencias);
      datos.put("Tasaciones", tasaciones);
      datos.put("Valoraciones", valoraciones);
      datos.put("Cargas", cargas);
      //datos.put("Intervinientes Fisicos", intervinientesFisicos);
      //datos.put("Intervinientes Juridicos", intervinientesJuridicos);
      datos.put("Intervinientes",intervinientes);
      datos.put("Datos Localizacion", datosLocalizacion);
      datos.put("Datos de Contacto", datosDeContacto);
      datos.put("Movimientos", movimientos);
      datos.put("Judicial", judicial);
      datos.put("Contratos Judicial", contratosJudiciales);
      datos.put("Demandados Judicial", demandadosJudiciales);
      datos.put("Procedimientos ETJN", procedimientosETJN);
      datos.put("Procedimientos ETJ", procedimientosETJ);
      datos.put("Procedimientos Hipotecario", procedimientosHipotecario);
      datos.put("Procedimientos Notarial", procedimientosEjecucionNotarial);
      datos.put("Procedimientos Monitorio", procedimientosMonitorio);
      datos.put("Procedimientos Verbal", procedimientosVerbal);
      datos.put("Procedimientos Ordinario", procedimientosOrdinario);
      datos.put("Procedimientos Concursal", procedimientosConcursal);
      datos.put("Subastas", subastas);
    }

    return datos;
  }

  public List<HistoricoAsignacionesDTOToList> getHistoricoAsignacionesByContrato(
      Integer idContrato, Integer idExpediente) throws IllegalAccessException {
    Contrato contrato = contratoRepository.findById(idContrato).orElse(null);

    List<ContratoHistoricoAsignaciones> historico =
        registroHistoricoUseCase.getHistoricoAsignacionesByContrato(contrato, idExpediente);

    return historico.stream().map(HistoricoAsignacionesDTOToList::new).collect(Collectors.toList());
  }

  @Override
  public Contrato findByIdCargaOrNull(String idCarga) {
	 return this.contratoRepository
		        .findByIdCarga(idCarga)
		        .orElse(null);
  }

  @Scheduled(cron = "0 0 3 * * *")
  public void chekVencimientoAnticipado() {
    log.info("Iniciando tarea programada: ContratoUseCaseImpl::chekVencimientoAnticipado");

    List<Contrato> contratos = contratoRepository.findAll();
    for(Contrato contrato: contratos){
      if (contrato.getFechaVencimientoAnticipado() != null) {
        contrato.setVencimientoAnticipado(true);
        var contratoSaved = contratoRepository.saveAndFlush(contrato);
        continue;
      }
      if (contrato.getFechaFormalizacion() == null || contrato.getFechaVencimientoInicial() == null) {
        contrato.setVencimientoAnticipado(false);
        var contratoSaved = contratoRepository.saveAndFlush(contrato);
        continue;
      }
      // criterio 1
      long longitud = contrato.getFechaVencimientoInicial().getTime() - contrato.getFechaFormalizacion().getTime();
      long mitad = contrato.getFechaFormalizacion().getTime() + longitud / 2;
      Saldo saldo = contrato.getUltimoSaldo();
      if (new Date().getTime() <= mitad) { // criterio 1
        if (saldo.getPrincipalVencidoImpagado() != null
                && saldo.getPrincipalVencidoImpagado() >= 0.03 * contrato.getImporteInicial()) {
          contrato.setVencimientoAnticipado(true);
          var contratoSaved = contratoRepository.saveAndFlush(contrato);
          continue;
        }
        if (contrato.getCuotasPendientes() != null && contrato.getCuotasPendientes() >= 12) {
          contrato.setVencimientoAnticipado(true);
          var contratoSaved = contratoRepository.saveAndFlush(contrato);
          continue;
        }
      } else { // criterio 2
        if (saldo.getPrincipalVencidoImpagado() != null
                && saldo.getPrincipalVencidoImpagado() >= 0.07 * contrato.getImporteInicial()) {
          contrato.setVencimientoAnticipado(true);
          var contratoSaved = contratoRepository.saveAndFlush(contrato);
          continue;
        }
        if (contrato.getCuotasPendientes() != null && contrato.getCuotasPendientes() >= 15) {
          contrato.setVencimientoAnticipado(true);
          var contratoSaved = contratoRepository.saveAndFlush(contrato);
          continue;
        }
      }
      contrato.setVencimientoAnticipado(false);
      var contratoSaved = contratoRepository.saveAndFlush(contrato);
    }

  }

@Override
public List<Contrato> findAllByExpedienteId(Integer idExpediente, Set<CarteraContratoRepresentante> criterios) {
	List<Sort.Order> orders = new ArrayList<>();
	for (CarteraContratoRepresentante crit : criterios) {
    Sort.Direction direccion = crit.getOrdenAsc() ? Sort.Direction.ASC : Sort.Direction.DESC;
		String propiedad = getPropiedadCriterio(crit);
		if (propiedad != null) {
		  orders.add(new Sort.Order(direccion, propiedad));
    }
	}

  return contratoRepository.findAllByExpedienteId(idExpediente, Sort.by(orders));
}

private String getPropiedadCriterio (CarteraContratoRepresentante criterio) {
	String resultado = null;
	switch (criterio.getCriterioContratoCartera().getCodigo()) {
	     case DEUDA:
	    	 resultado = "deudaImpagada";
	    	 break;
	     case RIESGO:
	    	 resultado=  "saldoGestion";
	    	 break;
	     case ANTIGUEDAD:
	    	 resultado =  "fechaPrimerImpago";
	    	 break;
	     case NUMERO:
	    	 resultado = "idCarga";
	    	 break;
	     case PRODUCTO:
	    	 resultado = "producto.codigo"; // TODO: usar el de abajo (corregir)
	    	 //resultado = "expediente.cartera.criterioContratoRepresentante.productos.orden";
	    	 break;
	}
	return resultado;
}

  @Override
  public List<ContratoDTOToList> findAllContratosByExpedienteId(Integer idExpediente)
      throws NotFoundException {
    Expediente expediente =
        expedienteRepository
            .findById(idExpediente)
            .orElseThrow(() -> new NotFoundException("Expediente", idExpediente));

    List<Contrato> listacontratos =
        expediente.getContratos().stream().filter(c -> c.getActivo()).collect(Collectors.toList());
    List<ContratoDTOToList> listaContratosDTO =
        contratoMapper.listContratoToListContratoDTOToList(listacontratos);
    return listaContratosDTO;
  }

  @Override
  public void createContrato(Integer idExpediente, FormalizacionContratoInputDTO input)
      throws NotFoundException, RequiredValueException {
    Expediente expediente =
        expedienteRepository
            .findById(idExpediente)
            .orElseThrow(() -> new NotFoundException("Expediente", idExpediente));
    Integer idContrato = formalizacionMapper.parseInputToEntity(input);

    Contrato contrato =
        contratoRepository
            .findById(idContrato)
            .orElseThrow(() -> new NotFoundException("Contrato", idContrato));

    contrato.setExpediente(expediente);
    contratoRepository.save(contrato);
  }
}
