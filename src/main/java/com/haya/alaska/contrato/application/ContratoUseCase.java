package com.haya.alaska.contrato.application;

import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDtoInput;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoDtoOutput;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input.FormalizacionContratoInputDTO;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.dto.HistoricoAsignacionesDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public interface ContratoUseCase {


  ListWithCountDTO<ContratoDTOToList> getAllFilteredContratos(
          Integer idExpediente,
          Integer id,
          String idOrigen,
          Boolean esRepresentante,
          String producto,
          String estadoContratos,
          String primerTitular,
          String saldoEnGestion,
          String intervinientes,
          String garantias,
          Boolean judicial,
          Boolean estado,
          String situacion,
          String estrategia,
          String orderField,
          String orderDirection,
          Integer size,
          Integer page);

  ContratoDtoOutput findContratoByIdDto(Integer id) throws IllegalAccessException, NotFoundException;
  ContratoDtoOutput findContratoByIdCargaDto(String idCarga) throws IllegalAccessException;

  ContratoDtoOutput update(Integer idContrato, ContratoDtoInput contratoDTO) throws IllegalAccessException, NotFoundException, com.haya.alaska.shared.exceptions.NotFoundException;

  ListWithCountDTO<DocumentoDTO> findDocumentosByContratoId(Integer id,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size, Integer page) throws IOException;

  ListWithCountDTO<DocumentoDTO> findDocumentosByExpedienteId(Integer id,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size, Integer page) throws IOException;

  void createDocumento(Integer idBien, MultipartFile file, MetadatoDocumentoInputDTO metadatoDocumento,Usuario usuario,String tipoDocumento) throws NotFoundException, IOException;

  LinkedHashMap<String, List<Collection<?>>> findExcelContratosByExpediente(Integer id) throws Exception;

  LinkedHashMap<String, List<Collection<?>>> findExcelContratoById(Integer id);

  List<HistoricoAsignacionesDTOToList> getHistoricoAsignacionesByContrato(Integer idContrato, Integer idExpediente) throws IllegalAccessException;

  Contrato findContratoById(Integer id) throws NotFoundException;

  List<Contrato> getContratosRelaciondosWithBien(Integer idBien);

  Contrato findByIdCargaOrNull(String idCarga);

  List<Contrato> getContratosRelaciondosWithInterviniente(Integer idInterviniente);

  List<Contrato> findAllByExpedienteId (Integer idExpediente, Set<CarteraContratoRepresentante> criterios);

  List<ContratoDTOToList> getAllContratos(List<Integer> expedientes);

  List <ContratoDTOToList> findAllContratosByExpedienteId(Integer idExpediente) throws NotFoundException;

  void cambiarCR(Integer idContrato, Usuario usuario) throws NotFoundException;

  void createContrato(Integer idExpediente, FormalizacionContratoInputDTO input)
    throws NotFoundException, RequiredValueException;
}
