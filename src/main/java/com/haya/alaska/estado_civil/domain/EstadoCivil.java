package com.haya.alaska.estado_civil.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_ESTADO_CIVIL")
@Entity
@Table(name = "LKUP_ESTADO_CIVIL")
@Getter
public class EstadoCivil extends Catalogo {
    @OneToMany(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_ESTADO_CIVIL")
    @NotAudited
    private Set<Interviniente> intervinientes = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_CIVIL")
  @NotAudited
  private Set<IntervinienteST> intervinienteSTSet = new HashSet<>();
}
