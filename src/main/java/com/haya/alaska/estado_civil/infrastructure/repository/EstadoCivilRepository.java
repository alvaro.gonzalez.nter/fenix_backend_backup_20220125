package com.haya.alaska.estado_civil.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estado_civil.domain.EstadoCivil;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoCivilRepository extends CatalogoRepository<EstadoCivil, Integer> {
}
