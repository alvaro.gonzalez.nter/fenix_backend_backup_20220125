package com.haya.alaska.integraciones.gd.application;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDeudorDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@Component
@Slf4j
public class GestorDocumentalUseCaseImpl implements GestorDocumentalUseCase {

  @Autowired
  private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired
  private ContratoRepository contratoRepository;
  @Autowired
  private IntervinienteRepository intervinienteRepository;
  @Autowired
  private BienRepository bienRepository;
  @Autowired
  private BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  private PropuestaRepository propuestaRepository;

  @Override
  public List<DocumentoDTO> listarDocumentosGestor(List<DocumentoDTO>listadoIdGestor){
    List<GestorDocumental> listadoIdGestorNuestraBase = gestorDocumentalRepository.findAll();
    //Obtengo id de nuestra base
    List<Long> idsGestorDocumental = listadoIdGestorNuestraBase.stream().map(gestorDocumental -> gestorDocumental.getId().longValue()).collect(Collectors.toList());
    //Comparamos id traidos de repo con los de nuestra base si coinciden los mostramos y seteamos el usuarioCreador
    //Obtenemos los IdHaya
    return listadoIdGestor.stream()
      .peek(
        documentoDTO ->{
          documentoDTO.setUsuarioCreador(
            idsGestorDocumental.contains(documentoDTO.getId())
              ? listadoIdGestorNuestraBase.stream()
              .filter(
                gestorDocumental ->
                  gestorDocumental.getId().longValue()
                    == documentoDTO.getId())
              .collect(Collectors.toList())
              .get(0)
              .getUsuario()
              .getNombre()
              : documentoDTO.getUsuarioCreador());
        })
      .collect(Collectors.toList());
  }

  @Override
  public List<DocumentoDTO> listarDocumentosGestorContrato(List<DocumentoDTO>listadoIdGestor){

    List<GestorDocumental> listadoIdGestorNuestraBase = gestorDocumentalRepository.findAll();
    //Obtengo id de nuestra base
    List<Long> idsGestorDocumental = listadoIdGestorNuestraBase.stream().map(gestorDocumental -> gestorDocumental.getId().longValue()).collect(Collectors.toList());
    //Comparamos id traidos de repo con los de nuestra base si coinciden los mostramos y seteamos el usuarioCreador
    return listadoIdGestor.stream().peek(documentoDTO -> {
      Optional<Contrato> optionalContrato=contratoRepository.findAllByIdHaya(documentoDTO.getIdHaya());
      optionalContrato.ifPresent(contrato -> documentoDTO.setIdEntidad(contrato.getIdCarga()));

      documentoDTO.setUsuarioCreador(
        idsGestorDocumental.contains(documentoDTO.getId())
          ? listadoIdGestorNuestraBase.stream()
          .filter(
            gestorDocumental ->
              gestorDocumental.getId().longValue()
                == documentoDTO.getId())
          .collect(Collectors.toList())
          .get(0)
          .getUsuario()
          .getNombre()
          : documentoDTO.getUsuarioCreador());
    }).collect(Collectors.toList());
  }


  @Override
  public List<DocumentoDTO> listarDocumentosGestorIntervinientes(List<DocumentoDTO>listadoIdGestor){

    List<GestorDocumental> listadoIdGestorNuestraBase = gestorDocumentalRepository.findAll();
    //Obtengo id de nuestra base
    List<Long> idsGestorDocumental = listadoIdGestorNuestraBase.stream().map(gestorDocumental -> gestorDocumental.getId().longValue()).collect(Collectors.toList());
    //Comparamos id traidos de repo con los de nuestra base si coinciden los mostramos y seteamos el usuarioCreador
    return listadoIdGestor.stream().peek(documentoDTO -> {
      Optional<Interviniente> optionalInterviniente=intervinienteRepository.findAllByIdHaya(documentoDTO.getIdHaya());
      optionalInterviniente.ifPresent(interviniente -> documentoDTO.setIdEntidad(interviniente.getId().toString()));

      documentoDTO.setUsuarioCreador(
        idsGestorDocumental.contains(documentoDTO.getId())
          ? listadoIdGestorNuestraBase.stream()
          .filter(
            gestorDocumental ->
              gestorDocumental.getId().longValue()
                == documentoDTO.getId())
          .collect(Collectors.toList())
          .get(0)
          .getUsuario()
          .getNombre()
          : documentoDTO.getUsuarioCreador());
    }).collect(Collectors.toList());
  }


  @Override
  public List<DocumentoDTO> listarDocumentosGestorBienes(List<DocumentoDTO>listadoIdGestor){

    List<GestorDocumental> listadoIdGestorNuestraBase = gestorDocumentalRepository.findAll();
    //Obtengo id de nuestra base
    List<Long> idsGestorDocumental = listadoIdGestorNuestraBase.stream().map(gestorDocumental -> gestorDocumental.getId().longValue()).collect(Collectors.toList());
    //Comparamos id traidos de repo con los de nuestra base si coinciden los mostramos y seteamos el usuarioCreador
    return listadoIdGestor.stream().peek(documentoDTO -> {
      Optional<Bien> optionalBien=bienRepository.findAllByIdHaya(documentoDTO.getIdHaya());
      optionalBien.ifPresent(bien -> documentoDTO.setIdEntidad(bien.getId().toString()));

      documentoDTO.setUsuarioCreador(
        idsGestorDocumental.contains(documentoDTO.getId())
          ? listadoIdGestorNuestraBase.stream()
          .filter(
            gestorDocumental ->
              gestorDocumental.getId().longValue()
                == documentoDTO.getId())
          .collect(Collectors.toList())
          .get(0)
          .getUsuario()
          .getNombre()
          : documentoDTO.getUsuarioCreador());
    }).collect(Collectors.toList());
  }



  @Override
  public List<DocumentoDTO> listarDocumentosGestorBienesPosesionNegociada(List<DocumentoDTO>listadoIdGestor,Integer idBienPN){

    List<GestorDocumental> listadoIdGestorNuestraBase = gestorDocumentalRepository.findAll();
    //Obtengo id de nuestra base
    List<Long> idsGestorDocumental = listadoIdGestorNuestraBase.stream().map(gestorDocumental -> gestorDocumental.getId().longValue()).collect(Collectors.toList());
    //Comparamos id traidos de repo con los de nuestra base si coinciden los mostramos y seteamos el usuarioCreador
    return listadoIdGestor.stream().peek(documentoDTO -> {
      documentoDTO.setUsuarioCreador(
        idsGestorDocumental.contains(documentoDTO.getId())
          ? listadoIdGestorNuestraBase.stream()
          .filter(
            gestorDocumental ->
              gestorDocumental.getId().longValue()
                == documentoDTO.getId())
          .collect(Collectors.toList())
          .get(0)
          .getUsuario()
          .getNombre()
          : documentoDTO.getUsuarioCreador());
        documentoDTO.setIdEntidad(idBienPN.toString());
    }).collect(Collectors.toList());
  }

  @Override
  public List<DocumentoDTO> listarDocumentosGestorPropuesta(List<DocumentoDTO>listadoIdGestor){

    List<GestorDocumental> listadoIdGestorNuestraBase = gestorDocumentalRepository.findAll();
    //Obtengo id de nuestra base
    List<Long> idsGestorDocumental = listadoIdGestorNuestraBase.stream().map(gestorDocumental -> gestorDocumental.getId().longValue()).collect(Collectors.toList());
    //Comparamos id traidos de repo con los de nuestra base si coinciden los mostramos y seteamos el usuarioCreador
    return listadoIdGestor.stream().peek(documentoDTO -> {
      Optional<Propuesta> optionalPropuesta=propuestaRepository.findById(Integer.parseInt(documentoDTO.getIdHaya()));
      optionalPropuesta.ifPresent(propuesta -> documentoDTO.setIdEntidad(documentoDTO.getIdHaya()));

      documentoDTO.setUsuarioCreador(
        idsGestorDocumental.contains(documentoDTO.getId())
          ? listadoIdGestorNuestraBase.stream()
          .filter(
            gestorDocumental ->
              gestorDocumental.getId().longValue()
                == documentoDTO.getId())
          .collect(Collectors.toList())
          .get(0)
          .getUsuario()
          .getNombre()
          : documentoDTO.getUsuarioCreador());
    }).collect(Collectors.toList());
  }

  @Override
  public List<DocumentoRepositorioDTO> listarDocumentosRepositorio(List<DocumentoRepositorioDTO>listadoIdGestor){

    List<GestorDocumental> listadoIdGestorNuestraBase = gestorDocumentalRepository.findAll();
    //Obtengo id de nuestra base
    List<Long> idsGestorDocumental = listadoIdGestorNuestraBase.stream().map(gestorDocumental -> gestorDocumental.getId().longValue()).collect(Collectors.toList());
    //Comparamos id traidos de repo con los de nuestra base si coinciden los mostramos y seteamos el usuarioCreador
    //A partir de el listado nuestro seteamos el valor usuario Creador que no viene desde el repositorio pero que si almacenamos en nuestra base
    //.peek para recorrer el siguiente objeto
    List<DocumentoRepositorioDTO>listFinal=listadoIdGestor.stream()
      .peek(
        documentoDTO ->{
          documentoDTO.setUsuarioCreador(
            idsGestorDocumental.contains(documentoDTO.getId())
              ? listadoIdGestorNuestraBase.stream()
              .filter(
                gestorDocumental ->
                  gestorDocumental.getId().longValue()
                    == documentoDTO.getId())
              .collect(Collectors.toList())
              .get(0)
              .getUsuario()
              .getNombre()
              : documentoDTO.getUsuarioCreador());
          documentoDTO.setTipoModeloCartera( idsGestorDocumental.contains(documentoDTO.getId())
            ? listadoIdGestorNuestraBase.stream()
            .filter(
              gestorDocumental ->
                gestorDocumental.getId().longValue()
                  == documentoDTO.getId())
            .collect(Collectors.toList())
            .get(0)
            .getTipoModeloCartera()
            : documentoDTO.getTipoModeloCartera());
          documentoDTO.setUsuarioDestino( idsGestorDocumental.contains(documentoDTO.getId())
            ? listadoIdGestorNuestraBase.stream()
            .filter(
              gestorDocumental ->
                gestorDocumental.getId().longValue()
                  == documentoDTO.getId())
            .collect(Collectors.toList())
            .get(0)
            .getUsuarioDestino().getNombre()
            : documentoDTO.getUsuarioDestino());
        })
      .collect(Collectors.toList());

    return listFinal;
  }

  @Override
  public List<DocumentoRepositorioDTO> listarDocumentosRepositorioCarteras(
      List<DocumentoRepositorioDTO> listadoIdGestor) {

    List<GestorDocumental> listadoIdGestorNuestraBase = gestorDocumentalRepository.findAll();
    // Obtengo id de nuestra base
    List<Long> idsGestorDocumental =
        listadoIdGestorNuestraBase.stream()
            .map(gestorDocumental -> gestorDocumental.getId().longValue())
            .collect(Collectors.toList());
    // Comparamos id traidos de repo con los de nuestra base si coinciden los mostramos y seteamos
    // el usuarioCreador
    // A partir de el listado nuestro seteamos el valor usuario Creador que no viene desde el
    // repositorio pero que si almacenamos en nuestra base
    // .peek para recorrer el siguiente objeto
    return listadoIdGestor.stream()
        .peek(
            documentoDTO -> {
              documentoDTO.setUsuarioCreador(
                  idsGestorDocumental.contains(documentoDTO.getId())
                      ? listadoIdGestorNuestraBase.stream()
                          .filter(
                              gestorDocumental ->
                                  gestorDocumental.getId().longValue() == documentoDTO.getId())
                          .collect(Collectors.toList())
                          .get(0)
                          .getUsuario()
                          .getNombre()
                      : documentoDTO.getUsuarioCreador());
              documentoDTO.setTipoModeloCartera(
                  idsGestorDocumental.contains(documentoDTO.getId())
                      ? listadoIdGestorNuestraBase.stream()
                          .filter(
                              gestorDocumental ->
                                  gestorDocumental.getId().longValue() == documentoDTO.getId())
                          .collect(Collectors.toList())
                          .get(0)
                          .getTipoModeloCartera()
                      : documentoDTO.getTipoModeloCartera());
            })
        .collect(Collectors.toList());
  }

  @Override
  public List<DocumentoDeudorDTO>listarDocumentoDeudorDTO(List<DocumentoDeudorDTO>listadoIdGestor){

    List<GestorDocumental> listadoIdGestorNuestraBase = gestorDocumentalRepository.findAll();
    //Obtengo id de nuestra base
    List<Long> idsGestorDocumental = listadoIdGestorNuestraBase.stream().map(gestorDocumental -> gestorDocumental.getId().longValue()).collect(Collectors.toList());
    //Comparamos id traidos de repo con los de nuestra base si coinciden los mostramos y seteamos el usuarioCreador
    //A partir de el listado nuestro seteamos el valor usuario Creador que no viene desde el repositorio pero que si almacenamos en nuestra base
    //.peek para recorrer el siguiente objeto
    List<DocumentoDeudorDTO>listFinal=listadoIdGestor.stream()
      .peek(
        documentoDTO ->{
          documentoDTO.setEstado(
            idsGestorDocumental.contains(documentoDTO.getId())
              ? listadoIdGestorNuestraBase.stream()
              .filter(
                gestorDocumental ->
                  gestorDocumental.getId().longValue()
                    == documentoDTO.getId())
              .collect(Collectors.toList())
              .get(0)
              .getEstado()
              .getValue()
              : documentoDTO.getEstado());
          documentoDTO.setFechaFirma( idsGestorDocumental.contains(documentoDTO.getId())
            ? listadoIdGestorNuestraBase.stream()
            .filter(
              gestorDocumental ->
                gestorDocumental.getId().longValue()
                  == documentoDTO.getId())
            .collect(Collectors.toList())
            .get(0)
            .getFechaFirma()
            : documentoDTO.getFechaFirma());
        })
      .collect(Collectors.toList());

    return listFinal;
  }

  @Override
  public List<DocumentoDeudorDTO> listarDocumentoDeudorClienteDTO(
      List<DocumentoDeudorDTO> listadoIdGestor, Integer idCliente) {

    List<GestorDocumental> listadoIdGestorNuestraBase =
        gestorDocumentalRepository.findAllByClienteId(idCliente);
    // Obtengo id de nuestra base
    List<Long> idsGestorDocumental =
        listadoIdGestorNuestraBase.stream()
            .map(gestorDocumental -> gestorDocumental.getId().longValue())
            .collect(Collectors.toList());

    return listadoIdGestor.stream()
        .peek(
            documentoDTO -> {
              documentoDTO.setUsuarioCreador(
                  idsGestorDocumental.contains(documentoDTO.getId())
                      ? listadoIdGestorNuestraBase.stream()
                          .filter(
                              gestorDocumental ->
                                  gestorDocumental.getId().longValue() == documentoDTO.getId())
                          .collect(Collectors.toList())
                          .get(0)
                          .getInterviniente()
                          .getNombre()
                      : documentoDTO.getUsuarioCreador());
            })
        .collect(Collectors.toList());
    }


  @Override
  public List<DocumentoRepositorioDTO>filterDocumentoRepositorioDTO(List<DocumentoRepositorioDTO>listFinal,String idDocumento,String usuarioCreador,String tipoDocumento,String tipoModeloCartera,String nombreDocumento, String fechaActualizacion,String fechaAlta,String orderDirection,String orderField){

      listFinal = listFinal.stream().filter(c -> {
      if (idDocumento != null && !c.getId().toString().toLowerCase().contains(idDocumento)) return false;
      if (usuarioCreador != null && !c.getUsuarioCreador().toLowerCase().contains(usuarioCreador.toLowerCase()))
        return false;
      if (tipoDocumento != null && !c.getTipoDocumento().toLowerCase().contains(tipoDocumento.toLowerCase()))
        return false;
      if (tipoModeloCartera != null && !c.getTipoModeloCartera().toLowerCase().contains(tipoModeloCartera.toLowerCase()))
        return false;
      if (nombreDocumento != null && !c.getNombreArchivo().toLowerCase().contains(nombreDocumento.toLowerCase()))
        return false;
      if (fechaActualizacion != null && !c.getFechaActualizacion().toLowerCase().contains(fechaActualizacion.toLowerCase()))
        return false;
      if (fechaAlta != null && !c.getFechaActualizacion().toLowerCase().contains(fechaAlta.toLowerCase()))
        return false;
      return true;
    }).collect(Collectors.toList());


    // Ordenacion //Cuando hay muchos if utilizar switch
    if (orderDirection != null && orderField != null) {
      Comparator<DocumentoRepositorioDTO> comparator = null;
      switch (orderField) {
        case "tipo":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getTipoDocumento);
          break;
        case "tipoModeloCartera":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getTipoModeloCartera);
          break;
        case "nombreDocumento":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getNombreArchivo);
          break;
        case "usuarioCreador":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getUsuarioCreador);
          break;
        case "fechaActualizacion":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getFechaActualizacion);
          break;
        case "fechaAlta":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getFechaAlta);
          break;
        default:
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getId);
      }
      if (orderDirection.equals("asc")) comparator.reversed();
      listFinal=listFinal.stream().sorted(comparator).collect(Collectors.toList());
    }
    return listFinal;
  }



  @Override
  public List<DocumentoDTO>filterOrderDocumentosDTO(List<DocumentoDTO>listFinal,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion,String nombreArchivo, String idHaya, String orderDirection,String orderField){
    Comparator<DocumentoDTO> comparator=null;
    if (idDocumento != null)
      listFinal = listFinal.stream().filter(c -> c.getId().toString().toLowerCase().contains(idDocumento)).collect(Collectors.toList());

    if (usuarioCreador!=null)
      listFinal = listFinal.stream().filter(c -> c.getUsuarioCreador().toLowerCase().contains(usuarioCreador.toLowerCase())).collect(Collectors.toList());

    if (tipo!=null)
      listFinal = listFinal.stream().filter(c -> c.getTipo().toLowerCase().contains(tipo.toLowerCase())).collect(Collectors.toList());

    if (idEntidad!=null)
      listFinal = listFinal.stream().filter(c -> c.getIdEntidad().toLowerCase().contains(idEntidad)).collect(Collectors.toList());

    if (fechaActualizacion!=null)
      listFinal = listFinal.stream().filter(c -> c.getFechaActualizacion().contains(fechaActualizacion)).collect(Collectors.toList());

    if(nombreArchivo != null)
      listFinal = listFinal.stream().filter(documentoDTO -> documentoDTO.getNombreArchivo().contains(nombreArchivo)).collect(Collectors.toList());

    if(idHaya != null)
      listFinal = listFinal.stream().filter(documentoDTO -> documentoDTO.getIdHaya().contains(idHaya)).collect(Collectors.toList());



    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("tipo"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing((DocumentoDTO doc)->doc.getTipo(),String.CASE_INSENSITIVE_ORDER).reversed()).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("tipo"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing((DocumentoDTO doc)->doc.getTipo(),String.CASE_INSENSITIVE_ORDER)).collect(Collectors.toList());
    }

    //Esto sirve para filtrar el iddocumento
    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("id"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDTO::getId).reversed()).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("id"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDTO::getId)).collect(Collectors.toList());
    }

    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("usuarioCreador"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing((DocumentoDTO doc)->doc.getUsuarioCreador(),String.CASE_INSENSITIVE_ORDER).reversed()).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("usuarioCreador"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing((DocumentoDTO doc)->doc.getUsuarioCreador(),String.CASE_INSENSITIVE_ORDER)).collect(Collectors.toList());
    }

    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("idEntidad"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDTO::getIdEntidad).reversed()).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("idEntidad"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDTO::getIdEntidad)).collect(Collectors.toList());
    }

    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("fechaActualizacion"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDTO::getFechaActualizacion).reversed()).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("fechaActualizacion"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDTO::getFechaActualizacion)).collect(Collectors.toList());
    }

    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("nombreArchivo"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing((DocumentoDTO doc)->doc.getNombreArchivo(),String.CASE_INSENSITIVE_ORDER).reversed()).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("nombreArchivo"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing((DocumentoDTO doc)->doc.getNombreArchivo(),String.CASE_INSENSITIVE_ORDER)).collect(Collectors.toList());
    }

    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("idHaya"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing((DocumentoDTO doc)->doc.getIdHaya(),String.CASE_INSENSITIVE_ORDER).reversed()).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("idHaya"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing((DocumentoDTO doc)->doc.getIdHaya(),String.CASE_INSENSITIVE_ORDER)).collect(Collectors.toList());
    }
    return listFinal;
  }

  @Override
  public List<DocumentoRepositorioDTO>busquedaDocumentoRepositorio(BusquedaRepositorioDTO busquedaRepositorioDTO, List<DocumentoRepositorioDTO>listdocumentoRepositorio){


    if (busquedaRepositorioDTO.getDocumentoId()!=null){
      listdocumentoRepositorio = listdocumentoRepositorio.stream().filter(c -> c.getId().toString().toLowerCase().contains(busquedaRepositorioDTO.getDocumentoId())).collect(Collectors.toList());
    }

    if (busquedaRepositorioDTO.getUsuarioCreador()!=null){
      listdocumentoRepositorio = listdocumentoRepositorio.stream().filter(c -> c.getUsuarioCreador().toLowerCase().contains(busquedaRepositorioDTO.getUsuarioCreador().toLowerCase())).collect(Collectors.toList());
    }
    /*  if (busquedaRepositorioDTO.getUsuarioDestinatario()!=null){
      listdocumentoRepositorio.stream().filter(documentoRepositorioDTO -> {
        if (busquedaRepositorioDTO.getUsuarioDestinatario().contains(documentoRepositorioDTO.getusuario))
      }).collect(Collectors.toList());
    }*/
    if (busquedaRepositorioDTO.getFechaActualizacion() != null) {
      String pattern = "yyyy-MM-dd";
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
      String busquedafechaActualizacion=simpleDateFormat.format(busquedaRepositorioDTO.getFechaActualizacion());

      listdocumentoRepositorio = listdocumentoRepositorio.stream().filter(c->c.getFechaActualizacion()!=null).filter(c -> c.getFechaActualizacion().contains(busquedafechaActualizacion)).collect(Collectors.toList());
    }

    if (busquedaRepositorioDTO.getFechaAlta()!=null){
      String pattern = "yyyy-MM-dd";
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
      String busquedafechaAlta=simpleDateFormat.format(busquedaRepositorioDTO.getFechaAlta());
      listdocumentoRepositorio = listdocumentoRepositorio.stream().filter(c -> c.getFechaAlta().toString().contains(busquedafechaAlta)).collect(Collectors.toList());
    }

    if (busquedaRepositorioDTO.getNombreDocumento()!=null){
      listdocumentoRepositorio = listdocumentoRepositorio.stream().filter(c -> c.getNombreArchivo().toLowerCase().contains(busquedaRepositorioDTO.getNombreDocumento().toLowerCase())).collect(Collectors.toList());
    }

    if (busquedaRepositorioDTO.getTipoDocumento()!=null){
      listdocumentoRepositorio = listdocumentoRepositorio.stream().filter(c->c.getTipoDocumento()!=null).filter(c -> c.getTipoDocumento().toLowerCase().contains(busquedaRepositorioDTO.getTipoDocumento().toLowerCase())).collect(Collectors.toList());
    }
    return listdocumentoRepositorio;
  }


  @Override
  public List<DocumentoDeudorDTO>filterOrderDocumentosDeudorDTO(List<DocumentoDeudorDTO>listFinal,String tipoDocumento, String fechaAlta,String orderDirection,String orderField){

    if (tipoDocumento!=null)
      listFinal = listFinal.stream().filter(c -> c.getTipoDocumento().toLowerCase().contains(tipoDocumento.toLowerCase())).collect(Collectors.toList());
    if (fechaAlta!=null)
      listFinal = listFinal.stream().filter(c->c.getFechaAlta()!=null).filter(c -> c.getFechaAlta().toString().contains(fechaAlta)).collect(Collectors.toList());

    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("tipoDocumento"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDeudorDTO::getTipoDocumento)).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("tipoDocumento"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDeudorDTO::getTipoDocumento).reversed()).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("fechaAlta"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDeudorDTO::getFechaAlta)).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("fechaAlta"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDeudorDTO::getFechaAlta).reversed()).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("estado"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDeudorDTO::getEstado)).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("estado"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDeudorDTO::getEstado).reversed()).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("desc")) && (orderField!=null && orderField.equals("fechaFirma"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDeudorDTO::getFechaFirma)).collect(Collectors.toList());
    }
    if((orderDirection!= null && orderDirection.equals("asc")) && (orderField!=null && orderField.equals("fechaFirma"))){
      listFinal=listFinal.stream().sorted(Comparator.comparing(DocumentoDeudorDTO::getFechaFirma).reversed()).collect(Collectors.toList());
    }
    return listFinal;
  }



  @Override
  public List<DocumentoDeudorDTO> busquedaDocumentoDeudorRepositorio(
    BusquedaRepositorioDTO busquedaRepositorioDTO,
    List<DocumentoDeudorDTO> listdocumentoDeudorRepositorio){
    /*listdocumentoDeudorRepositorio=listdocumentoDeudorRepositorio.stream().filter(c->{
      if(busquedaRepositorioDTO.getDocumentoId()!=null && !c.getId().toString().toLowerCase().contains(busquedaRepositorioDTO.getDocumentoId()))return false;
      if(busquedaRepositorioDTO.getUsuarioCreador()!=null && !c.getUsuarioCreador().toLowerCase().contains(busquedaRepositorioDTO.getUsuarioCreador()))return false;
      if (busquedaRepositorioDTO.getNombreDocumento()!=null && !c.getNombreArchivo().toLowerCase().contains(busquedaRepositorioDTO.getNombreDocumento()))return false;
      if (busquedaRepositorioDTO.getFechaAlta()!=null && !c.getFechaAlta().toLowerCase().contains(busquedaRepositorioDTO.getFechaAlta().toString().toLowerCase()))return false;
      return true;
    }).collect(Collectors.toList());*/

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    if(busquedaRepositorioDTO.getUsuarioCreador() != null)
      listdocumentoDeudorRepositorio = listdocumentoDeudorRepositorio.stream().filter((DocumentoDeudorDTO doc) -> StringUtils.containsIgnoreCase(doc.getUsuarioCreador(), busquedaRepositorioDTO.getUsuarioCreador())).collect(Collectors.toList());
    if(busquedaRepositorioDTO.getNombreDocumento() != null)
      listdocumentoDeudorRepositorio = listdocumentoDeudorRepositorio.stream().filter((DocumentoDeudorDTO doc) -> StringUtils.containsIgnoreCase(doc.getNombreArchivo(), busquedaRepositorioDTO.getNombreDocumento())).collect(Collectors.toList());
    if(busquedaRepositorioDTO.getTipoDocumento() != null)
      listdocumentoDeudorRepositorio = listdocumentoDeudorRepositorio.stream().filter((DocumentoDeudorDTO doc) -> StringUtils.containsIgnoreCase(doc.getTipoDocumento(), busquedaRepositorioDTO.getTipoDocumento())).collect(Collectors.toList());
    if(busquedaRepositorioDTO.getFechaAlta() != null)
      listdocumentoDeudorRepositorio = listdocumentoDeudorRepositorio.stream().filter((DocumentoDeudorDTO doc) -> StringUtils.containsIgnoreCase(doc.getFechaAlta(), formatter.format(busquedaRepositorioDTO.getFechaAlta()))).collect(Collectors.toList());

    return listdocumentoDeudorRepositorio;
  }

  @Override
  public List<DocumentoDeudorDTO>filterDocumentoDeudor(List<DocumentoDeudorDTO> listfinal,
                                                       String idDocumento,
                                                       String nombreDocumento,
                                                       String tipoDocumento,
                                                       String fechaAlta,
                                                       String usuarioCreador,
                                                       String orderDirection,
                                                       String orderField){

    /*listfinal=listfinal.stream().filter(c->{
      if (idDocumento!=null && !c.getId().toString().toLowerCase().contains(idDocumento))return false;
      if (usuarioCreador != null && !c.getUsuarioCreador().toLowerCase().contains(usuarioCreador.toLowerCase()))
        return false;
      if (nombreDocumento != null && !c.getNombreArchivo().toLowerCase().contains(nombreDocumento.toLowerCase()))
        return false;
      if (fechaAlta!=null && !c.getFechaAlta().toLowerCase().contains(fechaAlta.toLowerCase()))return false;
      return true;
    }).collect(Collectors.toList());
*/
    if(idDocumento != null)
      listfinal = listfinal.stream().filter((DocumentoDeudorDTO doc) -> StringUtils.containsIgnoreCase(doc.getId().toString(), idDocumento)).collect(Collectors.toList());
    if(nombreDocumento != null)
      listfinal = listfinal.stream().filter((DocumentoDeudorDTO doc) -> StringUtils.containsIgnoreCase(doc.getNombreArchivo(), nombreDocumento)).collect(Collectors.toList());
    if(tipoDocumento != null)
      listfinal = listfinal.stream().filter((DocumentoDeudorDTO doc) -> StringUtils.containsIgnoreCase(doc.getTipoDocumento(), tipoDocumento)).collect(Collectors.toList());
    if(fechaAlta != null)
      listfinal = listfinal.stream().filter((DocumentoDeudorDTO doc) -> StringUtils.containsIgnoreCase(doc.getFechaAlta(), fechaAlta)).collect(Collectors.toList());
    if(usuarioCreador != null)
      listfinal = listfinal.stream().filter((DocumentoDeudorDTO doc) -> StringUtils.containsIgnoreCase(doc.getUsuarioCreador(), usuarioCreador)).collect(Collectors.toList());

    if (orderDirection!=null && orderField!=null){
      Comparator<DocumentoDeudorDTO> comparator = null;
      switch (orderField) {
        case "tipoDocumento":
          comparator = Comparator.comparing(DocumentoDeudorDTO::getTipoDocumento);
          break;
        case "nombreDocumento":
          comparator = Comparator.comparing(DocumentoDeudorDTO::getNombreArchivo);
          break;
        case "usuarioCreador":
          comparator = Comparator.comparing(DocumentoDeudorDTO::getUsuarioCreador);
          break;
        case "fechaAlta":
          comparator = Comparator.comparing(DocumentoDeudorDTO::getFechaAlta);
          break;
        case "idDocumento":
        case "id":
        default:
          comparator = Comparator.comparing(DocumentoDeudorDTO::getId);
      }
      if (orderDirection.equals("asc")) comparator.reversed();
      listfinal=listfinal.stream().sorted(comparator).collect(Collectors.toList());
    }
    return listfinal;
  }


}
