package com.haya.alaska.integraciones.gd.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
public enum CodigoEntidad {
  CLIENTE("EN", "02"),
  CONTRATO("AF", "01"),
  ACTIVO_INMOBILIARIO("AI", "03"),
  GARANTIA("GA", "03"),
  INTERVINIENTE("EN", "02"),

  PROPUESTAS_VENTA_CONSENSUADA("OP", "07"),
  PROPUESTAS_DACION("OP", "16"),
  PROPUESTAS_POSESION_NEGOCIADA("OP", "39"),
  PROPUESTAS_CANCELACIONES("OP", "40"),
  PROPUESTAS_REACTIVACIONES("OP", "41"),
  PROPUESTAS_REESTRUCTURACION_REFINANCIACION("OP", "23"),
  PROPUESTAS_VENTA_DE_CREDITO("OP", "45"),

  USUARIOS("HA", "01"),
  CARTERA("HA", "02"),
  DEUDOR("HA", "03"),
  PORTALCLIENTE("HA", "04"),

  HIPOTECARIO(
    "PR",
    "01",
    Arrays.asList(
          "HIP",
          "H001",
          "H006",
          "H035",
          "H021",
          "H009",
          "H030",
          "H034",
          "H042",
          "H044",
          "H046",
          "H050",
          "H058",
          "H060",
          "H062",
          "H064",
          "Hipotecario")),
  EJECUTIVO(
      "PR",
      "02",
      Arrays.asList(
          "ETJ",
          "ETNJ",
          "H019",
          "H023",
          "H037",
          "H041",
          "H025",
          "H031",
          "H027",
          "H018",
          "H020",
          "H038",
          "H017",
          "H036",
          "H054",
          "Ejecución Notarial",
          "ART 579")),
  ORDINARIO("PR", "03", Arrays.asList("ORD", "H024", "Ordinario")),
  MONITORIO("PR", "05", Arrays.asList("MON", "H022", "Monitorio")),
  CAMBIARIO("PR", "06", Arrays.asList("H016", "Cambiario")),
  VERBAL("PR", "07", Arrays.asList("VERB", "H026", "H028", "Verbal", "Verbal desde Monitorio")),
  SUBASTA_TERCERO(
      "PR",
      "08",
      Arrays.asList(
          "H004", "H032", "H039", "H065", "H066", "H029", "H069", "P465", "H072", "P466", "H071",
          "P475", "P476", "P477", "P478", "P479", "P480", "P481", "P482", "H085", "H083", "H078",
          "H082", "H080", "H100")),
  CONCURSO(
      "PR",
      "09",
      Arrays.asList(
          "CON", "P400", "P404", "P400", "P404", "H011", "H048", "H040", "H052", "H007", "H015",
          "H010", "H012", "H008", "H033", "H003", "H043")),
  RECUPERACIONES(
      "PR",
      "10",
      Arrays.asList(
          "H067", "H068", "NOLIT", "P461", "P462", "P463", "P464", "H073", "P467", "P468", "P469",
          "P470", "P471", "P472", "P450", "P456", "P454", "P453", "P455", "P452", "P451",
          "Recobro")),
  PRECONTENCIOSO("PR", "13", Arrays.asList("PCO", "Precontencioso")),
  ;

  private final String tipo;
  private final String clase;
  private List<String> tipoProcedimientos = new ArrayList<>();

  CodigoEntidad(String tipo, String clase) {
    this.tipo = tipo;
    this.clase = clase;
  }

  CodigoEntidad(String tipo, String clase, List<String> tipoProcedimientos) {
    this.tipo = tipo;
    this.clase = clase;
    this.tipoProcedimientos = tipoProcedimientos;
  }

  public String getMatricula() {
    return this.tipo + "-" + this.clase;
  }

  public static CodigoEntidad obtenerPorTipoProcedimiento(String tipoProcedimiento) {
    return Arrays.stream(CodigoEntidad.values())
        .filter(codigoEntidad -> codigoEntidad.getTipoProcedimientos().contains(tipoProcedimiento))
        .findFirst()
        .orElse(null);
  }
}
