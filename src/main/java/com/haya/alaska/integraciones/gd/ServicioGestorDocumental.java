package com.haya.alaska.integraciones.gd;

import com.haya.alaska.catalogo.domain.CatalogoDocumental;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoDocumentalDTO;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.Documento;
import com.haya.alaska.integraciones.gd.model.Fichero;
import com.haya.alaska.integraciones.gd.model.PeticionHttp;
import com.haya.alaska.integraciones.gd.model.respuesta.*;
import com.haya.alaska.shared.dto.MetadatoContenedorInputDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ServicioGestorDocumental {
  private final PeticionHttp peticionHttp;

  @Autowired GestorDocumentalRepository gestorDocumentalRepository;

  public ServicioGestorDocumental(Config config) {
    this.peticionHttp = new PeticionHttp(config);
  }

  public List<Documento> listarDocumentos(CodigoEntidad codigoEntidad, String idActivo)
      throws IOException {
    return new RespuestaListaDocumentos(
            peticionHttp.get(
                "ListarDocumentos/"
                    + codigoEntidad.getTipo()
                    + "/"
                    + codigoEntidad.getClase()
                    + "/"
                    + idActivo))
        .getDocumentos();
  }

  public List<Documento> listarDocumetosPropuesta(CodigoEntidad codigoEntidad, String idPropuesta)
      throws IOException {
    try {
      return new RespuestaListaDocumentos(
              peticionHttp.get(
                  "ListarDocumentos/"
                      + codigoEntidad.getTipo()
                      + "/"
                      + codigoEntidad.getClase()
                      + "/"
                      + idPropuesta))
          .getDocumentos();
    } catch (Exception e) {
      return new ArrayList<>();
    }
  }

  public List<Documento> listarDocumentosMulti(CodigoEntidad codigoEntidad, List<String> idsActivos)
      throws IOException {
    Map<String, Object> parametrosGet = peticionHttp.getDefaultParametros();
    parametrosGet.put("lista_activos", String.join(",", idsActivos));
    return new RespuestaListaDocumentos(
            peticionHttp.get(
                "ListarDocumentosMulti/" + codigoEntidad.getTipo() + "/" + codigoEntidad.getClase(),
                parametrosGet))
        .getDocumentos();
  }

  public RespuestaCrearDocumento crearDocumento(
      CodigoEntidad codigoEntidad,
      String idActivo,
      MultipartFile multipartFile,
      JSONObject metadatos)
      throws IOException {
    return crearDocumento(codigoEntidad, idActivo, new Fichero(multipartFile), metadatos);
  }

  public RespuestaCrearDocumento crearDocumentoPropuestaNueva(
      CodigoEntidad codigoEntidad,
      Integer idPropuesta,
      MultipartFile multipartFile,
      JSONObject metadatos)
      throws IOException {
    return crearDocumentoPropuestas(
        codigoEntidad, idPropuesta, new Fichero(multipartFile), metadatos);
  }

  public RespuestaCrearDocumento crearDocumento(
      CodigoEntidad codigoEntidad, String idActivo, Fichero documento, JSONObject metadatos)
      throws IOException {
    Map<String, Object> parametrosGet = peticionHttp.getDefaultParametros();
    Map<String, Object> parametrosPost = new HashMap<>();
    Map<String, Fichero> ficherosPost = new HashMap<>();
    parametrosPost.put("metadata", metadatos);
    ficherosPost.put("documento", documento);

    String path2 =
        "CrearDocumento/"
            + codigoEntidad.getTipo()
            + "/"
            + codigoEntidad.getClase()
            + "/"
            + idActivo;

    return new RespuestaCrearDocumento(
        peticionHttp.post(
            "CrearDocumento/"
                + codigoEntidad.getTipo()
                + "/"
                + codigoEntidad.getClase()
                + "/"
                + idActivo,
            parametrosGet,
            parametrosPost,
            ficherosPost));
  }

  public RespuestaCrearContenedor crearContenedor(
      CodigoEntidad codigoEntidad, JSONObject metadatos, String clase, String tipo)
      throws IOException {
    Map<String, Object> parametrosGet = peticionHttp.getDefaultParametros();
    Map<String, Object> parametrosPost = new HashMap<>();
    Map<String, Fichero> ficherosPost = new HashMap<>();
    parametrosPost.put("tipo_expediente", tipo);
    parametrosPost.put("clase_expediente", clase);
    parametrosPost.put("metadata", metadatos);
    return new RespuestaCrearContenedor(
        peticionHttp.post("CrearContenedor/", parametrosGet, parametrosPost, ficherosPost));
  }

  public RespuestaCrearDocumento crearDocumentoPropuestas(
      CodigoEntidad codigoEntidad, Integer idPropuesta, Fichero documento, JSONObject metadatos)
      throws IOException {
    Map<String, Object> parametrosGet = peticionHttp.getDefaultParametros();
    Map<String, Object> parametrosPost = new HashMap<>();
    Map<String, Fichero> ficherosPost = new HashMap<>();
    parametrosPost.put("metadata", metadatos);
    ficherosPost.put("documento", documento);
    return new RespuestaCrearDocumento(
        peticionHttp.post(
            "CrearDocumento/"
                + codigoEntidad.getTipo()
                + "/"
                + codigoEntidad.getClase()
                + "/"
                + idPropuesta,
            parametrosGet,
            parametrosPost,
            ficherosPost));
  }

  public List<CatalogoDocumental> getCatalogosDocumentalPropuesta(CodigoEntidad codigoEntidad)
      throws IOException {
    return new RespuestaCatalogosDocumentales(
            peticionHttp.get(
                "CatalogoDocumental/" + codigoEntidad.getTipo() + "/" + codigoEntidad.getClase()))
        .getCatalogoDocumentalPropuesta(codigoEntidad);
  }

  public RespuestaCrearDocumento procesarDocumento(
      CodigoEntidad codigoEntidad,
      String idHaya,
      MultipartFile file,
      MetadatoDocumentoInputDTO metadatoDocumentoInputDTO)
      throws IOException {
    JSONObject metadatos = new JSONObject();
    JSONObject archivoFisico = new JSONObject();
    archivoFisico.put("contenedor", "CONT");
    metadatos.put(
        "General documento", metadatoDocumentoInputDTO.createDescripcionGeneralDocumento());
    metadatos.put("Archivo físico", archivoFisico);
    Fichero fichero = new Fichero(file);
    return crearDocumento(codigoEntidad, idHaya, fichero, metadatos);
  }

  public RespuestaCrearDocumento procesarDocumentoPropuesta(
      CodigoEntidad codigoEntidad,
      Integer idPropuesta,
      MultipartFile file,
      MetadatoDocumentoInputDTO metadatoDocumentoInputDTO)
      throws IOException {
    JSONObject metadatos = new JSONObject();
    JSONObject archivoFisico = new JSONObject();
    archivoFisico.put("contenedor", "CONT");
    metadatos.put(
        "General documento", metadatoDocumentoInputDTO.createDescripcionGeneralDocumento());
    metadatos.put("Archivo físico", archivoFisico);
    Fichero fichero = new Fichero(file);
    return crearDocumentoPropuestas(codigoEntidad, idPropuesta, fichero, metadatos);
  }

  public RespuestaCrearDocumento procesarDocumentoUsuario(
      CodigoEntidad codigoEntidad,
      String nombreUsuario,
      MultipartFile file,
      MetadatoDocumentoInputDTO metadatoDocumentoInputDTO)
      throws IOException {
    JSONObject metadatos = new JSONObject();
    JSONObject archivoFisico = new JSONObject();
    archivoFisico.put("contenedor", "CONT");
    metadatos.put(
        "General documento", metadatoDocumentoInputDTO.createDescripcionGeneralDocumento());
    metadatos.put(
        "ExtraDatos", metadatoDocumentoInputDTO.createDescripcionExtraDatos(nombreUsuario));
    metadatos.put("Archivo físico", archivoFisico);
    Fichero fichero = new Fichero(file);
    return crearDocumento(codigoEntidad, nombreUsuario, fichero, metadatos);
  }

  public RespuestaCrearContenedor procesarContenedor(
      CodigoEntidad codigoEntidad, MetadatoContenedorInputDTO metadatoContenedorInputDTO)
      throws Exception {
    try {
      JSONObject metadatos = new JSONObject();
      JSONObject archivoFisico = new JSONObject();
      archivoFisico.put("contenedor", "CONT");
      metadatos.put("Operación", metadatoContenedorInputDTO.createDescripcion());
      return crearContenedor(
          codigoEntidad, metadatos, codigoEntidad.getClase(), codigoEntidad.getTipo());
    } catch (Exception e) {
      return null;
    }
  }

  public RespuestaCrearContenedor procesarContenedorUsuario(
      CodigoEntidad codigoEntidad, MetadatoContenedorInputDTO metadatoContenedorInputDTO)
      throws Exception {
    try {
      JSONObject metadatos = new JSONObject();
      JSONObject archivoFisico = new JSONObject();
      archivoFisico.put("contenedor", "CONT");
      metadatos.put("Haya Corp", metadatoContenedorInputDTO.createDescripcion());
      return crearContenedor(
          codigoEntidad, metadatos, codigoEntidad.getClase(), codigoEntidad.getTipo());
    } catch (Exception e) {
      throw new Exception(e.getMessage());
    }
  }

  public RespuestaCrearContenedor procesarContenedorCartera(
      CodigoEntidad codigoEntidad, MetadatoContenedorInputDTO metadatoContenedorInputDTO)
      throws Exception {
    try {
      JSONObject metadatos = new JSONObject();
      JSONObject archivoFisico = new JSONObject();
      archivoFisico.put("contenedor", "CONT");
      metadatos.put("Haya Corp", metadatoContenedorInputDTO.createDescripcion());
      return crearContenedor(
          codigoEntidad, metadatos, codigoEntidad.getClase(), codigoEntidad.getTipo());
    } catch (Exception e) {
      throw new Exception(e.getMessage());
    }
  }

  public RespuestaCrearContenedor procesarContenedorDeudor(
      CodigoEntidad codigoEntidad, MetadatoContenedorInputDTO metadatoContenedorInputDTO)
      throws Exception {
    try {
      JSONObject metadatos = new JSONObject();
      JSONObject archivoFisico = new JSONObject();
      archivoFisico.put("contenedor", "CONT");
      metadatos.put("Haya Corp", metadatoContenedorInputDTO.createDescripcion());
      return crearContenedor(
          codigoEntidad, metadatos, codigoEntidad.getClase(), codigoEntidad.getTipo());
    } catch (Exception e) {
      throw new Exception(e.getMessage());
    }
  }

  public RespuestaEliminarDocumento eliminarDocumento(Documento documento) throws IOException {
    return this.eliminarDocumento(documento.getIdentificadorNodo());
  }

  public RespuestaEliminarDocumento eliminarDocumento(long idDocumento) throws IOException {
    GestorDocumental gestorDocumental=gestorDocumentalRepository.findById((int)idDocumento).orElse(null);
    if(gestorDocumental != null) {
      gestorDocumental.setActivo(false);
      gestorDocumentalRepository.save(gestorDocumental);
    }
    return new RespuestaEliminarDocumento(peticionHttp.post("BajaDocumento/" + idDocumento));
  }

  public RespuestaEliminarDocumento eliminarDocumentoUsuarios(long idDocumento) throws IOException {
    Integer idDocumentoint = Math.toIntExact(idDocumento);
    GestorDocumental gestorDocumental =
        gestorDocumentalRepository.findById(idDocumentoint).orElse(null);
    if(gestorDocumental != null)
      gestorDocumentalRepository.delete(gestorDocumental);
    return new RespuestaEliminarDocumento(peticionHttp.post("BajaDocumento/" + idDocumento));
  }

  public RespuestaDescargaDocumento descargarDocumento(Documento documento) throws IOException {
    return this.descargarDocumento(documento.getIdentificadorNodo());
  }

  public RespuestaDescargaDocumento descargarDocumento(long idDocumento) throws IOException {
    return new RespuestaDescargaDocumento(
        peticionHttp.getBinary("DescargarDocumento/" + idDocumento));
  }

  public List<CatalogoDocumental> getCatalogosDocumentales(CodigoEntidad codigoEntidad)
      throws IOException {
    return new RespuestaCatalogosDocumentales(
            peticionHttp.get(
                "CatalogoDocumental/" + codigoEntidad.getTipo() + "/" + codigoEntidad.getClase()))
        .getCatalogosDocumentales(codigoEntidad);
  }

  public CatalogoDocumentalDTO getCatalogoDocumental(CodigoEntidad codigoEntidad, String matricula)
      throws IOException {
    CatalogoDocumental catalogo = new CatalogoDocumental();
    List<CatalogoDocumental> listaCatalogosDocumentales =
        new RespuestaCatalogosDocumentales(
                peticionHttp.get(
                    "CatalogoDocumental/"
                        + codigoEntidad.getTipo()
                        + "/"
                        + codigoEntidad.getClase()))
            .getCatalogosDocumentales(codigoEntidad);
    for (CatalogoDocumental nuevo : listaCatalogosDocumentales) {
      if (nuevo.getMatricula().equals(matricula)) {
        catalogo = nuevo;
      }
    }
    CatalogoDocumentalDTO nuevoCatalogo = new CatalogoDocumentalDTO(catalogo);
    return nuevoCatalogo;
  }

  public String enlaceDescargarDocumento(long idDocumento) {
    return peticionHttp.getUrl(
        "DescargarDocumento/" + idDocumento, this.peticionHttp.getDefaultParametros());
  }

  public void eliminarDocumentos(List<Integer> documentoIdList) throws IOException {
    for (Integer id : documentoIdList) {
      this.eliminarDocumento(id);
    }
  }

  public void eliminarDocumentosVarios(List<Integer> documentoIdList) throws IOException {
    for (Integer id : documentoIdList) {
      this.eliminarDocumentoUsuarios(id);
    }
  }
}
