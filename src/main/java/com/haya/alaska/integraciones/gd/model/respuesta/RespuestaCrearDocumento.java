package com.haya.alaska.integraciones.gd.model.respuesta;

import com.mashape.unirest.http.JsonNode;

public class RespuestaCrearDocumento extends AbstractRespuestaJson {
  public RespuestaCrearDocumento(JsonNode response) {
    super(response);
  }

  public long getIdDocumento() {
    return this.getDatos().getLong("idDocumento");
  }
}
