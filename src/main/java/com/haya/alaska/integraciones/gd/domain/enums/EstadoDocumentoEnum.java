package com.haya.alaska.integraciones.gd.domain.enums;

public enum EstadoDocumentoEnum {
  FIRMADO("Firmado"),
  PENDIENTE("Pendiente"),
  ;

  private final String value;

  EstadoDocumentoEnum(String estadoContrato) {
    this.value = estadoContrato;
  }

  public String getValue() {
    return this.value;
  }
}
