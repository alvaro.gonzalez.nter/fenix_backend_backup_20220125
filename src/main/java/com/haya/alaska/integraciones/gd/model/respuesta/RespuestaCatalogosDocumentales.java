package com.haya.alaska.integraciones.gd.model.respuesta;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haya.alaska.catalogo.domain.CatalogoDocumental;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.mashape.unirest.http.JsonNode;
import lombok.SneakyThrows;

import java.util.List;
import java.util.stream.Collectors;

public class RespuestaCatalogosDocumentales extends AbstractRespuestaJson {
  public RespuestaCatalogosDocumentales(JsonNode response) {
    super(response);
  }

  @SneakyThrows
  public List<CatalogoDocumental> getCatalogosDocumentales(CodigoEntidad codigoEntidad) {
    List<CatalogoDocumental> lista =
        new ObjectMapper()
            .readValue(
                this.getDatos().getJSONArray("catalogosDocumentales").toString(),
                new TypeReference<>() {});
    List<String> listaEN =
        List.of(
            "EN-01-ACUI-04",
            "EN-01-CERJ-AP",
            "EN-02-DECL-06",
            "EN-01-ESCR-04",
            "EN-01-ESCR-21",
            "EN-01-ESCR-51",
            "EN-01-ESCR-52",
            "EN-01-ESCR-53",
            "EN-01-ESCR-54",
            "EN-01-ESCR-55",
            "EN-01-ESCR-56",
            "EN-01-ESCR-57",
            "EN-11-ESCR-65",
            "EN-04-ESIN-40",
            "EN-04-FICH-03",
            "EN-04-NOTS-07",
            "EN-01-DOCI-01",
            "EN-01-DOCI-02",
            "EN-01-DOCI-03",
            "EN-01-DOCI-04",
            "EN-01-DOCI-05",
            "EN-01-DOCI-06",
            "EN-01-DOCI-07",
            "EN-01-CNCV-82",
            "EN-04-COMU-AY",
            "EN-01-ESCR-15",
            "EN-04-FICH-50",
            "EN-01-CERJ-55",
            "EN-01-CERA-DU");
    List<String> listaAI =
        List.of(
            "AI-09-CEDU-02",
            "AI-15-CERA-BR",
            "AI-01-CERA-DR",
            "AI-01-CERJ-25",
            "AI-04-CERJ-49",
            "AI-01-ESCR-10",
            "AI-01-ESCR-13",
            "AI-01-ESCR-14",
            "AI-01-ESCR-17",
            "AI-01-ESCR-45",
            "AI-01-ESCR-48",
            "AI-01-ESCR-59",
            "AI-01-ESCR-61",
            "AI-01-ESCR-62",
            "AI-01-ESCR-63",
            "AI-01-FICH-02",
            "AI-01-FOTO-04",
            "AI-05-FOTO-09",
            "AI-09-LIPR-13",
            "AI-09-LIPR-14",
            "AI-01-PBLO-28",
            "AI-01-PLAO-27",
            "AI-04-TASA-11",
            "AI-01-DOCJ-BJ",
            "AI-01-SERE-24",
            "AI-01-SERE-26",
            "AI-01-NOTS-08",
            "AI-01-NOTS-09",
            "AI-02-CNCV-04",
            "AI-02-DOCJ-AJ",
            "AI-01-NOTS-01",
            "AI-02-CERJ-43",
            "AI-02-CERJ-54",
            "AI-01-ESIN-BR",
            "AI-05-FOTO-12");
    List<String> listaAF =
        List.of(
            "AF-03-CERJ-07",
            "AF-03-CERJ-57",
            "AF-02-CERJ-AF",
            "AF-08-DOCJ-AU",
            "AF-03-DOCN-08",
            "AF-02-ESCR-01",
            "AF-02-ESCR-02",
            "AF-02-ESCR-16",
            "AF-02-ESCR-18",
            "AF-02-ESCR-23",
            "AF-02-ESCR-16",
            "AF-02-ESCR-18",
            "AF-01-ESCR-32",
            "AF-01-ESCR-33",
            "AF-01-ESCR-34",
            "AF-02-ESCR-37",
            "AF-01-ESCR-38",
            "AF-02-ESCR-60",
            "AF-02-FACT-BS",
            "AF-02-NOVA-04",
            "AF-02-NOVA-05",
            "AF-02-NOVA-06",
            "AF-02-PBLO-29");
    List<String> listaGA =
        List.of(
            "GA-03-CEDU-02",
            "GA-03-CERA-BR",
            "GA-01-CERA-DR",
            "GA-01-CERJ-25",
            "GA-01-CERJ-43",
            "GA-01-CERJ-49",
            "GA-01-CERJ-54",
            "GA-01-CNCV-04",
            "GA-02-DOCJ-AJ",
            "GA-01-ESCR-10",
            "GA-01-ESCR-13",
            "GA-05-ESCR-14",
            "GA-01-ESCR-17",
            "GA-01-ESCR-45",
            "GA-01-ESCR-48",
            "GA-01-ESCR-59",
            "GA-01-ESCR-61",
            "GA-01-ESCR-62",
            "GA-01-ESCR-63",
            "GA-01-ESIN-BR",
            "GA-01-FICH-02",
            "GA-01-FOTO-04",
            "GA-05-FOTO-09",
            "GA-03-FOTO-12",
            "GA-06-LIPR-13",
            "GA-03-LIPR-14",
            "GA-01-PBLO-28",
            "GA-01-PLAO-27",
            "GA-02-TASA-09",
            "GA-01-DOCJ-BJ",
            "GA-01-SERE-24",
            "GA-01-SERE-26",
            "GA-01-NOTS-01",
            "GA-01-NOTS-08",
            "GA-01-NOTS-09",
            "GA-01-COMU-AY",
          "GA-01-CERA-DU",
          "GA-01-CERA-DV");

    switch (codigoEntidad.getTipo()) {
      case "EN":
        return lista.stream()
            .filter(catalogoDocumental -> listaEN.contains(catalogoDocumental.getMatricula()))
            .collect(Collectors.toList());
      case "AI":
        return lista.stream()
            .filter(catalogoDocumental -> listaAI.contains(catalogoDocumental.getMatricula()))
            .collect(Collectors.toList());
      case "AF":
        return lista.stream()
            .filter(catalogoDocumental -> listaAF.contains(catalogoDocumental.getMatricula()))
            .collect(Collectors.toList());
      case "GA":
        return lista.stream()
            .filter(catalogoDocumental -> listaGA.contains(catalogoDocumental.getMatricula()))
            .collect(Collectors.toList());
      default:
        return lista.stream()
            .filter(
                catalogoDocumental ->
                    catalogoDocumental.getMatricula().startsWith(codigoEntidad.getTipo()))
            .collect(Collectors.toList());
    }
  }

  @SneakyThrows
  public List<CatalogoDocumental> getCatalogoDocumentalPropuesta(CodigoEntidad codigoEntidad) {
    List<CatalogoDocumental> lista =
        new ObjectMapper()
            .readValue(
                this.getDatos().getJSONArray("catalogosDocumentales").toString(),
                new TypeReference<>() {});
    List<String> listaPn =
        List.of(
            "OP-17-DOCI-01",
            "OP-17-DOCI-02",
            "OP-17-DOCI-03",
            "OP-17-DOCI-04",
            "OP-17-DOCI-05",
            "OP-17-DOCI-06",
            "OP-17-DOCI-07",
            "OP-02-COMU-AV",
            "OP-02-COMU-AW",
            "OP-02-COMU-AX",
            "OP-01-FICH-46",
            "OP-02-FICH-47",
            "OP-02-FICH-48",
            "OP-02-FICH-49",
            "OP-17-CERJ-43",
            "OP-17-CERJ-54",
            "OP-17-CNCV-04",
            "OP-05-CNCV-82",
            "OP-02-COMU-AY",
            "OP-17-CORR-01",
            "OP-17-CORR-02",
            "OP-17-DOCJ-AJ",
            "OP-17-ESCR-15",
            "OP-17-ESIN-BR",
            "OP-17-FOTO-12",
            "OP-17-NOTS-01",
            "OP-02-FICH-50",
            "OP-17-CERA-DU",
            "OP-17-CERJ-55",
            "OP-17-CERA-DV",
            "OP-17-ESIN-DY");
    List<String> listaNormales =
        List.of(
            "OP-17-DOCI-01",
            "OP-17-DOCI-02",
            "OP-17-DOCI-03",
            "OP-17-DOCI-04",
            "OP-17-DOCI-05",
            "OP-17-DOCI-06",
            "OP-17-DOCI-07",
            "OP-02-COMU-AW",
            "OP-02-COMU-AX",
            "OP-01-FICH-46",
            "OP-02-FICH-47",
            "OP-02-FICH-48",
            "OP-02-FICH-49",
            "OP-03-ACUI-04",
            "OP-17-CEDU-02",
            "OP-17-CERA-BR",
            "OP-17-CERA-DQ",
            "OP-17-CERA-DR",
            "OP-17-CERJ-01",
            "OP-17-CERJ-07",
            "OP-17-CERJ-25",
            "OP-17-CERJ-49",
            "OP-17-CERJ-57",
            "OP-17-CERJ-AF",
            "OP-17-CERJ-AP",
            "OP-17-CNCV-04",
            "OP-17-CNCV-66",
            "OP-05-CNCV-82",
            "OP-02-COMU-AY",
            "OP-17-CORR-01",
            "OP-17-CORR-02",
            "OP-17-CORR-03",
            "OP-17-DECL-06",
            "OP-17-DOCA-68",
            "OP-17-DOCJ-AJ",
            "OP-17-DOCJ-AU",
            "OP-17-DOCN-08",
            "OP-17-ESCR-01",
            "OP-17-ESCR-02",
            "OP-17-ESCR-04",
            "OP-17-ESCR-10",
            "OP-17-ESCR-13",
            "OP-17-ESCR-14",
            "OP-17-ESCR-15",
            "OP-17-ESCR-16",
            "OP-17-ESCR-17",
            "OP-17-ESCR-18",
            "OP-17-ESCR-21",
            "OP-17-ESCR-23",
            "OP-17-ESCR-24",
            "OP-17-ESCR-25",
            "OP-17-ESCR-32",
            "OP-17-ESCR-33",
            "OP-17-ESCR-34",
            "OP-17-ESCR-37",
            "OP-17-ESCR-38",
            "OP-17-ESCR-45",
            "OP-17-ESCR-48",
            "OP-17-ESCR-51",
            "OP-17-ESCR-52",
            "OP-17-ESCR-53",
            "OP-17-ESCR-54",
            "OP-17-ESCR-55",
            "OP-17-ESCR-56",
            "OP-17-ESCR-57",
            "OP-17-ESCR-59",
            "OP-17-ESCR-60",
            "OP-17-ESCR-61",
            "OP-17-ESCR-62",
            "OP-17-ESCR-63",
            "OP-17-ESCR-65",
            "OP-17-ESIN-40",
            "OP-17-FACT-BS",
            "OP-17-FICH-02",
            "OP-17-FICH-03",
            "OP-17-FICH-05",
            "OP-17-FICH-20",
            "OP-17-FOTO-04",
            "OP-17-FOTO-09",
            "OP-17-LIPR-13",
            "OP-17-LIPR-14",
            "OP-17-NOTS-07",
            "OP-17-NOVA-03",
            "OP-17-NOVA-04",
            "OP-17-NOVA-05",
            "OP-17-NOVA-06",
            "OP-17-PBLO-28",
            "OP-17-PBLO-29",
            "OP-17-PLAO-27",
            "OP-17-TASA-09",
            "OP-17-DOCJ-BJ",
            "OP-17-SERE-24",
            "OP-17-SERE-26",
            "OP-17-NOTS-01",
            "OP-17-NOTS-08",
            "OP-17-NOTS-09",
            "OP-03-ACUE-49",
            "OP-02-FICH-50",
            "OP-17-CERA-DU",
            "OP-17-CERJ-55",
            "OP-17-CERA-DV",
            "OP-17-ESIN-DY",
            "OP-40-FICH-25",
            "OP-17-ESIN-BK",
            "OP-40-DOCJ-29",
            "OP-40-ACUI-20",
            "OP-40-TASA-11",
            "OP-40-INTR-17",
            "OP-40-CERA-16",
            "OP-40-CERA-12",
            "OP-40-CERJ-69",
            "OP-40-CERA-05",
            "OP-40-INLI-12",
            "OP-40-INLI-20",
            "OP-40-CERT-05",
            "OP-40-LIPR-06",
            "OP-40-LIPR-01",
            "OP-40-LIPR-03",
            "OP-40-SEGU-11",
            "OP-40-CERT-19",
            "OP-40-CERJ-BD",
            "OP-40-ACUE-37",
            "OP-40-LIPR-01",
            "OP-40-ESIN-DU",
            "OP-40-LIPR-05",
            "OP-40-MEMO-16",
            "OP-40-MEMO-17",
            "OP-40-CERT-39",
            "OP-40-ACTT-09",
            "OP-40-DOCN-01",
            "OP-40-CERJ-14",
            "OP-40-CEDU-01",
            "OP-17-CNCV-48",
            "OP-40-CERA-21",
            "OP-40-DOCA-85",
            "OP-17-CERJ-54",
            "OP-17-ESIN-BR",
            "OP-40-CERJ-AB",
            "OP-17-ESIN-BE",
            "OP-17-ESIN-BD",
            "OP-17-ESIN-AR");
    if (codigoEntidad.getTipo().equals("OP") && codigoEntidad.getClase().equals("39")) {
      // posesion negociada
      return lista.stream()
          .filter(catalogoDocumental -> listaPn.contains(catalogoDocumental.getMatricula()))
          .collect(Collectors.toList());
    } else {
      // las que no son posesion negociada
      return lista.stream()
          .filter(catalogoDocumental -> listaNormales.contains(catalogoDocumental.getMatricula()))
          .collect(Collectors.toList());
    }
  }
}
