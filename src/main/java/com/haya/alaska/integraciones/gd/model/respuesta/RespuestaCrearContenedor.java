package com.haya.alaska.integraciones.gd.model.respuesta;

import com.mashape.unirest.http.JsonNode;

public class RespuestaCrearContenedor extends AbstractRespuestaJson{

  public RespuestaCrearContenedor(JsonNode response){
    super(response);
  }
  public long getIdExpediente(){
    return this.getDatos().getLong("idExpediente");
  }
}
