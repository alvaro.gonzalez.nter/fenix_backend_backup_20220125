package com.haya.alaska.integraciones.gd;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Config {
  @Value("${integraciones.gd.url}")
  private String url;

  @Value("${integraciones.gd.usuario}")
  private String usuario;

  @Value("${integraciones.gd.clave}")
  private String clave;

  @Value("${integraciones.gd.secret}")
  private String secret;
}
