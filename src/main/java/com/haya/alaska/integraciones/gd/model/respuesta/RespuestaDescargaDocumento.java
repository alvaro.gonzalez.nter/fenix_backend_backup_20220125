package com.haya.alaska.integraciones.gd.model.respuesta;

import com.haya.alaska.integraciones.gd.model.RespuestaHttp;

import java.io.InputStream;

public class RespuestaDescargaDocumento {
  private final RespuestaHttp response;

  public RespuestaDescargaDocumento(RespuestaHttp response) {
    this.response = response;
  }

  public InputStream getBody() {
    return this.response.getBody();
  }

  public String getContentType() {
    return this.response.getHeader("Content-Type");
  }

  public String getContentDisposition() {
    return this.response.getHeader("Content-Disposition");
  }

  public String getAccessControlExposeHeaders() {
    return this.response.getHeader("Access-Control-Expose-Headers");
  }

}
