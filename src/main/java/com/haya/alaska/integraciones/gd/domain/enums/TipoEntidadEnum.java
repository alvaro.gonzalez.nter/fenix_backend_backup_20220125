package com.haya.alaska.integraciones.gd.domain.enums;

public enum TipoEntidadEnum {

  CONTRATO("CONTRATO"),
  GARANTIA("GARANTIA"),
  ACTIVOINMOBILIARIO("ACTIVO INMOBILIARIO"),
  INTERVINIENTE("INTERVINIENTE"),
  PROPUESTA("PROPUESTA"),
  ;

  private final String value;

  TipoEntidadEnum(String entidad) {
    this.value = entidad;
  }

  public String getValue(){
    return value;
  }
}
