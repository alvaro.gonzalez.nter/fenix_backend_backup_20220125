package com.haya.alaska.integraciones.gd.model.respuesta;

import com.haya.alaska.integraciones.gd.model.exception.RespuestaErrorException;
import com.mashape.unirest.http.JsonNode;
import lombok.SneakyThrows;
import org.json.JSONObject;

public abstract class AbstractRespuestaJson {
  private final JSONObject datos;

  @SneakyThrows
  public AbstractRespuestaJson(JsonNode response) {
    this.datos = response.getObject();

    String error = this.datos.getString("codigoError");
    if (!error.equals("")) {
      throw new RespuestaErrorException(this.datos);
    }
  }

  protected JSONObject getDatos() {
    return this.datos;
  }
}
