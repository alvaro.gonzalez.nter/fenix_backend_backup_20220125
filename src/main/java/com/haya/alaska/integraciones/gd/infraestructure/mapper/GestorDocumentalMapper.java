package com.haya.alaska.integraciones.gd.infraestructure.mapper;

import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoFirmaDto;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.infraestructure.controller.dto.GestorDocumentalDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface GestorDocumentalMapper {
  GestorDocumentalMapper INSTANCE = Mappers.getMapper(GestorDocumentalMapper.class);

  @Mapping(target = "idUsuario", source = "usuario.id")
  @Mapping(target = "idUsuarioDestino", source = "usuarioDestino.id")
  @Mapping(target = "estado", expression = "java(gestorDocumental.obtenerEstado())")
  GestorDocumentalDto toGestorDocumentalDto(GestorDocumental gestorDocumental);

  @Mapping(target = "tipoDocumento", source = "tipoDocumento.texto")
  @Mapping(target = "concepto", ignore = true)
  @Mapping(target = "estado", expression = "java(gestorDocumental.obtenerEstado())")
  @Mapping(target = "fechaAlta", source = "fechaGeneracion")
  DocumentoFirmaDto toDocumentoFirmaDto(GestorDocumental gestorDocumental);

  //El de la izquierda
  @Mapping(target = "id", source = "id")
  @Mapping(target = "tipo", source = "tipoDocumento.texto")
  @Mapping(target = "usuarioCreador", source = "usuario.nombre")
  @Mapping(target = "idEntidad", source = "idEntidad")
  @Mapping(target = "fechaActualizacion", source = "fechaActualizacion")
  @Mapping(target = "nombreArchivo", source = "nombreDocumento")
  @Mapping(target = "idHaya", source = "idHaya")
  DocumentoDTO toDocumentoDto(GestorDocumental gestorDocumental);
}
