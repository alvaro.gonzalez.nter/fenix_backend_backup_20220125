package com.haya.alaska.integraciones.gd.infraestructure.mapper;

import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DocumentoDeudorMapper {

  public List<DocumentoDTO> parseGestorToDocumento(List<GestorDocumental> gestorDocumentalList) {
    List<DocumentoDTO> documentoDTOList = new ArrayList<>();
    for (GestorDocumental ges : gestorDocumentalList) {
      DocumentoDTO documentoDTO = new DocumentoDTO();
      documentoDTO.setId(ges.getId().longValue());
      documentoDTO.setIdEntidad(ges.getIdEntidad());
      documentoDTO.setUsuarioCreador(ges.getUsuario().getNombre());
      documentoDTO.setFechaActualizacion(ges.getFechaActualizacion().toString());
      documentoDTO.setNombreArchivo(ges.getNombreDocumento());
      documentoDTO.setIdHaya(ges.getIdHaya());
      documentoDTOList.add(documentoDTO);
    }
    return documentoDTOList;
  }
}
