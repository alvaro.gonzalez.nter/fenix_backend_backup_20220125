package com.haya.alaska.integraciones.gd.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Documento {
  private String claseExpediente;
  private String contentType;
  @JsonProperty(value = "createdate")
  private String createDate;
  private String descripcionDocumento;
  private String fechaDocumento;
  private String fileSize;
  private long identificadorNodo;
  @JsonProperty(value = "id_activo")
  private String idActivo;
  private String nombreNodo;
  private boolean rel;
  private String serieDocumental;
  private String tdn1;
  private String tdn2;
  @JsonProperty(value = "tdn2_desc")
  private String tdn2Desc;
  private String tipoExpediente;
}
