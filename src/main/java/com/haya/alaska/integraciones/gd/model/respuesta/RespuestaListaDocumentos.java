package com.haya.alaska.integraciones.gd.model.respuesta;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haya.alaska.integraciones.gd.model.Documento;
import com.mashape.unirest.http.JsonNode;
import lombok.SneakyThrows;

import java.util.List;

public class RespuestaListaDocumentos extends AbstractRespuestaJson {
  public RespuestaListaDocumentos(JsonNode response) {
    super(response);
  }

  @SneakyThrows
  public List<Documento> getDocumentos() {
    return new ObjectMapper().readValue(
        this.getDatos().getJSONArray("documentos").toString(),
        new TypeReference<>() {
        }
    );
  }
}
