package com.haya.alaska.integraciones.gd.model.exception;

import lombok.Getter;
import org.json.JSONObject;

@Getter
public class RespuestaErrorException extends Exception {
  private final JSONObject datos;
  private final int codigo;

  public RespuestaErrorException(JSONObject datos) {
    super(datos.getString("mensajeError"));
    this.datos = datos;
    this.codigo = Integer.parseInt(datos.getString("codigoError"));
  }
}
