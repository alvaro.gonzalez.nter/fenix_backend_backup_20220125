package com.haya.alaska.integraciones.gd.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.http.entity.ContentType;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Getter
@AllArgsConstructor
public class Fichero {
  private final InputStream stream;
  private final ContentType contentType;
  private final String nombre;
  private final long longitud;

  public Fichero(MultipartFile file) throws IOException {
    this.stream = file.getInputStream();
    String contentType = file.getContentType();
    this.contentType = contentType != null ? ContentType.create(contentType) : ContentType.APPLICATION_OCTET_STREAM;
    this.nombre = file.getOriginalFilename();
    this.longitud = file.getSize();
  }
}
