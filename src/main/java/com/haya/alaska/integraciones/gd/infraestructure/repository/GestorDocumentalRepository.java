package com.haya.alaska.integraciones.gd.infraestructure.repository;

import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.domain.enums.EstadoDocumentoEnum;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface GestorDocumentalRepository extends JpaRepository<GestorDocumental, Integer> {
  // Para cuando es usuario emisor
  List<GestorDocumental> findAllByUsuarioAndUsuarioDestinoIsNotNull(Usuario usuario);
  // List<GestorDocumental>findAllByUsuarioDestino(Usuario usuario);
  List<GestorDocumental> findAllByClienteId(Integer clienteId);

  List<GestorDocumental> findAllByIdHayaIn(List<String> idHayaList);

  List<GestorDocumental> findAllByIdHayaAndClienteDeudorIdAndTipoDocumentoAndFechaGeneracionAfter(
      String idHaya, Integer idCliente, MatriculasEnum tipoDocumento, Date fechaInicio);
  List<GestorDocumental> findAllByIdHayaAndClienteDeudorIdAndTipoDocumento(
      String idHaya, Integer idCliente, MatriculasEnum tipoDocumento);

  List<GestorDocumental> findAllByIdIn(List<Integer> idDocumentosGestor);


  List<GestorDocumental> findAllByEstadoAndIdHayaIn(EstadoDocumentoEnum estado, List<String> idsHaya);
}
