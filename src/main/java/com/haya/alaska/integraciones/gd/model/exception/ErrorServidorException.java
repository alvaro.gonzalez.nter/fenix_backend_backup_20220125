package com.haya.alaska.integraciones.gd.model.exception;

import lombok.Getter;
import org.apache.http.client.methods.CloseableHttpResponse;

@Getter
public class ErrorServidorException extends Exception {
  private final int statusCode;
  private final String datos;

  public ErrorServidorException(CloseableHttpResponse response) {
    this.statusCode = response.getStatusLine().getStatusCode();
    this.datos = response.getEntity().toString();
  }
}
