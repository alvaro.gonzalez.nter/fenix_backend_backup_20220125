package com.haya.alaska.integraciones.gd.model;

import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
public enum MatriculasEnum {
  DNI(Collections.singletonList("EN-01-DOCI-01"), "DNI", CodigoEntidad.INTERVINIENTE),
  MINISTERIO_ASUNTOS_EXTERIORES(
      Collections.singletonList("EN-01-DOCI-02"),
      "Documento identidad Ministerio de Asuntos Exteriores",
      CodigoEntidad.INTERVINIENTE),
  OFICIAL_IDENTIDAD(
      Collections.singletonList("EN-01-DOCI-03"),
      "Documento oficial identidad (país de origen UE)",
      CodigoEntidad.INTERVINIENTE),
  LIBRO_FAMILIA(
      Collections.singletonList("EN-01-DOCI-04"), "Libro de familia", CodigoEntidad.INTERVINIENTE),
  PASAPORTE(Collections.singletonList("EN-01-DOCI-05"), "Pasaporte", CodigoEntidad.INTERVINIENTE),
  TARJETA_RESIDENCIA(
      Collections.singletonList("EN-01-DOCI-06"),
      "Tarjeta de residencia",
      CodigoEntidad.INTERVINIENTE),
  CIF(Collections.singletonList("EN-01-DOCI-07"), "CIF", CodigoEntidad.INTERVINIENTE),
  AUTORIZACION_FIRMA(
      Collections.singletonList("EN-01-ACUI-04"),
      "Autorización firma escritura",
      CodigoEntidad.INTERVINIENTE),
  SOLICITUD_CEDULA_HABITABILIDAD(
      Arrays.asList("AI-09-CEDU-02", "GA-03-CEDU-02"),
      "Solicitud Cédula de habitabilidad",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  INFORMES_CEDULA_HABITABILIDAD(
      Arrays.asList("AI-15-CERA-BR", "GA-03-CERA-BR"),
      "Informes técnicos y obtención documentos: Cédula Habitabilidad (Justificante de pago)",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  CORRIENTE_PAGO(
      Arrays.asList("AI-01-CERA-DR", "GA-01-CERA-DR"),
      "Comunidad de propietarios: Certificado corriente de pago",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  DEUDA_SALDO_CERO(
      Collections.singletonList("AF-03-CERJ-07"),
      "Certificado deuda saldo cero",
      CodigoEntidad.CONTRATO),
  IBI(
      Arrays.asList("AI-01-CERJ-25", "GA-01-CERJ-25"),
      "Impuesto sobre bienes inmuebles (IBI): certificado",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  ENTREGA_LLAVES(
      Arrays.asList("AI-02-CERJ-43", "GA-01-CERJ-43"),
      "Recibí entrega llaves",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  TASACION(
      Arrays.asList("AI-04-CERJ-49", "GA-01-CERJ-49"),
      "Tasación: certificado",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  ALQUILER_DEPOSITO_FIANZA(
      Arrays.asList("AI-02-CERJ-54", "GA-01-CERJ-54"),
      "Alquiler: justificante deposito fianza",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  DEUDA_SALDO_PENDIENTE(
      Collections.singletonList("AF-03-CERJ-57"),
      "Certificado deuda saldo pendiente",
      CodigoEntidad.CONTRATO),
  CANCELACION_EMBARGOS(
      Collections.singletonList("AF-02-CERJ-AF"),
      "Certificado cancelación embargos",
      CodigoEntidad.CONTRATO),
  AXESOR_INFORMA(
      Collections.singletonList("EN-01-CERJ-AP"),
      "Certificado Axesor/Informa",
      CodigoEntidad.INTERVINIENTE),
  ALQUILER_CONTRATO(
      Arrays.asList("AI-02-CNCV-04", "GA-01-CNCV-04"),
      "Alquiler: contrato",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  GDPR(
      Collections.singletonList("EN-01-CNCV-82"),
      "Consentimiento protección de datos (GDPR)",
      CodigoEntidad.INTERVINIENTE),
  IRPF(
      Collections.singletonList("EN-02-DECL-06"),
      "Impuesto renta personas físicas (IRPF): declaración",
      CodigoEntidad.INTERVINIENTE),
  DILIGENCIA_JUDICIAL_LANZAMIENTO(
      Arrays.asList("AI-02-DOCJ-AJ", "GA-02-DOCJ-AJ"),
      "Diligencia judicial del lanzamiento",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  DECLARACION_CONCURSO(
      Collections.singletonList("AF-08-DOCJ-AU"),
      "Auto Declaración de Concurso",
      CodigoEntidad.CONTRATO),
  LIQUIDACION_CIERRE_FIJACION_SALDO(
      Collections.singletonList("AF-03-DOCN-08"),
      "Acta de liquidación y cierre  / Acta de fijación de Saldo",
      CodigoEntidad.CONTRATO),
  CANCELACION_CARGA_HIPOTECARIA(
      Collections.singletonList("AF-02-ESCR-01"),
      "Cancelación carga hipotecaria: escritura",
      CodigoEntidad.CONTRATO),
  CANCELACION(
      Collections.singletonList("AF-02-ESCR-02"), "Cancelación: escritura", CodigoEntidad.CONTRATO),
  CONSTITUCION(
      Collections.singletonList("EN-01-ESCR-04"),
      "Constitución: escritura",
      CodigoEntidad.INTERVINIENTE),
  COMPRAVENTA(
      Arrays.asList("AI-01-ESCR-10", "GA-01-ESCR-10"),
      "Escritura compraventa",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  OBRA_NUEVA_DIVISION_HORIZONTAL(
      Arrays.asList("AI-01-ESCR-13", "GA-01-ESCR-13"),
      "Escritura obra nueva y/ o división horizontal",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  PERMUTA(
      Arrays.asList("AI-01-ESCR-14", "GA-05-ESCR-14"),
      "Escritura permuta",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  PODER(Collections.singletonList("EN-01-ESCR-15"), "Escritura poder", CodigoEntidad.INTERVINIENTE),
  PRESTAMO(
      Collections.singletonList("AF-02-ESCR-16"), "Escritura préstamo", CodigoEntidad.CONTRATO),
  SEGREGACION_AGRUPACION(
      Arrays.asList("AI-01-ESCR-17", "GA-01-ESCR-17"),
      "Escritura segregación y/o agrupación",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  SUBROGACION(
      Collections.singletonList("AF-02-ESCR-18"), "Escritura subrogación", CodigoEntidad.CONTRATO),
  HERENCIA_ADJUDICACI_ACEPTACION(
      Collections.singletonList("EN-01-ESCR-21"),
      "Herencia: escritura adjudicación y aceptación",
      CodigoEntidad.INTERVINIENTE),
  OPERACIONES_SINDICADAS_ENTIDADES_PARTICPANTES(
      Collections.singletonList("AF-02-ESCR-23"),
      "Operaciones sindicadas: escritura entidades participantes",
      CodigoEntidad.CONTRATO),
  PRESTAMO_ORIGINARIO(
      Collections.singletonList("AF-02-ESCR-16"),
      "Préstamo originario: escritura",
      CodigoEntidad.CONTRATO),
  PRESTAMO_ORIGINARIO_SUBROGACION(
      Collections.singletonList("AF-02-ESCR-18"),
      "Préstamo originario: escritura subrogación",
      CodigoEntidad.CONTRATO),
  AMPLIACION(
      Collections.singletonList("AF-01-ESCR-32"),
      "Escritura de ampliación",
      CodigoEntidad.CONTRATO),
  DISTRIBUCION_RESPONSABILIDAD_HIPOTECARIA(
      Collections.singletonList("AF-01-ESCR-33"),
      "Escritura de Distribución de Responsabilidad Hipotecaria",
      CodigoEntidad.CONTRATO),
  AFIANZAMIENTO(
      Collections.singletonList("AF-01-ESCR-34"),
      "Escritura de Afianzamiento",
      CodigoEntidad.CONTRATO),
  PRESTAMO_CARACTER_EJECTIVO(
      Collections.singletonList("AF-02-ESCR-37"),
      "Escritura préstamo con carácter ejecutivo",
      CodigoEntidad.CONTRATO),
  AMPLIACION_CARACTER_EJECTIVO(
      Collections.singletonList("AF-01-ESCR-38"),
      "Escritura de ampliación con carácter ejecutivo",
      CodigoEntidad.CONTRATO),
  DECLARACION_OBRA_REHABILITACION(
      Arrays.asList("AI-01-ESCR-45", "GA-01-ESCR-45"),
      "Escritura de declaración de obra / Rehabilitación",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  TITULO_INSCRITO_ESCRITURA(
      Arrays.asList("AI-01-ESCR-48", "GA-01-ESCR-48"),
      "Título inscrito (Escritura)",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  AMPLIACION_CAPITAL(
      Collections.singletonList("EN-01-ESCR-51"),
      "Escritura de ampliación de capital",
      CodigoEntidad.INTERVINIENTE),
  CAMBIO_DENOMINACION(
      Collections.singletonList("EN-01-ESCR-52"),
      "Escritura de cambio de denominación social",
      CodigoEntidad.INTERVINIENTE),
  CAMBIO_DOMICILIO(
      Collections.singletonList("EN-01-ESCR-53"),
      "Escritura de cambio de domicilio social",
      CodigoEntidad.INTERVINIENTE),
  CAMBIO_ESTATUTOS(
      Collections.singletonList("EN-01-ESCR-54"),
      "Escritura de cambio de estatutos sociales",
      CodigoEntidad.INTERVINIENTE),
  CAMBIO_OBJETO(
      Collections.singletonList("EN-01-ESCR-55"),
      "Escritura de cambio de objeto social",
      CodigoEntidad.INTERVINIENTE),
  FUSION_SOCIEDADES(
      Collections.singletonList("EN-01-ESCR-56"),
      "Escritura de fusión de sociedades",
      CodigoEntidad.INTERVINIENTE),
  VENTA_PARTICIPANTES(
      Collections.singletonList("EN-01-ESCR-57"),
      "Escritura de venta de participaciones",
      CodigoEntidad.INTERVINIENTE),
  LEASING(
      Arrays.asList("AI-01-ESCR-59", "GA-01-ESCR-59"),
      "Escritura de Leasing",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  RATIFICACION(
      Collections.singletonList("AF-02-ESCR-60"),
      "Escritura de ratificación",
      CodigoEntidad.CONTRATO),
  APORTACION_ACTIVOS(
      Arrays.asList("AI-01-ESCR-61", "GA-01-ESCR-61"),
      "Escritura de aportación de activos",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  OBRA_NUEVA(
      Arrays.asList("AI-01-ESCR-62", "GA-01-ESCR-62"),
      "Escritura Obra Nueva",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  DIVISION_HORIZONTAL(
      Arrays.asList("AI-01-ESCR-63", "GA-01-ESCR-63"),
      "Escritura División Horizontal",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  DISOLUCION_LIQUIDACION_SOCIEDAD(
      Collections.singletonList("EN-11-ESCR-65"),
      "Escritura de disolución y liquidación de la sociedad",
      CodigoEntidad.INTERVINIENTE),
  SOLVENCIA_TITULAR_AVALISTA(
      Collections.singletonList("EN-04-ESIN-40"),
      "Informe solvencia titular / avalista",
      CodigoEntidad.INTERVINIENTE),
  OCUPACIONAL(
      Arrays.asList("AI-01-ESIN-BR", "GA-01-ESIN-BR"),
      "Informe Ocupacional",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  MINUTA_ESCRITURA_CANCELACION(
      Collections.singletonList("AF-02-FACT-BS"),
      "Minuta de escritura de cancelación",
      CodigoEntidad.CONTRATO),
  CATASTRO(
      Arrays.asList("AI-01-FICH-02", "GA-01-FICH-02"),
      "Catastro: ficha",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  DECLARACION_BIENES(
      Collections.singletonList("EN-04-FICH-03"),
      "Declaración de bienes",
      CodigoEntidad.INTERVINIENTE),
  OTRA_FOTOGRAFIA(
      Arrays.asList("AI-01-FOTO-04", "GA-01-FOTO-04"),
      "Otra fotografía",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  FOTOGRAFIA_WEB(
      Arrays.asList("AI-05-FOTO-09", "GA-05-FOTO-09"),
      "Fotografía web",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  DOCUMENTO_FOTOGRAFICO_ENTREGA_LLAVES(
      Arrays.asList("AI-05-FOTO-12", "GA-03-FOTO-12"),
      "Documento fotográfico a la Entrega Llaves",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  INFORME_TECNICO_HABITABILIDAD(
      Arrays.asList("AI-09-LIPR-13", "GA-06-LIPR-13"),
      "Informe técnico de habitabilidad",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  SUSTITUTIVO_CEDULA_HABITABILIDAD(
      Arrays.asList("AI-09-LIPR-14", "GA-03-LIPR-14"),
      "Certificado sustitutivo de Cédula de Habitabilidad",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  SERVICIO_INDICES_SOLVENCIA_PATRIMONIAL(
      Collections.singletonList("EN-04-NOTS-07"),
      "Registro Propiedad: Servicio de índices de solvencia patrimonial",
      CodigoEntidad.INTERVINIENTE),
  NOVACION_ESCRITURA_CONTRATO(
      Collections.singletonList("AF-02-NOVA-04"),
      "Novación: escritura/contrato",
      CodigoEntidad.CONTRATO),
  QUITA_ESCRITURA_CONTRATO(
      Collections.singletonList("AF-02-NOVA-05"),
      "Quita: escritura/ contrato",
      CodigoEntidad.CONTRATO),
  REFINANCIACION_ESCRITURA_CONTRATO(
      Collections.singletonList("AF-02-NOVA-06"),
      "Refinanciación: escritura/contrato",
      CodigoEntidad.CONTRATO),
  SUBASTA_EDICTO(
      Arrays.asList("AI-01-PBLO-28", "GA-01-PBLO-28"),
      "Subasta: edicto",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  DECLARACION_CONCURSO_PUBLICACION(
      Collections.singletonList("AF-02-PBLO-29"),
      "Declaración concurso: publicación",
      CodigoEntidad.CONTRATO),
  PLANO_SITUACION(
      Arrays.asList("AI-01-PLAO-27", "GA-01-PLAO-27"),
      "Plano situación",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  TASACION_ACTIVO(
      Arrays.asList("AI-04-TASA-11", "GA-02-TASA-09"),
      "Tasación activo",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  TITULO_INSCRITO_TESTIMONIO(
      Arrays.asList("AI-01-DOCJ-BJ", "GA-01-DOCJ-BJ"),
      "Título inscrito (Testimonio)",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  SUBASTA_DECRETO_ADJUDICACION(
      Arrays.asList("AI-01-SERE-24", "GA-01-SERE-24"),
      "Subasta: auto / decreto adjudicación",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  SUBASTA_TESTIMONIO_DECRETO_ADJUDICACION(
      Arrays.asList("AI-01-SERE-26", "GA-01-SERE-26"),
      "Subasta: testimonio decreto adjudicación",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  NOTA_SIMPLE_LITERAL(
      Arrays.asList("AI-01-NOTS-01", "GA-01-NOTS-01"),
      "Registro Propiedad: nota simple / literal",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  NOTA_SIMPLE_SIN_CARGAS(
      Arrays.asList("AI-01-NOTS-08", "GA-01-NOTS-08"),
      "Registro Propiedad: nota simple inscrita/sin cargas",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  NOTA_SIMPLE_CON_CARGAS(
      Arrays.asList("AI-01-NOTS-09", "GA-01-NOTS-09"),
      "Registro Propiedad: nota simple inscrita/con cargas",
      CodigoEntidad.ACTIVO_INMOBILIARIO),
  ;

  private final List<String> matricula;
  private final String texto;
  private final CodigoEntidad codigoEntidad;

  MatriculasEnum(List<String> matricula, String texto, CodigoEntidad codigoEntidad) {
    this.matricula = matricula;
    this.texto = texto;
    this.codigoEntidad = codigoEntidad;
  }

  public static MatriculasEnum obtenerPorMatricula(String matricula) {
    return Arrays.stream(MatriculasEnum.values())
        .filter(matriculas -> matriculas.getMatricula().contains(matricula))
        .findFirst()
        .orElse(null);
  }
}
