package com.haya.alaska.integraciones.gd.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.integraciones.gd.domain.enums.EstadoDocumentoEnum;
import com.haya.alaska.integraciones.gd.domain.enums.TipoEntidadEnum;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Builder
@Audited
@AuditTable(value = "HIST_GESTOR_DOCUMENTAL")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_GESTOR_DOCUMENTAL")
public class GestorDocumental {

  @Id
  @Column(name = "ID_GESTOR_DOCUMENTAL")
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID", nullable = false)
  private Usuario usuario;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO_DESTINO")
  private Usuario usuarioDestino;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_GENERACION")
  private Date fechaGeneracion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ACTUALIZACION")
  private Date fechaActualizacion;

  @Column(name = "DES_NOMBRE_DOCUMENTO")
  private String nombreDocumento;

  @Enumerated(value = EnumType.STRING)
  @Column(name = "DES_TIPO_DOCUMENTO")
  private MatriculasEnum tipoDocumento;

  @Column(name = "DES_TIPO_MODELO_CARTERA")
  private String tipoModeloCartera;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FIRMA")
  private Date fechaFirma;

  @Enumerated(value = EnumType.STRING)
  @Column(name = "DES_ESTADO")
  private EstadoDocumentoEnum estado;

  @Column(name = "DES_ID_ENTIDAD")
  private String idEntidad;

  @Column(name = "DES_COMENTARIO")
  private String comentario;

  @Column(name = "DES_TITULO")
  private String titulo;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_INTERVINIENTE")
  private Interviniente interviniente;

  // Cliente para portal cliente
  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLIENTE")
  private Cliente cliente;

  // Cliente para portal deudor
  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLIENTE_DEUDOR")
  private Cliente clienteDeudor;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;

  @Enumerated(value = EnumType.STRING)
  @Column(name = "DES_TIPO_ENTIDAD")
  private TipoEntidadEnum tipoEntidad;

  @Column(name = "IND_ACTIVO")
  private Boolean activo = true;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  // Este constructor borrat tras modificar todos
  public GestorDocumental(
      Integer id,
      Usuario usuario,
      Usuario usuarioDestino,
      Date fechaGeneracion,
      Date fechaActualizacion,
      String nombreDocumento,
      MatriculasEnum tipoDocumento,
      String tipoModeloCartera,
      String idEntidad) {
    this.id = id;
    this.usuario = usuario;
    this.usuarioDestino = usuarioDestino;
    this.fechaGeneracion = fechaGeneracion;
    this.fechaActualizacion = fechaActualizacion;
    this.nombreDocumento = nombreDocumento;
    this.tipoDocumento = tipoDocumento;
    this.tipoModeloCartera = tipoModeloCartera;
  }

  public GestorDocumental(
      Integer id,
      Usuario usuario,
      Usuario usuarioDestino,
      Date fechaGeneracion,
      Date fechaActualizacion,
      String nombreDocumento,
      MatriculasEnum tipoDocumento,
      String tipoModeloCartera,
      String idEntidad,
      Cartera cartera) {
    this.id = id;
    this.usuario = usuario;
    this.usuarioDestino = usuarioDestino;
    this.fechaGeneracion = fechaGeneracion;
    this.fechaActualizacion = fechaActualizacion;
    this.nombreDocumento = nombreDocumento;
    this.tipoDocumento = tipoDocumento;
    this.tipoModeloCartera = tipoModeloCartera;
    this.cartera = cartera;
  }

  public GestorDocumental(
      Integer id,
      Usuario usuario,
      Usuario usuarioDestino,
      Date fechaGeneracion,
      Date fechaActualizacion,
      String nombreDocumento,
      MatriculasEnum tipoDocumento,
      String tipoModeloCartera,
      String idEntidad,
      String idHaya,
      TipoEntidadEnum tipoEntidadEnum) {
    this.id = id;
    this.usuario = usuario;
    this.usuarioDestino = usuarioDestino;
    this.fechaGeneracion = fechaGeneracion;
    this.fechaActualizacion = fechaActualizacion;
    this.nombreDocumento = nombreDocumento;
    this.tipoDocumento = tipoDocumento;
    this.tipoModeloCartera = tipoModeloCartera;
    this.idEntidad = idEntidad;
    this.idHaya = idHaya;
    this.tipoEntidad = tipoEntidadEnum;
  }

  // Guardamos idEntidad, el tipoEntidadEnum(contrato,interviniente, bien) y el idHayai
  public GestorDocumental(
      Integer id,
      Usuario usuario,
      Usuario usuarioDestino,
      Date fechaGeneracion,
      Date fechaActualizacion,
      String nombreDocumento,
      MatriculasEnum tipoDocumento,
      String tipoModeloCartera,
      String idEntidad,
      TipoEntidadEnum tipoEntidadEnum,
      String idHaya) {
    this.id = id;
    this.usuario = usuario;
    this.usuarioDestino = usuarioDestino;
    this.fechaGeneracion = fechaGeneracion;
    this.fechaActualizacion = fechaActualizacion;
    this.nombreDocumento = nombreDocumento;
    this.tipoDocumento = tipoDocumento;
    this.tipoModeloCartera = tipoModeloCartera;
    this.idEntidad = idEntidad;
    this.idHaya = idHaya;
    this.tipoEntidad = tipoEntidadEnum;
  }

  // Guardamos idEntidad, el tipoEntidadEnum(contrato,interviniente, bien) y el idHayai
  public GestorDocumental(
      Integer id,
      Usuario usuario,
      Usuario usuarioDestino,
      Date fechaGeneracion,
      Date fechaActualizacion,
      String nombreDocumento,
      MatriculasEnum tipoDocumento,
      String tipoModeloCartera,
      String idEntidad,
      TipoEntidadEnum tipoEntidadEnum,
      String idHaya,
      Cliente clienteDeudor) {
    this.id = id;
    this.usuario = usuario;
    this.usuarioDestino = usuarioDestino;
    this.fechaGeneracion = fechaGeneracion;
    this.fechaActualizacion = fechaActualizacion;
    this.nombreDocumento = nombreDocumento;
    this.tipoDocumento = tipoDocumento;
    this.tipoModeloCartera = tipoModeloCartera;
    this.idEntidad = idEntidad;
    this.idHaya = idHaya;
    this.tipoEntidad = tipoEntidadEnum;
    this.clienteDeudor = clienteDeudor;
  }

  public GestorDocumental(Integer idgestorDocumental, Usuario usuario) {
    this.id = idgestorDocumental;
    this.usuario = usuario;
  }

  public GestorDocumental(
      Integer idgestorDocumental,
      Usuario usuario,
      String comentario,
      String titulo,
      Interviniente idInterviniente,
      Cliente idCliente,
      String nombreDocumento,
      Date fechaGeneracion) {
    this.id = idgestorDocumental;
    this.usuario = usuario;
    this.comentario = comentario;
    this.titulo = titulo;
    this.interviniente = idInterviniente;
    this.cliente = idCliente;
  }

  public GestorDocumental(
      Integer idgestorDocumental,
      Usuario usuario,
      Usuario usuarioDestino,
      Date fechaGeneracion,
      Date fechaActualizacion,
      String nombreDocumento,
      MatriculasEnum tipoDocumento,
      String tipoModeloCartera) {
    this.id = idgestorDocumental;
    this.usuario = usuario;
    this.usuarioDestino = usuarioDestino;
    this.fechaGeneracion = fechaGeneracion;
    this.fechaActualizacion = fechaActualizacion;
    this.nombreDocumento = nombreDocumento;
    this.tipoDocumento = tipoDocumento;
    this.tipoModeloCartera = tipoModeloCartera;
  }

  public String obtenerEstado() {
    return !Objects.isNull(this.getEstado())
        ? this.getEstado().getValue()
        : EstadoDocumentoEnum.PENDIENTE.getValue();
  }
}
