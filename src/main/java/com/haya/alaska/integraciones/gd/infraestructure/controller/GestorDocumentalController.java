package com.haya.alaska.integraciones.gd.infraestructure.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/GestorDoc")
public class GestorDocumentalController {




  @ApiOperation(value = "Importa los datos",
    notes = "Datos en nuestra base datos Correctos")
  @GetMapping("/carga")
  public void cargaDatosGestorDocumental() throws Exception {

  }


}
