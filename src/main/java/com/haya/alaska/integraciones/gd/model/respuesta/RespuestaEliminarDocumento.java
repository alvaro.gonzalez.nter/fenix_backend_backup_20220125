package com.haya.alaska.integraciones.gd.model.respuesta;

import com.mashape.unirest.http.JsonNode;

public class RespuestaEliminarDocumento extends AbstractRespuestaJson {
  public RespuestaEliminarDocumento(JsonNode response) {
    super(response);
  }
}
