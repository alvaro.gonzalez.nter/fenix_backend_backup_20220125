package com.haya.alaska.integraciones.gd.infraestructure.controller.dto;

import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GestorDocumentalDto {

  public Integer id;

  public Integer idUsuario;

  public Integer idUsuarioDestino;

  public Date fechaGeneracion;

  public Date fechaActualizacion;

  public String nombreDocumento;

  public MatriculasEnum tipoDocumento;

  public String tipoModeloCartera;

  public Date fechaFirma;

  public String estado;

  public String comentario;
}
