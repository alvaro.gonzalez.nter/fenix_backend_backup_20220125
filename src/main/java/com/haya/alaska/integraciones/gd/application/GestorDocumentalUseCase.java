package com.haya.alaska.integraciones.gd.application;

import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDeudorDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;

import java.util.List;

public interface GestorDocumentalUseCase {

  List<DocumentoDTO> listarDocumentosGestor(List<DocumentoDTO> listadoIdGestor);

  List<DocumentoDTO> listarDocumentosGestorContrato(List<DocumentoDTO> listadoIdGestor);

  List<DocumentoDTO> listarDocumentosGestorIntervinientes(List<DocumentoDTO> listadoIdGestor);

  List<DocumentoDTO> listarDocumentosGestorBienes(List<DocumentoDTO> listadoIdGestor);

  List<DocumentoDTO> listarDocumentosGestorBienesPosesionNegociada(
      List<DocumentoDTO> listadoIdGestor,Integer idBienPN);

  List<DocumentoDTO> listarDocumentosGestorPropuesta(List<DocumentoDTO> listadoIdGestor);

  List<DocumentoRepositorioDTO> listarDocumentosRepositorio(
      List<DocumentoRepositorioDTO> listadoIdGestor);

  List<DocumentoRepositorioDTO> listarDocumentosRepositorioCarteras(
      List<DocumentoRepositorioDTO> listadoIdGestor);

  List<DocumentoDeudorDTO> filterOrderDocumentosDeudorDTO(
      List<DocumentoDeudorDTO> listfinal,
      String tipoDocumento,
      String fechaAlta,
      String orderDirection,
      String orderField);

  List<DocumentoDTO> filterOrderDocumentosDTO(
      List<DocumentoDTO> listfinal,
      String idDocumento,
      String usuarioCreador,
      String tipo,
      String idEntidad,
      String fechaActualizacion,
      String nombreArchivo,
      String idHaya,
      String orderDirection,
      String orderField);

  List<DocumentoRepositorioDTO> filterDocumentoRepositorioDTO(
      List<DocumentoRepositorioDTO> list,
      String idDocumento,
      String usuarioCreador,
      String tipoDocumento,
      String tipoModeloCartera,
      String nombreDocumento,
      String fechaActualizacion,
      String fechaAlta,
      String orderDirection,
      String orderField);

  List<DocumentoRepositorioDTO> busquedaDocumentoRepositorio(
      BusquedaRepositorioDTO busquedaRepositorioDTO,
      List<DocumentoRepositorioDTO> listdocumentoRepositorio);

  List<DocumentoDeudorDTO> busquedaDocumentoDeudorRepositorio(
    BusquedaRepositorioDTO busquedaRepositorioDTO,
    List<DocumentoDeudorDTO> listdocumentoDeudorRepositorio);

  List<DocumentoDeudorDTO>filterDocumentoDeudor(List<DocumentoDeudorDTO> listfinal,
                                           String idDocumento,
                                           String nombreDocumento,
                                           String tipoDocumento,
                                           String fechaAlta,
                                           String usuarioCreador,
                                           String orderDirection,
                                           String orderField);

  List<DocumentoDeudorDTO> listarDocumentoDeudorDTO(List<DocumentoDeudorDTO> listadoIdGestor);

  List<DocumentoDeudorDTO> listarDocumentoDeudorClienteDTO(List<DocumentoDeudorDTO> listadoIdGestor,Integer idCliente);

}
