package com.haya.alaska.integraciones.gd.model;

import com.haya.alaska.integraciones.gd.Config;
import com.mashape.unirest.http.JsonNode;
import lombok.SneakyThrows;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.poi.util.IOUtils;
import org.springframework.security.crypto.codec.Hex;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.*;

public class PeticionHttp {
  private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");

  private final Config config;

  public PeticionHttp(Config config) {
    this.config = config;
  }

  public JsonNode get(String path) throws IOException {
    return this.get(path, this.getDefaultParametros());
  }

  public JsonNode get(String path, Map<String, Object> parametros) throws IOException {
    return toJson(this.getBinary(path, parametros));
  }

  public RespuestaHttp getBinary(String path) throws IOException {
    return this.getBinary(path, this.getDefaultParametros());
  }

  public RespuestaHttp getBinary(String path, Map<String, Object> parametros) throws IOException {
    try (CloseableHttpClient client = HttpClients.createDefault()) {
      HttpGet request = new HttpGet(this.getUrl(path, parametros));
      request.addHeader("ws_cs_token", this.generateToken());
      return getResponse(client, request);
    }
  }

  public JsonNode post(String path) throws IOException {
    return this.post(path, this.getDefaultParametros());
  }

  public JsonNode post(String path, Map<String, Object> parametros) throws IOException {
    return this.post(path, parametros, new HashMap<>(), new HashMap<>());
  }

  public JsonNode post(
          String path, Map<String, Object> parametros, Map<String, Object> parametrosPost, Map<String, Fichero> ficheros
  ) throws IOException {
    try (CloseableHttpClient client = HttpClients.createDefault()) {
      HttpPost request = new HttpPost(this.getUrl(path, parametros));
      request.addHeader("ws_cs_token", this.generateToken());

      MultipartEntityBuilder bodyBuilder = MultipartEntityBuilder.create();
      bodyBuilder.setCharset(StandardCharsets.UTF_8);
      bodyBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
      for (String key : parametrosPost.keySet()) {
        Object item = parametrosPost.get(key);
        bodyBuilder.addTextBody(key, item.toString(), ContentType.create("text/plain", StandardCharsets.UTF_8));
      }
      for (String key : ficheros.keySet()) {
        Fichero fichero = ficheros.get(key);
        bodyBuilder.addPart(key, new InputStreamBody(fichero.getStream(), fichero.getContentType(), fichero.getNombre()) {
          @Override
          public long getContentLength() {
            return fichero.getLongitud();
          }
        });
      }
      request.setEntity(bodyBuilder.build());

      return toJson(getResponse(client, request));
    }
  }

  @SneakyThrows
  private String generateToken() {
    String date = simpleDateFormat.format(new Date());
    String token = config.getSecret() + ":" + config.getUsuario() + ":" + date;
    MessageDigest digest = MessageDigest.getInstance("SHA-256");
    byte[] hash = digest.digest(token.getBytes(StandardCharsets.UTF_8));
    return new String(Hex.encode(hash));
  }

  public String getUrl(String path, Map<String, Object> parametros) {
    String url = config.getUrl() + "/OTCS/cs.exe/csHaya/" + path;
    if (!parametros.isEmpty()) {
      List<String> stringParams = new ArrayList<>();
      for (String key : parametros.keySet()) {
        stringParams.add(key + "=" + parametros.get(key));
      }
      url += "?" + String.join("&", stringParams);
    }
    return url;
  }

  public Map<String, Object> getDefaultParametros() {
    Map<String, Object> parametros = new HashMap<>();
    parametros.put("usuario", this.config.getUsuario());
    parametros.put("usuarioOperacional", this.config.getUsuario());
    return parametros;
  }

  @SneakyThrows
  private static RespuestaHttp getResponse(CloseableHttpClient client, HttpRequestBase request) {
    try (CloseableHttpResponse response = client.execute(request)) {
      return new RespuestaHttp(response);
    }
  }

  private static JsonNode toJson(RespuestaHttp respuestaHttp) throws IOException {
    if (!respuestaHttp.getHeader("Content-Type").startsWith("application/json")) {
      throw new IllegalArgumentException("Content-Type no válido");
    }
    String data = new String(IOUtils.toByteArray(respuestaHttp.getBody()), StandardCharsets.UTF_8);
    return new JsonNode(data);
  }
}
