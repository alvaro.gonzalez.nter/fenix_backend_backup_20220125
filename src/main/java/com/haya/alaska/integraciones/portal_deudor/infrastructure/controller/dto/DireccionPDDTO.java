package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class DireccionPDDTO implements Serializable {
  private Integer id;
  private String nombre;
  private String numero;
  private String portal;
  private String bloque;
  private String escalera;
  private String piso;
  private String puerta;
}
