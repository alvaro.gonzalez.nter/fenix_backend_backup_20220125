package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class MovimientoPDOutputDTO implements Serializable {
  private Date fechaValor;
  private String contrato;
  private CatalogoMinInfoDTO producto;
  private String importe;
  private String pago;
  private Boolean origenWeb;
}
