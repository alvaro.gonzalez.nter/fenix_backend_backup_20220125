package com.haya.alaska.integraciones.portal_deudor.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "fenixPortalDeudor", url = "${portalDeudor.host}")
public interface PortalDeudorFeignClient {

  @GetMapping("/guardarContrasena")
  String guardarContrasena(
          @RequestParam(value = "nombre") String nombre,
          @RequestParam(value = "password") String password,
          @RequestParam(value = "idInterviniente") Integer idInterviniente,
          @RequestParam(value = "idGestor") Integer idGestor);

  @GetMapping("/ping")
  String ping();
}
