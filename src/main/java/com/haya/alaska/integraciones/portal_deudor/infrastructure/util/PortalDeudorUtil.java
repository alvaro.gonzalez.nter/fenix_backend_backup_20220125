package com.haya.alaska.integraciones.portal_deudor.infrastructure.util;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class PortalDeudorUtil {

  // Ivan, tu escribes tus autowired arriba de este comentario



  // Arriba los autowired, abajo los métodos



  // Ivan, tu escribes tus métodos arriba de este comentario

  public List<Contrato> getContratos(Interviniente i, Cartera c){
    List<Contrato> result = i.getContratos().stream().filter(
      contratoInterviniente -> {
        return contratoInterviniente.getContrato() != null;
      })
      .map(ContratoInterviniente::getContrato).distinct().collect(Collectors.toList());
    if (c != null){
      result = result.stream().filter(co -> co.getExpediente().getCartera().getId().equals(c.getId())).collect(Collectors.toList());
    }
    return result;
  }

  public List<Expediente> getExpedientes(Interviniente i, Cartera c){
    List<Contrato> contratos = this.getContratos(i, c);

    List<Expediente> result =
        contratos.stream()
            .filter(
                contrato -> {
                  return contrato.getExpediente() != null;
                })
            .map(Contrato::getExpediente)
            .distinct()
            .collect(Collectors.toList());
    if (c != null){
      result = result.stream().filter(ex -> ex.getCartera().getId().equals(c.getId())).collect(Collectors.toList());
    }
    return result;
  }

  public List<AcuerdoPago> getAcuerdos(Interviniente i, Cartera c){
    List<Contrato> contratos = this.getContratos(i, c);
    List<AcuerdoPago> result = new ArrayList<>();
    for (Contrato co : contratos){
      result.addAll(co.getAcuerdosPago());
    }
    result = result.stream().distinct().collect(Collectors.toList());
    return result;
  }

  //TODO
  public List<Usuario> getGestores(Interviniente i, Cartera c){
    List<Expediente> expedientes = getExpedientes(i, c);
    List<Usuario> usuarios = new ArrayList<>();
    for (Expediente e : expedientes){
      Usuario gestor = e.getGestor();
      if (gestor != null) usuarios.add(gestor);
    }
    return usuarios;
  }

  public static char[] generadorPassword(int longitudMinima, int longitudMaxima, int numMayusculas, int numDigitos, int numEspeciales) {
    final String MAYUSCULA  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    final String MINUSCULA   = "abcdefghijklmnopqrstuvwxyz";
    final String NUMERO     = "0123456789";
    final String ESPECIAL   = "!@#$%^&*_=+-/";

    Random rnd = new Random();
    int len = rnd.nextInt(longitudMaxima - longitudMinima + 1) + longitudMinima;
    char[] pswd = new char[len];
    int index = 0;

    for (int i = 0; i < numMayusculas; i++) {
      index = getNextIndex(rnd, len, pswd);
      pswd[index] = MAYUSCULA.charAt(rnd.nextInt(MAYUSCULA.length()));
    }

    for (int i = 0; i < numDigitos; i++) {
      index = getNextIndex(rnd, len, pswd);
      pswd[index] = NUMERO.charAt(rnd.nextInt(NUMERO.length()));
    }

    for (int i = 0; i < numEspeciales; i++) {
      index = getNextIndex(rnd, len, pswd);
      pswd[index] = ESPECIAL.charAt(rnd.nextInt(ESPECIAL.length()));
    }

    for(int i = 0; i < len; i++) {
      if(pswd[i] == 0) {
        pswd[i] = MINUSCULA.charAt(rnd.nextInt(MINUSCULA.length()));
      }
    }

    return pswd;
  }

  private static int getNextIndex(Random rnd, int len, char[] pswd) {
    int index = rnd.nextInt(len);
    while(pswd[index = rnd.nextInt(len)] != 0);
    return index;
  }
}
