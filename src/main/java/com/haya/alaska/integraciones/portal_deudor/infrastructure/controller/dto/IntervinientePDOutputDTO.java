package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class IntervinientePDOutputDTO implements Serializable {
  //Datos de contacto
  private String nombre;
  private String apellidos;
  private DatoContactoPDDTO principal;
  private List<DatoContactoPDDTO> contactos;
  //Domicilio y datos de pago
  private DireccionPDDTO direccion;
  //Datos personales
  private Date fechaNacimiento;
  private CatalogoMinInfoDTO paisNacimiento;
  private CatalogoMinInfoDTO nacionalidad;
  private CatalogoMinInfoDTO residencia;
  private String job;
  private CatalogoMinInfoDTO estadoCivil;
}
