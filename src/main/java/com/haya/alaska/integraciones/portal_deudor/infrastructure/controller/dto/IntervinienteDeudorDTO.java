package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class IntervinienteDeudorDTO implements Serializable {

  Double deudaPendiente;//a partir del saldo
  Double pagosEfectuados;//a partir de los movimientos
  Integer nContratos;
}
