package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class IntervinienteAcuerdoPagoDTO implements Serializable {
  Double importe;//Planes de pago
  Integer cuotas; //planes de pago
  String liquidacion;//planes de pago
  Integer nContratosPlanes; //planes de pago
  Date fechaAcuerdoPago;
  List<DatoContratoPDDTO>datoContratoPDDTOList;
}
