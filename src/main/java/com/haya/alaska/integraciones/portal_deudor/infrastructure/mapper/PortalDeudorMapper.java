package com.haya.alaska.integraciones.portal_deudor.infrastructure.mapper;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.estado_civil.domain.EstadoCivil;
import com.haya.alaska.estado_civil.infrastructure.repository.EstadoCivilRepository;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto.*;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoOutputDto;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.plazo_acuerdo_pago.domain.PlazoAcuerdoPago;
import com.haya.alaska.plazo_acuerdo_pago.infrastructure.repository.PlazoAcuerdoPagoRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
public class PortalDeudorMapper {

  // Ivan, tu escribes tus autowired arriba de este comentario
  @Autowired
  DatoContactoRepository datoContactoRepository;
  @Autowired
  PaisRepository paisRepository;
  @Autowired
  EstadoCivilRepository estadoCivilRepository;
  @Autowired
  DireccionRepository direccionRepository;
  @Autowired
  PlazoAcuerdoPagoRepository plazoAcuerdoPagoRepository;
  @Autowired
  ClienteRepository clienteRepository;

  // Arriba los autowired, abajo los métodos
  public IntervinienteAcuerdoPagoDTO parseToIntervinienteAcuerdoDTO(AcuerdoPago acuerdoPago) {
    IntervinienteAcuerdoPagoDTO intervinienteAcuerdoPagoDTO = new IntervinienteAcuerdoPagoDTO();
    intervinienteAcuerdoPagoDTO.setImporte(acuerdoPago.getImporteTotal());
    intervinienteAcuerdoPagoDTO.setCuotas(acuerdoPago.getNumeroPlazos());
    intervinienteAcuerdoPagoDTO.setLiquidacion(acuerdoPago.getPeriodicidadAcuerdoPago().getValor());
    intervinienteAcuerdoPagoDTO.setNContratosPlanes(acuerdoPago.getContratos().size());
    List<Contrato>contratoList=acuerdoPago.getContratos().stream().collect(Collectors.toList());
    List<DatoContratoPDDTO>datoContratoPDDTOList=new ArrayList<>();
    for (Contrato contrato :contratoList) {
      DatoContratoPDDTO datoContratoPDDTO=new DatoContratoPDDTO();
      datoContratoPDDTO.setContrato(contrato.getIdCarga());
      datoContratoPDDTO.setProducto(contrato.getProducto().getValor());
      datoContratoPDDTOList.add(datoContratoPDDTO);
    }
    intervinienteAcuerdoPagoDTO.setDatoContratoPDDTOList(datoContratoPDDTOList);
    List<PlazoAcuerdoPago> plazoAcuerdoPagoList = plazoAcuerdoPagoRepository.findAllByAcuerdoPagoIdOrderByFechaAsc(acuerdoPago.getId());
    plazoAcuerdoPagoList = plazoAcuerdoPagoList.stream().filter(plazoAcuerdoPago -> {
      if (plazoAcuerdoPago.getAbonado()) {
        return false;
      }
      return true;
    }).collect(Collectors.toList());
    intervinienteAcuerdoPagoDTO.setFechaAcuerdoPago(plazoAcuerdoPagoList.get(0).getFecha());
    return intervinienteAcuerdoPagoDTO;
  }

  public IntervinienteDeudorContratoDTO parseToInformacionDeudorContratoDTO(Contrato contrato, Interviniente interviniente, Integer idCliente) throws NotFoundException {
    IntervinienteDeudorContratoDTO intervinienteDeudorContratoDTO = new IntervinienteDeudorContratoDTO();
    Cliente cliente = clienteRepository
      .findById(idCliente)
      .orElseThrow(
        () ->
          new NotFoundException("cliente", idCliente));

    Cartera cartera=contrato.getExpediente().getCartera();
    List<ClienteCartera>clientecarteraList=cartera.getClientes().stream().collect(Collectors.toList());

//Supuestamente no debería haber ningún problema al obtener la primera
   // Optional<ClienteCartera> clienteCartera=cliente.getCarteras().stream().findFirst();
    intervinienteDeudorContratoDTO.setIdInterviniente(interviniente.getId());
    intervinienteDeudorContratoDTO.setContrato(contrato.getIdCarga());
    intervinienteDeudorContratoDTO.setDeuda(contrato.getUltimoSaldo().getSaldoGestion());
    intervinienteDeudorContratoDTO.setProducto(contrato.getProducto().getValor());
    intervinienteDeudorContratoDTO.setCliente(clientecarteraList.stream().findFirst().get().getCiaPropiedadCartera());
    return intervinienteDeudorContratoDTO;
  }

  // Ivan, tu escribes tus métodos arriba de este comentario

  public IntervinientePDOutputDTO createOutput(Interviniente i) {
    IntervinientePDOutputDTO result = new IntervinientePDOutputDTO();

    if (i.getPersonaJuridica()){
      result.setNombre(i.getRazonSocial());
      result.setFechaNacimiento(i.getFechaConstitucion());
    } else {
      result.setNombre(i.getNombre());
      result.setApellidos(i.getApellidos());
      result.setFechaNacimiento(i.getFechaNacimiento());
    }
    result.setJob(i.getDescripcionActividad());


    if (i.getPaisNacimiento() != null)
      result.setPaisNacimiento(new CatalogoMinInfoDTO(i.getPaisNacimiento()));
    if (i.getNacionalidad() != null)
      result.setNacionalidad(new CatalogoMinInfoDTO(i.getNacionalidad()));
    if (i.getResidencia() != null)
      result.setResidencia(new CatalogoMinInfoDTO(i.getResidencia()));
    if (i.getEstadoCivil() != null)
      result.setEstadoCivil(new CatalogoMinInfoDTO(i.getEstadoCivil()));


    DatoContacto principal = null;
    List<DatoContacto> datosPD = datoContactoRepository.findAllByIntervinienteIdAndOrigen(i.getId(), "Portal Deudor");
    if (!datosPD.isEmpty()) principal = datosPD.get(datosPD.size() - 1);

    List<DatoContactoPDDTO> datos = new ArrayList<>();
    for (DatoContacto dc : i.getDatosContacto()) {
      if (principal == null && dc.getOrden() != null && dc.getOrden().equals(1)) {
        principal = dc;
      } else {
        if (principal != null && !principal.getId().equals(dc.getId()))
          datos.add(this.parseDatoContacto(dc));
      }
    }
    result.setContactos(datos);
    if (principal != null) result.setPrincipal(this.parseDatoContacto(principal));

    Direccion dP = null;
    List<Direccion> direccionesPD = direccionRepository.findAllByIntervinienteIdAndOrigen(i.getId(), "Portal Deudor");
    if (!direccionesPD.isEmpty()) dP = direccionesPD.get(direccionesPD.size() - 1);

    if (dP == null){
      for (Direccion d : i.getDirecciones()) {
        if (d.getPrincipal() != null && d.getPrincipal()) {
          dP = d;
          break;
        }
      }
    }

    if (dP != null) result.setDireccion(this.parseDireccion(dP));

    return result;
  }

  private DatoContactoPDDTO parseDatoContacto(DatoContacto dc) {
    DatoContactoPDDTO result = new DatoContactoPDDTO();
    BeanUtils.copyProperties(dc, result);
    return result;
  }

  private DireccionPDDTO parseDireccion(Direccion d) {
    DireccionPDDTO result = new DireccionPDDTO();
    BeanUtils.copyProperties(d, result);
    return result;
  }

  public Boolean actualizarInterviniente(Interviniente i, IntervinientePDInputDTO input) throws NotFoundException {
    Boolean mensaje = false;
    if (i.getPersonaJuridica()){
      if (input.getNombre() != null)
        i.setRazonSocial(input.getNombre());
    } else {
      if (input.getNombre() != null)
        i.setNombre(input.getNombre());
      if (input.getApellidos() != null)
        i.setApellidos(input.getApellidos());
    }

    if (input.getDireccion() != null) {
      DireccionPDDTO dir = input.getDireccion();
      Direccion d = new Direccion();

      actualizarDireccion(d, dir);
      d.setInterviniente(i);
      direccionRepository.save(d);
    }

    if (i.getPersonaJuridica()){
      if (input.getFechaNacimiento() != null)
        i.setFechaConstitucion(input.getFechaNacimiento());
    } else {
      if (input.getFechaNacimiento() != null)
        i.setFechaNacimiento(input.getFechaNacimiento());
    }

    if (input.getTrabajo() != null){
      i.setDescripcionActividad(input.getTrabajo());
    }
    if (input.getPaisNacimiento() != null) {
      Pais p = paisRepository.findById(input.getPaisNacimiento()).orElseThrow(() ->
        new NotFoundException("Pais", input.getPaisNacimiento()));
      i.setPaisNacimiento(p);
    }
    if (input.getNacionalidad() != null) {
      Pais p = paisRepository.findById(input.getNacionalidad()).orElseThrow(() ->
        new NotFoundException("Pais", input.getNacionalidad()));
      i.setNacionalidad(p);
    }
    if (input.getResidencia() != null) {
      Pais p = paisRepository.findById(input.getResidencia()).orElseThrow(() ->
        new NotFoundException("Pais", input.getResidencia()));
      i.setResidencia(p);
    }

    if (input.getEstadoCivil() != null) {
      EstadoCivil e = estadoCivilRepository.findById(input.getEstadoCivil()).orElseThrow(() ->
        new NotFoundException("EstadoCivil", input.getEstadoCivil()));
      i.setEstadoCivil(e);
    }

    return mensaje;
  }

  public Boolean actualizarDC(Interviniente i, DatoContacto dc, DatoContactoPDDTO contacto) throws NotFoundException {
    Boolean mensaje = false;
    if (dc.getId() == null) {
      dc = new DatoContacto();
      dc.setFijoValidado(false);
      dc.setEmailValidado(false);
      dc.setMovilValidado(false);
      dc.setOrigen("Portal Deudor");
      dc.setOrden(2);
      dc.setInterviniente(i);
      dc.setFechaAlta(new Date());
    }
    dc.setFechaActualizacion(new Date());
    if (dc.getMovil() != null) {
      if (contacto.getMovil() == null){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("The mobile data is missing.");
        else throw new NotFoundException("Falta el dato del movil.");
      }
      if (dc.getMovil().equals(contacto.getMovil())) mensaje = true;
      dc.setMovil(contacto.getMovil());
    } else {
      if (contacto.getMovil() != null) {
        mensaje = true;
        dc.setMovil(contacto.getMovil());
      }
    }

    if (dc.getFijo() != null) {
      if (contacto.getFijo() == null){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("The landline data is missing.");
        else throw new NotFoundException("Falta el dato del fijo.");
      }
      if (dc.getFijo().equals(contacto.getFijo())) mensaje = true;
      dc.setFijo(contacto.getFijo());
    } else {
      if (contacto.getFijo() != null) {
        mensaje = true;
        dc.setFijo(contacto.getFijo());
      }
    }

    if (dc.getEmail() != null) {
      if (contacto.getEmail() == null){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("The mobile data is missing.");
        else throw new NotFoundException("Falta el dato del movil.");
      }
      if (dc.getEmail().equals(contacto.getEmail())) mensaje = true;
      dc.setEmail(contacto.getEmail());
    } else {
      if (contacto.getEmail() != null) {
        mensaje = true;
        dc.setEmail(contacto.getEmail());
      }
    }

    datoContactoRepository.save(dc);

    return mensaje;
  }

  public void actualizarDireccion(Direccion d, DireccionPDDTO dir) {
    if (d.getId() == null) {
      d.setOrigen("Portal Deudor");
      d.setPrincipal(true);
    }
    if (dir.getNombre() != null) {
      d.setNombre(dir.getNombre());
    }
    if (dir.getNumero() != null) {
      d.setNumero(dir.getNumero());
    }
    if (dir.getPortal() != null) {
      d.setPortal(dir.getPortal());
    }
    if (dir.getBloque() != null) {
      d.setBloque(dir.getBloque());
    }
    if (dir.getEscalera() != null) {
      d.setEscalera(dir.getEscalera());
    }
    if (dir.getPiso() != null) {
      d.setPiso(dir.getPiso());
    }
    if (dir.getPuerta() != null) {
      d.setPuerta(dir.getPuerta());
    }
    d.setFechaActualizacion(new Date());
  }

  public MovimientoPDOutputDTO parseToMovimientoPDOutputDTO(Movimiento movimiento) {
    MovimientoPDOutputDTO movimientoPDOutputDTO = new MovimientoPDOutputDTO();
    if (movimiento != null) {
      movimientoPDOutputDTO.setFechaValor(movimiento.getFechaValor());
      movimientoPDOutputDTO.setContrato(movimiento.getContrato() != null ? movimiento.getContrato().getIdCarga() : null);
      movimientoPDOutputDTO.setProducto(movimiento.getContrato() != null ? movimiento.getContrato().getProducto() != null ? new CatalogoMinInfoDTO(movimiento.getContrato().getProducto()) : null : null);
      movimientoPDOutputDTO.setImporte(String.valueOf(movimiento.getImporte()));
      movimientoPDOutputDTO.setPago(String.valueOf(movimiento.getId()));
    }
    return movimientoPDOutputDTO;
  }

  public MovimientoPDOutputDTO parseToMovimientoPDOutputDTO(MovimientoOutputDto movimiento) {
    MovimientoPDOutputDTO movimientoPDOutputDTO = new MovimientoPDOutputDTO();
    if (movimiento != null) {
      movimientoPDOutputDTO.setFechaValor(movimiento.getFechaValor());
      movimientoPDOutputDTO.setContrato(movimiento.getIdCarga());
      movimientoPDOutputDTO.setProducto(movimiento.getProducto());
      movimientoPDOutputDTO.setImporte(String.valueOf(movimiento.getImporte()));
      movimientoPDOutputDTO.setPago(String.valueOf(movimiento.getId()));
    }
    return movimientoPDOutputDTO;
  }
}
