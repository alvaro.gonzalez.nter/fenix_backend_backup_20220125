package com.haya.alaska.integraciones.portal_deudor.application;

import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoFirmaDto;
import com.haya.alaska.integraciones.gd.infraestructure.controller.dto.GestorDocumentalDto;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto.*;
import com.haya.alaska.shared.exceptions.ExternalApiErrorException;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface PortalDeudorUseCase {

  IntervinienteDeudorDTO findInformacionIntervinienteDeudor(
      Integer idInterviniente, Integer idCliente) throws NotFoundException;

  List<IntervinienteAcuerdoPagoDTO> findInformacionPlanesPago(
      Integer idInterviniente, Integer idCliente) throws NotFoundException;

  List<IntervinienteDeudorContratoDTO> findInformacionContratoDeudor(
      Integer idInterviniente, Integer idCliente) throws NotFoundException;

  IntervinientePDOutputDTO getInterviniente(Integer id) throws NotFoundException;

  Boolean actualizarInterviniente(Integer id, Usuario creador, IntervinientePDInputDTO input)
      throws NotFoundException, RequiredValueException;

  Boolean actualizarDatoContacto(Integer id, Usuario creador, DatoContactoPDDTO input)
      throws NotFoundException, RequiredValueException;

  Boolean crearLlamada(Integer id, Usuario creador, String fecha, String telefono)
      throws com.haya.alaska.shared.exceptions.NotFoundException, RequiredValueException;

  String crearUsuario(String dni, Usuario creador, RegistroUsuarioDTO user, MultipartFile file)
      throws Exception;

  MovimientosPDOutputDTO findMovimientosByIdInterviniente(
      Integer idInterviniente, Integer idCliente) throws IllegalAccessException, NotFoundException;

  Integer eventoRevisionDeudor(
      Integer intervinienteId, Integer gestorId, Integer idEvento, String comentario)
      throws NotFoundException, RequiredValueException, InvalidCodeException, IOException;

  byte[] descargaDocumento(Integer id) throws IOException;

  GestorDocumentalDto obtenerDocumento(Integer id) throws NotFoundException;

  void guardarDocumento(GestorDocumentalDto inputDto) throws NotFoundException;

  List<DocumentoFirmaDto> listarDocumentosFirma(
      Integer idInterviniente, Integer idCliente, MatriculasEnum tipoDocumento, Date fechaInicio)
      throws NotFoundException, IOException, ExternalApiErrorException;
}
