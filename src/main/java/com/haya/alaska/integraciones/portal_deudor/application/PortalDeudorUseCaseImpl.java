package com.haya.alaska.integraciones.portal_deudor.application;

import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoFirmaDto;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import com.haya.alaska.estado_evento.infrastructure.repository.EstadoEventoRepository;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoOutputDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.importancia_evento.domain.ImportanciaEvento;
import com.haya.alaska.importancia_evento.infrastructure.repository.ImportanciaEventoRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.domain.enums.EstadoDocumentoEnum;
import com.haya.alaska.integraciones.gd.infraestructure.controller.dto.GestorDocumentalDto;
import com.haya.alaska.integraciones.gd.infraestructure.mapper.GestorDocumentalMapper;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.integraciones.gd.model.respuesta.RespuestaDescargaDocumento;
import com.haya.alaska.integraciones.portal_deudor.client.PortalDeudorFeignClient;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto.*;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.mapper.PortalDeudorMapper;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.util.PortalDeudorUtil;
import com.haya.alaska.integraciones.solicitud_firma.application.SolicitudFirmaUseCase;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.infrastructure.mapper.MovimientoMapper;
import com.haya.alaska.movimiento.infrastructure.repository.MovimientoRepository;
import com.haya.alaska.nivel_evento.domain.NivelEvento;
import com.haya.alaska.nivel_evento.infrastructure.repository.NivelEventoRepository;
import com.haya.alaska.periodicidad_evento.infrastructure.repository.PeriodicidadEventoRepository;
import com.haya.alaska.resultado_evento.infrastructure.repository.ResultadoEventoRepository;
import com.haya.alaska.shared.dto.MetadatoContenedorInputDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.ExternalApiErrorException;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class PortalDeudorUseCaseImpl implements PortalDeudorUseCase {
  @Autowired PortalDeudorUtil portalDeudorUtil;
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired ContratoRepository contratoRepository;
  @Autowired PortalDeudorMapper portalDeudorMapper;
  @Autowired DatoContactoRepository datoContactoRepository;
  @Autowired ClienteRepository clienteRepository;
  @Autowired CarteraRepository carteraRepository;
  @Autowired PortalDeudorFeignClient portalDeudorFeignClient;
  @Autowired ServicioGestorDocumental servicioGestorDocumental;
  @Autowired GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired ExpedienteRepository expedienteRepository;

  // Ivan, tu escribes tus autowired arriba de este comentario
  @Autowired EventoUseCase eventoUseCase;
  @Autowired SubtipoEventoRepository subtipoEventoRepository;
  @Autowired PeriodicidadEventoRepository periodicidadEventoRepository;
  @Autowired EstadoEventoRepository estadoEventoRepository;
  @Autowired ImportanciaEventoRepository importanciaEventoRepository;
  @Autowired ResultadoEventoRepository resultadoEventoRepository;
  @Autowired NivelEventoRepository nivelEventoRepository;
  @Autowired UsuarioRepository usuarioRepository;
  @Autowired MovimientoRepository movimientoRepository;
  @Autowired MovimientoMapper movimientoMapper;
  @Autowired SolicitudFirmaUseCase solicitudFirmaUseCase;

  // Arriba los autowired, abajo los métodos
  @Override
  public IntervinienteDeudorDTO findInformacionIntervinienteDeudor(
      Integer idInterviniente, Integer idCliente) throws NotFoundException {
    Interviniente interviniente = this.obtenerInterviniente(idInterviniente);

    List<Cartera> carteraList = carteraRepository.findAllByClientesClienteId(idCliente);
    List<Contrato> contratoList = new ArrayList<>();
    for (Cartera cartera : carteraList) {
      List<Contrato> contratoList1 = portalDeudorUtil.getContratos(interviniente, cartera);
      contratoList.addAll(contratoList1);
    }
    Double pagosEfectuados = 0.0;
    Double deudaPendiente = 0.0;
    IntervinienteDeudorDTO intervinienteDeudorDTO = new IntervinienteDeudorDTO();
    intervinienteDeudorDTO.setNContratos(contratoList.size());
    for (Contrato contrato : contratoList) {
      List<Movimiento> movimientoList =
          contrato.getMovimientos().stream().collect(Collectors.toList());
      if (contrato.getUltimoSaldo() != null) {
        Double deuda = contrato.getUltimoSaldo().getSaldoGestion();
        deudaPendiente += deuda;
      }
      for (Movimiento movimiento : movimientoList) {
        if (movimiento.getContabilidad() != null
            && !movimiento.getContabilidad()
            && movimiento.getTipo().getValor().equals("Abono")
            && movimiento.getSentido().equals("+")) pagosEfectuados += movimiento.getImporte();
      }
    }
    intervinienteDeudorDTO.setPagosEfectuados(pagosEfectuados);
    intervinienteDeudorDTO.setDeudaPendiente(deudaPendiente);
    return intervinienteDeudorDTO;
  }

  public List<IntervinienteAcuerdoPagoDTO> findInformacionPlanesPago(
      Integer idInterviniente, Integer idCliente) throws NotFoundException {
    Interviniente interviniente = this.obtenerInterviniente(idInterviniente);

    List<Cartera> carteraList = carteraRepository.findAllByClientesClienteId(idCliente);
    List<AcuerdoPago> acuerdoPagoList = new ArrayList<>();
    for (Cartera cartera : carteraList) {
      List<AcuerdoPago> acuerdoPagoList1 = portalDeudorUtil.getAcuerdos(interviniente, cartera);
      acuerdoPagoList.addAll(acuerdoPagoList1);
    }
    List<IntervinienteAcuerdoPagoDTO> intervinienteAcuerdoPagoDTOList = new ArrayList<>();
    for (AcuerdoPago acuerdoPago : acuerdoPagoList) {
      IntervinienteAcuerdoPagoDTO intervinienteAcuerdoPagoDTO =
          portalDeudorMapper.parseToIntervinienteAcuerdoDTO(acuerdoPago);
      intervinienteAcuerdoPagoDTOList.add(intervinienteAcuerdoPagoDTO);
    }
    return intervinienteAcuerdoPagoDTOList;
  }

  @Override
  public List<IntervinienteDeudorContratoDTO> findInformacionContratoDeudor(
      Integer idInterviniente, Integer idCliente) throws NotFoundException {
    Interviniente interviniente = this.obtenerInterviniente(idInterviniente);

    List<Cartera> carteraList = carteraRepository.findAllByClientesClienteId(idCliente);
    List<Contrato> contratoList = new ArrayList<>();
    for (Cartera cartera : carteraList) {
      List<Contrato> contratoList1 = portalDeudorUtil.getContratos(interviniente, cartera);
      contratoList.addAll(contratoList1);
    }
    List<IntervinienteDeudorContratoDTO> intervinienteDeudorContratoDTOList = new ArrayList<>();
    for (Contrato contrato : contratoList) {
      IntervinienteDeudorContratoDTO intervinienteDeudorContratoDTO =
          portalDeudorMapper.parseToInformacionDeudorContratoDTO(
              contrato, interviniente, idCliente);
      intervinienteDeudorContratoDTOList.add(intervinienteDeudorContratoDTO);
    }
    return intervinienteDeudorContratoDTOList;
  }

  // Destinatario es el gestor
  @Override
  public Integer eventoRevisionDeudor(
      Integer intervinienteId, Integer gestorId, Integer idEvento, String comentario)
      throws NotFoundException, RequiredValueException, InvalidCodeException, IOException {
    EventoInputDTO newInput = getInputRevisionDeudor();

    Integer nivel;
    Integer idCartera;
    nivel = 4;
    Usuario logged =
        usuarioRepository
            .findById(gestorId)
            .orElseThrow(() -> new NotFoundException("usuario", gestorId));

    Interviniente interviniente = this.obtenerInterviniente(intervinienteId);

    // Pillo el primero
    List<Expediente> expedienteList = portalDeudorUtil.getExpedientes(interviniente, null);
    Expediente expediente = expedienteList.get(0);
    Cartera ca = expediente.getCartera();

    // El niveliD es el nivel del interviniente y el nivel es el 4
    newInput.setNivel(nivel);
    newInput.setCartera(ca.getId());
    newInput.setNivelId(intervinienteId);
    newInput.setIdExpediente(expediente.getId());

    // Necesito la cartera para el subtipoEvento están duplicados por cartera coger el de mi
    // cartera PDN
    SubtipoEvento se =
        subtipoEventoRepository
            .findByCodigoAndCarteraId("PD_SD", ca.getId())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "subtipoEvento", "código", "'PD_SD'", "cartera", ca.getId()));
    newInput.setSubtipoEvento(se.getId());
    TipoEvento te = se.getTipoEvento();
    newInput.setTipoEvento(te.getId());
    ClaseEvento ce = te.getClaseEvento();
    newInput.setClaseEvento(ce.getId());
    newInput.setDestinatario(gestorId);

    String descripcion = "";
    descripcion += "Documentacion del usuario deudor con id: ";
    descripcion += interviniente.getNumeroDocumento();
    newInput.setComentariosDestinatario(descripcion);

    EventoOutputDTO output;
    if (idEvento == null) {
      output = eventoUseCase.create(newInput, logged);
    } else {
      output = eventoUseCase.update(idEvento, newInput, logged);
    }
    return output.getId();
  }

  private EventoInputDTO getInputRevisionDeudor() throws NotFoundException {
    EventoInputDTO newInput = new EventoInputDTO();

    newInput.setTitulo("Envío documentacion desde portal Deudor");

    newInput.setImportancia(2); // Media importancia
    newInput.setPeriodicidad(null);
    // Revisamos el estado evento pendiente
    EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("PEN").orElse(null);
    if (estadoEvento == null)
      throw new NotFoundException("estado evento", "código", "'PEN'");
    newInput.setEstado(estadoEvento.getId());
    newInput.setResultado(null);

    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.YEAR, 100);
    newInput.setFechaLimite(c.getTime());

    newInput.setFechaAlerta(new Date());
    newInput.setProgramacion(false);
    newInput.setCorreo(false);
    newInput.setAutogenerado(false);
    newInput.setRevisado(false);

    return newInput;
  }

  // Ivan, tu escribes tus métodos arriba de este comentario

  @Override
  public IntervinientePDOutputDTO getInterviniente(Integer id) throws NotFoundException {
    Interviniente i = this.obtenerInterviniente(id);

    IntervinientePDOutputDTO result = portalDeudorMapper.createOutput(i);
    return result;
  }

  @Override
  public Boolean actualizarInterviniente(Integer id, Usuario creador, IntervinientePDInputDTO input)
      throws NotFoundException, RequiredValueException {
    Interviniente i = this.obtenerInterviniente(id);

    Boolean b = portalDeudorMapper.actualizarInterviniente(i, input);
    Interviniente iSaved = intervinienteRepository.save(i);
    if (b) crearAlerta(iSaved, creador);
    return b;
  }

  @Override
  public Boolean actualizarDatoContacto(Integer id, Usuario creador, DatoContactoPDDTO input)
      throws NotFoundException, RequiredValueException {
    Interviniente i = this.obtenerInterviniente(id);

    DatoContacto dc = new DatoContacto();
    // Si se ha cambiado algo se envia la alerta //Si quieren que
    Boolean b = portalDeudorMapper.actualizarDC(i, dc, input);
    if (b) crearAlerta(i, creador);
    return b;
  }

  @Override
  public Boolean crearLlamada(Integer id, Usuario creador, String fechaS, String telefono)
      throws NotFoundException, RequiredValueException {
    Interviniente i = this.obtenerInterviniente(id);

    Date fecha = new Date(fechaS);

    String nombreResult = "";
    String nombre = i.getNombre() != null ? i.getNombre() : "";
    String apellidos = i.getApellidos() != null ? i.getApellidos() : "";
    if (i.getPersonaJuridica() != null && i.getPersonaJuridica()) {
      nombreResult = i.getRazonSocial() != null ? i.getRazonSocial() : "";
    } else {
      nombreResult = nombre + " " + apellidos;
    }

    String titulo = "Alerta de llamada de " + nombreResult;
    String des =
        "El interviniente "
            + nombreResult
            + " a solicitado una llamada a fecha: "
            + fecha
            + ". Teléfono: "
            + telefono;
    String subtipo = "PD_LA";
    String estado = "PEN";

    crearEvento(i, creador.getId(), fecha, fecha, titulo, des, subtipo, estado);
    return true;
  }

  // El user es el que solicita acceder al portal deudor
  public String crearUsuario(
      String dni, Usuario creador, RegistroUsuarioDTO user, MultipartFile file) throws Exception {
    Interviniente i = intervinienteRepository.findByNumeroDocumento(dni).orElse(null);
    if (i == null) return "No se ha encontrado un interviniente con dni: " + dni;

    Usuario usuario = usuarioRepository.findById(creador.getId()).orElse(null);
    String nombreFichero = file.getOriginalFilename();

    // EN-01-DOCI-01 hace referencia al DNI
    // Proceso de subida de DNI al gestor Documental
    try {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
          new MetadatoContenedorInputDTO(i.getIdHaya(), "");
      this.servicioGestorDocumental.procesarContenedorDeudor(
          CodigoEntidad.DEUDOR, metadatoContenedorInputDTO);
      MetadatoDocumentoInputDTO metadatoDocumento =
          new MetadatoDocumentoInputDTO(nombreFichero, "EN-01-DOCI-01");


     /* MetadatoContenedorInputDTO metadatoContenedorInputDTO = new MetadatoContenedorInputDTO(nombreCliente,"");
      this.servicioGestorDocumental.procesarContenedorCartera(CodigoEntidad.DEUDOR, metadatoContenedorInputDTO);
      long id = servicioGestorDocumental.procesarDocumento(CodigoEntidad.DEUDOR,nombreCliente, file, metadatoDocumento).getIdDocumento();*/


      long id =
          servicioGestorDocumental
              .procesarDocumento(CodigoEntidad.INTERVINIENTE, i.getIdHaya(), file, metadatoDocumento)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental = new GestorDocumental(idgestorDocumental, creador);
      gestorDocumentalRepository.save(gestorDocumental);
      crearAlertaCreacion(i, creador, user.getMail(), Integer.parseInt(user.getPhoneNumber()));
      // TODO guardar el dni el archivo //el email se le envía al interviniente
      return null;
    } catch (Exception e) {
      MetadatoDocumentoInputDTO metadatoDocumento =
          new MetadatoDocumentoInputDTO(nombreFichero, "EN-01-DOCI-01");
      long id =
          servicioGestorDocumental
              .procesarDocumento(CodigoEntidad.INTERVINIENTE, i.getIdHaya(), file, metadatoDocumento)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental = new GestorDocumental(idgestorDocumental, creador);
      gestorDocumentalRepository.save(gestorDocumental);
      crearAlertaCreacion(i, creador, user.getMail(), Integer.parseInt(user.getPhoneNumber()));
      return null;
    }
  }

  // Crear otro metodo en el que llame a este donde pase los documentos para que se quede almacenada
  // la imagen del dni
  private void crearAlertaCreacion(
      Interviniente i, Usuario creador, String userName, Integer phoneNumber)
      throws NotFoundException, RequiredValueException {
    String nombreResult = "";
    String nombre = i.getNombre() != null ? i.getNombre() : "";
    String apellidos = i.getApellidos() != null ? i.getApellidos() : "";
    if (i.getPersonaJuridica() != null && i.getPersonaJuridica()) {
      nombreResult = i.getRazonSocial() != null ? i.getRazonSocial() : "";
    } else {
      nombreResult = nombre + " " + apellidos;
    }

    String titulo =
        "Solicitud de Usuario Portal Deudor: "
            + userName
            + " con el numero de telefono de: "
            + phoneNumber;
    String des =
        "El interviniente " + nombreResult + " a solicitado una cuenta en el Portal Deudor";
    String subtipo = "PD_NDC";
    String estado = "PEN";

    Date now = Calendar.getInstance().getTime();
    Calendar c = Calendar.getInstance();
    c.setTime(now);
    c.add(Calendar.DATE, 1);
    c.add(Calendar.YEAR, 100);

    crearEvento(i, creador.getId(), c.getTime(), now, titulo, des, subtipo, estado);
  }

  private void crearAlerta(Interviniente i, Usuario creador)
      throws NotFoundException, RequiredValueException {
    String nombreResult = "";
    String nombre = i.getNombre() != null ? i.getNombre() : "";
    String apellidos = i.getApellidos() != null ? i.getApellidos() : "";
    if (i.getPersonaJuridica() != null && i.getPersonaJuridica()) {
      nombreResult = i.getRazonSocial() != null ? i.getRazonSocial() : "";
    } else {
      nombreResult = nombre + " " + apellidos;
    }

    String titulo = "Nuevos datos de contacto";
    String des = "Se han modificado los datos de contacto del interviniente " + nombreResult;
    String subtipo = "PD_NDC";
    String estado = "PEN";

    crearEvento(i, creador.getId(), new Date(), new Date(), titulo, des, subtipo, estado);
  }

  private void crearEvento(
      Interviniente i,
      Integer idEmisor,
      Date fechaLimite,
      Date fechaAlerta,
      String titulo,
      String des,
      String subtipo,
      String estado)
      throws NotFoundException, RequiredValueException {
    EventoInputDTO newIn = new EventoInputDTO();

    List<Expediente> expedientes = portalDeudorUtil.getExpedientes(i, null);

    ImportanciaEvento ie =
        importanciaEventoRepository
            .findByCodigo("ALT")
            .orElseThrow(
                () -> new NotFoundException("importanciaEvento", "código", "'ALT'"));
    Usuario creador =
        usuarioRepository
            .findById(idEmisor)
            .orElseThrow(() -> new NotFoundException("Usuario", idEmisor));

    newIn.setTitulo(titulo);
    newIn.setComentariosDestinatario(des);

    NivelEvento ne =
        nivelEventoRepository
            .findByCodigo("INTE")
            .orElseThrow(
                () -> new NotFoundException("Nivel Evento", "código", "'INTE'"));
    EstadoEvento ee =
        estadoEventoRepository
            .findByCodigo(estado)
            .orElseThrow(
                () ->
                    new NotFoundException(
                            "Estado Evento", "código", "'" + estado + "'"));

    newIn.setFechaCreacion(new Date());
    newIn.setFechaAlerta(fechaAlerta);
    newIn.setFechaLimite(fechaLimite);

    newIn.setEstado(ee.getId());

    newIn.setImportancia(ie.getId());
    newIn.setAutogenerado(true);

    newIn.setNivel(ne.getId());
    newIn.setNivelId(i.getId());

    for (Expediente ex : expedientes) {
      Cartera ca = ex.getCartera();
      SubtipoEvento se =
          subtipoEventoRepository
              .findByCodigoAndCarteraId(subtipo, ex.getCartera().getId())
              .orElseThrow(
                  () ->
                      new NotFoundException(
                              "subtipoEvento", "código", "'" + subtipo + "'"));
      TipoEvento te = se.getTipoEvento();
      ClaseEvento ce = te.getClaseEvento();

      Usuario dest = ex.getGestor();
      if (dest == null) dest = ex.getUsuarioByPerfil("Responsable de cartera");
      if (dest == null)
        throw new NotFoundException(
            "Gestor ni Responsable", "expediente", ex.getId());
      //Destinatario debe haber siempre comprobar el boolean
      newIn.setDestinatario(dest.getId());
      newIn.setCartera(ca.getId());
      newIn.setIdExpediente(ex.getId());

      newIn.setClaseEvento(ce.getId());
      newIn.setTipoEvento(te.getId());
      newIn.setSubtipoEvento(se.getId());

      eventoUseCase.create(newIn, creador);
    }
  }

  @Override
  public MovimientosPDOutputDTO findMovimientosByIdInterviniente(
      Integer idInterviniente, Integer idCliente) throws NotFoundException {
    //  List<Movimiento> movimientos =
    // movimientoRepository.findAllByContratoIntervinientesIntervinienteIdAndContabilidadNotAndSentidoEquals(idInterviniente, true, "-");
    Interviniente interviniente = this.obtenerInterviniente(idInterviniente);

    List<Cartera> carteraList = carteraRepository.findAllByClientesClienteId(idCliente);
    List<Contrato> contratoList = new ArrayList<>();
    List<MovimientoPDOutputDTO> movimientoPDOutputDTOList = new ArrayList<>();
    for (Cartera cartera : carteraList) {
      List<Contrato> contratoList1 = portalDeudorUtil.getContratos(interviniente, cartera);
      contratoList.addAll(contratoList1);
    }
    Double pagosEfectuados = 0.0;
    // De los contratos sacamos una lista de movimientos
    for (Contrato contrato : contratoList) {
      List<Movimiento> movimientoList =
          contrato.getMovimientos().stream().collect(Collectors.toList());
      for (Movimiento movimiento : movimientoList) {
        if (movimiento.getContabilidad() != null
            && !movimiento.getContabilidad()
            && movimiento.getSentido().equals("+")
            && movimiento.getTipo().getValor().equals("Abono")) {
          MovimientoPDOutputDTO movimientoPDOutputDTO =
              portalDeudorMapper.parseToMovimientoPDOutputDTO(movimiento);
          movimientoPDOutputDTOList.add(movimientoPDOutputDTO);
          pagosEfectuados += movimiento.getImporte();
        }
      }
    }
    MovimientosPDOutputDTO movimientoPDOutputDTO = new MovimientosPDOutputDTO();
    movimientoPDOutputDTO.setImporteTotal(pagosEfectuados);
    movimientoPDOutputDTO.setMovimientos(movimientoPDOutputDTOList);

    return movimientoPDOutputDTO;
  }

  @Override
  public byte[] descargaDocumento(Integer id) throws IOException {
    RespuestaDescargaDocumento respuestaDescargaDocumento =
        servicioGestorDocumental.descargarDocumento(id);

    return IOUtils.toByteArray(respuestaDescargaDocumento.getBody());
  }

  @Override
  public GestorDocumentalDto obtenerDocumento(Integer id) throws NotFoundException {
    GestorDocumental documento =
        gestorDocumentalRepository
            .findById(id)
            .orElseThrow(
                () -> new NotFoundException("documento", id));

    return GestorDocumentalMapper.INSTANCE.toGestorDocumentalDto(documento);
  }

  @Override
  public void guardarDocumento(GestorDocumentalDto inputDto) throws NotFoundException {

    Usuario usuario = obtenerUsuario(inputDto.getIdUsuario());
    Usuario usuarioDestino = obtenerUsuario(inputDto.getIdUsuarioDestino());

    GestorDocumental documento =
        GestorDocumental.builder()
            .id(inputDto.getId())
            .usuario(usuario)
            .usuarioDestino(usuarioDestino)
            .fechaActualizacion(inputDto.getFechaActualizacion())
            .fechaGeneracion(inputDto.getFechaGeneracion())
            .nombreDocumento(inputDto.getNombreDocumento())
            .tipoDocumento(inputDto.getTipoDocumento())
            .tipoModeloCartera(inputDto.getTipoModeloCartera())
            .fechaFirma(inputDto.getFechaFirma())
            .estado(EstadoDocumentoEnum.valueOf(inputDto.getEstado()))
            .comentario(inputDto.getComentario())
            .build();

    gestorDocumentalRepository.save(documento);
  }

  @Override
  public List<DocumentoFirmaDto> listarDocumentosFirma(
      Integer idInterviniente, Integer idCliente, MatriculasEnum tipoDocumento, Date fechaInicio)
      throws NotFoundException, IOException, ExternalApiErrorException {
    Interviniente interviniente = this.obtenerInterviniente(idInterviniente);

    this.solicitudFirmaUseCase.actualizarEstadoSolicitudesEnCurso();

    List<GestorDocumental> list;
    if (fechaInicio != null) {
      list =
          gestorDocumentalRepository
              .findAllByIdHayaAndClienteDeudorIdAndTipoDocumentoAndFechaGeneracionAfter(
                  interviniente.getIdHaya(), idCliente, tipoDocumento, fechaInicio);
    } else {
      list =
          gestorDocumentalRepository.findAllByIdHayaAndClienteDeudorIdAndTipoDocumento(
              interviniente.getIdHaya(), idCliente, tipoDocumento);
    }

    return list.stream()
        .map(GestorDocumentalMapper.INSTANCE::toDocumentoFirmaDto)
        .collect(Collectors.toList());
  }

  private Usuario obtenerUsuario(Integer id) throws NotFoundException {
    if (id == null) return null;
    return usuarioRepository.findById(id).orElseThrow(() -> new NotFoundException("Usuario", id));
  }

  private Interviniente obtenerInterviniente(Integer idInterviniente) throws NotFoundException {
    return intervinienteRepository
        .findById(idInterviniente)
        .orElseThrow(
            () ->
                new NotFoundException(
                    "Interviniente"
                        , idInterviniente));
  }
}
