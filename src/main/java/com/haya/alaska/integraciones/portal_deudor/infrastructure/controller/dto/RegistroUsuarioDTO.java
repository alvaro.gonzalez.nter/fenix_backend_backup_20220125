package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistroUsuarioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private String lastName;

    private String documentId;

    private String mail;

    private String phoneNumber;

    private Boolean checkPrivacity;

    private Boolean checkPublicity;

}
