package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MovimientosPDOutputDTO implements Serializable {
  private Double importeTotal;
  private List<MovimientoPDOutputDTO> movimientos;
}
