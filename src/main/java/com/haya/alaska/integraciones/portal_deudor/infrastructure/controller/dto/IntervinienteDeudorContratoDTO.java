package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class IntervinienteDeudorContratoDTO implements Serializable {
  Integer idInterviniente;
  Double deuda;
  String contrato;
  String producto;
  String cliente;
}
