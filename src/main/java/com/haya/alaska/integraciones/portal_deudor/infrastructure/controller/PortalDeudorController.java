package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller;

import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoFirmaDto;
import com.haya.alaska.integraciones.gd.infraestructure.controller.dto.GestorDocumentalDto;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.integraciones.portal_deudor.application.PortalDeudorUseCase;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto.*;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.ExternalApiErrorException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/v1/portalDeudor")
public class PortalDeudorController {
  @Autowired PortalDeudorUseCase portalDeudorUseCase;
  // Ivan, tu escribes tus autowired arriba de este comentario

  // Arriba los autowired, abajo los endpoints
  @ApiOperation(
      value = "Listado informacion interviniente deudor",
      notes = "Devuelve informacion del cliente deudor")
  @GetMapping("/informacionClienteDeudor")
  public IntervinienteDeudorDTO findClienteDeudor(
      @RequestParam(required = false) Integer idInterviniente, @RequestParam Integer idCliente)
      throws NotFoundException {
    return portalDeudorUseCase.findInformacionIntervinienteDeudor(idInterviniente, idCliente);
  }

  @ApiOperation(
      value = "Listado acuerdos pago por interviniente",
      notes = "Devuelve todos los acuerdos pago por interviniente")
  @GetMapping("/acuerdosPago-Interviniente")
  public List<IntervinienteAcuerdoPagoDTO> findAcuerdosPagoInterviniente(
      @RequestParam(required = false) Integer idInterviniente, @RequestParam Integer idCliente)
      throws NotFoundException {
    return portalDeudorUseCase.findInformacionPlanesPago(idInterviniente, idCliente);
  }

  @ApiOperation(
      value = "Listado contratos del deudor",
      notes = "Devuelve informacion de los contratos del deudor")
  @GetMapping("/informacionContratosDeudor")
  public List<IntervinienteDeudorContratoDTO> findInformacionContratoDeudor(
      @RequestParam(required = false) Integer idInterviniente, @RequestParam Integer idCliente)
      throws NotFoundException {
    return portalDeudorUseCase.findInformacionContratoDeudor(idInterviniente, idCliente);
  }

  // Ivan, tu escribes tus endpoints arriba de este comentario
  @ApiOperation(value = "Crea una llamada", notes = "Crea un evento de llamada a los gestores del ")
  @PostMapping("/llamada")
  public Boolean crearLlamada(
      @RequestParam(value = "intervinienteId") Integer intervinienteId,
      @RequestParam(value = "fecha") String fecha,
      @RequestParam(value = "telefono") String telefono,
      @ApiIgnore CustomUserDetails principal)
      throws NotFoundException, RequiredValueException {
    Usuario creador = (Usuario) principal.getPrincipal();
    return portalDeudorUseCase.crearLlamada(intervinienteId, creador, fecha, telefono);
  }

  @ApiOperation(value = "Obtener interviniente", notes = "Devuelve el interviniente con id enviado")
  @GetMapping("/{id}")
  public IntervinientePDOutputDTO getIntervinienteById(@PathVariable("id") Integer id)
      throws Exception {
    return portalDeudorUseCase.getInterviniente(id);
  }

  @ApiOperation(
      value = "Actualiza el interviniente",
      notes = "Actualiza los datos del interviniente")
  @PutMapping("/{id}")
  public Boolean actualizarInterviniente(
      @PathVariable("id") Integer id,
      @RequestBody @Valid IntervinientePDInputDTO input,
      @ApiIgnore CustomUserDetails principal)
      throws NotFoundException, RequiredValueException {
    Usuario creador = (Usuario) principal.getPrincipal();
    return portalDeudorUseCase.actualizarInterviniente(id, creador, input);
  }

  @ApiOperation(value = "Actualiza el datoContacto", notes = "Actualiza los datos del datoContacto")
  @PutMapping("/{id}/datoContacto")
  public Boolean actualizarDatoContacto(
      @PathVariable("id") Integer id,
      @RequestBody @Valid DatoContactoPDDTO input,
      @ApiIgnore CustomUserDetails principal)
      throws NotFoundException, RequiredValueException {
    Usuario creador = (Usuario) principal.getPrincipal();
    return portalDeudorUseCase.actualizarDatoContacto(id, creador, input);
  }

  @ApiOperation(
      value = "Obtener movimientos por interviniente",
      notes = "Devuelve los movimientos con id interviniente enviado")
  @GetMapping("/{idInterviniente}/movimientos")
  public MovimientosPDOutputDTO getMovimientosByIdInterviniente(
      @PathVariable("idInterviniente") Integer idInterviniente, @RequestParam Integer idCliente)
      throws IllegalAccessException, NotFoundException {
    return portalDeudorUseCase.findMovimientosByIdInterviniente(idInterviniente, idCliente);
  }

  // Tengo que añadir el archivo file de imagen del dni
  @PostMapping(value = "/crearUsuario")
  public String crearUsuario(
      @ApiIgnore CustomUserDetails principal,
      @RequestParam(required = false) String dni,
      RegistroUsuarioDTO user,
      MultipartFile file)
      throws Exception {
    Usuario creador = (Usuario) principal.getPrincipal();
    return portalDeudorUseCase.crearUsuario(dni, creador, user, file);
  }

  @ApiOperation(
      value = "Obtener bytes documento",
      notes = "Obtiene la informacion de bytes de un documento del gestor documental")
  @GetMapping("descargaDocumento/{id}")
  public byte[] descargarDocumento(@PathVariable Integer id) throws IOException {
    return portalDeudorUseCase.descargaDocumento(id);
  }

  @ApiOperation(
      value = "Obtener documento",
      notes = "Obtiene la informacion de un documento del gestor documental")
  @GetMapping("documento/{id}")
  public GestorDocumentalDto obtenerDocumento(@PathVariable Integer id) throws NotFoundException {
    return portalDeudorUseCase.obtenerDocumento(id);
  }

  @ApiOperation(value = "Guardar documento", notes = "Guarda el documento en BD")
  @PostMapping("documento")
  public ResponseEntity<HttpStatus> guardarDocumento(@RequestBody GestorDocumentalDto inputDto)
      throws NotFoundException {
    portalDeudorUseCase.guardarDocumento(inputDto);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @ApiOperation(
      value = "Listar documentos firma",
      notes = "Lista los documentos del interviniente firmados y ha firmar")
  @GetMapping("documentos")
  public List<DocumentoFirmaDto> listarDocumentosFirma(
      @RequestParam Integer idInterviniente,
      @RequestParam Integer idCliente,
      @RequestParam MatriculasEnum tipoDocumento,
      @RequestParam(required = false)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd")
          Date fechaInicio)
      throws NotFoundException, IOException, ExternalApiErrorException {
    return portalDeudorUseCase.listarDocumentosFirma(
        idInterviniente, idCliente, tipoDocumento, fechaInicio);
  }
}
