package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class DatoContratoPDDTO implements Serializable {
  String contrato;
  String producto;
}
