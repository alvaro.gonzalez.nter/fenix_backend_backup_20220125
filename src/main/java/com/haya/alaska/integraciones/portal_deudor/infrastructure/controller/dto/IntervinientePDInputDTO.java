package com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class IntervinientePDInputDTO implements Serializable {
  //Datos de contacto
  private String nombre;
  private String apellidos;
  //private List<DatoContactoPDDTO> contactos;
  //Domicilio y datos de pago
  private DireccionPDDTO direccion;
  //Datos personales
  private Date fechaNacimiento;
  private Integer paisNacimiento;
  private Integer nacionalidad;
  private Integer residencia;
  private String trabajo;
  private Integer estadoCivil;
}
