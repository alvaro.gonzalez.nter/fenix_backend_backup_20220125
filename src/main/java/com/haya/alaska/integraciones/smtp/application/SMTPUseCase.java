package com.haya.alaska.integraciones.smtp.application;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface SMTPUseCase {
  Boolean sendEmail(String[] destinatario, String sujeto, String mensaje, MultipartFile[] file) throws IOException;
  Boolean sendEmail(String[] destinatario, String sujeto, String mensaje, ResponseEntity<byte[]> mFile, String nombre)
    throws IOException;
}
