package com.haya.alaska.integraciones.smtp.infrastructure.controller;

import com.haya.alaska.integraciones.smtp.application.SMTPUseCase;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.io.IOException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/smtp")
public class SMTPController {
  @Autowired
  SMTPUseCase smtpUseCase;

  @ApiOperation(value = "Listado de todos los eventoes",
    notes = "Devuelve todos los eventoes registrados en la base de datos")
  @PostMapping
  public Boolean getFilteredEvents(
    @RequestParam @NotBlank String[] destinatarios,
    @RequestParam String sujeto,
    @RequestParam(value = "mensaje", required = false) String mensaje,
    @RequestPart(value = "files", required = false) MultipartFile[] files) throws IOException {

    return smtpUseCase.sendEmail(destinatarios, sujeto, mensaje, files);
  }
}
