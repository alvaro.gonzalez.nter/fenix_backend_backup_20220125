package com.haya.alaska.integraciones.smtp.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class EmailSMTUInputDTO implements Serializable {
  private String username;
  private String[] to;
  private String subject;
  private String text;
  private MultipartFile[] files;
  private ResponseEntity<byte[]> fileFormalizacion;
  private Date sendDate;
  private Boolean success;
}
