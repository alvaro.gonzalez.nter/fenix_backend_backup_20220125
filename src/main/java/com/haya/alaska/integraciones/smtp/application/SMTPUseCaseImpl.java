package com.haya.alaska.integraciones.smtp.application;

import com.haya.alaska.formalizacion.infrastructure.util.BASE64DecodedMultipartFile;
import com.haya.alaska.integraciones.smtp.infrastructure.controller.dto.EmailSMTUInputDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class SMTPUseCaseImpl implements SMTPUseCase {
  @Autowired private JavaMailSender sender;

  @Value("${spring.mail.username}")
  String from;

  @Override
  public Boolean sendEmail(String[] destinatario, String sujeto, String mensaje, MultipartFile[] mFile)
      throws IOException {
    EmailSMTUInputDTO mail = new EmailSMTUInputDTO();
    mail.setTo(destinatario);
    mail.setSubject(sujeto);
    mail.setText(mensaje);

    mail.setSendDate(new Date());
    if (mFile != null && mFile.length > 0) mail.setFiles(mFile);
    return sendEmailTool(mail, "File");
  }

  @Override
  public Boolean sendEmail(String[] destinatario, String sujeto, String mensaje, ResponseEntity<byte[]> mFile, String nombre)
      throws IOException {
    EmailSMTUInputDTO mail = new EmailSMTUInputDTO();
    mail.setTo(destinatario);
    mail.setSubject(sujeto);
    mail.setText(mensaje);

    mail.setSendDate(new Date());
    mail.setFileFormalizacion(mFile);
    return sendEmailTool(mail, nombre);
  }

  private Boolean sendEmailTool(EmailSMTUInputDTO mail, String nombre) {
    try {
      MimeMessage message = sender.createMimeMessage();
      MimeMessageHelper helper = new MimeMessageHelper(message, true);
      helper.setFrom(from);
      helper.setTo(mail.getTo());
      helper.setSubject(mail.getSubject());
      helper.setText(mail.getText(), true);
      if (mail.getFileFormalizacion() != null){
        MultipartFile mpf = new BASE64DecodedMultipartFile(mail.getFileFormalizacion().getBody());
        helper.addAttachment(
          nombre,
          mpf);
      } else if (mail.getFiles() != null)
        for (MultipartFile file : mail.getFiles()){
          helper.addAttachment(
            file.getOriginalFilename() != null
              ? StringUtils.stripAccents(file.getOriginalFilename())
              : nombre,
            file);
        }


      sender.send(message);
      mail.setSuccess(true);
      log.info("¡Correo con archivos adjuntos enviados!");
      System.out.println("¡Correo con archivos adjuntos enviados!");
    } catch (MessagingException e) {
      mail.setSuccess(false);
      log.error("¡Se produjo una excepción al enviar un mensaje con un archivo adjunto!", e);
    }
    return mail.getSuccess();
  }

  private File mpfToF(MultipartFile mFile) throws IOException {
    File file = new File("D:/a.txt");
    mFile.transferTo(file);
    return file;
  }
}
