package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import com.haya.alaska.integraciones.solicitud_firma.domain.enums.EstadoSolicitudFirmaBackupEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PeticionGetSignOutputDto {

  private Integer result;

  private List<String> msg;

  private DatosPeticionGetSignOutputDto datos;

  public boolean respuestaEsOK() {
    return this.result.equals(1);
  }

  /**
   * Comprueba si la solicitud de firma consultada está completada o no
   *
   * @return El resultado de la comprobación
   */
  public boolean estaFirmada() {
    return this.getDatos().getGlobalStatus().equals(EstadoSolicitudFirmaBackupEnum.SIGNED);
  }

  /**
   * Devuelve la ID en el Gestor Documental del Documento firmado
   *
   * @return
   */
  public Integer obtenerIdDocumentoFirmado() {
    return this.getDatos().getDocuments().get(0).getDataIDSigned();
  }

  public Date obtenerFechaFirma() {
    return this.getDatos().obtenerFechaFirmaDocumento();
  }
}
