package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DocumentsDatosPeticionGetSignOutputDto {
  @JsonProperty("DocumentName")
  private String documentName;

  @JsonProperty("DataIDSigned")
  private Integer dataIDSigned;

  @JsonProperty("DataIDPrintable")
  private Integer dataIDPrintable;

  @JsonProperty("DataIDStamp")
  private Integer dataIDStamp;

  @JsonProperty("Status")
  private StatusDocumentsDatosPeticionGetSignOutputDto status;

  public Date obtenerFechaFirma() {
    return this.getStatus().obtenerFechaFirma();
  }
}
