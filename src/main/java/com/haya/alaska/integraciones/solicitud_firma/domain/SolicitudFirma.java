package com.haya.alaska.integraciones.solicitud_firma.domain;

import com.haya.alaska.integraciones.solicitud_firma.domain.enums.EstadoSolicitudFirmaEnum;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_SOLICITUD_FIRMA")
@Entity
public class SolicitudFirma {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "DES_ID")
  private Integer id;

  @Column(name = "FECHA_CREACION")
  private Date fechaCreacion;

  @Column(name = "FECHA_VENCIMIENTO")
  private Date fechaVencimiento;

  @Column(name = "FECHA_CONCLUIDO")
  private Date fechaConcluido;

  @Enumerated(value = EnumType.STRING)
  @Column(name = "ENUM_ESTADO")
  private EstadoSolicitudFirmaEnum estado;

  @Column(name = "ID_PETICION_BACKUP")
  private Integer idPeticion;

  @Column(name = "ID_DOCUMENTO")
  private Integer idDocumento;

  @Column(name = "FECHA_FIRMA")
  private Date fechaFirma;

  public boolean estaEnCurso() {
    return this.getEstado().equals(EstadoSolicitudFirmaEnum.EN_CURSO);
  }
}
