package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller;

import com.haya.alaska.integraciones.solicitud_firma.application.SolicitudFirmaUseCase;
import com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto.ListIntervinienteFirmaInputDto;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.ExternalApiErrorException;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.UnfulfilledConditionException;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.text.ParseException;

@AllArgsConstructor
@RestController
@RequestMapping("api/v1/firma")
public class SolicitudFirmaController {

  private final SolicitudFirmaUseCase solicitudFirmaUseCase;

  // 8171983
  @PostMapping("/crearFirma")
  public void crearFirma(@RequestParam Integer idDocumento, @RequestParam Integer idInterviniente)
      throws IOException, UnfulfilledConditionException, ExternalApiErrorException,
          LockedChangeException, NotFoundException, ParseException {
    solicitudFirmaUseCase.iniciarSolicitudFirma(idDocumento, idInterviniente);
  }

  @PostMapping("/crearFirmaMulti")
  public void crearFirmaMulti(
      @RequestPart ListIntervinienteFirmaInputDto intervinientes,
      @RequestPart MultipartFile file,
      @ApiIgnore CustomUserDetails principal)
      throws UnfulfilledConditionException, LockedChangeException, NotFoundException, IOException,
          ExternalApiErrorException, ParseException {
    solicitudFirmaUseCase.iniciarSolicitudFirmaMulti(
        intervinientes.getIntervinientes(), file, (Usuario) principal.getPrincipal());
  }

  @PostMapping("actualizar")
  public HttpStatus actualizarSolicitudes()
      throws IOException, ExternalApiErrorException, NotFoundException {
    solicitudFirmaUseCase.actualizarEstadoSolicitudesEnCurso();

    return HttpStatus.ACCEPTED;
  }

  @GetMapping("caducar")
  public HttpStatus caducarSolicitudes() {
    solicitudFirmaUseCase.cerrarSolicitudesCaducadas();

    return HttpStatus.ACCEPTED;
  }
}
