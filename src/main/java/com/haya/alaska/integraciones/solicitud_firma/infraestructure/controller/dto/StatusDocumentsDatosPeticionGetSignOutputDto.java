package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haya.alaska.integraciones.solicitud_firma.domain.enums.EstadoSolicitudFirmaBackupEnum;
import lombok.Getter;

import java.util.Date;
import java.util.List;

@Getter
public class StatusDocumentsDatosPeticionGetSignOutputDto {
  @JsonProperty("Overall")
  public EstadoSolicitudFirmaBackupEnum overall;

  @JsonProperty("FullySigned")
  public Boolean fullySigned;

  @JsonProperty("Signatories")
  public List<SignatoriesStatusDocumentsDatosPeticionGetSignOutputDto> signatories;

  public Date obtenerFechaFirma() {
    return this.getSignatories().get(0).obtenerFechaFirma();
  }
}
