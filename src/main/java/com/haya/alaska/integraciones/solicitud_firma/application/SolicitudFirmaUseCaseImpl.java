package com.haya.alaska.integraciones.solicitud_firma.application;

import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.domain.enums.EstadoDocumentoEnum;
import com.haya.alaska.integraciones.gd.domain.enums.TipoEntidadEnum;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.integraciones.gd.model.respuesta.RespuestaDescargaDocumento;
import com.haya.alaska.integraciones.solicitud_firma.domain.SolicitudFirma;
import com.haya.alaska.integraciones.solicitud_firma.domain.enums.EstadoSolicitudFirmaEnum;
import com.haya.alaska.integraciones.solicitud_firma.infraestructure.client.FirmaDigitalClient;
import com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto.*;
import com.haya.alaska.integraciones.solicitud_firma.infraestructure.repository.SolicitudFirmaRepository;
import com.haya.alaska.interviniente.application.IntervinienteUseCase;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.shared.FechasUtil;
import com.haya.alaska.shared.TelefonosUtil;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.ExternalApiErrorException;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.UnfulfilledConditionException;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.poi.util.IOUtils;
import org.json.JSONObject;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
@Component
public class SolicitudFirmaUseCaseImpl implements SolicitudFirmaUseCase {

  // Constantes
  private static final String USER = "open@bosonit.com";
  // Repositorio principal
  private final SolicitudFirmaRepository repository;
  // Repositorios relacionados
  private final GestorDocumentalRepository gestorDocumentalRepository;
  private final IntervinienteRepository intervinienteRepository;
  private final ContratoRepository contratoRepository;
  // Servicios asociados
  private final FirmaDigitalClient firmaDigitalClient;
  private final ServicioGestorDocumental servicioGestorDocumental;

  private final IntervinienteUseCase intervinienteUseCase;

  @Override
  public void iniciarSolicitudFirmaMulti(
      List<IntervinienteFirmaInputDto> inputDto, MultipartFile file, Usuario usuario)
      throws UnfulfilledConditionException, LockedChangeException, NotFoundException, IOException,
          ExternalApiErrorException, ParseException {

    List<Integer> idsContratos =
        inputDto.stream()
            .map(IntervinienteFirmaInputDto::getIdInterviniente)
            .distinct()
            .collect(Collectors.toList());

    if (idsContratos.size() != inputDto.size()) {
      Locale loc = LocaleContextHolder.getLocale();
      String defaultLocal = loc.getLanguage();
      if (defaultLocal == null || defaultLocal.equals("en"))
        throw new UnfulfilledConditionException(
            "Only one participant with one contract can be selected, not one participant with more than 1 contract");
      else
        throw new UnfulfilledConditionException(
            "Solo se puede seleccionar un interviniente con un contrato, no un interviniente con mas de 1 contrato");
    }

    for (IntervinienteFirmaInputDto intervinienteDto : inputDto) {
      Interviniente interviniente =
          this.obtenerInterviniente(intervinienteDto.getIdInterviniente());
      Cliente cliente =
          this.obtenerContrato(intervinienteDto.getIdContrato())
              .getExpediente()
              .getCartera()
              .getCliente();

      Integer idDocumento = this.subirDocumento(interviniente, cliente, file, usuario);

      this.iniciarSolicitudFirma(idDocumento, interviniente.getId());
    }
  }

  private Integer subirDocumento(
      Interviniente interviniente, Cliente cliente, MultipartFile file, Usuario usuario)
      throws NotFoundException, IOException {
    if (interviniente.getIdHaya() == null) {
      throw new NotFoundException("Este interviniente no tiene un identificador en el Maestro");
    }

    MetadatoDocumentoInputDTO metadatoDocumento =
        new MetadatoDocumentoInputDTO(
            file.getOriginalFilename(), MatriculasEnum.GDPR.getMatricula().get(0));
    JSONObject metadatos = new JSONObject();
    JSONObject archivoFisico = new JSONObject();
    archivoFisico.put("contenedor", "CONT");
    metadatos.put("General documento", metadatoDocumento.createDescripcionGeneralDocumento());
    metadatos.put("Archivo físico", archivoFisico);

    long id =
        servicioGestorDocumental
            .crearDocumento(CodigoEntidad.INTERVINIENTE, interviniente.getIdHaya(), file, metadatos)
            .getIdDocumento();
    Integer idgestorDocumental = (int) id;
    GestorDocumental gestorDocumental =
        new GestorDocumental(
            idgestorDocumental,
            usuario,
            null,
            new Date(),
            new Date(),
            metadatoDocumento.getNombreFichero(),
            MatriculasEnum.GDPR,
            null,
            interviniente.getId().toString(),
            TipoEntidadEnum.INTERVINIENTE,
            interviniente.getIdHaya(),
            cliente);
    gestorDocumentalRepository.save(gestorDocumental);

    return idgestorDocumental;
  }

  @Override
  public void iniciarSolicitudFirma(Integer idDocumento, Integer idInterviniente)
      throws ExternalApiErrorException, IOException, UnfulfilledConditionException,
          LockedChangeException, NotFoundException, ParseException {

    this.comprobarNoExistenSolicitudesEnCurso(idDocumento);

    PeticionCreateSignOutputDto respuestaPeticion = this.crearFirma(idDocumento, idInterviniente);

    if (respuestaPeticion.estaOK()) {
      this.procesarRespuestaOK(respuestaPeticion.getIdPeticion(), idDocumento);
    } else {
      this.procesarRespuestaKO(respuestaPeticion.getMsg());
    }
  }

  private void comprobarNoExistenSolicitudesEnCurso(Integer idDocumento)
      throws LockedChangeException {
    Optional<SolicitudFirma> solicitudPreviaEnCurso =
        repository.findAllByFechaConcluidoIsNullAndIdDocumento(idDocumento);

    if (solicitudPreviaEnCurso.isPresent()) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new LockedChangeException("A signature request process has already been started for the document " + idDocumento + ".");
      else throw new LockedChangeException("Ya se ha iniciado un proceso de solicitud de firma para el documnento " + idDocumento + ". ");
    }
  }

  @Override
  public void actualizarEstadoSolicitudesEnCurso() throws NotFoundException, IOException {
    List<SolicitudFirma> solicitudesSinConcluir =
        repository.findAllByFechaConcluidoIsNull().stream()
            .filter(SolicitudFirma::estaEnCurso)
            .collect(Collectors.toList());

    for (SolicitudFirma solicitud : solicitudesSinConcluir) {
      PeticionGetSignOutputDto respuestaPeticion =
          this.consultarEstadoSolicitud(solicitud.getIdPeticion());

      if (respuestaPeticion.estaFirmada()) {
        Date fechaFirma = respuestaPeticion.obtenerFechaFirma();
        solicitud.setFechaFirma(fechaFirma);
        solicitud.setFechaConcluido(fechaFirma);
        solicitud.setEstado(EstadoSolicitudFirmaEnum.COMPLETADA);
        repository.save(solicitud);

        Integer idDocumentoFirmado = respuestaPeticion.obtenerIdDocumentoFirmado();

        this.completarSolicitud(solicitud, idDocumentoFirmado, fechaFirma);
      }
    }
  }

  private void completarSolicitud(
      SolicitudFirma solicitud, Integer idDocumentoFirmado, Date fechaFirma)
      throws NotFoundException, IOException {
    GestorDocumental documento = this.obtenerDocumento(solicitud.getIdDocumento());

    servicioGestorDocumental.eliminarDocumentoUsuarios(solicitud.getIdDocumento());

    documento.setId(idDocumentoFirmado);
    documento.setEstado(EstadoDocumentoEnum.FIRMADO);
    documento.setFechaFirma(fechaFirma);

    //intervinienteUseCase.createDocumentoInterviniente(documento.getid);
  /*  MetadatoDocumentoInputDTO metadatoDocumentoInputDTO = new MetadatoDocumentoInputDTO(documento.getNombreDocumento(), "EN-01-CNCV-82");

    Interviniente interviniente=intervinienteRepository.findById(Integer.parseInt(documento.getIdEntidad())).orElse(null);

    servicioGestorDocumental
      .crearDocumento(CodigoEntidad.INTERVINIENTE, interviniente.getIdHaya(), file, metadatoDocumentoInputDTO)
      .getIdDocumento();*/




    gestorDocumentalRepository.save(documento);
  }

  private GestorDocumental obtenerDocumento(Integer id) throws NotFoundException {
    return gestorDocumentalRepository
        .findById(id)
        .orElseThrow(
          () -> new NotFoundException("documento", id));
  }

  @Override
  public void cerrarSolicitudesCaducadas() {
    List<SolicitudFirma> solicitudesCaducadas = this.obtenerSolicitudesCaducadas();

    for (SolicitudFirma solicitud : solicitudesCaducadas) {
      this.caducarSolicitud(solicitud);
    }
  }

  private List<SolicitudFirma> obtenerSolicitudesCaducadas() {
    return repository.findAllByFechaConcluidoIsNullAndFechaVencimientoBefore(FechasUtil.hoy());
  }

  private void caducarSolicitud(SolicitudFirma solicitud) {
    solicitud.setFechaConcluido(FechasUtil.hoy());
    solicitud.setEstado(EstadoSolicitudFirmaEnum.CADUCADA);
    repository.save(solicitud);
  }

  private PeticionGetSignOutputDto consultarEstadoSolicitud(Integer idPeticion) {
    String idPetSign = idPeticion.toString();
    return firmaDigitalClient.obtenerFirma(idPetSign, USER);
  }

  private void procesarRespuestaOK(Integer idPeticion, Integer idDocumento) throws ParseException {

    SolicitudFirma solicitudFirma =
        SolicitudFirma.builder()
            .fechaCreacion(FechasUtil.hoy())
            .fechaVencimiento(new SimpleDateFormat("yyyy-MM-dd").parse(this.generarFechaValidez()))
            .estado(EstadoSolicitudFirmaEnum.EN_CURSO)
            .idPeticion(idPeticion)
            .idDocumento(idDocumento)
            .build();

    repository.save(solicitudFirma);
  }

  private void procesarRespuestaKO(List<String> msg) throws ExternalApiErrorException {
    String mensajeError = String.join(",", msg);

    throw new ExternalApiErrorException(mensajeError);
  }

  private PeticionCreateSignOutputDto crearFirma(Integer idDocumento, Integer idInterviniente)
      throws UnfulfilledConditionException, IOException, NotFoundException {

    String validity = this.generarFechaValidez();
    Interviniente interviniente = this.obtenerInterviniente(idInterviniente);

    CreateSignDocumentInputDto document = this.generarDtoDocumento(idDocumento);
    CreateSignSignatoryInputDto signatory = this.generarDtoSignatory(idDocumento, interviniente);
    CreateSignSignatureInputDto signature = this.generarDtoSignature(idDocumento);

    PeticionCreateSignInputDto inputDto =
        PeticionCreateSignInputDto.builder()
            .user(interviniente.getEmailPrincipal())
            .name("Solicitud firma del documento " + idDocumento)
            .reminder(null)
            .validity(validity)
            .typeOfUpload(1)
            .app("FENIX")
            .gathererName("170014 - Soporte y desarrollo de Aplicaciones")
            .companyName("HAYA REAL ESTATE S.A.U.")
            .document(Collections.singletonList(document))
            .signatory(Collections.singletonList(signatory))
            .signature(Collections.singletonList(signature))
            .build();

    return firmaDigitalClient.crearFirma(inputDto);
  }

  private String generarFechaValidez() {
    Date fechaValidez = FechasUtil.sumarDias(FechasUtil.hoy(), 7);
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    return df.format(fechaValidez);
  }

  private CreateSignSignatureInputDto generarDtoSignature(Integer idDocumento) throws IOException {
    int documentId = 1;
    int signatoryId = 1;
    int type = 1;

    byte[] inputStream = this.descargarDocumento(idDocumento);

    CreateSignSignaturePositionInputDto position =
        new CreateSignSignaturePositionInputDto()
            .posicionPorDefecto(String.valueOf(PDDocument.load(inputStream).getNumberOfPages()));

    return CreateSignSignatureInputDto.builder()
        .documentId(documentId)
        .signatoryId(signatoryId)
        .type(type)
        .position(position)
        .build();
  }

  private byte[] descargarDocumento(Integer idDocumento) throws IOException {
    RespuestaDescargaDocumento respuestaDescargaDocumento =
        servicioGestorDocumental.descargarDocumento(idDocumento);

    return IOUtils.toByteArray(respuestaDescargaDocumento.getBody());
  }

  private CreateSignSignatoryInputDto generarDtoSignatory(
      Integer idDocumento, Interviniente interviniente) throws UnfulfilledConditionException {
    int id = 1;
    int order = 1;
    String name =
        Objects.nonNull(interviniente.getNombre())
            ? interviniente.getNombre()
            : interviniente.getRazonSocial();
    if (Objects.isNull(name) || name.equals("")) {
      throw new UnfulfilledConditionException("Nombre no informado");
    }
    String idCard = interviniente.getNumeroDocumento();
    String email = interviniente.getEmailPrincipal();
    if (Objects.isNull(email) || email.equals("")) {
      throw new UnfulfilledConditionException("Email no informado");
    }
    String cellPhone = this.sanitizarTelefono(interviniente.getTelefonoPrincipal());

    return CreateSignSignatoryInputDto.builder()
        .id(id)
        .order(order)
        .name(name)
        .idCard(idCard)
        .cellPhone(cellPhone)
        .email(email)
        .build();
  }

  private Interviniente obtenerInterviniente(Integer idInterviniente) throws NotFoundException {
    return intervinienteRepository
        .findById(idInterviniente)
        .orElseThrow(() -> new NotFoundException("Interviniente", idInterviniente));
  }

  private Contrato obtenerContrato(String idContrato) throws NotFoundException {
    return contratoRepository
        .findByIdCarga(idContrato)
        .orElseThrow(() -> new NotFoundException("Contrato", idContrato));
  }

  private String sanitizarTelefono(String telefono) throws UnfulfilledConditionException {
    if (Objects.isNull(telefono) || telefono.equals("")) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new UnfulfilledConditionException("Phone not informed");
      else throw new UnfulfilledConditionException("Telefono no informado");
    }

    return TelefonosUtil.sanitizarTelefono(telefono);
  }

  private CreateSignDocumentInputDto generarDtoDocumento(Integer idDocumento) {
    int id = 1;
    int order = 1;
    boolean mandatorySignature = true;
    int dataID = idDocumento;

    return CreateSignDocumentInputDto.builder()
        .id(id)
        .order(order)
        .mandatorySignature(mandatorySignature)
        .dataID(dataID)
        .build();
  }
}
