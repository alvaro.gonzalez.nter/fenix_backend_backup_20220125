package com.haya.alaska.integraciones.solicitud_firma.domain.enums;

public enum EstadoSolicitudFirmaEnum {
  EN_CURSO,
  COMPLETADA,
  CADUCADA,
  ;
}
