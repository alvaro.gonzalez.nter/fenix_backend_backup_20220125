package com.haya.alaska.integraciones.solicitud_firma.domain.enums;

public enum EstadoSolicitudFirmaBackupEnum {
  PENDING,
  SIGNED,
  REJECTED,
  EXPIRED,
  ;
}
