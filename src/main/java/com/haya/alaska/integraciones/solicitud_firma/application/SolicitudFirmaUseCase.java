package com.haya.alaska.integraciones.solicitud_firma.application;

import com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto.IntervinienteFirmaInputDto;
import com.haya.alaska.shared.exceptions.ExternalApiErrorException;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.UnfulfilledConditionException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface SolicitudFirmaUseCase {

  void iniciarSolicitudFirma(Integer idDocumento, Integer idInterviniente)
      throws ExternalApiErrorException, IOException, UnfulfilledConditionException,
          LockedChangeException, NotFoundException, ParseException;

  void actualizarEstadoSolicitudesEnCurso()
      throws IOException, ExternalApiErrorException, NotFoundException;

  void cerrarSolicitudesCaducadas();

  void iniciarSolicitudFirmaMulti(
      List<IntervinienteFirmaInputDto> idsInterviniente, MultipartFile file, Usuario usuario)
      throws UnfulfilledConditionException, LockedChangeException, NotFoundException, IOException,
          ExternalApiErrorException, ParseException;
}
