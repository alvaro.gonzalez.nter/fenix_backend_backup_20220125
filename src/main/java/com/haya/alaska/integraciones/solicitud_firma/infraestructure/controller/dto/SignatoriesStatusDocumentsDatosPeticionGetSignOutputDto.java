package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.Date;

@Getter
public class SignatoriesStatusDocumentsDatosPeticionGetSignOutputDto {
  @JsonProperty("FullName")
  public String fullName;

  @JsonProperty("IdCard")
  public String idCard;

  @JsonProperty("PhoneNumber")
  public String phoneNumber;

  @JsonProperty("EmailAddress")
  public String emailAddress;

  @JsonProperty("RejectReason")
  public String rejectReason;

  @JsonProperty("Signature")
  public SignatureSignatoriesStatusDocumentsDatosPeticionGetSignOutputDto signature;

  public Date obtenerFechaFirma() {
    return this.getSignature().obtenerFechaFirma();
  }
}
