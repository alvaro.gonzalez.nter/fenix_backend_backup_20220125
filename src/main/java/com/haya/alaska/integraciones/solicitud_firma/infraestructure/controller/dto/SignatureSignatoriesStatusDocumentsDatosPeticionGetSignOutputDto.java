package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.Date;

@Getter
public class SignatureSignatoriesStatusDocumentsDatosPeticionGetSignOutputDto {

  @JsonProperty("Type")
  public String type;

  @JsonProperty("Signed")
  public Boolean signed;

  @JsonProperty("SignedDate")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  public Date signedDate;

  public Date obtenerFechaFirma() {
    return this.getSignedDate();
  }
}
