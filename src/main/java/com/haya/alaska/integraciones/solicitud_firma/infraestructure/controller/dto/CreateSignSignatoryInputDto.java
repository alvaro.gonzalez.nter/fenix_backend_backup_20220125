package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CreateSignSignatoryInputDto {

  private int id;

  private int order;

  private String name;

  private String idCard;

  private String cellPhone;

  private String email;
}
