package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PeticionCreateSignOutputDto {

  private Integer result;

  private List<String> msg;

  private Integer idPeticion;

  /**
   * Comprueba si la petición ha resultado OK o KO
   *
   * @return El resultado de la comprobación
   */
  public boolean estaOK() {
    return this.result.equals(1);
  }
}
