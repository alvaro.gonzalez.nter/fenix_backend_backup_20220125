package com.haya.alaska.integraciones.solicitud_firma.infraestructure.client;

import com.haya.alaska.config.FeignConfig;
import com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto.PeticionCancelSignOutputDto;
import com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto.PeticionCreateSignInputDto;
import com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto.PeticionCreateSignOutputDto;
import com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto.PeticionGetSignOutputDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
    value = "fenixFirmaDigital",
    url = "${firma.digital.baseUrl}",
    configuration = FeignConfig.class)
public interface FirmaDigitalClient {

  @PostMapping("${firma.digital.createSign}")
  PeticionCreateSignOutputDto crearFirma(@RequestBody PeticionCreateSignInputDto inputDto);

  @GetMapping("${firma.digital.getSign}")
  PeticionGetSignOutputDto obtenerFirma(@RequestParam String idPetSign, @RequestParam String user);

  @GetMapping("${firma.digital.closeSign}")
  PeticionCancelSignOutputDto concluirFirma(
      @RequestParam String idPetSign, @RequestParam String user);
}
