package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateSignSignaturePositionInputDto {

  // Constantes de clase

  /**
   * El punto Y = 0 y X = 0, es la esquina inferior izquierda del documento.
   */
  private static final String DEFAULT_POSITION_Y = "20";
  private static final String DEFAULT_POSITION_X = "400";

  /**
   * Medidas de la firma en pixeles.
   */
  private static final String DEFAULT_WIDTH = "100";

  private static final String DEFAULT_HEIGHT = "100";

  private String positionY;

  private String positionX;

  private String width;

  private String height;

  private String page;

  public CreateSignSignaturePositionInputDto posicionPorDefecto(String page) {
    this.positionY = DEFAULT_POSITION_Y;
    this.positionX = DEFAULT_POSITION_X;
    this.width = DEFAULT_WIDTH;
    this.height = DEFAULT_HEIGHT;
    this.page = page;

    return this;
  }
}
