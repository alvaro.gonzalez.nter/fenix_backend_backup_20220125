package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.haya.alaska.integraciones.solicitud_firma.domain.enums.EstadoSolicitudFirmaBackupEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DatosPeticionGetSignOutputDto {
  @JsonProperty("IdDossier")
  private Integer idDossier;

  @JsonProperty("Name")
  private String name;

  @JsonProperty("Date")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date date;

  @JsonProperty("ValidityDate")
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date validityDate;

  @JsonProperty("Reminder")
  private Boolean reminder;

  @JsonProperty("User")
  private String user;

  @JsonProperty("GlobalStatus")
  private EstadoSolicitudFirmaBackupEnum globalStatus;

  @JsonProperty("SignIsOpen")
  private Boolean signIsOpen;

  @JsonProperty("Documents")
  private List<DocumentsDatosPeticionGetSignOutputDto> documents;

  public Date obtenerFechaFirmaDocumento() {
    return this.getDocuments().get(0).obtenerFechaFirma();
  }
}
