package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PeticionCancelSignOutputDto {
  private int codigoError;

  private String mensajeError;
}
