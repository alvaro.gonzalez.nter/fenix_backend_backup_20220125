package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateSignSignatureInputDto {

  private int documentId;

  private int signatoryId;

  private int type;

  private CreateSignSignaturePositionInputDto position;
}
