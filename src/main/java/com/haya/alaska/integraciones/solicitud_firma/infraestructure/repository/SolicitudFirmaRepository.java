package com.haya.alaska.integraciones.solicitud_firma.infraestructure.repository;

import com.haya.alaska.integraciones.solicitud_firma.domain.SolicitudFirma;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface SolicitudFirmaRepository extends JpaRepository<SolicitudFirma, Integer> {

  List<SolicitudFirma> findAllByFechaConcluidoIsNull();

  Optional<SolicitudFirma> findAllByFechaConcluidoIsNullAndIdDocumento(Integer idDocumento);

  List<SolicitudFirma> findAllByFechaConcluidoIsNullAndFechaVencimientoBefore(Date dia);
}
