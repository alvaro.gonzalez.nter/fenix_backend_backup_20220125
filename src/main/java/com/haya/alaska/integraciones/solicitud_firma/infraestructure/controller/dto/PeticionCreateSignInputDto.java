package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.List;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PeticionCreateSignInputDto {

  private String user;

  private String name;

  private Integer reminder;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private String validity;

  private int typeOfUpload;

  private String app;

  private String gathererName;

  private String companyName;

  private List<CreateSignDocumentInputDto> document;

  private List<CreateSignSignatoryInputDto> signatory;

  private List<CreateSignSignatureInputDto> signature;
}
