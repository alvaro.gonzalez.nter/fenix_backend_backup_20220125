package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class CrearFirmaInputDto {
  @NotNull private Integer idDocumento;
  private String nombre;
  private String telefono;
  private String email;
}
