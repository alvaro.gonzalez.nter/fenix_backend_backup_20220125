package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import lombok.Getter;

@Getter
public class IntervinienteFirmaInputDto {
  private Integer idInterviniente;
  private String idContrato;
}
