package com.haya.alaska.integraciones.solicitud_firma.infraestructure.controller.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class ListIntervinienteFirmaInputDto {
  private List<IntervinienteFirmaInputDto> intervinientes;
}
