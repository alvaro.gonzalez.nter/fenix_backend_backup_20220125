package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordDiscarttedData implements Serializable {

  private String concept;

  private String code;
}
