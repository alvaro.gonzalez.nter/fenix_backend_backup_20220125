package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ResponseDto {
    
  @JsonProperty("code")
  private Integer status;

  private String type;

  private String label;

  private ResponseResponseDto response;

  public boolean isOkGet() {
    return this.obtenerRespuestaGet() != null
        && !(this.getResponse().getResponse() instanceof List);
  }

  public boolean isOkPost() {
    return this.getResponse().getRecordDiscartted() == null || this.obtenerErrorPost().isEmpty();
  }

  public boolean notFound() {
    return this.obtenerRespuestaGet() instanceof List;
  }

  public String error() {
    if (this.getResponse().getRecordDiscartted() == null) return null;

    return !this.obtenerErrorPost().isEmpty() ? this.obtenerErrorPost().get(0).getReason() : null;
  }

  public Object obtenerRespuestaGet() {
    return this.getResponse().getResponse();
  }

  public Object obtenerRespuestaPost() {
    return this.getResponse().getRecordOk();
  }

  public List<RecordDiscartted> obtenerErrorPost() {
    return this.getResponse().getRecordDiscartted();
  }
}
