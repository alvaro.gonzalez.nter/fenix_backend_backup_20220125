package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import lombok.Data;

import java.util.List;

@Data
public class ResponseResponseDto {

  private Boolean success;

  private Object response;

  private Object recordOk;

  private List<RecordDiscartted> recordDiscartted;
}
