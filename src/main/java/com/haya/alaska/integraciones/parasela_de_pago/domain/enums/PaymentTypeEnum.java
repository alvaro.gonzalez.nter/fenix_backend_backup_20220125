package com.haya.alaska.integraciones.parasela_de_pago.domain.enums;

public enum PaymentTypeEnum {
  UNICO(1, "PA-UN"),
  RECURRENTE(2, "PA-RE"),
  ;

  private final Integer id;
  private final String codigo;

  PaymentTypeEnum(Integer id, String codigo) {
    this.id = id;
    this.codigo = codigo;
  }

  public Integer getId() {
    return this.id;
  }

  public String getCodigo() {
    return this.codigo;
  }
}
