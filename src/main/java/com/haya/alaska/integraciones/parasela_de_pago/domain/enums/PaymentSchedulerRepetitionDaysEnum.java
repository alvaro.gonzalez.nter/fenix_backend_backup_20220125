package com.haya.alaska.integraciones.parasela_de_pago.domain.enums;

public enum PaymentSchedulerRepetitionDaysEnum {
  LUNES("L"),
  MARTES("M"),
  MIERCOLES("X"),
  JUEVES("J"),
  VIERNES("V"),
  SABADO("S"),
  DOMINGO("D"),
  ;

  private final String codigo;

  PaymentSchedulerRepetitionDaysEnum(String codigo) {
    this.codigo = codigo;
  }

  public String getCodigo() {
    return this.codigo;
  }
}
