package com.haya.alaska.integraciones.parasela_de_pago.mapper;

import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.InfoEfectuarPagoDto;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto.IntervinienteDeudorContratoDTO;
import com.haya.alaska.interviniente.domain.Interviniente;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface InfoEfectuarPagoMapper {
  InfoEfectuarPagoMapper INSTANCE = Mappers.getMapper(InfoEfectuarPagoMapper.class);

  @Mapping(target = "cliente", source = "dto.cliente")
  @Mapping(target = "deuda", source = "dto.deuda")
  @Mapping(target = "contrato", source = "dto.contrato")
  @Mapping(target = "nombrePagador", source = "interviniente.nombre")
  @Mapping(target = "telefonoPagador", expression = "java(interviniente.getTelefonoPrincipal())")
  @Mapping(target = "correoPagador", expression = "java(interviniente.getEmailPrincipal())")
  InfoEfectuarPagoDto toDto(IntervinienteDeudorContratoDTO dto, Interviniente interviniente);
}
