package com.haya.alaska.integraciones.parasela_de_pago.domain.enums;

public enum PaymentChannelEnum {
  REDSYS(1),
  PAYPAL(2),
  CECA(3),
  BIZUM(4),
  TRANFB(5),
  ;

  private final Integer id;

  PaymentChannelEnum(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return this.id;
  }
}
