package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentScheduleDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("schedule_status_id")
  private Integer scheduleStatusId;

  private String target;

  private Integer mode;

  @JsonProperty("start_date")
  private String startDate;

  @JsonProperty("repetition_period_number")
  private String repetitionPeriodNumber;

  @JsonProperty("repetition_period")
  private String repetitionPeriod;

  @JsonProperty("repetition_days")
  private String repetitionDays;

  @JsonProperty("repetition_fixed_num_day")
  private String repetitionFixedNumDay;

  @JsonProperty("end_mode")
  private String endMode;

  @JsonProperty("fixed_end_date")
  private String fixedEndDate;

  @JsonProperty("end_repetition_number")
  private String endRepetitionNumber;

  @JsonProperty("fixed_amount")
  private String fixedAmount;

  private String amount;

  @JsonProperty("fixed_concept")
  private String fixedConcept;

  private String concept;
}
