package com.haya.alaska.integraciones.parasela_de_pago.domain.enums;

public enum ApplicationIdEnum {
  HRE_CIBELES(1, "hre-cibeles"),
  HRE_ALQUILERES(2, "hre-alquileres"),
  ALASKA(3, "alaska"),
  PORTAL_INQUILINO(4, "portal-inquilino"),
  PORTAL_WEB(5, "portal-web"),
  OTRAS(6, "otras"),
  NEREA(7, "NEREA001"),
  FENIX(17, "0001"),
  ;

  private final Integer id;
  private final String codigo;

  ApplicationIdEnum(Integer id, String codigo) {
    this.id = id;
    this.codigo = codigo;
  }

  public Integer getId() {
    return this.id;
  }

  public String getCodigo() {
    return this.codigo;
  }
}
