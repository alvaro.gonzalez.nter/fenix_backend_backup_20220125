package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller;

import com.haya.alaska.integraciones.parasela_de_pago.application.PasarelaPagoUseCase;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.HistoricoPagoDto;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.InfoEfectuarPagoDto;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.RespuestaPasarelaPagoCompletadaDTO;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.SendPagoDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.UnfulfilledConditionException;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import org.apache.tomcat.websocket.AuthenticationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@AllArgsConstructor
@RestController
@RequestMapping("api/v1/pasarelapago")
public class PasarelaPagoController {

  private final PasarelaPagoUseCase pasarelaPagoService;

  @PostMapping("/efectuar/pago")
  private ResponseEntity<Object> efectuarPago(@ApiIgnore CustomUserDetails principal, 
	  @RequestParam Integer idInterviniente, @RequestBody SendPagoDTO sendPago) {
    try {
      return ResponseEntity.ok(pasarelaPagoService.efectuarPago(idInterviniente, sendPago, (Usuario) principal.getPrincipal()));
    } catch (AuthenticationException
        | UnfulfilledConditionException
        | LockedChangeException
        | NotFoundException e) {
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping("efectuarPago")
  private InfoEfectuarPagoDto obtenerInfoPago(@RequestParam Integer idInterviniente, @RequestParam Integer idCliente, @RequestParam String contrato)
      throws NotFoundException {
    return pasarelaPagoService.obtenerInfoContrato(idInterviniente, idCliente, contrato);
  }

  @GetMapping("historico")
  private HistoricoPagoDto obtenerHistorico(@RequestParam Integer idInterviniente, @RequestParam Integer idCliente) {
    return pasarelaPagoService.obtenerHistorico(idInterviniente, idCliente);
  }

  @PostMapping("/completado")
  private void actualizadoDePagos(@ApiIgnore CustomUserDetails principal, @RequestHeader("token") String token,
      @RequestBody RespuestaPasarelaPagoCompletadaDTO respuestaPasarelaPagoCompletadaDTO) 
      throws Exception {
    pasarelaPagoService.procesoPagoCompletado(token, respuestaPasarelaPagoCompletadaDTO);
  }

  @DeleteMapping("cancelar")
  private void cancelarPago(@RequestParam Integer idPasarelaPago)
      throws NotFoundException, UnfulfilledConditionException {
    pasarelaPagoService.cancelarPago(idPasarelaPago);
  }
}
