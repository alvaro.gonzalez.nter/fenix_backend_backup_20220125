package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class SendCancelPaymentDTO extends SendDto {

  @JsonProperty("payment_id")
  private String paymentId;

  public SendCancelPaymentDTO(String token, String paymentId) {
    super(token);
    this.paymentId = paymentId;
  }
}
