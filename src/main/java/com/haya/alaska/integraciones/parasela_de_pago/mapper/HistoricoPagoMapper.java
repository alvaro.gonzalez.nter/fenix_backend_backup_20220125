package com.haya.alaska.integraciones.parasela_de_pago.mapper;

import com.haya.alaska.integraciones.parasela_de_pago.domain.HistoricoPago;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.HistoricoDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface HistoricoPagoMapper {
  HistoricoPagoMapper INSTANCE = Mappers.getMapper(HistoricoPagoMapper.class);

  @Mapping(target = "fechaValor", source = "fecha")
  @Mapping(target = "importe", source = "importePago")
  @Mapping(target = "pago", source = "idPago")
  @Mapping(target = "cancelar", expression = "java(historicoPago.isCancelable())")
  HistoricoDto toDto(HistoricoPago historicoPago);
}
