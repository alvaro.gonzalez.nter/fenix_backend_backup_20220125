package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InfoEfectuarPagoDto {
  public String cliente;
  public String deuda;
  public String contrato;
  public String idAplicativo;
  public String canalDePago;
  public String nombrePagador;
  public String telefonoPagador;
  public String correoPagador;
  public Date fechaExpiracion;
}
