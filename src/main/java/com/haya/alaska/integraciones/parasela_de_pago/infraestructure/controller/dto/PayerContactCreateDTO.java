package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PayerContactCreateDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String code;

  private String name;

  private String cifnif;

  private String email;

  private String address;

  private String telephone;

  @JsonProperty("geo_city_id")
  private Integer geoCityId;

  @JsonProperty("geo_postal_code_id")
  private Integer geoPostalCodeId;

  @JsonProperty("haya_id")
  private Integer hayaId;

  @JsonProperty("contract_id")
  private Integer contractId;
}
