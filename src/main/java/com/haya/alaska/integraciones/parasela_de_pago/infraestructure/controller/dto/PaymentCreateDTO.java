package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentCreateDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String concept;

  @JsonProperty("application_id")
  private Integer applicationId;

  @JsonProperty("application_name")
  private String applicationName;

  @JsonProperty("application_data")
  private List<ApplicationDataResponseDto> applicationData;

  @JsonProperty("haya_id")
  private String hayaId;

  @JsonProperty("contract_id")
  private Integer contractId;

  @JsonProperty("payment_type_id")
  private Integer paymentTypeId;

  @JsonProperty("payment_status_id")
  private Integer paymentStatusId;

  @JsonProperty("payment_channel_id")
  private Integer paymentChannelId;

  @JsonProperty("start_date")
  private String startDate;

  @JsonProperty("payment_date")
  private String paymentDate;

  @JsonProperty("expiration_date")
  private String expirationDate;

  @JsonProperty("payer_contact_id")
  private Integer payerContactId;

  @JsonProperty("payment_schedule_id")
  private Integer paymentScheduleId;

  private Double amount;

  private String data;
}
