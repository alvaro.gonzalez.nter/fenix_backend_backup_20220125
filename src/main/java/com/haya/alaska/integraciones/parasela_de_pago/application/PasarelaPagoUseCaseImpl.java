package com.haya.alaska.integraciones.parasela_de_pago.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.evento.application.EventoUseCaseImpl;
import com.haya.alaska.integraciones.parasela_de_pago.domain.HistoricoPago;
import com.haya.alaska.integraciones.parasela_de_pago.domain.enums.ApplicationIdEnum;
import com.haya.alaska.integraciones.parasela_de_pago.domain.enums.PaymentStatusEnum;
import com.haya.alaska.integraciones.parasela_de_pago.domain.enums.PaymentTypeEnum;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.CpMiddlewarePreHayaFeignClient;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto.*;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.*;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.repository.PagosRepository;
import com.haya.alaska.integraciones.parasela_de_pago.mapper.HistoricoPagoMapper;
import com.haya.alaska.integraciones.parasela_de_pago.mapper.InfoEfectuarPagoMapper;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto.IntervinienteDeudorContratoDTO;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.controller.dto.MovimientoPDOutputDTO;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.mapper.PortalDeudorMapper;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.util.PortalDeudorUtil;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.movimiento.application.MovimientoUseCase;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoInputDto;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoOutputDto;
import com.haya.alaska.shared.FechasUtil;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.UnfulfilledConditionException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.tomcat.websocket.AuthenticationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RequiredArgsConstructor
@Service
public class PasarelaPagoUseCaseImpl implements PasarelaPagoUseCase {

  // Respositorio principal
  private final PagosRepository repository;

  // Respositorios relacionados
  private final IntervinienteRepository intervinienteRepository;
  private final CarteraRepository carteraRepository;
  private final ContratoRepository contaContratoRepository;
  private final UsuarioRepository usuarioRepository;

  // Servicios relacionados
  private final PortalDeudorMapper portalDeudorMapper;
  private final PortalDeudorUtil portalDeudorUtil;
  private final MovimientoUseCase movimientoUseCase;
  private final EventoUseCaseImpl eventoUseCase;
  private final CpMiddlewarePreHayaFeignClient hreCibelesClient;

  @Value("${pasarela.pago.wsCustomerToken}")
  private String tokenCostumer;

  @Value("${pasarela.pago.token}")
  private String token;

  @Value("${token.cibeles.use.api}")
  private String tokenCibeles;


  @Override
  public Object efectuarPago(Integer idInterviniente, SendPagoDTO sendPago, Usuario usuario)
      throws AuthenticationException, UnfulfilledConditionException, LockedChangeException,
          NotFoundException {
    if (token == null) {
	throw new AuthenticationException("Problema al obtener el Token de Fenix");
    }

    PaymentCreateDTO sendPagoClient = this.obtenerDtoEfectuarPago(idInterviniente, sendPago, usuario);

    if (sendPagoClient == null) {
      throw new UnfulfilledConditionException("Problema al efectuar el pago.");
    }

    ResponseDto respuestaSendPagoDTO =
        this.createPayment(Collections.singletonList(sendPagoClient));

    if (respuestaSendPagoDTO.isOkPost()) {
      return respuestaSendPagoDTO.obtenerRespuestaPost();
    } else {
	throw new LockedChangeException("Problema al efectuar el pago.");
    }
  }

  @Override
  public InfoEfectuarPagoDto obtenerInfoContrato(Integer idInterviniente, Integer idCliente, String contrato)
      throws NotFoundException {

    Interviniente interviniente = this.obtenerInterviniente(idInterviniente);
    IntervinienteDeudorContratoDTO contratoDTO = this.obtenerContrato(idInterviniente, idCliente, contrato);

    System.out.println("PRE LLAMADA");
    InfoEfectuarPagoDto infoEfectuarPagoDto = InfoEfectuarPagoMapper.INSTANCE.toDto(contratoDTO, interviniente);
    System.out.println("POST LLAMADA");

    infoEfectuarPagoDto.setIdAplicativo(ApplicationIdEnum.FENIX.toString());
    infoEfectuarPagoDto.setCanalDePago("Redsys/BBVA");
    infoEfectuarPagoDto.setFechaExpiracion(FechasUtil.sumarDias(FechasUtil.hoy(), 7));
    System.out.println("POST AÑADIR");

    return infoEfectuarPagoDto;
  }

  @Override
  public void procesoPagoCompletado(String token, RespuestaPasarelaPagoCompletadaDTO respuestaPasarelaPagoCompletadaDTO)
      throws Exception, NotFoundException {

    if(tokenCibeles.equals(token)) {

      String estadoPago = respuestaPasarelaPagoCompletadaDTO.getPaymentStatusId();
      String abonado = String.valueOf(PaymentStatusEnum.ABONADO.getId());
      String aceptado = String.valueOf(PaymentStatusEnum.ACEPTADO.getId());

      // Obtener el usuario
      Integer idUsuario = respuestaPasarelaPagoCompletadaDTO.getApplicationData().getIdUsuario();
      Usuario usuario = usuarioRepository.findById(idUsuario)
	      .orElseThrow(() -> new NotFoundException("Usuario no encontrado"));

      // Obtenemos el objeto desde Cibeles con la respuesta del proceso del pago
      // Crearemos el movimiento si el esstado de pago es Abonado
      if (respuestaPasarelaPagoCompletadaDTO != null && abonado.equals(estadoPago) || aceptado.equals(estadoPago)) {

	if(respuestaPasarelaPagoCompletadaDTO.getContractId() == null) {
	  throw new NotFoundException("No hay Contrato en el proceso de pago");
	}

        Contrato contrato = obtenerContratoByIdCarga(respuestaPasarelaPagoCompletadaDTO.getContractId());

        Boolean isCoreBanking = contrato.getExpediente().getCartera().getDatosCoreBanking();

        // Comprobamos que la Cartera no sea de aprovisionamiento automatico para generar el movimiento
  	if(!isCoreBanking ) {

          // Hay que poblar el objeto SendMovimientoDTO para realizar la peticion al servicio Crear un Movimiento
  	  MovimientoInputDto sendMovimientoDTO = this.obtenerMovimientoInputDto(respuestaPasarelaPagoCompletadaDTO);

          // Llamamos al servicio de Fenix para crear un movimiento
          MovimientoOutputDto movimientoOutputDto = movimientoUseCase.create(contrato.getId(), null, false, sendMovimientoDTO, usuario);

          if (movimientoOutputDto != null) {
            HistoricoPago historicoPago = new HistoricoPago();
            MovimientoPDOutputDTO movimientoOutputDTO = portalDeudorMapper.parseToMovimientoPDOutputDTO(movimientoOutputDto);
            historicoPago.setFecha(movimientoOutputDTO.getFechaValor());
            historicoPago.setContrato(movimientoOutputDTO.getContrato());
            historicoPago.setIdPago(movimientoOutputDTO.getPago());
            historicoPago.setImportePago(movimientoOutputDTO.getImporte());
            historicoPago.setProducto(movimientoOutputDTO.getProducto().getValor());
            historicoPago.setIdInterviniente(this.obtenerIdInterviniente(respuestaPasarelaPagoCompletadaDTO.getPayerContactId()));
            historicoPago.setIdPagoPasarela(respuestaPasarelaPagoCompletadaDTO.getId());
            historicoPago.setIdMovimiento(movimientoOutputDto.getId());
            historicoPago.setIdCliente(this.obtenerIdClienteByIdCargaContrato(respuestaPasarelaPagoCompletadaDTO.getContractId()));
            repository.save(historicoPago);
          }

          // Por el expediente, buscamos el gestor del Expediente
          Usuario usuarioGestor = contrato.getExpediente().getGestor();

          if(usuarioGestor == null) {
              usuarioGestor = contrato.getExpediente().getCartera().getResponsableCartera();
          }

          // Crear evento alerta de pago al gestor
          eventoUseCase.crearEventoPago(contrato, usuarioGestor);
        }
      }
    // Si no es abonado, por ahora no haremos nada
    // Se podría definir para que front recoja el tipo de pago y notificarle el proceso
    // Puede ser un proceso programado a futuro: Su pago se ha registrado correctamente. Se procederá el pago "fecha"
    // Si se ha pagado en el momento y correctamente: Pago procesado correctamente.
    // Si ha surgido algún problema, notificarlo: Problemas en el pago, pruebe de nuevo.
    } else {
	throw new Exception("No tiene permiso para acceder a la API FENIX");
    }
  }

  @Override
  public HistoricoPagoDto obtenerHistorico(Integer idInterviniente, Integer idCliente) {
    List<HistoricoPago> listHistoricoPago = repository.findAllByIdClienteAndIdInterviniente(idCliente, idInterviniente);

    List<HistoricoDto> dto = new ArrayList<>();
    int total = 0;
    for (HistoricoPago historico : listHistoricoPago) {
      total += Integer.parseInt(historico.getImportePago());
      dto.add(HistoricoPagoMapper.INSTANCE.toDto(historico));
    }

    return new HistoricoPagoDto(String.valueOf(total), dto);
  }

  @Override
  public void cancelarPago(Integer idPasarelaPago)
      throws NotFoundException, UnfulfilledConditionException {
    HistoricoPago pago = repository.findById(idPasarelaPago)
	    .orElseThrow(() -> new NotFoundException("Pago", idPasarelaPago));

    if (!pago.isCancelable()) {
      throw new UnfulfilledConditionException("No se puede cancelar el pago");
    }

    this.cancelPayment(pago.getIdPagoPasarela());

    repository.delete(pago);
  }

  // Bloque métodos de llamadas a CIBELES

  private ResponseDto getPayerContact(String payerContact) {
    return hreCibelesClient.getPayerContact(token, new SendGetPayerContactDTO(tokenCostumer, payerContact));
  }

  private ResponseDto getPayment(String payment) {
    return hreCibelesClient.getPayment(token, new SendGetPaymentDTO(tokenCostumer, payment));
  }

  private ResponseDto getSchedulePayment(Integer paymentSchedule) {
    return hreCibelesClient.getPaymentSchedule(token, new SendGetPaymentScheduleDTO(tokenCostumer, paymentSchedule));
  }

  private ResponseDto createPayment(List<PaymentCreateDTO> payment) {
    return hreCibelesClient.createPayment(token, new SendCreatePaymentDTO(tokenCostumer, payment));
  }

  private ResponseDto createPaymentSchedule(List<PaymentScheduleDTO> paymentSchedule) {
    return hreCibelesClient.createPaymentSchedule(token, new SendCreatePaymentScheduleDTO(tokenCostumer, paymentSchedule));
  }

  private ResponseDto createPayerContact(List<PayerContactCreateDTO> payerContact) {
    return hreCibelesClient.createPayerContact(token, new SendCreatePayerContactDTO(tokenCostumer, payerContact));
  }

  private ResponseDto cancelPayment(String paymentId) {
    return hreCibelesClient.cancelPayment(token, new SendCancelPaymentDTO(tokenCostumer, paymentId));
  }

  private ResponseDto cancelPaymentSchedule(String paymentScheduleId) {
    return hreCibelesClient.cancelPaymentSchedule(token, new SendCancelPaymentScheduleDTO(tokenCostumer, paymentScheduleId));
  }

  // Bloque métodos propios
  private Contrato obtenerContratoByIdCarga(String idCargaContrato) throws NotFoundException {
      return contaContratoRepository.findByIdCarga(idCargaContrato)
	      .orElseThrow(() -> new NotFoundException("Contrato no encontrado: ", idCargaContrato));
  }

  private Integer obtenerIdClienteByIdCargaContrato(String idCargaContrato) throws NotFoundException {
      Contrato contrato = contaContratoRepository.findByIdCarga(idCargaContrato)
	      .orElseThrow(() -> new NotFoundException("No se ha encontrado un Contrato: ", idCargaContrato));

      return contrato.getCliente().getId();
  }

  private Integer obtenerIdCliente(String contractId) throws NotFoundException {
    Contrato contrato = contaContratoRepository.findById(Integer.parseInt(contractId))
	    .orElseThrow(() -> new NotFoundException("Contrato", contractId));

    return contrato.getCliente().getId();
  }

  private Integer obtenerIdInterviniente(String payerContactId) throws NotFoundException {
    ResponseDto payerContact = this.getPayerContact(payerContactId);
    PayetContactCreateResponseDto payectContactResponseDTO = new PayetContactCreateResponseDto();
    try {
	BeanUtils.copyProperties(payectContactResponseDTO, payerContact.getResponse().getResponse());
    } catch (IllegalAccessException | InvocationTargetException e) {
	throw new NotFoundException("No encontrado el Objeto de Respuesta de Pago");
    }

    if(payectContactResponseDTO.getCifnif() == null || payectContactResponseDTO.getCifnif().isEmpty()) {
	return this.obtenerIntervinienteByEmail(payectContactResponseDTO.getEmail()).getId();
    }

    return this.obtenerInterviniente(payectContactResponseDTO.getCifnif()).getId();
  }

  private MovimientoInputDto obtenerMovimientoInputDto(RespuestaPasarelaPagoCompletadaDTO dto) {

    SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date fecha = null;

    try {
	fecha = spdf.parse(dto.getStartDate());
    } catch (ParseException e) {
	e.printStackTrace();
    }

    MovimientoInputDto inputDto = new MovimientoInputDto();
    inputDto.setTipoMovimiento(0);
    inputDto.setDescripcion(0);
    inputDto.setSentido("+");
    inputDto.setFechaValor(fecha);
    inputDto.setFechaContable(fecha);
    inputDto.setImporte(dto.getAmount());
    inputDto.setAfecta(true);

    return inputDto;
  }

  private PaymentCreateDTO obtenerDtoEfectuarPago(Integer idInterviniente, SendPagoDTO sendPago, Usuario usuario)
      throws NotFoundException {
    return PaymentCreateDTO.builder()
        .concept(sendPago.getConcepto())
        .applicationId(ApplicationIdEnum.FENIX.getId())
        .applicationData(this.applicationData(usuario))
        .contractId(this.obtenerContratoByIdCarga(sendPago.getContratoId()).getId())
        .paymentTypeId(PaymentTypeEnum.UNICO.getId())
        .paymentStatusId(PaymentStatusEnum.EMITIDO_ENCURSO.getId())
        .paymentChannelId(sendPago.getCanalDePago().getId())
        .startDate(sendPago.getFechaEmision())
        .expirationDate(sendPago.getFechaExpiracion())
        .payerContactId(this.obtenerPayerContact(idInterviniente, sendPago.getContratoId()))
        .amount(sendPago.getCantidad())
        .build();
  }

  private List<ApplicationDataResponseDto> applicationData(Usuario usuario) {
    AdditionalData additionalData = new AdditionalData(usuario.getId());
    return Collections.singletonList(new ApplicationDataResponseDto(Collections.singletonList(new CallbacksResponseDto(additionalData, tokenCibeles))));
  }

  private Integer obtenerPayerContact(Integer idInterviniente, String idContrato)
      throws NotFoundException {
    Interviniente interviniente = this.obtenerInterviniente(idInterviniente);
    ResponseDto payerContact = this.getPayerContact(interviniente.getEmailPrincipal());

    if (payerContact.notFound()) {
      PayerContactCreateDTO dto = PayerContactCreateDTO.builder()
	      .name(interviniente.getNombre())
              .cifnif(interviniente.getNumeroDocumento())
              .email(interviniente.getEmailPrincipal())
              .telephone(interviniente.getTelefonoPrincipal())
              .hayaId(Integer.valueOf(interviniente.getIdHaya()))
              .contractId(this.obtenerContratoByIdCarga(idContrato).getId())
              .build();

      ResponseDto newPayerContact = this.createPayerContact(Collections.singletonList(dto));

      ObjectMapper mapper = new ObjectMapper();
      return Arrays.asList(mapper.convertValue(newPayerContact.obtenerRespuestaPost(), PayetContactCreateResponseDto[].class))
          .get(0)
          .getId();
    } else {
      ObjectMapper mapper = new ObjectMapper();
      return mapper.convertValue(payerContact.obtenerRespuestaGet(), PayetContactCreateResponseDto.class).getId();
    }
  }

  private Interviniente obtenerInterviniente(Integer idInterviniente) throws NotFoundException {
    return intervinienteRepository.findById(idInterviniente)
	    .orElseThrow(() -> new NotFoundException("Interviniente", idInterviniente));
  }

  private Interviniente obtenerInterviniente(String dni) throws NotFoundException {
    Optional<Interviniente> interviniente = intervinienteRepository.findByNumeroDocumento(dni);

    if (interviniente.isPresent()) {
      return interviniente.get();
    } else {
      throw new NotFoundException("No se ha encontrado un interviniente con el dni " + dni);
    }
  }

  private Interviniente obtenerIntervinienteByEmail(String email) throws NotFoundException {
      Optional<Interviniente> interviniente = intervinienteRepository.findByDatosContactoEmail(email);

      if (interviniente.isPresent()) {
	  return interviniente.get();
      } else {
	  throw new NotFoundException("No se ha encontrado un interviniente con el email " + email);
      }
  }

  private IntervinienteDeudorContratoDTO obtenerContrato(Integer idInterviniente, Integer idCliente, String contrato) throws NotFoundException {
    List<IntervinienteDeudorContratoDTO> informacionContratoDeudor = this.findInformacionContratoDeudor(idInterviniente, idCliente);

    Optional<IntervinienteDeudorContratoDTO> contratoDTO =
        informacionContratoDeudor.stream()
            .filter(intervinienteDeudorContratoDTO -> intervinienteDeudorContratoDTO.getContrato().equals(contrato))
            .findAny();
    if (contratoDTO.isEmpty()) {
      throw new NotFoundException("No se ha encontrado el contrato con el codigo " + contrato);
    }

    return contratoDTO.get();
  }

  private List<IntervinienteDeudorContratoDTO> findInformacionContratoDeudor(
      Integer idInterviniente, Integer idCliente) throws NotFoundException {
    Interviniente interviniente = intervinienteRepository.findById(idInterviniente)
            .orElseThrow(() -> new NotFoundException("No se ha encontrado un interviniente con el idInterviniente " + idInterviniente));

    List<Cartera> carteraList = carteraRepository.findAllByClientesClienteId(idCliente);
    List<Contrato> contratoList = new ArrayList<>();

    for (Cartera cartera : carteraList) {
      List<Contrato> contratoList1 = portalDeudorUtil.getContratos(interviniente, cartera);
      contratoList.addAll(contratoList1);
    }

    List<IntervinienteDeudorContratoDTO> intervinienteDeudorContratoDTOList = new ArrayList<>();

    for (Contrato contrato : contratoList) {
      IntervinienteDeudorContratoDTO intervinienteDeudorContratoDTO =  portalDeudorMapper.parseToInformacionDeudorContratoDTO(contrato,
	      interviniente, idCliente);
      intervinienteDeudorContratoDTOList.add(intervinienteDeudorContratoDTO);
    }

    return intervinienteDeudorContratoDTOList;
  }
}
