package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.PayerContactCreateDTO;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class SendCreatePayerContactDTO extends SendDto {

  @JsonProperty("payer_contact")
  private List<PayerContactCreateDTO> payerContact;

  public SendCreatePayerContactDTO(String token, List<PayerContactCreateDTO> payerContact) {
    super(token);
    this.payerContact = payerContact;
  }
}
