package com.haya.alaska.integraciones.parasela_de_pago.domain.enums;

public enum PaymentScheduleStatusEnum {
  BORRADOR_PLANIFICADO(1, "01"),
  ENCURSO_ACTIVO(2, "02"),
  PARCIALMENTE_EMITIDO(3, "03"),
  TOTALMENTE_EMITIDO(4, "04"),
  COMPLETADO(5, "05"),
  CANCELADO(6, "06"),
  ;

  private final Integer id;
  private final String codigo;

  PaymentScheduleStatusEnum(Integer id, String codigo) {
    this.id = id;
    this.codigo = codigo;
  }

  public Integer getId() {
    return this.id;
  }

  public String getCodigo() {
    return this.codigo;
  }
}
