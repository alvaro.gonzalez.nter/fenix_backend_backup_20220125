package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class SendGetPaymentScheduleDTO extends SendDto {

  @JsonProperty("payment_schedule")
  private Integer paymentSchedule;

  public SendGetPaymentScheduleDTO(String token, Integer paymentSchedule) {
    super(token);
    this.paymentSchedule = paymentSchedule;
  }
}
