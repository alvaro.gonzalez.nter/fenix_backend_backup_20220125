package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class SendCancelPaymentScheduleDTO extends SendDto {

  @JsonProperty("payment_schedule_id")
  private String paymentScheduleId;

  public SendCancelPaymentScheduleDTO(String token, String paymentScheduleId) {
    super(token);
    this.paymentScheduleId = paymentScheduleId;
  }
}
