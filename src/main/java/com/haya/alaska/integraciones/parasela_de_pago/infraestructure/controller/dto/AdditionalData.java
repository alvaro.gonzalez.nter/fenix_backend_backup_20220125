package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdditionalData implements Serializable {
    
  private static final long serialVersionUID = 1L;
  
  private Integer idUsuario;
}
