package com.haya.alaska.integraciones.parasela_de_pago.domain.enums;

public enum PaymentStatusEnum {
  BORRADOR_PLANIFICACION(1, "01"),
  EMITIDO_ENCURSO(2, "02"),
  ACEPTADO(3, "03"),
  RECHAZADO(4, "04"),
  CANCELADO(5, "05"),
  ABONADO(6, "06"),
  CONFIRMADO(7, "07"),
  IMPAGADO(8, "08"),
  NOTIFICADO(9, "09"),
  RENOTIFICADO(10, "10"),
  ;

  private final Integer id;
  private final String codigo;

  PaymentStatusEnum(Integer id, String codigo) {
    this.id = id;
    this.codigo = codigo;
  }

  public Integer getId() {
    return this.id;
  }

  public String getCodigo() {
    return this.codigo;
  }
}
