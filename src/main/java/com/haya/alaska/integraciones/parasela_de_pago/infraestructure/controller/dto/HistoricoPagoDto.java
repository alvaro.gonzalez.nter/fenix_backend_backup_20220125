package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class HistoricoPagoDto {
  private String importeTotal;
  private List<HistoricoDto> movimientos;
}
