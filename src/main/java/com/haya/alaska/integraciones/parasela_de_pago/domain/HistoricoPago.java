package com.haya.alaska.integraciones.parasela_de_pago.domain;

import com.haya.alaska.shared.FechasUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_HISTORICO_PAGO")
@Entity
public class HistoricoPago {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "DES_ID")
  private Integer id;

  @Column(name = "DES_FECHA")
  private Date fecha;

  @Column(name = "DES_CONTRATO")
  private String contrato;

  @Column(name = "DES_PRODUCTO")
  private String producto;

  @Column(name = "DES_IMPORTE_PAGO")
  private String importePago;

  @Column(name = "DES_ID_PAGO")
  private String idPago;

  @Column(name = "DES_ID_PAGO_PASARELA")
  private String idPagoPasarela;

  @Column(name = "DES_ID_INTERVINIENTE")
  private Integer idInterviniente;

  @Column(name = "DES_ID_CLIENTE")
  private Integer idCliente;

  @Column(name = "DES_ID_MOVIMIENTO")
  private Integer idMovimiento;

  public boolean isCancelable() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(this.getFecha());
    int x = calendar.get(Calendar.DAY_OF_YEAR);

    Calendar calendar2 = Calendar.getInstance();
    calendar2.setTime(FechasUtil.hoy());
    int y = calendar2.get(Calendar.DAY_OF_YEAR);

    return x > y;
  }
}
