package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RespuestaPasarelaPagoCompletadaDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String id;
  private String created;
  private String modified;
  private String deleted;
  private String code;
  private String concept;

  @JsonProperty("application_id")
  private String applicationId;

  @JsonProperty("application_name")
  private String applicationName;

  @JsonProperty("application_data")
  private AdditionalData applicationData;

  @JsonProperty("haya_id")
  private String hayaId;

  @JsonProperty("contract_id")
  private String contractId;

  @JsonProperty("payment_type_id")
  private String paymentTypeId;

  @JsonProperty("payment_status_id")
  private String paymentStatusId;

  @JsonProperty("payment_channel_id")
  private String paymentChannelId;

  @JsonProperty("valid_payment_channels")
  private String validPaymentChannels;

  @JsonProperty("start_date")
  private String startDate;

  @JsonProperty("payment_date")
  private String paymentDate;

  @JsonProperty("expiration_date")
  private String expirationDate;

  @JsonProperty("payer_contact_id")
  private String payerContactId;

  @JsonProperty("payment_schedule_id")
  private String paymentScheduleId;

  private Double amount;
  
  private String data;
}
