package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CallbacksResponseDto {
  private String token;
  private String workflowId;
  private String url;
  private String method;
  private boolean reqAuth;
  private String content;
  private AdditionalData additionalData;

  public CallbacksResponseDto(AdditionalData additionalData, String token) {
    this.workflowId = "paymentOk";
    this.url = "https://desfenix.haya.es:8080/api/v1/pasarelapago/completado";
    this.method = "POST";
    this.reqAuth = true;
    this.content = "self";
    this.token = token;
    this.additionalData = additionalData;
  }
}
