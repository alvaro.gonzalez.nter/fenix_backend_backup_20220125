package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import java.io.Serializable;

import com.haya.alaska.integraciones.parasela_de_pago.domain.enums.PaymentChannelEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SendPagoDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String contratoId;

  private String concepto;

  private PaymentChannelEnum canalDePago;

  private double cantidad;

  private String fechaEmision;
  
  private String fechaExpiracion;
  
}
