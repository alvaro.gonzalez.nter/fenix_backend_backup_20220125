package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.PaymentCreateDTO;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class SendCreatePaymentDTO extends SendDto {

  @JsonProperty("payment")
  private List<PaymentCreateDTO> payment;

  public SendCreatePaymentDTO(String token, List<PaymentCreateDTO> payment) {
    super(token);
    this.payment = payment;
  }
}
