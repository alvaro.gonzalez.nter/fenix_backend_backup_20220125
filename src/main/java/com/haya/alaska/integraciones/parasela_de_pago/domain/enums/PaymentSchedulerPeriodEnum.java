package com.haya.alaska.integraciones.parasela_de_pago.domain.enums;

public enum PaymentSchedulerPeriodEnum {
  DIARIO(1, "D"),
  SEMANAL(2, "W"),
  MENSUAL(3, "M"),
  ANUAL(4, "Y"),
  ;

  private final Integer id;
  private final String codigo;

  PaymentSchedulerPeriodEnum(Integer id, String codigo) {
    this.id = id;
    this.codigo = codigo;
  }

  public Integer getId() {
    return this.id;
  }

  public String getCodigo() {
    return this.codigo;
  }
}
