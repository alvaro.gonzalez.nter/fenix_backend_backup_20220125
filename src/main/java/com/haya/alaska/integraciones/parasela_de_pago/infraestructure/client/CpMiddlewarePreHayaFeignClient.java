package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client;

import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto.*;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.ResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@FeignClient(name = "CpMiddlewarePre", url = "${pasarela.pago.cp.middleware}")
public interface CpMiddlewarePreHayaFeignClient {

  @GetMapping(value = "${get.payer.contact}")
  @Produces(MediaType.TEXT_PLAIN)
  ResponseDto getPayerContact(
      @RequestHeader("token") String token, @RequestBody SendGetPayerContactDTO sendPayerContact);

  @GetMapping(value = "${get.payment}")
  @Produces(MediaType.TEXT_PLAIN)
  ResponseDto getPayment(
      @RequestHeader("token") String token, @RequestBody SendGetPaymentDTO sendPayect);

  @GetMapping(value = "${get.payment.schedule}")
  @Produces(MediaType.TEXT_PLAIN)
  ResponseDto getPaymentSchedule(
      @RequestHeader("token") String token,
      @RequestBody SendGetPaymentScheduleDTO sendPaymentSchedule);

  @PostMapping(value = "${create.payment}")
  @Produces(MediaType.TEXT_PLAIN)
  ResponseDto createPayment(
      @RequestHeader("token") String token, @RequestBody SendCreatePaymentDTO sendCreatePayment);

  @PostMapping(value = "${create.payment.schedule}")
  @Produces(MediaType.TEXT_PLAIN)
  ResponseDto createPaymentSchedule(
      @RequestHeader("token") String token,
      @RequestBody SendCreatePaymentScheduleDTO sendCreatePaymentSchedule);

  @PostMapping(value = "${create.payer.contact}")
  @Produces(MediaType.TEXT_PLAIN)
  ResponseDto createPayerContact(
      @RequestHeader("token") String token,
      @RequestBody SendCreatePayerContactDTO sendCreatePayerContact);

  @PostMapping(value = "${cancel.payment}")
  @Produces(MediaType.TEXT_PLAIN)
  ResponseDto cancelPayment(
      @RequestHeader("token") String token, @RequestBody SendCancelPaymentDTO sendCancelPayment);

  @PostMapping(value = "${cancel.payment.schedule}")
  @Produces(MediaType.TEXT_PLAIN)
  ResponseDto cancelPaymentSchedule(
      @RequestHeader("token") String token,
      @RequestBody SendCancelPaymentScheduleDTO sendCancelPaymentSchedule);
}
