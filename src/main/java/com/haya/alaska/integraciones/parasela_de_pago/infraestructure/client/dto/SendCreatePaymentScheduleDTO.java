package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.PaymentScheduleDTO;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class SendCreatePaymentScheduleDTO extends SendDto {

  @JsonProperty("payment_schedule")
  private List<PaymentScheduleDTO> paymentSchedule;

  public SendCreatePaymentScheduleDTO(String token, List<PaymentScheduleDTO> paymentSchedule) {
    super(token);
    this.paymentSchedule = paymentSchedule;
  }
}
