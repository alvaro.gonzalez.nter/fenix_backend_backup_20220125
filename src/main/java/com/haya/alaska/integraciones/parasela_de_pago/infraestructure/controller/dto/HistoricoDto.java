package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import java.util.Date;

public class HistoricoDto {
  public Integer id;
  public Date fechaValor;
  public String contrato;
  public String producto;
  public String importe;
  public String pago;
  public Boolean cancelar;
}
