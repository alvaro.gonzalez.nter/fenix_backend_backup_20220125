package com.haya.alaska.integraciones.parasela_de_pago.domain.enums;

public enum PaymentschedulerModeEndEnum {
  NUNCA(1, "NEVER"),
  FECHA_FIJA(2, "FIXED"),
  NUMERO_OCURRENCIAS(3, "NRECC"),
  ;

  private final Integer id;
  private final String codigo;

  PaymentschedulerModeEndEnum(Integer id, String codigo) {
    this.id = id;
    this.codigo = codigo;
  }

  public Integer getIdo() {
    return this.id;
  }

  public String getCodigo() {
    return this.codigo;
  }
}
