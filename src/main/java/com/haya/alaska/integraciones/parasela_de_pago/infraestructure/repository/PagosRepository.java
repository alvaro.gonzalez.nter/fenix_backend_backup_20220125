package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.repository;

import com.haya.alaska.integraciones.parasela_de_pago.domain.HistoricoPago;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagosRepository extends JpaRepository<HistoricoPago, Integer> {

  List<HistoricoPago> findAllByIdClienteAndIdInterviniente(Integer idCliente, Integer idInterviniente);
}
