package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class SendGetPaymentDTO extends SendDto {

  @JsonProperty("payment")
  private String payment;

  public SendGetPaymentDTO(String token, String payment) {
    super(token);
    this.payment = payment;
  }
}
