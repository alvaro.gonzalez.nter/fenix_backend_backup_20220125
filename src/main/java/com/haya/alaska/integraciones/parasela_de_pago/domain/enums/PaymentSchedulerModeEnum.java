package com.haya.alaska.integraciones.parasela_de_pago.domain.enums;

public enum PaymentSchedulerModeEnum {
  FIJA(1, "FIXED"),
  RECURRENTE(2, "RECC"),
  ;

  private final Integer id;
  private final String codigo;

  PaymentSchedulerModeEnum(Integer id, String codigo) {
    this.id = id;
    this.codigo = codigo;
  }

  public Integer getId() {
    return this.id;
  }

  public String getCodigo() {
    return this.codigo;
  }
}
