package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationDataResponseDto {
    
  List<CallbacksResponseDto> callbacks;
  
}
