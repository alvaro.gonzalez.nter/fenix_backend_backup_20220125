package com.haya.alaska.integraciones.parasela_de_pago.application;

import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.HistoricoPagoDto;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.InfoEfectuarPagoDto;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.RespuestaPasarelaPagoCompletadaDTO;
import com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto.SendPagoDTO;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.UnfulfilledConditionException;
import com.haya.alaska.usuario.domain.Usuario;
import org.apache.tomcat.websocket.AuthenticationException;

public interface PasarelaPagoUseCase {

  Object efectuarPago(Integer idInterviniente, SendPagoDTO sendPago, Usuario usuario)
      throws AuthenticationException, UnfulfilledConditionException, LockedChangeException,
          NotFoundException;

  InfoEfectuarPagoDto obtenerInfoContrato(
      Integer idInterviniente, Integer idCliente, String contrato) throws NotFoundException;

  void procesoPagoCompletado(String token, RespuestaPasarelaPagoCompletadaDTO respuestaPasarelaPagoCompletadaDTO)
      throws Exception, NotFoundException;

  HistoricoPagoDto obtenerHistorico(Integer idInterviniente, Integer idCliente);

  void cancelarPago(Integer idPasarelaPago) throws NotFoundException, UnfulfilledConditionException;
}
