package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class SendGetPayerContactDTO extends SendDto {

  @JsonProperty("payer_contact")
  private String payerContact;

  public SendGetPayerContactDTO(String token, String payerContact) {
    super(token);
    this.payerContact = payerContact;
  }
}
