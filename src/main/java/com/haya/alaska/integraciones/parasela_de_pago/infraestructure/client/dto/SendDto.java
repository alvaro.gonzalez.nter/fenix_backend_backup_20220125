package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
public class SendDto implements Serializable {

  @JsonProperty("ws_customer_token")
  private String wsCustomerToken;

}
