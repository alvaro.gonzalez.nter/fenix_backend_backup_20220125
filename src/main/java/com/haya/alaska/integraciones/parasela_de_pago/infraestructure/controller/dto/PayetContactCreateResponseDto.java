package com.haya.alaska.integraciones.parasela_de_pago.infraestructure.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PayetContactCreateResponseDto {
  private Integer id;
  private String created;
  private String modified;
  private String deleted;
  private String code;
  private String name;
  private String cifnif;
  private String email;
  private String address;
  private String telephone;

  @JsonProperty("geo_city_id")
  private Integer geoCityId;

  @JsonProperty("geo_postal_code_id")
  private Integer geoPostalCodeId;

  @JsonProperty("haya_id")
  private Integer hayaId;

  @JsonProperty("contract_id")
  private Integer contractId;

  private String data;
}
