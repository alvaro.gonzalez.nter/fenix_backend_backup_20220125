package com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class EstimacionPCOutputDTO implements Serializable {
  private Date fecha;
  private CatalogoMinInfoDTO tipoPropuesta;
  private Double importe;
}
