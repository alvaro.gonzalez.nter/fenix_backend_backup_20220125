package com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DetalleEstimacionPCOutputDTO implements Serializable {
  private Integer idEstimacion;
  private Integer idExpediente;
  private String idExpedienteConcatenado;
  private Boolean procedimientoJudicial;
  private Double importeRecuperado;
  private Double importeSalida;
  private String intervaloEstimado;
  private Date fechaEstimada;
  private String probabilidadResolucion;
  private String tipoEstrategia;
  private String subtipoEstrategia;
  private String tipoSolucion;
  private Boolean cumplimiento;
  private String cumplida;
  private String comentarios;
  private List<ContratoEstimacionPCOutputDTO> contratos;
  private Boolean nivelEstimacionExpediente;
}
