package com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class PropuestaPCOutputDTO implements Serializable {
  private CatalogoMinInfoDTO estado;
  private CatalogoMinInfoDTO tipo;
  private String importeRecuperado;
  private String importeSalida;
}
