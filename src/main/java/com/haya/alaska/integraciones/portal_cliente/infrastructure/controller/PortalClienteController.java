package com.haya.alaska.integraciones.portal_cliente.infrastructure.controller;

import com.haya.alaska.catalogo.domain.enums.EntidadEnum;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoOutputDTO;
import com.haya.alaska.integraciones.portal_cliente.application.PortalClienteUseCase;
import com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto.*;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/v1/portalCliente")
public class PortalClienteController {

  @Autowired PortalClienteUseCase portalClienteUseCase;
  @Autowired EventoUseCase eventoUseCase;

  @ApiOperation(
      value = "Obtener propuestas por cartera",
      notes = "Devuelve las propuestas con id cartera enviado")
  @GetMapping("/{idCartera}/propuestas")
  public List<PropuestaPCOutputDTO> obtenerPropuestasAgrupadasCartera(
      @PathVariable("idCartera") Integer idCartera) {
    return portalClienteUseCase.obtenerPropuestasAgrupadasCartera(idCartera);
  }

  @ApiOperation(
      value = "Obtener propuestas por cartera y estado",
      notes = "Devuelve las propuestas con id cartera e id estado enviados")
  @GetMapping("/{idCartera}/propuestas/estado/{idEstado}")
  public List<DetalleListadoPropuestaPCOutputDTO> obtenerPropuestasCarteraEstado(
      @PathVariable("idCartera") Integer idCartera, @PathVariable("idEstado") Integer idEstado) {
    return portalClienteUseCase.obtenerPropuestasCarteraEstado(idCartera, idEstado);
  }

  @ApiOperation(
      value = "Obtener propuestas por cartera y tipo",
      notes = "Devuelve las propuestas con id cartera e id tipo enviados")
  @GetMapping("/{idCartera}/propuestas/tipo/{idTipo}")
  public List<DetalleListadoPropuestaPCOutputDTO> obtenerPropuestasCarteraTipo(
      @PathVariable("idCartera") Integer idCartera, @PathVariable("idTipo") Integer idTipo) {
    return portalClienteUseCase.obtenerPropuestasCarteraTipo(idCartera, idTipo);
  }

  @ApiOperation(
      value = "Obtener propuestas por cartera y filtros",
      notes = "Devuelve las propuestas con id cartera y filtros enviados")
  @GetMapping("/{idCartera}/propuestas/filtradas")
  public ListWithCountDTO<DetalleListadoPropuestaPCOutputDTO> obtenerPropuestasCarteraEstadoFiltradas(
      @PathVariable("idCartera") Integer idCartera,
      @RequestParam(value = "idEstado", required = false) Integer idEstado,
      @RequestParam(value = "idTipo", required = false) Integer idTipo,
      @RequestParam(value = "idExpediente", required = false) String idExpediente,
      @RequestParam(value = "fase", required = false) String fase,
      @RequestParam(value = "estado", required = false) String estado,
      @RequestParam(value = "gestorExpediente", required = false) String gestorExpediente,
      @RequestParam(value = "primerTitular", required = false) String primerTitular,
      @RequestParam(value = "estrategia", required = false) String estrategia,
      @RequestParam(value = "saldoGestion", required = false) String saldoGestion,
      @RequestParam(value = "fechaElevacion", required = false) String fechaElevacion,
      @RequestParam(value = "supervisor", required = false) String supervisor,
      @RequestParam(value = "documentos", required = false) Boolean documentos,
      @RequestParam(value = "orderField", required = false) String orderField,
      @RequestParam(value = "orderDirection", required = false) String orderDirection,
      @RequestParam(value = "size", required = true) Integer size,
      @RequestParam(value = "page", required = true) Integer page) {
    return portalClienteUseCase.obtenerPropuestasCarteraEstadoFiltradas(
        idCartera,
        idEstado,
        idTipo,
        idExpediente,
        fase,
        estado,
        gestorExpediente,
        primerTitular,
        estrategia,
        saldoGestion,
        fechaElevacion,
        supervisor,
        documentos,
        orderField,
        orderDirection,
        size,
        page);
  }

  @ApiOperation(
      value = "Revisión del Expediente",
      notes = "Genera el evento de revisión del expediente.")
  @PostMapping("/revision")
  public Integer revision(
      @RequestParam Integer idExpediente,
      @RequestParam String nombreEmisor,
      @RequestParam Integer destinatarioId,
      @RequestParam String mensaje,
      @RequestParam Boolean posesionNegociada,
      @RequestParam(required = false) Integer idEvento,
      @ApiIgnore CustomUserDetails principal)
      throws InvalidCodeException, RequiredValueException, NotFoundException, IOException {
    Usuario logUser = (Usuario) principal.getPrincipal();
    return portalClienteUseCase.eventoRevisionCliente(
        logUser, idExpediente, nombreEmisor, destinatarioId, mensaje, posesionNegociada, idEvento);
  }

  @ApiOperation(
      value = "Obtener estimaciones por cartera",
      notes = "Devuelve las estimaciones con id cartera enviado")
  @GetMapping("/{idCartera}/estimaciones")
  public List<EstimacionPCOutputDTO> obtenerEstimacionesAgrupadasCartera(
      @PathVariable("idCartera") Integer idCartera, @RequestParam String filtro) {
    return portalClienteUseCase.obtenerEstimacionesAgrupadasCartera(filtro, idCartera);
  }

  @ApiOperation(
      value = "Obtener estimaciones por cartera y valoración",
      notes = "Devuelve las estimaciones con id cartera e id valoración enviados")
  @GetMapping("/{idCartera}/estimaciones/valoracion/{idTipo}")
  public List<DetalleListadoEstimacionPCOutputDTO> obtenerEstimacionesCarteraValoracion(
      @PathVariable("idCartera") Integer idCartera, @PathVariable("idTipo") Integer idTipo) {
    return portalClienteUseCase.obtenerEstimacionesCarteraValoracion(idCartera, idTipo);
  }

  @ApiOperation(
      value = "Obtener propuestas por cartera y bien",
      notes = "Devuelve las propuestas con id cartera e id bien enviados")
  @GetMapping("/{idCartera}/estimaciones/bien/{idTipo}")
  public List<DetalleListadoEstimacionPCOutputDTO> obtenerEstimacionesCarteraBien(
      @PathVariable("idCartera") Integer idCartera, @PathVariable("idTipo") Integer idTipo) {
    return portalClienteUseCase.obtenerEstimacionesCarteraBien(idCartera, idTipo);
  }

  @ApiOperation(
      value = "Obtener estimaciones por cartera y filtros",
      notes = "Devuelve las estimaciones con id cartera y filtros enviados")
  @GetMapping("/{idCartera}/estimaciones/filtradas")
  public ListWithCountDTO<DetalleListadoEstimacionPCOutputDTO> obtenerEstimacionesCarteraFiltradas(
      @PathVariable("idCartera") Integer idCartera,
      @RequestParam(value = "posesionNegociada", required = true) Boolean posesionNegociada,
      @RequestParam(value = "idTipo", required = false) Integer idTipo,
      @RequestParam(value = "fecha", required = false) Date fecha,
      @RequestParam(value = "idExpediente", required = false) String idExpediente,
      @RequestParam(value = "idContrato", required = false) String idContrato,
      @RequestParam(value = "tipo", required = false) String tipo,
      @RequestParam(value = "estrategia", required = false) String estrategia,
      @RequestParam(value = "importeRecuperado", required = false) String importeRecuperado,
      @RequestParam(value = "importeSalida", required = false) String importeSalida,
      @RequestParam(value = "fechaEstimadaRecuperacion", required = false) String fechaEstimadaRecuperacion,
      @RequestParam(value = "probabilidadResolucion", required = false) String probabilidadResolucion,
      @RequestParam(value = "gestor", required = false) String gestor,
      @RequestParam(value = "supervisor", required = false) String supervisor,
      @RequestParam(value = "orderField", required = false) String orderField,
      @RequestParam(value = "orderDirection", required = false) String orderDirection,
      @RequestParam(value = "size", required = true) Integer size,
      @RequestParam(value = "page", required = true) Integer page) {
    return portalClienteUseCase.obtenerEstimacionesCarteraFiltradas(
        idCartera,
        posesionNegociada,
        idTipo,
        fecha,
        idExpediente,
        idContrato,
        tipo,
        estrategia,
        importeRecuperado,
        importeSalida,
        fechaEstimadaRecuperacion,
        probabilidadResolucion,
        gestor,
        supervisor,
        orderField,
        orderDirection,
        size,
        page);
  }

  @ApiOperation(
      value = "Obtener estimacion por id",
      notes = "Devuelve la estimacion con id enviados")
  @GetMapping("/estimacion/{idEstimacion}")
  public DetalleEstimacionPCOutputDTO obtenerEstimacion(
      @PathVariable("idEstimacion") Integer idEstimacion) throws NotFoundException {
    return portalClienteUseCase.obtenerEstimacion(idEstimacion);
  }

  @ApiOperation(
      value = "Listado de todos los eventoes",
      notes = "Devuelve todos los eventoes registrados en la base de datos")
  @GetMapping("/{idCartera}/eventos")
  public ListWithCountDTO<EventoOutputDTO> getFilteredEvents(
      @RequestParam(value = "emisorId", required = false) Integer emisorId,
      @RequestParam(value = "destinatarioId", required = false) Integer destinatarioId,
      @RequestParam(value = "nivelId", required = false) Integer nivelId,
      @RequestParam(value = "nivelObjectId", required = false) Integer nivelObjectId,
      @PathVariable(value = "idCartera", required = false) Integer idCartera,
      @RequestParam(value = "fCreaMin", required = false) String fCreaMin,
      @RequestParam(value = "fCreaMax", required = false) String fCreaMax,
      @RequestParam(value = "fAlerMin", required = false) String fAlerMin,
      @RequestParam(value = "fAlerMax", required = false) String fAlerMax,
      @RequestParam(value = "fLimiMin", required = false) String fLimiMin,
      @RequestParam(value = "fLimiMax", required = false) String fLimiMax,
      @RequestParam(value = "clase", required = false) List<Integer> clase,
      @RequestParam(value = "tipo", required = false) Integer tipo,
      @RequestParam(value = "subtipo", required = false) Integer subtipo,
      @RequestParam(value = "subtipoList", required = false) List<String> subtipoList,
      @RequestParam(value = "estado", required = false) List<Integer> estado,
      @RequestParam(value = "importancia", required = false) Integer importancia,
      @RequestParam(value = "resultado", required = false) Integer resultado,
      @RequestParam(value = "titulo", required = false) String titulo,
      @RequestParam(value = "activo", required = false) Boolean activo,
      @RequestParam(value = "revisado", required = false) Boolean revisado,
      @RequestParam(value = "orderField", required = false) String orderField,
      @RequestParam(value = "orderDirection", required = false) String orderDirection,
      @RequestParam(value = "size") Integer size,
      @RequestParam(value = "page") Integer page,
      @ApiIgnore CustomUserDetails principal)
      throws NotFoundException {

    return portalClienteUseCase.findAll(
        null,
        emisorId,
        destinatarioId,
        nivelId,
        nivelObjectId,
        fCreaMin,
        fCreaMax,
        fAlerMin,
        fAlerMax,
        fLimiMin,
        fLimiMax,
        clase,
        tipo,
        subtipo,
        subtipoList,
        idCartera,
        estado,
        importancia,
        resultado,
        titulo,
        activo,
        revisado,
        orderField,
        orderDirection,
        page,
        size);
  }

  @ApiOperation(value = "Añadir un documento a una cartera")
  @PostMapping("/{idCartera}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoCartera(
      @PathVariable Integer idCartera,
      @RequestPart @NotNull @NotBlank MultipartFile file,
      @RequestParam String idMatricula,
      @RequestParam EntidadEnum entidadEnum,
      @ApiIgnore CustomUserDetails principal)
      throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    portalClienteUseCase.createDocumentoPortalCliente(
        idCartera, idMatricula, entidadEnum, file, usuario);
  }

  @ApiOperation(
      value = "Obtener documentación de cliente",
      notes = "Devuelve la documentación disponible del contrato con id enviado")
  @PostMapping("/{idCartera}/documentosObt")
  @Transactional
  public ListWithCountDTO<DocumentoRepositorioDTO> getDocumentosByCliente(
      @PathVariable(value = "idCartera", required = false) Integer idCartera,
      @RequestBody(required = false) BusquedaRepositorioDTO busquedaRepositorioDTO,
      @RequestParam(value="tipoDocumento",required = false) String tipoDocumento,
      @RequestParam(value="id",required = false) String idDocumento,
      @RequestParam(value="usuarioCreador",required = false) String usuarioCreador,
      @RequestParam(value="nombreArchivo",required = false) String nombreDocumento,
      @RequestParam(value = "fechaAlta", required = false) String fechaAlta,
      @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
      @RequestParam(value = "orderField", required = false) String orderField,
      @RequestParam(value = "orderDirection", required = false) String orderDirection,
      @RequestParam EntidadEnum entidadEnum,
      @RequestParam(value = "size") Integer size,
      @RequestParam(value = "page") Integer page)
      throws IOException {
    return portalClienteUseCase.findDocumentosByCliente(busquedaRepositorioDTO, tipoDocumento, idDocumento, usuarioCreador, nombreDocumento, fechaAlta, fechaActualizacion, orderField, orderDirection, idCartera, entidadEnum, size, page);
  }

  @ApiOperation(
      value = "Crea el usuario del cliente en fenix",
      notes = "Crea el usuario del portal cliente en fenix.")
  @PostMapping("/usuario")
  public void createUsuario(
      @RequestParam(value = "idCliente") Integer idCliente,
      @RequestParam(value = "email", required = false) String email,
      @RequestParam(value = "nombre", required = false) String nombre)
      throws NotFoundException {
    portalClienteUseCase.createUsuario(idCliente, email, nombre);
  }

  @ApiOperation(
      value = "Listado de carteras",
      notes = "Listado de carteras asociadas al usuario del cliente en fenix.")
  @GetMapping("/carteras")
  public List<CarteraDTOToList> getCarteras(@RequestParam(value = "idCliente") Integer idCliente)
      throws NotFoundException {
    return portalClienteUseCase.getCarterasCliente(idCliente);
  }

  @ApiOperation(
      value = "Get resultados",
      notes = "Obtiene los resultados diponibles para el evento.")
  @GetMapping("/resultados")
  public List<CatalogoMinInfoDTO> getResultados(@RequestParam(value = "idEvento") Integer idEvento)
      throws NotFoundException {
    return portalClienteUseCase.getResultados(idEvento);
  }

  @ApiOperation(value = "Actualiza el Evento", notes = "Actualiza el resultado del evento.")
  @GetMapping("/actualizar")
  public void actualizarEvento(
      @RequestParam(value = "idEvento") Integer idEvento,
      @RequestParam(value = "idResultado") Integer idResultado,
      @RequestParam(value = "comentario", required = false) String comentario,
      @ApiIgnore CustomUserDetails principal)
      throws NotFoundException, InvalidCodeException, RequiredValueException, IOException {
    portalClienteUseCase.actualizar(idEvento, idResultado, comentario, (Usuario) principal.getPrincipal());
  }

  /*@ApiOperation(
    value = "Listado de objetivos",
    notes = "Listado de objetivos de los expedientes")
  @GetMapping("/seguimiento")
  public List<CarteraDTOToList> getSeguimientoObjetivos(
    @RequestParam(value = "walletId") Integer idCartera, @RequestParam(value = "year") Integer annio) {

    return portalClienteUseCase.getSeguimientoObjetivos(idCartera, annio);
  }*/

  @ApiOperation(value = "Segmentación de la cartera", notes = "Segmentación de la cartera")
  @GetMapping("/segmentacionCartera")
  public List<SegmentacionCarteraDTO> getSegmentacionCartera(
      @RequestParam(value = "walletId") Integer idCartera,
      @RequestParam(value = "idSegmentation") Integer idSegmentacion) {

    return portalClienteUseCase.getSegmentacionCartera(idCartera, idSegmentacion);
  }

  @ApiOperation(value = "Usuarios del Expediente", notes = "Obtiene los usuarios asociados al expediente")
  @GetMapping("/usuariosExpediente")
  public List<UsuarioOutputDTO> getUsuarios(
    @RequestParam(value = "idExpediente") Integer idExpediente,
    @RequestParam(value = "posesionNegociada") Boolean posesionNegociada
  ){
    return portalClienteUseCase.getByExpediente(idExpediente, posesionNegociada);
  }
}
