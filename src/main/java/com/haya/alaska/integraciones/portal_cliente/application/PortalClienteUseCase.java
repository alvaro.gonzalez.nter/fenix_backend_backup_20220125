package com.haya.alaska.integraciones.portal_cliente.application;

import com.haya.alaska.catalogo.domain.enums.EntidadEnum;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoOutputDTO;
import com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto.*;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface PortalClienteUseCase {
  Integer eventoRevisionCliente(
      Usuario logged,
      Integer idExpediente,
      String nombreEmisor,
      Integer destinatarioId,
      String mensaje,
      Boolean posesionNegociada,
      Integer idEvento)
      throws NotFoundException, RequiredValueException, InvalidCodeException, IOException;

  List<PropuestaPCOutputDTO> obtenerPropuestasAgrupadasCartera(Integer idCartera);

  List<DetalleListadoPropuestaPCOutputDTO> obtenerPropuestasCarteraEstado(
      Integer idCartera, Integer idEstado);

  List<DetalleListadoPropuestaPCOutputDTO> obtenerPropuestasCarteraTipo(
      Integer idCartera, Integer idTipo);

  List<EstimacionPCOutputDTO> obtenerEstimacionesAgrupadasCartera(String filtro, Integer idCartera);

  List<DetalleListadoEstimacionPCOutputDTO> obtenerEstimacionesCarteraValoracion(
      Integer idCartera, Integer idTipo);

  List<DetalleListadoEstimacionPCOutputDTO> obtenerEstimacionesCarteraBien(
      Integer idCartera, Integer idTipo);

  DetalleEstimacionPCOutputDTO obtenerEstimacion(Integer idEstimacion) throws NotFoundException;

  ListWithCountDTO<EventoOutputDTO> findAll(
      Integer loggedUser,
      Integer emisorId,
      Integer destinatarioId,
      Integer nivelId,
      Integer nivelObjectId,
      String fCreaMin,
      String fCreaMax,
      String fAlerMin,
      String fAlerMax,
      String fLimiMin,
      String fLimiMax,
      List<Integer> clase,
      Integer tipo,
      Integer subtipo,
      List<String> subtipoList,
      Integer cartera,
      List<Integer> estado,
      Integer importancia,
      Integer resultado,
      String titulo,
      Boolean activo,
      Boolean revisado,
      String orderField,
      String orderDirection,
      Integer page,
      Integer size)
      throws NotFoundException;

  ListWithCountDTO<DetalleListadoPropuestaPCOutputDTO> obtenerPropuestasCarteraEstadoFiltradas(
    Integer idCartera,
    Integer idEstado,
    Integer idTipo,
    String idExpediente,
    String fase,
    String estado,
    String gestorExpediente,
    String primerTitular,
    String estrategia,
    String saldoGestion,
    String fechaElevacion,
    String supervisor,
    Boolean documentos, String orderField,
    String orderDirection,
    Integer size,
    Integer page);

  ListWithCountDTO<DetalleListadoEstimacionPCOutputDTO> obtenerEstimacionesCarteraFiltradas(
    Integer idCartera,
    Boolean posesionNegociada,
    Integer idTipo,
    Date fecha,
    String idExpediente,
    String idContrato,
    String tipo,
    String estrategia,
    String importeRecuperado,
    String importeSalida,
    String fechaEstimadaRecuperacion,
    String probabilidadResolucion,
    String gestor,
    String supervisor,
    String orderField,
    String orderDirection,
    Integer size,
    Integer page);

  void createDocumentoPortalCliente(
      Integer idCartera,
      String idMatricula,
      EntidadEnum entidadEnum,
      MultipartFile file,
      Usuario usuario)
      throws Exception;

  ListWithCountDTO<DocumentoRepositorioDTO> findDocumentosByCliente(BusquedaRepositorioDTO busquedaRepositorioDTO, String tipoDocumento, String idDocumento, String usuarioCreador, String nombreDocumento, String fechaAlta, String fechaActualizacion, String orderField, String orderDirection,
                                                                    Integer idCliente, EntidadEnum entidadEnum, Integer size, Integer page) throws IOException;

  void createUsuario(Integer idCliente, String email, String nombre) throws NotFoundException;

  List<CarteraDTOToList> getCarterasCliente(Integer idCliente) throws NotFoundException;

  List<CatalogoMinInfoDTO> getResultados(Integer idEvento) throws NotFoundException;

  void actualizar(Integer idEvento, Integer idResultado, String comentario, Usuario logged)
      throws NotFoundException, InvalidCodeException, RequiredValueException, IOException;

  List<SegmentacionCarteraDTO> getSegmentacionCartera(Integer idCartera, Integer idSegmentacion);
  List<UsuarioOutputDTO> getByExpediente(Integer idEx, Boolean posesionNegociada);
}
