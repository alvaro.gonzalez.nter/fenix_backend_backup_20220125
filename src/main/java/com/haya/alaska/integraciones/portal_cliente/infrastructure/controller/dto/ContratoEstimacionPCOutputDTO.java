package com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContratoEstimacionPCOutputDTO implements Serializable {
  private Boolean contratoPrincipal;
  private String idContrato;
  private String categoria;
  private String primerTitular;
  private Double importe;
  private Double importeSalida;
}
