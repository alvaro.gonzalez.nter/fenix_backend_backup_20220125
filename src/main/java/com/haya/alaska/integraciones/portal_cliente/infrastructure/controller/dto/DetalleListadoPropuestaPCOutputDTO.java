package com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class DetalleListadoPropuestaPCOutputDTO implements Serializable {
  private String idExpediente;
  private String idExpedienteConcatenado;
  private String idPropuesta;
  private String resolucion;
  private String estado;
  private String gestorExpediente;
  private String supervisorExpediente;
  private String primerTitular;
  private String estrategia;
  private String saldoGestion;
  private Date fechaElevacion;
  private Boolean documentos;
  private Boolean posesionNegociada;
  private Integer idTipoPropuesta;
  private String valorTipoPropuesta;
}
