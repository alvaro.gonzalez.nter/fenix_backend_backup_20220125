package com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author aarauz creaated on 6/4/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class SegmentacionCarteraDTO implements Serializable {

  Integer expedientes;
  Double saldoEnGestion;
  Integer contratos;

}
