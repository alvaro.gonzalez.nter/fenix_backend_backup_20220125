package com.haya.alaska.integraciones.portal_cliente.infrastructure.mapper;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.importe_estimacion_contrato.domain.ImporteEstimacionContrato;
import com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto.*;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class PortalClienteMapper {

  public static PropuestaPCOutputDTO parseToPropuestaPCOutputDTO(Propuesta propuesta) {
    PropuestaPCOutputDTO propuestaPCOutputDTO = new PropuestaPCOutputDTO();
    if (propuesta != null) {
      propuestaPCOutputDTO.setEstado(
          propuesta.getEstadoPropuesta() != null
              ? new CatalogoMinInfoDTO(propuesta.getEstadoPropuesta())
              : null);
      propuestaPCOutputDTO.setTipo(
          propuesta.getTipoPropuesta() != null
              ? new CatalogoMinInfoDTO(propuesta.getTipoPropuesta())
              : null);
      propuestaPCOutputDTO.setImporteRecuperado(String.valueOf(propuesta.getImporteTotal()));
      double importeSalida = 0;
      if (!propuesta.getContratos().isEmpty()) {
        for (Contrato contrato : propuesta.getContratos()) {
          importeSalida += contrato.getSaldoGestion();
        }
      }
      propuestaPCOutputDTO.setImporteSalida(String.valueOf(importeSalida));
    }
    return propuestaPCOutputDTO;
  }

  public static DetalleListadoPropuestaPCOutputDTO parseToDetalleListadoPropuestaPCOutputDTO(
      Propuesta propuesta, boolean documentos) {
    DetalleListadoPropuestaPCOutputDTO detalleListadoPropuestaPCOutputDTO =
        new DetalleListadoPropuestaPCOutputDTO();
    if (propuesta != null) {
      if (propuesta.getExpediente() != null) {
        detalleListadoPropuestaPCOutputDTO.setIdExpediente(
            String.valueOf(propuesta.getExpediente().getId()));
        detalleListadoPropuestaPCOutputDTO.setIdExpedienteConcatenado(
            String.valueOf(propuesta.getExpediente().getIdConcatenado()));
      } else if (propuesta.getExpedientePosesionNegociada() != null) {
        detalleListadoPropuestaPCOutputDTO.setIdExpediente(
            String.valueOf(propuesta.getExpedientePosesionNegociada().getId()));
        detalleListadoPropuestaPCOutputDTO.setIdExpedienteConcatenado(
            String.valueOf(propuesta.getExpedientePosesionNegociada().getIdConcatenado()));
      }

      detalleListadoPropuestaPCOutputDTO.setIdPropuesta(String.valueOf(propuesta.getId()));

      detalleListadoPropuestaPCOutputDTO.setResolucion(
          propuesta.getResolucionPropuesta() != null
              ? propuesta.getResolucionPropuesta().getValor()
              : null);

      detalleListadoPropuestaPCOutputDTO.setEstado(
          propuesta.getEstadoPropuesta() != null
              ? propuesta.getEstadoPropuesta().getValor()
              : null);

      if (propuesta.getExpediente() != null && propuesta.getExpediente().getGestor() != null) {
        detalleListadoPropuestaPCOutputDTO.setGestorExpediente(
            propuesta.getExpediente().getGestor().getNombre());
      } else if (propuesta.getExpedientePosesionNegociada() != null
          && propuesta.getExpedientePosesionNegociada().getGestor() != null) {
        detalleListadoPropuestaPCOutputDTO.setGestorExpediente(
            propuesta.getExpedientePosesionNegociada().getGestor().getNombre());
      }

      if (propuesta.getExpediente() != null && propuesta.getExpediente().getUsuarioByPerfil("Supervisor") != null) {
        detalleListadoPropuestaPCOutputDTO.setSupervisorExpediente(
            propuesta.getExpediente().getUsuarioByPerfil("Supervisor").getNombre());
      } else if (propuesta.getExpedientePosesionNegociada() != null
          && propuesta.getExpedientePosesionNegociada().getUsuarioByPerfil("Supervisor") != null) {
        detalleListadoPropuestaPCOutputDTO.setSupervisorExpediente(
            propuesta.getExpedientePosesionNegociada().getUsuarioByPerfil("Supervisor").getNombre());
      }

      detalleListadoPropuestaPCOutputDTO.setPrimerTitular(null);

      if (propuesta.getExpediente() != null
          && propuesta.getExpediente().getContratoRepresentante() != null
          && propuesta.getExpediente().getContratoRepresentante().getPrimerInterviniente()
              != null) {
        Interviniente temp = propuesta.getExpediente().getContratoRepresentante().getPrimerInterviniente();
        detalleListadoPropuestaPCOutputDTO.setPrimerTitular(temp.getNombre() + " " + temp.getApellidos());
      } else if (propuesta.getExpedientePosesionNegociada() != null
          && propuesta.getOcupantes() != null) {
        for (Ocupante ocupante : propuesta.getOcupantes()) {
          if (Objects.nonNull(ocupante.getBienes())) {
            for (OcupanteBien ocupanteBien : ocupante.getBienes()) {
              if (ocupanteBien.getOrden() == 1) {
                if (Objects.nonNull(ocupanteBien.getTipoOcupante())
                    && ocupanteBien.getTipoOcupante().getCodigo().equals("TIT")) {
                  detalleListadoPropuestaPCOutputDTO.setPrimerTitular(
                      ocupanteBien.getOcupante().getNombreCompleto());
                  break;
                }
              }
            }
          }
        }
      }

      detalleListadoPropuestaPCOutputDTO.setEstrategia(
          propuesta.getEstrategia() != null && propuesta.getEstrategia().getEstrategia() != null
              ? propuesta.getEstrategia().getEstrategia().getValor()
              : null);

      double importeSalida = 0;
      if (propuesta.getExpediente() != null && !propuesta.getContratos().isEmpty()) {
        for (Contrato contrato : propuesta.getContratos()) {
          importeSalida += contrato.getSaldoGestion();
        }
      } else if (propuesta.getExpedientePosesionNegociada() != null
          && !propuesta.getBienes().isEmpty()) {
        for (PropuestaBien propuestaBien : propuesta.getBienes()) {
          if (Objects.nonNull(propuestaBien.getBien())) {
            importeSalida += propuestaBien.getBien().getImporteBien();
          }
        }
      }
      detalleListadoPropuestaPCOutputDTO.setSaldoGestion(String.valueOf(importeSalida));

      detalleListadoPropuestaPCOutputDTO.setFechaElevacion(propuesta.getFechaElevacion());

      detalleListadoPropuestaPCOutputDTO.setDocumentos(documentos);

      if (propuesta.getExpediente() != null) {
        detalleListadoPropuestaPCOutputDTO.setPosesionNegociada(false);
      } else if (propuesta.getExpedientePosesionNegociada() != null) {
        detalleListadoPropuestaPCOutputDTO.setPosesionNegociada(true);
      }

      if (propuesta.getTipoPropuesta() != null) {
        detalleListadoPropuestaPCOutputDTO.setValorTipoPropuesta(
            propuesta.getTipoPropuesta().getValor());

        detalleListadoPropuestaPCOutputDTO.setIdTipoPropuesta(propuesta.getTipoPropuesta().getId());
      }
    }
    return detalleListadoPropuestaPCOutputDTO;
  }

  public static EstimacionPCOutputDTO parseToEstimacionPCOutputDTO(Estimacion estimacion) {
    EstimacionPCOutputDTO estimacionPCOutputDTO = new EstimacionPCOutputDTO();
    if (estimacion != null) {
      estimacionPCOutputDTO.setFecha(estimacion.getFechaEstimada());
      estimacionPCOutputDTO.setTipoPropuesta(
          estimacion.getTipo() != null ? new CatalogoMinInfoDTO(estimacion.getTipo()) : null);
      estimacionPCOutputDTO.setImporte(
          estimacion.getImporteSalida() != null ? estimacion.getImporteSalida() : null);
    }
    return estimacionPCOutputDTO;
  }

  public static DetalleListadoEstimacionPCOutputDTO parseToDetalleListadoEstimacionPCOutputDTO(
      Estimacion estimacion) {
    DecimalFormat df = new DecimalFormat("#0.00");
    df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));

    DetalleListadoEstimacionPCOutputDTO detalleListadoEstimacionPCOutputDTO =
        new DetalleListadoEstimacionPCOutputDTO();
    if (estimacion != null) {
      detalleListadoEstimacionPCOutputDTO.setIdEstimacion(estimacion.getId());

      detalleListadoEstimacionPCOutputDTO.setFecha(estimacion.getFecha());

      detalleListadoEstimacionPCOutputDTO.setTipo(
          estimacion.getTipo() != null ? estimacion.getTipo().getValor() : null);

      detalleListadoEstimacionPCOutputDTO.setEstrategia(
          estimacion.getEstrategia() != null ? estimacion.getEstrategia() : null);

      detalleListadoEstimacionPCOutputDTO.setImporteRecuperado(
          estimacion.getImporteRecuperado() != null
              ?  df.format(estimacion.getImporteRecuperado())
              : null);

      detalleListadoEstimacionPCOutputDTO.setImporteSalida(
          estimacion.getImporteSalida() != null
              ? df.format(estimacion.getImporteSalida())
              : null);

      detalleListadoEstimacionPCOutputDTO.setFechaEstimadaRecuperacion(
          estimacion.getFechaEstimada());

      detalleListadoEstimacionPCOutputDTO.setProbabilidadResolucion(
          estimacion.getProbabilidadResolucionString());

      detalleListadoEstimacionPCOutputDTO.setCumplida(
          estimacion.getCumplida() != null ? estimacion.getCumplida().getValor() : null);

      if (estimacion.getExpediente() != null) {
        detalleListadoEstimacionPCOutputDTO.setIdExpediente(
            String.valueOf(estimacion.getExpediente().getId()));

        if (estimacion.getExpediente().getGestor() != null) {
          detalleListadoEstimacionPCOutputDTO.setGestor(
              estimacion.getExpediente().getGestor().getNombre());
        }
        if (estimacion.getExpediente().getUsuarioByPerfil("Supervisor") != null) {
          detalleListadoEstimacionPCOutputDTO.setSupervisor(
              estimacion.getExpediente().getUsuarioByPerfil("Supervisor").getNombre());
        }
      } else if (estimacion.getExpedientePosesionNegociada() != null) {
        detalleListadoEstimacionPCOutputDTO.setIdExpediente(
            String.valueOf(estimacion.getExpedientePosesionNegociada().getId()));

        if (estimacion.getExpedientePosesionNegociada().getGestor() != null) {
          detalleListadoEstimacionPCOutputDTO.setGestor(
              estimacion.getExpedientePosesionNegociada().getGestor().getNombre());
        }
        if (estimacion.getExpedientePosesionNegociada().getUsuarioByPerfil("Supervisor") != null) {
          detalleListadoEstimacionPCOutputDTO.setSupervisor(
            estimacion.getExpedientePosesionNegociada().getUsuarioByPerfil("Supervisor").getNombre());
        }
      }

      detalleListadoEstimacionPCOutputDTO.setDocumentos(null);

      if (estimacion.getImportesContratos().isEmpty()
          && estimacion.getImportesBienesPosesionNegociada().isEmpty()) {
        if (estimacion.getExpediente() != null) {
          detalleListadoEstimacionPCOutputDTO.setContratoExpediente(
              "E" + estimacion.getExpediente().getIdConcatenado());
        }
        if (estimacion.getExpedientePosesionNegociada() != null) {
          detalleListadoEstimacionPCOutputDTO.setContratoExpediente(
              "E" + estimacion.getExpedientePosesionNegociada().getIdConcatenado());
        }
      } else {
        if (!estimacion.getImportesContratos().isEmpty()) {
          detalleListadoEstimacionPCOutputDTO.setContratoExpediente(
              estimacion.getImportesContratos().stream()
                  .map(ic -> "C" + ic.getContrato().getIdCarga())
                  .collect(Collectors.joining(", ")));
        }
        if (!estimacion.getImportesBienesPosesionNegociada().isEmpty()) {
          detalleListadoEstimacionPCOutputDTO.setContratoExpediente(
              estimacion.getImportesBienesPosesionNegociada().stream()
                  .map(ic -> "B" + ic.getBienPosesionNegociada().getId())
                  .collect(Collectors.joining(", ")));
        }
      }
    }
    return detalleListadoEstimacionPCOutputDTO;
  }

  public static DetalleEstimacionPCOutputDTO parseToDetalleEstimacionPCOutputDTO(
      Estimacion estimacion) {
    DetalleEstimacionPCOutputDTO detalleEstimacionPCOutputDTO = new DetalleEstimacionPCOutputDTO();
    if (estimacion != null) {
      detalleEstimacionPCOutputDTO.setIdEstimacion(estimacion.getId());
      if (estimacion.getExpediente() != null) {
        detalleEstimacionPCOutputDTO.setIdExpediente(estimacion.getExpediente().getId());
        detalleEstimacionPCOutputDTO.setIdExpedienteConcatenado(
            estimacion.getExpediente().getIdConcatenado());
      } else if (estimacion.getExpedientePosesionNegociada() != null) {
        detalleEstimacionPCOutputDTO.setIdExpediente(
            estimacion.getExpedientePosesionNegociada().getId());
        detalleEstimacionPCOutputDTO.setIdExpedienteConcatenado(
            estimacion.getExpedientePosesionNegociada().getIdConcatenado());
      }

      detalleEstimacionPCOutputDTO.setProcedimientoJudicial(estimacion.getProcedimientoJudicial());
      detalleEstimacionPCOutputDTO.setImporteRecuperado(estimacion.getImporteRecuperado());
      detalleEstimacionPCOutputDTO.setImporteSalida(estimacion.getImporteSalida());
      if (estimacion.getIntervalo() != null) {
        detalleEstimacionPCOutputDTO.setIntervaloEstimado(estimacion.getIntervalo().getValor());
      }
      detalleEstimacionPCOutputDTO.setFechaEstimada(estimacion.getFechaEstimada());
      detalleEstimacionPCOutputDTO.setProbabilidadResolucion(
          estimacion.getProbabilidadResolucionString());
      if (estimacion.getTipo() != null) {
        detalleEstimacionPCOutputDTO.setTipoEstrategia(estimacion.getTipo().getValor());
      }
      if (estimacion.getSubtipo() != null) {
        detalleEstimacionPCOutputDTO.setSubtipoEstrategia(estimacion.getSubtipo().getValor());
      }
      detalleEstimacionPCOutputDTO.setTipoSolucion(estimacion.getTipoSolucion());
      detalleEstimacionPCOutputDTO.setCumplimiento(estimacion.getCumplimiento());
      if (estimacion.getCumplida() != null) {
        detalleEstimacionPCOutputDTO.setCumplida(estimacion.getCumplida().getValor());
      }
      detalleEstimacionPCOutputDTO.setComentarios(estimacion.getComentarios());

      List<ContratoEstimacionPCOutputDTO> contratos = new ArrayList<>();
      for (ImporteEstimacionContrato importeEstimacionContrato :
          estimacion.getImportesContratos()) {
        if (importeEstimacionContrato.getContrato() != null) {
          ContratoEstimacionPCOutputDTO contrato =
              new ContratoEstimacionPCOutputDTO(
                  importeEstimacionContrato.getContrato().getEsRepresentante(),
                  importeEstimacionContrato.getContrato().getIdCarga(),
                  null,
                  null,
                  importeEstimacionContrato.getImporteTotal(),
                  importeEstimacionContrato.getImporteTotalSalida());

          if (importeEstimacionContrato.getContrato().getProducto() != null) {
            contrato.setCategoria(importeEstimacionContrato.getContrato().getProducto().getValor());
          }

          if (importeEstimacionContrato.getContrato().getPrimerInterviniente() != null) {
            contrato.setPrimerTitular(
                importeEstimacionContrato.getContrato().getPrimerInterviniente().getNombreReal());
          }

          contratos.add(contrato);
        }
      }
      detalleEstimacionPCOutputDTO.setContratos(contratos);

      if (estimacion.getImportesContratos().isEmpty()
          && estimacion.getImportesBienesPosesionNegociada().isEmpty()) {
        detalleEstimacionPCOutputDTO.setNivelEstimacionExpediente(true);
      } else {
        detalleEstimacionPCOutputDTO.setNivelEstimacionExpediente(false);
      }
    }
    return detalleEstimacionPCOutputDTO;
  }
}
