package com.haya.alaska.integraciones.portal_cliente.application;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente_;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada_;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.Bien_;
import com.haya.alaska.cartera.application.CarteraUseCase;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.domain.enums.EntidadEnum;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente_;
import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import com.haya.alaska.estado_evento.infrastructure.repository.EstadoEventoRepository;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta_;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion.domain.Estimacion_;
import com.haya.alaska.estimacion.infrastructure.repository.EstimacionRepository;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.estrategia.domain.Estrategia_;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada_;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoOutputDTO;
import com.haya.alaska.evento.infrastructure.mapper.EventoMapper;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada_;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.application.GestorDocumentalUseCase;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto.*;
import com.haya.alaska.integraciones.portal_cliente.infrastructure.mapper.PortalClienteMapper;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.domain.Interviniente_;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.domain.Ocupante_;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien_;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.domain.Perfil_;
import com.haya.alaska.perfil.infrastructure.repository.PerfilRepository;
import com.haya.alaska.propuesta.aplication.PropuestaUseCase;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.domain.Propuesta_;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien_;
import com.haya.alaska.resolucion_propuesta.domain.ResolucionPropuesta;
import com.haya.alaska.resolucion_propuesta.domain.ResolucionPropuesta_;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoContenedorInputDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.tipo_ocupante.domain.TipoOcupante;
import com.haya.alaska.tipo_ocupante.domain.TipoOcupante_;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta_;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.mapper.UsuarioMapper;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class PortalClienteUseCaseImpl implements PortalClienteUseCase {
  @PersistenceContext private EntityManager entityManager;
  @Autowired EventoUseCase eventoUseCase;
  @Autowired EventoMapper eventoMapper;
  @Autowired EventoRepository eventoRepository;
  @Autowired SubtipoEventoRepository subtipoEventoRepository;
  @Autowired EstadoEventoRepository estadoEventoRepository;

  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;

  @Autowired private PropuestaRepository propuestaRepository;
  @Autowired private PropuestaUseCase propuestaUseCase;
  @Autowired private EstimacionRepository estimacionRepository;
  @Autowired ClienteRepository clienteRepository;
  @Autowired CarteraRepository carteraRepository;
  @Autowired GestorDocumentalUseCase gestorDocumentalUseCase;
  @Autowired private ServicioGestorDocumental servicioGestorDocumental;
  @Autowired private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired private UsuarioRepository usuarioRepository;
  @Autowired private UsuarioMapper usuarioMapper;
  @Autowired private PerfilRepository perfilRepository;
  @Autowired private CarteraUseCase carteraUseCase;

  public Integer eventoRevisionCliente(
      Usuario logged,
      Integer idExpediente,
      String nombreEmisor,
      Integer destinatarioId,
      String mensaje,
      Boolean posesionNegociada,
      Integer idEvento)
      throws NotFoundException, RequiredValueException, InvalidCodeException, IOException {
    EventoInputDTO newInput = getInputRevisionCliente();

    Integer nivel;
    Integer idCartera;
    if (posesionNegociada) {
      nivel = 7;
      ExpedientePosesionNegociada epn =
          expedientePosesionNegociadaRepository
              .findById(idExpediente)
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "expedientePosesiónNegociada", idExpediente));
      idCartera = epn.getCartera().getId();
    } else {
      nivel = 1;
      Expediente e =
          expedienteRepository
              .findById(idExpediente)
              .orElseThrow(
                  () -> new NotFoundException("expediente", idExpediente));
      idCartera = e.getCartera().getId();
    }
    // El niveliD es el nivel del interviniente y el nivel es el 4
    newInput.setNivel(nivel);
    // La cartera no vendría mal (a partir del interviniente si tiene varias escojo el primer
    // expediente)
    newInput.setCartera(idCartera);
    newInput.setNivelId(idExpediente);
    newInput.setIdExpediente(idExpediente);

    // Necesito la cartera para el subtipoEvento están duplicados por cartera coger el de mi cartera
    // PDN
    SubtipoEvento se =
        subtipoEventoRepository
            .findByCodigoAndCarteraId("CLI_01", idCartera)
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "subtipoEvento", "código", "'CLI_01'", "cartera", idCartera));
    newInput.setSubtipoEvento(se.getId());
    TipoEvento te = se.getTipoEvento();
    newInput.setTipoEvento(te.getId());
    ClaseEvento ce = te.getClaseEvento();
    newInput.setClaseEvento(ce.getId());
    // eL GESTOR DESTINATARIO
    newInput.setDestinatario(destinatarioId);

    String descripcion = nombreEmisor;
    descripcion += " ha escrrito este mensaje: \n";
    descripcion += mensaje;
    newInput.setComentariosDestinatario(descripcion);

    EventoOutputDTO output;
    if (idEvento == null) {
      output = eventoUseCase.create(newInput, logged);
    } else {
      output = eventoUseCase.update(idEvento, newInput, logged);
    }

    return output.getId();
  }

  private EventoInputDTO getInputRevisionCliente() throws NotFoundException {
    EventoInputDTO newInput = new EventoInputDTO();

    newInput.setTitulo("Revisión desde Portal Cliente");

    newInput.setImportancia(2); // Media importancia
    newInput.setPeriodicidad(null);
    // Revisamos el estado evento pendiente
    EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("PEN").orElse(null);
    if (estadoEvento == null)
      throw new NotFoundException("estado evento", "código", "'PEN'");
    newInput.setEstado(estadoEvento.getId());
    newInput.setResultado(null);

    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.YEAR, 100);
    newInput.setFechaLimite(c.getTime());

    newInput.setFechaAlerta(new Date());
    newInput.setProgramacion(false);
    newInput.setCorreo(false);
    newInput.setAutogenerado(false);
    newInput.setRevisado(false);

    return newInput;
  }

  @Override
  public List<PropuestaPCOutputDTO> obtenerPropuestasAgrupadasCartera(Integer idCartera) {
    return propuestaRepository
        .findAllByExpedienteCarteraIdOrExpedientePosesionNegociadaCarteraId(idCartera, idCartera)
        .stream()
        .map(PortalClienteMapper::parseToPropuestaPCOutputDTO)
        .collect(Collectors.toList());
  }

  @Override
  public List<DetalleListadoPropuestaPCOutputDTO> obtenerPropuestasCarteraEstado(
      Integer idCartera, Integer idEstado) {
    return propuestaRepository
        .findAllByEstadoPropuestaIdAndExpedienteCarteraIdOrExpedientePosesionNegociadaCarteraId(
            idEstado, idCartera, idCartera)
        .stream()
        .map(
            propuesta -> {
              try {
                return PortalClienteMapper.parseToDetalleListadoPropuestaPCOutputDTO(
                    propuesta,
                    propuestaUseCase.findDocumentosByPropuesta(propuesta.getId()).isEmpty());
              } catch (NotFoundException e) {
                e.printStackTrace();
              } catch (IOException e) {
                e.printStackTrace();
              }
              return null;
            })
        .collect(Collectors.toList());
  }

  @Override
  public List<DetalleListadoPropuestaPCOutputDTO> obtenerPropuestasCarteraTipo(
      Integer idCartera, Integer idTipo) {
    return propuestaRepository
        .findAllByTipoPropuestaIdAndExpedienteCarteraIdOrExpedientePosesionNegociadaCarteraId(
            idTipo, idCartera, idCartera)
        .stream()
        .map(
            propuesta -> {
              try {
                return PortalClienteMapper.parseToDetalleListadoPropuestaPCOutputDTO(
                    propuesta,
                    propuestaUseCase.findDocumentosByPropuesta(propuesta.getId()).isEmpty());
              } catch (NotFoundException e) {
                e.printStackTrace();
              } catch (IOException e) {
                e.printStackTrace();
              }
              return null;
            })
        .collect(Collectors.toList());
  }

  @Override
  public List<EstimacionPCOutputDTO> obtenerEstimacionesAgrupadasCartera(
      String filtro, Integer idCartera) {
    if (filtro.equals("Valoracion")) {
      return estimacionRepository.findAllByExpedienteCarteraId(idCartera).stream()
          .map(PortalClienteMapper::parseToEstimacionPCOutputDTO)
          .collect(Collectors.toList());
    } else {
      return estimacionRepository.findAllByExpedientePosesionNegociadaCarteraId(idCartera).stream()
          .map(PortalClienteMapper::parseToEstimacionPCOutputDTO)
          .collect(Collectors.toList());
    }
  }

  @Override
  public List<DetalleListadoEstimacionPCOutputDTO> obtenerEstimacionesCarteraValoracion(
      Integer idCartera, Integer idTipo) {
    return estimacionRepository.findAllByExpedienteCarteraIdAndTipoId(idCartera, idTipo).stream()
        .map(PortalClienteMapper::parseToDetalleListadoEstimacionPCOutputDTO)
        .collect(Collectors.toList());
  }

  @Override
  public List<DetalleListadoEstimacionPCOutputDTO> obtenerEstimacionesCarteraBien(
      Integer idCartera, Integer idTipo) {
    return estimacionRepository
        .findAllByExpedientePosesionNegociadaCarteraIdAndTipoId(idCartera, idTipo).stream()
        .map(PortalClienteMapper::parseToDetalleListadoEstimacionPCOutputDTO)
        .collect(Collectors.toList());
  }

  @Override
  public DetalleEstimacionPCOutputDTO obtenerEstimacion(Integer idEstimacion)
      throws NotFoundException {
    Estimacion estimacion =
        estimacionRepository
            .findById(idEstimacion)
            .orElseThrow(
                () -> new NotFoundException("estimacion", idEstimacion));
    return new PortalClienteMapper().parseToDetalleEstimacionPCOutputDTO(estimacion);
  }

  @Override
  public ListWithCountDTO<EventoOutputDTO> findAll(
      Integer loggedUser,
      Integer emisorId,
      Integer destinatarioId,
      Integer nivelId,
      Integer nivelObjectId,
      String fCreaMin,
      String fCreaMax,
      String fAlerMin,
      String fAlerMax,
      String fLimiMin,
      String fLimiMax,
      List<Integer> clase,
      Integer tipo,
      Integer subtipo,
      List<String> subtipoList,
      Integer cartera,
      List<Integer> estado,
      Integer importancia,
      Integer resultado,
      String titulo,
      Boolean activo,
      Boolean revisado,
      String orderField,
      String orderDirection,
      Integer page,
      Integer size)
      throws NotFoundException {
    List<String> subtiposCodigos = new ArrayList<>();
    if (subtipoList != null) {
      for (String s : subtipoList) {
        switch (s.toUpperCase()) {
          case "COMITES":
            subtiposCodigos.add("CLI_05");
            break;
          case "REPORTES":
            subtiposCodigos.add("CLI_04");
            break;
          case "FACTURACION":
            subtiposCodigos.add("CLI_02");
            break;
          case "REVISION":
            subtiposCodigos.add("CLI_01");
            break;
          case "SANCIONES":
            subtiposCodigos.add("REV07");//revision cliente
            break;
        }
      }
    }

    List<Integer> subtiposIds = new ArrayList<>();
    for (String s : subtiposCodigos) {
      SubtipoEvento se = subtipoEventoRepository.findByCodigoAndCarteraId(s, cartera).orElse(null);
      if (se != null) subtiposIds.add(se.getId());
    }

    return eventoUseCase.findAll(
        null,
        emisorId,
        destinatarioId,
        nivelId,
        nivelObjectId,
        getFecha(fCreaMin),
        getFecha(fCreaMax),
        getFecha(fAlerMin),
        getFecha(fAlerMax),
        getFecha(fLimiMin),
        getFecha(fLimiMax),
        clase,
        tipo,
        subtipo,
        subtiposIds,
        cartera,
        estado,
        importancia,
        resultado,
        titulo,
        activo,
        revisado,
        orderField,
        orderDirection,
        page,
        size);
  }

  @Override
  public ListWithCountDTO<DetalleListadoPropuestaPCOutputDTO> obtenerPropuestasCarteraEstadoFiltradas(
    Integer idCartera,
    Integer idEstado,
    Integer idTipo,
    String idExpediente,
    String fase,
    String estado,
    String gestorExpediente,
    String primerTitular,
    String estrategia,
    String saldoGestion,
    String fechaElevacion,
    String supervisor, Boolean documentos, String orderField,
    String orderDirection,
    Integer size,
    Integer page) {
    List<Propuesta> result = querysPropuestaGA(idCartera, idEstado, idTipo, idExpediente, fase,estado, gestorExpediente,primerTitular,
                                                                    estrategia,saldoGestion, fechaElevacion,supervisor,documentos, orderField,orderDirection,size,page);
    result.addAll(querysPropuestaPN(idCartera, idEstado, idTipo, idExpediente, fase,estado, gestorExpediente,primerTitular,
                                    estrategia,saldoGestion, fechaElevacion,supervisor,documentos, orderField,orderDirection,size,page));

    List<Propuesta> propuestasFinales = result.stream().skip(size * page).limit(size).collect(Collectors.toList());

    List<DetalleListadoPropuestaPCOutputDTO> propuestasMapeadas = propuestasFinales.stream()
      .map(
        propuesta -> {
          try {
            return PortalClienteMapper.parseToDetalleListadoPropuestaPCOutputDTO(
              propuesta,
              propuestaUseCase.countDocumentosByPropuesta(propuesta.getId()) == 0);
          } catch (NotFoundException e) {
            e.printStackTrace();
          } catch (IOException e) {
            e.printStackTrace();
          }
          return null;
        })
      .collect(Collectors.toList());


    if(orderDirection.compareTo("asc") == 0)
    {
      if(orderField.compareTo("idExpedienteConcatenado") == 0)      propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getIdExpedienteConcatenado, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("estadoPropuesta") == 0)         propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getEstado, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("supervisorExpediente") == 0)    propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getSupervisorExpediente, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("primerTitular") == 0)           propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getPrimerTitular, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("estrategia") == 0)              propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getEstrategia, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("saldoGestion") == 0)            propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getSaldoGestion, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("fechaElevacion") == 0)          propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getFechaElevacion, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("documentos") == 0)              propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getDocumentos, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
    }
    else if(orderDirection.compareTo("desc") == 0)
    {
      if(orderField.compareTo("idExpedienteConcatenado") == 0)      propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getIdExpedienteConcatenado, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("estadoPropuesta") == 0)         propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getEstado, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("supervisorExpediente") == 0)    propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getSupervisorExpediente, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("primerTitular") == 0)           propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getPrimerTitular, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("estrategia") == 0)              propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getEstrategia, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("saldoGestion") == 0)            propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getSaldoGestion, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("fechaElevacion") == 0)          propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getFechaElevacion, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("documentos") == 0)              propuestasMapeadas = propuestasMapeadas.stream().sorted(Comparator.comparing(DetalleListadoPropuestaPCOutputDTO::getDocumentos, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
    }

    return new ListWithCountDTO<>(propuestasMapeadas, result.size());
  }

  private List<Propuesta> querysPropuestaGA(Integer idCartera, Integer idEstado, Integer idTipo, String idExpediente, String resolucion,
                                                                     String estado, String gestorExpediente, String primerTitular, String estrategia, String saldoGestion,
                                                                     String fechaElevacion, String supervisor, Boolean documentos, String orderField, String orderDirection, Integer size, Integer page)
  {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Propuesta> query = cb.createQuery(Propuesta.class);

    Root<Propuesta> root = query.from(Propuesta.class);

    List<Predicate> predicates = new ArrayList<>();

    Join<Propuesta, Expediente> joinExp = root.join(Propuesta_.EXPEDIENTE, JoinType.INNER);
    Join<Expediente, Cartera> joinExpCartera = joinExp.join(Expediente_.CARTERA, JoinType.INNER);
    predicates.add(cb.equal(cb.upper(joinExpCartera.get(Cartera_.ID)), idCartera));

    if (idEstado != null) {
      Join<Propuesta, EstadoPropuesta> joinFiltroEstado =
        root.join(Propuesta_.ESTADO_PROPUESTA, JoinType.INNER);

      predicates.add(cb.equal(cb.upper(joinFiltroEstado.get(EstadoPropuesta_.ID)), idEstado));
    }
    if (idTipo != null) {
      Join<Propuesta, TipoPropuesta> joinFiltroTipo =
        root.join(Propuesta_.TIPO_PROPUESTA, JoinType.INNER);

      predicates.add(cb.equal(cb.upper(joinFiltroTipo.get(TipoPropuesta_.ID)), idTipo));
    }

    if (idExpediente != null) {
      predicates.add(
        cb.like(joinExp.get(Expediente_.ID_CONCATENADO).as(String.class),"%" + idExpediente + "%"));
    }

    if (resolucion != null) {
      Join<Propuesta, ResolucionPropuesta> joinResolucion =
        root.join(Propuesta_.RESOLUCION_PROPUESTA, JoinType.INNER);

      predicates.add(
        cb.like(joinResolucion.get(ResolucionPropuesta_.VALOR), "%" + resolucion + "%"));
    }

    if (estado != null) {
      Join<Propuesta, EstadoPropuesta> joinEstado =
        root.join(Propuesta_.ESTADO_PROPUESTA, JoinType.INNER);

      predicates.add(cb.like(joinEstado.get(EstadoPropuesta_.VALOR), "%" + estado + "%"));
    }

    if (gestorExpediente != null) {
      Join<Expediente, AsignacionExpediente> joinExpAsignacionExpediente =
        joinExp.join(Expediente_.ASIGNACIONES, JoinType.INNER);
      Join<AsignacionExpediente, Usuario> joinAsignacionExpUsuario =
        joinExpAsignacionExpediente.join(AsignacionExpediente_.USUARIO, JoinType.INNER);
      Join<Usuario, Perfil> joinUsuarioPerfil =
        joinAsignacionExpUsuario.join(Usuario_.PERFIL, JoinType.INNER);

      predicates.add(cb.equal(joinUsuarioPerfil.get(Perfil_.NOMBRE), "Gestor"));
      predicates.add(
        cb.like(joinAsignacionExpUsuario.get(Usuario_.NOMBRE), "%" + gestorExpediente + "%"));
    }

    if (supervisor != null) {
      Join<Expediente, AsignacionExpediente> joinExpAsignacionExpediente =
        joinExp.join(Expediente_.ASIGNACIONES, JoinType.INNER);
      Join<AsignacionExpediente, Usuario> joinAsignacionExpUsuario =
        joinExpAsignacionExpediente.join(AsignacionExpediente_.USUARIO, JoinType.INNER);
      Join<Usuario, Perfil> joinUsuarioPerfil =
        joinAsignacionExpUsuario.join(Usuario_.PERFIL, JoinType.INNER);

      predicates.add(cb.equal(joinUsuarioPerfil.get(Perfil_.NOMBRE), "Supervisor"));
      predicates.add(
        cb.like(joinAsignacionExpUsuario.get(Usuario_.NOMBRE), "%" + supervisor + "%"));
    }

    if (primerTitular != null) {
      Join<Expediente, Contrato> joinContratoRepresentante =
        joinExp.join(Expediente_.CONTRATOS, JoinType.INNER);
      joinContratoRepresentante.on(
        cb.equal(joinContratoRepresentante.get(Contrato_.ES_REPRESENTANTE), true));
      Join<Contrato, ContratoInterviniente> joinContratoInterviniente =
        joinContratoRepresentante.join(Contrato_.INTERVINIENTES);
      joinContratoInterviniente.on(
        cb.equal(joinContratoInterviniente.get(ContratoInterviniente_.ORDEN_INTERVENCION), 1));
      Join<ContratoInterviniente, Interviniente> joinInterviniente =
        joinContratoInterviniente.join(ContratoInterviniente_.INTERVINIENTE, JoinType.INNER);

      predicates.add(
        cb.like(joinInterviniente.get(Interviniente_.NOMBRE), "%" + primerTitular + "%"));
    }

    if (estrategia != null) {
      Join<Propuesta, EstrategiaAsignada> joinEstrategiaAsignada =
        root.join(Propuesta_.ESTRATEGIA, JoinType.INNER);
      Join<EstrategiaAsignada, Estrategia> joinEstrategia =
        joinEstrategiaAsignada.join(EstrategiaAsignada_.ESTRATEGIA, JoinType.INNER);

      predicates.add(cb.like(joinEstrategia.get(Estrategia_.VALOR), "%" + estrategia + "%"));
    }

    if (fechaElevacion != null){
      predicates.add(cb.like(root.get(Propuesta_.fechaElevacion).as(String.class), "%" + fechaElevacion + "%"));
    }

    if (saldoGestion != null) {
      Join<Propuesta, Contrato> joinContratos = root.join(Propuesta_.CONTRATOS, JoinType.INNER);

      Subquery<Long> subqueryL = cb.createQuery().subquery(Long.class);
      Root<Contrato> fromC = subqueryL.from(Contrato.class);
      subqueryL
        .select(cb.sum(fromC.get(Contrato_.SALDO_GESTION)))
        .where(cb.equal(joinContratos.get(Contrato_.ID), fromC.get(Contrato_.ID)));

      predicates.add(cb.like(subqueryL.as(String.class), "%" + saldoGestion.replace(",", ".") + "%"));
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    return entityManager.createQuery(query).getResultList();
  }

  private List<Propuesta> querysPropuestaPN(Integer idCartera, Integer idEstado, Integer idTipo, String idExpediente, String resolucion,
                                                                     String estado, String gestorExpediente, String primerTitular, String estrategia, String saldoGestion,
                                                                     String fechaElevacion, String supervisor, Boolean documentos, String orderField, String orderDirection, Integer size, Integer page)
  {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Propuesta> query = cb.createQuery(Propuesta.class);

    Root<Propuesta> root = query.from(Propuesta.class);

    List<Predicate> predicates = new ArrayList<>();

    Join<Propuesta, Expediente> joinExp = root.join(Propuesta_.EXPEDIENTE_POSESION_NEGOCIADA, JoinType.INNER);
    Join<ExpedientePosesionNegociada, Cartera> joinExpCartera = joinExp.join(ExpedientePosesionNegociada_.CARTERA, JoinType.INNER);
    predicates.add(cb.equal(cb.upper(joinExpCartera.get(Cartera_.ID)), idCartera));

    if (idEstado != null) {
      Join<Propuesta, EstadoPropuesta> joinFiltroEstado = root.join(Propuesta_.ESTADO_PROPUESTA, JoinType.INNER);
      predicates.add(cb.equal(cb.upper(joinFiltroEstado.get(EstadoPropuesta_.ID)), idEstado));
    }

    if (idTipo != null) {
      Join<Propuesta, TipoPropuesta> joinFiltroTipo = root.join(Propuesta_.TIPO_PROPUESTA, JoinType.INNER);
      predicates.add(cb.equal(cb.upper(joinFiltroTipo.get(TipoPropuesta_.ID)), idTipo));
    }

    if (idExpediente != null) {
      predicates.add(cb.like(joinExp.get(ExpedientePosesionNegociada_.ID_CONCATENADO).as(String.class), "%" + idExpediente + "%"));
    }

    if (resolucion != null) {
      Join<Propuesta, ResolucionPropuesta> joinResolucion = root.join(Propuesta_.RESOLUCION_PROPUESTA, JoinType.INNER);
      predicates.add(cb.like(joinResolucion.get(ResolucionPropuesta_.VALOR), "%" + resolucion + "%"));
    }

    if (estado != null) {
      Join<Propuesta, EstadoPropuesta> joinEstado =root.join(Propuesta_.ESTADO_PROPUESTA, JoinType.INNER);
      predicates.add(cb.like(joinEstado.get(EstadoPropuesta_.VALOR), "%" + estado + "%"));
    }

    if (gestorExpediente != null) {
      Join<ExpedientePosesionNegociada, AsignacionExpediente> joinExpAsignacionExpediente = joinExp.join(ExpedientePosesionNegociada_.ASIGNACIONES, JoinType.INNER);
      Join<AsignacionExpedientePosesionNegociada, Usuario> joinAsignacionExpUsuario = joinExpAsignacionExpediente.join(AsignacionExpedientePosesionNegociada_.USUARIO, JoinType.INNER);
      Join<Usuario, Perfil> joinUsuarioPerfil = joinAsignacionExpUsuario.join(Usuario_.PERFIL, JoinType.INNER);

      predicates.add(cb.equal(joinUsuarioPerfil.get(Perfil_.NOMBRE), "Gestor"));
      predicates.add(cb.like(joinAsignacionExpUsuario.get(Usuario_.NOMBRE), "%" + gestorExpediente + "%"));
    }

    if (supervisor != null) {
      Join<ExpedientePosesionNegociada, AsignacionExpediente> joinExpAsignacionExpediente = joinExp.join(ExpedientePosesionNegociada_.ASIGNACIONES, JoinType.INNER);
      Join<AsignacionExpedientePosesionNegociada, Usuario> joinAsignacionExpUsuario = joinExpAsignacionExpediente.join(AsignacionExpedientePosesionNegociada_.USUARIO, JoinType.INNER);
      Join<Usuario, Perfil> joinUsuarioPerfil = joinAsignacionExpUsuario.join(Usuario_.PERFIL, JoinType.INNER);

      predicates.add(cb.equal(joinUsuarioPerfil.get(Perfil_.NOMBRE), "Supervisor"));
      predicates.add(cb.like(joinAsignacionExpUsuario.get(Usuario_.NOMBRE), "%" + supervisor + "%"));
    }

    if (estrategia != null) {
      Join<Propuesta, EstrategiaAsignada> joinEstrategiaAsignada = root.join(Propuesta_.ESTRATEGIA, JoinType.INNER);
      Join<EstrategiaAsignada, Estrategia> joinEstrategia = joinEstrategiaAsignada.join(EstrategiaAsignada_.ESTRATEGIA, JoinType.INNER);
      predicates.add(cb.like(joinEstrategia.get(Estrategia_.VALOR), "%" + estrategia + "%"));
    }

    if (primerTitular != null) {
      Join<Propuesta, Ocupante> joinOcupante = root.join(Propuesta_.OCUPANTES, JoinType.INNER);
      Join<Ocupante, OcupanteBien> joinOcupanteBien =
          joinOcupante.join(Ocupante_.BIENES, JoinType.INNER);
      Join<OcupanteBien, TipoOcupante> joinTipoOcupante =
        joinOcupanteBien.join(OcupanteBien_.TIPO_OCUPANTE, JoinType.INNER);

      predicates.add(cb.equal(joinOcupanteBien.get(OcupanteBien_.ORDEN), 1));
      predicates.add(cb.equal(joinTipoOcupante.get(TipoOcupante_.CODIGO), "TIT"));
      predicates.add(cb.like(joinOcupante.get(Ocupante_.NOMBRE), "%" + primerTitular + "%"));
    }

    if (fechaElevacion != null){
      predicates.add(cb.like(root.get(Propuesta_.fechaElevacion).as(String.class), "%" + fechaElevacion + "%"));
    }

    if (saldoGestion != null) {
      Join<Propuesta, PropuestaBien> joinPropuestaBien = root.join(Propuesta_.BIENES, JoinType.INNER);
      Join<PropuestaBien, Bien> joinBien = joinPropuestaBien.join(PropuestaBien_.BIEN, JoinType.INNER);

      Subquery<Long> subqueryL = cb.createQuery().subquery(Long.class);
      Root<Bien> fromC = subqueryL.from(Bien.class);
      subqueryL
          .select(cb.sum(fromC.get(Bien_.IMPORTE_BIEN)))
          .where(cb.equal(joinBien.get(Bien_.ID), fromC.get(Bien_.ID)));

      predicates.add(cb.like(subqueryL.as(String.class), "%" + saldoGestion.replace(",", ".") + "%"));
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    return entityManager.createQuery(query).getResultList();
  }

  @Override
  public ListWithCountDTO<DetalleListadoEstimacionPCOutputDTO> obtenerEstimacionesCarteraFiltradas(
    Integer idCartera,
    Boolean posesionNegociada,
    Integer idTipo,
    Date fecha,
    String idExpediente,
    String idContrato,
    String tipo,
    String estrategia,
    String importeRecuperado,
    String importeSalida,
    String fechaEstimadaRecuperacion,
    String probabilidadResolucion,
    String gestor,
    String supervisor, String orderField,
    String orderDirection,
    Integer size,
    Integer page) {

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Estimacion> query = cb.createQuery(Estimacion.class);

    Root<Estimacion> root = query.from(Estimacion.class);

    List<Predicate> predicates = new ArrayList<>();

    if (!posesionNegociada) {
      Join<Estimacion, Expediente> joinExp = root.join(Estimacion_.EXPEDIENTE, JoinType.INNER);
      Join<Expediente, Cartera> joinExpCartera = joinExp.join(Expediente_.CARTERA, JoinType.INNER);
      predicates.add(cb.equal(cb.upper(joinExpCartera.get(Cartera_.ID)), idCartera));

      if (idExpediente != null) {
        predicates.add(
            cb.like(joinExp.get(Expediente_.ID_CONCATENADO).as(String.class), "%" + idExpediente + "%"));
      }

      if (gestor != null) {
        Join<Expediente, AsignacionExpediente> joinExpAsignacionExpediente =
            joinExp.join(Expediente_.ASIGNACIONES, JoinType.INNER);
        Join<AsignacionExpediente, Usuario> joinAsignacionExpUsuario =
            joinExpAsignacionExpediente.join(AsignacionExpediente_.USUARIO, JoinType.INNER);
        Join<Usuario, Perfil> joinUsuarioPerfil =
            joinAsignacionExpUsuario.join(Usuario_.PERFIL, JoinType.INNER);

        predicates.add(cb.equal(joinUsuarioPerfil.get(Perfil_.NOMBRE), "Gestor"));
        predicates.add(cb.like(joinAsignacionExpUsuario.get(Usuario_.NOMBRE), "%" + gestor + "%"));
      }

      if (idContrato != null) {
        Join<Expediente, Contrato> joinContratoRepresentante =
            joinExp.join(Expediente_.CONTRATOS, JoinType.INNER);
        joinContratoRepresentante.on(
            cb.equal(joinContratoRepresentante.get(Contrato_.ES_REPRESENTANTE), true));

        predicates.add(
            cb.like(joinContratoRepresentante.get(Contrato_.ID_CARGA), "%" + idContrato + "%"));
      }

    } else {
      Join<Estimacion, ExpedientePosesionNegociada> joinExpPosNeg =
          root.join(Estimacion_.EXPEDIENTE_POSESION_NEGOCIADA, JoinType.INNER);
      Join<ExpedientePosesionNegociada, Cartera> joinExpPosNegCartera =
          joinExpPosNeg.join(ExpedientePosesionNegociada_.CARTERA, JoinType.INNER);
      predicates.add(cb.equal(cb.upper(joinExpPosNegCartera.get(Cartera_.ID)), idCartera));

      if (idExpediente != null) {
        predicates.add(
            cb.like(
                joinExpPosNeg.get(ExpedientePosesionNegociada_.ID_CONCATENADO).as(String.class),
                "%" + idExpediente + "%"));
      }

      if (gestor != null) {
        Join<ExpedientePosesionNegociada, AsignacionExpediente> joinExpAsignacionExpediente =
            joinExpPosNeg.join(ExpedientePosesionNegociada_.ASIGNACIONES, JoinType.INNER);
        Join<AsignacionExpediente, Usuario> joinAsignacionExpUsuario =
            joinExpAsignacionExpediente.join(AsignacionExpediente_.USUARIO, JoinType.INNER);
        Join<Usuario, Perfil> joinUsuarioPerfil =
            joinAsignacionExpUsuario.join(Usuario_.PERFIL, JoinType.INNER);

        predicates.add(cb.equal(joinUsuarioPerfil.get(Perfil_.NOMBRE), "Gestor"));
        predicates.add(cb.like(joinAsignacionExpUsuario.get(Usuario_.NOMBRE), "%" + gestor + "%"));
      }

      if (idContrato != null) {
        Join<ExpedientePosesionNegociada, Contrato> joinContratoRepresentante =
            joinExpPosNeg.join(ExpedientePosesionNegociada_.CONTRATO, JoinType.INNER);
        joinContratoRepresentante.on(
            cb.equal(joinContratoRepresentante.get(Contrato_.ES_REPRESENTANTE), true));

        predicates.add(
            cb.like(joinContratoRepresentante.get(Contrato_.ID_CARGA), "%" + idContrato + "%"));
      }
    }

    if (idTipo != null) {
      Join<Estimacion, TipoPropuesta> joinTipo = root.join(Estimacion_.TIPO, JoinType.INNER);

      predicates.add(cb.equal(cb.upper(joinTipo.get(TipoPropuesta_.ID)), idTipo));
    }

    if (fecha != null) {
      predicates.add(cb.equal(root.get(Estimacion_.FECHA).as(Date.class), fecha));
    }

    if (tipo != null) {
      Join<Estimacion, TipoPropuesta> joinTipo = root.join(Estimacion_.TIPO, JoinType.INNER);

      predicates.add(cb.like(joinTipo.get(TipoPropuesta_.VALOR), "%" + tipo + "%"));
    }

    if (estrategia != null) {
      Join<Estimacion, TipoPropuesta> joinTipo = root.join(Estimacion_.TIPO, JoinType.INNER);

      predicates.add(cb.like(joinTipo.get(TipoPropuesta_.VALOR), "%" + estrategia + "%"));
    }

    if (probabilidadResolucion != null) {
      switch (probabilidadResolucion) {
        case "alta":
          predicates.add(cb.equal(root.get(Estimacion_.PROBABILIDAD_RESOLUCION), 1));
          break;
        case "media":
          predicates.add(cb.equal(root.get(Estimacion_.PROBABILIDAD_RESOLUCION), 2));
          break;
        case "baja":
          predicates.add(cb.equal(root.get(Estimacion_.PROBABILIDAD_RESOLUCION), 3));
          break;
      }
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));


    List<DetalleListadoEstimacionPCOutputDTO> result = entityManager.createQuery(query).getResultList().stream()
        .map(PortalClienteMapper::parseToDetalleListadoEstimacionPCOutputDTO)
        .collect(Collectors.toList());

    if(orderDirection.compareTo("asc") == 0)
    {
      if(orderField.compareTo("contratoExpediente") == 0)      result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getIdExpediente, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("tipo") == 0)         result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getTipo, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("supervisor") == 0)    result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getSupervisor, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("estrategia") == 0)           result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getEstrategia, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("importeRecuperado") == 0)              result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getImporteRecuperado, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("fechaEstimadaRecuperacion") == 0)            result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getFechaEstimadaRecuperacion, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("probabilidadResolucion") == 0)          result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getProbabilidadResolucion, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("cumplida") == 0)              result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getCumplida, Comparator.nullsFirst(Comparator.naturalOrder()))).collect(Collectors.toList());
    }
    if(orderDirection.compareTo("desc") == 0)
    {
      if(orderField.compareTo("contratoExpediente") == 0)      result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getIdExpediente, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("tipo") == 0)         result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getTipo, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("supervisor") == 0)    result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getSupervisor, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("estrategia") == 0)           result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getEstrategia, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("importeRecuperado") == 0)              result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getImporteRecuperado, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("fechaEstimadaRecuperacion") == 0)            result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getFechaEstimadaRecuperacion, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("probabilidadResolucion") == 0)          result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getProbabilidadResolucion, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
      else if(orderField.compareTo("cumplida") == 0)              result = result.stream().sorted(Comparator.comparing(DetalleListadoEstimacionPCOutputDTO::getCumplida, Comparator.nullsFirst(Comparator.reverseOrder()))).collect(Collectors.toList());
    }

    if (importeSalida != null) {
      result = result.stream().filter(detalleListadoPropuestaPCOutputDTO -> Objects.nonNull(detalleListadoPropuestaPCOutputDTO.getImporteSalida()) && detalleListadoPropuestaPCOutputDTO.getImporteSalida().contains(importeSalida.replace(",","."))).collect(Collectors.toList());
    }

    if (importeRecuperado != null) {
      result = result.stream().filter(detalleListadoPropuestaPCOutputDTO -> Objects.nonNull(detalleListadoPropuestaPCOutputDTO.getImporteRecuperado()) && detalleListadoPropuestaPCOutputDTO.getImporteRecuperado().contains(importeRecuperado.replace(",", "."))).collect(Collectors.toList());
    }

    if (supervisor != null)
      result = result.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getSupervisor()), supervisor)).collect(Collectors.toList());

    if (fechaEstimadaRecuperacion != null) {
      result = result.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(new SimpleDateFormat("dd/MM/yyyy").format(c.getFechaEstimadaRecuperacion())), fechaEstimadaRecuperacion)).collect(Collectors.toList());
    }

    List<DetalleListadoEstimacionPCOutputDTO> estimacionesPaginadas = result.stream().skip(size * page).limit(size).collect(Collectors.toList());

    return new ListWithCountDTO<>(estimacionesPaginadas, result.size());
  }

  private Date getFecha(String s) {
    if (s == null) return null;
    return new Date(s);
  }

  // Guardamos la cartera pero enviamos el cliente
  @Override
  public void createDocumentoPortalCliente(
      Integer idCartera,
      String idMatricula,
      EntidadEnum entidadEnum,
      MultipartFile file,
      Usuario usuario)
      throws Exception {
    Cartera cartera = carteraRepository.findById(idCartera).orElse(null);
    Cliente cliente = cartera.getCliente();
    // String cartera1=cartera.getIdHaya();
    String nombreFichero = file.getOriginalFilename();
    String nombreCliente = cliente.getNombre();
    String nombreCartera = cartera.getNombre().replace(" ", "-");

    MetadatoDocumentoInputDTO metadatoDocumentoInputDTO =
        new MetadatoDocumentoInputDTO(nombreFichero, idMatricula);
    try {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
          new MetadatoContenedorInputDTO(nombreCartera, "");
      this.servicioGestorDocumental.procesarContenedorCartera(
          CodigoEntidad.PORTALCLIENTE, metadatoContenedorInputDTO);
      long id =
          servicioGestorDocumental
              .procesarDocumento(
                  CodigoEntidad.PORTALCLIENTE, nombreCartera, file, metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      Date fechaAlta = new Date();
      // TipoDocumento Revisar a la hora de guardar

      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental,
              usuario,
              null,
              fechaAlta,
              null,
              nombreFichero,
              null,
              entidadEnum.getValue(),
              null);
      gestorDocumentalRepository.save(gestorDocumental);
    } catch (Exception e) {
      long id =
          servicioGestorDocumental
              .procesarDocumentoUsuario(
                  CodigoEntidad.PORTALCLIENTE, nombreCartera, file, metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      Date fechaAlta = new Date();
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental,
              usuario,
              null,
              fechaAlta,
              null,
              nombreFichero,
              null,
              entidadEnum.getValue(),
              null,
              cartera);
      gestorDocumentalRepository.save(gestorDocumental);
    }
  }

  public List<DocumentoRepositorioDTO> filtrar(String tipoDocumento, String idDocumento, String usuarioCreador, String nombreDocumento, String fechaAlta, String fechaActualizacion, String orderField, String orderDirection, List<DocumentoRepositorioDTO> listfinal) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    if(idDocumento != null)
      listfinal = listfinal.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(doc.getId().toString(), idDocumento)).collect(Collectors.toList());
    if(nombreDocumento != null)
      listfinal = listfinal.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(doc.getNombreArchivo(), nombreDocumento)).collect(Collectors.toList());
    if(tipoDocumento != null)
      listfinal = listfinal.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(doc.getTipoDocumento(), tipoDocumento)).collect(Collectors.toList());
    if(fechaAlta != null)
      listfinal = listfinal.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(formatter.format(doc.getFechaAlta()), fechaAlta)).collect(Collectors.toList());
    if(fechaActualizacion != null)
      listfinal = listfinal.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(doc.getFechaActualizacion(), fechaActualizacion)).collect(Collectors.toList());
    if(usuarioCreador != null)
      listfinal = listfinal.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(doc.getUsuarioCreador(), usuarioCreador)).collect(Collectors.toList());

    if (orderDirection!=null && orderField!=null){
      Comparator<DocumentoRepositorioDTO> comparator = null;
      switch (orderField) {
        case "tipoDocumento":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getTipoDocumento);
          break;
        case "nombreDocumento":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getNombreArchivo);
          break;
        case "usuarioCreador":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getUsuarioCreador);
          break;
        case "fechaAlta":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getFechaAlta);
          break;
        case "fechaActualizacion":
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getFechaActualizacion);
          break;
        case "idDocumento":
        case "id":
        default:
          comparator = Comparator.comparing(DocumentoRepositorioDTO::getId);
      }
      if (orderDirection.equals("asc")) comparator.reversed();
      listfinal=listfinal.stream().sorted(comparator).collect(Collectors.toList());
    }
    return listfinal;
  }

  public List<DocumentoRepositorioDTO> busquedaDocumentoDeudorRepositorio(
    BusquedaRepositorioDTO busquedaRepositorioDTO,
    List<DocumentoRepositorioDTO> listdocumentoDeudorRepositorio){
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    if(busquedaRepositorioDTO.getUsuarioCreador() != null)
      listdocumentoDeudorRepositorio = listdocumentoDeudorRepositorio.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(doc.getUsuarioCreador(), busquedaRepositorioDTO.getUsuarioCreador())).collect(Collectors.toList());
    if(busquedaRepositorioDTO.getNombreDocumento() != null)
      listdocumentoDeudorRepositorio = listdocumentoDeudorRepositorio.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(doc.getNombreArchivo(), busquedaRepositorioDTO.getNombreDocumento())).collect(Collectors.toList());
    if(busquedaRepositorioDTO.getTipoDocumento() != null)
      listdocumentoDeudorRepositorio = listdocumentoDeudorRepositorio.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(doc.getTipoDocumento(), busquedaRepositorioDTO.getTipoDocumento())).collect(Collectors.toList());
    if(busquedaRepositorioDTO.getFechaAlta() != null)
      listdocumentoDeudorRepositorio = listdocumentoDeudorRepositorio.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(formatter.format(doc.getFechaAlta()), formatter.format(busquedaRepositorioDTO.getFechaAlta()))).collect(Collectors.toList());
    if(busquedaRepositorioDTO.getFechaActualizacion() != null)
      listdocumentoDeudorRepositorio = listdocumentoDeudorRepositorio.stream().filter((DocumentoRepositorioDTO doc) -> StringUtils.containsIgnoreCase(doc.getFechaActualizacion(), formatter.format(busquedaRepositorioDTO.getFechaActualizacion()))).collect(Collectors.toList());

    return listdocumentoDeudorRepositorio;
  }

  public ListWithCountDTO<DocumentoRepositorioDTO> findDocumentosByCliente(BusquedaRepositorioDTO busquedaRepositorioDTO,
                                                                           String tipoDocumento, String idDocumento, String usuarioCreador, String nombreDocumento, String fechaAlta, String fechaActualizacion, String orderField, String orderDirection, Integer idCartera, EntidadEnum entidadEnum, Integer size, Integer page) throws IOException {
    Cartera cartera = carteraRepository.findById(idCartera).orElse(null);
    String nombreCartera = cartera.getNombre().replace(" ", "-");
    Cliente cliente = cartera.getCliente();
    // Obtenemos los documentos del GD
    List<DocumentoRepositorioDTO> listadoIdGestor = new ArrayList<>();
    try {
      listadoIdGestor =
          this.servicioGestorDocumental.listarDocumentos(CodigoEntidad.PORTALCLIENTE, nombreCartera)
              .stream()
              .map(DocumentoRepositorioDTO::new)
              .collect(Collectors.toList());

      // Seteamos dos variables que no vienen del gestor
      listadoIdGestor =
          listadoIdGestor.stream()
              .peek(
                  dto -> {
                    dto.setNombreCartera(cartera.getNombre());
                    dto.setNombreCliente(cliente.getNombre());
                  })
              .collect(Collectors.toList());


      // Filtrar por los documentos en nuestra BD
      List<DocumentoRepositorioDTO> list =
          gestorDocumentalUseCase.listarDocumentosRepositorioCarteras(listadoIdGestor);

      List<DocumentoRepositorioDTO>listFinal=new ArrayList<>();

      // Filtrar por tipoModeloCartera
      List<DocumentoRepositorioDTO> listfiltradaTipo =
        list.stream()
          .filter(
            documentoRepositorioDTO ->
              documentoRepositorioDTO.getTipoModeloCartera().equals(entidadEnum.getValue()))
          .collect(Collectors.toList());

      listFinal.addAll(listfiltradaTipo);

      List<DocumentoRepositorioDTO> listadoNoNuestraBase=
        list.stream()
          .filter(
            documentoRepositorioDTO ->
              !documentoRepositorioDTO.getTipoModeloCartera().equals(entidadEnum.getValue()))
          .collect(Collectors.toList());

      if (entidadEnum.getValue().equals("manuales")) {
        listadoNoNuestraBase =
            listadoNoNuestraBase.stream()
                .filter(
                    documentoRepositorioDTO ->
                        documentoRepositorioDTO.getTipoDocumento().equals("Manual cliente"))
                .collect(Collectors.toList());
      }
      if (entidadEnum.getValue().equals("modelos")) {
        listadoNoNuestraBase =
            listadoNoNuestraBase.stream()
                .filter(
                    documentoRepositorioDTO -> {
                      if (documentoRepositorioDTO.getTipoDocumento().equals("Contrato")
                          || documentoRepositorioDTO
                              .getTipoDocumento()
                              .equals("Modelos y plantillas cliente")
                          || documentoRepositorioDTO
                              .getTipoDocumento()
                              .equals("Facturas cliente")) {
                        return true;
                      }
                      return false;
                    })
                .collect(Collectors.toList());
      }

      if (entidadEnum.getValue().equals("modelos")) {
        listadoNoNuestraBase =
            listadoNoNuestraBase.stream()
                .filter(
                    documentoRepositorioDTO -> {
                      if (documentoRepositorioDTO.getTipoDocumento().equals("Contrato")
                          || documentoRepositorioDTO
                              .getTipoDocumento()
                              .equals("Modelos y plantillas cliente")
                          || documentoRepositorioDTO
                              .getTipoDocumento()
                              .equals("Facturas cliente")) {
                        return true;
                      }
                      return false;
                    })
                .collect(Collectors.toList());
      }

      if (entidadEnum.getValue().equals("otros")) {
        listadoNoNuestraBase =
            listadoNoNuestraBase.stream()
                .filter(
                    documentoRepositorioDTO -> {
                      if (documentoRepositorioDTO.getTipoDocumento().equals("Contrato")
                          || documentoRepositorioDTO.getTipoDocumento().equals("Facturas cliente")
                          || documentoRepositorioDTO
                              .getTipoDocumento()
                              .equals("Documento compartido con el cliente")
                          || documentoRepositorioDTO
                              .getTipoDocumento()
                              .equals("Sanción propuesta cliente")
                          || documentoRepositorioDTO.getTipoDocumento().equals("Recibo cliente")) {
                        return true;
                      }
                      return false;
                    })
                .collect(Collectors.toList());
      }

      listFinal.addAll(listadoNoNuestraBase);


      //Filtros nuevos
      listFinal = busquedaDocumentoDeudorRepositorio(busquedaRepositorioDTO,listFinal);
      listFinal = filtrar(tipoDocumento, idDocumento, usuarioCreador, nombreDocumento, fechaAlta, fechaActualizacion, orderField, orderDirection,listFinal);

      // Paginar
      List<DocumentoRepositorioDTO> listaFiltradaPaginada = listFinal.stream()
              .skip(size * page)
              .limit(size)
              .collect(Collectors.toList());

      return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
    } catch (Exception ex) {
      return new ListWithCountDTO<>(listadoIdGestor, listadoIdGestor.size());
    }
  }

  public void createUsuario(Integer idCliente, String email, String nombre)
      throws NotFoundException {
    Usuario user = null;
    if (idCliente != null)
      user = usuarioRepository.findByIdCliente(idCliente).orElse(null);
    if (user == null)
      user = usuarioRepository.findByEmail(email).orElse(new Usuario());
    if (user.getId() == null) {
      Perfil p =
          perfilRepository
              .findByNombre("Cliente")
              .orElseThrow(() -> new NotFoundException("perfil", "nombre", "'Cliente'"));
      user.setPerfil(p);
      user.setEmail(email);
      user.setNombre(nombre);
    }
    user.setIdCliente(idCliente);
    usuarioRepository.save(user);
  }

  public List<CarteraDTOToList> getCarterasCliente(Integer idCliente) throws NotFoundException {
    Usuario user =
        usuarioRepository
            .findByIdCliente(idCliente)
            .orElseThrow(
                () ->
                    new NotFoundException(
                            "usuario", "idCliente", idCliente));

    return carteraUseCase.getCarteraByUser(user);
  }

  @Override
  public List<CatalogoMinInfoDTO> getResultados(Integer idEvento) throws NotFoundException {
    Evento e =
        eventoRepository
            .findById(idEvento)
            .orElseThrow(() -> new NotFoundException("evento", idEvento));

    return eventoUseCase.findResultados(e.getSubtipoEvento().getId(), idEvento);
  }

  @Override
  public void actualizar(Integer idEvento, Integer idResultado, String comentario, Usuario logged)
      throws NotFoundException, InvalidCodeException, RequiredValueException, IOException {
    Evento e =
        eventoRepository
            .findById(idEvento)
            .orElseThrow(() -> new NotFoundException("evento", idEvento));

    EventoInputDTO input = eventoMapper.parseInput(e);
    input.setResultado(idResultado);
    if (comentario != null)
      input.setComentariosDestinatario(comentario);

    eventoUseCase.update(idEvento, input, logged);
  }

  @Override
  public List<SegmentacionCarteraDTO> getSegmentacionCartera(
      Integer idCartera, Integer idSegmentacion) {
    return null;
  }

  @Override
  public List<UsuarioOutputDTO> getByExpediente(Integer idEx, Boolean posesionNegociada){
    List<Usuario> usuarios;
    if (posesionNegociada){
      usuarios = usuarioRepository.findAllByAsignacionExpedientesPNExpedienteId(idEx);
    } else {

      usuarios = usuarioRepository.findAllByAsignacionExpedientesExpedienteId(idEx);
    }
    List<UsuarioOutputDTO> result = usuarios.stream().map(usuarioMapper::entityToDTO).collect(Collectors.toList());
    return result;
  }
}
