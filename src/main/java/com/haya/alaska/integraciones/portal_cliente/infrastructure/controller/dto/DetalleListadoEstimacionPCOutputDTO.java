package com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DetalleListadoEstimacionPCOutputDTO implements Serializable {
  private Integer idEstimacion;
  private Date fecha;
  private String idExpediente;
  private String contratoExpediente;
  private String tipo;
  private String estrategia;
  private String importeRecuperado;
  private String importeSalida;
  private Date fechaEstimadaRecuperacion;
  private String probabilidadResolucion;
  private String gestor;
  private String supervisor;
  private Boolean documentos;
  private String cumplida;
}
