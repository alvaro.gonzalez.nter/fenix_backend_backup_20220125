package com.haya.alaska.integraciones.pbc.infrastructure.soap;

import com.haya.alaska.integraciones.pbc.infrastructure.mapper.PBCSoapClientMapper;
import com.haya.alaska.integraciones.pbc.wsdl.WsPBCCompleted;
import com.haya.alaska.integraciones.pbc.wsdl.WsPBCCompletedResponse;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapMessage;

@Component
@Slf4j
public class SoapClient extends WebServiceGatewaySupport {

  @Autowired PBCSoapClientMapper mapper;
  @Autowired PropuestaRepository propuestaRepository;

  private static final String SOAP_ACTION_HEADER =
      "http://intranet.haya.es/Pipeline/WsPBCCompleted";

  public String notificarPBC(Propuesta propuesta) {
    // Creamos el request
    WsPBCCompleted request = new WsPBCCompleted();
    request.setVobjREDOperacion(mapper.parseToPBCREDOperacion(propuesta));

    // Invocamos el servicio
    WsPBCCompletedResponse response =
        (WsPBCCompletedResponse)
            getWebServiceTemplate()
                .marshalSendAndReceive(
                    request, message -> ((SoapMessage) message).setSoapAction(SOAP_ACTION_HEADER));

    // Retornamos el dato
    return response.getWsPBCCompletedResult();
  }
}
