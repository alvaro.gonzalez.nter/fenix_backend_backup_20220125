package com.haya.alaska.integraciones.pbc.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase Java para ArrayOfPBC_RED_Contrato complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta
 * clase.
 *
 * <pre>
 * &lt;complexType name="ArrayOfPBC_RED_Contrato"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PBC_RED_Contrato" type="{http://intranet.haya.es/Pipeline/}PBC_RED_Contrato" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "ArrayOfPBC_RED_Contrato",
    propOrder = {"pbcredContrato"})
public class ArrayOfPBCREDContrato {

  @XmlElement(name = "PBC_RED_Contrato", nillable = true)
  protected List<PBCREDContrato> pbcredContrato;

  /**
   * Gets the value of the pbcredContrato property.
   *
   * <p>This accessor method returns a reference to the live list, not a snapshot. Therefore any
   * modification you make to the returned list will be present inside the JAXB object. This is why
   * there is not a <CODE>set</CODE> method for the pbcredContrato property.
   *
   * <p>For example, to add a new item, do as follows:
   *
   * <pre>
   *    getPBCREDContrato().add(newItem);
   * </pre>
   *
   * <p>Objects of the following type(s) are allowed in the list {@link PBCREDContrato }
   */
  public List<PBCREDContrato> getPBCREDContrato() {
    if (pbcredContrato == null) {
      pbcredContrato = new ArrayList<PBCREDContrato>();
    }
    return this.pbcredContrato;
  }
}
