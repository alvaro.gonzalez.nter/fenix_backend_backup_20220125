
package com.haya.alaska.integraciones.pbc.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PBC_RED_Interviniente complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="PBC_RED_Interviniente"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CodInterviniente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Apellidos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoPersona" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PaisNacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MunicipioNacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProvinciaNacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PaisNacionalidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EstadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegimenEconomico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TieneActividadProfesional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SinActividadProfesional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNOActividadProfesional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNOActividadProfesionalAnterior" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ModalidadIngresos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Domicilio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Municipio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Provincia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PaisResidencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FormaJuridica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaConstitucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DatosRegistrales_Registro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DatosRegistrales_Tomo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DatosRegistrales_Libro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DatosRegistrales_Hoja" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNOSectorActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AccionesPortador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Cotizada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DerechoPublico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EntidadFinanciera" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EsFilial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PBC_RED_Interviniente", propOrder = {
    "codInterviniente",
    "nombre",
    "apellidos",
    "tipoPersona",
    "fechaNacimiento",
    "paisNacimiento",
    "municipioNacimiento",
    "provinciaNacimiento",
    "paisNacionalidad",
    "estadoCivil",
    "regimenEconomico",
    "tieneActividadProfesional",
    "sinActividadProfesional",
    "cnoActividadProfesional",
    "cnoActividadProfesionalAnterior",
    "modalidadIngresos",
    "tipoDocumento",
    "numeroDocumento",
    "domicilio",
    "municipio",
    "provincia",
    "paisResidencia",
    "formaJuridica",
    "fechaConstitucion",
    "datosRegistralesRegistro",
    "datosRegistralesTomo",
    "datosRegistralesLibro",
    "datosRegistralesHoja",
    "cnoSectorActividad",
    "accionesPortador",
    "cotizada",
    "derechoPublico",
    "entidadFinanciera",
    "esFilial"
})
public class PBCREDInterviniente {

    @XmlElement(name = "CodInterviniente")
    protected String codInterviniente;
    @XmlElement(name = "Nombre")
    protected String nombre;
    @XmlElement(name = "Apellidos")
    protected String apellidos;
    @XmlElement(name = "TipoPersona")
    protected String tipoPersona;
    @XmlElement(name = "FechaNacimiento")
    protected String fechaNacimiento;
    @XmlElement(name = "PaisNacimiento")
    protected String paisNacimiento;
    @XmlElement(name = "MunicipioNacimiento")
    protected String municipioNacimiento;
    @XmlElement(name = "ProvinciaNacimiento")
    protected String provinciaNacimiento;
    @XmlElement(name = "PaisNacionalidad")
    protected String paisNacionalidad;
    @XmlElement(name = "EstadoCivil")
    protected String estadoCivil;
    @XmlElement(name = "RegimenEconomico")
    protected String regimenEconomico;
    @XmlElement(name = "TieneActividadProfesional")
    protected String tieneActividadProfesional;
    @XmlElement(name = "SinActividadProfesional")
    protected String sinActividadProfesional;
    @XmlElement(name = "CNOActividadProfesional")
    protected String cnoActividadProfesional;
    @XmlElement(name = "CNOActividadProfesionalAnterior")
    protected String cnoActividadProfesionalAnterior;
    @XmlElement(name = "ModalidadIngresos")
    protected String modalidadIngresos;
    @XmlElement(name = "TipoDocumento")
    protected String tipoDocumento;
    @XmlElement(name = "NumeroDocumento")
    protected String numeroDocumento;
    @XmlElement(name = "Domicilio")
    protected String domicilio;
    @XmlElement(name = "Municipio")
    protected String municipio;
    @XmlElement(name = "Provincia")
    protected String provincia;
    @XmlElement(name = "PaisResidencia")
    protected String paisResidencia;
    @XmlElement(name = "FormaJuridica")
    protected String formaJuridica;
    @XmlElement(name = "FechaConstitucion")
    protected String fechaConstitucion;
    @XmlElement(name = "DatosRegistrales_Registro")
    protected String datosRegistralesRegistro;
    @XmlElement(name = "DatosRegistrales_Tomo")
    protected String datosRegistralesTomo;
    @XmlElement(name = "DatosRegistrales_Libro")
    protected String datosRegistralesLibro;
    @XmlElement(name = "DatosRegistrales_Hoja")
    protected String datosRegistralesHoja;
    @XmlElement(name = "CNOSectorActividad")
    protected String cnoSectorActividad;
    @XmlElement(name = "AccionesPortador")
    protected String accionesPortador;
    @XmlElement(name = "Cotizada")
    protected String cotizada;
    @XmlElement(name = "DerechoPublico")
    protected String derechoPublico;
    @XmlElement(name = "EntidadFinanciera")
    protected String entidadFinanciera;
    @XmlElement(name = "EsFilial")
    protected String esFilial;

    /**
     * Obtiene el valor de la propiedad codInterviniente.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCodInterviniente() {
        return codInterviniente;
    }

    /**
     * Define el valor de la propiedad codInterviniente.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCodInterviniente(String value) {
        this.codInterviniente = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidos.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Define el valor de la propiedad apellidos.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setApellidos(String value) {
        this.apellidos = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPersona.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * Define el valor de la propiedad tipoPersona.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTipoPersona(String value) {
        this.tipoPersona = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaNacimiento.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Define el valor de la propiedad fechaNacimiento.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFechaNacimiento(String value) {
        this.fechaNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad paisNacimiento.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPaisNacimiento() {
        return paisNacimiento;
    }

    /**
     * Define el valor de la propiedad paisNacimiento.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPaisNacimiento(String value) {
        this.paisNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad municipioNacimiento.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMunicipioNacimiento() {
        return municipioNacimiento;
    }

    /**
     * Define el valor de la propiedad municipioNacimiento.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMunicipioNacimiento(String value) {
        this.municipioNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad provinciaNacimiento.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getProvinciaNacimiento() {
        return provinciaNacimiento;
    }

    /**
     * Define el valor de la propiedad provinciaNacimiento.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProvinciaNacimiento(String value) {
        this.provinciaNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad paisNacionalidad.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPaisNacionalidad() {
        return paisNacionalidad;
    }

    /**
     * Define el valor de la propiedad paisNacionalidad.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPaisNacionalidad(String value) {
        this.paisNacionalidad = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoCivil.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Define el valor de la propiedad estadoCivil.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEstadoCivil(String value) {
        this.estadoCivil = value;
    }

    /**
     * Obtiene el valor de la propiedad regimenEconomico.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRegimenEconomico() {
        return regimenEconomico;
    }

    /**
     * Define el valor de la propiedad regimenEconomico.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRegimenEconomico(String value) {
        this.regimenEconomico = value;
    }

    /**
     * Obtiene el valor de la propiedad tieneActividadProfesional.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTieneActividadProfesional() {
        return tieneActividadProfesional;
    }

    /**
     * Define el valor de la propiedad tieneActividadProfesional.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTieneActividadProfesional(String value) {
        this.tieneActividadProfesional = value;
    }

    /**
     * Obtiene el valor de la propiedad sinActividadProfesional.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSinActividadProfesional() {
        return sinActividadProfesional;
    }

    /**
     * Define el valor de la propiedad sinActividadProfesional.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSinActividadProfesional(String value) {
        this.sinActividadProfesional = value;
    }

    /**
     * Obtiene el valor de la propiedad cnoActividadProfesional.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCNOActividadProfesional() {
        return cnoActividadProfesional;
    }

    /**
     * Define el valor de la propiedad cnoActividadProfesional.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCNOActividadProfesional(String value) {
        this.cnoActividadProfesional = value;
    }

    /**
     * Obtiene el valor de la propiedad cnoActividadProfesionalAnterior.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCNOActividadProfesionalAnterior() {
        return cnoActividadProfesionalAnterior;
    }

    /**
     * Define el valor de la propiedad cnoActividadProfesionalAnterior.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCNOActividadProfesionalAnterior(String value) {
        this.cnoActividadProfesionalAnterior = value;
    }

    /**
     * Obtiene el valor de la propiedad modalidadIngresos.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getModalidadIngresos() {
        return modalidadIngresos;
    }

    /**
     * Define el valor de la propiedad modalidadIngresos.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setModalidadIngresos(String value) {
        this.modalidadIngresos = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumento.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Define el valor de la propiedad tipoDocumento.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroDocumento.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    /**
     * Define el valor de la propiedad numeroDocumento.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNumeroDocumento(String value) {
        this.numeroDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad domicilio.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDomicilio() {
        return domicilio;
    }

    /**
     * Define el valor de la propiedad domicilio.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDomicilio(String value) {
        this.domicilio = value;
    }

    /**
     * Obtiene el valor de la propiedad municipio.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMunicipio() {
        return municipio;
    }

    /**
     * Define el valor de la propiedad municipio.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMunicipio(String value) {
        this.municipio = value;
    }

    /**
     * Obtiene el valor de la propiedad provincia.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getProvincia() {
        return provincia;
    }

    /**
     * Define el valor de la propiedad provincia.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProvincia(String value) {
        this.provincia = value;
    }

    /**
     * Obtiene el valor de la propiedad paisResidencia.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPaisResidencia() {
        return paisResidencia;
    }

    /**
     * Define el valor de la propiedad paisResidencia.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPaisResidencia(String value) {
        this.paisResidencia = value;
    }

    /**
     * Obtiene el valor de la propiedad formaJuridica.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFormaJuridica() {
        return formaJuridica;
    }

    /**
     * Define el valor de la propiedad formaJuridica.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFormaJuridica(String value) {
        this.formaJuridica = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaConstitucion.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFechaConstitucion() {
        return fechaConstitucion;
    }

    /**
     * Define el valor de la propiedad fechaConstitucion.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFechaConstitucion(String value) {
        this.fechaConstitucion = value;
    }

    /**
     * Obtiene el valor de la propiedad datosRegistralesRegistro.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDatosRegistralesRegistro() {
        return datosRegistralesRegistro;
    }

    /**
     * Define el valor de la propiedad datosRegistralesRegistro.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDatosRegistralesRegistro(String value) {
        this.datosRegistralesRegistro = value;
    }

    /**
     * Obtiene el valor de la propiedad datosRegistralesTomo.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDatosRegistralesTomo() {
        return datosRegistralesTomo;
    }

    /**
     * Define el valor de la propiedad datosRegistralesTomo.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDatosRegistralesTomo(String value) {
        this.datosRegistralesTomo = value;
    }

    /**
     * Obtiene el valor de la propiedad datosRegistralesLibro.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDatosRegistralesLibro() {
        return datosRegistralesLibro;
    }

    /**
     * Define el valor de la propiedad datosRegistralesLibro.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDatosRegistralesLibro(String value) {
        this.datosRegistralesLibro = value;
    }

    /**
     * Obtiene el valor de la propiedad datosRegistralesHoja.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDatosRegistralesHoja() {
        return datosRegistralesHoja;
    }

    /**
     * Define el valor de la propiedad datosRegistralesHoja.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDatosRegistralesHoja(String value) {
        this.datosRegistralesHoja = value;
    }

    /**
     * Obtiene el valor de la propiedad cnoSectorActividad.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCNOSectorActividad() {
        return cnoSectorActividad;
    }

    /**
     * Define el valor de la propiedad cnoSectorActividad.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCNOSectorActividad(String value) {
        this.cnoSectorActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad accionesPortador.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAccionesPortador() {
        return accionesPortador;
    }

    /**
     * Define el valor de la propiedad accionesPortador.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAccionesPortador(String value) {
        this.accionesPortador = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizada.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCotizada() {
        return cotizada;
    }

    /**
     * Define el valor de la propiedad cotizada.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCotizada(String value) {
        this.cotizada = value;
    }

    /**
     * Obtiene el valor de la propiedad derechoPublico.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDerechoPublico() {
        return derechoPublico;
    }

    /**
     * Define el valor de la propiedad derechoPublico.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDerechoPublico(String value) {
        this.derechoPublico = value;
    }

    /**
     * Obtiene el valor de la propiedad entidadFinanciera.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEntidadFinanciera() {
        return entidadFinanciera;
    }

    /**
     * Define el valor de la propiedad entidadFinanciera.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEntidadFinanciera(String value) {
        this.entidadFinanciera = value;
    }

    /**
     * Obtiene el valor de la propiedad esFilial.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEsFilial() {
        return esFilial;
    }

    /**
     * Define el valor de la propiedad esFilial.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEsFilial(String value) {
        this.esFilial = value;
    }

}
