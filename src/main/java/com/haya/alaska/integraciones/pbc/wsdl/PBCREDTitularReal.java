package com.haya.alaska.integraciones.pbc.wsdl;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase Java para PBC_RED_TitularReal complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta
 * clase.
 *
 * <pre>
 * &lt;complexType name="PBC_RED_TitularReal"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Interviniente" type="{http://intranet.haya.es/Pipeline/}PBC_RED_Interviniente" minOccurs="0"/&gt;
 *         &lt;element name="TipoRelacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DocumentoSociedad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Porcentaje" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "PBC_RED_TitularReal",
    propOrder = {"interviniente", "tipoRelacion", "documentoSociedad", "porcentaje"})
public class PBCREDTitularReal {

  @XmlElement(name = "Interviniente")
  protected PBCREDInterviniente interviniente;

  @XmlElement(name = "TipoRelacion")
  protected String tipoRelacion;

  @XmlElement(name = "DocumentoSociedad")
  protected String documentoSociedad;

  @XmlElement(name = "Porcentaje", required = true)
  protected BigDecimal porcentaje;

  /**
   * Obtiene el valor de la propiedad interviniente.
   *
   * @return possible object is {@link PBCREDInterviniente }
   */
  public PBCREDInterviniente getInterviniente() {
    return interviniente;
  }

  /**
   * Define el valor de la propiedad interviniente.
   *
   * @param value allowed object is {@link PBCREDInterviniente }
   */
  public void setInterviniente(PBCREDInterviniente value) {
    this.interviniente = value;
  }

  /**
   * Obtiene el valor de la propiedad tipoRelacion.
   *
   * @return possible object is {@link String }
   */
  public String getTipoRelacion() {
    return tipoRelacion;
  }

  /**
   * Define el valor de la propiedad tipoRelacion.
   *
   * @param value allowed object is {@link String }
   */
  public void setTipoRelacion(String value) {
    this.tipoRelacion = value;
  }

  /**
   * Obtiene el valor de la propiedad documentoSociedad.
   *
   * @return possible object is {@link String }
   */
  public String getDocumentoSociedad() {
    return documentoSociedad;
  }

  /**
   * Define el valor de la propiedad documentoSociedad.
   *
   * @param value allowed object is {@link String }
   */
  public void setDocumentoSociedad(String value) {
    this.documentoSociedad = value;
  }

  /**
   * Obtiene el valor de la propiedad porcentaje.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getPorcentaje() {
    return porcentaje;
  }

  /**
   * Define el valor de la propiedad porcentaje.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setPorcentaje(BigDecimal value) {
    this.porcentaje = value;
  }
}
