package com.haya.alaska.integraciones.pbc.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase Java para PBC_RED_Acreditado complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta
 * clase.
 *
 * <pre>
 * &lt;complexType name="PBC_RED_Acreditado"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Interviniente" type="{http://intranet.haya.es/Pipeline/}PBC_RED_Interviniente" minOccurs="0"/&gt;
 *         &lt;element name="TitularesReales" type="{http://intranet.haya.es/Pipeline/}ArrayOfPBC_RED_TitularReal" minOccurs="0"/&gt;
 *         &lt;element name="Representante" type="{http://intranet.haya.es/Pipeline/}PBC_RED_Representante" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "PBC_RED_Acreditado",
    propOrder = {"interviniente", "titularesReales", "representante"})
public class PBCREDAcreditado {

  @XmlElement(name = "Interviniente")
  protected PBCREDInterviniente interviniente;

  @XmlElement(name = "TitularesReales")
  protected ArrayOfPBCREDTitularReal titularesReales;

  @XmlElement(name = "Representante")
  protected PBCREDRepresentante representante;

  /**
   * Obtiene el valor de la propiedad interviniente.
   *
   * @return possible object is {@link PBCREDInterviniente }
   */
  public PBCREDInterviniente getInterviniente() {
    return interviniente;
  }

  /**
   * Define el valor de la propiedad interviniente.
   *
   * @param value allowed object is {@link PBCREDInterviniente }
   */
  public void setInterviniente(PBCREDInterviniente value) {
    this.interviniente = value;
  }

  /**
   * Obtiene el valor de la propiedad titularesReales.
   *
   * @return possible object is {@link ArrayOfPBCREDTitularReal }
   */
  public ArrayOfPBCREDTitularReal getTitularesReales() {
    return titularesReales;
  }

  /**
   * Define el valor de la propiedad titularesReales.
   *
   * @param value allowed object is {@link ArrayOfPBCREDTitularReal }
   */
  public void setTitularesReales(ArrayOfPBCREDTitularReal value) {
    this.titularesReales = value;
  }

  /**
   * Obtiene el valor de la propiedad representante.
   *
   * @return possible object is {@link PBCREDRepresentante }
   */
  public PBCREDRepresentante getRepresentante() {
    return representante;
  }

  /**
   * Define el valor de la propiedad representante.
   *
   * @param value allowed object is {@link PBCREDRepresentante }
   */
  public void setRepresentante(PBCREDRepresentante value) {
    this.representante = value;
  }
}
