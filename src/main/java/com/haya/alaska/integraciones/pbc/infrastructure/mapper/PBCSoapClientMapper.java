package com.haya.alaska.integraciones.pbc.infrastructure.mapper;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.integraciones.pbc.wsdl.*;
import com.haya.alaska.propuesta.domain.Propuesta;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class PBCSoapClientMapper {

  public PBCREDOperacion parseToPBCREDOperacion(Propuesta propuesta) {
    PBCREDOperacion pbcredOperacion = new PBCREDOperacion();
    if (propuesta != null) {
      // -IdPropuesta
      pbcredOperacion.setIdPropuesta(String.valueOf(propuesta.getId()));
      // -CodExpediente ? + Cartera
      if (Objects.nonNull(propuesta.getExpediente())) {
        pbcredOperacion.setCodExpediente(propuesta.getExpediente().getIdConcatenado());
        pbcredOperacion.setCartera(propuesta.getExpediente().getCartera().getId());
      } else {
        pbcredOperacion.setCodExpediente(
            propuesta.getExpedientePosesionNegociada().getIdConcatenado());
        pbcredOperacion.setCartera(propuesta.getExpedientePosesionNegociada().getCartera().getId());
      }
      // -EstadoPropuesta
      pbcredOperacion.setEstadoPropuesta(propuesta.getEstadoPropuesta().getId());
      // -SancionPropuesta
      if (Objects.nonNull(propuesta.getSancionPropuesta())) {
        pbcredOperacion.setSancionPropuesta(propuesta.getSancionPropuesta().getId());
      }
      // -TipoPropuesta
      pbcredOperacion.setTipoPropuesta(propuesta.getTipoPropuesta().getId());
      // -SubTipoPropuesta
      pbcredOperacion.setSubTipoPropuesta(propuesta.getSubtipoPropuesta().getId());
      // -FechaPrevistaF5 ?
      /*pbcredOperacion.setFechaPrevistaF5(
          String.valueOf(new ArrayList<>(propuesta.getFormalizaciones()).get(0).getFechaFirma()));
      pbcredOperacion.setFechaPrevistaF5(null);*/
      // -FechaAlta
      pbcredOperacion.setFechaAlta(
          Objects.nonNull(propuesta.getFechaAlta())
              ? new SimpleDateFormat("yyyy-MM-dd").format(propuesta.getFechaAlta())
              : null);
      // -FechaSancion
      pbcredOperacion.setFechaSancion(
          Objects.nonNull(propuesta.getFechaSancion())
              ? new SimpleDateFormat("yyyy-MM-dd").format(propuesta.getFechaSancion())
              : null);
      // -FechaEnvio + FechaAcordada + FechaEscrituracion
      if (!propuesta.getFormalizaciones().isEmpty()) {
        var fechaEnvio =
            new ArrayList<>(propuesta.getFormalizaciones())
                .get(0)
                .getFEnvioMinutaAClienteFormalizacion();

        pbcredOperacion.setFechaEnvio(
            Objects.nonNull(fechaEnvio) ? String.valueOf(fechaEnvio) : null);

        var fechaFirma = new ArrayList<>(propuesta.getFormalizaciones()).get(0).getFechaFirma();

        pbcredOperacion.setFechaAcordada(
            Objects.nonNull(fechaFirma)
                ? new SimpleDateFormat("yyyy-MM-dd").format(fechaFirma)
                : "1990-12-31");

        pbcredOperacion.setFechaEscrituracion(
            Objects.nonNull(fechaFirma)
                ? new SimpleDateFormat("yyyy-MM-dd").format(fechaFirma)
                : null);
      }
      // -IdColabora
      if (Objects.nonNull(propuesta.getNumColabora())) {
        pbcredOperacion.setIdColabora(String.valueOf(propuesta.getNumColabora()));
      }
      // -TipoProducto ?
      if (!propuesta.getContratos().isEmpty()) {
        var producto = new ArrayList<>(propuesta.getContratos()).get(0).getProducto();

        pbcredOperacion.setTipoProducto(
            Objects.nonNull(producto)
                ? this.obtenerValorTipoProductoPBC(producto.getValor())
                : null);
      }
      // -WorkResources ?
      pbcredOperacion.setWorkResources(0);
      // -Vinculaciones ?
      pbcredOperacion.setVinculaciones(0);
      // -AmbitoPBC
      pbcredOperacion.setAmbitoPBC("P");
      // -NumAdvisoryNote
      if (Objects.nonNull(propuesta.getNumAdvisoryNote())) {
        pbcredOperacion.setNumAdvisoryNote(String.valueOf(propuesta.getNumAdvisoryNote()));
      }
      // -FechaEnvioWG
      if (Objects.nonNull(propuesta.getFechaEnvioWorkingGroup())) {
        pbcredOperacion.setFechaEnvioWG(
            new SimpleDateFormat("yyyy-MM-dd").format(propuesta.getFechaEnvioWorkingGroup()));
      }
      // -FechaEnvioCES
      if (Objects.nonNull(propuesta.getFechaEnvioCES())) {
        pbcredOperacion.setFechaEnvioCES(
            new SimpleDateFormat("yyyy-MM-dd").format(propuesta.getFechaEnvioCES()));
      }
      // -FechaRecepAprob
      if (Objects.nonNull(propuesta.getFechaRecepcionAprobacionAdvisoryNote())) {
        pbcredOperacion.setFechaRecepAprob(
            new SimpleDateFormat("yyyy-MM-dd")
                .format(propuesta.getFechaRecepcionAprobacionAdvisoryNote()));
      }

      // -- ACREDITADO + TERCEROS --
      if (!propuesta.getContratos().isEmpty()) {
        ContratoInterviniente titular =
            propuesta.getContratos().stream()
                .map(Contrato::getIntervinientes)
                .flatMap(Collection::stream)
                .filter(ContratoInterviniente::isPrimerInterviniente)
                .max(Comparator.comparing(ci -> ci.getContrato().getSaldoGestion()))
                .orElse(null);

        if (Objects.nonNull(titular)) {
          PBCREDInterviniente interv = new PBCREDInterviniente();
          // -CodInterviniente
          interv.setCodInterviniente(titular.getInterviniente().getIdCarga());
          // -Nombre
          interv.setNombre(titular.getInterviniente().getNombre());
          // -Apellidos
          interv.setApellidos(titular.getInterviniente().getApellidos());
          // -TipoPersona
          interv.setTipoPersona(titular.getInterviniente().getPersonaJuridica() ? "J" : "F");
          // -FechaNacimiento
          interv.setFechaNacimiento(
              Objects.nonNull(titular.getInterviniente().getFechaNacimiento())
                  ? new SimpleDateFormat("yyyy-MM-dd")
                      .format(titular.getInterviniente().getFechaNacimiento())
                  : null);
          // -PaisNacimiento
          interv.setPaisNacimiento(
              Objects.nonNull(titular.getInterviniente().getPaisNacimiento())
                  ? String.valueOf(titular.getInterviniente().getPaisNacimiento().getCodigo())
                  : null);
          // -PaisNacionalidad
          interv.setPaisNacionalidad(
              Objects.nonNull(titular.getInterviniente().getNacionalidad())
                  ? String.valueOf(titular.getInterviniente().getNacionalidad().getCodigo())
                  : null);
          // -Documento
          interv.setNumeroDocumento(titular.getInterviniente().getNumeroDocumento());
          // -TipoDocumento
          interv.setTipoDocumento(
              Objects.nonNull(titular.getInterviniente().getTipoDocumento())
                  ? this.obtenerCodigoTipoDocumentoPBC(
                      titular.getInterviniente().getTipoDocumento().getValor())
                  : null);
          // -FechaConstitucion
          interv.setFechaConstitucion(
              Objects.nonNull(titular.getInterviniente().getFechaConstitucion())
                  ? new SimpleDateFormat("yyyy-MM-dd")
                      .format(titular.getInterviniente().getFechaConstitucion())
                  : null);

          PBCREDAcreditado acreditado = new PBCREDAcreditado();
          acreditado.setInterviniente(interv);
          pbcredOperacion.setAcreditado(acreditado);
        }

        List<ContratoInterviniente> terceros =
            propuesta.getContratos().stream()
                .map(Contrato::getIntervinientes)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());

        if (terceros.size() > 1) {
          terceros.remove(titular);
          pbcredOperacion.setTerceros(new ArrayOfPBCREDTercero());
          for (ContratoInterviniente tercero : terceros) {
            PBCREDTercero pbcredTercero = new PBCREDTercero();
            // -TipoRelacion
            pbcredTercero.setTipoRelacion(
                this.obtenerValorTipoRelacionPBC(tercero.getTipoIntervencion().getValor()));
            // -Interviniente
            PBCREDInterviniente interv = new PBCREDInterviniente();
            // -CodInterviniente
            interv.setCodInterviniente(tercero.getInterviniente().getIdCarga());
            // -Nombre
            interv.setNombre(tercero.getInterviniente().getNombre());
            // -Apellidos
            interv.setApellidos(tercero.getInterviniente().getApellidos());
            // -TipoPersona
            interv.setTipoPersona(tercero.getInterviniente().getPersonaJuridica() ? "J" : "F");
            // -FechaNacimiento
            interv.setFechaNacimiento(
                Objects.nonNull(tercero.getInterviniente().getFechaNacimiento())
                    ? new SimpleDateFormat("yyyy-MM-dd")
                        .format(tercero.getInterviniente().getFechaNacimiento())
                    : null);
            // -PaisNacimiento
            interv.setPaisNacimiento(
                Objects.nonNull(tercero.getInterviniente().getPaisNacimiento())
                    ? String.valueOf(tercero.getInterviniente().getPaisNacimiento().getCodigo())
                    : null);
            // -PaisNacionalidad
            interv.setPaisNacionalidad(
                Objects.nonNull(tercero.getInterviniente().getNacionalidad())
                    ? String.valueOf(tercero.getInterviniente().getNacionalidad().getCodigo())
                    : null);
            // -Documento
            interv.setNumeroDocumento(tercero.getInterviniente().getNumeroDocumento());
            // -TipoDocumento
            interv.setTipoDocumento(
                Objects.nonNull(tercero.getInterviniente().getTipoDocumento())
                    ? this.obtenerCodigoTipoDocumentoPBC(
                        titular.getInterviniente().getTipoDocumento().getValor())
                    : null);
            // -FechaConstitucion
            interv.setFechaConstitucion(
                Objects.nonNull(tercero.getInterviniente().getFechaConstitucion())
                    ? new SimpleDateFormat("yyyy-MM-dd")
                        .format(tercero.getInterviniente().getFechaConstitucion())
                    : null);

            pbcredTercero.setInterviniente(interv);
            pbcredOperacion.getTerceros().getPBCREDTercero().add(pbcredTercero);
          }
        }
      }

      // -- CONTRATOS --
      if (!propuesta.getContratos().isEmpty()) {
        pbcredOperacion.setContratos(new ArrayOfPBCREDContrato());
        for (Contrato contrato : propuesta.getContratos()) {
          PBCREDContrato pbcredContrato = new PBCREDContrato();
          // -IdAAFF
          pbcredContrato.setIdAAFF(Integer.parseInt(contrato.getIdHaya()));
          // -ImporteColabora
          if (Objects.nonNull(propuesta.getImporteColabora())) {
            pbcredContrato.setImporteColabora(BigDecimal.valueOf(propuesta.getImporteColabora()));
          }
          // -ImporteCancelacion ?
          pbcredContrato.setImporteCancelacion(
              Objects.nonNull(propuesta.getImporteTotal())
                  ? BigDecimal.valueOf(propuesta.getImporteTotal())
                  : null);
          // -ImporteCondonacion
          if (!propuesta.getImportesPropuestas().isEmpty()) {
            var importeCondonacion =
                new ArrayList<>(propuesta.getImportesPropuestas()).get(0).getCondonacionDeuda();

            pbcredContrato.setImporteCondonacion(
                Objects.nonNull(importeCondonacion)
                    ? BigDecimal.valueOf(importeCondonacion)
                    : null);
          }
          // -ImporteDesembolso
          pbcredContrato.setImporteDesembolso(
              Objects.nonNull(contrato.getImportesPropuestas())
                  ? BigDecimal.valueOf(
                      new ArrayList<>(contrato.getImportesPropuestas())
                          .get(0)
                          .getImporteEntregaDineraria())
                  : null);
          // -ImporteRemanente
          pbcredContrato.setImporteRemanente(
              Objects.nonNull(contrato.getRestoHipotecario())
                  ? BigDecimal.valueOf(contrato.getRestoHipotecario())
                  : null);
          // -- FINCAS REGISTRALES --
          if (!contrato.getBienes().isEmpty()) {
            pbcredContrato.setFincasRegistrales(new ArrayOfPBCREDFincaRegistral());
            for (ContratoBien contratoBien : contrato.getBienes()) {
              PBCREDFincaRegistral pbcredFincaRegistral = new PBCREDFincaRegistral();
              // -IdAAII
              pbcredFincaRegistral.setIdAAII(contratoBien.getBien().getIdHaya());
              // -FincaRegistral
              pbcredFincaRegistral.setNumeroFincaRegistral(contratoBien.getBien().getFinca());
              // -ImporteFinca
              pbcredFincaRegistral.setImporteFinca(
                  BigDecimal.valueOf(contratoBien.getBien().getResponsabilidadHipotecaria()));

              pbcredContrato
                  .getFincasRegistrales()
                  .getPBCREDFincaRegistral()
                  .add(pbcredFincaRegistral);
            }
          }
          pbcredOperacion.getContratos().getPBCREDContrato().add(pbcredContrato);
        }
      }
    }
    return pbcredOperacion;
  }

  private String obtenerValorTipoProductoPBC(String valorTipoProductoFenix) {
    switch (valorTipoProductoFenix) {
      case "Hipoteca":
      case "Préstamo promotor":
      case "Préstamo personal":
      case "Préstamo consumo":
        return "7";
      case "Tarjeta":
        return "9";
      case "Descubierto":
        return "3";
      case "Leasing":
        return "11";
      case "Renting":
        return "8";
      case "Aval":
        return "1";
      case "Pólizas":
        return null;
      default:
        return null;
    }
  }

  private String obtenerCodigoTipoDocumentoPBC(String valorDocumentoFenix) {
    switch (valorDocumentoFenix) {
      case "C.I.F. NACIONAL":
      case "C.I.F. EXTRANJERO":
      case "C.I.F. PROVISIONAL":
      case "C.I.F.EXTRANJERO DE":
        return "C";
      case "D.N.I.":
      case "N.I.F. NACIONAL":
      case "< 14 NIF PROV. AEAT":
        return "D";
      case "NIE":
        return "N";
      case "PASAPORTE":
        return "P";
      case "TARJETA RESIDENCIA":
        return "T";
      default:
        return "O";
    }
  }

  private String obtenerValorTipoRelacionPBC(String valorTipoRelacionFenix) {
    switch (valorTipoRelacionFenix) {
      case "TITULAR":
        return "1";
      case "AVALISTA":
      case "HIPOTECANTE NO DEUDOR":
      case "CORREDOR COMERCIO":
      case "ORGANISMO DE SUBVENCIÓN":
      case "AVALISTA NO SOLIDARIO":
      case "PIGNO.N.DE":
      case "ENT. CEDENTE":
      case "ASEGURADOR":
        return "2";
      case "ASEGURADO":
      case "BANCO AGENTE":
      case "ENT. GESTORA":
        return "3";
      case "TUTOR":
      case "REPR. LEGAL":
      case "NOTARIO":
      case "APODERADO":
      case "USUFRUCTUARIO":
      case "APODERADO DEL AVALISTA":
      case "APODERADO AVALISTA NO SOLIDARIO":
      case "APOD. DEL BENEFICIA":
      case "APOD. HIPOTECANT":
      case "APOD. DE AVAL. SUBSI":
      case "APO.PIGNOD":
        return "4";
      default:
        return null;
    }
  }
}
