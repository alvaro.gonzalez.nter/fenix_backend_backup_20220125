package com.haya.alaska.integraciones.pbc.wsdl;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase Java para PBC_RED_Contrato complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta
 * clase.
 *
 * <pre>
 * &lt;complexType name="PBC_RED_Contrato"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdAAFF" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="ImporteColabora" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ImporteCancelacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ImporteCondonacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ImporteDesembolso" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ImporteAportacionBienes" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ImporteRemanente" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="FincasRegistrales" type="{http://intranet.haya.es/Pipeline/}ArrayOfPBC_RED_FincaRegistral" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "PBC_RED_Contrato",
    propOrder = {
      "idAAFF",
      "importeColabora",
      "importeCancelacion",
      "importeCondonacion",
      "importeDesembolso",
      "importeAportacionBienes",
      "importeRemanente",
      "fincasRegistrales"
    })
public class PBCREDContrato {

  @XmlElement(name = "IdAAFF")
  protected int idAAFF;

  @XmlElement(name = "ImporteColabora", required = true)
  protected BigDecimal importeColabora;

  @XmlElement(name = "ImporteCancelacion", required = true)
  protected BigDecimal importeCancelacion;

  @XmlElement(name = "ImporteCondonacion", required = true)
  protected BigDecimal importeCondonacion;

  @XmlElement(name = "ImporteDesembolso", required = true)
  protected BigDecimal importeDesembolso;

  @XmlElement(name = "ImporteAportacionBienes", required = true)
  protected BigDecimal importeAportacionBienes;

  @XmlElement(name = "ImporteRemanente", required = true)
  protected BigDecimal importeRemanente;

  @XmlElement(name = "FincasRegistrales")
  protected ArrayOfPBCREDFincaRegistral fincasRegistrales;

  /**
   * Obtiene el valor de la propiedad idAAFF.
   *
   * @return possible object is {@link int }
   */
  public int getIdAAFF() {
    return idAAFF;
  }

  /**
   * Define el valor de la propiedad idAAFF.
   *
   * @param value allowed object is {@link int }
   */
  public void setIdAAFF(int value) {
    this.idAAFF = value;
  }

  /**
   * Obtiene el valor de la propiedad importeColabora.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getImporteColabora() {
    return importeColabora;
  }

  /**
   * Define el valor de la propiedad importeColabora.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setImporteColabora(BigDecimal value) {
    this.importeColabora = value;
  }

  /**
   * Obtiene el valor de la propiedad importeCancelacion.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getImporteCancelacion() {
    return importeCancelacion;
  }

  /**
   * Define el valor de la propiedad importeCancelacion.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setImporteCancelacion(BigDecimal value) {
    this.importeCancelacion = value;
  }

  /**
   * Obtiene el valor de la propiedad importeCondonacion.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getImporteCondonacion() {
    return importeCondonacion;
  }

  /**
   * Define el valor de la propiedad importeCondonacion.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setImporteCondonacion(BigDecimal value) {
    this.importeCondonacion = value;
  }

  /**
   * Obtiene el valor de la propiedad importeDesembolso.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getImporteDesembolso() {
    return importeDesembolso;
  }

  /**
   * Define el valor de la propiedad importeDesembolso.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setImporteDesembolso(BigDecimal value) {
    this.importeDesembolso = value;
  }

  /**
   * Obtiene el valor de la propiedad importeAportacionBienes.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getImporteAportacionBienes() {
    return importeAportacionBienes;
  }

  /**
   * Define el valor de la propiedad importeAportacionBienes.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setImporteAportacionBienes(BigDecimal value) {
    this.importeAportacionBienes = value;
  }

  /**
   * Obtiene el valor de la propiedad importeRemanente.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getImporteRemanente() {
    return importeRemanente;
  }

  /**
   * Define el valor de la propiedad importeRemanente.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setImporteRemanente(BigDecimal value) {
    this.importeRemanente = value;
  }

  /**
   * Obtiene el valor de la propiedad fincasRegistrales.
   *
   * @return possible object is {@link ArrayOfPBCREDFincaRegistral }
   */
  public ArrayOfPBCREDFincaRegistral getFincasRegistrales() {
    return fincasRegistrales;
  }

  /**
   * Define el valor de la propiedad fincasRegistrales.
   *
   * @param value allowed object is {@link ArrayOfPBCREDFincaRegistral }
   */
  public void setFincasRegistrales(ArrayOfPBCREDFincaRegistral value) {
    this.fincasRegistrales = value;
  }
}
