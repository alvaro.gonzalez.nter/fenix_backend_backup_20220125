package com.haya.alaska.integraciones.pbc.infrastructure.soap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.transport.http.HttpUrlConnectionMessageSender;

import java.time.Duration;

@Configuration
public class SoapConfiguration {

  @Value("${integraciones.pbc.url}")
  private String urlPbcService;

  @Value("${integraciones.pbc.wsdl}")
  private String wsdlPackage;

  @Value("${integraciones.pbc.timeout}")
  private int timeout;

  private Jaxb2Marshaller marshaller() {
    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
    marshaller.setContextPath(wsdlPackage);
    return marshaller;
  }

  @Bean
  public SoapClient soapClient() {
    Jaxb2Marshaller marshaller = marshaller();

    SoapClient client = new SoapClient();
    client.setDefaultUri(urlPbcService);
    client.setMarshaller(marshaller);
    client.setUnmarshaller(marshaller);
    client.setMessageSender(httpUrlConnectionMessageSender());
    return client;
  }

  @Bean
  public HttpUrlConnectionMessageSender httpUrlConnectionMessageSender() {
    Duration limit = Duration.ofSeconds(timeout);
    HttpUrlConnectionMessageSender httpUrlConnectionMessageSender =
        new HttpUrlConnectionMessageSender();
    httpUrlConnectionMessageSender.setReadTimeout(limit);
    httpUrlConnectionMessageSender.setConnectionTimeout(limit);
    return httpUrlConnectionMessageSender;
  }
}
