package com.haya.alaska.integraciones.pbc.infrastructure.controller.dto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UpdateResultadoPBCInputDTO implements Serializable {

  private String codExpediente;
  private String resultadoPBC;
  private Date fechaResolucionPBC;
}
