package com.haya.alaska.integraciones.pbc.infrastructure.controller;

import com.haya.alaska.integraciones.pbc.infrastructure.controller.dto.UpdateResultadoPBCInputDTO;
import com.haya.alaska.propuesta.aplication.PropuestaUseCase;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("api/v1/pbc")
public class PbcController {

  @Autowired PropuestaUseCase propuestaUseCase;

  @ApiOperation(value = "Modificar el resultado pbc de una propuesta de un expediente")
  @PutMapping("/{idPropuesta}/resultado-pbc")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void updateResultadoPBC(
    @PathVariable("idPropuesta") Integer idPropuesta,
    @RequestBody UpdateResultadoPBCInputDTO updateResultadoPBCInputDTO,
    @ApiIgnore CustomUserDetails principal)
      throws Exception {

    propuestaUseCase.updateResultadoPBC(idPropuesta, updateResultadoPBCInputDTO, (Usuario) principal.getPrincipal());
  }
}
