package com.haya.alaska.integraciones.pbc.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase Java para ArrayOfPBC_RED_FincaRegistral complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta
 * clase.
 *
 * <pre>
 * &lt;complexType name="ArrayOfPBC_RED_FincaRegistral"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PBC_RED_FincaRegistral" type="{http://intranet.haya.es/Pipeline/}PBC_RED_FincaRegistral" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "ArrayOfPBC_RED_FincaRegistral",
    propOrder = {"pbcredFincaRegistral"})
public class ArrayOfPBCREDFincaRegistral {

  @XmlElement(name = "PBC_RED_FincaRegistral", nillable = true)
  protected List<PBCREDFincaRegistral> pbcredFincaRegistral;

  /**
   * Gets the value of the pbcredFincaRegistral property.
   *
   * <p>This accessor method returns a reference to the live list, not a snapshot. Therefore any
   * modification you make to the returned list will be present inside the JAXB object. This is why
   * there is not a <CODE>set</CODE> method for the pbcredFincaRegistral property.
   *
   * <p>For example, to add a new item, do as follows:
   *
   * <pre>
   *    getPBCREDFincaRegistral().add(newItem);
   * </pre>
   *
   * <p>Objects of the following type(s) are allowed in the list {@link PBCREDFincaRegistral }
   */
  public List<PBCREDFincaRegistral> getPBCREDFincaRegistral() {
    if (pbcredFincaRegistral == null) {
      pbcredFincaRegistral = new ArrayList<PBCREDFincaRegistral>();
    }
    return this.pbcredFincaRegistral;
  }
}
