package com.haya.alaska.integraciones.pbc.wsdl;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase Java para PBC_RED_FincaRegistral complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta
 * clase.
 *
 * <pre>
 * &lt;complexType name="PBC_RED_FincaRegistral"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdAAII" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroFincaRegistral" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ImporteFinca" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="TasacionAtlas" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "PBC_RED_FincaRegistral",
    propOrder = {"idAAII", "numeroFincaRegistral", "importeFinca", "tasacionAtlas"})
public class PBCREDFincaRegistral {

  @XmlElement(name = "IdAAII")
  protected String idAAII;

  @XmlElement(name = "NumeroFincaRegistral")
  protected String numeroFincaRegistral;

  @XmlElement(name = "ImporteFinca", required = true)
  protected BigDecimal importeFinca;

  @XmlElement(name = "TasacionAtlas", required = true)
  protected BigDecimal tasacionAtlas;

  /**
   * Obtiene el valor de la propiedad idAAII.
   *
   * @return possible object is {@link String }
   */
  public String getIdAAII() {
    return idAAII;
  }

  /**
   * Define el valor de la propiedad idAAII.
   *
   * @param value allowed object is {@link String }
   */
  public void setIdAAII(String value) {
    this.idAAII = value;
  }

  /**
   * Obtiene el valor de la propiedad numeroFincaRegistral.
   *
   * @return possible object is {@link String }
   */
  public String getNumeroFincaRegistral() {
    return numeroFincaRegistral;
  }

  /**
   * Define el valor de la propiedad numeroFincaRegistral.
   *
   * @param value allowed object is {@link String }
   */
  public void setNumeroFincaRegistral(String value) {
    this.numeroFincaRegistral = value;
  }

  /**
   * Obtiene el valor de la propiedad importeFinca.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getImporteFinca() {
    return importeFinca;
  }

  /**
   * Define el valor de la propiedad importeFinca.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setImporteFinca(BigDecimal value) {
    this.importeFinca = value;
  }

  /**
   * Obtiene el valor de la propiedad tasacionAtlas.
   *
   * @return possible object is {@link BigDecimal }
   */
  public BigDecimal getTasacionAtlas() {
    return tasacionAtlas;
  }

  /**
   * Define el valor de la propiedad tasacionAtlas.
   *
   * @param value allowed object is {@link BigDecimal }
   */
  public void setTasacionAtlas(BigDecimal value) {
    this.tasacionAtlas = value;
  }
}
