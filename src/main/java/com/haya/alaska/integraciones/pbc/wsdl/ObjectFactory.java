package com.haya.alaska.integraciones.pbc.wsdl;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java element interface
 * generated in the es.haya.intranet.pipeline package.
 *
 * <p>An ObjectFactory allows you to programatically construct new instances of the Java
 * representation for XML content. The Java representation of XML content can consist of schema
 * derived interfaces and classes representing the binding of schema type definitions, element
 * declarations and model groups. Factory methods for each of these are provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

  /**
   * Create a new ObjectFactory that can be used to create new instances of schema derived classes
   * for package: es.haya.intranet.pipeline
   */
  public ObjectFactory() {}

  /** Create an instance of {@link WsPBCCompleted } */
  public WsPBCCompleted createWsPBCCompleted() {
    return new WsPBCCompleted();
  }

  /** Create an instance of {@link PBCREDOperacion } */
  public PBCREDOperacion createPBCREDOperacion() {
    return new PBCREDOperacion();
  }

  /** Create an instance of {@link WsPBCCompletedResponse } */
  public WsPBCCompletedResponse createWsPBCCompletedResponse() {
    return new WsPBCCompletedResponse();
  }

  /** Create an instance of {@link PBCREDAcreditado } */
  public PBCREDAcreditado createPBCREDAcreditado() {
    return new PBCREDAcreditado();
  }

  /** Create an instance of {@link PBCREDInterviniente } */
  public PBCREDInterviniente createPBCREDInterviniente() {
    return new PBCREDInterviniente();
  }

  /** Create an instance of {@link ArrayOfPBCREDTitularReal } */
  public ArrayOfPBCREDTitularReal createArrayOfPBCREDTitularReal() {
    return new ArrayOfPBCREDTitularReal();
  }

  /** Create an instance of {@link PBCREDTitularReal } */
  public PBCREDTitularReal createPBCREDTitularReal() {
    return new PBCREDTitularReal();
  }

  /** Create an instance of {@link PBCREDRepresentante } */
  public PBCREDRepresentante createPBCREDRepresentante() {
    return new PBCREDRepresentante();
  }

  /** Create an instance of {@link ArrayOfPBCREDTercero } */
  public ArrayOfPBCREDTercero createArrayOfPBCREDTercero() {
    return new ArrayOfPBCREDTercero();
  }

  /** Create an instance of {@link PBCREDTercero } */
  public PBCREDTercero createPBCREDTercero() {
    return new PBCREDTercero();
  }

  /** Create an instance of {@link ArrayOfPBCREDContrato } */
  public ArrayOfPBCREDContrato createArrayOfPBCREDContrato() {
    return new ArrayOfPBCREDContrato();
  }

  /** Create an instance of {@link PBCREDContrato } */
  public PBCREDContrato createPBCREDContrato() {
    return new PBCREDContrato();
  }

  /** Create an instance of {@link ArrayOfPBCREDFincaRegistral } */
  public ArrayOfPBCREDFincaRegistral createArrayOfPBCREDFincaRegistral() {
    return new ArrayOfPBCREDFincaRegistral();
  }

  /** Create an instance of {@link PBCREDFincaRegistral } */
  public PBCREDFincaRegistral createPBCREDFincaRegistral() {
    return new PBCREDFincaRegistral();
  }
}
