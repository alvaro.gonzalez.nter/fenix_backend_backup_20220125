package com.haya.alaska.integraciones.pbc.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase Java para PBC_RED_Operacion complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta
 * clase.
 *
 * <pre>
 * &lt;complexType name="PBC_RED_Operacion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IdPropuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CodExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EstadoPropuesta" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SancionPropuesta" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TipoPropuesta" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SubTipoPropuesta" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Cartera" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="FechaPrevistaF5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaAlta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaEnvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaAcordada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaEscrituracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IdColabora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Provincia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TipoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WorkResources" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="WorkResourcesDes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Vinculaciones" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="VinculacionesDes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FirmaGestoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AltoRiesgo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AmbitoPBC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumAdvisoryNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaEnvioWG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaEnvioCES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FechaRecepAprob" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Acreditado" type="{http://intranet.haya.es/Pipeline/}PBC_RED_Acreditado" minOccurs="0"/&gt;
 *         &lt;element name="Terceros" type="{http://intranet.haya.es/Pipeline/}ArrayOfPBC_RED_Tercero" minOccurs="0"/&gt;
 *         &lt;element name="Contratos" type="{http://intranet.haya.es/Pipeline/}ArrayOfPBC_RED_Contrato" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "PBC_RED_Operacion",
    propOrder = {
      "idPropuesta",
      "codExpediente",
      "estadoPropuesta",
      "sancionPropuesta",
      "tipoPropuesta",
      "subTipoPropuesta",
      "cartera",
      "fechaPrevistaF5",
      "fechaAlta",
      "fechaEnvio",
      "fechaSancion",
      "fechaAcordada",
      "fechaEscrituracion",
      "idColabora",
      "provincia",
      "tipoProducto",
      "workResources",
      "workResourcesDes",
      "vinculaciones",
      "vinculacionesDes",
      "firmaGestoria",
      "altoRiesgo",
      "ambitoPBC",
      "numAdvisoryNote",
      "fechaEnvioWG",
      "fechaEnvioCES",
      "fechaRecepAprob",
      "acreditado",
      "terceros",
      "contratos"
    })
public class PBCREDOperacion {

  @XmlElement(name = "IdPropuesta")
  protected String idPropuesta;

  @XmlElement(name = "CodExpediente")
  protected String codExpediente;

  @XmlElement(name = "EstadoPropuesta")
  protected int estadoPropuesta;

  @XmlElement(name = "SancionPropuesta")
  protected int sancionPropuesta;

  @XmlElement(name = "TipoPropuesta")
  protected int tipoPropuesta;

  @XmlElement(name = "SubTipoPropuesta")
  protected int subTipoPropuesta;

  @XmlElement(name = "Cartera")
  protected int cartera;

  @XmlElement(name = "FechaPrevistaF5")
  protected String fechaPrevistaF5;

  @XmlElement(name = "FechaAlta")
  protected String fechaAlta;

  @XmlElement(name = "FechaEnvio")
  protected String fechaEnvio;

  @XmlElement(name = "FechaSancion")
  protected String fechaSancion;

  @XmlElement(name = "FechaAcordada")
  protected String fechaAcordada;

  @XmlElement(name = "FechaEscrituracion")
  protected String fechaEscrituracion;

  @XmlElement(name = "IdColabora")
  protected String idColabora;

  @XmlElement(name = "Provincia")
  protected String provincia;

  @XmlElement(name = "TipoProducto")
  protected String tipoProducto;

  @XmlElement(name = "WorkResources")
  protected int workResources;

  @XmlElement(name = "WorkResourcesDes")
  protected String workResourcesDes;

  @XmlElement(name = "Vinculaciones")
  protected int vinculaciones;

  @XmlElement(name = "VinculacionesDes")
  protected String vinculacionesDes;

  @XmlElement(name = "FirmaGestoria")
  protected String firmaGestoria;

  @XmlElement(name = "AltoRiesgo")
  protected String altoRiesgo;

  @XmlElement(name = "AmbitoPBC")
  protected String ambitoPBC;

  @XmlElement(name = "NumAdvisoryNote")
  protected String numAdvisoryNote;

  @XmlElement(name = "FechaEnvioWG")
  protected String fechaEnvioWG;

  @XmlElement(name = "FechaEnvioCES")
  protected String fechaEnvioCES;

  @XmlElement(name = "FechaRecepAprob")
  protected String fechaRecepAprob;

  @XmlElement(name = "Acreditado")
  protected PBCREDAcreditado acreditado;

  @XmlElement(name = "Terceros")
  protected ArrayOfPBCREDTercero terceros;

  @XmlElement(name = "Contratos")
  protected ArrayOfPBCREDContrato contratos;

  /**
   * Obtiene el valor de la propiedad idPropuesta.
   *
   * @return possible object is {@link String }
   */
  public String getIdPropuesta() {
    return idPropuesta;
  }

  /**
   * Define el valor de la propiedad idPropuesta.
   *
   * @param value allowed object is {@link String }
   */
  public void setIdPropuesta(String value) {
    this.idPropuesta = value;
  }

  /**
   * Obtiene el valor de la propiedad codExpediente.
   *
   * @return possible object is {@link String }
   */
  public String getCodExpediente() {
    return codExpediente;
  }

  /**
   * Define el valor de la propiedad codExpediente.
   *
   * @param value allowed object is {@link String }
   */
  public void setCodExpediente(String value) {
    this.codExpediente = value;
  }

  /** Obtiene el valor de la propiedad estadoPropuesta. */
  public int getEstadoPropuesta() {
    return estadoPropuesta;
  }

  /** Define el valor de la propiedad estadoPropuesta. */
  public void setEstadoPropuesta(int value) {
    this.estadoPropuesta = value;
  }

  /** Obtiene el valor de la propiedad sancionPropuesta. */
  public int getSancionPropuesta() {
    return sancionPropuesta;
  }

  /** Define el valor de la propiedad sancionPropuesta. */
  public void setSancionPropuesta(int value) {
    this.sancionPropuesta = value;
  }

  /** Obtiene el valor de la propiedad tipoPropuesta. */
  public int getTipoPropuesta() {
    return tipoPropuesta;
  }

  /** Define el valor de la propiedad tipoPropuesta. */
  public void setTipoPropuesta(int value) {
    this.tipoPropuesta = value;
  }

  /** Obtiene el valor de la propiedad subTipoPropuesta. */
  public int getSubTipoPropuesta() {
    return subTipoPropuesta;
  }

  /** Define el valor de la propiedad subTipoPropuesta. */
  public void setSubTipoPropuesta(int value) {
    this.subTipoPropuesta = value;
  }

  /** Obtiene el valor de la propiedad cartera. */
  public int getCartera() {
    return cartera;
  }

  /** Define el valor de la propiedad cartera. */
  public void setCartera(int value) {
    this.cartera = value;
  }

  /**
   * Obtiene el valor de la propiedad fechaPrevistaF5.
   *
   * @return possible object is {@link String }
   */
  public String getFechaPrevistaF5() {
    return fechaPrevistaF5;
  }

  /**
   * Define el valor de la propiedad fechaPrevistaF5.
   *
   * @param value allowed object is {@link String }
   */
  public void setFechaPrevistaF5(String value) {
    this.fechaPrevistaF5 = value;
  }

  /**
   * Obtiene el valor de la propiedad fechaAlta.
   *
   * @return possible object is {@link String }
   */
  public String getFechaAlta() {
    return fechaAlta;
  }

  /**
   * Define el valor de la propiedad fechaAlta.
   *
   * @param value allowed object is {@link String }
   */
  public void setFechaAlta(String value) {
    this.fechaAlta = value;
  }

  /**
   * Obtiene el valor de la propiedad fechaEnvio.
   *
   * @return possible object is {@link String }
   */
  public String getFechaEnvio() {
    return fechaEnvio;
  }

  /**
   * Define el valor de la propiedad fechaEnvio.
   *
   * @param value allowed object is {@link String }
   */
  public void setFechaEnvio(String value) {
    this.fechaEnvio = value;
  }

  /**
   * Obtiene el valor de la propiedad fechaSancion.
   *
   * @return possible object is {@link String }
   */
  public String getFechaSancion() {
    return fechaSancion;
  }

  /**
   * Define el valor de la propiedad fechaSancion.
   *
   * @param value allowed object is {@link String }
   */
  public void setFechaSancion(String value) {
    this.fechaSancion = value;
  }

  /**
   * Obtiene el valor de la propiedad fechaAcordada.
   *
   * @return possible object is {@link String }
   */
  public String getFechaAcordada() {
    return fechaAcordada;
  }

  /**
   * Define el valor de la propiedad fechaAcordada.
   *
   * @param value allowed object is {@link String }
   */
  public void setFechaAcordada(String value) {
    this.fechaAcordada = value;
  }

  /**
   * Obtiene el valor de la propiedad fechaEscrituracion.
   *
   * @return possible object is {@link String }
   */
  public String getFechaEscrituracion() {
    return fechaEscrituracion;
  }

  /**
   * Define el valor de la propiedad fechaEscrituracion.
   *
   * @param value allowed object is {@link String }
   */
  public void setFechaEscrituracion(String value) {
    this.fechaEscrituracion = value;
  }

  /**
   * Obtiene el valor de la propiedad idColabora.
   *
   * @return possible object is {@link String }
   */
  public String getIdColabora() {
    return idColabora;
  }

  /**
   * Define el valor de la propiedad idColabora.
   *
   * @param value allowed object is {@link String }
   */
  public void setIdColabora(String value) {
    this.idColabora = value;
  }

  /**
   * Obtiene el valor de la propiedad provincia.
   *
   * @return possible object is {@link String }
   */
  public String getProvincia() {
    return provincia;
  }

  /**
   * Define el valor de la propiedad provincia.
   *
   * @param value allowed object is {@link String }
   */
  public void setProvincia(String value) {
    this.provincia = value;
  }

  /**
   * Obtiene el valor de la propiedad tipoProducto.
   *
   * @return possible object is {@link String }
   */
  public String getTipoProducto() {
    return tipoProducto;
  }

  /**
   * Define el valor de la propiedad tipoProducto.
   *
   * @param value allowed object is {@link String }
   */
  public void setTipoProducto(String value) {
    this.tipoProducto = value;
  }

  /** Obtiene el valor de la propiedad workResources. */
  public int getWorkResources() {
    return workResources;
  }

  /** Define el valor de la propiedad workResources. */
  public void setWorkResources(int value) {
    this.workResources = value;
  }

  /**
   * Obtiene el valor de la propiedad workResourcesDes.
   *
   * @return possible object is {@link String }
   */
  public String getWorkResourcesDes() {
    return workResourcesDes;
  }

  /**
   * Define el valor de la propiedad workResourcesDes.
   *
   * @param value allowed object is {@link String }
   */
  public void setWorkResourcesDes(String value) {
    this.workResourcesDes = value;
  }

  /** Obtiene el valor de la propiedad vinculaciones. */
  public int getVinculaciones() {
    return vinculaciones;
  }

  /** Define el valor de la propiedad vinculaciones. */
  public void setVinculaciones(int value) {
    this.vinculaciones = value;
  }

  /**
   * Obtiene el valor de la propiedad vinculacionesDes.
   *
   * @return possible object is {@link String }
   */
  public String getVinculacionesDes() {
    return vinculacionesDes;
  }

  /**
   * Define el valor de la propiedad vinculacionesDes.
   *
   * @param value allowed object is {@link String }
   */
  public void setVinculacionesDes(String value) {
    this.vinculacionesDes = value;
  }

  /**
   * Obtiene el valor de la propiedad firmaGestoria.
   *
   * @return possible object is {@link String }
   */
  public String getFirmaGestoria() {
    return firmaGestoria;
  }

  /**
   * Define el valor de la propiedad firmaGestoria.
   *
   * @param value allowed object is {@link String }
   */
  public void setFirmaGestoria(String value) {
    this.firmaGestoria = value;
  }

  /**
   * Obtiene el valor de la propiedad altoRiesgo.
   *
   * @return possible object is {@link String }
   */
  public String getAltoRiesgo() {
    return altoRiesgo;
  }

  /**
   * Define el valor de la propiedad altoRiesgo.
   *
   * @param value allowed object is {@link String }
   */
  public void setAltoRiesgo(String value) {
    this.altoRiesgo = value;
  }

  /**
   * Obtiene el valor de la propiedad ambitoPBC.
   *
   * @return possible object is {@link String }
   */
  public String getAmbitoPBC() {
    return ambitoPBC;
  }

  /**
   * Define el valor de la propiedad ambitoPBC.
   *
   * @param value allowed object is {@link String }
   */
  public void setAmbitoPBC(String value) {
    this.ambitoPBC = value;
  }

  /**
   * Obtiene el valor de la propiedad numAdvisoryNote.
   *
   * @return possible object is {@link String }
   */
  public String getNumAdvisoryNote() {
    return numAdvisoryNote;
  }

  /**
   * Define el valor de la propiedad numAdvisoryNote.
   *
   * @param value allowed object is {@link String }
   */
  public void setNumAdvisoryNote(String value) {
    this.numAdvisoryNote = value;
  }

  /**
   * Obtiene el valor de la propiedad fechaEnvioWG.
   *
   * @return possible object is {@link String }
   */
  public String getFechaEnvioWG() {
    return fechaEnvioWG;
  }

  /**
   * Define el valor de la propiedad fechaEnvioWG.
   *
   * @param value allowed object is {@link String }
   */
  public void setFechaEnvioWG(String value) {
    this.fechaEnvioWG = value;
  }

  /**
   * Obtiene el valor de la propiedad fechaEnvioCES.
   *
   * @return possible object is {@link String }
   */
  public String getFechaEnvioCES() {
    return fechaEnvioCES;
  }

  /**
   * Define el valor de la propiedad fechaEnvioCES.
   *
   * @param value allowed object is {@link String }
   */
  public void setFechaEnvioCES(String value) {
    this.fechaEnvioCES = value;
  }

  /**
   * Obtiene el valor de la propiedad fechaRecepAprob.
   *
   * @return possible object is {@link String }
   */
  public String getFechaRecepAprob() {
    return fechaRecepAprob;
  }

  /**
   * Define el valor de la propiedad fechaRecepAprob.
   *
   * @param value allowed object is {@link String }
   */
  public void setFechaRecepAprob(String value) {
    this.fechaRecepAprob = value;
  }

  /**
   * Obtiene el valor de la propiedad acreditado.
   *
   * @return possible object is {@link PBCREDAcreditado }
   */
  public PBCREDAcreditado getAcreditado() {
    return acreditado;
  }

  /**
   * Define el valor de la propiedad acreditado.
   *
   * @param value allowed object is {@link PBCREDAcreditado }
   */
  public void setAcreditado(PBCREDAcreditado value) {
    this.acreditado = value;
  }

  /**
   * Obtiene el valor de la propiedad terceros.
   *
   * @return possible object is {@link ArrayOfPBCREDTercero }
   */
  public ArrayOfPBCREDTercero getTerceros() {
    return terceros;
  }

  /**
   * Define el valor de la propiedad terceros.
   *
   * @param value allowed object is {@link ArrayOfPBCREDTercero }
   */
  public void setTerceros(ArrayOfPBCREDTercero value) {
    this.terceros = value;
  }

  /**
   * Obtiene el valor de la propiedad contratos.
   *
   * @return possible object is {@link ArrayOfPBCREDContrato }
   */
  public ArrayOfPBCREDContrato getContratos() {
    return contratos;
  }

  /**
   * Define el valor de la propiedad contratos.
   *
   * @param value allowed object is {@link ArrayOfPBCREDContrato }
   */
  public void setContratos(ArrayOfPBCREDContrato value) {
    this.contratos = value;
  }
}
