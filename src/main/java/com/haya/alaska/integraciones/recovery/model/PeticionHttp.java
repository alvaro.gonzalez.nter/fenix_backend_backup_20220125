package com.haya.alaska.integraciones.recovery.model;

import com.haya.alaska.integraciones.recovery.ConfigRecovery;
import com.mashape.unirest.http.JsonNode;
import lombok.SneakyThrows;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.util.Strings;
import org.apache.poi.util.IOUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

public class PeticionHttp {
  private final ConfigRecovery config;
  private final CookieStore cookieStore;

  public PeticionHttp(ConfigRecovery config) {
    this.config = config;
    this.cookieStore = new BasicCookieStore();
  }

  public JsonNode post(String path, JSONObject json) throws IOException {
    HttpClientBuilder builder = HttpClientBuilder.create().setDefaultCookieStore(this.cookieStore);
    try (CloseableHttpClient client = builder.build()) {
      HttpPost request = new HttpPost(this.getUrl(path));
      request.setHeader("Content-Type", "application/json");
      request.setEntity(new ByteArrayEntity(json.toString().getBytes(StandardCharsets.UTF_8)));

      return toJson(getResponse(client, request));
    }
  }

  private String getUrl(String path) {
    return config.getUrl().concat(path);
  }

  public boolean sesionIniciada() {
    this.cookieStore.clearExpired(new Date());
    return this.cookieStore.getCookies().size() != 0;
  }

  @SneakyThrows
  private static RespuestaHttp getResponse(CloseableHttpClient client, HttpRequestBase request) {
    try (CloseableHttpResponse response = client.execute(request)) {
      return new RespuestaHttp(response);
    }
  }

  private static JsonNode toJson(RespuestaHttp respuestaHttp) throws IOException {
    String contentType = respuestaHttp.getHeader("Content-Type");
    if (Strings.isEmpty(contentType) || !contentType.startsWith("application/json")) {
      throw new IllegalArgumentException("Content-Type no válido");
    }
    String data = new String(IOUtils.toByteArray(respuestaHttp.getBody()), StandardCharsets.UTF_8);
    return new JsonNode(data);
  }
}
