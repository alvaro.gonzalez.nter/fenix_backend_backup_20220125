package com.haya.alaska.integraciones.recovery;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import com.haya.alaska.estado_evento.infrastructure.repository.EstadoEventoRepository;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.integraciones.recovery.model.ProcedimientoExcel;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.jcraft.jsch.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

@Component
@Slf4j
public class BatchProcedimientosRecovery {

  @Autowired private EstadoEventoRepository estadoEventoRepository;

  @Autowired private SubtipoEventoRepository subtipoEventoRepository;

  @Autowired private EventoUseCase eventoUseCase;

  @Autowired private ContratoRepository contratoRepository;

  @Autowired private UsuarioRepository usuarioRepository;

  @Value("${spring.profiles.active}")
  String entorno;

  @Value("${integraciones.recovery.procedimiento.csv}")
  String pathCsv;

  @Value("${integraciones.recovery.ftp.user}")
  String user;

  @Value("${integraciones.recovery.ftp.password}")
  String password;

  @Value("${integraciones.recovery.ftp.port}")
  String port;

  @Value("${integraciones.recovery.ftp.host}")
  String host;

  @Value("${integraciones.recovery.knownhosts}")
  String knownHost;

  @Scheduled(cron = "${integraciones.recovery.procedimiento.cron}")
  @Transactional
  private void generarProcedimientosRecovery() throws Exception {
    if(entorno.equals("PRO")) {
      log.info("Iniciando tarea programada: BatchProcedimientoRecovery::generarProcedimientosRecovery");
      List<File> files = recogerFicheros();
      for (File file : files) {
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String linea;
        List<ProcedimientoExcel> lista = new ArrayList<>();
        while ((linea = br.readLine()) != null) lista.add(new ProcedimientoExcel(linea));
        for (ProcedimientoExcel proc : lista) {
          Contrato contrato = contratoRepository.findByIdCarga(proc.getCNT_CONTRATO()).orElse(null);
          if (contrato == null) contrato = contratoRepository.findByIdCarga(añadirCeros(proc.getCNT_CONTRATO())).orElse(null);
          if (contrato == null) continue;
          Expediente expediente = contrato.getExpediente();
          Cartera cartera = expediente.getCartera();
          // subtipo REcovery automatico alerta
          SubtipoEvento se = subtipoEventoRepository.findByValorAndCarteraId("Recovery", cartera.getId()).orElseThrow(() -> new NotFoundException("subtipoEvento", "código", "'Recovery'", "cartera", cartera.getNombre()));

          TipoEvento te = se.getTipoEvento();
          ClaseEvento ce = te.getClaseEvento();

          EventoInputDTO newInput = new EventoInputDTO();
          newInput.setCartera(cartera.getId());
          newInput.setIdExpediente(expediente.getId());

          newInput.setSubtipoEvento(se.getId());
          newInput.setTipoEvento(te.getId());
          newInput.setClaseEvento(ce.getId());

          newInput.setImportancia(2); // Media importancia
          EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("PEN").orElse(null);
          if (estadoEvento == null) throw new NotFoundException("estado evento", "código", "'PEN'");
          newInput.setEstado(estadoEvento.getId());
          newInput.setResultado(null);
          newInput.setFechaAlerta(proc.getFECHACREAR());
          newInput.setFechaLimite(proc.getFECHACREAR());
          newInput.setFechaCreacion(proc.getFECHACREAR());

          newInput.setNivel(2);
          newInput.setNivelId(contrato.getId());
          newInput.setIdRecovery(Long.valueOf(proc.getASU_ID()));

          if (expediente.getGestor() != null) {
            newInput.setDestinatario(expediente.getGestor().getId());
          } else if (expediente.getSupervisor() != null) {
            newInput.setDestinatario(expediente.getSupervisor().getId());
          } else {
            newInput.setDestinatario(cartera.getResponsableCartera().getId());
          }

          newInput.setTitulo("Nuevo hito");
          newInput.setComentariosDestinatario("Nuevo hito: " + proc.getTAR_DESCRIPCION() + "\n\n Nuevo Dato: " + proc.getTEV_NOMBRE() + "\n Nuevo Valor: " + proc.getTEV_VALOR());

          Usuario remitente = usuarioRepository.findByNombre("Recovery").orElse(null);
          eventoUseCase.create(newInput, remitente);
        }
      }
      log.info("Acabada tarea programada: BatchProcedimientoRecovery::generarProcedimientosRecovery");
    }else{
      log.info("No ejecutada tarea programada: BatchProcedimientoRecovery::generarProcedimientosRecovery");
    }
  }

  private List<File> recogerFicheros() throws JSchException, IOException, SftpException {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -1);
    DateFormat formatoFecha = new SimpleDateFormat("yyyyMMdd");
    String fechaAyer = formatoFecha.format(cal.getTime());
    if (fechaAyer.equals("20211007") || fechaAyer.equals("20211008")|| fechaAyer.equals("20211006")) fechaAyer = "20210615";

    List<File> files = new ArrayList<>();
    JSch jsch = new JSch();
    jsch.setKnownHosts(knownHost);
    Session jschSession = jsch.getSession(user, host, Integer.parseInt(port));
    Properties config = new Properties();
    config.put("StrictHostKeyChecking", "no");
    jschSession.setConfig(config);
    jschSession.setPassword(password);
    jschSession.connect();

    ChannelSftp channelSftp = (ChannelSftp) jschSession.openChannel("sftp");
    channelSftp.connect();
    try {
      String archivo = "0182_" + fechaAyer + "_TAREAS_RCV.dat";
      File file = new File("./src/main/resources/recovery/procedimiento/" + archivo);
      file.createNewFile();
      FileOutputStream stream = new FileOutputStream(file);
      channelSftp.get("Archivos/rcvtofenix/" + archivo, stream);
      stream.close();
      files.add(file);
    } catch (Exception ex) {
    }
    try {
      String archivo = "0240_" + fechaAyer + "_TAREAS_RCV.dat";
      File file = new File("./src/main/resources/recovery/procedimiento/" + archivo);
      file.createNewFile();
      FileOutputStream stream = new FileOutputStream(file);
      channelSftp.get("Archivos/rcvtofenix/" + archivo, stream);
      stream.close();
      files.add(file);
    } catch (Exception ex) {
    }
    channelSftp.disconnect();
    return files;
  }

  private String añadirCeros(String idCarga) {
    while (idCarga.length() < 18) idCarga = "0" + idCarga;
    return idCarga;
  }
}
