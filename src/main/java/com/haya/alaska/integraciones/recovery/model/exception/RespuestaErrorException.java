package com.haya.alaska.integraciones.recovery.model.exception;

import lombok.Getter;
import org.json.JSONObject;

@Getter
public class RespuestaErrorException extends Exception {
  private final JSONObject datos;
  private final int estado;

  public RespuestaErrorException(JSONObject datos) {
    super(datos.getString("message"));

    this.datos = datos;
    this.estado = datos.getInt("status");
  }
}
