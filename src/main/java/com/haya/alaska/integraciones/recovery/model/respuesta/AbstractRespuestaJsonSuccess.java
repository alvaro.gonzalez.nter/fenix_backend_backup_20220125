package com.haya.alaska.integraciones.recovery.model.respuesta;

import com.haya.alaska.integraciones.recovery.model.exception.RespuestaErrorException;
import com.mashape.unirest.http.JsonNode;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class AbstractRespuestaJsonSuccess {
  @Getter(AccessLevel.PROTECTED)
  private final JSONObject datos;

  @SneakyThrows
  public AbstractRespuestaJsonSuccess(JsonNode response) {
    this.datos = response.getObject();
    if (!this.datos.getBoolean("success")) {
      throw new RespuestaErrorException(this.datos);
    }
  }

  protected JSONObject getData() {
    return this.datos.getJSONObject("data");
  }

  protected JSONArray getDataArray() {
    return this.datos.getJSONArray("data");
  }
}
