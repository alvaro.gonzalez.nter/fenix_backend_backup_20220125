package com.haya.alaska.integraciones.recovery.model;

import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class ProcedimientoExcel {
  private String ASU_ID; //id asunto recovery
  private String ASU_NOMBRE;
  private String CNT_ID;
  private String CNT_CONTRATO;// id haya contrato
  private String PRC_ID;
  private String TPO_ID;
  private String TPO_CODIGO;
  private String TPO_DESCRIPCION;
  private String TAR_ID;
  private String TAR_CODIGO;
  private String TAR_DESCRIPCION;
  private String TEX_ID;
  private String TEV_ID;
  private String TEV_NOMBRE;
  private String TEV_VALOR;
  private Date FECHACREAR;
  private Date FECHAMODIFICAR;

  public ProcedimientoExcel(String row) throws ParseException {
    String[] fila = row.split("î");
    int celda = 0;
    this.setASU_ID(fila[celda++]);
    this.setASU_NOMBRE(fila[celda++]);
    this.setCNT_ID(fila[celda++]);
    this.setCNT_CONTRATO(fila[celda++]);
    this.setPRC_ID(fila[celda++]);
    this.setTPO_ID(fila[celda++]);
    this.setTPO_CODIGO(fila[celda++]);
    this.setTPO_DESCRIPCION(fila[celda++]);
    this.setTAR_ID(fila[celda++]);
    this.setTAR_CODIGO(fila[celda++]);
    this.setTAR_DESCRIPCION(fila[celda++]);
    this.setTEX_ID(fila[celda++]);
    this.setTEV_ID(fila[celda++]);
    this.setTEV_NOMBRE(fila[celda++]);
    this.setTEV_VALOR(fila[celda++]);
    try {
      String date = fila[celda++];
      if (date != null && date != "")
        this.setFECHACREAR(new SimpleDateFormat("yyyy-MM-dd").parse(date));
    } catch (Exception ex) {
    }
    try {
      String dateM = fila[celda++];
      if (dateM != null && dateM != "")
        this.setFECHAMODIFICAR(new SimpleDateFormat("yyyy-MM-dd").parse(dateM));
    } catch (Exception ex) {
    }
  }

}
