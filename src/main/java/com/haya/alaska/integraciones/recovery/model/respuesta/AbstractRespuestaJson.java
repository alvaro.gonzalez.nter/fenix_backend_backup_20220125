package com.haya.alaska.integraciones.recovery.model.respuesta;

import com.mashape.unirest.http.JsonNode;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import org.json.JSONObject;

public abstract class AbstractRespuestaJson {
  @Getter(AccessLevel.PROTECTED)
  private final JSONObject datos;

  @SneakyThrows
  public AbstractRespuestaJson(JsonNode response) {
    this.datos = response.getObject();
  }
}
