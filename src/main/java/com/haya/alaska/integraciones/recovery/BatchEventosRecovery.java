package com.haya.alaska.integraciones.recovery;

import com.haya.alaska.carga_control_transformacion.domain.CsvBean;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.integraciones.recovery.model.Asunto;
import com.haya.alaska.integraciones.recovery.model.EventoCsvBean;
import com.haya.alaska.integraciones.recovery.model.LineaProcesal;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import com.haya.alaska.interviniente_skip_tracing.infraestructure.repository.IntervinienteSTRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@Slf4j
public class BatchEventosRecovery {

  @Autowired
  private EventoRepository eventoRepository;

  @Autowired
  private SubtipoEventoRepository subtipoEventoRepository;

  @Autowired
  private IntervinienteSTRepository intervinienteSTRepository;

  @Autowired
  private ContratoIntervinienteRepository contratoIntervinienteRepository;

  @Autowired
  private PropuestaRepository propuestaRepository;

  @Autowired
  private ExpedienteRepository expedienteRepository;

  @Value("${spring.profiles.active}")
  String entorno;

  @Value("${integraciones.recovery.alertas.csv}")
  String pathCsv;

  @Value("${integraciones.recovery.ftp.user}")
  String user;

  @Value("${integraciones.recovery.ftp.password}")
  String password;

  @Value("${integraciones.recovery.ftp.port}")
  String port;

  @Value("${integraciones.recovery.ftp.host}")
  String host;

  @Value("${integraciones.recovery.knownhosts}")
  String knownHost;

  SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
  List<CsvBean> eventoCSV = new ArrayList<>();

  @Autowired
  private ServicioRecovery servicioRecovery;

  final String CAJAMAR = "0240";
  final String CERBERUS = "0182";

  SimpleDateFormat formatFichero = new SimpleDateFormat("yyyyMMdd");
  @Scheduled(cron = "${integraciones.recovery.alertas.cron}")
  private void generarEventosRecovery() throws Exception {
    if(entorno.equals("PRO")) {
      log.info("Iniciando tarea programada: BatchEventosRecovery::generarEventosRecovery");
      Date hoy = new Date();
      Date ayer = new Date();
      Calendar cal = Calendar.getInstance();
      hoy.setTime(cal.getTimeInMillis());
      cal.add(Calendar.DATE, -1);
      ayer.setTime(cal.getTimeInMillis());
      SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");
      List<Propuesta> propuestas = propuestaRepository.findAllByIdResolucionPropuestaAndFechaResolucionBetween(1, formatFecha.format(ayer), formatFecha.format(hoy));
      for (Propuesta propuesta : propuestas) {
        for (Contrato contrato : propuesta.getContratos()) {
          if (getRecovery(contrato)) recovery(contrato, format.format(propuesta.getFechaResolucion()), "Propuesta firmada");
        }
      }
      List<SubtipoEvento> subtipoEventosST = subtipoEventoRepository.findAllByCodigo("COM_ST");
      for (SubtipoEvento subtipoEvento : subtipoEventosST) {
        List<Evento> eventos = eventoRepository.findAllBySubtipoEventoIdAndFechaCreacionBetween(subtipoEvento.getId(), formatFecha.format(ayer), formatFecha.format(hoy));
        for (Evento evento : eventos) {
          if (evento.getTitulo() != null && evento.getTitulo().startsWith("Cierre")) {
            Integer idObjeto = evento.getIdNivel();
            ContratoInterviniente contratoInterviniente = contratoIntervinienteRepository.findById(idObjeto).orElse(null);
            Interviniente interviniente = contratoInterviniente.getInterviniente();
            Contrato contrato = contratoInterviniente.getContrato();
            IntervinienteST intervinienteST = intervinienteSTRepository.findByInterviniente(interviniente).orElse(null);
            if (intervinienteST != null) {
              for (DatoDireccionST datosDireccionST : intervinienteST.getDatosDireccionST()) {
                if (datosDireccionST.getPrincipal()) {
                  if (getRecovery(contrato)) recovery(contrato, datosDireccionST.getNombre(), "Contacto/Localizacion");
                }
              }
            }
          }

        }
      }
      propuestas = propuestaRepository.findAllByFechaSancionBetween(formatFecha.format(ayer), formatFecha.format(hoy));
      for (Propuesta propuesta : propuestas) {
        if (propuesta.getSancionPropuesta() != null && (propuesta.getSancionPropuesta().getCodigo().equals("DEN") || propuesta.getSancionPropuesta().getCodigo().equals("AUT"))) {
          for (Contrato contrato : propuesta.getContratos()) {
            if (getRecovery(contrato)) recovery(contrato, propuesta.getSancionPropuesta().getValor(), "Sancion");
          }
        }
      }
      List<Evento> eventos = eventoRepository.findAllByFechaCreacionBetween(formatFecha.format(ayer), formatFecha.format(hoy));
      for (Evento evento : eventos) {
        if (evento.getTitulo() != null && evento.getTitulo().startsWith("Elevación Propuesta")) {
          Integer idObjeto = evento.getIdNivel();
          Propuesta propuesta = propuestaRepository.findById(idObjeto).orElse(null);
          if (propuesta != null) {
            for (Contrato contrato : propuesta.getContratos()) {
              if (getRecovery(contrato)) recovery(contrato, evento.getTitulo(), "Propuesta Elevada");
            }
          }
        }
      }
      File fileCerberus = escribirCerberus();
      File fileCajamar = escribirCajamar();
      JSch jsch = new JSch();
      jsch.setKnownHosts(knownHost);
      Session jschSession = jsch.getSession(user, host, Integer.valueOf(port));
      Properties config = new Properties();
      config.put("StrictHostKeyChecking", "no");
      jschSession.setConfig(config);
      jschSession.setPassword(password);
      jschSession.connect(60000);

      ChannelSftp channelSftp = (ChannelSftp) jschSession.openChannel("sftp");
      channelSftp.connect();
      channelSftp.cd("Archivos/fenixtorcv/");
      channelSftp.put(new FileInputStream(fileCajamar), fileCajamar.getName());
      channelSftp.put(new FileInputStream(fileCerberus), fileCerberus.getName());
      channelSftp.disconnect();
      log.info("Fin tarea programada: BatchEventosRecovery::generarEventosRecovery");
    }else{
      log.info("No ejecutada tarea programada: BatchEventosRecovery::generarEventosRecovery");
    }
  }

  private File escribirCerberus() throws IOException {
    String nombreFichero = pathCsv + "ANOTACIONES_RCV_" + CERBERUS + "_" + formatFichero.format(new Date()) + ".dat";
    FileWriter fichero = new FileWriter(nombreFichero);
    PrintWriter pw = new PrintWriter(fichero);
    for (int i = 0; i < eventoCSV.size(); i++) {
      EventoCsvBean evento = (EventoCsvBean) eventoCSV.get(i);
      if (CERBERUS.equals(evento.getIdFichero()))
        pw.println(evento.getAsu_id() + "Î" + evento.getTitulo_anotacion() + "Î" + evento.getUsu_username() + "Î" + evento.getTar_descripcion());
    }
    pw.close();
    fichero.close();
    return new File(nombreFichero);
  }

  private File escribirCajamar() throws IOException {
    String nombreFichero = pathCsv + "ANOTACIONES_RCV_" + CAJAMAR + "_" + formatFichero.format(new Date()) + ".dat";
    FileWriter fichero = new FileWriter(nombreFichero);
    PrintWriter pw = new PrintWriter(fichero);
    for (int i = 0; i < eventoCSV.size(); i++) {
      EventoCsvBean evento = (EventoCsvBean) eventoCSV.get(i);
      if (CAJAMAR.equals(evento.getIdFichero()))
        pw.println(evento.getAsu_id() + "Î" + evento.getTitulo_anotacion() + "Î" + evento.getUsu_username() + "Î" + evento.getTar_descripcion());
    }
    pw.close();
    fichero.close();
    return new File(nombreFichero);
  }

  private void recovery(Contrato contrato, String descripcion, String titulo) {
    try {
      List<String> letrados = new ArrayList<>();
      List<Asunto> asuntos = servicioRecovery.getAsuntos(contrato);
      for (Asunto asuntoG : asuntos) {
        Asunto asunto = servicioRecovery.getAsunto(contrato, String.valueOf(asuntoG.getId()));
        for (LineaProcesal iter : asunto.getItersProcesales()) {
          if (!iter.getLetrado().equals("") && !letrados.contains(iter.getLetrado())) {
            letrados.add(iter.getLetrado());
            EventoCsvBean eventoCsvBean = new EventoCsvBean();
            eventoCsvBean.setAsu_id(String.valueOf(asunto.getId()));
            eventoCsvBean.setTar_descripcion(descripcion);
            eventoCsvBean.setUsu_username(iter.getLetrado());
            eventoCsvBean.setTitulo_anotacion(titulo);
            eventoCsvBean.setIdFichero(getCartera(contrato));
            eventoCSV.add(eventoCsvBean);
          }
        }
      }
    } catch (Exception ex) {
    }
  }

  private String getCartera(Contrato contrato) {
    if (contrato.getExpediente() != null) {
      if (contrato.getExpediente().getCartera() != null) {
        if (2 == contrato.getExpediente().getCartera().getId() || 54 == contrato.getExpediente().getCartera().getId() || 55 == contrato.getExpediente().getCartera().getId() || 56 == contrato.getExpediente().getCartera().getId() || 53 == contrato.getExpediente().getCartera().getId()) {//Cajamar
          return CAJAMAR;
        } else if (4 == contrato.getExpediente().getCartera().getId() || 5 == contrato.getExpediente().getCartera().getId() || 11 == contrato.getExpediente().getCartera().getId() || 36 == contrato.getExpediente().getCartera().getId()) {//Cerberus
          return CERBERUS;
        }
      }
    }
    return null;
  }

  private boolean getRecovery(Contrato contrato) {
    if (contrato.getExpediente() != null) {
      if (contrato.getExpediente().getCartera() != null) {
        if (contrato.getExpediente().getCartera().getDatosJudicial() != null) {
          return contrato.getExpediente().getCartera().getDatosJudicial();
        }
      }
    }
    return false;
  }
}
