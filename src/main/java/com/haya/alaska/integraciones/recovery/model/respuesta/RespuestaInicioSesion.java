package com.haya.alaska.integraciones.recovery.model.respuesta;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haya.alaska.integraciones.recovery.model.Usuario;
import com.mashape.unirest.http.JsonNode;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j

public class RespuestaInicioSesion extends AbstractRespuestaJson {
  public RespuestaInicioSesion(JsonNode response) {
    super(response);
  }

  @SneakyThrows
  public Usuario getUsuario() {
    log.info("LOGRECOVERY " + this.getDatos().toString());
    return new ObjectMapper().readValue(
      this.getDatos().getJSONObject("usuario").toString(),
      new TypeReference<>() {}
    );
  }

  public String getJwt() {
    return this.getDatos().getString("jwt");
  }

  public String getJSpringSecurityKey() {
    return this.getDatos().getString("jSpringSecurityKey");
  }

  public Date getFecha() {
    return new Date(this.getDatos().getLong("loginDate"));
  }
}
