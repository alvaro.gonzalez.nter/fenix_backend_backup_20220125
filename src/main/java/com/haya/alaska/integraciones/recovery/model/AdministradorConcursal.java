package com.haya.alaska.integraciones.recovery.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class AdministradorConcursal {
  private String nombre;
  private String telefono1;
  private String telefono2;
  private String email;
}
