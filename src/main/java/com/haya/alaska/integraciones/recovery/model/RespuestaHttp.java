package com.haya.alaska.integraciones.recovery.model;

import com.haya.alaska.integraciones.recovery.model.exception.ErrorServidorException;
import lombok.Getter;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class RespuestaHttp {
  @Getter
  private final int code;
  private final HashMap<String, String> headers;
  @Getter
  private final InputStream body;

  public RespuestaHttp(CloseableHttpResponse response) throws ErrorServidorException, IOException {
    this.code = response.getStatusLine().getStatusCode();
    if (this.code >= 400) {
      throw new ErrorServidorException(response);
    }

    this.headers = new HashMap<>();
    for (Header header : response.getAllHeaders()) {
      this.headers.put(header.getName(), header.getValue());
    }

    InputStream content = response.getEntity().getContent();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    content.transferTo(baos);
    this.body = new ByteArrayInputStream(baos.toByteArray());
  }

  public String getHeader(String name) {
    return this.headers.get(name);
  }
}
