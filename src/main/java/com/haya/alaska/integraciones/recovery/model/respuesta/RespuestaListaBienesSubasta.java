package com.haya.alaska.integraciones.recovery.model.respuesta;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haya.alaska.integraciones.recovery.model.BienSubastadoRecovery;
import com.mashape.unirest.http.JsonNode;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j

public class RespuestaListaBienesSubasta extends AbstractRespuestaJsonSuccess {
  public RespuestaListaBienesSubasta(JsonNode response) {
    super(response);
  }

  @SneakyThrows
  public List<BienSubastadoRecovery> getBienes() {
    log.info("LOGRECOVERY " + this.getDatos().toString());
    return new ObjectMapper().readValue(
      this.getDataArray().toString(),
      new TypeReference<>() {
      }
    );
  }
}
