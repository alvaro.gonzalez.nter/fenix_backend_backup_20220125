package com.haya.alaska.integraciones.recovery.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Contrato {
  @JsonProperty(value = "id")
  private String idHaya;
  private String numeroContratoIris;//Preguntar equivalencia en Fenix

  public String getIdHaya(){
    return String.valueOf(this.idHaya);
  }
}
