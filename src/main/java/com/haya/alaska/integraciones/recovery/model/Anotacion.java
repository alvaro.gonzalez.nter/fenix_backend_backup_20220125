package com.haya.alaska.integraciones.recovery.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Anotacion {
  private Long id;
  private Long idTarea;
  private String mensaje;
  private String respuesta;
  private String remitente;
  private Long fecha;
}
