package com.haya.alaska.integraciones.recovery;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.integraciones.recovery.model.Anotacion;
import com.haya.alaska.integraciones.recovery.model.Asunto;
import com.haya.alaska.integraciones.recovery.model.BienSubastadoRecovery;
import com.haya.alaska.integraciones.recovery.model.PeticionHttp;
import com.haya.alaska.integraciones.recovery.model.respuesta.*;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class ServicioRecovery {
  private final ConfigRecovery config;
  private final PeticionHttp peticionHttp;
  private String cartera;
  private String cliente;

  public ServicioRecovery(ConfigRecovery config) {
    this.config = config;
    this.peticionHttp = new PeticionHttp(config);
  }

  private boolean iniciarSesion(Cartera cartera) throws IOException {
    JSONObject json = new JSONObject();
    if (2 == cartera.getId() || 53 == cartera.getId() || 54 == cartera.getId() || 55 == cartera.getId() || 56 == cartera.getId()) {//Cajamar
      json.put("username", this.config.getUsuarioCajamar());
      json.put("password", this.config.getClaveCajamar());
      this.cliente = "CAJAMAR";
    } else if (4 == cartera.getId() || 5 == cartera.getId() || 11 == cartera.getId() || 36 == cartera.getId()) {//Cerberus
      json.put("username", this.config.getUsuarioCerberus());
      json.put("password", this.config.getClaveCerberus());
      this.cliente = "CERBERUS";
    } else {
      return false;
    }
    new RespuestaInicioSesion(
      this.peticionHttp.post("/login", json)
    );
    return true;
  }

  public List<Asunto> getAsuntos(Contrato contrato) throws IOException {
    if (this.requerirInicioSesion(contrato.getExpediente().getCartera()) == false) return null;
    String idCarga = contrato.getIdCarga();
    if (this.cliente.equals("CAJAMAR")) {
      idCarga = contrato.getIdCarga().replaceFirst("^0+", "");
    } else {
      if (idCarga.length() >= 10) {
        idCarga = contrato.getIdCarga().split("05202")[1];
      }
      idCarga = String.format("%010d", Integer.valueOf(idCarga));
    }
    JSONObject json = createJson(
      createJsonHeader(), createJsonData(idCarga));
    return new RespuestaListaAsuntos(
      this.peticionHttp.post("/rs/asunto/getInfoAsuntosByContrato", json)
    ).getAsuntos();//subfase
  }

  public Asunto getAsunto(Contrato contrato, String id) throws IOException {
    if (this.requerirInicioSesion(contrato.getExpediente().getCartera()) == false) return null;
    JSONObject json = createJson(null, createJsonData(id));
    return new RespuestaAsunto(
      this.peticionHttp.post("/rs/asunto/getAsuntoByAsuId", json)
    ).getAsunto();//subFase
  }

  //Se ha cambiado a List<Asunto> ya que es lo que devuelve Recovery.
  public List<Asunto> getSubastas(Contrato contrato, String idAsunto) throws IOException {
    if (this.requerirInicioSesion(contrato.getExpediente().getCartera()) == false) return null;
    JSONObject json = createJson(
      createJsonHeader(), createJsonData(idAsunto)
    );
    return new RespuestaListaSubastas(
      this.peticionHttp.post("/rs/subasta/getSubastasDeItersByIdAsunto", json)
    ).getSubastas();//Se debe cambiar el método getSubastas una vez que se modifiquen los datos por Recovery
  }

  public List<BienSubastadoRecovery> getBienesSubasta(Contrato contrato, String idSubasta) throws IOException {
    if (this.requerirInicioSesion(contrato.getExpediente().getCartera()) == false) return null;
    JSONObject json = createJson(
      createJsonHeader(), createJsonData(idSubasta)
    );
    return new RespuestaListaBienesSubasta(
      this.peticionHttp.post("/rs/bien/getBienesBySubasta", json)
    ).getBienes();
  }

  public List<Anotacion> getAnotaciones(Contrato contrato, String idAsunto) throws IOException {
    if (this.requerirInicioSesion(contrato.getExpediente().getCartera()) == false) return null;
    JSONObject json = createJson(
      createJsonHeader(), createJsonData(idAsunto)
    );
    return new RespuestaListaAnotaciones(
      this.peticionHttp.post("/rs/anotacion/getAnotacionesByAsunto", json)
    ).getAnotaciones();
  }

  private boolean requerirInicioSesion(Cartera cartera) throws IOException {
    if (!this.peticionHttp.sesionIniciada() || !cartera.getId().equals(this.cartera)) {
      if (this.iniciarSesion(cartera) == false) return false;
      this.cartera = String.valueOf(cartera.getId());
    }
    return true;
  }

  private static JSONObject createJsonHeader() {
    JSONObject json = new JSONObject();
    json.put("page", 1);
    json.put("start", 0);
    json.put("limit", 99999);
    return json;
  }

  private static JSONObject createJsonData(String id) {
    JSONObject json = new JSONObject();
    json.put("id", id);
    return json;
  }

  private static JSONObject createJson(JSONObject header, JSONObject data) {
    JSONObject json = new JSONObject();
    if (header != null) {
      json.put("header", header);
    }
    if (data != null) {
      json.put("data", data);
    }
    return json;
  }
}
