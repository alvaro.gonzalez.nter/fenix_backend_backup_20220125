package com.haya.alaska.integraciones.recovery.model;

import com.haya.alaska.carga_control_transformacion.domain.CsvBean;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EventoCsvBean extends CsvBean {

  private String asu_id;
  private String usu_username;
  private String tar_descripcion;
  private String titulo_anotacion;
  private String idFichero;


}
