package com.haya.alaska.integraciones.recovery.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Demandado {
  @JsonProperty(value = "docId")
  private String numeroDocumento;
  private String nombre;
}
