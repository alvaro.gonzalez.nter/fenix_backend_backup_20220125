package com.haya.alaska.integraciones.recovery.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haya.alaska.estado_procedimiento.domain.EstadoProcedimiento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class Asunto {
  private Long id;
  @JsonProperty(value = "estadoAsunto")
  private String estado;//No viene informado en el getAsuntosByContrato
  @JsonProperty(value = "motivoParalizacion")
  private String motivo;
  private String despacho;
  private List<LineaProcesal> itersProcesales;
  private String procedimientos;
  private String letradoContrario;
  private String procuradorContrario;

  public EstadoProcedimiento getEstado(){
    EstadoProcedimiento estadoProcedimiento = new EstadoProcedimiento();
    estadoProcedimiento.setValor(this.estado);
    return estadoProcedimiento;
  }

}
