package com.haya.alaska.integraciones.recovery.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haya.alaska.resultado_subasta.domain.ResultadoSubasta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class BienSubastadoRecovery {
  private String id;
  @JsonProperty(value = "numFinca")
  private String numeroFinca;
  @JsonProperty(value = "conPostores")
  private Boolean postores;
  private Double importeAdjudicacion;
  private Long fechaDecretoAdjudicacion;
  private Long fechaTestimonio;
  @JsonProperty(value = "fechaSenyalamientoPosesion")
  private Long fechaSenyalamiento;
  private Long fechaPosesion;//Preguntar equivalencia Fenix
  @JsonProperty(value = "resultadoPosesion")
  private String resultadoPosesion;
  private String detalle;
  private String tipoSubasta;


  public String getId() {
    return id;
  }

  public Date getFechaDecretoAdjudicacion() {
    if (fechaDecretoAdjudicacion == null) return null;
    return getDate(fechaDecretoAdjudicacion);
  }

  public Date getFechaTestimonio() {
    if (fechaTestimonio == null) return null;
    return getDate(fechaTestimonio);
  }

  public Date getFechaSenyalamiento() {
    if (fechaSenyalamiento == null) return null;
    return getDate(fechaSenyalamiento);
  }

  public Date getFechaPosesion() {
    if (fechaPosesion == null) return null;
    return getDate(fechaPosesion);
  }

  public ResultadoSubasta getResultadoSubasta() {
    ResultadoSubasta resultadoSubasta = new ResultadoSubasta();
    resultadoSubasta.setValor(this.resultadoPosesion);
    return resultadoSubasta;
  }

  private Date getDate(long fechaLong) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(fechaLong);
    calendar.add(Calendar.HOUR_OF_DAY, 2);
    return calendar.getTime();
  }
}
