package com.haya.alaska.integraciones.recovery.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
public class LineaProcesal {
  private Long id;
  private String procedimientoPrincipal;
  @JsonProperty(value = "tipoProcedimiento")
  private String tipo;
  private String fase;//Preguntar equivalencia en Fenix
  private String subfase;//Preguntar equivalencia en Fenix
  @JsonProperty(value = "hito")
  private String hitoProcedimiento;
  @JsonProperty(value = "numAuto")
  private String numeroAutos;
  private String plaza;
  private String juzgado;
  private Double importeDemanda;
  private List<Contrato> contratos;
  private List<Demandado> demandados;
  @JsonProperty(value = "provinciaJuzgado")
  private String provincia;
  private Letrado letrado;
  private Procurador procurador;
  private AdministradorConcursal administradorConcursal;
  private GestorLitigio gestorLitigio;//Preguntar equivalencia en Fenix
  private GestorConcursos gestorConcursos;
  private Integer orden;
  private List<InfoAdicional> infoAdicional;//Preguntar equivalencia en Fenix
  private List<Subasta> subastas;

  public TipoProcedimiento getTipo() {
    TipoProcedimiento tipoProcedimiento = new TipoProcedimiento();
    tipoProcedimiento.setValor(this.tipo);
    return tipoProcedimiento;
  }

  public HitoProcedimiento getHitoRecovery() {
    HitoProcedimiento hitoProcedimiento = new HitoProcedimiento();
    hitoProcedimiento.setValor(this.getHitoProcedimiento());
    return hitoProcedimiento;
  }

  public Set<com.haya.alaska.contrato.domain.Contrato> getContratos() {
    Set<com.haya.alaska.contrato.domain.Contrato> contratosFenix = new HashSet<com.haya.alaska.contrato.domain.Contrato>();
    if (this.contratos == null) return contratosFenix;
    for (Contrato contrato : this.contratos) {
      com.haya.alaska.contrato.domain.Contrato c = new com.haya.alaska.contrato.domain.Contrato();
      BeanUtils.copyProperties(contrato, c);
      contratosFenix.add(c);
    }
    return contratosFenix;
  }

  public Set<Interviniente> getDemandados() {
    Set<Interviniente> intervinientesFenix = new HashSet<>();
    if (this.demandados == null) return intervinientesFenix;
    for (Demandado demandado : this.demandados) {
      Interviniente i = new Interviniente();
      BeanUtils.copyProperties(demandado, i);
      intervinientesFenix.add(i);
    }
    return intervinientesFenix;
  }

  public List<Demandado> getDemandadosRecovery() {
    return this.demandados;
  }

  public List<Contrato> getContratoRecovery() {
    return this.contratos;
  }

  public Provincia getProvincia() {
    Provincia provinciaFenix = new Provincia();
    provinciaFenix.setValor(this.provincia);
    return provinciaFenix;
  }

  public String getLetrado() {
    if (letrado != null)
      return this.letrado.getNombre();
    return "";
  }

  public String getTelefonoLetrado() {
    if (letrado != null)
      return this.letrado.getTelefono1();
    return "";
  }

  public String getTelefono2Letrado() {
    if (letrado != null)
      return this.letrado.getTelefono2();
    return "";
  }

  public String getEmailLetrado() {
    if (letrado != null)
      return this.letrado.getEmail();
    return "";
  }

  public String getProcurador() {
    if (procurador != null)
      return this.procurador.getNombre();
    return "";
  }

  public String getTelefonoProcurador() {
    if (procurador != null)
      return this.procurador.getTelefono1();
    return "";
  }

  public String getEmailProcurador() {
    if (procurador != null)
      return this.procurador.getEmail();
    return "";
  }

  public String getNombreAdminConcursal() {
    if (administradorConcursal != null)
      return this.administradorConcursal.getNombre();
    return "";
  }

  public String getDireccionAdminConcursal() {
    return "";
  }

  public String getTelefonoAdminConcursal() {
    if (administradorConcursal != null)
      return this.administradorConcursal.getTelefono1();
    return "";
  }

  public String getEmailAdminConcursal() {
    if (administradorConcursal != null)
      return this.administradorConcursal.getEmail();
    return "";
  }

  public String getGestorConcursosHaya() {
    if (gestorConcursos != null)
      return this.gestorConcursos.getNombre();
    return "";
  }

  public String getMailGestorConcursosHaya() {
    if (gestorConcursos != null)
      return this.gestorConcursos.getEmail();
    return "";
  }

  public Set<com.haya.alaska.subasta.domain.Subasta> getSubastas() {
    Set<com.haya.alaska.subasta.domain.Subasta> subastasFenix = new HashSet<com.haya.alaska.subasta.domain.Subasta>();
    if (this.subastas == null) return subastasFenix;
    for (Subasta subasta : this.subastas) {
      com.haya.alaska.subasta.domain.Subasta s = new com.haya.alaska.subasta.domain.Subasta();
      BeanUtils.copyProperties(subasta, s, "id");
      s.setIdRecovery(subasta.getId());
      s.setFechaInicioPujas(subasta.getFechaInicioPujas());
      s.setFechaFinPujas(subasta.getFechaFinPujas());
      s.setEstado(subasta.getEstado());
      s.setFechaNotificacionDecreto(subasta.getFechaNotificacionDecreto());
      s.setFechaPagoTasa(subasta.getFechaPagoTasa());
      subastasFenix.add(s);
    }
    return subastasFenix;


  }
}
