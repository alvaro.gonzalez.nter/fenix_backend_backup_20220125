package com.haya.alaska.integraciones.recovery.model;

import com.haya.alaska.carga_control_transformacion.domain.CsvBean;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProcedimientoCsvBean extends CsvBean {

  private String tipoAsunto;
  private String codRecovery;
  private String codContrato;
  private String tipoProcedimiento;
  private String nifPrincipal;
  private String codigoWF;
  private String letrado;
  private String fechaPeticion;
  private String nombreExtra;
  private String idFichero;

}
