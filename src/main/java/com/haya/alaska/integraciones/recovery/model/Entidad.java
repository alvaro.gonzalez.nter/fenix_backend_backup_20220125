package com.haya.alaska.integraciones.recovery.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class Entidad {
  private long id;
  private String descripcion;
  private String codigo;
  private String descripcionLarga;
}
