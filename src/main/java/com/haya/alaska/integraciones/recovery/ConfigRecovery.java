package com.haya.alaska.integraciones.recovery;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ConfigRecovery {
  @Value("${integraciones.recovery.url}")
  private String url;

  @Value("${integraciones.recovery.usuario.cajamar}")
  private String usuarioCajamar;

  @Value("${integraciones.recovery.clave.cajamar}")
  private String claveCajamar;

  @Value("${integraciones.recovery.usuario.cerberus}")
  private String usuarioCerberus;

  @Value("${integraciones.recovery.clave.cerberus}")
  private String claveCerberus;
}
