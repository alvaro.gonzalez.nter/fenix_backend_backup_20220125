package com.haya.alaska.integraciones.recovery;

import com.haya.alaska.carga_control_transformacion.domain.CsvBean;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.integraciones.recovery.model.Asunto;
import com.haya.alaska.integraciones.recovery.model.LineaProcesal;
import com.haya.alaska.integraciones.recovery.model.ProcedimientoCsvBean;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class BatchJudicializacionRecovery {

  @Autowired
  private EventoRepository eventoRepository;
  @Autowired
  private TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired
  private IntervinienteRepository intervinienteRepository;
  @Autowired
  private ContratoRepository contratoRepository;
  @Autowired
  private UsuarioRepository usuarioRepository;
  @Autowired
  private ServicioRecovery servicioRecovery;

  @Value("${spring.profiles.active}")
  String entorno;

  @Value("${integraciones.recovery.judicializacion.csv}")
  String pathCsv;

  @Value("${integraciones.recovery.ftp.user}")
  String user;

  @Value("${integraciones.recovery.ftp.password}")
  String password;

  @Value("${integraciones.recovery.ftp.port}")
  String port;

  @Value("${integraciones.recovery.ftp.host}")
  String host;

  @Value("${integraciones.recovery.knownhosts}")
  String knownHost;

  private List<CsvBean> procedimientoCSV = new ArrayList<>();

  final String CAJAMAR = "0240";

  final String CERBERUS = "0182";

  SimpleDateFormat formatFichero = new SimpleDateFormat("yyyyMMdd");

  @Scheduled(cron = "${integraciones.recovery.judicializacion.cron}")
  private void generarAltaJudicialRecovery() throws Exception {
    if(entorno.equals("PRO")) {
      log.info("Iniciando tarea programada: BatchJudicializacionRecovery::generarAltaJudicialRecovery");
      Date hoy = new Date();
      Date ayer = new Date();
      Calendar cal = Calendar.getInstance();
      hoy.setTime(cal.getTimeInMillis());
      cal.add(Calendar.DATE, -1);
      ayer.setTime(cal.getTimeInMillis());
      SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");
      List<Evento> eventos = eventoRepository.findAllByFechaCreacionBetween(formatFecha.format(ayer), formatFecha.format(hoy));
      Usuario destinatario = usuarioRepository.findByEmail("recovery@haya.es").orElse(null);

      List<Evento> eventosFiltrados = eventos.stream().filter(e -> {
        if (!e.getTipoEvento().getCodigo().equals("ACC_05")) {
          return false;
        }
        if (!e.getResultado().getCodigo().equals("AUTO")) {
          return false;
        }
        if (!e.getNivel().getCodigo().equals("JUD")) {
          return false;
        }
        if (!e.getDestinatario().getId().equals(destinatario.getId())) {
          return false;
        }
        return true;
      }).collect(Collectors.toList());


      for (Evento evento : eventosFiltrados) {
        boolean recovery = evento.getCartera().getDatosJudicial();
        if (recovery) {
          Contrato contrato = contratoRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("Contrato", evento.getIdNivel()));
          if (getRecovery(contrato)) recovery(contrato, evento);
        }
      }

      File cerberus = escribirCajamar();
      File cajamar = escribirCerberus();

      JSch jsch = new JSch();
      jsch.setKnownHosts(knownHost);
      Session jschSession = jsch.getSession(user, host, Integer.valueOf(port));
      Properties config = new Properties();
      config.put("StrictHostKeyChecking", "no");
      jschSession.setConfig(config);
      jschSession.setPassword(password);
      jschSession.connect(120000);

      ChannelSftp channelSftp = (ChannelSftp) jschSession.openChannel("sftp");
      channelSftp.connect();
      channelSftp.cd("Archivos/fenixtorcv/");
      channelSftp.put(new FileInputStream(cajamar), cajamar.getName());
      channelSftp.put(new FileInputStream(cerberus), cerberus.getName());
      channelSftp.disconnect();
      log.info("Fin tarea programada: BatchJudicializacionRecovery::generarAltaJudicialRecovery");
    }else{
      log.info("No ejecutada tarea programada: BatchJudicializacionRecovery::generarAltaJudicialRecovery");
    }
  }

  private void recovery(Contrato contrato, Evento evento) {
    String tipoAsunto = "01";//Valor por defecto en Fenix
    try {
      List<Asunto> asuntos = servicioRecovery.getAsuntos(contrato);
      for (Asunto asuntoG : asuntos) {
        Asunto asunto = servicioRecovery.getAsunto(contrato, String.valueOf(asuntoG.getId()));
        for (LineaProcesal iter : asunto.getItersProcesales()) {
          if (!iter.getLetrado().equals("")) {
            String[] ids = separarComentario(evento);
            Interviniente interviniente = intervinienteRepository.findByNumeroDocumento(ids[1]).orElse(null);
            String idCarga = contrato.getIdCarga();
            if (getCartera(contrato).equals("CAJAMAR")) {
              idCarga = contrato.getIdCarga().replaceFirst("^0+", "");
            } else {
              if (idCarga.length() >= 10) {
                idCarga = contrato.getIdCarga().split("05202")[1];
              }
              idCarga = String.format("%010d", Integer.valueOf(idCarga));
            }
            ProcedimientoCsvBean procedimientoCsvBean = new ProcedimientoCsvBean(tipoAsunto, "", idCarga, "PCO", interviniente.getNumeroDocumento() != null ? interviniente.getNumeroDocumento() : "", "", iter.getLetrado(), ids[2] != null ? ids[2] : "", "", getCartera(contrato));
            procedimientoCSV.add(procedimientoCsvBean);
          }
        }
      }
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    }
  }

  private String[] separarComentario(Evento evento) {
    String[] comentario = evento.getComentariosDestinatario().split("\n");
    String[] lineas = new String[3];
    lineas[0] = comentario[0].split(":")[1].trim();
    lineas[1] = comentario[1].split(":")[1].trim().split(" ")[0];
    lineas[2] = comentario[2].split(":")[1].trim();
    return lineas;
  }

  private String getCartera(Contrato contrato) {
    if (contrato.getExpediente() != null) {
      if (contrato.getExpediente().getCartera() != null) {
        if (2 == contrato.getExpediente().getCartera().getId() || 53 == contrato.getExpediente().getCartera().getId() || 54 == contrato.getExpediente().getCartera().getId() || 55 == contrato.getExpediente().getCartera().getId() || 56 == contrato.getExpediente().getCartera().getId()) {//Cajam<<<ar
          return CAJAMAR;
        } else if (4 == contrato.getExpediente().getCartera().getId() || 5 == contrato.getExpediente().getCartera().getId() || 11 == contrato.getExpediente().getCartera().getId() || 36 == contrato.getExpediente().getCartera().getId()) {//Cerberus
          return CERBERUS;
        }
      }
    }
    return null;
  }

  private boolean getRecovery(Contrato contrato) {
    if (contrato.getExpediente() != null) {
      if (contrato.getExpediente().getCartera() != null) {
        if (contrato.getExpediente().getCartera().getDatosJudicial() != null) {
          return contrato.getExpediente().getCartera().getDatosJudicial();
        }
      }
    }
    return false;
  }

  private File escribirCerberus() throws IOException {
    String nombreFichero = pathCsv +  "ASUNTOS_" + CERBERUS + "_" + formatFichero.format(new Date()) + ".dat";
    FileWriter fichero = new FileWriter(nombreFichero);
    PrintWriter pw = new PrintWriter(fichero);
    for (int i = 0; i < procedimientoCSV.size(); i++) {
      ProcedimientoCsvBean proceso = (ProcedimientoCsvBean) procedimientoCSV.get(i);
      if (CERBERUS.equals(proceso.getIdFichero()))
        pw.println(proceso.getTipoAsunto() + "Î" + proceso.getCodRecovery() + "Î" + proceso.getCodContrato() + "Î" + proceso.getTipoProcedimiento() + "Î" + proceso.getNifPrincipal() + "Î" + proceso.getCodigoWF() + "Î" + proceso.getLetrado() + "Î" + proceso.getFechaPeticion() + "Î" + proceso.getNombreExtra());
    }
    pw.close();
    fichero.close();
    return new File(nombreFichero);
  }

  private File escribirCajamar() throws IOException {
    String nombreFichero = pathCsv + "ASUNTOS_" + CAJAMAR + "_" + formatFichero.format(new Date()) + ".dat";
    FileWriter fichero = new FileWriter(nombreFichero);
    PrintWriter pw = new PrintWriter(fichero);
    for (int i = 0; i < procedimientoCSV.size(); i++) {
      ProcedimientoCsvBean proceso = (ProcedimientoCsvBean) procedimientoCSV.get(i);
      if (CAJAMAR.equals(proceso.getIdFichero()))
        pw.println(proceso.getTipoAsunto() + "Î" + proceso.getCodRecovery() + "Î" + proceso.getCodContrato() + "Î" + proceso.getTipoProcedimiento() + "Î" + proceso.getNifPrincipal() + "Î" + proceso.getCodigoWF() + "Î" + proceso.getLetrado() + "Î" + proceso.getFechaPeticion() + "Î" + proceso.getNombreExtra());
    }
    pw.close();
    fichero.close();
    return new File(nombreFichero);
  }
}


