package com.haya.alaska.integraciones.recovery.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haya.alaska.estado_subasta.domain.EstadoSubasta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class Subasta {
  private String id;
  @JsonProperty(value = "fechaDecretoConvocatoria")
  private Long fechaNotificacionDecreto;
  private Long fechaPagoTasa;//Preguntar equivalencia en Fenix
  @JsonProperty(value = "fechaInicioPeriodoPujas")
  private Long fechaInicioPujas;
  @JsonProperty(value = "fechaFinPeriodoPujas")
  private Long fechaFinPujas;
  private String decisionSuspension;
  @JsonProperty(value = "estadoSubasta")
  private String estado;
  @JsonProperty(value = "motivoSuspension")
  private String motivoSuspensionCancelacion;


  public String getId() {
    return this.id;
  }

  public Date getFechaNotificacionDecreto() {
    if (this.fechaNotificacionDecreto == null) return null;
    return getDate(this.fechaNotificacionDecreto);
  }

  public Date getFechaPagoTasa() {
    if (this.fechaPagoTasa == null) return null;
    return getDate(this.fechaPagoTasa);
  }

  public Date getFechaInicioPujas() {
    if (this.fechaInicioPujas == null) return null;
    return getDate(this.fechaInicioPujas);
  }

  public Date getFechaFinPujas() {
    if (this.fechaFinPujas == null) return null;
    return getDate(this.fechaFinPujas);
  }

  public EstadoSubasta getEstado() {
    EstadoSubasta estadoSubasta = new EstadoSubasta();
    estadoSubasta.setValor(this.estado);
    return estadoSubasta;
  }

  private Date getDate(long fechaLong) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(fechaLong);
    calendar.add(Calendar.HOUR_OF_DAY, 2);
    return calendar.getTime();
  }
}
