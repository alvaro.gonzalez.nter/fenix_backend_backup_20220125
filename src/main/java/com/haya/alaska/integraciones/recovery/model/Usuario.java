package com.haya.alaska.integraciones.recovery.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class Usuario {
  private long id;
  private String username;
  private String nombre;
  private String apellido1;
  private String apellido2;
  private String email;
  private String telefono;
  private boolean usuarioExterno;
  private Entidad entidad;
  private boolean usuarioGrupo;
  private boolean enabled;
  private String jsessionid;
  private String nombreApellidos;
  private String nombreApellido;
  private String apellidos;
  private String apellidoNombre;
}
