package com.haya.alaska.integraciones.recovery.model.exception;

import lombok.Getter;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Getter
public class ErrorServidorException extends Exception {
  private final int statusCode;
  private final JSONObject datos;

  public ErrorServidorException(CloseableHttpResponse response) {
    JSONObject datos = null;
    try {
      datos = new JSONObject(
        IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8));
    } catch (IOException e) {
      e.printStackTrace();
    }

    this.statusCode = response.getStatusLine().getStatusCode();
    this.datos = datos;
  }

  public String getMessage() {
    return this.datos.getString("message");
  }
}
