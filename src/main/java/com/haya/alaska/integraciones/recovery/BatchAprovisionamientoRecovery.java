package com.haya.alaska.integraciones.recovery;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

@Slf4j
@Getter
@Setter
@Component
public class BatchAprovisionamientoRecovery {

  String[] archivos = {"Verbal", "Subastas", "Procedimientos_Demandados", "Procedimientos", "Ordinario", "Monitorio", "Hipotecario", "ETNJ",  "ETJ", "Ejecucion_Notarial", "Concurso", "Bienes"};
  @Value("${integraciones.recovery.ftp.user}")
  private String user;
  @Value("${integraciones.recovery.ftp.password}")
  private String password;
  @Value("${integraciones.recovery.ftp.port}")
  private String port;
  @Value("${integraciones.recovery.ftp.host}")
  private String host;
  @Value("${integraciones.recovery.knownhosts}")
  private String knownHost;

  @Scheduled(cron = "0 0 12 * * 2-6") //A las 12 de martes a sabado
  public void cargaJudicial() throws Exception {

    log.info("Iniciando tarea programada: BatchAprovisionamientoRecovery::generarFicherosJudicial");

    DateFormat formatoFecha = new SimpleDateFormat("yyyyMMdd");
    Date ayer = new Date();
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -1);
    ayer.setTime(cal.getTimeInMillis());
    String fecha = formatoFecha.format(ayer);

    JSch jsch = new JSch();
    jsch.setKnownHosts(knownHost);
    Session jschSession = jsch.getSession(user, host, Integer.parseInt(port));
    Properties config = new Properties();
    config.put("StrictHostKeyChecking", "no");
    jschSession.setConfig(config);
    jschSession.setPassword(password);
    jschSession.connect();

    ChannelSftp channelSftp = (ChannelSftp) jschSession.openChannel("sftp");
    channelSftp.connect();

    for (String fichero : archivos) {
      System.out.println("./src/main/resources/csvs/5209/" + fichero.toUpperCase() + ".csv");
      File file = new File("./src/main/resources/csvs/5209/" + fichero.toUpperCase() + ".csv");
      file.delete();
      FileOutputStream stream = null;
      file.createNewFile();
      stream = new FileOutputStream(file);
      String ficheroFecha = fichero + "_" + fecha + ".CSV";
      channelSftp.get("Archivos/aprovisionamiento/sareb/" + ficheroFecha, stream);
      stream.close();
    }
    channelSftp.disconnect();
    File file = new File("./src/main/resources/csvs/5209/INICIO_JUDICIAL_5209.txt");
    file.createNewFile();
    log.info("Fin tarea programada: BatchAprovisionamientoRecovery::generarFicherosJudicial");
  }
}
