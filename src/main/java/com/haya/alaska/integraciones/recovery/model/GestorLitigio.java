package com.haya.alaska.integraciones.recovery.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class GestorLitigio {
  private String nombre;
  private String email;
}
