package com.haya.alaska.integraciones.recovery.model.respuesta;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haya.alaska.integraciones.recovery.model.Asunto;
import com.mashape.unirest.http.JsonNode;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j

public class RespuestaAsunto extends AbstractRespuestaJsonSuccess {
  public RespuestaAsunto(JsonNode response) {
    super(response);
  }

  @SneakyThrows
  public Asunto getAsunto() {
    log.info("LOGRECOVERY " + this.getDatos().toString());
    return new ObjectMapper().configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true).readValue(
      this.getData().toString(),
      new TypeReference<>() {
      }
    );
  }
}
