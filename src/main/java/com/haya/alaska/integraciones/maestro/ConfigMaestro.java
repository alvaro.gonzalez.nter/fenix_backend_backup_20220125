package com.haya.alaska.integraciones.maestro;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.transport.http.HttpUrlConnectionMessageSender;

import java.time.Duration;

@Configuration
public class ConfigMaestro {
  public static final String STRING_FORMAT = "STRING";
  public static final String DATE_FORMAT = "dd-MM-yyyy HH:MM:ss";
  public static final String RESPONSE_OK_ACTIVOS = "1000";
  public static final String RESPONSE_OK_PERSONAS = "2000";
  public static final String OPERACION_NUEVO = "14";

  @Value("${integraciones.maestro.url.ws}")
  private String urlMasterService;

  @Value("${integraciones.maestro.wsdl}")
  private String wsdlPackage;

  @Value("${integraciones.maestro.timeout}")
  private int timeout;


  /** Este metodo se ha cambiado ya que el context path con la integracion con PBC se jodio y mezclaba maestro y PBC.
   Se ha hecho que se llame al marshaller al inicialziar tanto PBC como Maestro
   */

  private Jaxb2Marshaller marshaller() {
    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
    marshaller.setContextPath(wsdlPackage);
    return marshaller;
  }

  @Bean
  public ServicioMaestro servicioMaestro() {
    Jaxb2Marshaller marshaller = marshaller();

    ServicioMaestro client = new ServicioMaestro();
    client.setDefaultUri(urlMasterService);
    client.setMarshaller(marshaller);
    client.setUnmarshaller(marshaller);
    client.setMessageSender(httpUrlConnectionMessageSender());
    return client;
  }

  @Bean
  public HttpUrlConnectionMessageSender httpUrlConnectionMessageSender() {
    Duration limit = Duration.ofSeconds(timeout);
    HttpUrlConnectionMessageSender httpUrlConnectionMessageSender = new HttpUrlConnectionMessageSender();
    httpUrlConnectionMessageSender.setReadTimeout(limit);
    httpUrlConnectionMessageSender.setConnectionTimeout(limit);
    return httpUrlConnectionMessageSender;
  }
}
