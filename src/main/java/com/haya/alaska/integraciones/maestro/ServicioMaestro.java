package com.haya.alaska.integraciones.maestro;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.integraciones.maestro.wsdl.KeyValuePair;
import com.haya.alaska.integraciones.maestro.wsdl.ProcessEventRequestType;
import com.haya.alaska.integraciones.maestro.wsdl.ProcessEventResponseType;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import javax.xml.bind.JAXBElement;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.haya.alaska.integraciones.maestro.ConfigMaestro.*;

@Component
@Slf4j
public class ServicioMaestro extends WebServiceGatewaySupport {
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
  private static final HashMap<String, String> LISTA_ROLES = new HashMap<>();

  //relación de códigos de tipo de intervención del excel de carga con los valores registrados en maestro
  static {
    LISTA_ROLES.put("1", "01");
    LISTA_ROLES.put("3", "02");
    LISTA_ROLES.put("18", "04");
    LISTA_ROLES.put("16", "19");
    LISTA_ROLES.put("15", "19");
    LISTA_ROLES.put("8", "36");
    LISTA_ROLES.put("10", "60");
    LISTA_ROLES.put("7", "18");
    LISTA_ROLES.put("5", "57");
    LISTA_ROLES.put("4", "02");
    LISTA_ROLES.put("24", "23");
    LISTA_ROLES.put("28", "57");
    LISTA_ROLES.put("29", "57");
    LISTA_ROLES.put("31", "57");
    LISTA_ROLES.put("34", "57");
    LISTA_ROLES.put("35", "57");
    LISTA_ROLES.put("37", "26");
    LISTA_ROLES.put("65", "02");
    LISTA_ROLES.put("66", "57");
    LISTA_ROLES.put("69", "17");
    LISTA_ROLES.put("70", "07");
    LISTA_ROLES.put("71", "28");
    LISTA_ROLES.put("72", "36");
  }

  public void registrarContrato(Contrato contrato, String cliente)
          throws NotFoundException, LockedChangeException {
    String idCliente = null;
    if (contrato.getIdOrigen() != null && contrato.getIdOrigen().length() != 0) {
      idCliente = contrato.getIdOrigen();
    } else if (contrato.getIdOrigen2() != null && contrato.getIdOrigen2().length() != 0) {
      idCliente = contrato.getIdOrigen2();
    } else if (contrato.getIdCarga() != null && contrato.getIdCarga().length() != 0) {
      idCliente = contrato.getIdCarga();
    }

    ProcessEventRequestType request = new ProcessEventRequestType();
    request.setEventName("ALTA_ACTIVO");
    addParameter(request, "id_activo_origen_nuevo", contrato.getId().toString());
    addParameter(request, "id_activo_origen_padre", null);
    addParameter(request, "id_cliente", cliente);
    addParameter(request, "id_tipo_activo", "RED");
    addParameter(request, "id_origen", "FENIX");
    addParameter(request, "flagMultiplicidad", "1");
    addParameter(request, "id_motivo_operacion", OPERACION_NUEVO);
    addParameter(request, "fecha_operacion", dateFormat.format(new Date()));
    addParameter(request, "id_entidad_cedente", null);
    addParameter(request, "id_activo_cliente", idCliente);

    log.info("Llamada al servicio de maestros -> Contrato: " + contrato.getIdCarga());

    ProcessEventResponseType response = getResponse(request);
    if (response.getResultCode() == null || !response.getResultCode().equals(RESPONSE_OK_ACTIVOS)) {
      throw new LockedChangeException(false);
    }

    List<KeyValuePair> keyValuePairs = response.getParameters().getParameter();
    KeyValuePair kvParameterIdHaya =
      keyValuePairs.stream()
        .filter(keyValuePair -> keyValuePair.getCode().equals("ID_ACTIVO_HAYA"))
        .findFirst()
        .orElseThrow(() -> new NotFoundException("ID", false));
    contrato.setIdHaya(kvParameterIdHaya.getValue());
    log.info("Respuesta del servicio de maestros -> Contrato: " + contrato.getIdHaya());
  }

  public void registrarBien(Bien bien, String cliente) throws NotFoundException, LockedChangeException {
    String idCliente = null;
    if (bien.getIdOrigen() != null && bien.getIdOrigen().length() != 0) {
      idCliente = bien.getIdOrigen();
    } else if (bien.getIdOrigen2() != null && bien.getIdOrigen2().length() != 0) {
      idCliente = bien.getIdOrigen2();
    } else if (bien.getIdCarga() != null && bien.getIdCarga().length() != 0) {
      idCliente = bien.getIdCarga();
    }

    ProcessEventRequestType request = new ProcessEventRequestType();
    request.setEventName("ALTA_ACTIVO");
    addParameter(request, "id_activo_origen_nuevo", bien.getId().toString());
    addParameter(request, "id_activo_origen_padre", null);
    addParameter(request, "id_cliente", cliente);
    addParameter(request, "id_tipo_activo", "COLATERAL");
    addParameter(request, "id_origen", "FENIX");
    addParameter(request, "flagMultiplicidad", "1");
    addParameter(request, "id_motivo_operacion", OPERACION_NUEVO);
    addParameter(request, "fecha_operacion", dateFormat.format(new Date()));
    addParameter(request, "id_entidad_cedente", null);
    addParameter(request, "id_activo_cliente", idCliente);

    log.info("Llamada al servicio de maestros -> Bien: " + idCliente);

    ProcessEventResponseType response = getResponse(request);
    if (response.getResultCode() == null || !response.getResultCode().equals(RESPONSE_OK_ACTIVOS)) {
      throw new LockedChangeException(false);
    }

    List<KeyValuePair> keyValuePairs = response.getParameters().getParameter();
    KeyValuePair kvParameterIdHaya =
      keyValuePairs.stream()
        .filter(keyValuePair -> keyValuePair.getCode().equals("ID_ACTIVO_HAYA"))
        .findFirst()
        .orElseThrow(() -> new NotFoundException("ID", false));
    bien.setIdHaya(kvParameterIdHaya.getValue());
    log.info("Respuesta del servicio de maestros -> Bien: " + bien.getIdHaya());
  }

  public void registrarInterviniente(Interviniente interviniente, String cliente, TipoIntervencion tipoIntervencion)
          throws NotFoundException, LockedChangeException {

    ProcessEventRequestType request = new ProcessEventRequestType();
    request.setEventName("ALTA_PERSONA_PBC");
    addParameter(request, "ID_CLIENTE", cliente);
    addParameter(request, "ID_ENTIDAD_CEDENTE", null);
    addParameter(request, "ID_PERSONA_CLIENTE", null);
    addParameter(request, "ID_PERSONA_ORIGEN", interviniente.getNumeroDocumento());
    addParameter(request, "ID_MOTIVO_OPERACION", null);
    addParameter(request, "ID_ORIGEN", "FENIX");
    addParameter(request, "ID_TIPO_ORIGEN", "FENIX");
    addParameter(request, "FECHA_OPERACION", dateFormat.format(new Date()));
    addParameter(request, "ID_TIPO_IDENTIFICADOR", "NIF/CIF");
    addParameter(request, "ID_ROL", getRolInterviniente(tipoIntervencion));

    log.info("Llamada al servicio de maestros -> Interviniente: " + interviniente.getNumeroDocumento());

    ProcessEventResponseType response = getResponse(request);
    if (response.getResultCode() == null
      || !response.getResultCode().equals(RESPONSE_OK_PERSONAS)) {
      throw new LockedChangeException(false);
    }

    List<KeyValuePair> keyValuePairs = response.getParameters().getParameter();
    KeyValuePair kvParameterIdHaya =
      keyValuePairs.stream()
        .filter(keyValuePair -> keyValuePair.getCode().equals("ID_PERSONA_HAYA"))
        .findFirst()
        .orElseThrow(() -> new NotFoundException("ID", false));

    if (kvParameterIdHaya.getValue().equals("-1")) {
      throw new NotFoundException("ID", false);
    }

    interviniente.setIdHaya(kvParameterIdHaya.getValue());
    log.info("Respuesta del servicio de maestros -> Interviniente: " + interviniente.getIdHaya());
  }

  private ProcessEventResponseType getResponse(ProcessEventRequestType request) {
    Object result = getWebServiceTemplate().marshalSendAndReceive(request);
    return ((JAXBElement<ProcessEventResponseType>) result).getValue();
  }

  private static List<String> getRolesInterviniente(Interviniente interviniente) throws NotFoundException {
    Set<String> roles = new HashSet<>();
    for (ContratoInterviniente ci : interviniente.getContratos()) {
      if (ci.getTipoIntervencion() != null) {
        roles.add(getRolInterviniente(ci.getTipoIntervencion()));
      }
    }
    return new ArrayList<>(roles);
  }

  private static String getRolInterviniente(TipoIntervencion tipoIntervencion) throws NotFoundException {
    if (LISTA_ROLES.containsKey(tipoIntervencion.getCodigo())) {
      return LISTA_ROLES.get(tipoIntervencion.getCodigo());
    } else {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("Invalid intervention type");
      else throw new NotFoundException("Tipo de intervención no válido");
    }
  }

  private static void addParameter(
    ProcessEventRequestType request, String code, String value) {
    KeyValuePair param = new KeyValuePair();
    param.setCode(code);
    param.setFormat(ConfigMaestro.STRING_FORMAT);
    param.setValue(value != null ? value : "");
    request.getParameters().getParameter().add(param);
  }
}
