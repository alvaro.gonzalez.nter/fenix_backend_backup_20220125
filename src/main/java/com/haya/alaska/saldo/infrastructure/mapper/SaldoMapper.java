package com.haya.alaska.saldo.infrastructure.mapper;

import com.haya.alaska.saldo.infrastructure.controller.dto.SaldoDto;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Map;

@Component
public class SaldoMapper {
  public SaldoDto parseHistorico(Map saldo) throws IllegalAccessException {
    SaldoDto saldoDTO = new SaldoDto();
    Field[] declaredFields = SaldoDto.class.getDeclaredFields();
    for (Field declaredField : declaredFields) { // non entity fields
      declaredField.setAccessible(true);
      String name = declaredField.getName();
      if (saldo.get(name) == null) continue;
      if (saldo.get(name).getClass().equals(BigDecimal.class)){
        declaredField.set(saldoDTO,Double.parseDouble(saldo.get(name).toString())) ;
      }
      else{
        declaredField.set(saldoDTO,saldo.get(name));
      }
    }
    if(saldoDTO.getHistorico()!=null)
      saldoDTO.getHistorico().setHistorico(null);
    return saldoDTO;
  }
}
