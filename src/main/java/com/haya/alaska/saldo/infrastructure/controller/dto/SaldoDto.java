package com.haya.alaska.saldo.infrastructure.controller.dto;

import com.haya.alaska.saldo.domain.Saldo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SaldoDto implements Serializable {

  private Date dia;
  private Double capitalPendiente;
  private Double saldoGestion;
  private Double principalVencidoImpagado;
  private Double interesesOrdinariosImpagados;
  private Double interesesDemora;
  private Double comisionesImpagados;
  private Double gastosImpagados;
  private Double deudaImpagada;
  private SaldoDto historico;

  public SaldoDto(Saldo saldo) {
    BeanUtils.copyProperties(saldo, this);
  }
}
