package com.haya.alaska.saldo.infrastructure.repository;

import com.haya.alaska.saldo.domain.Saldo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface SaldoRepository extends JpaRepository<Saldo, Integer> {

  Optional<Saldo> findTopByContratoIdOrderByDiaDesc(@Param("idContrato") Integer idContrato);

  List<Saldo> findAllByContratoIdOrderByDiaDesc(@Param("idContrato") Integer idContrato);

  Page<Saldo> findAllByContratoIdOrderByDiaDesc(@Param("idContrato") Integer idContrato, Pageable pageable);

  Integer countByContratoId(@Param("id") Integer idContrato);

  List<Saldo> findAllByContratoIdAndDiaLessThanEqualOrderByDiaDesc(Integer idContrato, Date dia);

  List<Saldo> findAllByContratoIdAndDiaLessThanOrderByDiaDesc(Integer idContrato, Date dia);

  List<Saldo> findAllByContratoIdAndDiaGreaterThanOrderByDiaAsc(Integer idContrato, Date dia);


  List<Saldo> findAllByContratoIdAndDiaEqualsOrderByDiaAsc(Integer idContrato, Date dia);


  @Query(
          value =
                  "SELECT sh.ID, sh.REV, sh.REVTYPE, sh.IND_ACTIVO, sh.NUM_CAPITAL_PENDIENTE, sh.NUM_COMISIONES_IMPAGADOS, sh.FCH_DIA, "
                          + "sh.NUM_GASTOS_IMPAGADOS, sh.NUM_IMPORTE_RECUPERADO, sh.NUM_INTERESES_DEMORA, sh.NUM_INTERESES_ORDINARIOS_IMPAGADOS, "
                          + "sh.NUM_PRINCIPAL_VENCIDO_IMAGADO, sh.NUM_SALDO_GESTION, sh.ID_CONTRATO, r.id, r.timestamp, r.ID_USUARIO "
                          + "FROM HIST_MSTR_SALDO sh "
                          + "JOIN REGISTRO_HISTORICO r ON sh.REV = r.id "
                          + "WHERE sh.ID_CONTRATO = ?1 "
                          + "AND r.ID_USUARIO IS NULL ORDER BY timestamp DESC LIMIT 1",
          nativeQuery = true)
  Map findHistoricoBySaldoId(Integer id);

  Saldo findByContratoIdAndDia(Integer idContrato, Date dia);
}
