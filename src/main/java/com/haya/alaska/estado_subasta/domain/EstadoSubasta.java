package com.haya.alaska.estado_subasta.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.subasta.domain.Subasta;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_ESTADO_SUBASTA")
@Entity
@Table(name = "LKUP_ESTADO_SUBASTA")
public class EstadoSubasta extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_SUBASTA")
  @NotAudited
  private Set<Subasta> subastas = new HashSet<>();

  @Column(name = "IND_RECOVERY", nullable = false)
  private boolean recovery = false;
}
