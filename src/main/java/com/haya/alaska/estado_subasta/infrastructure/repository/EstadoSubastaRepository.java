package com.haya.alaska.estado_subasta.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estado_subasta.domain.EstadoSubasta;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EstadoSubastaRepository extends CatalogoRepository<EstadoSubasta, Integer> {
  Optional<EstadoSubasta> findByValorAndRecoveryIsTrue(String valor);
}
