package com.haya.alaska.carga_control_transformacion.tasklet;

import com.haya.alaska.carga_control_transformacion.services.ControlFicheroVacioService;
import com.haya.alaska.shared.exceptions.EmptyFileException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Slf4j
@Getter
@Setter
@Component
@StepScope
public class ControlFicheroVacioTasklet implements Tasklet {


  @Autowired
  private ControlFicheroVacioService controlFicheroVacioService;

  //@Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  private String fileName;

  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    this.carteraId = chunkContext.getStepContext().getStepExecution()
      .getJobParameters().getString("defaultCartera");
    log.info("ControlFicheroVacioTasklet iniciated");

    Boolean esValido = controlFicheroVacioService.esFicheroValido(fileName);

    if (!esValido) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new EmptyFileException("The file " + fileName + " contains one line or less");
      else throw new EmptyFileException("El fichero " + fileName + " contiene una linea o menos");
    }

    log.info("Evalualuado el fichero {} ¿está vacio? {} ", esValido);

    return RepeatStatus.FINISHED;
  }

}
