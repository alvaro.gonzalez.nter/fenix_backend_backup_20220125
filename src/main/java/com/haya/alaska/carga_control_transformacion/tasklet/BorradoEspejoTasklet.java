package com.haya.alaska.carga_control_transformacion.tasklet;

import com.haya.alaska.espejo.bien.infrastructure.repository.BienEspejoRepository;
import com.haya.alaska.espejo.carga.infrastructure.repository.CargaEspejoRepository;
import com.haya.alaska.espejo.contrato.infrastructure.repository.ContratoEspejoRepository;
import com.haya.alaska.espejo.contrato_bien.infrastructure.repository.ContratoBienEspejoRepository;
import com.haya.alaska.espejo.contrato_interviniente.infrastructure.repository.ContratoIntervinienteEspejoRepository;
import com.haya.alaska.espejo.dato_contacto.infrastructure.repository.DatoContactoEspejoRepository;
import com.haya.alaska.espejo.direccion.infrastructure.repository.DireccionEspejoRepository;
import com.haya.alaska.espejo.expediente.infrastructure.repository.ExpedienteEspejoRepository;
import com.haya.alaska.espejo.interviniente.infrastructure.repository.IntervinienteEspejoRepository;
import com.haya.alaska.espejo.movimiento.infrastructure.repository.MovimientoEspejoRepository;
import com.haya.alaska.espejo.saldo.infrastructure.repository.SaldoEspejoRepository;
import com.haya.alaska.espejo.tasacion.infrastructure.repository.TasacionEspejoRepository;
import com.haya.alaska.espejo.valoracion.infrastructure.repository.ValoracionEspejoRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

@Slf4j
public class BorradoEspejoTasklet implements Tasklet {

  @Value("${spring.datasource.url}")
  private String dataSourceUrl;

  @Value("${spring.datasource.username}")
  private String dataSourceUser;

  @Value("${spring.datasource.password}")
  private String dataSourcePassword;

  @PersistenceContext
  private EntityManager manager;

  @Autowired
  private ContratoEspejoRepository contratoEspejoRepository;

  @Autowired
  private ValoracionEspejoRepository valoracionEspejoRepository;

  @Autowired
  private ContratoIntervinienteEspejoRepository contratoIntervinienteEspejoRepository;

  @Autowired
  private SaldoEspejoRepository saldoEspejoRepository;

  @Autowired
  private DatoContactoEspejoRepository datoContactoEspejoRepository;

  @Autowired
  private MovimientoEspejoRepository movimientoEspejoRepository;

  @Autowired
  private ContratoBienEspejoRepository contratoBienEspejoRepository;

  @Autowired
  private DireccionEspejoRepository direccionEspejoRepository;

  @Autowired
  private ExpedienteEspejoRepository expedienteEspejoRepository;

  @Autowired
  private IntervinienteEspejoRepository intervinienteEspejoRepository;

  @Autowired
  private TasacionEspejoRepository tasacionEspejoRepository;

  @Autowired
  private BienEspejoRepository bienEspejoRepository;

  @Autowired
  private CargaEspejoRepository cargaEspejoRepository;

  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

    log.info("Inicio Borrado Tablas");
    Connection conn = DriverManager.getConnection(dataSourceUrl, dataSourceUser, dataSourcePassword);
    String query = "delete from ";
    String[] table = {
      StringUtils.toRootUpperCase("MSTR_MOVIMIENTO_ESPEJO"),
      StringUtils.toRootUpperCase("MSTR_VALORACION_ESPEJO"),
      StringUtils.toRootUpperCase("RELA_CONTRATO_INTERVINIENTE_ESPEJO"),
      StringUtils.toRootUpperCase("MSTR_SALDO_ESPEJO"),
      StringUtils.toRootUpperCase("RELA_CONTRATO_BIEN_ESPEJO"),
      StringUtils.toRootUpperCase("MSTR_DATO_CONTACTO_ESPEJO"),
      StringUtils.toRootUpperCase("MSTR_CONTRATO_ESPEJO"),
      StringUtils.toRootUpperCase("MSTR_DIRECCION_ESPEJO"),
      StringUtils.toRootUpperCase("MSTR_EXPEDIENTE_ESPEJO"),
      StringUtils.toRootUpperCase("MSTR_INTERVINIENTE_ESPEJO"),
      StringUtils.toRootUpperCase("MSTR_TASACION_ESPEJO"),
      StringUtils.toRootUpperCase("RELA_BIEN_CARGA_ESPEJO"),
      StringUtils.toRootUpperCase("MSTR_BIEN_ESPEJO"),
      StringUtils.toRootUpperCase("MSTR_CARGA_ESPEJO")
    };

    for (int i = 0; i < table.length; i++) {
      PreparedStatement preparedStmt = conn.prepareStatement(query + table[i]);
      preparedStmt.execute();
      log.info("Tabla " + table[i] + " Borrada");
    }
    conn.close();
    log.info("Tablas Espejo Borradas");


    return RepeatStatus.FINISHED;
  }

}
