package com.haya.alaska.carga_control_transformacion.tasklet;

import com.haya.alaska.carga_control_transformacion.services.CalculateExpedienteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@StepScope
public class CalculateExpedienteTasklet implements Tasklet {


  @Autowired
  private CalculateExpedienteService calculateExpedienteService;

  //@Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  private Boolean multicartera;

  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    this.carteraId = chunkContext.getStepContext().getStepExecution()
      .getJobParameters().getString("defaultCartera");

    this.multicartera = Boolean.valueOf(chunkContext.getStepContext().getStepExecution()
      .getJobParameters().getString("multicartera"));
    log.info("CalculateExpedienteTasklet iniciated");

    calculateExpedienteService.agruparContratos(carteraId, multicartera);

    log.info("CalculateExpedienteTasklet para cartera {}", carteraId);

    return RepeatStatus.FINISHED;
  }

}
