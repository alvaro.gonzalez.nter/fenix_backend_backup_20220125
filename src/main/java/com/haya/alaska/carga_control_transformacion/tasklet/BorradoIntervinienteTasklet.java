package com.haya.alaska.carga_control_transformacion.tasklet;

import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Slf4j
public class BorradoIntervinienteTasklet implements Tasklet {

  @PersistenceContext
  private EntityManager manager;

  @Autowired
  private IntervinienteRepository intervinienteRepository;

  @Autowired
  private ContratoIntervinienteRepository contratoIntervinienteRepository;

  @Autowired
  private DireccionRepository direccionRepository;

  @Autowired
  private DatoContactoRepository datoContactoRepository;

  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
    log.info("Inicio Borrado Franco");
    try {
      Interviniente interviniente = intervinienteRepository.findByNumeroDocumento("00000001R").orElse(null);
      if (interviniente != null) {
        direccionRepository.deleteAll(interviniente.getDirecciones());
        datoContactoRepository.deleteAll(interviniente.getDatosContacto());
        contratoIntervinienteRepository.deleteAll(interviniente.getContratos());
        intervinienteRepository.delete(interviniente);
      }
    } catch (Exception ex) {
    }

    return RepeatStatus.FINISHED;
  }

}
