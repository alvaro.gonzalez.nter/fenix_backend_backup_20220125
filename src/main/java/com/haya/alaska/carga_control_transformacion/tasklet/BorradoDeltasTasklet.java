package com.haya.alaska.carga_control_transformacion.tasklet;

import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.estado_contrato.infrastructure.repository.EstadoContratoRepository;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Slf4j
@Getter
@Setter
public class BorradoDeltasTasklet implements Tasklet {

  @Value("${service.warehouse.csvs}")
  private String csvsFolder;

  @Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  @Value("${spring.datasource.url}")
  private String dataSourceUrl;

  @Value("${spring.datasource.username}")
  private String dataSourceUser;

  @Value("${spring.datasource.password}")
  private String dataSourcePassword;

  @PersistenceContext
  private EntityManager manager;

  private BienRepository bienRepository;

  private ContratoRepository contratoRepository;

  private IntervinienteRepository intervinienteRepository;

  private CarteraRepository carteraRepository;

  private EstadoContratoRepository estadoContratoRepository;

  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

    log.info("Inicio borrado deltas");
    long start = System.currentTimeMillis();
    Cartera cartera = getCartera(carteraId);
    EstadoContrato estadoContrato = estadoContratoRepository.findByCodigo("CER").orElse(new EstadoContrato());

    if (cartera.getDatosCoreBanking() != null && cartera.getDatosCoreBanking() == true) {
      contratoRepository.updateAllBorradoDeltasContrato(estadoContrato.getId(), carteraId);
      //contratoRepository.flush();
      /*

      List<Contrato> contratos = contratoRepository.findAllByExpedienteCarteraIdCargaAndExpedienteCarteraIdCargaSubcarteraIsNotNull(carteraId);
      for(Contrato contrato : contratos){
        contrato.setActivo(false);
        contrato.setEstadoContrato(estadoContrato);
      }*/

      contratoRepository.updateAllBorradoDeltasBien(carteraId);
      /*
      List<Bien> bienes = bienRepository.findByContratosContratoExpedienteCarteraIdCargaAndContratosContratoExpedienteCarteraIdCargaSubcarteraIsNotNull(carteraId);
      for(Bien bien : bienes){
        bien.setActivo(false);
        for(ContratoBien contratoBien : bien.getContratos()){
          contratoBien.setActivo(false);
        }
      }*/

      contratoRepository.updateAllBorradoDeltasInterviniente(carteraId);
      /*
      List<Interviniente> intervinientes = intervinienteRepository.findByContratosContratoExpedienteCarteraIdCargaAndContratosContratoExpedienteCarteraIdCargaSubcarteraIsNotNull(carteraId);
      for(Interviniente interviniente : intervinientes){
        interviniente.setActivo(false);
        for(ContratoInterviniente contratoInterviniente : interviniente.getContratos()){
          contratoInterviniente.setActivo(false);
        }

      }*/
      //contratoRepository.saveAll(contratos);
      //bienRepository.saveAll(bienes);
      //intervinienteRepository.saveAll(intervinientes);

    }
    long finish = System.currentTimeMillis();
    long timeElapsed = finish - start;
    log.info("Fin borrado deltas (" + timeElapsed + "ms)");

    return RepeatStatus.FINISHED;
  }

  private Cartera getCartera(String idCargaCartera) {
    try {
      var a = carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(idCargaCartera);
      Cartera cartera = a.get();
      return cartera;
    } catch (Exception ex) {
      System.out.println(idCargaCartera + "EXCEPCION AL BUSCAR CARTERA");
      ex.printStackTrace();
    }
    return null;
  }

}
