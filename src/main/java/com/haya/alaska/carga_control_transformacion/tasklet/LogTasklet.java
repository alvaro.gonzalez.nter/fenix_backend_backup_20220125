package com.haya.alaska.carga_control_transformacion.tasklet;

import com.haya.alaska.carga_control_transformacion.domain.AprovisionamientoValidacion;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.cartera_fecha_excepcion.domain.CarteraFechaExcepcion;
import com.haya.alaska.espejo.bien.infrastructure.repository.BienEspejoRepository;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.espejo.contrato.infrastructure.repository.ContratoEspejoRepository;
import com.haya.alaska.espejo.contrato_interviniente.domain.ContratoIntervinienteEspejo;
import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import com.haya.alaska.espejo.interviniente.infrastructure.repository.IntervinienteEspejoRepository;
import com.haya.alaska.espejo.tasacion.infrastructure.repository.TasacionEspejoRepository;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.estado_contrato.infrastructure.repository.EstadoContratoRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Getter
@Setter
public class LogTasklet implements Tasklet {

  @Value("${service.warehouse.csvs}")
  private String csvsFolder;

  @Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  @Value("${spring.datasource.url}")
  private String dataSourceUrl;

  @Value("${spring.datasource.username}")
  private String dataSourceUser;

  @Value("${spring.datasource.password}")
  private String dataSourcePassword;

  @PersistenceContext
  private EntityManager manager;

  @Value("#{jobParameters['manual']}")
  private String manual;

  private BienEspejoRepository bienEspejoRepository;

  private ContratoEspejoRepository contratoEspejoRepository;

  private IntervinienteEspejoRepository intervinienteEspejoRepository;

  private TasacionEspejoRepository tasacionEspejoRepository;

  private CarteraRepository carteraRepository;

  private EstadoContratoRepository estadoContratoRepository;

  private Date fechaFichero;

  private List<AprovisionamientoValidacion> aprovisionamientoValidaciones = new ArrayList<>();

  private Double toleranciaArriba;

  private Double toleranciaAbajo;

  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
    if (manual.equals("false")) {
      Cartera cartera = carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(carteraId).get();
      Integer idCartera = cartera.getId();
      Set<CarteraFechaExcepcion> fechasExcepcion = cartera.getCarteraFechaExcepciones();
      Date hoy = new Date();
      SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
      boolean validaciones = false;
      for (CarteraFechaExcepcion carteraFechaExcepcion : fechasExcepcion) {
        try {
          if (objSDF.format(hoy).compareTo(objSDF.format(carteraFechaExcepcion.getFecha())) == 0
            && carteraFechaExcepcion.getActivo())
            validaciones = false;
        } catch (Exception exception) {
        }
      }
      if (validaciones) {
        List<Integer> idCarteras = new ArrayList<>();
        idCarteras.add(idCartera);
        if (idCartera == 4) {
          idCarteras.add(5);
          idCarteras.add(11);
          idCarteras.add(36);
        }
        log.info("Inicio Validaciones");
        String fechaFicheroString = chunkContext.getStepContext().getStepExecution()
          .getJobParameters().getString("fechaFichero");
        this.fechaFichero = new Date(Integer.valueOf(fechaFicheroString.substring(0, 4)) - 1900, Integer.valueOf(fechaFicheroString.substring(4, 6)) - 1, Integer.valueOf(fechaFicheroString.substring(6, 8)));
        int registrosOrigen = contratoEspejoRepository.findContadordestAyer(idCarteras);
        int registrosCargados = contratoEspejoRepository.findContadordest();

        Integer toleranciaBD = cartera.getTolerancia();
        if (toleranciaBD != null) {
          Double tolerancia = Double.valueOf(toleranciaBD);
          this.toleranciaArriba = (tolerancia + 100) / 100;
          this.toleranciaAbajo = (tolerancia - 100) / 100;
          Integer diasRetraso = cartera.getDiasRestrasoCancelacion();

          File iris = new File(csvsFolder + "ERRORES.txt");
          if (iris.exists()) {
            integridadBienes(iris);
            integridadIntervinientes(iris);
            anadirTasaciones(iris);
          }
          int contadorDest = contratoEspejoRepository.findContadordest();
          int contadorDestAyer = contratoEspejoRepository.findContadordestAyer(idCarteras);
          Double sumaImpago = contratoEspejoRepository.findSumaImpagodest();
          Double sumaImpagoAyer = contratoEspejoRepository.findSumaImpagodestAyer(idCarteras);
          Double sumaPendiente = contratoEspejoRepository.findSumaPendientedest();
          Double sumaPendienteAyer = contratoEspejoRepository.findSumaPendientedestAyer(idCarteras);
          Double sumaDeuda = contratoEspejoRepository.findSumaDeudadest();
          Double sumaDeudaAyer = contratoEspejoRepository.findSumaDeudadestAyer(idCarteras);
          int vigentes = intervinienteEspejoRepository.findVigentes();
          int vigentesAyer = intervinienteEspejoRepository.findVigentesAyer(idCarteras);
          int estado = bienEspejoRepository.findEstado();
          int estadoAyer = bienEspejoRepository.findEstadoAyer(idCarteras);
          int refCatastral = bienEspejoRepository.findRefCatastral();
          int refCatastralAyer = bienEspejoRepository.findRefCatastralAyer(idCarteras);
          int activos = bienEspejoRepository.findActivos();
          int activosAyer = bienEspejoRepository.findActivosAyer(idCarteras);
          Double importe = tasacionEspejoRepository.findSumaImporte();
          Double importeAyer = tasacionEspejoRepository.findSumaImporteAyer(idCarteras);
          List<Map> clasificacionContable = contratoEspejoRepository.findTpContable();
          List<Map> clasificacionContableAyer = contratoEspejoRepository.findTpContableAyer(idCarteras);
          List<Map> tpInterviniente = intervinienteEspejoRepository.findTpIntervinientes();
          List<Map> tpIntervinienteAyer = intervinienteEspejoRepository.findTpIntervinientesAyer(idCarteras);
          List<Map> tpDocumento = intervinienteEspejoRepository.findTpDocumento();
          List<Map> tpDocumentoAyer = intervinienteEspejoRepository.findTpDocumentoAyer(idCarteras);
          List<Map> producto = contratoEspejoRepository.findProducto();
          List<Map> productoAyer = contratoEspejoRepository.findProductoAyer(idCarteras);
          DecimalFormat df = new DecimalFormat("#");
          df.setMaximumFractionDigits(10);
          if (contadorDest * toleranciaArriba >= contadorDestAyer
            && contadorDest * toleranciaAbajo <= contadorDestAyer) {
          } else {
            AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Contrato", "Estados del contrato Hoy: " + contadorDest + " Ayer: " + contadorDestAyer);
            aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
          }
          if (sumaImpago * toleranciaArriba >= sumaImpagoAyer
            && sumaImpago * toleranciaAbajo <= sumaImpagoAyer) {
          } else {
            AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Contrato", "Principal en impago Hoy: " + df.format(sumaImpago) + " Ayer: " + df.format(sumaImpagoAyer));
            aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
          }

          if (sumaPendiente * toleranciaArriba >= sumaPendienteAyer
            && sumaPendiente * toleranciaAbajo <= sumaPendienteAyer) {
          } else {
            AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Contrato", "Principal pendiente de vencer Hoy: " + df.format(sumaPendiente) + " Ayer: " + df.format(sumaPendienteAyer));
            aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
          }

          if (sumaDeuda * toleranciaArriba >= sumaDeudaAyer
            && sumaDeuda * toleranciaAbajo <= sumaDeudaAyer) {
          } else {
            AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Contrato", "Deuda en impago Hoy: " + df.format(sumaDeuda) + " Ayer: " + df.format(sumaDeudaAyer));
            aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
          }

          if (vigentes * toleranciaArriba >= vigentesAyer
            && vigentes * toleranciaAbajo <= vigentesAyer) {
          } else {
            AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Intervinientes", "Vigentes Hoy: " + vigentes + " Ayer: " + vigentesAyer);
            aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
          }

          for (Map clasificacionContableMap : clasificacionContable) {
            int clasificacionContableCont = ((BigInteger) clasificacionContableMap.get("Contador")).intValue();
            String clasificacionContableDesc = (String) clasificacionContableMap.get("Tipo");
            for (Map clasificacionContableAyerMap : clasificacionContableAyer) {
              int clasificacionContableAyerCont = ((BigInteger) clasificacionContableAyerMap.get("Contador")).intValue();
              String clasificacionContableAyerDesc = (String) clasificacionContableAyerMap.get("Tipo");
              if (clasificacionContableDesc.equals(clasificacionContableAyerDesc)) {
                if (clasificacionContableCont * toleranciaArriba >= clasificacionContableAyerCont
                  && clasificacionContableCont * toleranciaAbajo <= clasificacionContableAyerCont) {
                } else {
                  AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Contrato", "Clasificación contable Tipo: " + clasificacionContableAyerDesc + " Hoy: " + clasificacionContableCont + " Ayer: " + clasificacionContableAyerCont);
                  aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
                }
              }
            }
          }

          for (Map tpDocumentoMap : tpDocumento) {
            int tpDocumentoCont = ((BigInteger) tpDocumentoMap.get("Contador")).intValue();
            String tpDocumentoDesc = (String) tpDocumentoMap.get("Tipo");
            for (Map tpDocumentoAyerMap : tpDocumentoAyer) {
              int tpDocumentoAyerCont = ((BigInteger) tpDocumentoAyerMap.get("Contador")).intValue();
              String tpDocumentoAyerDesc = (String) tpDocumentoAyerMap.get("Tipo");
              if (tpDocumentoDesc.equals(tpDocumentoAyerDesc)) {
                if (tpDocumentoCont * toleranciaArriba >= tpDocumentoAyerCont
                  && tpDocumentoCont * toleranciaAbajo <= tpDocumentoAyerCont) {
                } else {
                  AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Intervinientes", "Tipo de documento Tipo: " + tpDocumentoAyerDesc + " Hoy: " + tpDocumentoCont + " Ayer: " + tpDocumentoAyerCont);
                  aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
                }
              }
            }
          }
          for (Map productoMap : producto) {
            int productoCont = ((BigInteger) productoMap.get("Contador")).intValue();
            String productoDesc = (String) productoMap.get("Tipo");
            for (Map productoAyerMap : productoAyer) {
              int productoAyerCont = ((BigInteger) productoAyerMap.get("Contador")).intValue();
              String productoAyerDesc = (String) productoAyerMap.get("Tipo");
              if (productoDesc.equals(productoAyerDesc)) {
                if (productoCont * toleranciaArriba >= productoAyerCont
                  && productoCont * toleranciaAbajo <= productoAyerCont) {
                } else {
                  AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Contrato", "Producto Tipo: " + productoAyerDesc + " Hoy: " + productoCont + " Ayer: " + productoAyerCont);
                  aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
                }
              }
            }
          }
          for (Map tpIntervinienteMap : tpInterviniente) {
            int tpIntervinienteCont = ((BigInteger) tpIntervinienteMap.get("Contador")).intValue();
            String tpIntervinienteDesc = (String) tpIntervinienteMap.get("Tipo");
            for (Map tpIntervinienteAyerMap : tpIntervinienteAyer) {
              int tpIntervinienteAyerCont = ((BigInteger) tpIntervinienteAyerMap.get("Contador")).intValue();
              String tpIntervinienteAyerDesc = (String) tpIntervinienteAyerMap.get("Tipo");
              if (tpIntervinienteDesc.equals(tpIntervinienteAyerDesc)) {
                if (tpIntervinienteCont * toleranciaArriba >= tpIntervinienteAyerCont
                  && tpIntervinienteCont * toleranciaAbajo <= tpIntervinienteAyerCont) {
                } else {
                  AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Intervinientes", "Tipo de intervencion  Tipo: " + tpIntervinienteAyerDesc + " Hoy: " + tpIntervinienteCont + " Ayer: " + tpIntervinienteAyerCont);
                  aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
                }
              }
            }
          }

          if (estado * toleranciaArriba >= estadoAyer
            && estado * toleranciaAbajo <= estadoAyer) {
          } else {
            AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Bienes", "Vigentes Hoy: " + estado + " Ayer: " + estadoAyer);
            aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
          }

          if (refCatastral * toleranciaArriba >= refCatastralAyer
            && refCatastral * toleranciaAbajo <= refCatastralAyer) {
          } else {
            AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Bienes", "Referencia catastral Hoy: " + refCatastral + " Ayer: " + refCatastralAyer);
            aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
          }

          if (activos * toleranciaArriba >= activosAyer
            && activos * toleranciaAbajo <= activosAyer) {
          } else {
            AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Bienes", "Tipo de activo Hoy: " + activos + " Ayer: " + activosAyer);
            aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
          }

          if (importe * toleranciaArriba >= importeAyer
            && importe * toleranciaAbajo <= importeAyer) {
          } else {
            AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Deltas", "Tasaciones", "Importe tasación Hoy: " + importe + " Ayer: " + importeAyer);
            aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
          }
        }
        List<ContratoEspejo> contratoEspejosCheck = new ArrayList<>();
        EstadoContrato estadoContrato = estadoContratoRepository.findByCodigo("ERR").orElse(new EstadoContrato());
        List<ContratoEspejo> contratosAviso = getContratosAviso(contratoEspejosCheck, estadoContrato);
        List<IntervinienteEspejo> intervinientesAviso = getIntervinientesAviso(contratoEspejosCheck, estadoContrato);
        List<String> bienesAviso = bienEspejoRepository.findBienesNoCargados(idCarteras);
        int contratosLiberacionAviso = bienEspejoRepository.findContratosLiberacion(idCarteras);
        contratoEspejoRepository.saveAll(contratoEspejosCheck);
        introducirAvisos(contratosAviso, intervinientesAviso, bienesAviso, contratosLiberacionAviso);
        GenerarExcel generarExcel = new GenerarExcel();
        generarExcel.generarExcel(csvsFolder, cartera, registrosOrigen, registrosCargados, fechaFichero,
          contratosLiberacionAviso + intervinientesAviso.size() + contratosAviso.size(), aprovisionamientoValidaciones);

        log.info("Fin Validaciones");

      }
    }
    return RepeatStatus.FINISHED;
  }

  private void introducirAvisos(List<ContratoEspejo> contratosAviso, List<IntervinienteEspejo> intervinientesAviso, List<String> bienesAviso, int liberacion) {
    for (ContratoEspejo contratoEspejo : contratosAviso) {
      AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Carga con aviso", "Formato erróneo", "Contrato", " ");
      aprovisionamientoValidacion.setCantidadIntegridad(contratosAviso.size());
      aprovisionamientoValidacion.setIdObjeto(contratoEspejo.getIdCarga());
      aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
    }
    for (IntervinienteEspejo intervinienteEspejo : intervinientesAviso) {
      AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Carga con aviso", "Formato erróneo", "Interviniente", " ");
      aprovisionamientoValidacion.setCantidadIntegridad(intervinientesAviso.size());
      aprovisionamientoValidacion.setIdObjeto(intervinienteEspejo.getIdCarga());
      aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
    }
    for (String s : bienesAviso) {
      AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Carga con aviso", "Liberación garantías", "Liberación garantías", " ");
      aprovisionamientoValidacion.setCantidadIntegridad(liberacion);
      aprovisionamientoValidacion.setIdObjeto(s);
      aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
    }
  }

  private List<IntervinienteEspejo> getIntervinientesAviso(List<ContratoEspejo> contratoEspejosCheck, EstadoContrato estadoContrato) {
    List<IntervinienteEspejo> intervinienteEspejos = intervinienteEspejoRepository.findAll();
    List<IntervinienteEspejo> intervinientesReturn = new ArrayList<>();
    for (IntervinienteEspejo intervinienteEspejo : intervinienteEspejos) {
      if (intervinienteEspejo.getNumeroDocumento().trim().length() != 9) {
        for (ContratoIntervinienteEspejo contratoIntervinienteEspejo : intervinienteEspejo.getContratos()) {
          if (!contratoEspejosCheck.contains(contratoIntervinienteEspejo.getContrato())) {
            contratoIntervinienteEspejo.getContrato().setEstadoContrato(estadoContrato);
            contratoEspejosCheck.add(contratoIntervinienteEspejo.getContrato());
          }
          intervinientesReturn.add(intervinienteEspejo);
        }
      }
    }
    return intervinientesReturn;
  }

  private List<ContratoEspejo> getContratosAviso(List<ContratoEspejo> contratoEspejosCheck, EstadoContrato estadoContrato) {
    List<ContratoEspejo> contratosEspejo = contratoEspejoRepository.findAll();
    List<ContratoEspejo> contratosReturn = new ArrayList<>();
    for (ContratoEspejo contratoEspejo : contratosEspejo) {
      if (contratoEspejo.getIdCarga().length() > 10) {
        if (!contratoEspejosCheck.contains(contratoEspejo)) {
          contratoEspejo.setEstadoContrato(estadoContrato);
          contratoEspejosCheck.add(contratoEspejo);
        }
        contratosReturn.add(contratoEspejo);
      }
    }
    return contratosReturn;
  }

  private void anadirTasaciones(File iris) throws IOException {
    FileReader fichero = new FileReader(iris);
    BufferedReader br = new BufferedReader(fichero);
    br.readLine();
    String linea;
    int tasaciones = 0;
    int intervinienteContacto = 0;
    int intervinienteDireccion = 0;
    int movimiento = 0;
    int contratoMaestro = 0;
    while ((linea = br.readLine()) != null) {
      String[] partes = linea.split(";");
      if (partes[1].equals("Tasacion con importe 0 para bien")) tasaciones++;
      if (partes[1].equals("Interviniente no encontrado con el id") && partes[0].equals("CONTACTO"))
        intervinienteContacto++;
      if (partes[1].equals("Interviniente no encontrado con el id") && partes[0].equals("DIRECCION"))
        intervinienteDireccion++;
      if (partes[1].equals("Contrato no encontrado fichero contratos") && partes[0].equals("MOVIMIENTO")) movimiento++;
      if (partes[1].equals("Contrato no encontrado en maestro") && partes[0].equals("CONTRATO")) contratoMaestro++;
    }
    fichero = new FileReader(iris);
    br = new BufferedReader(fichero);
    br.readLine();
    while ((linea = br.readLine()) != null) {
      String[] partes = linea.split(";");
      if (partes[1].equals("Tasacion con importe 0 para bien")) {
        AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Otras validaciones", "Otras validaciones", "Tasaciones con importe 0", "Tasaciones con importe 0", partes[2]);
        aprovisionamientoValidacion.setCantidadIntegridad(tasaciones);
        aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
      }
      if (partes[1].equals("Interviniente no encontrado con el id") && partes[0].equals("CONTACTO")) {
        AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Otras validaciones", "Otras validaciones", "Fichero Contacto", "Id_Interviniente asociado a contacto no encontrado en fichero interviniente", partes[2]);
        aprovisionamientoValidacion.setCantidadIntegridad(intervinienteContacto);
        aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
      }
      if (partes[1].equals("Interviniente no encontrado con el id") && partes[0].equals("DIRECCION")) {
        AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Otras validaciones", "Otras validaciones", "Fichero Direccion", "Id_Interviniente asociado a direccion no encontrado en fichero interviniente", partes[2]);
        aprovisionamientoValidacion.setCantidadIntegridad(intervinienteDireccion);
        aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
      }
      if (partes[1].equals("Contrato no encontrado fichero contratos") && partes[0].equals("MOVIMIENTO")) {
        AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Otras validaciones", "Otras validaciones", "Fichero Movimiento", "Id_Contrato asociado a movimiento no encontrado en fichero CNT", partes[2]);
        aprovisionamientoValidacion.setCantidadIntegridad(movimiento);
        aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
      }
      if (partes[1].equals("Contrato no encontrado en maestro") && partes[0].equals("CONTRATO")) {
        AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Otras validaciones", "Otras validaciones", "Fichero Contrato", "Contrato no encontrado en maestro", partes[2]);
        aprovisionamientoValidacion.setCantidadIntegridad(contratoMaestro);
        aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
      }

    }
  }

  private void integridadBienes(File iris) throws IOException {
    FileReader fichero = new FileReader(iris);
    BufferedReader br = new BufferedReader(fichero);
    String linea;
    br.readLine();
    //Integridad bienes con contrato-bien
    int bienes = 0;
    while ((linea = br.readLine()) != null) {
      String[] partes = linea.split(";");
      if (partes[1].equals("Bien no encontrado en el fichero de bienes")
        || partes[1].equals("Bien no encontrado en fichero bienes contrato")
        || (partes[1].equals("Contrato no encontrado fichero contratos") && partes[0].equals("BIEN"))
        || (partes[1].equals("Contrato no encontrado fichero bienes") && partes[0].equals("BIEN"))) bienes++;
    }
    Double bienesEspejo = Double.valueOf(bienEspejoRepository.findAll().size());
    if (bienes * toleranciaArriba >= bienesEspejo
      && bienes * toleranciaAbajo <= bienesEspejo) {
      fichero = new FileReader(iris);
      br = new BufferedReader(fichero);
      br.readLine();
      while ((linea = br.readLine()) != null) {
        String[] partes = linea.split(";");
        if (partes[1].equals("Bien no encontrado en el fichero de bienes")
          || partes[1].equals("Bien no encontrado en fichero bienes contrato")
          || (partes[1].equals("Contrato no encontrado fichero contratos") && partes[0].equals("BIEN"))
          || (partes[1].equals("Contrato no encontrado fichero bienes") && partes[0].equals("BIEN"))) {
          AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Integridad", "Contrato-Bien/Contrato-Interviniente", "Relacion Bien-ContratoBien/ContratoBien-Bien sin integridad", partes[2]);
          aprovisionamientoValidacion.setCantidadIntegridad(bienes);
          aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
        }
      }
    } else {
      fichero = new FileReader(iris);
      br = new BufferedReader(fichero);
      br.readLine();
      while ((linea = br.readLine()) != null) {
        String[] partes = linea.split(";");
        if (partes[1].equals("Bien no encontrado en el fichero de bienes")
          || partes[1].equals("Bien no encontrado en fichero bienes contrato")
          || (partes[1].equals("Contrato no encontrado fichero contratos") && partes[0].equals("BIEN"))
          || (partes[1].equals("Contrato no encontrado fichero bienes") && partes[0].equals("BIEN"))) {
          AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("", "Integridad", "Contrato-Bien/Contrato-Interviniente", "Relacion Bien-ContratoBien/ContratoBien-Bien sin integridad", partes[2]);
          aprovisionamientoValidacion.setCantidadIntegridad(bienes);
          aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
        }
      }
    }
  }

  private void integridadIntervinientes(File iris) throws IOException {
    String linea;
    FileReader fichero = new FileReader(iris);
    BufferedReader br = new BufferedReader(fichero);
    br.readLine();
    //Integridad intervinientes con contrato-interviniente
    int intervinientes = 0;
    while ((linea = br.readLine()) != null) {
      String[] partes = linea.split(";");
      if (partes[1].equals("Interviniente no encontrado en el fichero de interviniente")
        || partes[1].equals("Interviniente no encontrado en fichero interviniente contrato")
        || (partes[1].equals("Contrato no encontrado fichero contratos") && partes[0].equals("INTERVINIENTE"))
        || (partes[1].equals("Contrato no encontrado fichero intervinientes") && partes[0].equals("INTERVINIENTE")))
        intervinientes++;
    }
    int intervinientesEspejo = intervinienteEspejoRepository.findAll().size();
    if (intervinientes * toleranciaArriba >= intervinientesEspejo
      && intervinientes * toleranciaAbajo <= intervinientesEspejo) {
      fichero = new FileReader(iris);
      br = new BufferedReader(fichero);
      br.readLine();
      while ((linea = br.readLine()) != null) {
        String[] partes = linea.split(";");
        if (partes[1].equals("Interviniente no encontrado en el fichero de interviniente")
          || partes[1].equals("Interviniente no encontrado en fichero interviniente contrato")
          || (partes[1].equals("Contrato no encontrado fichero contratos") && partes[0].equals("INTERVINIENTE"))
          || (partes[1].equals("Contrato no encontrado fichero intervinientes") && partes[0].equals("INTERVINIENTE"))) {
          AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("Crítico", "Integridad", "Contrato-Bien/Contrato-Interviniente", "Relacion Interviniente-ContratoInterviniente/ContratoInterviniente-Interviniente sin integridad", partes[2]);
          aprovisionamientoValidacion.setCantidadIntegridad(intervinientes);
          aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
        }
      }
    } else {
      fichero = new FileReader(iris);
      br = new BufferedReader(fichero);
      br.readLine();
      while ((linea = br.readLine()) != null) {
        String[] partes = linea.split(";");
        if (partes[1].equals("Interviniente no encontrado en el fichero de interviniente")
          || partes[1].equals("Interviniente no encontrado en fichero interviniente contrato")
          || (partes[1].equals("Contrato no encontrado fichero contratos") && partes[0].equals("INTERVINIENTE"))
          || (partes[1].equals("Contrato no encontrado fichero intervinientes") && partes[0].equals("INTERVINIENTE"))) {
          AprovisionamientoValidacion aprovisionamientoValidacion = new AprovisionamientoValidacion("", "Integridad", "Contrato-Bien/Contrato-Interviniente", "Relacion Interviniente-ContratoInterviniente/ContratoInterviniente-Interviniente sin integridad", partes[2]);
          aprovisionamientoValidacion.setCantidadIntegridad(intervinientes);
          aprovisionamientoValidaciones.add(aprovisionamientoValidacion);
        }
      }
    }
  }

  public int[] recoger(ArrayList<int[]> resultadoQuery) {
    int i = 0;
    int[] datos = new int[resultadoQuery.size() + resultadoQuery.size()];
    for (int[] anIntArray : resultadoQuery) {
      for (int aNumber : anIntArray) {
        datos[i] = aNumber;
        i++;
      }
    }
    return datos;
  }
}
