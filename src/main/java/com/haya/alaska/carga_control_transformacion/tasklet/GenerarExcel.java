package com.haya.alaska.carga_control_transformacion.tasklet;

import com.haya.alaska.carga_control_transformacion.domain.AprovisionamientoValidacion;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class GenerarExcel {

  @Autowired
  private CarteraRepository carteraRepository;

  public void generarExcel(String csvsFolder, Cartera cartera, int registrosOrigen, int registrosCargados, Date fechaFichero, int avisos, List<AprovisionamientoValidacion> aprovisionamientoValidaciones) throws Exception {
    Date fecha = new Date();
    SimpleDateFormat objSDF = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat objString = new SimpleDateFormat("ddMMyyyy");
    String archcsvinterviniente = csvsFolder + "Informe_errores_aprovisionamiento_" + objString.format(fecha) + "_" + cartera.getNombre() + ".xls";
    HSSFWorkbook objWB = new HSSFWorkbook();

    HSSFSheet hoja1 = objWB.createSheet("Resumen");
    HSSFSheet hoja2 = objWB.createSheet("Detalle");

    CellStyle cellStyleCabecera = objWB.createCellStyle();
    Font cellFont = objWB.createFont();
    cellFont.setBold(true);
    cellFont.setItalic(true);
    cellStyleCabecera.setFont(cellFont);

    CellStyle cellStyleSub = objWB.createCellStyle();

    int filaNum = 0;

    HSSFRow fila = hoja1.createRow(filaNum++);
    HSSFCell celda = fila.createCell(0);
    celda.setCellValue("Cabecera");
    celda.setCellStyle(cellStyleCabecera);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Fecha de carga ");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(objSDF.format(fecha));
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Fecha de fichero ");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(objSDF.format(fechaFichero));
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Cartera ");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(cartera.getNombre());
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Cliente ");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(cartera.getCliente().getNombre());
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Totales ");
    celda.setCellStyle(cellStyleCabecera);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Numero de registros origen ");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(registrosOrigen);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Numero de registros cargados ok ");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(registrosCargados - avisos);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Numero de registros cargados con aviso ");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(avisos);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Errores criticos");
    celda.setCellStyle(cellStyleCabecera);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Completitud - Fichero fuente vacío");
    celda.setCellStyle(cellStyleCabecera);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Contratos");
    celda.setCellStyle(cellStyleSub);
    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Intervinientes");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Integridad - error en correspondencia");
    celda.setCellStyle(cellStyleCabecera);
    celda = fila.createCell(1);
    celda.setCellValue("");//TODO
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Contratos - Personas");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(0);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Bienes - contratos");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(0);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Deltas - variación mayor al margen definido");
    celda.setCellStyle(cellStyleCabecera);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Contratos");
    celda.setCellStyle(cellStyleCabecera);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Producto");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Estados del contrato");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Principal en impago");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Principal pendiente de vencer");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Deuda en impago");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);


    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Clasificación contable");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Intervininetes");
    celda.setCellStyle(cellStyleCabecera);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Vigentes");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Tipo de intervención");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Tipo de intervención");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);


    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Bienes");
    celda.setCellStyle(cellStyleCabecera);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Vigentes");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Referencia catastral");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Tipo de activo");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Tasaciones");
    celda.setCellStyle(cellStyleCabecera);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("  Importe tasación");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("ok");
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Carga con aviso");
    celda.setCellStyle(cellStyleCabecera);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Contrato con formato erróneo");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(0);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Interviniente con NIF erróneo");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(0);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Liberación de garantías");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(0);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("Otras validaciones");
    celda.setCellStyle(cellStyleCabecera);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Tasaciones con importe 0");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(0);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Fichero Contacto");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(0);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Fichero Direccion");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(0);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Fichero Movimiento");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(0);
    celda.setCellStyle(cellStyleSub);

    fila = hoja1.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue(" Fichero Contrato");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue(0);
    celda.setCellStyle(cellStyleSub);

    rellenarValidaciones(aprovisionamientoValidaciones, hoja1);

    filaNum = 0;
    fila = hoja2.createRow(filaNum++);
    celda = fila.createCell(0);
    celda.setCellValue("TIPO ERROR");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(1);
    celda.setCellValue("CLASIFICACIÓN");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(2);
    celda.setCellValue("NIVEL");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(3);
    celda.setCellValue("DESCRIPCIÓN");
    celda.setCellStyle(cellStyleSub);
    celda = fila.createCell(4);
    celda.setCellValue("ID");
    celda.setCellStyle(cellStyleSub);

    for (AprovisionamientoValidacion aprovisionamientoValidacion : aprovisionamientoValidaciones) {
      fila = hoja2.createRow(filaNum++);
      celda = fila.createCell(0);
      celda.setCellValue(!aprovisionamientoValidacion.getTipoError().equals("") ? aprovisionamientoValidacion.getTipoError() : "Crítico");
      celda.setCellStyle(cellStyleSub);
      celda = fila.createCell(1);
      celda.setCellValue(aprovisionamientoValidacion.getClasificacion());
      celda.setCellStyle(cellStyleSub);
      celda = fila.createCell(2);
      celda.setCellValue(aprovisionamientoValidacion.getNivel());
      celda.setCellStyle(cellStyleSub);
      celda = fila.createCell(3);
      celda.setCellValue(aprovisionamientoValidacion.getDescripcion());
      celda.setCellStyle(cellStyleSub);
      celda = fila.createCell(4);
      try {
        celda.setCellValue(Long.valueOf(aprovisionamientoValidacion.getIdObjeto()));
      } catch (Exception ex) {
        celda.setCellValue(aprovisionamientoValidacion.getIdObjeto());
      }
      celda.setCellStyle(cellStyleSub);
    }

    File objFile = new File(archcsvinterviniente);
    FileOutputStream archivoSalida = new FileOutputStream(objFile);
    objWB.write(archivoSalida);
    archivoSalida.close();

    comprobarCritico(aprovisionamientoValidaciones, cartera);

  }

  private void rellenarValidaciones(List<AprovisionamientoValidacion> aprovisionamientoValidaciones, HSSFSheet hoja1) {
    for (AprovisionamientoValidacion aprovisionamientoValidacion : aprovisionamientoValidaciones) {
      if (aprovisionamientoValidacion.getClasificacion().equals("Deltas")) {
        if (aprovisionamientoValidacion.getDescripcion().contains("Producto")) {
          hoja1.getRow(18).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Estados del contrato")) {
          hoja1.getRow(19).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Principal en impago")) {
          hoja1.getRow(20).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Principal pendiente de vencer")) {
          hoja1.getRow(21).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Deuda en impago")) {
          hoja1.getRow(22).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Clasificación contable")) {
          hoja1.getRow(23).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Vigentes")) {
          hoja1.getRow(25).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Tipo de documento")) {
          hoja1.getRow(26).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Tipo de intervencion")) {
          hoja1.getRow(27).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Vigentes ")) {
          hoja1.getRow(29).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Referencia catastral")) {
          hoja1.getRow(30).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Tipo de activo")) {
          hoja1.getRow(31).getCell(1).setCellValue("ko");
        } else if (aprovisionamientoValidacion.getDescripcion().contains("Importe tasación")) {
          hoja1.getRow(33).getCell(1).setCellValue("ko");
        }
      } else if (aprovisionamientoValidacion.getTipoError().equals("Carga con aviso")) {
        switch (aprovisionamientoValidacion.getNivel()) {
          case "Contrato":
            hoja1.getRow(35).getCell(1).setCellValue(aprovisionamientoValidacion.getCantidadIntegridad());
            break;
          case "Interviniente":
            hoja1.getRow(36).getCell(1).setCellValue(aprovisionamientoValidacion.getCantidadIntegridad());
            break;
          case "Liberación garantías":
            hoja1.getRow(37).getCell(1).setCellValue(aprovisionamientoValidacion.getCantidadIntegridad());
            break;
        }
      } else if (aprovisionamientoValidacion.getClasificacion().equals("Integridad")) {
        switch (aprovisionamientoValidacion.getDescripcion()) {
          case "Relacion Interviniente-ContratoInterviniente/ContratoInterviniente-Interviniente sin integridad":
            hoja1.getRow(14).getCell(1).setCellValue(aprovisionamientoValidacion.getCantidadIntegridad());
            break;
          case "Relacion Bien-ContratoBien/ContratoBien-Bien sin integridad":
            hoja1.getRow(15).getCell(1).setCellValue(aprovisionamientoValidacion.getCantidadIntegridad());
            break;
        }
      } else if (aprovisionamientoValidacion.getClasificacion().equals("Otras validaciones")) {
        switch (aprovisionamientoValidacion.getNivel()) {
          case "Tasaciones con importe 0":
            hoja1.getRow(39).getCell(1).setCellValue(aprovisionamientoValidacion.getCantidadIntegridad());
            break;
          case "Fichero Contacto":
            hoja1.getRow(40).getCell(1).setCellValue(aprovisionamientoValidacion.getCantidadIntegridad());
            break;
          case "Fichero Direccion":
            hoja1.getRow(41).getCell(1).setCellValue(aprovisionamientoValidacion.getCantidadIntegridad());
            break;
          case "Fichero Movimiento":
            hoja1.getRow(42).getCell(1).setCellValue(aprovisionamientoValidacion.getCantidadIntegridad());
            break;
          case "Fichero Contrato":
            hoja1.getRow(43).getCell(1).setCellValue(aprovisionamientoValidacion.getCantidadIntegridad());
            break;
        }
      }
    }
  }

  private void comprobarCritico(List<AprovisionamientoValidacion> aprovisionamientoValidaciones, Cartera cartera) throws Exception {
    String error = "";
    for (AprovisionamientoValidacion aprovisionamientoValidacion : aprovisionamientoValidaciones) {
      if (aprovisionamientoValidacion.getTipoError().equals("Crítico")) {
        error = error.concat(aprovisionamientoValidacion.getDescripcion()).concat(",");
      }
    }
    if (!error.equals("")) {
      throw new Exception("Error en validaciones de carga para cartera " + cartera.getNombre() + ": " + error);
    }
  }
}
