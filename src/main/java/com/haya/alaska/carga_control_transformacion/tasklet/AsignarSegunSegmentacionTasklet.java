package com.haya.alaska.carga_control_transformacion.tasklet;

import com.haya.alaska.carga_control_transformacion.services.SegmentacionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@StepScope
public class AsignarSegunSegmentacionTasklet implements Tasklet {


  @Autowired
  private SegmentacionService segmentacionService;

  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

    segmentacionService.asignarSegmentacion();

    log.info("AsignarSegmentacionTasklet");

    return RepeatStatus.FINISHED;
  }

}
