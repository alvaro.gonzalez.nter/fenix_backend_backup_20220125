package com.haya.alaska.carga_control_transformacion.domain;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Slf4j
public class FileUtils {

  private File file;

  public static final Optional<String> getFilename(String pathFolder, String patternName) {
    Pattern pattern = Pattern.compile(patternName);
    File folder = new File(pathFolder);
    return Arrays.stream(folder.listFiles())
      .map(file -> file.getName())
      .filter(pattern.asPredicate())
      .findFirst();
  }

  @SneakyThrows
  public static final boolean deleteFiles(String pathFolder, List<String> filenames) {
    log.info("Eliminación de los ficheros csv de la carga");
    for (String parttern : filenames) {
      Optional<String> optional = getFilename(pathFolder, parttern);
      if (optional.isPresent()) Files.delete(Paths.get(pathFolder + optional.get()));
      log.info("Eliminado el fichero -> " + optional.get());
    }
    Files.deleteIfExists(Paths.get(pathFolder + "ERRORES.csv"));
    log.info("Eliminado el fichero -> " + "ERRORES.csv");

    return true;
  }

  public void getZip(MultipartFile multipartFile, String pathFolderToWrite, String idCarga) throws IOException {
    byte[] bytes = multipartFile.getBytes();
    Path path = Paths.get(pathFolderToWrite + idCarga + "/" + multipartFile.getOriginalFilename());
    log.info(path.toString());
    Files.write(path, bytes);
    this.file = path.toFile();
  }

  @SneakyThrows
  public List<File> unzip(String unzipsFolder) {
    /* valida si existe el directorio */
    List<File> files = new ArrayList<>();
    if (file.exists()) {
      log.info("Descomprimiendo.....");
      try {
        File folder = new File(unzipsFolder);
        if (!folder.exists()) folder.mkdir();

        /* crea un buffer temporal para el archivo que se va descomprimir */
        ZipInputStream zis = new ZipInputStream(new FileInputStream(file));

        ZipEntry salida;
        /* recorre el buffer extrayendo uno a uno cada archivo.zip y creándolos de nuevo en su archivo original */
        while (null != (salida = zis.getNextEntry())) {
          log.info("Nombre del Archivo: " + salida.getName());
          FileOutputStream fos = new FileOutputStream(unzipsFolder + salida.getName());
          files.add(new File(unzipsFolder + salida.getName()));
          int leer;
          byte[] buffer = new byte[1024];
          while (0 < (leer = zis.read(buffer))) {
            fos.write(buffer, 0, leer);
          }
          fos.close();
          zis.closeEntry();
        }
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
      log.info("Directorio de salida: " + unzipsFolder);
    } else {
      log.error(
        String.format(
          "No se encontró la rura (%s) donde descomprimir el paquete (%s)",
          unzipsFolder, file.getCanonicalFile()));
    }
    return files;
  }
}
