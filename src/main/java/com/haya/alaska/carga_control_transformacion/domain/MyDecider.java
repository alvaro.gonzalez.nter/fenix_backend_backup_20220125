package com.haya.alaska.carga_control_transformacion.domain;


import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.espejo.bien.infrastructure.repository.BienEspejoRepository;
import com.haya.alaska.espejo.carga.domain.CargaEspejo;
import com.haya.alaska.espejo.carga.infrastructure.repository.CargaEspejoRepository;
import com.haya.alaska.espejo.contrato_interviniente.domain.ContratoIntervinienteEspejo;
import com.haya.alaska.espejo.contrato_interviniente.infrastructure.repository.ContratoIntervinienteEspejoRepository;
import com.haya.alaska.espejo.dato_contacto.domain.DatoContactoEspejo;
import com.haya.alaska.espejo.dato_contacto.infrastructure.repository.DatoContactoEspejoRepository;
import com.haya.alaska.espejo.direccion.domain.DireccionEspejo;
import com.haya.alaska.espejo.direccion.infrastructure.repository.DireccionEspejoRepository;
import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import com.haya.alaska.espejo.interviniente.infrastructure.repository.IntervinienteEspejoRepository;
import com.haya.alaska.espejo.tasacion.domain.TasacionEspejo;
import com.haya.alaska.espejo.tasacion.infrastructure.repository.TasacionEspejoRepository;
import com.haya.alaska.espejo.valoracion.domain.ValoracionEspejo;
import com.haya.alaska.espejo.valoracion.infrastructure.repository.ValoracionEspejoRepository;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import com.haya.alaska.tipo_carga.infrastructure.repository.TipoCargaRepository;
import lombok.SneakyThrows;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@EnableBatchProcessing
//@Bean
@JobScope
//@Scope(value="prototype", proxyMode= ScopedProxyMode.TARGET_CLASS)
@Component
public class MyDecider implements JobExecutionDecider {

  @Autowired
  CargaEspejoRepository cargaEspejoRepository;

  @Autowired
  BienEspejoRepository bienEspejoRepository;

  @Autowired
  DatoContactoEspejoRepository datoContactoEspejoRepository;

  @Autowired
  DireccionEspejoRepository direccionEspejoRepository;

  @Autowired
  IntervinienteEspejoRepository intervinienteEspejoRepository;

  @Autowired
  TasacionEspejoRepository tasacionEspejoRepository;

  @Autowired
  ValoracionEspejoRepository valoracionEspejoRepository;

  @Autowired
  ContratoIntervinienteEspejoRepository contratoIntervinienteEspejoRepository;

  @Autowired
  TipoCargaRepository tipoCargaRepository;

  @Value("${service.warehouse.csvs}")
  private String csvsFolder;

  @Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  @Value("#{jobParameters['multicartera']}")
  private String multicartera;

  public static <T> Predicate<T> distinctByKey(
    Function<? super T, ?> keyExtractor) {

    Map<Object, Boolean> seen = new ConcurrentHashMap<>();
    return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
  }

  private static <T> Predicate<T> distinctByKeys(Function<? super T, ?>... keyExtractors) {
    final Map<List<?>, Boolean> seen = new ConcurrentHashMap<>();

    return t ->
    {
      final List<?> keys = Arrays.stream(keyExtractors)
        .map(ke -> ke.apply(t))
        .collect(Collectors.toList());

      return seen.putIfAbsent(keys, Boolean.TRUE) == null;
    };
  }

  @SneakyThrows
  @Transactional
  public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
    String status;
    boolean valido = false;
    String cartera = stepExecution.getJobParameters().getString("defaultCartera");
    String manual = stepExecution.getJobParameters().getString("manual");


    if (manual == "true") {

      List<BienEspejo> bienes = bienEspejoRepository.findAll();
      List<DatoContactoEspejo> datosContacto = datoContactoEspejoRepository.findAll();
      List<DireccionEspejo> direcciones = direccionEspejoRepository.findAll();
      List<IntervinienteEspejo> intervinientes = intervinienteEspejoRepository.findAll();
      List<ContratoIntervinienteEspejo> contratosInterviniente = contratoIntervinienteEspejoRepository.findAll();
      List<TasacionEspejo> tasaciones = tasacionEspejoRepository.findAll();
      List<ValoracionEspejo> valoraciones = valoracionEspejoRepository.findAll();
      List<CargaEspejo> cargas = cargaEspejoRepository.findAll();
      //VALIDACIONES BIEN en BienEspejoFieldSetMapper

      //Tasación: Casuística 1
      Set<TasacionEspejo> tasacionesDistintas = tasaciones.stream()
        .filter(distinctByKeys(TasacionEspejo::getBien, TasacionEspejo::getFecha, TasacionEspejo::getImporte, TasacionEspejo::getTasadora))
        .collect(Collectors.toSet());

      tasaciones = tasaciones.stream()
        .map(d -> {
          d.setValido(tasacionesDistintas.contains(d));
          return d;
        })
        .collect(Collectors.toList());

      //Tasación: Casuística 2
      tasaciones = tasaciones.stream()
        .map(t -> {
          t.setValido(((t.getImporte() != null && t.getImporte() != 0) || (t.getFecha() != null)) && (t.getImporte() != null && t.getImporte() != 0) && t.getValido() != false);
          return t;
        })
        .collect(Collectors.toList());

      //Tasación: Casuística 3
      //posiblemente falte añadir el ind_modificado y des_modificado
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
      String dateInString = "1900-01-01";

      try {
        Date date = formatter.parse(dateInString);
        tasaciones = tasaciones.stream()
          .map(t -> {
            t.setModificado(t.getFecha() == null);
            t.setFecha((t.getFecha() == null) ? date : t.getFecha());
            t.setDescripcionMod(t.getModificado() != null && t.getModificado() == true ? "fecha modificada 01-01-1900" : null);
            return t;
          })
          .collect(Collectors.toList());
      } catch (ParseException e) {
        e.printStackTrace();
      }

      //Valoración: Casuística 1
      Set<ValoracionEspejo> valoracionesDistintos = valoraciones.stream()
        .filter(distinctByKeys(ValoracionEspejo::getBien, ValoracionEspejo::getFecha, ValoracionEspejo::getValorador, ValoracionEspejo::getImporte))
        .collect(Collectors.toSet());

      valoraciones = valoraciones.stream()
        .map(v -> {
          v.setValido(valoracionesDistintos.contains(v));
          return v;
        })
        .collect(Collectors.toList());

      //Valoración: Casuística 2
      valoraciones = valoraciones.stream()
        .map(v -> {
          v.setValido(((v.getImporte() != null && v.getImporte() != 0) || (v.getFecha() != null)) && (v.getImporte() != null && v.getImporte() != 0) && v.getValido() != false);
          return v;
        })
        .collect(Collectors.toList());

      //Valoración: Casuística 3
      try {
        Date date = formatter.parse(dateInString);
        valoraciones = valoraciones.stream()
          .map(v -> {
            v.setModificado(v.getFecha() == null);
            v.setFecha((v.getFecha() == null) ? date : v.getFecha());
            v.setDescripcionMod(v.getModificado() != null && v.getModificado() == true ? "fecha modificada 01-01-1900" : null);
            return v;
          })
          .collect(Collectors.toList());
      } catch (ParseException e) {
        e.printStackTrace();
      }

      //Cargas: Casuística 1
      //posiblemente falte añadir el ind_modificado y des_modificado
      TipoCarga tipoCargaTres = tipoCargaRepository.findByCodigo("3").orElse(null);
      bienes = bienes.stream()
        .map(b -> {
          Set cargasDistintas = b.getCargas().stream()
            .filter(distinctByKeys(CargaEspejo::getRango, CargaEspejo::getTipoCarga)).collect(Collectors.toSet());

          b.setCargas(b.getCargas().stream()
            .map(c -> {
              c.setTipoCarga(((Set<CargaEspejo>) cargasDistintas.stream().collect(Collectors.toSet()))
                .contains(c) ? c.getTipoCarga() : tipoCargaTres);
              c.setModificado(!((Set<CargaEspejo>) cargasDistintas.stream().collect(Collectors.toSet()))
                .contains(c));
              c.setDescripcionMod(c.getModificado() != null && c.getModificado() == true ? "Tipo de carga modificado por un 3" : null);
              return c;
            }).collect(Collectors.toSet()));
          return b;
        })
        .collect(Collectors.toList());

      //Interviniente: Validación casuística 1
      HashMap<String, List<IntervinienteEspejo>> intervGroup = (HashMap<String, List<IntervinienteEspejo>>) intervinientes.stream()
        .collect(Collectors.groupingBy(IntervinienteEspejo::getNumeroDocumento));

      HashMap<IntervinienteEspejo, Set<IntervinienteEspejo>> intervToLevel = (HashMap<IntervinienteEspejo, Set<IntervinienteEspejo>>) intervGroup.entrySet().stream()
        .filter(i -> i.getValue().size() > 1)
        .map(e -> {
          List<Map.Entry<IntervinienteEspejo, Long>> intervinienteCount = e.getValue().stream().map(i -> {
            Map.Entry<IntervinienteEspejo, Long> intervContrato = Map.entry(i, i.getContratos().stream().count());
            return intervContrato;
          }).collect(Collectors.toList());

          IntervinienteEspejo resultInter = intervinienteCount.stream().max(Map.Entry.comparingByValue()).get().getKey();
          return Map.entry(resultInter, intervinienteCount.stream().filter(i -> i.getKey() != resultInter).map(i -> i.getKey()).collect(Collectors.toSet()));
        }).collect(Collectors.toMap(
          entry -> entry.getKey(),
          entry -> entry.getValue()));

      HashMap<String, IntervinienteEspejo> intervContratoReplaceMap = (HashMap<String, IntervinienteEspejo>) intervToLevel.entrySet().stream()
        .map(i -> Map.entry(i.getKey().getNumeroDocumento(), i.getKey()))
        .collect(Collectors.toMap(
          entry -> entry.getKey(),
          entry -> entry.getValue()));

      //Se reemplazan los intervinientes del contrato por los nivelados
      contratosInterviniente = contratosInterviniente.stream()
        .map(c -> {
          c.setInterviniente(intervContratoReplaceMap.containsKey(c.getInterviniente().getNumeroDocumento()) ?
            intervContratoReplaceMap.get(c.getInterviniente().getNumeroDocumento()) : c.getInterviniente());
          return c;
        }).collect(Collectors.toList());

      contratoIntervinienteEspejoRepository.saveAll(contratosInterviniente);

      //Intervinientes que sobran después del nivelado
      Set<IntervinienteEspejo> intervinientesToDelete = intervToLevel.entrySet().stream()
        .flatMap(i -> i.getValue().stream())
        .collect(Collectors.toSet());

      //Se marcan como modificados los intervinientes nivelados
      intervinientes = intervinientes.stream()
        .map(c -> {
          c.setModificado(intervinientesToDelete.contains(c));
          c.setValido((c.getValido() == null || c.getValido() != false) && c.getModificado() != true);
          c.setDescripcionMod(c.getModificado() != null && c.getModificado() == true ? "ID_INTERVINIENTE nivelado" : null);
          return c;
        }).collect(Collectors.toList());

      //Interviniente: Validación de datos de localización casuística 2
      Set<DireccionEspejo> direccionesDistintas = direcciones.stream()
        .filter(distinctByKeys(DireccionEspejo::getNombre, DireccionEspejo::getLocalidad, DireccionEspejo::getCodigoPostal, DireccionEspejo::getProvincia, DireccionEspejo::getInterviniente))
        .collect(Collectors.toSet());

      direcciones = direcciones.stream()
        .map(d -> {
          d.setValido(direccionesDistintas.contains(d));
          return d;
        })
        .collect(Collectors.toList());

      //Interviniente: Validación de datos de contacto casuística 1
      datosContacto = datosContacto.stream()
        .map(c -> {
          c.setValido(c.getOrden() <= 2);
          return c;
        }).collect(Collectors.toList());

      //Interviniente: Validación de datos de contacto casuística 1
      Set<DatoContactoEspejo> datosContactoDistintos = datosContacto.stream()
        .filter(distinctByKeys(DatoContactoEspejo::getInterviniente, DatoContactoEspejo::getEmail, DatoContactoEspejo::getFijo,
          DatoContactoEspejo::getMovil, DatoContactoEspejo::getOrden))
        .collect(Collectors.toSet());

      datosContacto = datosContacto.stream()
        .map(c -> {
          c.setValido(datosContactoDistintos.contains(c) && c.getValido() != false);
          return c;
        }).collect(Collectors.toList());

      //Se hace un set de los campos IND_VALIDO a TRUE
      intervinientes = intervinientes.stream()
        .map(c -> {
          c.setValido(c.getValido() == null || c.getValido());
          return c;
        }).collect(Collectors.toList());

      direcciones = direcciones.stream()
        .map(c -> {
          c.setValido(c.getValido() == null || c.getValido());
          return c;
        }).collect(Collectors.toList());

      datosContacto = datosContacto.stream()
        .map(c -> {
          c.setValido(c.getValido() == null || c.getValido());
          return c;
        }).collect(Collectors.toList());

      valoraciones = valoraciones.stream()
        .map(c -> {
          c.setValido(c.getValido() == null || c.getValido());
          return c;
        }).collect(Collectors.toList());

      tasaciones = tasaciones.stream()
        .map(c -> {
          c.setValido(c.getValido() == null || c.getValido());
          return c;
        }).collect(Collectors.toList());

      cargas = cargas.stream()
        .map(c -> {
          c.setValido(c.getValido() == null || c.getValido());
          return c;
        }).collect(Collectors.toList());

      bienes = bienes.stream()
        .map(c -> {
          c.setValido(c.getValido() == null || c.getValido());
          return c;
        }).collect(Collectors.toList());

      //ACTUALIZAR LAS TABLAS ESPEJO
      bienEspejoRepository.saveAll(bienes);
      intervinienteEspejoRepository.saveAll(intervinientes);
      direccionEspejoRepository.saveAll(direcciones);
      datoContactoEspejoRepository.saveAll(datosContacto);
      valoracionEspejoRepository.saveAll(valoraciones);
      tasacionEspejoRepository.saveAll(tasaciones);
      cargaEspejoRepository.saveAll(cargas);

      GenerarExcelRA generarExcelRA = new GenerarExcelRA();
      // generarExcelRA.generarExcel(csvsFolder, bienEspejoRepository.findCliente(carteraId), bienes, intervinientes, direcciones, datosContacto, valoraciones, tasaciones, cargas);

      String ruta = "./src/main/resources/csvs/" + carteraId + "/";
      File fichero = new File(ruta + "OKEY_CHECKS_PENTAHO_" + carteraId + "_" + multicartera + ".txt");
      fichero.createNewFile();
    }
    status = "COMPLETED";
    return new FlowExecutionStatus(status);
  }
}
