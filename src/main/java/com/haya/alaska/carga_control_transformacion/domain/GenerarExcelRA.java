package com.haya.alaska.carga_control_transformacion.domain;

import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.espejo.carga.domain.CargaEspejo;
import com.haya.alaska.espejo.dato_contacto.domain.DatoContactoEspejo;
import com.haya.alaska.espejo.direccion.domain.DireccionEspejo;
import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import com.haya.alaska.espejo.tasacion.domain.TasacionEspejo;
import com.haya.alaska.espejo.valoracion.domain.ValoracionEspejo;
import com.opencsv.CSVReader;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Getter
@Setter
public class GenerarExcelRA {

  public void generarExcel(String csvsFolder, String carteraId, List<BienEspejo> bienes, List<IntervinienteEspejo> intervinientes, List<DireccionEspejo> direcciones, List<DatoContactoEspejo> datosContacto, List<ValoracionEspejo> valoraciones, List<TasacionEspejo> tasaciones, List<CargaEspejo> cargas) throws IOException {
    Integer i = 1;
    int importe = 0;
    int fechaNul = 0;
    int duplicado = 0;
    int principal = 0;
    int total = 0;

    Date fecha = new Date();
    SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
    Calendar fechaViejo = Calendar.getInstance();
    fechaViejo.setTime(fecha);
    fechaViejo.add(Calendar.MONTH, -2);
    String archcsvinterviniente = csvsFolder + "Controles_carga_para_revisión_Informe_Resumen_RA-" + objSDF.format(fechaViejo.getTime()) + ".xls";
    File ficheroViejo = new File(archcsvinterviniente);
    if (ficheroViejo.exists()) {
      ficheroViejo.delete();
    }
    archcsvinterviniente = csvsFolder + "Controles_carga_para_revisión_Informe_Resumen_RA-" + objSDF.format(fecha) + ".xls";
    HSSFWorkbook objWB = new HSSFWorkbook();

    HSSFSheet hoja1 = objWB.createSheet("Informe_Resumen");
    HSSFSheet hoja2 = objWB.createSheet("Bienes_RA");
    HSSFSheet hoja3 = objWB.createSheet("Tasación_RA");
    HSSFSheet hoja4 = objWB.createSheet("Valoración_RA");
    HSSFSheet hoja5 = objWB.createSheet("Carga_RA");
    HSSFSheet hoja6 = objWB.createSheet("Interviniente_RA");
    HSSFSheet hoja7 = objWB.createSheet("Direccion_RA");
    HSSFSheet hoja8 = objWB.createSheet("Contacto_RA");

    CellStyle cellStyleCabecera = objWB.createCellStyle();
    Font cellFont = objWB.createFont();
    cellFont.setBold(true);
    cellFont.setItalic(true);
    cellStyleCabecera.setFont(cellFont);

    CellStyle cellStyleSub = objWB.createCellStyle();
    Font cellFontSub = objWB.createFont();
    cellFontSub.setUnderline(HSSFFont.U_SINGLE);
    cellStyleSub.setFont(cellFontSub);

    CellStyle cellStyleSub1 = objWB.createCellStyle();
    Font cellFontSub1 = objWB.createFont();
    cellFontSub1.setBold(true);
    cellFontSub1.setUnderline(HSSFFont.U_SINGLE);
    cellStyleSub1.setFont(cellFontSub1);

    HSSFRow fila = hoja1.createRow((short) 1);
    HSSFCell celda = fila.createCell((short) 0);
    celda.setCellValue("Fecha de carga ");
    celda = fila.createCell((short) 1);
    celda.setCellValue(objSDF.format(fecha));

    HSSFRow fila1 = hoja1.createRow((short) 2);
    celda = fila1.createCell((short) 0);
    celda.setCellValue("Cartera ");
    celda = fila1.createCell((short) 1);
    celda.setCellValue(carteraId);

    HSSFRow fila2 = hoja1.createRow((short) 3);
    celda = fila2.createCell((short) 0);
    celda.setCellValue("Cliente ");
    celda = fila2.createCell((short) 1);
    celda.setCellValue(carteraId);

    HSSFRow fila3 = hoja1.createRow((short) 5);
    celda = fila3.createCell((short) 0);
    celda.setCellStyle(cellStyleCabecera);
    celda.setCellValue("Totales ");

    HSSFRow fila4 = hoja1.createRow((short) 6);
    celda = fila4.createCell((short) 0);
    celda.setCellValue("Número de registros origen ");
    celda = fila4.createCell((short) 1);
    celda.setCellValue(bienes.size() + intervinientes.size() + direcciones.size() + datosContacto.size() + valoraciones.size() + tasaciones.size() + cargas.size());

    HSSFRow fila5 = hoja1.createRow((short) 7);
    celda = fila5.createCell((short) 0);
    celda.setCellValue("Número de registros cargados ok ");

    HSSFRow fila6 = hoja1.createRow((short) 8);
    celda = fila6.createCell((short) 0);
    celda.setCellValue("Número de resgitros con incidencia ");

    HSSFRow fila7 = hoja1.createRow((short) 9);
    celda = fila7.createCell((short) 0);
    celda.setCellStyle(cellStyleCabecera);
    celda.setCellValue("Incidencias ");

    HSSFRow fila8 = hoja1.createRow((short) 10);
    celda = fila8.createCell((short) 0);
    celda.setCellStyle(cellStyleSub1);
    celda.setCellValue("Bienes");

    HSSFRow fila9 = hoja1.createRow((short) 11);
    celda = fila9.createCell((short) 0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Datos identificativos ");

    HSSFRow fila10 = hoja1.createRow((short) 12);
    celda = fila10.createCell((short) 0);
    celda.setCellValue("Mismo ID_Bien con datos identificativos distintos ");


    CSVReader reader = null;
    reader = new CSVReader(new FileReader(csvsFolder + "BIEN.csv"));
    String[] nextLine = null;
    ArrayList<String> bien = new ArrayList<>();
    while ((nextLine = reader.readNext()) != null) {
      bien.addAll(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(",")));
    }
    HSSFRow filaBien = hoja2.createRow((short) 0);
    celda = filaBien.createCell((short) 0);
    celda.setCellValue("Incidencia");
    String[] bienFila = bien.get(0).replace('"', ' ').split(";");
    for (int e = 0; e < bienFila.length; e++) {
      celda = filaBien.createCell((short) e + 1);
      celda.setCellValue(bienFila[e]);
    }

    principal = 0;
    for (int j = 1; j < bien.size(); j++) {
      for (int p = 1; p < bien.size(); p++) {
        if (Arrays.asList(bien.get(j).replace(",", ";").split(";")).get(0).equals(Arrays.asList(bien.get(p).replace(",", ";").split(";")).get(0))) {
          principal++;
          if (principal > 1) {
            HSSFRow filaBien1 = hoja2.createRow(Short.parseShort(i.toString()));
            celda = filaBien1.createCell((short) 0);
            celda.setCellValue("Mismo ID_Bien con datos identificativos distintos ");
            for (int y = 0; y < Arrays.asList(bien.get(p).replace(",", ";").split(";")).size(); y++) {
              celda = filaBien1.createCell((short) y + 1);
              celda.setCellValue(Arrays.asList(bien.get(j).replace('"', ' ').replace(",", ";").split(";")).get(y));
            }
            duplicado++;
            i++;
            HSSFRow filaBien2 = hoja2.createRow(Short.parseShort(i.toString()));
            celda = filaBien2.createCell((short) 0);
            celda.setCellValue("Mismo ID_Bien con datos identificativos distintos ");
            for (int y = 0; y < Arrays.asList(bien.get(p).replace(",", ";").split(";")).size(); y++) {
              celda = filaBien2.createCell((short) y + 1);
              celda.setCellValue(Arrays.asList(bien.get(p).replace('"', ' ').replace(",", ";").split(";")).get(y));
            }
            duplicado++;
            i++;
          }
        }
      }
      bien.remove(j);
      principal = 0;
    }
    celda = fila10.createCell((short) 1);
    celda.setCellValue(duplicado);

    total = total + duplicado;
    i = 1;
    importe = 0;
    fechaNul = 0;
    duplicado = 0;
    ArrayList<String> tasacion = new ArrayList<>();
    reader = new CSVReader(new FileReader(csvsFolder + "TASACION.csv"), ';');
    while ((nextLine = reader.readNext()) != null) {
      HSSFRow filaTasacion1 = hoja3.createRow(Short.parseShort(i.toString()));
      tasacion.addAll(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(",")));
      if (Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).get(3).length() <= 1) {
        celda = filaTasacion1.createCell((short) 0);
        celda.setCellValue("Importe de tasación nulo ");
        for (int y = 0; y < Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).size(); y++) {
          celda = filaTasacion1.createCell((short) y + 1);
          celda.setCellValue(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).get(y));
        }
        importe++;
        i++;
      }
      filaTasacion1 = hoja3.createRow(Short.parseShort(i.toString()));
      if (Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).get(4).length() <= 1) {
        celda = filaTasacion1.createCell((short) 0);
        celda.setCellValue("Fecha de tasación nula ");
        for (int y = 0; y < Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).size(); y++) {
          celda = filaTasacion1.createCell((short) y + 1);
          celda.setCellValue(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).get(y));
        }
        fechaNul++;
        i++;
      }
    }
    principal = 0;
    for (int j = 1; j < tasacion.size(); j++) {
      for (int p = 1; p < tasacion.size(); p++) {
        if (tasacion.get(j).equals(tasacion.get(p))) {
          principal++;
          if (principal > 1) {
            HSSFRow filaTasacion1 = hoja3.createRow(Short.parseShort(i.toString()));
            celda = filaTasacion1.createCell((short) 0);
            celda.setCellValue("Tasaciones duplicadas");
            for (int y = 0; y < Arrays.asList(tasacion.get(p).replace(",", ";").split(";")).size(); y++) {
              celda = filaTasacion1.createCell((short) y + 1);
              celda.setCellValue(Arrays.asList(tasacion.get(p).replace(",", ";").split(";")).get(y));
            }
            duplicado++;
            i++;
          }
        }
      }
      tasacion.remove(j);
      principal = 0;
    }

    HSSFRow filaTasacion = hoja3.createRow((short) 0);
    celda = filaTasacion.createCell((short) 0);
    celda.setCellValue("Incidencia");
    celda = filaTasacion.createCell((short) 1);
    celda.setCellValue("id_bien");
    celda = filaTasacion.createCell((short) 2);
    celda.setCellValue("tasadora");
    celda = filaTasacion.createCell((short) 3);
    celda.setCellValue("tipo_tasacion");
    celda = filaTasacion.createCell((short) 4);
    celda.setCellValue("importe_tasacion");
    celda = filaTasacion.createCell((short) 5);
    celda.setCellValue("fecha_tasacion");
    celda = filaTasacion.createCell((short) 6);
    celda.setCellValue("liquidez");


    HSSFRow fila11 = hoja1.createRow((short) 13);
    celda = fila11.createCell((short) 0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Tasación ");

    HSSFRow fila12 = hoja1.createRow((short) 14);
    celda = fila12.createCell((short) 0);
    celda.setCellValue("Tasaciones duplicadas");
    celda = fila12.createCell((short) 1);
    celda.setCellValue(duplicado);

    HSSFRow fila13 = hoja1.createRow((short) 15);
    celda = fila13.createCell((short) 0);
    celda.setCellValue("Importe de tasación nulo");
    celda = fila13.createCell((short) 1);
    celda.setCellValue(importe);

    HSSFRow fila14 = hoja1.createRow((short) 16);
    celda = fila14.createCell((short) 0);
    celda.setCellValue("Fecha de tasación nulo");
    celda = fila14.createCell((short) 1);
    celda.setCellValue(fechaNul);

    total = total + duplicado;
    i = 1;
    importe = 0;
    fechaNul = 0;
    duplicado = 0;
    ArrayList<String> valoracion = new ArrayList<>();
    reader = new CSVReader(new FileReader(csvsFolder + "VALORACION.csv"), ';');
    while ((nextLine = reader.readNext()) != null) {
      HSSFRow filafilaValidacion1 = hoja4.createRow(Short.parseShort(i.toString()));
      valoracion.addAll(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(",")));
      if (Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).get(3).length() <= 1) {
        celda = filafilaValidacion1.createCell((short) 0);
        celda.setCellValue("Importe de tasación nulo ");
        for (int y = 0; y < Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).size(); y++) {
          celda = filafilaValidacion1.createCell((short) y + 1);
          celda.setCellValue(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).get(y));
        }
        importe++;
        i++;
      }
      filafilaValidacion1 = hoja4.createRow(Short.parseShort(i.toString()));
      if (Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).get(4).length() <= 1) {
        celda = filafilaValidacion1.createCell((short) 0);
        celda.setCellValue("Fecha de tasación nula ");
        for (int y = 0; y < Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).size(); y++) {
          celda = filafilaValidacion1.createCell((short) y + 1);
          celda.setCellValue(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(";")).get(y));
        }
        fechaNul++;
        i++;
      }
    }

    principal = 0;
    for (int j = 1; j < valoracion.size(); j++) {
      for (int p = 1; p < valoracion.size(); p++) {
        if (valoracion.get(j).equals(valoracion.get(p))) {
          principal++;
          if (principal > 1) {
            HSSFRow filaTasacion1 = hoja3.createRow(Short.parseShort(i.toString()));
            celda = filaTasacion1.createCell((short) 0);
            celda.setCellValue("Tasaciones duplicadas");
            for (int y = 0; y < Arrays.asList(valoracion.get(p).replace(",", ";").split(";")).size(); y++) {
              celda = filaTasacion1.createCell((short) y + 1);
              celda.setCellValue(Arrays.asList(valoracion.get(p).replace(",", ";").split(";")).get(y));
            }
            duplicado++;
            i++;
          }
        }
      }
      valoracion.remove(j);
      principal = 0;
    }

    HSSFRow filaValidacion = hoja4.createRow((short) 0);
    celda = filaValidacion.createCell((short) 0);
    celda.setCellValue("Incidencia");
    celda = filaValidacion.createCell((short) 1);
    celda.setCellValue("id_bien");
    celda = filaValidacion.createCell((short) 2);
    celda.setCellValue("valorador");
    celda = filaValidacion.createCell((short) 3);
    celda.setCellValue("tipo_valoracion");
    celda = filaValidacion.createCell((short) 4);
    celda.setCellValue("importe_valoracion");
    celda = filaValidacion.createCell((short) 5);
    celda.setCellValue("fecha_valoracion");
    celda = filaValidacion.createCell((short) 6);
    celda.setCellValue("liquidez");

    HSSFRow fila15 = hoja1.createRow((short) 17);
    celda = fila15.createCell((short) 0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Valoración ");

    HSSFRow fila16 = hoja1.createRow((short) 18);
    celda = fila16.createCell((short) 0);
    celda.setCellValue("Valoracones duplicadas");
    celda = fila16.createCell((short) 1);
    celda.setCellValue(duplicado);

    HSSFRow fila17 = hoja1.createRow((short) 19);
    celda = fila17.createCell((short) 0);
    celda.setCellValue("Importe de valoración nulo");
    celda = fila17.createCell((short) 1);
    celda.setCellValue(importe);

    HSSFRow fila18 = hoja1.createRow((short) 20);
    celda = fila18.createCell((short) 0);
    celda.setCellValue("Fecha de valoración nulo");
    celda = fila18.createCell((short) 1);
    celda.setCellValue(fechaNul);

    i = 1;
    total = total + duplicado;
    duplicado = 0;
    ArrayList<String> carga = new ArrayList();
    reader = new CSVReader(new FileReader(csvsFolder + "CARGA.csv"), ';');
    while ((nextLine = reader.readNext()) != null) {
      carga.addAll(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(",")));
    }

    HSSFRow filaCarga = hoja5.createRow((short) 0);
    celda = filaCarga.createCell((short) 0);
    celda.setCellValue("Incidencia");
    String[] cargaFila = carga.get(0).replace('"', ' ').split(";");
    for (int e = 0; e < cargaFila.length; e++) {
      celda = filaCarga.createCell((short) e + 1);
      celda.setCellValue(cargaFila[e]);
    }

    principal = 0;
    for (int j = 1; j < carga.size(); j++) {
      for (int p = 1; p < carga.size(); p++) {
        if (carga.get(j).equals(carga.get(p))) {
          principal++;
          if (principal > 1) {
            HSSFRow filaCarga1 = hoja5.createRow(Short.parseShort(i.toString()));
            celda = filaCarga1.createCell((short) 0);
            celda.setCellValue("Cargas duplicadas");
            for (int y = 0; y < Arrays.asList(carga.get(p).replace(",", ";").split(";")).size(); y++) {
              celda = filaCarga1.createCell((short) y + 1);
              celda.setCellValue(Arrays.asList(carga.get(p).replace(",", ";").split(";")).get(y));
            }
            i++;
            HSSFRow filaCarga2 = hoja5.createRow(Short.parseShort(i.toString()));
            celda = filaCarga2.createCell((short) 0);
            celda.setCellValue("Cargas duplicadas");
            for (int y = 0; y < Arrays.asList(carga.get(p).replace(",", ";").split(";")).size(); y++) {
              celda = filaCarga2.createCell((short) y + 1);
              if (y != 2) {
                celda.setCellValue(Arrays.asList(carga.get(p).replace(",", ";").split(";")).get(y));
              } else {
                celda.setCellValue("3");
              }
            }
            duplicado++;
            i++;
          }
        }
      }
      carga.remove(j);
      principal = 0;
    }

    HSSFRow fila19 = hoja1.createRow((short) 21);
    celda = fila19.createCell((short) 0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Carga ");

    HSSFRow fila20 = hoja1.createRow((short) 22);
    celda = fila20.createCell((short) 0);
    celda.setCellValue("Cargas duplicadas");
    celda = fila20.createCell((short) 1);
    celda.setCellValue(duplicado);


    ArrayList<String> interviniente = new ArrayList();
    reader = new CSVReader(new FileReader(csvsFolder + "INTERVINIENTE.csv"), ';');
    while ((nextLine = reader.readNext()) != null) {
      interviniente.addAll(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(",")));

    }
    HSSFRow filaIntervinientes = hoja6.createRow((short) 0);
    celda = filaIntervinientes.createCell((short) 0);
    celda.setCellValue("Incidencia");
    String[] intervinientesFila = interviniente.get(0).replace('"', ' ').split(";");
    for (int e = 0; e < intervinientesFila.length; e++) {
      celda = filaIntervinientes.createCell((short) e + 1);
      celda.setCellValue(intervinientesFila[e]);
    }

    i = 1;
    duplicado = 0;
    for (int j = 1; j < carga.size(); j++) {
      for (int p = 1; p < carga.size(); p++) {
        if (Arrays.asList(interviniente.get(j).replace(",", ";").split(";")).get(7).equals(Arrays.asList(interviniente.get(p).replace(",", ";").split(";")).get(7))) {
          if (!Arrays.asList(interviniente.get(j).replace(",", ";").split(";")).get(0).equals(Arrays.asList(interviniente.get(p).replace(",", ";").split(";")).get(0))) {
            HSSFRow filaInterviniente1 = hoja6.createRow(Short.parseShort(i.toString()));
            celda = filaInterviniente1.createCell((short) 0);
            celda.setCellValue("ID_ Interviniente - ID_Documento inconsistentes");
            for (int y = 0; y < Arrays.asList(interviniente.get(p).replace(",", ";").split(";")).size(); y++) {
              celda = filaInterviniente1.createCell((short) y + 1);
              celda.setCellValue(Arrays.asList(interviniente.get(p).replace(",", ";").split(";")).get(y));
            }
            duplicado++;
            i++;
          }
        }
      }
    }

    HSSFRow fila21 = hoja1.createRow((short) 23);
    celda = fila21.createCell((short) 0);
    celda.setCellStyle(cellStyleSub1);
    celda.setCellValue("Interviniente");

    HSSFRow fila22 = hoja1.createRow((short) 24);
    celda = fila22.createCell((short) 0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Datos identificativos ");

    HSSFRow fila23 = hoja1.createRow((short) 25);
    celda = fila23.createCell((short) 0);
    celda.setCellValue("ID_ Interviniente - ID_Documento inconsistentes");
    celda = fila23.createCell((short) 1);
    celda.setCellValue(duplicado);

    ArrayList<String> direccion = new ArrayList();
    reader = new CSVReader(new FileReader(csvsFolder + "DIRECCION.csv"), ';');
    while ((nextLine = reader.readNext()) != null) {
      direccion.addAll(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(",")));

    }
    HSSFRow filadireccion = hoja7.createRow((short) 0);
    celda = filadireccion.createCell((short) 0);
    celda.setCellValue("Incidencia");
    String[] direccionFila = direccion.get(0).replace('"', ' ').split(";");
    for (int e = 0; e < direccionFila.length; e++) {
      celda = filadireccion.createCell((short) e + 1);
      celda.setCellValue(direccionFila[e]);
    }

    i = 1;
    duplicado = 0;
    for (int j = 1; j < carga.size(); j++) {
      for (int p = 1; p < carga.size(); p++) {
        if (Arrays.asList(direccion.get(j).replace(",", ";").split(";")).get(0).equals(Arrays.asList(direccion.get(p).replace(",", ";").split(";")).get(0))) {
          if (!Arrays.asList(direccion.get(j).replace(",", ";").split(";")).get(2).equals(Arrays.asList(direccion.get(p).replace(",", ";").split(";")).get(2)) ||
            !Arrays.asList(direccion.get(j).replace(",", ";").split(";")).get(11).equals(Arrays.asList(direccion.get(p).replace(",", ";").split(";")).get(11)) ||
            !Arrays.asList(direccion.get(j).replace(",", ";").split(";")).get(12).equals(Arrays.asList(direccion.get(p).replace(",", ";").split(";")).get(12)) ||
            !Arrays.asList(direccion.get(j).replace(",", ";").split(";")).get(14).equals(Arrays.asList(direccion.get(p).replace(",", ";").split(";")).get(14))
          ) {
            HSSFRow filadireccion1 = hoja6.createRow(Short.parseShort(i.toString()));
            filadireccion1.createCell((short) 0);
            celda.setCellValue("Mismo ID Inerviniente con datos de localización diferentes");
            for (int y = 0; y < Arrays.asList(direccion.get(p).replace(",", ";").split(";")).size(); y++) {
              celda = filadireccion1.createCell((short) y + 1);
              celda.setCellValue(Arrays.asList(direccion.get(p).replace(",", ";").split(";")).get(y));
            }
            duplicado++;
            i++;
          }
        }
      }
    }
    HSSFRow fila24 = hoja1.createRow((short) 26);
    celda = fila24.createCell((short) 0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Datos de localización ");

    HSSFRow fila25 = hoja1.createRow((short) 27);
    celda = fila25.createCell((short) 0);
    celda.setCellValue("Mismo ID Inerviniente con datos de localización diferentes");
    celda = fila25.createCell((short) 1);
    celda.setCellValue(duplicado);

    ArrayList<String> contacto = new ArrayList();
    reader = new CSVReader(new FileReader(csvsFolder + "CONTACTO.csv"), ';');
    while ((nextLine = reader.readNext()) != null) {
      contacto.addAll(Arrays.asList(Arrays.toString(nextLine).replace("[", "").replace("]", "").replace(",", ";").split(",")));

    }
    HSSFRow filaContacto = hoja8.createRow((short) 0);
    celda = filaContacto.createCell((short) 0);
    celda.setCellValue("Incidencia");
    String[] contactoFila = contacto.get(0).replace('"', ' ').split(";");
    for (int e = 0; e < contactoFila.length; e++) {
      celda = filaContacto.createCell((short) e + 1);
      celda.setCellValue(contactoFila[e]);
    }

    HSSFRow fila26 = hoja1.createRow((short) 28);
    celda = fila26.createCell((short) 0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Datos de contacto ");

    HSSFRow fila27 = hoja1.createRow((short) 29);
    celda = fila27.createCell((short) 0);
    celda.setCellValue("Mismo ID Inerviniente con datos de contacto diferentes");
    celda = fila27.createCell((short) 1);
    celda.setCellValue(duplicado);

    HSSFRow fila28 = hoja1.createRow((short) 30);
    celda = fila28.createCell((short) 0);
    celda.setCellValue("Mismo ID Inerviniente con datos de contacto iguales");
    celda = fila28.createCell((short) 1);
    celda.setCellValue(duplicado);


    celda = fila5.createCell((short) 1);
    celda.setCellValue(bienes.size() + intervinientes.size() + direcciones.size() + datosContacto.size() + valoraciones.size() + tasaciones.size() + cargas.size() - total);

    celda = fila6.createCell((short) 1);
    celda.setCellValue(total);


    reader.close();
    File objFile = new File(archcsvinterviniente);
    FileOutputStream archivoSalida = new FileOutputStream(objFile);
    objWB.write(archivoSalida);
    archivoSalida.close();
  }
}
