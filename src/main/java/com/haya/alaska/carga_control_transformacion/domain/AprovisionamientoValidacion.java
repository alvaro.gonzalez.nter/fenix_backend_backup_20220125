package com.haya.alaska.carga_control_transformacion.domain;

import lombok.Data;

@Data
public class AprovisionamientoValidacion {

  private String tipoError;

  private String clasificacion;

  private String nivel;

  private String descripcion;

  private String idObjeto;

  private int cantidadIntegridad;


  public AprovisionamientoValidacion(String tipoError, String clasificacion, String nivel, String descripcion) {
    this.tipoError = tipoError;
    this.clasificacion = clasificacion;
    this.nivel = nivel;
    this.descripcion = descripcion;
  }

  public AprovisionamientoValidacion(String tipoError, String clasificacion, String nivel, String descripcion, String id) {
    this.tipoError = tipoError;
    this.clasificacion = clasificacion;
    this.nivel = nivel;
    this.descripcion = descripcion;
    this.idObjeto = id;
  }
}
