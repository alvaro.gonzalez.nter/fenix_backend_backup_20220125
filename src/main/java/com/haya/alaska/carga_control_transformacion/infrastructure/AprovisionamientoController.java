package com.haya.alaska.carga_control_transformacion.infrastructure;

import com.haya.alaska.carga_control_transformacion.application.FileInUseCase;
import com.haya.alaska.carga_control_transformacion.services.CalculateContratoRepresentanteService;
import com.haya.alaska.carga_control_transformacion.services.SegmentacionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/api/v1/aprovisionamiento")
public class AprovisionamientoController {

  @Autowired
  private FileInUseCase fileInUseCase;
  @Autowired
  private SegmentacionService segmentacionService;
  @Autowired
  private CalculateContratoRepresentanteService calculateContratoRepresentanteService;

  @PostMapping("zip")
  public ResponseEntity<String> myService(@RequestParam("file") MultipartFile file,
                                          @RequestParam(value = "cartera", defaultValue = "") String carteraIdCarga,
                                          @RequestParam(value = "manual", required = false) boolean manual,
                                          @RequestParam(value = "multicartera", required = false, defaultValue = "false") boolean multicartera) {

    String msg;
    HttpStatus code;
    if (!file.isEmpty()) {
      try {
        fileInUseCase.saveFiles(file, carteraIdCarga, manual, multicartera);
        code = HttpStatus.ACCEPTED;
        msg = "Fichero recibido";
      } catch (FileNotFoundException fnfe) {
        log.error(fnfe.getMessage());
        msg = "Fichero no encontrado";
        code = HttpStatus.EXPECTATION_FAILED;
      } catch (IOException ioe) {
        log.error(ioe.getMessage());
        msg = "No se pudo leer el fichero";
        code = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
      } catch (Exception e) {
        log.error(e.getMessage());
        msg = "SERVER ERROR";
        code = HttpStatus.NOT_IMPLEMENTED;
      }
    } else {
      msg = "Fichero no encontrado";
      code = HttpStatus.EXPECTATION_FAILED;
    }
    log.info(String.format("Response - HttpStatus [%d] => %s", code.value(), msg));
    return new ResponseEntity<>(msg, code);
  }

  @PostMapping("/segmentacion")
  public void segmentacion() {
    segmentacionService.asignarSegmentacion();
  }

  @PostMapping("/generarFicheroComprobaciones/{carteraId}")
  public void generarFicheroComprobaciones(@PathVariable Integer carteraId) throws IOException {
    String ruta = "./src/main/resources/csvs/" + carteraId + "/";
    File fileComp = new File(ruta + "COMPROBACIONES.txt");
    fileComp.createNewFile();
  }

  @PostMapping("/generarFicheroFinalizacion/{carteraId}")
  public void generarFicheroFinalizacion(@PathVariable Integer carteraId) throws IOException {
    String ruta = "./src/main/resources/csvs/" + carteraId + "/";
    File fileComp = new File(ruta + "FINALIZACION_CARGA.txt");
    fileComp.createNewFile();
  }

  @PostMapping("/generarCR/{carteraId}")
  public void generarCR(@PathVariable String carteraId) throws IOException {
    calculateContratoRepresentanteService.establecerContratoRepresentate(carteraId);
  }

  @GetMapping("/ping/clientes")
  public String pingClientes(){
    return segmentacionService.pruebas();
  }
}
