package com.haya.alaska.carga_control_transformacion.application;

import com.haya.alaska.carga_control_transformacion.domain.FileUtils;
import com.haya.alaska.carga_control_transformacion.services.MaestroService;
import com.haya.alaska.carga_control_transformacion.services.SegmentacionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class FileInUseCaseImpl implements FileInUseCase {

  @Autowired private JobLauncher jobLauncher;

  @Autowired private Job ctcJob;

  @Value("${service.warehouse.csvs}")
  private String unzipsFolder;

  @Autowired private LogImpl logImpl;

  @Autowired private SegmentacionService segmentacionService;

  @Autowired private MaestroService maestroService;

  public void saveFiles(
      MultipartFile multipartFile,
      String carteraIdCarga,
      boolean manual,
      boolean multicarteraBoolean)
      throws Exception {
    Date fechaInicio = new Date();
    FileUtils fileUtils = new FileUtils();
    fileUtils.getZip(multipartFile, unzipsFolder, carteraIdCarga);
    String folder = unzipsFolder + carteraIdCarga + "/";
    if (manual)folder = unzipsFolder;
    List<File> files = fileUtils.unzip(folder);

    if (manual) {
      JobParametersBuilder jobBuilder = new JobParametersBuilder();
      jobBuilder.addString("manual", String.valueOf(manual));
      jobBuilder.addString("multicartera", String.valueOf(multicarteraBoolean));
      jobBuilder.addString("fechaFichero", multipartFile.getOriginalFilename().split("_")[2]);
      if (carteraIdCarga != null && !carteraIdCarga.isEmpty())
        jobBuilder.addString("defaultCartera", carteraIdCarga);
      JobParameters jobParameters = jobBuilder.toJobParameters();
      jobLauncher.run(ctcJob, jobParameters);
    } else {
      String fechaFichero = multipartFile.getOriginalFilename().split("_")[2];
      String multicartera = multicarteraBoolean ? "1" : "0";

      /** Compruebo ficheros vacios */
      for (File file : files) {
        if (file.getName().contains("CONTRATO")) {
          if (comprobarFichero(file) != true) throw new Exception("Fichero de CONTRATO vacio");
        } else if (file.getName().contains("INTERVINIENTE")) {
          if (comprobarFichero(file) != true) throw new Exception("Fichero de INTERVINIENTE vacio");
        }
      }
      log.info("Ficheros validos");
      /* Llamo a CONTROL-M para la carga de Espejo */

      String ruta = "./src/main/resources/csvs/" + carteraIdCarga + "/";
      File fichero =
          new File(ruta + "INICIO_CONTROL_M_" + carteraIdCarga + "_" + multicartera + ".txt");
      fichero.createNewFile();
      boolean comprobacion = true;
      while (comprobacion) {
        Thread.sleep(60000);
        File fileComp = new File(ruta + "COMPROBACIONES.txt");
        if (fileComp.exists()) {
          log.info(
              "FLAGETLExiste el fichero " + ruta + "COMPROBACIONES.txt para dia: " + fechaFichero);
          comprobacion = false;
          fileComp.delete();
        } else {
          log.info(
              "FLAGETLNo existe fichero  " + ruta + "COMPROBACIONES.txt para dia: " + fechaFichero);
        }
      }

      /* Llamo a la generacfion de Logs */
    //  boolean logs = logImpl.execute(carteraIdCarga, "false", fechaFichero, multicartera);

      ruta = "./src/main/resources/csvs/" + carteraIdCarga + "/";
      fichero = new File(ruta + "OKEY_CHECKS_PENTAHO_" + carteraIdCarga + "_" + multicartera + ".txt");
      fichero.createNewFile();

      ruta = "./src/main/resources/csvs/" + carteraIdCarga + "/";
      comprobacion = true;
      while (comprobacion) {
        Thread.sleep(60000);
        File fileComp = new File(ruta + "FINALIZACION_CARGA.txt");
        if (fileComp.exists()) {
          log.info(
              "FLAGETLExiste el fichero "
                  + ruta
                  + "FINALIZACION_CARGA.txt para dia: "
                  + fechaFichero);
          comprobacion = false;
          fileComp.delete();
        } else {
          log.info(
              "FLAGETLNo existe fichero  "
                  + ruta
                  + "FINALIZACION_CARGA.txt para dia: "
                  + fechaFichero);
        }
      }
      log.info("Asignar Segmentacion");

      segmentacionService.asignarSegmentacion();

      log.info("Asignar Segmentacion Acabada");

      log.info("FLAGETLInicio: " + fechaInicio + " - Fin: " + new Date());
    }
  }

  private boolean comprobarFichero(File fichero) throws IOException {
    boolean result = true;
    FileReader fileReader = new FileReader(fichero);
    BufferedReader bufferedReader = new BufferedReader(fileReader);
    long lines = bufferedReader.lines().count();
    if (lines <= 1) {
      result = false;
    }
    bufferedReader.close();
    return result;
  }
}
