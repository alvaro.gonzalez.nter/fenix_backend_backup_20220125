package com.haya.alaska.carga_control_transformacion.application;

import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileInUseCase {

  void saveFiles(MultipartFile multipartFile, String carteraIdCarga, boolean manual, boolean multicartera) throws Exception;
}
