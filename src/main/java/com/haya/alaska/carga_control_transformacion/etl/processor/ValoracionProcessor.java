package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.espejo.valoracion.domain.ValoracionEspejo;
import com.haya.alaska.tipo_valoracion.domain.TipoValoracion;
import com.haya.alaska.tipo_valoracion.infrastructure.repository.TipoValoracionRepository;
import com.haya.alaska.valoracion.domain.Valoracion;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@StepScope
public class ValoracionProcessor implements ItemProcessor<ValoracionEspejo, Valoracion> {

  @Autowired
  private BienRepository bienRepository;

  @Autowired
  private TipoValoracionRepository tipoValoracionRepository;

  @Value("#{jobParameters['manual']}")
  private String manual;

  @Override
  public Valoracion process(ValoracionEspejo item) throws Exception {
    Bien bien = bienRepository.findByIdCarga(item.getBien().getIdCarga()).orElse(null);
    TipoValoracion tipoValoracion = tipoValoracionRepository.findByCodigo(item.getTipoValoracion().getCodigo()).orElse(null);

    Valoracion valoracion = new Valoracion(null, bien, item.getValorador(), item.getLiquidez(), tipoValoracion,
      item.getImporte(), item.getFecha(), item.getActivo());

    //Se hacen las validaciones con el campo ind_valido de la tabla espejo
    if (manual.equals("true")) {
      if (item.getValido() != null && !item.getValido()) {
        valoracion = null;
      }
    }
    return valoracion;

  }
}
