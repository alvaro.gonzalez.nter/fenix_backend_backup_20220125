package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.dato_contacto_skip_tracing.domain.DatoContactoST;
import com.haya.alaska.dato_contacto_skip_tracing.infraestructure.repository.DatoContactoSTRepository;
import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import com.haya.alaska.dato_direccion_skip_tracing.infraestructure.repository.DatoDireccionSTRepository;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.espejo.contrato_interviniente.domain.ContratoIntervinienteEspejo;
import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import com.haya.alaska.estado_civil.domain.EstadoCivil;
import com.haya.alaska.estado_civil.infrastructure.repository.EstadoCivilRepository;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.sexo.domain.Sexo;
import com.haya.alaska.sexo.infrastructure.repository.SexoRepository;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_contrato.domain.TipoContrato;
import com.haya.alaska.tipo_contrato.infrastructure.repository.TipoContratoRepository;
import com.haya.alaska.tipo_documento.domain.TipoDocumento;
import com.haya.alaska.tipo_documento.infrastructure.repository.TipoDocumentoRepository;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import com.haya.alaska.tipo_ocupacion.domain.TipoOcupacion;
import com.haya.alaska.tipo_ocupacion.infrastructure.repository.TipoOcupacionRepository;
import com.haya.alaska.tipo_prestacion.domain.TipoPrestacion;
import com.haya.alaska.tipo_prestacion.infrastructure.repository.TipoPrestacionRepository;
import com.haya.alaska.vulnerabilidad.domain.Vulnerabilidad;
import com.haya.alaska.vulnerabilidad.infrastructure.repository.VulnerabilidadRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Component
@StepScope
@Slf4j
public class IntervinienteProcessor implements ItemProcessor<IntervinienteEspejo, Interviniente> {

  @Autowired
  private ServicioMaestro servicioMaestro;

  @Autowired
  private CarteraRepository carteraRepository;

  @Autowired
  private TipoIntervencionRepository tipoIntervencionRepository;

  @Autowired
  private TipoDocumentoRepository tipoDocumentoRepository;

  @Autowired
  private PaisRepository paisRepository;

  @Autowired
  private TipoOcupacionRepository tipoOcupacionRepository;

  @Autowired
  private TipoContratoRepository tipoContratoRepository;

  @Autowired
  private TipoPrestacionRepository tipoPrestacionRepository;

  @Autowired
  private VulnerabilidadRepository vulnerabilidadRepository;

  @Autowired
  private ContratoRepository contratoRepository;

  @Autowired
  private IntervinienteRepository intervinienteRepository;

  @Autowired
  private ContratoIntervinienteRepository contratoIntervinienteRepository;

  @Autowired
  private DatoContactoRepository datoContactoRepository;

  @Autowired
  private DatoContactoSTRepository datoContactoSTRepository;

  @Autowired
  private DatoDireccionSTRepository datoDireccionSTRepository;

  @Autowired
  private EstadoCivilRepository estadoCivilRepository;

  @Autowired
  private SexoRepository sexoRepository;

  @Autowired
  private DireccionRepository direccionRepository;


  @Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  @Value("#{jobParameters['manual']}")
  private String manual;

  @Value("#{jobParameters['multicartera']}")
  private Boolean multicartera;


  @Override
  public Interviniente process(IntervinienteEspejo item) throws Exception {
    TipoDocumento tipoDocumento = null;
    Pais paisNacimiento = null;
    Pais paisResidencia = null;
    Pais paisNacionalidad = null;
    TipoOcupacion tipoOcupacion = null;
    TipoContrato tipoContrato = null;
    TipoPrestacion tipoPrestacion = null;
    Vulnerabilidad vulnerabilidad = null;
    EstadoCivil estadoCivil = null;
    Sexo sexo = null;
    if (item.getTipoDocumento() != null)
      tipoDocumento = tipoDocumentoRepository.findByCodigo(item.getTipoDocumento().getCodigo()).orElse(null);
    if (item.getPaisNacimiento() != null)
      paisNacimiento = paisRepository.findByCodigo(item.getPaisNacimiento().getCodigo()).orElse(null);
    if (item.getResidencia() != null)
      paisResidencia = paisRepository.findByCodigo(item.getResidencia().getCodigo()).orElse(null);
    if (item.getNacionalidad() != null)
      paisNacionalidad = paisRepository.findByCodigo(item.getNacionalidad().getCodigo()).orElse(null);
    if (item.getTipoOcupacion() != null)
      tipoOcupacion = tipoOcupacionRepository.findByCodigo(item.getTipoOcupacion().getCodigo()).orElse(null);
    if (item.getTipoPrestacion() != null)
      tipoPrestacion = tipoPrestacionRepository.findByCodigo(item.getTipoPrestacion().getCodigo()).orElse(null);
    if (item.getOtrasSituacionesVulnerabilidad() != null)
      vulnerabilidad = vulnerabilidadRepository.findByCodigo(item.getOtrasSituacionesVulnerabilidad().getCodigo()).orElse(null);
    if (item.getEstadoCivil() != null)
      estadoCivil = estadoCivilRepository.findByCodigo(item.getEstadoCivil().getCodigo()).orElse(null);
    if (item.getSexo() != null)
      sexo = sexoRepository.findByCodigo(item.getSexo().getCodigo()).orElse(null);

    Interviniente interviniente = intervinienteRepository.findByIdCarga(item.getIdCarga()).orElse(new Interviniente());
    interviniente.setIdHaya(item.getIdHaya());
    interviniente.setIdDataType(item.getIdDataType());
    interviniente.setIdOrigen(item.getIdOrigen());
    interviniente.setIdOrigen2(item.getIdOrigen2());
    interviniente.setIdCarga(item.getIdCarga());
    interviniente.setPersonaJuridica(item.getPersonaJuridica());
    interviniente.setNumeroDocumento(item.getNumeroDocumento());
    interviniente.setNombre(item.getNombre());
    interviniente.setApellidos(item.getApellidos());
    interviniente.setSexo(sexo);
    interviniente.setEstadoCivil(estadoCivil);
    interviniente.setFechaNacimiento(item.getFechaNacimiento());
    interviniente.setRazonSocial(item.getRazonSocial());
    interviniente.setFechaConstitucion(item.getFechaConstitucion());
    interviniente.setEmpresa(item.getEmpresa());
    interviniente.setTelefonoEmpresa(item.getTelefonoEmpresa());
    interviniente.setContactoEmpresa(item.getContactoEmpresa());
    interviniente.setCnae(item.getCnae());
    interviniente.setIngresosNetosMensuales(item.getIngresosNetosMensuales());
    interviniente.setDescripcionActividad(item.getDescripcionActividad());
    interviniente.setNumHijosDependientes(item.getNumHijosDependientes());
    interviniente.setOtrosRiesgos(item.getOtrosRiesgos());
    interviniente.setVulnerabilidad(item.getVulnerabilidad());
    interviniente.setPensionista(item.getPensionista());
    interviniente.setDependiente(item.getDependiente());
    interviniente.setDiscapacitados(item.getDiscapacitados());
    interviniente.setPrestaciones(item.getPrestaciones());
    interviniente.setNotas1(item.getNotas1());
    interviniente.setNotas2(item.getNotas2());
    interviniente.setContactado(item.getContactado());
    interviniente.setCodigoSocioProfesional(item.getCodigoSocioProfesional());
    interviniente.setNacionalidad(paisNacionalidad);
    interviniente.setPaisNacimiento(paisNacimiento);
    interviniente.setResidencia(paisResidencia);
    //interviniente.setProcedimientos(null);
    //interviniente.setPropuestas(null);
    interviniente.setTipoDocumento(tipoDocumento);
    interviniente.setTipoOcupacion(tipoOcupacion);
    interviniente.setTipoContrato(tipoContrato);
    interviniente.setTipoPrestacion(tipoPrestacion);
    interviniente.setOtrasSituacionesVulnerabilidad(vulnerabilidad);
    interviniente.setFechaCarga(item.getFechaCarga());

    Cartera cartera = getCartera(carteraId);
    //Este control se deja por si se tiene que añadir una logica adicional para apto automatico o manual
    if (cartera.getDatosCoreBanking() != null && cartera.getDatosCoreBanking() == true) {
      interviniente.setActivo(true);
    } else {
      interviniente.setActivo(true);
    }

    //Relación de contratos interviniente
    for (ContratoIntervinienteEspejo contratoIntervinienteEspejo : item.getContratos()) {
      if (contratoIntervinienteEspejo.getContrato() != null) {
        Contrato contrato = contratoRepository.findByIdCarga(contratoIntervinienteEspejo.getContrato().getIdCarga()).orElse(null);
        TipoIntervencion tipoIntervencion = contratoIntervinienteEspejo.getTipoIntervencion();
        if (tipoIntervencion == null) tipoIntervencion = tipoIntervencionRepository.findByCodigo("999").orElse(null);
        ContratoInterviniente contratoInterviniente = null;
        if (interviniente.getId() != null && contrato.getId() != null) {
          contratoInterviniente = contratoIntervinienteRepository.findByIntervinienteIdAndContratoIdAndTipoIntervencionId(interviniente.getId(), contrato.getId(), tipoIntervencion.getId()).orElse(new ContratoInterviniente());
        } else {
          contratoInterviniente = new ContratoInterviniente();
        }
        contratoInterviniente.setContrato(contrato);
        contratoInterviniente.setTipoIntervencion(tipoIntervencion);
        contratoInterviniente.setOrdenIntervencion(contratoIntervinienteEspejo.getOrdenIntervencion());
        //Este control se deja por si se tiene que añadir una logica adicional para apto automatico o manual
        if (cartera.getDatosCoreBanking() != null && cartera.getDatosCoreBanking() == true) {
          contratoInterviniente.setActivo(true);
        } else {
          contratoInterviniente.setActivo(true);
        }
        contratoInterviniente.setInterviniente(interviniente);
        interviniente.getContratos().add(contratoInterviniente);
      }
    }

    //Se borran los datos de contacto antiguos del interviniente,
    // no es posible actualizar ya que no existe un idCarga de datoContacto
    if (interviniente.getId() != null) {
      for (DatoContacto datoContacto : interviniente.getDatosContacto()) {
        List<DatoContactoST> datoContactoSTS = datoContactoSTRepository.findAllByDatoContactoId(datoContacto.getId());
        if (datoContactoSTS == null || datoContactoSTS.size() == 0)
          datoContactoRepository.delete(datoContacto);
      }
    }
    interviniente.setDatosContacto(new HashSet<>() {
    });

    //Se borran las direcciones antiguas del interviniente,
    // no es posible actualizar ya que no existe un idCarga de dirección
    if (interviniente != null) {
      for (Direccion direccion : interviniente.getDirecciones()) {
        DatoDireccionST datoDireccionST = datoDireccionSTRepository.findByDatoDireccionId(direccion.getId()).orElse(null);
        if (datoDireccionST == null)
          direccionRepository.delete(direccion);
      }
    }
    interviniente.setDirecciones(new HashSet<>() {
    });

    if (cartera != null && item.getIdHaya() == null && !multicartera) {
      //se llama a maestro con el interviniente recién creado
      for (ContratoInterviniente ci : interviniente.getContratos()) {
        Interviniente masterInterviniente = callMaster(interviniente, cartera.getCliente().getNombre(), ci.getTipoIntervencion());
        item.setIdHaya(masterInterviniente.getIdHaya());
      }
      // Se asigna el IDHaya que devuelve maestro

    }
    item.setFechaCarga(new Date());

    //Se hacen las validaciones con el campo valido de la tabla espejo
    if (manual.equals("true")) {
      if (item.getValido() != null && !item.getValido()) {
        interviniente = null;
      }
    }

    return interviniente;
  }

  private Interviniente callMaster(Interviniente interviniente, String cliente, TipoIntervencion tipoIntervencion) throws NotFoundException {
    // Llamada al servicio de maestros para obtener el idHaya del contrato
    try {
      servicioMaestro.registrarInterviniente(interviniente, cliente, tipoIntervencion);
    } catch (NotFoundException | WebServiceIOException | LockedChangeException e) {
      log.warn(e.getMessage());
      interviniente.setIdHaya(null);
    }
    log.debug(String.format("Interviniente.idHaya -> %s", interviniente.getId()));
    return interviniente;
  }

  private Cartera getCartera(String idCargaCartera) {
    Cartera cartera = carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(idCargaCartera).orElse(null);
    return cartera;
  }

}
