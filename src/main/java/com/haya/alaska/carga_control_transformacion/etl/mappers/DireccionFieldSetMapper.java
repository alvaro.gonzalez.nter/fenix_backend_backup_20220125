package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

@Getter
@Setter
public class DireccionFieldSetMapper implements FieldSetMapper<Direccion> {

  private LocalidadRepository localidadRepository;

  private MunicipioRepository municipioRepository;

  private ProvinciaRepository provinciaRepository;

  private TipoViaRepository tipoViaRepository;

  private EntornoRepository entornoRepository;

  private IntervinienteRepository intervinienteRepository;

  @Override
  public Direccion mapFieldSet(FieldSet fieldSet) throws BindException {

    Direccion direccion = new Direccion();
    if (isvalidString(fieldSet.readString("interviniente.idCarga"))) {
      Interviniente interviniente = createInterviniente(fieldSet.readString("interviniente.idCarga"));
      if (interviniente != null) {
        direccion.setInterviniente(interviniente);
      }
    }

    if (isvalidString(fieldSet.readString("tipoVia.codigo"))) {
      direccion.setTipoVia(createTipoVia(fieldSet.readString("tipoVia.codigo")));
    }
    if (isvalidString(fieldSet.readString("nombre"))) {
      direccion.setNombre(fieldSet.readString("nombre"));
    }
    if (isvalidString(fieldSet.readString("numero"))) {
      direccion.setNumero(fieldSet.readString("numero"));
    }
    if (isvalidString(fieldSet.readString("portal"))) {
      direccion.setPortal(fieldSet.readString("portal"));
    }
    if (isvalidString(fieldSet.readString("bloque"))) {
      direccion.setBloque(fieldSet.readString("bloque"));
    }
    if (isvalidString(fieldSet.readString("escalera"))) {
      direccion.setEscalera(fieldSet.readString("escalera"));
    }
    if (isvalidString(fieldSet.readString("piso"))) {
      direccion.setPiso(fieldSet.readString("piso"));
    }
    if (isvalidString(fieldSet.readString("puerta"))) {
      direccion.setPuerta(fieldSet.readString("puerta"));
    }
    if (isvalidString(fieldSet.readString("entorno.codigo"))) {
      direccion.setEntorno(createEntorno(fieldSet.readString("entorno.codigo")));
    }
    if (isvalidString(fieldSet.readString("municipio.codigo"))) {
      direccion.setMunicipio(createMunicipio(fieldSet.readString("municipio.codigo")));
    }
    if (isvalidString(fieldSet.readString("localidad.codigo"))) {
      direccion.setLocalidad(createLocalidad(fieldSet.readString("localidad.codigo")));
    }
    if (isvalidString(fieldSet.readString("provincia.codigo"))) {
      direccion.setProvincia(createProvincia(fieldSet.readString("provincia.codigo")));
    }
    if (isvalidString(fieldSet.readString("comentario"))) {
      direccion.setComentario(fieldSet.readString("comentario"));
    }
    if (isvalidString(fieldSet.readString("codigoPostal"))) {
      direccion.setCodigoPostal(fieldSet.readString("codigoPostal"));
    }
    if (isvalidString(fieldSet.readString("longitud"))) {
      direccion.setLongitud(fieldSet.readDouble("longitud"));
    }
    if (isvalidString(fieldSet.readString("latitud"))) {
      direccion.setLatitud(fieldSet.readDouble("latitud"));
    }
    if (isvalidString(fieldSet.readString("descripcion"))) {
      direccion.setComentario(fieldSet.readString("descripcion"));
    }
    direccion.setDireccionGarantia(false);

    return direccion;
  }

  private Interviniente createInterviniente(String idCargaInterviniente) {
    Interviniente interviniente = intervinienteRepository.findByIdCarga(idCargaInterviniente).orElse(null);
    return interviniente;

  }

  private TipoVia createTipoVia(String codigoTipoVia) {
    TipoVia tipoVia = tipoViaRepository.findByCodigo(codigoTipoVia).orElse(null);
    return tipoVia;
  }

  private Entorno createEntorno(String codigoEntorno) {
    String codToUse = getIntValue(codigoEntorno);
    Entorno entorno = entornoRepository.findByCodigo(codToUse).orElse(null);
    return entorno;
  }


  private Municipio createMunicipio(String codigoMunicipio) {
    String codToUse = getIntValue(codigoMunicipio);
    Municipio municipio = municipioRepository.findByCodigo(codToUse).orElse(null);
    return municipio;
  }

  private Localidad createLocalidad(String codigoLocalidad) {
    String codToUse = getIntValue(codigoLocalidad);
    Localidad localidad = localidadRepository.findByCodigo(codToUse).orElse(null);
    return localidad;
  }

  private Provincia createProvincia(String codigoProvincia) {
    String codToUse = getIntValue(codigoProvincia);
    Provincia provincia = provinciaRepository.findByCodigo(codToUse).orElse(null);
    return provincia;
  }

  private Boolean isvalidString(String string) {
    if (string != null && !string.equals("")) {
      return true;
    } else {
      return false;
    }
  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }

}
