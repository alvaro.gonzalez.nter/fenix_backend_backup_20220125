package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.descripcion_movimiento.domain.DescripcionMovimiento;
import com.haya.alaska.descripcion_movimiento.infrastructure.repository.DescripcionMovimientoRepository;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.espejo.contrato.infrastructure.repository.ContratoEspejoRepository;
import com.haya.alaska.espejo.movimiento.domain.MovimientoEspejo;
import com.haya.alaska.tipo_movimiento.domain.TipoMovimiento;
import com.haya.alaska.tipo_movimiento.infrastructure.repository.TipoMovimientoRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

@Getter
@Setter
public class MovimientoEspejoFieldSetMapper implements FieldSetMapper<MovimientoEspejo> {

  private TipoMovimientoRepository tipoMovimientoRepository;

  private DescripcionMovimientoRepository descripcionMovimientoRepository;

  private ContratoEspejoRepository contratoEspejoRepository;


  @Override
  public MovimientoEspejo mapFieldSet(FieldSet fieldSet) throws BindException {
    MovimientoEspejo movimiento = new MovimientoEspejo();

    if (isvalidString(fieldSet.readString("contrato.id"))) {
      ContratoEspejo contrato = createContrato(fieldSet.readString("contrato.id"));
      if (contrato != null) {
        movimiento.setContrato(contrato);
      }
    }

    if (isvalidString(fieldSet.readString("tipoMovimiento.codigo"))) {
      movimiento.setTipo(createTipoMovimiento(fieldSet.readString("tipoMovimiento.codigo")));
    }
    if (isvalidString(fieldSet.readString("descripcionMovimiento.codigo"))) {
      movimiento.setDescripcion(createDescripcionMovimiento(fieldSet.readString("descripcionMovimiento.codigo")));
    }
    if (isvalidString(fieldSet.readString("fechaValor"))) {
      movimiento.setFechaValor(fieldSet.readDate("fechaValor", "dd/MM/yyyy"));
    }
    if (isvalidString(fieldSet.readString("fechaContable"))) {
      movimiento.setFechaContable(fieldSet.readDate("fechaContable", "dd/MM/yyyy"));
    }
    if (isvalidString(fieldSet.readString("sentido"))) {
      movimiento.setSentido(fieldSet.readString("sentido"));
    }
    if (isvalidString(fieldSet.readString("importe"))) {
      movimiento.setImporte(fieldSet.readDouble("importe"));
    }
    if (isvalidString(fieldSet.readString("principalCapital"))) {
      movimiento.setPrincipalCapital(fieldSet.readDouble("principalCapital"));
    }
    if (isvalidString(fieldSet.readString("interesesOrdinarios"))) {
      movimiento.setInteresesOrdinarios(fieldSet.readDouble("interesesOrdinarios"));
    }
    if (isvalidString(fieldSet.readString("interesesDemora"))) {
      movimiento.setInteresesDemora(fieldSet.readDouble("interesesDemora"));
    }
    if (isvalidString(fieldSet.readString("comisiones"))) {
      movimiento.setComisiones(fieldSet.readDouble("comisiones"));
    }
    if (isvalidString(fieldSet.readString("gastos"))) {
      movimiento.setGastos(fieldSet.readDouble("gastos"));
    }
    if (isvalidString(fieldSet.readString("impuestos"))) {
      movimiento.setImpuestos(fieldSet.readDouble("impuestos"));
    }
    if (isvalidString(fieldSet.readString("principalPendienteVencer"))) {
      movimiento.setPrincipalPendienteVencer(fieldSet.readDouble("principalPendienteVencer"));
    }
    if (isvalidString(fieldSet.readString("deudaImpagada"))) {
      movimiento.setDeudaImpagada(fieldSet.readDouble("deudaImpagada"));
    }
    //saldoGestion
    if (isvalidString(fieldSet.readString("usuario"))) {
      movimiento.setUsuario(fieldSet.readString("usuario"));
    }
    if (isvalidString(fieldSet.readString("importeRecuperado"))) {
      movimiento.setImporteRecuperado(fieldSet.readDouble("importeRecuperado"));
    }
    if (isvalidString(fieldSet.readString("salidaDudoso"))) {
      movimiento.setSalidaDudoso(fieldSet.readDouble("salidaDudoso"));
    }
    if (isvalidString(fieldSet.readString("importeFacturable"))) {
      movimiento.setImporteFacturable(fieldSet.readDouble("importeFacturable"));
    }
    if (isvalidString(fieldSet.readString("numeroFactura"))) {
      movimiento.setNumeroFactura(fieldSet.readString("numeroFactura"));
    }
    if (isvalidString(fieldSet.readString("detalleGasto"))) {
      movimiento.setDetalleGasto(fieldSet.readString("detalleGasto"));
    }
    if (isvalidString(fieldSet.readString("id"))) {
      movimiento.setIdCarga(fieldSet.readString("id"));
    }
    if (isvalidString(fieldSet.readString("afecta"))) {
      movimiento.setAfecta(createBooleanValue(fieldSet.readString("afecta")));
    } else {
      movimiento.setAfecta(false);
    }
    if (isvalidString(fieldSet.readString("estado"))) {
      movimiento.setActivo(createBooleanValue(fieldSet.readString("estado")));
    }
    if (isvalidString(fieldSet.readString("comentarios"))) {
      movimiento.setComentarios(fieldSet.readString("comentarios"));
    }
    movimiento.setActivo(true);
    return movimiento;

  }

  private ContratoEspejo createContrato(String idCargaContrato) {
    ContratoEspejo contrato = contratoEspejoRepository.findByIdCarga(idCargaContrato).orElse(null);
    return contrato;
  }

  private TipoMovimiento createTipoMovimiento(String codigoTipoMovimiento) {
    String codToUse = getIntValue(codigoTipoMovimiento);
    TipoMovimiento tipoMovimiento = tipoMovimientoRepository.findByCodigo(codToUse).orElse(null);
    return tipoMovimiento;
  }

  private DescripcionMovimiento createDescripcionMovimiento(String codigoDescripcionMovimiento) {
    String codToUse = getIntValue(codigoDescripcionMovimiento);
    DescripcionMovimiento descripcionMovimiento = descripcionMovimientoRepository.findByCodigo(codToUse).orElse(null);
    return descripcionMovimiento;
  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }

  private Boolean isvalidString(String string) {
    if (string != null && !string.equals("")) {
      return true;
    } else {
      return false;
    }
  }

  private Boolean createBooleanValue(String code) {
    String codeToUse = getIntValue(code);
    switch (codeToUse) {
      case "0":
        return false;
      case "1":
        return true;
      default:
        return null;
    }
  }

}
