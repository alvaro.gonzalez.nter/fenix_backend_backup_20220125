package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.tipo_valoracion.domain.TipoValoracion;
import com.haya.alaska.tipo_valoracion.infrastructure.repository.TipoValoracionRepository;
import com.haya.alaska.valoracion.domain.Valoracion;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

@Getter
@Setter
public class ValoracionFieldSetMapper implements FieldSetMapper<Valoracion> {

  private BienRepository bienRepository;

  private TipoValoracionRepository tipoValoracionRepository;

  private LiquidezRepository liquidezRepository;


  @Override
  public Valoracion mapFieldSet(FieldSet fieldSet) throws BindException {
    Valoracion valoracion = new Valoracion();
    if (isvalidString(fieldSet.readString("bien.id"))) {
      Bien bien = createBien(fieldSet.readString("bien.id"));
      if (bien != null) {
        //valoracion.setBienes(bien);
        valoracion.setBien(bien);
      }
    }
    if (isvalidString(fieldSet.readString("valorador"))) {
      valoracion.setValorador(fieldSet.readString("valorador"));
    }
    if (isvalidString(fieldSet.readString("tipoValoracion.codigo"))) {
      valoracion.setTipoValoracion(createTipoValoracion(fieldSet.readString("tipoValoracion.codigo")));
    }
    if (isvalidString(fieldSet.readString("importeValoracion"))) {
      valoracion.setImporte(fieldSet.readDouble("importeValoracion"));
    }
    if (isvalidString(fieldSet.readString("fechaValoracion"))) {
      valoracion.setFecha(fieldSet.readDate("fechaValoracion", "dd/MM/yyyy"));
    }
    if (isvalidString(fieldSet.readString("liquidez.codigo"))) {
      valoracion.setLiquidez(createLiquidez(fieldSet.readString("liquidez.codigo")));
    }

    return valoracion;
  }

  private Bien createBien(String idCargaBien) {
    Bien bien = bienRepository.findByIdCarga(idCargaBien).orElse(null);
    return bien;
  }

  private TipoValoracion createTipoValoracion(String codigoTipoValoracion) {
    String codToUse = getIntValue(codigoTipoValoracion);
    TipoValoracion tipoValoracion = tipoValoracionRepository.findByCodigo(codToUse).orElse(null);
    return tipoValoracion;
  }

  private Liquidez createLiquidez(String codigoLiquidez) {
    Liquidez liquidez = liquidezRepository.findByCodigo(codigoLiquidez).orElse(null);
    return liquidez;
  }

  private Boolean isvalidString(String string) {
    if (string != null && !string.equals("")) {
      return true;
    } else {
      return false;
    }
  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }
}
