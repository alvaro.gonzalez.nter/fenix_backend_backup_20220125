package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.espejo.expediente.domain.ExpedienteEspejo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

@Getter
@Setter
public class ExpedienteEspejoFieldSetMapper implements FieldSetMapper<ExpedienteEspejo> {

  private CarteraRepository carteraRepository;

  @Override
  public ExpedienteEspejo mapFieldSet(FieldSet fieldSet) throws BindException {

    ExpedienteEspejo expediente = new ExpedienteEspejo();
    expediente.setActivo(true);
    if (!fieldSet.readString("idCarga").equals("")) {
      // si existe el idCarga generamos un expediente
      expediente.setIdCarga(fieldSet.readString("idCarga"));
      if (!fieldSet.readString("cartera.id").equals("")) {
        expediente.setCartera(createCartera(fieldSet.readString("cartera.id")));
      }
      if (!fieldSet.readString("idOrigen").equals("")) {
        expediente.setIdOrigen(fieldSet.readString("idOrigen"));
      }
    } else {
      expediente = null;
    }

    return expediente;
  }

  private Cartera createCartera(String carteraIdCarga) {
    String codToUse = getIntValue(carteraIdCarga);
    Cartera cartera = carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(carteraIdCarga).get();
    return cartera;

  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }

}
