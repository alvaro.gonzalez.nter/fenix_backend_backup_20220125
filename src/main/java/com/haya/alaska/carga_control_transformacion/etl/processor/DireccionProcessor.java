package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.espejo.direccion.domain.DireccionEspejo;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.interviniente.infrastructure.util.IntervinienteUtil;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Set;

@Component
@StepScope
public class DireccionProcessor implements ItemProcessor<DireccionEspejo, Direccion> {

  @Autowired
  private IntervinienteRepository intervinienteRepository;

  @Autowired
  private LocalidadRepository localidadRepository;

  @Autowired
  private MunicipioRepository municipioRepository;

  @Autowired
  private ProvinciaRepository provinciaRepository;

  @Autowired
  private TipoViaRepository tipoViaRepository;

  @Autowired
  private EntornoRepository entornoRepository;

  @Autowired
  private PaisRepository paisRepository;

  @Autowired
  private DireccionRepository direccionRepository;

  @Autowired
  private IntervinienteUtil intervinienteUtil;

  @Value("#{jobParameters['manual']}")
  private String manual;

  @Override
  public Direccion process(DireccionEspejo item) throws Exception {
    Interviniente interviniente = intervinienteRepository.findByIdCarga(item.getInterviniente().getIdCarga()).orElse(null);

    Localidad localidad = null;
    Municipio municipio = null;
    Provincia provincia = null;
    TipoVia tipoVia = null;
    Entorno entorno = null;
    Pais pais = null;
    if (item.getLocalidad() != null)
      localidad = localidadRepository.findByCodigo(item.getLocalidad().getCodigo()).orElse(null);
    if (item.getMunicipio() != null)
      municipio = municipioRepository.findByCodigo(item.getMunicipio().getCodigo()).orElse(null);
    if (item.getProvincia() != null)
      provincia = provinciaRepository.findByCodigo(item.getProvincia().getCodigo()).orElse(null);
    if (item.getTipoVia() != null)
      tipoVia = tipoViaRepository.findByCodigo(item.getTipoVia().getCodigo()).orElse(null);
    if (item.getEntorno() != null)
      entorno = entornoRepository.findByCodigo(item.getEntorno().getCodigo()).orElse(null);
    if (item.getPais() != null)
      pais = paisRepository.findByCodigo(item.getPais().getCodigo()).orElse(null);


    if (interviniente != null) {
      Set<Direccion> direcciones = interviniente.getDirecciones();
      if (direcciones != null && direcciones.isEmpty()) {
        item.setPrincipal(true);
      } else {
        item.setPrincipal(false);
      }
    }
    Direccion direccion = new Direccion(
      interviniente,
      item.getPrincipal(),
      tipoVia,
      item.getNombre(),
      item.getNumero(),
      item.getPortal(),
      item.getBloque(),
      item.getEscalera(),
      item.getPiso(),
      item.getPuerta(),
      municipio,
      localidad,
      entorno,
      provincia,
      pais,
      item.getComentario(),
      item.getCodigoPostal(),
      item.getLongitud(),
      item.getLatitud(),
      item.getActivo(),
      item.getValidada(),
      item.getDireccionGarantia(),
      "Carga",
      new Date(),
      new Date()
    );

    /*if (direccion.getLatitud() == null || direccion.getLongitud() == null)
      intervinienteUtil.direccionGoogle(direccion);*/

    //Se hacen las validaciones con el campo valido de la tabla espejo
    if (manual.equals("true")) {
      if (item.getValido() != null && !item.getValido()) {
        direccion = null;
      }
    }

    return direccion;

  }
}
