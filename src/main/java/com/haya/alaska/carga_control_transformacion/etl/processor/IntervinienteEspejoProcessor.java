package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@StepScope
@Slf4j
public class IntervinienteEspejoProcessor implements ItemProcessor<IntervinienteEspejo, IntervinienteEspejo> {

  @Autowired
  private ServicioMaestro servicioMaestro;

  @Autowired
  private CarteraRepository carteraRepository;

  @Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  @Value("#{jobParameters['multicartera']}")
  private Boolean multicartera;


  @Override
  public IntervinienteEspejo process(IntervinienteEspejo item) throws Exception {
    Cartera cartera = getCartera(carteraId);
    if (cartera != null && item.getIdHaya() == null && !multicartera) {
      //se llama a maestro con el interviniente recién creado
    }
    item.setFechaCarga(new Date());
    return item;
  }
  private Cartera getCartera(String idCargaCartera) {
    Cartera cartera = carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(idCargaCartera).orElse(null);
    return cartera;
  }

}
