package com.haya.alaska.carga_control_transformacion.etl.config;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.infrastructure.repository.CargaRepository;
import com.haya.alaska.carga_control_transformacion.domain.MyDecider;
import com.haya.alaska.carga_control_transformacion.etl.mappers.*;
import com.haya.alaska.carga_control_transformacion.etl.processor.*;
import com.haya.alaska.carga_control_transformacion.tasklet.*;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.clasificacion_contable.infrastructure.repository.ClasificacionContableRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.infrastructure.repository.ContratoBienRepository;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.descripcion_movimiento.infrastructure.repository.DescripcionMovimientoRepository;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.espejo.bien.infrastructure.repository.BienEspejoRepository;
import com.haya.alaska.espejo.carga.domain.CargaEspejo;
import com.haya.alaska.espejo.carga.infrastructure.repository.CargaEspejoRepository;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.espejo.contrato.infrastructure.repository.ContratoEspejoRepository;
import com.haya.alaska.espejo.contrato_bien.infrastructure.repository.ContratoBienEspejoRepository;
import com.haya.alaska.espejo.contrato_interviniente.infrastructure.repository.ContratoIntervinienteEspejoRepository;
import com.haya.alaska.espejo.dato_contacto.domain.DatoContactoEspejo;
import com.haya.alaska.espejo.dato_contacto.infrastructure.repository.DatoContactoEspejoRepository;
import com.haya.alaska.espejo.direccion.domain.DireccionEspejo;
import com.haya.alaska.espejo.direccion.infrastructure.repository.DireccionEspejoRepository;
import com.haya.alaska.espejo.expediente.domain.ExpedienteEspejo;
import com.haya.alaska.espejo.expediente.infrastructure.repository.ExpedienteEspejoRepository;
import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import com.haya.alaska.espejo.interviniente.infrastructure.repository.IntervinienteEspejoRepository;
import com.haya.alaska.espejo.movimiento.domain.MovimientoEspejo;
import com.haya.alaska.espejo.tasacion.domain.TasacionEspejo;
import com.haya.alaska.espejo.tasacion.infrastructure.repository.TasacionEspejoRepository;
import com.haya.alaska.espejo.valoracion.domain.ValoracionEspejo;
import com.haya.alaska.espejo.valoracion.infrastructure.repository.ValoracionEspejoRepository;
import com.haya.alaska.estado_civil.infrastructure.repository.EstadoCivilRepository;
import com.haya.alaska.estado_contrato.infrastructure.repository.EstadoContratoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.indice_referencia.infrastructure.repository.IndiceReferenciaRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.periodicidad_liquidacion.infrastructure.repository.PeriodicidadLiquidacionRepository;
import com.haya.alaska.producto.infrastructure.repository.ProductoRepository;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.sexo.infrastructure.repository.SexoRepository;
import com.haya.alaska.sistema_amortizacion.infrastructure.repository.SistemaAmortizacionRepository;
import com.haya.alaska.sub_tipo_bien.infrastructure.repository.SubTipoBienRepository;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.infrastructure.repository.TasacionRepository;
import com.haya.alaska.tipo_acreedor.infrastructure.repository.TipoAcreedorRepository;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_carga.infrastructure.repository.TipoCargaRepository;
import com.haya.alaska.tipo_contrato.infrastructure.repository.TipoContratoRepository;
import com.haya.alaska.tipo_documento.infrastructure.repository.TipoDocumentoRepository;
import com.haya.alaska.tipo_garantia.infrastructure.repository.TipoGarantiaRepository;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import com.haya.alaska.tipo_movimiento.infrastructure.repository.TipoMovimientoRepository;
import com.haya.alaska.tipo_ocupacion.infrastructure.repository.TipoOcupacionRepository;
import com.haya.alaska.tipo_prestacion.infrastructure.repository.TipoPrestacionRepository;
import com.haya.alaska.tipo_reestructuracion.infrastructure.repository.TipoReestructuracionRepository;
import com.haya.alaska.tipo_tasacion.infrastructure.repository.TipoTasacionRepository;
import com.haya.alaska.tipo_valoracion.infrastructure.repository.TipoValoracionRepository;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.uso.infrastructure.repository.UsoRepository;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.valoracion.infrastructure.repository.ValoracionRepository;
import com.haya.alaska.vulnerabilidad.infrastructure.repository.VulnerabilidadRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.List;

@Configuration
@ImportResource("classpath:integration-configurations.xml")
@EnableBatchProcessing
public class CTCConfig {

  @Autowired
  private JobBuilderFactory jobBuilderFactory;

  @Autowired
  private StepBuilderFactory stepBuilderFactory;

  @Autowired
  private EntityManagerFactory entityManagerFactory;

  @Autowired
  @Qualifier("patternsFilenameList")
  private List<String> myList;

  @Autowired
  private CarteraRepository carteraRepository;

  @Autowired
  private ContratoRepository contratoRepository;

  @Autowired
  private ContratoEspejoRepository contratoEspejoRepository;

  @Autowired
  private SistemaAmortizacionRepository sistemaAmortizacionRepository;

  @Autowired
  private ClasificacionContableRepository clasificacionContableRepository;

  @Autowired
  private TipoReestructuracionRepository tipoReestructuracionRepository;

  @Autowired
  private IndiceReferenciaRepository indiceReferenciaRepository;

  @Autowired
  private PeriodicidadLiquidacionRepository periodicidadLiquidacionRepository;

  @Autowired
  private ProductoRepository productoRepository;

  @Autowired
  private ExpedienteRepository expedienteRepository;

  @Autowired
  private ExpedienteEspejoRepository expedienteEspejoRepository;

  @Autowired
  private TipoIntervencionRepository tipoIntervencionRepository;

  @Autowired
  private TipoDocumentoRepository tipoDocumentoRepository;

  @Autowired
  private PaisRepository paisRepository;

  @Autowired
  private TipoOcupacionRepository tipoOcupacionRepository;

  @Autowired
  private TipoContratoRepository tipoContratoRepository;

  @Autowired
  private TipoPrestacionRepository tipoPrestacionRepository;

  @Autowired
  private VulnerabilidadRepository vulnerabilidadRepository;

  @Autowired
  private LocalidadRepository localidadRepository;

  @Autowired
  private MunicipioRepository municipioRepository;

  @Autowired
  private ProvinciaRepository provinciaRepository;

  @Autowired
  private TipoViaRepository tipoViaRepository;

  @Autowired
  private EntornoRepository entornoRepository;

  @Autowired
  private IntervinienteRepository intervinienteRepository;

  @Autowired
  private CargaEspejoRepository cargaEspejoRepository;

  @Autowired
  private IntervinienteEspejoRepository intervinienteEspejoRepository;

  @Autowired
  private TipoBienRepository tipoBienRepository;

  @Autowired
  private SubTipoBienRepository subTipoBienRepository;

  @Autowired
  private TipoGarantiaRepository tipoGarantiaRepository;

  @Autowired
  private ValoracionEspejoRepository valoracionEspejoRepository;

  @Autowired
  private TipoActivoRepository tipoActivoRepository;

  @Autowired
  private BienRepository bienRepository;

  @Autowired
  private BienEspejoRepository bienEspejoRepository;

  @Autowired
  private TipoCargaRepository tipoCargaRepository;

  @Autowired
  private CargaRepository cargaRepository;


  @Autowired
  private TipoAcreedorRepository tipoAcreedorRepository;

  @Autowired
  private UsoRepository usoRepository;

  @Autowired
  private TipoTasacionRepository tipoTasacionRepository;

  @Autowired
  private TipoValoracionRepository tipoValoracionRepository;

  @Autowired
  private ValoracionRepository valoracionRepository;

  @Autowired
  private TipoMovimientoRepository tipoMovimientoRepository;

  @Autowired
  private DescripcionMovimientoRepository descripcionMovimientoRepository;

  @Autowired
  private ContratoIntervinienteRepository contratoIntervinienteRepository;

  @Autowired
  private ContratoIntervinienteEspejoRepository contratoIntervinienteEspejoRepository;

  @Autowired
  private EstadoCivilRepository estadoCivilRepository;

  @Autowired
  private EstadoContratoRepository estadoContratoRepository;

  @Autowired
  private SexoRepository sexoRepository;

  @Autowired
  private ContratoBienRepository contratoBienRepository;

  @Autowired
  private ContratoBienEspejoRepository contratoBienEspejoRepository;

  @Autowired
  private LiquidezRepository liquidezRepository;

  @Autowired
  private DireccionEspejoRepository direccionEspejoRepository;

  @Autowired
  private DireccionRepository direccionRepository;

  @Autowired
  private TasacionEspejoRepository tasacionEspejoRepository;

  @Autowired
  private TasacionRepository tasacionRepository;

  @Autowired
  private DatoContactoEspejoRepository datoContactoEspejoRepository;

  @Autowired
  private DatoContactoRepository datoContactoRepository;

  @Autowired
  private MyDecider decider;

  @Value("${service.warehouse}")
  private String upload_folder;

  @Value("${service.warehouse.csvs}")
  private String csvsFolder;

  @Value("${spring.datasource.driver-class-name}")
  private String dataSourceDriver;

  @Value("${spring.datasource.url}")
  private String dataSourceUrl;

  @Value("${spring.datasource.username}")
  private String dataSourceUser;

  @Value("${spring.datasource.password}")
  private String dataSourcePassword;

  @Bean
  public JobRepository jobRepository() throws Exception {
    MapJobRepositoryFactoryBean factory = new MapJobRepositoryFactoryBean();
    // factory.setTransactionManager(transactionManager());
    return factory.getObject();
  }

  @Bean
  public JobLauncher jobLauncher() throws Exception {
    SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
    jobLauncher.setJobRepository(jobRepository());
    jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
    jobLauncher.afterPropertiesSet();
    return jobLauncher;
  }

  @Bean
  @Qualifier("exp")
  public CalculateExpedienteTasklet calculateExpedienteStepTasklet() {
    return new CalculateExpedienteTasklet();
  }

  @Bean
  @Qualifier("CR")
  public CalculateCRTasklet calculateCRStepTasklet() {
    return new CalculateCRTasklet();
  }

  @Bean
  @Qualifier("responsableCartera")
  public AsignarResponsableCarteraTasklet responsableCarteraTasklet() {
    return new AsignarResponsableCarteraTasklet();
  }

  @Bean
  @Qualifier("segmentacion")
  public AsignarSegunSegmentacionTasklet segmentacionTasklet() {
    return new AsignarSegunSegmentacionTasklet();
  }

  @Bean("contratoVacio")
  public ControlFicheroVacioTasklet controlContratoVacioTasklet() {
    //fichero de contratos
    String fileName = csvsFolder + myList.get(1) + ".csv";
    //String fileName = "C:\\Pruebas\\archivos\\CONTRATO.csv";
    ControlFicheroVacioTasklet controlFicheroContratoTasklet = new ControlFicheroVacioTasklet();
    controlFicheroContratoTasklet.setFileName(fileName);
    return controlFicheroContratoTasklet;

  }

  @Bean("intervinienteVacio")
  public ControlFicheroVacioTasklet controlIntervinienteVacioTasklet() {
    String fileName = csvsFolder + myList.get(3) + ".csv";
    //String fileName = "C:\\Pruebas\\archivos\\INTERVINENTE.csv";
    ControlFicheroVacioTasklet controlFicheroContratoTasklet = new ControlFicheroVacioTasklet();
    controlFicheroContratoTasklet.setFileName(fileName);
    return controlFicheroContratoTasklet;

  }

  @Bean("borradoInterviniente")
  public BorradoIntervinienteTasklet borradoIntervinienteTasklet() {
    return new BorradoIntervinienteTasklet();
  }

  @Bean("borradoEspejo")
  @StepScope
  public BorradoEspejoTasklet borradoEspejoTasklet() {
    BorradoEspejoTasklet borradoEspejoTasklet = new BorradoEspejoTasklet();
    return borradoEspejoTasklet;

  }

  @Bean("validaciones")
  @StepScope
  public LogTasklet logTasklet() {
    LogTasklet logTasklet = new LogTasklet();
    logTasklet.setBienEspejoRepository(bienEspejoRepository);
    logTasklet.setContratoEspejoRepository(contratoEspejoRepository);
    logTasklet.setIntervinienteEspejoRepository(intervinienteEspejoRepository);
    logTasklet.setTasacionEspejoRepository(tasacionEspejoRepository);
    logTasklet.setCarteraRepository(carteraRepository);
    logTasklet.setEstadoContratoRepository(estadoContratoRepository);

    return logTasklet;

  }

  @Bean("borradoDeltas")
  @StepScope
  public BorradoDeltasTasklet borradoDeltas() {
    BorradoDeltasTasklet borradoDeltasTasklet = new BorradoDeltasTasklet();
    borradoDeltasTasklet.setBienRepository(bienRepository);
    borradoDeltasTasklet.setContratoRepository(contratoRepository);
    borradoDeltasTasklet.setIntervinienteRepository(intervinienteRepository);
    borradoDeltasTasklet.setCarteraRepository(carteraRepository);
    borradoDeltasTasklet.setEstadoContratoRepository(estadoContratoRepository);
    return borradoDeltasTasklet;

  }


  /***** READERS *****/

  //@Bean
  @Qualifier("expedienteReader")
  @StepScope
  public ItemReader<Expediente> expedienteReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<Expediente> csvFileReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    csvFileReader.setResource(new FileSystemResource(filename));
    //	csvFileReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\EXPEDIENTE.csv"));
    csvFileReader.setLinesToSkip(1);

    LineMapper<Expediente> expedienteLineMapper = createExpedienteLineMapper();
    csvFileReader.setLineMapper(expedienteLineMapper);

    return csvFileReader;
  }

  //@Bean
  @Qualifier("expedienteEspejoReader")
  @StepScope
  public ItemReader<ExpedienteEspejo> expedienteEspejoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<ExpedienteEspejo> csvFileReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    csvFileReader.setResource(new FileSystemResource(filename));
    //	csvFileReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\EXPEDIENTE.csv"));
    csvFileReader.setLinesToSkip(1);

    LineMapper<ExpedienteEspejo> expedienteLineMapper = createExpedienteEspejoLineMapper();
    csvFileReader.setLineMapper(expedienteLineMapper);

    return csvFileReader;
  }

  //@Bean
  @Qualifier("contratoReader")
  @StepScope
  public ItemReader<Contrato> contratoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<Contrato> csvFileReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    csvFileReader.setResource(new FileSystemResource(filename));
    //	csvFileReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\CONTRATO.csv"));
    csvFileReader.setLinesToSkip(1);

    LineMapper<Contrato> contratoLineMapper = createContratoLineMapper();
    csvFileReader.setLineMapper(contratoLineMapper);

    return csvFileReader;
  }

  //@Bean
  @Qualifier("contratoEspejoReader")
  @StepScope
  public ItemReader<ContratoEspejo> contratoEspejoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<ContratoEspejo> csvFileReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    csvFileReader.setResource(new FileSystemResource(filename));
    //	csvFileReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\CONTRATO.csv"));
    csvFileReader.setLinesToSkip(1);

    LineMapper<ContratoEspejo> contratoEspejoLineMapper = createContratoEspejoLineMapper();
    csvFileReader.setLineMapper(contratoEspejoLineMapper);

    return csvFileReader;
  }

  //@Bean
  @Qualifier("movimientoReader")
  @StepScope
  public ItemReader<Movimiento> movimientoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<Movimiento> movimientoReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    movimientoReader.setResource(new FileSystemResource(filename));
    //movimientoReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\MOVIMIENTO.csv"));
    movimientoReader.setLinesToSkip(1);

    LineMapper<Movimiento> movimientoLineMapper = createMovimientoLineMapper();
    movimientoReader.setLineMapper(movimientoLineMapper);

    return movimientoReader;
  }

  //@Bean
  @Qualifier("movimientoEspejoReader")
  @StepScope
  public ItemReader<MovimientoEspejo> movimientoEspejoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<MovimientoEspejo> movimientoReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    movimientoReader.setResource(new FileSystemResource(filename));
    //movimientoReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\MOVIMIENTO.csv"));
    movimientoReader.setLinesToSkip(1);

    LineMapper<MovimientoEspejo> movimientoLineMapper = createMovimientoEspejoLineMapper();
    movimientoReader.setLineMapper(movimientoLineMapper);

    return movimientoReader;
  }

  //@Bean
  @Qualifier("intervinienteReader")
  @StepScope
  public RepositoryItemReader<IntervinienteEspejo> intervinienteReader() {
    RepositoryItemReader<IntervinienteEspejo> reader = new RepositoryItemReader<>();
    reader.setRepository(intervinienteEspejoRepository);
    reader.setMethodName("findAll");
    reader.setSort(Collections.singletonMap("id", Sort.Direction.ASC));
    return reader;
  }

  //@Bean
  @Qualifier("intervinienteEspejoReader")
  @StepScope
  public ItemReader<IntervinienteEspejo> intervinienteEspejoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<IntervinienteEspejo> intervinienteReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    intervinienteReader.setResource(new FileSystemResource(filename));
    //intervinienteReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\INTERVINIENTE.csv"));
    intervinienteReader.setLinesToSkip(1);

    LineMapper<IntervinienteEspejo> intervinienteLineMapper = createIntervinienteEspejoLineMapper();
    intervinienteReader.setLineMapper(intervinienteLineMapper);

    return intervinienteReader;
  }

  //@Bean
  @Qualifier("direccionReader")
  @StepScope
  public RepositoryItemReader<DireccionEspejo> direccionReader() {
    RepositoryItemReader<DireccionEspejo> reader = new RepositoryItemReader<>();
    reader.setRepository(direccionEspejoRepository);
    reader.setMethodName("findAll");
    reader.setSort(Collections.singletonMap("id", Sort.Direction.ASC));
    return reader;
  }


  //@Bean
  @Qualifier("direccionEspejoReader")
  @StepScope
  public ItemReader<DireccionEspejo> direccionEspejoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<DireccionEspejo> direccionReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    direccionReader.setResource(new FileSystemResource(filename));
    //	direccionReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\DIRECCION.csv"));
    direccionReader.setLinesToSkip(1);

    LineMapper<DireccionEspejo> direccionLineMapper = createDireccionEspejoLineMapper();
    direccionReader.setLineMapper(direccionLineMapper);

    return direccionReader;
  }

  //@Bean
  @Qualifier("contactoReader")
  @StepScope
  public RepositoryItemReader<DatoContactoEspejo> contactoReader() {
    RepositoryItemReader<DatoContactoEspejo> reader = new RepositoryItemReader<>();
    reader.setRepository(datoContactoEspejoRepository);
    reader.setMethodName("findAll");
    reader.setSort(Collections.singletonMap("id", Sort.Direction.ASC));
    return reader;
  }

  //@Bean
  @Qualifier("contactoEspejoReader")
  @StepScope
  public ItemReader<DatoContactoEspejo> contactoEspejoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<DatoContactoEspejo> contactoReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    contactoReader.setResource(new FileSystemResource(filename));
    //contactoReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\CONTACTO.csv"));
    contactoReader.setLinesToSkip(1);

    LineMapper<DatoContactoEspejo> contactoLineMapper = createContactoEspejoLineMapper();
    contactoReader.setLineMapper(contactoLineMapper);

    return contactoReader;
  }

  //@Bean
  @Qualifier("bienReader")
  @StepScope
  public RepositoryItemReader<BienEspejo> bienReader() {
    RepositoryItemReader<BienEspejo> reader = new RepositoryItemReader<>();
    reader.setRepository(bienEspejoRepository);
    reader.setMethodName("findAll");
    reader.setSort(Collections.singletonMap("id", Sort.Direction.ASC));
    return reader;
  }

  //@Bean
  @Qualifier("bienEspejoReader")
  @StepScope
  public ItemReader<BienEspejo> bienEspejoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<BienEspejo> bienEspejoReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    bienEspejoReader.setResource(new FileSystemResource(filename));
    //bienReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\BIEN.csv"));
    bienEspejoReader.setLinesToSkip(1);

    LineMapper<BienEspejo> bienLineMapper = createBienEspejoLineMapper();
    bienEspejoReader.setLineMapper(bienLineMapper);

    return bienEspejoReader;
  }

  //@Bean
  @Qualifier("cargaReader")
  @StepScope
  public RepositoryItemReader<CargaEspejo> cargaReader() {
    RepositoryItemReader<CargaEspejo> reader = new RepositoryItemReader<>();
    reader.setRepository(cargaEspejoRepository);
    reader.setMethodName("findAll");
    reader.setSort(Collections.singletonMap("id", Sort.Direction.ASC));
    return reader;
  }

  //@Bean
  @Qualifier("cargaEspejoReader")
  @StepScope
  public ItemReader<CargaEspejo> cargaEspejoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<CargaEspejo> cargaReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    cargaReader.setResource(new FileSystemResource(filename));
    //cargaReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\CARGA.csv"));
    cargaReader.setLinesToSkip(1);

    LineMapper<CargaEspejo> cargaLineMapper = createCargaEspejoLineMapper();
    cargaReader.setLineMapper(cargaLineMapper);

    return cargaReader;
  }

  //@Bean
  @Qualifier("tasacionReader")
  @StepScope
  public RepositoryItemReader<DireccionEspejo> tasacionReader() {
    RepositoryItemReader<DireccionEspejo> reader = new RepositoryItemReader<>();
    reader.setRepository(tasacionEspejoRepository);
    reader.setMethodName("findAll");
    reader.setSort(Collections.singletonMap("id", Sort.Direction.ASC));
    return reader;
  }

  //@Bean
  @Qualifier("tasacionEspejoReader")
  @StepScope
  public ItemReader<TasacionEspejo> tasacionEspejoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<TasacionEspejo> tasacionReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    tasacionReader.setResource(new FileSystemResource(filename));
    //tasacionReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\TASACION.csv"));
    tasacionReader.setLinesToSkip(1);

    LineMapper<TasacionEspejo> tasacionLineMapper = createTasacionEspejoLineMapper();
    tasacionReader.setLineMapper(tasacionLineMapper);

    return tasacionReader;
  }

  //@Bean
  @Qualifier("valoracionReader")
  @StepScope
  public RepositoryItemReader<ValoracionEspejo> valoracionReader() {
    RepositoryItemReader<ValoracionEspejo> reader = new RepositoryItemReader<>();
    reader.setRepository(valoracionEspejoRepository);
    reader.setMethodName("findAll");
    reader.setSort(Collections.singletonMap("id", Sort.Direction.ASC));
    return reader;
  }

  //@Bean
  @Qualifier("valoracionEspejoReader")
  @StepScope
  public ItemReader<ValoracionEspejo> valoracionEspejoReader(List<String> myList, int index, String pathCsvsFolder) {
    FlatFileItemReader<ValoracionEspejo> valoracionReader = new FlatFileItemReader<>();
    String filename = pathCsvsFolder + myList.get(index) + ".csv";
    valoracionReader.setResource(new FileSystemResource(filename));
    //valoracionReader.setResource(new FileSystemResource("C:\\Pruebas\\archivos\\VALORACION.csv"));
    valoracionReader.setLinesToSkip(1);

    LineMapper<ValoracionEspejo> valoracionLineMapper = createValoracionEspejoLineMapper();
    valoracionReader.setLineMapper(valoracionLineMapper);

    return valoracionReader;
  }

  /***** PROCESORS *****/
  @Bean
  @Qualifier("valoracionProcessor")
  public ItemProcessor<ValoracionEspejo, Valoracion> valoracionProcessor() {
    return new ValoracionProcessor();
  }

  @Bean
  @Qualifier("cargaProcessor")
  public ItemProcessor<CargaEspejo, Carga> cargaProcessor() {
    return new CargaProcessor();
  }

  @Bean
  @Qualifier("tasacionProcessor")
  public ItemProcessor<TasacionEspejo, Tasacion> tasacionProcessor() {
    return new TasacionProcessor();
  }

  @Bean
  @Qualifier("expedienteProcessor")
  public ItemProcessor<Expediente, Expediente> expedienteProcessor() {
    return new ExpedienteProcessor();
  }

  @Bean
  @Qualifier("expedienteEspejoProcessor")
  public ItemProcessor<ExpedienteEspejo, ExpedienteEspejo> expedienteEspejoProcessor() {
    return new ExpedienteEspejoProcessor();
  }

  @Bean
  @Qualifier("contratoProcessor")
  public ItemProcessor<Contrato, Contrato> contratoProcessor() {
    return new ContratoProcessor();
  }

  @Bean
  @Qualifier("contratoEspejoProcessor")
  public ItemProcessor<ContratoEspejo, ContratoEspejo> contratoEspejoProcessor() {
    return new ContratoEspejoProcessor();
  }

  @Bean
  @Qualifier("movimientoProcessor")
  public ItemProcessor<Movimiento, Movimiento> movimientoProcessor() {
    return new MovimientoProcessor();
  }

  @Bean
  @Qualifier("movimientoEspejoProcessor")
  public ItemProcessor<MovimientoEspejo, MovimientoEspejo> movimientoEspejoProcessor() {
    return new MovimientoEspejoProcessor();
  }

  @Bean
  @Qualifier("intervinienteProcessor")
  public ItemProcessor<IntervinienteEspejo, Interviniente> intervinienteProcessor() {
    return new IntervinienteProcessor();
  }

  @Bean
  @Qualifier("intervinienteEspejoProcessor")
  public ItemProcessor<IntervinienteEspejo, IntervinienteEspejo> intervinienteEspejoProcessor() {
    return new IntervinienteEspejoProcessor();
  }

  @Bean
  @Qualifier("direccionProcessor")

  public ItemProcessor<DireccionEspejo, Direccion> direccionProcessor() {
    return new DireccionProcessor();
  }

  @Bean
  @Qualifier("direccionEspejoProcessor")

  public ItemProcessor<DireccionEspejo, DireccionEspejo> direccionEspejoProcessor() {
    return new DireccionEspejoProcessor();
  }

  @Bean
  @Qualifier("contactoProcessor")

  public ItemProcessor<DatoContactoEspejo, DatoContacto> contactoProcessor() {
    return new ContactoProcessor();
  }

  @Bean
  @Qualifier("contactoEspejoProcessor")

  public ItemProcessor<DatoContactoEspejo, DatoContactoEspejo> contactoEspejoProcessor() {
    return new ContactoEspejoProcessor();
  }

  @Bean
  @Qualifier("bienProcessor")
  public ItemProcessor<BienEspejo, Bien> bienProcessor() {
    return new BienProcessor();
  }

  @Bean
  @Qualifier("bienEspejoProcessor")

  public ItemProcessor<BienEspejo, BienEspejo> bienEspejoProcessor() {
    return new BienEspejoProcessor();
  }

  /**
   * **** WRITERS *****
   **/
  @Bean
  @Qualifier("expedienteWriter")
  public ItemWriter<Expediente> jpaExpedienteWriter() {
    JpaItemWriter<Expediente> writer = new JpaItemWriter<Expediente>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("expedienteEspejoWriter")
  public ItemWriter<ExpedienteEspejo> jpaExpedienteEspejoWriter() {
    JpaItemWriter<ExpedienteEspejo> writer = new JpaItemWriter<ExpedienteEspejo>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("contratoWriter")
  public ItemWriter<Contrato> jpaContratoWriter() {
    JpaItemWriter<Contrato> writer = new JpaItemWriter<Contrato>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("contratoEspejoWriter")
  public ItemWriter<ContratoEspejo> jpaContratoEspejoWriter() {
    JpaItemWriter<ContratoEspejo> writer = new JpaItemWriter<ContratoEspejo>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("movimientoWriter")
  public ItemWriter<Movimiento> jpaMovimientoWriter() {
    JpaItemWriter<Movimiento> writer = new JpaItemWriter<Movimiento>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("movimientoEspejoWriter")
  public ItemWriter<MovimientoEspejo> jpaMovimientoEspejoWriter() {
    JpaItemWriter<MovimientoEspejo> writer = new JpaItemWriter<MovimientoEspejo>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("intervinienteWriter")
  public RepositoryItemWriter<Interviniente> repositoryIntervinienteWriter() {
    RepositoryItemWriter<Interviniente> writer = new RepositoryItemWriter<>();
    writer.setRepository(intervinienteRepository);
    writer.setMethodName("save");
    return writer;
  }

  @Bean
  @Qualifier("intervinienteEspejoWriter")
  public ItemWriter<IntervinienteEspejo> jpaIntervinienteEspejoWriter() {
    JpaItemWriter<IntervinienteEspejo> writer = new JpaItemWriter<IntervinienteEspejo>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("direccionWriter")
  public RepositoryItemWriter<Direccion> repositoryDireccionWriter() {
    RepositoryItemWriter<Direccion> writer = new RepositoryItemWriter<>();
    writer.setRepository(direccionRepository);
    writer.setMethodName("save");
    return writer;
  }

  @Bean
  @Qualifier("direccionEspejoWriter")
  public ItemWriter<DireccionEspejo> jpaDireccionEspejoWriter() {
    JpaItemWriter<DireccionEspejo> writer = new JpaItemWriter<DireccionEspejo>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("contactoWriter")
  public RepositoryItemWriter<DatoContacto> repositoryContactoWriter() {
    RepositoryItemWriter<DatoContacto> writer = new RepositoryItemWriter<>();
    writer.setRepository(datoContactoRepository);
    writer.setMethodName("save");
    return writer;
  }

  @Bean
  @Qualifier("contactoEspejoWriter")
  public ItemWriter<DatoContactoEspejo> jpaContactoEspejoWriter() {
    JpaItemWriter<DatoContactoEspejo> writer = new JpaItemWriter<DatoContactoEspejo>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("bienWriter")
  public RepositoryItemWriter<Bien> repositoryBienWriter() {
    RepositoryItemWriter<Bien> writer = new RepositoryItemWriter<>();
    writer.setRepository(bienRepository);
    writer.setMethodName("save");
    return writer;
  }

  @Bean
  @Qualifier("bienEspejoWriter")
  public ItemWriter<BienEspejo> jpaBienEspejoWriter() {
    JpaItemWriter<BienEspejo> writer = new JpaItemWriter<BienEspejo>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("cargaWriter")
  public RepositoryItemWriter<Carga> repositoryCargaWriter() {
    RepositoryItemWriter<Carga> writer = new RepositoryItemWriter<>();
    writer.setRepository(cargaRepository);
    writer.setMethodName("save");
    return writer;
  }

  @Bean
  @Qualifier("cargaEspejoWriter")
  public ItemWriter<CargaEspejo> jpaCargaEspejoWriter() {
    JpaItemWriter<CargaEspejo> writer = new JpaItemWriter<CargaEspejo>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("tasacionWriter")
  public RepositoryItemWriter<Tasacion> repositoryTasacionWriter() {
    RepositoryItemWriter<Tasacion> writer = new RepositoryItemWriter<>();
    writer.setRepository(tasacionRepository);
    writer.setMethodName("save");
    return writer;
  }

  @Bean
  @Qualifier("tasacionEspejoWriter")
  public ItemWriter<TasacionEspejo> jpaTasacionEspejoWriter() {
    JpaItemWriter<TasacionEspejo> writer = new JpaItemWriter<TasacionEspejo>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  @Bean
  @Qualifier("valoracionWriter")
  public RepositoryItemWriter<Valoracion> repositoryValoracionWriter() {
    RepositoryItemWriter<Valoracion> writer = new RepositoryItemWriter<>();
    writer.setRepository(valoracionRepository);
    writer.setMethodName("save");
    return writer;
  }

  @Bean
  @Qualifier("valoracionEspejoWriter")
  public ItemWriter<ValoracionEspejo> jpaValoracionEspejoWriter() {
    JpaItemWriter<ValoracionEspejo> writer = new JpaItemWriter<ValoracionEspejo>();
    writer.setEntityManagerFactory(entityManagerFactory);
    return writer;
  }

  /***** STEPS *****/
  @Bean
  public Step ficheroContrato(@Qualifier("contratoReader") ItemReader reader,
                              @Qualifier("contratoProcessor") ItemProcessor processor,
                              @Qualifier("contratoWriter") ItemWriter<Contrato> writer) {
    return stepBuilderFactory.get("contratoStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer).build();
  }

  @Bean
  public Step ficheroExpediente(@Qualifier("expedienteReader") ItemReader reader,
                                @Qualifier("expedienteProcessor") ItemProcessor processor,
                                @Qualifier("expedienteWriter") ItemWriter<Expediente> writer) {
    return stepBuilderFactory.get("expedienteStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer).build();
  }

  @Bean

  public Step ficheroMovimiento(@Qualifier("movimientoReader") ItemReader reader,
                                @Qualifier("movimientoProcessor") ItemProcessor processor,
                                @Qualifier("movimientoWriter") ItemWriter<Movimiento> writer) {
    return stepBuilderFactory.get("movimientoStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean

  public Step ficheroInterviniente(@Qualifier("intervinienteReader") ItemReader reader,
                                   @Qualifier("intervinienteProcessor") ItemProcessor processor,
                                   @Qualifier("intervinienteWriter") ItemWriter<Interviniente> writer) {
    return stepBuilderFactory.get("intervinienteStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean

  public Step ficheroDireccion(@Qualifier("direccionReader") ItemReader reader,
                               @Qualifier("direccionProcessor") ItemProcessor processor,
                               @Qualifier("direccionWriter") ItemWriter<Direccion> writer) {
    return stepBuilderFactory.get("direccionStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean

  public Step ficheroContacto(@Qualifier("contactoReader") ItemReader reader,
                              @Qualifier("contactoProcessor") ItemProcessor processor,
                              @Qualifier("contactoWriter") ItemWriter<DatoContacto> writer) {
    return stepBuilderFactory.get("contactoStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean

  public Step ficheroBien(@Qualifier("bienReader") ItemReader reader,
                          @Qualifier("bienProcessor") ItemProcessor processor,
                          @Qualifier("bienWriter") ItemWriter<Bien> writer) {
    return stepBuilderFactory.get("bienStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean
  public Step ficheroCarga(@Qualifier("cargaReader") ItemReader reader,
                           @Qualifier("contactoProcessor") ItemProcessor processor,
                           @Qualifier("cargaWriter") ItemWriter writer) {
    return stepBuilderFactory.get("cargaStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean
  public Step ficheroTasacion(@Qualifier("tasacionReader") ItemReader reader,
                              @Qualifier("contactoProcessor") ItemProcessor processor,
                              @Qualifier("tasacionWriter") ItemWriter writer) {
    return stepBuilderFactory.get("tasacionStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean
  public Step ficheroValoracion(@Qualifier("valoracionReader") ItemReader reader,
                                @Qualifier("contactoProcessor") ItemProcessor processor,
                                @Qualifier("valoracionWriter") ItemWriter writer) {
    return stepBuilderFactory.get("valoracionStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  /**
   * TABLAS ESPEJO
   */
  @Bean
  public Step ficheroContratoEspejo(@Qualifier("contratoEspejoReader") ItemReader reader,
                                    @Qualifier("contratoEspejoProcessor") ItemProcessor processor,
                                    @Qualifier("contratoEspejoWriter") ItemWriter<ContratoEspejo> writer) {
    return stepBuilderFactory.get("contratoEspejoStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer).build();
  }

  @Bean
  public Step ficheroExpedienteEspejo(@Qualifier("expedienteEspejoReader") ItemReader reader,
                                      @Qualifier("expedienteEspejoProcessor") ItemProcessor processor,
                                      @Qualifier("expedienteEspejoWriter") ItemWriter<ExpedienteEspejo> writer) {
    return stepBuilderFactory.get("expedienteEspejoStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer).build();
  }

  @Bean
  public Step ficheroMovimientoEspejo(@Qualifier("movimientoEspejoReader") ItemReader reader,
                                      @Qualifier("movimientoEspejoProcessor") ItemProcessor processor,
                                      @Qualifier("movimientoEspejoWriter") ItemWriter<MovimientoEspejo> writer) {
    return stepBuilderFactory.get("movimientoEspejoStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean
  public Step ficheroIntervinienteEspejo(@Qualifier("intervinienteEspejoReader") ItemReader reader,
                                         @Qualifier("intervinienteEspejoProcessor") ItemProcessor processor,
                                         @Qualifier("intervinienteEspejoWriter") ItemWriter<IntervinienteEspejo> writer) {
    return stepBuilderFactory.get("intervinienteEspejoStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean
  public Step ficheroDireccionEspejo(@Qualifier("direccionEspejoReader") ItemReader reader,
                                     @Qualifier("direccionEspejoProcessor") ItemProcessor processor,
                                     @Qualifier("direccionEspejoWriter") ItemWriter writer) {
    return stepBuilderFactory.get("direccionEspejoStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean
  public Step ficheroContactoEspejo(@Qualifier("contactoEspejoReader") ItemReader reader,
                                    @Qualifier("contactoEspejoProcessor") ItemProcessor processor,
                                    @Qualifier("contactoEspejoWriter") ItemWriter writer) {
    return stepBuilderFactory.get("contactoEspejoStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean
  public Step ficheroBienEspejo(@Qualifier("bienEspejoReader") ItemReader reader,
                                @Qualifier("bienEspejoProcessor") ItemProcessor processor,
                                @Qualifier("bienEspejoWriter") ItemWriter<BienEspejo> writer) {
    return stepBuilderFactory.get("bienEspejoStep").chunk(1)
      .reader(reader)
      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean
  public Step ficheroCargaEspejo(@Qualifier("cargaEspejoReader") ItemReader reader,
//                                 @Qualifier("contactoProcessor") ItemProcessor processor,
                                 @Qualifier("cargaEspejoWriter") ItemWriter writer) {
    return stepBuilderFactory.get("cargaEspejoStep").chunk(1)
      .reader(reader)
//      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean
  public Step ficheroTasacionEspejo(@Qualifier("tasacionEspejoReader") ItemReader reader,
//                                    @Qualifier("contactoProcessor") ItemProcessor processor,
                                    @Qualifier("tasacionEspejoWriter") ItemWriter writer) {
    return stepBuilderFactory.get("tasacionEspejoStep").chunk(1)
      .reader(reader)
//      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean
  public Step ficheroValoracionEspejo(@Qualifier("valoracionEspejoReader") ItemReader reader,
                                      //    @Qualifier("contactoProcessor") ItemProcessor processor,
                                      @Qualifier("valoracionEspejoWriter") ItemWriter writer) {
    return stepBuilderFactory.get("valoracionEspejoStep").chunk(1)
      .reader(reader)
//      .processor(processor)
      .writer(writer)
      .build();
  }

  @Bean()
  @Qualifier("calculateExpedienteStep")
  public Step calculateExpedienteStep(@Qualifier("exp") Tasklet calculateExpedienteTasklet) {

    return stepBuilderFactory.get("calculateExpedienteStep").tasklet(calculateExpedienteTasklet).build();

  }

  @Bean()
  @Qualifier("asignarSegunSegmentacionStep")
  public Step asignarSegunSegmentacionStep(@Qualifier("segmentacion") Tasklet asignarSegunSegmentacionTasklet) {

    return stepBuilderFactory.get("asignarSegunSegmentacionStep").tasklet(asignarSegunSegmentacionTasklet).build();

  }

  @Bean()
  @Qualifier("asignarResponsableCarteraStep")
  public Step asignarResponsableCarteraStep(@Qualifier("responsableCartera") Tasklet asignarResponsableCarteraTasklet) {

    return stepBuilderFactory.get("asignarResponsableCarteraStep").tasklet(asignarResponsableCarteraTasklet).build();
  }

  @Bean()
  @Qualifier("borradoIntervinienteStep")
  public Step borradoIntervinienteStep(@Qualifier("borradoInterviniente") Tasklet borradoIntervinienteTasklet) {
    return stepBuilderFactory.get("borradoIntervinienteStep").tasklet(borradoIntervinienteTasklet).build();
  }

  @Bean()
  @Qualifier("calculateCRStep")
  public Step calculateCRStep(@Qualifier("CR") Tasklet calculateCRTasklet) {

    return stepBuilderFactory.get("calculateCRStep").tasklet(calculateCRTasklet).build();
  }

  @Bean("contratoVacioStep")
  public Step ficheroContratosVacioStep(@Qualifier("contratoVacio") Tasklet controlFicheroVacioTasklet) {

    return stepBuilderFactory.get("ficheroContratosVacioStep").tasklet(controlFicheroVacioTasklet).build();
  }

  @Bean("intervinienteVacioStep")
  public Step ficheroIntervinienteVacioStep(@Qualifier("intervinienteVacio") Tasklet controlIntervinienteVacioTasklet) {
    return stepBuilderFactory.get("ficheroIntervinienteVacioStep").tasklet(controlIntervinienteVacioTasklet).build();
  }

  @Bean("borradoEspejoStep")
  public Step borradoTablaEspejoStep(@Qualifier("borradoEspejo") Tasklet borradoEspejoTasklet) {
    return stepBuilderFactory.get("borradoTablaEspejoStep").tasklet(borradoEspejoTasklet).build();
  }

  @Bean("logStep")
  //@JobScope
  public Step logStep(@Qualifier("validaciones") Tasklet logTasklet) {
    return stepBuilderFactory.get("logStep").tasklet(logTasklet).build();
  }

  @Bean("borradoDeltasStep")
  //@JobScope
  public Step borradoDeltasStep(@Qualifier("borradoDeltas") Tasklet borradoDeltasTasklet) {
    return stepBuilderFactory.get("borradoDeltasStep").tasklet(borradoDeltasTasklet).build();
  }


  /**
   * *** JOB ****
   **/
  @Bean(name = "provisioning")
  @Transactional
  public Job job(@Qualifier("calculateExpedienteStep") Step calculateExpedienteStep
    , @Qualifier("calculateCRStep") Step calculateContratoRepresentanteStep
    , @Qualifier("contratoVacioStep") Step ficheroContratosVacioStep
    , @Qualifier("intervinienteVacioStep") Step ficheroIntervinienteVacioStep
    , @Qualifier("borradoEspejoStep") Step borradoTablaEspejoStep
    , @Qualifier("logStep") Step logStep
    , @Qualifier("borradoDeltasStep") Step borradoDeltasStep
    , @Qualifier("asignarSegunSegmentacionStep") Step asignarSegunSegmentacionStep
    , @Qualifier("asignarResponsableCarteraStep") Step asignarResponsableCarteraStep
    , @Qualifier("borradoIntervinienteStep") Step borradoIntervinienteStep
  ) throws FileNotFoundException {
    return jobBuilderFactory.get("provisioning")
      .incrementer(new RunIdIncrementer())
      .start(ficheroContratosVacioStep)
      .next(ficheroIntervinienteVacioStep)
      .next(borradoTablaEspejoStep)
      .next(ficheroExpedienteEspejo(expedienteEspejoReader(myList, 0, csvsFolder), expedienteEspejoProcessor(), jpaExpedienteEspejoWriter()))
      .next(ficheroContratoEspejo(contratoEspejoReader(myList, 1, csvsFolder), contratoEspejoProcessor(), jpaContratoEspejoWriter()))
      .next(ficheroMovimientoEspejo(movimientoEspejoReader(myList, 2, csvsFolder), movimientoEspejoProcessor(), jpaMovimientoEspejoWriter()))
      .next(ficheroIntervinienteEspejo(intervinienteEspejoReader(myList, 3, csvsFolder), intervinienteEspejoProcessor(), jpaIntervinienteEspejoWriter()))
      .next(ficheroDireccionEspejo(direccionEspejoReader(myList, 4, csvsFolder), direccionEspejoProcessor(), jpaDireccionEspejoWriter()))
      .next(ficheroContactoEspejo(contactoEspejoReader(myList, 5, csvsFolder), contactoEspejoProcessor(), jpaContactoEspejoWriter()))
      .next(ficheroBienEspejo(bienEspejoReader(myList, 6, csvsFolder), bienEspejoProcessor(), jpaBienEspejoWriter()))
      .next(ficheroCargaEspejo(cargaEspejoReader(myList, 7, csvsFolder), jpaCargaEspejoWriter()))
      .next(ficheroTasacionEspejo(tasacionEspejoReader(myList, 8, csvsFolder), jpaTasacionEspejoWriter()))
      .next(ficheroValoracionEspejo(valoracionEspejoReader(myList, 9, csvsFolder), jpaValoracionEspejoWriter()))
      .next(logStep)
      .next(decider).on("FAILED").end()
      .next(borradoDeltasStep)
      .next(ficheroExpediente(expedienteReader(myList, 0, csvsFolder), expedienteProcessor(), jpaExpedienteWriter()))
      .next(ficheroContrato(contratoReader(myList, 1, csvsFolder), contratoProcessor(), jpaContratoWriter()))
      .next(ficheroMovimiento(movimientoReader(myList, 2, csvsFolder), movimientoProcessor(), jpaMovimientoWriter()))
      .next(ficheroInterviniente(intervinienteReader(), intervinienteProcessor(), repositoryIntervinienteWriter()))
      .next(ficheroDireccion(direccionReader(), direccionProcessor(), repositoryDireccionWriter()))
      .next(ficheroContacto(contactoReader(), contactoProcessor(), repositoryContactoWriter()))
      .next(ficheroBien(bienReader(), bienProcessor(), repositoryBienWriter()))
      .next(ficheroCarga(cargaReader(), cargaProcessor(), repositoryCargaWriter()))
      .next(ficheroTasacion(tasacionReader(), tasacionProcessor(), repositoryTasacionWriter()))
      .next(ficheroValoracion(valoracionReader(), valoracionProcessor(), repositoryValoracionWriter()))
      .next(borradoIntervinienteStep)
      .next(calculateExpedienteStep)
      .next(calculateContratoRepresentanteStep)
      .next(asignarResponsableCarteraStep)
      .next(asignarSegunSegmentacionStep) //TODO No genera bien las segmentaciones, se comenta para la subida del 17/06. Hablar con jesus
      .end()
      .build();
  }

  private LineMapper<Contrato> createContratoLineMapper() {
    DefaultLineMapper<Contrato> contratoLineMapper = new DefaultLineMapper<>();

    LineTokenizer contratoLineTokenizer = createContratoLineTokenizer();
    contratoLineMapper.setLineTokenizer(contratoLineTokenizer);

    FieldSetMapper<Contrato> contratoInformationMapper = createContratoInformationMapper();
    contratoLineMapper.setFieldSetMapper(contratoInformationMapper);

    return contratoLineMapper;
  }

  private LineMapper<ContratoEspejo> createContratoEspejoLineMapper() {
    DefaultLineMapper<ContratoEspejo> contratoLineMapper = new DefaultLineMapper<>();

    LineTokenizer contratoLineTokenizer = createContratoLineTokenizer();
    contratoLineMapper.setLineTokenizer(contratoLineTokenizer);

    FieldSetMapper<ContratoEspejo> contratoInformationMapper = createContratoEspejoInformationMapper();
    contratoLineMapper.setFieldSetMapper(contratoInformationMapper);

    return contratoLineMapper;
  }

  private LineTokenizer createContratoLineTokenizer() {
    DelimitedLineTokenizer contratoLineTokenizer = new DelimitedLineTokenizer();
    contratoLineTokenizer.setDelimiter(";");
    contratoLineTokenizer.setNames(new String[]{"id", "expediente.id", "idOrigen", "estadoContrato",
      "producto.codigo", "sistemaAmortizacion.codigo", "periodicidadLiquidacion.codigo",
      "indiceReferencia.codigo", "diferencial", "tin", "tae", "rangoHipotecario", "reestructurado",
      "tipoReestructuracion.codigo", "clasificacionContable.codigo", "campania", "fechaFormalizacion",
      "fechaVencimientoInicial", "fechaVencimientoAnticipado", "fechaPaseRegular", "cuotasPendientes",
      "fechaLiquidacionCuota", "importeInicial", "importeDispuesto", "saldoContrato.capitalPendiente",
      "saldoContrato.saldoGestion", "cuotasTotales", "importeUltimaCuota", "fechaPrimerImpago",
      "numeroCuotasImpagadas", "deudaImpagada", "restoHipotecario", "origen",
      "saldoContrato.principalVencidoImpagado", "saldoContrato.interesesOrdinariosImpagados",
      "saldoContrato.interesesDemora", "saldoContrato.comisionesImpagados", "saldoContrato.gastosImpagados",
      "gastosJudiciales", "importeCuotasImpagadas", "notas1", "notas2"});
    return contratoLineTokenizer;
  }

  private FieldSetMapper<Contrato> createContratoInformationMapper() {
    ContratoFieldSetMapper contratoFieldSetMapper = new ContratoFieldSetMapper();
    contratoFieldSetMapper.setSistemaAmortizacionRepository(sistemaAmortizacionRepository);
    contratoFieldSetMapper.setClasificacionContableRepository(clasificacionContableRepository);
    contratoFieldSetMapper.setIndiceReferenciaRepository(indiceReferenciaRepository);
    contratoFieldSetMapper.setPeriodicidadLiquidacionRepository(periodicidadLiquidacionRepository);
    contratoFieldSetMapper.setProductoRepository(productoRepository);
    contratoFieldSetMapper.setTipoReestructuracionRepository(tipoReestructuracionRepository);
    contratoFieldSetMapper.setExpedienteRepository(expedienteRepository);
    contratoFieldSetMapper.setEstadoContratoRepository(estadoContratoRepository);
    return contratoFieldSetMapper;
  }

  private FieldSetMapper<ContratoEspejo> createContratoEspejoInformationMapper() {
    ContratoEspejoFieldSetMapper contratoFieldSetMapper = new ContratoEspejoFieldSetMapper();
    contratoFieldSetMapper.setSistemaAmortizacionRepository(sistemaAmortizacionRepository);
    contratoFieldSetMapper.setClasificacionContableRepository(clasificacionContableRepository);
    contratoFieldSetMapper.setIndiceReferenciaRepository(indiceReferenciaRepository);
    contratoFieldSetMapper.setPeriodicidadLiquidacionRepository(periodicidadLiquidacionRepository);
    contratoFieldSetMapper.setProductoRepository(productoRepository);
    contratoFieldSetMapper.setTipoReestructuracionRepository(tipoReestructuracionRepository);
    contratoFieldSetMapper.setExpedienteEspejoRepository(expedienteEspejoRepository);
    contratoFieldSetMapper.setEstadoContratoRepository(estadoContratoRepository);
    return contratoFieldSetMapper;
  }

  private LineMapper<Expediente> createExpedienteLineMapper() {
    DefaultLineMapper<Expediente> expedienteLineMapper = new DefaultLineMapper<>();

    LineTokenizer expedienteLineTokenizer = createExpedienteLineTokenizer();
    expedienteLineMapper.setLineTokenizer(expedienteLineTokenizer);

    FieldSetMapper<Expediente> expedienteInformationMapper = createExpedienteInformationMapper();
    expedienteLineMapper.setFieldSetMapper(expedienteInformationMapper);

    return expedienteLineMapper;
  }

  private LineMapper<ExpedienteEspejo> createExpedienteEspejoLineMapper() {
    DefaultLineMapper<ExpedienteEspejo> expedienteLineMapper = new DefaultLineMapper<>();

    LineTokenizer expedienteLineTokenizer = createExpedienteLineTokenizer();
    expedienteLineMapper.setLineTokenizer(expedienteLineTokenizer);

    FieldSetMapper<ExpedienteEspejo> expedienteInformationMapper = createExpedienteEspejoInformationMapper();
    expedienteLineMapper.setFieldSetMapper(expedienteInformationMapper);

    return expedienteLineMapper;
  }

  private LineTokenizer createExpedienteLineTokenizer() {
    DelimitedLineTokenizer expedienteLineTokenizer = new DelimitedLineTokenizer();
    expedienteLineTokenizer.setDelimiter(";");
    expedienteLineTokenizer.setNames(new String[]{"idCarga", "cartera.id", "idOrigen"});
    return expedienteLineTokenizer;
  }

  private FieldSetMapper<Expediente> createExpedienteInformationMapper() {
    ExpedienteFieldSetMapper expedienteFieldSetMapper = new ExpedienteFieldSetMapper();
    expedienteFieldSetMapper.setCarteraRepository(carteraRepository);
    return expedienteFieldSetMapper;
  }

  private FieldSetMapper<ExpedienteEspejo> createExpedienteEspejoInformationMapper() {
    ExpedienteEspejoFieldSetMapper expedienteFieldSetMapper = new ExpedienteEspejoFieldSetMapper();
    expedienteFieldSetMapper.setCarteraRepository(carteraRepository);
    return expedienteFieldSetMapper;
  }

  private LineMapper<Movimiento> createMovimientoLineMapper() {
    DefaultLineMapper<Movimiento> movimientoLineMapper = new DefaultLineMapper<>();

    LineTokenizer movimientoLineTokenizer = createMovimientoLineTokenizer();
    movimientoLineMapper.setLineTokenizer(movimientoLineTokenizer);

    FieldSetMapper<Movimiento> movimientoInformationMapper = createMovimientoInformationMapper();
    movimientoLineMapper.setFieldSetMapper(movimientoInformationMapper);

    return movimientoLineMapper;
  }

  private LineMapper<MovimientoEspejo> createMovimientoEspejoLineMapper() {
    DefaultLineMapper<MovimientoEspejo> movimientoLineMapper = new DefaultLineMapper<>();

    LineTokenizer movimientoLineTokenizer = createMovimientoLineTokenizer();
    movimientoLineMapper.setLineTokenizer(movimientoLineTokenizer);

    FieldSetMapper<MovimientoEspejo> movimientoInformationMapper = createMovimientoEspejoInformationMapper();
    movimientoLineMapper.setFieldSetMapper(movimientoInformationMapper);

    return movimientoLineMapper;
  }

  private LineTokenizer createMovimientoLineTokenizer() {
    DelimitedLineTokenizer movimientoLineTokenizer = new DelimitedLineTokenizer();
    movimientoLineTokenizer.setDelimiter(";");
    movimientoLineTokenizer.setNames(new String[]{"contrato.id", "tipoMovimiento.codigo",
      "descripcionMovimiento.codigo", "fechaValor", "fechaContable", "sentido", "importe",
      "principalCapital", "interesesOrdinarios", "interesesDemora", "comisiones", "gastos", "impuestos",
      "principalPendienteVencer", "deudaImpagada", "saldoGestion", "usuario", "importeRecuperado", "salidaDudoso",
      "importeFacturable", "numeroFactura", "detalleGasto", "id", "afecta", "estado", "comentarios"});
    return movimientoLineTokenizer;
  }

  private FieldSetMapper<Movimiento> createMovimientoInformationMapper() {
    MovimientoFieldSetMapper movimientoInformationMapper = new MovimientoFieldSetMapper();
    movimientoInformationMapper.setContratoRepository(contratoRepository);
    movimientoInformationMapper.setDescripcionMovimientoRepository(descripcionMovimientoRepository);
    movimientoInformationMapper.setTipoMovimientoRepository(tipoMovimientoRepository);
    return movimientoInformationMapper;
  }

  private FieldSetMapper<MovimientoEspejo> createMovimientoEspejoInformationMapper() {
    MovimientoEspejoFieldSetMapper movimientoInformationMapper = new MovimientoEspejoFieldSetMapper();
    movimientoInformationMapper.setContratoEspejoRepository(contratoEspejoRepository);
    movimientoInformationMapper.setDescripcionMovimientoRepository(descripcionMovimientoRepository);
    movimientoInformationMapper.setTipoMovimientoRepository(tipoMovimientoRepository);
    return movimientoInformationMapper;
  }

  private LineMapper<Interviniente> createIntervinienteLineMapper() {
    DefaultLineMapper<Interviniente> intervinienteLineMapper = new DefaultLineMapper<>();

    LineTokenizer intervinienteLineTokenizer = createIntervinienteLineTokenizer();
    intervinienteLineMapper.setLineTokenizer(intervinienteLineTokenizer);

    FieldSetMapper<Interviniente> intervinienteInformationMapper = createIntervinienteInformationMapper();
    intervinienteLineMapper.setFieldSetMapper(intervinienteInformationMapper);

    return intervinienteLineMapper;
  }

  private LineMapper<IntervinienteEspejo> createIntervinienteEspejoLineMapper() {
    DefaultLineMapper<IntervinienteEspejo> intervinienteLineMapper = new DefaultLineMapper<>();

    LineTokenizer intervinienteLineTokenizer = createIntervinienteLineTokenizer();
    intervinienteLineMapper.setLineTokenizer(intervinienteLineTokenizer);

    FieldSetMapper<IntervinienteEspejo> intervinienteInformationMapper = createIntervinienteEspejoInformationMapper();
    intervinienteLineMapper.setFieldSetMapper(intervinienteInformationMapper);

    return intervinienteLineMapper;
  }

  private LineTokenizer createIntervinienteLineTokenizer() {
    DelimitedLineTokenizer intervinienteLineTokenizer = new DelimitedLineTokenizer();
    intervinienteLineTokenizer.setDelimiter(";");
    intervinienteLineTokenizer.setNames(new String[]{"id", "contrato.id", "idOrigen", "tipoIntervencion.codigo",
      "estado", "ordenIntervencion", "tipoDocumento.codigo", "numeroDocumento", "nombre", "apellidos", "sexo",
      "estadoCivil", "fechaNacimiento", "razonSocial", "fechaConstitucion", "residente", "pais.codigo", "pais.codigo",
      "tipoOcupacion.codigo", "empresa", "telefonoEmpresa", "contactoEmpresa", "tipoContrato.codigo",
      "codigoSocioProfesional", "cnae", "ingresosNetosMensuales", "descripcionActividad", "numHijosDependientes",
      "otrosRiesgos", "vulnerabilidad", "pensionista", "tipoPrestacion.codigo", "dependiente",
      "otrasSituacionesVulnerabilidad.codigo", "discapacitados", "notas1", "notas2"});
    return intervinienteLineTokenizer;
  }

  private FieldSetMapper<Interviniente> createIntervinienteInformationMapper() {
    IntervinienteFieldSetMapper intervinienteFieldSetMapper = new IntervinienteFieldSetMapper();
    intervinienteFieldSetMapper.setPaisRepository(paisRepository);
    intervinienteFieldSetMapper.setTipoContratoRepository(tipoContratoRepository);
    intervinienteFieldSetMapper.setTipoDocumentoRepository(tipoDocumentoRepository);
    intervinienteFieldSetMapper.setTipoIntervencionRepository(tipoIntervencionRepository);
    intervinienteFieldSetMapper.setTipoOcupacionRepository(tipoOcupacionRepository);
    intervinienteFieldSetMapper.setVulnerabilidadRepository(vulnerabilidadRepository);
    intervinienteFieldSetMapper.setContratoRepository(contratoRepository);
    intervinienteFieldSetMapper.setIntervinienteRepository(intervinienteRepository);
    intervinienteFieldSetMapper.setContratoIntervinienteRepository(contratoIntervinienteRepository);
    intervinienteFieldSetMapper.setTipoPrestacionRepository(tipoPrestacionRepository);
    intervinienteFieldSetMapper.setSexoRepository(sexoRepository);
    intervinienteFieldSetMapper.setEstadoCivilRepository(estadoCivilRepository);
    return intervinienteFieldSetMapper;
  }

  private FieldSetMapper<IntervinienteEspejo> createIntervinienteEspejoInformationMapper() {
    IntervinienteEspejoFieldSetMapper intervinienteFieldSetMapper = new IntervinienteEspejoFieldSetMapper();
    intervinienteFieldSetMapper.setPaisRepository(paisRepository);
    intervinienteFieldSetMapper.setTipoContratoRepository(tipoContratoRepository);
    intervinienteFieldSetMapper.setTipoDocumentoRepository(tipoDocumentoRepository);
    intervinienteFieldSetMapper.setTipoIntervencionRepository(tipoIntervencionRepository);
    intervinienteFieldSetMapper.setTipoOcupacionRepository(tipoOcupacionRepository);
    intervinienteFieldSetMapper.setVulnerabilidadRepository(vulnerabilidadRepository);
    intervinienteFieldSetMapper.setContratoEspejoRepository(contratoEspejoRepository);
    intervinienteFieldSetMapper.setIntervinienteEspejoRepository(intervinienteEspejoRepository);
    intervinienteFieldSetMapper.setContratoIntervinienteRepository(contratoIntervinienteRepository);
    intervinienteFieldSetMapper.setTipoPrestacionRepository(tipoPrestacionRepository);
    intervinienteFieldSetMapper.setSexoRepository(sexoRepository);
    intervinienteFieldSetMapper.setEstadoCivilRepository(estadoCivilRepository);
    return intervinienteFieldSetMapper;
  }

  private LineMapper<Direccion> createDireccionLineMapper() {
    DefaultLineMapper<Direccion> direccionLineMapper = new DefaultLineMapper<>();

    LineTokenizer direccionLineTokenizer = createDireccionLineTokenizer();
    direccionLineMapper.setLineTokenizer(direccionLineTokenizer);

    FieldSetMapper<Direccion> direccionInformationMapper = createDireccionInformationMapper();
    direccionLineMapper.setFieldSetMapper(direccionInformationMapper);

    return direccionLineMapper;
  }

  private LineMapper<DireccionEspejo> createDireccionEspejoLineMapper() {
    DefaultLineMapper<DireccionEspejo> direccionLineMapper = new DefaultLineMapper<>();

    LineTokenizer direccionLineTokenizer = createDireccionLineTokenizer();
    direccionLineMapper.setLineTokenizer(direccionLineTokenizer);

    FieldSetMapper<DireccionEspejo> direccionInformationMapper = createDireccionEspejoInformationMapper();
    direccionLineMapper.setFieldSetMapper(direccionInformationMapper);

    return direccionLineMapper;
  }

  private LineTokenizer createDireccionLineTokenizer() {
    DelimitedLineTokenizer direccionLineTokenizer = new DelimitedLineTokenizer();
    direccionLineTokenizer.setDelimiter(";");
    direccionLineTokenizer.setNames(new String[]{"interviniente.idCarga", "tipoVia.codigo", "nombre", "numero", "portal", "bloque",
      "escalera", "piso", "puerta", "entorno.codigo", "municipio.codigo", "localidad.codigo", "provincia.codigo",
      "comentario", "codigoPostal", "longitud", "latitud", "descripcion"});
    return direccionLineTokenizer;
  }

  private FieldSetMapper<Direccion> createDireccionInformationMapper() {
    DireccionFieldSetMapper direccionInformationMapper = new DireccionFieldSetMapper();
    direccionInformationMapper.setEntornoRepository(entornoRepository);
    direccionInformationMapper.setIntervinienteRepository(intervinienteRepository);
    direccionInformationMapper.setLocalidadRepository(localidadRepository);
    direccionInformationMapper.setMunicipioRepository(municipioRepository);
    direccionInformationMapper.setProvinciaRepository(provinciaRepository);
    direccionInformationMapper.setTipoViaRepository(tipoViaRepository);
    return direccionInformationMapper;
  }

  private FieldSetMapper<DireccionEspejo> createDireccionEspejoInformationMapper() {
    DireccionEspejoFieldSetMapper direccionInformationMapper = new DireccionEspejoFieldSetMapper();
    direccionInformationMapper.setEntornoRepository(entornoRepository);
    direccionInformationMapper.setIntervinienteEspejoRepository(intervinienteEspejoRepository);
    direccionInformationMapper.setLocalidadRepository(localidadRepository);
    direccionInformationMapper.setMunicipioRepository(municipioRepository);
    direccionInformationMapper.setProvinciaRepository(provinciaRepository);
    direccionInformationMapper.setTipoViaRepository(tipoViaRepository);
    return direccionInformationMapper;
  }

  private LineMapper<DatoContacto> createContactoLineMapper() {
    DefaultLineMapper<DatoContacto> contactoLineMapper = new DefaultLineMapper<>();

    LineTokenizer contactoLineTokenizer = createContactoLineTokenizer();
    contactoLineMapper.setLineTokenizer(contactoLineTokenizer);

    FieldSetMapper<DatoContacto> contactoInformationMapper = createContactoInformationMapper();
    contactoLineMapper.setFieldSetMapper(contactoInformationMapper);

    return contactoLineMapper;
  }

  private LineMapper<DatoContactoEspejo> createContactoEspejoLineMapper() {
    DefaultLineMapper<DatoContactoEspejo> contactoLineMapper = new DefaultLineMapper<>();

    LineTokenizer contactoLineTokenizer = createContactoLineTokenizer();
    contactoLineMapper.setLineTokenizer(contactoLineTokenizer);

    FieldSetMapper<DatoContactoEspejo> contactoInformationMapper = createContactoEspejoInformationMapper();
    contactoLineMapper.setFieldSetMapper(contactoInformationMapper);

    return contactoLineMapper;
  }

  private LineTokenizer createContactoLineTokenizer() {
    DelimitedLineTokenizer contactoLineTokenizer = new DelimitedLineTokenizer();
    contactoLineTokenizer.setDelimiter(";");
    contactoLineTokenizer.setNames(new String[]{"interviniente.id", "tipo.codigo", "valor", "orden.codigo", "descripcion"});
    return contactoLineTokenizer;
  }

  private FieldSetMapper<DatoContacto> createContactoInformationMapper() {
    ContactoFieldSetMapper contactoInformationMapper = new ContactoFieldSetMapper();
    contactoInformationMapper.setIntervinienteRepository(intervinienteRepository);
    return contactoInformationMapper;
  }

  private FieldSetMapper<DatoContactoEspejo> createContactoEspejoInformationMapper() {
    ContactoEspejoFieldSetMapper contactoInformationMapper = new ContactoEspejoFieldSetMapper();
    contactoInformationMapper.setIntervinienteEspejoRepository(intervinienteEspejoRepository);
    return contactoInformationMapper;
  }

  private LineMapper<Bien> createBienLineMapper() {
    DefaultLineMapper<Bien> bienLineMapper = new DefaultLineMapper<>();

    LineTokenizer bienLineTokenizer = createBienLineTokenizer();
    bienLineMapper.setLineTokenizer(bienLineTokenizer);

    FieldSetMapper<Bien> bienInformationMapper = createBienInformationMapper();
    bienLineMapper.setFieldSetMapper(bienInformationMapper);

    return bienLineMapper;
  }

  private LineMapper<BienEspejo> createBienEspejoLineMapper() {
    DefaultLineMapper<BienEspejo> bienLineMapper = new DefaultLineMapper<>();

    LineTokenizer bienLineTokenizer = createBienLineTokenizer();
    bienLineMapper.setLineTokenizer(bienLineTokenizer);

    FieldSetMapper<BienEspejo> bienInformationMapper = createBienEspejoInformationMapper();
    bienLineMapper.setFieldSetMapper(bienInformationMapper);

    return bienLineMapper;
  }

  private LineTokenizer createBienLineTokenizer() {
    DelimitedLineTokenizer bienLineTokenizer = new DelimitedLineTokenizer();
    bienLineTokenizer.setDelimiter(";");
    bienLineTokenizer.setNames(new String[]{"id", "contrato.id", "idHaya", "cliente.id", "cliente2.id", "tipoBien.codigo",
      "subTipoBien.codigo", "tipoGarantia.codigo", "estado", "referenciaCatastral", "finca", "localidadRegistro",
      "registro", "tomo", "libro", "folio", "fechaInscripcion", "idufir", "reoOrigen", "fechaReoConversion",
      "importeGarantizado", "responsabilidadHipotecaria", "posesionNegociada", "subasta", "tipoVia.codigo", "nombreVia",
      "numero", "portal", "bloque", "escalera", "piso", "puerta", "entorno.codigo", "municipio.codigo", "localidad.codigo",
      "provincia.codigo", "comentario", "codigoPostal", "pais.codigo", "longitud", "latitud", "tipoActivo.codigo",
      "uso", "anioConstruccion", "superficieSuelo", "superficieConstruida", "superficieUtil", "anejoGaraje",
      "anejoTrastero", "anejoOtros", "rango", "liquidez.codigo", "notas1", "notas2"});
    return bienLineTokenizer;
  }

  private FieldSetMapper<Bien> createBienInformationMapper() {
    BienFieldSetMapper bienInformationMapper = new BienFieldSetMapper();
    bienInformationMapper.setContratoRepository(contratoRepository);
    bienInformationMapper.setEntornoRepository(entornoRepository);
    bienInformationMapper.setLocalidadRepository(localidadRepository);
    bienInformationMapper.setMunicipioRepository(municipioRepository);
    bienInformationMapper.setPaisRepository(paisRepository);
    bienInformationMapper.setProvinciaRepository(provinciaRepository);
    bienInformationMapper.setSubTipoBienRepository(subTipoBienRepository);
    bienInformationMapper.setTipoActivoRepository(tipoActivoRepository);
    bienInformationMapper.setTipoBienRepository(tipoBienRepository);
    bienInformationMapper.setTipoGarantiaRepository(tipoGarantiaRepository);
    bienInformationMapper.setTipoViaRepository(tipoViaRepository);
    bienInformationMapper.setUsoRepository(usoRepository);
    bienInformationMapper.setBienRepository(bienRepository);
    bienInformationMapper.setContratoBienRepository(contratoBienRepository);
    bienInformationMapper.setLiquidezRepository(liquidezRepository);
    return bienInformationMapper;
  }

  private FieldSetMapper<BienEspejo> createBienEspejoInformationMapper() {
    BienEspejoFieldSetMapper bienInformationMapper = new BienEspejoFieldSetMapper();
    bienInformationMapper.setContratoEspejoRepository(contratoEspejoRepository);
    bienInformationMapper.setEntornoRepository(entornoRepository);
    bienInformationMapper.setLocalidadRepository(localidadRepository);
    bienInformationMapper.setMunicipioRepository(municipioRepository);
    bienInformationMapper.setPaisRepository(paisRepository);
    bienInformationMapper.setProvinciaRepository(provinciaRepository);
    bienInformationMapper.setSubTipoBienRepository(subTipoBienRepository);
    bienInformationMapper.setTipoActivoRepository(tipoActivoRepository);
    bienInformationMapper.setTipoBienRepository(tipoBienRepository);
    bienInformationMapper.setTipoGarantiaRepository(tipoGarantiaRepository);
    bienInformationMapper.setTipoViaRepository(tipoViaRepository);
    bienInformationMapper.setUsoRepository(usoRepository);
    bienInformationMapper.setBienEspejoRepository(bienEspejoRepository);
    bienInformationMapper.setContratoBienEspejoRepository(contratoBienEspejoRepository);
    bienInformationMapper.setLiquidezRepository(liquidezRepository);
    return bienInformationMapper;
  }

  private LineMapper<Carga> createCargaLineMapper() {
    DefaultLineMapper<Carga> cargaLineMapper = new DefaultLineMapper<>();

    LineTokenizer cargaLineTokenizer = createCargaLineTokenizer();
    cargaLineMapper.setLineTokenizer(cargaLineTokenizer);

    FieldSetMapper<Carga> cargaInformationMapper = createCargaInformationMapper();
    cargaLineMapper.setFieldSetMapper(cargaInformationMapper);

    return cargaLineMapper;
  }

  private LineMapper<CargaEspejo> createCargaEspejoLineMapper() {
    DefaultLineMapper<CargaEspejo> cargaLineMapper = new DefaultLineMapper<>();

    LineTokenizer cargaLineTokenizer = createCargaLineTokenizer();
    cargaLineMapper.setLineTokenizer(cargaLineTokenizer);

    FieldSetMapper<CargaEspejo> cargaInformationMapper = createCargaEspejoInformationMapper();
    cargaLineMapper.setFieldSetMapper(cargaInformationMapper);

    return cargaLineMapper;
  }

  private LineTokenizer createCargaLineTokenizer() {
    DelimitedLineTokenizer cargaLineTokenizer = new DelimitedLineTokenizer();
    cargaLineTokenizer.setDelimiter(";");
    cargaLineTokenizer.setNames(new String[]{"bien.id", "rangoCarga", "tipoCarga.codigo",
      "importeCarga", "tipoAcreedor.codigo", "nombreAcreedor", "fechaAnotacion",
      "incluidaOtrosColaterales", "fechaVencimientoCarga"});
    return cargaLineTokenizer;
  }

  private FieldSetMapper<Carga> createCargaInformationMapper() {
    CargaFieldSetMapper cargaInformationMapper = new CargaFieldSetMapper();
    cargaInformationMapper.setBienRepository(bienRepository);
    cargaInformationMapper.setTipoAcreedorRepository(tipoAcreedorRepository);
    cargaInformationMapper.setTipoCargaRepository(tipoCargaRepository);
    return cargaInformationMapper;
  }

  private FieldSetMapper<CargaEspejo> createCargaEspejoInformationMapper() {
    CargaEspejoFieldSetMapper cargaEspejoInformationMapper = new CargaEspejoFieldSetMapper();
    cargaEspejoInformationMapper.setBienEspejoRepository(bienEspejoRepository);
    cargaEspejoInformationMapper.setTipoAcreedorRepository(tipoAcreedorRepository);
    cargaEspejoInformationMapper.setTipoCargaRepository(tipoCargaRepository);
    return cargaEspejoInformationMapper;
  }

  private LineMapper<Tasacion> createTasacionLineMapper() {
    DefaultLineMapper<Tasacion> tasacionLineMapper = new DefaultLineMapper<>();

    LineTokenizer tasacionLineTokenizer = createTasacionLineTokenizer();
    tasacionLineMapper.setLineTokenizer(tasacionLineTokenizer);

    FieldSetMapper<Tasacion> tasacionInformationMapper = createTasacionInformationMapper();
    tasacionLineMapper.setFieldSetMapper(tasacionInformationMapper);

    return tasacionLineMapper;
  }

  private LineMapper<TasacionEspejo> createTasacionEspejoLineMapper() {
    DefaultLineMapper<TasacionEspejo> tasacionLineMapper = new DefaultLineMapper<>();

    LineTokenizer tasacionLineTokenizer = createTasacionLineTokenizer();
    tasacionLineMapper.setLineTokenizer(tasacionLineTokenizer);

    FieldSetMapper<TasacionEspejo> tasacionInformationMapper = createTasacionEspejoInformationMapper();
    tasacionLineMapper.setFieldSetMapper(tasacionInformationMapper);

    return tasacionLineMapper;
  }

  private LineTokenizer createTasacionLineTokenizer() {
    DelimitedLineTokenizer tasacionLineTokenizer = new DelimitedLineTokenizer();
    tasacionLineTokenizer.setDelimiter(";");
    tasacionLineTokenizer.setNames(new String[]{"bien.id", "tasadora", "tipoTasacion.codigo",
      "importeTasacion", "fechaTasacion", "liquidez.codigo"});
    return tasacionLineTokenizer;
  }

  private FieldSetMapper<Tasacion> createTasacionInformationMapper() {
    TasacionFieldSetMapper tasacionInformationMapper = new TasacionFieldSetMapper();
    tasacionInformationMapper.setBienRepository(bienRepository);
    tasacionInformationMapper.setTipoTasacionRepository(tipoTasacionRepository);
    tasacionInformationMapper.setLiquidezRepository(liquidezRepository);
    return tasacionInformationMapper;
  }

  private FieldSetMapper<TasacionEspejo> createTasacionEspejoInformationMapper() {
    TasacionEspejoFieldSetMapper tasacionInformationMapper = new TasacionEspejoFieldSetMapper();
    tasacionInformationMapper.setBienEspejoRepository(bienEspejoRepository);
    tasacionInformationMapper.setTipoTasacionRepository(tipoTasacionRepository);
    tasacionInformationMapper.setLiquidezRepository(liquidezRepository);
    return tasacionInformationMapper;
  }

  private LineMapper<Valoracion> createValoracionLineMapper() {
    DefaultLineMapper<Valoracion> valoracionLineMapper = new DefaultLineMapper<>();

    LineTokenizer valoracionLineTokenizer = createValoracionLineTokenizer();
    valoracionLineMapper.setLineTokenizer(valoracionLineTokenizer);

    FieldSetMapper<Valoracion> valoracionInformationMapper = createValoracionInformationMapper();
    valoracionLineMapper.setFieldSetMapper(valoracionInformationMapper);

    return valoracionLineMapper;
  }

  private LineMapper<ValoracionEspejo> createValoracionEspejoLineMapper() {
    DefaultLineMapper<ValoracionEspejo> valoracionLineMapper = new DefaultLineMapper<>();

    LineTokenizer valoracionLineTokenizer = createValoracionLineTokenizer();
    valoracionLineMapper.setLineTokenizer(valoracionLineTokenizer);

    FieldSetMapper<ValoracionEspejo> valoracionInformationMapper = createValoracionEspejoInformationMapper();
    valoracionLineMapper.setFieldSetMapper(valoracionInformationMapper);

    return valoracionLineMapper;
  }

  private LineTokenizer createValoracionLineTokenizer() {
    DelimitedLineTokenizer valoracionLineTokenizer = new DelimitedLineTokenizer();
    valoracionLineTokenizer.setDelimiter(";");
    valoracionLineTokenizer.setNames(new String[]{"bien.id", "valorador", "tipoValoracion.codigo",
      "importeValoracion", "fechaValoracion", "liquidez.codigo"});
    return valoracionLineTokenizer;
  }

  private FieldSetMapper<Valoracion> createValoracionInformationMapper() {
    ValoracionFieldSetMapper valoracionInformationMapper = new ValoracionFieldSetMapper();
    valoracionInformationMapper.setBienRepository(bienRepository);
    valoracionInformationMapper.setTipoValoracionRepository(tipoValoracionRepository);
    valoracionInformationMapper.setLiquidezRepository(liquidezRepository);
    return valoracionInformationMapper;
  }

  private FieldSetMapper<ValoracionEspejo> createValoracionEspejoInformationMapper() {
    ValoracionEspejoFieldSetMapper valoracionInformationMapper = new ValoracionEspejoFieldSetMapper();
    valoracionInformationMapper.setBienEspejoRepository(bienEspejoRepository);
    valoracionInformationMapper.setTipoValoracionRepository(tipoValoracionRepository);
    valoracionInformationMapper.setLiquidezRepository(liquidezRepository);
    return valoracionInformationMapper;
  }
}
