package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.espejo.contrato.infrastructure.repository.ContratoEspejoRepository;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
@Getter
@Setter
@StepScope
public class ContratoEspejoProcessor implements ItemProcessor<ContratoEspejo, ContratoEspejo> {

  @Autowired
  private ServicioMaestro servicioMaestro;

  @Autowired
  private ContratoEspejoRepository contratoRepository;

  @Autowired
  private CarteraRepository carteraRepository;

  @Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  @Value("#{jobParameters['multicartera']}")
  private Boolean multicartera;

  @Override
  public ContratoEspejo process(ContratoEspejo item) throws Exception {
    //si existe un contrato con el idCarga se actualiza el Id del item para no duplicar contratos
    ContratoEspejo contratoOld = contratoRepository.findByIdCarga(item.getIdCarga()).orElse(null);
    if (contratoOld != null) {
      item.setId(contratoOld.getId());
      item.setIdHaya(contratoOld.getIdHaya());
    }
    item.setCalculado(false);
    item.setEsRepresentante(false);
    Cartera cartera = getCartera(carteraId);

    if (cartera != null && item.getIdHaya() == null && !multicartera) {
      contratoRepository.save(item);
    }

    item.setFechaCarga(new Date());
    item.setReestructurado(false);
    item.setVencimientoAnticipado(false);
    return item;

  }

  private Cartera getCartera(String idCargaCartera) {
    try {
      var a = carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(idCargaCartera);
      Cartera cartera = a.get();
      return cartera;
    } catch (Exception ex) {
      System.out.println(idCargaCartera + "EXCEPTION");
      ex.printStackTrace();
    }
    return null;
  }

}
