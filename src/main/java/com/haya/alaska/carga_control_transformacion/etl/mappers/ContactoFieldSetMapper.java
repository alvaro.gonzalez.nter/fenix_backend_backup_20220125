package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class ContactoFieldSetMapper implements FieldSetMapper<DatoContacto> {


  private IntervinienteRepository intervinienteRepository;


  @Override
  public DatoContacto mapFieldSet(FieldSet fieldSet) throws BindException {

    DatoContacto datoContacto = new DatoContacto();
    if (isvalidString(fieldSet.readString("interviniente.id"))) {
      Interviniente interviniente = createInterviniente(fieldSet.readString("interviniente.id"));
      if (interviniente != null) {
        datoContacto.setInterviniente(interviniente);
      }
    }

    if (isvalidString(fieldSet.readString("tipo.codigo"))) {
      String codToUse = getIntValue(fieldSet.readString("tipo.codigo"));
      switch (codToUse) {
        case "1":
          datoContacto.setMovil(fieldSet.readString("valor"));
          break;
        case "2":
          datoContacto.setFijo(fieldSet.readString("valor"));
          break;
        case "3":
          datoContacto.setEmail(fieldSet.readString("valor"));
          break;
      }
    }
    datoContacto.setFechaActualizacion(new Date());
    if (isvalidString(fieldSet.readString("orden.codigo"))) {
      datoContacto.setOrden(fieldSet.readInt("orden.codigo"));
    }
    Set<DatoContacto> datosContacto = datoContacto.getInterviniente().getDatosContacto();
    if (datosContacto != null && !datosContacto.isEmpty()) {
      for (DatoContacto dc : datosContacto) {
        if (dc.getOrden().equals(datoContacto.getOrden())) {
          datoContacto = completeDatoContacto(datoContacto, dc);
        }
      }
    }

    return datoContacto;
  }

  private Interviniente createInterviniente(String idCargaInterviniente) {
    Interviniente interviniente = intervinienteRepository.findByIdCarga(idCargaInterviniente).orElse(null);
    return interviniente;

  }

  private DatoContacto completeDatoContacto(DatoContacto datoActual, DatoContacto datoOld) {
    if (datoOld.getEmail() != null && datoActual.getEmail() == null) {
      datoActual.setEmail(datoOld.getEmail());
    }
    if (datoOld.getFijo() != null && datoActual.getFijo() == null) {
      datoActual.setFijo(datoOld.getFijo());
    }
    if (datoOld.getMovil() != null && datoActual.getMovil() == null) {
      datoActual.setMovil(datoOld.getMovil());
    }
    if (datoOld.getId() != null && datoActual.getId() == null) {
      datoActual.setId(datoOld.getId());
    }
    return datoActual;
  }

  private Boolean isvalidString(String string) {
    if (string != null && !string.equals("")) {
      return true;
    } else {
      return false;
    }
  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }

}
