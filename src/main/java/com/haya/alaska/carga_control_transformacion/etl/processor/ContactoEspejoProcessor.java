package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.espejo.dato_contacto.domain.DatoContactoEspejo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
@StepScope
public class ContactoEspejoProcessor implements ItemProcessor<DatoContactoEspejo, DatoContactoEspejo> {


  @Override
  public DatoContactoEspejo process(DatoContactoEspejo item) throws Exception {
    if (item.getEmailValidado() == null) {
      item.setEmailValidado(false);
    }
    if (item.getFijoValidado() == null) {
      item.setFijoValidado(false);
    }
    if (item.getMovilValidado() == null) {
      item.setMovilValidado(false);
    }
    item.setFechaActualizacion(new Date());
    return item;

  }
}
