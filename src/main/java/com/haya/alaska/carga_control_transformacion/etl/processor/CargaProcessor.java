package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.espejo.carga.domain.CargaEspejo;
import com.haya.alaska.tipo_acreedor.domain.TipoAcreedor;
import com.haya.alaska.tipo_acreedor.infrastructure.repository.TipoAcreedorRepository;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import com.haya.alaska.tipo_carga.infrastructure.repository.TipoCargaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
@StepScope
public class CargaProcessor implements ItemProcessor<CargaEspejo, Carga> {

  @Value("#{jobParameters['manual']}")
  private String manual;

  @Autowired
  BienRepository bienRepository;

  @Autowired
  TipoCargaRepository tipoCargaRepository;

  @Autowired
  TipoAcreedorRepository tipoAcreedorRepository;

  @Override
  public Carga process(CargaEspejo item) throws Exception {
    TipoCarga tipoCarga = null;
    TipoAcreedor tipoAcreedor = null;

    if (item.getTipoCarga() != null)
      tipoCarga = tipoCargaRepository.findByCodigo(item.getTipoCarga().getCodigo()).orElse(null);

    if (item.getTipoAcreedor() != null)
      tipoAcreedor = tipoAcreedorRepository.findByCodigo(item.getTipoAcreedor().getCodigo()).orElse(null);

    Carga carga = new Carga(null, null, tipoCarga, tipoAcreedor, item.getRango(),
      item.getImporte(), item.getNombreAcreedor(), item.getFechaAnotacion(), item.getIncluidaOtrosColaterales(),
      item.getFechaVencimiento(), item.getActivo(),null,null,null,null);

    Set<Bien> bienes = new HashSet<>();

    for (BienEspejo bienEspejo : item.getBienes()) {
      if (bienEspejo != null) {
        Bien bien = bienRepository.findByIdCarga(bienEspejo.getIdCarga()).orElse(null);
        bienes.add(bien);
      }
    }

    carga.setBienes(bienes);

    //Se hacen las validaciones con el campo ind_valido de la tabla espejo
    if (manual.equals("true")) {
      if (item.getValido() != null && !item.getValido()) {
        carga = null;
      }
    }
    return carga;

  }
}
