package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.espejo.expediente.domain.ExpedienteEspejo;
import com.haya.alaska.espejo.expediente.infrastructure.repository.ExpedienteEspejoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@StepScope
public class ExpedienteProcessor implements ItemProcessor<Expediente, Expediente> {


  @Autowired
  private ExpedienteEspejoRepository expedienteRepository;


  @Override
  public Expediente process(Expediente item) throws Exception {
    ExpedienteEspejo expedienteOld = expedienteRepository.findByIdCargaAndCarteraIdCargaAndIdOrigen(
      item.getIdCarga(), item.getCartera().getIdCarga(), item.getIdOrigen())
      .orElse(null);
    if (expedienteOld != null) {
      item.setId(expedienteOld.getId());
    }
    return item;
  }
}

