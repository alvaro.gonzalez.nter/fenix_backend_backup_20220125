package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tipo_tasacion.domain.TipoTasacion;
import com.haya.alaska.tipo_tasacion.infrastructure.repository.TipoTasacionRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

@Getter
@Setter
public class TasacionFieldSetMapper implements FieldSetMapper<Tasacion> {

  private BienRepository bienRepository;

  private TipoTasacionRepository tipoTasacionRepository;

  private LiquidezRepository liquidezRepository;


  @Override
  public Tasacion mapFieldSet(FieldSet fieldSet) throws BindException {
    Tasacion tasacion = new Tasacion();
    if (isvalidString(fieldSet.readString("bien.id"))) {
      Bien bien = createBien(fieldSet.readString("bien.id"));
      if (bien != null) {
        tasacion.setBien(bien);
      }
    }
    if (isvalidString(fieldSet.readString("tasadora"))) {
      tasacion.setTasadora(fieldSet.readString("tasadora"));
    }
    if (isvalidString(fieldSet.readString("tipoTasacion.codigo"))) {
      tasacion.setTipoTasacion(createTipoTasacion(fieldSet.readString("tipoTasacion.codigo")));
    }
    if (isvalidString(fieldSet.readString("importeTasacion"))) {
      tasacion.setImporte(fieldSet.readDouble("importeTasacion"));
    }
    if (isvalidString(fieldSet.readString("fechaTasacion"))) {
      tasacion.setFecha(fieldSet.readDate("fechaTasacion", "dd/MM/yyyy"));
    }
    if (isvalidString(fieldSet.readString("liquidez.codigo"))) {
      tasacion.setLiquidez(createLiquidez(fieldSet.readString("liquidez.codigo")));
    }

    return tasacion;
  }

  private Bien createBien(String idCargaBien) {
    Bien bien = bienRepository.findByIdCarga(idCargaBien).orElse(null);
    return bien;
  }

  private TipoTasacion createTipoTasacion(String codigoTipoTasacion) {
    String codToUse = getIntValue(codigoTipoTasacion);
    TipoTasacion tipoTasacion = tipoTasacionRepository.findByCodigo(codToUse).orElse(null);
    return tipoTasacion;
  }

  private Liquidez createLiquidez(String codigoLiquidez) {
    Liquidez liquidez = liquidezRepository.findByCodigo(codigoLiquidez).orElse(null);
    return liquidez;
  }

  private Boolean isvalidString(String string) {
    if (string != null && !string.equals("")) {
      return true;
    } else {
      return false;
    }
  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }
}
