package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.espejo.tasacion.domain.TasacionEspejo;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tipo_tasacion.domain.TipoTasacion;
import com.haya.alaska.tipo_tasacion.infrastructure.repository.TipoTasacionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@StepScope
public class TasacionProcessor implements ItemProcessor<TasacionEspejo, Tasacion> {

  @Autowired
  private BienRepository bienRepository;

  @Autowired
  private TipoTasacionRepository tipoTasacionRepository;

  @Autowired
  private LiquidezRepository liquidezRepository;

  @Value("#{jobParameters['manual']}")
  private String manual;

  @Override
  public Tasacion process(TasacionEspejo item) throws Exception {
    Bien bien = bienRepository.findByIdCarga(item.getBien().getIdCarga()).orElse(null);
    TipoTasacion tipoTasacion = null;
    Liquidez liquidez = null;

    if (item.getTipoTasacion() != null)
      tipoTasacion = tipoTasacionRepository.findByCodigo(item.getTipoTasacion().getCodigo()).orElse(null);

    if (item.getLiquidez() != null)
      liquidez = liquidezRepository.findByCodigo(item.getLiquidez().getCodigo()).orElse(null);

    Tasacion tasacion = new Tasacion(null, bien, item.getTasadora(), tipoTasacion, liquidez,
      item.getImporte(), item.getFecha(), item.getActivo());

    //Se hacen las validaciones con el campo ind_valido de la tabla espejo
    if (manual.equals("true")) {
      if (item.getValido() != null && !item.getValido()) {
        tasacion = null;
      }
    }
    return tasacion;

  }
}
