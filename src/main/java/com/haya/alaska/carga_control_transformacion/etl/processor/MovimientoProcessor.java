package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.infrastructure.repository.MovimientoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@StepScope
public class MovimientoProcessor implements ItemProcessor<Movimiento, Movimiento> {


  @Autowired
  private MovimientoRepository movimientoRepository;


  @Override
  public Movimiento process(Movimiento item) throws Exception {
    //si existe un bien con el idCarga se actualiza el Id del item para no duplicar bienes
    Movimiento movimientoOld = movimientoRepository.findByIdCarga(item.getIdCarga()).orElse(null);
    if (movimientoOld != null) {
      item.setId(movimientoOld.getId());
    }
    item.setAbonado(false);
    return item;

  }
}
