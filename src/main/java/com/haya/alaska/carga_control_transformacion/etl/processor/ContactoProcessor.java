package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.espejo.dato_contacto.domain.DatoContactoEspejo;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
@StepScope
public class ContactoProcessor implements ItemProcessor<DatoContactoEspejo, DatoContacto> {

  @Autowired
  private IntervinienteRepository intervinienteRepository;

  @Autowired
  private DatoContactoRepository datoContactoRepository;

  @Value("#{jobParameters['manual']}")
  private String manual;

  @Override
  public DatoContacto process(DatoContactoEspejo item) throws Exception {
    Interviniente interviniente = intervinienteRepository.findByIdCarga(item.getInterviniente().getIdCarga()).orElse(null);

    if (item.getEmailValidado() == null) {
      item.setEmailValidado(false);
    }
    if (item.getFijoValidado() == null) {
      item.setFijoValidado(false);
    }
    if (item.getMovilValidado() == null) {
      item.setMovilValidado(false);
    }
    item.setFechaActualizacion(new Date());

    //La relación con el ocupante no se rellena en la carga del fichero
    DatoContacto datoContacto = new DatoContacto(null, item.getFijo(), item.getFijoValidado(), item.getMovil(),
      item.getMovilValidado(), item.getEmail(), item.getEmailValidado(), item.getOrigen(), item.getFechaActualizacion(),
      item.getOrden(), null, interviniente, null, item.getActivo());

    //Se hacen las validaciones con el campo valido de la tabla espejo
    if (manual.equals("true")) {
      if (item.getValido() != null && !item.getValido()) {
        datoContacto = null;
      }
    }
    return datoContacto;

  }
}
