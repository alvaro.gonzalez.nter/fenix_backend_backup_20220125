package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable;
import com.haya.alaska.clasificacion_contable.infrastructure.repository.ClasificacionContableRepository;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.espejo.expediente.domain.ExpedienteEspejo;
import com.haya.alaska.espejo.expediente.infrastructure.repository.ExpedienteEspejoRepository;
import com.haya.alaska.espejo.saldo.domain.SaldoEspejo;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.estado_contrato.infrastructure.repository.EstadoContratoRepository;
import com.haya.alaska.indice_referencia.domain.IndiceReferencia;
import com.haya.alaska.indice_referencia.infrastructure.repository.IndiceReferenciaRepository;
import com.haya.alaska.periodicidad_liquidacion.domain.PeriodicidadLiquidacion;
import com.haya.alaska.periodicidad_liquidacion.infrastructure.repository.PeriodicidadLiquidacionRepository;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.producto.infrastructure.repository.ProductoRepository;
import com.haya.alaska.sistema_amortizacion.domain.SistemaAmortizacion;
import com.haya.alaska.sistema_amortizacion.infrastructure.repository.SistemaAmortizacionRepository;
import com.haya.alaska.tipo_reestructuracion.domain.TipoReestructuracion;
import com.haya.alaska.tipo_reestructuracion.infrastructure.repository.TipoReestructuracionRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class ContratoEspejoFieldSetMapper implements FieldSetMapper<ContratoEspejo> {

  private SistemaAmortizacionRepository sistemaAmortizacionRepository;

  private ClasificacionContableRepository clasificacionContableRepository;

  private TipoReestructuracionRepository tipoReestructuracionRepository;

  private IndiceReferenciaRepository indiceReferenciaRepository;

  private PeriodicidadLiquidacionRepository periodicidadLiquidacionRepository;

  private ProductoRepository productoRepository;

  private ExpedienteEspejoRepository expedienteEspejoRepository;

  private EstadoContratoRepository estadoContratoRepository;

  @Override
  public ContratoEspejo mapFieldSet(FieldSet fieldSet) throws BindException {
    ContratoEspejo contrato = new ContratoEspejo();
    //se marca como false. Si ocurre algun error n obloqueante se marcará como true
    contrato.setEnRevision(false);
    Set<SaldoEspejo> saldos = new HashSet<>();
    contrato.setSaldos(saldos);
    SaldoEspejo saldo = new SaldoEspejo();
    if (!fieldSet.readString("expediente.id").equals("")) {
      contrato.setExpediente(createExpediente(fieldSet.readString("expediente.id")));
    }
    if (!fieldSet.readString("id").equals("")) {
      contrato.setIdCarga(fieldSet.readString("id"));
    }
    if (!fieldSet.readString("idOrigen").equals("")) {
      contrato.setIdOrigen(fieldSet.readString("idOrigen"));
    }
    if (!fieldSet.readString("estadoContrato").equals("")) {
      contrato.setEstadoContrato(createEstadoContrato(fieldSet.readString("estadoContrato")));
    }
    if (!fieldSet.readString("producto.codigo").equals("")) {
      contrato.setProducto(createProducto(fieldSet.readString("producto.codigo")));
    }
    if (!fieldSet.readString("sistemaAmortizacion.codigo").equals("")) {
      contrato.setSistemaAmortizacion(createSistemaAmortizacion(fieldSet.readString("sistemaAmortizacion.codigo")));
    }
    if (!fieldSet.readString("periodicidadLiquidacion.codigo").equals("")) {
      contrato.setPeriodicidadLiquidacion(createPeriodicidadLiquidacion(fieldSet.readString("periodicidadLiquidacion.codigo")));
    }
    if (!fieldSet.readString("indiceReferencia.codigo").equals("")) {
      contrato.setIndiceReferencia(createIndiceReferencia(fieldSet.readString("indiceReferencia.codigo")));
    }
    if (!fieldSet.readString("diferencial").equals("")) {
      contrato.setDiferencial(fieldSet.readDouble("diferencial"));
    }
    if (!fieldSet.readString("tin").equals("")) {
      contrato.setTin(fieldSet.readDouble("tin"));
    }
    if (!fieldSet.readString("tae").equals("")) {
      contrato.setTae(fieldSet.readDouble("tae"));
    }
    if (!fieldSet.readString("rangoHipotecario").equals("")) {
      contrato.setRangoHipotecario(getIntValue(fieldSet.readString("rangoHipotecario")));
    }
    if (!fieldSet.readString("reestructurado").equals("")) {
      contrato.setReestructurado(createBooleanValue(fieldSet.readString("reestructurado")));
    }
    if (!fieldSet.readString("tipoReestructuracion.codigo").equals("")) {
      contrato.setTipoReestructuracion(createTipoReestructuracion(fieldSet.readString("tipoReestructuracion.codigo")));
    }
    if (!fieldSet.readString("clasificacionContable.codigo").equals("")) {
      contrato.setClasificacionContable(createClasificacionContable(fieldSet.readString("clasificacionContable.codigo")));
    }
    if (!fieldSet.readString("campania").equals("")) {
      contrato.setCampania(fieldSet.readString("campania"));
    }
    if (!fieldSet.readString("fechaFormalizacion").equals("")) {
      contrato.setFechaFormalizacion(fieldSet.readDate("fechaFormalizacion", "dd/MM/yyyy"));
    }
    if (!fieldSet.readString("fechaVencimientoInicial").equals("")) {
      contrato.setFechaVencimientoInicial(fieldSet.readDate("fechaVencimientoInicial", "dd/MM/yyyy"));
    }
    if (!fieldSet.readString("fechaVencimientoAnticipado").equals("")) {
      contrato.setFechaVencimientoAnticipado(fieldSet.readDate("fechaVencimientoAnticipado", "dd/MM/yyyy") != null ? fieldSet.readDate("fechaVencimientoAnticipado", "dd/MM/yyyy") : new Date(01 / 01 / 1970));
    }
    if (!fieldSet.readString("fechaPaseRegular").equals("")) {
      contrato.setFechaPaseRegular(fieldSet.readDate("fechaPaseRegular", "dd/MM/yyyy") != null ? fieldSet.readDate("fechaPaseRegular", "dd/MM/yyyy") : new Date(01 / 01 / 1970));
    }
    if (!fieldSet.readString("cuotasPendientes").equals("")) {
      contrato.setCuotasPendientes(fieldSet.readInt("cuotasPendientes"));
    }
    if (!fieldSet.readString("fechaLiquidacionCuota").equals("")) {
      contrato.setFechaLiquidacionCuota(fieldSet.readDate("fechaLiquidacionCuota", "dd/MM/yyyy"));
    }
    if (!fieldSet.readString("importeInicial").equals("")) {
      contrato.setImporteInicial(fieldSet.readDouble("importeInicial"));
    }
    if (!fieldSet.readString("importeDispuesto").equals("")) {
      contrato.setImporteDispuesto(fieldSet.readDouble("importeDispuesto"));
    }
    /**SALDOS**/
    if (!fieldSet.readString("saldoContrato.capitalPendiente").equals("")) {
      saldo.setCapitalPendiente(fieldSet.readDouble("saldoContrato.capitalPendiente"));
    }
    if (!fieldSet.readString("saldoContrato.saldoGestion").equals("")) {
      saldo.setSaldoGestion(fieldSet.readDouble("saldoContrato.saldoGestion"));
      //propiedad utilizada para el cálculo del contrato representante
      contrato.setSaldoGestion(fieldSet.readDouble("saldoContrato.saldoGestion"));
    }
    if (!fieldSet.readString("saldoContrato.principalVencidoImpagado").equals("")) {
      saldo.setPrincipalVencidoImpagado(fieldSet.readDouble("saldoContrato.principalVencidoImpagado"));
    }
    if (!fieldSet.readString("saldoContrato.interesesOrdinariosImpagados").equals("")) {
      saldo.setInteresesOrdinariosImpagados(fieldSet.readDouble("saldoContrato.interesesOrdinariosImpagados"));
    }
    if (!fieldSet.readString("saldoContrato.interesesOrdinariosImpagados").equals("")) {
      saldo.setInteresesOrdinariosImpagados(fieldSet.readDouble("saldoContrato.interesesOrdinariosImpagados"));
    }
    if (!fieldSet.readString("saldoContrato.interesesDemora").equals("")) {
      saldo.setInteresesDemora(fieldSet.readDouble("saldoContrato.interesesDemora"));
    }
    if (!fieldSet.readString("saldoContrato.comisionesImpagados").equals("")) {
      saldo.setComisionesImpagados(fieldSet.readDouble("saldoContrato.comisionesImpagados"));
    }
    if (!fieldSet.readString("saldoContrato.gastosImpagados").equals("")) {
      saldo.setGastosImpagados(fieldSet.readDouble("saldoContrato.gastosImpagados"));
    }
    if (!fieldSet.readString("deudaImpagada").equals("")) {
      saldo.setDeudaImpagada(fieldSet.readDouble("deudaImpagada"));
    }
    contrato.getSaldos().add(saldo);

    if (!fieldSet.readString("cuotasTotales").equals("")) {
      contrato.setCuotasTotales(fieldSet.readInt("cuotasTotales"));
    }
    if (!fieldSet.readString("importeUltimaCuota").equals("")) {
      contrato.setImporteUltimaCuota(fieldSet.readDouble("importeUltimaCuota"));
    }
    if (!fieldSet.readString("fechaPrimerImpago").equals("")) {
      contrato.setFechaPrimerImpago(fieldSet.readDate("fechaPrimerImpago", "dd/MM/yyyy"));
    }
    if (!fieldSet.readString("numeroCuotasImpagadas").equals("")) {
      contrato.setNumeroCuotasImpagadas(fieldSet.readInt("numeroCuotasImpagadas"));
    }
    if (!fieldSet.readString("deudaImpagada").equals("")) {
      contrato.setDeudaImpagada(fieldSet.readDouble("deudaImpagada"));
    }
    if (!fieldSet.readString("restoHipotecario").equals("")) {
      contrato.setRestoHipotecario(fieldSet.readDouble("restoHipotecario"));
    }
    if (!fieldSet.readString("origen").equals("")) {
      contrato.setOrigen(fieldSet.readString("origen"));
    }
    if (!fieldSet.readString("gastosJudiciales").equals("")) {
      contrato.setGastosJudiciales(fieldSet.readDouble("gastosJudiciales"));
    }
    if (!fieldSet.readString("importeCuotasImpagadas").equals("")) {
      contrato.setImporteCuotasImpagadas(fieldSet.readDouble("importeCuotasImpagadas"));
    }
    if (!fieldSet.readString("notas1").equals("")) {
      contrato.setNotas1(fieldSet.readString("notas1"));
    }
    if (!fieldSet.readString("notas2").equals("")) {
      contrato.setNotas2(fieldSet.readString("notas2"));
    }
    return contrato;
  }

  private EstadoContrato createEstadoContrato(String codigoEstadoContrato) {
    EstadoContrato estadoContrato = estadoContratoRepository.findByCodigo(codigoEstadoContrato).orElse(null);
    return estadoContrato;
  }

  private ExpedienteEspejo createExpediente(String idCargaExpediente) {
    ExpedienteEspejo expediente = expedienteEspejoRepository.findByIdCarga(idCargaExpediente).get();
    expediente.setIdCarga(idCargaExpediente);
    return expediente;
  }

  private Producto createProducto(String codigoProducto) {
    //en caso de que el código llegue como double
    String codToUse = getIntValue(codigoProducto);
    Producto producto = productoRepository.findByCodigo(codToUse).orElse(null);
    return producto;
  }

  private SistemaAmortizacion createSistemaAmortizacion(String codigoSistemaAmortizacion) {
    //en caso de que el código llegue como double
    String codToUse = getIntValue(codigoSistemaAmortizacion);
    SistemaAmortizacion sistemaAmortizacion = sistemaAmortizacionRepository.findByCodigo(codToUse).orElse(null);
    return sistemaAmortizacion;
  }

  private PeriodicidadLiquidacion createPeriodicidadLiquidacion(String codigoPeriodicidadLiquidacion) {
    //en caso de que el código llegue como double
    String codToUse = getIntValue(codigoPeriodicidadLiquidacion);
    PeriodicidadLiquidacion periodicidadLiquidacion = periodicidadLiquidacionRepository.findByCodigo(codToUse).orElse(null);
    return periodicidadLiquidacion;
  }

  private IndiceReferencia createIndiceReferencia(String codigoIndiceReferencia) {
    //en caso de que el código llegue como double
    String codToUse = getIntValue(codigoIndiceReferencia);
    IndiceReferencia indiceReferencia = indiceReferenciaRepository.findByCodigo(codToUse).orElse(null);
    return indiceReferencia;
  }

  private Boolean createBooleanValue(String code) {
    switch (code) {
      case "0":
        return false;
      case "1":
        return true;
      default:
        return null;
    }
  }

  private TipoReestructuracion createTipoReestructuracion(String codigoTipoReestructuracion) {
    //en caso de que el código llegue como double
    String codToUse = getIntValue(codigoTipoReestructuracion);
    TipoReestructuracion tipoReestructuracion = tipoReestructuracionRepository.findByCodigo(codToUse).orElse(null);
    ;
    return tipoReestructuracion;
  }

  private ClasificacionContable createClasificacionContable(String codigoClasificacionContable) {
    //en caso de que el código llegue como double
    String codToUse = getIntValue(codigoClasificacionContable);
    ClasificacionContable clasificacionContable = clasificacionContableRepository.findByCodigo(codToUse).orElse(null);
    return clasificacionContable;
  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }

}
