package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.infrastructure.repository.ContratoBienRepository;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.shared.exceptions.InvalidDataException;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien;
import com.haya.alaska.sub_tipo_bien.infrastructure.repository.SubTipoBienRepository;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;
import com.haya.alaska.tipo_garantia.infrastructure.repository.TipoGarantiaRepository;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.uso.domain.Uso;
import com.haya.alaska.uso.infrastructure.repository.UsoRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import java.util.Date;

@Getter
@Setter
public class BienFieldSetMapper implements FieldSetMapper<Bien> {

  private LocalidadRepository localidadRepository;

  private MunicipioRepository municipioRepository;

  private ProvinciaRepository provinciaRepository;

  private TipoViaRepository tipoViaRepository;

  private EntornoRepository entornoRepository;

  private ContratoRepository contratoRepository;

  private TipoBienRepository tipoBienRepository;

  private SubTipoBienRepository subTipoBienRepository;

  private TipoGarantiaRepository tipoGarantiaRepository;

  private PaisRepository paisRepository;

  private TipoActivoRepository tipoActivoRepository;

  private UsoRepository usoRepository;

  private BienRepository bienRepository;

  private ContratoBienRepository contratoBienRepository;

  private LiquidezRepository liquidezRepository;

  @Override
  public Bien mapFieldSet(FieldSet fieldSet) throws BindException {
    Bien bienOld = bienRepository.findByIdCarga(fieldSet.readString("id")).orElse(null);
    if (bienOld != null) {
      if (isValidString(fieldSet.readString("cliente.id"))) {
        bienOld.setIdOrigen(fieldSet.readString("cliente.id"));
      }
      if (isValidString(fieldSet.readString("cliente2.id"))) {
        bienOld.setIdOrigen2(fieldSet.readString("cliente2.id"));
      }
      if (isValidString(fieldSet.readString("tipoBien.codigo"))) {
        bienOld.setTipoBien(createTipoBien(fieldSet.readString("tipoBien.codigo")));
      }
      if (isValidString(fieldSet.readString("subTipoBien.codigo"))) {
        bienOld.setSubTipoBien(createSubTipoBien(fieldSet.readString("subTipoBien.codigo")));
      }
      if (isValidString(fieldSet.readString("estado"))) {
        bienOld.setActivo(createBooleanValue(getIntValue(fieldSet.readString("estado"))));
      } else {
        bienOld.setActivo(true);
      }
      if (isValidString(fieldSet.readString("referenciaCatastral"))) {
        bienOld.setReferenciaCatastral(fieldSet.readString("referenciaCatastral"));
      }
      if (isValidString(fieldSet.readString("finca"))) {
        bienOld.setFinca(fieldSet.readString("finca"));
      }
      if (isValidString(fieldSet.readString("localidadRegistro"))) {
        bienOld.setRegistro(fieldSet.readString("localidadRegistro"));
      }
      if (isValidString(fieldSet.readString("registro"))) {
        bienOld.setRegistro(fieldSet.readString("registro"));
      }
      if (isValidString(fieldSet.readString("tomo"))) {
        bienOld.setTomo(getIntValue(fieldSet.readString("tomo")));
      }
      if (isValidString(fieldSet.readString("libro"))) {
        bienOld.setLibro(getIntValue(fieldSet.readString("libro")));
      }
      if (isValidString(fieldSet.readString("folio"))) {
        bienOld.setFolio(getIntValue(fieldSet.readString("folio")));
      }
      if (isValidString(fieldSet.readString("fechaInscripcion"))) {
        Date fechaInscripcion = fieldSet.readDate("fechaInscripcion", "dd/MM/yyyy");
        if (fechaInscripcion != new Date("11/11/1111"))
          bienOld.setFechaInscripcion(fieldSet.readDate("fechaInscripcion", "dd/MM/yyyy"));
      }
      if (isValidString(fieldSet.readString("idufir"))) {
        bienOld.setIdufir(fieldSet.readString("idufir"));
      }
      if (isValidString(fieldSet.readString("reoOrigen"))) {
        bienOld.setReoOrigen(fieldSet.readString("reoOrigen"));
      }
      if (isValidString(fieldSet.readString("fechaReoConversion"))) {
        bienOld.setFechaReoConversion(fieldSet.readDate("fechaReoConversion", "dd/MM/yyyy"));
      }
      if (isValidString(fieldSet.readString("fechaReoConversion"))) {
        bienOld.setFechaReoConversion(fieldSet.readDate("fechaReoConversion", "dd/MM/yyyy"));
      }
      if (isValidString(fieldSet.readString("posesionNegociada"))) {
        bienOld.setPosesionNegociada(createBooleanValue(fieldSet.readString("posesionNegociada")));
      } else {
        bienOld.setPosesionNegociada(false);
      }
      if (isValidString(fieldSet.readString("subasta"))) {
        bienOld.setSubasta(createBooleanValue(fieldSet.readString("subasta")));
      } else {
        bienOld.setSubasta(false);
      }
      if (isValidString(fieldSet.readString("tipoVia.codigo"))) {
        bienOld.setTipoVia(createTipoVia(fieldSet.readString("tipoVia.codigo")));
      }
      if (isValidString(fieldSet.readString("nombreVia"))) {
        bienOld.setNombreVia(fieldSet.readString("nombreVia"));
      }
      if (isValidString(fieldSet.readString("numero"))) {
        bienOld.setNumero(fieldSet.readString("numero"));
      }
      if (isValidString(fieldSet.readString("portal"))) {
        bienOld.setPortal(fieldSet.readString("portal"));
      }
      if (isValidString(fieldSet.readString("bloque"))) {
        bienOld.setBloque(fieldSet.readString("bloque"));
      }
      if (isValidString(fieldSet.readString("escalera"))) {
        bienOld.setEscalera(fieldSet.readString("escalera"));
      }
      if (isValidString(fieldSet.readString("piso"))) {
        bienOld.setPiso(fieldSet.readString("piso"));
      }
      if (isValidString(fieldSet.readString("puerta"))) {
        bienOld.setPuerta(fieldSet.readString("puerta"));
      }
      if (isValidString(fieldSet.readString("entorno.codigo"))) {
        bienOld.setEntorno(createEntorno(fieldSet.readString("entorno.codigo")));
      }
      if (isValidString(fieldSet.readString("municipio.codigo"))) {
        bienOld.setMunicipio(createMunicipio(fieldSet.readString("municipio.codigo")));
      }
      if (isValidString(fieldSet.readString("localidad.codigo"))) {
        bienOld.setLocalidad(createLocalidad(fieldSet.readString("localidad.codigo")));
      }
      if (isValidString(fieldSet.readString("provincia.codigo"))) {
        bienOld.setProvincia(createProvincia(fieldSet.readString("provincia.codigo")));
      }
      if (isValidString(fieldSet.readString("comentario"))) {
        bienOld.setComentarioDireccion(fieldSet.readString("comentario"));
      }
      if (isValidString(fieldSet.readString("codigoPostal"))) {
        bienOld.setCodigoPostal(fieldSet.readString("codigoPostal"));
      }
      if (isValidString(fieldSet.readString("pais.codigo"))) {
        bienOld.setPais(createPais(fieldSet.readString("pais.codigo")));
      }
      if (isValidString(fieldSet.readString("longitud"))) {
        bienOld.setLongitud(fieldSet.readDouble("longitud"));
      }
      if (isValidString(fieldSet.readString("latitud"))) {
        bienOld.setLatitud(fieldSet.readDouble("latitud"));
      }
      if (isValidString(fieldSet.readString("tipoActivo.codigo"))) {
        bienOld.setTipoActivo(createTipoActivo(fieldSet.readString("tipoActivo.codigo")));
      }
      if (isValidString(fieldSet.readString("uso"))) {
        bienOld.setUso(createUso(fieldSet.readString("uso")));
      }
      if (isValidString(fieldSet.readString("anioConstruccion"))) {
        bienOld.setAnioConstruccion(fieldSet.readInt("anioConstruccion"));
      }
      if (isValidString(fieldSet.readString("superficieSuelo"))) {
        bienOld.setSuperficieSuelo(fieldSet.readDouble("superficieSuelo") / 100);
      }
      if (isValidString(fieldSet.readString("superficieConstruida"))) {
        bienOld.setSuperficieConstruida(fieldSet.readDouble("superficieConstruida") / 100);
      }
      if (isValidString(fieldSet.readString("superficieUtil"))) {
        bienOld.setSuperficieUtil(fieldSet.readDouble("superficieUtil"));
      }
      if (isValidString(fieldSet.readString("anejoGaraje"))) {
        bienOld.setAnejoGaraje(fieldSet.readString("anejoGaraje"));
      }
      if (isValidString(fieldSet.readString("anejoTrastero"))) {
        bienOld.setAnejoTrastero(fieldSet.readString("anejoTrastero"));
      }
      if (isValidString(fieldSet.readString("anejoOtros"))) {
        bienOld.setAnejoOtros(fieldSet.readString("anejoOtros"));
      }
      if (isValidString(fieldSet.readString("notas1"))) {
        bienOld.setNotas1(fieldSet.readString("notas1"));
      }
      if (isValidString(fieldSet.readString("notas2"))) {
        bienOld.setNotas2(fieldSet.readString("notas2"));
      }

      /**contrato_bien***/
      //solo se crea la relación si no existe prevciamente
      Contrato contrato = contratoRepository.findByIdCarga(fieldSet.readString("contrato.id")).orElse(null);
      if (contrato != null) {
        ContratoBien contratoBienOld = contratoBienRepository
          .findByContratoIdAndBienId(contrato.getId(), bienOld.getId()).orElse(null);
        if (contratoBienOld == null) {
          ContratoBien contratoBien = new ContratoBien();
          contratoBien.setActivo(true);
          contratoBien.setEstado(true);
          if (isValidString(fieldSet.readString("importeGarantizado"))) {
            contratoBien.setImporteGarantizado(fieldSet.readDouble("importeGarantizado"));
          }
          if (isValidString(fieldSet.readString("liquidez.codigo"))) {
            contratoBien.setLiquidez(createLiquidez(fieldSet.readString("liquidez.codigo")));
          }
          if (isValidString(fieldSet.readString("responsabilidadHipotecaria"))) {
            contratoBien.setResponsabilidadHipotecaria(fieldSet.readDouble("responsabilidadHipotecaria"));
          }

          if (isValidString(fieldSet.readString("tipoGarantia.codigo"))) {
            contratoBien.setTipoGarantia(createTipoGarantia(fieldSet.readString("tipoGarantia.codigo")));
          }
          contratoBien.setContrato(contrato);
          contratoBien.setBien(bienOld);
          bienOld.getContratos().add(contratoBien);
        }
      } else {
        throw new InvalidDataException(bienOld);
      }
      return bienOld;
    } else {
      Bien bien = new Bien();
      bien.setIdCarga(fieldSet.readString("id"));
      if (isValidString(fieldSet.readString("idHaya"))) {
        bien.setIdHaya(fieldSet.readString("idHaya"));
      }
      if (isValidString(fieldSet.readString("cliente.id"))) {
        bien.setIdOrigen(fieldSet.readString("cliente.id"));
      }
      if (isValidString(fieldSet.readString("cliente2.id"))) {
        bien.setIdOrigen2(fieldSet.readString("cliente2.id"));
      }
      if (isValidString(fieldSet.readString("tipoBien.codigo"))) {
        bien.setTipoBien(createTipoBien(fieldSet.readString("tipoBien.codigo")));
      }
      if (isValidString(fieldSet.readString("subTipoBien.codigo"))) {
        bien.setSubTipoBien(createSubTipoBien(fieldSet.readString("subTipoBien.codigo")));
      }
      if (isValidString(fieldSet.readString("estado"))) {
        bien.setActivo(createBooleanValue(getIntValue(fieldSet.readString("estado"))));
      } else {
        bien.setActivo(true);
      }
      if (isValidString(fieldSet.readString("referenciaCatastral"))) {
        bien.setReferenciaCatastral(fieldSet.readString("referenciaCatastral"));
      }
      if (isValidString(fieldSet.readString("finca"))) {
        bien.setFinca(fieldSet.readString("finca"));
      }
      if (isValidString(fieldSet.readString("rango"))) {
        bien.setRango(fieldSet.readDouble("rango"));
      }
      if (isValidString(fieldSet.readString("localidadRegistro"))) {
        bienOld.setRegistro(fieldSet.readString("localidadRegistro"));
      }
      if (isValidString(fieldSet.readString("registro"))) {
        bien.setRegistro(fieldSet.readString("registro"));
      }
      if (isValidString(fieldSet.readString("tomo"))) {
        bien.setTomo(getIntValue(fieldSet.readString("tomo")));
      }
      if (isValidString(fieldSet.readString("libro"))) {
        bien.setLibro(getIntValue(fieldSet.readString("libro")));
      }
      if (isValidString(fieldSet.readString("folio"))) {
        bien.setFolio(getIntValue(fieldSet.readString("folio")));
      }
      if (isValidString(fieldSet.readString("fechaInscripcion"))) {
        Date fechaInscripcion = fieldSet.readDate("fechaInscripcion", "dd/MM/yyyy");
        if (fechaInscripcion != new Date("11/11/1111"))
          bien.setFechaInscripcion(fieldSet.readDate("fechaInscripcion", "dd/MM/yyyy"));
      }
      if (isValidString(fieldSet.readString("idufir"))) {
        bien.setIdufir(fieldSet.readString("idufir"));
      }
      if (isValidString(fieldSet.readString("reoOrigen"))) {
        bien.setReoOrigen(fieldSet.readString("reoOrigen"));
      }
      if (isValidString(fieldSet.readString("fechaReoConversion"))) {
        bien.setFechaReoConversion(fieldSet.readDate("fechaReoConversion", "dd/MM/yyyy"));
      }
      if (isValidString(fieldSet.readString("fechaReoConversion"))) {
        bien.setFechaReoConversion(fieldSet.readDate("fechaReoConversion", "dd/MM/yyyy"));
      }
      if (isValidString(fieldSet.readString("posesionNegociada"))) {
        bien.setPosesionNegociada(createBooleanValue(fieldSet.readString("posesionNegociada")));
      } else {
        bien.setPosesionNegociada(false);
      }
      if (isValidString(fieldSet.readString("subasta"))) {
        bien.setSubasta(createBooleanValue(fieldSet.readString("subasta")));
      } else {
        bien.setSubasta(false);
      }
      if (isValidString(fieldSet.readString("tipoVia.codigo"))) {
        bien.setTipoVia(createTipoVia(fieldSet.readString("tipoVia.codigo")));
      }
      if (isValidString(fieldSet.readString("nombreVia"))) {
        bien.setNombreVia(fieldSet.readString("nombreVia"));
      }
      if (isValidString(fieldSet.readString("numero"))) {
        bien.setNumero(fieldSet.readString("numero"));
      }
      if (isValidString(fieldSet.readString("portal"))) {
        bien.setPortal(fieldSet.readString("portal"));
      }
      if (isValidString(fieldSet.readString("bloque"))) {
        bien.setBloque(fieldSet.readString("bloque"));
      }
      if (isValidString(fieldSet.readString("escalera"))) {
        bien.setEscalera(fieldSet.readString("escalera"));
      }
      if (isValidString(fieldSet.readString("piso"))) {
        bien.setPiso(fieldSet.readString("piso"));
      }
      if (isValidString(fieldSet.readString("puerta"))) {
        bien.setPuerta(fieldSet.readString("puerta"));
      }
      if (isValidString(fieldSet.readString("entorno.codigo"))) {
        bien.setEntorno(createEntorno(fieldSet.readString("entorno.codigo")));
      }
      if (isValidString(fieldSet.readString("municipio.codigo"))) {
        bien.setMunicipio(createMunicipio(fieldSet.readString("municipio.codigo")));
      }
      if (isValidString(fieldSet.readString("localidad.codigo"))) {
        bien.setLocalidad(createLocalidad(fieldSet.readString("localidad.codigo")));
      }
      if (isValidString(fieldSet.readString("provincia.codigo"))) {
        bien.setProvincia(createProvincia(fieldSet.readString("provincia.codigo")));
      }
      if (isValidString(fieldSet.readString("comentario"))) {
        bien.setComentarioDireccion(fieldSet.readString("comentario"));
      }
      if (isValidString(fieldSet.readString("codigoPostal"))) {
        bien.setCodigoPostal(fieldSet.readString("codigoPostal"));
      }
      if (isValidString(fieldSet.readString("pais.codigo"))) {
        bien.setPais(createPais(fieldSet.readString("pais.codigo")));
      }
      if (isValidString(fieldSet.readString("longitud"))) {
        bien.setLongitud(fieldSet.readDouble("longitud"));
      }
      if (isValidString(fieldSet.readString("latitud"))) {
        bien.setLatitud(fieldSet.readDouble("latitud"));
      }
      if (isValidString(fieldSet.readString("tipoActivo.codigo"))) {
        bien.setTipoActivo(createTipoActivo(fieldSet.readString("tipoActivo.codigo")));
      }
      if (isValidString(fieldSet.readString("uso"))) {
        bien.setUso(createUso(fieldSet.readString("uso")));
      }
      if (isValidString(fieldSet.readString("anioConstruccion"))) {
        bien.setAnioConstruccion(fieldSet.readInt("anioConstruccion"));
      }
      if (isValidString(fieldSet.readString("superficieSuelo"))) {
        bien.setSuperficieSuelo(fieldSet.readDouble("superficieSuelo"));
      }
      if (isValidString(fieldSet.readString("superficieConstruida"))) {
        bien.setSuperficieConstruida(fieldSet.readDouble("superficieConstruida"));
      }
      if (isValidString(fieldSet.readString("superficieUtil"))) {
        bien.setSuperficieUtil(fieldSet.readDouble("superficieUtil"));
      }
      if (isValidString(fieldSet.readString("anejoGaraje"))) {
        bien.setAnejoGaraje(fieldSet.readString("anejoGaraje"));
      }
      if (isValidString(fieldSet.readString("anejoTrastero"))) {
        bien.setAnejoTrastero(fieldSet.readString("anejoTrastero"));
      }
      if (isValidString(fieldSet.readString("anejoOtros"))) {
        bien.setAnejoOtros(fieldSet.readString("anejoOtros"));
      }
      if (isValidString(fieldSet.readString("notas1"))) {
        bien.setNotas1(fieldSet.readString("notas1"));
      }
      if (isValidString(fieldSet.readString("notas2"))) {
        bien.setNotas2(fieldSet.readString("notas2"));
      }
      Contrato contrato = contratoRepository.findByIdCarga(fieldSet.readString("contrato.id")).orElse(null);
      ContratoBien contratoBien = new ContratoBien();
      contratoBien.setActivo(true);
      contratoBien.setEstado(true);
      if (isValidString(fieldSet.readString("importeGarantizado"))) {
        contratoBien.setImporteGarantizado(fieldSet.readDouble("importeGarantizado"));
      }
      if (isValidString(fieldSet.readString("liquidez.codigo"))) {
        //contratoBien.setLiquidez(createLiquidez(fieldSet.readString("tipoGarantia.codigo")));
      }
      if (isValidString(fieldSet.readString("responsabilidadHipotecaria"))) {
        contratoBien.setResponsabilidadHipotecaria(fieldSet.readDouble("responsabilidadHipotecaria"));
      }

      if (isValidString(fieldSet.readString("tipoGarantia.codigo"))) {
        contratoBien.setTipoGarantia(createTipoGarantia(fieldSet.readString("tipoGarantia.codigo")));
      }

      if (contrato != null) {
        contratoBien.setContrato(contrato);
      }
      contratoBien.setBien(bien);
      bien.getContratos().add(contratoBien);
      return bien;
    }

  }

  private TipoVia createTipoVia(String codigoTipoVia) {
    return tipoViaRepository.findByCodigo(codigoTipoVia).orElse(null);
  }

  private Entorno createEntorno(String codigoEntorno) {
    String codToUse = getIntValue(codigoEntorno);
    return entornoRepository.findByCodigo(codToUse).orElse(null);
  }

  private Municipio createMunicipio(String codigoMunicipio) {
    String codToUse = getIntValue(codigoMunicipio);
    return municipioRepository.findByCodigo(codToUse).orElse(null);
  }

  private Localidad createLocalidad(String codigoLocalidad) {
    return localidadRepository.findByValor(codigoLocalidad).orElse(null);
  }

  private Provincia createProvincia(String codigoProvincia) {
    String codToUse = getIntValue(codigoProvincia);
    return provinciaRepository.findByCodigo(codToUse).orElse(null);
  }

  private Pais createPais(String codigopais) {
    String codToUse = getIntValue(codigopais);
    return paisRepository.findByCodigo(codToUse).orElse(null);
  }

  private TipoBien createTipoBien(String codigoTipoBien) {
    String codToUse = getIntValue(codigoTipoBien);
    return tipoBienRepository.findByCodigo(codToUse).orElse(null);
  }

  private SubTipoBien createSubTipoBien(String codigoSubTipoBien) {
    String codToUse = getIntValue(codigoSubTipoBien);
    return subTipoBienRepository.findByCodigo(codToUse).orElse(null);
  }

  private Liquidez createLiquidez(String codigoLiquidez) {
    return liquidezRepository.findByCodigo(codigoLiquidez).orElse(null);
  }

  private TipoActivo createTipoActivo(String codigoTipoActivo) {
    String codToUse = getIntValue(codigoTipoActivo);
    return tipoActivoRepository.findByCodigo(codToUse).orElse(null);
  }

  private Uso createUso(String codigoUso) {
    String codToUse = getIntValue(codigoUso);
    return usoRepository.findByCodigo(codToUse).orElse(null);
  }

  private TipoGarantia createTipoGarantia(String codigoTipoGarantia) {
    String codToUse = getIntValue(codigoTipoGarantia);
    return tipoGarantiaRepository.findByCodigo(codToUse).orElse(null);
  }

  private Boolean isValidString(String string) {
    return string != null && !string.equals("");
  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }

  private Boolean createBooleanValue(String code) {
    String codeToUSe = getIntValue(code);
    switch (codeToUSe) {
      case "0":
        return false;
      case "1":
        return true;
      default:
        return null;
    }
  }

}
