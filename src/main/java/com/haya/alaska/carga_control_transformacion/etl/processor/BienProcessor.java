package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.infrastructure.repository.CargaRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.infrastructure.repository.ContratoBienRepository;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.espejo.contrato_bien.domain.ContratoBienEspejo;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import com.haya.alaska.interviniente.infrastructure.util.IntervinienteUtil;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien;
import com.haya.alaska.sub_tipo_bien.infrastructure.repository.SubTipoBienRepository;
import com.haya.alaska.subasta.infrastructure.repository.SubastaRepository;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.infrastructure.repository.TasacionRepository;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;
import com.haya.alaska.tipo_garantia.infrastructure.repository.TipoGarantiaRepository;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.uso.domain.Uso;
import com.haya.alaska.uso.infrastructure.repository.UsoRepository;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.valoracion.infrastructure.repository.ValoracionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;

import java.util.Date;
import java.util.HashSet;

@Slf4j
@Component
@StepScope
public class BienProcessor implements ItemProcessor<BienEspejo, Bien> {

  @Autowired
  private ServicioMaestro servicioMaestro;

  @Autowired
  private CarteraRepository carteraRepository;

  @Autowired
  private BienRepository bienRepository;

  @Autowired
  private ContratoRepository contratoRepository;

  @Autowired
  private TipoGarantiaRepository tipoGarantiaRepository;

  @Autowired
  private SubastaRepository subastaRepository;

  @Autowired
  private PaisRepository paisRepository;

  @Autowired
  private ProvinciaRepository provinciaRepository;

  @Autowired
  private UsoRepository usoRepository;

  @Autowired
  private TipoActivoRepository tipoActivoRepository;

  @Autowired
  private TipoBienRepository tipoBienRepository;

  @Autowired
  private TipoViaRepository tipoViaRepository;

  @Autowired
  private SubTipoBienRepository subTipoBienRepository;

  @Autowired
  private EntornoRepository entornoRepository;

  @Autowired
  private MunicipioRepository municipioRepository;

  @Autowired
  private LocalidadRepository localidadRepository;

  @Autowired
  private TasacionRepository tasacionRepository;

  @Autowired
  private ValoracionRepository valoracionRepository;

  @Autowired
  private ContratoBienRepository contratoBienRepository;

  @Autowired
  private CargaRepository cargaRepository;

  @Autowired
  private IntervinienteUtil intervinienteUtil;

  @Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  @Value("#{jobParameters['manual']}")
  private String manual;

  @Value("#{jobParameters['multicartera']}")
  private Boolean multicartera;

  @Override
  public Bien process(BienEspejo item) throws Exception {

    Pais pais = null;
    Provincia provincia = null;
    TipoActivo tipoActivo = null;
    Uso uso = null;
    TipoBien tipoBien = null;
    SubTipoBien subTipoBien = null;
    Entorno entorno = null;
    Municipio municipio = null;
    Localidad localidad = null;
    TipoVia tipoVia = null;

    if (item.getPais() != null)
      pais = paisRepository.findByCodigo(item.getPais().getCodigo()).orElse(null);

    if (item.getProvincia() != null)
      provincia = provinciaRepository.findByCodigo(item.getProvincia().getCodigo()).orElse(null);

    if (item.getTipoActivo() != null)
      tipoActivo = tipoActivoRepository.findByCodigo(item.getTipoActivo().getCodigo()).orElse(null);

    if (item.getUso() != null)
      uso = usoRepository.findByCodigo(item.getUso().getCodigo()).orElse(null);

    if (item.getTipoBien() != null)
      tipoBien = tipoBienRepository.findByCodigo(item.getTipoBien().getCodigo()).orElse(null);

    if (item.getSubTipoBien() != null)
      subTipoBien = subTipoBienRepository.findByCodigo(item.getSubTipoBien().getCodigo()).orElse(null);

    if (item.getMunicipio() != null)
      municipio = municipioRepository.findByCodigo(item.getMunicipio().getCodigo()).orElse(null);

    if (item.getLocalidad() != null)
      localidad = localidadRepository.findByCodigo(item.getLocalidad().getCodigo()).orElse(null);

    if (item.getEntorno() != null)
      entorno = entornoRepository.findByCodigo(item.getEntorno().getCodigo()).orElse(null);

    if (item.getTipoVia() != null)
      tipoVia = tipoViaRepository.findByCodigo(item.getTipoVia().getCodigo()).orElse(null);

    Bien bien = bienRepository.findByIdCarga(item.getIdCarga()).orElse(new Bien());
    bien.setIdHaya(item.getIdHaya());
    bien.setIdDatatape(item.getIdDatatape());
    bien.setIdOrigen(item.getIdOrigen());
    bien.setIdOrigen2(item.getIdOrigen2());
    bien.setTipoBien(tipoBien);
    bien.setSubTipoBien(subTipoBien);
    bien.setReferenciaCatastral(item.getReferenciaCatastral());
    bien.setFinca(item.getFinca());
    bien.setLocalidadRegistro(item.getLocalidadRegistro());
    bien.setRegistro(item.getRegistro());
    bien.setTomo(item.getTomo());
    bien.setLibro(item.getLibro());
    bien.setFolio(item.getFolio());
    bien.setFechaInscripcion(item.getFechaInscripcion());
    bien.setIdufir(item.getIdufir());
    bien.setReoOrigen(item.getReoOrigen());
    bien.setFechaReoConversion(item.getFechaReoConversion());
    bien.setImporteGarantizado(item.getImporteGarantizado());
    bien.setResponsabilidadHipotecaria(item.getResponsabilidadHipotecaria());
    bien.setPosesionNegociada(item.getPosesionNegociada());
    bien.setTramitado(item.getTramitado());
    bien.setSubasta(item.getSubasta());
    bien.setRango(item.getRango());
    bien.setTipoVia(tipoVia);
    bien.setNombreVia(item.getNombreVia());
    bien.setNumero(item.getNumero());
    bien.setPortal(item.getPortal());
    bien.setBloque(item.getBloque());
    bien.setEscalera(item.getEscalera());
    bien.setPiso(item.getPiso());
    bien.setPuerta(item.getPuerta());
    bien.setEntorno(entorno);
    bien.setMunicipio(municipio);
    bien.setLocalidad(localidad);
    bien.setProvincia(provincia);
    bien.setComentarioDireccion(item.getComentarioDireccion());
    bien.setCodigoPostal(item.getCodigoPostal());
    bien.setPais(pais);
    Double latitud = item.getLatitud();
    if (latitud != null)
      bien.setLatitud(item.getLatitud());
    Double longitud = item.getLongitud();
    if (longitud != null)
      bien.setLongitud(item.getLongitud());
    bien.setTipoActivo(tipoActivo);
    bien.setUso(uso);
    bien.setAnioConstruccion(item.getAnioConstruccion());
    bien.setSuperficieSuelo(item.getSuperficieSuelo());
    bien.setSuperficieConstruida(item.getSuperficieConstruida());
    bien.setSuperficieUtil(item.getSuperficieUtil());
    bien.setAnejoGaraje(item.getAnejoGaraje());
    bien.setAnejoTrastero(item.getAnejoTrastero());
    bien.setAnejoOtros(item.getAnejoOtros());
    bien.setNotas1(item.getNotas1());
    bien.setNotas2(item.getNotas2());
    bien.setDescripcion(item.getDescripcion());
    bien.setIdCarga(item.getIdCarga());
    bien.setCarteraREM(item.getCarteraREM());
    bien.setSubcarteraREM(item.getSubcarteraREM());
    bien.setDescripcion(item.getDescripcion());
    bien.setFechaCarga(item.getFechaCarga());
    bien.setImporteBien(item.getImporteBien());

    Cartera cartera = getCartera(carteraId);
    //Este control se deja por si se tiene que añadir una logica adicional para apto automatico o manual
    if (cartera.getDatosCoreBanking() != null && cartera.getDatosCoreBanking() == true) {
      bien.setActivo(true);
    } else {
      bien.setActivo(true);
    }
    //Se borran los datos de tasacion antiguos del bien,
    // no es posible actualizar ya que no existe un idCarga de tasacion
    if (bien.getId() != null) {
      for (Tasacion tasacion : bien.getTasaciones()) {
        tasacionRepository.delete(tasacion);
      }
    }

    bien.setTasaciones(new HashSet<>());

    //Se borran los datos de valoracion antiguos del bien,
    // no es posible actualizar ya que no existe un idCarga de valoracion
    if (bien.getId() != null) {
      for (Valoracion valoracion : bien.getValoraciones()) {
        valoracionRepository.delete(valoracion);
      }
    }

    bien.setValoraciones(new HashSet<>());

    //Se borran los datos de valoracion antiguos del bien,
    // no es posible actualizar ya que no existe un idCarga de valoracion
    if (bien.getId() != null) {
      for (Valoracion valoracion : bien.getValoraciones()) {
        valoracionRepository.delete(valoracion);
      }
    }

    bien.setValoraciones(new HashSet<>());

    //Se borran los datos de cargas antiguos del bien,
    // no es posible actualizar ya que no existe un idCarga de cada Carga
    if (bien.getId() != null) {
      for (Carga carga : bien.getCargas()) {
        cargaRepository.delete(carga);
      }
    }

    bien.setCargas(new HashSet<>());

    for (ContratoBienEspejo contratoBienEspejo : item.getContratos()) {
      if (contratoBienEspejo.getContrato() != null) {
        Contrato contrato = contratoRepository.findByIdCarga(contratoBienEspejo.getContrato().getIdCarga()).orElse(null);
        ContratoBien contratoBien = null;
        if (bien.getId() != null && contrato.getId() != null) {
          contratoBien = contratoBienRepository.findByContratoIdAndBienId(contrato.getId(), bien.getId()).orElse(new ContratoBien());
        } else {
          contratoBien = new ContratoBien();
        }
        contratoBien.setContrato(contrato);
        TipoGarantia tipoGarantia = tipoGarantiaRepository.findByCodigo(contratoBienEspejo.getTipoGarantia().getCodigo()).orElse(null);
        contratoBien.setBien(bien);
        if (cartera.getDatosCoreBanking() != null && cartera.getDatosCoreBanking() == true) {
          contratoBien.setActivo(true);
        } else {
          contratoBien.setActivo(true);
        }
        contratoBien.setEstado(contratoBienEspejo.getEstado());
        contratoBien.setLiquidez(contratoBienEspejo.getLiquidez());
        contratoBien.setRango(contratoBienEspejo.getRango());
        contratoBien.setImporteGarantizado(contratoBienEspejo.getImporteGarantizado());
        contratoBien.setTipoGarantia(tipoGarantia);
        contratoBien.setResponsabilidadHipotecaria(contratoBienEspejo.getResponsabilidadHipotecaria());
        bien.getContratos().add(contratoBien);
      }
    }

    /*if (bien.getLongitud() == null || bien.getLatitud() == null)
      intervinienteUtil.direccionGoogle(bien);*/

    if (cartera != null && item.getIdHaya() == null && !multicartera) {
      bienRepository.save(bien);
      Bien master = callMaster(bien, cartera.getCliente().getNombre());
      // Se asigna el IDHaya que devuelve maestro
      item.setIdHaya(master.getIdHaya());
    }
    item.setFechaCarga(new Date());

    //Se hacen las validaciones con el campo ind_valido de la tabla espejo
    if (manual.equals("true")) {
      if (item.getValido() != null && !item.getValido()) {
        bien = null;
      }
    }

    return bien;
  }

  private Bien callMaster(Bien bien, String cliente) throws NotFoundException {
    // Llamada al servicio de maestros para obtener el idHaya del bien
    try {
      servicioMaestro.registrarBien(bien, cliente);
    } catch (NotFoundException | WebServiceIOException | LockedChangeException e) {
      log.warn(e.getMessage());
      bien.setIdHaya(null);
    }
    log.debug(String.format("Bien.idHaya -> %s", bien.getIdHaya()));
    return bien;
  }

  private Cartera getCartera(String idCargaCartera) {
    return carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(idCargaCartera).orElse(null);
  }
}
