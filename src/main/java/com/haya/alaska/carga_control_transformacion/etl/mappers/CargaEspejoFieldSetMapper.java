package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.espejo.bien.infrastructure.repository.BienEspejoRepository;
import com.haya.alaska.espejo.carga.domain.CargaEspejo;
import com.haya.alaska.tipo_acreedor.domain.TipoAcreedor;
import com.haya.alaska.tipo_acreedor.infrastructure.repository.TipoAcreedorRepository;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import com.haya.alaska.tipo_carga.infrastructure.repository.TipoCargaRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

@Getter
@Setter
public class CargaEspejoFieldSetMapper implements FieldSetMapper<CargaEspejo> {

  private BienEspejoRepository bienEspejoRepository;

  private TipoCargaRepository tipoCargaRepository;

  private TipoAcreedorRepository tipoAcreedorRepository;

  @Override
  public CargaEspejo mapFieldSet(FieldSet fieldSet) throws BindException {
    CargaEspejo carga = new CargaEspejo();
    if (isvalidString(fieldSet.readString("bien.id"))) {
      BienEspejo bien = createBien(fieldSet.readString("bien.id"));
      if (bien != null) {
        carga.getBienes().add(bien);
      }
    }
    if (isvalidString(fieldSet.readString("rangoCarga"))) {
      carga.setRango(fieldSet.readInt("rangoCarga"));
    }
    if (isvalidString(fieldSet.readString("tipoCarga.codigo"))) {
      carga.setTipoCarga(createTipoCarga(fieldSet.readString("tipoCarga.codigo")));
    }
    if (isvalidString(fieldSet.readString("importeCarga"))) {
      carga.setImporte(fieldSet.readDouble("importeCarga"));
    }
    if (isvalidString(fieldSet.readString("tipoAcreedor.codigo"))) {
      carga.setTipoAcreedor(createTipoAcreedor(fieldSet.readString("tipoAcreedor.codigo")));
    }
    if (isvalidString(fieldSet.readString("nombreAcreedor"))) {
      carga.setNombreAcreedor(fieldSet.readString("nombreAcreedor"));
    }
    if (isvalidString(fieldSet.readString("fechaAnotacion"))) {
      carga.setFechaAnotacion(fieldSet.readDate("fechaAnotacion", "dd/MM/yyyy"));
    }
    if (isvalidString(fieldSet.readString("incluidaOtrosColaterales"))) {
      carga.setIncluidaOtrosColaterales(createBooleanValue(fieldSet.readString("incluidaOtrosColaterales")));
    } else {
      carga.setIncluidaOtrosColaterales(false);
    }
    if (isvalidString(fieldSet.readString("fechaVencimientoCarga"))) {
      carga.setFechaVencimiento(fieldSet.readDate("fechaVencimientoCarga", "dd/MM/yyyy"));
    }

    return carga;
  }

  private BienEspejo createBien(String idCargaBien) {
    BienEspejo bien = bienEspejoRepository.findByIdCarga(idCargaBien).orElse(null);
    return bien;
  }

  private TipoCarga createTipoCarga(String codigoTipoCarga) {
    String codToUse = getIntValue(codigoTipoCarga);
    TipoCarga tipoCarga = tipoCargaRepository.findByCodigo(codToUse).orElse(null);
    return tipoCarga;
  }

  private TipoAcreedor createTipoAcreedor(String codigoTipoAcreedor) {
    String codToUse = getIntValue(codigoTipoAcreedor);
    TipoAcreedor tipoAcreedor = tipoAcreedorRepository.findByCodigo(codToUse).orElse(null);
    return tipoAcreedor;
  }

  private Boolean isvalidString(String string) {
    if (string != null && !string.equals("")) {
      return true;
    } else {
      return false;
    }
  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }

  private Boolean createBooleanValue(String code) {
    String codeToUse = getIntValue(code);
    switch (codeToUse) {
      case "0":
        return false;
      case "1":
        return true;
      default:
        return null;
    }
  }

}
