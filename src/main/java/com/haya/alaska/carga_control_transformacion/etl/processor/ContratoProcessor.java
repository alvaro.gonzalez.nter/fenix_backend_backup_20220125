package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.espejo.contrato.infrastructure.repository.ContratoEspejoRepository;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;

import java.util.Date;

@Slf4j
@Getter
@Setter
@Component("contratoProcessor")
@StepScope
public class ContratoProcessor implements ItemProcessor<Contrato, Contrato> {

  @Autowired
  private ServicioMaestro servicioMaestro;

  @Autowired
  private ContratoRepository contratoRepository;

  @Autowired
  private ContratoEspejoRepository contratoEspejoRepository;

  @Autowired
  private CarteraRepository carteraRepository;

  @Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  @Value("#{jobParameters['multicartera']}")
  private Boolean multicartera;


  @Override
  public Contrato process(Contrato item) throws Exception {
    //si existe un contrato con el idCarga se actualiza el Id del item para no duplicar contratos
    Contrato contratoOld = contratoRepository.findByIdCarga(item.getIdCarga()).orElse(new Contrato());
    ContratoEspejo contratoEspejo = contratoEspejoRepository.findByIdCarga(item.getIdCarga()).orElse(new ContratoEspejo());
    contratoOld.setEstadoContrato(contratoEspejo.getEstadoContrato());
    contratoOld.setCalculado(false);
    contratoOld.setEsRepresentante(false);


    Cartera cartera = getCartera(carteraId);
    //Este control se deja por si se tiene que añadir una logica adicional para apto automatico o manual
    if (cartera.getDatosCoreBanking() != null && cartera.getDatosCoreBanking() == true) {
      contratoOld.setActivo(true);
    } else {
      contratoOld.setActivo(true);
    }

    contratoOld.setExpediente(item.getExpediente());
    contratoOld.setEstrategia(item.getEstrategia());
    contratoOld.setSaldos(item.getSaldos());
    contratoOld.setIdOrigen(item.getIdOrigen());
    contratoOld.setIdOrigen2(item.getIdOrigen2());
    contratoOld.setIdDatatape(item.getIdDatatape());
    contratoOld.setEnRevision(item.getEnRevision());
    contratoOld.setVencimientoAnticipado(item.getVencimientoAnticipado());
    contratoOld.setProducto(item.getProducto());
    contratoOld.setSistemaAmortizacion(item.getSistemaAmortizacion());
    contratoOld.setDescripcionProducto(item.getDescripcionProducto());
    contratoOld.setPeriodicidadLiquidacion(item.getPeriodicidadLiquidacion());
    contratoOld.setIndiceReferencia(item.getIndiceReferencia());
    contratoOld.setSituacion(item.getSituacion());
    contratoOld.setDiferencial(item.getDiferencial());
    contratoOld.setTin(item.getTin());
    contratoOld.setTae(item.getTae());
    contratoOld.setReestructurado(item.getReestructurado());
    contratoOld.setTipoReestructuracion(item.getTipoReestructuracion());
    contratoOld.setClasificacionContable(item.getClasificacionContable());
    contratoOld.setFechaFormalizacion(item.getFechaFormalizacion());
    contratoOld.setFechaVencimientoInicial(item.getFechaVencimientoInicial());
    contratoOld.setFechaVencimientoAnticipado(item.getFechaVencimientoAnticipado());
    contratoOld.setFechaPaseRegular(item.getFechaPaseRegular());
    contratoOld.setCuotasPendientes(item.getCuotasPendientes());
    contratoOld.setFechaLiquidacionCuota(item.getFechaLiquidacionCuota());
    contratoOld.setImporteInicial(item.getImporteInicial());
    contratoOld.setImporteDispuesto(item.getImporteDispuesto());
    contratoOld.setCuotasTotales(item.getCuotasTotales());
    contratoOld.setImporteUltimaCuota(item.getImporteUltimaCuota());
    contratoOld.setFechaPrimerImpago(item.getFechaPrimerImpago());
    contratoOld.setDiasImpago(item.getDiasImpago());
    contratoOld.setNumeroCuotasImpagadas(item.getNumeroCuotasImpagadas());
    contratoOld.setRestoHipotecario(item.getRestoHipotecario());
    contratoOld.setOrigen(item.getOrigen());
    contratoOld.setGastosJudiciales(item.getGastosJudiciales());
    contratoOld.setImporteCuotasImpagadas(item.getImporteCuotasImpagadas());
    contratoOld.setNotas1(item.getNotas1());
    contratoOld.setNotas2(item.getNotas2());
    //TODO COMO PONER EN EL EXCEL LAS CAMPAÑAS?
    //contratoOld.setCampania(item.getCampania());
    contratoOld.setDeudaImpagada(item.getDeudaImpagada());
    contratoOld.setIdCarga(item.getIdCarga());
    contratoOld.setCarteraREM(item.getCarteraREM());
    contratoOld.setRangoHipotecario(item.getRangoHipotecario());
    contratoOld.setTerritorial(item.getTerritorial());
    contratoOld.setSaldoGestion(item.getSaldoGestion());
    contratoOld.setFechaCarga(item.getFechaCarga());
    contratoOld.setFechaPrimeraAsignacion(item.getFechaPrimeraAsignacion());
    contratoOld.setFechaUltimaAsignacion(item.getFechaUltimaAsignacion());

    if (cartera != null && item.getIdHaya() == null && !multicartera) {
      contratoRepository.save(contratoOld);
      Contrato master = callContractsMaster(contratoOld, cartera.getCliente().getNombre());
      // Se asigna el IDHaya que devuelve maestro
      contratoOld.setIdHaya(master.getIdHaya());
    }
    contratoOld.setFechaCarga(new Date());
    contratoOld.setReestructurado(false);
    contratoOld.setVencimientoAnticipado(false);
    return contratoOld;

  }

  private Contrato callContractsMaster(Contrato contrato, String cliente) throws NotFoundException {

    // Llamada al servicio de maestros para obtener el idHaya del contrato
    try {
      servicioMaestro.registrarContrato(contrato, cliente);
    } catch (NotFoundException | WebServiceIOException | LockedChangeException e) {
      log.warn(e.getMessage());
      contrato.setIdHaya(null);
    }
    log.debug(String.format("Contrato.idHaya -> %s", contrato.getIdHaya()));
    return contrato;
  }

  private Cartera getCartera(String idCargaCartera) {
    try {
      var a = carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(idCargaCartera);
      Cartera cartera = a.get();
      return cartera;
    } catch (Exception ex) {
      System.out.println(idCargaCartera + "EXCEPCION AL BUSCAR CARTERA");
      ex.printStackTrace();
    }
    return null;
  }

}
