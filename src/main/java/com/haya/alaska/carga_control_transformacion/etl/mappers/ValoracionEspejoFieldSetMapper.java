package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.espejo.bien.infrastructure.repository.BienEspejoRepository;
import com.haya.alaska.espejo.valoracion.domain.ValoracionEspejo;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.tipo_valoracion.domain.TipoValoracion;
import com.haya.alaska.tipo_valoracion.infrastructure.repository.TipoValoracionRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

@Getter
@Setter
public class ValoracionEspejoFieldSetMapper implements FieldSetMapper<ValoracionEspejo> {

  private BienEspejoRepository bienEspejoRepository;

  private TipoValoracionRepository tipoValoracionRepository;

  private LiquidezRepository liquidezRepository;


  @Override
  public ValoracionEspejo mapFieldSet(FieldSet fieldSet) throws BindException {
    ValoracionEspejo valoracion = new ValoracionEspejo();
    if (isvalidString(fieldSet.readString("bien.id"))) {
      BienEspejo bien = createBien(fieldSet.readString("bien.id"));
      if (bien != null) {
        //valoracion.setBienes(bien);
        valoracion.setBien(bien);
      }
    }
    if (isvalidString(fieldSet.readString("valorador"))) {
      valoracion.setValorador(fieldSet.readString("valorador"));
    }
    if (isvalidString(fieldSet.readString("tipoValoracion.codigo"))) {
      valoracion.setTipoValoracion(createTipoValoracion(fieldSet.readString("tipoValoracion.codigo")));
    }
    if (isvalidString(fieldSet.readString("importeValoracion"))) {
      valoracion.setImporte(fieldSet.readDouble("importeValoracion"));
    }
    if (isvalidString(fieldSet.readString("fechaValoracion"))) {
      valoracion.setFecha(fieldSet.readDate("fechaValoracion", "dd/MM/yyyy"));
    }
    if (isvalidString(fieldSet.readString("liquidez.codigo"))) {
      valoracion.setLiquidez(createLiquidez(fieldSet.readString("liquidez.codigo")));
    }

    return valoracion;
  }

  private BienEspejo createBien(String idCargaBien) {
    BienEspejo bien = bienEspejoRepository.findByIdCarga(idCargaBien).orElse(null);
    return bien;
  }

  private TipoValoracion createTipoValoracion(String codigoTipoValoracion) {
    String codToUse = getIntValue(codigoTipoValoracion);
    TipoValoracion tipoValoracion = tipoValoracionRepository.findByCodigo(codToUse).orElse(null);
    return tipoValoracion;
  }

  private Liquidez createLiquidez(String codigoLiquidez) {
    Liquidez liquidez = liquidezRepository.findByCodigo(codigoLiquidez).orElse(null);
    return liquidez;
  }

  private Boolean isvalidString(String string) {
    if (string != null && !string.equals("")) {
      return true;
    } else {
      return false;
    }
  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }
}
