package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.espejo.direccion.domain.DireccionEspejo;
import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import com.haya.alaska.espejo.interviniente.infrastructure.repository.IntervinienteEspejoRepository;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@StepScope
public class DireccionEspejoProcessor implements ItemProcessor<DireccionEspejo, DireccionEspejo> {

  @Autowired
  private IntervinienteEspejoRepository intervinienteRepository;


  @Override
  public DireccionEspejo process(DireccionEspejo item) throws Exception {
    IntervinienteEspejo interviniente = intervinienteRepository.findById(item.getInterviniente().getId()).orElse(null);

    if (interviniente != null) {
      Set<DireccionEspejo> direcciones = interviniente.getDirecciones();
      if (direcciones != null && direcciones.isEmpty()) {
        item.setPrincipal(true);
      } else {
        item.setPrincipal(false);
      }
    }

    return item;

  }
}
