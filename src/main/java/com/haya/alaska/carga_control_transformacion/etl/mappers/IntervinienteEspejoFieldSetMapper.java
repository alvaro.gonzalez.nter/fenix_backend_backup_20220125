package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.espejo.contrato.infrastructure.repository.ContratoEspejoRepository;
import com.haya.alaska.espejo.contrato_interviniente.domain.ContratoIntervinienteEspejo;
import com.haya.alaska.espejo.interviniente.domain.IntervinienteEspejo;
import com.haya.alaska.espejo.interviniente.infrastructure.repository.IntervinienteEspejoRepository;
import com.haya.alaska.estado_civil.domain.EstadoCivil;
import com.haya.alaska.estado_civil.infrastructure.repository.EstadoCivilRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.sexo.domain.Sexo;
import com.haya.alaska.sexo.infrastructure.repository.SexoRepository;
import com.haya.alaska.shared.exceptions.InvalidDataException;
import com.haya.alaska.tipo_contrato.domain.TipoContrato;
import com.haya.alaska.tipo_contrato.infrastructure.repository.TipoContratoRepository;
import com.haya.alaska.tipo_documento.domain.TipoDocumento;
import com.haya.alaska.tipo_documento.infrastructure.repository.TipoDocumentoRepository;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import com.haya.alaska.tipo_ocupacion.domain.TipoOcupacion;
import com.haya.alaska.tipo_ocupacion.infrastructure.repository.TipoOcupacionRepository;
import com.haya.alaska.tipo_prestacion.domain.TipoPrestacion;
import com.haya.alaska.tipo_prestacion.infrastructure.repository.TipoPrestacionRepository;
import com.haya.alaska.vulnerabilidad.domain.Vulnerabilidad;
import com.haya.alaska.vulnerabilidad.infrastructure.repository.VulnerabilidadRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

@Getter
@Setter
public class IntervinienteEspejoFieldSetMapper implements FieldSetMapper<IntervinienteEspejo> {

  private TipoIntervencionRepository tipoIntervencionRepository;

  private TipoDocumentoRepository tipoDocumentoRepository;

  private PaisRepository paisRepository;

  private TipoOcupacionRepository tipoOcupacionRepository;

  private TipoContratoRepository tipoContratoRepository;

  private TipoPrestacionRepository tipoPrestacionRepository;

  private VulnerabilidadRepository vulnerabilidadRepository;

  private ContratoEspejoRepository contratoEspejoRepository;

  private IntervinienteEspejoRepository intervinienteEspejoRepository;

  private ContratoIntervinienteRepository contratoIntervinienteRepository;

  private EstadoCivilRepository estadoCivilRepository;

  private SexoRepository sexoRepository;

  @Override
  public IntervinienteEspejo mapFieldSet(FieldSet fieldSet) throws BindException {
    IntervinienteEspejo intervinienteOld = intervinienteEspejoRepository.findByIdCarga(fieldSet.readString("id")).orElse(null);

    if (intervinienteOld != null) {
      intervinienteOld.setIdCarga(fieldSet.readString("id"));

      if (isvalidString(fieldSet.readString("idOrigen"))) {
        intervinienteOld.setIdOrigen(fieldSet.readString("idOrigen"));
      }
      if (isvalidString(fieldSet.readString("estado"))) {
        intervinienteOld.setActivo(createBooleanValue(fieldSet.readString("estado")));
      } else {
        intervinienteOld.setActivo(true);
      }

      if (isvalidString(fieldSet.readString("tipoDocumento.codigo"))) {
        intervinienteOld.setTipoDocumento(createTipoDocumento((fieldSet.readString("tipoDocumento.codigo"))));
      }
      if (isvalidString(fieldSet.readString("numeroDocumento"))) {
        intervinienteOld.setNumeroDocumento(fieldSet.readString("numeroDocumento"));
      }
      if (isvalidString(fieldSet.readString("nombre"))) {
        intervinienteOld.setNombre(fieldSet.readString("nombre"));
      }
      if (isvalidString(fieldSet.readString("apellidos"))) {
        intervinienteOld.setApellidos(fieldSet.readString("apellidos"));
      }
      if (isvalidString(fieldSet.readString("sexo"))) {
        intervinienteOld.setSexo(createSexoEnum(fieldSet.readString("sexo")));
      }
      if (isvalidString(fieldSet.readString("estadoCivil"))) {
        intervinienteOld.setEstadoCivil(createEstadoCivil(fieldSet.readString("estadoCivil")));
      }
      if (isvalidString(fieldSet.readString("fechaNacimiento"))) {
        intervinienteOld.setFechaNacimiento(fieldSet.readDate("fechaNacimiento", "dd/MM/yyyy"));
      }
      if (isvalidString(fieldSet.readString("razonSocial"))) {
        intervinienteOld.setRazonSocial(fieldSet.readString("razonSocial"));
        intervinienteOld.setPersonaJuridica(true);
      } else {
        intervinienteOld.setPersonaJuridica(false);
      }
      if (isvalidString(fieldSet.readString("fechaConstitucion"))) {
        intervinienteOld.setFechaConstitucion(fieldSet.readDate("fechaConstitucion", "dd/MM/yyyy"));
      }
      if (isvalidString(fieldSet.readString("residente"))) {
        intervinienteOld.setResidencia(createPais(fieldSet.readString("residente")));
      }
      if (isvalidString(fieldSet.readString("pais.codigo"))) {
        intervinienteOld.setNacionalidad(createPais(fieldSet.readString("pais.codigo")));
        intervinienteOld.setPaisNacimiento(createPais(fieldSet.readString("pais.codigo")));
      }
      if (isvalidString(fieldSet.readString("tipoOcupacion.codigo"))) {
        intervinienteOld.setTipoOcupacion(createTipoOcupacion(fieldSet.readString("tipoOcupacion.codigo")));
      }
      if (isvalidString(fieldSet.readString("empresa"))) {
        intervinienteOld.setEmpresa(fieldSet.readString("empresa"));
      }
      if (isvalidString(fieldSet.readString("telefonoEmpresa"))) {
        intervinienteOld.setTelefonoEmpresa(fieldSet.readString("telefonoEmpresa"));
      }
      if (isvalidString(fieldSet.readString("contactoEmpresa"))) {
        intervinienteOld.setContactoEmpresa(fieldSet.readString("contactoEmpresa"));
      }
      if (isvalidString(fieldSet.readString("tipoContrato.codigo"))) {
        intervinienteOld.setTipoContrato(createTipoContrato(fieldSet.readString("tipoContrato.codigo")));
      }
      if (isvalidString(fieldSet.readString("codigoSocioProfesional"))) {
        intervinienteOld.setCodigoSocioProfesional(fieldSet.readString("codigoSocioProfesional"));
      }
      if (isvalidString(fieldSet.readString("cnae"))) {
        intervinienteOld.setCnae(fieldSet.readString("cnae"));
      }
      if (isvalidString(fieldSet.readString("ingresosNetosMensuales"))) {
        intervinienteOld.setIngresosNetosMensuales(fieldSet.readDouble("ingresosNetosMensuales"));
      }
      if (isvalidString(fieldSet.readString("descripcionActividad"))) {
        intervinienteOld.setDescripcionActividad(fieldSet.readString("descripcionActividad"));
      }
      if (isvalidString(fieldSet.readString("numHijosDependientes"))) {
        intervinienteOld.setNumHijosDependientes(fieldSet.readInt(getIntValue(fieldSet.readString("numHijosDependientes"))));
      }
      if (isvalidString(fieldSet.readString("otrosRiesgos"))) {
        intervinienteOld.setOtrosRiesgos(createBooleanValue(fieldSet.readString("otrosRiesgos")));
      } else {
        intervinienteOld.setOtrosRiesgos(false);
      }
      if (isvalidString(fieldSet.readString("vulnerabilidad"))) {
        intervinienteOld.setVulnerabilidad(createBooleanValue(fieldSet.readString("vulnerabilidad")));
      } else {
        intervinienteOld.setVulnerabilidad(false);
      }
      if (isvalidString(fieldSet.readString("pensionista"))) {
        intervinienteOld.setPensionista(createBooleanValue(fieldSet.readString("pensionista")));
      } else {
        intervinienteOld.setPensionista(false);
      }
      if (isvalidString(fieldSet.readString("dependiente"))) {
        intervinienteOld.setDependiente(createBooleanValue(fieldSet.readString("dependiente")));
      } else {
        intervinienteOld.setDependiente(false);
      }
      if (isvalidString(fieldSet.readString("discapacitados"))) {
        intervinienteOld.setDiscapacitados(createBooleanValue(fieldSet.readString("discapacitados")));
      } else {
        intervinienteOld.setDiscapacitados(false);
      }
      if (isvalidString(fieldSet.readString("tipoPrestacion.codigo"))) {
        intervinienteOld.setTipoPrestacion(createTipoPrestacion(fieldSet.readString("tipoPrestacion.codigo")));
      }
      if (isvalidString(fieldSet.readString("otrasSituacionesVulnerabilidad.codigo"))) {
        intervinienteOld.setOtrasSituacionesVulnerabilidad(createVulnerabilidad(fieldSet.readString("otrasSituacionesVulnerabilidad.codigo")));
      }
      if (isvalidString(fieldSet.readString("notas1"))) {
        intervinienteOld.setNotas1(fieldSet.readString("notas1"));
      }
      if (isvalidString(fieldSet.readString("notas2"))) {
        intervinienteOld.setNotas2(fieldSet.readString("notas2"));
      }

      /**contrato_interviniente***/
      //solo se crea la relación si no existe previamente
      ContratoEspejo contrato = contratoEspejoRepository.findByIdCarga(fieldSet.readString("contrato.id")).orElse(null);
      ContratoInterviniente contratoIntervinienteOld = null;
      if (contrato != null) {
        TipoIntervencion tipoIntervencion = createTipoIntervencion(fieldSet.readString("tipoIntervencion.codigo"));
        contratoIntervinienteOld = contratoIntervinienteRepository
          .findByIntervinienteIdAndContratoIdAndTipoIntervencionId(contrato.getId(), intervinienteOld.getId(), tipoIntervencion != null ? tipoIntervencion.getId() : null).orElse(null);
      } else {
        throw new InvalidDataException(intervinienteOld);
      }

      if (contratoIntervinienteOld == null) {
        ContratoIntervinienteEspejo contratoInterviniente = new ContratoIntervinienteEspejo();
        contratoInterviniente.setOrdenIntervencion(fieldSet.readInt("ordenIntervencion"));
        contratoInterviniente.setTipoIntervencion(createTipoIntervencion(fieldSet.readString("tipoIntervencion.codigo")));

        if (contrato != null) {
          contratoInterviniente.setContrato(contrato);
        }
        intervinienteOld.getContratos().add(contratoInterviniente);
        contratoInterviniente.setInterviniente(intervinienteOld);
      }

      return intervinienteOld;
    } else {
      IntervinienteEspejo interviniente = new IntervinienteEspejo();
      interviniente.setIdCarga(fieldSet.readString("id"));

      if (isvalidString(fieldSet.readString("idOrigen"))) {
        interviniente.setIdOrigen(fieldSet.readString("idOrigen"));
      }

      if (isvalidString(fieldSet.readString("estado"))) {
        interviniente.setActivo(createBooleanValue(fieldSet.readString("estado")));
      } else {
        interviniente.setActivo(true);
      }

      if (isvalidString(fieldSet.readString("tipoDocumento.codigo"))) {
        interviniente.setTipoDocumento(createTipoDocumento((fieldSet.readString("tipoDocumento.codigo"))));
      }
      if (isvalidString(fieldSet.readString("numeroDocumento"))) {
        interviniente.setNumeroDocumento(fieldSet.readString("numeroDocumento"));
      }
      if (isvalidString(fieldSet.readString("nombre"))) {
        interviniente.setNombre(fieldSet.readString("nombre"));
      }
      if (isvalidString(fieldSet.readString("apellidos"))) {
        interviniente.setApellidos(fieldSet.readString("apellidos"));
      }
      if (isvalidString(fieldSet.readString("sexo"))) {
        interviniente.setSexo(createSexoEnum(fieldSet.readString("sexo")));
      }
      if (isvalidString(fieldSet.readString("estadoCivil"))) {
        interviniente.setEstadoCivil(createEstadoCivil(fieldSet.readString("estadoCivil")));
      }
      if (isvalidString(fieldSet.readString("fechaNacimiento"))) {
        interviniente.setFechaNacimiento(fieldSet.readDate("fechaNacimiento", "dd/MM/yyyy"));
      }
      if (isvalidString(fieldSet.readString("razonSocial"))) {
        interviniente.setRazonSocial(fieldSet.readString("razonSocial"));
        interviniente.setPersonaJuridica(true);
      } else {
        interviniente.setPersonaJuridica(false);
      }
      if (isvalidString(fieldSet.readString("fechaConstitucion"))) {
        interviniente.setFechaNacimiento(fieldSet.readDate("fechaConstitucion", "dd/MM/yyyy"));
      }
      if (isvalidString(fieldSet.readString("residente"))) {
        interviniente.setResidencia(createPais(fieldSet.readString("residente")));
      }
      if (isvalidString(fieldSet.readString("pais.codigo"))) {
        interviniente.setNacionalidad(createPais(fieldSet.readString("pais.codigo")));
        interviniente.setPaisNacimiento(createPais(fieldSet.readString("pais.codigo")));
      }
      if (isvalidString(fieldSet.readString("tipoOcupacion.codigo"))) {
        interviniente.setTipoOcupacion(createTipoOcupacion(fieldSet.readString("tipoOcupacion.codigo")));
      }
      if (isvalidString(fieldSet.readString("empresa"))) {
        interviniente.setEmpresa(fieldSet.readString("empresa"));
      }
      if (isvalidString(fieldSet.readString("telefonoEmpresa"))) {
        interviniente.setTelefonoEmpresa(fieldSet.readString("telefonoEmpresa"));
      }
      if (isvalidString(fieldSet.readString("contactoEmpresa"))) {
        interviniente.setContactoEmpresa(fieldSet.readString("contactoEmpresa"));
      }
      if (isvalidString(fieldSet.readString("tipoContrato.codigo"))) {
        interviniente.setTipoContrato(createTipoContrato(fieldSet.readString("tipoContrato.codigo")));
      }
      if (isvalidString(fieldSet.readString("codigoSocioProfesional"))) {
        interviniente.setCodigoSocioProfesional(fieldSet.readString("codigoSocioProfesional"));
      }
      if (isvalidString(fieldSet.readString("cnae"))) {
        interviniente.setCnae(fieldSet.readString("cnae"));
      }
      if (isvalidString(fieldSet.readString("ingresosNetosMensuales"))) {
        interviniente.setIngresosNetosMensuales(fieldSet.readDouble("ingresosNetosMensuales"));
      }
      if (isvalidString(fieldSet.readString("descripcionActividad"))) {
        interviniente.setDescripcionActividad(fieldSet.readString("descripcionActividad"));
      }
      if (isvalidString(fieldSet.readString("numHijosDependientes"))) {
        interviniente.setNumHijosDependientes(fieldSet.readInt(getIntValue(fieldSet.readString("numHijosDependientes"))));
      }
      if (isvalidString(fieldSet.readString("otrosRiesgos"))) {
        interviniente.setOtrosRiesgos(createBooleanValue(fieldSet.readString("otrosRiesgos")));
      } else {
        interviniente.setOtrosRiesgos(false);
      }
      if (isvalidString(fieldSet.readString("vulnerabilidad"))) {
        interviniente.setVulnerabilidad(createBooleanValue(fieldSet.readString("vulnerabilidad")));
      } else {
        interviniente.setVulnerabilidad(false);
      }
      if (isvalidString(fieldSet.readString("pensionista"))) {
        interviniente.setPensionista(createBooleanValue(fieldSet.readString("pensionista")));
      } else {
        interviniente.setPensionista(false);
      }
      if (isvalidString(fieldSet.readString("dependiente"))) {
        interviniente.setDependiente(createBooleanValue(fieldSet.readString("dependiente")));
      } else {
        interviniente.setDependiente(false);
      }
      if (isvalidString(fieldSet.readString("discapacitados"))) {
        interviniente.setDiscapacitados(createBooleanValue(fieldSet.readString("discapacitados")));
      } else {
        interviniente.setDiscapacitados(false);
      }
      if (isvalidString(fieldSet.readString("tipoPrestacion.codigo"))) {
        interviniente.setTipoPrestacion(createTipoPrestacion(fieldSet.readString("tipoPrestacion.codigo")));
      }
      if (isvalidString(fieldSet.readString("otrasSituacionesVulnerabilidad.codigo"))) {
        interviniente.setOtrasSituacionesVulnerabilidad(createVulnerabilidad(fieldSet.readString("otrasSituacionesVulnerabilidad.codigo")));
      }
      if (isvalidString(fieldSet.readString("notas1"))) {
        interviniente.setNotas1(fieldSet.readString("notas1"));
      }
      if (isvalidString(fieldSet.readString("notas2"))) {
        interviniente.setNotas2(fieldSet.readString("notas2"));
      }

      /**contrato_interviniente***/
      //solo se crea la relación si no existe prevciamente
      ContratoEspejo contrato = contratoEspejoRepository.findByIdCarga(fieldSet.readString("contrato.id")).orElse(null);

      ContratoIntervinienteEspejo contratoInterviniente = new ContratoIntervinienteEspejo();
      contratoInterviniente.setOrdenIntervencion(fieldSet.readInt("ordenIntervencion"));
      contratoInterviniente.setTipoIntervencion(createTipoIntervencion(fieldSet.readString("tipoIntervencion.codigo")));
      if (contrato != null) {
        contratoInterviniente.setContrato(contrato);
      }
      interviniente.getContratos().add(contratoInterviniente);
      contratoInterviniente.setInterviniente(interviniente);

      return interviniente;
    }
    /***/

  }

  private TipoIntervencion createTipoIntervencion(String codigoTipoIntervencion) {
    //en caso de que el código llegue como double
    String codToUse = getIntValue(codigoTipoIntervencion);
    TipoIntervencion tipoIntervencion = tipoIntervencionRepository.findByCodigo(codToUse).orElse(null);
    return tipoIntervencion;
  }

  private TipoDocumento createTipoDocumento(String codigoTipoDocumento) {
    String codToUse = getIntValue(codigoTipoDocumento);
    TipoDocumento tipoDocumento = tipoDocumentoRepository.findByCodigo(codToUse).orElse(null);
    return tipoDocumento;
  }

  private Pais createPais(String codigopais) {
    String codToUse = getIntValue(codigopais);
    Pais pais = paisRepository.findByCodigo(codToUse).orElse(null);
    return pais;
  }

  private TipoOcupacion createTipoOcupacion(String codigotipoOcupacion) {
    String codToUse = getIntValue(codigotipoOcupacion);
    TipoOcupacion tipoOcupacion = tipoOcupacionRepository.findByCodigo(codToUse).orElse(null);
    return tipoOcupacion;
  }

  private TipoContrato createTipoContrato(String codigoTipoContrato) {
    String codToUse = getIntValue(codigoTipoContrato);
    TipoContrato tipoContrato = tipoContratoRepository.findByCodigo(codToUse).orElse(null);
    return tipoContrato;
  }

  private TipoPrestacion createTipoPrestacion(String codigoTipoPrestacion) {
    String codToUse = getIntValue(codigoTipoPrestacion);
    TipoPrestacion tipoPrestacion = tipoPrestacionRepository.findByCodigo(codToUse).orElse(null);
    return tipoPrestacion;
  }

  private Vulnerabilidad createVulnerabilidad(String codigoVulnerabilidad) {
    String codToUse = getIntValue(codigoVulnerabilidad);
    Vulnerabilidad vulnerabilidad = vulnerabilidadRepository.findByCodigo(codToUse).orElse(null);
    return vulnerabilidad;
  }

  private Sexo createSexoEnum(String codigoSexo) {
    Sexo sexo = sexoRepository.findByCodigo(codigoSexo).orElse(null);
    return sexo;
  }

  private EstadoCivil createEstadoCivil(String codigoEstadoCivil) {
    EstadoCivil estadoCivil = estadoCivilRepository.findByCodigo(codigoEstadoCivil).orElse(null);
    return estadoCivil;

  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }

  private Boolean isvalidString(String string) {
    if (string != null && !string.equals("")) {
      return true;
    } else {
      return false;
    }
  }

  private Boolean createBooleanValue(String code) {
    String codeToUse = getIntValue(code);
    switch (codeToUse) {
      case "0":
        return false;
      case "1":
        return true;
      default:
        return null;
    }
  }

}
