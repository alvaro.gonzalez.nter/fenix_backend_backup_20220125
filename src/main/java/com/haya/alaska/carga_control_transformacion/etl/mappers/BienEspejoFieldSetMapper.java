package com.haya.alaska.carga_control_transformacion.etl.mappers;

import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.espejo.bien.infrastructure.repository.BienEspejoRepository;
import com.haya.alaska.espejo.contrato.domain.ContratoEspejo;
import com.haya.alaska.espejo.contrato.infrastructure.repository.ContratoEspejoRepository;
import com.haya.alaska.espejo.contrato_bien.domain.ContratoBienEspejo;
import com.haya.alaska.espejo.contrato_bien.infrastructure.repository.ContratoBienEspejoRepository;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.shared.exceptions.InvalidDataException;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien;
import com.haya.alaska.sub_tipo_bien.infrastructure.repository.SubTipoBienRepository;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;
import com.haya.alaska.tipo_garantia.infrastructure.repository.TipoGarantiaRepository;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.uso.domain.Uso;
import com.haya.alaska.uso.infrastructure.repository.UsoRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class BienEspejoFieldSetMapper implements FieldSetMapper<BienEspejo> {

  private LocalidadRepository localidadRepository;

  private MunicipioRepository municipioRepository;

  private ProvinciaRepository provinciaRepository;

  private TipoViaRepository tipoViaRepository;

  private EntornoRepository entornoRepository;

  private ContratoEspejoRepository contratoEspejoRepository;

  private TipoBienRepository tipoBienRepository;

  private SubTipoBienRepository subTipoBienRepository;

  private TipoGarantiaRepository tipoGarantiaRepository;

  private PaisRepository paisRepository;

  private TipoActivoRepository tipoActivoRepository;

  private UsoRepository usoRepository;

  private BienEspejoRepository bienEspejoRepository;

  private ContratoBienEspejoRepository contratoBienEspejoRepository;

  private LiquidezRepository liquidezRepository;

  @Override
  public BienEspejo mapFieldSet(FieldSet fieldSet) throws BindException {
    BienEspejo bienOld = bienEspejoRepository.findByIdCarga(fieldSet.readString("id")).orElse(null);
    if (bienOld != null) {
      /**contrato_bien***/
      //solo se crea la relación si no existe previamente
      ContratoEspejo contrato = contratoEspejoRepository.findByIdCarga(fieldSet.readString("contrato.id")).orElse(null);
      if (contrato != null) {
        ContratoBienEspejo contratoBienOld = contratoBienEspejoRepository
          .findByContratoIdAndBienId(contrato.getId(), bienOld.getId()).orElse(null);
        Boolean flagBienCambiado = false;
        if (contratoBienOld == null) {
          contratoBienOld = new ContratoBienEspejo();
          contratoBienOld.setActivo(true);
          contratoBienOld.setEstado(true);
          contratoBienOld.setContrato(contrato);
          if (isvalidString(fieldSet.readString("importeGarantizado"))) {
            contratoBienOld.setImporteGarantizado(fieldSet.readDouble("importeGarantizado"));
          }
          if (isvalidString(fieldSet.readString("liquidez.codigo"))) {
            contratoBienOld.setLiquidez(createLiquidez(fieldSet.readString("liquidez.codigo")));
          }
          if (isvalidString(fieldSet.readString("responsabilidadHipotecaria"))) {
            contratoBienOld.setResponsabilidadHipotecaria(fieldSet.readDouble("responsabilidadHipotecaria"));
          }

          if (isvalidString(fieldSet.readString("tipoGarantia.codigo"))) {
            contratoBienOld.setTipoGarantia(createTipoGarantia(fieldSet.readString("tipoGarantia.codigo")));
          }

        }

        BienEspejo bienTest = new BienEspejo();
        bienTest.setIdCarga(bienOld.getIdCarga());
        if (bienOld.compareTo(actualizarBien(fieldSet, bienTest)) != 0) {
          //Entra en casuistica 1 si no son iguales
          //Lógica de validaciones casuistica 1
          if (contieneTipoGarantia1Representante(bienOld).size() < 1 &&
            !esTipoGarantia1Representante(contratoBienOld)) {
            if (contieneTipoGarantia1(bienOld).size() < 1 && !esTipoGarantia1(contratoBienOld)) {
              if (contieneRepresentante(bienOld).size() < 1 && !esRepresentante(contratoBienOld)) {
                if (esContieneMasCamposIdentificativos(fieldSet, bienOld)) {
                  bienOld = actualizarBien(fieldSet, bienOld);
                } else {
                  //Nos quedamos con la direccion mas larga
                  if (isvalidString(fieldSet.readString("nombreVia"))) {
                    if (fieldSet.readString("nombreVia").length() > bienOld.getNombreVia().length()) {
                      bienOld.setNombreVia(fieldSet.readString("nombreVia"));
                      flagBienCambiado = true;
                    }
                  }
                }
              } else {
                //Asuncion 4
                if (esRepresentante(contratoBienOld)) {
                  bienOld = actualizarBien(fieldSet, bienOld);
                  flagBienCambiado = true;
                }
              }
            } else {
              //Asuncion 2
              if (esTipoGarantia1(contratoBienOld)) {
                bienOld = actualizarBien(fieldSet, bienOld);
                flagBienCambiado = true;
              }
            }
          } else {
            //Asuncion 1
            if (esTipoGarantia1Representante(contratoBienOld)) {
              bienOld = actualizarBien(fieldSet, bienOld);
              flagBienCambiado = true;
            }
          }
        } else {
          //Añadir contratos a un bien ya existente
          flagBienCambiado = true;
        }
        //Eliminar validacion en caso de querer conservar todos los contratos
        if (flagBienCambiado) {
          contratoBienOld.setBien(bienOld);
          bienOld.getContratos().add(contratoBienOld);
        }
      } else {
        throw new InvalidDataException(bienOld);
      }
      return bienOld;
    } else {
      BienEspejo bien = new BienEspejo();
      bien.setIdCarga(fieldSet.readString("id"));
      if (isvalidString(fieldSet.readString("idHaya"))) {
        bien.setIdHaya(fieldSet.readString("idHaya"));
      }
      if (isvalidString(fieldSet.readString("cliente.id"))) {
        bien.setIdOrigen(fieldSet.readString("cliente.id"));
      }
      if (isvalidString(fieldSet.readString("cliente2.id"))) {
        bien.setIdOrigen2(fieldSet.readString("cliente2.id"));
      }
      if (isvalidString(fieldSet.readString("tipoBien.codigo"))) {
        bien.setTipoBien(createTipoBien(fieldSet.readString("tipoBien.codigo")));
      }
      if (isvalidString(fieldSet.readString("subTipoBien.codigo"))) {
        bien.setSubTipoBien(createSubTipoBien(fieldSet.readString("subTipoBien.codigo")));
      }
      if (isvalidString(fieldSet.readString("estado"))) {
        bien.setActivo(createBooleanValue(getIntValue(fieldSet.readString("estado"))));
      } else {
        bien.setActivo(true);
      }
      if (isvalidString(fieldSet.readString("referenciaCatastral"))) {
        bien.setReferenciaCatastral(fieldSet.readString("referenciaCatastral"));
      }
      if (isvalidString(fieldSet.readString("finca"))) {
        bien.setFinca(fieldSet.readString("finca"));
      }
      if (isvalidString(fieldSet.readString("rango"))) {
        bien.setRango(fieldSet.readDouble("rango"));
      }
      if (isvalidString(fieldSet.readString("registro"))) {
        bien.setRegistro(fieldSet.readString("registro"));
      }
      if (isvalidString(fieldSet.readString("localidadRegistro"))) {
        bien.setLocalidadRegistro(fieldSet.readString("localidadRegistro"));
      }
      if (isvalidString(fieldSet.readString("tomo"))) {
        bien.setTomo(getIntValue(fieldSet.readString("tomo")));
      }
      if (isvalidString(fieldSet.readString("libro"))) {
        bien.setLibro(getIntValue(fieldSet.readString("libro")));
      }
      if (isvalidString(fieldSet.readString("folio"))) {
        bien.setFolio(getIntValue(fieldSet.readString("folio")));
      }
      if (isvalidString(fieldSet.readString("fechaInscripcion"))) {
        Date fechaInscripcion = fieldSet.readDate("fechaInscripcion", "dd/MM/yyyy");
        if (fechaInscripcion != new Date("11/11/1111"))
          bien.setFechaInscripcion(fieldSet.readDate("fechaInscripcion", "dd/MM/yyyy"));
      }
      if (isvalidString(fieldSet.readString("idufir"))) {
        bien.setIdufir(fieldSet.readString("idufir"));
      }
      if (isvalidString(fieldSet.readString("reoOrigen"))) {
        bien.setReoOrigen(fieldSet.readString("reoOrigen"));
      }
      if (isvalidString(fieldSet.readString("fechaReoConversion"))) {
        bien.setFechaReoConversion(fieldSet.readDate("fechaReoConversion", "dd/MM/yyyy"));
      }
      if (isvalidString(fieldSet.readString("fechaReoConversion"))) {
        bien.setFechaReoConversion(fieldSet.readDate("fechaReoConversion", "dd/MM/yyyy"));
      }
      if (isvalidString(fieldSet.readString("posesionNegociada"))) {
        bien.setPosesionNegociada(createBooleanValue(fieldSet.readString("posesionNegociada")));
      } else {
        bien.setPosesionNegociada(false);
      }
      if (isvalidString(fieldSet.readString("subasta"))) {
        bien.setSubasta(createBooleanValue(fieldSet.readString("subasta")));
      } else {
        bien.setSubasta(false);
      }
      if (isvalidString(fieldSet.readString("tipoVia.codigo"))) {
        bien.setTipoVia(createTipoVia(fieldSet.readString("tipoVia.codigo")));
      }
      if (isvalidString(fieldSet.readString("nombreVia"))) {
        bien.setNombreVia(fieldSet.readString("nombreVia"));
      }
      if (isvalidString(fieldSet.readString("numero"))) {
        bien.setNumero(fieldSet.readString("numero"));
      }
      if (isvalidString(fieldSet.readString("portal"))) {
        bien.setPortal(fieldSet.readString("portal"));
      }
      if (isvalidString(fieldSet.readString("bloque"))) {
        bien.setBloque(fieldSet.readString("bloque"));
      }
      if (isvalidString(fieldSet.readString("escalera"))) {
        bien.setEscalera(fieldSet.readString("escalera"));
      }
      if (isvalidString(fieldSet.readString("piso"))) {
        bien.setPiso(fieldSet.readString("piso"));
      }
      if (isvalidString(fieldSet.readString("puerta"))) {
        bien.setPuerta(fieldSet.readString("puerta"));
      }
      if (isvalidString(fieldSet.readString("entorno.codigo"))) {
        bien.setEntorno(createEntorno(fieldSet.readString("entorno.codigo")));
      }
      if (isvalidString(fieldSet.readString("municipio.codigo"))) {
        bien.setMunicipio(createMunicipio(fieldSet.readString("municipio.codigo")));
      }
      if (isvalidString(fieldSet.readString("localidad.codigo"))) {
        bien.setLocalidad(createLocalidad(fieldSet.readString("localidad.codigo")));
      }
      if (isvalidString(fieldSet.readString("provincia.codigo"))) {
        bien.setProvincia(createProvincia(fieldSet.readString("provincia.codigo")));
      }
      if (isvalidString(fieldSet.readString("comentario"))) {
        bien.setComentarioDireccion(fieldSet.readString("comentario"));
      }
      if (isvalidString(fieldSet.readString("codigoPostal"))) {
        bien.setCodigoPostal(fieldSet.readString("codigoPostal"));
      }
      if (isvalidString(fieldSet.readString("pais.codigo"))) {
        bien.setPais(createPais(fieldSet.readString("pais.codigo")));
      }
      if (isvalidString(fieldSet.readString("longitud"))) {
        bien.setLongitud(fieldSet.readDouble("longitud"));
      }
      if (isvalidString(fieldSet.readString("latitud"))) {
        bien.setLatitud(fieldSet.readDouble("latitud"));
      }
      if (isvalidString(fieldSet.readString("responsabilidadHipotecaria"))) {
        bien.setResponsabilidadHipotecaria(fieldSet.readDouble("responsabilidadHipotecaria"));
      }
      if (isvalidString(fieldSet.readString("importeGarantizado"))) {
        bien.setImporteGarantizado(fieldSet.readDouble("importeGarantizado"));
      }
      if (isvalidString(fieldSet.readString("tipoActivo.codigo"))) {
        bien.setTipoActivo(createTipoActivo(fieldSet.readString("tipoActivo.codigo")));
      }
      if (isvalidString(fieldSet.readString("uso"))) {
        bien.setUso(createUso(fieldSet.readString("uso")));
      }
      if (isvalidString(fieldSet.readString("anioConstruccion"))) {
        bien.setAnioConstruccion(fieldSet.readInt("anioConstruccion"));
      }
      if (isvalidString(fieldSet.readString("superficieSuelo"))) {
        bien.setSuperficieSuelo(fieldSet.readDouble("superficieSuelo") / 100);
      }
      if (isvalidString(fieldSet.readString("superficieConstruida"))) {
        bien.setSuperficieConstruida(fieldSet.readDouble("superficieConstruida") / 100);
      }
      if (isvalidString(fieldSet.readString("superficieUtil"))) {
        bien.setSuperficieUtil(fieldSet.readDouble("superficieUtil"));
      }
      if (isvalidString(fieldSet.readString("anejoGaraje"))) {
        bien.setAnejoGaraje(fieldSet.readString("anejoGaraje"));
      }
      if (isvalidString(fieldSet.readString("anejoTrastero"))) {
        bien.setAnejoTrastero(fieldSet.readString("anejoTrastero"));
      }
      if (isvalidString(fieldSet.readString("anejoOtros"))) {
        bien.setAnejoOtros(fieldSet.readString("anejoOtros"));
      }
      if (isvalidString(fieldSet.readString("notas1"))) {
        bien.setNotas1(fieldSet.readString("notas1"));
      }
      if (isvalidString(fieldSet.readString("notas2"))) {
        bien.setNotas2(fieldSet.readString("notas2"));
      }
      ContratoEspejo contrato = contratoEspejoRepository.findByIdCarga(fieldSet.readString("contrato.id")).orElse(null);
      ContratoBienEspejo contratoBien = new ContratoBienEspejo();
      contratoBien.setActivo(true);
      contratoBien.setEstado(true);
      if (isvalidString(fieldSet.readString("importeGarantizado"))) {
        contratoBien.setImporteGarantizado(fieldSet.readDouble("importeGarantizado"));
      }
      if (isvalidString(fieldSet.readString("liquidez.codigo"))) {
        //contratoBien.setLiquidez(createLiquidez(fieldSet.readString("tipoGarantia.codigo")));
      }
      if (isvalidString(fieldSet.readString("responsabilidadHipotecaria"))) {
        contratoBien.setResponsabilidadHipotecaria(fieldSet.readDouble("responsabilidadHipotecaria"));
      }

      if (isvalidString(fieldSet.readString("tipoGarantia.codigo"))) {
        contratoBien.setTipoGarantia(createTipoGarantia(fieldSet.readString("tipoGarantia.codigo")));
      }

      if (contrato != null) {
        contratoBien.setContrato(contrato);
      }
      contratoBien.setBien(bien);
      bien.getContratos().add(contratoBien);
      return bien;
    }

  }

  private BienEspejo actualizarBien(FieldSet fieldSet, BienEspejo bien) {
    if (isvalidString(fieldSet.readString("cliente.id"))) {
      bien.setIdOrigen(fieldSet.readString("cliente.id"));
    }
    if (isvalidString(fieldSet.readString("cliente2.id"))) {
      bien.setIdOrigen2(fieldSet.readString("cliente2.id"));
    }
    if (isvalidString(fieldSet.readString("tipoBien.codigo"))) {
      bien.setTipoBien(createTipoBien(fieldSet.readString("tipoBien.codigo")));
    }
    if (isvalidString(fieldSet.readString("subTipoBien.codigo"))) {
      bien.setSubTipoBien(createSubTipoBien(fieldSet.readString("subTipoBien.codigo")));
    }
    if (isvalidString(fieldSet.readString("estado"))) {
      bien.setActivo(createBooleanValue(getIntValue(fieldSet.readString("estado"))));
    } else {
      bien.setActivo(true);
    }
    if (isvalidString(fieldSet.readString("referenciaCatastral"))) {
      bien.setReferenciaCatastral(fieldSet.readString("referenciaCatastral"));
    }
    if (isvalidString(fieldSet.readString("finca"))) {
      bien.setFinca(fieldSet.readString("finca"));
    }
    if (isvalidString(fieldSet.readString("rango"))) {
      bien.setRango(fieldSet.readDouble("rango"));
    }
    if (isvalidString(fieldSet.readString("registro"))) {
      bien.setRegistro(fieldSet.readString("registro"));
    }
    if (isvalidString(fieldSet.readString("localidadRegistro"))) {
      bien.setLocalidadRegistro(fieldSet.readString("localidadRegistro"));
    }
    if (isvalidString(fieldSet.readString("tomo"))) {
      bien.setTomo(getIntValue(fieldSet.readString("tomo")));
    }
    if (isvalidString(fieldSet.readString("libro"))) {
      bien.setLibro(getIntValue(fieldSet.readString("libro")));
    }
    if (isvalidString(fieldSet.readString("folio"))) {
      bien.setFolio(getIntValue(fieldSet.readString("folio")));
    }
    if (isvalidString(fieldSet.readString("fechaInscripcion"))) {
      Date fechaInscripcion = fieldSet.readDate("fechaInscripcion", "dd/MM/yyyy");
      if (fechaInscripcion != new Date("11/11/1111"))
        bien.setFechaInscripcion(fieldSet.readDate("fechaInscripcion", "dd/MM/yyyy"));
    }
    if (isvalidString(fieldSet.readString("idufir"))) {
      bien.setIdufir(fieldSet.readString("idufir"));
    }
    if (isvalidString(fieldSet.readString("reoOrigen"))) {
      bien.setReoOrigen(fieldSet.readString("reoOrigen"));
    }
    if (isvalidString(fieldSet.readString("fechaReoConversion"))) {
      bien.setFechaReoConversion(fieldSet.readDate("fechaReoConversion", "dd/MM/yyyy"));
    }
    if (isvalidString(fieldSet.readString("fechaReoConversion"))) {
      bien.setFechaReoConversion(fieldSet.readDate("fechaReoConversion", "dd/MM/yyyy"));
    }
    if (isvalidString(fieldSet.readString("posesionNegociada"))) {
      bien.setPosesionNegociada(createBooleanValue(fieldSet.readString("posesionNegociada")));
    } else {
      bien.setPosesionNegociada(false);
    }
    if (isvalidString(fieldSet.readString("subasta"))) {
      bien.setSubasta(createBooleanValue(fieldSet.readString("subasta")));
    } else {
      bien.setSubasta(false);
    }
    if (isvalidString(fieldSet.readString("tipoVia.codigo"))) {
      bien.setTipoVia(createTipoVia(fieldSet.readString("tipoVia.codigo")));
    }
    if (isvalidString(fieldSet.readString("nombreVia"))) {
      bien.setNombreVia(fieldSet.readString("nombreVia"));
    }
    if (isvalidString(fieldSet.readString("numero"))) {
      bien.setNumero(fieldSet.readString("numero"));
    }
    if (isvalidString(fieldSet.readString("portal"))) {
      bien.setPortal(fieldSet.readString("portal"));
    }
    if (isvalidString(fieldSet.readString("bloque"))) {
      bien.setBloque(fieldSet.readString("bloque"));
    }
    if (isvalidString(fieldSet.readString("escalera"))) {
      bien.setEscalera(fieldSet.readString("escalera"));
    }
    if (isvalidString(fieldSet.readString("piso"))) {
      bien.setPiso(fieldSet.readString("piso"));
    }
    if (isvalidString(fieldSet.readString("puerta"))) {
      bien.setPuerta(fieldSet.readString("puerta"));
    }
    if (isvalidString(fieldSet.readString("entorno.codigo"))) {
      bien.setEntorno(createEntorno(fieldSet.readString("entorno.codigo")));
    }
    if (isvalidString(fieldSet.readString("municipio.codigo"))) {
      bien.setMunicipio(createMunicipio(fieldSet.readString("municipio.codigo")));
    }
    if (isvalidString(fieldSet.readString("localidad.codigo"))) {
      bien.setLocalidad(createLocalidad(fieldSet.readString("localidad.codigo")));
    }
    if (isvalidString(fieldSet.readString("localidad.codigo"))) {
      bien.setLocalidad(createLocalidad(fieldSet.readString("localidad.codigo")));
    }
    if (isvalidString(fieldSet.readString("provincia.codigo"))) {
      bien.setProvincia(createProvincia(fieldSet.readString("provincia.codigo")));
    }
    if (isvalidString(fieldSet.readString("comentario"))) {
      bien.setComentarioDireccion(fieldSet.readString("comentario"));
    }
    if (isvalidString(fieldSet.readString("codigoPostal"))) {
      bien.setCodigoPostal(fieldSet.readString("codigoPostal"));
    }
    if (isvalidString(fieldSet.readString("pais.codigo"))) {
      bien.setPais(createPais(fieldSet.readString("pais.codigo")));
    }
    if (isvalidString(fieldSet.readString("longitud"))) {
      bien.setLongitud(fieldSet.readDouble("longitud"));
    }
    if (isvalidString(fieldSet.readString("latitud"))) {
      bien.setLatitud(fieldSet.readDouble("latitud"));
    }
    if (isvalidString(fieldSet.readString("responsabilidadHipotecaria"))) {
      bien.setResponsabilidadHipotecaria(fieldSet.readDouble("responsabilidadHipotecaria"));
    }
    if (isvalidString(fieldSet.readString("importeGarantizado"))) {
      bien.setImporteGarantizado(fieldSet.readDouble("importeGarantizado"));
    }
    if (isvalidString(fieldSet.readString("tipoActivo.codigo"))) {
      bien.setTipoActivo(createTipoActivo(fieldSet.readString("tipoActivo.codigo")));
    }
    if (isvalidString(fieldSet.readString("uso"))) {
      bien.setUso(createUso(fieldSet.readString("uso")));
    }
    if (isvalidString(fieldSet.readString("anioConstruccion"))) {
      bien.setAnioConstruccion(fieldSet.readInt("anioConstruccion"));
    }
    if (isvalidString(fieldSet.readString("superficieSuelo"))) {
      bien.setSuperficieSuelo(fieldSet.readDouble("superficieSuelo"));
    }
    if (isvalidString(fieldSet.readString("superficieConstruida"))) {
      bien.setSuperficieConstruida(fieldSet.readDouble("superficieConstruida"));
    }
    if (isvalidString(fieldSet.readString("superficieUtil"))) {
      bien.setSuperficieUtil(fieldSet.readDouble("superficieUtil"));
    }
    if (isvalidString(fieldSet.readString("anejoGaraje"))) {
      bien.setAnejoGaraje(fieldSet.readString("anejoGaraje"));
    }
    if (isvalidString(fieldSet.readString("anejoTrastero"))) {
      bien.setAnejoTrastero(fieldSet.readString("anejoTrastero"));
    }
    if (isvalidString(fieldSet.readString("anejoOtros"))) {
      bien.setAnejoOtros(fieldSet.readString("anejoOtros"));
    }
    if (isvalidString(fieldSet.readString("notas1"))) {
      bien.setNotas1(fieldSet.readString("notas1"));
    }
    if (isvalidString(fieldSet.readString("notas2"))) {
      bien.setNotas2(fieldSet.readString("notas2"));
    }
    return bien;
  }

  private List<ContratoBienEspejo> contieneTipoGarantia1Representante(BienEspejo bien) {
    return bien.getContratos().stream().filter(c -> {
      return esTipoGarantia1Representante(c);
    }).collect(Collectors.toList());
  }

  private Boolean esTipoGarantia1Representante(ContratoBienEspejo contrato) {
    return (contrato.getTipoGarantia().getCodigo().equals("1") &&
      contrato.getContrato().isRepresentante());
  }

  private List<ContratoBienEspejo> contieneTipoGarantia1(BienEspejo bien) {
    return bien.getContratos().stream().filter(c -> {
      return esTipoGarantia1(c);
    }).collect(Collectors.toList());
  }

  private Boolean esTipoGarantia1(ContratoBienEspejo contrato) {
    return (contrato.getTipoGarantia().getCodigo().equals("1"));
  }

  private List<ContratoBienEspejo> contieneRepresentante(BienEspejo bien) {
    return bien.getContratos().stream().filter(c -> {
      return esRepresentante(c);
    }).collect(Collectors.toList());
  }

  private Boolean esRepresentante(ContratoBienEspejo contrato) {
    return (contrato.getContrato().isRepresentante());
  }

  private List<ContratoBienEspejo> contieneMasCamposIdentificativos(BienEspejo bien) {
    return bien.getContratos().stream().filter(c -> {
      return esTipoGarantia1Representante(c);
    }).collect(Collectors.toList());
  }

  private Boolean esContieneMasCamposIdentificativos(FieldSet fieldSet, BienEspejo bien) {
    if (isvalidString(fieldSet.readString("estado"))) {
      if (createBooleanValue(getIntValue(fieldSet.readString("estado"))) == true && bien.getActivo() == false) {
        return true;
      }
    }
    if (isvalidString(fieldSet.readString("referenciaCatastral"))) {
      if (fieldSet.readString("referenciaCatastral") != null && !fieldSet.readString("referenciaCatastral").equals("") &&
        (bien.getReferenciaCatastral() == null || bien.getReferenciaCatastral().equals(""))) {
        return true;
      }
    }
    if (isvalidString(fieldSet.readString("idufir"))) {
      if (fieldSet.readString("idufir") != null && !fieldSet.readString("idufir").equals("") &&
        (bien.getIdufir() == null || bien.getIdufir().equals(""))) {
        return true;
      }
    }
    if (isvalidString(fieldSet.readString("registro"))) {
      if (fieldSet.readString("registro") != null && !fieldSet.readString("registro").equals("") &&
        (bien.getRegistro() == null || bien.getRegistro().equals(""))) {
        return true;
      }
    }
    if (isvalidString(fieldSet.readString("fechaInscripcion"))) {
      if (fieldSet.readDate("fechaInscripcion", "dd/MM/yyyy") != null && bien.getFechaInscripcion() == null) {
        return true;
      }
    }
    if (isvalidString(fieldSet.readString("finca"))) {
      if (fieldSet.readString("finca") != null && !fieldSet.readString("finca").equals("") &&
        (bien.getFinca() == null || bien.getFinca().equals(""))) {
        return true;
      }
    }
    if (isvalidString(fieldSet.readString("tomo"))) {
      if (fieldSet.readString("tomo") != null && !fieldSet.readString("tomo").equals("") &&
        (bien.getTomo() == null || bien.getTomo().equals(""))) {
        return true;
      }
    }
    if (isvalidString(fieldSet.readString("libro"))) {
      if (fieldSet.readString("libro") != null && !fieldSet.readString("libro").equals("") &&
        (bien.getLibro() == null || bien.getLibro().equals(""))) {
        return true;
      }
    }
    if (isvalidString(fieldSet.readString("folio"))) {
      if (fieldSet.readString("folio") != null && !fieldSet.readString("folio").equals("") &&
        (bien.getFolio() == null || bien.getFolio().equals(""))) {
        return true;
      }
    }
    return false;
  }

    /*public boolean areEqual(BienEspejo a, BienEspejo b){
      return ((a.getIdCarga()!=null && b.getIdCarga()!=null || a.getIdCarga()==b.getIdCarga()) && (a.getIdCarga().equals(b.getIdCarga()))) &&
        ((a.getActivo()!=null && b.getActivo()!=null) && (a.getActivo()==b.getActivo())) &&
        ((a.getIdufir()!=null && b.getIdufir()!=null) && (a.getIdufir().equals(b.getIdufir())))&&
        ((a.getRegistro()!=null && b.getRegistro()!=null) && (a.getRegistro().equals(b.getRegistro()))) &&
        ((a.getFechaInscripcion()!=null && b.getFechaInscripcion()!=null) && (a.getFechaInscripcion()==b.getFechaInscripcion())) &&
        ((a.getFinca()!=null && b.getFinca()!=null) && (a.getFinca().equals(b.getFinca()))) &&
        ((a.getTomo()!=null && b.getTomo()!=null) && (a.getTomo().equals(b.getTomo()))) &&
        ((a.getLibro()!=null && b.getLibro()!=null) && (a.getLibro().equals(b.getLibro()))) &&
        ((a.getFolio()!=null && b.getFolio()!=null) && (a.getFolio().equals(b.getFolio())));

    }*/

  private boolean equal(String a, String b) {
    return a == b || a.equals(b);
  }

  private TipoVia createTipoVia(String codigoTipoVia) {
    TipoVia tipoVia = tipoViaRepository.findByCodigo(codigoTipoVia).orElse(null);
    return tipoVia;
  }

  private Entorno createEntorno(String codigoEntorno) {
    String codToUse = getIntValue(codigoEntorno);
    Entorno entorno = entornoRepository.findByCodigo(codToUse).orElse(null);
    return entorno;
  }

  private Municipio createMunicipio(String codigoMunicipio) {
    String codToUse = getIntValue(codigoMunicipio);
    Municipio municipio = municipioRepository.findByCodigo(codToUse).orElse(null);
    return municipio;
  }

  private Localidad createLocalidad(String codigoLocalidad) {
    Localidad localidad = localidadRepository.findByCodigo(codigoLocalidad).orElse(null);
    return localidad;
  }

  private Provincia createProvincia(String codigoProvincia) {
    String codToUse = getIntValue(codigoProvincia);
    Provincia provincia = provinciaRepository.findByCodigo(codToUse).orElse(null);
    return provincia;
  }

  private Pais createPais(String codigopais) {
    String codToUse = getIntValue(codigopais);
    Pais pais = paisRepository.findByCodigo(codToUse).orElse(null);
    return pais;
  }

  private TipoBien createTipoBien(String codigoTipoBien) {
    String codToUse = getIntValue(codigoTipoBien);
    TipoBien tipoBien = tipoBienRepository.findByCodigo(codToUse).orElse(null);
    return tipoBien;
  }

  private SubTipoBien createSubTipoBien(String codigoSubTipoBien) {
    String codToUse = getIntValue(codigoSubTipoBien);
    SubTipoBien subTipoBien = subTipoBienRepository.findByCodigo(codToUse).orElse(null);
    return subTipoBien;
  }

  private Liquidez createLiquidez(String codigoLiquidez) {
    Liquidez liquidez = liquidezRepository.findByCodigo(codigoLiquidez).orElse(null);
    return liquidez;
  }

  private TipoActivo createTipoActivo(String codigoTipoActivo) {
    String codToUse = getIntValue(codigoTipoActivo);
    TipoActivo tipoActivo = tipoActivoRepository.findByCodigo(codToUse).orElse(null);
    return tipoActivo;
  }

  private Uso createUso(String codigoUso) {
    String codToUse = getIntValue(codigoUso);
    Uso uso = usoRepository.findByCodigo(codToUse).orElse(null);
    return uso;
  }

  private TipoGarantia createTipoGarantia(String codigoTipoGarantia) {
    String codToUse = getIntValue(codigoTipoGarantia);
    TipoGarantia tipoGarantia = tipoGarantiaRepository.findByCodigo(codToUse).orElse(null);
    return tipoGarantia;
  }

  private Boolean isvalidString(String string) {
    if (string != null && !string.equals("")) {
      return true;
    } else {
      return false;
    }
  }

  private String getIntValue(String value) {
    String result = "";
    if (value != null && !value.equals("")) {
      if (value.contains(".")) {
        result = value.substring(0, value.indexOf('.'));
      } else {
        result = value;
      }
    }
    return result;
  }

  private Boolean createBooleanValue(String code) {
    String codeToUSe = getIntValue(code);
    switch (codeToUSe) {
      case "0":
        return false;
      case "1":
        return true;
      default:
        return null;
    }
  }

}
