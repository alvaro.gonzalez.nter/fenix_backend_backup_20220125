package com.haya.alaska.carga_control_transformacion.etl.processor;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.espejo.bien.domain.BienEspejo;
import com.haya.alaska.espejo.bien.infrastructure.repository.BienEspejoRepository;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
@StepScope
public class BienEspejoProcessor implements ItemProcessor<BienEspejo, BienEspejo> {

  @Autowired
  private ServicioMaestro servicioMaestro;

  @Autowired
  private CarteraRepository carteraRepository;

  @Autowired
  private BienEspejoRepository bienEspejoRepository;

  @Value("#{jobParameters['defaultCartera']}")
  private String carteraId;

  @Value("#{jobParameters['multicartera']}")
  private Boolean multicartera;

  @Override
  public BienEspejo process(BienEspejo item) throws Exception {

    Cartera cartera = getCartera(carteraId);
    if (cartera != null && item.getIdHaya() == null && !multicartera) {
      bienEspejoRepository.save(item);
    }
    item.setFechaCarga(new Date());
    return item;
  }
  private Cartera getCartera(String idCargaCartera) {
    return carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(idCargaCartera).orElse(null);
  }
}
