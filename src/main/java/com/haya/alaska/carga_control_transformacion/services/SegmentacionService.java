package com.haya.alaska.carga_control_transformacion.services;

import com.haya.alaska.asignacion_expediente.application.AsignacionExpedienteUseCaseImpl;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.infrastructure.repository.AsignacionExpedienteRepository;
import com.haya.alaska.asignacion_expediente_posesion_negociada.repository.AsignacionExpedientePosesionNegociadaRepository;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.integraciones.portal_deudor.client.PortalDeudorFeignClient;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.infrastructure.repository.PerfilRepository;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.segmentacion.infraestructure.repository.SegmentacionRepository;
import com.haya.alaska.segmentacion.infraestructure.util.SegmentacionUtil;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;


@Service
public class SegmentacionService {


  @Autowired
  AsignacionExpedienteRepository asignacionExpedienteRepository;
  @Autowired
  AsignacionExpedientePosesionNegociadaRepository asignacionExpedientePosesionNegociadaRepository;
  @Autowired
  UsuarioRepository usuarioRepository;
  @Autowired
  ExpedienteRepository expedienteRepository;
  @Autowired
  SegmentacionRepository segmentacionRepository;
  @Autowired
  PerfilRepository perfilRepository;
  @Autowired
  SegmentacionUtil segmentacionUtil;
  @Autowired
  AsignacionExpedienteUseCaseImpl asignacionExpedienteUseCase;
  @Autowired
  PortalDeudorFeignClient portalDeudorFeignClient;

  public Integer contarExpedientesAsignados(Usuario gestor) {
    Integer n_asignados = asignacionExpedienteRepository.countAllByUsuarioIdAndExpedienteContratosEstadoContratoValorNot(gestor.getId(), "CERRADO");
    n_asignados += asignacionExpedientePosesionNegociadaRepository.countAllByUsuarioId(gestor.getId());
    return n_asignados;
  }

  public String pruebas(){
    String ping =
      portalDeudorFeignClient.ping();

    return ping;
  }

  public void asignarSegmentacion() {

    List<Segmentacion> segmentaciones = segmentacionRepository.findByGuardadoTrue();
    Perfil perfil = perfilRepository.findByNombre("Gestor").orElse(null);

    for (Segmentacion segmentacion : segmentaciones) {

      if (segmentacion.getCartera() == null)
        continue;

      List<Expediente> expedientesSegmentacion = segmentacionUtil.sacarExpedientesFiltrados(segmentacion);

      //recorrer expedientes
      for (Expediente expediente : expedientesSegmentacion) {
        boolean asignado = false;

        if (expediente.getGestor() != null) continue;

        //Si se selecciona área, gestor y grupo, a ese gestor es al que se le asigna.
        //Si ese gestor ya tiene el máximo de expedientes asignados, no se asigna a nadie.
        if (segmentacion.getGestor() != null) {

          AsignacionExpediente asignacionActual = new AsignacionExpediente();

          // Realiza las comprobaciones de los límites de expedientes que un usuario puede gestiona
          Integer n_asignados = contarExpedientesAsignados(segmentacion.getGestor());
          Integer n_expedientes = segmentacion.getGestor().getLimiteExpedientes();

          if (n_expedientes != null && n_expedientes - n_asignados > 0) {
            asignacionActual.setExpediente(expediente);
            asignacionActual.setUsuarioAsignacion(expediente.getCartera().getResponsableCartera());
            asignacionActual.setUsuario(segmentacion.getGestor());
            AsignacionExpediente asignacionFinal = asignacionExpedienteRepository.saveAndFlush(asignacionActual);
            Set<AsignacionExpediente> asignaciones = expediente.getAsignaciones();
            asignaciones.add(asignacionFinal);
            expediente.setAsignaciones(asignaciones);

            expedienteRepository.saveAndFlush(expediente);
            asignado = true;
          }
        } else {
          //Si no se selecciona gestor y sí los otros dos, se hace lo siguiente:
          List<Usuario> usuariosPorGrupoArea = null;
          if (segmentacion.getGrupo() != null && segmentacion.getArea() != null) {
            usuariosPorGrupoArea = usuarioRepository.findByGrupoUsuariosIdAndAreasIdAndPerfilId(segmentacion.getGrupo().getId(), segmentacion.getArea().getId(), perfil.getId());
          } else if (segmentacion.getGrupo() != null) {
            usuariosPorGrupoArea = usuarioRepository.findByGrupoUsuariosIdAndPerfilId(segmentacion.getGrupo().getId(), perfil.getId());
          } else if (segmentacion.getArea() != null) {
            usuariosPorGrupoArea = usuarioRepository.findByAreasIdAndPerfilId(segmentacion.getArea().getId(), perfil.getId());
          }
          if (usuariosPorGrupoArea != null && usuariosPorGrupoArea.size() > 0) {

            AsignacionExpediente asignacionActual = new AsignacionExpediente();
            //-Si NO existen gestores en esa área y grupo, no se asigna gestor
            for (Usuario usuario : usuariosPorGrupoArea) {

              //-Si SÍ existen gestores en esa área y grupo, se REPARTEN de esta forma:
              //Se asignan a los gestores de la misma provincia que el primer titular del contrato representante, si no han llegado al límite de expedientes asignados

              Direccion direccion = expediente.getContratoRepresentante().getPrimerInterviniente().getDirecciones().stream().findFirst().orElse(null);
              if (direccion != null && direccion.getProvincia() != null && usuario.getProvincias().contains(direccion.getProvincia())) {

                Integer n_asignados = contarExpedientesAsignados(usuario);
                Integer n_expedientes = usuario.getLimiteExpedientes();

                if (n_expedientes != null && n_expedientes - n_asignados > 0) {
                  asignacionActual.setExpediente(expediente);
                  asignacionActual.setUsuarioAsignacion(expediente.getCartera().getResponsableCartera());
                  asignacionActual.setUsuario(usuario);
                  AsignacionExpediente asignacionFinal = asignacionExpedienteRepository.saveAndFlush(asignacionActual);
                  Set<AsignacionExpediente> asignaciones = expediente.getAsignaciones();
                  asignaciones.add(asignacionFinal);
                  expediente.setAsignaciones(asignaciones);

                  expedienteRepository.saveAndFlush(expediente);
                  asignado = true;
                  break;
                }
              }
            }

            //Si no se ha podido, se asignan a los gestores de otra provincia del mismo área y grupo, también teniendo en cuenta los limites.
            if (!asignado) {
              for (Usuario usuario : usuariosPorGrupoArea) {

                Integer n_asignados = contarExpedientesAsignados(usuario);
                Integer n_expedientes = usuario.getLimiteExpedientes();

                if (n_expedientes != null && n_expedientes - n_asignados > 0) {
                  asignacionActual.setExpediente(expediente);
                  asignacionActual.setUsuarioAsignacion(expediente.getCartera().getResponsableCartera());
                  asignacionActual.setUsuario(usuario);
                  AsignacionExpediente asignacionFinal = asignacionExpedienteRepository.saveAndFlush(asignacionActual);
                  Set<AsignacionExpediente> asignaciones = expediente.getAsignaciones();
                  asignaciones.add(asignacionFinal);
                  expediente.setAsignaciones(asignaciones);

                  expedienteRepository.saveAndFlush(expediente);
                  asignado = true;
                  break;
                }
              }
            }
          }
        }
      }
    }
    asignacionExpedienteUseCase.asignarSegmentacionPN();
  }
}
