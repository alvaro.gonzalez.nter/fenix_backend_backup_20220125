package com.haya.alaska.carga_control_transformacion.services;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


@Service
public class ControlFicheroVacioService {

  public Boolean esFicheroValido(String fichero) throws IOException {

    boolean result = true;

    FileReader fileReader = new FileReader(fichero);
    BufferedReader bufferedReader = new BufferedReader(fileReader);
    long lines = bufferedReader.lines().count();
    if (lines <= 1) {
      result = false;
    }
    bufferedReader.close();
    return result;

  }

}
