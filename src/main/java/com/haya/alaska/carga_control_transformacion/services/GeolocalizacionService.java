package com.haya.alaska.carga_control_transformacion.services;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.interviniente.infrastructure.util.IntervinienteUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class GeolocalizacionService {

  @Autowired
  private DireccionRepository direccionRepository;

  @Autowired
  private BienRepository bienRepository;

  @Autowired
  private IntervinienteUtil intervinienteUtil;

  @SneakyThrows
  public void llamar() {
    List<Direccion> direcciones = direccionRepository.findAllByLongitudIsNullAndLatitudIsNull();
    for (Direccion d : direcciones) {
      intervinienteUtil.direccionGoogle(d);
      direccionRepository.save(d);
    }

    List<Bien> bienes = bienRepository.findAllByLongitudIsNullAndLatitudIsNull();
    for (Bien b : bienes) {
      intervinienteUtil.direccionGoogle(b);
      bienRepository.save(b);
    }
  }
}
