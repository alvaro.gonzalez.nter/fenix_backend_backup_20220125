package com.haya.alaska.carga_control_transformacion.services;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.subtipo_estado.infrastructure.repository.SubtipoEstadoRepository;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.tipo_estado.infrastructure.repository.TipoEstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class CalculateExpedienteService {

  @Autowired
  private CarteraRepository carteraRepository;

  @Autowired
  private ContratoRepository contratoRepository;

  @Autowired
  private TipoEstadoRepository tipoEstadoRepository;

  @Autowired
  private SubtipoEstadoRepository subtipoEstadoRepository;

  @Autowired
  private ExpedienteRepository expedienteRepository;

  public static final String CLIENTE = "cliente";
  public static final String CONTRATO = "contrato";
  public static final String HEREDADO = "heredado";

  public static final String JAIPUR_IRIS = "JAIPUR";
  public static final String AGORA_IRIS = "PROMONTORIA AGORA";
  public static final String EGEO_IRIS = "PROMONTORIA 227";

  public static final String JAIPUR_FENIX = "JAIPUR";
  public static final String AGORA_FENIX = "AGORA";
  public static final String EGEO_FENIX = "EGEO PROMONTORIA";

  private static final HashMap<String, String> IRISFENIXConv = new HashMap<>();

  static {
    IRISFENIXConv.put(JAIPUR_IRIS, JAIPUR_FENIX);
    IRISFENIXConv.put(AGORA_IRIS, AGORA_FENIX);
    IRISFENIXConv.put(EGEO_IRIS, EGEO_FENIX);
  }

  public void agruparContratos(String idCartera, Boolean multicartera) {
    Cartera cartera = carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(idCartera).orElse(null);
    String criterioExpediente = cartera.getCriterioGeneracionExpediente().getCodigo();
    if (criterioExpediente.equals(CLIENTE)) {
      agruparContratosPorCriterioCliente(cartera, multicartera);
    } else if (criterioExpediente.equals(CONTRATO)) {
      agruparContratosPorCriterioContrato(cartera);
    } else if (criterioExpediente.equals(HEREDADO)) {
      agruparContratosPorCriterioHeredado(cartera);
    }
  }

  private void agruparContratosPorCriterioCliente(Cartera cartera, Boolean multicartera) {
    TipoEstado estadoPendiente = tipoEstadoRepository.findByCodigo("1").orElse(null);//pendiente de gestión
    SubtipoEstado subEstadoPendiente = subtipoEstadoRepository.findByCodigo("11").orElse(null);//pendiente de gestión
    Map<Interviniente, List<Contrato>> contratosOrdenados = new HashMap<Interviniente, List<Contrato>>();
    //recuperar todos los contratos que tengan el expediente nulo
    List<Contrato> contratosParaOrdenar = contratoRepository.findAllByExpedienteId(null);
    //agrupar los contratos por interviniente
    //para cada contrato se recupera su primer interviniente y se crea un mapa
    //id:interviniente valor (lista de contratos)
    for (Contrato contrato : contratosParaOrdenar) {
      Set<ContratoInterviniente> intervinientes = contrato.getIntervinientes();
      for (ContratoInterviniente ci : intervinientes) {
        List<Contrato> contratosPorInterviniente = new ArrayList<Contrato>();
        if (ci.getOrdenIntervencion() == 1 && ci.getTipoIntervencion().getValor().equals("TITULAR")) {
          contratosPorInterviniente.add(ci.getContrato());
          //se comprueba si el primer interviniente ya existe en la lista de claves
          if (contratosOrdenados.containsKey(ci.getInterviniente())) {
            contratosOrdenados.get(ci.getInterviniente()).add(contrato);
          } else {
            contratosOrdenados.put(ci.getInterviniente(), contratosPorInterviniente);
          }
        }
      }
    }
    // para cada elemento del mapa se asigna un nuevo expediente con la cartera recibida por parámetro
    List<Cartera> carteras = carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNotNull(cartera.getIdCarga());

    Set<Interviniente> intervinientes = contratosOrdenados.keySet();
    for (Interviniente interviniente : intervinientes) {
      ContratoInterviniente ci_old = interviniente.getContratos().stream().filter(ci -> {
        if (ci.getContrato().getExpediente() != null && ci.getOrdenIntervencion() == 1 && ci.getTipoIntervencion().getCodigo().equals("1"))
          return true;
        return false;
      }).findFirst().orElse(null);
      Expediente expediente = new Expediente();
      if (ci_old != null) {
        expediente = ci_old.getContrato().getExpediente();
      }
      expediente.setFechaCreacion(new Date());
      Cartera carteraDef = cartera;
      if (multicartera) {
        ContratoInterviniente ci = interviniente.getContratos().stream().findFirst().orElse(null);
        if (ci != null) {
          carteraDef = carteras.stream().filter(c -> c.getNombre().equals(IRISFENIXConv.get(ci.getContrato().getIdOrigen()))).findFirst().orElse(carteraDef);
        }
        expediente.setCartera(carteraDef);
      } else {
        expediente.setCartera(carteraDef);
      }
      expediente.setTipoEstado(estadoPendiente);
      expediente.setSubEstado(subEstadoPendiente);
      if (interviniente.getIdCarga() != null) {
        List<ClienteCartera> clientes = new ArrayList<>(carteraDef.getClientes());
        Cliente cliente = clientes.get(0).getCliente();
        String concatenado = generarNoHeredado(cliente.getId(), carteraDef.getId(), interviniente.getIdCarga());
        expediente.setIdConcatenado(concatenado);
      }
      for (Contrato c : contratosOrdenados.get(interviniente)) {
        c.setExpediente(expediente);
        //se marcan como nulos. En el siguiente paso se realizará el cálculo
        c.setEsRepresentante(false);
        c.setCalculado(false);

      }
      //calcular contrato representante
      //	calcularContratoRepresentante(contratosOrdenados.get(interviniente),interviniente, cartera);
    }
  }

  private void agruparContratosPorCriterioContrato(Cartera cartera) {
    //recuperar todos los contratos que tengan el expediente nulo
    List<Contrato> contratosParaOrdenar = contratoRepository.findAllByExpedienteId(null);
    //para cada contrato se genera un expediente nuevo
    for (Contrato contrato : contratosParaOrdenar) {
      //se crea expediente y se le asigna cartera
      Expediente expediente = new Expediente();
      expediente.setFechaCreacion(new Date());
      expediente.setCartera(cartera);
      expediente.setIdOrigen(contrato.getIdOrigen());
      if (contrato.getIdCarga() != null) {
        List<ClienteCartera> clientes = new ArrayList<>(cartera.getClientes());
        Cliente cliente = clientes.get(0).getCliente();
        String concatenado = generarNoHeredado(cliente.getId(), cartera.getId(), contrato.getIdCarga());
        expediente.setIdConcatenado(concatenado);
      }
      //se asigna el expediente
      contrato.setExpediente(expediente);
      //el contrato es representante ya que es el único del expediente
      contrato.setEsRepresentante(true);
    }
  }

  private void agruparContratosPorCriterioHeredado(Cartera cartera) {
    //todos los contratos deberían tener el contrato asociado durante la carga
    List<Contrato> contratosParaOrdenar = contratoRepository.findAllByExpedienteId(null);
    //para cada contrato se genera un expediente nuevo
    for (Contrato contrato : contratosParaOrdenar) {
      //se crea expediente y se le asigna cartera
      Expediente expediente = expedienteRepository.findByIdCarga(contrato.getIdOrigen()).orElse(new Expediente());
      if (expediente.getId() == null) {
        expediente.setFechaCreacion(new Date());
        expediente.setCartera(cartera);
        expediente.setIdCarga(contrato.getIdOrigen());
      }
      if (contrato.getIdCarga() != null) {
        List<ClienteCartera> clientes = new ArrayList<>(cartera.getClientes());
        Cliente cliente = clientes.get(0).getCliente();
        String concatenado = generarHeredado(cliente.getId(), cartera.getId(), contrato.getIdCarga());
        expediente.setIdConcatenado(concatenado);
      }
      //se asigna el expediente
      contrato.setExpediente(expediente);
    }
  }

  public StringBuilder generarConcatenado(Integer cliente, Integer cartera) {
    StringBuilder result = new StringBuilder();
    String clienteS = cliente.toString();
    for (Integer sice = clienteS.length(); sice < 4; sice++) {
      result.append("0");
    }
    result.append(clienteS);
    String carteraS = cartera.toString();
    for (Integer sice = carteraS.length(); sice < 4; sice++) {
      result.append("0");
    }
    result.append(carteraS);

    return result;
  }

  private String generarHeredado(Integer cliente, Integer cartera, String carga) {
    StringBuilder result = generarConcatenado(cliente, cartera);
    result.append(carga);
    return result.toString();
  }

  private String generarNoHeredado(Integer cliente, Integer cartera, String carga) {
    StringBuilder result = generarConcatenado(cliente, cartera);

    if (carga.length() == 10) result.append(carga);
    else if (carga.length() > 10) result.append(carga.substring(carga.length() - 10));
    else {
      for (Integer sice = carga.length(); sice < 10; sice++) {
        result.append("0");
      }
      result.append(carga);
    }
    return result.toString();
  }
}
