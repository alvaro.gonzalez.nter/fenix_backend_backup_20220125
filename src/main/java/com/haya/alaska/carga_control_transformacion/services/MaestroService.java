package com.haya.alaska.carga_control_transformacion.services;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.WebServiceIOException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class MaestroService {

  @Autowired
  private ContratoRepository contratoRepository;

  @Autowired
  private IntervinienteRepository intervinienteRepository;

  @Autowired
  private BienRepository bienRepository;

  @Autowired
  private CarteraRepository carteraRepository;

  @Autowired
  private ServicioMaestro servicioMaestro;

  @SneakyThrows
  public void llamar(String idCartera) throws NotFoundException {
    List<Bien> bienes = bienRepository.findAllByFechaCargaAndIdHayaIsNull(new Date());
    List<Bien> bienesAux = new ArrayList<>();
    for (int i = 0;i <= 100; i++) {
        Bien bien = bienes.get(i);
        Set<ContratoBien> contratoBienes = bien.getContratos();
        for (ContratoBien contratoBien : contratoBienes) {
          Bien bienHaya = callMaster(bien, contratoBien.getContrato().getExpediente().getCartera().getCliente().getNombre());
          bien.setIdHaya(bienHaya.getIdHaya());
          bienesAux.add(bien);
        }
      }
    bienRepository.saveAll(bienesAux);

    List<Interviniente> intervinientes = intervinienteRepository.findAllByFechaCargaAndIdHayaIsNull(new Date());
    List<Interviniente> intervinientesAux = new ArrayList<>();
      for (int i = 0;i <= 100; i++) {
        Interviniente interviniente = intervinientes.get(i);
        Set<ContratoInterviniente> contratoIntervinientes = interviniente.getContratos();
        for (ContratoInterviniente contratoInterviniente : contratoIntervinientes) {
          Interviniente intervinienteHaya = callMaster(interviniente, contratoInterviniente.getContrato().getExpediente().getCartera().getCliente().getNombre(), contratoInterviniente.getTipoIntervencion());
          interviniente.setIdHaya(intervinienteHaya.getIdHaya());
          intervinientesAux.add(interviniente);
      }
    }
    intervinienteRepository.saveAll(intervinientesAux);

    List<Contrato> contratos = contratoRepository.findAllByFechaCargaAndIdHayaIsNull(new Date());
    List<Contrato> contratoAux = new ArrayList<>();
      for (int i = 0;i <= 100; i++) {
        Contrato contrato = contratos.get(i);
        Contrato contratoHaya = callMaster(contrato, contrato.getExpediente().getCartera().getCliente().getNombre());
        contrato.setIdHaya(contratoHaya.getIdHaya());
        contratoAux.add(contrato);
      }
    contratoRepository.saveAll(contratoAux);
  }

  private Bien callMaster(Bien bien, String cliente) {
    // Llamada al servicio de maestros para obtener el idHaya del bien
    try {
      servicioMaestro.registrarBien(bien, cliente);
    } catch (WebServiceIOException | NotFoundException | LockedChangeException e) {
      log.warn(e.getMessage());
      bien.setIdHaya(null);
    }
    log.debug(String.format("Bien.idHaya -> %s", bien.getIdHaya()));
    return bien;
  }

  private Contrato callMaster(Contrato contrato, String cliente) {

    // Llamada al servicio de maestros para obtener el idHaya del contrato
    try {
      servicioMaestro.registrarContrato(contrato, cliente);
    } catch (NotFoundException | WebServiceIOException | LockedChangeException e) {
      log.warn(e.getMessage());
      contrato.setIdHaya(null);
    }
    log.debug(String.format("Contrato.idHaya -> %s", contrato.getIdHaya()));
    return contrato;
  }

  private Interviniente callMaster(Interviniente interviniente, String cliente, TipoIntervencion tipoIntervencion) {
    // Llamada al servicio de maestros para obtener el idHaya del contrato
    try {
      servicioMaestro.registrarInterviniente(interviniente, cliente, tipoIntervencion);
    } catch (NotFoundException | WebServiceIOException | LockedChangeException e) {
      log.warn(e.getMessage());
      interviniente.setIdHaya(null);
    }
    log.debug(String.format("Interviniente.idHaya -> %s", interviniente.getId()));
    return interviniente;
  }
}
