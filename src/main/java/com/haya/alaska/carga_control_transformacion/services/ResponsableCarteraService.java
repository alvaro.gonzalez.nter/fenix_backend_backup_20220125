package com.haya.alaska.carga_control_transformacion.services;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.infrastructure.repository.AsignacionExpedienteRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ResponsableCarteraService {

  @Autowired
  AsignacionExpedienteRepository asignacionExpedienteRepository;
  @Autowired
  ExpedienteRepository expedienteRepository;

  public void asignarResponsableCartera() {

    List<Expediente> listaInicial = expedienteRepository.findByFechaCreacion(new Date());

    //recorrer expedientes
    for (Expediente expediente : listaInicial) {

      Usuario usuario = expediente.getCartera() != null && expediente.getCartera().getResponsableCartera() != null ? expediente.getCartera().getResponsableCartera() : null;

      if (usuario != null) {

        List<AsignacionExpediente> listaAsignaciones = asignacionExpedienteRepository.findAllByExpedienteIdAndUsuarioId(expediente.getId(), usuario.getId());

        boolean anadir = true;
        for (AsignacionExpediente asignacion : listaAsignaciones) {
          if (asignacion.getUsuario().equals(usuario))
            anadir = false;
        }
        if (anadir) {

          //crear la asignacion
          AsignacionExpediente asignacionActual = new AsignacionExpediente(expediente, expediente.getCartera().getResponsableCartera(), expediente.getCartera().getResponsableCartera());

          //guardar asignacion y actualizar expediente
          AsignacionExpediente asignacionAnadida = asignacionExpedienteRepository.save(asignacionActual);
          expediente.getAsignaciones().add(asignacionAnadida);
          expedienteRepository.save(expediente);
        }
      }
    }

  }


}
