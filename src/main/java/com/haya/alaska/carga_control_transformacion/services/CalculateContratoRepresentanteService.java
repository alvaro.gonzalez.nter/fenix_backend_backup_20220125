package com.haya.alaska.carga_control_transformacion.services;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;
import com.haya.alaska.contrato.application.ContratoUseCase;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CalculateContratoRepresentanteService {

  @Autowired private CarteraRepository carteraRepository;

  @Autowired private ContratoRepository contratoRepository;

  @Autowired private ContratoUseCase contratoUseCase;

  public void establecerContratoRepresentate(String idCartera) {

    Cartera cartera =
        carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(idCartera).orElse(null);
    calcularContratoRepresentante(cartera);
  }

  private void calcularContratoRepresentante(Cartera cartera) {
    // se recuperan los criterios de cálculo del contrato representante
    Set<CarteraContratoRepresentante> criterios = cartera.getCriterioContratoRepresentante();

    // se recuperan los contratos que no tienen el representante calculado
    List<Contrato> contratosSinRepresentante = contratoRepository.findAllByCalculadoIsFalse();

    // se guardan los id de expediente que no tienen representante
    Set<Integer> expedienteIds = new HashSet<Integer>();
    for (Contrato ce : contratosSinRepresentante) {
      if (ce.getExpediente() != null) {
        expedienteIds.add(ce.getExpediente().getId());
      }
    }
    // se invoca al servicio de ordenación para cada expediente
    List<Contrato> contratoSave = new ArrayList<>();
    for (Integer id : expedienteIds) {
      List<Contrato> contratosPorExpediente = contratoUseCase.findAllByExpedienteId(id, criterios);
      // el contrato en posición 0 es el representante
      contratosPorExpediente.get(0).setEsRepresentante(true);
      // se pone el indicado de calculo a true para indicar que esos contratos no debne ser tratados
      // de nuevo
      for (Contrato conEs : contratosPorExpediente) {
        conEs.setCalculado(true);
      }
      contratoSave.addAll(contratosPorExpediente);
    }
    contratoRepository.saveAll(contratoSave);
  }
}
