package com.haya.alaska.perfil.infrastructure.repository;

import com.haya.alaska.perfil.domain.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Integer> {
  Optional<Perfil> findByNombre(String nombre);

  Optional<Perfil> findByNombreIngles(String nombre);

  List<Perfil> findAllDistinctByUsuariosAsignacionExpedientesExpedienteIdIn(List<Integer> id);

  List<Perfil> findAllDistinctByUsuariosAsignacionExpedientesPNExpedienteIdIn(List<Integer> id);

}
