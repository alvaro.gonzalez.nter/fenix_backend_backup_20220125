package com.haya.alaska.perfil.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.area_perfil.domain.AreaPerfil;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_PERFIL")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_PERFIL")
public class Perfil implements Serializable {
  private static final long serialVersionUID = 1L;

  public static final String GESTOR = "Gestor";
  public static final String RESPONSABLE_DE_CARTERA = "Responsable de cartera";
  public static final String SUPERVISOR = "Supervisor";
  public static final String GESTOR_FORMALIZACION = "Gestor formalización";
  public static final String RESPONSABLE_FORMALIZACION = "Responsable formalización";
  public static final String SOPORTE_FORMALIZACION = "Soporte formalización";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "DES_NOMBRE")
  private String nombre;

  @Column(name = "DES_NOMBRE_INGLES")
  private String nombreIngles;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERFIL")
  @NotAudited
  private Set<Usuario> usuarios = new HashSet<>();

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AREA_PERFIL")
  private AreaPerfil areaPerfil;

  @ManyToMany
  @JoinTable(name = "RELA_CARTERA_PERFIL",
    joinColumns = { @JoinColumn(name = "ID_PERFIL") },
    inverseJoinColumns = { @JoinColumn(name = "ID_CARTERA") }
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_CARTERA_PERFIL")
  private Set<Cartera> carteras = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERFIL")
  @NotAudited
  private Set<PermisoAsignado> permisos = new HashSet<>();
}
