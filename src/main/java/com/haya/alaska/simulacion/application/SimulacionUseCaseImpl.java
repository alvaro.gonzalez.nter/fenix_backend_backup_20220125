package com.haya.alaska.simulacion.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.propuesta.aplication.PropuestaUseCase;
import com.haya.alaska.propuesta.infrastructure.controller.dto.BienPrecioPropuestaInputDTO;
import com.haya.alaska.propuesta.infrastructure.controller.dto.PropuestaDTO;
import com.haya.alaska.propuesta.infrastructure.controller.dto.PropuestaInputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.simulacion.domain.Simulacion;
import com.haya.alaska.simulacion.infrastructure.controller.dto.ConsolidacionPropuestaInputDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.SimulacionVariablesDto;
import com.haya.alaska.simulacion.infrastructure.controller.dto.excel.SimulacionGraficaPalancaExcelDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.excel.SimulacionPropuestaExcelDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.excel.SimulacionResultadoExcelDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.excel.SimulacionVariablesExcelDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.grafica.SimulacionGraficaDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.grafica.SimulacionGraficaPalancaDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.input.*;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.*;
import com.haya.alaska.simulacion.infrastructure.mapper.SimulacionMapper;
import com.haya.alaska.simulacion.infrastructure.repository.SimulacionRepository;
import com.haya.alaska.simulacion.infrastructure.util.SimulacionUtil;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.tipo_propuesta.infrastructure.repository.TipoPropuestaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.haya.alaska.variable_ajd.controller.dto.input.ModificarVariableAJDInputDTO;
import com.haya.alaska.variable_ajd.domain.VariableAjd;
import com.haya.alaska.variable_ajd.mapper.VariableAjdMapper;
import com.haya.alaska.variable_ajd.repository.VariableAjdRepository;
import com.haya.alaska.variable_itp.controller.dto.input.ModificarVariableITPInputDTO;
import com.haya.alaska.variable_itp.domain.VariableItp;
import com.haya.alaska.variable_itp.mapper.VariableItpMapper;
import com.haya.alaska.variable_itp.repository.VariableItpRepository;
import com.haya.alaska.variable_simulacion.controller.dto.VariableSimulacionBienDTO;
import com.haya.alaska.variable_simulacion.controller.dto.input.ModificarVariableSimulacionInputDTO;
import com.haya.alaska.variable_simulacion.domain.VariableSimulacion;
import com.haya.alaska.variable_simulacion.repository.VariableSimulacionRepository;
import com.haya.alaska.variable_simulacion.util.VariableSimulacionMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SimulacionUseCaseImpl implements SimulacionUseCase {

  private static final String NIVEL_SOLUCION = "SOLUCION";
  private static final String NIVEL_CONTRATO = "CONTRATO";
  private static final String NIVEL_BIEN = "BIEN";
  private static final String NIVEL_GARANTIA = "GARANTIA";
  private static final ObjectMapper objectMapper = new ObjectMapper();

  @Autowired SimulacionRepository simulacionRepository;
  @Autowired SimulacionMapper simulacionMapper;
  @Autowired SimulacionUtil simulacionUtil;
  @Autowired VariableSimulacionRepository variableSimulacionRepository;
  @Autowired VariableSimulacionMapper variableSimulacionMapper;
  @Autowired VariableItpRepository variableItpRepository;
  @Autowired VariableItpMapper variableItpMapper;
  @Autowired VariableAjdRepository variableAjdRepository;
  @Autowired VariableAjdMapper variableAjdMapper;
  @Autowired PropuestaUseCase propuestaUseCase;
  @Autowired UsuarioRepository usuarioRepository;
  @Autowired TipoPropuestaRepository tipoPropuestaRepository;
  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired ContratoRepository contratoRepository;
  @Autowired BienRepository bienRepository;
  @Autowired IntervinienteRepository intervinienteRepository;

  public SimulacionCabeceraDTO cabecera(SimulacionCabeceraInputDTO simulacionCabeceraInputDTO)
      throws NotFoundException {
    return simulacionMapper.inputACabecera(simulacionCabeceraInputDTO);
  }

  public SimulacionDTO iniciar(SimulacionInputDTO simulacionInputDTO) throws Exception {
    return simulacionMapper.inputADtoNormal(simulacionInputDTO);
  }

  public SimulacionDTO simular(SimulacionInputDTO simulacionInputDTO) throws Exception {
    SimulacionDTO simulacionDTO = simulacionMapper.inputADtoNormal(simulacionInputDTO);
    simulacionDTO.setResultado(
        simulacionUtil.calcularResultado(
            simulacionDTO,
            simulacionInputDTO.getRecalculado(),
            simulacionInputDTO.getBusqueda().getFechaSolucionAmistosa()));
    return simulacionDTO;
  }

  public SimulacionGraficaDTO obtenerGrafica(SimulacionInputDTO simulacionInputDTO)
    throws Exception {
    SimulacionGraficaDTO grafica = new SimulacionGraficaDTO();
    Map<String, SimulacionGraficaPalancaDTO> palancas = new HashMap<>();
    SimulacionDTO simulacionDTO = simulacionMapper.inputADtoNormal(simulacionInputDTO);

    SimulacionGraficaPalancaDTO cancelacion = simulacionUtil.calcularGraficaCancelacion(
      simulacionDTO,simulacionInputDTO.getBusqueda().getFechaSolucionAmistosa());

    SimulacionGraficaPalancaDTO ventaColateral = simulacionUtil.calcularGraficaVentaColateral(
      simulacionDTO,simulacionInputDTO.getBusqueda().getFechaSolucionAmistosa());

    SimulacionGraficaPalancaDTO refinanciacion = simulacionUtil.calcularGraficaRefinanciacion(
      simulacionDTO,simulacionInputDTO.getBusqueda().getFechaSolucionAmistosa());

    SimulacionGraficaPalancaDTO dacion = simulacionUtil.calcularGraficaDacion(
      simulacionDTO,simulacionInputDTO.getBusqueda().getFechaSolucionAmistosa());

    SimulacionGraficaPalancaDTO adjudicacion = simulacionUtil.calcularGraficaAdjudicacion(
      simulacionDTO,simulacionInputDTO.getBusqueda().getFechaSolucionAmistosa());

    SimulacionResultadoDTO result =
        simulacionUtil.calcularResultado(
            simulacionDTO,
            simulacionInputDTO.getRecalculado(),
            simulacionInputDTO.getBusqueda().getFechaSolucionAmistosa());

    for (var entry : cancelacion.getAnyos().entrySet()) {
      entry.getValue().get(0).setVan(result.getVan().getCancelacion());
      entry.getValue().get(0).setTir(result.getTir().getCancelacion());
      entry.getValue().get(0).setMfp(result.getMfp().getCancelacion());
    }
    for (var entry : ventaColateral.getAnyos().entrySet()) {
      entry.getValue().get(0).setVan(result.getVan().getVentaColateral());
      entry.getValue().get(0).setTir(result.getTir().getVentaColateral());
      entry.getValue().get(0).setMfp(result.getMfp().getVentaColateral());
    }
    for (var entry : refinanciacion.getAnyos().entrySet()) {
      entry.getValue().get(0).setVan(result.getVan().getRefinanciacion());
      entry.getValue().get(0).setTir(result.getTir().getRefinanciacion());
      entry.getValue().get(0).setMfp(result.getMfp().getRefinanciacion());
    }
    for (var entry : dacion.getAnyos().entrySet()) {
      entry.getValue().get(0).setVan(result.getVan().getDacion());
      entry.getValue().get(0).setTir(result.getTir().getDacion());
      entry.getValue().get(0).setMfp(result.getMfp().getDacion());
    }
    for (var entry : adjudicacion.getAnyos().entrySet()) {
      entry.getValue().get(0).setVan(result.getVan().getAdjudicacion());
      entry.getValue().get(0).setTir(result.getTir().getAdjudicacion());
      entry.getValue().get(0).setMfp(result.getMfp().getAdjudicacion());
    }

    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      palancas.put("Cancellation", cancelacion);
      palancas.put("Collateral sale", ventaColateral);
      palancas.put("Refinancing", refinanciacion);
      palancas.put("Donation", dacion);
      palancas.put("Adjudication", adjudicacion);
    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      palancas.put("Cancelación", cancelacion);
      palancas.put("Venta de Colateral", ventaColateral);
      palancas.put("Refinanciación", refinanciacion);
      palancas.put("Dación", dacion);
      palancas.put("Adjudicación", adjudicacion);
    }

    grafica.setPalancas(palancas);

    return grafica;
  }

  public SimulacionDTO guardarSimulacion(
    Integer idSimulacion, String nombre, SimulacionInputDTO simulacionInputDTO)
    throws Exception {
    SimulacionDTO simulacionDTO = this.simular(simulacionInputDTO);
    Simulacion simulacion =
        simulacionMapper.dtoAEntidad(idSimulacion, nombre, simulacionInputDTO, simulacionDTO.getResultado());
    if (Objects.nonNull(simulacion)) {
      simulacion = simulacionRepository.save(simulacion);
      return simulacionMapper.entidadADto(simulacion);
    }
    return null;
  }

  public SimulacionDTO obtenerSimulacion(Integer idSimulacion)
    throws Exception {
    Simulacion simulacion =
        simulacionRepository
            .findById(idSimulacion)
            .orElseThrow(
                () ->
                    new NotFoundException("Simulacion con id " + idSimulacion + " no encontrada"));

    return Objects.nonNull(simulacion) ? simulacionMapper.entidadADto(simulacion) : null;
  }

  public SimulacionCabeceraDTO obtenerCabecera(Integer idSimulacion)
      throws JsonProcessingException, NotFoundException {
    Simulacion simulacion =
        simulacionRepository
            .findById(idSimulacion)
            .orElseThrow(
                () ->
                    new NotFoundException("Simulacion con id " + idSimulacion + " no encontrada"));

    return Objects.nonNull(simulacion) ? simulacionMapper.entidadACabeceraDto(simulacion) : null;
  }

  public SimulacionDTO consolidar(Usuario loged, Integer idSimulacion, ConsolidacionPropuestaInputDTO input)
    throws Exception {
    Usuario user = usuarioRepository.findById(loged.getId()).orElseThrow(() ->
      new NotFoundException("Usuario", loged.getId()));
    if (idSimulacion == null) throw new RequiredValueException("idSimulacion");
    Simulacion simulacion =
        simulacionRepository
            .findById(idSimulacion)
            .orElseThrow(
                () ->
                    new NotFoundException("Simulacion", idSimulacion));
    if (simulacion != null) {
      simulacion.setConsolidada(true);
      simulacionRepository.save(simulacion);
      SimulacionInputDTO simulacionInputDTO =
        objectMapper.readValue(simulacion.getCamposGuardados(), SimulacionInputDTO.class);
      SimulacionBusquedaInputDTO busqueda = simulacionInputDTO.getBusqueda();
      PropuestaDTO propuesta = consolidarPropuesta(user, input, busqueda);

      SimulacionDTO result = simulacionMapper.entidadADto(simulacion);
      result.setIdPropuesta(propuesta != null ? propuesta.getId() : null);
      return result;
    }
    return null;
  }

  private PropuestaDTO consolidarPropuesta(Usuario user, ConsolidacionPropuestaInputDTO input, SimulacionBusquedaInputDTO busqueda) throws Exception {
    PropuestaInputDTO pInput = new PropuestaInputDTO();
    pInput.setClase(input.getClasePropuesta());
    pInput.setTipo(input.getTipoPropuesta());
    pInput.setSubtipo(input.getSubtipoPropuesta());

    pInput.setContratos(busqueda.getIdsContrato());
    pInput.setIntervinientes(busqueda.getIdsInterviniente());

    List<BienPrecioPropuestaInputDTO> bInputList = new ArrayList<>();
    for (Integer idBien : busqueda.getIdsBien()){
      BienPrecioPropuestaInputDTO bInput = new BienPrecioPropuestaInputDTO();
      bInput.setIdBien(idBien);
      bInputList.add(bInput);
    }
    pInput.setBienes(bInputList);

    PropuestaDTO propuesta = propuestaUseCase.createPropuesta(pInput, busqueda.getIdExpediente(), user, false);
    return propuesta;
  }

  public List<CatalogoMinInfoDTO> getPalancas(){
    List<String> codigos = new ArrayList<>();
    codigos.add("C");
    codigos.add("V");
    codigos.add("R");
    codigos.add("D");

    List<TipoPropuesta> tipos = tipoPropuestaRepository.findAllByCodigoIn(codigos);
    List<CatalogoMinInfoDTO> result = tipos.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
    return result;
  }

  public SimulacionVariablesDto getVariables() {
    SimulacionVariablesDto simulacionVariablesDto = new SimulacionVariablesDto();

    simulacionVariablesDto.setVariablesSolucion(
        variableSimulacionRepository.findAllByNivel(NIVEL_SOLUCION).stream()
            .map(vs -> variableSimulacionMapper.entidadADto(vs))
            .collect(Collectors.toList()));

    simulacionVariablesDto.setVariablesContrato(
        variableSimulacionRepository.findAllByNivel(NIVEL_CONTRATO).stream()
            .map(vs -> variableSimulacionMapper.entidadADto(vs))
            .collect(Collectors.toList()));

    VariableSimulacionBienDTO variablesBien = new VariableSimulacionBienDTO();
    variablesBien.setVariablesGenerales(
        variableSimulacionRepository.findAllByNivel(NIVEL_BIEN).stream()
            .map(vs -> variableSimulacionMapper.entidadADto(vs))
            .collect(Collectors.toList()));
    variablesBien.setVariablesITP(
        variableItpRepository.findAll().stream()
            .map(vi -> variableItpMapper.entidadADto(vi))
            .collect(Collectors.toList()));
    variablesBien.setVariablesAJD(
        variableAjdRepository.findAll().stream()
            .map(va -> variableAjdMapper.entidadADto(va))
            .collect(Collectors.toList()));

    simulacionVariablesDto.setVariablesBien(variablesBien);

    return simulacionVariablesDto;
  }

  @Override
  public SimulacionVariablesDto modificarVariables(
      ModificarVariablesSimulacionInputDTO modificarVariablesSimulacionInputDTO)
      throws NotFoundException {
    for (ModificarVariableSimulacionInputDTO variableSimulacionInput :
        modificarVariablesSimulacionInputDTO.getVariablesSimulacion()) {
      if (Objects.nonNull(variableSimulacionInput.getNuevoValor())) {
        VariableSimulacion variableSimulacion =
            variableSimulacionRepository
                .findById(variableSimulacionInput.getId())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "Variable simulacion con id "
                                + variableSimulacionInput.getId()
                                + " no encontrada"));

        variableSimulacion.setValor(variableSimulacionInput.getNuevoValor());
        variableSimulacionRepository.saveAndFlush(variableSimulacion);
      }
    }

    for (ModificarVariableITPInputDTO variableITPInput :
        modificarVariablesSimulacionInputDTO.getVariablesITP()) {
      if (Objects.nonNull(variableITPInput.getNuevoPorcentaje())
          || Objects.nonNull(variableITPInput.getNuevoImporteMinimo())
          || Objects.nonNull(variableITPInput.getNuevoImporteMaximo())) {
        VariableItp variableItp =
            variableItpRepository
                .findById(variableITPInput.getId())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "Variable ITP con id " + variableITPInput.getId() + " no encontrada"));

        if (Objects.nonNull(variableITPInput.getNuevoPorcentaje())) {
          variableItp.setPorcentaje(variableITPInput.getNuevoPorcentaje());
        }
        if (Objects.nonNull(variableITPInput.getNuevoImporteMinimo())) {
          variableItp.setImporteMinimo(variableITPInput.getNuevoImporteMinimo());
        }
        if (Objects.nonNull(variableITPInput.getNuevoImporteMaximo())) {
          variableItp.setImporteMaximo(variableITPInput.getNuevoImporteMaximo());
        }
        variableItpRepository.saveAndFlush(variableItp);
      }
    }

    for (ModificarVariableAJDInputDTO variableAJDInput :
        modificarVariablesSimulacionInputDTO.getVariablesAJD()) {
      if (Objects.nonNull(variableAJDInput.getNuevoPorcentaje())) {
        VariableAjd variableAjd =
            variableAjdRepository
                .findById(variableAJDInput.getId())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "Variable ITP con id " + variableAJDInput.getId() + " no encontrada"));

        variableAjd.setPorcentaje(variableAJDInput.getNuevoPorcentaje());
        variableAjdRepository.saveAndFlush(variableAjd);
      }
    }

    return this.getVariables();
  }

  public ListWithCountDTO<SimulacionDTOToList> obtenerHistorico() throws JsonProcessingException {
    List<SimulacionDTOToList> simulacionDTOToList = new ArrayList<>();
    List<Simulacion> listaTodas = simulacionRepository.findAllByOrderByFechaUltimoCambioDesc();
    List<Simulacion> lista = new ArrayList<>();
    int contador=0;
    for(Simulacion simulacion: listaTodas) {
      if(contador<10) {
        lista.add(simulacion);
        contador++;
      } else break;
    }
    for (Simulacion simulacion : lista) {
      simulacionDTOToList.add(simulacionMapper.entidadADtoToList(simulacion));
    }
    return new ListWithCountDTO<>(simulacionDTOToList, simulacionDTOToList.size());
  }

  public LinkedHashMap<String, List<Collection<?>>> findExcelSimulacion(
    SimulacionInputDTO simulacionInputDTO, Usuario usuario) throws Exception {
    SimulacionDTO simulacionDTO = this.simular(simulacionInputDTO);
    SimulacionGraficaDTO simulacionGraficaDTO = this.obtenerGrafica(simulacionInputDTO);
    return this.createSimulacionDataForExcel(simulacionDTO,simulacionGraficaDTO,simulacionInputDTO.getBusqueda(),usuario);
  }

  private LinkedHashMap<String, List<Collection<?>>> createSimulacionDataForExcel(
      SimulacionDTO simulacionDTO, SimulacionGraficaDTO simulacionGraficaDTO, SimulacionBusquedaInputDTO busqueda, Usuario usuario) {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> resultado = new ArrayList<>();
    resultado.add(SimulacionResultadoExcelDTO.cabeceras);
    resultado.add(
        new SimulacionResultadoExcelDTO(simulacionDTO.getResultado(), "cancelacion")
            .getValuesList());
    resultado.add(
        new SimulacionResultadoExcelDTO(simulacionDTO.getResultado(), "ventaColateral")
            .getValuesList());
    resultado.add(
        new SimulacionResultadoExcelDTO(simulacionDTO.getResultado(), "refinanciacion")
            .getValuesList());
    resultado.add(
        new SimulacionResultadoExcelDTO(simulacionDTO.getResultado(), "dacion").getValuesList());
    resultado.add(
        new SimulacionResultadoExcelDTO(simulacionDTO.getResultado(), "adjudicacion")
            .getValuesList());
    datos.put("Resultado", resultado);

    List<Collection<?>> variables = new ArrayList<>();
    variables.add(SimulacionVariablesExcelDTO.cabeceras);
    variables = addVariablesSolucion(variables, simulacionDTO.getVariablesSolucion());
    variables = addVariablesContratos(variables, simulacionDTO.getVariablesContratos());
    variables = addVariablesBienes(variables, simulacionDTO.getVariablesBienes());
    datos.put("Variables", variables);

    List<Collection<?>> propuestas = new ArrayList<>();
    propuestas.add(SimulacionPropuestaExcelDTO.cabeceras);
    propuestas = addVariablesPropuesta(propuestas, busqueda, usuario);
    datos.put("Propuestas", propuestas);

    //por cada palanca meter una pestaña. todas usaran el mismo objeto
    var filas = simulacionUtil.generarFilas();

    List<Collection<?>> cancelacion = new ArrayList<>();
    SimulacionGraficaPalancaDTO cancelacionDto = new SimulacionGraficaPalancaDTO();
    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cancelacionDto = simulacionGraficaDTO.getPalancas().get("Cancellation");
    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      cancelacionDto = simulacionGraficaDTO.getPalancas().get("Cancelación");
    }
    SimulacionGraficaPalancaExcelDTO cancelacionExcelDTO = new SimulacionGraficaPalancaExcelDTO(cancelacionDto);
    cancelacion.add(cancelacionExcelDTO.cabeceras);
    for(var fila: filas) {
      SimulacionGraficaPalancaExcelDTO palancaDTOTemp = new SimulacionGraficaPalancaExcelDTO(cancelacionDto,fila.getNombre());
      cancelacion.add(palancaDTOTemp.getDatos());
    }
    datos.put("Cancelación", cancelacion);


    List<Collection<?>> ventaColateral = new ArrayList<>();
    SimulacionGraficaPalancaDTO ventaColateralDTO = new SimulacionGraficaPalancaDTO();
    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      ventaColateralDTO = simulacionGraficaDTO.getPalancas().get("Collateral sale");
    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      ventaColateralDTO = simulacionGraficaDTO.getPalancas().get("Venta de Colateral");
    }
    SimulacionGraficaPalancaExcelDTO ventaColateralExcelDTO = new SimulacionGraficaPalancaExcelDTO(ventaColateralDTO);
    ventaColateral.add(ventaColateralExcelDTO.cabeceras);
    for(var fila: filas) {
      SimulacionGraficaPalancaExcelDTO palancaDTOTemp = new SimulacionGraficaPalancaExcelDTO(ventaColateralDTO,fila.getNombre());
      ventaColateral.add(palancaDTOTemp.getDatos());
    }
    datos.put("Venta de colateral", ventaColateral);


    List<Collection<?>> refinanciacion = new ArrayList<>();
    SimulacionGraficaPalancaDTO refinanciacionDTO = new SimulacionGraficaPalancaDTO();
    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      refinanciacionDTO = simulacionGraficaDTO.getPalancas().get("Refinancing");
    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      refinanciacionDTO = simulacionGraficaDTO.getPalancas().get("Refinanciación");
    }
    SimulacionGraficaPalancaExcelDTO refinanciacionExcelDTO = new SimulacionGraficaPalancaExcelDTO(refinanciacionDTO);
    refinanciacion.add(refinanciacionExcelDTO.cabeceras);
    for(var fila: filas) {
      SimulacionGraficaPalancaExcelDTO palancaDTOTemp = new SimulacionGraficaPalancaExcelDTO(refinanciacionDTO,fila.getNombre());
      refinanciacion.add(palancaDTOTemp.getDatos());
    }
    datos.put("Refinanciación", refinanciacion);


    List<Collection<?>> dacion = new ArrayList<>();
    SimulacionGraficaPalancaDTO dacionDTO = new SimulacionGraficaPalancaDTO();
    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      dacionDTO = simulacionGraficaDTO.getPalancas().get("Donation");
    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      dacionDTO = simulacionGraficaDTO.getPalancas().get("Dación");
    }
    SimulacionGraficaPalancaExcelDTO dacionExcelDTO = new SimulacionGraficaPalancaExcelDTO(dacionDTO);
    dacion.add(dacionExcelDTO.cabeceras);
    for(var fila: filas) {
      SimulacionGraficaPalancaExcelDTO palancaDTOTemp = new SimulacionGraficaPalancaExcelDTO(dacionDTO,fila.getNombre());
      dacion.add(palancaDTOTemp.getDatos());
    }
    datos.put("Dación", dacion);


    List<Collection<?>> adjudicacion = new ArrayList<>();
    SimulacionGraficaPalancaDTO adjudicacionDTO = new SimulacionGraficaPalancaDTO();
    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      adjudicacionDTO = simulacionGraficaDTO.getPalancas().get("Adjudication");
    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      adjudicacionDTO = simulacionGraficaDTO.getPalancas().get("Adjudicación");
    }
    SimulacionGraficaPalancaExcelDTO adjudicacionExcelDTO = new SimulacionGraficaPalancaExcelDTO(adjudicacionDTO);
    adjudicacion.add(adjudicacionExcelDTO.cabeceras);
    for(var fila: filas) {
      SimulacionGraficaPalancaExcelDTO palancaDTOTemp = new SimulacionGraficaPalancaExcelDTO(adjudicacionDTO,fila.getNombre());
      adjudicacion.add(palancaDTOTemp.getDatos());
    }
    datos.put("Adjudicación", adjudicacion);

    return datos;
  }
  private List<Collection<?>> addVariablesPropuesta(
    List<Collection<?>> variables, SimulacionBusquedaInputDTO busqueda, Usuario usuario) {
    Expediente expediente = expedienteRepository.findById(busqueda.getIdExpediente()).orElse(null);
    List<Contrato> contratos = contratoRepository.findAllByIdIn(busqueda.getIdsContrato());
    List<Bien> bienes = bienRepository.findAllByIdIn(busqueda.getIdsBien());
    List<Interviniente> intervinientes = intervinienteRepository.findAllByIdIn(busqueda.getIdsInterviniente());

    String idsCargaContratos = contratos.size()>0 ? contratos.stream().map(contrato -> contrato.getIdCarga()).collect(Collectors.joining(", ")) : "";
    String idsCargaBienes = bienes.size()>0 ? bienes.stream().map(bien -> bien.getIdCarga()).collect(Collectors.joining(", ")) : "";
    String idsCargaIntervinientes = intervinientes.size()>0 ? intervinientes.stream().map(interviniente -> interviniente.getIdCarga()).collect(Collectors.joining(", ")) : "";

    variables.add(new SimulacionPropuestaExcelDTO("Usuario",usuario.getNombre()).getValuesList());
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    variables.add(new SimulacionPropuestaExcelDTO("Fecha",dateFormat.format(new Date())).getValuesList());
    variables.add(new SimulacionPropuestaExcelDTO("Expediente", expediente != null ? expediente.getIdConcatenado() : "").getValuesList());
    variables.add(new SimulacionPropuestaExcelDTO("Contratos",idsCargaContratos).getValuesList());
    variables.add(new SimulacionPropuestaExcelDTO("Bienes",idsCargaBienes).getValuesList());
    variables.add(new SimulacionPropuestaExcelDTO("Intervinientes",idsCargaIntervinientes).getValuesList());

    return variables;
  }


  private List<Collection<?>> addVariablesSolucion(
      List<Collection<?>> variables, SimulacionSolucionDTO simulacionSolucionDTO) {
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Deduda Actual",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getDeuda().getDeudaActual(),
                SimulacionVariablesExcelDTO.TIPO_DOUBLE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Entregas Dinerarias",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getIngresos().getImporteEntregasDinerarias(),
                SimulacionVariablesExcelDTO.TIPO_DOUBLE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Fecha Entregas Dinerarias",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getIngresos().getFechasEntregasDinerarias(),
                SimulacionVariablesExcelDTO.TIPO_DATE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Entregas No Dinerarias",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getIngresos().getImporteEntregasNoDinerarias(),
                SimulacionVariablesExcelDTO.TIPO_DOUBLE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Fecha Entregas No Dinerarias",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getIngresos().getFechaEntregasNoDinerarias(),
                SimulacionVariablesExcelDTO.TIPO_DATE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Dotaciones",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getIngresos().getDotaciones(),
                SimulacionVariablesExcelDTO.TIPO_DOUBLE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Fecha Recuperación Dotaciones",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getIngresos().getFechaRecuperacionDotaciones(),
                SimulacionVariablesExcelDTO.TIPO_DATE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Dotaciones Refin.",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getIngresos().getDotacionesRefin(),
                SimulacionVariablesExcelDTO.TIPO_DOUBLE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Fecha Dotaciones Refin.",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getIngresos().getFechaDotacionesRefin(),
                SimulacionVariablesExcelDTO.TIPO_DATE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Número Años Refinanciación",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getIngresos().getNumeroAnyosRefinanciacion(),
                SimulacionVariablesExcelDTO.TIPO_INTEGER)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Letrado",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getGastos().getLetrado(),
                SimulacionVariablesExcelDTO.TIPO_DOUBLE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Fecha Letrado",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getGastos().getFechaLetrado(),
                SimulacionVariablesExcelDTO.TIPO_DATE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Procurador",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getGastos().getProcurador(),
                SimulacionVariablesExcelDTO.TIPO_DOUBLE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Fecha Procurador",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getGastos().getFechaProcurador(),
                SimulacionVariablesExcelDTO.TIPO_DATE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Tasa Judicial",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getGastos().getTasaJudicial(),
                SimulacionVariablesExcelDTO.TIPO_DOUBLE)
            .getValuesList());
    variables.add(
        new SimulacionVariablesExcelDTO(
                "Fecha Tasa Judicialn",
                NIVEL_SOLUCION,
                null,
                simulacionSolucionDTO.getGastos().getFechaTasaJudicial(),
                SimulacionVariablesExcelDTO.TIPO_DATE)
            .getValuesList());

    if (Objects.nonNull(simulacionSolucionDTO.getGastos().getNuevosGastosAsociados())) {
      for (int i = 0;
          i < simulacionSolucionDTO.getGastos().getNuevosGastosAsociados().size();
          i++) {
        variables.add(
            new SimulacionVariablesExcelDTO(
                    "Nuevo Gasto Asociado " + i,
                    NIVEL_SOLUCION,
                    null,
                    simulacionSolucionDTO
                        .getGastos()
                        .getNuevosGastosAsociados()
                        .get(i)
                        .getOtroGastos(),
                    SimulacionVariablesExcelDTO.TIPO_SIMPLE)
                .getValuesList());
        variables.add(
            new SimulacionVariablesExcelDTO(
                    "Fecha Nuevo Gasto Asociado " + i,
                    NIVEL_SOLUCION,
                    null,
                    simulacionSolucionDTO
                        .getGastos()
                        .getNuevosGastosAsociados()
                        .get(i)
                        .getFechaOtroGasto(),
                    SimulacionVariablesExcelDTO.TIPO_SIMPLE)
                .getValuesList());
      }
    }

    variables.add(new SimulacionVariablesExcelDTO().getValuesList());

    return variables;
  }

  private List<Collection<?>> addVariablesContratos(
      List<Collection<?>> variables, List<SimulacionContratoDTO> simulacionContratoDTOS) {
    for (SimulacionContratoDTO contrato : simulacionContratoDTOS) {
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Deduda",
                  NIVEL_CONTRATO,
                  contrato.getIdCarga(),
                  contrato.getDeuda().getDeudaActual(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Importe Concedido",
                  NIVEL_CONTRATO,
                  contrato.getIdCarga(),
                  contrato.getInversion().getImporteConcedido(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Fecha Importe Concedido",
                  NIVEL_CONTRATO,
                  contrato.getIdCarga(),
                  contrato.getInversion().getFechaImporteConcedido(),
                  SimulacionVariablesExcelDTO.TIPO_DATE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Gastos Constitución",
                  NIVEL_CONTRATO,
                  contrato.getIdCarga(),
                  contrato.getInversion().getGastosConstitucion(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Fecha Gastos Constitución",
                  NIVEL_CONTRATO,
                  contrato.getIdCarga(),
                  contrato.getInversion().getFechaGastosConstitucion(),
                  SimulacionVariablesExcelDTO.TIPO_DATE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Fecha Impago",
                  NIVEL_CONTRATO,
                  contrato.getIdCarga(),
                  contrato.getIngresos().getFechaImpago(),
                  SimulacionVariablesExcelDTO.TIPO_DATE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Tipo Interés",
                  NIVEL_CONTRATO,
                  contrato.getIdCarga(),
                  contrato.getIngresos().getTipoInteres(),
                  SimulacionVariablesExcelDTO.TIPO_PORCENTAJE)
              .getValuesList());

      variables.add(new SimulacionVariablesExcelDTO().getValuesList());
    }

    return variables;
  }

  private List<Collection<?>> addVariablesBienes(
      List<Collection<?>> variables, List<SimulacionBienDTO> simulacionBienDTOS) {
    for (SimulacionBienDTO bien : simulacionBienDTOS) {
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Importe Subasta",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getIngresos().getImporteSubasta(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Porcetaje Adjudicación",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getIngresos().getPorcentajeAdjudicacion(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Fecha Adjudicación",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getIngresos().getFechaAdjudicacion(),
                  SimulacionVariablesExcelDTO.TIPO_DATE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Valoración Actual del Activo",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getIngresos().getValoracionActualActivo(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Fecha Venta Estimada",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getIngresos().getFechaVentaEstimada(),
                  SimulacionVariablesExcelDTO.TIPO_DATE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Fecha Acuerdo Venta",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getIngresos().getFechaAcuerdoVenta(),
                  SimulacionVariablesExcelDTO.TIPO_DATE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Liquidez del Activo",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getIngresos().getLiquidezActivo(),
                  SimulacionVariablesExcelDTO.TIPO_STRING)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "ITP",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getITP(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Fecha ITP",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getFechaITP(),
                  SimulacionVariablesExcelDTO.TIPO_DATE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "AJD",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getAJD(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Fecha AJD",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getFechaAJD(),
                  SimulacionVariablesExcelDTO.TIPO_DATE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "IBI",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getIBI(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Periodicidad IBI",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getPeriodicidadIBI(),
                  SimulacionVariablesExcelDTO.TIPO_STRING)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Comunidad",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getComunidad(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Periodicidad Comunidad",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getPeriodicidadComunidad(),
                  SimulacionVariablesExcelDTO.TIPO_STRING)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Facilities",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getFacilities(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Capex en el Activo",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getCapexEnElActivo(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Seguros",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getSeguros(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Periodicidad Pago Seguros",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getPeriodicidadPagoSeguros(),
                  SimulacionVariablesExcelDTO.TIPO_STRING)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Notaría",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getNotaria(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Registro",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getRegistro(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Tasación",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getTasacion(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "CEE",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getCEE(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Fecha CEE",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getFechaCEE(),
                  SimulacionVariablesExcelDTO.TIPO_DATE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Tramites Derecho Tanteo",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getTramitesDerechoTanteo(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Fecha Derecho Tanteo",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getFechaDerechoTanteo(),
                  SimulacionVariablesExcelDTO.TIPO_DATE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Cargas Asumidas",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getCargasAsumidas(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());
      variables.add(
          new SimulacionVariablesExcelDTO(
                  "Comisión API",
                  NIVEL_GARANTIA,
                  bien.getIdCarga(),
                  bien.getGastos().getComisionApi(),
                  SimulacionVariablesExcelDTO.TIPO_DOUBLE)
              .getValuesList());

      variables.add(new SimulacionVariablesExcelDTO().getValuesList());
    }

    return variables;
  }
}
