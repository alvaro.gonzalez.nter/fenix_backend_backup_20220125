package com.haya.alaska.simulacion.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.simulacion.infrastructure.controller.dto.ConsolidacionPropuestaInputDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.SimulacionVariablesDto;
import com.haya.alaska.simulacion.infrastructure.controller.dto.grafica.SimulacionGraficaDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.input.*;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionCabeceraDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionDTO;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionDTOToList;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface SimulacionUseCase {
  SimulacionCabeceraDTO cabecera(SimulacionCabeceraInputDTO simulacionCabeceraInputDTO)
      throws NotFoundException;

  SimulacionDTO iniciar(SimulacionInputDTO simulacionInputDTO) throws Exception;

  SimulacionDTO simular(SimulacionInputDTO simulacionInputDTO) throws Exception;

  SimulacionGraficaDTO obtenerGrafica(SimulacionInputDTO simulacionInputDTO) throws Exception;

  SimulacionDTO guardarSimulacion(
      Integer idSimulacion, String nombre, SimulacionInputDTO simulacionInputDTO)
    throws Exception;

  SimulacionDTO obtenerSimulacion(Integer idSimulacion)
    throws Exception;

  SimulacionCabeceraDTO obtenerCabecera(Integer idSimulacion)
      throws JsonProcessingException, NotFoundException;

  SimulacionDTO consolidar(Usuario user, Integer idSimulacion, ConsolidacionPropuestaInputDTO input) throws Exception;

  SimulacionVariablesDto getVariables();

  List<CatalogoMinInfoDTO> getPalancas();

  SimulacionVariablesDto modificarVariables(
      ModificarVariablesSimulacionInputDTO modificarVariablesSimulacionInputDTO)
      throws NotFoundException;

  ListWithCountDTO<SimulacionDTOToList> obtenerHistorico() throws JsonProcessingException;

  LinkedHashMap<String, List<Collection<?>>> findExcelSimulacion(SimulacionInputDTO simulacionInputDTO, Usuario usuario) throws Exception;
}
