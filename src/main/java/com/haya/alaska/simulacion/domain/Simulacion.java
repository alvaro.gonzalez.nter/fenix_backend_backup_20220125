package com.haya.alaska.simulacion.domain;

import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.expediente.domain.Expediente;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_SIMULACION")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_SIMULACION")
public class Simulacion {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE")
  private Expediente expediente;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLIENTE")
  private Cliente cliente;

  @Column(name = "DES_CAMPOS_GUARDADOS")
  @Lob
  private String camposGuardados;

  @Column(name = "DES_RESULTADO")
  @Lob
  private String resultado;

  @Column(name = "IND_CONSOLIDADA")
  private Boolean consolidada;

  @Column(name = "FCH_ULTIMO_CAMBIO")
  private Date fechaUltimoCambio;

  @Column(name = "DES_NOMBRE")
  private String nombre;
}
