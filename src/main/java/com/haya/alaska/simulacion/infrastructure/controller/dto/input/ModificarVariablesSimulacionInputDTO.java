package com.haya.alaska.simulacion.infrastructure.controller.dto.input;

import com.haya.alaska.variable_ajd.controller.dto.input.ModificarVariableAJDInputDTO;
import com.haya.alaska.variable_itp.controller.dto.input.ModificarVariableITPInputDTO;
import com.haya.alaska.variable_simulacion.controller.dto.input.ModificarVariableSimulacionInputDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ModificarVariablesSimulacionInputDTO implements Serializable {
  List<ModificarVariableSimulacionInputDTO> variablesSimulacion;
  List<ModificarVariableITPInputDTO> variablesITP;
  List<ModificarVariableAJDInputDTO> variablesAJD;
}
