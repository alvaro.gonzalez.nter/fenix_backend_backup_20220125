package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SimulacionSolucionIngresosDTO implements Serializable {
  private SimulacionFilaDoubleDTO importeEntregasDinerarias;
  private SimulacionFilaDatesDTO fechasEntregasDinerarias;
  private SimulacionFilaDoubleDTO importeEntregasNoDinerarias;
  private SimulacionFilaDatesDTO fechaEntregasNoDinerarias;
  private SimulacionFilaDoubleDTO dotaciones; // d, d, null, d, d
  private SimulacionFilaDatesDTO fechaRecuperacionDotaciones; // d, d, null, d, d
  private SimulacionFilaDoubleDTO dotacionesRefin; // null, null, d, null, null
  private SimulacionFilaDatesDTO fechaDotacionesRefin; // null, null, d, null, null
  private SimulacionFilaIntegerDTO numeroAnyosRefinanciacion; // null, null, d, null, null
}
