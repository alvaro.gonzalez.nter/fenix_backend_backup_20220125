package com.haya.alaska.simulacion.infrastructure.controller.dto.grafica;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionGraficaFilaDTO implements Serializable {

  String grupo;
  String nombre;
  Double q0;
  Double q1;
  Double q2;
  Double q3;
  Double q4;
  Double tasa; // porcentaje

  Double van;
  Double tir;
  Double mfp;

  public SimulacionGraficaFilaDTO(String grupo, String nombre) {
    this.grupo=grupo;
    this.nombre=nombre;
    this.q0=0d;
    this.q1=0d;
    this.q2=0d;
    this.q3=0d;
    this.q4=0d;
    this.tasa=0d;
    this.van=0d;
    this.tir=0d;
    this.mfp=0d;
  }
}
