package com.haya.alaska.simulacion.infrastructure.controller.dto;

import com.haya.alaska.variable_simulacion.controller.dto.VariableSimulacionBienDTO;
import com.haya.alaska.variable_simulacion.controller.dto.VariableSimulacionDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionVariablesDto implements Serializable {
  List<VariableSimulacionDTO> variablesSolucion;
  List<VariableSimulacionDTO> variablesContrato;
  VariableSimulacionBienDTO variablesBien;
}
