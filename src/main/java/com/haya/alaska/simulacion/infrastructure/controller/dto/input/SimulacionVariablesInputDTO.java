package com.haya.alaska.simulacion.infrastructure.controller.dto.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionVariablesInputDTO implements Serializable {
  private SimulacionVariablesSolucionInputDTO solucion;
  private List<SimulacionVariablesContratoInputDTO> contratos;
  private List<SimulacionVariablesBienInputDTO> bienes;
  private SimulacionVariablesCatalogoInputDTO catalogo;
}
