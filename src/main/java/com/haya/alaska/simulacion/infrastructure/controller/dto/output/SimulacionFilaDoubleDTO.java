package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SimulacionFilaDoubleDTO implements Serializable {
  private Double cancelacion;
  private Double ventaColateral;
  private Double refinanciacion;
  private Double dacion;
  private Double adjudicacion;

  public SimulacionFilaDoubleDTO(Double todasIguales) {
    this.cancelacion=todasIguales;
    this.ventaColateral=todasIguales;
    this.refinanciacion=todasIguales;
    this.dacion=todasIguales;
    this.adjudicacion=todasIguales;
  }
}
