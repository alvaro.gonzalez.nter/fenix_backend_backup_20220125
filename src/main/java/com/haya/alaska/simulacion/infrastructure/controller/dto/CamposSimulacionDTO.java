package com.haya.alaska.simulacion.infrastructure.controller.dto;

import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionSolucionDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionSolucionGastosDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CamposSimulacionDTO implements Serializable {
  SimulacionSolucionDTO simuladorSolucionDTO;
  SimulacionSolucionGastosDTO simuladorSolucionGastosDTO;
}
