package com.haya.alaska.simulacion.infrastructure.controller.dto.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionCabeceraInputDTO implements Serializable {
  SimulacionBusquedaInputDTO busqueda;
  Boolean recalculado;
}
