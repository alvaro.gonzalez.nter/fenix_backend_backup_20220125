package com.haya.alaska.simulacion.infrastructure.util;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.simulacion.infrastructure.controller.dto.OtrosGastosAsociadosDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.grafica.SimulacionGraficaFilaDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.grafica.SimulacionGraficaPalancaDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.input.SimulacionInputDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.input.SimulacionVariablesContratoInputDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.*;
import com.haya.alaska.simulacion.infrastructure.mapper.SimulacionMapper;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.variable_simulacion.domain.VariableSimulacion;
import com.haya.alaska.variable_simulacion.repository.VariableSimulacionRepository;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.poi.ss.formula.functions.Finance;
import org.apache.poi.ss.formula.functions.Irr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class SimulacionUtil {

  @Autowired ContratoRepository contratoRepository;
  @Autowired BienRepository bienRepository;
  @Autowired SimulacionMapper simulacionMapper;
  @Autowired VariableSimulacionRepository variableSimulacionRepository;

  public SimulacionResultadoDTO calcularResultado(
    SimulacionDTO simulacionDTO, boolean recalculado, Date fechaSolucionAmistosa) throws Exception {

    this.validarCampos(simulacionDTO,recalculado,fechaSolucionAmistosa);
    Map<String, Double> cancelacion =
      this.calcularCancelacion(simulacionDTO, fechaSolucionAmistosa);
    Map<String, Double> ventaColateral =
      this.calcularVentaColateral(simulacionDTO, fechaSolucionAmistosa);
    Map<String, Double> adjudicacion =
      this.calcularAdjudicacion(simulacionDTO, fechaSolucionAmistosa);
    Map<String, Double> refinanciacion =
      this.calcularRefinanciacion(simulacionDTO, fechaSolucionAmistosa);
    Map<String, Double> dacion = this.calcularDacion(simulacionDTO, fechaSolucionAmistosa);
    SimulacionResultadoDTO simulacionResultadoDTO = new SimulacionResultadoDTO();
    simulacionResultadoDTO.setTipoSimulacion(recalculado ? "Manual" : "Automática");
    simulacionResultadoDTO.setVan(
      new SimulacionFilaDoubleDTO(
        cancelacion.get("van"),
        ventaColateral.get("van"),
        refinanciacion.get("van"),
        dacion.get("van"),
        adjudicacion.get("van")));
    simulacionResultadoDTO.setTir(
      new SimulacionFilaDoubleDTO(
        cancelacion.get("tir"),
        ventaColateral.get("tir"),
        refinanciacion.get("tir"),
        dacion.get("tir"),
        adjudicacion.get("tir")));
    simulacionResultadoDTO.setMfp(
      new SimulacionFilaDoubleDTO(
        cancelacion.get("mfp"),
        ventaColateral.get("mfp"),
        refinanciacion.get("mfp"),
        dacion.get("mfp"),
        adjudicacion.get("mfp")));

    return simulacionResultadoDTO;
  }

  public void validarCampos(
    SimulacionDTO simulacionDTO, boolean recalculado, Date fechaSolucionAmistosa) throws Exception {
    for(var contrato: simulacionDTO.getVariablesContratos()) {
      if(Objects.isNull(contrato.getInversion().getFechaImporteConcedido().getAdjudicacion()) ||
        Objects.isNull(contrato.getInversion().getFechaImporteConcedido().getDacion()) ||
        Objects.isNull(contrato.getInversion().getFechaImporteConcedido().getVentaColateral()) ||
        Objects.isNull(contrato.getInversion().getFechaImporteConcedido().getCancelacion()) ||
        Objects.isNull(contrato.getInversion().getFechaImporteConcedido().getRefinanciacion())) {

        if(LocaleContextHolder.getLocale().getLanguage().equals("en"))
          throw new Exception("There is no date of granted amount nor date of formalization. It is not possible to create a simulation.");
        else if(LocaleContextHolder.getLocale().getLanguage().equals("es"))
          throw new Exception("No hay fecha de importe concedido ni de formalización. No se puede realizar una simulación.");
      }
      if(contrato.getIngresos().getFechaImpago().getAdjudicacion().before(contrato.getInversion().getFechaImporteConcedido().getAdjudicacion()) ||
        contrato.getIngresos().getFechaImpago().getDacion().before(contrato.getInversion().getFechaImporteConcedido().getDacion()) ||
        contrato.getIngresos().getFechaImpago().getVentaColateral().before(contrato.getInversion().getFechaImporteConcedido().getVentaColateral()) ||
        contrato.getIngresos().getFechaImpago().getCancelacion().before(contrato.getInversion().getFechaImporteConcedido().getRefinanciacion()) ||
        contrato.getIngresos().getFechaImpago().getRefinanciacion().before(contrato.getInversion().getFechaImporteConcedido().getRefinanciacion())) {
        if(LocaleContextHolder.getLocale().getLanguage().equals("en"))
          throw new Exception("The date of granted amount/formalization is later than the first default date. It has to be prior. It is not possible to create a simulation.");
        else if(LocaleContextHolder.getLocale().getLanguage().equals("es"))
          throw new Exception("La fecha de importe concedido/formalización es posterior a la de primer impago, debe ser anterior. No se puede realizar una simulación.");
      }
    }
  }

  public Date encontrarPrimeraFecha(List<SimulacionContratoDTO> contratos, String palanca) {
    if(palanca.equalsIgnoreCase("cancelacion")) {
      return contratos.stream().map(
        simulacionContratoDTO -> simulacionContratoDTO.getInversion().getFechaImporteConcedido().getCancelacion()).min(Date::compareTo).get();
    } else if(palanca.equalsIgnoreCase("dacion")) {
      return contratos.stream().map(
        simulacionContratoDTO -> simulacionContratoDTO.getInversion().getFechaImporteConcedido().getDacion()).min(Date::compareTo).get();
    } else if(palanca.equalsIgnoreCase("refinanciacion")) {
      return contratos.stream().map(
        simulacionContratoDTO -> simulacionContratoDTO.getInversion().getFechaImporteConcedido().getRefinanciacion()).min(Date::compareTo).get();
    }else if(palanca.equalsIgnoreCase("ventaColateral")) {
      return contratos.stream().map(
        simulacionContratoDTO -> simulacionContratoDTO.getInversion().getFechaImporteConcedido().getVentaColateral()).min(Date::compareTo).get();
    }else if(palanca.equalsIgnoreCase("adjudicacion")) {
      return contratos.stream().map(
        simulacionContratoDTO -> simulacionContratoDTO.getInversion().getFechaImporteConcedido().getAdjudicacion()).min(Date::compareTo).get();
    }
    return null;
  }

  public List<SimulacionGraficaFilaDTO> generarFilas() {
    List<SimulacionGraficaFilaDTO> lista = new ArrayList<>();
    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      //lista.add(new SimulacionGraficaFilaDTO("Debt","Current debt"));

      lista.add(new SimulacionGraficaFilaDTO("Investment","Granted amount"));
      lista.add(new SimulacionGraficaFilaDTO("Investment","Constitution expenses"));

      lista.add(new SimulacionGraficaFilaDTO("Income","Amount of cash deliveries"));
      lista.add(new SimulacionGraficaFilaDTO("Income","Non monetary deliveries"));
      lista.add(new SimulacionGraficaFilaDTO("Income","Percentage of adjudication"));
      lista.add(new SimulacionGraficaFilaDTO("Income","Endowments"));
      lista.add(new SimulacionGraficaFilaDTO("Income","Current assessment of asset"));

      lista.add(new SimulacionGraficaFilaDTO("Expense","ITP tax"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","AJD tax"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","IBI tax"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Community"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Facilities"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Asset capex"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Insurances"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Notary"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Register"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Taxation"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","CEE tax"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Procedure of right of first refusal"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Assumed charges"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Realtor commission"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Lawyer"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Proxy"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Court of justice tax"));
      lista.add(new SimulacionGraficaFilaDTO("Expense","Other associated expenses"));

    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      //lista.add(new SimulacionGraficaFilaDTO("Deuda","Deuda actual"));

      lista.add(new SimulacionGraficaFilaDTO("Inversión","Importe concedido"));
      lista.add(new SimulacionGraficaFilaDTO("Inversión","Gastos de constitución"));

      lista.add(new SimulacionGraficaFilaDTO("Ingreso","Importe entregas dinerarias"));
      lista.add(new SimulacionGraficaFilaDTO("Ingreso","Entregas no dinerarias"));
      lista.add(new SimulacionGraficaFilaDTO("Ingreso","Porcentaje adjudicación"));
      lista.add(new SimulacionGraficaFilaDTO("Ingreso","Dotaciones"));
      lista.add(new SimulacionGraficaFilaDTO("Ingreso","Valoración actual del activo"));

      lista.add(new SimulacionGraficaFilaDTO("Gasto","ITP"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","AJD"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","IBI"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Comunidad"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Facilities"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Capex en el activo"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Seguros"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Notaría"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Registro"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Tasación"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","CEE"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Trámites derecho de tanteo"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Gargas asumidas"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Comisión API"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Letrado"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Procurador"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Tasa judicial"));
      lista.add(new SimulacionGraficaFilaDTO("Gasto","Otros gastos asociados"));
    }


    return lista;
  }

  public SortedMap<Integer,List<SimulacionGraficaFilaDTO>> buscarFechaYMeter(SortedMap<Integer,List<SimulacionGraficaFilaDTO>> anyos, Date fecha, String nombre, Double importe) {
    if(Objects.isNull(fecha)) {
      return anyos;
    }
    ZoneId timeZone = ZoneId.systemDefault();
    LocalDate fechaLocal = Instant.ofEpochMilli(fecha.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    int anyo=fechaLocal.getYear();
    LocalDate fechaInicial = fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    LocalDate q0 = LocalDate.of(anyo,1,1);
    LocalDate q1 = LocalDate.of(anyo,3,31);
    LocalDate q2 = LocalDate.of(anyo,6,30);
    LocalDate q3 = LocalDate.of(anyo,9,30);
    LocalDate q4 = LocalDate.of(anyo,12,31);

    List<SimulacionGraficaFilaDTO> filas = anyos.get(anyo);
    if(filas != null) {
      for(SimulacionGraficaFilaDTO fila: filas) {
        if(fila.getNombre().equalsIgnoreCase(nombre)) {
          if(fechaInicial.equals(q0) && (fila.getNombre().equals("Importe concedido") || fila.getNombre().equals("Granted amount"))) {
            fila.setQ0(fila.getQ0()+importe);
          } else if(fechaLocal.equals(q0) || (fechaLocal.isAfter(q0)&&fechaLocal.isBefore(q1)) || fechaLocal.equals(q1)) {
            fila.setQ1(fila.getQ1()+importe);
          } else if((fechaLocal.isAfter(q1)&&fechaLocal.isBefore(q2)) || fechaLocal.equals(q2)) {
            fila.setQ2(fila.getQ2()+importe);
          } else if((fechaLocal.isAfter(q2)&&fechaLocal.isBefore(q3)) || fechaLocal.equals(q4)) {
            fila.setQ3(fila.getQ3()+importe);
          } else if((fechaLocal.isAfter(q3)&&fechaLocal.isBefore(q4)) || fechaLocal.equals(q4)) {
            fila.setQ4(fila.getQ4()+importe);
          }
        }
      }
    } //TODO revisar esto si se van a introducir mas años de los que hay
    /*else {
     List<SimulacionGraficaFilaDTO> filasNuevas = generarFilas();
     for(SimulacionGraficaFilaDTO fila: filasNuevas) {
       if(fila.getNombre().equalsIgnoreCase(nombre)) {
         if(fechaLocal.equals(q0) || (fechaLocal.isAfter(q0)&&fechaLocal.isBefore(q1)) || fechaLocal.equals(q1)) {
           fila.setQ1(fila.getQ1()+importe);
         } else if((fechaLocal.isAfter(q1)&&fechaLocal.isBefore(q2)) || fechaLocal.equals(q2)) {
           fila.setQ2(fila.getQ2()+importe);
         } else if((fechaLocal.isAfter(q2)&&fechaLocal.isBefore(q3)) || fechaLocal.equals(q4)) {
           fila.setQ3(fila.getQ3()+importe);
         } else if((fechaLocal.isAfter(q3)&&fechaLocal.isBefore(q4)) || fechaLocal.equals(q4)) {
           fila.setQ4(fila.getQ4()+importe);
         }
       }
     }
     anyos.put(anyo,filasNuevas);

   }*/

    return anyos;
  }


  private Map<String, Double> calcularCancelacion(SimulacionDTO dto, Date fechaSolucionAmistosa) {
    double importeT0Final = 0d, tipoInteres = 0d, importeT0TiFinalDouble = 0d, valorActivo = 0d;
    List<Double> importeT0TiFinalList = new ArrayList<>();
    VariableSimulacion variableInteres =
      variableSimulacionRepository.findByCodigo("interes").orElse(null);
    tipoInteres = Objects.nonNull(variableInteres.getValor()) ? variableInteres.getValor()/100 : 0d;

    for (SimulacionContratoDTO contrato : dto.getVariablesContratos()) {
      //todo aqui no se si esto tiene que ir negativo o no
      Double importeConcedido = contrato.getInversion().getImporteConcedido().getCancelacion();
      Double deudaActual = contrato.getDeuda().getDeudaActual().getCancelacion();

      Date fechaT0 = contrato.getInversion().getFechaImporteConcedido().getCancelacion();
      Double importeT0 =
        importeConcedido + contrato.getInversion().getGastosConstitucion().getCancelacion();

      Date fechaTi = contrato.getIngresos().getFechaImpago().getCancelacion();
      Integer trimestres = (int)(this.restarFechas(fechaTi, fechaT0) / 90);
      Double importeT0Ti =
        (importeConcedido - deudaActual) / trimestres;

      importeT0Final += importeT0;

      //cambiado: importe de entregas dinerarias es trimestral
      for(int i=0;i<trimestres;i++) {
        var temp =  importeT0Ti
          * Math.pow(
          (1 + tipoInteres), (this.restarFechas(fechaSolucionAmistosa, fechaTi) / 365));
        importeT0TiFinalList.add(temp);
        importeT0TiFinalDouble += temp;
      }

    }

    for (SimulacionBienDTO bien : dto.getVariablesBienes()) {
      valorActivo += bien.getIngresos().getValoracionActualActivo().getCancelacion();
    }

    Double tasas =
      dto.getVariablesSolucion().getGastos().getTasaJudicial().getCancelacion()
        + dto.getVariablesSolucion().getGastos().getLetrado().getCancelacion()
        + dto.getVariablesSolucion().getGastos().getProcurador().getCancelacion()
        + dto.getVariablesSolucion().getIngresos().getDotaciones().getCancelacion();

    importeT0TiFinalList.add(tasas);
    Map<String, Double> cancelacion = new HashMap<>();
    //cambiado: el van puesto sin interes porque solo tiene que estar en entregas dinerarias, ya calculado
    cancelacion.put(
      "van",
      this.calcularVan(
        0d, (importeT0Final * -1), List.of(importeT0TiFinalDouble, tasas+valorActivo)));
    //todo en el tir va el valor activo?
    cancelacion.put(
      "tir", Irr.irr(new double[] {(importeT0Final * -1), importeT0TiFinalDouble, tasas+valorActivo}) * 100);
    cancelacion.put("mfp",  (importeT0TiFinalDouble + tasas+valorActivo) / importeT0Final);

    return cancelacion;
  }

  public SimulacionGraficaPalancaDTO calcularGraficaCancelacion(SimulacionDTO dto, Date fechaSolucionAmistosa) {
    java.util.Date fechaT0, fechaT1;
    SimulacionGraficaPalancaDTO cancelacion = new SimulacionGraficaPalancaDTO();
    SortedMap<Integer,List<SimulacionGraficaFilaDTO>> anyos = new TreeMap<>();

    //sacar cuáles son los años para meterlos antes de nada
    fechaT0 = encontrarPrimeraFecha(dto.getVariablesContratos(),"cancelacion");
    LocalDate fechaT0Local = Instant.ofEpochMilli(fechaT0.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    int primerAnyo=fechaT0Local.getYear();
    fechaT1 = fechaSolucionAmistosa;
    LocalDate fechaT1Local = Instant.ofEpochMilli(fechaT1.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    Integer ultimoAnyo=fechaT1Local.getYear();

    //en el map que irá metido en el objeto que se devuelve, meto por cada año una lista donde luego se meterán todas las variables
    for(int f=primerAnyo; f<=ultimoAnyo; f++) {
      anyos.put(f,this.generarFilas());
    }

    String stringImporteConcedido="";
    String stringDeudaActual="";
    String stringGastosConstitucion="";
    String stringImporteEntregasDinerarias="";
    String stringTasaJudicial="";
    String stringLetrado="";
    String stringProcurador="";
    String stringDotaciones="";

    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      stringImporteConcedido = "Granted amount";
      stringDeudaActual = "Current debt";
      stringGastosConstitucion = "Constitution expenses";
      stringImporteEntregasDinerarias = "Amount of cash deliveries";
      stringTasaJudicial = "Court of justice tax";
      stringLetrado = "Lawyer";
      stringProcurador = "Proxy";
      stringDotaciones = "Endowments";

    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      stringImporteConcedido = "Importe concedido";
      stringDeudaActual = "Deuda actual";
      stringGastosConstitucion = "Gastos de constitución";
      stringImporteEntregasDinerarias = "Importe entregas dinerarias";
      stringTasaJudicial = "Tasa judicial";
      stringLetrado = "Letrado";
      stringProcurador = "Procurador";
      stringDotaciones = "Dotaciones";
    }

    Double valorActivo = 0d;
    VariableSimulacion variableInteres =
      variableSimulacionRepository.findByCodigo("interes").orElse(null);
    Double tipoInteres = Objects.nonNull(variableInteres.getValor()) ? variableInteres.getValor()/100 : 0d;

    for (SimulacionContratoDTO contrato : dto.getVariablesContratos()) {
      Date fechaT0Contrato = contrato.getInversion().getFechaImporteConcedido().getCancelacion();

      LocalDate fechaLocal = Instant.ofEpochMilli(fechaT0Contrato.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
      int anyo=fechaLocal.getYear();
      Date fechaImporteConcedido = Date.from(LocalDate.of(anyo,1,1).atStartOfDay(ZoneId.systemDefault()).toInstant());
      Double importeConcedido = contrato.getInversion().getImporteConcedido().getCancelacion();
      anyos = this.buscarFechaYMeter(anyos,fechaImporteConcedido,stringImporteConcedido,-importeConcedido);

      Double deudaActual = contrato.getDeuda().getDeudaActual().getCancelacion();
      //anyos = this.buscarFechaYMeter(anyos,fechaT0Contrato,stringDeudaActual,deudaActual);

      //Date fechaTiContrato = contrato.getIngresos().getFechaImpago().getCancelacion();

      Double gastosConstitucion = -(contrato.getInversion().getGastosConstitucion().getCancelacion());
      Date fechaGastosConstitucion = contrato.getInversion().getFechaGastosConstitucion().getCancelacion();
      anyos = this.buscarFechaYMeter(anyos,fechaGastosConstitucion,stringGastosConstitucion,gastosConstitucion);


      Date fechaTi = contrato.getIngresos().getFechaImpago().getCancelacion();
      Integer trimestres = (int)(this.restarFechas(fechaTi, fechaT0) / 90);
      Double importeT0Ti =
        ((importeConcedido - deudaActual) / trimestres)
          + ((((importeConcedido - deudaActual) / 12) * 3) * (tipoInteres / 4));

      for(int i=0;i<trimestres;i++) {
        var entregasDinerariasTrimestre =
          importeT0Ti
            * Math.pow(
            (1 + tipoInteres), (this.restarFechas(fechaSolucionAmistosa, fechaTi) / 365));
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaT0Contrato);
        cal.add(Calendar.DATE, (93*i));
        Date fecha = cal.getTime();
        anyos = this.buscarFechaYMeter(anyos, fecha, stringImporteEntregasDinerarias,entregasDinerariasTrimestre);
      }
    }

    for (SimulacionBienDTO bien : dto.getVariablesBienes()) {
      valorActivo += bien.getIngresos().getValoracionActualActivo().getCancelacion();
    }

    //Double entregasDinerarias = dto.getVariablesSolucion().getIngresos().getImporteEntregasDinerarias().getCancelacion();
    Date fechaEntregasDinerarias = dto.getVariablesSolucion().getIngresos().getFechasEntregasDinerarias().getCancelacion();
    anyos = this.buscarFechaYMeter(anyos, fechaEntregasDinerarias, stringImporteEntregasDinerarias,valorActivo);

    Double tasaJudicial = -(dto.getVariablesSolucion().getGastos().getTasaJudicial().getCancelacion());
    Date fechaTasaJudicial = dto.getVariablesSolucion().getGastos().getFechaTasaJudicial().getCancelacion();
    anyos = this.buscarFechaYMeter(anyos, fechaTasaJudicial, stringTasaJudicial,tasaJudicial);

    Double letrado = -(dto.getVariablesSolucion().getGastos().getLetrado().getCancelacion());
    Date fechaLetrado = dto.getVariablesSolucion().getGastos().getFechaLetrado().getCancelacion();
    anyos = this.buscarFechaYMeter(anyos, fechaLetrado, stringLetrado,letrado);

    Double procurador = -(dto.getVariablesSolucion().getGastos().getProcurador().getCancelacion());
    Date fechaProcurador = dto.getVariablesSolucion().getGastos().getFechaProcurador().getCancelacion();
    anyos = this.buscarFechaYMeter(anyos, fechaProcurador, stringProcurador,procurador);

    Double dotaciones = dto.getVariablesSolucion().getIngresos().getDotaciones().getCancelacion();
    Date fechaDotaciones = dto.getVariablesSolucion().getIngresos().getFechaRecuperacionDotaciones().getCancelacion();
    anyos = this.buscarFechaYMeter(anyos, fechaDotaciones, stringDotaciones,dotaciones);

    //lo meto dentro del objeto y lo devuelvo
    cancelacion.setAnyos(anyos);
    return cancelacion;
  }

  private Map<String, Double> calcularDacion(SimulacionDTO dto, Date fechaSolucionAmistosa) {
    Double importeT0Final = 0d, importeT0TiFinalDouble = 0d, importeTnFinal = 0d, tipoInteres = 0d, valorActivo = 0d;
    List<Double> importeT0TiFinalList = new ArrayList<>();
    VariableSimulacion variableInteres =
      variableSimulacionRepository.findByCodigo("interes").orElse(null);
    tipoInteres = Objects.nonNull(variableInteres.getValor()) ? variableInteres.getValor()/100 : 0d;
    Date fechaTi = null;

    for (SimulacionContratoDTO contrato : dto.getVariablesContratos()) {
      //todo no se si esto es negativo
      Double importeConcedido = contrato.getInversion().getImporteConcedido().getDacion();
      Double deudaActual = contrato.getDeuda().getDeudaActual().getDacion();

      Date fechaT0 = contrato.getInversion().getFechaImporteConcedido().getDacion();
      Double importeT0 =
        importeConcedido + contrato.getInversion().getGastosConstitucion().getDacion();

      fechaTi = contrato.getIngresos().getFechaImpago().getDacion();
      Integer trimestres = (int)(this.restarFechas(fechaTi, fechaT0) / 90);
      Double importeT0Ti =
        ((importeConcedido - deudaActual) / trimestres)
          + ((((importeConcedido - deudaActual) / 12) * 3) * (tipoInteres / 4));

      importeT0Final += importeT0;

      //cambiado:entregas dinerarias es trimestral
      for(int i=0;i<trimestres;i++) {
        var temp =
          importeT0Ti
            * Math.pow(
            (1 + tipoInteres), (this.restarFechas(fechaSolucionAmistosa, fechaTi) / 365));
        importeT0TiFinalList.add(temp);
        importeT0TiFinalDouble += temp;
      }
    }

    double importeT1Final =
      dto.getVariablesSolucion().getIngresos().getDotaciones().getDacion()
        - dto.getVariablesSolucion().getGastos().getTasaJudicial().getDacion()
        - dto.getVariablesSolucion().getGastos().getLetrado().getDacion()
        - dto.getVariablesSolucion().getGastos().getProcurador().getDacion()
      ;

    for (SimulacionBienDTO bien : dto.getVariablesBienes()) {
      double diff = this.restarFechas(fechaTi, fechaSolucionAmistosa);
      double diffT1Tn =
        this.restarFechas(
          fechaSolucionAmistosa, bien.getIngresos().getFechaVentaEstimada().getDacion());

      importeT1Final -=
        calcularPeriodicidad(
          bien.getGastos().getPeriodicidadIBI().getDacion(),
          bien.getGastos().getIBI().getDacion(),
          diff);
      importeTnFinal -=
        calcularPeriodicidad(
          bien.getGastos().getPeriodicidadIBI().getDacion(),
          bien.getGastos().getIBI().getDacion(),
          diffT1Tn);
      importeT1Final -=
        calcularPeriodicidad(
          bien.getGastos().getPeriodicidadComunidad().getDacion(),
          bien.getGastos().getComunidad().getDacion(),
          diff);
      importeTnFinal -=
        calcularPeriodicidad(
          bien.getGastos().getPeriodicidadComunidad().getDacion(),
          bien.getGastos().getComunidad().getDacion(),
          diffT1Tn);

      importeT1Final -= bien.getGastos().getFacilities().getDacion();
      importeT1Final -= bien.getGastos().getCapexEnElActivo().getDacion();
      importeT1Final -= bien.getGastos().getTasacion().getDacion();
      importeT1Final -= bien.getGastos().getRegistro().getDacion();
      importeT1Final -= bien.getGastos().getCargasAsumidas().getDacion();
      importeT1Final -= bien.getGastos().getNotaria().getDacion();
      importeT1Final -= bien.getGastos().getAJD().getDacion();

      valorActivo += bien.getIngresos().getValoracionActualActivo().getDacion();
      importeTnFinal -= bien.getGastos().getCEE().getDacion();
      importeTnFinal -= bien.getGastos().getTramitesDerechoTanteo().getDacion();
      importeTnFinal -= bien.getGastos().getComisionApi().getDacion();
    }

    Map<String, Double> dacion = new HashMap<>();
    importeT0TiFinalList.add(importeT1Final);
    importeT0TiFinalList.add(importeTnFinal);
    //cambiado: el van puesto sin interes porque solo tiene que estar en entregas dinerarias, ya calculado

    dacion.put(
      "van",
      this.calcularVan(
        0d, (importeT0Final * -1), List.of(importeT0TiFinalDouble, importeT1Final, importeTnFinal + valorActivo)));
    dacion.put(
      "tir", Irr.irr(new double[] {(importeT0Final * -1), importeT0TiFinalDouble, importeT1Final, importeTnFinal + valorActivo}) * 100);
    dacion.put("mfp",  (importeT0TiFinalDouble + importeT1Final + importeTnFinal + valorActivo) / importeT0Final);
    /*dacion.put(
        "van",
        this.calcularVan(
            0d,
            (importeT0Final * -1), importeT0TiFinalList));
    dacion.put(
        "tir",
        Irr.irr(
                new double[] {
                  (importeT0Final * -1), importeT0TiFinalDouble, importeT1Final, importeTnFinal+valorActivo
                })
            * 100);
    dacion.put("mfp", importeT0Final / (importeT0TiFinalDouble + importeT1Final + importeTnFinal + valorActivo));*/

    return dacion;
  }

  public SimulacionGraficaPalancaDTO calcularGraficaDacion(SimulacionDTO dto, Date fechaSolucionAmistosa) {
    java.util.Date fechaT0, fechaTi = null, fechaT1;
    SimulacionGraficaPalancaDTO dacion = new SimulacionGraficaPalancaDTO();
    SortedMap<Integer,List<SimulacionGraficaFilaDTO>> anyos = new TreeMap<>();

    //sacar cuáles son los años para meterlos antes de nada
    fechaT0 = encontrarPrimeraFecha(dto.getVariablesContratos(),"dacion");
    LocalDate fechaT0Local = Instant.ofEpochMilli(fechaT0.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    int primerAnyo=fechaT0Local.getYear();
    fechaT1 = fechaSolucionAmistosa;
    LocalDate fechaT1Local = Instant.ofEpochMilli(fechaT1.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    Integer ultimoAnyo=fechaT1Local.getYear();


    //en el map que irá metido en el objeto que se devuelve, meto por cada año una lista donde luego se meterán todas las variables
    for(int f=primerAnyo; f<=ultimoAnyo; f++) {
      anyos.put(f,this.generarFilas());
    }

    String stringImporteConcedido="";
    String stringDeudaActual="";
    String stringGastosConstitucion="";
    String stringImporteEntregasDinerarias="";
    String stringTasaJudicial="";
    String stringLetrado="";
    String stringProcurador="";
    String stringDotaciones="";
    String stringIBI="";
    String stringComunidad="";
    String stringFacilities="";
    String stringCapex="";
    String stringTasacion="";
    String stringRegistro="";
    String stringCargasAsumidas="";
    String stringNotaria="";
    String stringAjd="";
    String stringValoracionActualActivo="";
    String stringCee="";
    String stringTanteo="";
    String stringComisionApi="";

    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      stringImporteConcedido = "Granted amount";
      stringDeudaActual = "Current debt";
      stringGastosConstitucion = "Constitution expenses";
      stringImporteEntregasDinerarias = "Amount of cash deliveries";
      stringTasaJudicial = "Court of justice tax";
      stringLetrado = "Lawyer";
      stringProcurador = "Proxy";
      stringDotaciones = "Endowments";
      stringIBI ="IBI tax";
      stringComunidad= "Community";
      stringFacilities= "Facilities";
      stringCapex="Asset capex";
      stringTasacion="Taxation";
      stringRegistro="Register";
      stringCargasAsumidas="Assumed charges";
      stringNotaria="Notary";
      stringAjd="AJD tax";
      stringValoracionActualActivo="Current assessment of asset";
      stringCee="CEE tax";
      stringTanteo="Procedure of right of first refusal";
      stringComisionApi="Realtor commission";

    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      stringImporteConcedido = "Importe concedido";
      stringDeudaActual = "Deuda actual";
      stringGastosConstitucion = "Gastos de constitución";
      stringImporteEntregasDinerarias = "Importe entregas dinerarias";
      stringTasaJudicial = "Tasa judicial";
      stringLetrado = "Letrado";
      stringProcurador = "Procurador";
      stringDotaciones = "Dotaciones";
      stringIBI ="IBI";
      stringComunidad= "Comunidad";
      stringFacilities= "Facilities";
      stringCapex="Capex en el activo";
      stringTasacion="Tasación";
      stringRegistro="Registro";
      stringCargasAsumidas="Cargas asumidas";
      stringNotaria="Notaría";
      stringAjd="AJD";
      stringValoracionActualActivo="Valoración actual del activo";
      stringCee="CEE";
      stringTanteo="Trámites derecho de tanteo";
      stringComisionApi="Comisión API";
    }

    //Double entregasDinerarias = 0d;
    VariableSimulacion variableInteres =
      variableSimulacionRepository.findByCodigo("interes").orElse(null);
    Double tipoInteres = Objects.nonNull(variableInteres.getValor()) ? variableInteres.getValor()/100 : 0d;

    for (SimulacionContratoDTO contrato : dto.getVariablesContratos()) {
      Date fechaT0Contrato = contrato.getInversion().getFechaImporteConcedido().getDacion();

      Double importeConcedido = contrato.getInversion().getImporteConcedido().getDacion();
      LocalDate fechaLocal = Instant.ofEpochMilli(fechaT0Contrato.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
      int anyo=fechaLocal.getYear();
      Date fechaImporteConcedido = Date.from(LocalDate.of(anyo,1,1).atStartOfDay(ZoneId.systemDefault()).toInstant());
      anyos = this.buscarFechaYMeter(anyos,fechaImporteConcedido,stringImporteConcedido,-importeConcedido);

      Double deudaActual = contrato.getDeuda().getDeudaActual().getDacion();
      //anyos = this.buscarFechaYMeter(anyos,fechaT0Contrato,stringDeudaActual,deudaActual);

      Date fechaTiContrato = contrato.getIngresos().getFechaImpago().getDacion();
      if(Objects.isNull(fechaTi) || fechaTiContrato.before(fechaTi))
        fechaTi = fechaTiContrato;

      Double gastosConstitucion = -(contrato.getInversion().getGastosConstitucion().getDacion());
      Date fechaGastosConstitucion = contrato.getInversion().getFechaGastosConstitucion().getDacion();
      anyos = this.buscarFechaYMeter(anyos,fechaGastosConstitucion,stringGastosConstitucion,gastosConstitucion);

      Integer trimestres = (int)(this.restarFechas(fechaTi, fechaT0) / 90);
      Double importeT0Ti =
        ((importeConcedido - deudaActual) / trimestres)
          + ((((importeConcedido - deudaActual) / 12) * 3) * (tipoInteres / 4));
      for(int i=0;i<trimestres;i++) {
        var entregasDinerariasTrimestre =
          importeT0Ti
            * Math.pow(
            (1 + tipoInteres), (this.restarFechas(fechaSolucionAmistosa, fechaTi) / 365));
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaT0Contrato);
        cal.add(Calendar.DATE, (93*i));
        Date fecha = cal.getTime();
        anyos = this.buscarFechaYMeter(anyos, fecha, stringImporteEntregasDinerarias,entregasDinerariasTrimestre);
      }
    }

    //Double entregasDinerarias = dto.getVariablesSolucion().getIngresos().getImporteEntregasDinerarias().getDacion();
    //Date fechaEntregasDinerarias = dto.getVariablesSolucion().getIngresos().getFechasEntregasDinerarias().getDacion();
    //anyos = this.buscarFechaYMeter(anyos, fechaEntregasDinerarias, stringImporteEntregasDinerarias,entregasDinerarias);

    Double tasaJudicial = -(dto.getVariablesSolucion().getGastos().getTasaJudicial().getDacion());
    Date fechaTasaJudicial = dto.getVariablesSolucion().getGastos().getFechaTasaJudicial().getDacion();
    anyos = this.buscarFechaYMeter(anyos, fechaTasaJudicial, stringTasaJudicial,tasaJudicial);

    Double letrado = -(dto.getVariablesSolucion().getGastos().getLetrado().getDacion());
    Date fechaLetrado = dto.getVariablesSolucion().getGastos().getFechaLetrado().getDacion();
    anyos = this.buscarFechaYMeter(anyos, fechaLetrado, stringLetrado,letrado);

    Double procurador = -(dto.getVariablesSolucion().getGastos().getProcurador().getDacion());
    Date fechaProcurador = dto.getVariablesSolucion().getGastos().getFechaProcurador().getDacion();
    anyos = this.buscarFechaYMeter(anyos, fechaProcurador, stringProcurador,procurador);

    Double dotaciones = dto.getVariablesSolucion().getIngresos().getDotaciones().getDacion();
    Date fechaDotaciones = dto.getVariablesSolucion().getIngresos().getFechaRecuperacionDotaciones().getDacion();
    anyos = this.buscarFechaYMeter(anyos, fechaDotaciones, stringDotaciones,dotaciones);


    for (SimulacionBienDTO bien : dto.getVariablesBienes()) {
      double diff = this.restarFechas(fechaTi, fechaSolucionAmistosa);
      Date fechaTn = bien.getIngresos().getFechaVentaEstimada().getDacion();
      double diffT1Tn =
        this.restarFechas(
          fechaSolucionAmistosa, bien.getIngresos().getFechaVentaEstimada().getDacion());

      Double ibiT1 =
        -(calcularPeriodicidad(
          bien.getGastos().getPeriodicidadIBI().getDacion(),
          bien.getGastos().getIBI().getDacion(),
          diff));
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringIBI, ibiT1);

      Double ibiTn =
        -(calcularPeriodicidad(
          bien.getGastos().getPeriodicidadIBI().getDacion(),
          bien.getGastos().getIBI().getDacion(),
          diffT1Tn));
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringIBI, ibiTn);

      Double comunidadT1 =
        -(calcularPeriodicidad(
          bien.getGastos().getPeriodicidadComunidad().getDacion(),
          bien.getGastos().getComunidad().getDacion(),
          diff));
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringComunidad, comunidadT1);

      Double comunidadTn=
        -(calcularPeriodicidad(
          bien.getGastos().getPeriodicidadComunidad().getDacion(),
          bien.getGastos().getComunidad().getDacion(),
          diffT1Tn));
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringComunidad, comunidadTn);

      Double facilities = -(bien.getGastos().getFacilities().getDacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringFacilities, facilities);

      Double capex = -(bien.getGastos().getCapexEnElActivo().getDacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringCapex, capex);

      Double tasacion = -(bien.getGastos().getTasacion().getDacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringTasacion, tasacion);

      Double registro = -(bien.getGastos().getRegistro().getDacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringRegistro, registro);

      Double cargas = -(bien.getGastos().getCargasAsumidas().getDacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringCargasAsumidas, cargas);

      Double notaria = -(bien.getGastos().getNotaria().getDacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringNotaria, notaria);

      Double ajd = -(bien.getGastos().getAJD().getDacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringAjd, ajd);

      Double valoracionActualActivo = bien.getIngresos().getValoracionActualActivo().getDacion();
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringValoracionActualActivo, valoracionActualActivo);

      Double cee = -(bien.getGastos().getCEE().getDacion());
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringCee, cee);

      Double tramitesDerechoTanteo = -(bien.getGastos().getTramitesDerechoTanteo().getDacion());
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringTanteo, tramitesDerechoTanteo);

      Double comisionApi = -(bien.getGastos().getComisionApi().getDacion());
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringComisionApi, comisionApi);
    }

    //lo meto dentro del objeto y lo devuelvo
    dacion.setAnyos(anyos);
    return dacion;
  }

  private Map<String, Double> calcularVentaColateral(
    SimulacionDTO dto, Date fechaSolucionAmistosa) {
    Double importeT0Final = 0d, tipoInteres = 0d, importeT0TiFinalDouble = 0d, valorActivo = 0d;
    List<Double> importeT0TiFinalList = new ArrayList<>();
    VariableSimulacion variableInteres =
      variableSimulacionRepository.findByCodigo("interes").orElse(null);
    tipoInteres = Objects.nonNull(variableInteres.getValor()) ? variableInteres.getValor()/100 : 0d;

    for (SimulacionContratoDTO contrato : dto.getVariablesContratos()) {
      //todo no se s i va negativo
      Double importeConcedido = contrato.getInversion().getImporteConcedido().getVentaColateral();
      Double deudaActual = contrato.getDeuda().getDeudaActual().getVentaColateral();

      Date fechaT0 = contrato.getInversion().getFechaImporteConcedido().getVentaColateral();
      Double importeT0 =
        importeConcedido + contrato.getInversion().getGastosConstitucion().getVentaColateral();

      Date fechaTi = contrato.getIngresos().getFechaImpago().getVentaColateral();
      Integer trimestres = (int)(this.restarFechas(fechaTi, fechaT0) / 90);
      Double importeT0Ti =
        ((importeConcedido - deudaActual) / trimestres);

      importeT0Final += importeT0;

      //cambiado:entregas dinerarias es trimestral
      for(int i=0;i<trimestres;i++) {
        var temp =
          importeT0Ti
            * Math.pow(
            (1 + tipoInteres), (this.restarFechas(fechaSolucionAmistosa, fechaTi) / 365));
        importeT0TiFinalList.add(temp);
        importeT0TiFinalDouble += temp;
      }
    }

    Double tasas =
      dto.getVariablesSolucion().getIngresos().getDotaciones().getVentaColateral()
        - dto.getVariablesSolucion().getGastos().getTasaJudicial().getVentaColateral()
        - dto.getVariablesSolucion().getGastos().getLetrado().getVentaColateral()
        - dto.getVariablesSolucion().getGastos().getProcurador().getVentaColateral()
      ;

    for (SimulacionBienDTO bien : dto.getVariablesBienes()) {
      valorActivo += bien.getIngresos().getValoracionActualActivo().getCancelacion();
      //tasas += bien.getIngresos().getValoracionActualActivo().getVentaColateral();
      tasas -= bien.getGastos().getCEE().getVentaColateral();
      tasas -= bien.getGastos().getTramitesDerechoTanteo().getVentaColateral();
      tasas -= bien.getGastos().getComisionApi().getVentaColateral();
    }
    importeT0TiFinalList.add(tasas);

    Map<String, Double> ventaColateral = new HashMap<>();
    //cambiado: el van puesto sin interes porque solo tiene que estar en entregas dinerarias, ya calculado
    ventaColateral.put(
      "van",
      this.calcularVan(
        0d, (importeT0Final * -1), List.of(importeT0TiFinalDouble, tasas+valorActivo)));
    ventaColateral.put(
      "tir", Irr.irr(new double[] {(importeT0Final * -1), importeT0TiFinalDouble, tasas+valorActivo}) * 100);
    ventaColateral.put("mfp",  (importeT0TiFinalDouble + tasas+valorActivo) / importeT0Final);

    /*ventaColateral.put(
        "van",
        this.calcularVan(
            0d, (importeT0Final * -1), importeT0TiFinalList));
    ventaColateral.put(
        "tir", Irr.irr(new double[] {(importeT0Final * -1), importeT0TiFinalDouble, tasas+valorActivo}) * 100);
    ventaColateral.put("mfp", importeT0Final / (importeT0TiFinalDouble + tasas + valorActivo));*/

    return ventaColateral;
  }

  public SimulacionGraficaPalancaDTO calcularGraficaVentaColateral(SimulacionDTO dto, Date fechaSolucionAmistosa) {
    java.util.Date fechaT0, fechaTi = null, fechaT1;
    SimulacionGraficaPalancaDTO ventaColateral = new SimulacionGraficaPalancaDTO();
    SortedMap<Integer,List<SimulacionGraficaFilaDTO>> anyos = new TreeMap<>();

    //sacar cuáles son los años para meterlos antes de nada
    fechaT0 = encontrarPrimeraFecha(dto.getVariablesContratos(),"ventaColateral");
    LocalDate fechaT0Local = Instant.ofEpochMilli(fechaT0.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    int primerAnyo=fechaT0Local.getYear();
    fechaT1 = fechaSolucionAmistosa;
    LocalDate fechaT1Local = Instant.ofEpochMilli(fechaT1.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    Integer ultimoAnyo=fechaT1Local.getYear();


    //en el map que irá metido en el objeto que se devuelve, meto por cada año una lista donde luego se meterán todas las variables
    for(int f=primerAnyo; f<=ultimoAnyo; f++) {
      anyos.put(f,this.generarFilas());
    }

    String stringImporteConcedido="";
    String stringDeudaActual="";
    String stringGastosConstitucion="";
    String stringImporteEntregasDinerarias="";
    String stringTasaJudicial="";
    String stringLetrado="";
    String stringProcurador="";
    String stringDotaciones="";
    String stringCee="";
    String stringTanteo="";
    String stringComisionApi="";
    String stringValoracionActualActivo="";

    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      stringImporteConcedido = "Granted amount";
      stringDeudaActual = "Current debt";
      stringGastosConstitucion = "Constitution expenses";
      stringImporteEntregasDinerarias = "Amount of cash deliveries";
      stringTasaJudicial = "Court of justice tax";
      stringLetrado = "Lawyer";
      stringProcurador = "Proxy";
      stringDotaciones = "Endowments";
      stringCee="CEE tax";
      stringTanteo="Procedure of right of first refusal";
      stringComisionApi="Realtor commission";
      stringValoracionActualActivo="Current assessment of asset";

    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      stringImporteConcedido = "Importe concedido";
      stringDeudaActual = "Deuda actual";
      stringGastosConstitucion = "Gastos de constitución";
      stringImporteEntregasDinerarias = "Importe entregas dinerarias";
      stringTasaJudicial = "Tasa judicial";
      stringLetrado = "Letrado";
      stringProcurador = "Procurador";
      stringDotaciones = "Dotaciones";
      stringCee="CEE";
      stringTanteo="Trámites derecho de tanteo";
      stringComisionApi="Comisión API";
      stringValoracionActualActivo="Valoración actual del activo";
    }

    Double entregasDinerarias = 0d;
    VariableSimulacion variableInteres =
      variableSimulacionRepository.findByCodigo("interes").orElse(null);
    Double tipoInteres = Objects.nonNull(variableInteres.getValor()) ? variableInteres.getValor()/100 : 0d;

    for (SimulacionContratoDTO contrato : dto.getVariablesContratos()) {
      Date fechaT0Contrato = contrato.getInversion().getFechaImporteConcedido().getVentaColateral();

      Double importeConcedido = contrato.getInversion().getImporteConcedido().getVentaColateral();
      LocalDate fechaLocal = Instant.ofEpochMilli(fechaT0Contrato.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
      int anyo=fechaLocal.getYear();
      Date fechaImporteConcedido = Date.from(LocalDate.of(anyo,1,1).atStartOfDay(ZoneId.systemDefault()).toInstant());
      anyos = this.buscarFechaYMeter(anyos,fechaImporteConcedido,stringImporteConcedido,-importeConcedido);

      Double deudaActual = contrato.getDeuda().getDeudaActual().getVentaColateral();
      //anyos = this.buscarFechaYMeter(anyos,fechaT0Contrato,stringDeudaActual,deudaActual);

      Date fechaTiContrato = contrato.getIngresos().getFechaImpago().getVentaColateral();
      if(Objects.isNull(fechaTi) || fechaTiContrato.before(fechaTi))
        fechaTi = fechaTiContrato;

      Double gastosConstitucion = -(contrato.getInversion().getGastosConstitucion().getVentaColateral());
      Date fechaGastosConstitucion = contrato.getInversion().getFechaGastosConstitucion().getVentaColateral();
      anyos = this.buscarFechaYMeter(anyos,fechaGastosConstitucion,stringGastosConstitucion,gastosConstitucion);

      Integer trimestres = (int)(this.restarFechas(fechaTi, fechaT0) / 90);
      Double importeT0Ti =
        ((importeConcedido - deudaActual) / trimestres)
          + ((((importeConcedido - deudaActual) / 12) * 3) * (tipoInteres / 4));
      for(int i=0;i<trimestres;i++) {
        var entregasDinerariasTrimestre =
          importeT0Ti
            * Math.pow(
            (1 + tipoInteres), (this.restarFechas(fechaSolucionAmistosa, fechaTi) / 365));
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaT0Contrato);
        cal.add(Calendar.DATE, (93*i));
        Date fecha = cal.getTime();
        anyos = this.buscarFechaYMeter(anyos, fecha, stringImporteEntregasDinerarias,entregasDinerariasTrimestre);
      }
    }

    //Double entregasDinerarias = dto.getVariablesSolucion().getIngresos().getImporteEntregasDinerarias().getVentaColateral();
    //Date fechaEntregasDinerarias = dto.getVariablesSolucion().getIngresos().getFechasEntregasDinerarias().getVentaColateral();
    //anyos = this.buscarFechaYMeter(anyos, fechaEntregasDinerarias, stringImporteEntregasDinerarias,entregasDinerarias);

    Double tasaJudicial = -(dto.getVariablesSolucion().getGastos().getTasaJudicial().getVentaColateral());
    Date fechaTasaJudicial = dto.getVariablesSolucion().getGastos().getFechaTasaJudicial().getVentaColateral();
    anyos = this.buscarFechaYMeter(anyos, fechaTasaJudicial, stringTasaJudicial,tasaJudicial);

    Double letrado = -(dto.getVariablesSolucion().getGastos().getLetrado().getVentaColateral());
    Date fechaLetrado = dto.getVariablesSolucion().getGastos().getFechaLetrado().getVentaColateral();
    anyos = this.buscarFechaYMeter(anyos, fechaLetrado, stringLetrado,letrado);

    Double procurador = -(dto.getVariablesSolucion().getGastos().getProcurador().getVentaColateral());
    Date fechaProcurador = dto.getVariablesSolucion().getGastos().getFechaProcurador().getVentaColateral();
    anyos = this.buscarFechaYMeter(anyos, fechaProcurador, stringProcurador,procurador);

    Double dotaciones = dto.getVariablesSolucion().getIngresos().getDotaciones().getVentaColateral();
    Date fechaDotaciones = dto.getVariablesSolucion().getIngresos().getFechaRecuperacionDotaciones().getVentaColateral();
    anyos = this.buscarFechaYMeter(anyos, fechaDotaciones, stringDotaciones,dotaciones);


    for (SimulacionBienDTO bien : dto.getVariablesBienes()) {
      Date fechaTn = bien.getIngresos().getFechaVentaEstimada().getVentaColateral();

      Double valoracionActualActivo = bien.getIngresos().getValoracionActualActivo().getVentaColateral();
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringValoracionActualActivo, valoracionActualActivo);

      Double cee = -(bien.getGastos().getCEE().getVentaColateral());
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringCee, cee);

      Double tramitesDerechoTanteo = -(bien.getGastos().getTramitesDerechoTanteo().getVentaColateral());
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringTanteo, tramitesDerechoTanteo);

      Double comisionApi = -(bien.getGastos().getComisionApi().getVentaColateral());
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringComisionApi, comisionApi);
    }

    //lo meto dentro del objeto y lo devuelvo
    ventaColateral.setAnyos(anyos);
    return ventaColateral;
  }

  private Map<String, Double> calcularAdjudicacion(SimulacionDTO dto, Date fechaSolucionAmistosa) {
    Double importeT0Final = 0d,
      importeT0TiFinalDouble = 0d,
      importeT1TnFinal = 0d,
      importeTnFinal = 0d,
      tipoInteres = 0d,
      valorActivo = 0d;
    Date fechaTi = null;
    List<Double> importeT0TiFinalList = new ArrayList<>();
    VariableSimulacion variableInteres =
      variableSimulacionRepository.findByCodigo("interes").orElse(null);
    tipoInteres = Objects.nonNull(variableInteres.getValor()) ? variableInteres.getValor()/100 : 0d;

    for (SimulacionContratoDTO contrato : dto.getVariablesContratos()) {
      //todo no se si esto tiene que ir negativo
      Double importeConcedido = contrato.getInversion().getImporteConcedido().getAdjudicacion();
      Double deudaActual = contrato.getDeuda().getDeudaActual().getAdjudicacion();

      Date fechaT0 = contrato.getInversion().getFechaImporteConcedido().getAdjudicacion();
      Double importeT0 =
        importeConcedido + contrato.getInversion().getGastosConstitucion().getAdjudicacion();

      fechaTi = contrato.getIngresos().getFechaImpago().getAdjudicacion();
      Integer trimestres = (int)(this.restarFechas(fechaTi, fechaT0) / 90);
      Double importeT0Ti =
        ((importeConcedido - deudaActual) / trimestres);

      importeT0Final += importeT0;

      //cambiado: importe entregas dinerarias es trimestral
      for(int i=0;i<trimestres;i++) {
        var temp =
          importeT0Ti
            * Math.pow(
            (1 + tipoInteres), (this.restarFechas(fechaSolucionAmistosa, fechaTi) / 365));
        importeT0TiFinalList.add(temp);
        importeT0TiFinalDouble += temp;
      }
    }

    Double importeT1Final =
      dto.getVariablesSolucion().getGastos().getTasaJudicial().getAdjudicacion()
        - dto.getVariablesSolucion().getGastos().getLetrado().getAdjudicacion()
        - dto.getVariablesSolucion().getGastos().getProcurador().getAdjudicacion();

    for (SimulacionBienDTO bien : dto.getVariablesBienes()) {
      Date fechaAdjudicacion = bien.getIngresos().getFechaAdjudicacion().getAdjudicacion();
      double diff = this.restarFechas(fechaTi, fechaAdjudicacion);
      double diffT1Tn =
        this.restarFechas(
          fechaAdjudicacion, bien.getIngresos().getFechaVentaEstimada().getAdjudicacion());

      importeT1Final -=
        calcularPeriodicidad(
          bien.getGastos().getPeriodicidadIBI().getAdjudicacion(),
          bien.getGastos().getIBI().getAdjudicacion(),
          diff);
      importeT1TnFinal -=
        calcularPeriodicidad(
          bien.getGastos().getPeriodicidadIBI().getAdjudicacion(),
          bien.getGastos().getIBI().getAdjudicacion(),
          diffT1Tn);
      importeT1Final -=
        calcularPeriodicidad(
          bien.getGastos().getPeriodicidadComunidad().getAdjudicacion(),
          bien.getGastos().getComunidad().getAdjudicacion(),
          diff);
      importeT1TnFinal -=
        calcularPeriodicidad(
          bien.getGastos().getPeriodicidadComunidad().getAdjudicacion(),
          bien.getGastos().getComunidad().getAdjudicacion(),
          diffT1Tn);

      importeT1Final -= bien.getGastos().getFacilities().getAdjudicacion();
      importeT1Final -= bien.getGastos().getCapexEnElActivo().getAdjudicacion();

      valorActivo += bien.getIngresos().getValoracionActualActivo().getCancelacion();
      //importeTnFinal += bien.getIngresos().getValoracionActualActivo().getAdjudicacion();
      importeTnFinal -= bien.getGastos().getCEE().getAdjudicacion();
      importeTnFinal -= bien.getGastos().getTramitesDerechoTanteo().getAdjudicacion();
      importeTnFinal -= bien.getGastos().getComisionApi().getAdjudicacion();
    }
    importeT0TiFinalList.add(importeT1Final);
    importeT0TiFinalList.add(importeT1TnFinal);
    importeT0TiFinalList.add(importeTnFinal);

    Map<String, Double> adjudicacion = new HashMap<>();
    //cambiado: el van puesto sin interes porque solo tiene que estar en entregas dinerarias, ya calculado
    adjudicacion.put(
      "van",
      this.calcularVan(
        0d, (importeT0Final * -1), List.of(importeT0TiFinalDouble, importeT1Final, importeT1TnFinal, importeTnFinal + valorActivo)));
    adjudicacion.put(
      "tir", Irr.irr(new double[] {(importeT0Final * -1), importeT0TiFinalDouble, importeT1Final, importeT1TnFinal, importeTnFinal + valorActivo}) * 100);
    adjudicacion.put("mfp",  (importeT0TiFinalDouble + importeT1Final + importeT1TnFinal + importeTnFinal + valorActivo) / importeT0Final);
    /*adjudicacion.put(
        "van",
        this.calcularVan(
            0d,
            (importeT0Final * -1),importeT0TiFinalList));
    adjudicacion.put(
        "tir",
        Irr.irr(
                new double[] {
                  (importeT0Final * -1),
                  importeT0TiFinalDouble,
                  importeT1Final,
                  importeT1TnFinal,
                  importeTnFinal+ valorActivo
                })
            * 100);
    adjudicacion.put(
        "mfp",
        importeT0Final / (importeT0TiFinalDouble + importeT1Final + importeT1TnFinal + importeTnFinal+ valorActivo));*/

    return adjudicacion;
  }

  public SimulacionGraficaPalancaDTO calcularGraficaAdjudicacion(SimulacionDTO dto, Date fechaSolucionAmistosa) {
    java.util.Date fechaT0, fechaTi = null, fechaT1;
    SimulacionGraficaPalancaDTO adjudicacion = new SimulacionGraficaPalancaDTO();
    SortedMap<Integer,List<SimulacionGraficaFilaDTO>> anyos = new TreeMap<>();

    //sacar cuáles son los años para meterlos antes de nada
    fechaT0 = encontrarPrimeraFecha(dto.getVariablesContratos(),"adjudicacion");
    LocalDate fechaT0Local = Instant.ofEpochMilli(fechaT0.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    int primerAnyo=fechaT0Local.getYear();
    fechaT1 = fechaSolucionAmistosa;
    LocalDate fechaT1Local = Instant.ofEpochMilli(fechaT1.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    Integer ultimoAnyo=fechaT1Local.getYear();


    //en el map que irá metido en el objeto que se devuelve, meto por cada año una lista donde luego se meterán todas las variables
    for(int f=primerAnyo; f<=ultimoAnyo; f++) {
      anyos.put(f,this.generarFilas());
    }

    String stringImporteConcedido="";
    String stringDeudaActual="";
    String stringGastosConstitucion="";
    String stringTasaJudicial="";
    String stringLetrado="";
    String stringProcurador="";
    String stringIBI="";
    String stringComunidad="";
    String stringFacilities="";
    String stringCapex="";
    String stringValoracionActualActivo="";
    String stringCee="";
    String stringTanteo="";
    String stringComisionApi="";
    String stringImporteEntregasDinerarias="";

    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      stringImporteConcedido = "Granted amount";
      stringDeudaActual = "Current debt";
      stringGastosConstitucion = "Constitution expenses";
      stringTasaJudicial = "Court of justice tax";
      stringLetrado = "Lawyer";
      stringProcurador = "Proxy";
      stringIBI ="IBI tax";
      stringComunidad= "Community";
      stringFacilities= "Facilities";
      stringCapex="Asset capex";
      stringValoracionActualActivo="Current assessment of asset";
      stringCee="CEE tax";
      stringTanteo="Procedure of right of first refusal";
      stringComisionApi="Realtor commission";
      stringImporteEntregasDinerarias="Amount of cash deliveries";

    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      stringImporteConcedido = "Importe concedido";
      stringDeudaActual = "Deuda actual";
      stringGastosConstitucion = "Gastos de constitución";
      stringTasaJudicial = "Tasa judicial";
      stringLetrado = "Letrado";
      stringProcurador = "Procurador";
      stringIBI ="IBI";
      stringComunidad= "Comunidad";
      stringFacilities= "Facilities";
      stringCapex="Capex en el activo";
      stringValoracionActualActivo="Valoración actual del activo";
      stringCee="CEE";
      stringTanteo="Trámites derecho de tanteo";
      stringComisionApi="Comisión API";
      stringImporteEntregasDinerarias="Importe entregas dinerarias";
    }

    Double entregasDinerarias = 0d;
    VariableSimulacion variableInteres =
      variableSimulacionRepository.findByCodigo("interes").orElse(null);
    Double tipoInteres = Objects.nonNull(variableInteres.getValor()) ? variableInteres.getValor()/100 : 0d;



    for (SimulacionContratoDTO contrato : dto.getVariablesContratos()) {
      Date fechaT0Contrato = contrato.getInversion().getFechaImporteConcedido().getAdjudicacion();

      Double importeConcedido = contrato.getInversion().getImporteConcedido().getAdjudicacion();
      LocalDate fechaLocal = Instant.ofEpochMilli(fechaT0Contrato.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
      int anyo=fechaLocal.getYear();
      Date fechaImporteConcedido = Date.from(LocalDate.of(anyo,1,1).atStartOfDay(ZoneId.systemDefault()).toInstant());
      anyos = this.buscarFechaYMeter(anyos,fechaImporteConcedido,stringImporteConcedido,-importeConcedido);

      Double deudaActual = contrato.getDeuda().getDeudaActual().getAdjudicacion();
      //anyos = this.buscarFechaYMeter(anyos,fechaT0Contrato,stringDeudaActual,deudaActual);

      Date fechaTiContrato = contrato.getIngresos().getFechaImpago().getAdjudicacion();
      if(Objects.isNull(fechaTi) || fechaTiContrato.before(fechaTi))
        fechaTi = fechaTiContrato;

      Double gastosConstitucion = -(contrato.getInversion().getGastosConstitucion().getAdjudicacion());
      Date fechaGastosConstitucion = contrato.getInversion().getFechaGastosConstitucion().getAdjudicacion();
      anyos = this.buscarFechaYMeter(anyos,fechaGastosConstitucion,stringGastosConstitucion,gastosConstitucion);

      fechaTi = contrato.getIngresos().getFechaImpago().getAdjudicacion();
      Integer trimestres = (int)(this.restarFechas(fechaTi, fechaT0) / 90);
      Double importeT0Ti =
        ((importeConcedido - deudaActual) / trimestres)
          + ((((importeConcedido - deudaActual) / 12) * 3) * (tipoInteres / 4));

      //cambiado: importe entregas dinerarias es trimestral
      for(int i=0;i<trimestres;i++) {
        var entregasDinerariasTrimestre =
          importeT0Ti
            * Math.pow(
            (1 + tipoInteres), (this.restarFechas(fechaSolucionAmistosa, fechaTi) / 365));
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaT0Contrato);
        cal.add(Calendar.DATE, (93*i));
        Date fecha = cal.getTime();
        anyos = this.buscarFechaYMeter(anyos, fecha, stringImporteEntregasDinerarias,entregasDinerariasTrimestre);
      }
    }


    //Date fechaEntregasDinerarias = dto.getVariablesSolucion().getIngresos().getFechasEntregasDinerarias().getAdjudicacion();
    //anyos = this.buscarFechaYMeter(anyos, fechaEntregasDinerarias, stringImporteEntregasDinerarias,entregasDinerarias);

    Double tasaJudicial = -(dto.getVariablesSolucion().getGastos().getTasaJudicial().getAdjudicacion());
    Date fechaTasaJudicial = dto.getVariablesSolucion().getGastos().getFechaTasaJudicial().getAdjudicacion();
    anyos = this.buscarFechaYMeter(anyos, fechaTasaJudicial, stringTasaJudicial,tasaJudicial);

    Double letrado = -(dto.getVariablesSolucion().getGastos().getLetrado().getAdjudicacion());
    Date fechaLetrado = dto.getVariablesSolucion().getGastos().getFechaLetrado().getAdjudicacion();
    anyos = this.buscarFechaYMeter(anyos, fechaLetrado, stringLetrado,letrado);

    Double procurador = -(dto.getVariablesSolucion().getGastos().getProcurador().getAdjudicacion());
    Date fechaProcurador = dto.getVariablesSolucion().getGastos().getFechaProcurador().getAdjudicacion();
    anyos = this.buscarFechaYMeter(anyos, fechaProcurador, stringProcurador,procurador);


    for (SimulacionBienDTO bien : dto.getVariablesBienes()) {
      Date fechaT1bien = bien.getIngresos().getFechaAdjudicacion().getAdjudicacion();
      double diff = this.restarFechas(fechaTi, fechaSolucionAmistosa);
      Date fechaTn = bien.getIngresos().getFechaVentaEstimada().getAdjudicacion();
      double diffT1Tn =
        this.restarFechas(
          fechaSolucionAmistosa, bien.getIngresos().getFechaVentaEstimada().getAdjudicacion());

      Double ibiT1 =
        -(calcularPeriodicidad(
          bien.getGastos().getPeriodicidadIBI().getAdjudicacion(),
          bien.getGastos().getIBI().getAdjudicacion(),
          diff));
      anyos = this.buscarFechaYMeter(anyos, fechaT1bien, stringIBI, ibiT1);

      Double ibiTn =
        -(calcularPeriodicidad(
          bien.getGastos().getPeriodicidadIBI().getAdjudicacion(),
          bien.getGastos().getIBI().getAdjudicacion(),
          diffT1Tn));
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringIBI, ibiTn);


      Double comunidadT1 =
        -(calcularPeriodicidad(
          bien.getGastos().getPeriodicidadComunidad().getAdjudicacion(),
          bien.getGastos().getComunidad().getAdjudicacion(),
          diff));
      anyos = this.buscarFechaYMeter(anyos, fechaT1bien, stringComunidad, comunidadT1);

      Double comunidadTn=
        -(calcularPeriodicidad(
          bien.getGastos().getPeriodicidadComunidad().getAdjudicacion(),
          bien.getGastos().getComunidad().getAdjudicacion(),
          diffT1Tn));
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringComunidad, comunidadTn);


      Double facilities = -(bien.getGastos().getFacilities().getAdjudicacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1bien, stringFacilities, facilities);

      Double capex = -(bien.getGastos().getCapexEnElActivo().getAdjudicacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1bien, stringCapex, capex);

      Double valoracionActualActivo = bien.getIngresos().getValoracionActualActivo().getAdjudicacion();
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringValoracionActualActivo, valoracionActualActivo);

      Double cee = -(bien.getGastos().getCEE().getAdjudicacion());
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringCee, cee);

      Double tramitesDerechoTanteo = -(bien.getGastos().getTramitesDerechoTanteo().getAdjudicacion());
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringTanteo, tramitesDerechoTanteo);

      Double comisionApi = -(bien.getGastos().getComisionApi().getAdjudicacion());
      anyos = this.buscarFechaYMeter(anyos, fechaTn, stringComisionApi, comisionApi);

      Date fechaItp = bien.getGastos().getFechaITP().getAdjudicacion();
      Double itp = -(bien.getGastos().getITP().getAdjudicacion());
      anyos = this.buscarFechaYMeter(anyos, fechaItp, stringComisionApi, itp);
    }

    //lo meto dentro del objeto y lo devuelvo
    adjudicacion.setAnyos(anyos);
    return adjudicacion;
  }

  private Map<String, Double> calcularRefinanciacion(
    SimulacionDTO dto, Date fechaSolucionAmistosa) {
    Double importeT0Final = 0d,
      importeT0TiFinal = 0d,
      importeT1Final = 0d,
      importeT1TnFinal = 0d,
      tipoInteres = 0d,
      descuento = 0d;

    VariableSimulacion variableInteres =
      variableSimulacionRepository.findByCodigo("interes").orElse(null);
    tipoInteres = Objects.nonNull(variableInteres.getValor()) ? variableInteres.getValor()/100 : 0d;
    VariableSimulacion variableDescuento =
      variableSimulacionRepository.findByCodigo("descuento").orElse(null);
    descuento = Objects.nonNull(variableDescuento.getValor()) ? variableDescuento.getValor()/100 : 0d;

    Integer anyos =
      dto.getVariablesSolucion().getIngresos().getNumeroAnyosRefinanciacion().getRefinanciacion();

    for (SimulacionBienDTO bien : dto.getVariablesBienes()) {
      importeT1Final += bien.getGastos().getTasacion().getRefinanciacion();
      importeT1Final += bien.getGastos().getRegistro().getRefinanciacion();
      importeT1Final += bien.getGastos().getCargasAsumidas().getRefinanciacion();
      importeT1Final += bien.getGastos().getNotaria().getRefinanciacion();
    }

    importeT1Final +=
      dto.getVariablesSolucion().getGastos().getTasaJudicial().getRefinanciacion()
        + dto.getVariablesSolucion().getGastos().getLetrado().getRefinanciacion()
        + dto.getVariablesSolucion().getGastos().getProcurador().getRefinanciacion()
        + dto.getVariablesSolucion().getDeuda().getDeudaActual().getRefinanciacion()
        + dto.getVariablesSolucion().getIngresos().getDotaciones().getRefinanciacion();

    Double dotaciones = dto.getVariablesSolucion().getIngresos().getDotacionesRefin().getRefinanciacion();
    for (SimulacionContratoDTO contrato : dto.getVariablesContratos()) {
      importeT1Final += contrato.getInversion().getGastosConstitucion().getRefinanciacion();
      //todo esto no se si va en negativo
      Double importeConcedido = contrato.getInversion().getImporteConcedido().getAdjudicacion();
      Double deudaActual = contrato.getDeuda().getDeudaActual().getAdjudicacion();


      //sacar el numero de trimestres
      var trimestresFinal = anyos * 4;
      var importeTrimestral = contrato.getInversion().getImporteConcedido().getRefinanciacion() / trimestresFinal;
      Double importe = 0d;
      //por cada trimestre sumar al importeT1TnFinal el importe de ese trimestre
      for(int i = 1;i<=trimestresFinal;i++) {
        Double importeMensual = importeTrimestral
          * (Math.pow((1 + tipoInteres), (double)i/4));
        importe += importeMensual;
      }


      Date fechaT0 = contrato.getInversion().getFechaImporteConcedido().getAdjudicacion();
      Double importeT0 =
        importeConcedido + contrato.getInversion().getGastosConstitucion().getAdjudicacion();

      Date fechaTi = contrato.getIngresos().getFechaImpago().getAdjudicacion();
      Integer trimestres = (int)(this.restarFechas(fechaTi, fechaT0) / 90);
      Double importeT0Ti =
        ((importeConcedido - deudaActual) / trimestres);

      importeT0Final += importeT0;

      //cambiado: importe entregas dinerarias es trimestral
      for(int i=0;i<trimestres;i++) {
        importeT0TiFinal +=
          importeT0Ti
            * Math.pow(
            (1 + tipoInteres), (this.restarFechas(fechaSolucionAmistosa, fechaTi) / 365));
      }

      //var importe2 = deudaActual * Math.pow((1 + descuento), (anyos));
      //var importe3 = deudaActual * (1 - Math.pow((1 + descuento), (-anyos)));
      var importeAnual = 0d;
      if(tipoInteres != null && !tipoInteres.equals(0d)) {
        importeAnual = -Finance.pmt(tipoInteres,anyos,deudaActual,0d);
      } else {
        importeAnual = deudaActual/anyos;
      }
      importeT1TnFinal += importeAnual;
    }

    List<Double> importes = new ArrayList<>();
    for(int i=0;i<anyos;i++) {
      importes.add(importeT1TnFinal);
    }

    double[] arrayImportes = new double[importes.size()+4];
    arrayImportes[0] = (importeT0Final * -1);
    arrayImportes[1] = (importeT0TiFinal);
    arrayImportes[2] = (-importeT1Final);
    arrayImportes[3] = (importeT1TnFinal);
    arrayImportes[3] = (dotaciones);
    int index = 4;
    for (var value: importes) {
      arrayImportes[index++] = value;
    }
    List<Double> listaParametrosVan = new ArrayList<>();
    listaParametrosVan.add(importeT0TiFinal);
    listaParametrosVan.add(-importeT1Final);
    listaParametrosVan.add(importeT1TnFinal);
    listaParametrosVan.add(dotaciones);

    for(var temp: importes) {
      listaParametrosVan.add(temp);
    }
    /*List<Double> listaVan = Stream.of(listaParametrosVan,importes)
      .flatMap(Collection::stream)
      .collect(Collectors.toList());*/

    Map<String, Double> refinanciacion = new HashMap<>();
    //cambiado: el van puesto sin interes porque solo tiene que estar en entregas dinerarias, ya calculado
    refinanciacion.put(
      "van",
      this.calcularVan(
        0d,
        (importeT0Final * -1),
        listaParametrosVan));
    refinanciacion.put(
      "tir",
      Irr.irr(arrayImportes)
        * 100);
    refinanciacion.put(
      "mfp", (importeT0TiFinal + importeT1Final + dotaciones + importeT1TnFinal*anyos) / importeT0Final);

    return refinanciacion;
  }

  public SimulacionGraficaPalancaDTO calcularGraficaRefinanciacion(SimulacionDTO dto, Date fechaSolucionAmistosa) {
    java.util.Date fechaT0, fechaTi = null, fechaT1;
    SimulacionGraficaPalancaDTO refinanciacion = new SimulacionGraficaPalancaDTO();
    SortedMap<Integer,List<SimulacionGraficaFilaDTO>> anyos = new TreeMap<>();

    //sacar cuáles son los años para meterlos antes de nada
    fechaT0 = encontrarPrimeraFecha(dto.getVariablesContratos(),"refinanciacion");
    LocalDate fechaT0Local = Instant.ofEpochMilli(fechaT0.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    int primerAnyo=fechaT0Local.getYear();
    fechaT1 = fechaSolucionAmistosa;
    LocalDate fechaT1Local = Instant.ofEpochMilli(fechaT1.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    Integer ultimoAnyo=fechaT1Local.getYear();


    //en el map que irá metido en el objeto que se devuelve, meto por cada año una lista donde luego se meterán todas las variables
    for(int f=primerAnyo; f<=ultimoAnyo; f++) {
      anyos.put(f,this.generarFilas());
    }

    String stringImporteConcedido="";
    String stringDeudaActual="";
    String stringGastosConstitucion="";
    String stringImporteEntregasDinerarias="";
    String stringTasaJudicial="";
    String stringLetrado="";
    String stringProcurador="";
    String stringDotaciones="";
    String stringTasacion="";
    String stringRegistro="";
    String stringCargasAsumidas="";
    String stringNotaria="";

    if(LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      stringImporteConcedido = "Granted amount";
      stringDeudaActual = "Current debt";
      stringGastosConstitucion = "Constitution expenses";
      stringImporteEntregasDinerarias = "Amount of cash deliveries";
      stringTasaJudicial = "Court of justice tax";
      stringLetrado = "Lawyer";
      stringProcurador = "Proxy";
      stringDotaciones = "Endowments";
      stringTasacion="Taxation";
      stringRegistro="Register";
      stringCargasAsumidas="Assumed charges";
      stringNotaria="Notary";

    } else if(LocaleContextHolder.getLocale().getLanguage().equals("es")) {
      stringImporteConcedido = "Importe concedido";
      stringDeudaActual = "Deuda actual";
      stringGastosConstitucion = "Gastos de constitución";
      stringImporteEntregasDinerarias = "Importe entregas dinerarias";
      stringTasaJudicial = "Tasa judicial";
      stringLetrado = "Letrado";
      stringProcurador = "Procurador";
      stringDotaciones = "Dotaciones";
      stringTasacion="Tasación";
      stringRegistro="Registro";
      stringCargasAsumidas="Cargas asumidas";
      stringNotaria="Notaría";
    }

    Integer nAnyos =
      dto.getVariablesSolucion().getIngresos().getNumeroAnyosRefinanciacion().getRefinanciacion();
    var trimestresFinal = nAnyos * 4;
    var importeTrimestral = 0d;
    Double entregasDinerarias = 0d;
    VariableSimulacion variableInteres =
      variableSimulacionRepository.findByCodigo("interes").orElse(null);
    Double tipoInteres = Objects.nonNull(variableInteres.getValor()) ? variableInteres.getValor()/100 : 0d;
    for (SimulacionContratoDTO contrato : dto.getVariablesContratos()) {
      Date fechaT0Contrato = contrato.getInversion().getFechaImporteConcedido().getRefinanciacion();

      Double importeConcedido = contrato.getInversion().getImporteConcedido().getRefinanciacion();
      LocalDate fechaLocal = Instant.ofEpochMilli(fechaT0Contrato.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
      int anyo=fechaLocal.getYear();
      Date fechaImporteConcedido = Date.from(LocalDate.of(anyo,1,1).atStartOfDay(ZoneId.systemDefault()).toInstant());
      anyos = this.buscarFechaYMeter(anyos,fechaImporteConcedido,stringImporteConcedido,-importeConcedido);

      Double deudaActual = contrato.getDeuda().getDeudaActual().getRefinanciacion();
      //anyos = this.buscarFechaYMeter(anyos,fechaT0Contrato,stringDeudaActual,deudaActual);

      Date fechaTiContrato = contrato.getIngresos().getFechaImpago().getRefinanciacion();
      if(Objects.isNull(fechaTi) || fechaTiContrato.before(fechaTi))
        fechaTi = fechaTiContrato;

      Double gastosConstitucion = -(contrato.getInversion().getGastosConstitucion().getRefinanciacion());
      Date fechaGastosConstitucion = contrato.getInversion().getFechaGastosConstitucion().getRefinanciacion();
      anyos = this.buscarFechaYMeter(anyos,fechaGastosConstitucion,stringGastosConstitucion,gastosConstitucion);

      importeTrimestral += contrato.getInversion().getImporteConcedido().getRefinanciacion() / trimestresFinal;
    }


    // Double entregasDinerarias = dto.getVariablesSolucion().getIngresos().getImporteEntregasDinerarias().getRefinanciacion();
    // Date fechaEntregasDinerarias = dto.getVariablesSolucion().getIngresos().getFechasEntregasDinerarias().getRefinanciacion();
    Integer contador = 1;

    for (int i = ultimoAnyo; i >= ultimoAnyo - nAnyos; i--){
      for (int j = 0; j <= 3; j++){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, i);
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.add(Calendar.MONTH, j * 3);
        Double importeMensual = importeTrimestral
          * (Math.pow((1 + tipoInteres), (double)(trimestresFinal - contador)/4));
        var fecha = c.getTime();
        anyos = this.buscarFechaYMeter(anyos, fecha, stringImporteEntregasDinerarias, importeMensual);
        contador++;
      }
    }

    //anyos = this.buscarFechaYMeter(anyos, fechaEntregasDinerarias, stringImporteEntregasDinerarias,entregasDinerarias);

    Double tasaJudicial = -(dto.getVariablesSolucion().getGastos().getTasaJudicial().getRefinanciacion());
    Date fechaTasaJudicial = dto.getVariablesSolucion().getGastos().getFechaTasaJudicial().getRefinanciacion();
    anyos = this.buscarFechaYMeter(anyos, fechaTasaJudicial, stringTasaJudicial,tasaJudicial);

    Double letrado = -(dto.getVariablesSolucion().getGastos().getLetrado().getRefinanciacion());
    Date fechaLetrado = dto.getVariablesSolucion().getGastos().getFechaLetrado().getRefinanciacion();
    anyos = this.buscarFechaYMeter(anyos, fechaLetrado, stringLetrado,letrado);

    Double procurador = -(dto.getVariablesSolucion().getGastos().getProcurador().getRefinanciacion());
    Date fechaProcurador = dto.getVariablesSolucion().getGastos().getFechaProcurador().getRefinanciacion();
    anyos = this.buscarFechaYMeter(anyos, fechaProcurador, stringProcurador,procurador);

    Double dotaciones = dto.getVariablesSolucion().getIngresos().getDotacionesRefin().getRefinanciacion();
    Date fechaDotaciones = dto.getVariablesSolucion().getIngresos().getFechaDotacionesRefin().getRefinanciacion();
    anyos = this.buscarFechaYMeter(anyos, fechaDotaciones, stringDotaciones,dotaciones);


    for (SimulacionBienDTO bien : dto.getVariablesBienes()) {

      Double tasacion = -(bien.getGastos().getTasacion().getRefinanciacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringTasacion, tasacion);

      Double registro = -(bien.getGastos().getRegistro().getRefinanciacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringRegistro, registro);

      Double cargas = -(bien.getGastos().getCargasAsumidas().getRefinanciacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringCargasAsumidas, cargas);

      Double notaria = -(bien.getGastos().getNotaria().getRefinanciacion());
      anyos = this.buscarFechaYMeter(anyos, fechaT1, stringNotaria, notaria);
    }

    //lo meto dentro del objeto y lo devuelvo
    refinanciacion.setAnyos(anyos);
    return refinanciacion;
  }

  private double restarFechas(Date fecha1, Date fecha2) {
    TimeUnit time = TimeUnit.DAYS;
    return ((double) time.convert((fecha1.getTime() - fecha2.getTime()), TimeUnit.MILLISECONDS));
  }

  private double calcularPeriodicidad(String periodicidad, Double importe, double diff) {
    switch (periodicidad) {
      case "mensual":
        return importe * (diff / 30);
      case "trimestral":
        return importe * (diff / 90);
      case "cuatrimestral":
        return importe * (diff / 120);
      case "semestral":
        return importe * (diff / 180);
      case "anual":
        return importe * (diff / 365);
      default:
        return 0d;
    }
  }

  private Double calcularVan(Double interes, Double deudaInicial, List<Double> cashFlows) {
    Double van = deudaInicial;

    for (var i = 0; i < cashFlows.size(); i++) {
      van += cashFlows.get(i) / Math.pow(interes + 1, i + 1);
    }

    return van;
  }

  public SimulacionSolucionDTO calculosSolucion(SimulacionInputDTO simulacionInputDTO) {

    List<Contrato> contratos =
      contratoRepository.findAllByIdIn(simulacionInputDTO.getBusqueda().getIdsContrato());
    List<Bien> bienes = bienRepository.findAllDistinctByContratosContratoIn(contratos);

    SimulacionSolucionDTO simulacionSolucionDTO = new SimulacionSolucionDTO();

    // FECHA SOLUCION AMISTOSA
    Date fechaSolucionAmistosa = simulacionInputDTO.getBusqueda().getFechaSolucionAmistosa();

    // DEUDA
    Double deudaActual = calcularDeudaActual(simulacionInputDTO, contratos);
    simulacionSolucionDTO.setDeuda(
      new SimulacionMultipleDeudaDTO(new SimulacionFilaDoubleDTO(deudaActual)));

    // INGRESOS
    Double entregasDinerarias = calcularEntregasDinerarias(simulacionInputDTO, bienes);
    Double entregaNoDineraria = calcularEntregasNoDinerarias(simulacionInputDTO, bienes);
    Date fechaEntregaNoDineraria = calcularFechaEntregaNoDineraria(simulacionInputDTO, bienes);
    Double dotaciones = calcularDotaciones(bienes, deudaActual);
    Double dotacionesRefin = deudaActual; //calcularDotacionesRefin(contratos, bienes);
    Integer numeroAnyosRefinanciacion =
      simulacionInputDTO.getCampos() != null
        ? simulacionInputDTO.getCampos().getSolucion().getNumeroAnyosRefinanciacion()
        : null;

    simulacionSolucionDTO.setIngresos(
      new SimulacionSolucionIngresosDTO(
        new SimulacionFilaDoubleDTO(entregasDinerarias),
        new SimulacionFilaDatesDTO(fechaSolucionAmistosa),
        new SimulacionFilaDoubleDTO(entregaNoDineraria),
        new SimulacionFilaDatesDTO(fechaEntregaNoDineraria),
        new SimulacionFilaDoubleDTO(dotaciones, dotaciones, dotaciones, dotaciones, dotaciones),
        new SimulacionFilaDatesDTO(fechaSolucionAmistosa, fechaSolucionAmistosa, fechaSolucionAmistosa, fechaSolucionAmistosa, fechaSolucionAmistosa),
        new SimulacionFilaDoubleDTO(null, null, dotacionesRefin, null, null),
        new SimulacionFilaDatesDTO(null, null, fechaSolucionAmistosa, null, null),
        new SimulacionFilaIntegerDTO(null, null, numeroAnyosRefinanciacion, null, null)));

    // GASTOS
    Double letrado = calcularLetrado(simulacionInputDTO, contratos, deudaActual);
    Double procurador = calcularProcurador(simulacionInputDTO, contratos, deudaActual);
    Double tasaJudicial = calcularTasaJudicial(simulacionInputDTO, contratos);
    List<OtrosGastosAsociadosDTO> gastosAsociados = calcularGastosAsociados(simulacionInputDTO);

    simulacionSolucionDTO.setGastos(
      new SimulacionSolucionGastosDTO(
        new SimulacionFilaDoubleDTO(letrado),
        new SimulacionFilaDatesDTO(fechaSolucionAmistosa),
        new SimulacionFilaDoubleDTO(procurador),
        new SimulacionFilaDatesDTO(fechaSolucionAmistosa),
        new SimulacionFilaDoubleDTO(tasaJudicial),
        new SimulacionFilaDatesDTO(fechaSolucionAmistosa),
        gastosAsociados));

    return simulacionSolucionDTO;
  }

  private Double calcularDeudaActual(SimulacionInputDTO inputDTO, List<Contrato> contratos) {
    Double deudaActual = 0d;
    if (Objects.nonNull(inputDTO.getRecalculado())
      && inputDTO.getRecalculado()
      && Objects.nonNull(inputDTO.getCampos().getContratos())) {
      for (SimulacionVariablesContratoInputDTO contrato : inputDTO.getCampos().getContratos()) {
        if (Objects.nonNull(contrato.getDeudaActual())) {
          deudaActual += contrato.getDeudaActual();
        } else {
          Contrato c = contratoRepository.findById(contrato.getId()).orElse(null);
          if (Objects.nonNull(c)) {
            deudaActual += Objects.nonNull(c.getSaldoGestion()) ? c.getSaldoGestion() : 0d;
          }
        }
      }
    } else {
      for (Contrato contrato : contratos) {
        deudaActual +=
          Objects.nonNull(contrato.getSaldoGestion()) ? contrato.getSaldoGestion() : 0d;
      }
    }
    return deudaActual;
  }

  private Double calcularEntregasDinerarias(
    SimulacionInputDTO simulacionInputDTO, List<Bien> bienes) {
    if (Objects.nonNull(simulacionInputDTO.getCampos())
      && Objects.nonNull(simulacionInputDTO.getCampos().getSolucion())
      && Objects.nonNull(
      simulacionInputDTO.getCampos().getSolucion().getImporteEntregasDinerarias())) {
      return simulacionInputDTO.getCampos().getSolucion().getImporteEntregasDinerarias();
    } else {
      return importeUltimasTasacionesValoraciones(bienes);
    }
  }

  private Double calcularEntregasNoDinerarias(
    SimulacionInputDTO simulacionInputDTO, List<Bien> bienes) {
    if (Objects.nonNull(simulacionInputDTO.getCampos())
      && Objects.nonNull(simulacionInputDTO.getCampos().getSolucion())
      && Objects.nonNull(
      simulacionInputDTO.getCampos().getSolucion().getImporteEntregasNoDinerarias())) {
      return simulacionInputDTO.getCampos().getSolucion().getImporteEntregasNoDinerarias();
    } else {
      return importeUltimasTasacionesValoraciones(bienes);
    }
  }

  private Double importeUltimasTasacionesValoraciones(List<Bien> bienes) {
    Double entregasDinerarias = 0d;
    for (Bien bien : bienes) {
      if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
        Tasacion tas =
          bien.getTasaciones().stream()
            .max(Comparator.comparing(Tasacion::getFecha))
            .orElse(null);
        if (Objects.nonNull(tas)) entregasDinerarias += tas.getImporte();
      } else if (Objects.nonNull(bien.getValoraciones()) && !bien.getValoraciones().isEmpty()) {
        Valoracion val =
          bien.getValoraciones().stream()
            .max(Comparator.comparing(Valoracion::getFecha))
            .orElse(null);
        if (Objects.nonNull(val)) entregasDinerarias += val.getImporte();
      }
    }
    return entregasDinerarias;
  }

  private Date calcularFechaEntregaNoDineraria(
    SimulacionInputDTO simulacionInputDTO, List<Bien> bienes) {
    if (Objects.nonNull(simulacionInputDTO.getCampos())
      && Objects.nonNull(simulacionInputDTO.getCampos().getSolucion())
      && Objects.nonNull(
      simulacionInputDTO.getCampos().getSolucion().getFechaEntregasNoDinerarias())) {
      return simulacionInputDTO.getCampos().getSolucion().getFechaEntregasNoDinerarias();
    } else {
      return fechaUltimaTasacionValoracion(bienes);
    }
  }

  private Date fechaUltimaTasacionValoracion(List<Bien> bienes) {
    List<Date> fechas = new ArrayList<>();
    for (Bien bien : bienes) {
      if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
        Tasacion tas =
          bien.getTasaciones().stream()
            .max(Comparator.comparing(Tasacion::getFecha))
            .orElse(null);
        if (Objects.nonNull(tas) && Objects.nonNull(tas.getFecha())) fechas.add(tas.getFecha());
      } else if (Objects.nonNull(bien.getValoraciones()) && !bien.getValoraciones().isEmpty()) {
        Valoracion val =
          bien.getValoraciones().stream()
            .max(Comparator.comparing(Valoracion::getFecha))
            .orElse(null);
        if (Objects.nonNull(val) && Objects.nonNull(val.getFecha())) fechas.add(val.getFecha());
      }
    }
    return !fechas.isEmpty() ? fechas.stream().max(Date::compareTo).orElse(null) : null;
  }

  private Double calcularDotaciones(List<Bien> bienes, Double deudaActual) {
    return deudaActual - importeUltimasValoraciones(bienes);
  }

  private Double calcularDotacionesRefin(List<Contrato> contratos, List<Bien> bienes) {
    Double importeValorRefin = importeValorRefin(contratos);
    Double importeTasaciones = importeUltimasTasaciones(bienes);
    if (Objects.isNull(bienes)) {
      return importeValorRefin;
    } else if (importeValorRefin > importeTasaciones) {
      return importeValorRefin - importeTasaciones;
    } else {
      return importeValorRefin * 0.05;
    }
  }

  private Double importeUltimasValoraciones(List<Bien> bienes) {
    Double entregasDinerarias = 0d;
    for (Bien bien : bienes) {
      if (Objects.nonNull(bien.getValoraciones()) && !bien.getValoraciones().isEmpty()) {
        Valoracion val =
          bien.getValoraciones().stream()
            .max(Comparator.comparing(Valoracion::getFecha))
            .orElse(null);
        if (Objects.nonNull(val)) entregasDinerarias += val.getImporte();
      }
    }
    return entregasDinerarias;
  }

  private Double importeValorRefin(List<Contrato> contratos) {
    Double importeValorRefin = 0d;
    for (Contrato contrato : contratos) {
      if (Objects.nonNull(contrato.getImporteInicial())) {
        importeValorRefin += contrato.getImporteInicial();
      }
    }
    return importeValorRefin;
  }

  private Double importeUltimasTasaciones(List<Bien> bienes) {
    Double importeTasaciones = 0d;
    for (Bien bien : bienes) {
      if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
        Tasacion tas =
          bien.getTasaciones().stream()
            .max(Comparator.comparing(Tasacion::getFecha))
            .orElse(null);
        if (Objects.nonNull(tas)) importeTasaciones += tas.getImporte();
      }
    }
    return importeTasaciones;
  }

  private Double calcularLetrado(
    SimulacionInputDTO simulacionInputDTO, List<Contrato> contratos, Double deudaActual) {
    Double letrado;
    if (Objects.nonNull(simulacionInputDTO.getCampos())
      && Objects.nonNull(simulacionInputDTO.getCampos().getCatalogo())
      && Objects.nonNull(simulacionInputDTO.getCampos().getCatalogo().getLetrado())) {
      letrado = simulacionInputDTO.getCampos().getCatalogo().getLetrado();
    } else {
      VariableSimulacion variableLetrado =
        variableSimulacionRepository.findByCodigo("letrado").orElse(null);
      letrado = Objects.nonNull(variableLetrado) ? variableLetrado.getValor() : 0d;
    }

    Double letradoSinSubasta;
    if (Objects.nonNull(simulacionInputDTO.getCampos())
      && Objects.nonNull(simulacionInputDTO.getCampos().getCatalogo())
      && Objects.nonNull(simulacionInputDTO.getCampos().getCatalogo().getLetradoSinSubasta())) {
      letradoSinSubasta = simulacionInputDTO.getCampos().getCatalogo().getLetradoSinSubasta();
    } else {
      VariableSimulacion variableLetradoSinSubasta =
        variableSimulacionRepository.findByCodigo("letradoSinSubasta").orElse(null);
      letradoSinSubasta =
        Objects.nonNull(variableLetradoSinSubasta) ? variableLetradoSinSubasta.getValor() : 0d;
    }

    if (this.haySubasta(contratos)) {
      return deudaActual * (letrado / 100);
    } else {
      return deudaActual * (letrado * (letradoSinSubasta / 100) / 100);
    }
  }

  private Double calcularProcurador(
    SimulacionInputDTO simulacionInputDTO, List<Contrato> contratos, Double deudaActual) {
    Double procurador;
    if (Objects.nonNull(simulacionInputDTO.getCampos())
      && Objects.nonNull(simulacionInputDTO.getCampos().getCatalogo())
      && Objects.nonNull(simulacionInputDTO.getCampos().getCatalogo().getProcurador())) {
      procurador = simulacionInputDTO.getCampos().getCatalogo().getProcurador();
    } else {
      VariableSimulacion variableProcurador =
        variableSimulacionRepository.findByCodigo("procurador").orElse(null);
      procurador = Objects.nonNull(variableProcurador) ? variableProcurador.getValor() : 0d;
    }

    Double procuradorSinSubasta;
    if (Objects.nonNull(simulacionInputDTO.getCampos())
      && Objects.nonNull(simulacionInputDTO.getCampos().getCatalogo())
      && Objects.nonNull(
      simulacionInputDTO.getCampos().getCatalogo().getProcuradorSinSubasta())) {
      procuradorSinSubasta = simulacionInputDTO.getCampos().getCatalogo().getProcuradorSinSubasta();
    } else {
      VariableSimulacion variableProcuradorSinSubasta =
        variableSimulacionRepository.findByCodigo("procuradorSinSubasta").orElse(null);
      procuradorSinSubasta =
        Objects.nonNull(variableProcuradorSinSubasta)
          ? variableProcuradorSinSubasta.getValor()
          : 0d;
    }

    if (this.haySubasta(contratos)) {
      return deudaActual * (procurador / 100);
    } else {
      return deudaActual * (procurador * (procuradorSinSubasta / 100) / 100);
    }
  }

  private Double calcularTasaJudicial(
    SimulacionInputDTO simulacionInputDTO, List<Contrato> contratos) {
    Double tasaIndividual;
    if (Objects.nonNull(simulacionInputDTO.getCampos())
      && Objects.nonNull(simulacionInputDTO.getCampos().getCatalogo())
      && Objects.nonNull(simulacionInputDTO.getCampos().getCatalogo().getTasaJudicial())) {
      tasaIndividual = simulacionInputDTO.getCampos().getCatalogo().getTasaJudicial();
    } else {
      VariableSimulacion tasaJudicial =
        variableSimulacionRepository.findByCodigo("tasaJudicial").orElse(null);
      tasaIndividual = Objects.nonNull(tasaJudicial.getValor()) ? tasaJudicial.getValor() : 0d;
    }

    Double numeroSubasta;
    if (Objects.nonNull(simulacionInputDTO.getCampos())
      && Objects.nonNull(simulacionInputDTO.getCampos().getCatalogo())
      && Objects.nonNull(
      simulacionInputDTO.getCampos().getCatalogo().getTasaJudicialSinSubasta())) {
      numeroSubasta = simulacionInputDTO.getCampos().getCatalogo().getTasaJudicialSinSubasta();
    } else {
      VariableSimulacion tasaJudicialSinSubasta =
        variableSimulacionRepository.findByCodigo("tasaJudicialSinSubasta").orElse(null);
      numeroSubasta =
        Objects.nonNull(tasaJudicialSinSubasta.getValor())
          ? tasaJudicialSinSubasta.getValor()
          : 0d;
    }

    Double tasa = 0d;
    for (Contrato contrato : contratos) {
      boolean haySubasta = false;
      if (Objects.nonNull(contrato.getProcedimientos())
        && !contrato.getProcedimientos().isEmpty()) {
        for (Procedimiento procedimiento : contrato.getProcedimientos()) {
          if (Objects.nonNull(procedimiento.getSubastas())
            && !procedimiento.getSubastas().isEmpty()) {
            haySubasta = true;
          }
        }
      }
      for (ContratoBien ignored : contrato.getBienes()) {
        if (haySubasta) {
          tasa += tasaIndividual;
        } else {
          tasa += tasaIndividual * (numeroSubasta / 100);
        }
      }
    }
    return tasa;
  }

  private List<OtrosGastosAsociadosDTO> calcularGastosAsociados(
    SimulacionInputDTO simulacionInputDTO) {
    if (Objects.nonNull(simulacionInputDTO.getCampos())
      && Objects.nonNull(simulacionInputDTO.getCampos().getSolucion())
      && Objects.nonNull(simulacionInputDTO.getCampos().getSolucion().getNuevosGastosAsociados())
      && !simulacionInputDTO.getCampos().getSolucion().getNuevosGastosAsociados().isEmpty()) {
      return simulacionInputDTO.getCampos().getSolucion().getNuevosGastosAsociados();
    }
    return null;
  }

  private boolean haySubasta(List<Contrato> contratos) {
    for (Contrato contrato : contratos) {
      if (Objects.nonNull(contrato.getProcedimientos())
        && !contrato.getProcedimientos().isEmpty()) {
        for (Procedimiento procedimiento : contrato.getProcedimientos()) {
          if (Objects.nonNull(procedimiento.getSubastas())
            && !procedimiento.getSubastas().isEmpty()) {
            return true;
          }
        }
      }
    }
    return false;
  }

  public List<Bien> getBienes(List<Integer> ids) {
    return bienRepository.findAllById(ids);
  }
}

