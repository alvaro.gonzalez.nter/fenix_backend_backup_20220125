package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SimulacionContratoInversionDTO implements Serializable {
  private SimulacionFilaDoubleDTO importeConcedido;
  private SimulacionFilaDatesDTO fechaImporteConcedido;
  private SimulacionFilaDoubleDTO gastosConstitucion;//Editable
  private SimulacionFilaDatesDTO fechaGastosConstitucion;
}
