package com.haya.alaska.simulacion.infrastructure.controller.dto.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionVariablesCatalogoInputDTO implements Serializable {
  // Solucion
  private Double letrado;
  private Double letradoSinSubasta;
  private Double procurador;
  private Double procuradorSinSubasta;
  private Double tasaJudicial;
  private Double tasaJudicialSinSubasta;

  // Contratos
  private Double gastosConstitucion;

  // Bienes
  private Double porcentajeAdjudicacion;
  private Double IBI;
  private Double comunidad;
  private Double facilities;
  private Double capexEnElActivo;
  private Double seguros;
  private Double notaria;
  private Double registro;
  private Double tasacion;
  private Double CEE;
  private Double tramitesDerechoTanteo;
  private Double comisionApi;

  private Map<Integer, Double> ITP;
  private Map<Integer, Double> AJD;
}
