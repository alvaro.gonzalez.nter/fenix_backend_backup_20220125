package com.haya.alaska.simulacion.infrastructure.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterables;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.bien.infrastructure.mapper.BienMapper;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.mapper.ContratoMapper;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.simulacion.domain.Simulacion;
import com.haya.alaska.simulacion.infrastructure.controller.dto.input.*;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.*;
import com.haya.alaska.simulacion.infrastructure.repository.SimulacionRepository;
import com.haya.alaska.simulacion.infrastructure.util.SimulacionUtil;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.variable_ajd.domain.VariableAjd;
import com.haya.alaska.variable_ajd.repository.VariableAjdRepository;
import com.haya.alaska.variable_itp.domain.VariableItp;
import com.haya.alaska.variable_itp.repository.VariableItpRepository;
import com.haya.alaska.variable_simulacion.domain.VariableSimulacion;
import com.haya.alaska.variable_simulacion.repository.VariableSimulacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class SimulacionMapper {

  private static final ObjectMapper objectMapper = new ObjectMapper();
  private static final Integer LIMITE_HISTORICO = 10;

  static {
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired CarteraRepository carteraRepository;
  @Autowired ContratoRepository contratoRepository;
  @Autowired ContratoMapper contratoMapper;
  @Autowired BienRepository bienRepository;
  @Autowired BienMapper bienMapper;
  @Autowired SimulacionUtil simulacionUtil;
  @Autowired VariableSimulacionRepository variableSimulacionRepository;
  @Autowired ClienteRepository clienteRepository;
  @Autowired VariableAjdRepository variableAjdRepository;
  @Autowired VariableItpRepository variableItpRepository;
  @Autowired SimulacionRepository simulacionRepository;

  public Simulacion dtoAEntidad(
      Integer idSimulacion, String nombre, SimulacionInputDTO simulacionInputDTO, SimulacionResultadoDTO resultado)
      throws JsonProcessingException, NotFoundException {

    if (Objects.isNull(simulacionInputDTO)) return null;

    Simulacion simulacion;
    if (Objects.nonNull(idSimulacion)) {
      simulacion =
          simulacionRepository
              .findById(idSimulacion)
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "Simulacion con id " + idSimulacion + " no encontrada"));
    } else {
      simulacion = new Simulacion();
    }
    simulacion.setNombre(nombre);
    simulacion.setExpediente(
        expedienteRepository
            .findById(simulacionInputDTO.getBusqueda().getIdExpediente())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "Expediente con id "
                            + simulacionInputDTO
                                .getBusqueda()
                                .getIdExpediente()
                            + " no encontrado")));
    simulacion.setCliente(
        clienteRepository
            .findById(simulacionInputDTO.getBusqueda().getIdCliente())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "Cliente con id "
                            + simulacionInputDTO
                                .getBusqueda()
                                .getIdCliente()
                            + " no encontrado")));
    simulacion.setFechaUltimoCambio(new Date());
    simulacion.setResultado(
        objectMapper.writeValueAsString(resultado));
    simulacion.setCamposGuardados(
        objectMapper.writeValueAsString(simulacionInputDTO));
    return simulacion;
  }

  public SimulacionDTO entidadADto(Simulacion simulacion)
    throws Exception {
    SimulacionDTO simulacionDTO = new SimulacionDTO();
    if (Objects.nonNull(simulacion)) {
      SimulacionInputDTO simulacionInputDTO =
          objectMapper.readValue(simulacion.getCamposGuardados(), SimulacionInputDTO.class);
      simulacionDTO = this.inputADtoNormal(simulacionInputDTO);
      simulacionDTO.setId(simulacion.getId());
      simulacionDTO.setNombre(simulacion.getNombre());
      simulacionDTO.setConsolidada(simulacion.getConsolidada());
    }
    return simulacionDTO;
  }

  public SimulacionDTOToList entidadADtoToList(Simulacion simulacion)
      throws JsonProcessingException {
    SimulacionDTOToList simulacionDTOToList = new SimulacionDTOToList();
    if (Objects.nonNull(simulacion)) {
      simulacionDTOToList.setId(simulacion.getId());
      simulacionDTOToList.setNombre(simulacion.getNombre());
      simulacionDTOToList.setIdExpediente(simulacion.getExpediente().getId());
      simulacionDTOToList.setIdConcatenadoExpediente(simulacion.getExpediente().getIdConcatenado());
      simulacionDTOToList.setCliente(simulacion.getCliente().getNombre());
      simulacionDTOToList.setFechaUltimoCambio(simulacion.getFechaUltimoCambio());
      simulacionDTOToList.setResultado(
          objectMapper.readValue(simulacion.getResultado(), SimulacionResultadoDTO.class));
      // simulacionDTOToList.setCabecera(this.entidadACabeceraDto(simulacion));
    }
    return simulacionDTOToList;
  }

  public SimulacionCabeceraDTO entidadACabeceraDto(Simulacion simulacion)
      throws JsonProcessingException, NotFoundException {
    SimulacionCabeceraInputDTO simulacionCabeceraInputDTO = new SimulacionCabeceraInputDTO();
    SimulacionCabeceraDTO simulacionCabeceraDTO = new SimulacionCabeceraDTO();
    if (simulacion != null) {
      SimulacionInputDTO simulacionInputDTO =
          objectMapper.readValue(simulacion.getCamposGuardados(), SimulacionInputDTO.class);
      simulacionCabeceraInputDTO.setBusqueda(simulacionInputDTO.getBusqueda());
      simulacionCabeceraInputDTO.setRecalculado(simulacionInputDTO.getRecalculado());
      simulacionCabeceraDTO = this.inputACabecera(simulacionCabeceraInputDTO);
    }
    simulacionCabeceraDTO.setId(simulacion.getId());
    return simulacionCabeceraDTO;
  }

  public SimulacionCabeceraDTO inputACabecera(SimulacionCabeceraInputDTO simulacionCabeceraInputDTO)
      throws NotFoundException {
    SimulacionCabeceraDTO simulacionCabeceraDTO = new SimulacionCabeceraDTO();

    Cartera cartera =
        carteraRepository
            .findById(simulacionCabeceraInputDTO.getBusqueda().getIdCartera())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "Cartera con id "
                            + simulacionCabeceraInputDTO.getBusqueda().getIdCartera()
                            + " no encontrada"));

    Cliente cliente =
        clienteRepository
            .findById(simulacionCabeceraInputDTO.getBusqueda().getIdCliente())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "Cliente con id "
                            + simulacionCabeceraInputDTO.getBusqueda().getIdCliente()
                            + " no encontrado"));

    Expediente expediente =
        expedienteRepository
            .findById(simulacionCabeceraInputDTO.getBusqueda().getIdExpediente())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "Expediente con id "
                            + simulacionCabeceraInputDTO.getBusqueda().getIdCartera()
                            + " no encontrado"));

    simulacionCabeceraDTO.setCliente(cliente.getNombre());
    simulacionCabeceraDTO.setIdCliente(cliente.getId());
    simulacionCabeceraDTO.setCartera(cartera.getNombre());
    simulacionCabeceraDTO.setIdCartera(cartera.getId());
    simulacionCabeceraDTO.setIdExpediente(expediente.getId());
    simulacionCabeceraDTO.setIdConcatenadoExpediente(expediente.getIdConcatenado());
    simulacionCabeceraDTO.setResponsableCartera(
        Objects.nonNull(cartera.getResponsableCartera())
            ? cartera.getResponsableCartera().getNombre()
            : null);
    Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");
    simulacionCabeceraDTO.setSupervisor(
        Objects.nonNull(supervisor) ? supervisor.getNombre() : null);
    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");
    simulacionCabeceraDTO.setGestor(Objects.nonNull(gestor) ? gestor.getNombre() : null);
    simulacionCabeceraDTO.setTipoSimulacion(
        simulacionCabeceraInputDTO.getRecalculado() ? "Manual" : "Automática");
    simulacionCabeceraDTO.setFechaSolucionAmistosa(
        simulacionCabeceraInputDTO.getBusqueda().getFechaSolucionAmistosa());

    simulacionCabeceraDTO.setContratos(
        contratoMapper.listContratoToListContratoDTOToList(
            contratoRepository.findAllById(
                simulacionCabeceraInputDTO.getBusqueda().getIdsContrato())));
    simulacionCabeceraDTO.setBienes(
        bienMapper.listBienToListBienDTOToList(
            bienRepository.findAllById(simulacionCabeceraInputDTO.getBusqueda().getIdsBien()),
            null));
    simulacionCabeceraDTO.setIntervinientes(
        simulacionCabeceraInputDTO.getBusqueda().getIdsInterviniente());

    return simulacionCabeceraDTO;
  }

  public SimulacionDTO inputADtoNormal(SimulacionInputDTO simulacionInputDTO)
    throws Exception {
    SimulacionDTO simulacionDTO = new SimulacionDTO();

    if (this.comprobarRecalcularBienes(simulacionInputDTO.getCampos())) {
      simulacionDTO.setVariablesBienes(
          this.recalcularBienes(
              simulacionInputDTO.getCampos().getBienes(),
              simulacionInputDTO.getCampos().getCatalogo(),
              simulacionInputDTO.getBusqueda().getFechaSolucionAmistosa()));
    } else {
      simulacionDTO.setVariablesBienes(
          this.iniciarBienes(
              bienRepository.findAllById(
                  simulacionInputDTO.getBusqueda().getIdsBien().stream()
                      .distinct()
                      .collect(Collectors.toList())),
              simulacionInputDTO.getBusqueda().getFechaSolucionAmistosa()));
    }

    if (this.comprobarRecalcularContratos(simulacionInputDTO.getCampos())) {
      simulacionDTO.setVariablesContratos(
          this.recalcularContratos(
              simulacionInputDTO.getCampos().getContratos(),
              simulacionInputDTO.getCampos().getCatalogo()));
    } else {
      simulacionDTO.setVariablesContratos(
          this.iniciarContratos(
              contratoRepository.findAllById(simulacionInputDTO.getBusqueda().getIdsContrato())));
    }

    simulacionDTO.setVariablesSolucion(simulacionUtil.calculosSolucion(simulacionInputDTO));

    return simulacionDTO;
  }

  private boolean comprobarRecalcularBienes(SimulacionVariablesInputDTO campos) {
    if (Objects.nonNull(campos)) {
      if (Objects.nonNull(campos.getBienes())) {
        return true;
      }
      if (Objects.nonNull(campos.getCatalogo())) {
        if (Objects.nonNull(campos.getCatalogo().getPorcentajeAdjudicacion())
            || Objects.nonNull(campos.getCatalogo().getIBI())
            || Objects.nonNull(campos.getCatalogo().getComunidad())
            || Objects.nonNull(campos.getCatalogo().getFacilities())
            || Objects.nonNull(campos.getCatalogo().getCapexEnElActivo())
            || Objects.nonNull(campos.getCatalogo().getSeguros())
            || Objects.nonNull(campos.getCatalogo().getNotaria())
            || Objects.nonNull(campos.getCatalogo().getRegistro())
            || Objects.nonNull(campos.getCatalogo().getTasacion())
            || Objects.nonNull(campos.getCatalogo().getCEE())
            || Objects.nonNull(campos.getCatalogo().getTramitesDerechoTanteo())
            || Objects.nonNull(campos.getCatalogo().getComisionApi())
            || Objects.nonNull(campos.getCatalogo().getITP())
            || Objects.nonNull(campos.getCatalogo().getAJD())) {
          return true;
        }
      }
    }
    return false;
  }

  private boolean comprobarRecalcularContratos(SimulacionVariablesInputDTO campos) {
    if (Objects.nonNull(campos)) {
      if (Objects.nonNull(campos.getContratos())) {
        return true;
      }
      if (Objects.nonNull(campos.getCatalogo())
          && Objects.nonNull(campos.getCatalogo().getGastosConstitucion())) {
        return true;
      }
    }
    return false;
  }

  private List<SimulacionBienDTO> recalcularBienes(
      List<SimulacionVariablesBienInputDTO> input,
      SimulacionVariablesCatalogoInputDTO inputCatalogo,
      Date fechaSolucion)
    throws Exception {
    List<SimulacionBienDTO> variablesBienes = new ArrayList<>();
    for (SimulacionVariablesBienInputDTO variablesBien : input) {
      Bien bien =
          bienRepository
              .findById(variablesBien.getId())
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "Bien con id " + variablesBien.getId() + " no encontrado"));

      variablesBienes.add(this.mapearBien(bien, variablesBien, inputCatalogo, fechaSolucion));
    }
    return variablesBienes;
  }

  private List<SimulacionBienDTO> iniciarBienes(List<Bien> bienes, Date fechaSolucionAmistosa)
    throws Exception {
    List<SimulacionBienDTO> bienesFinales = new ArrayList<>();
    for (Bien bien : bienes) {
      SimulacionVariablesBienInputDTO simulacionVariablesBienInputDTO =
          new SimulacionVariablesBienInputDTO();
      simulacionVariablesBienInputDTO.setFechaSolucionAmistosa(fechaSolucionAmistosa);
      bienesFinales.add(
          this.mapearBien(
              bien,
              simulacionVariablesBienInputDTO,
              new SimulacionVariablesCatalogoInputDTO(),
              fechaSolucionAmistosa));
    }
    return bienesFinales;
  }

  private List<SimulacionContratoDTO> recalcularContratos(
      List<SimulacionVariablesContratoInputDTO> input,
      SimulacionVariablesCatalogoInputDTO inputCatalogo)
      throws NotFoundException {
    List<SimulacionContratoDTO> variablesContratos = new ArrayList<>();
    for (SimulacionVariablesContratoInputDTO variablesContrato : input) {
      Contrato contrato =
          contratoRepository
              .findById(variablesContrato.getId())
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "Bien con id " + variablesContrato.getId() + " no encontrado"));

      variablesContratos.add(this.mapearContrato(contrato, variablesContrato, inputCatalogo));
    }
    return variablesContratos;
  }

  private List<SimulacionContratoDTO> iniciarContratos(List<Contrato> contratos)
      throws NotFoundException {
    List<SimulacionContratoDTO> contratosFinales = new ArrayList<>();
    for (Contrato contrato : contratos) {
      contratosFinales.add(
          this.mapearContrato(
              contrato,
              new SimulacionVariablesContratoInputDTO(),
              new SimulacionVariablesCatalogoInputDTO()));
    }
    return contratosFinales;
  }

  private SimulacionBienDTO mapearBien(
      Bien bien,
      SimulacionVariablesBienInputDTO variablesBien,
      SimulacionVariablesCatalogoInputDTO variablesCatalogo,
      Date fechaSolucion)
    throws Exception {
    SimulacionBienDTO result = new SimulacionBienDTO();

    if(Objects.isNull(bien.getProvincia())) {
      throw new Exception("El bien con ID "+bien.getId()+" e ID carga "+bien.getIdCarga()+" no tiene provincia. No se puede realizar la simulación");
    }
    result.setId(bien.getId());
    result.setIdCarga(bien.getIdCarga());
    result.setIngresos(mapearBienIngresos(bien, variablesBien, variablesCatalogo, fechaSolucion));
    result.setGastos(mapearBienGastos(bien, variablesBien, variablesCatalogo, fechaSolucion));
    return result;
  }

  private SimulacionBienIngresosDTO mapearBienIngresos(
      Bien bien,
      SimulacionVariablesBienInputDTO variablesBien,
      SimulacionVariablesCatalogoInputDTO variablesCatalogo,
      Date fechaSolucion) {
    SimulacionBienIngresosDTO result = new SimulacionBienIngresosDTO();

    if (Objects.nonNull(variablesBien)) {
      if (Objects.nonNull(variablesBien.getImporteSubasta())) {
        result.setImporteSubasta(
            new SimulacionFilaDoubleDTO(null, null, null, null, variablesBien.getImporteSubasta()));
      } else {
        result.setImporteSubasta(new SimulacionFilaDoubleDTO());
      }

      if (Objects.nonNull(variablesBien.getFechaAdjudicacion())) {
        result.setFechaAdjudicacion(
            new SimulacionFilaDatesDTO(
                null, null, null, null, variablesBien.getFechaAdjudicacion()));
      } else {
        result.setFechaAdjudicacion(new SimulacionFilaDatesDTO());
      }

      if (Objects.nonNull(variablesBien.getValoracionActualActivo())) {
        result.setValoracionActualActivo(
            new SimulacionFilaDoubleDTO(
                variablesBien.getValoracionActualActivo(),
                variablesBien.getValoracionActualActivo(),
                null,
                variablesBien.getValoracionActualActivo(),
                variablesBien.getValoracionActualActivo()));
      } else {
        result.setValoracionActualActivo(new SimulacionFilaDoubleDTO());
      }

      if (Objects.nonNull(variablesBien.getFechaAcuerdoVenta())) {
        result.setFechaAcuerdoVenta(
            new SimulacionFilaDatesDTO(
                null, variablesBien.getFechaAcuerdoVenta(), null, null, null));
      } else {
        result.setFechaAcuerdoVenta(new SimulacionFilaDatesDTO());
      }

      if (Objects.nonNull(variablesBien.getLiquidezActivo())) {
        result.setLiquidezActivo(
            new SimulacionFilaStringDTO(
                variablesBien.getLiquidezActivo(),
                variablesBien.getLiquidezActivo(),
                null,
                variablesBien.getLiquidezActivo(),
                variablesBien.getLiquidezActivo()));
      } else {
        result.setLiquidezActivo(new SimulacionFilaStringDTO());
      }

      if (Objects.nonNull(variablesBien.getFechaVentaEstimada())) {
        result.setFechaVentaEstimada(
            new SimulacionFilaDatesDTO(
              variablesBien.getFechaVentaEstimada(),
              variablesBien.getFechaVentaEstimada(),
              variablesBien.getFechaVentaEstimada(),
                variablesBien.getFechaVentaEstimada(),
                variablesBien.getFechaVentaEstimada()));
      }
      else {
        result.setFechaVentaEstimada(new SimulacionFilaDatesDTO());
      }
    }

    if (!this.comprobarPropiedades(result.getImporteSubasta())) {
      List<BienSubastado> listBS = new ArrayList<>(bien.getBienesSubastados());
      if (Objects.nonNull(listBS) && !listBS.isEmpty()) {
        result.setImporteSubasta(
            new SimulacionFilaDoubleDTO(
                null, null, null, null, listBS.get(0).getImporteAdjudicacion()));
      } else {
        result.setImporteSubasta(new SimulacionFilaDoubleDTO());
      }
    } else {
      result.setImporteSubasta(new SimulacionFilaDoubleDTO());
    }

    if (Objects.nonNull(bien.getBienesSubastados()) && !bien.getBienesSubastados().isEmpty()) {
      Double porcentajeAdjudicacion = 0d;
      if (Objects.nonNull(variablesCatalogo) && Objects.nonNull(variablesCatalogo.getPorcentajeAdjudicacion())) {
        porcentajeAdjudicacion = variablesCatalogo.getPorcentajeAdjudicacion();
      } else {
        VariableSimulacion importeAdjudicacion =
            variableSimulacionRepository.findByCodigo("porcentajeAdjudicacion").orElse(null);
        if (Objects.nonNull(importeAdjudicacion))
          porcentajeAdjudicacion = importeAdjudicacion.getValor();
      }

      List<BienSubastado> listaBS = new ArrayList<>(bien.getBienesSubastados());
      BienSubastado bs = listaBS.get(0);
      Double total =
          Objects.nonNull(bs.getImporteAdjudicacion())
              ? bs.getImporteAdjudicacion() * (porcentajeAdjudicacion / 100)
              : 0d;
      result.setPorcentajeAdjudicacion(new SimulacionFilaDoubleDTO(null, null, null, null, total));
    }

    if(!comprobarPropiedades(result.getPorcentajeAdjudicacion())) {
      result.setPorcentajeAdjudicacion(new SimulacionFilaDoubleDTO());
    }

    if (Objects.isNull(result.getFechaAdjudicacion())) {
      if(Objects.nonNull(calcularFechaAdjudicacion(bien))) {
        result.setFechaAdjudicacion(
          new SimulacionFilaDatesDTO(
            null, null, null, null, calcularFechaAdjudicacion(bien)));
      } else {
        result.setFechaAdjudicacion(
          new SimulacionFilaDatesDTO(
            null, null, null, null, calcularFechaAdjudicacionPorDefecto()));
      }
    }

    Valoracion valoracion = null;
    Tasacion tasacion = null;
    Double imp = null;
    //coger valoracion o tasacion, la ultima de las dos
    List<Valoracion> valoraciones = new ArrayList<>(bien.getValoraciones());
    List<Tasacion> tasaciones = new ArrayList<>(bien.getTasaciones());
    if (Objects.nonNull(valoraciones) && !valoraciones.isEmpty()) {
      valoracion = valoraciones.stream().filter(valoracion1 ->  Objects.nonNull(valoracion1.getFecha())).max(Comparator.comparing(Valoracion::getFecha)).orElse(null);
      imp = valoracion.getImporte();
    }
    if (Objects.nonNull(tasaciones) && !tasaciones.isEmpty()) {
      tasacion = tasaciones.stream().filter(tasacion1 ->  Objects.nonNull(tasacion1.getFecha())).max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      imp = tasacion.getImporte();
    }

    Liquidez l = null;
    Double importe = null;
    //sacar cuál es el último
    if(valoracion == null || valoracion.getFecha() == null || tasacion.getFecha().after(valoracion.getFecha())) {
      if (Objects.nonNull(tasacion) && Objects.nonNull(tasacion.getLiquidez()))
        l = tasacion.getLiquidez();

      if (!this.comprobarPropiedades(result.getValoracionActualActivo())) {
        if (Objects.nonNull(tasacion)) {
          Double numero = tasacion.getImporte();
          result.setValoracionActualActivo(
            new SimulacionFilaDoubleDTO(numero, numero, null, numero, numero));
        } else {
          result.setValoracionActualActivo(new SimulacionFilaDoubleDTO(0d, 0d, null, 0d, 0d));
        }
      }

    } else {
      if (Objects.nonNull(valoracion) && Objects.nonNull(valoracion.getLiquidez()))
        l = valoracion.getLiquidez();

      if (!this.comprobarPropiedades(result.getValoracionActualActivo())) {
        if (Objects.nonNull(valoracion)) {
          Double numero = valoracion.getImporte();
          result.setValoracionActualActivo(
            new SimulacionFilaDoubleDTO(numero, numero, null, numero, numero));
        } else {
          result.setValoracionActualActivo(new SimulacionFilaDoubleDTO(0d, 0d, null, 0d, 0d));
        }
      }
    }

    if (!this.comprobarPropiedades(result.getFechaVentaEstimada())) {
      if (Objects.nonNull(l)) {
        Calendar c = Calendar.getInstance();
        c.setTime(fechaSolucion);
        Integer dias;
        //Double imp = v.getImporte();
        if(imp != null) {
          if (imp <= 100000) dias = l.getN_0_100();
          else if (100000 < imp && imp <= 250000) dias = l.getN_100_250();
          else dias = l.getN_250_max();
          if (dias != null) c.add(Calendar.DATE, dias);
          result.setFechaVentaEstimada(
            new SimulacionFilaDatesDTO(c.getTime(), c.getTime(), c.getTime(), c.getTime(), c.getTime()));
        }
      }
    }

    if (!this.comprobarPropiedades(result.getLiquidezActivo())) {
      if (Objects.nonNull(l)) {
        String liquidez = "";
        if(valoracion != null) liquidez = valoracion.getLiquidez().getValor();
        if(tasacion != null) liquidez = tasacion.getLiquidez().getValor();
        if(!liquidez.equals(""))
          result.setLiquidezActivo(
             new SimulacionFilaStringDTO(liquidez, liquidez, null, liquidez, liquidez));
      }
    }

    return result;
  }

  private boolean comprobarPropiedades(SimulacionFilaDoubleDTO fila) {
    if(fila==null) return false;
    if(fila.getAdjudicacion() == null
    && fila.getDacion() == null
    && fila.getRefinanciacion() == null
    && fila.getCancelacion() == null
    && fila.getVentaColateral() == null)
      return false;
    else return true;
  }

  private boolean comprobarPropiedades(SimulacionFilaDatesDTO fila) {
    if(fila==null) return false;
    if(fila.getAdjudicacion() == null
      && fila.getDacion() == null
      && fila.getRefinanciacion() == null
      && fila.getCancelacion() == null
      && fila.getVentaColateral() == null)
      return false;
    else return true;
  }

  private boolean comprobarPropiedades(SimulacionFilaStringDTO fila) {
    if(fila==null) return false;
    if(fila.getAdjudicacion() == null
      && fila.getDacion() == null
      && fila.getRefinanciacion() == null
      && fila.getCancelacion() == null
      && fila.getVentaColateral() == null)
      return false;
    else return true;
  }

  private SimulacionContratoDTO mapearContrato(
      Contrato contrato,
      SimulacionVariablesContratoInputDTO variablesContrato,
      SimulacionVariablesCatalogoInputDTO variablesCatalogo)
      throws NotFoundException {
    SimulacionContratoDTO result = new SimulacionContratoDTO();

    result.setId(contrato.getId());
    result.setIdCarga(contrato.getIdCarga());

    result.setDeuda(mapearContratoDeuda(contrato, variablesContrato));
    result.setInversion(mapearContratoInversion(contrato, variablesContrato, variablesCatalogo));
    result.setIngresos(mapearContratoIngresos(contrato, variablesContrato));

    return result;
  }

  private SimulacionMultipleDeudaDTO mapearContratoDeuda(
      Contrato contrato, SimulacionVariablesContratoInputDTO variables) {
    if (Objects.isNull(variables.getDeudaActual())) {
      return new SimulacionMultipleDeudaDTO(
          new SimulacionFilaDoubleDTO(
              Objects.nonNull(contrato.getSaldoGestion()) ? contrato.getSaldoGestion() : 0d));
    } else {
      return new SimulacionMultipleDeudaDTO(
          new SimulacionFilaDoubleDTO(variables.getDeudaActual()));
    }
  }

  private SimulacionContratoInversionDTO mapearContratoInversion(
      Contrato contrato,
      SimulacionVariablesContratoInputDTO variablesContrato,
      SimulacionVariablesCatalogoInputDTO variablesCatalogo)
      throws NotFoundException {
    SimulacionContratoInversionDTO simulacionContratoInversionDTO =
        new SimulacionContratoInversionDTO();

    Double importeConcedido;
    if (Objects.isNull(variablesContrato.getImporteConcedido())) {
      importeConcedido =
          Objects.nonNull(contrato.getImporteInicial()) ? contrato.getImporteInicial() : 0d;
    } else {
      importeConcedido = variablesContrato.getImporteConcedido();
    }
    simulacionContratoInversionDTO.setImporteConcedido(
        new SimulacionFilaDoubleDTO(importeConcedido));

    Date fechaImporteConcedido;
    if (Objects.isNull(variablesContrato.getFechaImporteConcedido())) {
      fechaImporteConcedido =
          Objects.nonNull(contrato.getFechaFormalizacion())
              ? contrato.getFechaFormalizacion()
              : null;
    } else {
      fechaImporteConcedido = variablesContrato.getFechaImporteConcedido();
    }
    simulacionContratoInversionDTO.setFechaImporteConcedido(
        new SimulacionFilaDatesDTO(fechaImporteConcedido));

    Double gastosConstitucion;
    if (Objects.isNull(variablesCatalogo)
        || Objects.isNull(variablesCatalogo.getGastosConstitucion())) {
      VariableSimulacion porcentajeGastosConstitucion =
          variableSimulacionRepository
              .findByCodigo("gastosConstitucion")
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "Contrato con código porcentajeGastosConstitucion no encontrado"));
      gastosConstitucion = importeConcedido * (porcentajeGastosConstitucion.getValor() / 100);
    } else {
      gastosConstitucion = importeConcedido * (variablesCatalogo.getGastosConstitucion() / 100);
    }
    simulacionContratoInversionDTO.setGastosConstitucion(
        new SimulacionFilaDoubleDTO(gastosConstitucion));

    Date fechaGastosConstitucion;
    if (Objects.isNull(variablesContrato.getFechaGastosConstitucion())) {
      fechaGastosConstitucion =
          Objects.nonNull(contrato.getFechaFormalizacion())
              ? contrato.getFechaFormalizacion()
              : null;
    } else {
      fechaGastosConstitucion = variablesContrato.getFechaGastosConstitucion();
    }
    simulacionContratoInversionDTO.setFechaGastosConstitucion(
        new SimulacionFilaDatesDTO(fechaGastosConstitucion));

    return simulacionContratoInversionDTO;
  }

  private SimulacionContratoIngresosDTO mapearContratoIngresos(
      Contrato contrato, SimulacionVariablesContratoInputDTO variables) {
    SimulacionContratoIngresosDTO simulacionContratoIngresosDTO =
        new SimulacionContratoIngresosDTO();

    Date fechaImpago;
    if (Objects.isNull(variables.getFechaImpago())) {
      fechaImpago =
          Objects.nonNull(contrato.getFechaPrimerImpago()) ? contrato.getFechaPrimerImpago() : null;
    } else {
      fechaImpago = variables.getFechaImpago();
    }
    simulacionContratoIngresosDTO.setFechaImpago(new SimulacionFilaDatesDTO(fechaImpago));

    Double tipoInteres;
    if (Objects.isNull(variables.getTipoInteres())) {
      tipoInteres = Objects.nonNull(contrato.getTin()) ? contrato.getTin() : 0d;
    } else {
      tipoInteres = variables.getTipoInteres();
    }
    simulacionContratoIngresosDTO.setTipoInteres(new SimulacionFilaDoubleDTO(tipoInteres));

    return simulacionContratoIngresosDTO;
  }

  private SimulacionBienGastosDTO mapearBienGastos(
      Bien bien,
      SimulacionVariablesBienInputDTO variablesBien,
      SimulacionVariablesCatalogoInputDTO variablesCatalogo,
      Date fechaSolucionAmistosa)
      throws NotFoundException {

    if (Objects.isNull(bien) || Objects.isNull(variablesBien)) return null;

    SimulacionBienGastosDTO simulacionBienGastosDTO = new SimulacionBienGastosDTO();

    Double itp =
        calcularItp(bien, Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getITP() : null);

    Date fechaItp = variablesBien.getFechaAdjudicacion();
    if (Objects.isNull(fechaItp)) fechaItp = calcularFechaAdjudicacion(bien);

    simulacionBienGastosDTO.setITP(
        new SimulacionFilaDoubleDTO(null, null, itp, itp, itp)); // Editable// null, null, d, d, d
    simulacionBienGastosDTO.setFechaITP(
        new SimulacionFilaDatesDTO(
            null, null, fechaItp, fechaItp, fechaItp)); // null, null, d, d, d

    Double ajd =
        calcularAjd(bien, Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getAJD() : null);

    Date fechaAjd = variablesBien.getFechaAdjudicacion();
    if (Objects.isNull(fechaAjd)) fechaAjd = calcularFechaAdjudicacion(bien);
    if (Objects.isNull(fechaAjd)) fechaAjd = calcularFechaAdjudicacionPorDefecto();

    simulacionBienGastosDTO.setAJD(
        new SimulacionFilaDoubleDTO(null, null, ajd, ajd, ajd)); // Editable// null, null, d, d, d
    simulacionBienGastosDTO.setFechaAJD(
        new SimulacionFilaDatesDTO(
            null, null, fechaAjd, fechaAjd, fechaAjd)); // null, null, d, d, d

    String periodicidadIbi;
    Double ibi =
        calcularIbi(bien, Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getIBI() : null);
    if (Objects.nonNull(variablesBien.getPeriodicidadIBI()) && !variablesBien.getPeriodicidadIBI().equals(""))
      periodicidadIbi = variablesBien.getPeriodicidadIBI();
    else periodicidadIbi = "Anual";

    simulacionBienGastosDTO.setIBI(
        new SimulacionFilaDoubleDTO(null, ibi, null, ibi, ibi)); // Editable//null, d, null, d, ?
    simulacionBienGastosDTO.setPeriodicidadIBI(
        new SimulacionFilaStringDTO(
            null,
            periodicidadIbi,
            null,
            periodicidadIbi,
            periodicidadIbi)); // Editable//null, texto, null, fecha, ?

    String periodicidadComunidad;
    Double comunidad =
        calcularComunidad(
            bien, Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getComunidad() : null);
    if (Objects.nonNull(variablesBien.getPeriodicidadComunidad()) && !variablesBien.getPeriodicidadComunidad().equals(""))
      periodicidadComunidad = variablesBien.getPeriodicidadComunidad();
    else periodicidadComunidad = "Mensual";

    simulacionBienGastosDTO.setComunidad(
        new SimulacionFilaDoubleDTO(
            null, comunidad, null, comunidad, comunidad)); // null, d, null, d, ?
    simulacionBienGastosDTO.setPeriodicidadComunidad(
        new SimulacionFilaStringDTO(
            null,
            periodicidadComunidad,
            null,
            periodicidadComunidad,
            periodicidadComunidad)); // null, numero, null, numero, ?

    Double facilities =
        calcularFacilities(
            bien, Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getFacilities() : null);
    simulacionBienGastosDTO.setFacilities(
        new SimulacionFilaDoubleDTO(
            null, facilities, null, facilities, facilities)); // Editable//null, d, null, d, ?

    Double capex =
        calcularCapex(
            bien,
            Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getCapexEnElActivo() : null);
    simulacionBienGastosDTO.setCapexEnElActivo(
        new SimulacionFilaDoubleDTO(
            null, null, null, capex, capex)); // Editable// null, null, null, d, ?

    Double seguros =
        calcularSeguros(
            bien, Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getSeguros() : null);
    String periodicidadSeguros = "Anual";
    if (Objects.nonNull(variablesBien.getPeriodicidadPagoSeguros()) && !variablesBien.getPeriodicidadPagoSeguros().equals(""))
      periodicidadSeguros = variablesBien.getPeriodicidadPagoSeguros();

    simulacionBienGastosDTO.setSeguros(
        new SimulacionFilaDoubleDTO(
            null, null, null, seguros, seguros)); // Editable// null, null, null, d, ?
    simulacionBienGastosDTO.setPeriodicidadPagoSeguros(
        new SimulacionFilaStringDTO(
            null,
            null,
            null,
            periodicidadSeguros,
            periodicidadSeguros)); // Editable// null, null, null, d, ?

    Double notaria =
        calcularNotaria(
            bien, Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getNotaria() : null);
    simulacionBienGastosDTO.setNotaria(
        new SimulacionFilaDoubleDTO(
            null, null, notaria, notaria, null)); // Editable// null, null, d, d, null

    Double registro =
        calcularRegistro(
            bien, Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getRegistro() : null);
    simulacionBienGastosDTO.setRegistro(
        new SimulacionFilaDoubleDTO(
            null, null, registro, registro, null)); // Editable// null, null, d, d, null

    Double tasacion =
        calcularTasacion(
            bien, Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getTasacion() : null);
    simulacionBienGastosDTO.setTasacion(
        new SimulacionFilaDoubleDTO(
            null, null, tasacion, tasacion, null)); // Editable// null, null, d, d, null

    Double cee =
        calcularCee(Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getCEE() : null);
    simulacionBienGastosDTO.setCEE(
        new SimulacionFilaDoubleDTO(null, cee, cee, cee, cee)); // Editable// null, null, d, d, d

    Date fechaCee = fechaSolucionAmistosa;
    if (Objects.nonNull(variablesBien.getFechaCEE())) fechaCee = variablesBien.getFechaCEE();

    simulacionBienGastosDTO.setFechaCEE(
        new SimulacionFilaDatesDTO(null, null, fechaCee, fechaCee, null)); // null, null, d, d, null

    Date fechaDerechoTanteo;
    Double derechoTanteo =
        calcularDerechoTanteo(
            bien,
            Objects.nonNull(variablesCatalogo)
                ? variablesCatalogo.getTramitesDerechoTanteo()
                : null);
    if (Objects.nonNull(variablesBien.getFechaDerechoTanteo()))
      fechaDerechoTanteo = variablesBien.getFechaDerechoTanteo();
    else fechaDerechoTanteo = fechaSolucionAmistosa;

    simulacionBienGastosDTO.setTramitesDerechoTanteo(
        new SimulacionFilaDoubleDTO(
            null,
            derechoTanteo,
            derechoTanteo,
            derechoTanteo,
            derechoTanteo)); // Editable// null, null, d, d, null
    simulacionBienGastosDTO.setFechaDerechoTanteo(
        new SimulacionFilaDatesDTO(
            null, null, fechaDerechoTanteo, fechaDerechoTanteo, null)); // null, null, d, d, null

    Double cargasAsumidas;
    if (Objects.nonNull(variablesBien.getCargasAsumidas()))
      cargasAsumidas = variablesBien.getCargasAsumidas();
    else cargasAsumidas = calcularCargasAsumidas(bien);

    simulacionBienGastosDTO.setCargasAsumidas(
        new SimulacionFilaDoubleDTO(
            null, null, cargasAsumidas, cargasAsumidas, null)); // null, null, null, d, null

    Double comisionApi =
        calcularValorApi(
            bien, Objects.nonNull(variablesCatalogo) ? variablesCatalogo.getComisionApi() : null);
    simulacionBienGastosDTO.setComisionApi(
        new SimulacionFilaDoubleDTO(
            null, comisionApi, null, comisionApi, comisionApi)); // Editable// null, d, null, d, d

    return simulacionBienGastosDTO;
  }

  private Date calcularFechaAdjudicacion(Bien bien) {
    if (Objects.nonNull(bien.getBienesSubastados()) || !bien.getBienesSubastados().isEmpty()) {
      BienSubastado bienSubastado =
          bien.getBienesSubastados().stream()
              .filter(b -> Objects.nonNull(b.getFechaDecretoAdjudicacion()))
              .max(Comparator.comparing(BienSubastado::getFechaDecretoAdjudicacion))
              .orElse(null);
      if (Objects.nonNull(bienSubastado)) return bienSubastado.getFechaDecretoAdjudicacion();
    }
    return null;
  }

  private Date calcularFechaAdjudicacionPorDefecto() {
    //esto se cambiará cuando hagan la tabla de fechas por juzgado
    Calendar calendar = new GregorianCalendar();
    calendar.setTime(new Date());
    calendar.add(Calendar.DATE, 90);
    return calendar.getTime();
  }

  private Double calcularItp(Bien bien, Map<Integer, Double> catalogoItp) {
    if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
      Tasacion tasacion =
          bien.getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      if (Objects.nonNull(tasacion)) {
        List<VariableItp> variablesItp;
        if (bien.getProvincia().getComunidadAutonoma().getValor().equals("ANDALUCIA")
            || bien.getProvincia().getComunidadAutonoma().getValor().equals("BALEARES")) {
          Boolean garaje = bien.getSubTipoBien().getValor().equals("Garaje");
          variablesItp =
              variableItpRepository.findByComunidadAutonomaIdAndGaraje(
                  bien.getProvincia().getComunidadAutonoma().getId(), garaje);
        } else {
          variablesItp =
              variableItpRepository.findByComunidadAutonomaId(
                  bien.getProvincia().getComunidadAutonoma().getId());
        }
        for (VariableItp itp : variablesItp) {
          Double porcentaje;
          if (itp == Iterables.getLast(variablesItp)) {
            porcentaje = itp.getPorcentaje();
            if (Objects.nonNull(catalogoItp) && catalogoItp.containsKey(itp.getId())) {
              porcentaje = catalogoItp.get(itp.getId());
            }
            return Objects.nonNull(tasacion.getImporte())
                ? tasacion.getImporte() * (porcentaje / 100)
                : 0d;
          } else if (itp.getImporteMinimo() <= tasacion.getImporte()
              && tasacion.getImporte() <= itp.getImporteMaximo()) {
            porcentaje = itp.getPorcentaje();
            if (Objects.nonNull(catalogoItp) && catalogoItp.containsKey(itp.getId())) {
              porcentaje = catalogoItp.get(itp.getId());
            }
            return Objects.nonNull(tasacion.getImporte())
                ? tasacion.getImporte() * (porcentaje / 100)
                : 0d;
          }
        }
      }
    }
    return 0d;
  }

  private Double calcularAjd(Bien bien, Map<Integer, Double> catalogoAjd) throws NotFoundException {
    if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
      Tasacion tasacion =
          bien.getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      if (Objects.nonNull(tasacion)) {
        VariableAjd ajd =
            variableAjdRepository
                .findByComunidadAutonomaId(bien.getProvincia().getComunidadAutonoma().getId())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "Porcentaje AJD para la CCAA con id "
                                + bien.getProvincia().getComunidadAutonoma().getId()
                                + " no encontrado"));
        if (Objects.nonNull(catalogoAjd) && catalogoAjd.containsKey(ajd.getId())) {
          return Objects.nonNull(tasacion.getImporte())
              ? tasacion.getImporte() * (catalogoAjd.get(ajd.getId()) / 100)
              : 0d;
        }
        return Objects.nonNull(tasacion.getImporte())
            ? tasacion.getImporte() * (ajd.getPorcentaje() / 100)
            : 0d;
      }
    }
    return 0d;
  }

  private Double calcularIbi(Bien bien, Double ibiMetido) {
    Double ibiNumero = 0d;
    if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
      Tasacion tasacion =
          bien.getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      if (Objects.nonNull(tasacion)) {
        if (Objects.nonNull(ibiMetido)) {
          ibiNumero = ibiMetido;
        } else {
          VariableSimulacion ibi = variableSimulacionRepository.findByCodigo("IBI").orElse(null);
          if (Objects.nonNull(ibi)) {
            ibiNumero = ibi.getValor();
          }
        }
        return tasacion.getImporte() * (ibiNumero / 100);
      }
    }
    return 0d;
  }

  private Double calcularComunidad(Bien bien, Double comunidadMetido) {
    Double comunidadNumero = 0d;
    if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
      Tasacion tasacion =
          bien.getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      if (Objects.nonNull(tasacion)) {
        if (Objects.nonNull(comunidadMetido)) {
          comunidadNumero = comunidadMetido;
        } else {
          VariableSimulacion comunidad =
              variableSimulacionRepository.findByCodigo("comunidad").orElse(null);
          if (Objects.nonNull(comunidad)) {
            comunidadNumero = comunidad.getValor();
          }
        }
        return tasacion.getImporte() * (comunidadNumero / 100);
      }
    }
    return 0d;
  }

  private Double calcularFacilities(Bien bien, Double facilitiesMetido) {
    Double facilitiesNumero = 0d;
    if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
      Tasacion tasacion =
          bien.getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      if (Objects.nonNull(tasacion)) {
        if (Objects.nonNull(facilitiesMetido)) {
          facilitiesNumero = facilitiesMetido;
        } else {
          VariableSimulacion facilities =
              variableSimulacionRepository.findByCodigo("facilities").orElse(null);
          if (Objects.nonNull(facilities)) {
            facilitiesNumero = facilities.getValor();
          }
        }
        return tasacion.getImporte() * (facilitiesNumero / 100);
      }
    }
    return 0d;
  }

  private Double calcularCapex(Bien bien, Double capexMetido) {
    Double capexNumero = 0d;
    if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
      Tasacion tasacion =
          bien.getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      if (Objects.nonNull(tasacion)) {
        if (Objects.nonNull(capexMetido)) {
          capexNumero = capexMetido;
        } else {
          VariableSimulacion capex =
              variableSimulacionRepository.findByCodigo("capexEnElActivo").orElse(null);
          if (Objects.nonNull(capex)) {
            capexNumero = capex.getValor();
          }
        }
        return tasacion.getImporte() * (capexNumero / 100);
      }
    }
    return 0d;
  }

  private Double calcularSeguros(Bien bien, Double segurosMetido) {
    Double segurosNumero = 0d;
    if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
      Tasacion tasacion =
          bien.getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      if (Objects.nonNull(tasacion)) {
        if (Objects.nonNull(segurosMetido)) {
          segurosNumero = segurosMetido;
        } else {
          VariableSimulacion seguros =
              variableSimulacionRepository.findByCodigo("seguros").orElse(null);
          if (Objects.nonNull(seguros)) {
            segurosNumero = seguros.getValor();
          }
        }
        return tasacion.getImporte() * (segurosNumero / 100);
      }
    }
    return 0d;
  }

  private Double calcularNotaria(Bien bien, Double notariaMetido) {
    Double notariaNumero = 0d;
    if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
      Tasacion tasacion =
          bien.getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      if (Objects.nonNull(tasacion)) {
        if (Objects.nonNull(notariaMetido)) {
          notariaNumero = notariaMetido;
        } else {
          VariableSimulacion notaria =
              variableSimulacionRepository.findByCodigo("notaria").orElse(null);
          if (Objects.nonNull(notaria)) {
            notariaNumero = notaria.getValor();
          }
        }
        return tasacion.getImporte() * (notariaNumero / 100);
      }
    }
    return 0d;
  }

  private Double calcularRegistro(Bien bien, Double registroMetido) {
    Double registroNumero = 0d;
    if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
      Tasacion tasacion =
          bien.getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      if (Objects.nonNull(tasacion)) {
        if (Objects.nonNull(registroMetido)) {
          registroNumero = registroMetido;
        } else {
          VariableSimulacion registro =
              variableSimulacionRepository.findByCodigo("registro").orElse(null);
          if (Objects.nonNull(registro)) {
            registroNumero = registro.getValor();
          }
        }
        return tasacion.getImporte() * (registroNumero / 100);
      }
    }
    return 0d;
  }

  private Double calcularTasacion(Bien bien, Double tasacionMetido) {
    Double tasacionNumero = 0d;
    if (Objects.nonNull(bien.getTasaciones()) && !bien.getTasaciones().isEmpty()) {
      Tasacion tasacion =
          bien.getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
      if (Objects.nonNull(tasacion)) {
        if (Objects.nonNull(tasacionMetido)) {
          tasacionNumero = tasacionMetido;
        } else {
          VariableSimulacion tasacionPorcentaje =
              variableSimulacionRepository.findByCodigo("tasacion").orElse(null);
          if (Objects.nonNull(tasacionPorcentaje)) {
            tasacionNumero = tasacionPorcentaje.getValor();
          }
        }
        return tasacion.getImporte() * (tasacionNumero / 100);
      }
    }
    return 0d;
  }

  private Double calcularCee(Double ceeMetido) {
    if (Objects.nonNull(ceeMetido)) {
      return ceeMetido;
    } else {
      VariableSimulacion cee = variableSimulacionRepository.findByCodigo("CEE").orElse(null);
      return Objects.nonNull(cee) ? cee.getValor() : 0d;
    }
  }

  private Double calcularDerechoTanteo(Bien bien, Double derechoTanteoMetido) {
    if (Objects.nonNull(derechoTanteoMetido)) {
      return derechoTanteoMetido;
    } else {
      if (bien.getProvincia().getComunidadAutonoma().getValor().equals("CATALUÑA")
          || bien.getProvincia().getComunidadAutonoma().getValor().equals("COMUNIDAD VALENCIANA")) {
        VariableSimulacion tanteo =
            variableSimulacionRepository.findByCodigo("tramitesDerechoTanteo").orElse(null);
        return Objects.nonNull(tanteo.getValor()) ? tanteo.getValor() : 0d;
      }
    }
    return 0d;
  }

  private Double calcularValorApi(Bien bien, Double valorApiMetido) {
    Double apiNumero = 0d;
    if (Objects.nonNull(bien.getValoraciones()) && !bien.getValoraciones().isEmpty()) {
      Valoracion valoracion =
          bien.getValoraciones().stream()
              .max(Comparator.comparing(Valoracion::getFecha))
              .orElse(null);
      if (Objects.nonNull(valoracion)) {
        if (Objects.nonNull(valorApiMetido)) {
          apiNumero = valorApiMetido;
        } else {
          VariableSimulacion api =
              variableSimulacionRepository.findByCodigo("comisionApi").orElse(null);
          if (Objects.nonNull(api)) {
            apiNumero = api.getValor();
          }
        }
        return valoracion.getImporte() * (apiNumero / 100);
      }
    }
    return 0d;
  }

  private Double calcularCargasAsumidas(Bien bien) {
    Double importe = 0d;
    for (Carga carga : bien.getCargas()) {
      importe += carga.getImporte();
    }
    return importe;
  }
}
