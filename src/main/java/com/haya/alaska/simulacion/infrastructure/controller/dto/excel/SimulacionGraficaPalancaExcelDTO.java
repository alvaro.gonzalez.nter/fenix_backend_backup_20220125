package com.haya.alaska.simulacion.infrastructure.controller.dto.excel;

import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.simulacion.infrastructure.controller.dto.grafica.SimulacionGraficaFilaDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.grafica.SimulacionGraficaPalancaDTO;

import java.util.*;

public class SimulacionGraficaPalancaExcelDTO<T> extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();
  private final LinkedHashMap<String, Object> datos = new LinkedHashMap<>();

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }

  public List<?> getDatos() {
    return new ArrayList<>(datos.values());
  }


  public SimulacionGraficaPalancaExcelDTO(SimulacionGraficaPalancaDTO grafica) {

    //añadir cabeceras
    cabeceras.add("Grupo");
    cabeceras.add("Variable");
    cabeceras.add("Q0");

    for (var entry : grafica.getAnyos().entrySet()) {

      cabeceras.add("Q1 " + entry.getKey());
      cabeceras.add("Q2 " + entry.getKey());
      cabeceras.add("Q3 " + entry.getKey());
      cabeceras.add("Q4 " + entry.getKey());
    }

    cabeceras.add("Tasa");
    cabeceras.add("VAN");
    cabeceras.add("TIR");
    cabeceras.add("MFP");

  }

  public SimulacionGraficaPalancaExcelDTO(SimulacionGraficaPalancaDTO grafica, String nombre) {

    //añadir cabeceras
    cabeceras.add("Grupo");
    cabeceras.add("Variable");
    cabeceras.add("Q0");

    for (var entry : grafica.getAnyos().entrySet()) {

      cabeceras.add("Q1 " + entry.getKey());
      cabeceras.add("Q2 " + entry.getKey());
      cabeceras.add("Q3 " + entry.getKey());
      cabeceras.add("Q4 " + entry.getKey());
    }

    cabeceras.add("Tasa");
    cabeceras.add("VAN");
    cabeceras.add("TIR");
    cabeceras.add("MFP");

    //añadir valores

    for (var entry : grafica.getAnyos().entrySet()) {
      var lista = entry.getValue();
      for(SimulacionGraficaFilaDTO valor: lista) {
        if(valor.getNombre().equals(nombre)) {
          datos.put("Grupo",valor.getGrupo());
          datos.put("Variable",valor.getNombre());

          Integer anyo = entry.getKey();
          var primera = true;
          for (var entry2 : grafica.getAnyos().entrySet()) {

            if(anyo.equals(entry2.getKey())) {
              if(primera) datos.put("Q0",valor.getQ0());
              datos.put("Q1 " + entry2.getKey(),valor.getQ1());
              datos.put("Q2 " + entry2.getKey(),valor.getQ2());
              datos.put("Q3 " + entry2.getKey(),valor.getQ3());
              datos.put("Q4 " + entry2.getKey(),valor.getQ4());
            } else {
              if(primera) if(datos.get(("Q0")) == null) datos.put("Q0",null);
              if(datos.get(("Q1 " + entry2.getKey())) == null) datos.put("Q1 " + entry2.getKey(),null);
              if(datos.get(("Q2 " + entry2.getKey())) == null) datos.put("Q2 " + entry2.getKey(),null);
              if(datos.get(("Q3 " + entry2.getKey())) == null) datos.put("Q3 " + entry2.getKey(),null);
              if(datos.get(("Q4 " + entry2.getKey())) == null) datos.put("Q4 " + entry2.getKey(),null);
            }
            primera = false;
          }

          datos.put("Tasa",valor.getTasa());
          datos.put("VAN",valor.getVan());
          datos.put("TIR",valor.getTir());
          datos.put("MFP",valor.getMfp());
        }

      }

    }

  }
}
