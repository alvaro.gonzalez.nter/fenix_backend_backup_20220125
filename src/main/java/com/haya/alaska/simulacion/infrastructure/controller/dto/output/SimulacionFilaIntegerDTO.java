package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SimulacionFilaIntegerDTO implements Serializable {
  private Integer cancelacion;
  private Integer ventaColateral;
  private Integer refinanciacion;
  private Integer dacion;
  private Integer adjudicacion;

  public SimulacionFilaIntegerDTO(Integer todasIguales) {
    this.cancelacion=todasIguales;
    this.ventaColateral=todasIguales;
    this.refinanciacion=todasIguales;
    this.dacion=todasIguales;
    this.adjudicacion=todasIguales;
  }
}
