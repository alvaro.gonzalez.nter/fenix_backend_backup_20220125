package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionDTO implements Serializable {
  Integer id;
  Integer idPropuesta;
  String nombre;
  Boolean consolidada;
  private SimulacionSolucionDTO variablesSolucion;
  private List<SimulacionContratoDTO> variablesContratos;
  private List<SimulacionBienDTO> variablesBienes;
  private SimulacionResultadoDTO resultado;

}
