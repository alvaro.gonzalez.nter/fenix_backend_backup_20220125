package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import com.haya.alaska.shared.dto.BienDTOToList;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionCabeceraDTO implements Serializable {
  Integer id;
  String cliente;
  Integer idCliente;
  String cartera;
  Integer idCartera;
  Integer idExpediente;
  String idConcatenadoExpediente;

  private String responsableCartera;
  private String supervisor;
  private String gestor;
  private String tipoSimulacion;
  private Date fechaSolucionAmistosa;

  private List<ContratoDTOToList> contratos;
  private List<BienDTOToList> bienes;
  private List<Integer> intervinientes;
}
