package com.haya.alaska.simulacion.infrastructure.controller.dto.grafica;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionGraficaDTO implements Serializable {

  Map<String, SimulacionGraficaPalancaDTO> palancas;
}
