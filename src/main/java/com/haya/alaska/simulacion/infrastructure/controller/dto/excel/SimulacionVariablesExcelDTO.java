package com.haya.alaska.simulacion.infrastructure.controller.dto.excel;

import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionFilaDatesDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionFilaDoubleDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionFilaIntegerDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionFilaStringDTO;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;

public class SimulacionVariablesExcelDTO<T> extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  public static final String TIPO_DOUBLE = "double";
  public static final String TIPO_STRING = "string";
  public static final String TIPO_DATE = "date";
  public static final String TIPO_INTEGER = "integer";
  public static final String TIPO_SIMPLE = "simple";
  public static final String TIPO_PORCENTAJE = "porcentaje";

  static {
    cabeceras.add("Nombre");
    cabeceras.add("Nivel");
    cabeceras.add("Id Contrato/Bien");
    cabeceras.add("Cancelación");
    cabeceras.add("Venta de colateral");
    cabeceras.add("Refinanciación");
    cabeceras.add("Dación");
    cabeceras.add("Adjudicación");
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }

  public SimulacionVariablesExcelDTO() {
    this.add("Nombre", null);
    this.add("Nivel", null);
    this.add("Id Contrato/Bien", null);
    this.add("Cancelación", null);
    this.add("Venta de colateral", null);
    this.add("Refinanciación", null);
    this.add("Dación", null);
    this.add("Adjudicación", null);
  }

  public SimulacionVariablesExcelDTO(
      String nombre, String nivel, String idContratoBien, T parametros, String tipoParametro) {
    this.add("Nombre", nombre);
    this.add("Nivel", nivel);
    this.add("Id Contrato/Bien", idContratoBien);
    if (Objects.isNull(parametros)) {
      tipoParametro = "N/A";
    }
    switch (tipoParametro) {
      case TIPO_DOUBLE:
        SimulacionFilaDoubleDTO decimales = (SimulacionFilaDoubleDTO) parametros;
        this.add("Cancelación", decimales.getCancelacion());
        this.add("Venta de colateral", decimales.getVentaColateral());
        this.add("Refinanciación", decimales.getRefinanciacion());
        this.add("Dación", decimales.getDacion());
        this.add("Adjudicación", decimales.getAdjudicacion());
        break;
      case TIPO_DATE:
        SimulacionFilaDatesDTO fechas = (SimulacionFilaDatesDTO) parametros;
        this.add("Cancelación", fechas.getCancelacion());
        this.add("Venta de colateral", fechas.getVentaColateral());
        this.add("Refinanciación", fechas.getRefinanciacion());
        this.add("Dación", fechas.getDacion());
        this.add("Adjudicación", fechas.getAdjudicacion());
        break;
      case TIPO_INTEGER:
        SimulacionFilaIntegerDTO enteros = (SimulacionFilaIntegerDTO) parametros;
        this.add("Cancelación", enteros.getCancelacion());
        this.add("Venta de colateral", enteros.getVentaColateral());
        this.add("Refinanciación", enteros.getRefinanciacion());
        this.add("Dación", enteros.getDacion());
        this.add("Adjudicación", enteros.getAdjudicacion());
        break;
      case TIPO_STRING:
        SimulacionFilaStringDTO cadenas = (SimulacionFilaStringDTO) parametros;
        this.add("Cancelación", cadenas.getCancelacion());
        this.add("Venta de colateral", cadenas.getVentaColateral());
        this.add("Refinanciación", cadenas.getRefinanciacion());
        this.add("Dación", cadenas.getDacion());
        this.add("Adjudicación", cadenas.getAdjudicacion());
        break;
      case TIPO_SIMPLE:
        this.add("Cancelación", parametros);
        this.add("Venta de colateral", parametros);
        this.add("Refinanciación", parametros);
        this.add("Dación", parametros);
        this.add("Adjudicación", parametros);
        break;
      case TIPO_PORCENTAJE:
        SimulacionFilaDoubleDTO porcentajes = (SimulacionFilaDoubleDTO) parametros;
        this.add("Cancelación", (porcentajes.getCancelacion() * 100) + "%");
        this.add("Venta de colateral", (porcentajes.getVentaColateral() * 100) + "%");
        this.add("Refinanciación", (porcentajes.getRefinanciacion() * 100) + "%");
        this.add("Dación", (porcentajes.getDacion() * 100) + "%");
        this.add("Adjudicación", (porcentajes.getAdjudicacion() * 100) + "%");
        break;
      case "N/A":
        this.add("Cancelación", "N/A");
        this.add("Venta de colateral", "N/A");
        this.add("Refinanciación", "N/A");
        this.add("Dación", "N/A");
        this.add("Adjudicación", "N/A");
        break;
      default:
        this.add("Cancelación", null);
        this.add("Venta de colateral", null);
        this.add("Refinanciación", null);
        this.add("Dación", null);
        this.add("Adjudicación", null);
    }
  }
}
