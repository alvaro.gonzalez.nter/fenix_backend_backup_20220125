package com.haya.alaska.simulacion.infrastructure.repository;

import com.haya.alaska.simulacion.domain.Simulacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SimulacionRepository extends JpaRepository<Simulacion, Integer> {
  List<Simulacion> findAllByOrderByFechaUltimoCambioAsc();

  List<Simulacion> findAllByOrderByFechaUltimoCambioDesc();
}
