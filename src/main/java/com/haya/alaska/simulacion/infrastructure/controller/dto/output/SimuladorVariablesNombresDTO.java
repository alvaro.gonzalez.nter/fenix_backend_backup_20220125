package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SimuladorVariablesNombresDTO<T> implements Serializable {
  private String nombre;
  private T valor;
}
