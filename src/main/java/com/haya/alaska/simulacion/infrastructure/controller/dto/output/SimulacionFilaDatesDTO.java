package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SimulacionFilaDatesDTO implements Serializable {
  private Date cancelacion;
  private Date ventaColateral;
  private Date refinanciacion;
  private Date dacion;
  private Date adjudicacion;

  public SimulacionFilaDatesDTO(Date todasIguales) {
    this.cancelacion = todasIguales;
    this.ventaColateral = todasIguales;
    this.refinanciacion = todasIguales;
    this.dacion = todasIguales;
    this.adjudicacion = todasIguales;
  }

  public SimulacionFilaDatesDTO(
    Date cancelacion, Date ventaColateral, Date refinanciacion, Date dacion, Date adjudicacion) {
    this.cancelacion = cancelacion;
    this.ventaColateral = ventaColateral;
    this.refinanciacion = refinanciacion;
    this.dacion = dacion;
    this.adjudicacion = adjudicacion;
  }
}
