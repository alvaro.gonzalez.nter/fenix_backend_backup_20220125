package com.haya.alaska.simulacion.infrastructure.controller.dto.input;

import com.haya.alaska.simulacion.infrastructure.controller.dto.OtrosGastosAsociadosDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionVariablesSolucionInputDTO implements Serializable {
  // Ingresos
  private Double importeEntregasDinerarias;
  private Double importeEntregasNoDinerarias;
  private Date fechaEntregasNoDinerarias;
  private Integer numeroAnyosRefinanciacion;
  //private Double dotaciones;
  // Gastos
//  private Double letrado;
//  private Double letradoSinSubasta;
//  private Double procurador;
//  private Double procuradorSinSubasta;
//  private Double tasaJudicial;
//  private Double tasaJudicialSinSubasta;
  private List<OtrosGastosAsociadosDTO> nuevosGastosAsociados;
}
