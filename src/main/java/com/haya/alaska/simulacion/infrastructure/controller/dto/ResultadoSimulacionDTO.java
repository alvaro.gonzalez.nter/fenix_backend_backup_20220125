package com.haya.alaska.simulacion.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class ResultadoSimulacionDTO implements Serializable {
  Double cancelacion;
  Double ventaColateral;
  Double refinanciacion;
  Double dacion;
  Double adjudicacion;
}
