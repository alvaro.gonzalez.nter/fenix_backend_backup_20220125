package com.haya.alaska.simulacion.infrastructure.controller.dto.excel;

import com.haya.alaska.shared.util.ExcelUtils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class SimulacionPropuestaExcelDTO<T> extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    cabeceras.add("Nombre");
    cabeceras.add("Valor");
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }

  public SimulacionPropuestaExcelDTO() {
    this.add("Nombre", null);
    this.add("Valor", null);
  }

  public SimulacionPropuestaExcelDTO(
      String nombre, String valor) {
    this.add("Nombre", nombre);
    this.add("Valor", valor);
  }
}
