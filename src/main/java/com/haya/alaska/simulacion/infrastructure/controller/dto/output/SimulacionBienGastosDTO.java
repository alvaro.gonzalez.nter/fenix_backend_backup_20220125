package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SimulacionBienGastosDTO implements Serializable {
  private SimulacionFilaDoubleDTO ITP; //Editable// null, null, d, d, d
  private SimulacionFilaDatesDTO fechaITP; // null, null, d, d, d
  private SimulacionFilaDoubleDTO AJD; //Editable// null, null, d, d, d
  private SimulacionFilaDatesDTO fechaAJD; // null, null, d, d, d
  private SimulacionFilaDoubleDTO IBI; //Editable//null, d, null, d, ?
  private SimulacionFilaStringDTO periodicidadIBI; //Editable//null, texto, null, fecha, ?
  private SimulacionFilaDoubleDTO comunidad; //null, d, null, d, ?
  private SimulacionFilaStringDTO periodicidadComunidad; //null, numero, null, numero, ?
  private SimulacionFilaDoubleDTO facilities; //Editable//null, d, null, d, ?
  //private SimulacionFilaDatesDTO fechaFacilities; //Editable//null, d, null, d, ?
  private SimulacionFilaDoubleDTO capexEnElActivo; //Editable// null, null, null, d, ?
  //private SimulacionFilaDatesDTO fechaDeCapex; //Editable// null, null, null, d, ?
  private SimulacionFilaDoubleDTO seguros; //Editable// null, null, null, d, ?
  private SimulacionFilaStringDTO periodicidadPagoSeguros; //Editable// null, null, null, d, ?
  private SimulacionFilaDoubleDTO notaria; //Editable// null, null, d, d, null
  //private SimulacionFilaDatesDTO fechaNotaria; //Editable// null, null, d, d, null
  private SimulacionFilaDoubleDTO registro; //Editable// null, null, d, d, null
  //private SimulacionFilaDatesDTO fechaRegistro; //Editable// null, null, d, d, null
  private SimulacionFilaDoubleDTO tasacion; //Editable// null, null, d, d, null
  //private SimulacionFilaDatesDTO fechaTasacion; //Editable// null, null, d, d, null
  private SimulacionFilaDoubleDTO CEE; //Editable// null, null, d, d, null
  private SimulacionFilaDatesDTO fechaCEE; // null, null, d, d, null
  private SimulacionFilaDoubleDTO tramitesDerechoTanteo; //Editable// null, null, d, d, null
  private SimulacionFilaDatesDTO fechaDerechoTanteo; // null, null, d, d, null
  private SimulacionFilaDoubleDTO cargasAsumidas; // null, null, null, d, null
  //private SimulacionFilaDatesDTO fechaDeCargas; // null, null, null, d, null
  private SimulacionFilaDoubleDTO comisionApi; //Editable// null, d, null, d, d
  //private SimulacionFilaDatesDTO fechaDeVentaEstimada; // null, null, null, d, d
  //private SimulacionFilaDatesDTO fechaDelAcuerdoDeVenta; // null, d, null, null, null
  //private SimulacionFilaDoubleDTO dotacionesRefin; // null, null, d, null, null
  //private SimulacionFilaDatesDTO fechaDotacionesRefin; // null, null, d, null, null
}
