package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SimulacionBienIngresosDTO implements Serializable {
  private SimulacionFilaDoubleDTO importeSubasta; //Editable// null, null, null, null, d
  private SimulacionFilaDoubleDTO porcentajeAdjudicacion; //Editable// porcentaje: null, null, null, null, d
  private SimulacionFilaDatesDTO fechaAdjudicacion; // null, null, null, null, d
  private SimulacionFilaDoubleDTO valoracionActualActivo; // d, d, null, d, d
  private SimulacionFilaDatesDTO fechaVentaEstimada; // null, null, null, d, d
  private SimulacionFilaDatesDTO fechaAcuerdoVenta; // null, d, null, null, null
  private SimulacionFilaStringDTO liquidezActivo; // d, d, null, d, d
}
