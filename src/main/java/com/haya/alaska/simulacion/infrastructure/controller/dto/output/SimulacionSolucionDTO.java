package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SimulacionSolucionDTO implements Serializable {
  private SimulacionMultipleDeudaDTO deuda;
  private SimulacionSolucionIngresosDTO ingresos;
  private SimulacionSolucionGastosDTO gastos;
}
