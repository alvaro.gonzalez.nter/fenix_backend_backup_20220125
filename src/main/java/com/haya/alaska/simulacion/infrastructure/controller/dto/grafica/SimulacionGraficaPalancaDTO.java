package com.haya.alaska.simulacion.infrastructure.controller.dto.grafica;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionGraficaPalancaDTO implements Serializable {

  SortedMap<Integer, List<SimulacionGraficaFilaDTO>> anyos;
}
