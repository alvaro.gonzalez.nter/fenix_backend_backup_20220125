package com.haya.alaska.simulacion.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class OtrosGastosAsociadosDTO implements Serializable {
  private Double otroGastos;
  private Date fechaOtroGasto;
}
