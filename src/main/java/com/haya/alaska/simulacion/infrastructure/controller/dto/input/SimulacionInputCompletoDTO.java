package com.haya.alaska.simulacion.infrastructure.controller.dto.input;


import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionResultadoDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SimulacionInputCompletoDTO implements Serializable {
  SimulacionInputDTO simulacion;
  SimulacionResultadoDTO resultado;
}
