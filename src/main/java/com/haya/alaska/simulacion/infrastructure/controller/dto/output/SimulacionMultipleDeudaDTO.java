package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SimulacionMultipleDeudaDTO implements Serializable {
  private SimulacionFilaDoubleDTO deudaActual;//Editable
}
