package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SimulacionContratoDTO implements Serializable {
  private Integer id;
  private String idCarga;
  private SimulacionMultipleDeudaDTO deuda;
  private SimulacionContratoInversionDTO inversion;
  private SimulacionContratoIngresosDTO ingresos;
}
