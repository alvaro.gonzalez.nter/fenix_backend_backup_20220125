package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import com.haya.alaska.simulacion.infrastructure.controller.dto.OtrosGastosAsociadosDTO;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SimulacionSolucionGastosDTO implements Serializable {
  private SimulacionFilaDoubleDTO letrado; //Editable
  private SimulacionFilaDatesDTO fechaLetrado;
  private SimulacionFilaDoubleDTO procurador;//Editable
  private SimulacionFilaDatesDTO fechaProcurador;
  private SimulacionFilaDoubleDTO tasaJudicial;//Editable
  private SimulacionFilaDatesDTO fechaTasaJudicial;
  private List<OtrosGastosAsociadosDTO> nuevosGastosAsociados; // Inputs
}
