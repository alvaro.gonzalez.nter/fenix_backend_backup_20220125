package com.haya.alaska.simulacion.infrastructure.controller.dto.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionVariablesContratoInputDTO implements Serializable {
  private Integer id;
  private String idCarga;
  //Deuda
  private Double deudaActual;
  //Inversion
  private Double importeConcedido;
  private Date fechaImporteConcedido;
//  private Double gastosConstitucion;
  private Date fechaGastosConstitucion;
  //Ingresos
  private Date fechaImpago;
  private Double tipoInteres;
}
