package com.haya.alaska.simulacion.infrastructure.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.simulacion.application.SimulacionUseCase;
import com.haya.alaska.simulacion.infrastructure.controller.dto.ConsolidacionPropuestaInputDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.SimulacionVariablesDto;
import com.haya.alaska.simulacion.infrastructure.controller.dto.grafica.SimulacionGraficaDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.input.*;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionCabeceraDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionDTO;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionDTOToList;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/simulacion")
public class SimulacionController {

  @Autowired SimulacionUseCase simulacionUseCase;

  @ApiOperation(
      value = "Obtener los datos de la cabecera",
      notes = "Obtener los datos de la cabecera")
  @PostMapping("/cabecera")
  public SimulacionCabeceraDTO cabecera(
      @RequestBody SimulacionCabeceraInputDTO simulacionCabeceraInputDTO) throws NotFoundException {
    return simulacionUseCase.cabecera(simulacionCabeceraInputDTO);
  }

  @ApiOperation(
      value = "Comenzar una simulacion",
      notes = "Comenzar una simulacion. No se guarda en bd")
  @PostMapping("/calcularSimulacion")
  public SimulacionDTO calcularSimulacion(@RequestBody SimulacionInputDTO simulacionInputDTO)
    throws Exception {
    return simulacionUseCase.iniciar(simulacionInputDTO);
  }

  @ApiOperation(
      value = "Simular. Calcular VAN, TIR, MFP",
      notes = "Simular. Calcular VAN, TIR, MFP")
  @PostMapping("/simular")
  public SimulacionDTO simular(@RequestBody SimulacionInputDTO simulacionInputDTO)
    throws Exception {
    return simulacionUseCase.simular(simulacionInputDTO);
  }

  @ApiOperation(value = "Obtener grafica trimestral", notes = "Obtener grafica trimestral")
  @PostMapping("/grafica")
  public SimulacionGraficaDTO obtenerGrafica(@RequestBody SimulacionInputDTO simulacionInputDTO)
    throws Exception {
    return simulacionUseCase.obtenerGrafica(simulacionInputDTO);
  }

  @ApiOperation(value = "Guardar simulacion", notes = "Guardar simulacion en bd")
  @PutMapping("/guardar")
  public SimulacionDTO guardarSimulacion(
      @RequestParam(value = "idSimulacion", required = false) Integer idSimulacion,
      @RequestParam(value = "nombre") String nombre,
      @RequestBody SimulacionInputDTO simulacionInputDTO)
    throws Exception {
    return simulacionUseCase.guardarSimulacion(idSimulacion, nombre, simulacionInputDTO);
  }

  @ApiOperation(value = "Obtener lista de historico", notes = "Obtener lista de historico")
  @GetMapping("/historico")
  public ListWithCountDTO<SimulacionDTOToList> getHistorico() throws JsonProcessingException {
    return simulacionUseCase.obtenerHistorico();
  }

  @ApiOperation(value = "Obtener una simulacion por id", notes = "Obtener una simulacion por id")
  @GetMapping("/{idSimulacion}")
  public SimulacionDTO getSimulacion(@PathVariable Integer idSimulacion)
    throws Exception {
    return simulacionUseCase.obtenerSimulacion(idSimulacion);
  }

  @ApiOperation(
      value = "Obtener la cabecera de una simulacion por id",
      notes = "Obtener la cabecera de una simulacion por id")
  @GetMapping("/cabecera/{idSimulacion}")
  public SimulacionCabeceraDTO getCabeceraSimulacion(@PathVariable Integer idSimulacion)
      throws JsonProcessingException, NotFoundException {
    return simulacionUseCase.obtenerCabecera(idSimulacion);
  }

  @ApiOperation(
      value = "Consolidar una simulacion por id",
      notes = "Consolidar una simulacion por id")
  @PostMapping("/consolidar/{idSimulacion}")
  // CREAR LAS PROPUESTAS A PARTIR DE LA SIMULACION
  public SimulacionDTO consolidar(
    @PathVariable Integer idSimulacion,
    @RequestBody ConsolidacionPropuestaInputDTO input,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    return simulacionUseCase.consolidar((Usuario) principal.getPrincipal(), idSimulacion, input);
  }

  @ApiOperation(value = "Descargar excel de simulacion", notes = "Descargar excel de simulacion")
  @PostMapping("/exportar")
  public ResponseEntity<InputStreamResource> exportar(
      @RequestBody SimulacionInputDTO simulacionInputDTO,
      @ApiIgnore CustomUserDetails principal) throws Exception {

    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel =
        excelExport.create(simulacionUseCase.findExcelSimulacion(simulacionInputDTO, (Usuario) principal.getPrincipal()));

    return excelExport.download(
        excel, "informe-simulacion" + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
  }

  @ApiOperation(
      value = "Obtener las variables que se modifican para todas las simulaciones",
      notes = "Obtener las variables que se modifican para todas las simulaciones")
  @GetMapping("/variables")
  public SimulacionVariablesDto getVariables() {
    return simulacionUseCase.getVariables();
  }

  @ApiOperation(
    value = "Obtener las palancas",
    notes = "Obtener las palancas como tipoPropuesta")
  @GetMapping("/palancas")
  public List<CatalogoMinInfoDTO> getPalancas() {
    return simulacionUseCase.getPalancas();
  }

  @ApiOperation(
      value = "Modificar las variables de los catalogos de las simulaciones",
      notes = "Modificar las variables de los catalogos de las simulaciones")
  @PutMapping("/variables/modificar")
  public SimulacionVariablesDto modificarVariables(
      @RequestBody ModificarVariablesSimulacionInputDTO modificarVariablesSimulacionInputDTO)
      throws NotFoundException {
    return simulacionUseCase.modificarVariables(modificarVariablesSimulacionInputDTO);
  }
}
