package com.haya.alaska.simulacion.infrastructure.controller.dto.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionVariablesBienInputDTO implements Serializable {
  private Integer id;
  private String idCarga;
  private Date fechaSolucionAmistosa;
  //Ingresos
  private Double importeSubasta;
//  private Double porcentajeAdjudicacion;
  private Date fechaAdjudicacion;
  private Double valoracionActualActivo;
  private Date fechaVentaEstimada;
  private Date fechaAcuerdoVenta;
  private String liquidezActivo;
  //Gastos
//  private Double ITP;
  private Date fechaITP;
//  private Double AJD;
  //private Date fechaAJD;
//  private Double IBI;
  private String periodicidadIBI;
//  private Double comunidad;
  private String periodicidadComunidad;
//  private Double facilities;
//  private Double capexEnElActivo;
//  private Double seguros;
  private String periodicidadPagoSeguros;
//  private Double notaria;
//  private Double registro;
//  private Double tasacion;
//  private Double CEE;
  private Date fechaCEE;
//  private Double tramitesDerechoTanteo;
  private Date fechaDerechoTanteo;
  private Double cargasAsumidas;
//  private Double comisionApi;
  private Date fechaDotacionesRefin;
}
