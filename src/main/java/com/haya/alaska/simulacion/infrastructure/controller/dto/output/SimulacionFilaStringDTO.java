package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SimulacionFilaStringDTO implements Serializable {
  private String cancelacion;
  private String ventaColateral;
  private String refinanciacion;
  private String dacion;
  private String adjudicacion;

  public SimulacionFilaStringDTO(String todasIguales) {
    this.cancelacion=todasIguales;
    this.ventaColateral=todasIguales;
    this.refinanciacion=todasIguales;
    this.dacion=todasIguales;
    this.adjudicacion=todasIguales;
  }
}
