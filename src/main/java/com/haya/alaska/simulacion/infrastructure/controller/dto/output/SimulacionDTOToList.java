package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionDTOToList implements Serializable {
  Integer id;
  String nombre;
  String idConcatenadoExpediente;
  Integer idExpediente;
  Date fechaUltimoCambio;
  String cliente;
  SimulacionResultadoDTO resultado;
}
