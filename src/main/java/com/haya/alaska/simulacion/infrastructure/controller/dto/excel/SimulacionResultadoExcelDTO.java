package com.haya.alaska.simulacion.infrastructure.controller.dto.excel;

import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.simulacion.infrastructure.controller.dto.output.SimulacionResultadoDTO;

import java.util.Collection;
import java.util.LinkedHashSet;

public class SimulacionResultadoExcelDTO extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();
  static {
    cabeceras.add("Tipo simulacion");
    cabeceras.add("Tipo palanca");
    cabeceras.add("VAN");
    cabeceras.add("TIR");
    cabeceras.add("MFP");
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }

  public SimulacionResultadoExcelDTO(SimulacionResultadoDTO simulacionResultadoDTO, String palanca) {
    this.add("Tipo simulacion",simulacionResultadoDTO.getTipoSimulacion());

    switch (palanca) {
      case "cancelacion":
        this.add("Tipo palanca","Cancelación");
        this.add("VAN",simulacionResultadoDTO.getVan().getCancelacion());
        this.add("TIR",simulacionResultadoDTO.getTir().getCancelacion());
        this.add("MFP",simulacionResultadoDTO.getMfp().getCancelacion());
        break;
      case "ventaColateral":
        this.add("Tipo palanca","Venta de colateral");
        this.add("VAN",simulacionResultadoDTO.getVan().getVentaColateral());
        this.add("TIR",simulacionResultadoDTO.getTir().getVentaColateral());
        this.add("MFP",simulacionResultadoDTO.getMfp().getVentaColateral());
        break;
      case "refinanciacion":
        this.add("Tipo palanca","Refinanciación");
        this.add("VAN",simulacionResultadoDTO.getVan().getRefinanciacion());
        this.add("TIR",simulacionResultadoDTO.getTir().getRefinanciacion());
        this.add("MFP",simulacionResultadoDTO.getMfp().getRefinanciacion());
        break;
      case "dacion":
        this.add("Tipo palanca","Dación");
        this.add("VAN",simulacionResultadoDTO.getVan().getDacion());
        this.add("TIR",simulacionResultadoDTO.getTir().getDacion());
        this.add("MFP",simulacionResultadoDTO.getMfp().getDacion());
        break;
      case "adjudicacion":
        this.add("Tipo palanca","Adjudicación");
        this.add("VAN",simulacionResultadoDTO.getVan().getAdjudicacion());
        this.add("TIR",simulacionResultadoDTO.getTir().getAdjudicacion());
        this.add("MFP",simulacionResultadoDTO.getMfp().getAdjudicacion());
    }

  }
}
