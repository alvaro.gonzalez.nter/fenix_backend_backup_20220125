package com.haya.alaska.simulacion.infrastructure.controller.dto.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SimulacionBusquedaInputDTO implements Serializable {
  Integer idExpediente;
  Integer idCliente;
  Integer idCartera;
  List<Integer> idsContrato;
  List<Integer> idsBien;
  List<Integer> idsInterviniente;
  Date fechaSolucionAmistosa;
}
