package com.haya.alaska.simulacion.infrastructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConsolidacionPropuestaInputDTO implements Serializable {
  private Integer clasePropuesta;
  private Integer tipoPropuesta;
  private Integer subtipoPropuesta;
}
