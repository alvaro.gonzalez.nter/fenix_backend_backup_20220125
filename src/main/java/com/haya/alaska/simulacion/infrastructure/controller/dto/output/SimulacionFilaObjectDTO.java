package com.haya.alaska.simulacion.infrastructure.controller.dto.output;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SimulacionFilaObjectDTO implements Serializable {
  private Object cancelacion;
  private Object ventaColateral;
  private Object refinanciacion;
  private Object dacion;
  private Object adjudicacion;

  public SimulacionFilaObjectDTO(String todasIguales) {
    this.cancelacion=todasIguales;
    this.ventaColateral=todasIguales;
    this.refinanciacion=todasIguales;
    this.dacion=todasIguales;
    this.adjudicacion=todasIguales;
  }
}
