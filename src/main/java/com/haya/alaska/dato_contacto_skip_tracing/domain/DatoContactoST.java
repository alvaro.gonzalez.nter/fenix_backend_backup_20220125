package com.haya.alaska.dato_contacto_skip_tracing.domain;

import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.datos_origen.domain.DatosOrigen;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import com.haya.alaska.tipo_contacto.domain.TipoContacto;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_DATO_CONTACTO_SKIP_TRACING")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_DATO_CONTACTO_SKIP_TRACING")
public class DatoContactoST implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_CONTACTO")
  private TipoContacto tipoContacto;

  @Column(name = "IND_VALIDADO")
  private Boolean Validado;

  @Column(name = "DES_VALOR")
  private String valor;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_VALIDACION")
  private Date fechaValidacion;

  @Column(name = "IND_PRINCIPAL")
  private Boolean principal = false;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_DATO_CONTACTO")
  private DatoContacto datoContacto;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_DATOS_ORIGEN")
  private DatosOrigen datosOrigen;

  //TODO Hacer para el listado de imagenes
  @ElementCollection()
  @CollectionTable(name = "RELA_DATO_CONTACTO_ST_FOTOS", joinColumns = @JoinColumn(name = "ID_DATO_CONTACTO_ST"))
  @Column(name = "ID_FOTO_GESTOR_DOCUMENTAL")
  @NotAudited
  private Set<Long> fotos = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "ID_INTERVINIENTE_SKIP_TRACING")
  private IntervinienteST intervinienteST;

  public void addFoto(long idDocumento) {
    this.fotos.add(idDocumento);
  }



}
