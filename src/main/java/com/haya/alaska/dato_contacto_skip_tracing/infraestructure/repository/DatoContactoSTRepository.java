package com.haya.alaska.dato_contacto_skip_tracing.infraestructure.repository;

import com.haya.alaska.dato_contacto_skip_tracing.domain.DatoContactoST;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DatoContactoSTRepository extends JpaRepository<DatoContactoST, Integer> {
  List<DatoContactoST> findAllByDatoContactoId(Integer idContacto);
  DatoContactoST findByTipoContactoIdAndIntervinienteSTIdAndPrincipalIsTrue(Integer tipoContactoId,Integer intervinienteID);


}
