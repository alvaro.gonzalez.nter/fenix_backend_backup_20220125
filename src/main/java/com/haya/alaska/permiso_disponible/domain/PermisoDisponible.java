package com.haya.alaska.permiso_disponible.domain;

import com.haya.alaska.opcion_permiso.domain.OpcionPermiso;
import com.haya.alaska.permiso.domain.Permiso;
import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Audited
@AuditTable(value = "HIST_RELA_PERMISO_DISPONIBLE")
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "RELA_PERMISO_DISPONIBLE",
  uniqueConstraints = {@UniqueConstraint(columnNames = {
    "ID_PERMISO", "ID_OPCION_PERMISO"
  })})

public class PermisoDisponible {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID", length = 50)
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERMISO")
  private Permiso permiso;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_OPCION_PERMISO")
  private OpcionPermiso opcionPermiso;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERMISO_DISPONIBLE")
  @NotAudited
  private List<PermisoAsignado> permisosAsignados = new ArrayList<>();
}
