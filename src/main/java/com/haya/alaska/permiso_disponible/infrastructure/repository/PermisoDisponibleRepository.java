package com.haya.alaska.permiso_disponible.infrastructure.repository;

import com.haya.alaska.permiso_disponible.domain.PermisoDisponible;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermisoDisponibleRepository extends JpaRepository<PermisoDisponible, Integer> {
  List<PermisoDisponible> findAllByPermisoId(Integer permisoId);
}
