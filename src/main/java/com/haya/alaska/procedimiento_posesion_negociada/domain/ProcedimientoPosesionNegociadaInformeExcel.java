package com.haya.alaska.procedimiento_posesion_negociada.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ProcedimientoPosesionNegociadaInformeExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Procedure ID");
      cabeceras.add("Active ID");
      cabeceras.add("Demand number");
      cabeceras.add("Court order");
      cabeceras.add("Court");
      cabeceras.add("Province Court");
      cabeceras.add("City Court");
      cabeceras.add("Cause of claim");
      cabeceras.add("Action type");
      cabeceras.add("Procedure type");
      cabeceras.add("Demand status");
      cabeceras.add("Admission to Processing Date");
      cabeceras.add("Amount recovered");
      cabeceras.add("Amount recovered costs");
      cabeceras.add("Date of 1st Instance Judgment");
      cabeceras.add("Date of 2st Instance Judgment");
      cabeceras.add("Position");
      cabeceras.add("Customer in person");
      cabeceras.add("Applicants");
      cabeceras.add("Respondents");
      cabeceras.add("Other positions");
      cabeceras.add("Lawyers");
      cabeceras.add("Manager");
      cabeceras.add("Procurator");

    } else {

      cabeceras.add("Id Procedimiento");
      cabeceras.add("Id Activo");
      cabeceras.add("Numero Demanda");
      cabeceras.add("Auto");
      cabeceras.add("Juzgado");
      cabeceras.add("Província Juzgado");
      cabeceras.add("Ciudad Juzgado");
      cabeceras.add("Causa Reclamación");
      cabeceras.add("Tipo De Acción");
      cabeceras.add("Tipo Procedimiento");
      cabeceras.add("Estado Demanda");
      cabeceras.add("Fecha Admisión A Trámite");
      cabeceras.add("Importe Recuperado");
      cabeceras.add("Importe Recuperado Costas");
      cabeceras.add("Fecha Sentencia 1ª Instancia");
      cabeceras.add("Fecha Sentencia 2ª Instancia");
      cabeceras.add("Posición");
      cabeceras.add("Cliente Personado");
      cabeceras.add("Demandantes");
      cabeceras.add("Demandados");
      cabeceras.add("Otras posiciones");
      cabeceras.add("Letrados");
      cabeceras.add("Gestor");
      cabeceras.add("Procurador");
    }
  }

  public ProcedimientoPosesionNegociadaInformeExcel(
    ProcedimientoPosesionNegociada procedimientoPosesionNegociada) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Procedure ID", procedimientoPosesionNegociada.getId());
      this.add("Active ID", procedimientoPosesionNegociada.getActivo());
      this.add("Demand number", procedimientoPosesionNegociada.getNumeroDemanda());
      this.add("Court order", procedimientoPosesionNegociada.getAutos());
      this.add("Court", procedimientoPosesionNegociada.getJuzgado());
      this.add("Province Court", procedimientoPosesionNegociada.getProvinciaJuzgado());
      this.add("City Court", procedimientoPosesionNegociada.getCiudadJuzgado());
      this.add("Cause of claim", procedimientoPosesionNegociada.getCausaReclamacion());
      this.add("Action type", procedimientoPosesionNegociada.getTipoAccion());
      this.add("Procedure type", procedimientoPosesionNegociada.getTipo());
      this.add("Demand status", procedimientoPosesionNegociada.getEstado());
      this.add("Admission to Processing Date", procedimientoPosesionNegociada.getFechaAdmisionATramite());
      this.add("Amount recovered", procedimientoPosesionNegociada.getImporteRecuperado());
      this.add("Amount recovered costs", procedimientoPosesionNegociada.getImporteRecuperadoCostas());
      this.add("Date of 1st Instance Judgment", procedimientoPosesionNegociada.getFechaSentencia1Instancia());
      this.add("Date of 2st Instance Judgment", procedimientoPosesionNegociada.getFechaSentencia2Instancia());
      this.add("Position", procedimientoPosesionNegociada.getPosicion());
      this.add("Customer in person", procedimientoPosesionNegociada.getClientePersonado());
      this.add("Applicants", procedimientoPosesionNegociada.getDemandante());
      this.add("Respondents", procedimientoPosesionNegociada.getDemandados().stream().findFirst().get().getNombre());
      this.add("Other positions", procedimientoPosesionNegociada.getOtrasPosiciones());
      this.add("Lawyers", procedimientoPosesionNegociada.getLetrado());
      this.add("Manager", procedimientoPosesionNegociada.getGestor());
      this.add("Procurator", procedimientoPosesionNegociada.getProcurador());

    }

    else{

    this.add("Id Procedimiento", procedimientoPosesionNegociada.getId());
    this.add("Id Activo", procedimientoPosesionNegociada.getActivo());
    this.add("Numero Demanda", procedimientoPosesionNegociada.getNumeroDemanda());
    this.add("Auto", procedimientoPosesionNegociada.getAutos());
    this.add("Juzgado", procedimientoPosesionNegociada.getJuzgado());
    this.add("Provincia Juzgado", procedimientoPosesionNegociada.getProvinciaJuzgado());
    this.add("Ciudad Juzgado", procedimientoPosesionNegociada.getCiudadJuzgado());
    this.add("Causa Reclamación", procedimientoPosesionNegociada.getCausaReclamacion());
    this.add("Tipo De Acción", procedimientoPosesionNegociada.getTipoAccion());
    this.add("Tipo Procedimiento", procedimientoPosesionNegociada.getTipo());
    this.add("Estado Demanda", procedimientoPosesionNegociada.getEstado());
    this.add("Fecha Admisión A Trámite", procedimientoPosesionNegociada.getFechaAdmisionATramite());
    this.add("Importe Recuperado", procedimientoPosesionNegociada.getImporteRecuperado());
    this.add("Importe Recuperado Costas", procedimientoPosesionNegociada.getImporteRecuperadoCostas());
    this.add("Fecha Sentencia 1ª Instancia", procedimientoPosesionNegociada.getFechaSentencia1Instancia());
    this.add("Fecha Sentencia 2ª Instancia", procedimientoPosesionNegociada.getFechaSentencia2Instancia());
    this.add("Posición", procedimientoPosesionNegociada.getPosicion());
    this.add("Cliente Personado", procedimientoPosesionNegociada.getClientePersonado());
    this.add("Demandantes", procedimientoPosesionNegociada.getDemandante());
    this.add("Demandados", procedimientoPosesionNegociada.getDemandados().stream().findFirst().get().getNombre());
    this.add("Otras posiciones", procedimientoPosesionNegociada.getOtrasPosiciones());
    this.add("Letrados", procedimientoPosesionNegociada.getLetrado());
    this.add("Gestor", procedimientoPosesionNegociada.getGestor());
    this.add("Procurador", procedimientoPosesionNegociada.getProcurador());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
