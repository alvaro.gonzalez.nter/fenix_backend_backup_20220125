package com.haya.alaska.procedimiento_posesion_negociada.domain;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.estado_procedimiento.domain.EstadoProcedimiento;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.tipo_accion_procedimiento.domain.TipoAccionProcedimiento;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import lombok.Data;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_PROCEDIMIENTO_POSESION_NEGOCIADA")
@Entity
@Table(name = "MSTR_PROCEDIMIENTO_POSESION_NEGOCIADA")
@Data
public class ProcedimientoPosesionNegociada implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  private BienPosesionNegociada bien;

  @Column(name = "DES_NUMERO_DEMANDA")
  private String numeroDemanda;

  @Column(name = "DES_NUM_AUTOS")
  private String autos;

  @Column(name = "DES_JUZGADO")
  private String juzgado;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA_JUZGADO")
  private Provincia provinciaJuzgado;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO_JUZGADO")
  private Municipio municipioJuzgado;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CIUDAD_JUZGADO")
  private Localidad ciudadJuzgado;

  @Column(name = "DES_CAUSA_RECLAMACION")
  private String causaReclamacion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_PROCEDIMIENTO")
  private TipoProcedimiento tipo;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ACCION_PROCEDIMIENTO")
  private TipoAccionProcedimiento tipoAccion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_PROCEDIMIENTO")
  private EstadoProcedimiento estado;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ADMISION_A_TRAMITE")
  private Date fechaAdmisionATramite;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PRESENTACION_DEMANDA")
  private Date fechaPresentacionDemanda;

  @Column(name = "NUM_IMPORTE_RECUPERADO")
  private Double importeRecuperado;
  @Column(name = "NUM_IMPORTE_RECUPERADO_COSTAS")
  private Double importeRecuperadoCostas;


  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_SENTENCIA_PRIMERA_INSTANCIA")
  private Date fechaSentencia1Instancia;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_SENTENCIA_SEGUNDA_INSTANCIA")
  private Date fechaSentencia2Instancia;

  @Column(name = "DES_POSICION")
  private String posicion;
  @Column(name = "DES_CLIENTE_PERSONADO")
  private String clientePersonado;

  @Column(name = "DES_DEMANDANTE")
  private String demandante;

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
    name = "RELA_PROCEDIMIENTO_POSESION_NEGOCIADA_OCUPANTE",
    joinColumns = @JoinColumn(name = "ID_PROCEDIMIENTO_POSESION_NEGOCIADA"),
    inverseJoinColumns = @JoinColumn(name = "ID_DEMANDADO"))
  @AuditJoinTable(name = "HIST_RELA_PROCEDIMIENTO_POSESION_NEGOCIADA_OCUPANTE")
  private Set<Ocupante> demandados = new HashSet<>();

  @Column(name = "DES_OTRAS_POSICIONES")
  private String otrasPosiciones;
  @Column(name = "DES_LETRADO")
  private String letrado;
  @Column(name = "DES_GESTOR")
  private String gestor;
  @Column(name = "DES_PROCURADOR")
  private String procurador;
}
