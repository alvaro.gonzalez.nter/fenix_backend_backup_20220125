package com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller;

import com.haya.alaska.bien.infrastructure.controller.dto.BienInputDto;
import com.haya.alaska.bien.infrastructure.controller.dto.BienOutputDto;
import com.haya.alaska.procedimiento_posesion_negociada.application.ProcedimientoPosesionNegociadaUseCase;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaDTOToList;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaInputDTO;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaOutputDTO;
import com.haya.alaska.shared.dto.BienDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientePosesionNegociada/{idExpediente}/bien/{idBienPosesionNegociada}/procedimientos")
public class ProcedimientoPosesionNegociadaController {
  @Autowired
  private ProcedimientoPosesionNegociadaUseCase ppnUseCase;

  @ApiOperation(value = "Listado procedimientos",
    notes = "Devuelve todas los procedimientos posesión negociada en el activo pasado")
  @GetMapping
  public ListWithCountDTO<ProcedimientoPosesionNegociadaDTOToList> getAll(
    @PathVariable Integer idBienPosesionNegociada,
    @RequestParam(value = "id", required = false) Integer id,
    @RequestParam(value = "numeroDemanda", required = false) String numeroDemanda,
    @RequestParam(value = "autos", required = false) String autos,
    @RequestParam(value = "juzgado", required = false) String juzgado,
    @RequestParam(value = "tipoProcedimiento", required = false) String tipoProcedimiento,
    @RequestParam(value = "fechaPresentacionDemanda", required = false) String fechaPresentacionDemanda,
    @RequestParam(value = "clientePersonado", required = false) String clientePersonado,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) throws NotFoundException {

    return ppnUseCase.getAllPPN(
      idBienPosesionNegociada, id, numeroDemanda, autos, juzgado, tipoProcedimiento,
      fechaPresentacionDemanda, clientePersonado, orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Obtención del procedimiento",
    notes = "Devuelve el procedimiento de posesión negociada con el id enviado")
  @GetMapping("/{idProcedimiento}")
  public ProcedimientoPosesionNegociadaOutputDTO getPPN(
    @PathVariable Integer idProcedimiento) throws NotFoundException {

    return ppnUseCase.getPPN(idProcedimiento);
  }

  @ApiOperation(value = "Crear procedimiento", notes = "Crea el procedimiento a partir del DTO enviado, y lo asigna al bien")
  @PostMapping
  public ProcedimientoPosesionNegociadaOutputDTO postPPN(@PathVariable("idBienPosesionNegociada") Integer idBienPosesionNegociada,
                                                         @RequestBody @Valid ProcedimientoPosesionNegociadaInputDTO input)
    throws Exception {
    return ppnUseCase.postPPN(idBienPosesionNegociada, input);
  }

  @ApiOperation(value = "Modificar procedimiento", notes = "Modifica el procedimiento con el id enviado a partir del DTO enviado")
  @PutMapping("/{idProcedimiento}")
  public ProcedimientoPosesionNegociadaOutputDTO putPPN(@PathVariable("idProcedimiento") Integer idProcedimiento,
                                                         @RequestBody @Valid ProcedimientoPosesionNegociadaInputDTO input)
    throws Exception {
    return ppnUseCase.putPPN(idProcedimiento, input);
  }

  @ApiOperation(value = "Borrar procedimiento", notes = "Borra lógicamente el procedimiento con el id enviado.")
  @DeleteMapping("/{idProcedimiento}")
  public ProcedimientoPosesionNegociadaOutputDTO deletePPN(@PathVariable("idProcedimiento") Integer idProcedimiento)
    throws Exception {
    return ppnUseCase.deletePPN(idProcedimiento);
  }
}
