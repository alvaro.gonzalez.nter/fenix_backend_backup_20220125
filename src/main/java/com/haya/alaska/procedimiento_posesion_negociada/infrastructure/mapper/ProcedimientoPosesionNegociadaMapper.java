package com.haya.alaska.procedimiento_posesion_negociada.infrastructure.mapper;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.estado_procedimiento.infrastructure.repository.EstadoProcedimientoRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.mapper.OcupanteMapper;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaInputDTO;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaOutputDTO;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_accion_procedimiento.infrastructure.repository.TipoAccionProcedimientoRepository;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProcedimientoPosesionNegociadaMapper {
  @Autowired
  ProvinciaRepository provinciaRepository;
  @Autowired
  MunicipioRepository municipioRepository;
  @Autowired
  LocalidadRepository localidadRepository;
  @Autowired
  TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired
  TipoAccionProcedimientoRepository tipoAccionProcedimientoRepository;
  @Autowired
  EstadoProcedimientoRepository estadoProcedimientoRepository;
  @Autowired
  OcupanteMapper ocupanteMapper;
  @Autowired
  OcupanteRepository ocupanteRepository;

  public ProcedimientoPosesionNegociadaOutputDTO entityToOutput(ProcedimientoPosesionNegociada ppn){
    ProcedimientoPosesionNegociadaOutputDTO output = new ProcedimientoPosesionNegociadaOutputDTO();
    BeanUtils.copyProperties(ppn, output);

    output.setProvinciaJuzgado(ppn.getProvinciaJuzgado() != null ?
      new CatalogoMinInfoDTO(ppn.getProvinciaJuzgado()) : null);
    output.setMunicipioJuzgado(ppn.getMunicipioJuzgado() != null ?
      new CatalogoMinInfoDTO(ppn.getMunicipioJuzgado()) : null);
    output.setCiudadJuzgado(ppn.getCiudadJuzgado() != null ?
      new CatalogoMinInfoDTO(ppn.getCiudadJuzgado()) : null);
    output.setTipo(ppn.getTipo() != null ?
      new CatalogoMinInfoDTO(ppn.getTipo()) : null);
    output.setTipoAccion(ppn.getTipoAccion() != null ?
      new CatalogoMinInfoDTO(ppn.getTipoAccion()) : null);
    output.setEstado(ppn.getEstado() != null ?
      new CatalogoMinInfoDTO(ppn.getEstado()) : null);

    OcupanteBien ob = new OcupanteBien();
    ob.setOrden(-1);

    output.setDemandados(ppn.getDemandados().stream().map(ocupante -> ocupanteMapper.OcupanteToOcupanteDTOToList(ocupante, ob)).collect(Collectors.toList()));
    output.setDemandadosIds(ppn.getDemandados().stream().map(Ocupante::getId).collect(Collectors.toList()));
    return output;
  }

  public ProcedimientoPosesionNegociada inputToEntity(ProcedimientoPosesionNegociadaInputDTO input, ProcedimientoPosesionNegociada ppn) throws NotFoundException {
    BeanUtils.copyProperties(input, ppn);

    ppn.setProvinciaJuzgado(input.getProvinciaJuzgado() != null ?
      provinciaRepository.findById(input.getProvinciaJuzgado()).orElseThrow(() -> new
        NotFoundException("Provincia", input.getProvinciaJuzgado())) : null);
    ppn.setMunicipioJuzgado(input.getMunicipioJuzgado() != null ?
      municipioRepository.findById(input.getMunicipioJuzgado()).orElseThrow(() -> new
        NotFoundException("Municipio", input.getMunicipioJuzgado())) : null);
    ppn.setCiudadJuzgado(input.getCiudadJuzgado() != null ?
      localidadRepository.findById(input.getCiudadJuzgado()).orElseThrow(() -> new
        NotFoundException("Ciudad", input.getCiudadJuzgado())) : null);
    ppn.setTipo(input.getTipo() != null ?
      tipoProcedimientoRepository.findById(input.getTipo()).orElseThrow(() -> new
        NotFoundException("TipoProcedimiento", input.getTipo())) : null);
    ppn.setTipoAccion(input.getTipoAccion() != null ?
      tipoAccionProcedimientoRepository.findById(input.getTipoAccion()).orElseThrow(() -> new
        NotFoundException("TipoAccionProcedimiento", input.getTipoAccion())) : null);
    ppn.setEstado(input.getEstado() != null ?
      estadoProcedimientoRepository.findById(input.getEstado()).orElseThrow(() -> new
        NotFoundException("EstadoProcedimiento", input.getEstado())) : null);

    List<Ocupante> demandados = ocupanteRepository.findAllById(input.getDemandados());
    ppn.setDemandados(new HashSet<>(demandados));

    return ppn;
  }
}
