package com.haya.alaska.procedimiento_posesion_negociada.infrastructure.repository;

import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProcedimientoPosesionNegociadaRepository extends JpaRepository<ProcedimientoPosesionNegociada, Integer> {

    Optional<ProcedimientoPosesionNegociada> findByNumeroDemanda(String stringValue);

    List<ProcedimientoPosesionNegociada> findAllByDemandadosId(Integer idInterviniente);


}
