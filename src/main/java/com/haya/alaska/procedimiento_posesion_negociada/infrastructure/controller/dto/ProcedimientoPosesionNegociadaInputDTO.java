package com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProcedimientoPosesionNegociadaInputDTO implements Serializable {
  private Boolean activo;
  private String numeroDemanda;
  private String autos;

  private String juzgado;
  private Integer provinciaJuzgado;
  private Integer municipioJuzgado;
  private Integer ciudadJuzgado;

  private String causaReclamacion;

  private Integer tipo;
  private Integer tipoAccion;
  private Integer estado;

  private Date fechaAdmisionATramite;
  private Date fechaPresentacionDemanda;

  private Double importeRecuperado;
  private Double importeRecuperadoCostas;

  private Date fechaSentencia1Instancia;
  private Date fechaSentencia2Instancia;

  private String posicion;
  private String clientePersonado;

  private String demandante;

  private Set<Integer> demandados;

  private String otrasPosiciones;
  private String letrado;
  private String gestor;
  private String procurador;
}
