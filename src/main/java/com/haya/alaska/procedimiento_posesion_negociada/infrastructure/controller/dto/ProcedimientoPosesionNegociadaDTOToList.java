package com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import lombok.*;
import org.springframework.beans.BeanUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProcedimientoPosesionNegociadaDTOToList implements Serializable {
  Integer id;
  String numeroDemanda;
  String autos;
  String juzgado;
  String tipo;
  String estado;
  String importeDemanda;
  String situacionProcesal;
  CatalogoMinInfoDTO tipoProcedimiento;
  Date fechaPresentacionDemanda;
  String clientePersonado;

  public ProcedimientoPosesionNegociadaDTOToList(ProcedimientoPosesionNegociada p){
    BeanUtils.copyProperties(p, this);
    this.tipoProcedimiento = p.getTipo() != null ? new CatalogoMinInfoDTO(p.getTipo()) : null;
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();
    if (defaultLocal == null || defaultLocal.equals("es")) {
      this.estado = p.getEstado() != null ? p.getEstado().getValor() : null;
      this.tipo = p.getTipo() != null ? p.getTipo().getValor() : null;
    }
    else if (defaultLocal.equals("en")) {
      this.estado = p.getEstado() != null ? p.getEstado().getValorIngles() : null;
      this.tipo = p.getTipo() != null ? p.getTipo().getValorIngles() : null;
    }

  }
}
