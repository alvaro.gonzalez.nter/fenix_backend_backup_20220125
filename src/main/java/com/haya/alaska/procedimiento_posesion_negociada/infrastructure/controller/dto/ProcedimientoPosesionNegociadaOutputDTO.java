package com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteDTOToList;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProcedimientoPosesionNegociadaOutputDTO implements Serializable {
  private Integer id;
  private Boolean activo;
  private String numeroDemanda;
  private String autos;

  private String juzgado;
  private CatalogoMinInfoDTO provinciaJuzgado;
  private CatalogoMinInfoDTO municipioJuzgado;
  private CatalogoMinInfoDTO ciudadJuzgado;

  private String causaReclamacion;

  private CatalogoMinInfoDTO tipo;
  private CatalogoMinInfoDTO tipoAccion;
  private CatalogoMinInfoDTO estado;

  private Date fechaAdmisionATramite;
  private Date fechaPresentacionDemanda;

  private Double importeRecuperado;
  private Double importeRecuperadoCostas;

  private Date fechaSentencia1Instancia;
  private Date fechaSentencia2Instancia;

  private String posicion;
  private String clientePersonado;

  private String demandante;

  private List<OcupanteDTOToList> demandados;
  private List<Integer> demandadosIds;

  private String otrasPosiciones;
  private String letrado;
  private String gestor;
  private String procurador;
}
