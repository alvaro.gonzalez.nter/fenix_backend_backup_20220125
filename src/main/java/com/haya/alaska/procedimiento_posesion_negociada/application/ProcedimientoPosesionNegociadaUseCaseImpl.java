package com.haya.alaska.procedimiento_posesion_negociada.application;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada_;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.catalogo.domain.Catalogo_;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada_;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaDTOToList;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaInputDTO;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaOutputDTO;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.mapper.ProcedimientoPosesionNegociadaMapper;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.repository.ProcedimientoPosesionNegociadaRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento_;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ProcedimientoPosesionNegociadaUseCaseImpl implements ProcedimientoPosesionNegociadaUseCase{
  @PersistenceContext
  private EntityManager entityManager;
  @Autowired
  ProcedimientoPosesionNegociadaRepository pPNRepository;
  @Autowired
  ProcedimientoPosesionNegociadaMapper pPNMapper;
  @Autowired
  BienPosesionNegociadaRepository bienPosesionNegociadaRepository;


  public ProcedimientoPosesionNegociadaOutputDTO getPPN(Integer id) throws NotFoundException {
    ProcedimientoPosesionNegociada ppn = pPNRepository.findById(id).orElseThrow(() -> new
      NotFoundException("ProcedimientoPosesionNegociada", id));
    ProcedimientoPosesionNegociadaOutputDTO result = pPNMapper.entityToOutput(ppn);
    return result;
  }

  public ListWithCountDTO<ProcedimientoPosesionNegociadaDTOToList> getAllPPN(
    Integer idBien, Integer id, String numeroDemanda, String autos, String juzgado,
    String tipoProcedimiento, String fechaPresentacionDemanda, String clientePersonado,
    String orderField, String orderDirection, Integer size, Integer page){
    List<ProcedimientoPosesionNegociada> all = filterPPN(idBien, id, numeroDemanda, autos, juzgado, tipoProcedimiento,
      fechaPresentacionDemanda, clientePersonado, orderField, orderDirection);
    List<ProcedimientoPosesionNegociada> procedimientos = all.stream().skip(size * page).limit(size).collect(Collectors.toList());
    List<ProcedimientoPosesionNegociadaDTOToList> result = procedimientos.stream().map(ProcedimientoPosesionNegociadaDTOToList::new).collect(Collectors.toList());
    return new ListWithCountDTO<>(result, all.size());
  }

  public ProcedimientoPosesionNegociadaOutputDTO postPPN(Integer idBien, ProcedimientoPosesionNegociadaInputDTO input) throws NotFoundException {
    ProcedimientoPosesionNegociada ppn = new ProcedimientoPosesionNegociada();
    pPNMapper.inputToEntity(input, ppn);
    BienPosesionNegociada bpp = bienPosesionNegociadaRepository.findById(idBien).orElseThrow(() -> new
      NotFoundException("BienPosesionNegociada", idBien));
    ppn.setBien(bpp);
    ProcedimientoPosesionNegociada ppnSaved = pPNRepository.save(ppn);
    ProcedimientoPosesionNegociadaOutputDTO result = pPNMapper.entityToOutput(ppnSaved);
    return result;
  }

  public ProcedimientoPosesionNegociadaOutputDTO putPPN(Integer id, ProcedimientoPosesionNegociadaInputDTO input) throws NotFoundException {
    ProcedimientoPosesionNegociada ppn = pPNRepository.findById(id).orElseThrow(() -> new
      NotFoundException("ProcedimientoPosesionNegociada", id));
    var procedimientoNuevo = pPNMapper.inputToEntity(input, ppn);
    ProcedimientoPosesionNegociada ppnSaved = pPNRepository.save(procedimientoNuevo);
    ProcedimientoPosesionNegociadaOutputDTO result = pPNMapper.entityToOutput(ppnSaved);
    return result;
  }

  public ProcedimientoPosesionNegociadaOutputDTO deletePPN(Integer id) throws NotFoundException {
    ProcedimientoPosesionNegociada ppn = pPNRepository.findById(id).orElseThrow(() -> new
      NotFoundException("ProcedimientoPosesionNegociada", id));
    ppn.setActivo(false);
    ProcedimientoPosesionNegociada ppnSaved = pPNRepository.save(ppn);
    ProcedimientoPosesionNegociadaOutputDTO result = pPNMapper.entityToOutput(ppnSaved);
    return result;
  }

  @Override
  public List<ProcedimientoPosesionNegociada> filterPPN(Integer idBien,
    Integer id, String numeroDemanda, String autos, String juzgado, String tipoProcedimiento,
    String fechaPresentacionDemanda, String clientePersonado, String orderField, String orderDirection){
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<ProcedimientoPosesionNegociada> query = cb.createQuery(ProcedimientoPosesionNegociada.class);

    Root<ProcedimientoPosesionNegociada> root = query.from(ProcedimientoPosesionNegociada.class);
    List<Predicate> predicates = new ArrayList<>();

    Join<ProcedimientoPosesionNegociada, BienPosesionNegociada> join = root.join(
      ProcedimientoPosesionNegociada_.bien, JoinType.INNER);
    Predicate onCond = cb.equal(join.get(BienPosesionNegociada_.id), idBien);
    join.on(onCond);

    predicates.add(cb.isTrue(root.get(ProcedimientoPosesionNegociada_.ACTIVO)));

    if (id != null) {
      predicates.add(cb.like(root.get(ProcedimientoPosesionNegociada_.id).as(String.class),
        "%" + id + "%"));
    }
    if (autos != null) {
      predicates.add(cb.like(root.get(ProcedimientoPosesionNegociada_.autos).as(String.class),
        "%" + autos + "%"));
    }
    if (numeroDemanda != null) {
      predicates.add(cb.like(root.get(ProcedimientoPosesionNegociada_.numeroDemanda),
        "%" + numeroDemanda + "%"));
    }
    if (juzgado != null) {
      predicates.add(cb.like(root.get(ProcedimientoPosesionNegociada_.juzgado),
        "%" + juzgado + "%"));
    }
    if (clientePersonado != null) {
      predicates.add(cb.like(root.get(ProcedimientoPosesionNegociada_.clientePersonado),
        "%" + clientePersonado + "%"));
    }
    if (fechaPresentacionDemanda != null) {
      predicates.add(cb.like(root.get(ProcedimientoPosesionNegociada_.fechaPresentacionDemanda).as(String.class),
        "%" + fechaPresentacionDemanda + "%"));
    }

    addOnCond(cb, root, ProcedimientoPosesionNegociada_.tipo, tipoProcedimiento);

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    if (orderField != null) {
      if (orderField.equals("tipoProcedimiento")){
        Join<ProcedimientoPosesionNegociada, TipoProcedimiento> join1 = root.join(ProcedimientoPosesionNegociada_.tipo, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(join1.get(TipoProcedimiento_.VALOR)));
        } else {
          rs.orderBy(cb.asc(join1.get(TipoProcedimiento_.VALOR)));
        }
      } else {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
          rs.orderBy(cb.desc(root.get(orderField)));
        else
          rs.orderBy(cb.asc(root.get(orderField)));
      }
    }
    Set<ParameterExpression<?>> q = query.getParameters();

    List<ProcedimientoPosesionNegociada> result = entityManager.createQuery(query).getResultList();

    return result;
  }

  private void addOnCond(CriteriaBuilder cb, Root<ProcedimientoPosesionNegociada> root,
                         SingularAttribute<ProcedimientoPosesionNegociada, ?> singularAtrribute, String campo) {
    if (campo == null)
      return;
    Join<ProcedimientoPosesionNegociada, ?> estadoJoin = root.join(singularAtrribute, JoinType.INNER);
    Predicate onCond = cb.like(estadoJoin.get(Catalogo_.VALOR), "%" + campo + "%");
    estadoJoin.on(onCond);
  }
}
