package com.haya.alaska.procedimiento_posesion_negociada.application;

import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaDTOToList;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaInputDTO;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.controller.dto.ProcedimientoPosesionNegociadaOutputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;

import java.util.List;

public interface ProcedimientoPosesionNegociadaUseCase {
  ProcedimientoPosesionNegociadaOutputDTO getPPN(Integer id) throws NotFoundException;
  ListWithCountDTO<ProcedimientoPosesionNegociadaDTOToList> getAllPPN(
    Integer idBien, Integer id, String numeroDemanda, String autos, String juzgado,
    String tipoProcedimiento, String fechaPresentacionDemanda, String clientePersonado,
    String orderField, String orderDirection, Integer size, Integer page);
  ProcedimientoPosesionNegociadaOutputDTO postPPN(Integer idBien, ProcedimientoPosesionNegociadaInputDTO input) throws NotFoundException;
  ProcedimientoPosesionNegociadaOutputDTO putPPN(Integer id, ProcedimientoPosesionNegociadaInputDTO input) throws NotFoundException;
  ProcedimientoPosesionNegociadaOutputDTO deletePPN(Integer id) throws NotFoundException;
  List<ProcedimientoPosesionNegociada> filterPPN(Integer idBien,
                                                 Integer id, String numeroDemanda, String autos, String juzgado, String tipoProcedimiento,
                                                 String fechaPresentacionDemanda, String clientePersonado, String orderField, String orderDirection);
}
