package com.haya.alaska.criterio_ms_remanente.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.criterio_ms_remanente.domain.CriterioMSRemanente;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CriterioMSRemanenteRepository extends CatalogoRepository<CriterioMSRemanente, Integer> {
  List<CriterioMSRemanente> findAllByCarteraId(Integer id);
  List<CriterioMSRemanente> findAllByCarteraIdAndActivoIsTrue(Integer ido);
}
