package com.haya.alaska.tipo_acreedor.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_acreedor.domain.TipoAcreedor;

@Repository
public interface TipoAcreedorRepository extends CatalogoRepository<TipoAcreedor, Integer> {
	 
}
