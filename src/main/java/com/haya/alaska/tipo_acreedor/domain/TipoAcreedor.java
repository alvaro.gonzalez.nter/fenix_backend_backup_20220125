package com.haya.alaska.tipo_acreedor.domain;

import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.catalogo.domain.Catalogo;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_ACREEDOR")
@Entity
@Table(name = "LKUP_TIPO_ACREEDOR")
public class TipoAcreedor extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ACREEDOR")
  @NotAudited
  private Set<Carga> cargas = new HashSet<>();
}
