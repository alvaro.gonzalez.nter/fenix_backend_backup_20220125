package com.haya.alaska.variable_ajd.controller.dto.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class ModificarVariableAJDInputDTO implements Serializable {
  private Integer id;
  private Double nuevoPorcentaje;
}
