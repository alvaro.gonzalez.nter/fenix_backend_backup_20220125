package com.haya.alaska.variable_ajd.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class VariableAJDDTO implements Serializable {
  private Integer id;
  private Double porcentaje;
  private String comunidadAutonoma;
}
