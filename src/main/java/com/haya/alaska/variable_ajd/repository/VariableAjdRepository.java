package com.haya.alaska.variable_ajd.repository;

import com.haya.alaska.variable_ajd.domain.VariableAjd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VariableAjdRepository extends JpaRepository<VariableAjd, Integer> {
  Optional<VariableAjd> findByComunidadAutonomaId(Integer id);
}
