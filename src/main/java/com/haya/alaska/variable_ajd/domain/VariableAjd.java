package com.haya.alaska.variable_ajd.domain;

import com.haya.alaska.comunidad_autonoma.domain.ComunidadAutonoma;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_MSTR_VARIABLE_AJD")
@Entity
@Getter
@Setter
@Table(name = "MSTR_VARIABLE_AJD")
public class VariableAjd implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "NUM_PORCENTAJE")
  private Double porcentaje;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_COMUNIDAD_AUTONOMA")
  public ComunidadAutonoma comunidadAutonoma;
}
