package com.haya.alaska.variable_ajd.mapper;

import com.haya.alaska.variable_ajd.controller.dto.VariableAJDDTO;
import com.haya.alaska.variable_ajd.domain.VariableAjd;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class VariableAjdMapper {

  public VariableAJDDTO entidadADto(VariableAjd entidad) {
    VariableAJDDTO variableAJDDTO = new VariableAJDDTO();
    BeanUtils.copyProperties(entidad,variableAJDDTO);
    variableAJDDTO.setComunidadAutonoma(entidad.comunidadAutonoma.getValor());
    return variableAJDDTO;
  }
}
