package com.haya.alaska.modelo_carta.infraestructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.modelo_carta.domain.ModeloCarta;

public interface ModeloCartaRepository extends CatalogoRepository<ModeloCarta, Integer>{}
