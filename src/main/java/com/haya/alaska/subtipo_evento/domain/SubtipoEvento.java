package com.haya.alaska.subtipo_evento.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.importancia_evento.domain.ImportanciaEvento;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.periodicidad_evento.domain.PeriodicidadEvento;
import com.haya.alaska.resultado_evento.domain.ResultadoEvento;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_SUBTIPO_EVENTO")
@Entity
@Table(name = "LKUP_SUBTIPO_EVENTO")
@Getter
@Setter
@NoArgsConstructor
public class SubtipoEvento extends Catalogo {
    @ManyToOne(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_TIPO_EVENTO", nullable = false)
    TipoEvento tipoEvento;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LIMITE_EMISOR")
    Perfil limiteEmisor;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LIMITE_DESTINATARIO")
    Perfil limiteDestinatario;

    @ManyToMany
    @JoinTable(name = "RELA_SUBTIPO_EVENTO_RESULTADO_EVENTO",
            joinColumns = { @JoinColumn(name = "ID_SUBTIPO_EVENTO") },
            inverseJoinColumns = { @JoinColumn(name = "ID_RESULTADO_EVENTO") }
    )
    @AuditJoinTable(name = "HIST_RELA_SUBTIPO_EVENTO_RESULTADO_EVENTO")
    @JsonIgnore
    private Set<ResultadoEvento> resultados = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_IMPORTANCIA_EVENTO")
    ImportanciaEvento importancia;

    @Column(name = "NUM_FECHA_LIMITE")
    Integer fechaLimite;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERIODICIDAD_EVENTO")
    private PeriodicidadEvento periodicidad;

    @Column(name = "DES_DESCRIPCION", columnDefinition = "varchar(280)")
    private String descripcion;

    @Column(name = "IND_CORREO", columnDefinition = "tinyint(1)")
    private Boolean correo;
    @Column(name = "IND_PROGRAMACION", columnDefinition = "tinyint(1)")
    private Boolean programacion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
    private Cartera cartera;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SUBTIPO_EVENTO")
  @NotAudited
  private Set<Evento> eventos = new HashSet<>();
}
