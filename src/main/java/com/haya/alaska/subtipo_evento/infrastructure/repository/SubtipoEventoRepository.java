package com.haya.alaska.subtipo_evento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubtipoEventoRepository extends CatalogoRepository<SubtipoEvento, Integer> {
    List<SubtipoEvento> findByTipoEventoIdAndActivoIsTrue(Integer id);
    List<SubtipoEvento> findByTipoEventoIdAndLimiteEmisorId(Integer id, Integer rolId);
    List<SubtipoEvento> findByTipoEventoIdAndLimiteEmisorIdAndCarteraIdAndActivoIsTrue(Integer id, Integer rolId, Integer carteraId);
    List<SubtipoEvento> findByActivoIsTrueAndTipoEventoIdAndCarteraId(Integer id, Integer carteraId);
    List<SubtipoEvento> findByActivoIsTrueAndCarteraIdAndTipoEventoIdIn(Integer carteraId, List<Integer> tipos);
    List<SubtipoEvento> findByActivoIsTrueAndCarteraIsNullAndTipoEventoIdIn(List<Integer> tipos);
    SubtipoEvento findByActivoIsTrueAndCodigoAndCarteraId(String id, Integer carteraId);
    List<SubtipoEvento> findByActivoIsTrueAndTipoEventoIdAndCarteraIsNull(Integer id);
    List<SubtipoEvento> findByTipoEventoIdAndLimiteEmisorIsNull(Integer id);
    List<SubtipoEvento> findByCarteraId(Integer id);
    List<SubtipoEvento> findByCarteraIsNull();
    List<SubtipoEvento> findByTipoEventoIdAndCarteraIdAndLimiteEmisorIsNullAndActivoIsTrue(Integer id, Integer carteraId);
    Optional<SubtipoEvento> findByCodigoAndCarteraId(String codigo, Integer carteraId);
    Optional<SubtipoEvento> findByCodigoAndCarteraIsNull(String codigo);
    Optional<SubtipoEvento> findByValorAndCarteraId(String valor, Integer carteraId);
    Optional<SubtipoEvento> findByValorAndCarteraIsNull(String valor);

  List<SubtipoEvento> findAllByCodigo(String codigo);
}
