package com.haya.alaska.origen_acuerdo.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * OrigenAcuerdo entity
 *
 * @author jesus.javier
 */
@Audited
@AuditTable(value = "HIST_LKUP_ORIGEN_ACUERDO")
@NoArgsConstructor
@Entity
@Table(name = "LKUP_ORIGEN_ACUERDO")
public class OrigenAcuerdo extends Catalogo {
	private static final long serialVersionUID = 8390593086290666611L;
}
