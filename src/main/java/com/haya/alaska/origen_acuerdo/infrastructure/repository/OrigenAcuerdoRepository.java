package com.haya.alaska.origen_acuerdo.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.origen_acuerdo.domain.OrigenAcuerdo;
import org.springframework.stereotype.Repository;

@Repository
public interface OrigenAcuerdoRepository extends CatalogoRepository<OrigenAcuerdo, Integer> {}
