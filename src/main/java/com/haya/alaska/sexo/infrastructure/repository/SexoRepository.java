package com.haya.alaska.sexo.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.sexo.domain.Sexo;
import org.springframework.stereotype.Repository;

@Repository
public interface SexoRepository extends CatalogoRepository<Sexo, Integer> {}
