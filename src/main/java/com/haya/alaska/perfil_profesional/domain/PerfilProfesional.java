package com.haya.alaska.perfil_profesional.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.datos_origen.domain.DatosOrigen;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_PERFIL_PROFESIONAL")
@Entity
@Table(name = "LKUP_PERFIL_PROFESIONAL")
public class PerfilProfesional extends Catalogo {
  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERFIL_PROFESIONAL")
  @NotAudited
  private Set<DatosOrigen> datosOrigenes = new HashSet<>();
}
