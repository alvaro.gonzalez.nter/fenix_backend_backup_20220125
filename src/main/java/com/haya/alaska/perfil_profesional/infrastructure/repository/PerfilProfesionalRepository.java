package com.haya.alaska.perfil_profesional.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.perfil_profesional.domain.PerfilProfesional;
import org.springframework.stereotype.Repository;

@Repository
public interface PerfilProfesionalRepository extends CatalogoRepository<PerfilProfesional, Integer> {}
