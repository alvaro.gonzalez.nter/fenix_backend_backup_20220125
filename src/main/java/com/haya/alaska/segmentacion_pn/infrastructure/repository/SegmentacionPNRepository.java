package com.haya.alaska.segmentacion_pn.infrastructure.repository;

import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SegmentacionPNRepository extends JpaRepository<SegmentacionPN, Integer> {

  List<SegmentacionPN> findByGuardadoTrue();
  List<SegmentacionPN> findByCarteraId(Integer idCartera);
  List<SegmentacionPN> findByCarteraIdOrderByFechaCreacionDesc(Integer idCartera);
  Optional<SegmentacionPN> findById(Integer idSegmentacion);
  List<SegmentacionPN> findAllByIdIn(List<Integer> segmentacionesId);
  List<SegmentacionPN> findByCarteraIdAndGuardadoTrueOrderByFechaCreacionDesc(Integer idCartera);
  List<SegmentacionPN> findByCarteraIdAndGuardadoFalseOrderByFechaCreacionDesc(Integer idCartera);
}
