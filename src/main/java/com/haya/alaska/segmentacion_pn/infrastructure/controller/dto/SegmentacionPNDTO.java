package com.haya.alaska.segmentacion_pn.infrastructure.controller.dto;

import com.haya.alaska.IntervaloImportesSegmentacion.controller.dto.IntervaloImportesSegmentacionDTO;
import com.haya.alaska.intervalo_fechas_segmentacion.controller.dto.IntervaloFechasSegmentacionDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SegmentacionPNDTO implements Serializable {

  private Integer id;
  private Integer idCartera;
  private String nombreSegmentacion;
  //private Date fechaCreacion;

  private Integer numeroGestores;

  private Integer numeroExpedientes;
  private Integer numeroOcupantes;
  private Integer numeroActivos;
  private Double totalValorActivos;

  private Integer numeroExpedientesSinAsignar;
  private Integer numeroOcupantesSinAsignar;
  private Integer numeroActivosSinAsignar;
  private Double totalValorActivosSinAsignar;


  private String area;
  private String grupo;
  private String gestor;


  //OCUPANTE
  private String tipoOcupante;



  //BIEN PN
  private String provincia;
  //private String municipio;
  private String localidad;
  private String tipoActivo;
  private String estrategia;

  //TOTAL BIENES PN
  private IntervaloImportesSegmentacionDTO valorActivo;

}
