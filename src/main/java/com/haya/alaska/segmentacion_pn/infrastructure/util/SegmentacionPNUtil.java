package com.haya.alaska.segmentacion_pn.infrastructure.util;

import com.haya.alaska.IntervaloImportesSegmentacion.domain.IntervaloImportesSegmentacion;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePNSegmentacionDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.mapper.ExpedientePosesionNegociadaMapper;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class SegmentacionPNUtil {

  @Autowired
  ExpedientePosesionNegociadaRepository expedienteRepository;
  @Autowired
  ExpedientePosesionNegociadaMapper expedientePosesionNegociadaMapper;


  public List<ExpedientePNSegmentacionDTO> sacarExpedientesSegmentacionPN(SegmentacionPN segmentacionPN) throws InvocationTargetException, IllegalAccessException {

    List<ExpedientePosesionNegociada> expedientesSacados = sacarExpedientesFiltrados(segmentacionPN);
    List<ExpedientePNSegmentacionDTO> expedientesAsignados = new ArrayList<>();
    for(ExpedientePosesionNegociada expediente: expedientesSacados) {
      expedientesAsignados.add(expedientePosesionNegociadaMapper.parseSegmentacionPN(expediente));
    }
    return expedientesAsignados;
  }


  public BienPosesionNegociada sacarBienPNPrincipal(ExpedientePosesionNegociada expedientePosesionNegociada) {
    BienPosesionNegociada bienPNPrincipal = null;
    Double importeProv = 0.0;

    if(expedientePosesionNegociada.getBienesPosesionNegociada() != null && expedientePosesionNegociada.getBienesPosesionNegociada().size() > 0) {
      for(BienPosesionNegociada bien: expedientePosesionNegociada.getBienesPosesionNegociada()) {
        if(bien != null && bien.getBien() != null && bien.getBien().getImporteBien() != null) {
          if(bien.getBien().getImporteBien() > importeProv)
            importeProv=bien.getBien().getImporteBien();
          bienPNPrincipal = bien;
        }
      }
    }

    return bienPNPrincipal;
  }


  public ProcedimientoPosesionNegociada sacarProcedimientoVivo(ExpedientePosesionNegociada expediente) {
    BienPosesionNegociada bienPosesionNegociada = sacarBienPNPrincipal(expediente);
    ProcedimientoPosesionNegociada procedimientoDef=null;
    for(ProcedimientoPosesionNegociada procedimiento: bienPosesionNegociada.getProcedimientos()) {
      if(procedimiento != null && procedimiento.getActivo() != null) {
        procedimientoDef = procedimiento;
        break;
      }

    }
    return procedimientoDef;
  }

  public List<ExpedientePNSegmentacionDTO> filtrarDtos(List<ExpedientePNSegmentacionDTO> listaExpedientes, String orderField, String orderDirection, String idConcatenado, String valorBienes, String provincia, String localidad, String tipoProcedimiento, String estadoOcupacion, Boolean cargasPosteriores, String estrategia, Boolean rankingAgencia) {

    if(listaExpedientes == null || listaExpedientes.size() == 0)
      return listaExpedientes;

    //FILTRAR
    if(idConcatenado != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedientePNSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getIdConcatenado(), idConcatenado)).collect(Collectors.toList());

    if(valorBienes != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedientePNSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getValorBienes().toString(), valorBienes)).collect(Collectors.toList());

    if(provincia != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedientePNSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getProvincia(), provincia)).collect(Collectors.toList());

    if(localidad != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedientePNSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getLocalidad(), localidad)).collect(Collectors.toList());

    if(tipoProcedimiento != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedientePNSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getTipoProcedimiento(), tipoProcedimiento)).collect(Collectors.toList());

    if(estadoOcupacion != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedientePNSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getEstadoOcupacion(), estadoOcupacion)).collect(Collectors.toList());

    if(cargasPosteriores != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedientePNSegmentacionDTO ex) -> ex.getCargasPosteriores().equals(cargasPosteriores)).collect(Collectors.toList());

    if(estrategia != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedientePNSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getEstrategia(), estrategia)).collect(Collectors.toList());

    if(rankingAgencia != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedientePNSegmentacionDTO ex) -> ex.getRankingAgencia().equals(rankingAgencia)).collect(Collectors.toList());

    if(orderDirection!= null && orderField!=null) {
      switch(orderDirection) {
        case "asc":
          switch(orderField) {
            case "idConcatenado":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getIdConcatenado)).collect(Collectors.toList());
              break;
            case "valorBienes":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getValorBienes)).collect(Collectors.toList());
              break;
            case "provincia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getProvincia)).collect(Collectors.toList());
              break;
            case "localidad":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getLocalidad)).collect(Collectors.toList());
              break;
            case "tipoProcedimiento":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getTipoProcedimiento)).collect(Collectors.toList());
              break;
            case "estadoOcupacion":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getEstadoOcupacion)).collect(Collectors.toList());
              break;
            case "cargasPosteriores":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getCargasPosteriores)).collect(Collectors.toList());
              break;
            case "estrategia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getEstrategia)).collect(Collectors.toList());
              break;
            case "rankingAgencia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getRankingAgencia)).collect(Collectors.toList());
              break;
          }
          break;
        case "desc":
          switch(orderField) {
            case "idConcatenado":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getIdConcatenado).reversed()).collect(Collectors.toList());
              break;
            case "valorBienes":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getValorBienes).reversed()).collect(Collectors.toList());
              break;
            case "provincia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getProvincia).reversed()).collect(Collectors.toList());
              break;
            case "localidad":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getLocalidad).reversed()).collect(Collectors.toList());
              break;
            case "tipoProcedimiento":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getTipoProcedimiento).reversed()).collect(Collectors.toList());
              break;
            case "estadoOcupacion":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getEstadoOcupacion).reversed()).collect(Collectors.toList());
              break;
            case "cargasPosteriores":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getCargasPosteriores).reversed()).collect(Collectors.toList());
              break;
            case "estrategia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getEstrategia).reversed()).collect(Collectors.toList());
              break;
            case "rankingAgencia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedientePNSegmentacionDTO::getRankingAgencia).reversed()).collect(Collectors.toList());
              break;
          }
          break;
      }
    }
    return listaExpedientes;
  }


  public List<ExpedientePosesionNegociada> sacarExpedientesFiltrados(SegmentacionPN segmentacion) {
    List<ExpedientePosesionNegociada> lista = new ArrayList<>();
    List<ExpedientePosesionNegociada> listaInicial = expedienteRepository.getExpedientesPNSinGestor(segmentacion.getCartera().getId());

    for(ExpedientePosesionNegociada expediente: listaInicial) {

      if(expediente == null)
        continue;


      //BIENPN

      BienPosesionNegociada bienPrincipal = sacarBienPNPrincipal(expediente);

      //IMPORTE SUMA DE LOS BIENES
      Double totalBienes = 0.00;
      Boolean valido = false;
      for(BienPosesionNegociada bienPN: expediente.getBienesPosesionNegociada()) {
        if(bienPN != null && bienPN.getBien() != null && bienPN.getBien().getImporteBien() != null)
          totalBienes += bienPN.getBien().getImporteBien();
          if(valido == false)
            valido = true;
      }


      //->tipo activo
      if(segmentacion.getTipoActivo()!=null) {
        boolean pasaFiltroTipoActivo=false;

        if(bienPrincipal == null || bienPrincipal.getBien() == null || bienPrincipal.getBien().getTipoActivo() == null)
          continue;

        if (bienPrincipal.getBien() != null
          && bienPrincipal.getBien().getTipoActivo() != null
          && bienPrincipal.getBien().getTipoActivo().equals(segmentacion.getTipoActivo()))
          pasaFiltroTipoActivo = true;

        if(!pasaFiltroTipoActivo)
          continue;
      }

      // ->importe
      if (segmentacion.getImporteBienPN() != null) {
        IntervaloImportesSegmentacion importe = segmentacion.getImporteBienPN();
        boolean pasaFiltroImporte=false;

        if(totalBienes.equals(0.00) && !valido)
          continue;

          if(importe.getMayor() != null) {
            if(totalBienes.compareTo(importe.getMayor()) > 0 )
              pasaFiltroImporte = true;
            else
              continue;
          }
          if(importe.getMenor() != null) {
            if(totalBienes.compareTo(importe.getMenor()) < 0 )
              pasaFiltroImporte = true;
            else
              continue;
          }
          if(importe.getIgual() != null) {
            if(totalBienes.compareTo(importe.getIgual()) == 0)
              pasaFiltroImporte=true;
            else
              continue;
          }

        if(!pasaFiltroImporte)
          continue;
      }

      //->provincia
      if(segmentacion.getProvinciaBienPN() != null) {
        boolean pasaFiltroProvincia = false;

        if(bienPrincipal == null || bienPrincipal.getBien() == null || bienPrincipal.getBien().getProvincia() == null)
          continue;

        if(bienPrincipal.getBien().getProvincia().equals(segmentacion.getProvinciaBienPN())) {
          pasaFiltroProvincia=true;
        }
        if(!pasaFiltroProvincia)
          continue;
      }

      //->localidad
      if(segmentacion.getLocalidadBienPN() != null) {
        boolean pasaFiltroLocalidad = false;

        if(bienPrincipal == null || bienPrincipal.getBien() == null || bienPrincipal.getBien().getLocalidad() == null)
          continue;

        if(bienPrincipal.getBien().getLocalidad().equals(segmentacion.getLocalidadBienPN())) {
          pasaFiltroLocalidad=true;
        }
        if(!pasaFiltroLocalidad)
          continue;
      }

      //->estrategia
      if(segmentacion.getEstrategiaBienPN() != null) {
        boolean pasaFiltroEstrategia = false;

        if(bienPrincipal == null || bienPrincipal.getEstrategia() == null || bienPrincipal.getEstrategia().getEstrategia() == null)
          continue;

        if(bienPrincipal.getEstrategia().getEstrategia().equals(segmentacion.getEstrategiaBienPN())) {
          pasaFiltroEstrategia=true;
        }
        if(!pasaFiltroEstrategia)
          continue;
      }

      //OCUPANTES

      //->tipo ocupante
      if(segmentacion.getTipoOcupante()!=null) {
        boolean pasaFiltroTipoOcupante = false;

        if(bienPrincipal == null || bienPrincipal.getOcupantes() == null)
          continue;

        for(OcupanteBien ocupante: bienPrincipal.getOcupantes()) {
          if (ocupante.getTipoOcupante() != null && ocupante.getTipoOcupante().equals(segmentacion.getTipoOcupante())) {
            pasaFiltroTipoOcupante = true;
            break;
          }
        }
        if(!pasaFiltroTipoOcupante)
          continue;
      }


      lista.add(expediente);
    }

    return lista;
  }


}
