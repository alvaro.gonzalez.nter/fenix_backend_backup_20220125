package com.haya.alaska.segmentacion_pn.infrastructure.mapper;

import com.haya.alaska.IntervaloImportesSegmentacion.mapper.IntervaloImportesSegmentacionMapper;
import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.area_usuario.infrastructure.repository.AreaUsuarioRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import com.haya.alaska.grupo_usuario.infrastructure.repository.GrupoUsuarioRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNDetalleDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNInputDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.util.SegmentacionPNUtil;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_ocupante.domain.TipoOcupante;
import com.haya.alaska.tipo_ocupante.infrastructure.repository.TipoOcupanteRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Component
public class SegmentacionPNMapper {

  @Autowired
  private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  private CarteraRepository carteraRepository;
  @Autowired
  private AreaUsuarioRepository areaUsuarioRepository;
  @Autowired
  private UsuarioRepository usuarioRepository;
  @Autowired
  private GrupoUsuarioRepository grupoUsuarioRepository;
  @Autowired
  private PaisRepository paisRepository;
  @Autowired
  private ProvinciaRepository provinciaRepository;
  @Autowired
  private MunicipioRepository municipioRepository;
  @Autowired
  private LocalidadRepository localidadRepository;
  @Autowired
  private TipoActivoRepository tipoActivoRepository;
  @Autowired
  private TipoOcupanteRepository tipoOcupanteRepository;
  @Autowired
  private IntervaloImportesSegmentacionMapper intervaloImportesSegmentacionMapper;
  @Autowired
  private SegmentacionPNUtil segmentacionPNUtil;
  @Autowired
  private EstrategiaRepository estrategiaRepository;

  public SegmentacionPNDTO segmentacionPNADto(SegmentacionPN segmentacionPN,Integer carteraId) throws InvocationTargetException, IllegalAccessException {

    SegmentacionPNDTO segmentacionPNDTO = new SegmentacionPNDTO();
    BeanUtils.copyProperties(segmentacionPN,segmentacionPNDTO);

    //campos de la propia segmentacionPN

    segmentacionPNDTO.setIdCartera(segmentacionPN.getCartera() != null ? segmentacionPN.getCartera().getId() : null);
    segmentacionPNDTO.setArea(segmentacionPN.getArea() != null ? segmentacionPN.getArea().getNombre() : null);
    segmentacionPNDTO.setGrupo(segmentacionPN.getGrupo() != null ? segmentacionPN.getGrupo().getNombre() : null);
    segmentacionPNDTO.setGestor(segmentacionPN.getGestor() != null ? segmentacionPN.getGestor().getNombre()  : null);
    segmentacionPNDTO.setProvincia(segmentacionPN.getProvinciaBienPN() != null ? segmentacionPN.getProvinciaBienPN().getValor() : null);
    //segmentacionPNDTO.setMunicipio(segmentacionPN.getMunicipioBienPN() != null ? segmentacionPN.getMunicipioBienPN().getValor() : null);
    segmentacionPNDTO.setLocalidad(segmentacionPN.getLocalidadBienPN() != null ? segmentacionPN.getLocalidadBienPN().getValor() : null);
    segmentacionPNDTO.setTipoActivo(segmentacionPN.getTipoActivo() != null ? segmentacionPN.getTipoActivo().getValor() : null);
    segmentacionPNDTO.setTipoOcupante(segmentacionPN.getTipoOcupante() != null ? segmentacionPN.getTipoOcupante().getValor() : null);
    segmentacionPNDTO.setEstrategia(segmentacionPN.getEstrategiaBienPN() != null ? segmentacionPN.getEstrategiaBienPN().getValor() : null);
    segmentacionPNDTO.setValorActivo(intervaloImportesSegmentacionMapper.entidadADto(segmentacionPN.getImporteBienPN()));
    //segmentacionPNDTO.setFechaCreacion(Date.from(segmentacionPN.getFechaCreacion().atZone(ZoneId.systemDefault()).toInstant()));

    //campos calculados

    //TODO campo gestores
    Integer numeroGestores = 0;

    Integer numeroExpedientes = 0;
    Integer numeroActivos = 0;
    Integer numeroOcupantes = 0;
    Double totalValorActivos = 0.00;

    Integer numeroExpedientesTotal = 0;
    Integer numeroActivosTotal = 0;
    Integer numeroOcupantesTotal = 0;
    Double totalValorActivosTotal = 0.00;

    List<ExpedientePosesionNegociada>expedienteList=expedientePosesionNegociadaRepository.findAllByCarteraId(carteraId);
    for(ExpedientePosesionNegociada expediente: expedienteList) {
      numeroExpedientesTotal++;
      totalValorActivosTotal += expediente.getSaldoGestion();

      for(BienPosesionNegociada bienPN: expediente.getBienesPosesionNegociada()) {
        numeroActivosTotal++;
        for(OcupanteBien ocupante: bienPN.getOcupantes()) {
          numeroOcupantesTotal++;
        }
      }
    }

    List<ExpedientePosesionNegociada> expedientesSacados = segmentacionPNUtil.sacarExpedientesFiltrados(segmentacionPN);
    for(ExpedientePosesionNegociada expediente: expedientesSacados) {
      numeroExpedientes++;
      totalValorActivos += expediente.getSaldoGestion();

      for(BienPosesionNegociada bienPN: expediente.getBienesPosesionNegociada()) {
        numeroActivos++;
        for(OcupanteBien ocupante: bienPN.getOcupantes()) {
          numeroOcupantes++;
        }
      }
    }

    segmentacionPNDTO.setNumeroExpedientes(numeroExpedientes);
    segmentacionPNDTO.setNumeroActivos(numeroActivos);
    segmentacionPNDTO.setNumeroOcupantes(numeroOcupantes);
    segmentacionPNDTO.setTotalValorActivos(totalValorActivos);

    segmentacionPNDTO.setNumeroExpedientesSinAsignar(numeroExpedientesTotal-numeroExpedientes);
    segmentacionPNDTO.setNumeroActivosSinAsignar(numeroActivosTotal-numeroActivos);
    segmentacionPNDTO.setNumeroOcupantesSinAsignar(numeroOcupantesTotal-numeroOcupantes);
    segmentacionPNDTO.setTotalValorActivosSinAsignar(totalValorActivosTotal-totalValorActivos);


    return segmentacionPNDTO;
  }

  public SegmentacionPNDetalleDTO segmentacionPNADetalleDto(SegmentacionPN segmentacionPN, Integer carteraId) {
    SegmentacionPNDetalleDTO segmentacionPNDTO = new SegmentacionPNDetalleDTO();
    BeanUtils.copyProperties(segmentacionPN,segmentacionPNDTO);

    //campos de la propia segmentacionPN

    segmentacionPNDTO.setCartera(segmentacionPN.getCartera() != null ? segmentacionPN.getCartera().getId() : null);
    segmentacionPNDTO.setArea(segmentacionPN.getArea() != null ? segmentacionPN.getArea().getId() : null);
    segmentacionPNDTO.setGrupo(segmentacionPN.getGrupo() != null ? segmentacionPN.getGrupo().getId() : null);
    segmentacionPNDTO.setGestor(segmentacionPN.getGestor() != null ? segmentacionPN.getGestor().getId()  : null);
    segmentacionPNDTO.setProvincia(segmentacionPN.getProvinciaBienPN() != null ? segmentacionPN.getProvinciaBienPN().getId() : null);
    //segmentacionPNDTO.setMunicipio(segmentacionPN.getMunicipioBienPN() != null ? segmentacionPN.getMunicipioBienPN().getId() : null);
    segmentacionPNDTO.setLocalidad(segmentacionPN.getLocalidadBienPN() != null ? segmentacionPN.getLocalidadBienPN().getId() : null);
    segmentacionPNDTO.setTipoActivo(segmentacionPN.getTipoActivo() != null ? segmentacionPN.getTipoActivo().getId() : null);
    segmentacionPNDTO.setTipoOcupante(segmentacionPN.getTipoOcupante() != null ? segmentacionPN.getTipoOcupante().getId() : null);
    segmentacionPNDTO.setEstrategia(segmentacionPN.getEstrategiaBienPN() != null ? segmentacionPN.getEstrategiaBienPN().getId() : null);
    segmentacionPNDTO.setValorActivo(intervaloImportesSegmentacionMapper.entidadADto(segmentacionPN.getImporteBienPN()));
    //segmentacionPNDTO.setFechaCreacion(Date.from(segmentacionPN.getFechaCreacion().atZone(ZoneId.systemDefault()).toInstant()));

    //campos calculados

    //TODO campo gestores
    Integer numeroGestores = 0;

    Integer numeroExpedientes = 0;
    Integer numeroActivos = 0;
    Integer numeroOcupantes = 0;
    Double totalValorActivos = 0.00;

    Integer numeroExpedientesTotal = 0;
    Integer numeroActivosTotal = 0;
    Integer numeroOcupantesTotal = 0;
    Double totalValorActivosTotal = 0.00;

    List<ExpedientePosesionNegociada>expedienteList=expedientePosesionNegociadaRepository.findAllByCarteraId(carteraId);
    for(ExpedientePosesionNegociada expediente: expedienteList) {
      numeroExpedientesTotal++;
      totalValorActivosTotal += expediente.getSaldoGestion();

      for(BienPosesionNegociada bienPN: expediente.getBienesPosesionNegociada()) {
        numeroActivosTotal++;
        for(OcupanteBien ocupante: bienPN.getOcupantes()) {
          numeroOcupantesTotal++;
        }
      }
    }

    List<ExpedientePosesionNegociada> expedientesSacados = segmentacionPNUtil.sacarExpedientesFiltrados(segmentacionPN);
    for(ExpedientePosesionNegociada expediente: expedientesSacados) {
      numeroExpedientes++;


      for(BienPosesionNegociada bienPN: expediente.getBienesPosesionNegociada()) {
        numeroActivos++;
        if(bienPN.getBien() != null && bienPN.getBien().getImporteBien() != null)
          totalValorActivos += bienPN.getBien().getImporteBien();

        for(OcupanteBien ocupante: bienPN.getOcupantes()) {
          numeroOcupantes++;
        }
      }
    }

    segmentacionPNDTO.setNumeroExpedientes(numeroExpedientes);
    segmentacionPNDTO.setNumeroActivos(numeroActivos);
    segmentacionPNDTO.setNumeroOcupantes(numeroOcupantes);
    segmentacionPNDTO.setTotalValorActivos(totalValorActivos);

    segmentacionPNDTO.setNumeroExpedientesSinAsignar(numeroExpedientesTotal-numeroExpedientes);
    segmentacionPNDTO.setNumeroActivosSinAsignar(numeroActivosTotal-numeroActivos);
    segmentacionPNDTO.setNumeroOcupantesSinAsignar(numeroOcupantesTotal-numeroOcupantes);
    segmentacionPNDTO.setTotalValorActivosSinAsignar(totalValorActivosTotal-totalValorActivos);


    return segmentacionPNDTO;
  }

  public SegmentacionPN segmentacionPNInputASegmentacionPN(SegmentacionPNInputDTO input) throws Exception {

    SegmentacionPN segmentacionPN=new SegmentacionPN();
    BeanUtils.copyProperties(input,segmentacionPN);

    Cartera cartera = null;
    AreaUsuario area = null;
    GrupoUsuario grupo = null;
    Usuario gestor = null;
    TipoActivo tipoActivo = null;
    Provincia provinciaBienPN = null;
    Municipio municipioBienPN = null;
    Localidad localidadBienPN = null;
    TipoOcupante tipoOcupante = null;
    Estrategia estrategia = null;

    if(input.getCartera() != null)
      cartera=carteraRepository.findById(input.getCartera()).orElseThrow(() -> new Exception("Cartera no encontrado con el id: " + input.getCartera()));
    else
      throw new Exception("El campo cartera es obligatorio");

    if(input.getArea() != null)
      area = areaUsuarioRepository.findById(input.getArea()).orElseThrow(() -> new Exception("Area usuario no encontrado con el id: " + input.getArea()));

    if(input.getGestor() != null)
      gestor = usuarioRepository.findById(input.getGestor()).orElseThrow(() -> new Exception("Usuario no encontrado con el id: " + input.getGestor()));

    if(input.getGrupo() != null)
      grupo = grupoUsuarioRepository.findById(input.getGrupo()).orElseThrow(() -> new Exception("Grupo no encontrado con el id: " + input.getGrupo()));

    if(input.getTipoOcupante() != null)
      tipoOcupante = tipoOcupanteRepository.findById(input.getTipoOcupante()).orElseThrow(() -> new Exception("Tipo de ocupante no encontrado con el id: "+input.getTipoOcupante()));

    if(input.getTipoActivo() != null)
      tipoActivo = tipoActivoRepository.findById(input.getTipoActivo()).orElseThrow(() -> new Exception("Tipo de activo no encontrado con el id: "+input.getTipoActivo()));

    if(input.getProvincia() != null)
      provinciaBienPN = provinciaRepository.findById(input.getProvincia()).orElseThrow(() -> new Exception("Provincia no encontrada con el id: "+input.getProvincia()));

    //if(input.getMunicipio() != null)
      //municipioBienPN = municipioRepository.findById(input.getMunicipio()).orElseThrow(() -> new Exception("Municipio no encontrado con el id: "+input.getMunicipio()));

    if(input.getLocalidad() != null)
      localidadBienPN = localidadRepository.findById(input.getLocalidad()).orElseThrow(() -> new Exception("Localidad no encontrada con el id: "+input.getLocalidad()));

    if(input.getEstrategia() != null)
      estrategia = estrategiaRepository.findById(input.getEstrategia()).orElseThrow(() -> new Exception("Estrategia no encontrada con el id: "+input.getEstrategia()));

    segmentacionPN.setCartera(cartera);
    segmentacionPN.setArea(area);
    segmentacionPN.setGrupo(grupo);
    segmentacionPN.setGestor(gestor);
    segmentacionPN.setTipoOcupante(tipoOcupante);
    segmentacionPN.setTipoActivo(tipoActivo);
    segmentacionPN.setProvinciaBienPN(provinciaBienPN);
    //segmentacionPN.setMunicipioBienPN(municipioBienPN);
    segmentacionPN.setLocalidadBienPN(localidadBienPN);
    segmentacionPN.setEstrategiaBienPN(estrategia);

    segmentacionPN.setImporteBienPN(intervaloImportesSegmentacionMapper.dtoAEntidad(input.getValorActivo()));

    return segmentacionPN;
  }
}
