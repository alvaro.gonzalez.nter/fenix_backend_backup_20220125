package com.haya.alaska.segmentacion_pn.infrastructure.controller;

import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePNSegmentacionDTO;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionDTO;
import com.haya.alaska.segmentacion_pn.application.SegmentacionPNUseCase;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNDetalleDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNInputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.List;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/segmentacionPN")
public class SegmentacionPNController {

  @Autowired
  SegmentacionPNUseCase segmentacionUseCase;

  @ApiOperation(
    value = "Expedientes sin Gestor",
    notes = "Devuelve los expedientes sin Gestor asignado")
  @GetMapping("/sinGestor")
  public SegmentacionPNDTO getExpedienteSinGestor(@RequestParam Integer carteraId) throws NotFoundException {
    return segmentacionUseCase.expedienteFiltradoCarteraSinGestor(carteraId);
  }

  @ApiOperation(
    value = "Obtener lista de segmentaciones de una cartera")
  @GetMapping("/all")
  public List<SegmentacionPNDTO> getSegmentaciones(@RequestParam Integer carteraId,
                                                   @RequestParam Boolean guardado) throws InvocationTargetException, IllegalAccessException {
    return segmentacionUseCase.getSegmentaciones(carteraId,guardado);
  }

  @ApiOperation(
    value = "Poner como guardadas las segmentaciones que se indiquen")
  @PostMapping("/guardar")
  public List<SegmentacionPNDTO> guardarSegmentaciones(@RequestBody List<Integer> segmentacionesId) throws InvocationTargetException, IllegalAccessException {
    return segmentacionUseCase.guardar(segmentacionesId);
  }

  @ApiOperation(
    value = "Obtener el detalle de una segmentacion")
  @GetMapping("/detalle/{segmentacionId}")
  public SegmentacionPNDetalleDTO getDetalleSegmentacion(@PathVariable Integer segmentacionId){
    return segmentacionUseCase.getDetalleSegmentacion(segmentacionId);
  }

  @ApiOperation(
    value = "Obtener los expedientes de una segmentacion")
  @GetMapping("/{segmentacionId}")
  public ListWithCountDTO<ExpedientePNSegmentacionDTO> getExpedientesSegmentacion(@PathVariable Integer segmentacionId,
                                                                                  @RequestParam(value = "size") Integer size,
                                                                                  @RequestParam(value = "page") Integer page,
                                                                                  @RequestParam(value = "orderField", required = false) String orderField,
                                                                                  @RequestParam(value = "orderDirection", required = false) String orderDirection,
                                                                                  @RequestParam(value = "idConcatenado", required = false) String idConcatenado,
                                                                                  @RequestParam(value = "valorBienes", required = false) String valorBienes,
                                                                                  @RequestParam(value = "provincia", required = false) String provincia,
                                                                                  @RequestParam(value = "localidad", required = false) String localidad,
                                                                                  @RequestParam(value = "tipoProcedimiento", required = false) String tipoProcedimiento,
                                                                                  @RequestParam(value = "estadoOcupacion", required = false) String estadoOcupacion,
                                                                                  @RequestParam(value = "cargasPosteriores", required = false) Boolean cargasPosteriores,
                                                                                  @RequestParam(value = "estrategia", required = false) String estrategia,
                                                                                  @RequestParam(value = "rankingAgencia", required = false) Boolean rankingAgencia
  ) throws NotFoundException, InvocationTargetException, IllegalAccessException {
    return segmentacionUseCase.getExpedientesSegmentacion(segmentacionId, size, page, orderField, orderDirection, idConcatenado, valorBienes, provincia, localidad, tipoProcedimiento, estadoOcupacion, cargasPosteriores, estrategia, rankingAgencia);
  }

  @ApiOperation(
    value = "Obtener la lista de expedientes sin asignar")
  @GetMapping("/expedientesSinAsignar")
  public ListWithCountDTO<ExpedientePNSegmentacionDTO> getExpedientesSinAsignar(@RequestParam Integer carteraId,
                                                                                @RequestParam(value = "size") Integer size,
                                                                                @RequestParam(value = "page") Integer page,
                                                                                @RequestParam(value = "orderField", required = false) String orderField,
                                                                                @RequestParam(value = "orderDirection", required = false) String orderDirection,
                                                                                @RequestParam(value = "idConcatenado", required = false) String idConcatenado,
                                                                                @RequestParam(value = "valorBienes", required = false) String valorBienes,
                                                                                @RequestParam(value = "provincia", required = false) String provincia,
                                                                                @RequestParam(value = "localidad", required = false) String localidad,
                                                                                @RequestParam(value = "tipoProcedimiento", required = false) String tipoProcedimiento,
                                                                                @RequestParam(value = "estadoOcupacion", required = false) String estadoOcupacion,
                                                                                @RequestParam(value = "cargasPosteriores", required = false) Boolean cargasPosteriores,
                                                                                @RequestParam(value = "estrategia", required = false) String estrategia,
                                                                                @RequestParam(value = "rankingAgencia", required = false) Boolean rankingAgencia
  ){
    return segmentacionUseCase.getExpedientesSinAsignar(carteraId, size, page, orderField, orderDirection, idConcatenado, valorBienes, provincia, localidad, tipoProcedimiento, estadoOcupacion, cargasPosteriores, estrategia, rankingAgencia);
  }

  @ApiOperation(value = "Crear una segmentacion")
  @PostMapping("/")
  public SegmentacionPNDTO createSegmentacion(@RequestBody SegmentacionPNInputDTO segmentacionInputDTO) throws Exception {
    return segmentacionUseCase.createSegmentacion(segmentacionInputDTO);
  }

  @ApiOperation(value = "Modificar una segmentacion")
  @PutMapping("/")
  public SegmentacionPNDTO modificarSegmentacion(@RequestBody SegmentacionPNInputDTO segmentacionInputDTO) throws Exception {
    return segmentacionUseCase.modificarSegmentacion(segmentacionInputDTO);
  }

}
