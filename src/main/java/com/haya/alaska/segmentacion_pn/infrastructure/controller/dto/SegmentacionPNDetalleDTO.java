package com.haya.alaska.segmentacion_pn.infrastructure.controller.dto;

import com.haya.alaska.IntervaloImportesSegmentacion.controller.dto.IntervaloImportesSegmentacionDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePNSegmentacionDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SegmentacionPNDetalleDTO implements Serializable {

  private Integer id;
  private Integer cartera;
  private String nombreSegmentacion;
  //private Date fechaCreacion;

  private Integer numeroGestores;

  private Integer numeroExpedientes;
  private Integer numeroOcupantes;
  private Integer numeroActivos;
  private Double totalValorActivos;

  private Integer numeroExpedientesSinAsignar;
  private Integer numeroOcupantesSinAsignar;
  private Integer numeroActivosSinAsignar;
  private Double totalValorActivosSinAsignar;


  private Integer area;
  private Integer grupo;
  private Integer gestor;


  //OCUPANTE
  private Integer tipoOcupante; //catalogo


  //BIEN PN
  private Integer provincia; //catalogo
  //private Integer municipio; //catalogo
  private Integer localidad; //catalogo
  private Integer tipoActivo; //catalogo
  private Integer estrategia; //catalogo

  //TOTAL BIENES PN
  private IntervaloImportesSegmentacionDTO valorActivo;
}
