package com.haya.alaska.segmentacion_pn.infrastructure.controller.dto;

import com.haya.alaska.IntervaloImportesSegmentacion.controller.dto.IntervaloImportesSegmentacionDTO;
import com.haya.alaska.intervalo_fechas_segmentacion.controller.dto.IntervaloFechasSegmentacionDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SegmentacionPNInputDTO implements Serializable {

  private Integer id;
  private Integer cartera;
  private String nombreSegmentacion;

  private Integer area;
  private Integer grupo;
  private Integer gestor;


  //OCUPANTES
  private Integer tipoOcupante;


  //ACTIVOS
  private Integer tipoActivo;
  private Integer provincia;
  //private Integer municipio;
  private Integer localidad;
  private Integer estrategia;

  //TOTAL ACTIVOS
  private IntervaloImportesSegmentacionDTO valorActivo;

}
