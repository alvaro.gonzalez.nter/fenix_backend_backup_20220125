package com.haya.alaska.segmentacion_pn.application;

import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePNSegmentacionDTO;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNDetalleDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNInputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface SegmentacionPNUseCase {

  SegmentacionPNDTO expedienteFiltradoCarteraSinGestor(Integer carteraID) throws NotFoundException;

  List<SegmentacionPNDTO> getSegmentaciones(Integer carteraId, Boolean guardado) throws InvocationTargetException, IllegalAccessException;

  List<SegmentacionPNDTO> guardar(List<Integer> segmentacionesId) throws InvocationTargetException, IllegalAccessException;

  SegmentacionPNDetalleDTO getDetalleSegmentacion (Integer segmentacionId);

  ListWithCountDTO<ExpedientePNSegmentacionDTO> getExpedientesSegmentacion(Integer segmentacionId, Integer size, Integer page, String orderField, String orderDirection, String idConcatenado, String valorBienes, String provincia, String localidad, String tipoProcedimiento, String estadoOcupacion, Boolean cargasPosteriores, String estrategia, Boolean rankingAgencia) throws NotFoundException, InvocationTargetException, IllegalAccessException;

  ListWithCountDTO<ExpedientePNSegmentacionDTO> getExpedientesSinAsignar(Integer carteraId, Integer size, Integer page, String orderField, String orderDirection, String idConcatenado, String valorBienes, String provincia, String localidad, String tipoProcedimiento, String estadoOcupacion, Boolean cargasPosteriores, String estrategia, Boolean rankingAgencia);

  SegmentacionPNDTO createSegmentacion(SegmentacionPNInputDTO segmentacionInputDTO) throws Exception;

  SegmentacionPNDTO modificarSegmentacion(SegmentacionPNInputDTO segmentacionInputDTO) throws Exception;
}
