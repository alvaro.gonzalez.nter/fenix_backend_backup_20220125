package com.haya.alaska.segmentacion_pn.application;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePNSegmentacionDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.mapper.ExpedientePosesionNegociadaMapper;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNDetalleDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.controller.dto.SegmentacionPNInputDTO;
import com.haya.alaska.segmentacion_pn.infrastructure.mapper.SegmentacionPNMapper;
import com.haya.alaska.segmentacion_pn.infrastructure.repository.SegmentacionPNRepository;
import com.haya.alaska.segmentacion_pn.infrastructure.util.SegmentacionPNUtil;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SegmentacionPNUseCaseImpl implements SegmentacionPNUseCase{

  @Autowired
  SegmentacionPNRepository segmentacionPNRepository;
  @Autowired
  SegmentacionPNMapper segmentacionPNMapper;
  @Autowired
  SegmentacionPNUtil segmentacionPNUtil;
  @Autowired
  ExpedientePosesionNegociadaMapper expedientePosesionNegociadaMapper;
  @Autowired
  ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;

  @Override
  public SegmentacionPNDTO expedienteFiltradoCarteraSinGestor(Integer carteraID) throws NotFoundException {
    List<ExpedientePosesionNegociada>expedienteList=expedientePosesionNegociadaRepository.findAllByCarteraId(carteraID);

    //todo el campo numero gestores, sin definir
    Integer numeroGestores = 0;

    Double totalValorActivos=0.0;
    Integer numeroExpedientes = 0;
    Integer numeroOcupantes = 0;
    Integer numeroActivos = 0;

    for (ExpedientePosesionNegociada expediente :expedienteList) {
      Usuario gestor=expediente.getGestor();
      if (gestor==null){
        numeroExpedientes++;

        for(BienPosesionNegociada bien : expediente.getBienesPosesionNegociada()) {
          numeroActivos++;
          totalValorActivos += bien.getBien().getImporteBien();

          for(OcupanteBien ocupante: bien.getOcupantes()) {
            numeroOcupantes++;
          }
        }
      }
    }

    SegmentacionPNDTO segmentacionDTO=new SegmentacionPNDTO();
    segmentacionDTO.setNumeroExpedientes(numeroExpedientes);
    segmentacionDTO.setNumeroActivos(numeroActivos);
    segmentacionDTO.setNumeroOcupantes(numeroOcupantes);
    segmentacionDTO.setNumeroGestores(numeroGestores);
    segmentacionDTO.setTotalValorActivos(totalValorActivos);

    return segmentacionDTO;
  }



  @Override
  public List<SegmentacionPNDTO> getSegmentaciones(Integer carteraId, Boolean guardado) throws InvocationTargetException, IllegalAccessException {
    List<SegmentacionPN> segmentaciones = new ArrayList<>();
    if(guardado)
      segmentaciones = segmentacionPNRepository.findByCarteraIdAndGuardadoTrueOrderByFechaCreacionDesc(carteraId);
    else
      segmentaciones = segmentacionPNRepository.findByCarteraIdAndGuardadoFalseOrderByFechaCreacionDesc(carteraId);

    //segmentaciones = segmentaciones.stream().sorted(Comparator.comparing(SegmentacionPN::getFechaCreacion).reversed()).collect(Collectors.toList());
    List<SegmentacionPNDTO> segmentacionesDto = new ArrayList<>();
    for(SegmentacionPN segmentacion: segmentaciones) {
      SegmentacionPNDTO segmentacionDTO=segmentacionPNMapper.segmentacionPNADto(segmentacion,carteraId);
      segmentacionesDto.add(segmentacionDTO);
    }
    return segmentacionesDto;
  }

  @Override
  public List<SegmentacionPNDTO> guardar(List<Integer> segmentacionesId) throws InvocationTargetException, IllegalAccessException {
    List<SegmentacionPN> segmentaciones = segmentacionPNRepository.findAllByIdIn(segmentacionesId);
    for(SegmentacionPN segmentacion: segmentaciones) {
      segmentacion.setGuardado(true);
    }
    List<SegmentacionPN> segmentacionesFinal = segmentacionPNRepository.saveAll(segmentaciones);

    List<SegmentacionPNDTO> segmentacionesDto = new ArrayList<>();
    for(SegmentacionPN segmentacion: segmentacionesFinal) {
      SegmentacionPNDTO segmentacionDTO=segmentacionPNMapper.segmentacionPNADto(segmentacion,segmentacion.getCartera().getId());
      segmentacionesDto.add(segmentacionDTO);
    }
    return segmentacionesDto;
  }


  @Override
  public SegmentacionPNDetalleDTO getDetalleSegmentacion (Integer segmentacionId) {
    SegmentacionPN segmentacion = segmentacionPNRepository.findById(segmentacionId).orElse(null);
    SegmentacionPNDetalleDTO segmentacionDTO = new SegmentacionPNDetalleDTO();
    if(segmentacion != null) {
      segmentacionDTO=segmentacionPNMapper.segmentacionPNADetalleDto(segmentacion,segmentacion.getCartera().getId());
    }
    return segmentacionDTO;
  }



  public ListWithCountDTO<ExpedientePNSegmentacionDTO> getExpedientesSegmentacion(Integer segmentacionId, Integer size, Integer page, String orderField, String orderDirection, String idConcatenado, String valorBienes, String provincia, String localidad, String tipoProcedimiento, String estadoOcupacion, Boolean cargasPosteriores, String estrategia, Boolean rankingAgencia) throws NotFoundException, InvocationTargetException, IllegalAccessException {
    SegmentacionPN segmentacion = segmentacionPNRepository.findById(segmentacionId).orElse(null);
    List<ExpedientePNSegmentacionDTO> listaExpedientes = new ArrayList<>();
    List<ExpedientePNSegmentacionDTO> expedientesDevolver = new ArrayList<>();

    if(segmentacion != null) {
      listaExpedientes = segmentacionPNUtil.sacarExpedientesSegmentacionPN(segmentacion);
      expedientesDevolver = segmentacionPNUtil.filtrarDtos(listaExpedientes, orderField, orderDirection, idConcatenado, valorBienes, provincia, localidad, tipoProcedimiento, estadoOcupacion, cargasPosteriores, estrategia, rankingAgencia);
    }
    List<ExpedientePNSegmentacionDTO> expedientesPaginados = expedientesDevolver.stream().skip(size * page).limit(size).collect(Collectors.toList());

    return new ListWithCountDTO<>(expedientesPaginados, expedientesDevolver.size());
  }



  public ListWithCountDTO<ExpedientePNSegmentacionDTO> getExpedientesSinAsignar(Integer carteraId, Integer size, Integer page, String orderField, String orderDirection, String idConcatenado, String valorBienes, String provincia, String localidad, String tipoProcedimiento, String estadoOcupacion, Boolean cargasPosteriores, String estrategia, Boolean rankingAgencia) {
    List<ExpedientePosesionNegociada> expedienteList=expedientePosesionNegociadaRepository.findAllByCarteraId(carteraId);
    List<ExpedientePNSegmentacionDTO>expedienteSinGestorList=new ArrayList<>();
    List<ExpedientePNSegmentacionDTO> expedientesDevolver = new ArrayList<>();

    for (ExpedientePosesionNegociada expediente :expedienteList) {
      Usuario gestor=expediente.getGestor();
      if (gestor==null){
        expedienteSinGestorList.add(expedientePosesionNegociadaMapper.parseSegmentacionPN(expediente));
      }
    }
    expedientesDevolver = segmentacionPNUtil.filtrarDtos(expedienteSinGestorList, orderField, orderDirection, idConcatenado, valorBienes, provincia, localidad, tipoProcedimiento, estadoOcupacion, cargasPosteriores, estrategia, rankingAgencia);
    List<ExpedientePNSegmentacionDTO> expedientesPaginados = expedientesDevolver.stream().skip(size * page).limit(size).collect(Collectors.toList());

    return new ListWithCountDTO<>(expedientesPaginados, expedientesDevolver.size());
  }

  public SegmentacionPNDTO createSegmentacion(SegmentacionPNInputDTO segmentacionInputDTO) throws Exception {
    SegmentacionPN segmentacion = segmentacionPNMapper.segmentacionPNInputASegmentacionPN(segmentacionInputDTO);
    SegmentacionPN segmentacionMetida = segmentacionPNRepository.save(segmentacion);
    return segmentacionPNMapper.segmentacionPNADto(segmentacionMetida, segmentacionInputDTO.getCartera());
  }



  public SegmentacionPNDTO modificarSegmentacion(SegmentacionPNInputDTO segmentacionInputDTO) throws Exception {
    if(segmentacionInputDTO.getId() == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("There is no such segmentation");
      else throw new NotFoundException("No existe esa segmentacion");
    }

    SegmentacionPN segmentacionAntigua = segmentacionPNRepository.findById(segmentacionInputDTO.getId()).orElse(null);
    if(segmentacionAntigua==null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("There is no such segmentation");
      else throw new NotFoundException("No existe esa segmentacion");
    }
    SegmentacionPN segmentacionNueva = segmentacionPNMapper.segmentacionPNInputASegmentacionPN(segmentacionInputDTO);
    SegmentacionPN segmentacionMetida = segmentacionPNRepository.save(segmentacionNueva);
    return segmentacionPNMapper.segmentacionPNADto(segmentacionMetida,segmentacionInputDTO.getCartera());
  }

}
