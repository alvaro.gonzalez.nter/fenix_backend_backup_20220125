package com.haya.alaska.bien_subastado.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.resultado_subasta.domain.ResultadoSubasta;
import com.haya.alaska.subasta.domain.Subasta;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_RELA_BIEN_SUBASTADO")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "RELA_BIEN_SUBASTADO")
public class BienSubastado implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SUBASTA")
  private Subasta subasta;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  private Bien bien;

  @Column(name = "DES_NUM_FINCA")
  private String numeroFinca;
  @Column(name = "IND_POSTORES")
  private Boolean postores;
  @Column(name = "DES_ENTIDAD_ADJUDICADA")
  private String entidadAdjudicada;
  @Column(name = "DES_DETALLE_RESULT")
  private String detalleResultado;
  @Column(name = "NUM_IMPORTE_ADJUDICACION")
  private Double importeAdjudicacion;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_DECRETO_ADJUDICACION")
  private Date fechaDecretoAdjudicacion;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_TESTIMONIO")
  private Date fechaTestimonio;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_SENYALAMIENTO")
  private Date fechaSenyalamiento;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_POSTERIOR_LANZAMIENTO")
  private Date fechaPosteriorLanzamiento;
  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESULTADO_SUBASTA")
  private ResultadoSubasta resultadoSubasta;

  private String idRecovery;

  private String tipoSubasta;
}
