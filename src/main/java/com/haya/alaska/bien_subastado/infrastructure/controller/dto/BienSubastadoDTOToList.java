package com.haya.alaska.bien_subastado.infrastructure.controller.dto;

import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BienSubastadoDTOToList implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer id;
  private Integer subasta;
  private Integer bien;
  private String numeroFinca;
  private Boolean postores;
  private String entidadAdjudicada;
  private String detalleResultado;
  private Double importeAdjudicacion;
  private Date fechaDecretoAdjudicacion;
  private Date fechaTestimonio;
  private Date fechaSenyalamiento;
  private Date fechaPosteriorLanzamiento;
  private CatalogoMinInfoDTO resultadoSubasta;
  private String idRecovery;
  private String tipoSubasta;

  public BienSubastadoDTOToList(BienSubastado bienSubastado) {

    BeanUtils.copyProperties(bienSubastado, this);

    this.resultadoSubasta =
      bienSubastado.getResultadoSubasta() != null
        ? new CatalogoMinInfoDTO(bienSubastado.getResultadoSubasta())
        : null;
    this.subasta = bienSubastado.getSubasta() != null ? bienSubastado.getSubasta().getId() : null;
    this.bien = bienSubastado.getBien() != null ? bienSubastado.getBien().getId() : null;
  }
}
