package com.haya.alaska.bien_subastado.infrastructure.repository;

import com.haya.alaska.bien.domain.Bien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.bien_subastado.domain.BienSubastado;

import java.util.Optional;
import java.util.Set;

@Repository
public interface BienSubastadoRepository extends JpaRepository<BienSubastado, Integer> {
  Optional<BienSubastado> findByBien(Bien bien);
  Set<BienSubastado> findAllByBien(Bien bien);
}
