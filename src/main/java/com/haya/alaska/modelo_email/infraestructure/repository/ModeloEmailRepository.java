package com.haya.alaska.modelo_email.infraestructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.modelo_email.domain.ModeloEmail;

@Repository
public interface ModeloEmailRepository extends CatalogoRepository<ModeloEmail, Integer>{}
