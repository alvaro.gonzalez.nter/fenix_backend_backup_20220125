package com.haya.alaska.modelo_burofax.infraestructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.modelo_burofax.domain.ModeloBurofax;

public interface ModeloBurofaxRepository extends CatalogoRepository<ModeloBurofax, Integer>{}
