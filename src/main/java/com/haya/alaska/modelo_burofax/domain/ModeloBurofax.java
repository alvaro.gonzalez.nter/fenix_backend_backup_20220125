package com.haya.alaska.modelo_burofax.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.haya.alaska.catalogo.domain.Catalogo;

import lombok.Getter;
import lombok.Setter;

@Audited
@AuditTable(value = "HIST_LKUP_MODELO_BUROFAX")
@Entity
@Table(name = "LKUP_MODELO_BUROFAX")
@Getter
@Setter
public class ModeloBurofax extends Catalogo {

  private static final long serialVersionUID = 1L;

  @Column(name = "DES_MOMBRE_MODELO")
  private String nombreModelo;
}
