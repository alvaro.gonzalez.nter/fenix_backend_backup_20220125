package com.haya.alaska.periodicidad_evento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.periodicidad_evento.domain.PeriodicidadEvento;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriodicidadEventoRepository extends CatalogoRepository<PeriodicidadEvento, Integer> {}
