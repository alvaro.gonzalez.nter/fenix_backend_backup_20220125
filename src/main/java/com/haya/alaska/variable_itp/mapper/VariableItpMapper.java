package com.haya.alaska.variable_itp.mapper;

import com.haya.alaska.variable_itp.controller.dto.VariableITPDTO;
import com.haya.alaska.variable_itp.domain.VariableItp;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class VariableItpMapper {

  public VariableITPDTO entidadADto(VariableItp entidad) {
    VariableITPDTO variableITPDTO = new VariableITPDTO();
    BeanUtils.copyProperties(entidad,variableITPDTO);
    variableITPDTO.setComunidadAutonoma(entidad.comunidadAutonoma.getValor());
    return variableITPDTO;
  }
}
