package com.haya.alaska.variable_itp.repository;

import com.haya.alaska.variable_itp.domain.VariableItp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VariableItpRepository extends JpaRepository<VariableItp, Integer> {
  List<VariableItp> findByComunidadAutonomaId(Integer id);

  List<VariableItp> findByComunidadAutonomaIdAndGaraje(Integer id, Boolean garaje);
}
