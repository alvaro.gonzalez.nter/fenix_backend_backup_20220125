package com.haya.alaska.variable_itp.controller.dto.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class ModificarVariableITPInputDTO implements Serializable {
  private Integer id;
  private Double nuevoImporteMinimo;
  private Double nuevoImporteMaximo;
  private Double nuevoPorcentaje;
}
