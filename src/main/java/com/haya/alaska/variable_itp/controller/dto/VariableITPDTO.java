package com.haya.alaska.variable_itp.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class VariableITPDTO implements Serializable {
  private Integer id;
  private Double importeMinimo;
  private Double importeMaximo;
  private Double porcentaje;
  private String comunidadAutonoma;
  private Boolean garaje;
}
