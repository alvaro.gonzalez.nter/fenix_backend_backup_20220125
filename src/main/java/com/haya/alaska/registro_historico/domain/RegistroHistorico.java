package com.haya.alaska.registro_historico.domain;

import com.haya.alaska.usuario.domain.Usuario;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import javax.persistence.*;

@Entity
@RevisionEntity(RegistroHistoricoListener.class)
@Getter
@Setter
@Table(name = "REGISTRO_HISTORICO")
public class RegistroHistorico {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @RevisionNumber
  private int id;

  @RevisionTimestamp
  private long timestamp;

  @JoinColumn(name = "ID_USUARIO")
  @ManyToOne(fetch = FetchType.LAZY)
  private Usuario usuario;
}
