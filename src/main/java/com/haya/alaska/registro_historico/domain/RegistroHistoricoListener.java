package com.haya.alaska.registro_historico.domain;

import com.haya.alaska.usuario.domain.Usuario;
import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class RegistroHistoricoListener implements RevisionListener {

  @Override
  public void newRevision(Object revisionEntity) {
    RegistroHistorico newRegistry = (RegistroHistorico) revisionEntity;
    Usuario usuario = null;
    if (isAuthenticated()) {
      usuario = getAuthenticated();
    }
    newRegistry.setUsuario(usuario);
  }

  public static boolean isAuthenticated() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return authentication != null && authentication.isAuthenticated();
  }

  public static Usuario getAuthenticated() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (isAnonymous(authentication)) return null;
    return (Usuario) authentication.getPrincipal();
  }

  private static boolean isAnonymous(Authentication authentication) {
    return authentication.getPrincipal().getClass().equals(String.class)
            && authentication.getPrincipal().equals("anonymousUser");
  }
}

