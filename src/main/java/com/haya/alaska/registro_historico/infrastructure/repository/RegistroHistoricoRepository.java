package com.haya.alaska.registro_historico.infrastructure.repository;

import com.haya.alaska.registro_historico.domain.RegistroHistorico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface RegistroHistoricoRepository  extends JpaRepository<RegistroHistorico, Integer> {



  @Query(
    value =
      "SELECT TIMESTAMP AS tiempo FROM REGISTRO_HISTORICO " +
        "WHERE ID = ?1",
    nativeQuery = true)
  BigInteger registroHistoricoTime(Integer idHistorico);






}
