package com.haya.alaska.registro_historico.application;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.historico.ContratoHistoricoAsignaciones;

import java.util.List;

public interface RegistroHistoricoUseCase {
  List<ContratoHistoricoAsignaciones> getHistoricoAsignacionesByContrato(Contrato contrato, Integer idExpediente) throws IllegalAccessException;
}
