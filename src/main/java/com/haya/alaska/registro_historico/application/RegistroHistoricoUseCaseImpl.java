package com.haya.alaska.registro_historico.application;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.historico.ContratoHistoricoAsignaciones;
import com.haya.alaska.contrato.historico.ContratoHistoricoInfo;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class RegistroHistoricoUseCaseImpl implements RegistroHistoricoUseCase{

  @Autowired
  private ContratoRepository contratoRepository;

  public ContratoHistoricoInfo parseContratoHistoricoInfo(Map historico)
          throws IllegalAccessException {
    ContratoHistoricoInfo contratoHistoricoInfo = new ContratoHistoricoInfo();
    Field[] declaredFields = ContratoHistoricoInfo.class.getDeclaredFields();
    for (Field declaredField : declaredFields) { // non entity fields
      declaredField.setAccessible(true);
      String name = declaredField.getName();
      declaredField.set(contratoHistoricoInfo, historico.get(name));
    }
    return contratoHistoricoInfo;
  }

  public List<ContratoHistoricoInfo> getRegistrosModificadosByActivo(List<Map> historico)
          throws IllegalAccessException {
    List<ContratoHistoricoInfo> result = new ArrayList<>();
    Boolean activo = true;
    Boolean primerRegistroActivo = true;
    for (Map registro : historico) {
      ContratoHistoricoInfo nuevoRegistro = parseContratoHistoricoInfo(registro);

      if (nuevoRegistro != null && nuevoRegistro.getExpedienteId() != null && nuevoRegistro.getFecha() != null){
        List<Map> usuarios = contratoRepository.findUsuHistGestorLess(nuevoRegistro.getIdEx(), nuevoRegistro.getFecha());
        if (usuarios.isEmpty()){
          usuarios = contratoRepository.findUsuHistGestorBig(nuevoRegistro.getIdEx(), nuevoRegistro.getFecha());
          if (usuarios.isEmpty());
          else {
            Map dato = usuarios.get(0);
            nuevoRegistro.setUsuarioGestor(dato.get("nombre").toString());
          }
        } else {
          Map dato = usuarios.get(0);
          nuevoRegistro.setUsuarioGestor(dato.get("nombre").toString());
        }
      }



      if(nuevoRegistro != null && nuevoRegistro.getActivo() != null && nuevoRegistro.getActivo() && primerRegistroActivo) {
        result.add(nuevoRegistro);
        primerRegistroActivo = false;
      }else if (nuevoRegistro.getActivo() != activo) {
        result.add(nuevoRegistro);
        activo = !activo;
      }
    }
    return result;
  }

  public List<ContratoHistoricoAsignaciones> getHistoricoAsignacionesByContrato(Contrato contrato, Integer idExpediente) throws IllegalAccessException {

    // Array de salida en el que se guardarán cada registro con fecha de inicio y fin de cada
    // asignación del contrato
    List<ContratoHistoricoAsignaciones> contratoHistoricoAsignaciones = new ArrayList<>();

    List<Map> historicoAsignaciones = contratoRepository.findAllRegistroHistoricoByContratoId(contrato.getId(), idExpediente);

    List<ContratoHistoricoInfo> historicoRegistros = this.getRegistrosModificadosByActivo(historicoAsignaciones);

    if(historicoRegistros.isEmpty()) return new ArrayList<>();
    // iteramos entre los registros en los que se ha editado el atributo activo seleccionados
    // previamente
    for (int i = 0; i < historicoRegistros.size() - 1; i += 2) {
      ContratoHistoricoInfo registroA = historicoRegistros.get(i);
      ContratoHistoricoInfo registroB = historicoRegistros.get(i + 1);
      Date fechaInicio = registroA.parseDate();
      Date fechaFin = registroB.parseDate();
      ContratoHistoricoAsignaciones nuevaAsignacion =
              new ContratoHistoricoAsignaciones(fechaInicio, fechaFin, registroA.getRevInfo(), registroB.getRevInfo(), registroA.getExpedienteId(), registroA.getUsuarioGestor());
      contratoHistoricoAsignaciones.add(nuevaAsignacion);
    }
    ContratoHistoricoInfo ultimoRegistro = historicoRegistros.get(historicoRegistros.size() - 1);
    ContratoHistoricoAsignaciones ultimaAsignacion =
            new ContratoHistoricoAsignaciones(ultimoRegistro.parseDate(), null, ultimoRegistro.getRevInfo(), null, ultimoRegistro.getExpedienteId(), ultimoRegistro.getUsuarioGestor());
    contratoHistoricoAsignaciones.add(ultimaAsignacion);

    return contratoHistoricoAsignaciones;
  }
}
