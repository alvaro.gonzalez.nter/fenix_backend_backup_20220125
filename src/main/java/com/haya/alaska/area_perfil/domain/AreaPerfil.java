package com.haya.alaska.area_perfil.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.usuario.domain.Usuario;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_AREA_PERFIL")
@Entity
@Table(name = "LKUP_AREA_PERFIL")
public class AreaPerfil extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AREA_PERFIL")
  @NotAudited
  private Set<Usuario> usuarios = new HashSet<>();
}
