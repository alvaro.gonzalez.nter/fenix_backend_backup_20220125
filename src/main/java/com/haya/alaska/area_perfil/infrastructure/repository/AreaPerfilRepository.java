package com.haya.alaska.area_perfil.infrastructure.repository;

import com.haya.alaska.area_perfil.domain.AreaPerfil;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AreaPerfilRepository extends CatalogoRepository<AreaPerfil, Integer> {}
