package com.haya.alaska.business_plan_estrategia.domain;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.business_plan.domain.BusinessPlan;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Audited
@AuditTable(value = "HIST_RELA_BUSINESS_PLAN_ESTRATEGIA")
@Entity
@Getter
@Setter
@Table(name = "RELA_BUSINESS_PLAN_ESTRATEGIA")
public class BusinessPlanEstrategia implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BUSINESS_PLAN")
  private BusinessPlan businessPlan;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTRATEGIA", nullable = false)
  private Estrategia estrategia;

  @Column(name = "NUM_IMPORTE_RECUPERADO")
  private Double importeRecuperado;

  @Column(name = "NUM_IMPORTE_SALIDA")
  private Double importeSalida;

  @Column(name = "NUM_PRECIO_COMPRA")
  private Double precioCompra;

  @Column(name = "NUM_GASTOS_DIRECTOS")
  private Double gastosDirectos;

  @Column(name = "NUM_GASTOS_INDIRECTOS")
  private Double gastosIndirectos;

  @Column(name = "NUM_VAN")
  private Double van;

  @Column(name = "FCH_FECHA_INICIO")
  private Date fechaInicio;

  @Column(name = "FCH_FECHA_FIN")
  private Date fechaFin;

  @Column(name = "NUM_NUMERO_OPERACION")
  private Integer numeroOperacion;

  @Column(name = "NUM_PORCENTAJE_CUMPLIMIENTO")
  private Double porcentajeCumplimiento;

  @Column(name = "NUM_FLUJO_NETO")
  private Double flujoNeto;

  @Column(name = "NUM_AMORTIZACION")
  private Double amortizacion;

  @Column(name = "NUM_MULTIPLO_INVERSION")
  private Double multiploInversion;

  @Column(name = "NUM_TIR")
  private Double tir;

  @Column(name = "FCH_FECHA_OBJETIVO")
  private Date fechaObjetivo;

  @Column(name = "DES_ACTIVIDAD")
  private String actividad;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  private Bien bien;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AREA_USUARIO")
  private AreaUsuario area;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  private Usuario gestor;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_GRUPO_USUARIO")
  private GrupoUsuario grupo;

  public void update(BusinessPlanEstrategia businessPlanEstrategia) {
    if (businessPlanEstrategia == null) return;
    if (businessPlanEstrategia.getImporteRecuperado() != null)
      this.setImporteRecuperado(businessPlanEstrategia.getImporteRecuperado());
    if (businessPlanEstrategia.getImporteSalida() != null)
      this.setImporteSalida(businessPlanEstrategia.getImporteSalida());
    if (businessPlanEstrategia.getActividad() != null) this.setActividad(businessPlanEstrategia.getActividad());
    if (businessPlanEstrategia.getAmortizacion() != null)
      this.setAmortizacion(businessPlanEstrategia.getAmortizacion());
    if (businessPlanEstrategia.getEstrategia() != null) this.setEstrategia(businessPlanEstrategia.getEstrategia());
    if (businessPlanEstrategia.getFechaObjetivo() != null)
      this.setFechaObjetivo(businessPlanEstrategia.getFechaObjetivo());
    if (businessPlanEstrategia.getFechaFin() != null) this.setFechaFin(businessPlanEstrategia.getFechaFin());
    if (businessPlanEstrategia.getFechaInicio() != null) this.setFechaInicio(businessPlanEstrategia.getFechaInicio());
    if (businessPlanEstrategia.getFlujoNeto() != null) this.setFlujoNeto(businessPlanEstrategia.getFlujoNeto());
    if (businessPlanEstrategia.getGastosDirectos() != null)
      this.setGastosDirectos(businessPlanEstrategia.getGastosDirectos());
    if (businessPlanEstrategia.getGastosIndirectos() != null)
      this.setGastosIndirectos(businessPlanEstrategia.getGastosIndirectos());
    if (businessPlanEstrategia.getMultiploInversion() != null)
      this.setMultiploInversion(businessPlanEstrategia.getMultiploInversion());
    if (businessPlanEstrategia.getNumeroOperacion() != null)
      this.setNumeroOperacion(businessPlanEstrategia.getNumeroOperacion());
    if (businessPlanEstrategia.getPorcentajeCumplimiento() != null)
      this.setPorcentajeCumplimiento(businessPlanEstrategia.getPorcentajeCumplimiento());
    if (businessPlanEstrategia.getTir() != null) this.setTir(businessPlanEstrategia.getTir());
    if (businessPlanEstrategia.getVan() != null) this.setVan(businessPlanEstrategia.getVan());
    if (businessPlanEstrategia.getGestor() != null) this.setGestor(businessPlanEstrategia.getGestor());
    if (businessPlanEstrategia.getArea() != null) this.setArea(businessPlanEstrategia.getArea());
    if (businessPlanEstrategia.getGrupo() != null) this.setGrupo(businessPlanEstrategia.getGrupo());
    if (businessPlanEstrategia.getBien() != null) this.setBien(businessPlanEstrategia.getBien());
  }
}
