package com.haya.alaska.business_plan_estrategia.infrastructure.mapper;

import com.haya.alaska.bien.infrastructure.mapper.BienMapper;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto.BusinessPlanEstrategiaInputDTO;
import com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto.BusinessPlanEstrategiaOutputDTO;
import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.GrupoUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.mapper.UsuarioMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BusinessPlanEstrategiaMapper {

  @Autowired
  CatalogoMinInfoMapper catalogoMinInfoMapper;
  @Autowired
  EstrategiaRepository estrategiaRepository;
  @Autowired
  UsuarioMapper usuarioMapper;
  @Autowired
  BienMapper bienMapper;

  public BusinessPlanEstrategia inputDTOtoEntity(BusinessPlanEstrategiaInputDTO businessPlanEstrategiaInputDTO) throws Exception {
    BusinessPlanEstrategia businessPlanEstrategia = new BusinessPlanEstrategia();
    businessPlanEstrategia.setFechaFin(businessPlanEstrategiaInputDTO.getFechaFin());
    businessPlanEstrategia.setFechaInicio(businessPlanEstrategiaInputDTO.getFechaInicio());
    businessPlanEstrategia.setGastosDirectos(businessPlanEstrategiaInputDTO.getGastosDirectos());
    businessPlanEstrategia.setGastosIndirectos(businessPlanEstrategia.getGastosIndirectos());
    businessPlanEstrategia.setImporteRecuperado(businessPlanEstrategiaInputDTO.getImporteRecuperado());
    businessPlanEstrategia.setImporteSalida(businessPlanEstrategiaInputDTO.getImporteSalida());
    businessPlanEstrategia.setVan(businessPlanEstrategiaInputDTO.getVan());
    businessPlanEstrategia.setPrecioCompra(businessPlanEstrategiaInputDTO.getPrecioCompra());
    businessPlanEstrategia.setFlujoNeto(businessPlanEstrategiaInputDTO.getFlujoNeto());
    businessPlanEstrategia.setAmortizacion(businessPlanEstrategiaInputDTO.getAmortizacion());
    businessPlanEstrategia.setMultiploInversion(businessPlanEstrategiaInputDTO.getMultiploInversion());
    businessPlanEstrategia.setPorcentajeCumplimiento(businessPlanEstrategiaInputDTO.getPorcentajeCumplimiento());
    businessPlanEstrategia.setNumeroOperacion(businessPlanEstrategiaInputDTO.getNumeroOperacion());
    businessPlanEstrategia.setTir(businessPlanEstrategiaInputDTO.getTir());
    businessPlanEstrategia.setFechaObjetivo(businessPlanEstrategiaInputDTO.getFechaObjetivo());
    businessPlanEstrategia.setActividad(businessPlanEstrategiaInputDTO.getActividad());
    businessPlanEstrategia.setEstrategia(estrategiaRepository.findById(businessPlanEstrategiaInputDTO.getEstrategia()).orElseThrow(() -> new Exception("Estrategia no encontrada con el id :" + businessPlanEstrategiaInputDTO.getEstrategia())));
    return businessPlanEstrategia;
  }

  public BusinessPlanEstrategiaOutputDTO entityToOutputDTO(BusinessPlanEstrategia businessPlanEstrategia) {
    BusinessPlanEstrategiaOutputDTO businessPlanEstrategiaOutputDTO = new BusinessPlanEstrategiaOutputDTO();
    businessPlanEstrategiaOutputDTO.setId(businessPlanEstrategia.getId());
    businessPlanEstrategiaOutputDTO.setFechaFin(businessPlanEstrategia.getFechaFin());
    businessPlanEstrategiaOutputDTO.setFechaInicio(businessPlanEstrategia.getFechaInicio());
    businessPlanEstrategiaOutputDTO.setGastosDirectos(businessPlanEstrategia.getGastosDirectos());
    businessPlanEstrategiaOutputDTO.setGastosIndirectos(businessPlanEstrategia.getGastosIndirectos());
    businessPlanEstrategiaOutputDTO.setImporteRecuperado(businessPlanEstrategia.getImporteRecuperado());
    businessPlanEstrategiaOutputDTO.setImporteSalida(businessPlanEstrategia.getImporteSalida());
    businessPlanEstrategiaOutputDTO.setVan(businessPlanEstrategia.getVan());
    businessPlanEstrategiaOutputDTO.setPrecioCompra(businessPlanEstrategia.getPrecioCompra());
    businessPlanEstrategiaOutputDTO.setFlujoNeto(businessPlanEstrategia.getFlujoNeto());
    businessPlanEstrategiaOutputDTO.setAmortizacion(businessPlanEstrategia.getAmortizacion());
    businessPlanEstrategiaOutputDTO.setMultiploInversion(businessPlanEstrategia.getMultiploInversion());
    businessPlanEstrategiaOutputDTO.setPorcentajeCumplimiento(businessPlanEstrategia.getPorcentajeCumplimiento());
    businessPlanEstrategiaOutputDTO.setNumeroOperacion(businessPlanEstrategia.getNumeroOperacion());
    businessPlanEstrategiaOutputDTO.setTir(businessPlanEstrategia.getTir());
    businessPlanEstrategiaOutputDTO.setFechaObjetivo(businessPlanEstrategia.getFechaObjetivo());
    businessPlanEstrategiaOutputDTO.setActividad(businessPlanEstrategia.getActividad());
    businessPlanEstrategiaOutputDTO.setTipoBP(businessPlanEstrategia.getBusinessPlan().getTipoBP());
    businessPlanEstrategiaOutputDTO.setGestor(businessPlanEstrategia.getGestor() != null ? usuarioMapper.entityToDTO(businessPlanEstrategia.getGestor()) : null);
    businessPlanEstrategiaOutputDTO.setBien(businessPlanEstrategia.getBien() != null ? bienMapper.entityOutputDto(businessPlanEstrategia.getBien(), null) : null);
    businessPlanEstrategiaOutputDTO.setArea(businessPlanEstrategia.getArea() != null ? new AreaUsuarioOutputDTO(businessPlanEstrategia.getArea()) : null);
    businessPlanEstrategiaOutputDTO.setGrupo(businessPlanEstrategia.getGrupo() != null ? new GrupoUsuarioOutputDTO(businessPlanEstrategia.getGrupo()) : null);
    businessPlanEstrategiaOutputDTO.setEstrategia(catalogoMinInfoMapper.entityToDto(businessPlanEstrategia.getEstrategia()));
    return businessPlanEstrategiaOutputDTO;
  }
}
