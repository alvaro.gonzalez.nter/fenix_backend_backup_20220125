package com.haya.alaska.business_plan_estrategia.infrastructure.controller;

import com.haya.alaska.business_plan_estrategia.application.BusinessPlanEstrategiaUseCase;
import com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto.BusinessPlanEstrategiaInputDTO;
import com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto.BusinessPlanEstrategiaOutputDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/businessPlan")
public class BusinessPlanEstrategiaController {

  @Autowired
  BusinessPlanEstrategiaUseCase businessPlanEstrategiaUseCase;

  @ApiOperation(value = "Actualizar un Business Plan Estrategia", notes = "")
  @PutMapping("/estrategia/{id}")
  public BusinessPlanEstrategiaOutputDTO updateBusinessPlanEstrategia(@PathVariable("id") int id, @RequestBody BusinessPlanEstrategiaInputDTO businessPlanEstrategiaInputDTO) throws Exception {
    return businessPlanEstrategiaUseCase.update(id, businessPlanEstrategiaInputDTO);
  }

  @ApiOperation(value = "Borrar un Business Plan Estrategia", notes = "")
  @DeleteMapping("/estrategia/{id}")
  public void deleteBusinessPlanEstrategia(@RequestParam int id) throws Exception {
    businessPlanEstrategiaUseCase.delete(id);
  }
}
