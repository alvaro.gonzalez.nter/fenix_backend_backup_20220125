package com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto;

import com.haya.alaska.bien.infrastructure.controller.dto.BienOutputDto;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.GrupoUsuarioOutputDTO;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusinessPlanEstrategiaOutputDTO {

  private Integer id;
  private CatalogoMinInfoDTO estrategia;
  private Double precioCompra;
  private Double importeRecuperado;
  private Double importeSalida;
  private Double gastosDirectos;
  private Double gastosIndirectos;
  private Double van;
  private Date fechaInicio;
  private Date fechaFin;
  private Double flujoNeto;
  private Double amortizacion;
  private Double multiploInversion;
  private Double porcentajeCumplimiento;
  private Integer numeroOperacion;
  private Double tir;
  private String actividad;
  private String tipoBP;
  private Date fechaObjetivo;
  private UsuarioOutputDTO gestor;
  private AreaUsuarioOutputDTO area;
  private GrupoUsuarioOutputDTO grupo;
  private BienOutputDto bien;

}
