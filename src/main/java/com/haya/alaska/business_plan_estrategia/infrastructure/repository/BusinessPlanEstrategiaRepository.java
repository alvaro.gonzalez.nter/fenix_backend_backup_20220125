package com.haya.alaska.business_plan_estrategia.infrastructure.repository;

import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessPlanEstrategiaRepository extends JpaRepository<BusinessPlanEstrategia, Integer> {

  void deleteAllByBusinessPlanId(Integer id);

  BusinessPlanEstrategia findFirstByOrderByFechaInicioAsc();

  BusinessPlanEstrategia findFirstByOrderByFechaFinDesc();
}
