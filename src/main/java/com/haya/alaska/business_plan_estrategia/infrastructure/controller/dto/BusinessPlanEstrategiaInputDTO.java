package com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusinessPlanEstrategiaInputDTO {

  private Double precioCompra;
  private Double importeRecuperado;
  private Double importeSalida;
  private Double gastosDirectos;
  private Double gastosIndirectos;
  private Double van;
  private Date fechaInicio;
  private Date fechaFin;
  private Double flujoNeto;
  private Double amortizacion;
  private Double multiploInversion;
  private Double porcentajeCumplimiento;
  private Integer numeroOperacion;
  private Double tir;
  private String actividad;
  private Date fechaObjetivo;

  private Integer estrategia;

}
