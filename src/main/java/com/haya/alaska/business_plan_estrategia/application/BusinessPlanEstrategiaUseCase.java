package com.haya.alaska.business_plan_estrategia.application;


import com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto.BusinessPlanEstrategiaInputDTO;
import com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto.BusinessPlanEstrategiaOutputDTO;

public interface BusinessPlanEstrategiaUseCase {
  BusinessPlanEstrategiaOutputDTO update(int id, BusinessPlanEstrategiaInputDTO businessPlanEstrategiaInputDTO) throws Exception;

  void delete(Integer id);
}
