package com.haya.alaska.business_plan_estrategia.application;

import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto.BusinessPlanEstrategiaInputDTO;
import com.haya.alaska.business_plan_estrategia.infrastructure.controller.dto.BusinessPlanEstrategiaOutputDTO;
import com.haya.alaska.business_plan_estrategia.infrastructure.mapper.BusinessPlanEstrategiaMapper;
import com.haya.alaska.business_plan_estrategia.infrastructure.repository.BusinessPlanEstrategiaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Slf4j
public class BusinessPlaEstrategianUseCaseImpl implements BusinessPlanEstrategiaUseCase {

  @Autowired
  BusinessPlanEstrategiaRepository businessPlanEstrategiaRepository;
  @Autowired
  BusinessPlanEstrategiaMapper businessPlanEstrategiaMapper;

  @Override
  @Transactional
  public BusinessPlanEstrategiaOutputDTO update(int id, BusinessPlanEstrategiaInputDTO businessPlanEstrategiaInputDTO) throws Exception {
    BusinessPlanEstrategia businessPlanEstrategia = businessPlanEstrategiaRepository.findById(id).orElseThrow(() -> new Exception("Business Plan no encontrado con el id:" + id));
    BusinessPlanEstrategia businessPlanEstrategiaInput = businessPlanEstrategiaMapper.inputDTOtoEntity(businessPlanEstrategiaInputDTO);
    businessPlanEstrategia.update(businessPlanEstrategiaInput);
    businessPlanEstrategiaRepository.save(businessPlanEstrategia);
    return businessPlanEstrategiaMapper.entityToOutputDTO(businessPlanEstrategia);
  }

  @Override
  @Transactional
  public void delete(Integer id) {
    businessPlanEstrategiaRepository.deleteById(id);
  }
}


