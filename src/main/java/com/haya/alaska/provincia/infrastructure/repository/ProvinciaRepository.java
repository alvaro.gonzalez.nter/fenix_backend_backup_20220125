package com.haya.alaska.provincia.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.provincia.domain.Provincia;

@Repository
public interface ProvinciaRepository extends CatalogoRepository<Provincia, Integer> {
	 
}

