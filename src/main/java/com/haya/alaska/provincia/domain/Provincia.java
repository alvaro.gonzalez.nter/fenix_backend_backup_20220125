package com.haya.alaska.provincia.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.comunidad_autonoma.domain.ComunidadAutonoma;
import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.Getter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_PROVINCIA")
@Entity
@Getter
@Table(name = "LKUP_PROVINCIA")
public class Provincia extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA")
  @NotAudited
  private Set<Municipio> municipios = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA")
  @NotAudited
  private Set<Direccion> direcciones = new HashSet<>();

  @ManyToMany
  @JoinTable(name = "RELA_USUARIO_PROVINCIA",
    joinColumns = { @JoinColumn(name = "ID_PROVINCIA") },
    inverseJoinColumns = { @JoinColumn(name = "ID_USUARIO") }
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_USUARIO_PROVINCIA")
  private Set<Usuario> usuarios = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA")
  @NotAudited
  private Set<DatoDireccionST> direccionesST = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA_GARANTIA")
  @NotAudited
  private Set<Segmentacion> segmentacionesGarantia = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA_INTERVINIENTE")
  @NotAudited
  private Set<Segmentacion> segmentacionesInterviniente = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA_BIEN_PN")
  @NotAudited
  private Set<SegmentacionPN> segmentacionesBienPN = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_COMUNIDAD_AUTONOMA")
  public ComunidadAutonoma comunidadAutonoma;
}
