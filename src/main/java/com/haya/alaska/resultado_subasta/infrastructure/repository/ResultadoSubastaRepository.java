package com.haya.alaska.resultado_subasta.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.resultado_subasta.domain.ResultadoSubasta;

@Repository
public interface ResultadoSubastaRepository extends CatalogoRepository<ResultadoSubasta, Integer> {}
