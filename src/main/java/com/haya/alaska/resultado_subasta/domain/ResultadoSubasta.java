package com.haya.alaska.resultado_subasta.domain;

import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.catalogo.domain.Catalogo;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_RESULTADO_SUBASTA")
@Entity
@Table(name = "LKUP_RESULTADO_SUBASTA")
public class ResultadoSubasta extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESULTADO_SUBASTA")
  @NotAudited
  private Set<BienSubastado> bienesSubastados = new HashSet<>();
}
