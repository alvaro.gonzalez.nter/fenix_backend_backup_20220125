package com.haya.alaska.tipo_movimiento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_movimiento.domain.TipoMovimiento;

import org.springframework.stereotype.Repository;

@Repository
public interface TipoMovimientoRepository extends CatalogoRepository<TipoMovimiento, Integer> {}
