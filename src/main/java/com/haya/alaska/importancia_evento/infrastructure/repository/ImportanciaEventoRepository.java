package com.haya.alaska.importancia_evento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.importancia_evento.domain.ImportanciaEvento;
import org.springframework.stereotype.Repository;

@Repository
public interface ImportanciaEventoRepository extends CatalogoRepository<ImportanciaEvento, Integer> {}
