package com.haya.alaska.importancia_evento.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.evento.domain.Evento;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_IMPORTANCIA_EVENTO")
@Entity
@Table(name = "LKUP_IMPORTANCIA_EVENTO")
@NoArgsConstructor
public class ImportanciaEvento extends Catalogo {

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_IMPORTANCIA_EVENTO")
  @NotAudited
  private Set<Evento> eventos = new HashSet<>();
}
