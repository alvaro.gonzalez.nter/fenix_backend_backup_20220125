package com.haya.alaska.estado_expediente_posesion_negociada.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.Data;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Audited
@AuditTable(value = "HIST_LKUP_EXP_POSESION_NEGOCIADA")
@Entity
@Table(name = "LKUP_ESTADO_EXP_POSESION_NEGOCIADA")
public class EstadoExpedientePosesionNegociada extends Catalogo {
  private static final long serialVersionUID = 1L;

  public final static String REVISION="REV";
}
