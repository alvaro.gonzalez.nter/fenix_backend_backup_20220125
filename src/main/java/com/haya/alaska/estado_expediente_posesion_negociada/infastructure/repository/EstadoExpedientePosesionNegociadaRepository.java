package com.haya.alaska.estado_expediente_posesion_negociada.infastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estado_expediente_posesion_negociada.domain.EstadoExpedientePosesionNegociada;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoExpedientePosesionNegociadaRepository extends CatalogoRepository<EstadoExpedientePosesionNegociada, Integer> {
}
