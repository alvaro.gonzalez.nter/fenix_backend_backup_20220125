package com.haya.alaska.propuesta_bien.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.propuesta.domain.Propuesta;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;



@Audited
@AuditTable(value = "HIST_RELA_PROPUESTA_BIEN")
@Entity
@Getter
@Setter
@Table(name = "RELA_PROPUESTA_BIEN")
public class PropuestaBien implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_PROPUESTA")
    private Propuesta propuesta;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
    private Bien bien;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
    private Contrato contrato;

    @Column(name = "NUM_PRECIO_MIN_VENTA")
    private Double precioMinimoVenta;

    @Column(name = "NUM_PRECIO_WEB_VENTA")
    private Double precioWebVenta;

    @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
    private Boolean activo = true;
}
