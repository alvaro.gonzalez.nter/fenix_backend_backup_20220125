package com.haya.alaska.propuesta_bien.infrastructure;

import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PropuestaBienRepository extends JpaRepository<PropuestaBien, Integer> {

    Optional<PropuestaBien> findByPropuestaIdAndBienIdAndContratoId(Integer propuestaId, Integer bienId, Integer contratoId);
    Optional<PropuestaBien> findByPropuestaIdAndBienId(Integer propuestaId, Integer bienId);
    Optional<PropuestaBien> findByBienId(Integer bienId);
}
