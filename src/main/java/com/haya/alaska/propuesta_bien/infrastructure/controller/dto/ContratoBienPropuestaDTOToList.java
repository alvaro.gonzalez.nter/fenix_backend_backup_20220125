package com.haya.alaska.propuesta_bien.infrastructure.controller.dto;

import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContratoBienPropuestaDTOToList implements Serializable {

  private Integer id;
  private String idOrigen;
  private String producto;
  private Double responsabilidadHipotecaria;
  private Double precioMinimoVenta;
  private Double precioWebVenta;

  private static final long serialVersionUID = 1L;

  public ContratoBienPropuestaDTOToList(PropuestaBien pb) {
    if (pb != null) {
      var contrato = pb.getContrato();
      this.id = contrato.getId();
      this.idOrigen = contrato.getIdCarga();
      if (contrato.getResponsabilidadHipotecaria() != null)
        this.responsabilidadHipotecaria = contrato.getResponsabilidadHipotecaria();
      if (pb.getPrecioMinimoVenta() != null) this.precioMinimoVenta = pb.getPrecioMinimoVenta();
      if (pb.getPrecioWebVenta() != null) this.precioWebVenta = pb.getPrecioWebVenta();

      if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
        if (contrato.getProducto() != null) this.producto = contrato.getProducto().getValorIngles();

      } else {
        if (contrato.getProducto() != null) this.producto = contrato.getProducto().getValor();
      }
    }
  }
}
