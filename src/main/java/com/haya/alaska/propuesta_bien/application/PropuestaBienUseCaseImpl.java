package com.haya.alaska.propuesta_bien.application;

import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.propuesta_bien.infrastructure.PropuestaBienRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Locale;
import java.util.Optional;


@Component
public class PropuestaBienUseCaseImpl implements PropuestaBienUseCase {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private PropuestaBienRepository propuestaBienRepository;

    @Override
    public PropuestaBien findByPropuestaIdAndBienIdAndContratoId(Integer propuestaId, Integer bienId, Integer contratoId) throws NotFoundException {
        Optional<PropuestaBien> propuestaBien = propuestaBienRepository.findByPropuestaIdAndBienIdAndContratoId(propuestaId, bienId, contratoId);
        if(propuestaBien.isEmpty()){
            Locale loc = LocaleContextHolder.getLocale();
            if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                throw new NotFoundException("There is no proposal-good relationship with the following data: Proposed ID " + propuestaId + ", Good ID " + bienId + ", contract ID " + contratoId);
            else throw new NotFoundException("No existe la relación propuesta-bien con los siguientes datos: ID propuesta " + propuestaId + ", ID bien " + bienId + ", ID contrato " + contratoId);
        }
        return propuestaBien.get();
    }
}