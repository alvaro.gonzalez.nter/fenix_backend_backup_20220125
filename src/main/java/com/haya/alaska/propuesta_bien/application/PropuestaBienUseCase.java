package com.haya.alaska.propuesta_bien.application;

import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.shared.exceptions.NotFoundException;


public interface PropuestaBienUseCase {

    PropuestaBien findByPropuestaIdAndBienIdAndContratoId(Integer propuestaId, Integer bienId, Integer contratoId) throws NotFoundException;
}