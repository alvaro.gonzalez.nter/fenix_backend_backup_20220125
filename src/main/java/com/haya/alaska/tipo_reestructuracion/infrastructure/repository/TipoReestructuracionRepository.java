package com.haya.alaska.tipo_reestructuracion.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_reestructuracion.domain.TipoReestructuracion;

@Repository
public interface TipoReestructuracionRepository
    extends CatalogoRepository<TipoReestructuracion, Integer> {
	 
}
