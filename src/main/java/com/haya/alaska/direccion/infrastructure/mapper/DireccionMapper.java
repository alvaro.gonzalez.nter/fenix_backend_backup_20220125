package com.haya.alaska.direccion.infrastructure.mapper;

import java.util.ArrayList;
import java.util.List;

import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_via.domain.TipoVia;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionDTO;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionInputDTO;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;

@Component
public class DireccionMapper {

  @Autowired MunicipioRepository municipioRepository;
  @Autowired PaisRepository paisRepository;
  @Autowired ProvinciaRepository provinciaRepository;
  @Autowired TipoViaRepository tipoViaRepository;
  @Autowired EntornoRepository entornoRepository;
  @Autowired LocalidadRepository localidadRepository;
  @Autowired DireccionRepository direccionRepository;

  public static DireccionDTO parseEntityToDTO(Direccion direccion) {

    DireccionDTO direccionDTO = new DireccionDTO();
    BeanUtils.copyProperties(direccion, direccionDTO);
    direccionDTO.setEntorno(
        direccion.getEntorno() != null ? new CatalogoMinInfoDTO(direccion.getEntorno()) : null);
    direccionDTO.setTipoVia(
        direccion.getTipoVia() != null ? new CatalogoMinInfoDTO(direccion.getTipoVia()) : null);
    direccionDTO.setLocalidad(
        direccion.getLocalidad() != null ? new CatalogoMinInfoDTO(direccion.getLocalidad()) : null);
    direccionDTO.setMunicipio(
        direccion.getMunicipio() != null ? new CatalogoMinInfoDTO(direccion.getMunicipio()) : null);
    direccionDTO.setProvincia(
        direccion.getProvincia() != null ? new CatalogoMinInfoDTO(direccion.getProvincia()) : null);
    direccionDTO.setPais(
        direccion.getPais() != null ? new CatalogoMinInfoDTO(direccion.getPais()) : null);

    return direccionDTO;
  }

  public Direccion parseDTOToEntity(DireccionDTO direccionDTO) {

    Direccion direccion = new Direccion();

    BeanUtils.copyProperties(direccion, direccionDTO);

    return direccion;
  }

  public Direccion parseInputDTOToEntity(DireccionInputDTO direccionDTO) {

    Direccion direccion = new Direccion();

    BeanUtils.copyProperties(direccionDTO, direccion);
    direccion.setLocalidad(localidadRepository.findById(direccionDTO.getLocalidad()).orElse(null));
    direccion.setEntorno(entornoRepository.findById(direccionDTO.getEntorno()).orElse(null));
    direccion.setMunicipio(municipioRepository.findById(direccionDTO.getMunicipio()).orElse(null));
    direccion.setPais(paisRepository.findById(direccionDTO.getPais()).orElse(null));
    direccion.setProvincia(provinciaRepository.findById(direccionDTO.getProvincia()).orElse(null));
    direccion.setTipoVia(tipoViaRepository.findById(direccionDTO.getTipoVia()).orElse(null));

    return direccion;
  }

  public Direccion parse(DireccionInputDTO direccionDTO) throws NotFoundException {
    Direccion direccion = new Direccion();
    if (direccionDTO.getId() != null) direccion = direccionRepository.findById(direccionDTO.getId()).orElse(new Direccion());
    BeanUtils.copyProperties(direccionDTO, direccion);
    Municipio municipio =
            direccionDTO.getMunicipio() != null
                    ? municipioRepository
                    .findById(direccionDTO.getMunicipio())
                    .orElseThrow(() -> new NotFoundException("Municipio", direccionDTO.getMunicipio()))
                    : null;
    direccion.setMunicipio(municipio);
    Localidad localidad =
            direccionDTO.getLocalidad() != null
                    ? localidadRepository
                    .findById(direccionDTO.getLocalidad())
                    .orElseThrow(() -> new NotFoundException("Localidad", direccionDTO.getLocalidad()))
                    : null;
    direccion.setLocalidad(localidad);
    Entorno entorno =
            direccionDTO.getEntorno() != null
                    ? entornoRepository
                    .findById(direccionDTO.getEntorno())
                    .orElseThrow(() -> new NotFoundException("Entorno", direccionDTO.getEntorno()))
                    : null;
    direccion.setEntorno(entorno);
    Provincia provincia =
            direccionDTO.getProvincia() != null
                    ? provinciaRepository
                    .findById(direccionDTO.getProvincia())
                    .orElseThrow(() -> new NotFoundException("Provincia", direccionDTO.getProvincia()))
                    : null;
    direccion.setProvincia(provincia);
    Pais pais =
            direccionDTO.getPais() != null
                    ? paisRepository
                    .findById(direccionDTO.getPais())
                    .orElseThrow(() -> new NotFoundException("País", direccionDTO.getPais()))
                    : null;
    direccion.setPais(pais);
    TipoVia tipoVia =
            direccionDTO.getTipoVia() != null
                    ? tipoViaRepository
                    .findById(direccionDTO.getTipoVia())
                    .orElseThrow(() -> new NotFoundException("Tipo vía", direccionDTO.getTipoVia()))
                    : null;
    direccion.setTipoVia(tipoVia);
    return direccion;
  }

  public List<Direccion> listInputDtoToEntityList(List<DireccionInputDTO> direccionesDTO) {
    List<Direccion> direcciones = new ArrayList<>();
    for (DireccionInputDTO direccionDTO : direccionesDTO) {
      if(direccionDTO.getId() != null) direcciones.add(parseInputDTOToEntity(direccionDTO));
    }
    return direcciones;
  }
}
