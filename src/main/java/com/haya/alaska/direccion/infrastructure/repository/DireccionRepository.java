package com.haya.alaska.direccion.infrastructure.repository;

import com.haya.alaska.direccion.domain.Direccion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DireccionRepository extends JpaRepository<Direccion, Integer> {

  List<Direccion> findByIntervinienteId(Integer id);

  List<Direccion> findByOcupanteId(Integer id);

  List<Direccion> findByOcupanteIdNotNull();

  @Query(value = "SELECT * FROM MSTR_DIRECCION WHERE NUM_LONGITUD =0 OR NUM_LONGITUD  IS NULL OR NUM_LATITUD = 0 OR NUM_LATITUD  IS NULL",
    nativeQuery = true)
  List<Direccion> findAllByLongitudIsNullAndLatitudIsNull();

  List<Direccion> findAllByIntervinienteIdAndOrigen(Integer idInterviniente, String origen);
}
