package com.haya.alaska.direccion.infrastructure.controller.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DireccionInputDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer id;
  private Boolean principal;

  private String nombre;
  private String numero;
  private String portal;
  private String bloque;
  private String escalera;
  private String piso;
  private String puerta;
  private String comentario;
  private String codigoPostal;
  private Double longitud;
  private Double latitud;
  private Boolean validada;
  private Boolean direccionGarantia;
  private String origen;
  private Date fechaActualizacion;

  private Integer tipoVia;
  private Integer municipio;
  private Integer localidad;
  private Integer entorno;
  private Integer provincia;
  private Integer pais;
}
