package com.haya.alaska.direccion.infrastructure.util;

import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.controller.dto.DireccionInputDTO;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.interviniente.infrastructure.util.IntervinienteUtil;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.OcupanteUtil;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.shared.Utils;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
public class DireccionUtil {

  @Autowired DireccionRepository direccionRepository;
  @Autowired MunicipioRepository municipioRepository;
  @Autowired LocalidadRepository localidadRepository;
  @Autowired EntornoRepository entornoRepository;
  @Autowired ProvinciaRepository provinciaRepository;
  @Autowired PaisRepository paisRepository;
  @Autowired TipoViaRepository tipoViaRepository;
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired IntervinienteUtil intervinienteUtil;
  @Autowired OcupanteUtil ocupanteUtil;
  @Autowired OcupanteRepository ocupanteRepository;

  public void createDireccionesToInterviniente(
      List<DireccionInputDTO> direccionesDTO, Interviniente interviniente) throws Exception {
    List<Direccion> direccionesCreadas = new ArrayList<>();
    Integer idInterviniente = interviniente.getId();
    for (DireccionInputDTO direccionInputDTO : direccionesDTO) {
      Direccion direccion = new Direccion();

      //       comprobamos que no haya una dirección para el interviniente marcada como principal
      if (!checkDireccionAsPrincipal(direccionInputDTO, idInterviniente)) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new RequiredValueException("There is already an address marked as main for the participant with the id:" + idInterviniente);
        else throw new RequiredValueException("Ya existe una dirección marcada como principal para el interviniente con el id: " + idInterviniente);
      }

      BeanUtils.copyProperties(direccionInputDTO, direccion, "origen", "fechaActualizacion");
      direccion.setOrigen("Manual");
      direccion.setFechaActualizacion(new Date());
      direccion.setInterviniente(interviniente);
      if (direccionInputDTO.getMunicipio() != null) {
        Municipio municipio =
            municipioRepository.findById(direccionInputDTO.getMunicipio()).orElse(null);
        if (municipio != null) direccion.setMunicipio(municipio);
      }
      if (direccionInputDTO.getLocalidad() != null) {
        Localidad localidad =
            localidadRepository.findById(direccionInputDTO.getLocalidad()).orElse(null);
        if (localidad != null) direccion.setLocalidad(localidad);
      }
      if (direccionInputDTO.getEntorno() != null) {
        Entorno entorno = entornoRepository.findById(direccionInputDTO.getEntorno()).orElse(null);
        if (entorno != null) direccion.setEntorno(entorno);
      }
      if (direccionInputDTO.getEntorno() != null) {
        Entorno entorno = entornoRepository.findById(direccionInputDTO.getEntorno()).orElse(null);
        if (entorno != null) direccion.setEntorno(entorno);
      }
      if (direccionInputDTO.getProvincia() != null) {
        Provincia provincia =
            provinciaRepository.findById(direccionInputDTO.getProvincia()).orElse(null);
        if (provincia != null) direccion.setProvincia(provincia);
      }
      if (direccionInputDTO.getPais() != null) {
        Pais pais = paisRepository.findById(direccionInputDTO.getPais()).orElse(null);
        if (pais != null) direccion.setPais(pais);
      }
      if (direccionInputDTO.getTipoVia() != null) {
        TipoVia tipoVia = tipoViaRepository.findById(direccionInputDTO.getTipoVia()).orElse(null);
        if (tipoVia != null) direccion.setTipoVia(tipoVia);
      }

      intervinienteUtil.direccionGoogle(direccion);
      Direccion direccionCreada = direccionRepository.saveAndFlush(direccion);
      direccionesCreadas.add(direccionCreada);
    }

    if (!direccionesCreadas.isEmpty()) {
      List<Integer> idDireccionesCreadas =
          direccionesCreadas.stream().map(Direccion::getId).collect(Collectors.toList());
      addDirecciones(interviniente.getId(), idDireccionesCreadas);
    }
  }

  public void createDireccionesToOcupante(
    List<DireccionInputDTO> direccionesDTO, Ocupante ocupante) throws Exception {
    List<Direccion> direccionesCreadas = new ArrayList<>();
    Integer idInterviniente = ocupante.getId();
    for (DireccionInputDTO direccionInputDTO : direccionesDTO) {
      Direccion direccion = new Direccion();

      //       comprobamos que no haya una dirección para el ocupante marcada como principal
      if (!checkDireccionAsPrincipalOcupante(direccionInputDTO, idInterviniente)) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new RequiredValueException("There is already an address marked as main for the occupant with the id:" + idInterviniente);
        else throw new RequiredValueException("Ya existe una dirección marcada como principal para el ocupante con el id: " + idInterviniente);
      }

      BeanUtils.copyProperties(direccionInputDTO, direccion);
      direccion.setOcupante(ocupante);
      if (direccionInputDTO.getMunicipio() != null) {
        Municipio municipio =
          municipioRepository.findById(direccionInputDTO.getMunicipio()).orElse(null);
        if (municipio != null) direccion.setMunicipio(municipio);
      }
      if (direccionInputDTO.getLocalidad() != null) {
        Localidad localidad =
          localidadRepository.findById(direccionInputDTO.getLocalidad()).orElse(null);
        if (localidad != null) direccion.setLocalidad(localidad);
      }
      if (direccionInputDTO.getEntorno() != null) {
        Entorno entorno = entornoRepository.findById(direccionInputDTO.getEntorno()).orElse(null);
        if (entorno != null) direccion.setEntorno(entorno);
      }
      if (direccionInputDTO.getEntorno() != null) {
        Entorno entorno = entornoRepository.findById(direccionInputDTO.getEntorno()).orElse(null);
        if (entorno != null) direccion.setEntorno(entorno);
      }
      if (direccionInputDTO.getProvincia() != null) {
        Provincia provincia =
          provinciaRepository.findById(direccionInputDTO.getProvincia()).orElse(null);
        if (provincia != null) direccion.setProvincia(provincia);
      }
      if (direccionInputDTO.getPais() != null) {
        Pais pais = paisRepository.findById(direccionInputDTO.getPais()).orElse(null);
        if (pais != null) direccion.setPais(pais);
      }
      if (direccionInputDTO.getTipoVia() != null) {
        TipoVia tipoVia = tipoViaRepository.findById(direccionInputDTO.getTipoVia()).orElse(null);
        if (tipoVia != null) direccion.setTipoVia(tipoVia);
      }
      intervinienteUtil.direccionGoogle(direccion);
      Direccion direccionCreada = direccionRepository.saveAndFlush(direccion);
      direccionesCreadas.add(direccionCreada);
    }

    if (!direccionesCreadas.isEmpty()) {
      List<Integer> idDireccionesCreadas =
        direccionesCreadas.stream().map(Direccion::getId).collect(Collectors.toList());
      addDireccionesOcupante(ocupante.getId(), idDireccionesCreadas);
    }
  }

  public Boolean checkDireccionAsPrincipal(
      DireccionInputDTO direccionInput, Integer idInterviniente) {
    List<Direccion> direcciones = direccionRepository.findByIntervinienteId(idInterviniente);
    if (direcciones.isEmpty()) {
      return true;
    }
    for (Direccion direccion : direcciones) {
      if (direccion.getId() != direccionInput.getId()
          && direccion.getPrincipal() == true
          && direccionInput.getPrincipal() == true) return false;
    }
    return true;
  }

  public Boolean checkDireccionAsPrincipalOcupante(
    DireccionInputDTO direccionInput, Integer idOcupante) {
    List<Direccion> direcciones = direccionRepository.findByOcupanteId(idOcupante);
    if (direcciones.isEmpty()) {
      return true;
    }
    for (Direccion direccion : direcciones) {
      if (direccion.getId() != direccionInput.getId()
        && direccion.getPrincipal() == true
        && direccionInput.getPrincipal() == true) return false;
    }
    return true;
  }

  public Direccion updateFields(Direccion direccion, DireccionInputDTO input, Interviniente interviniente)
      throws NotFoundException {

    BeanUtils.copyProperties(input, direccion, "id");
    direccion.setFechaActualizacion(new Date());
    direccion.setInterviniente(interviniente);

    /*if (direccion.getMunicipio() != null) {
      Municipio municipio =
          municipioRepository.findById(direccion.getMunicipio().getId()).orElse(null);
      if (municipio != null) direccion.setMunicipio(municipio);
    }
    if (direccion.getLocalidad() != null) {
      Localidad localidad =
          localidadRepository.findById(direccion.getLocalidad().getId()).orElse(null);
      if (localidad != null) direccion.setLocalidad(localidad);
    }
    if (direccion.getEntorno() != null) {
      Entorno entorno = entornoRepository.findById(direccion.getEntorno().getId()).orElse(null);
      if (entorno != null) direccion.setEntorno(entorno);
    }
    if (direccion.getProvincia() != null) {
      Provincia provincia =
          provinciaRepository.findById(direccion.getProvincia().getId()).orElse(null);
      if (provincia != null) direccion.setProvincia(provincia);
    }
    if (direccion.getPais() != null) {
      Pais pais = paisRepository.findById(direccion.getPais().getId()).orElse(null);
      if (pais != null) direccion.setPais(pais);
    }
    if (direccion.getTipoVia() != null) {
      VariableSimulacion tipoVia = tipoViaRepository.findById(direccion.getTipoVia().getId()).orElse(null);
      if (tipoVia != null) direccion.setTipoVia(tipoVia);
    }*/
    intervinienteUtil.direccionGoogle(direccion);
    return direccionRepository.save(direccion);
  }

  public Direccion updateFieldsOcupante(Direccion direccion, DireccionInputDTO input, Ocupante ocupante)
    throws NotFoundException {

    BeanUtils.copyProperties(input, direccion, "id");
    direccion.setFechaActualizacion(new Date());
    direccion.setOcupante(ocupante);
    intervinienteUtil.direccionGoogle(direccion);
    return direccionRepository.save(direccion);
  }

  public Direccion createDireccionToInterviniente(
      DireccionInputDTO direccionDTO, Interviniente interviniente) throws Exception {

    Municipio municipio =
        direccionDTO.getMunicipio() != null
            ? municipioRepository
                .findById(direccionDTO.getMunicipio())
                .orElseThrow(() -> new NotFoundException("Municipio", direccionDTO.getMunicipio()))
            : null;
    Localidad localidad =
        direccionDTO.getLocalidad() != null
            ? localidadRepository
                .findById(direccionDTO.getLocalidad())
                .orElseThrow(() -> new NotFoundException("Localidad", direccionDTO.getLocalidad()))
            : null;
    Entorno entorno =
        direccionDTO.getEntorno() != null
            ? entornoRepository
                .findById(direccionDTO.getEntorno())
                .orElseThrow(() -> new NotFoundException("Entorno", direccionDTO.getEntorno()))
            : null;
    Provincia provincia =
        direccionDTO.getProvincia() != null
            ? provinciaRepository
                .findById(direccionDTO.getProvincia())
                .orElseThrow(() -> new NotFoundException("Provincia", direccionDTO.getProvincia()))
            : null;
    Pais pais =
        direccionDTO.getPais() != null
            ? paisRepository
                .findById(direccionDTO.getPais())
                .orElseThrow(() -> new NotFoundException("País", direccionDTO.getPais()))
            : null;
    TipoVia tipoVia =
        direccionDTO.getTipoVia() != null
            ? tipoViaRepository
                .findById(direccionDTO.getTipoVia())
                .orElseThrow(() -> new NotFoundException("Tipo vía", direccionDTO.getTipoVia()))
            : null;
    Direccion direccion =
        new Direccion(
            null,
            direccionDTO.getPrincipal(),
            tipoVia,
            direccionDTO.getNombre(),
            direccionDTO.getNumero(),
            direccionDTO.getPortal(),
            direccionDTO.getBloque(),
            direccionDTO.getEscalera(),
            direccionDTO.getPiso(),
            direccionDTO.getPuerta(),
            municipio,
            localidad,
            entorno,
            provincia,
            pais,
            direccionDTO.getComentario(),
            direccionDTO.getCodigoPostal(),
            direccionDTO.getLongitud(),
            direccionDTO.getLatitud(),
            true,
            direccionDTO.getValidada(),
            direccionDTO.getDireccionGarantia(),
          "Manual",
          new Date(),
          new Date());
    direccion.setInterviniente(interviniente);
    intervinienteUtil.direccionGoogle(direccion);
    Direccion direccionCreada = direccionRepository.saveAndFlush(direccion);

    intervinienteUtil.addDireccion(interviniente.getId(), direccionCreada.getId());
    return direccionCreada;
  }

  public Direccion createDireccionToOcupante(
    DireccionInputDTO direccionDTO, Ocupante ocupante) throws Exception {

    Municipio municipio =
      direccionDTO.getMunicipio() != null
        ? municipioRepository
        .findById(direccionDTO.getMunicipio())
        .orElseThrow(() -> new NotFoundException("Municipio", direccionDTO.getMunicipio()))
        : null;
    Localidad localidad =
      direccionDTO.getLocalidad() != null
        ? localidadRepository
        .findById(direccionDTO.getLocalidad())
        .orElseThrow(() -> new NotFoundException("Localidad", direccionDTO.getLocalidad()))
        : null;
    Entorno entorno =
      direccionDTO.getEntorno() != null
        ? entornoRepository
        .findById(direccionDTO.getEntorno())
        .orElseThrow(() -> new NotFoundException("Entorno", direccionDTO.getEntorno()))
        : null;
    Provincia provincia =
      direccionDTO.getProvincia() != null
        ? provinciaRepository
        .findById(direccionDTO.getProvincia())
        .orElseThrow(() -> new NotFoundException("Provincia", direccionDTO.getProvincia()))
        : null;
    Pais pais =
      direccionDTO.getPais() != null
        ? paisRepository
        .findById(direccionDTO.getPais())
        .orElseThrow(() -> new NotFoundException("País", direccionDTO.getPais()))
        : null;
    TipoVia tipoVia =
      direccionDTO.getTipoVia() != null
        ? tipoViaRepository
        .findById(direccionDTO.getTipoVia())
        .orElseThrow(() -> new NotFoundException("Tipo vía", direccionDTO.getTipoVia()))
        : null;
    Direccion direccion =
      new Direccion(
        null,
        direccionDTO.getPrincipal(),
        tipoVia,
        direccionDTO.getNombre(),
        direccionDTO.getNumero(),
        direccionDTO.getPortal(),
        direccionDTO.getBloque(),
        direccionDTO.getEscalera(),
        direccionDTO.getPiso(),
        direccionDTO.getPuerta(),
        municipio,
        localidad,
        entorno,
        provincia,
        pais,
        direccionDTO.getComentario(),
        direccionDTO.getCodigoPostal(),
        direccionDTO.getLongitud(),
        direccionDTO.getLatitud(),
        true,
        direccionDTO.getValidada(),
        direccionDTO.getDireccionGarantia(),
        "Manual",
        new Date(),
        new Date());
    direccion.setOcupante(ocupante);
    intervinienteUtil.direccionGoogle(direccion);
    Direccion direccionCreada = direccionRepository.saveAndFlush(direccion);

    ocupanteUtil.addDireccionOcupante(ocupante.getId(), direccionCreada.getId());
    return direccionCreada;
  }

  public Interviniente addDirecciones(Integer idInterviniente, List<Integer> idDirecciones)
      throws NotFoundException {
    Interviniente interviniente =
        intervinienteRepository
            .findById(idInterviniente)
            .orElseThrow(() -> new NotFoundException("interviniente", idInterviniente));
    List<Direccion> direcciones = direccionRepository.findAllById(idDirecciones);
    interviniente.addDirecciones(direcciones);
    return intervinienteRepository.save(interviniente);
  }

  public Ocupante addDireccionesOcupante(Integer idOcupante, List<Integer> idDirecciones)
    throws NotFoundException {
    Ocupante ocupante =
      ocupanteRepository
        .findById(idOcupante)
        .orElseThrow(() -> new NotFoundException("interviniente", idOcupante));
    List<Direccion> direcciones = direccionRepository.findAllById(idDirecciones);
    ocupante.addDirecciones(direcciones);
    return ocupanteRepository.save(ocupante);
  }
}
