package com.haya.alaska.direccion.infrastructure.controller.dto;

import java.io.Serializable;
import java.util.Date;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class DireccionDTO implements Serializable {
  private static final long serialVersionUID = 1L;
  private Integer id;
  private Boolean principal;

  private String nombre;
  private String numero;
  private String portal;
  private String bloque;
  private String escalera;
  private String piso;
  private String puerta;
  private String comentario;
  private String codigoPostal;
  private Double longitud;
  private Double latitud;
  private Boolean validada;
  private Boolean direccionGarantia;
  private String origen;
  private Date fechaActualizacion;
  private Date fechaAlta;

  private CatalogoMinInfoDTO entorno;
  private CatalogoMinInfoDTO tipoVia;
  private CatalogoMinInfoDTO localidad;
  private CatalogoMinInfoDTO municipio;
  private CatalogoMinInfoDTO provincia;
  private CatalogoMinInfoDTO pais;

}
