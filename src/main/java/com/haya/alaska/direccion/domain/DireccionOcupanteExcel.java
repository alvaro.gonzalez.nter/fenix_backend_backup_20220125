package com.haya.alaska.direccion.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class DireccionOcupanteExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Asset Id");
      cabeceras.add("Occupant Id");
      cabeceras.add("Validated");
      cabeceras.add("Type of road");
      cabeceras.add("Street name");
      cabeceras.add("Nº");
      cabeceras.add("Block");
      cabeceras.add("Stairs");
      cabeceras.add("Floor");
      cabeceras.add("Door");
      cabeceras.add("Environment");
      cabeceras.add("Location");
      cabeceras.add("Municipality");
      cabeceras.add("Province");
      cabeceras.add("Comment Address");
      cabeceras.add("Postal Code");
      cabeceras.add("Country");
      cabeceras.add("Length");
      cabeceras.add("Latitude");
      cabeceras.add("View on Google");
      cabeceras.add("Origin");
    } else {
      cabeceras.add("Id Activo");
      cabeceras.add("Id Ocupante");
      cabeceras.add("Validada");
      cabeceras.add("Tipo de vía");
      cabeceras.add("Nombre de la calle");
      cabeceras.add("Nº");
      cabeceras.add("Bloque");
      cabeceras.add("Escalera");
      cabeceras.add("Piso");
      cabeceras.add("Puerta");
      cabeceras.add("Entorno");
      cabeceras.add("Localidad");
      cabeceras.add("Municipio");
      cabeceras.add("Provincia");
      cabeceras.add("Comentario Direccion");
      cabeceras.add("CP");
      cabeceras.add("País");
      cabeceras.add("Longitud");
      cabeceras.add("Latitud");
      cabeceras.add("Ver en Google");
      cabeceras.add("Origen");
    }
  }

  public DireccionOcupanteExcel(Direccion direccion) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Occupant Id", direccion.getOcupante().getId());
      this.add("Asset Id", direccion.getActivo());
      if (direccion.getValidada() != null)
        this.add("Under age", direccion.getValidada() ? "Yes" : "No");
      this.add("Type of road", direccion.getTipoVia());
      this.add("Street name", direccion.getNombre());
      this.add("Nº", direccion.getNumero());
      this.add("Block", direccion.getBloque());
      this.add("Stairs", direccion.getEscalera());
      this.add("Floor", direccion.getPiso());
      this.add("Door", direccion.getPuerta());
      this.add("Environment", direccion.getEntorno());
      this.add("Location", direccion.getLocalidad());
      this.add("Municipality", direccion.getMunicipio());
      this.add("Province", direccion.getProvincia());
      this.add("Comment Address", direccion.getComentario());
      this.add("Postal Code", direccion.getCodigoPostal());
      this.add("Country", direccion.getPais());
      this.add("Length", direccion.getLongitud());
      this.add("Latitude", direccion.getLatitud());
      this.add("View on Google", direccion.verEnGoogle());
      this.add("Origin", direccion.getOrigen());
    } else {
      this.add("Id Ocupante", direccion.getOcupante().getId());
      this.add("Id Activo", direccion.getActivo());
      if (direccion.getValidada() != null)
        this.add("Menor de edad", direccion.getValidada() ? "Si" : "No");
      this.add("Tipo de vía", direccion.getTipoVia());
      this.add("Nombre de la calle", direccion.getNombre());
      this.add("Nº", direccion.getNumero());
      this.add("Bloque", direccion.getBloque());
      this.add("Escalera", direccion.getEscalera());
      this.add("Piso", direccion.getPiso());
      this.add("Puerta", direccion.getPuerta());
      this.add("Entorno", direccion.getEntorno());
      this.add("Localidad", direccion.getLocalidad());
      this.add("Municipio", direccion.getMunicipio());
      this.add("Provincia", direccion.getProvincia());
      this.add("Comentario Direccion", direccion.getComentario());
      this.add("CP", direccion.getCodigoPostal());
      this.add("País", direccion.getPais());
      this.add("Longitud", direccion.getLongitud());
      this.add("Latitud", direccion.getLatitud());
      this.add("Ver en Google", direccion.verEnGoogle());
      this.add("Origen", direccion.getOrigen());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
