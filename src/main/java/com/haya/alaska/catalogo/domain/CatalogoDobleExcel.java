package com.haya.alaska.catalogo.domain;

import com.haya.alaska.campania.domain.Campania;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class CatalogoDobleExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Code (Type)");
      cabeceras.add("Value (Type)");
      cabeceras.add("English value (Type)");
      cabeceras.add("Code (Subtype)");
      cabeceras.add("Value (Subtype)");
      cabeceras.add("English value (Subtype)");

    } else {
      cabeceras.add("Codigo (Tipo)");
      cabeceras.add("Valor (Tipo)");
      cabeceras.add("Codigo (Subtipo)");
      cabeceras.add("Valor (Subtipo)");
    }
  }

  public CatalogoDobleExcel(Catalogo tipo, Catalogo subtipo) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Code (Type)", tipo.getCodigo());
      this.add("Value (Type)", tipo.getValor());
      this.add("English value (Type)", tipo.getValorIngles());
      this.add("Code (Subtype)", subtipo.getCodigo());
      this.add("Value (Subtype)", subtipo.getValor());
      this.add("English value (Subtype)", subtipo.getValorIngles());
    } else {
      this.add("Codigo (Tipo)", tipo.getCodigo());
      this.add("Valor (Tipo)", tipo.getValor());
      this.add("Codigo (Subtipo)", subtipo.getCodigo());
      this.add("Valor (Subtipo)", subtipo.getValor());
    }
  }


  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
