package com.haya.alaska.catalogo.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_MSTR_CATALOGO_LIST")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_CATALOGO_LIST")
public class CatalogoList implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @Column(name = "DES_CODIGO", unique = true)
  private String codigo;
  @Column(name = "DES_NOMBRE")
  private String nombre;
  @Column(name = "DES_NOMBRE_INGLES")
  private String nombreIngles;
  @Column(name = "DES_PAQUETE")
  private String paquete;


  @Column(name = "IND_MODIFICABLE", nullable = false, columnDefinition = "boolean default true")
  private Boolean modificable = true;
}
