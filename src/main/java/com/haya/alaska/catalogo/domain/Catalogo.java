package com.haya.alaska.catalogo.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

@MappedSuperclass
@Getter
@Setter
@Audited
public abstract class Catalogo implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "DES_CODIGO")
  private String codigo;
  @Column(name = "DES_VALOR")
  private String valor;
  @Column(name = "DES_VALOR_INGLES")
  private String valorIngles;
  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  public static <T extends Catalogo> HashMap<String, T> toHasMap(List<T> lista) {
    HashMap<String, T> result = new HashMap<>();
    for (var catalogo : lista) {
      Catalogo put = result.put(catalogo.getCodigo(), catalogo);
    }
    return result;
  }
}
