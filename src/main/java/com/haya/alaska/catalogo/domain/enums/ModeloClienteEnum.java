package com.haya.alaska.catalogo.domain.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum ModeloClienteEnum {
  MANUALES(Collections.singletonList("HA-04-LICM-23")),
  MODELOS(Arrays.asList("HA-04-CNCV-63","HA-04-LICM-24","HA-04-FACT-BU")),
  OTROS(Arrays.asList("HA-04-CNCV-63","HA-04-FACT-BU","HA-04-OTRO-21","HA-04-ACUE-58","HA-04-FACT-BV")),
  ;

  private final List<String> value;

  ModeloClienteEnum(List<String> entidad) {
    this.value = entidad;
  }

  public List<String> getValue(){
    return value;
  }
}
