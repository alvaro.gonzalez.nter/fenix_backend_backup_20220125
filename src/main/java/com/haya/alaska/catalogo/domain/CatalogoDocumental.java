package com.haya.alaska.catalogo.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CatalogoDocumental {
  private Integer codSerieDocumental;
  private String descSerieDocumental;
  private String codTdn1;
  private String descTdn1;
  private String codTdn2;
  private String descTdn2;
  @JsonProperty(value = "detalle_aplicacion")
  private String detalleAplicacion;
  private String matricula;
}
