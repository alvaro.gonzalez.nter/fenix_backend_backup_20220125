package com.haya.alaska.catalogo.domain;

import com.haya.alaska.campania.domain.Campania;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class CatalogoExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      //cabeceras.add("Id");
      cabeceras.add("Code");
      cabeceras.add("Value");
      cabeceras.add("English value");

    } else {
      //cabeceras.add("Id");
      cabeceras.add("Codigo");
      cabeceras.add("Valor");
      //cabeceras.add("Valor ingles");
    }
  }

  public CatalogoExcel(Catalogo catalogo) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      //this.add("Id", catalogo.getId());
      this.add("Code", catalogo.getCodigo());
      this.add("Value", catalogo.getValor());
      this.add("English value", catalogo.getValorIngles());

    } else {
      //this.add("Id", catalogo.getId());
      this.add("Codigo", catalogo.getCodigo());
      this.add("Valor", catalogo.getValor());
      //this.add("Valor ingles", catalogo.getValorIngles());
    }
  }

  public CatalogoExcel(Campania campania) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      //this.add("Id", catalogo.getId());
      this.add("Code", campania.getId());
      this.add("Value", campania.getNombre());

    } else {
      //this.add("Id", catalogo.getId());
      this.add("Codigo", campania.getId());
      this.add("Valor", campania.getNombre());
      //this.add("Valor ingles", catalogo.getValorIngles());
    }
  }


  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
