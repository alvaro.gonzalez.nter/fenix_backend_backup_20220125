package com.haya.alaska.catalogo.domain.enums;

public enum EntidadEnum {
  MANUALES("manuales"),
  MODELOS("modelos"),
  OTROS("otros"),
  ;

  private final String value;

  EntidadEnum(String entidad) {
    this.value = entidad;
  }

  public String getValue(){
    return value;
  }
}
