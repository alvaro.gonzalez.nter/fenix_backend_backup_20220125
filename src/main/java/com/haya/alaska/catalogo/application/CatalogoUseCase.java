package com.haya.alaska.catalogo.application;

import com.haya.alaska.catalogo.domain.enums.ModeloClienteEnum;
import com.haya.alaska.catalogo.infrastructure.controller.dto.*;
import com.haya.alaska.evento.infrastructure.controller.dto.SubtipoEventoOutputDTO;
import com.haya.alaska.shared.exceptions.InvalidCatalogException;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface CatalogoUseCase {

  List<String> getAllCatalogo();

  void updateCatalogo(String catalogo, List<CatalogoEnlaceInputDTO> catalogos) throws InvalidCatalogException, NotFoundException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, RequiredValueException;

  ListWithCountDTO<CatalogoEnlaceOutputDTO> getEnlazados(String nombre, Integer size, Integer page, Boolean activos) throws InvalidCodeException, InvalidCatalogException;

  List<SubtipoEventoOutputDTO> updateSubtipoEvento(List<SubtipoEventoInputDTO> input) throws NotFoundException;

  ListWithCountDTO<SubtipoEventoOutputDTO> getSubtipoEvento(Integer clase, Integer cartera, Boolean activo, Integer size, Integer page);

  List<TipoEventoOutPutDTO> updatetipoEvento(List<TipoEventoInputDTO> input) throws NotFoundException;

  List<SubtipoPropuestaOutputDTO> updateSubtipoPropuesta(List<SubtipoPropuestaInputDTO> input) throws NotFoundException;

  List<ModeloDTO> getAllModelos(String modelo);

  ModeloDTO getModeloByCodigo(String codigo, String modelo);

  List<CatalogoDocumentalDTO>getCatalogos(String entidad) throws InvalidCatalogException, IOException;

  List<CatalogoDocumentalDTO>getCatalogosPortalCliente(ModeloClienteEnum entidad) throws InvalidCatalogException, IOException;

}
