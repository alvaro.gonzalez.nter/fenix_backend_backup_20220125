package com.haya.alaska.catalogo.application;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.catalogo.domain.CatalogoList;
import com.haya.alaska.catalogo.domain.enums.ModeloClienteEnum;
import com.haya.alaska.catalogo.infrastructure.controller.dto.*;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoListRepository;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.clase_evento.domain.ClaseEvento_;
import com.haya.alaska.clase_evento.infrastructure.repository.ClaseEventoRepository;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estado_propuesta.infrastructure.repository.EstadoPropuestaRepository;
import com.haya.alaska.evento.infrastructure.controller.dto.SubtipoEventoOutputDTO;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.shared.exceptions.InvalidCatalogException;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.hito_procedimiento.infrastructure.repository.HitoProcedimientoRepository;
import com.haya.alaska.importancia_evento.domain.ImportanciaEvento;
import com.haya.alaska.importancia_evento.infrastructure.repository.ImportanciaEventoRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.modelo_email.infraestructure.repository.ModeloEmailRepository;
import com.haya.alaska.modelo_sms.infraestructure.repository.ModeloSMSRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.infrastructure.repository.PerfilRepository;
import com.haya.alaska.periodicidad_evento.domain.PeriodicidadEvento;
import com.haya.alaska.periodicidad_evento.infrastructure.repository.PeriodicidadEventoRepository;
import com.haya.alaska.permiso.domain.Permiso;
import com.haya.alaska.permiso.infrastructure.repository.PermisoRepository;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.resultado_evento.domain.ResultadoEvento;
import com.haya.alaska.resultado_evento.infrastructure.repository.ResultadoEventoRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien;
import com.haya.alaska.sub_tipo_bien.infrastructure.repository.SubTipoBienRepository;
import com.haya.alaska.subestado_formalizacion.domain.SubestadoFormalizacion;
import com.haya.alaska.subestado_formalizacion.infrastructure.repository.SubestadoFormalizacionRepository;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.subtipo_estado.infrastructure.repository.SubtipoEstadoRepository;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento_;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import com.haya.alaska.subtipo_propuesta.infrastructure.repository.SubtipoPropuestaRepository;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_estado.infrastructure.repository.TipoEstadoRepository;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.tipo_evento.domain.TipoEvento_;
import com.haya.alaska.tipo_evento.infrastructure.repository.TipoEventoRepository;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.tipo_propuesta.infrastructure.repository.TipoPropuestaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CatalogoUseCaseImpl implements CatalogoUseCase {
  @Autowired
  Map<String, CatalogoRepository<? extends Catalogo, Integer>> catalogoRepository;
  @PersistenceContext
  private EntityManager entityManager;
  @Autowired
  ModelMapper modelMapper;
  @Autowired
  EventoUtil eventoUtil;

  @Autowired
  CatalogoListRepository catalogoListRepository;
  @Autowired
  HitoProcedimientoRepository hitoProcedimientoRepository;
  @Autowired
  TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired
  LocalidadRepository localidadRepository;
  @Autowired
  MunicipioRepository municipioRepository;
  @Autowired
  ProvinciaRepository provinciaRepository;
  @Autowired
  SubtipoEstadoRepository subtipoEstadoRepository;
  @Autowired
  TipoEstadoRepository tipoEstadoRepository;
  @Autowired
  SubtipoPropuestaRepository subtipoPropuestaRepository;
  @Autowired
  SubestadoFormalizacionRepository subestadoFormalizacionRepository;
  @Autowired
  EstadoPropuestaRepository estadoPropuestaRepository;
  @Autowired
  TipoPropuestaRepository tipoPropuestaRepository;
  @Autowired
  PermisoRepository permisoRepository;
  @Autowired
  ClaseEventoRepository claseEventoRepository;
  @Autowired
  TipoEventoRepository tipoEventoRepository;
  @Autowired
  SubtipoEventoRepository subtipoEventoRepository;
  @Autowired
  ResultadoEventoRepository resultadoEventoRepository;
  @Autowired
  PerfilRepository perfilRepository;
  @Autowired
  ImportanciaEventoRepository importanciaEventoRepository;
  @Autowired
  PeriodicidadEventoRepository periodicidadEventoRepository;
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  ModeloEmailRepository modeloEmailRepository;
  @Autowired
  ModeloSMSRepository modeloSMSRepository;
  @Autowired
  ServicioGestorDocumental servicioGestorDocumental;
  @Autowired
  SubTipoBienRepository subtipoBienRepository;
  @Autowired
  TipoBienRepository tipoBienRepository;


  @Override
  public List<String> getAllCatalogo() {
    List<CatalogoList> catalogos = catalogoListRepository.findByModificableIsTrue();
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();
    List<String> result = new ArrayList<>();
    if (defaultLocal == null || defaultLocal.equals("es")) result =
      catalogos.stream().map(CatalogoList::getNombre).collect(Collectors.toList());
    else if (defaultLocal.equals("en")) result =
      catalogos.stream().map(CatalogoList::getNombreIngles).collect(Collectors.toList());
    return result;
  }

  @Override
  public void updateCatalogo(String catalogo, List<CatalogoEnlaceInputDTO> catalogos)
    throws InvalidCatalogException, NotFoundException, ClassNotFoundException,
    NoSuchMethodException, IllegalAccessException, InvocationTargetException,
    InstantiationException, RequiredValueException {
    if (catalogo.equals("Plazo") || catalogo.equals("plazo")) {
      for (CatalogoEnlaceInputDTO c : catalogos) {
        if (c.getValor() == null || c.getValor().equals("")) continue;
        try {
          Integer.parseInt(c.getValor());
        } catch (Exception e) {
          Locale loc = LocaleContextHolder.getLocale();
          if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
            throw new InvalidCatalogException("The catalog value has to be an integer.");
          else throw new InvalidCatalogException("El valor del catálogo tiene que ser un número entero.");
        }
      }
    }
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();

    CatalogoList catalogoList = catalogoListRepository.findByNombre(catalogo);

    if (defaultLocal == null || defaultLocal.equals("es"))
      catalogoList = catalogoListRepository.findByNombre(catalogo);
    else if (defaultLocal.equals("en"))
      catalogoList = catalogoListRepository.findByNombreIngles(catalogo);

    CatalogoRepository<? extends Catalogo, Integer> repo =
      catalogoRepository.get(catalogoList.getCodigo() + "Repository");
    if (repo == null){
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCatalogException("Catalog not found: " + catalogo);
      else throw new InvalidCatalogException("Catálogo no encontrado: " + catalogo);
    }

    for (CatalogoEnlaceInputDTO in : catalogos) {
      switch (catalogoList.getCodigo()) {
        case "hitoProcedimiento": {
          HitoProcedimiento cata;
          if (in.getId() == null) {
            cata = new HitoProcedimiento();
            var codigo = hitoProcedimientoRepository.findByCodigoAndRecoveryIsFalse(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException();
          } else {
            cata = hitoProcedimientoRepository.findById(in.getId()).orElseThrow(
              () ->
                new NotFoundException(
                  "hitoProcedimiento", in.getId()));
            if(cata.getCodigo() != in.getCodigo()) {
              var codigo = hitoProcedimientoRepository.findByCodigoAndRecoveryIsFalse(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != cata.getId())
                throw new InvalidCatalogException();
            }
          }
          if (in.getEnlazado() != null) {
            cata.setTipoProcedimiento(
              tipoProcedimientoRepository.findById(in.getEnlazado()).orElse(null));
          }
          BeanUtils.copyProperties(in, cata, "valor");
          if (defaultLocal == null || defaultLocal.equals("es")) cata.setValor(in.getValor());
          else if (defaultLocal.equals("en")) cata.setValorIngles(in.getValor());
          repo.save(cata);
          break;
        }
        case "localidad": {
          Localidad cata;
          if (in.getId() == null) {
            cata = new Localidad();
            var codigo=localidadRepository.findByCodigo(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException();
          } else {
            cata = localidadRepository.findById(in.getId()).orElseThrow(
              () ->
                new NotFoundException(
                  "localidad", in.getId()));
            if(cata.getCodigo() != in.getCodigo()) {
              var codigo=localidadRepository.findByCodigo(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != cata.getId())
                throw new InvalidCatalogException();
            }
          }
          if (in.getEnlazado() != null) {
            cata.setMunicipio(municipioRepository.findById(in.getEnlazado()).orElse(null));
          }
          BeanUtils.copyProperties(in, cata, "valor");
          if (defaultLocal == null || defaultLocal.equals("es")) cata.setValor(in.getValor());
          else if (defaultLocal.equals("en")) cata.setValorIngles(in.getValor());
          repo.save(cata);
          break;
        }
        case "municipio": {
          Municipio cata;
          if (in.getId() == null) {
            cata = new Municipio();
            var codigo=municipioRepository.findByCodigo(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException();
          } else {
            cata = municipioRepository.findById(in.getId()).orElseThrow(
              () ->
                new NotFoundException(
                  "municipio", in.getId()));
            if(cata.getCodigo() != in.getCodigo()) {
              var codigo=municipioRepository.findByCodigo(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != cata.getId())
                throw new InvalidCatalogException();
            }
          }
          if (in.getEnlazado() != null) {
            cata.setProvincia(provinciaRepository.findById(in.getEnlazado()).orElse(null));
          }
          BeanUtils.copyProperties(in, cata, "valor");
          if (defaultLocal == null || defaultLocal.equals("es")) cata.setValor(in.getValor());
          else if (defaultLocal.equals("en")) cata.setValorIngles(in.getValor());
          repo.save(cata);
          break;
        }
        case "subtipoEstado": {
          SubtipoEstado cata;

          if (in.getId() == null) {
            cata = new SubtipoEstado();
            var codigo=subtipoEstadoRepository.findByCodigo(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException();
          } else {
            cata = subtipoEstadoRepository.findById(in.getId()).orElseThrow(
              () ->
                new NotFoundException(
                  "subtipoestado", in.getId()));
            if(cata.getCodigo() != in.getCodigo()) {
              var codigo=subtipoEstadoRepository.findByCodigo(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != cata.getId())
                throw new InvalidCatalogException();
            }
          }
          if (in.getEnlazado() != null) {
            cata.setEstado(tipoEstadoRepository.findById(in.getEnlazado()).orElse(null));
          }
          BeanUtils.copyProperties(in, cata, "valor");
          if (defaultLocal == null || defaultLocal.equals("es")) cata.setValor(in.getValor());
          else if (defaultLocal.equals("en")) cata.setValorIngles(in.getValor());
          repo.save(cata);
          break;
        }
        case "subestadoFormalizacion": {
          SubestadoFormalizacion cata;

          if (in.getId() == null) {
            cata = new SubestadoFormalizacion();
            var codigo = subestadoFormalizacionRepository.findByCodigo(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException();
          } else {
            cata = subestadoFormalizacionRepository.findById(in.getId()).orElseThrow(
              () ->
                new NotFoundException(
                  "SubestadoFormalizacion", in.getId()));
            if(cata.getCodigo() != in.getCodigo()) {
              var codigo=subestadoFormalizacionRepository.findByCodigo(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != cata.getId())
                throw new InvalidCatalogException();
            }
          }
          if (in.getEnlazado() != null) {
            cata.setEstadoPropuesta(estadoPropuestaRepository.findById(in.getEnlazado()).orElse(null));
          }
          BeanUtils.copyProperties(in, cata, "valor");
          if (defaultLocal == null || defaultLocal.equals("es")) cata.setValor(in.getValor());
          else if (defaultLocal.equals("en")) cata.setValorIngles(in.getValor());
          repo.save(cata);
          break;
        }
        case "subtipoPropuesta": {
          SubtipoPropuesta cata;
          if (in.getId() == null) {
            cata = new SubtipoPropuesta();
            var codigo=subtipoPropuestaRepository.findByCodigo(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException();
          } else {
            cata = subtipoPropuestaRepository.findById(in.getId()).orElseThrow(
              () ->
                new NotFoundException(
                  "subtipopropuesta", in.getId()));
            if(cata.getCodigo() != in.getCodigo()) {
              var codigo=subtipoPropuestaRepository.findByCodigo(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != cata.getId())
                throw new InvalidCatalogException();
            }
          }
          if (in.getEnlazado() != null) {
            cata.setTipoPropuesta(
              tipoPropuestaRepository.findById(in.getEnlazado()).orElse(null));
          }
          BeanUtils.copyProperties(in, cata, "valor");
          if (defaultLocal == null || defaultLocal.equals("es")) cata.setValor(in.getValor());
          else if (defaultLocal.equals("en")) cata.setValorIngles(in.getValor());
          repo.save(cata);
          break;
        }
        case "subtipoBien":
        case "subTipoBien": {
          SubTipoBien cata;
          if (in.getId() == null) {
            cata = new SubTipoBien();
            var codigo=subtipoPropuestaRepository.findByCodigo(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException("Ya existe un valor del catálgo con el mismo código");
          } else {
            cata = subtipoBienRepository.findById(in.getId()).orElseThrow(
              () ->
                new NotFoundException(
                  "No se ha encontrado un subtipopropuesta de id: " + in.getId()));
            if(cata.getCodigo() != in.getCodigo()) {
              var codigo=subtipoBienRepository.findByCodigo(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != cata.getId())
                throw new InvalidCatalogException("Ya existe un valor del catálgo con el mismo código");
            }
          }
          if (in.getEnlazado() != null) {
            cata.setTipoBien(
              tipoBienRepository.findById(in.getEnlazado()).orElse(null));
          }
          BeanUtils.copyProperties(in, cata, "valor");
          if (defaultLocal == null || defaultLocal.equals("es")) cata.setValor(in.getValor());
          else if (defaultLocal.equals("en")) cata.setValorIngles(in.getValor());
          repo.save(cata);
          break;
        }
        case "permiso": {
          Permiso cata;
          if (in.getId() == null) {
            cata = new Permiso();
            var codigo=permisoRepository.findByCodigo(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException();
          } else {
            cata = permisoRepository.findById(in.getId()).orElseThrow(
              () ->
                new NotFoundException(
                  "subtipopropuesta", in.getId()));
            if(cata.getCodigo() != in.getCodigo()) {
              var codigo=permisoRepository.findByCodigo(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != cata.getId())
                throw new InvalidCatalogException();
            }
          }
          if (in.getEnlazado() != null) {
            cata.setPermisoPadre(permisoRepository.findById(in.getEnlazado()).orElse(null));
          }
          BeanUtils.copyProperties(in, cata, "valor");
          if (defaultLocal == null || defaultLocal.equals("es")) cata.setValor(in.getValor());
          else if (defaultLocal.equals("en")) cata.setValorIngles(in.getValor());
          repo.save(cata);
          break;
        }
        case "tipoEvento": {
          TipoEvento cata;
          if (in.getId() == null) {
            cata = new TipoEvento();
            var codigo=tipoEventoRepository.findByCodigo(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException();
          } else {
            cata = tipoEventoRepository.findById(in.getId()).orElseThrow(
              () ->
                new NotFoundException(
                  "tipoevento", in.getId()));
            if(cata.getCodigo() != in.getCodigo()) {
              var codigo=tipoEventoRepository.findByCodigo(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != cata.getId())
                throw new InvalidCatalogException();
            }
          }
          BeanUtils.copyProperties(in, cata, "valor", "id");
          if (defaultLocal == null || defaultLocal.equals("es")) cata.setValor(in.getValor());
          else if (defaultLocal.equals("en")) cata.setValorIngles(in.getValor());
          if (in.getEnlazado() != null) {
            cata.setClaseEvento(claseEventoRepository.findById(in.getEnlazado()).orElse(null));
          }
          repo.save(cata);
          break;
        }
        case "subtipoEvento": {
          SubtipoEvento cata;
          if (in.getId() == null) {
            cata = new SubtipoEvento();
            var codigo=subtipoEventoRepository.findByCodigo(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException();
          } else {
            cata = subtipoEventoRepository.findById(in.getId()).orElseThrow(
              () ->
                new NotFoundException(
                  "subtipoEvento", in.getId()));
            if(cata.getCodigo() != in.getCodigo()) {
              var codigo=subtipoEventoRepository.findByCodigo(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != cata.getId())
                throw new InvalidCatalogException();
            }
          }
          BeanUtils.copyProperties(in, cata, "valor", "id");
          if (defaultLocal == null || defaultLocal.equals("es")) cata.setValor(in.getValor());
          else if (defaultLocal.equals("en")) cata.setValorIngles(in.getValor());
          if (in.getEnlazado() != null) {
            cata.setTipoEvento(tipoEventoRepository.findById(in.getEnlazado()).orElseThrow(
              () ->
                new NotFoundException(
                  "tipoEvento", in.getEnlazado())));
          } else {
            if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
              throw new RequiredValueException("The field 'enlazado' is mandatory for the SubtypeEvent");
            else throw new RequiredValueException("El campo 'enlazado' es obligatorio para los SubtipoEvento");
          }
          repo.save(cata);
          break;
        }
        default:
          Catalogo saved;
          if (in.getId() != null) {
            saved =
              repo.findById(in.getId())
                .orElseThrow(
                  () ->
                    new NotFoundException(
                      catalogo, in.getId()));
            if(saved.getCodigo() != in.getCodigo()) {
              var codigo=repo.findByCodigo(in.getCodigo()).orElse(null);
              if(codigo != null && codigo.getId() != saved.getId())
                throw new InvalidCatalogException();
            }
          } else {
            var codigo = repo.findByCodigo(in.getCodigo()).orElse(null);
            if (codigo != null)
              throw new InvalidCatalogException();
            String pack = catalogoList.getPaquete();
            Class<?> clazz = Class.forName(pack);
            Constructor<?> ctor = clazz.getConstructor();
            saved = (Catalogo) ctor.newInstance(new Object[]{});
          }
          BeanUtils.copyProperties(in, saved, "valor");
          if (defaultLocal == null || defaultLocal.equals("es")) saved.setValor(in.getValor());
          else if (defaultLocal.equals("en")) saved.setValorIngles(in.getValor());
          repo.save(saved);
          break;
      }

      switch (catalogoList.getCodigo()) {
        case "tipoProcedimiento": {
          if (!in.getActivo()) {
            List<HitoProcedimiento> hijos =
              hitoProcedimientoRepository.findAllByTipoProcedimientoIdAndActivoIsTrueAndRecoveryIsFalse(in.getId());
            for (HitoProcedimiento hijo : hijos) {
              hijo.setActivo(false);
              hitoProcedimientoRepository.save(hijo);
            }
          }
          break;
        }
        case "municipio": {
          if (!in.getActivo()) {
            List<Localidad> hijos = localidadRepository.findByMunicipioIdAndActivoIsTrueOrderByValor(in.getId());
            for (Localidad hijo : hijos) {
              hijo.setActivo(false);
              localidadRepository.save(hijo);
            }
          }
          break;
        }
        case "provincia": {
          if (!in.getActivo()) {
            List<Municipio> hijos = municipioRepository.findByProvinciaIdAndActivoIsTrueOrderByValor(in.getId());
            for (Municipio hijo : hijos) {
              hijo.setActivo(false);
              municipioRepository.save(hijo);
            }
          }
          break;
        }
        case "tipoEstado": {
          if (!in.getActivo()) {
            List<SubtipoEstado> hijos = subtipoEstadoRepository.findAllByEstadoIdAndActivoIsTrue(in.getId());
            for (SubtipoEstado hijo : hijos) {
              hijo.setActivo(false);
              subtipoEstadoRepository.save(hijo);
            }
          }
          break;
        }
        case "estadoPropuesta": {
          if (!in.getActivo()) {
            List<SubestadoFormalizacion> hijos = subestadoFormalizacionRepository.findAllByEstadoPropuestaIdAndActivoIsTrue(in.getId());
            for (SubestadoFormalizacion hijo : hijos) {
              hijo.setActivo(false);
              subestadoFormalizacionRepository.save(hijo);
            }
          }
          break;
        }
        case "tipoPropuesta": {
          if (!in.getActivo()) {
            List<SubtipoPropuesta> hijos =
              subtipoPropuestaRepository.findByTipoPropuestaIdAndActivoIsTrue(in.getId());
            for (SubtipoPropuesta hijo : hijos) {
              hijo.setActivo(false);
              subtipoPropuestaRepository.save(hijo);
            }
          }
          break;
        }
        case "permiso": {
          if (!in.getActivo()) {
            List<Permiso> hijos = permisoRepository.findAllByPermisoPadreId(in.getId());
            for (Permiso hijo : hijos) {
              hijo.setActivo(false);
              permisoRepository.save(hijo);
            }
          }
          break;
        }
        case "tipoEvento": {
          if (!in.getActivo()) {
            List<SubtipoEvento> hijos = subtipoEventoRepository.findByTipoEventoIdAndActivoIsTrue(in.getId());
            for (SubtipoEvento hijo : hijos) {
              hijo.setActivo(false);
              subtipoEventoRepository.save(hijo);
            }
          }
          break;
        }
        default:
          break;
      }
    }
  }

  @Override
  public ListWithCountDTO<CatalogoEnlaceOutputDTO> getEnlazados(
    String nombre, Integer size, Integer page, Boolean activos) throws InvalidCatalogException {
    List<CatalogoEnlaceOutputDTO> result;

    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();

    CatalogoList catalogoList = catalogoListRepository.findByNombre(nombre);

    if (defaultLocal == null || defaultLocal.equals("es"))
      catalogoList = catalogoListRepository.findByNombre(nombre);
    else if (defaultLocal.equals("en"))
      catalogoList = catalogoListRepository.findByNombreIngles(nombre);

    CatalogoRepository<? extends Catalogo, Integer> repo =
      catalogoRepository.get(catalogoList.getCodigo() + "Repository");

    Integer numero;

    switch (catalogoList.getCodigo()) {
      case "hitoProcedimiento": {
        List<HitoProcedimiento> catalogos = hitoProcedimientoRepository.findAll();
        if (activos) {
          catalogos = hitoProcedimientoRepository.findAllByActivoIsTrueAndRecoveryIsFalse();
        }
        numero = catalogos.size();
        List<HitoProcedimiento> paginado =
          catalogos.stream().skip(size * page).limit(size).collect(Collectors.toList());
        result =
          paginado.stream()
            .map(
              cat ->
                new CatalogoEnlaceOutputDTO(
                  cat, cat.getTipoProcedimiento(), "Tipo Procedimiento"))
            .collect(Collectors.toList());
        break;
      }
      case "localidad": {
        List<Localidad> catalogos = localidadRepository.findAll();
        if (activos) {
          catalogos = localidadRepository.findAllByActivoIsTrue();
        }
        numero = catalogos.size();
        List<Localidad> paginado =
          catalogos.stream().skip(size * page).limit(size).collect(Collectors.toList());
        result =
          paginado.stream()
            .map(cat -> new CatalogoEnlaceOutputDTO(cat, cat.getMunicipio(), "Municipio"))
            .collect(Collectors.toList());
        break;
      }
      case "municipio": {
        List<Municipio> catalogos = municipioRepository.findAll();
        if (activos) {
          catalogos = municipioRepository.findAllByActivoIsTrue();
        }
        numero = catalogos.size();
        List<Municipio> paginado =
          catalogos.stream().skip(size * page).limit(size).collect(Collectors.toList());
        result =
          paginado.stream()
            .map(cat -> new CatalogoEnlaceOutputDTO(cat, cat.getProvincia(), "Provincia"))
            .collect(Collectors.toList());
        break;
      }
      case "subtipoEstado": {
        List<SubtipoEstado> catalogos = subtipoEstadoRepository.findAll();
        if (activos) {
          catalogos = subtipoEstadoRepository.findAllByActivoIsTrue();
        }
        numero = catalogos.size();
        List<SubtipoEstado> paginado =
          catalogos.stream().skip(size * page).limit(size).collect(Collectors.toList());
        result =
          paginado.stream()
            .map(cat -> new CatalogoEnlaceOutputDTO(cat, cat.getEstado(), "Tipo Estado"))
            .collect(Collectors.toList());
        break;
      }
      case "subestadoFormalizacion": {
        List<SubestadoFormalizacion> catalogos = subestadoFormalizacionRepository.findAll();
        if (activos) {
          catalogos = subestadoFormalizacionRepository.findAllByActivoIsTrue();
        }
        numero = catalogos.size();
        List<SubestadoFormalizacion> paginado =
          catalogos.stream().skip(size * page).limit(size).collect(Collectors.toList());
        result =
          paginado.stream()
            .map(cat -> new CatalogoEnlaceOutputDTO(cat, cat.getEstadoPropuesta(), "Estado Propuesta"))
            .collect(Collectors.toList());
        break;
      }
      case "subtipoPropuesta": {
        List<SubtipoPropuesta> catalogos = subtipoPropuestaRepository.findAll();
        if (activos) {
          catalogos = subtipoPropuestaRepository.findAllByActivoIsTrue();
        }
        numero = catalogos.size();
        List<SubtipoPropuesta> paginado =
          catalogos.stream().skip(size * page).limit(size).collect(Collectors.toList());
        result =
          paginado.stream()
            .map(
              cat ->
                new CatalogoEnlaceOutputDTO(
                  cat, cat.getTipoPropuesta(), "Tipo Propuesta"))
            .collect(Collectors.toList());
        break;
      }
      case "permiso": {
        List<Permiso> catalogos = permisoRepository.findAll();
        if (activos) {
          catalogos = permisoRepository.findAllByActivoIsTrue();
        }
        numero = catalogos.size();
        List<Permiso> paginado =
          catalogos.stream().skip(size * page).limit(size).collect(Collectors.toList());
        result =
          paginado.stream()
            .map(cat -> new CatalogoEnlaceOutputDTO(cat, cat.getPermisoPadre(), "Permiso"))
            .collect(Collectors.toList());
        break;
      }
      case "tipoEvento": {
        List<TipoEvento> catalogos = tipoEventoRepository.findAll();
        if (activos) {
          catalogos = tipoEventoRepository.findAllByActivoIsTrue();
        }
        numero = catalogos.size();
        List<TipoEvento> paginado =
          catalogos.stream().skip(size * page).limit(size).collect(Collectors.toList());
        result =
          paginado.stream()
            .map(
              cat -> new CatalogoEnlaceOutputDTO(cat, cat.getClaseEvento(), "Clase Evento"))
            .collect(Collectors.toList());
        break;
      }

      case "subtipoEvento": {
        List<SubtipoEvento> catalogos = subtipoEventoRepository.findAll();
        if (activos) {
          catalogos = subtipoEventoRepository.findAllByActivoIsTrue();
        }
        numero = catalogos.size();
        List<SubtipoEvento> paginado =
          catalogos.stream().skip(size * page).limit(size).collect(Collectors.toList());
        result =
          paginado.stream()
            .map(cat -> new CatalogoEnlaceOutputDTO(cat, cat.getTipoEvento(), "Tipo Evento"))
            .collect(Collectors.toList());
        break;
      }

      case "subtipoBien":
      case "subTipoBien": {
        List<SubTipoBien> catalogos = subtipoBienRepository.findAll();
        if (activos) {
          catalogos = subtipoBienRepository.findAllByActivoIsTrue();
        }
        numero = catalogos.size();
        List<SubTipoBien> paginado =
          catalogos.stream().skip(size * page).limit(size).collect(Collectors.toList());
        result =
          paginado.stream()
            .map(cat -> new CatalogoEnlaceOutputDTO(cat, cat.getTipoBien(), "Tipo Bien"))
            .collect(Collectors.toList());
        break;
      }

      default:
        if (repo != null) {
          List<Catalogo> catalogos = (List<Catalogo>) repo.findAll();
          if (activos) {
            catalogos = (List<Catalogo>) repo.findAllByActivoIsTrue();
          }
          numero = catalogos.size();
          result =
            catalogos.stream()
              .skip(size * page)
              .limit(size)
              .map(cat -> new CatalogoEnlaceOutputDTO(cat, null, null))
              .collect(Collectors.toList());
        } else {
          if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
            throw new InvalidCatalogException("Catalog not found: " + nombre);
          else throw new InvalidCatalogException("Catálogo no encontrado: " + nombre);
        }
        break;
    }

    return new ListWithCountDTO<CatalogoEnlaceOutputDTO>(result, numero);
  }

  @Override
  public ListWithCountDTO<SubtipoEventoOutputDTO> getSubtipoEvento(Integer clase, Integer cartera, Boolean activo, Integer size, Integer page) {
    List<SubtipoEvento> subtipos = filteredSubtipos(clase, cartera, activo);
    List<SubtipoEventoOutputDTO> result =
      subtipos.stream()
        .skip(size * page)
        .limit(size)
        .map(SubtipoEventoOutputDTO::new)
        .collect(Collectors.toList());
    return new ListWithCountDTO<SubtipoEventoOutputDTO>(result, subtipos.size());
  }

  @Override
  public List<SubtipoEventoOutputDTO> updateSubtipoEvento(List<SubtipoEventoInputDTO> input)
    throws NotFoundException {
    List<SubtipoEvento> subtipos = new ArrayList<>();
    for (SubtipoEventoInputDTO in : input) {
      SubtipoEvento se;
      if (in.getId() == null) {
        se = new SubtipoEvento();
      } else {
        se =
          subtipoEventoRepository
            .findById(in.getId())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "subtipoEvento", in.getId()));
      }
      Locale loc = LocaleContextHolder.getLocale();
      String defaultLocal = loc.getLanguage();

      BeanUtils.copyProperties(in, se, "valor");
      if (defaultLocal == null || defaultLocal.equals("es")) se.setValor(in.getValor());
      else if (defaultLocal.equals("en")) se.setValorIngles(in.getValor());
      if (in.getEmisor() != null) {
        Perfil emisor =
          perfilRepository
            .findById(in.getEmisor())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "perfil", in.getEmisor()));
        se.setLimiteEmisor(emisor);
      } else {
        se.setLimiteEmisor(null);
      }
      if (in.getDestinatario() != null) {
        Perfil destinatario =
          perfilRepository
            .findById(in.getDestinatario())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "perfil", in.getDestinatario()));
        if (se.getLimiteDestinatario() != null && se.getLimiteDestinatario() != destinatario)
          eventoUtil.reasignarEventos(se, destinatario);
        se.setLimiteDestinatario(destinatario);
      } else {
        se.setLimiteDestinatario(null);
      }
      if (in.getTipoEvento() != null) {
        TipoEvento tipo =
          tipoEventoRepository
            .findById(in.getTipoEvento())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "tipoEvento", in.getTipoEvento()));
        se.setTipoEvento(tipo);
      }
      if (in.getImportancia() != null) {
        ImportanciaEvento importancia =
          importanciaEventoRepository
            .findById(in.getImportancia())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "importancia", in.getImportancia()));
        se.setImportancia(importancia);
      } else {
        se.setImportancia(null);
      }
      if (in.getPeriodicidad() != null) {
        PeriodicidadEvento periodicidad =
          periodicidadEventoRepository
            .findById(in.getPeriodicidad())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "importancia", in.getPeriodicidad()));
        se.setPeriodicidad(periodicidad);
      } else {
        se.setPeriodicidad(null);
      }
      if (in.getIdCartera() != null) {
        Cartera cartera =
          carteraRepository
            .findById(in.getIdCartera())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "cartera", in.getIdCartera()));
        se.setCartera(cartera);
      } else {
        se.setCartera(null);
      }
      SubtipoEvento seSaved = subtipoEventoRepository.saveAndFlush(se);

      Set<ResultadoEvento> resultados = new HashSet<>();
      for (Integer reId : in.getResultados()) {
        ResultadoEvento re =
          resultadoEventoRepository
            .findById(reId)
            .orElseThrow(
              () ->
                new NotFoundException(
                  "ResultadoEvento", reId));
        resultados.add(re);
      }
      if (resultados.isEmpty()){
        ResultadoEvento re1 = resultadoEventoRepository.findById(1).orElseThrow(() ->
          new NotFoundException("ResultadoEvento", 1));
        ResultadoEvento re2 = resultadoEventoRepository.findById(2).orElseThrow(() ->
          new NotFoundException("ResultadoEvento", 2));
        resultados.add(re1);
        resultados.add(re2);
      }
      seSaved.setResultados(resultados);
      subtipos.add(subtipoEventoRepository.saveAndFlush(seSaved));
    }
    List<SubtipoEventoOutputDTO> result =
      subtipos.stream().map(SubtipoEventoOutputDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public List<TipoEventoOutPutDTO> updatetipoEvento(List<TipoEventoInputDTO> input)
    throws NotFoundException {
    List<TipoEvento> tipoEvento = new ArrayList<>();
    for (TipoEventoInputDTO in : input) {
      TipoEvento se;
      if (in.getId() == null) {
        se = new TipoEvento();
      } else {
        se =
          tipoEventoRepository
            .findById(in.getId())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "tipoEvento", in.getId()));
      }
      Locale loc = LocaleContextHolder.getLocale();
      String defaultLocal = loc.getLanguage();

      BeanUtils.copyProperties(in, se, "valor");
      if (defaultLocal == null || defaultLocal.equals("es")) se.setValor(in.getValor());
      else if (defaultLocal.equals("en")) se.setValorIngles(in.getValor());

      if (in.getClaseEvento() != null) {
        ClaseEvento clase =
          claseEventoRepository
            .findById(in.getClaseEvento())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "claseEvento", in.getClaseEvento()));
        se.setClaseEvento(clase);
      } else {
        se.setClaseEvento(null);
      }
      TipoEvento seSaved = tipoEventoRepository.saveAndFlush(se);
      tipoEvento.add(tipoEventoRepository.saveAndFlush(seSaved));
    }
    List<TipoEventoOutPutDTO> result =
      tipoEvento.stream().map(TipoEventoOutPutDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public List<SubtipoPropuestaOutputDTO> updateSubtipoPropuesta(
    List<SubtipoPropuestaInputDTO> input) throws NotFoundException {
    List<SubtipoPropuesta> subtipos = new ArrayList<>();
    for (SubtipoPropuestaInputDTO in : input) {
      SubtipoPropuesta se;
      if (in.getId() == null) {
        se = new SubtipoPropuesta();
      } else {
        se =
          subtipoPropuestaRepository
            .findById(in.getId())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "subtipopropuesta", in.getId()));
      }
      Locale loc = LocaleContextHolder.getLocale();
      String defaultLocal = loc.getLanguage();

      BeanUtils.copyProperties(in, se, "valor");
      if (defaultLocal == null || defaultLocal.equals("es")) se.setValor(in.getValor());
      else if (defaultLocal.equals("en")) se.setValorIngles(in.getValor());

      if (in.getTipoPropuesta() != null) {
        TipoPropuesta tipo =
          tipoPropuestaRepository
            .findById(in.getTipoPropuesta())
            .orElseThrow(
              () ->
                new NotFoundException(
                  "tipoPropuesta", in.getTipoPropuesta()));
        se.setTipoPropuesta(tipo);
      } else {
        se.setTipoPropuesta(null);
      }
      SubtipoPropuesta seSaved = subtipoPropuestaRepository.saveAndFlush(se);
      subtipos.add(subtipoPropuestaRepository.saveAndFlush(seSaved));
    }
    List<SubtipoPropuestaOutputDTO> result =
      subtipos.stream().map(SubtipoPropuestaOutputDTO::new).collect(Collectors.toList());
    return result;
  }


  @Override
  public List<ModeloDTO> getAllModelos(String modelo) {

    List<ModeloDTO> listadoDatosTipoModelo = new ArrayList<>();

    switch (modelo) {
      case "email": {
        listadoDatosTipoModelo = modeloEmailRepository.findAll().stream().map(model -> modelMapper.map(modelo, ModeloDTO.class)).collect(Collectors.toList());
        break;
      }
      case "sms": {
        listadoDatosTipoModelo = modeloSMSRepository.findAll().stream().map(model -> modelMapper.map(modelo, ModeloDTO.class)).collect(Collectors.toList());
        break;
      }
      case "carta": {
        listadoDatosTipoModelo = modeloEmailRepository.findAll().stream().map(model -> modelMapper.map(modelo, ModeloDTO.class)).collect(Collectors.toList());
        break;
      }
      case "burofax": {
        listadoDatosTipoModelo = modeloSMSRepository.findAll().stream().map(model -> modelMapper.map(modelo, ModeloDTO.class)).collect(Collectors.toList());
        break;
      }
      case "whatsappweb": {
        listadoDatosTipoModelo = modeloSMSRepository.findAll().stream().map(model -> modelMapper.map(modelo, ModeloDTO.class)).collect(Collectors.toList());
        break;
      }
    }

    return listadoDatosTipoModelo;
  }

  @Override
  public ModeloDTO getModeloByCodigo(String codigo, String modelo) {

    ModeloDTO modeloDTO = null;

    switch (modelo) {
      case "email": {
        modeloDTO = modelMapper.map(modeloEmailRepository.findByCodigo(codigo), ModeloDTO.class);
        break;
      }
      case "sms": {
        modeloDTO = modelMapper.map(modeloSMSRepository.findByCodigo(codigo), ModeloDTO.class);
        break;
      }
      case "carta": {
        modeloDTO = modelMapper.map(modeloEmailRepository.findByCodigo(codigo), ModeloDTO.class);
        break;
      }
      case "burofax": {
        modeloDTO = modelMapper.map(modeloSMSRepository.findByCodigo(codigo), ModeloDTO.class);
        break;
      }
      case "whatsappweb": {
        modeloDTO = modelMapper.map(modeloSMSRepository.findByCodigo(codigo), ModeloDTO.class);
        break;
      }
    }

    return modeloDTO;
  }

  private List<SubtipoEvento> filteredSubtipos(Integer clase, Integer cartera, Boolean activo) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<SubtipoEvento> query = cb.createQuery(SubtipoEvento.class);

    Root<SubtipoEvento> root = query.from(SubtipoEvento.class);
    List<Predicate> predicates = new ArrayList<>();

    if (clase != null) {
      Join<SubtipoEvento, TipoEvento> tipoJoin =
        root.join(SubtipoEvento_.tipoEvento, JoinType.INNER);
      Join<TipoEvento, ClaseEvento> claseJoin =
        tipoJoin.join(TipoEvento_.claseEvento, JoinType.INNER);
      Predicate onClase = cb.equal(claseJoin.get(ClaseEvento_.id), clase);
      predicates.add(onClase);
    }

    if (activo != null && activo){
      predicates.add(cb.isTrue(root.get(SubtipoEvento_.ACTIVO)));
    }

    if (cartera != null) {
      Join<SubtipoEvento, Cartera> carteraJoin = root.join(SubtipoEvento_.cartera, JoinType.INNER);
      Predicate onCartera = cb.equal(carteraJoin.get(Cartera_.id), cartera);
      predicates.add(onCartera);
    } else {
      Predicate onCartera = cb.isNull(root.get(SubtipoEvento_.cartera));
      predicates.add(onCartera);
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    Set<ParameterExpression<?>> q = query.getParameters();

    List<SubtipoEvento> listaPropuesta = entityManager.createQuery(query).getResultList();
    return listaPropuesta;
  }

  @Override
  public List<CatalogoDocumentalDTO>getCatalogos(String entidad) throws InvalidCatalogException, IOException {
    CodigoEntidad codigoEntidad = null;
    switch (entidad) {
      case "contratos":
        codigoEntidad = CodigoEntidad.CONTRATO;
        break;
      case "bienes":
        codigoEntidad = CodigoEntidad.GARANTIA;
        break;
      case "intervinientes":
        codigoEntidad = CodigoEntidad.INTERVINIENTE;
        break;
      case "activoInmobiliario":
        codigoEntidad=CodigoEntidad.ACTIVO_INMOBILIARIO;
        break;
      case "usuarios":
        codigoEntidad=CodigoEntidad.USUARIOS;
        break;
      case "manuales":
      case "modelos":
    case "otros":
    case "global":
    codigoEntidad=CodigoEntidad.CARTERA;
    break;
      default:
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCatalogException("Documentary catalog not found");
        else throw new InvalidCatalogException("Catálogo documental no encontrado");
  }
    if (entidad.equals("manuales")){
      //Manuales
      List<CatalogoDocumentalDTO>catalogoDocumentalDTOList=new ArrayList<>();
      catalogoDocumentalDTOList=this.servicioGestorDocumental.getCatalogosDocumentales(codigoEntidad).stream()
        .map(CatalogoDocumentalDTO::new)
        .collect(Collectors.toList());
      catalogoDocumentalDTOList=catalogoDocumentalDTOList.stream()
        .filter(
          catalogoDocumentalDTO -> {
            if (catalogoDocumentalDTO.getId().equals("HA-02-LICM-14")) {
              return true;
            } else {
              return false;
            }
          })
        .collect(Collectors.toList());
      return catalogoDocumentalDTOList;
    }
    //otros
    if(entidad.contains("otros")){
      List<CatalogoDocumentalDTO>catalogoDocumentalDTOList=new ArrayList<>();
      catalogoDocumentalDTOList=this.servicioGestorDocumental.getCatalogosDocumentales(codigoEntidad).stream()
        .map(CatalogoDocumentalDTO::new)
        .collect(Collectors.toList());
      catalogoDocumentalDTOList=catalogoDocumentalDTOList.stream()
        .filter(
          catalogoDocumentalDTO -> {
            if (catalogoDocumentalDTO.getId().contains("OTRO") || catalogoDocumentalDTO.getId().contains("NORM")
              ||catalogoDocumentalDTO.getId().contains("FACT") ||catalogoDocumentalDTO.getId().contains("HA-02-LICM-22")){
              return true;
            } else {
              return false;
            }
          })
        .collect(Collectors.toList());
      return catalogoDocumentalDTOList;
    }
    if (entidad.contains("modelos")){
      List<CatalogoDocumentalDTO>catalogoDocumentalDTOList=new ArrayList<>();
      catalogoDocumentalDTOList=this.servicioGestorDocumental.getCatalogosDocumentales(codigoEntidad).stream()
        .map(CatalogoDocumentalDTO::new)
        .collect(Collectors.toList());
      catalogoDocumentalDTOList=catalogoDocumentalDTOList.stream()
        .filter(
          catalogoDocumentalDTO -> {
            if (!catalogoDocumentalDTO.getId().equals("HA-02-LICM-14")) {
              return true;
            } else {
              return false;
            }
          })
        .collect(Collectors.toList());
      return catalogoDocumentalDTOList;

    }else{
      return this.servicioGestorDocumental.getCatalogosDocumentales(codigoEntidad).stream()
        .map(CatalogoDocumentalDTO::new)
        .collect(Collectors.toList());
    }
  }
  //Un enum entidad
  public List<CatalogoDocumentalDTO> getCatalogosPortalCliente(ModeloClienteEnum entidad)
      throws InvalidCatalogException, IOException {

List<String> matriculas;

    switch (entidad) {
      case MANUALES:
        matriculas = new ArrayList<>(Arrays.asList("HA-04-LICM-23"));
        break;
      case MODELOS:
        matriculas = new ArrayList<>(Arrays.asList("HA-04-CNCV-63","HA-04-LICM-24","HA-04-FACT-BU"));
        break;
      case OTROS:
        matriculas = new ArrayList<>(Arrays.asList("HA-04-CNCV-63","HA-04-FACT-BU","HA-04-OTRO-21","HA-04-ACUE-58","HA-04-FACT-BV"));
        break;
      default:
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCatalogException("Documentary catalog not found");
        else throw new InvalidCatalogException("Catálogo documental no encontrado");
    }

    return
      this.servicioGestorDocumental.getCatalogosDocumentales(CodigoEntidad.PORTALCLIENTE).stream()
        .map(CatalogoDocumentalDTO::new).filter(catalogoDocumentalDTO -> matriculas.contains(catalogoDocumentalDTO.getId())
      )
        .collect(Collectors.toList());
  }
}
