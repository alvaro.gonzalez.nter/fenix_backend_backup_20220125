package com.haya.alaska.catalogo.infrastructure.controller.dto;

import com.haya.alaska.tipo_evento.domain.TipoEvento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class TipoEventoOutPutDTO implements Serializable {
  private Integer id;
  private Boolean activo;
  private String codigo;
  private String valor;
  private CatalogoMinInfoDTO claseEvento;



  public TipoEventoOutPutDTO(TipoEvento tipoEvento){
    this.id=tipoEvento.getId();
    this.activo=tipoEvento.getActivo();
    this.codigo=tipoEvento.getCodigo();
    this.valor=tipoEvento.getValor();
    this.claseEvento=tipoEvento.getClaseEvento()!=null? new CatalogoMinInfoDTO(tipoEvento.getClaseEvento()):null;
  }

}
