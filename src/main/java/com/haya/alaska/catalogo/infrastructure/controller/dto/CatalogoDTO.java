package com.haya.alaska.catalogo.infrastructure.controller.dto;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.config.IdiomConfigurator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;

@Getter
@NoArgsConstructor
public class CatalogoDTO implements Serializable {
  Integer id;
  String codigo;
  String valor;
  Boolean activo;

  public CatalogoDTO(Catalogo catalogo) {
    this.id = catalogo.getId();
    this.codigo = catalogo.getCodigo();
    String valor = null;
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();
    if (defaultLocal == null || defaultLocal.equals("es")) valor = catalogo.getValor();
    else if (defaultLocal.equals("en")) valor = catalogo.getValorIngles();
    this.valor = valor;
    this.activo = catalogo.getActivo();
  }
}
