package com.haya.alaska.catalogo.infrastructure.controller;

import com.haya.alaska.catalogo.application.CatalogoUseCase;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.catalogo.domain.enums.ModeloClienteEnum;
import com.haya.alaska.catalogo.infrastructure.controller.dto.*;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.evento.infrastructure.controller.dto.SubtipoEventoOutputDTO;
import com.haya.alaska.formalizacion.aplication.FormalizacionUseCase;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.procedimiento.application.ProcedimientoUseCase;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.output.HitoProcedimientoDTO;
import com.haya.alaska.propuesta.aplication.PropuestaUseCase;
import com.haya.alaska.propuesta.infrastructure.controller.dto.SubtipoPropuestaDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.InvalidCatalogException;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/catalogos")
public class CatalogoController {

  @Autowired
  private Map<String, CatalogoRepository<? extends Catalogo, Integer>> catalogoRepository;

  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;

  @Autowired
  private PropuestaUseCase propuestaUseCase;
  @Autowired
  private FormalizacionUseCase formalizacionUseCase;

  @Autowired
  private CatalogoUseCase catalogoUseCase;

  @Autowired
  private ProcedimientoUseCase procedimientoUseCase;

  @ApiOperation(value = "Listado de valores de un catálogo", notes = "Devuelve todos los valores de un catálogo determinado")
  @GetMapping("{catalogo}")
  public List<CatalogoDTO> getAll(@PathVariable("catalogo") String catalogo, @RequestParam(value = "inactivos", defaultValue = "false") boolean inactivos) throws InvalidCatalogException {
    CatalogoRepository<? extends Catalogo, Integer> repository = catalogoRepository.get(catalogo + "Repository");
    if (repository != null) {
      List<? extends Catalogo> lista = null;
      List<Catalogo> listaPaisEspaña = new ArrayList<>();
      List<Catalogo> listaPais = new ArrayList<>();

      if (catalogo.equals("pais")) {

        lista = repository.findAllByOrderByValorAsc();

        for (Catalogo nuevo : lista) {

          var codigo = nuevo.getCodigo();

          if (codigo.equals("011")) {
            listaPaisEspaña.add(nuevo);
          } else {
            listaPais.add(nuevo);
          }
        }
        for (Catalogo unionNuevo : listaPais) {
          listaPaisEspaña.add(unionNuevo);
        }
      } else if (catalogo.equals("tipoProcedimiento")) {
        lista = repository.findAllByActivoIsTrueOrderByValorAsc();
        List<Catalogo> tiposAux = new ArrayList<>();
        for (Catalogo catalogoAux : lista) {
          TipoProcedimiento catalogoProc = (TipoProcedimiento) catalogoAux;
          if (!catalogoProc.isRecovery()) tiposAux.add(catalogoAux);
        }
        return tiposAux.stream().map(CatalogoDTO::new).collect(Collectors.toList());
      } else if (inactivos) {
        //  lista = repository.findAll();
        lista = repository.findAllByOrderByValorAsc();
      } else {
        //  lista = repository.findAllByActivoIsTrue();
        lista = repository.findAllByActivoIsTrueOrderByValorAsc();
      }
      if (catalogo.equals("pais")) {
        return listaPaisEspaña.stream().map(CatalogoDTO::new).collect(Collectors.toList());
      }
      if (catalogo.equals("redSocial")) {
        lista = repository.findAllByActivoIsTrue();
      }

      return lista.stream().map(CatalogoDTO::new).collect(Collectors.toList());
    } else {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCatalogException("Documentary catalog not found: " + catalogo);
      else throw new InvalidCatalogException("Catálogo documental no encontrado: " + catalogo);
    }
  }

  @ApiOperation(value = "Obtener el listado de subtipo de propuestas en función del tipo de propuesta enviado")
  @GetMapping("/{idTipoPropuesta}/subtipoPropuestas")
  @ResponseStatus(HttpStatus.CREATED)
  public List<SubtipoPropuestaDTO> getSubtipoPropuestasByTipo(@PathVariable("idTipoPropuesta") Integer idTipoPropuesta) throws Exception {
    return propuestaUseCase.findSubtipoPropuestaByTipoPropuesta(idTipoPropuesta);
  }

  @ApiOperation(value = "Obtener el listado de subtipo de propuestas en función del tipo de propuesta enviado")
  @GetMapping("/{idTipoPropuesta}/subtipoPropuestasFormalizacion")
  @ResponseStatus(HttpStatus.CREATED)
  public List<SubtipoPropuestaDTO> getSubtipoPropuestasFormByTipo(@PathVariable("idTipoPropuesta") Integer idTipoPropuesta) throws Exception {
    return formalizacionUseCase.getSubtiposFormalizacion(idTipoPropuesta);
  }

  @ApiOperation(value = "Obtener el listado de tipo de propuestas en función de si es Posesion Negociada")
  @GetMapping("/tipoPropuestas")
  @ResponseStatus(HttpStatus.CREATED)
  public List<CatalogoMinInfoDTO> findTipoPropuestaByPN(@RequestParam("posesionNegociada") Boolean posesionNegociada) throws Exception {
    return propuestaUseCase.findTipoPropuestaByPN(posesionNegociada);
  }

  @ApiOperation(value = "Obtener el listado de hitos de un procedimiento en función del tipo de procedimiento enviado")
  @GetMapping("/{idTipoProcedimiento}/hitos")
  @ResponseStatus(HttpStatus.CREATED)
  public List<HitoProcedimientoDTO> getHitosByTipoProcedimiento(@PathVariable("idTipoProcedimiento") Integer idTipoProcedimiento) throws Exception {
    return procedimientoUseCase.findHitosByTipoProcedimiento(idTipoProcedimiento);
  }

  @ApiOperation(value = "Obtener listado de tipos de documentos en función de la entidad")
  @GetMapping("/catalogoDocumental/{entidad}")
  public List<CatalogoDocumentalDTO> listarCatalogosDocumento(@PathVariable("entidad") String entidad) throws InvalidCatalogException, IOException {
    return catalogoUseCase.getCatalogos(entidad);
  }

  @ApiOperation(value = "Obtener listado de tipos de documentos de un interviniente")
  @GetMapping("/catalogoDocumentalPropuesta/{ValorTipoPropuesta}")
  public List<CatalogoDocumentalDTO> listarCatalogosDocumentoPropuesta(@PathVariable("ValorTipoPropuesta") String ValorTipoPropuesta) throws InvalidCatalogException, IOException {
    CodigoEntidad codigoEntidad = null;
    //Aquí le pasas el valor del tipoPropuesta del catálogo
    switch (ValorTipoPropuesta) {
      case "Refinanciación":
        codigoEntidad = CodigoEntidad.PROPUESTAS_REESTRUCTURACION_REFINANCIACION;
        break;
      case "Venta consensuada":
        codigoEntidad = CodigoEntidad.PROPUESTAS_VENTA_CONSENSUADA;
        break;
      case "Dación":
        codigoEntidad = CodigoEntidad.PROPUESTAS_DACION;
        break;
      case "De posesión":
        codigoEntidad = CodigoEntidad.PROPUESTAS_POSESION_NEGOCIADA;
        break;
      case "Cancelación":
        codigoEntidad = CodigoEntidad.PROPUESTAS_CANCELACIONES;
        break;
      case "Reactivación":
        codigoEntidad = CodigoEntidad.PROPUESTAS_REACTIVACIONES;
        break;
      case "Venta de crédito":
        codigoEntidad = CodigoEntidad.PROPUESTAS_VENTA_DE_CREDITO;
        break;
    }
    if (codigoEntidad == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCatalogException("Documentary catalog not found");
      else throw new InvalidCatalogException("Catálogo documental no encontrado");
    }
    return this.servicioGestorDocumental.getCatalogosDocumentalPropuesta(codigoEntidad).stream().map(CatalogoDocumentalDTO::new).collect(Collectors.toList());
  }

  @ApiOperation(value = "Obtiene los nombres de todos los catálogos.")
  @GetMapping("/admin")
  public List<String> getAllCatalogo() {
    return catalogoUseCase.getAllCatalogo();
  }

  @ApiOperation(value = "Obtiene todos los catálogos para la edición.")
  @GetMapping("/admin/{catalogo}")
  public ListWithCountDTO<CatalogoEnlaceOutputDTO> getCatalogo(@PathVariable("catalogo") String catalogo, @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page, @RequestParam(value = "activos") Boolean activos) throws Exception {
    return catalogoUseCase.getEnlazados(catalogo, size, page, activos);
  }

  @ApiOperation(value = "Actualiza un catálogo.")
  @PutMapping("/admin/{catalogo}")
  public void updateCatalogo(@PathVariable("catalogo") String catalogo, @RequestBody List<CatalogoEnlaceInputDTO> catalogos) throws Exception {
    catalogoUseCase.updateCatalogo(catalogo, catalogos);
  }

  @ApiOperation(value = "Obtiene los subtiposEvento")
  @GetMapping("/admin/subtipoE")
  public ListWithCountDTO<SubtipoEventoOutputDTO> getSubtipos(@RequestParam(value = "clase", required = false) Integer clase, @RequestParam(value = "cartera", required = false) Integer cartera, @RequestParam(value = "activo", required = false) Boolean activo, @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) throws Exception {
    return catalogoUseCase.getSubtipoEvento(clase, cartera, activo, size, page);
  }

  @ApiOperation(value = "Actualiza los subtiposEvento.")
  @PutMapping("/admin/subtipoE")
  public List<SubtipoEventoOutputDTO> updateSubtipos(@RequestBody List<SubtipoEventoInputDTO> input) throws Exception {
    return catalogoUseCase.updateSubtipoEvento(input);
  }

  @ApiOperation(value = "Obtiene el listado de Modelos por su tipo")
  @GetMapping("/modelo/{tipoModelo}")
  public List<ModeloDTO> getAllModelos(@PathVariable("tipoModelo") String tipoModelo) throws Exception {
    return catalogoUseCase.getAllModelos(tipoModelo);
  }

  @ApiOperation(value = "Obtiene un Modelo por su Codigo y Tipo")
  @GetMapping("/modelo/{tipoModelo}/{codigo}")
  public ModeloDTO getModeloByCodigo(@PathVariable("tipoModelo") String tipoModelo, @PathVariable("codigo") String codigo) throws Exception {
    return catalogoUseCase.getModeloByCodigo(codigo, tipoModelo);
  }

  @ApiOperation(value = "Obtener listado de tipos de documentos en función de la entidad para el portal Cliente")
  @GetMapping("/catalogoDocumentalCliente")
  public List<CatalogoDocumentalDTO> listarCatalogosDocumentoPortalCliente(@RequestParam ModeloClienteEnum entidad) throws InvalidCatalogException, IOException {
    return catalogoUseCase.getCatalogosPortalCliente(entidad);
  }

}
