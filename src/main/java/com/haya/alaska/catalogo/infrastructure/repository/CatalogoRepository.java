package com.haya.alaska.catalogo.infrastructure.repository;

import com.haya.alaska.catalogo.domain.Catalogo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface CatalogoRepository<C, ID> extends JpaRepository<C, ID> {
  <S extends Catalogo> S save(S var1);

  Optional<C> findByValor(String valor);

  Optional<C> findByCodigo(String codigo);
  Optional<C> findByActivoIsTrueAndCodigo(String codigo);

  List<C> findAllByActivoIsTrue();

  List<C> findAllByOrderByValorAsc();

  List<C> findAllByActivoIsTrueOrderByValorAsc();

  List<C> findAllByCodigoIn(List<String> codigos);
}
