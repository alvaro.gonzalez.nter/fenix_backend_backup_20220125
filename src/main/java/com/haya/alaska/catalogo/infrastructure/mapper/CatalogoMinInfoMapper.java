package com.haya.alaska.catalogo.infrastructure.mapper;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import org.springframework.stereotype.Component;

@Component
public class CatalogoMinInfoMapper {

  public CatalogoMinInfoDTO entityToDto(Catalogo c) {
    if (c == null) {
      return null;
    }
    CatalogoMinInfoDTO catalogoMinInfoDTO = new CatalogoMinInfoDTO(c);
    return catalogoMinInfoDTO;
  }
}
