package com.haya.alaska.catalogo.infrastructure.controller.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ModeloDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer id;
  private String codigo;
  private String nombreModelo;
  private String valor;
  private Boolean activo;

}
