package com.haya.alaska.catalogo.infrastructure.controller.dto;

import com.haya.alaska.catalogo.domain.CatalogoDocumental;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CatalogoDocumentalDTO {
  private String id;
  private String valor;

  public CatalogoDocumentalDTO(CatalogoDocumental catalogoDocumental) {
    this.id = catalogoDocumental.getMatricula();
    this.valor = catalogoDocumental.getDescTdn2();
  }
}
