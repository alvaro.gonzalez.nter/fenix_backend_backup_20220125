package com.haya.alaska.catalogo.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class CatalogoEnlaceInputDTO implements Serializable {
  Integer id;
  String codigo;
  String valor;
  Boolean activo;
  Integer enlazado;
  //private Integer tipo;

}
