package com.haya.alaska.catalogo.infrastructure.controller.dto;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public class CatalogoEnlaceOutputDTO implements Serializable {
  Integer id;
  String codigo;
  String valor;
  //String valorIngles;
  Boolean activo;
  String nombreEnlace;
  CatalogoMinInfoDTO enlazado;

  public CatalogoEnlaceOutputDTO(Catalogo catalogo, Catalogo enlazado, String nombreEnlace) {
    this.id = catalogo.getId();
    this.codigo = catalogo.getCodigo();
    String valor = null;
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();
    if (defaultLocal == null || defaultLocal.equals("es")) valor = catalogo.getValor();
    else if (defaultLocal.equals("en")) valor = catalogo.getValorIngles();
    this.valor = valor;
    this.activo = catalogo.getActivo();
    this.nombreEnlace = nombreEnlace;
    this.enlazado = enlazado != null ? new CatalogoMinInfoDTO(enlazado) : null;
  }
}
