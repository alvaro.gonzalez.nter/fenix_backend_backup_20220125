package com.haya.alaska.catalogo.infrastructure.controller.dto;

import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SubtipoPropuestaOutputDTO implements Serializable {
  private Integer id;
  private Boolean activo;
  private String codigo;
  private String valor;
  private CatalogoMinInfoDTO tipoPropuesta;


  public SubtipoPropuestaOutputDTO(SubtipoPropuesta subtipoPropuesta){
    this.id=subtipoPropuesta.getId();
    this.codigo=subtipoPropuesta.getCodigo();
    this.valor=subtipoPropuesta.getValor();
    this.tipoPropuesta=subtipoPropuesta.getTipoPropuesta()!=null ? new CatalogoMinInfoDTO(subtipoPropuesta.getTipoPropuesta()):null;
  }
}
