package com.haya.alaska.catalogo.infrastructure.controller.dto;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class CatalogoMultiEnlaceDTO implements Serializable {
  Integer id;
  String codigo;
  String valor;
  Boolean activo;
  String nombreEnlace;
  List<CatalogoMinInfoDTO> enlazado;

  public CatalogoMultiEnlaceDTO(Catalogo catalogo, List<Catalogo> enlazados, String nombreEnlace) {
    this.id = catalogo.getId();
    this.codigo = catalogo.getCodigo();
    this.valor = catalogo.getValor();
    this.activo = catalogo.getActivo();
    this.nombreEnlace = nombreEnlace;
    this.enlazado = enlazados.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
  }
}
