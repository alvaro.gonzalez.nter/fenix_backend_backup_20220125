package com.haya.alaska.catalogo.infrastructure.controller.dto;

import com.haya.alaska.evento.infrastructure.controller.dto.PerfilEventoOutputDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SubtipoEventoInputDTO implements Serializable {
  private Integer id;
  private Integer idCartera;
  private Boolean activo;
  private String codigo;
  private String valor;
  private Integer tipoEvento;
  private Integer emisor;
  private Integer destinatario;
  private Integer importancia;
  private Integer fechaLimite;
  private Integer periodicidad;
  private String descripcion;
  private Boolean correo;
  private Boolean programacion;
  private List<Integer> resultados;
}
