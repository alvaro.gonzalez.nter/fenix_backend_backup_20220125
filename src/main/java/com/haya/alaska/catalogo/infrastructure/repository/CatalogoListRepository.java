package com.haya.alaska.catalogo.infrastructure.repository;

import com.haya.alaska.catalogo.domain.CatalogoList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.ws.rs.QueryParam;
import java.util.List;

public interface CatalogoListRepository extends JpaRepository<CatalogoList, Integer> {
  CatalogoList findByCodigo (String codigo);
  CatalogoList findByNombre (String nombre);
  CatalogoList findByNombreIngles (String nombre);
  List<CatalogoList> findByModificableIsTrue ();

  @Query(
      value =
          "SELECT "
              + "  distinct(TABLE_NAME)"
              + "FROM"
              + "  INFORMATION_SCHEMA.KEY_COLUMN_USAGE"
              + "WHERE"
              + "  REFERENCED_TABLE_SCHEMA = 'fenix' AND"
              + "  TABLE_NAME like 'LKUP%';",
      nativeQuery = true)
  List<String> getEnlazadosNames();
}
