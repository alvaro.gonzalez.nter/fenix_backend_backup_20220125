package com.haya.alaska.catalogo.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class TipoEventoInputDTO implements Serializable {
private Integer id;
  private Boolean activo;
  private String codigo;
  private String valor;
  private Integer claseEvento;
}
