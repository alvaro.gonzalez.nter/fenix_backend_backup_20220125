package com.haya.alaska.catalogo.infrastructure.controller.dto;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CatalogoMinInfoDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private Integer id;
  private String codigo;
  private String valor;
  //private String valorIngles;

  public CatalogoMinInfoDTO (Catalogo c){
    this.id = c.getId();
    this.codigo = c.getCodigo();
    String valor = null;
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();
    if (defaultLocal == null || defaultLocal.equals("es")) valor = c.getValor();
    else if (defaultLocal.equals("en")) valor = c.getValorIngles();
    this.valor = valor;
}
}
