package com.haya.alaska.campania_asignada.domain;

import com.haya.alaska.campania.domain.Campania;
import com.haya.alaska.contrato.domain.Contrato;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_RELA_CAMPANIA_ASIGNADA")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "RELA_CAMPANIA_ASIGNADA")
public class CampaniaAsignada {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_CAMPANIA")
  private Campania campania;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_CONTRATO")
  private Contrato contrato;

  @Column(name = "FCH_FECHA_ASIGNACION")
  private Date fechaAsignacion;

}
