package com.haya.alaska.campania_asignada.infraestructure.repository;

import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CampaniaAsignadaRepository extends JpaRepository<CampaniaAsignada, Integer> {

  List<CampaniaAsignada> findAllByCampaniaId(Integer campaniaId);

  List<CampaniaAsignada> findAllByContratoIdIn(List<Integer> contratoId);

  Optional<CampaniaAsignada> findByCampaniaId(Integer campaniaId);

  void deleteById(Integer id);

  List<CampaniaAsignada>findAllByContratoId(Integer contratoId);
}
