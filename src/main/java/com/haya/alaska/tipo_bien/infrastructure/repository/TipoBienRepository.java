package com.haya.alaska.tipo_bien.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_bien.domain.TipoBien;

@Repository
public interface TipoBienRepository extends CatalogoRepository<TipoBien, Integer> {
	 
}
