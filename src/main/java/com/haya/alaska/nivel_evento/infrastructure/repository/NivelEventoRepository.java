package com.haya.alaska.nivel_evento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.nivel_evento.domain.NivelEvento;
import org.springframework.stereotype.Repository;

@Repository
public interface NivelEventoRepository extends CatalogoRepository<NivelEvento, Integer> {}
