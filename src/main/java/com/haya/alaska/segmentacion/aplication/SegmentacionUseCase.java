package com.haya.alaska.segmentacion.aplication;

import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteSegmentacionDTO;
import com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto.SegmentacionCarteraDTO;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionDTO;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionDetalleDTO;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionInputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface SegmentacionUseCase {

  SegmentacionDTO expedienteFiltradoCarteraSinGestor(Integer carteraID);

  SegmentacionCarteraDTO expedientePorCatalogo(Integer carteraId, String nombreCatalogo, Integer idValorCatalogo);

  List<SegmentacionDTO> getSegmentaciones(Integer carteraId, Boolean guardado) throws InvocationTargetException, IllegalAccessException;

  List<SegmentacionDTO> guardar(List<Integer> segmentacionesId) throws InvocationTargetException, IllegalAccessException;

  SegmentacionDetalleDTO getSegmentacion(Integer segmentacionId);

  ListWithCountDTO<ExpedienteSegmentacionDTO> getExpedientesSegmentacion(Integer segmentacionId, Integer size, Integer page, String orderField, String orderDirection, String idConcatenado, String tipoInterviniente, String garantia, String tipoProducto, String valorGarantia, String provinciaPrimeraGarantia, String localidadPrimeraGarantia, String direccionPrimerTitular, String tipoProcedimiento, String hitoJudicial, String situacionContable, Boolean cargasPosteriores, String estrategia, Boolean rankingAgencia) throws InvocationTargetException, IllegalAccessException;

  ListWithCountDTO<ExpedienteSegmentacionDTO> getExpedientesSinAsignar(Integer carteraId, Integer size, Integer page, String orderField, String orderDirection, String idConcatenado, String tipoInterviniente, String garantia, String tipoProducto, String valorGarantia, String provinciaPrimeraGarantia, String localidadPrimeraGarantia, String direccionPrimerTitular, String tipoProcedimiento, String hitoJudicial, String situacionContable, Boolean cargasPosteriores, String estrategia, Boolean rankingAgencia);

  SegmentacionDTO createSegmentacion(SegmentacionInputDTO segmentacionInputDTO) throws Exception;

  SegmentacionDTO modificarSegmentacion(SegmentacionInputDTO segmentacionInputDTO) throws Exception;

}
