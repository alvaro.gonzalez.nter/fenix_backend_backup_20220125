package com.haya.alaska.segmentacion.aplication;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable;
import com.haya.alaska.clasificacion_contable.infrastructure.repository.ClasificacionContableRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteSegmentacionDTO;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto.SegmentacionCarteraDTO;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.producto.infrastructure.repository.ProductoRepository;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionDTO;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionDetalleDTO;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionInputDTO;
import com.haya.alaska.segmentacion.infraestructure.mapper.SegmentacionMapper;
import com.haya.alaska.segmentacion.infraestructure.repository.SegmentacionRepository;
import com.haya.alaska.segmentacion.infraestructure.util.SegmentacionUtil;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.situacion.domain.Situacion;
import com.haya.alaska.situacion.infrastructure.repository.SituacionRepository;
import com.haya.alaska.tipo_evento.infrastructure.repository.TipoEventoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SegmentacionUseCaseImpl implements SegmentacionUseCase {

  @Autowired
  ExpedienteRepository expedienteRepository;

  @Autowired
  ContratoRepository contratoRepository;

  @Autowired
  SegmentacionRepository segmentacionRepository;

  @Autowired
  SegmentacionMapper segmentacionMapper;

  @Autowired
  ExpedienteMapper expedienteMapper;

  @Autowired
  SegmentacionUtil segmentacionUtil;

  @Autowired
  SituacionRepository situacionRepository;

  @Autowired
  TipoEventoRepository tipoEventoRepository;

  @Autowired
  ProductoRepository productoRepository;

  @Autowired
  ClasificacionContableRepository clasificacionContableRepository;

  @Autowired
  ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;

  @Autowired
  BienPosesionNegociadaRepository bienPosesionNegociadaRepository;

  @Override
  public SegmentacionDTO expedienteFiltradoCarteraSinGestor(Integer carteraID) {

    List<Expediente>expedienteList=expedienteRepository.findAllByCarteraId(carteraID);
    List<Expediente>expedienteSinGestorList=new ArrayList<>();
    List<Integer>idExpedientesSinGestor=new ArrayList<>();
    Double totalSaldoGestion=0.0;
    for (Expediente expediente :expedienteList) {
      Usuario gestor=expediente.getGestor();
      if (gestor==null){
        expedienteSinGestorList.add(expediente);
        idExpedientesSinGestor.add(expediente.getId());
      }
    }
    List<Contrato>contratoList=contratoRepository.findAllByExpedienteIdIn(idExpedientesSinGestor);
    List<Bien>bienList=new ArrayList<>();
    List<ContratoInterviniente> intervinientes = new ArrayList<>();
    for (Contrato contrato:contratoList) {
      List<ContratoBien>contratoBienList=contrato.getBienes().stream().collect(Collectors.toList());
      List<ContratoInterviniente>intervinienteList=contrato.getIntervinientes().stream().collect(Collectors.toList());

      Double saldoenGestion=contrato.getSaldoContrato().getSaldoGestion();
      totalSaldoGestion+=saldoenGestion;
      for (ContratoBien contratoBien: contratoBienList) {
        Bien bien=contratoBien.getBien();
        bienList.add(bien);
      }
      for(ContratoInterviniente contratoInterviniente: intervinienteList) {
        intervinientes.add(contratoInterviniente);
      }
    }



    SegmentacionDTO segmentacionDTO=new SegmentacionDTO();
    segmentacionDTO.setNumeroExpedientes(expedienteSinGestorList.size());
    segmentacionDTO.setNumeroContratos(contratoList.size());
    segmentacionDTO.setNumeroGarantias(bienList.size());
    segmentacionDTO.setNumeroIntervinientes(intervinientes.size());
    segmentacionDTO.setTotalSaldoGestion(totalSaldoGestion);

    return segmentacionDTO;
  }

  @Override
  public SegmentacionCarteraDTO expedientePorCatalogo(Integer carteraId, String nombreCatalogo, Integer idValorCatalogo) {
    SegmentacionCarteraDTO segmentacionDTO = new SegmentacionCarteraDTO();


    List<Integer> idsRA = new ArrayList<>();
    Integer nCRA = 0;
    Double sRA = 0.0;
    List<Integer> idsPN = new ArrayList<>();
    Integer nCPN = 0;
    Double sPN = 0.0;

    switch (nombreCatalogo) {
      case "situacionGestion":
        //Situacion situacion = situacionRepository.findById(idValorCatalogo).orElse(null);
        idsRA = expedienteRepository.getDistinctIdByCarteraIdAndContratosSituacionId(carteraId, idValorCatalogo);
        if (!idsRA.isEmpty()){
          nCRA = contratoRepository.countAllBySituacionIdAndExpedienteIdIn(idValorCatalogo, idsRA);
          sRA = contratoRepository.sumSaldoGestionBySituacionIdAndExpedienteIdIn(idValorCatalogo, idsRA);
        }

        idsPN = expedientePosesionNegociadaRepository.findAllDistinctIdByCarteraIdAndBienesPosesionNegociadaSituacionId(carteraId, idValorCatalogo);
        if (!idsPN.isEmpty()) {
          nCPN = bienPosesionNegociadaRepository.countAllBySituacionIdAndExpedientePosesionNegociadaIdIn(idValorCatalogo, idsPN);
          sPN = bienPosesionNegociadaRepository.sumAllBySituacion(idValorCatalogo, idsPN);
        }
        //Double sPN = bienPosesionNegociadaRepository.s


        /*for (Expediente expediente : expedienteTemp) {
          boolean anadirExpediente = false;
          for (Contrato c : expediente.getContratos()) {
            if (c.getSituacion() != null && c.getSituacion().equals(situacion)) {
              numeroContratos++;
              anadirExpediente = true;
            }
          }
          if (anadirExpediente) {
            numeroExpedientes++;
            saldoGestion += expediente.getSaldoGestion();
          }
        }

        for (ExpedientePosesionNegociada expediente : expedientePNTemp) {
          boolean anadirExpediente = false;
          for (BienPosesionNegociada bienPN : expediente.getBienesPosesionNegociada()) {
            if (bienPN.getSituacion() != null && bienPN.getSituacion().equals(situacion))
              anadirExpediente = true;
          }
          if (anadirExpediente) {
            numeroExpedientes++;
            saldoGestion += expediente.getSaldoGestion();
          }
        }*/
        break;

      case "modoGestion":
        idsRA = expedienteRepository.findAllDistinctIdByCarteraIdAndTipoEstadoId(carteraId, idValorCatalogo);
        if (!idsRA.isEmpty()) {
          nCRA = contratoRepository.countAllByExpedienteIdIn(idsRA);
          sRA = contratoRepository.sumSaldoGestionByExpedienteIdIn(idsRA);
        }

        idsPN = expedientePosesionNegociadaRepository.findAllDistinctIdByCarteraIdAndTipoEstadoId(carteraId, idValorCatalogo);
        if (!idsPN.isEmpty()) {
          nCPN = bienPosesionNegociadaRepository.countAllByExpedientePosesionNegociadaIdIn(idsPN);
          sPN = bienPosesionNegociadaRepository.sumAllByModo(idsPN);
        }

        /*for(Expediente expediente: expedientes) {
          numeroContratos += expediente.getContratos().size();
          saldoGestion += expediente.getSaldoGestion();
        }

        expedientesPN = expedientePosesionNegociadaRepository.findAllByCarteraIdAndTipoEstadoId(carteraId, idValorCatalogo);
        for(ExpedientePosesionNegociada expedientePosesionNegociada: expedientesPN) {
          saldoGestion += expedientePosesionNegociada.getSaldoGestion();
        }

        numeroExpedientes += expedientes.size();
        numeroExpedientes += expedientesPN.size();*/

        break;

      case "tipoProducto":
        idsRA = expedienteRepository.findAllDistinctIdByCarteraIdAndContratosProductoId(carteraId, idValorCatalogo);
        if (!idsRA.isEmpty()) {
          nCRA = contratoRepository.countAllByProductoIdAndExpedienteIdIn(idValorCatalogo, idsRA);
          sRA = contratoRepository.sumSaldoGestionByProductoIdAndExpedienteIdIn(idValorCatalogo, idsRA);
        }

        /*Producto producto = productoRepository.findById(idValorCatalogo).orElse(null);
        for (Expediente expediente : expedienteTemp) {
          boolean anadirExpediente = false;
          for (Contrato c : expediente.getContratos()) {
            if (c.getProducto().equals(producto)) {
              numeroContratos++;
              anadirExpediente = true;
            }
          }
          if (anadirExpediente) {
            numeroExpedientes++;
            saldoGestion += expediente.getSaldoGestion();
          }
        }

        //PN no tiene*/

        break;

      case "judicializado":
        if (idValorCatalogo == 1){
          idsRA = expedienteRepository.findAllByCountProcedimientosMore(carteraId);
          if (!idsRA.isEmpty()) {
            nCRA = contratoRepository.countAllByCountProcedimientosMore(idsRA);
            sRA = contratoRepository.sumAllByCountProcedimientosMore(idsRA);
          }

          idsPN = expedientePosesionNegociadaRepository.findAllByCountProcedimientosMore(carteraId);
          if (!idsPN.isEmpty()) {
            nCPN = bienPosesionNegociadaRepository.countAllByCountProcedimientosMore(idsPN);
            sPN = bienPosesionNegociadaRepository.sumAllByCountProcedimientosMore(idsPN);
          }
        }
        else if (idValorCatalogo == 0){
          idsRA = expedienteRepository.findAllByCountProcedimientosZero(carteraId);
          if (!idsRA.isEmpty()) {
            nCRA = contratoRepository.countAllByCountProcedimientosZero(idsRA);
            sRA = contratoRepository.sumAllByCountProcedimientosZero(idsRA);
          }

          idsPN = expedientePosesionNegociadaRepository.findAllByCountProcedimientosZero(carteraId);
          if (!idsPN.isEmpty()) {
            nCPN = bienPosesionNegociadaRepository.countAllByCountProcedimientosZero(idsPN);
            sPN = bienPosesionNegociadaRepository.sumAllByCountProcedimientosZero(idsPN);
          }
        }


        /*for (Expediente expediente : expedienteTemp) {
          boolean anadirExpediente = false;
          for (Contrato c : expediente.getContratos()) {
            if ((c.getProcedimientos().size() > 0 && idValorCatalogo == 1)
                || (c.getProcedimientos().size() == 0 && idValorCatalogo == 0))  {
              numeroContratos++;
              anadirExpediente = true;
            }
          }
          if (anadirExpediente){
            numeroExpedientes++;
            saldoGestion += expediente.getSaldoGestion();
          }
        }

        for (ExpedientePosesionNegociada expediente : expedientePNTemp) {
          boolean anadirExpediente = false;
          for (BienPosesionNegociada bienPN : expediente.getBienesPosesionNegociada()) {
            if ((bienPN.getProcedimientos().size() > 0 && idValorCatalogo == 1)
                || (bienPN.getProcedimientos().size() == 0 && idValorCatalogo == 0))
              anadirExpediente = true;
          }
          if (anadirExpediente) {
            numeroExpedientes++;
            saldoGestion += expediente.getSaldoGestion();
          }
          }*/
        break;

      case "clasificacionContable":
        idsRA = expedienteRepository.findAllDistinctIdByCarteraIdAndContratosClasificacionContableId(carteraId, idValorCatalogo);
        if (!idsRA.isEmpty()) {
          nCRA = contratoRepository.countAllByClasificacionContableIdAndExpedienteIdIn(idValorCatalogo, idsRA);
          sRA = contratoRepository.sumSaldoGestionByClasificacionContableIdAndExpedienteIdIn(idValorCatalogo, idsRA);
        }

        /*ClasificacionContable clasificacionContable =
            clasificacionContableRepository.findById(idValorCatalogo).orElse(null);
        for (Expediente expediente : expedienteTemp) {
          boolean anadirExpediente = false;
          for (Contrato c : expediente.getContratos()) {
            if (c.getClasificacionContable().equals(clasificacionContable)) {
              numeroContratos++;
              anadirExpediente = true;
            }
          }
          if (anadirExpediente) {
            numeroExpedientes++;
            saldoGestion += expediente.getSaldoGestion();
          }
        }

        //PN no tiene*/

        break;
    }

    Integer numeroExpedientes = idsRA.size() + idsPN.size();
    Integer numeroContratos = nCRA + nCPN;
    Double saldoGestion = sRA + sPN;

    segmentacionDTO.setExpedientes(numeroExpedientes);
    segmentacionDTO.setContratos(numeroContratos);
    segmentacionDTO.setSaldoEnGestion(saldoGestion);
    return segmentacionDTO;

  }

  @Override
  public List<SegmentacionDTO> getSegmentaciones(Integer carteraId, Boolean guardado) throws InvocationTargetException, IllegalAccessException {

    List<Segmentacion> segmentaciones = new ArrayList<>();
    if(guardado)
      segmentaciones = segmentacionRepository.findByCarteraIdAndGuardadoTrueOrderByFechaCreacionDesc(carteraId);
    else
      segmentaciones = segmentacionRepository.findByCarteraIdAndGuardadoFalseOrderByFechaCreacionDesc(carteraId);

    //segmentaciones = segmentaciones.stream().sorted(Comparator.comparing(Segmentacion::getFechaCreacion).reversed()).collect(Collectors.toList());
    List<SegmentacionDTO> segmentacionesDto = new ArrayList<>();
    for(Segmentacion segmentacion: segmentaciones) {
      SegmentacionDTO segmentacionDTO=segmentacionMapper.segmentacionADto(segmentacion,carteraId);
      segmentacionesDto.add(segmentacionDTO);
    }
    return segmentacionesDto;
  }

  @Override
  public List<SegmentacionDTO> guardar(List<Integer> segmentacionesId) throws InvocationTargetException, IllegalAccessException {
    List<Segmentacion> segmentaciones = segmentacionRepository.findAllByIdIn(segmentacionesId);
    for(Segmentacion segmentacion: segmentaciones) {
      segmentacion.setGuardado(true);
    }
    List<Segmentacion> segmentacionesFinal = segmentacionRepository.saveAll(segmentaciones);

    List<SegmentacionDTO> segmentacionesDto = new ArrayList<>();
    for(Segmentacion segmentacion: segmentacionesFinal) {
      SegmentacionDTO segmentacionDTO=segmentacionMapper.segmentacionADto(segmentacion,segmentacion.getCartera().getId());
      segmentacionesDto.add(segmentacionDTO);
    }
    return segmentacionesDto;
  }

  @Override
  public SegmentacionDetalleDTO getSegmentacion(Integer segmentacionId) {

    Segmentacion segmentacion = segmentacionRepository.findById(segmentacionId).orElse(null);
    SegmentacionDetalleDTO segmentacionDTO = new SegmentacionDetalleDTO();
    if(segmentacion != null) {
      segmentacionDTO=segmentacionMapper.segmentacionADetalleDto(segmentacion,segmentacion.getCartera().getId());
    }
    return segmentacionDTO;
  }

  @Override
  public ListWithCountDTO<ExpedienteSegmentacionDTO> getExpedientesSegmentacion(Integer segmentacionId, Integer size, Integer page, String orderField, String orderDirection, String idConcatenado, String tipoInterviniente, String garantia, String tipoProducto, String valorGarantia, String provinciaPrimeraGarantia, String localidadPrimeraGarantia, String direccionPrimerTitular, String tipoProcedimiento, String hitoJudicial, String situacionContable, Boolean cargasPosteriores, String estrategia, Boolean rankingAgencia) throws InvocationTargetException, IllegalAccessException {

    Segmentacion segmentacion = segmentacionRepository.findById(segmentacionId).orElse(null);
    List<ExpedienteSegmentacionDTO> listaExpedientes = new ArrayList<>();
    List<ExpedienteSegmentacionDTO> expedientesDevolver = new ArrayList<>();

    if(segmentacion != null) {
      listaExpedientes = segmentacionUtil.sacarExpedientesSegmentacion(segmentacion);
      expedientesDevolver = segmentacionUtil.filtrarDtos(listaExpedientes, orderField, orderDirection, idConcatenado, tipoInterviniente, garantia, tipoProducto, valorGarantia, provinciaPrimeraGarantia, localidadPrimeraGarantia, direccionPrimerTitular, tipoProcedimiento, hitoJudicial, situacionContable, cargasPosteriores, estrategia, rankingAgencia);
    }

    List<ExpedienteSegmentacionDTO> expedientesPaginados = expedientesDevolver.stream().skip(size * page).limit(size).collect(Collectors.toList());
    return new ListWithCountDTO<>(expedientesPaginados, expedientesDevolver.size());
  }

  @Override
  public ListWithCountDTO<ExpedienteSegmentacionDTO> getExpedientesSinAsignar(Integer carteraId, Integer size, Integer page, String orderField, String orderDirection, String idConcatenado, String tipoInterviniente, String garantia, String tipoProducto, String valorGarantia, String provinciaPrimeraGarantia, String localidadPrimeraGarantia, String direccionPrimerTitular, String tipoProcedimiento, String hitoJudicial, String situacionContable, Boolean cargasPosteriores, String estrategia, Boolean rankingAgencia) {

    List<Expediente>expedienteList=expedienteRepository.findAllByCarteraId(carteraId);
    List<ExpedienteSegmentacionDTO> expedienteSinGestorList = new ArrayList<>();
    List<ExpedienteSegmentacionDTO> expedientesDevolver = new ArrayList<>();

    for (Expediente expediente :expedienteList) {
      Usuario gestor=expediente.getGestor();
      if (gestor==null){
        expedienteSinGestorList.add(expedienteMapper.parseSegmentacion(expediente));
      }
    }
    expedientesDevolver = segmentacionUtil.filtrarDtos(expedienteSinGestorList, orderField, orderDirection, idConcatenado, tipoInterviniente, garantia, tipoProducto, valorGarantia, provinciaPrimeraGarantia, localidadPrimeraGarantia, direccionPrimerTitular, tipoProcedimiento, hitoJudicial, situacionContable, cargasPosteriores, estrategia, rankingAgencia);
    List<ExpedienteSegmentacionDTO> expedientesPaginados = expedientesDevolver.stream().skip(size * page).limit(size).collect(Collectors.toList());
    return new ListWithCountDTO<>(expedientesPaginados, expedientesDevolver.size());
  }


  @Override
  public SegmentacionDTO createSegmentacion(SegmentacionInputDTO segmentacionInputDTO) throws Exception {

    Segmentacion segmentacion = segmentacionMapper.segmentacionInputASegmentacion(segmentacionInputDTO);
    Segmentacion segmentacionMetida = segmentacionRepository.save(segmentacion);
    return segmentacionMapper.segmentacionADto(segmentacionMetida, segmentacionInputDTO.getCartera());
  }

  @Override
  public SegmentacionDTO modificarSegmentacion(SegmentacionInputDTO segmentacionInputDTO) throws Exception {

    if(segmentacionInputDTO.getId() == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("There is no such segmentation");
      else throw new NotFoundException("No existe esa segmentacion");
    }

    Segmentacion segmentacionAntigua = segmentacionRepository.findById(segmentacionInputDTO.getId()).orElse(null);
    if(segmentacionAntigua==null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("There is no such segmentation");
      else throw new NotFoundException("No existe esa segmentacion");
    }
    Segmentacion segmentacionNueva = segmentacionMapper.segmentacionInputASegmentacion(segmentacionInputDTO);
    Segmentacion segmentacionMetida = segmentacionRepository.save(segmentacionNueva);
    return segmentacionMapper.segmentacionADto(segmentacionMetida,segmentacionInputDTO.getCartera());
  }

}
