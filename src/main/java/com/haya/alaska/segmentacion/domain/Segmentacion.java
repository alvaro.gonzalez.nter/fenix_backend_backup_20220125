package com.haya.alaska.segmentacion.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.IntervaloImportesSegmentacion.domain.IntervaloImportesSegmentacion;
import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.intervalo_fechas_segmentacion.domain.IntervaloFechasSegmentacion;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_SEGMENTACION")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_SEGMENTACION")
public class Segmentacion {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AREA")
  private AreaUsuario area;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_GRUPO")
  private GrupoUsuario grupo;


  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USUARIO")
  private Usuario gestor;


  @Column(name="DES_NOMBRE_SEGMENTACION")
  private String nombreSegmentacion;


  @Column(name = "FCH_CREACION")
  @CreationTimestamp
  private LocalDateTime fechaCreacion;

  //EXPEDIENTE

  /*@OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_EXPEDIENTE")
  @NotAudited
  private Set<IntervaloFechasSegmentacion> fechasExpediente = new HashSet<>();*/


  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_IMPORTE_EXPEDIENTE", referencedColumnName = "ID")
  private IntervaloImportesSegmentacion importeExpediente;


  //CONTRATO

  @Column(name = "IND_AVALISTAS")
  private Boolean avalistas;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PRODUCTO")
  private Producto producto;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_FECHA_CONTRATO", referencedColumnName = "ID")
  private IntervaloFechasSegmentacion fechaContrato;

  /*@OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_CONTRATO")
  @NotAudited
  private Set<IntervaloFechasSegmentacion> fechasContrato = new HashSet<>();*/

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_CONTRATO")
  @NotAudited
  private Set<IntervaloImportesSegmentacion> importesContrato = new HashSet<>();

  @Column(name = "IND_REESTRUCTURADO")
  private Boolean reestructurado;


  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_CUOTAS", referencedColumnName="ID")
  @NotAudited
  private IntervaloImportesSegmentacion cuotasImpagadas;


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CLASIFICACION_CONTABLE")
  private ClasificacionContable clasificacionContable;

  @Column(name = "DES_RANGO_HIPOTECARIO")
  private String rangoHipotecario;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTRATEGIA")
  private Estrategia estrategia;


  //INTERVINIENTES

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA_INTERVINIENTE")
  private Provincia provinciaInterviniente;


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LOCALIDAD_INTERVINIENTE")
  private Localidad localidadInterviniente;

  @Column(name="DES_TIPO_INTERVINIENTE")
  private String tipoInterviniente;

  /*@Column(name="IND_TELEFONO_FIJO")
  private Boolean telefonoFijo;

  @Column(name="IND_TELEFONO_MOVIL")
  private Boolean telefonoMovil;

  @Column(name="IND_EMAIL")
  private Boolean email;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESIDENCIA")
  private Pais residencia;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_NACIONALIDAD")
  private Pais nacionalidad;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_INTERVINIENTE")
  @NotAudited
  private Set<IntervaloFechasSegmentacion> fechasInterviniente = new HashSet<>();*/


  //GARANTIA

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA_GARANTIA")
  private Provincia provinciaGarantia;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LOCALIDAD_GARANTIA")
  private Localidad localidadGarantia;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_VALOR_GARANTIA", referencedColumnName = "ID")
  private IntervaloImportesSegmentacion valorGarantia;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_BIEN")
  private TipoBien tipoBien;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_CARGA")
  private TipoCarga tipoCarga;


  /*@Column(name="IND_GARANTIA")
  private Boolean esGarantia;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_GARANTIA")
  @NotAudited
  private Set<IntervaloFechasSegmentacion> fechasGarantia = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_GARANTIA")
  @NotAudited
  private Set<IntervaloImportesSegmentacion> importesGarantia = new HashSet<>();*/



  //JUDICIAL

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_PROCEDIMIENTO")
  private TipoProcedimiento tipoProcedimiento;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_HITO_PROCEDIMIENTO")
  private HitoProcedimiento hitoProcedimiento;
/*
  @ManyToMany
  @JoinTable(name = "RELA_SEGMENTACION_HITO",
    joinColumns = { @JoinColumn(name = "ID_HITO") },
    inverseJoinColumns = { @JoinColumn(name = "ID_SEGMENTACION") }
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_SEGMENTACION_HITO")
  private Set<HitoProcedimiento> hitosProcedimiento = new HashSet<>();
*/
  @Column(name="IND_JUDICIALIZADO")
  private Boolean judicializado;

  /*@OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_PROCEDIMIENTO")
  @NotAudited
  private Set<IntervaloFechasSegmentacion> fechasProcedimiento = new HashSet<>();*/

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_FECHA_HITO_PROCEDIMIENTO", referencedColumnName = "ID")
  private IntervaloFechasSegmentacion fechaUltimoHito;

  @Column(name = "IND_GUARDADO", nullable = false, columnDefinition = "boolean default false")
  private Boolean guardado = false;

}
