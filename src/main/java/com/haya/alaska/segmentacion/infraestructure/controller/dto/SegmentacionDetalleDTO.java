package com.haya.alaska.segmentacion.infraestructure.controller.dto;

import com.haya.alaska.IntervaloImportesSegmentacion.controller.dto.IntervaloImportesSegmentacionDTO;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteSegmentacionDTO;
import com.haya.alaska.intervalo_fechas_segmentacion.controller.dto.IntervaloFechasSegmentacionDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SegmentacionDetalleDTO implements Serializable {

  private Integer id;
  private Integer cartera;
  private String nombreSegmentacion;
  //private Date fechaCreacion;

  private Integer numeroGestores;

  private Integer numeroExpedientes;
  private Integer numeroContratos;
  private Integer numeroIntervinientes;
  private Integer numeroGarantias;
  private Double totalSaldoGestion;

  private Integer numeroExpedientesSinAsignar;
  private Integer numeroContratosSinAsignar;
  private Integer numeroIntervinientesSinAsignar;
  private Integer numeroGarantiasSinAsignar;
  private Double totalSaldoGestionSinAsignar;


  private Integer area;
  private Integer grupo;
  private Integer gestor;


  //EXPEDIENTE
  //private List<IntervaloFechasSegmentacionDTO> fechasExpediente;
  private IntervaloImportesSegmentacionDTO importeExpediente;


  //CONTRATO
  private Boolean avalistas;
  private Integer producto;
  private IntervaloFechasSegmentacionDTO fechaContrato;
  private List<IntervaloImportesSegmentacionDTO> importesContrato;
  private IntervaloImportesSegmentacionDTO cuotasImpagadas;
  private Integer clasificacionContable;
  private Boolean reestructurado;
  private String rangoHipotecario;
  private Integer estrategia;


  //INTERVINIENTES
  private Integer provinciaInterviniente;
  private Integer localidadInterviniente;
  private String tipoInterviniente;
  //private Boolean telefonoFijo;
  //private Boolean telefonoMovil;
  //private Boolean email;
  //private Integer paisResidencia;
  //private Integer paisNacionalidad;
  //private List<IntervaloFechasSegmentacionDTO> fechasInterviniente;


  //GARANTIA
  private Integer provinciaGarantia;
  private Integer localidadGarantia;
  //private Boolean esGarantia;
  private Integer tipoBien;
  private Integer tipoCarga;
  //private List<IntervaloFechasSegmentacionDTO> fechasGarantia;
  //private List<IntervaloImportesSegmentacionDTO> importesGarantia;
  private IntervaloImportesSegmentacionDTO valorGarantia;

  //JUDICIAL
  private Integer tipoProcedimiento;
  private Integer hitoProcedimiento;
  private Boolean judicializado;
  private IntervaloFechasSegmentacionDTO fechaUltimoHito;

}
