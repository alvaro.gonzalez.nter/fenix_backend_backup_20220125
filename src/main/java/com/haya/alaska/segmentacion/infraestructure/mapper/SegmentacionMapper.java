package com.haya.alaska.segmentacion.infraestructure.mapper;

import com.haya.alaska.IntervaloImportesSegmentacion.mapper.IntervaloImportesSegmentacionMapper;
import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.area_usuario.infrastructure.repository.AreaUsuarioRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable;
import com.haya.alaska.clasificacion_contable.infrastructure.repository.ClasificacionContableRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteSegmentacionDTO;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import com.haya.alaska.grupo_usuario.infrastructure.repository.GrupoUsuarioRepository;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.hito_procedimiento.infrastructure.repository.HitoProcedimientoRepository;
import com.haya.alaska.intervalo_fechas_segmentacion.mapper.IntervaloFechasSegmentacionMapper;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.producto.infrastructure.repository.ProductoRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionDetalleDTO;
import com.haya.alaska.segmentacion.infraestructure.util.SegmentacionUtil;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionDTO;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionInputDTO;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import com.haya.alaska.tipo_carga.infrastructure.repository.TipoCargaRepository;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SegmentacionMapper {

  @Autowired
  private ExpedienteRepository expedienteRepository;
  @Autowired
  private ContratoRepository contratoRepository;
  @Autowired
  private CarteraRepository carteraRepository;
  @Autowired
  private AreaUsuarioRepository areaUsuarioRepository;
  @Autowired
  private UsuarioRepository usuarioRepository;
  @Autowired
  private GrupoUsuarioRepository grupoUsuarioRepository;
  @Autowired
  private TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired
  private HitoProcedimientoRepository hitoProcedimientoRepository;
  @Autowired
  private ProductoRepository productoRepository;
  @Autowired
  private PaisRepository paisRepository;
  @Autowired
  private ProvinciaRepository provinciaRepository;
  @Autowired
  private LocalidadRepository localidadRepository;
  @Autowired
  private EstrategiaRepository estrategiaRepository;
  @Autowired
  private TipoBienRepository tipoBienRepository;
  @Autowired
  private TipoCargaRepository tipoCargaRepository;
  @Autowired
  private ClasificacionContableRepository clasificacionContableRepository;
  @Autowired
  private ExpedienteMapper expedienteMapper;
  @Autowired
  private IntervaloImportesSegmentacionMapper intervaloImportesSegmentacionMapper;
  @Autowired
  private IntervaloFechasSegmentacionMapper intervaloFechasSegmentacionMapper;
  @Autowired
  private SegmentacionUtil segmentacionUtil;

  public SegmentacionDTO segmentacionADto(Segmentacion segmentacion,Integer carteraId) throws InvocationTargetException, IllegalAccessException {

    SegmentacionDTO segmentacionDTO = new SegmentacionDTO();
    BeanUtils.copyProperties(segmentacion,segmentacionDTO);

    //campos de la propia segmentacion

    segmentacionDTO.setIdCartera(segmentacion.getCartera() != null ? segmentacion.getCartera().getId() : null);
    segmentacionDTO.setArea(segmentacion.getArea() != null ? segmentacion.getArea().getNombre() : null);
    segmentacionDTO.setGrupo(segmentacion.getGrupo() != null ? segmentacion.getGrupo().getNombre() : null);
    segmentacionDTO.setGestor(segmentacion.getGestor() != null ? segmentacion.getGestor().getNombre()  : null);
    segmentacionDTO.setTipoProcedimiento(segmentacion.getTipoProcedimiento() != null ? segmentacion.getTipoProcedimiento().getValor() : null);
    segmentacionDTO.setHitoProcedimiento(segmentacion.getHitoProcedimiento() != null ? segmentacion.getHitoProcedimiento().getValor() : null);
    segmentacionDTO.setProducto(segmentacion.getProducto() != null ? segmentacion.getProducto().getValor() : null);
    segmentacionDTO.setProvinciaGarantia(segmentacion.getProvinciaGarantia() != null ? segmentacion.getProvinciaGarantia().getValor() : null);
    segmentacionDTO.setProvinciaInterviniente(segmentacion.getProvinciaInterviniente() != null ? segmentacion.getProvinciaInterviniente().getValor() : null);
    segmentacionDTO.setLocalidadGarantia(segmentacion.getLocalidadGarantia() != null ? segmentacion.getLocalidadGarantia().getValor() : null);
    segmentacionDTO.setLocalidadInterviniente(segmentacion.getLocalidadInterviniente() != null ? segmentacion.getLocalidadInterviniente().getValor() : null);
    segmentacionDTO.setClasificacionContable(segmentacion.getClasificacionContable() != null ? segmentacion.getClasificacionContable().getValor() : null);
    segmentacionDTO.setEstrategia(segmentacion.getEstrategia() != null ? segmentacion.getEstrategia().getValor() : null);
    segmentacionDTO.setTipoBien(segmentacion.getTipoBien() != null ? segmentacion.getTipoBien().getValor() : null);
    segmentacionDTO.setTipoCarga(segmentacion.getTipoCarga() != null ? segmentacion.getTipoCarga().getValor() : null);

    segmentacionDTO.setCuotasImpagadas(intervaloImportesSegmentacionMapper.entidadADto(segmentacion.getCuotasImpagadas()));
    segmentacionDTO.setImporteExpediente(intervaloImportesSegmentacionMapper.entidadADto(segmentacion.getImporteExpediente()));
    segmentacionDTO.setImportesContrato(intervaloImportesSegmentacionMapper.entidadListADtoList(segmentacion.getImportesContrato()));
    segmentacionDTO.setValorGarantia(intervaloImportesSegmentacionMapper.entidadADto(segmentacion.getValorGarantia()));

    segmentacionDTO.setFechaContrato(intervaloFechasSegmentacionMapper.entidadADto(segmentacion.getFechaContrato()));
    segmentacionDTO.setFechaUltimoHito(intervaloFechasSegmentacionMapper.entidadADto(segmentacion.getFechaUltimoHito()));
    //segmentacionDTO.setFechaCreacion(Date.from(segmentacion.getFechaCreacion().atZone(ZoneId.systemDefault()).toInstant()));

    //campos calculados

    //TODO campo gestores
    Integer numeroGestores = 0;
    Integer numeroExpedientes = 0;
    Integer numeroContratos = 0;
    Integer numeroIntervinientes = 0;
    Integer numeroGarantias = 0;
    Double totalSaldoGestion = 0.00;

    Integer numeroExpedientesTotal = 0;
    Integer numeroContratosTotal = 0;
    Integer numeroIntervinientesTotal = 0;
    Integer numeroGarantiasTotal = 0;
    Double totalSaldoGestionTotal = 0.00;

    List<Expediente>expedienteList=expedienteRepository.findAllByCarteraId(carteraId);
    for(Expediente expediente: expedienteList) {
      numeroExpedientesTotal++;
      totalSaldoGestionTotal += expediente.getSaldoGestion();

      for(Contrato contrato: expediente.getContratos()) {
        numeroContratosTotal++;
        for(ContratoBien bien: contrato.getBienes()) {
          numeroGarantiasTotal++;
        }
        for(ContratoInterviniente interviniente: contrato.getIntervinientes()) {
          numeroIntervinientesTotal++;
        }
      }
    }

    List<Expediente> expedientesSacados = segmentacionUtil.sacarExpedientesFiltrados(segmentacion);
    for(Expediente expediente: expedientesSacados) {
      numeroExpedientes++;
      totalSaldoGestion += expediente.getSaldoGestion();

      for(Contrato contrato: expediente.getContratos()) {
        numeroContratos++;
        for(ContratoBien bien: contrato.getBienes()) {
          numeroGarantias++;
        }
        for(ContratoInterviniente interviniente: contrato.getIntervinientes()) {
          numeroIntervinientes++;
        }
      }
    }

    segmentacionDTO.setNumeroExpedientes(numeroExpedientes);
    segmentacionDTO.setNumeroContratos(numeroContratos);
    segmentacionDTO.setNumeroIntervinientes(numeroIntervinientes);
    segmentacionDTO.setNumeroGarantias(numeroGarantias);
    segmentacionDTO.setTotalSaldoGestion(totalSaldoGestion);

    segmentacionDTO.setNumeroExpedientesSinAsignar(numeroExpedientesTotal-numeroExpedientes);
    segmentacionDTO.setNumeroContratosSinAsignar(numeroContratosTotal-numeroContratos);
    segmentacionDTO.setNumeroIntervinientesSinAsignar(numeroIntervinientesTotal-numeroIntervinientes);
    segmentacionDTO.setNumeroGarantiasSinAsignar(numeroGarantiasTotal-numeroGarantias);
    segmentacionDTO.setTotalSaldoGestionSinAsignar(totalSaldoGestionTotal-totalSaldoGestion);


    return segmentacionDTO;
  }

  public SegmentacionDetalleDTO segmentacionADetalleDto(Segmentacion segmentacion, Integer carteraId) {
    SegmentacionDetalleDTO segmentacionDTO = new SegmentacionDetalleDTO();
    BeanUtils.copyProperties(segmentacion,segmentacionDTO);

    //campos de la propia segmentacion

    segmentacionDTO.setCartera(segmentacion.getCartera() != null ? segmentacion.getCartera().getId() : null);
    segmentacionDTO.setArea(segmentacion.getArea() != null ? segmentacion.getArea().getId() : null);
    segmentacionDTO.setGrupo(segmentacion.getGrupo() != null ? segmentacion.getGrupo().getId() : null);
    segmentacionDTO.setGestor(segmentacion.getGestor() != null ? segmentacion.getGestor().getId()  : null);
    segmentacionDTO.setTipoProcedimiento(segmentacion.getTipoProcedimiento() != null ? segmentacion.getTipoProcedimiento().getId() : null);
    segmentacionDTO.setHitoProcedimiento(segmentacion.getHitoProcedimiento() != null ? segmentacion.getHitoProcedimiento().getId() : null);
    segmentacionDTO.setProducto(segmentacion.getProducto() != null ? segmentacion.getProducto().getId() : null);
    segmentacionDTO.setProvinciaGarantia(segmentacion.getProvinciaGarantia() != null ? segmentacion.getProvinciaGarantia().getId() : null);
    segmentacionDTO.setProvinciaInterviniente(segmentacion.getProvinciaInterviniente() != null ? segmentacion.getProvinciaInterviniente().getId() : null);
    segmentacionDTO.setLocalidadGarantia(segmentacion.getLocalidadGarantia() != null ? segmentacion.getLocalidadGarantia().getId() : null);
    segmentacionDTO.setLocalidadInterviniente(segmentacion.getLocalidadInterviniente() != null ? segmentacion.getLocalidadInterviniente().getId() : null);
    segmentacionDTO.setClasificacionContable(segmentacion.getClasificacionContable() != null ? segmentacion.getClasificacionContable().getId() : null);
    segmentacionDTO.setEstrategia(segmentacion.getEstrategia() != null ? segmentacion.getEstrategia().getId() : null);
    segmentacionDTO.setTipoBien(segmentacion.getTipoBien() != null ? segmentacion.getTipoBien().getId() : null);
    segmentacionDTO.setTipoCarga(segmentacion.getTipoCarga() != null ? segmentacion.getTipoCarga().getId() : null);

    segmentacionDTO.setCuotasImpagadas(intervaloImportesSegmentacionMapper.entidadADto(segmentacion.getCuotasImpagadas()));
    segmentacionDTO.setImporteExpediente(intervaloImportesSegmentacionMapper.entidadADto(segmentacion.getImporteExpediente()));
    segmentacionDTO.setImportesContrato(intervaloImportesSegmentacionMapper.entidadListADtoList(segmentacion.getImportesContrato()));
    segmentacionDTO.setValorGarantia(intervaloImportesSegmentacionMapper.entidadADto(segmentacion.getValorGarantia()));

    segmentacionDTO.setFechaContrato(intervaloFechasSegmentacionMapper.entidadADto(segmentacion.getFechaContrato()));
    segmentacionDTO.setFechaUltimoHito(intervaloFechasSegmentacionMapper.entidadADto(segmentacion.getFechaUltimoHito()));
    //segmentacionDTO.setFechaCreacion(Date.from(segmentacion.getFechaCreacion().atZone(ZoneId.systemDefault()).toInstant()));

    //campos calculados

    //TODO campo gestores
    Integer numeroGestores = 0;
    Integer numeroExpedientes = 0;
    Integer numeroContratos = 0;
    Integer numeroIntervinientes = 0;
    Integer numeroGarantias = 0;
    Double totalSaldoGestion = 0.00;

    Integer numeroExpedientesTotal = 0;
    Integer numeroContratosTotal = 0;
    Integer numeroIntervinientesTotal = 0;
    Integer numeroGarantiasTotal = 0;
    Double totalSaldoGestionTotal = 0.00;

    List<Expediente>expedienteList=expedienteRepository.findAllByCarteraId(carteraId);
    for(Expediente expediente: expedienteList) {
      numeroExpedientesTotal++;
      totalSaldoGestionTotal += expediente.getSaldoGestion();

      for(Contrato contrato: expediente.getContratos()) {
        numeroContratosTotal++;
        for(ContratoBien bien: contrato.getBienes()) {
          numeroGarantiasTotal++;
        }
        for(ContratoInterviniente interviniente: contrato.getIntervinientes()) {
          numeroIntervinientesTotal++;
        }
      }
    }

    List<Expediente> expedientesSacados = segmentacionUtil.sacarExpedientesFiltrados(segmentacion);
    for(Expediente expediente: expedientesSacados) {
      numeroExpedientes++;
      totalSaldoGestion += expediente.getSaldoGestion();

      for(Contrato contrato: expediente.getContratos()) {
        numeroContratos++;
        for(ContratoBien bien: contrato.getBienes()) {
          numeroGarantias++;
        }
        for(ContratoInterviniente interviniente: contrato.getIntervinientes()) {
          numeroIntervinientes++;
        }
      }
    }

    segmentacionDTO.setNumeroExpedientes(numeroExpedientes);
    segmentacionDTO.setNumeroContratos(numeroContratos);
    segmentacionDTO.setNumeroIntervinientes(numeroIntervinientes);
    segmentacionDTO.setNumeroGarantias(numeroGarantias);
    segmentacionDTO.setTotalSaldoGestion(totalSaldoGestion);

    segmentacionDTO.setNumeroExpedientesSinAsignar(numeroExpedientesTotal-numeroExpedientes);
    segmentacionDTO.setNumeroContratosSinAsignar(numeroContratosTotal-numeroContratos);
    segmentacionDTO.setNumeroIntervinientesSinAsignar(numeroIntervinientesTotal-numeroIntervinientes);
    segmentacionDTO.setNumeroGarantiasSinAsignar(numeroGarantiasTotal-numeroGarantias);
    segmentacionDTO.setTotalSaldoGestionSinAsignar(totalSaldoGestionTotal-totalSaldoGestion);


    return segmentacionDTO;
  }

  public Segmentacion segmentacionInputASegmentacion(SegmentacionInputDTO input) throws Exception {

    Segmentacion segmentacion=new Segmentacion();
    BeanUtils.copyProperties(input,segmentacion);

    Cartera cartera = null;
    AreaUsuario area = null;
    GrupoUsuario grupo = null;
    Usuario gestor = null;
    TipoProcedimiento tipoProcedimiento = null;
    HitoProcedimiento hitoProcedimiento = null;
    Producto producto = null;
    Provincia provinciaInterviniente = null;
    Provincia provinciaGarantia = null;
    Localidad localidadInterviniente = null;
    Localidad localidadGarantia = null;
    ClasificacionContable clasificacionContable = null;
    Estrategia estrategia = null;
    TipoBien tipoBien = null;
    TipoCarga tipoCarga = null;

    if(input.getCartera() != null)
      cartera=carteraRepository.findById(input.getCartera()).orElseThrow(() -> new Exception("Cartera no encontrado con el id: " + input.getCartera()));
    else
      throw new Exception("El campo cartera es obligatorio");

    if(input.getArea() != null)
      area = areaUsuarioRepository.findById(input.getArea()).orElseThrow(() -> new Exception("Area usuario no encontrado con el id: " + input.getArea()));

    if(input.getGestor() != null)
      gestor = usuarioRepository.findById(input.getGestor()).orElseThrow(() -> new Exception("Usuario no encontrado con el id: " + input.getGestor()));

    if(input.getGrupo() != null)
      grupo = grupoUsuarioRepository.findById(input.getGrupo()).orElseThrow(() -> new Exception("Grupo no encontrado con el id: " + input.getGrupo()));

    if(input.getTipoProcedimiento() != null)
      tipoProcedimiento = tipoProcedimientoRepository.findById(input.getTipoProcedimiento()).orElseThrow(() -> new Exception("Tipo de procedimiento no encontrado con el id: "+input.getTipoProcedimiento()));

    if(input.getHitoProcedimiento() != null)
      hitoProcedimiento = hitoProcedimientoRepository.findById(input.getHitoProcedimiento()).orElseThrow(() -> new Exception("Hito de procedimiento no encontrado con el id: "+input.getHitoProcedimiento()));

    if(input.getProducto() != null)
      producto = productoRepository.findById(input.getProducto()).orElseThrow(() -> new Exception("Producto no encontrado con el id: "+input.getProducto()));

    if(input.getProvinciaInterviniente() != null)
      provinciaInterviniente = provinciaRepository.findById(input.getProvinciaInterviniente()).orElseThrow(() -> new Exception("Provincia no encontrada con el id: "+input.getProvinciaInterviniente()));

    if(input.getProvinciaGarantia() != null)
      provinciaGarantia = provinciaRepository.findById(input.getProvinciaGarantia()).orElseThrow(() -> new Exception("Provincia no encontrada con el id: "+input.getProvinciaGarantia()));

    if(input.getLocalidadInterviniente() != null)
      localidadInterviniente = localidadRepository.findById(input.getLocalidadInterviniente()).orElseThrow(() -> new Exception("Localidad no encontrada con el id: "+input.getLocalidadInterviniente()));

    if(input.getLocalidadGarantia() != null)
      localidadGarantia = localidadRepository.findById(input.getLocalidadGarantia()).orElseThrow(() -> new Exception("Localidad no encontrada con el id: "+input.getLocalidadGarantia()));

    if(input.getClasificacionContable() != null)
      clasificacionContable = clasificacionContableRepository.findById(input.getClasificacionContable()).orElseThrow(() -> new Exception("Clasificacion contable no encontrada con el id: "+input.getClasificacionContable()));

    if(input.getEstrategia() != null)
      estrategia = estrategiaRepository.findById(input.getEstrategia()).orElseThrow(() -> new Exception("Estrategia no encontrada con el id: "+input.getEstrategia()));

    if(input.getTipoBien() != null)
      tipoBien = tipoBienRepository.findById(input.getTipoBien()).orElseThrow(() -> new Exception("Tipo Bien no encontrado con el id: "+input.getTipoBien()));

    if(input.getTipoCarga() != null)
      tipoCarga = tipoCargaRepository.findById(input.getTipoCarga()).orElseThrow(() -> new Exception("Tipo Carga no encontrado con el id: "+input.getTipoCarga()));

    segmentacion.setCartera(cartera);
    segmentacion.setArea(area);
    segmentacion.setGrupo(grupo);
    segmentacion.setGestor(gestor);
    segmentacion.setTipoProcedimiento(tipoProcedimiento);
    segmentacion.setHitoProcedimiento(hitoProcedimiento);
    segmentacion.setProducto(producto);
    segmentacion.setProvinciaGarantia(provinciaGarantia);
    segmentacion.setProvinciaInterviniente(provinciaInterviniente);
    segmentacion.setLocalidadGarantia(localidadGarantia);
    segmentacion.setLocalidadInterviniente(localidadInterviniente);
    segmentacion.setClasificacionContable(clasificacionContable);
    segmentacion.setEstrategia(estrategia);
    segmentacion.setTipoBien(tipoBien);
    segmentacion.setTipoCarga(tipoCarga);

    segmentacion.setCuotasImpagadas(intervaloImportesSegmentacionMapper.dtoAEntidad(input.getCuotasImpagadas()));
    segmentacion.setImporteExpediente(intervaloImportesSegmentacionMapper.dtoAEntidad(input.getImporteExpediente()));
    segmentacion.setImportesContrato(intervaloImportesSegmentacionMapper.dtoListAEntidadList(input.getImportesContrato()));
    segmentacion.setValorGarantia(intervaloImportesSegmentacionMapper.dtoAEntidad(input.getValorGarantia()));

    segmentacion.setFechaContrato(intervaloFechasSegmentacionMapper.dtoAEntidad(input.getFechaContrato()));
    segmentacion.setFechaUltimoHito(intervaloFechasSegmentacionMapper.dtoAEntidad(input.getFechaUltimoHito()));

    return segmentacion;
  }
}
