package com.haya.alaska.segmentacion.infraestructure.util;

import com.haya.alaska.IntervaloImportesSegmentacion.domain.IntervaloImportesSegmentacion;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteSegmentacionDTO;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.hito_procedimiento.infrastructure.repository.HitoProcedimientoRepository;
import com.haya.alaska.intervalo_fechas_segmentacion.domain.IntervaloFechasSegmentacion;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.procedimiento.domain.*;
import com.haya.alaska.procedimiento.infrastructure.repository.*;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.valoracion.domain.Valoracion;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class SegmentacionUtil {

  @Autowired
  ExpedienteRepository expedienteRepository;
  @Autowired
  HitoProcedimientoRepository hitoProcedimientoRepository;
  @Autowired
  ProcedimientoConcursalRepository procedimientoConcursalRepository;
  @Autowired
  ProcedimientoEjecucionNotarialRepository procedimientoEjecucionNotarialRepository;
  @Autowired
  ProcedimientoEtjRepository procedimientoEtjRepository;
  @Autowired
  ProcedimientoEtnjRepository procedimientoEtnjRepository;
  @Autowired
  ProcedimientoHipotecarioRepository procedimientoHipotecarioRepository;
  @Autowired
  ProcedimientoMonitorioRepository procedimientoMonitorioRepository;
  @Autowired
  ProcedimientoOrdinarioRepository procedimientoOrdinarioRepository;
  @Autowired
  ProcedimientoVerbalRepository procedimientoVerbalRepository;
  @Autowired
  BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  ExpedienteMapper expedienteMapper;


  public List<ExpedienteSegmentacionDTO> sacarExpedientesSegmentacion(Segmentacion segmentacion) throws InvocationTargetException, IllegalAccessException {

    List<Expediente> expedientesSacados = sacarExpedientesFiltrados(segmentacion);
    List<ExpedienteSegmentacionDTO> expedientesAsignados = new ArrayList<>();
    for(Expediente expediente: expedientesSacados) {
      expedientesAsignados.add(expedienteMapper.parseSegmentacion(expediente));
    }
    return expedientesAsignados;
  }

  public Double calcularImporteGarantizado(Expediente expediente) {
    Double importe = 0.00;
    for(Contrato contrato: expediente.getContratos()) {
      for(ContratoBien bien: contrato.getBienes())
        if(bien != null && bien.getImporteGarantizado() != null)
          importe+=bien.getImporteGarantizado();
    }
    return importe;
  }

  public Bien sacarBienPrincipal(Contrato contratoRepresentante) {
    ContratoBien bienPrincipal = null;
    Double importeProv = 0.0;
    if(contratoRepresentante.getBienes() != null && contratoRepresentante.getBienes().size() > 0) {
      for(ContratoBien contratoBien: contratoRepresentante.getBienes()) {
        if(contratoBien.getBien().getImporteBien() > importeProv)
          importeProv=contratoBien.getBien().getImporteBien();
          bienPrincipal=contratoBien;
      }
    }
    if(bienPrincipal == null)
      return null;
    else
      return bienPrincipal.getBien();
  }

  public Procedimiento sacarProcedimientoVivo(Contrato contratoRepresentante) {
    Procedimiento procedimientoDef=null;
    for(Procedimiento procedimiento: contratoRepresentante.getProcedimientos()) {
      if(procedimiento != null && procedimiento.getActivo() != null) {
        procedimientoDef=procedimiento;
        break;
      }
    }
    return procedimientoDef;
  }

  public List<ExpedienteSegmentacionDTO> filtrarDtos(List<ExpedienteSegmentacionDTO> listaExpedientes, String orderField, String orderDirection, String idConcatenado, String tipoInterviniente, String garantia, String tipoProducto, String valorGarantia, String provinciaPrimeraGarantia, String localidadPrimeraGarantia, String direccionPrimerTitular, String tipoProcedimiento, String hitoJudicial, String situacionContable, Boolean cargasPosteriores, String estrategia, Boolean rankingAgencia) {

    if(listaExpedientes == null || listaExpedientes.size() == 0)
      return listaExpedientes;

    //FILTRAR
    if(idConcatenado != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getIdConcatenado(), idConcatenado)).collect(Collectors.toList());

    if(tipoInterviniente != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getTipoInterviniente(), tipoInterviniente)).collect(Collectors.toList());

    if(garantia != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getGarantia(), garantia)).collect(Collectors.toList());

    if(tipoProducto != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getTipoProducto(), tipoProducto)).collect(Collectors.toList());

    if(valorGarantia != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getValorGarantia().toString(), valorGarantia)).collect(Collectors.toList());

    if(provinciaPrimeraGarantia != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getProvinciaPrimeraGarantia(), provinciaPrimeraGarantia)).collect(Collectors.toList());

    if(localidadPrimeraGarantia != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getLocalidadPrimeraGarantia(), localidadPrimeraGarantia)).collect(Collectors.toList());

    if(direccionPrimerTitular != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getDireccionPrimerTitular(), direccionPrimerTitular)).collect(Collectors.toList());

    if(tipoProcedimiento != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getTipoProcedimiento(), tipoProcedimiento)).collect(Collectors.toList());

    if(hitoJudicial != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getHitoJudicial(), hitoJudicial)).collect(Collectors.toList());

    if(situacionContable != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getSituacionContable(), situacionContable)).collect(Collectors.toList());

    if(cargasPosteriores != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> ex.getCargasPosteriores().equals(cargasPosteriores)).collect(Collectors.toList());

    if(estrategia != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> StringUtils.containsIgnoreCase(ex.getEstrategia(),estrategia)).collect(Collectors.toList());

    if(rankingAgencia != null)
      listaExpedientes = listaExpedientes.stream().filter((ExpedienteSegmentacionDTO ex) -> ex.getRankingAgencia().equals(rankingAgencia)).collect(Collectors.toList());

    if(orderDirection!= null && orderField!=null) {
      switch(orderDirection) {
        case "asc":
          switch(orderField) {
            case "idConcatenado":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getIdConcatenado)).collect(Collectors.toList());
              break;
            case "tipoInterviniente":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getTipoInterviniente)).collect(Collectors.toList());
              break;
            case "garantia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getGarantia)).collect(Collectors.toList());
              break;
            case "tipoProducto":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getTipoProducto)).collect(Collectors.toList());
              break;
            case "valorGarantia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getValorGarantia)).collect(Collectors.toList());
              break;
            case "provinciaPrimeraGarantia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getProvinciaPrimeraGarantia)).collect(Collectors.toList());
              break;
            case "localidadPrimeraGarantia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getLocalidadPrimeraGarantia)).collect(Collectors.toList());
              break;
            case "direccionPrimerTitular":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getDireccionPrimerTitular)).collect(Collectors.toList());
              break;
            case "tipoProcedimiento":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getTipoProcedimiento)).collect(Collectors.toList());
              break;
            case "hitoJudicial":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getHitoJudicial)).collect(Collectors.toList());
              break;
            case "situacionContable":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getSituacionContable)).collect(Collectors.toList());
              break;
            case "cargasPosteriores":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getCargasPosteriores)).collect(Collectors.toList());
              break;
            case "estrategia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getEstrategia)).collect(Collectors.toList());
              break;
            case "rankingAgencia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getRankingAgencia)).collect(Collectors.toList());
              break;
          }
          break;
        case "desc":
          switch(orderField) {
            case "idConcatenado":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getIdConcatenado).reversed()).collect(Collectors.toList());
              break;
            case "tipoInterviniente":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getTipoInterviniente).reversed()).collect(Collectors.toList());
              break;
            case "garantia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getGarantia).reversed()).collect(Collectors.toList());
              break;
            case "tipoProducto":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getTipoProducto).reversed()).collect(Collectors.toList());
              break;
            case "valorGarantia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getValorGarantia).reversed()).collect(Collectors.toList());
              break;
            case "provinciaPrimeraGarantia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getProvinciaPrimeraGarantia).reversed()).collect(Collectors.toList());
              break;
            case "localidadPrimeraGarantia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getLocalidadPrimeraGarantia).reversed()).collect(Collectors.toList());
              break;
            case "direccionPrimerTitular":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getDireccionPrimerTitular).reversed()).collect(Collectors.toList());
              break;
            case "tipoProcedimiento":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getTipoProcedimiento).reversed()).collect(Collectors.toList());
              break;
            case "hitoJudicial":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getHitoJudicial).reversed()).collect(Collectors.toList());
              break;
            case "situacionContable":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getSituacionContable).reversed()).collect(Collectors.toList());
              break;
            case "cargasPosteriores":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getCargasPosteriores).reversed()).collect(Collectors.toList());
              break;
            case "estrategia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getEstrategia).reversed()).collect(Collectors.toList());
              break;
            case "rankingAgencia":
              listaExpedientes = listaExpedientes.stream().sorted(Comparator.comparing(ExpedienteSegmentacionDTO::getRankingAgencia).reversed()).collect(Collectors.toList());
              break;
          }
          break;
      }
    }
    return listaExpedientes;
  }

  //sacar la lista de expedientes de una segmentacion (todos)
  public List<Expediente> sacarExpedientesFiltrados(Segmentacion segmentacion) {
    List<Expediente> lista = new ArrayList<>();
    List<Expediente> listaInicial = expedienteRepository.getExpedientesSinGestor(segmentacion.getCartera().getId());
    for(Expediente expediente: listaInicial) {

      if(expediente == null)
        continue;


      //-> importe expediente
      if(segmentacion.getImporteExpediente() != null) {
        boolean pasaFiltroImporteExpediente = false;
        IntervaloImportesSegmentacion importe = segmentacion.getImporteExpediente();
        Double importeExpediente = expediente.getSaldoGestion();

        if(importeExpediente == null)
          continue;

        if(importe.getMayor() != null) {
          if(importeExpediente.compareTo(importe.getMayor()) > 0 )
            pasaFiltroImporteExpediente = true;
          else
            continue;
        }
        if(importe.getMenor() != null) {
          if(importeExpediente.compareTo(importe.getMenor()) < 0 )
            pasaFiltroImporteExpediente = true;
          else
            continue;
        }
        if(importe.getIgual() != null) {
          if(importeExpediente.compareTo(importe.getIgual()) == 0)
            pasaFiltroImporteExpediente = true;
          else
            continue;
        }

          if (!pasaFiltroImporteExpediente)
            continue;
      }



      //CONTRATO

      Contrato contratoRepresentante = expediente.getContratoRepresentante();

      //-> avalistas contrato
      if(segmentacion.getAvalistas() != null && segmentacion.getAvalistas() == true) {
        boolean pasaFiltroAvalistas=false;

        if(contratoRepresentante.getIntervinientes() == null)
          continue;

        for(ContratoInterviniente interviniente: contratoRepresentante.getIntervinientes()) {
          if(interviniente.getTipoIntervencion() != null && interviniente.getTipoIntervencion().getValor().equals("AVALISTA")) {
            pasaFiltroAvalistas=true;
            break;
          }
        }

        if(!pasaFiltroAvalistas)
          continue;
      }
      else if(segmentacion.getAvalistas() != null && segmentacion.getAvalistas() == false) {
        boolean pasaFiltroAvalistas=true;

        if(contratoRepresentante.getIntervinientes() == null)
          continue;

        for(ContratoInterviniente interviniente: contratoRepresentante.getIntervinientes()) {
          if(interviniente.getTipoIntervencion() != null && interviniente.getTipoIntervencion().getValor().equals("AVALISTA")) {
            pasaFiltroAvalistas=false;
            break;
          }
        }

        if(!pasaFiltroAvalistas)
          continue;
      }

      //-> reestructurado (contrato)
      if(segmentacion.getReestructurado() != null) {
        boolean pasaFiltroReestructurado=false;
        if(contratoRepresentante.getReestructurado() != null
          && contratoRepresentante.getReestructurado().equals(segmentacion.getReestructurado())) {
          pasaFiltroReestructurado=true;
        }
        if(!pasaFiltroReestructurado)
          continue;
      }


      //->producto
      if(segmentacion.getProducto()!=null) {
        boolean pasaFiltroProducto=false;
        if(contratoRepresentante.getProducto() != null
          && contratoRepresentante.getProducto().equals(segmentacion.getProducto()))
          pasaFiltroProducto = true;

        if(!pasaFiltroProducto)
          continue;
      }

      //->estrategia
      if(segmentacion.getEstrategia()!=null) {
        boolean pasaFiltroEstrategia=false;
        if(contratoRepresentante.getEstrategia() != null
          && contratoRepresentante.getEstrategia().getEstrategia() != null
          && contratoRepresentante.getEstrategia().getEstrategia().equals(segmentacion.getEstrategia()))
          pasaFiltroEstrategia = true;

        if(!pasaFiltroEstrategia)
          continue;
      }

      //->rango hipotecario
      if(segmentacion.getRangoHipotecario()!=null) {
        boolean pasaFiltroRango=false;
        if(contratoRepresentante.getRangoHipotecario() != null
          && contratoRepresentante.getRangoHipotecario().equals(segmentacion.getRangoHipotecario()))
          pasaFiltroRango = true;

        if(!pasaFiltroRango)
          continue;
      }



      //->fecha contrato
      if(segmentacion.getFechaContrato()!=null) {
        IntervaloFechasSegmentacion intervaloFecha = segmentacion.getFechaContrato();
        boolean pasaFiltroFechasContrato = false;
          Date fechaAComparar = null;
          fechaAComparar=contratoRepresentante.getFechaPrimerImpago();

          if(intervaloFecha.getFechaInicio() != null && intervaloFecha.getFechaFin() != null  && contratoRepresentante.getFechaPrimerImpago() != null) {
            if(intervaloFecha.getFechaInicio().equals(intervaloFecha.getFechaFin()) && fechaAComparar.equals(intervaloFecha.getFechaInicio()))
              pasaFiltroFechasContrato=true;
            else if(fechaAComparar.after(intervaloFecha.getFechaInicio()) && fechaAComparar.before(intervaloFecha.getFechaFin()))
              pasaFiltroFechasContrato=true;
          } else if(intervaloFecha.getFechaInicio() != null  && contratoRepresentante.getFechaPrimerImpago() != null) {
            if(fechaAComparar.after(intervaloFecha.getFechaInicio()))
              pasaFiltroFechasContrato=true;
          } else if(intervaloFecha.getFechaFin() != null && contratoRepresentante.getFechaPrimerImpago() != null) {
            if(fechaAComparar.before(intervaloFecha.getFechaFin()))
              pasaFiltroFechasContrato=true;
          } else {
            pasaFiltroFechasContrato = false;
          }
        if(!pasaFiltroFechasContrato)
          continue;
      }


      //->importes contrato
      if(segmentacion.getImportesContrato()!=null && segmentacion.getImportesContrato().size()>0) {
        boolean pasaFiltroImporteContrato=false;

        for(IntervaloImportesSegmentacion importe: segmentacion.getImportesContrato()) {
          Double importeContrato = null;
          if(importe == null)
            continue;
          else if(importe.getDescripcion().equals("importeInicial"))
            importeContrato=contratoRepresentante.getImporteInicial();
          else if(importe.getDescripcion().equals("deudaImpagada"))
            importeContrato=contratoRepresentante.getDeudaImpagada();
          else if(importe.getDescripcion().equals("saldoGestion"))
            importeContrato=contratoRepresentante.getSaldoGestion();
          else if(importe.getDescripcion().equals("capitalPendiente"))
            importeContrato=contratoRepresentante.getSaldoContrato() != null ? contratoRepresentante.getSaldoContrato().getCapitalPendiente() : null;
          else
            continue;

          if(importeContrato == null)
            continue;

          if(importe.getMayor() != null) {
            if(importeContrato.compareTo(importe.getMayor()) <= 0 ) {
              pasaFiltroImporteContrato=false;
              break;
            } else
              pasaFiltroImporteContrato = true;
          }
          if(importe.getMenor() != null) {
            if(importeContrato.compareTo(importe.getMenor()) >= 0 ) {
              pasaFiltroImporteContrato=false;
              break;
            } else
              pasaFiltroImporteContrato = true;
          }
          if(importe.getIgual() != null) {
            if(importeContrato.compareTo(importe.getIgual()) != 0) {
              pasaFiltroImporteContrato=false;
              break;
            } else
              pasaFiltroImporteContrato=true;
          }
          //pasaFiltroImporteContrato = true;
        }

        if(!pasaFiltroImporteContrato)
          continue;
      }



      //->cuotas impagadas
      if(segmentacion.getCuotasImpagadas()!=null) {
        boolean pasaFiltroCuotas=true;

        if(contratoRepresentante.getCuotasPendientes() == null)
          continue;

        if(segmentacion.getCuotasImpagadas().getMayor() != null) {
          Integer cuotas = segmentacion.getCuotasImpagadas().getMayor().intValue();
          if(contratoRepresentante.getCuotasPendientes().compareTo(cuotas) < 0)
            pasaFiltroCuotas=false;
        }
        if(segmentacion.getCuotasImpagadas().getMenor() != null) {
          Integer cuotas = segmentacion.getCuotasImpagadas().getMenor().intValue();
          if(contratoRepresentante.getCuotasPendientes().compareTo(cuotas) > 0)
            pasaFiltroCuotas=false;
        }
        if(segmentacion.getCuotasImpagadas().getIgual() != null) {
          Integer cuotas = segmentacion.getCuotasImpagadas().getIgual().intValue();
          if(contratoRepresentante.getCuotasPendientes().compareTo(cuotas) != 0)
            pasaFiltroCuotas=false;
        }
        if(!pasaFiltroCuotas)
          continue;
      }


      //->clasificacion contable
      if(segmentacion.getClasificacionContable()!=null) {
        boolean pasaFiltroContable=false;
        if(segmentacion.getClasificacionContable() != null
          && segmentacion.getClasificacionContable().equals(contratoRepresentante.getClasificacionContable()))
          pasaFiltroContable=true;

        if(!pasaFiltroContable)
          continue;
      }


      //INTERVINIENTES
      Interviniente primerInterviniente = contratoRepresentante.getPrimerInterviniente();

      //->provincia
      if(segmentacion.getProvinciaInterviniente() != null) {
        boolean pasaFiltroProvincia=false;
        Provincia provincia = null;

        if(primerInterviniente.getDirecciones() == null)
          continue;

        for(Direccion direcc: primerInterviniente.getDirecciones()) {
          if(direcc != null && direcc.getNombre() != null && !direcc.getNombre().equals("") && direcc.getProvincia() != null) {
            provincia = direcc.getProvincia();
            break;
          }
        }
        if(provincia != null && provincia.equals(segmentacion.getProvinciaInterviniente())) {
          pasaFiltroProvincia=true;
        }
        if(!pasaFiltroProvincia)
          continue;
      }

      //-->localidad
      if(segmentacion.getLocalidadInterviniente() != null) {
        boolean pasaFiltroLocalidad=false;
        Localidad localidad = null;

        if(primerInterviniente.getDirecciones() == null)
          continue;

        for(Direccion direcc: primerInterviniente.getDirecciones()) {
          if(direcc.getNombre() != null && !direcc.getNombre().equals("") && direcc.getLocalidad() != null) {
            localidad = direcc.getLocalidad();
            break;
          }
        }
        if(localidad != null && localidad.equals(segmentacion.getLocalidadInterviniente())) {
          pasaFiltroLocalidad=true;
        }
        if(!pasaFiltroLocalidad)
          continue;
      }

      //-->tipo intervinientes (persona fisica o juridica)
      if(segmentacion.getTipoInterviniente() != null) {
        boolean pasaFiltroTipoInterviniente=false;

        if(primerInterviniente.getPersonaJuridica() == true && segmentacion.getTipoInterviniente().equals("juridica")) {
            pasaFiltroTipoInterviniente = true;

        } else if(primerInterviniente.getPersonaJuridica() == false && segmentacion.getTipoInterviniente().equals("fisica")) {
            pasaFiltroTipoInterviniente = true;

        }
        if(!pasaFiltroTipoInterviniente)
          continue;
      }



      //GARANTIA
      Bien bienPrincipal = sacarBienPrincipal(contratoRepresentante);
      boolean valido = false;

      if(contratoRepresentante.getBienes() != null && contratoRepresentante.getBienes().size()>0 && bienPrincipal != null)
        valido = true;

        //-> provincia garantia
        if(segmentacion.getProvinciaGarantia()!=null) {
          boolean pasaFiltroProvinciaGarantia = false;
          if(valido==true && bienPrincipal.getProvincia() != null && segmentacion.getProvinciaGarantia().equals(bienPrincipal.getProvincia()))
            pasaFiltroProvinciaGarantia = true;

        if(!pasaFiltroProvinciaGarantia)
          continue;
        }

        //->tipo bien
        if(segmentacion.getTipoBien()!=null) {
          boolean pasaFiltroTipoBien=false;
          if(valido==true && bienPrincipal.getTipoBien() != null && segmentacion.getTipoBien().equals(bienPrincipal.getTipoBien()))
            pasaFiltroTipoBien=true;

          if(!pasaFiltroTipoBien)
            continue;
        }

        //->tipo carga
        if(segmentacion.getTipoCarga()!=null) {
          boolean pasaFiltroTipoCarga=false;

          if(valido != true || bienPrincipal.getCargas() == null)
            continue;

          for(Carga carga: bienPrincipal.getCargas()) {
            if(carga != null && carga.getTipoCarga() != null && segmentacion.getTipoCarga().equals(carga.getTipoCarga())) {
              pasaFiltroTipoCarga=true;
              break;
            }
          }
          if(!pasaFiltroTipoCarga)
            continue;
        }


        //-> localidad garantia
        if(segmentacion.getLocalidadGarantia()!=null) {
          boolean pasaFiltroLocalidadGarantia = false;
          if(valido==true && bienPrincipal != null && bienPrincipal.getLocalidad() != null && segmentacion.getLocalidadGarantia().equals(bienPrincipal.getLocalidad())) {
            pasaFiltroLocalidadGarantia = true;
          }
          if(!pasaFiltroLocalidadGarantia)
            continue;
        }

        //-->valor garantia (sacar la ultima tasacion)
        if(segmentacion.getValorGarantia() != null) {
          Boolean pasaFiltroValorGarantia = false;

          if(valido != true)
            continue;

          Tasacion tasacion = null;
          Date fechaProv = null;
          for(Tasacion tasacionProv: bienPrincipal.getTasaciones()) {
            if(fechaProv==null) {
              fechaProv = tasacionProv.getFecha();
            }
            if(fechaProv != null && tasacionProv.getFecha().after(fechaProv)) {
              fechaProv = tasacionProv.getFecha();
              tasacion=tasacionProv;
            }
          }
          if(tasacion == null)
            continue;

          Double importeGarantia = tasacion.getImporte();
          IntervaloImportesSegmentacion importe = segmentacion.getValorGarantia();

          if(importe.getMayor() != null) {
            if(importeGarantia.compareTo(importe.getMayor()) > 0 )
              pasaFiltroValorGarantia = true;
            else
              continue;
          }
          if(importe.getMenor() != null) {
            if(importeGarantia.compareTo(importe.getMenor()) < 0 )
              pasaFiltroValorGarantia = true;
            else
              continue;
          }
          if(importe.getIgual() != null) {
            if(importeGarantia.compareTo(importe.getIgual()) == 0)
              pasaFiltroValorGarantia=true;
            else
              continue;
          }

          if(!pasaFiltroValorGarantia)
            continue;
        }

      //}





      //JUDICIAL

      //-> judicializado
      if(segmentacion.getJudicializado() == null || segmentacion.getJudicializado() == false) {
        if(contratoRepresentante.getProcedimientos() != null  && contratoRepresentante.getProcedimientos().size() != 0) {
          continue;
        }
      }
      else if(segmentacion.getJudicializado() == true) {
        if(contratoRepresentante.getProcedimientos() == null || contratoRepresentante.getProcedimientos().size() == 0)
          continue;

        Procedimiento procedimiento = sacarProcedimientoVivo(contratoRepresentante);

        //-> tipo de procedimiento
        if(!procedimiento.getTipo().getId().equals(segmentacion.getTipoProcedimiento().getId())) {
          continue;
        }

        //-> hitos procedimiento
        if(segmentacion.getHitoProcedimiento()!=null) {
          boolean pasaFiltroHitos=false;

          if(procedimiento.getHitoProcedimiento().equals(segmentacion.getHitoProcedimiento()))
            pasaFiltroHitos = true;

          if(!pasaFiltroHitos)
            continue;
          }

        //-->fecha ultimo hito
        if(segmentacion.getFechaUltimoHito() != null) {
          boolean pasaFiltroFechasJudicial=true;
          IntervaloFechasSegmentacion fecha=segmentacion.getFechaUltimoHito();
            if(fecha.getFechaInicio() != null || fecha.getFechaFin() != null) {
              HitoProcedimiento fechaHito=segmentacion.getHitoProcedimiento();
              switch (procedimiento.getTipo().getCodigo()) {
                case "HIP":
                  ProcedimientoHipotecario procedimientoHipotecario = procedimientoHipotecarioRepository.findById(procedimiento.getId()).orElse(null);
                  if(procedimientoHipotecario == null)
                    break;
                  switch (fechaHito.getCodigo()) {
                    case "HIP_02": //fecha oposicion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoHipotecario.getFechaOposicion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoHipotecario.getFechaOposicion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoHipotecario.getFechaOposicion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;

                    case "HIP_05": //fecha notificacion auto
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoHipotecario.getFechaNotificacionAuto().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoHipotecario.getFechaNotificacionAuto().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoHipotecario.getFechaNotificacionAuto().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;

                    case "HIP_06": //fecha certificacion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoHipotecario.getFechaCertificacion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoHipotecario.getFechaCertificacion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoHipotecario.getFechaCertificacion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "HIP_09": //fecha presentacion demanda
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoHipotecario.getFechaPresentacionDemanda().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoHipotecario.getFechaPresentacionDemanda().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoHipotecario.getFechaPresentacionDemanda().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    default:
                      break;
                  }

                case "ETNJ":
                  ProcedimientoEtnj procedimientoEtnj = procedimientoEtnjRepository.findById(procedimiento.getId()).orElse(null);
                  if(procedimientoEtnj == null)
                    break;
                  switch (fechaHito.getCodigo()) {
                    case "ETNJ_02": //fecha oposicion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEtnj.getFechaOposicion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEtnj.getFechaOposicion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEtnj.getFechaOposicion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "ETNJ_04": //fecha notificacion req pago
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEtnj.getFechaNotificacionReqPago().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEtnj.getFechaNotificacionReqPago().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEtnj.getFechaNotificacionReqPago().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "ETNJ_05": //fecha auto despachado
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEtnj.getFechaAutoDespachando().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEtnj.getFechaAutoDespachando().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEtnj.getFechaAutoDespachando().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "ETNJ_07": //fecha presentacion demanda
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEtnj.getFechaPresentacionDemanda().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else{
                        if(fecha.getFechaInicio() != null && !procedimientoEtnj.getFechaPresentacionDemanda().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEtnj.getFechaPresentacionDemanda().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    default:
                      break;
                  }
                  break;
                case "EN":
                  ProcedimientoEjecucionNotarial procedimientoEn = procedimientoEjecucionNotarialRepository.findById(procedimiento.getId()).orElse(null);
                  if(procedimientoEn == null)
                    break;
                  switch (fechaHito.getCodigo()) {
                    case "EN_01": //fecha posesion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEn.getFechaPosesion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEn.getFechaPosesion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEn.getFechaPosesion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "EN_02": //fecha firma escritura
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEn.getFechaFirmaEscritura().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEn.getFechaFirmaEscritura().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEn.getFechaFirmaEscritura().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "EN_06": //fecha subasta
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEn.getFechaSubasta().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEn.getFechaSubasta().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEn.getFechaSubasta().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "EN_07": //fecha auto
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEn.getFechaAuto().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEn.getFechaAuto().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEn.getFechaAuto().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "EN_08": //fecha req pago deudor
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEn.getFechaReqPagoDeudor().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEn.getFechaReqPagoDeudor().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEn.getFechaReqPagoDeudor().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "EN_09": //fecha recepcion certificacion registral
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEn.getFechaRecepcionCertificacionRegistral().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEn.getFechaRecepcionCertificacionRegistral().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEn.getFechaRecepcionCertificacionRegistral().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "EN_10": //fecha firma acta
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEn.getFechaFirmaActa().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEn.getFechaFirmaActa().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEn.getFechaFirmaActa().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    default:
                      break;
                  }
                  break;
                case "MON":
                  ProcedimientoMonitorio procedimientoMonitorio = procedimientoMonitorioRepository.findById(procedimiento.getId()).orElse(null);
                  if(procedimientoMonitorio == null)
                    break;
                  switch (fechaHito.getCodigo()) {
                    case "MON_02": //fecha oposicion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoMonitorio.getFechaOposicion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoMonitorio.getFechaOposicion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoMonitorio.getFechaOposicion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "MON_03": //fecha
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoMonitorio.getFecha().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoMonitorio.getFecha().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoMonitorio.getFecha().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "MON_04": //fecha admision demanda
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoMonitorio.getFechaAdmisionDemanda().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoMonitorio.getFechaAdmisionDemanda().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoMonitorio.getFechaAdmisionDemanda().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "MON_06": //fecha presentacion demanda
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoMonitorio.getFechaPresentacionDemanda().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoMonitorio.getFechaPresentacionDemanda().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoMonitorio.getFechaPresentacionDemanda().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    default:
                      break;
                  }
                  break;
                case "VERB":
                  ProcedimientoVerbal procedimientoVerbal = procedimientoVerbalRepository.findById(procedimiento.getId()).orElse(null);
                  if(procedimientoVerbal == null)
                    break;
                  switch (fechaHito.getCodigo()) {
                    case "VERB_02": //fecha celebracion juicio
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoVerbal.getFechaCelebracionJuicio().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoVerbal.getFechaCelebracionJuicio().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoVerbal.getFechaCelebracionJuicio().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "VERB_04": //fecha notificacion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoVerbal.getFechaNotificacion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoVerbal.getFechaNotificacion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoVerbal.getFechaNotificacion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "VERB_05": //fecha admision demanda
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoVerbal.getFechaAdmisionDemanda().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoVerbal.getFechaAdmisionDemanda().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoVerbal.getFechaAdmisionDemanda().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "VERB_07": //fecha presentacion demanda
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoVerbal.getFechaPresentacionDemanda().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoVerbal.getFechaPresentacionDemanda().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoVerbal.getFechaPresentacionDemanda().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    default:
                      break;
                  }
                  break;
                case "ORD":
                  ProcedimientoOrdinario procedimientoOrdinario = procedimientoOrdinarioRepository.findById(procedimiento.getId()).orElse(null);
                  if(procedimientoOrdinario == null)
                    break;
                  switch (fechaHito.getCodigo()) {
                    case "ORD_02": //fecha oposicion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoOrdinario.getFechaOposicion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoOrdinario.getFechaOposicion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoOrdinario.getFechaOposicion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "ORD_03": //fecha req pago deudor
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoOrdinario.getFechaReqPagoDeudor().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoOrdinario.getFechaReqPagoDeudor().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoOrdinario.getFechaReqPagoDeudor().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "ORD_06": //fecha admision demanda
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoOrdinario.getFechaAdmisionDemanda().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoOrdinario.getFechaAdmisionDemanda().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoOrdinario.getFechaAdmisionDemanda().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "ORD_08": //fecha presentacion demanda
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoOrdinario.getFechaPresentacionDemanda().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoOrdinario.getFechaPresentacionDemanda().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoOrdinario.getFechaPresentacionDemanda().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    default:
                      break;
                  }
                  break;
                case "ETJ":
                  ProcedimientoEtj procedimientoEtj = procedimientoEtjRepository.findById(procedimiento.getId()).orElse(null);
                  if(procedimientoEtj == null)
                    break;
                  switch (fechaHito.getCodigo()) {
                    case "ETJ_02": //fecha oposicion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEtj.getFechaOposicion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEtj.getFechaOposicion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEtj.getFechaOposicion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "ETJ_03": //fecha notificacion req pago
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEtj.getFechaNotificacionReqPago().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEtj.getFechaNotificacionReqPago().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEtj.getFechaNotificacionReqPago().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "ETJ_04": //fecha auto despachando ejecucion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEtj.getFechaAutoDespachandoEjecucion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {

                        if(fecha.getFechaInicio() != null && !procedimientoEtj.getFechaAutoDespachandoEjecucion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEtj.getFechaAutoDespachandoEjecucion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "ETJ_06": //fecha presentacion demanda
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoEtj.getFechaPresentacionDemanda().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoEtj.getFechaPresentacionDemanda().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoEtj.getFechaPresentacionDemanda().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    default:
                      break;
                  }
                  break;
                case "CON":
                  ProcedimientoConcursal procedimientoConcursal = procedimientoConcursalRepository.findById(procedimiento.getId()).orElse(null);
                  if(procedimientoConcursal == null)
                    break;
                  switch (fechaHito.getCodigo()) {
                    case "CON_01": //fecha decreto
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoConcursal.getFechaDecreto().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoConcursal.getFechaDecreto().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoConcursal.getFechaDecreto().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "CON_02": //fecha resolucion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoConcursal.getFechaResolucion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoConcursal.getFechaResolucion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoConcursal.getFechaResolucion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "CON_03": //fecha apertura
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoConcursal.getFechaApertura().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoConcursal.getFechaApertura().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoConcursal.getFechaApertura().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "CON_05": //fecha auto fase convenio
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoConcursal.getFechaAutoFaseConvenio().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoConcursal.getFechaAutoFaseConvenio().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoConcursal.getFechaAutoFaseConvenio().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "CON_06": //fecha auto declarando concurso
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoConcursal.getFechaAutoDeclarandoConcurso().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoConcursal.getFechaAutoDeclarandoConcurso().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoConcursal.getFechaAutoDeclarandoConcurso().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    case "CON_07": //fecha publicacion
                      if(fecha.getFechaInicio().equals(fecha.getFechaFin()) && !procedimientoConcursal.getFechaPublicacion().equals(fecha.getFechaInicio()))
                        pasaFiltroFechasJudicial=false;
                      else {
                        if(fecha.getFechaInicio() != null && !procedimientoConcursal.getFechaPublicacion().after(fecha.getFechaInicio()))
                          pasaFiltroFechasJudicial=false;
                        if(fecha.getFechaFin() != null && !procedimientoConcursal.getFechaPublicacion().before(fecha.getFechaFin()))
                          pasaFiltroFechasJudicial=false;
                      }
                      break;
                    default:
                      break;
                  }
                  break;
                default:
                  break;
              }

            }
          if(!pasaFiltroFechasJudicial)
            continue;
          }


      }





      lista.add(expediente);
    }

    return lista;
  }



}
