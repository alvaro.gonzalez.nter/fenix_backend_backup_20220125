package com.haya.alaska.segmentacion.infraestructure.repository;

import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SegmentacionRepository extends JpaRepository<Segmentacion, Integer> {

  List<Segmentacion> findByGuardadoTrue();
  List<Segmentacion> findByCarteraId(Integer idCartera);
  List<Segmentacion> findByCarteraIdOrderByFechaCreacionDesc(Integer idCartera);
  List<Segmentacion> findByCarteraIdAndGuardadoTrueOrderByFechaCreacionDesc(Integer idCartera);
  List<Segmentacion> findByCarteraIdAndGuardadoFalseOrderByFechaCreacionDesc(Integer idCartera);
  Optional<Segmentacion> findById(Integer idSegmentacion);
  List<Segmentacion> findAllByIdIn(List<Integer> segmentacionesId);

}
