package com.haya.alaska.segmentacion.infraestructure.controller;

import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteSegmentacionDTO;
import com.haya.alaska.integraciones.portal_cliente.infrastructure.controller.dto.SegmentacionCarteraDTO;
import com.haya.alaska.segmentacion.aplication.SegmentacionUseCase;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionDTO;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionDetalleDTO;
import com.haya.alaska.segmentacion.infraestructure.controller.dto.SegmentacionInputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.List;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/segmentacion")
public class SegmentacionController {

  @Autowired
  SegmentacionUseCase segmentacionUseCase;

  @ApiOperation(
    value = "Obtener numero de expedientes, contratos, intervinientes, garantias, saldo, que no tienen gestor asignado",
    notes = "Devuelve el número de expedientes, contratos, etc sin gestor asignado")
  @GetMapping("/sinGestor")
  public SegmentacionDTO getExpedienteSinGestor(@RequestParam Integer carteraId) {
    return segmentacionUseCase.expedienteFiltradoCarteraSinGestor(carteraId);
  }

  @ApiOperation(
    value = "Obtener numero de expedientes, contratos, saldo, que tienen un valor de un catálogo")
  @GetMapping("/porCatalogo")
  public SegmentacionCarteraDTO getExpedientePorCatalogo(@RequestParam Integer carteraId,
                                                         @RequestParam String nombreCatalogo,
                                                         @RequestParam Integer idValorCatalogo) {
    return segmentacionUseCase.expedientePorCatalogo(carteraId, nombreCatalogo, idValorCatalogo);
  }

  @ApiOperation(
    value = "Obtener lista de segmentaciones de una cartera")
  @GetMapping("/all")
  public List<SegmentacionDTO> getSegmentaciones(@RequestParam Integer carteraId,
                                                 @RequestParam Boolean guardado) throws InvocationTargetException, IllegalAccessException {
    return segmentacionUseCase.getSegmentaciones(carteraId,guardado);
  }

  @ApiOperation(
    value = "Poner como guardadas las segmentaciones que se indiquen")
  @PostMapping("/guardar")
  public List<SegmentacionDTO> guardarSegmentaciones(@RequestBody List<Integer> segmentacionesId) throws InvocationTargetException, IllegalAccessException {
    return segmentacionUseCase.guardar(segmentacionesId);
  }

  @ApiOperation(
    value = "Obtener el detalle de una segmentación")
  @GetMapping("/detalle/{segmentacionId}")
  public SegmentacionDetalleDTO getSegmentacion(@PathVariable Integer segmentacionId){
    return segmentacionUseCase.getSegmentacion(segmentacionId);
  }

  @ApiOperation(
      value = "Obtener la lista de expedientes sin asignar")
  @GetMapping("/expedientesSinAsignar")
  public ListWithCountDTO<ExpedienteSegmentacionDTO> getExpedientesSinAsignar(@RequestParam Integer carteraId,
                                                                                @RequestParam(value = "size") Integer size,
                                                                                @RequestParam(value = "page") Integer page,
                                                                              @RequestParam(value = "orderField", required = false) String orderField,
                                                                              @RequestParam(value = "orderDirection", required = false) String orderDirection,
                                                                              @RequestParam(value = "idConcatenado", required = false) String idConcatenado,
                                                                              @RequestParam(value = "tipoInterviniente", required = false) String tipoInterviniente,
                                                                              @RequestParam(value = "garantia", required = false) String garantia,
                                                                              @RequestParam(value = "tipoProducto", required = false) String tipoProducto,
                                                                              @RequestParam(value = "valorGarantia", required = false) String valorGarantia,
                                                                              @RequestParam(value = "provinciaPrimeraGarantia", required = false) String provinciaPrimeraGarantia,
                                                                              @RequestParam(value = "localidadPrimeraGarantia", required = false) String localidadPrimeraGarantia,
                                                                              @RequestParam(value = "direccionPrimerTitular", required = false) String direccionPrimerTitular,
                                                                              @RequestParam(value = "tipoProcedimiento", required = false) String tipoProcedimiento,
                                                                              @RequestParam(value = "hitoJudicial", required = false) String hitoJudicial,
                                                                              @RequestParam(value = "situacionContable", required = false) String situacionContable,
                                                                              @RequestParam(value = "cargasPosteriores", required = false) Boolean cargasPosteriores,
                                                                              @RequestParam(value = "estrategia", required = false) String estrategia,
                                                                              @RequestParam(value = "rankingAgencia", required = false) Boolean rankingAgencia
  ) throws InvocationTargetException, IllegalAccessException {
    return segmentacionUseCase.getExpedientesSinAsignar(carteraId, size, page, orderField, orderDirection, idConcatenado, tipoInterviniente, garantia, tipoProducto, valorGarantia, provinciaPrimeraGarantia, localidadPrimeraGarantia, direccionPrimerTitular, tipoProcedimiento, hitoJudicial, situacionContable, cargasPosteriores, estrategia, rankingAgencia);
  }

  @ApiOperation(
    value = "Obtener la lista de expedientes de una segmentacion")
  @GetMapping("/{segmentacionId}")
  public ListWithCountDTO<ExpedienteSegmentacionDTO> getExpedientesSegmentacion(@PathVariable Integer segmentacionId,
                                                                                @RequestParam(value = "size") Integer size,
                                                                                @RequestParam(value = "page") Integer page,
                                                                                @RequestParam(value = "orderField", required = false) String orderField,
                                                                                @RequestParam(value = "orderDirection", required = false) String orderDirection,
                                                                                @RequestParam(value = "idConcatenado", required = false) String idConcatenado,
                                                                                @RequestParam(value = "tipoInterviniente", required = false) String tipoInterviniente,
                                                                                @RequestParam(value = "garantia", required = false) String garantia,
                                                                                @RequestParam(value = "tipoProducto", required = false) String tipoProducto,
                                                                                @RequestParam(value = "valorGarantia", required = false) String valorGarantia,
                                                                                @RequestParam(value = "provinciaPrimeraGarantia", required = false) String provinciaPrimeraGarantia,
                                                                                @RequestParam(value = "localidadPrimeraGarantia", required = false) String localidadPrimeraGarantia,
                                                                                @RequestParam(value = "direccionPrimerTitular", required = false) String direccionPrimerTitular,
                                                                                @RequestParam(value = "tipoProcedimiento", required = false) String tipoProcedimiento,
                                                                                @RequestParam(value = "hitoJudicial", required = false) String hitoJudicial,
                                                                                @RequestParam(value = "situacionContable", required = false) String situacionContable,
                                                                                @RequestParam(value = "cargasPosteriores", required = false) Boolean cargasPosteriores,
                                                                                @RequestParam(value = "estrategia", required = false) String estrategia,
                                                                                @RequestParam(value = "rankingAgencia", required = false) Boolean rankingAgencia

  ) throws InvocationTargetException, IllegalAccessException {
    return segmentacionUseCase.getExpedientesSegmentacion(segmentacionId, size, page, orderField, orderDirection, idConcatenado, tipoInterviniente, garantia, tipoProducto, valorGarantia, provinciaPrimeraGarantia, localidadPrimeraGarantia, direccionPrimerTitular, tipoProcedimiento, hitoJudicial, situacionContable, cargasPosteriores, estrategia, rankingAgencia);
  }

  @ApiOperation(value = "Crear una segmentacion")
  @PostMapping("/")
  public SegmentacionDTO createSegmentacion(@RequestBody SegmentacionInputDTO segmentacionInputDTO) throws Exception {
    return segmentacionUseCase.createSegmentacion(segmentacionInputDTO);
  }

  @ApiOperation(value = "Modificar una segmentacion")
  @PutMapping("/")
  public SegmentacionDTO modificarSegmentacion(@RequestBody SegmentacionInputDTO segmentacionInputDTO) throws Exception {
    return segmentacionUseCase.modificarSegmentacion(segmentacionInputDTO);
  }

}
