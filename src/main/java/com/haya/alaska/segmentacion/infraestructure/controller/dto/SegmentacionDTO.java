package com.haya.alaska.segmentacion.infraestructure.controller.dto;

import com.haya.alaska.IntervaloImportesSegmentacion.controller.dto.IntervaloImportesSegmentacionDTO;
import com.haya.alaska.intervalo_fechas_segmentacion.controller.dto.IntervaloFechasSegmentacionDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SegmentacionDTO implements Serializable {

  private Integer id;
  private Integer idCartera;
  private String nombreSegmentacion;
  //private Date fechaCreacion;

  private Integer numeroGestores;

  private Integer numeroExpedientes;
  private Integer numeroContratos;
  private Integer numeroIntervinientes;
  private Integer numeroGarantias;
  private Double totalSaldoGestion;

  private Integer numeroExpedientesSinAsignar;
  private Integer numeroContratosSinAsignar;
  private Integer numeroIntervinientesSinAsignar;
  private Integer numeroGarantiasSinAsignar;
  private Double totalSaldoGestionSinAsignar;


  private String area;
  private String grupo;
  private String gestor;


  //EXPEDIENTE
  //private List<IntervaloFechasSegmentacionDTO> fechasExpediente;
  private IntervaloImportesSegmentacionDTO importeExpediente;


  //CONTRATO
  private Boolean avalistas;
  private String producto;
  private IntervaloFechasSegmentacionDTO fechaContrato;
  private List<IntervaloImportesSegmentacionDTO> importesContrato;
  private IntervaloImportesSegmentacionDTO cuotasImpagadas;
  private String clasificacionContable;
  private Boolean reestructurado;
  private String rangoHipotecario;
  private String estrategia;


  //INTERVINIENTES
  private String provinciaInterviniente;
  private String localidadInterviniente;
  private String tipoInterviniente;
  //private Boolean telefonoFijo;
  //private Boolean telefonoMovil;
  //private Boolean email;
  //private String paisResidencia;
  //private String paisNacionalidad;
  //private List<IntervaloFechasSegmentacionDTO> fechasInterviniente;


  //GARANTIA
  private String provinciaGarantia;
  private String localidadGarantia;
  //private Boolean esGarantia;
  private String tipoBien;
  private String tipoCarga;
  //private List<IntervaloFechasSegmentacionDTO> fechasGarantia;
  //private List<IntervaloImportesSegmentacionDTO> importesGarantia;
  private IntervaloImportesSegmentacionDTO valorGarantia;

  //JUDICIAL
  private String tipoProcedimiento;
  private String hitoProcedimiento;
  private Boolean judicializado;
  private IntervaloFechasSegmentacionDTO fechaUltimoHito;


}
