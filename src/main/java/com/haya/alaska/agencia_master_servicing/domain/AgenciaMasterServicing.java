package com.haya.alaska.agencia_master_servicing.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.expediente.domain.Expediente;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_AGENCIA_MASTER_SERVICING")
@Entity
@Table(name = "LKUP_AGENCIA_MASTER_SERVICING")
public class AgenciaMasterServicing extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AREA_PERFIL")
  @NotAudited
  private Set<Expediente> expedientes = new HashSet<>();
}
