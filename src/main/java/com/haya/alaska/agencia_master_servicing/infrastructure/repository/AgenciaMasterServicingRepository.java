package com.haya.alaska.agencia_master_servicing.infrastructure.repository;

import com.haya.alaska.agencia_master_servicing.domain.AgenciaMasterServicing;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgenciaMasterServicingRepository extends CatalogoRepository<AgenciaMasterServicing, Integer> {}
