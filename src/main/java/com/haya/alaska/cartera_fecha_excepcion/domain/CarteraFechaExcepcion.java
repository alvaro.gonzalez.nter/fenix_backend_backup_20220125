package com.haya.alaska.cartera_fecha_excepcion.domain;

import com.haya.alaska.cartera.domain.Cartera;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_CARTERA_FECHA_EXCEPCION")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_CARTERA_FECHA_EXCEPCION")
public class CarteraFechaExcepcion implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA")
  private Date fecha;
  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;
}
