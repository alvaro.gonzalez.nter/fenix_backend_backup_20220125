package com.haya.alaska.cartera_fecha_excepcion.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.cartera_fecha_excepcion.domain.CarteraFechaExcepcion;

@Repository
public interface CarteraFechaExcepcionRepository
    extends JpaRepository<CarteraFechaExcepcion, Integer> {}
