package com.haya.alaska.situacion.domain;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.contrato.domain.Contrato;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_SITUACION")
@Entity
@Table(name = "LKUP_SITUACION")
public class Situacion extends Catalogo {

    private static final long serialVersionUID = 1L;

    @OneToMany(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_SITUACION")
    @NotAudited
    private Set<Contrato> contratos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SITUACION")
    @NotAudited
    private Set<BienPosesionNegociada> bienesPN = new HashSet<>();
}
