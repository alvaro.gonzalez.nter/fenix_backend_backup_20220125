package com.haya.alaska.situacion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.situacion.domain.Situacion;
import org.springframework.stereotype.Repository;

@Repository
public interface SituacionRepository extends CatalogoRepository<Situacion, Integer> {}
