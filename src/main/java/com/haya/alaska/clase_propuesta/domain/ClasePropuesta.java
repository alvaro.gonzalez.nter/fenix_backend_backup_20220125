package com.haya.alaska.clase_propuesta.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * ClasePropuesta entity
 *
 * @author agonzalez
 */
@Audited
@AuditTable(value = "HIST_LKUP_CLASE_PROPUESTA")
@NoArgsConstructor
@Entity
@Getter
@Table(name = "LKUP_CLASE_PROPUESTA")
public class ClasePropuesta extends Catalogo implements Serializable {

}