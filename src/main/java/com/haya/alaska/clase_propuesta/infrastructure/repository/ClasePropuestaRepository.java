package com.haya.alaska.clase_propuesta.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.clase_propuesta.domain.ClasePropuesta;
import org.springframework.stereotype.Repository;

@Repository
public interface ClasePropuestaRepository extends CatalogoRepository<ClasePropuesta, Integer> {}
