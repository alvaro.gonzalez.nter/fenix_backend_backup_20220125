package com.haya.alaska.ocupante_bien.infrastructure.repository;

import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OcupanteBienRepository extends JpaRepository<OcupanteBien, Integer> {

  Optional<OcupanteBien> findByBienPosesionNegociadaIdAndOrden(Integer idBien, Integer orden);

  Optional<OcupanteBien> findByBienPosesionNegociadaIdAndOcupanteIdAndOrden(Integer idBien, Integer idOcupante, Integer orden);

  Optional<OcupanteBien> findByBienPosesionNegociadaIdAndOcupanteId(Integer idBien, Integer idOcupante);

  List<OcupanteBien> findAllByOcupanteId(Integer idOcupante);
  List<OcupanteBien> findDistinctByBienPosesionNegociadaIdIn(List<Integer> bienes);


  List<OcupanteBien> findAllByBienPosesionNegociadaId(Integer idBien);
}
