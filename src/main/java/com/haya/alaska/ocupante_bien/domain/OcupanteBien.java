package com.haya.alaska.ocupante_bien.domain;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.tipo_ocupante.domain.TipoOcupante;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_RELA_OCUPANTE_BIEN")
@Entity
@Getter
@Setter
@Table(name = "RELA_OCUPANTE_BIEN")
public class OcupanteBien  implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_OCUPANTE")
  private Ocupante ocupante;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN_POSESION_NEGOCIADA")
  private BienPosesionNegociada bienPosesionNegociada;

  @Column(name = "NUM_ORDEN", nullable = false)
  private Integer orden;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_OCUPANTE")
  private TipoOcupante tipoOcupante;

}
