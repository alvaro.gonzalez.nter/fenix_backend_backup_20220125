package com.haya.alaska.datos_check_list.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.documentacion_validada.domain.DocumentacionValidada;
import com.haya.alaska.nombres_check_list.domain.NombresCheckList;
import com.haya.alaska.propuesta.domain.Propuesta;
import lombok.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_RELA_DATOS_CHECK_LIST")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "RELA_DATOS_CHECK_LIST",
  uniqueConstraints = {@UniqueConstraint(columnNames = {
    "ID_NOMBRE_CHECK_LIST", "ID_PROPUESTA"
  })})
public class DatosCheckList implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_NOMBRE_CHECK_LIST", nullable = false)
  private NombresCheckList nombre;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_DOCUMENTACION_VALIDADA")
  private DocumentacionValidada documentacionValidada;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROPUESTA", nullable = false)
  private Propuesta propuesta;

  @ManyToMany
  @JoinTable(name = "RELA_BIEN_DATOS_CHECK_LIST",
    joinColumns = {@JoinColumn(name = "ID_DATOS_CHECK_LIST")},
    inverseJoinColumns = {@JoinColumn(name = "ID_BIEN")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_BIEN_DATOS_CHECK_LIST")
  private Set<Bien> bienes = new HashSet<>();
}
