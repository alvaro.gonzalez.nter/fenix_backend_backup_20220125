package com.haya.alaska.datos_check_list.infrastructure.repository;

import com.haya.alaska.datos_check_list.domain.DatosCheckList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DatosCheckListRepository extends JpaRepository<DatosCheckList, Integer> {
  List<DatosCheckList> findAllByPropuestaId(Integer idPropuesta);
  List<DatosCheckList> findAllByNombreId(Integer idNombre);
  Optional<DatosCheckList> findByPropuestaIdAndNombreId(Integer idPropuesta, Integer idNombre);
  Optional<DatosCheckList> findByPropuestaIdAndNombreCodigo(Integer idPropuesta, String codNombre);
}

