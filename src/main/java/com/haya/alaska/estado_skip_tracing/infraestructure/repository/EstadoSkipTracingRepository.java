package com.haya.alaska.estado_skip_tracing.infraestructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estado_skip_tracing.domain.EstadoSkipTracing;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoSkipTracingRepository extends CatalogoRepository<EstadoSkipTracing, Integer> {
}
