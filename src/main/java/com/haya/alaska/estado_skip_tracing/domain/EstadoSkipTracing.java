package com.haya.alaska.estado_skip_tracing.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_ESTADO_SKIP_TRACING")
@Entity
@Table(name = "LKUP_ESTADO_SKIP_TRACING")
@Getter
public class EstadoSkipTracing extends Catalogo{

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_SKIP_TRACING")
  @NotAudited
  private Set<SkipTracing> skipTracings = new HashSet<>();
}
