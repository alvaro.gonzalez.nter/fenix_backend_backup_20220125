package com.haya.alaska.importe_estimacion_contrato.infrastructure.repository;

import com.haya.alaska.importe_estimacion_contrato.domain.ImporteEstimacionContrato;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ImporteEstimacionContratoRepository extends JpaRepository<ImporteEstimacionContrato, Integer> {
  @Modifying
  @Query("delete from ImporteEstimacionContrato t where t.id = ?1")
  void deleteById(Integer entityId);
}
