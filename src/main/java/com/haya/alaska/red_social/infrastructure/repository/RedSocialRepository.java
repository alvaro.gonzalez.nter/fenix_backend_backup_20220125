package com.haya.alaska.red_social.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.red_social.domain.RedSocial;
import org.springframework.stereotype.Repository;

@Repository
public interface RedSocialRepository extends CatalogoRepository<RedSocial, Integer> {

}

