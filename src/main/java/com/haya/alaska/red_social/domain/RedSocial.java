package com.haya.alaska.red_social.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.datos_origen.domain.DatosOrigen;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_RED_SOCIAL")
@Entity
@Table(name = "LKUP_RED_SOCIAL")
public class RedSocial extends Catalogo {
  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RED_SOCIAL")
  @NotAudited
  private Set<DatosOrigen> datosOrigenes = new HashSet<>();
}
