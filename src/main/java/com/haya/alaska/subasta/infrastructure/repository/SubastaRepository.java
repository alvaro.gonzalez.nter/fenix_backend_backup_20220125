package com.haya.alaska.subasta.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.subasta.domain.Subasta;

@Repository
public interface SubastaRepository extends JpaRepository<Subasta, Integer> {}
