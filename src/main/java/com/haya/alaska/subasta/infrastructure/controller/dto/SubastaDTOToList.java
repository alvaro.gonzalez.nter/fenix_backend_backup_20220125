package com.haya.alaska.subasta.infrastructure.controller.dto;

import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.bien_subastado.infrastructure.controller.dto.BienSubastadoDTOToList;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.subasta.domain.Subasta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SubastaDTOToList implements Serializable {

  private static final long serialVersionUID = 1L;

  private String id;
  private Boolean activo;
  private Integer orden;
  private Date fechaNotificacionDecreto;
  private Date fechaPagoTasa;
  private Date fechaInicioPujas;
  private Date fechaFinPujas;
  private String decisionSuspension;
  private String motivoSuspensionCancelacion;
  private CatalogoMinInfoDTO estado;
  private Integer procedimiento;
  private List<BienSubastadoDTOToList> bienesSubastados;

  public SubastaDTOToList(Subasta subasta) {
    this.id = String.valueOf(subasta.getId());
    this.activo = subasta.getActivo();
    this.orden = subasta.getOrden();
    this.fechaNotificacionDecreto = subasta.getFechaNotificacionDecreto();
    this.fechaPagoTasa = subasta.getFechaPagoTasa();
    this.fechaInicioPujas = subasta.getFechaInicioPujas();
    this.fechaFinPujas = subasta.getFechaFinPujas();
    this.decisionSuspension = subasta.getDecisionSuspension();
    this.motivoSuspensionCancelacion = subasta.getMotivoSuspensionCancelacion();
    this.estado = subasta.getEstado() != null ? new CatalogoMinInfoDTO(subasta.getEstado()) : null;
    this.procedimiento = subasta.getProcedimiento() != null ? subasta.getProcedimiento().getId() : null;

    List<BienSubastadoDTOToList> bienesSubastados = new ArrayList<>();
    for (BienSubastado bs : subasta.getBienesSubastados()){
        bienesSubastados.add(new BienSubastadoDTOToList(bs));
    }
    this.bienesSubastados = bienesSubastados;
}
}
