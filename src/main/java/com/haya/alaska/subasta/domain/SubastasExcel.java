package com.haya.alaska.subasta.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class SubastasExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Procedure ID");
      cabeceras.add("Notification date decree");
      cabeceras.add("Payment Date Rate");
      cabeceras.add("Bidding Start Date");
      cabeceras.add("Bidding End Date");
      cabeceras.add("Decision Suspension");
      cabeceras.add("Status");
      cabeceras.add("Reason Suspension Cancellation");
      cabeceras.add("Assets ID");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Procedimiento");
      cabeceras.add("Fecha Notificacion Decreto");
      cabeceras.add("Fecha Pago Tasa");
      cabeceras.add("Fecha Inicio Pujas");
      cabeceras.add("Fecha Fin Pujas");
      cabeceras.add("Decision Suspension");
      cabeceras.add("Estado");
      cabeceras.add("Motivo Suspension Cancelacion");
      cabeceras.add("Id Bienes");
    }
  }

  public SubastasExcel(Subasta subasta, Integer idContrato, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Procedure ID", subasta.getId());
      this.add("Notification date decree", subasta.getFechaNotificacionDecreto());
      this.add("Payment Date Rate", subasta.getFechaPagoTasa());
      this.add("Bidding Start Date", subasta.getFechaInicioPujas());
      this.add("Bidding End Date", subasta.getFechaFinPujas());
      this.add("Decision Suspension", subasta.getDecisionSuspension());
      this.add("Status", subasta.getEstado() != null ? subasta.getEstado().getValorIngles() : null);
      this.add("Reason Suspension Cancellation", subasta.getMotivoSuspensionCancelacion());
      List<Bien> bienes =
        subasta.getBienesSubastados().stream()
          .map(BienSubastado::getBien)
          .collect(Collectors.toList());
      String allBienes = "";
      for (Bien bien : bienes) {
        allBienes += bien.getId() + " ,";
      }
      if (bienes.size() > 0) {
        allBienes = allBienes.substring(0, allBienes.length() - 2);
      }
      this.add("Assets ID", allBienes);

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Contrato", idContrato);
      this.add("Id Procedimiento", subasta.getId());
      this.add("Fecha Notificacion Decreto", subasta.getFechaNotificacionDecreto());
      this.add("Fecha Pago Tasa", subasta.getFechaPagoTasa());
      this.add("Fecha Inicio Pujas", subasta.getFechaInicioPujas());
      this.add("Fecha Fin Pujas", subasta.getFechaFinPujas());
      this.add("Decision Suspension", subasta.getDecisionSuspension());
      this.add("Estado", subasta.getEstado() != null ? subasta.getEstado().getValor() : null);
      this.add("Motivo Suspension Cancelacion", subasta.getMotivoSuspensionCancelacion());
      List<Bien> bienes =
          subasta.getBienesSubastados().stream()
              .map(BienSubastado::getBien)
              .collect(Collectors.toList());
      String allBienes = "";
      for (Bien bien : bienes) {
        allBienes += bien.getId() + " ,";
      }
      if (bienes.size() > 0) {
        allBienes = allBienes.substring(0, allBienes.length() - 2);
      }
      this.add("Id Bienes", allBienes);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
