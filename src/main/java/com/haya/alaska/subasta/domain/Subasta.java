package com.haya.alaska.subasta.domain;

import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.estado_subasta.domain.EstadoSubasta;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_SUBASTA")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_SUBASTA")
public class Subasta implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "ID_CARGA")
  private String idCarga;

  @Column(name = "NUM_ORDEN")
  private Integer orden;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_NOTIFICACION_DECRETO")
  private Date fechaNotificacionDecreto;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PAGO_TASA")
  private Date fechaPagoTasa;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_INICIO_PUJAS")
  private Date fechaInicioPujas;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FIN_PUJAS")
  private Date fechaFinPujas;

  @Column(name = "DES_DECISION_SUSPENSION")
  private String decisionSuspension;

  @Column(name = "DES_MOTIVO_SUSPENSION_CANCELACION")
  private String motivoSuspensionCancelacion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROCEDIMIENTO")
  private Procedimiento procedimiento;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_SUBASTA")
  private EstadoSubasta estado;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SUBASTA")
  @NotAudited
  private Set<BienSubastado> bienesSubastados = new HashSet<>();

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  //No es un campo que se tenga que guardar en base de datos.
  private String idRecovery;
}
