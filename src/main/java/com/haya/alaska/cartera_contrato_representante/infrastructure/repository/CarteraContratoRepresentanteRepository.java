package com.haya.alaska.cartera_contrato_representante.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;

@Repository
public interface CarteraContratoRepresentanteRepository
    extends JpaRepository<CarteraContratoRepresentante, Integer> {}
