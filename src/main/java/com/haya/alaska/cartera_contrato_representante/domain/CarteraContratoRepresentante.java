package com.haya.alaska.cartera_contrato_representante.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera_contrato_representante_producto.domain.CarteraContratoRepresentanteProducto;
import com.haya.alaska.criterio_contrato_representante.domain.CriterioContratoRepresentante;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_RELA_CARTERA_CONTRATO_REPRESENTANTE")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "RELA_CARTERA_CONTRATO_REPRESENTANTE")
public class CarteraContratoRepresentante implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CRITERIO_CONTRATO_REPRESENTANTE")
  private CriterioContratoRepresentante criterioContratoCartera;

  @Column(name = "NUM_ORDEN")
  private Integer orden;

  @Column(name = "IND_ORDEN_ASC")
  private Boolean ordenAsc;

  @OneToMany(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_CARTERA_CONTRATO_REPRESENTANTE")
  @OrderColumn(name = "orden")
  @NotAudited
  private Set<CarteraContratoRepresentanteProducto> productos = new HashSet<>();

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

}
