package com.haya.alaska.criterio_ms_antiguedad_deuda.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.criterio_ms_antiguedad_deuda.domain.CriterioMSAntiguedadDeuda;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CriterioMSAntiguedadDeudaRepository extends CatalogoRepository<CriterioMSAntiguedadDeuda, Integer> {
  List<CriterioMSAntiguedadDeuda> findAllByCarteraId(Integer id);
  List<CriterioMSAntiguedadDeuda> findAllByCarteraIdAndActivoIsTrue(Integer ido);
}
