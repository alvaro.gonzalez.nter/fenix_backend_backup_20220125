package com.haya.alaska.tipo_garantia.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;

@Repository
public interface TipoGarantiaRepository extends CatalogoRepository<TipoGarantia, Integer> {
	 
}
