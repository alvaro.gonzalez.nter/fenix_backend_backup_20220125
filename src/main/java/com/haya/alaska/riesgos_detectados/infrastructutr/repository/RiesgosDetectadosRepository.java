package com.haya.alaska.riesgos_detectados.infrastructutr.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.riesgos_detectados.domain.RiesgosDetectados;
import org.springframework.stereotype.Repository;

@Repository
public interface RiesgosDetectadosRepository extends CatalogoRepository<RiesgosDetectados, Integer> {}
