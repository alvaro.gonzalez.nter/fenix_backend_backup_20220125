package com.haya.alaska.riesgos_detectados.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@AuditTable(value = "HIST_LKUP_RIESGOS_DETECTADOS")
@NoArgsConstructor
@Entity
@Table(name = "LKUP_RIESGOS_DETECTADOS")
public class RiesgosDetectados extends Catalogo {
}
