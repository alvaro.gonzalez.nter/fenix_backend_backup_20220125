package com.haya.alaska.campania_asignada_pn.infraestructure.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;

@Repository
public interface CampaniaAsignadaBienPNRepository extends JpaRepository<CampaniaAsignadaBienPN, Integer> {

  List<CampaniaAsignadaBienPN> findAllByCampaniaId(Integer campaniaId);

  List<CampaniaAsignadaBienPN> findAllByBienPosesionNegociadaIdIn(List<Integer> bienPosesionNegociada);

  Optional<CampaniaAsignadaBienPN> findByCampaniaId(Integer campaniaId);

  void deleteById(Integer id);

}
