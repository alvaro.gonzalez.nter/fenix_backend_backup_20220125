package com.haya.alaska.sistema_amortizacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.contrato.domain.Contrato;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_SISTEMA_AMORTIZACION")
@Entity
@Table(name = "LKUP_SISTEMA_AMORTIZACION")
public class SistemaAmortizacion extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SISTEMA_AMORTIZACION")
  @NotAudited
  private Set<Contrato> contratos = new HashSet<>();
}
