package com.haya.alaska.sistema_amortizacion.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.sistema_amortizacion.domain.SistemaAmortizacion;

@Repository
public interface SistemaAmortizacionRepository extends CatalogoRepository<SistemaAmortizacion, Integer> {
	 
}
