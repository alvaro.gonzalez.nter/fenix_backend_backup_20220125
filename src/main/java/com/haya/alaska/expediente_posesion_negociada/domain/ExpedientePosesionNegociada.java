package com.haya.alaska.expediente_posesion_negociada.domain;


import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.comentario_expediente_posesion_negociada.domain.ComentarioExpedientePosesionNegociada;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estado_expediente_posesion_negociada.domain.EstadoExpedientePosesionNegociada;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.origen_expediente_posesion_negociada.domain.OrigenExpedientePosesionNegociada;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Audited
@AuditTable(value = "HIST_MSTR_EXPEDIENTE_POSESION_NEGOCIADA")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_EXPEDIENTE_POSESION_NEGOCIADA")
public class ExpedientePosesionNegociada implements Serializable  {


  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "ID_ORIGEN_EXCEL")
  private String idOrigen;
  @Column(name = "DES_ID_CONCATENADO", unique = true)
  private String idConcatenado;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE")
  @NotAudited
  private Set<AsignacionExpedientePosesionNegociada> asignaciones = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXP_POS_NEGOCIADA")
  @NotAudited
  private Set<AcuerdoPago> acuerdosPago = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXP_POS_NEGOCIADA")
  @NotAudited
  private Set<Propuesta> propuestas = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXP_POS_NEGOCIADA")
  @NotAudited
  private Set<Estimacion> estimaciones = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE_RECUPERACION_AMISTOSA")
  private Expediente expediente;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ORIGEN")
  private OrigenExpedientePosesionNegociada origen;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private Contrato contrato;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "expedientePosesionNegociada")
  private Set<BienPosesionNegociada> bienesPosesionNegociada  = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO")
  private EstadoExpedientePosesionNegociada estadoExpedienteRecuperacionAmistosa;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "expediente")
  private Set<ComentarioExpedientePosesionNegociada> comentariosPosesionNegociada  = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ESTADO")
  private TipoEstado tipoEstado;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_SUBESTADO")
  private SubtipoEstado subEstado;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CIERRE")
  private Date fechaCierre;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CREACION")
  private Date fechaCreacion;

  @Column(name="RiesgoReputacional")
  Boolean riesgoReputacional; // Este campo puede ser null para indicar que no se ha establecido si tiene riesgo.

  @Transient
  public Double importeBien;

  public Usuario getGestor() {
    return this.getUsuarioByPerfil("Gestor");
  }

  public String getUsuariosByPerfil(String perfil) {
    List<AsignacionExpedientePosesionNegociada> asignaciones = new ArrayList<>();
    for (AsignacionExpedientePosesionNegociada asignacion : this.asignaciones) {
      if (Boolean.TRUE.equals(asignacion.getActivo())) {
        Perfil perfilUsuario = asignacion.getUsuario().getPerfil();
        if (perfilUsuario != null && perfilUsuario.getNombre().equals(perfil)) {
          asignaciones.add(asignacion);
        }
      }
    }
    if(asignaciones != null && asignaciones.size() != 0) {
      String nombresConcatenados = asignaciones.stream().map(asignacionExpediente->asignacionExpediente.getUsuario().getNombre()).collect(Collectors.joining(", "));
      return nombresConcatenados;
    } else {
      return null;
    }
  }

  public Usuario getUsuarioByPerfil(String perfil) {
    List<AsignacionExpedientePosesionNegociada> asignaciones = new ArrayList<>();
    for (AsignacionExpedientePosesionNegociada asignacion : this.asignaciones) {
      if (Boolean.TRUE.equals(asignacion.getActivo())) {
        Perfil perfilUsuario = asignacion.getUsuario().getPerfil();
        if (perfilUsuario != null && perfilUsuario.getNombre().equals(perfil)) {
          asignaciones.add(asignacion);
        }
      }
    }

    AsignacionExpedientePosesionNegociada ae = asignaciones.stream().max(Comparator.comparing(
      AsignacionExpedientePosesionNegociada::getId)).orElse(new AsignacionExpedientePosesionNegociada());

    return ae.getUsuario();
  }

  public double calculaImporteBien() {
    double total = 0;
    for (BienPosesionNegociada bien : bienesPosesionNegociada) {
      Double importe = bien.getBien().getImporteBien();// getSaldoValoracion();
      if (importe != null) {
        total += importe;
      }
    }
    return total;
  }

  /*
   * Cálculo del saldo en gestión de un expediente: Sumatorio del capital pendiente de vencer más
   * saldo en impago de los contratos que conforman el expediente. Información extraída del SALDO
   * del contrato (literal del funcional)
   * Si tenemos calculado el saldo en gestión para el saldo de un contrato, obtenemos este saldo en gestión.
   * En caso contrario, comprobamos los valores de capital pendiente de vencer y principal vencido impagado
   */
  public double getSaldoGestion() {
    double total = 0;
    Contrato contrato = this.contrato;
    if(contrato!=null) {
      Saldo saldo = contrato.getSaldoContrato();
      if (saldo != null) {
        if (saldo.getSaldoGestion() != null) {
          total += saldo.getSaldoGestion();
        } else {
          if (saldo.getCapitalPendiente() != null) {
            total += saldo.getCapitalPendiente();
          }
          if (saldo.getPrincipalVencidoImpagado() != null) {
            total += saldo.getPrincipalVencidoImpagado();
          }
        }
      }
    }
    return total;
  }


  /**
   * Cálculo del importe recuperado de un expediente: Sumatorio de todos los movimientos de cobro
   * (movimientos de tipo abono) de todos los contratos que conforman el expediente. (literal del
   * funcional)
   */
  public double getImporteRecuperado() {
    double total = 0;
    Contrato contrato = this.contrato;
    if(contrato!=null) {
      for (Movimiento movimiento : contrato.getMovimientos()) {
        if (movimiento.getTipo() != null && movimiento.getTipo().getCodigo().equals("2")) {
          if (movimiento.getImporte() != null && movimiento.getActivo() && movimiento.getAfecta() != null && movimiento.getAfecta()) {
            total += movimiento.getImporte();
          }
        }
      }
    }
    return total;
  }

  public double getPrincipalPendiente() {
    double total = 0;
    Contrato contrato = this.contrato;
    if(contrato!=null) {
      Saldo saldo = contrato.getSaldoContrato();
      if (saldo != null) {
        if (saldo.getPrincipalVencidoImpagado() != null) {
          total += saldo.getPrincipalVencidoImpagado();
        }
      }
    }
    return total;
  }


  /*
  public void setBienPosesionNegociada(Set<BienPosesionNegociada> bienesPosesionNegociada )
  {
    for (var bienPosesionNegociada : bienesPosesionNegociada) {
      bienPosesionNegociada.setExpedientePosesionNegociada(this);
    }
    this.bienesPosesionNegociada=bienesPosesionNegociada;
  }*/
}
