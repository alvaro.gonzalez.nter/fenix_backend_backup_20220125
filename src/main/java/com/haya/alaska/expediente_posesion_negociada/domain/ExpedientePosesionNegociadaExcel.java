package com.haya.alaska.expediente_posesion_negociada.domain;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.comentario_expediente_posesion_negociada.domain.ComentarioExpedientePosesionNegociada;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.grupo_cliente.domain.GrupoCliente;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;
import java.util.stream.Collectors;

public class ExpedientePosesionNegociadaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  private Evento alerta;
  private Evento accion;
  private Evento actividad;

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Origin Connection");
      cabeceras.add("Customer");
      cabeceras.add("Customer group");
      cabeceras.add("Portfolio");
      cabeceras.add("First holder");
      cabeceras.add("Portfolio Manager");
      cabeceras.add("Supervisor");
      cabeceras.add("Management area");
      cabeceras.add("Connection manager");
      cabeceras.add("Formalization Manager");
      cabeceras.add("Skip tracing manager");
      cabeceras.add("Origin");
      cabeceras.add("Status");
      cabeceras.add("Owner/Awardee");
      cabeceras.add("debt");
      cabeceras.add("Value");
      cabeceras.add("Active");
      cabeceras.add("Number collaterals");
      cabeceras.add("Number occupants");
      cabeceras.add("Number minors");
      cabeceras.add("Number disabled");
      cabeceras.add("Usual residence");
      cabeceras.add("Household income");
      cabeceras.add("Reputational risk");
      cabeceras.add("Proposal");
      cabeceras.add("Proposed status");
      cabeceras.add("Situation");
      cabeceras.add("Comment");
      cabeceras.add("Action");
      cabeceras.add("Action date");
      cabeceras.add("Alert");
      cabeceras.add("Alert Date");
      cabeceras.add("Activity");
      cabeceras.add("Date of Activity");
      cabeceras.add("Judicial");
      cabeceras.add("Procedure Type");
      cabeceras.add("Phase");
      cabeceras.add("Date");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Expediente Origen");
      cabeceras.add("Cliente");
      cabeceras.add("Grupo Cliente");
      cabeceras.add("Cartera");
      cabeceras.add("Primer titular");
      cabeceras.add("Responsable Cartera");
      cabeceras.add("Supervisor");
      cabeceras.add("Área de gestión");
      cabeceras.add("Gestor del expediente");
      cabeceras.add("Gestor de formalización");
      cabeceras.add("Gestor de skip tracing");
      cabeceras.add("Origen");
      cabeceras.add("Estado");
      cabeceras.add("Propietario/Adjudicatario");
      cabeceras.add("Deuda");
      cabeceras.add("Valor");
      cabeceras.add("Activo");
      cabeceras.add("Número colaterales");
      cabeceras.add("Número Ocupantes");
      cabeceras.add("Número menores");
      cabeceras.add("Número discapacitados");
      cabeceras.add("Residencia habitual");
      cabeceras.add("Ingresos unidad familiar");
      cabeceras.add("Riesgo Reputacional");
      cabeceras.add("Propuesta");
      cabeceras.add("Estado Propuesta");
      cabeceras.add("Situación");
      cabeceras.add("Comentario");
      cabeceras.add("Acción");
      cabeceras.add("Fecha Acción");
      cabeceras.add("Alerta");
      cabeceras.add("Fecha Alerta");
      cabeceras.add("Actividad");
      cabeceras.add("Fecha Actividad");
      cabeceras.add("Judicial");
      cabeceras.add("Tipo Procedimiento");
      cabeceras.add("Fase");
      cabeceras.add("Fecha");
    }
  }

  public ExpedientePosesionNegociadaExcel(
      ExpedientePosesionNegociada expedientePosesionNegociada,
      Evento alerta,
      Evento accion,
      Evento actividad) {
    this.alerta = alerta;
    this.accion = accion;
    this.actividad = actividad;

    Expediente expediente = expedientePosesionNegociada.getExpediente();
    Cartera cartera = expedientePosesionNegociada.getCartera();
    List<Cliente> clientes = new ArrayList<>();
    List<GrupoCliente> grupoClientes = new ArrayList<>();
    if (cartera != null) {
      clientes =
          cartera.getClientes().stream()
              .map(ClienteCartera::getCliente)
              .collect(Collectors.toList());
      grupoClientes = clientes.stream().map(Cliente::getGrupo).collect(Collectors.toList());
    }

    Usuario supervisor = expedientePosesionNegociada.getUsuarioByPerfil("Supervisor");
    Usuario responsableCartera =
        expedientePosesionNegociada.getUsuarioByPerfil("Responsable de cartera");
    Usuario gestor = expedientePosesionNegociada.getUsuarioByPerfil("Gestor");
    // Cálculo de número de Ocupantes, cálculo de número de Colaterales, cálculo de número de
    // ocupantes menores de edad
    Set<Ocupante> listaOcupantes = new HashSet<>();
    Set<BienPosesionNegociada> listaBienPosesionNegociada = new HashSet<>();
    Set<Ocupante> listadoOcupantesMenoresEdad = new HashSet<>();
    Set<Ocupante> listadoOcupanteDiscapacitado = new HashSet<>();

    List<ProcedimientoPosesionNegociada> listadoProcedimientos = new ArrayList<>();
    Date fechaprocedimiento = null;
    TipoProcedimiento tipoProcedimiento = null;
    Integer ocupanteResidenciaHabitual = 0;
    for (BienPosesionNegociada bienPosesionNegociada :
        expedientePosesionNegociada.getBienesPosesionNegociada()) {
      listaBienPosesionNegociada.add(bienPosesionNegociada);

      // Calcular fechaPropuesta
      if (bienPosesionNegociada.getProcedimientos() != null
          && !bienPosesionNegociada.getProcedimientos().isEmpty()) {
        listadoProcedimientos =
            bienPosesionNegociada.getProcedimientos().stream().collect(Collectors.toList());

        Date fechanueva = listadoProcedimientos.get(0).getFechaPresentacionDemanda();

        for (ProcedimientoPosesionNegociada procedimientoPosesionNegociada :
            listadoProcedimientos) {
          if (procedimientoPosesionNegociada.getFechaPresentacionDemanda() != null) {
            if (fechanueva == null) {
              fechanueva = procedimientoPosesionNegociada.getFechaPresentacionDemanda();
            }
            if (procedimientoPosesionNegociada.getFechaPresentacionDemanda().after(fechanueva)) {
              tipoProcedimiento = procedimientoPosesionNegociada.getTipo();
              fechaprocedimiento = procedimientoPosesionNegociada.getFechaPresentacionDemanda();
            }
          }
        }
      }
      String arearGestion = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          arearGestion += au.getNombre() + ", ";
        }
      }

      List<BienPosesionNegociada> bienesPosesionesNegociadas =
          new ArrayList<>(expedientePosesionNegociada.getBienesPosesionNegociada());
      List<Bien> bienes =
          bienesPosesionesNegociadas.stream()
              .map(BienPosesionNegociada::getBien)
              .distinct()
              .collect(Collectors.toList());
      List<Ocupante> ocupantes = new ArrayList<>();
      for (BienPosesionNegociada bpn : bienesPosesionesNegociadas) {
        ocupantes.addAll(
            bpn.getOcupantes().stream()
                .map(OcupanteBien::getOcupante)
                .distinct()
                .collect(Collectors.toList()));
      }

      Double valor = 0.0;
      for (Bien b : bienes) {
        if (b != null && b.getImporteBien() != null) valor += b.getImporteBien();
      }

      Integer nMenores = 0;
      Integer nDiscapacitados = 0;
      Integer nResidenciaHab = 0;
      Double ingresosNetosMensuales = 0.0;
      for (Ocupante o : ocupantes) {
        if (o.getDiscapacitado() != null && o.getDiscapacitado()) nDiscapacitados++;
        if (o.getMenorEdad() != null && o.getMenorEdad()) nMenores++;
        if (o.getResidenciaHabitual() != null && o.getResidenciaHabitual()) nResidenciaHab++;
        if (o.getIngresosNetosMensuales() != null)
          ingresosNetosMensuales += o.getIngresosNetosMensuales();
      }

      List<Propuesta> propuestas = new ArrayList<>(expedientePosesionNegociada.getPropuestas());
      EstadoPropuesta estadoPropuesta = null;
      if (!propuestas.isEmpty()) {
        Date fecha = propuestas.get(0).getFechaAlta();
        for (Propuesta propuesta : propuestas) {
          estadoPropuesta = propuesta.getEstadoPropuesta();
          if (propuesta.getFechaAlta().after(fecha)) {
            estadoPropuesta = propuesta.getEstadoPropuesta();
            fecha = propuesta.getFechaAlta();
          }
        }
      }

      List<ComentarioExpedientePosesionNegociada> comentarioExpedientePosesionNegociadas =
          new ArrayList<>(expedientePosesionNegociada.getComentariosPosesionNegociada());
      ComentarioExpedientePosesionNegociada comentarioExpedientePosesionNegociada = null;
      if (!comentarioExpedientePosesionNegociadas.isEmpty()) {
        comentarioExpedientePosesionNegociada =
            comentarioExpedientePosesionNegociadas.get(
                comentarioExpedientePosesionNegociadas.size() - 1);
      }
      List<ProcedimientoPosesionNegociada> procedimientoPosesionNegociadaList = new ArrayList<>();
      for (BienPosesionNegociada bpn : bienesPosesionesNegociadas) {
        procedimientoPosesionNegociadaList.addAll(bpn.getProcedimientos());
      }

      String procedimientojudicial = "";
      if (listadoProcedimientos.size() > 0) {
        procedimientojudicial = "Si";
      } else {
        procedimientojudicial = "No";
      }

      if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
        this.add("Connection Id", expedientePosesionNegociada.getIdConcatenado());
        this.add(
            "Portfolio Manager",
            responsableCartera != null ? responsableCartera.getNombre() : null);
        this.add("Origin Connection", expediente != null ? expediente.getIdOrigen() : null);
        this.add(
            "Customer",
            clientes.stream().map(Cliente::getNombre).collect(Collectors.joining(", ")));
        this.add(
            "Customer group",
            grupoClientes.stream().map(GrupoCliente::getNombre).collect(Collectors.joining(", ")));
        this.add("Portfolio", cartera != null ? cartera.getNombre() : null);
        this.add(
            "Portfolio Manager",
            responsableCartera != null ? responsableCartera.getNombre() : null);
        this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
        this.add("Management area", arearGestion);
        this.add("Connection manager", gestor != null ? gestor.getNombre() : null);
        this.add(
            "Origin",
            expedientePosesionNegociada.getOrigen() != null
                ? expedientePosesionNegociada.getOrigen().getValorIngles()
                : null);
        this.add(
            "Status",
            expedientePosesionNegociada.getEstadoExpedienteRecuperacionAmistosa() != null
                ? expedientePosesionNegociada.getEstadoExpedienteRecuperacionAmistosa().getValorIngles()
                : null);
        this.add("Owner/Awardee", clientes.isEmpty() ? null : clientes.get(0).getNombre());
        this.add("debt", expediente != null ? expediente.getSaldoGestion() : null);
        this.add("Value", valor);
        this.add("Active", expedientePosesionNegociada.getActivo() ? "SI" : "NO");
        this.add("Number collaterals", bienesPosesionesNegociadas.size());
        this.add("Number occupants", ocupantes.size());
        this.add("Number minors", nMenores);
        this.add("Number disabled", nDiscapacitados);
        this.add("Usual residence", nResidenciaHab);
        this.add("Household income", ingresosNetosMensuales);
        if (expedientePosesionNegociada.getActivo() != null)
          this.add("Status", expedientePosesionNegociada.getActivo() ? "YES" : "No");
        if (expedientePosesionNegociada.getRiesgoReputacional() != null)
          this.add(
              "Reputational risk",
              expedientePosesionNegociada.getRiesgoReputacional() ? "YES" : "No");
        this.add("Proposal", propuestas != null ? "YES" : "No");
        this.add("Proposed status", estadoPropuesta);
        this.add(
            "Comment",
            comentarioExpedientePosesionNegociada != null
                ? comentarioExpedientePosesionNegociada.getComentario()
                : "");
        this.add("Action", accion != null ? accion.getTitulo() : null);
        this.add("Action date", accion != null ? accion.getFechaCreacion() : null);
        this.add("Alert", alerta != null ? alerta.getTitulo() : null);
        this.add("Alert Date", alerta != null ? alerta.getFechaCreacion() : null);
        this.add("Activity", actividad != null ? actividad.getTitulo() : null);
        this.add("Date of Activity", actividad != null ? actividad.getFechaCreacion() : null);
        this.add(
            "Judicial",
            procedimientojudicial); // Lo mismo si tiene o no ProcedimientoPosesionNegociada
        this.add(
          "Procedure Type",
          tipoProcedimiento != null
            ? tipoProcedimiento.getValorIngles()
            : null); // Comprobar si presenta procedimiento SI/NO
        this.add("Date", fechaprocedimiento);

        bienes = new ArrayList();
        if (expedientePosesionNegociada.getBienesPosesionNegociada() != null) {
          for (var bienPN : expedientePosesionNegociada.getBienesPosesionNegociada()) {
            bienes.add(bienPN.getBien());
          }
        }
        Iterator<Bien> bienIterator = bienes.iterator();
        while (bienIterator.hasNext()) {
          Bien bienProv = bienIterator.next();
          if (bienProv.getImporteBien() == null) {
            bienIterator.remove();
          }
        }
        List<Bien> bienesOrdenados =
          bienes.stream()
            .sorted(Comparator.comparingDouble(Bien::getImporteBien).reversed())
            .collect(Collectors.toList());

        Boolean obtenido = false;
        Boolean obtenidoSituacion = false;
        for (var bienSacado : bienesOrdenados) {
          for (var bienPNSacado : expedientePosesionNegociada.getBienesPosesionNegociada()) {
            if (obtenido == false && bienPNSacado.getBien().equals(bienSacado)) {
              for (var ocupanteSacado : bienPNSacado.getOcupantes()) {
                if (obtenido == false && ocupanteSacado.getTipoOcupante() != null) {
                  if (ocupanteSacado.getTipoOcupante().getCodigo().equals("TIT")) {
                    if (ocupanteSacado.getOcupante().getNombreCompleto() != null) {
                      this.add("Primer titular", ocupanteSacado.getOcupante().getNombreCompleto());
                      obtenido = true;
                    }
                  }
                }
                if (obtenidoSituacion == false && bienPNSacado.getBien().equals(bienSacado)) {
                  if (bienPNSacado.getSituacion() != null
                    && bienPNSacado.getSituacion().getValorIngles() != null) {
                    this.add("Situación", bienPNSacado.getSituacion().getValorIngles());
                    obtenidoSituacion = true;
                  }
                }
              }
            }
          }
        }
        this.add(
          "Formalization Manager",
          expedientePosesionNegociada.getUsuarioByPerfil("Formalization Manager") != null
            && expedientePosesionNegociada
            .getUsuarioByPerfil("Formalization Manager")
            .getNombre()
            != null
            ? expedientePosesionNegociada.getUsuarioByPerfil("Formalization Manager").getNombre()
            : null);
        this.add(
          "Skip tracing manager",
          expedientePosesionNegociada.getUsuarioByPerfil("Skip tracing manager") != null
            && expedientePosesionNegociada
            .getUsuarioByPerfil("Skip tracing manager")
            .getNombre()
            != null
            ? expedientePosesionNegociada.getUsuarioByPerfil("Skip tracing manager").getNombre()
            : null);



      } else {
        this.add("Id Expediente", expedientePosesionNegociada.getIdConcatenado());
        this.add(
            "Responsable Cartera",
            responsableCartera != null ? responsableCartera.getNombre() : null);
        this.add("Expediente Origen", expediente != null ? expediente.getIdOrigen() : null);
        this.add(
            "Cliente", clientes.stream().map(Cliente::getNombre).collect(Collectors.joining(", ")));
        this.add(
            "Grupo Cliente",
            grupoClientes.stream().map(GrupoCliente::getNombre).collect(Collectors.joining(", ")));
        this.add("Cartera", cartera != null ? cartera.getNombre() : null);
        this.add(
            "Responsable Cartera",
            responsableCartera != null ? responsableCartera.getNombre() : null);
        this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
        this.add("Área de gestión", arearGestion);
        this.add("Gestor del expediente", gestor != null ? gestor.getNombre() : null);
        this.add(
            "Origen",
            expedientePosesionNegociada.getOrigen() != null
                ? expedientePosesionNegociada.getOrigen().getValor()
                : null);
        this.add(
            "Estado",
            expedientePosesionNegociada.getEstadoExpedienteRecuperacionAmistosa() != null
                ? expedientePosesionNegociada.getEstadoExpedienteRecuperacionAmistosa().getValor()
                : null);
        this.add(
            "Propietario/Adjudicatario", clientes.isEmpty() ? null : clientes.get(0).getNombre());
        this.add("Deuda", expediente != null ? expediente.getSaldoGestion() : null);
        this.add("Valor", valor);
        this.add("Activo", expedientePosesionNegociada.getActivo() ? "SI" : "NO");
        this.add("Número colaterales", bienesPosesionesNegociadas.size());
        this.add("Número Ocupantes", ocupantes.size());
        this.add("Número menores", nMenores);
        this.add("Número discapacitados", nDiscapacitados);
        this.add("Residencia habitual", nResidenciaHab);
        this.add("Ingresos unidad familiar", ingresosNetosMensuales);
        if (expedientePosesionNegociada.getActivo() != null)
          this.add("Estado", expedientePosesionNegociada.getActivo() ? "Si" : "No");
        if (expedientePosesionNegociada.getRiesgoReputacional() != null)
          this.add(
              "Riesgo Reputacional",
              expedientePosesionNegociada.getRiesgoReputacional() ? "Si" : "No");
        this.add("Propuesta", propuestas != null ? "Si" : "No");
        this.add("Estado Propuesta", estadoPropuesta);
        this.add(
            "Comentario",
            comentarioExpedientePosesionNegociada != null
                ? comentarioExpedientePosesionNegociada.getComentario()
                : "");
        this.add("Acción", accion != null ? accion.getTitulo() : null);
        this.add("Fecha Acción", accion != null ? accion.getFechaCreacion() : null);
        this.add("Alerta", alerta != null ? alerta.getTitulo() : null);
        this.add("Fecha Alerta", alerta != null ? alerta.getFechaCreacion() : null);
        this.add("Actividad", actividad != null ? actividad.getTitulo() : null);
        this.add("Fecha Actividad", actividad != null ? actividad.getFechaCreacion() : null);
        this.add(
            "Judicial",
            procedimientojudicial); // Lo mismo si tiene o no ProcedimientoPosesionNegociada
        this.add(
            "Tipo Procedimiento",
            tipoProcedimiento != null
                ? tipoProcedimiento.getValor()
                : null); // Comprobar si presenta procedimiento SI/NO
        this.add("Fecha", fechaprocedimiento);

        bienes = new ArrayList();
        if (expedientePosesionNegociada.getBienesPosesionNegociada() != null) {
          for (var bienPN : expedientePosesionNegociada.getBienesPosesionNegociada()) {
            bienes.add(bienPN.getBien());
          }
        }
        Iterator<Bien> bienIterator = bienes.iterator();
        while (bienIterator.hasNext()) {
          Bien bienProv = bienIterator.next();
          if (bienProv.getImporteBien() == null) {
            bienIterator.remove();
          }
        }

        List<Bien> bienesOrdenados =
            bienes.stream()
                .sorted(Comparator.comparingDouble(Bien::getImporteBien).reversed())
                .collect(Collectors.toList());

        Boolean obtenido = false;
        Boolean obtenidoSituacion = false;
        for (var bienSacado : bienesOrdenados) {
          for (var bienPNSacado : expedientePosesionNegociada.getBienesPosesionNegociada()) {
            if (obtenido == false && bienPNSacado.getBien().equals(bienSacado)) {
              for (var ocupanteSacado : bienPNSacado.getOcupantes()) {
                if (obtenido == false && ocupanteSacado.getTipoOcupante() != null) {
                  if (ocupanteSacado.getTipoOcupante().getCodigo().equals("TIT")) {
                    if (ocupanteSacado.getOcupante().getNombreCompleto() != null) {
                      this.add("Primer titular", ocupanteSacado.getOcupante().getNombreCompleto());
                      obtenido = true;
                    }
                  }
                }
                if (obtenidoSituacion == false && bienPNSacado.getBien().equals(bienSacado)) {
                  if (bienPNSacado.getSituacion() != null
                      && bienPNSacado.getSituacion().getValor() != null) {
                    this.add("Situación", bienPNSacado.getSituacion().getValor());
                    obtenidoSituacion = true;
                  }
                }
              }
            }
          }
        }
        this.add(
            "Gestor de formalización",
            expedientePosesionNegociada.getUsuarioByPerfil("Gestor formalización") != null
                    && expedientePosesionNegociada
                            .getUsuarioByPerfil("Gestor formalización")
                            .getNombre()
                        != null
                ? expedientePosesionNegociada.getUsuarioByPerfil("Gestor formalización").getNombre()
                : null);

        this.add(
            "Gestor de skip tracing",
            expedientePosesionNegociada.getUsuarioByPerfil("Gestor skip tracing") != null
                    && expedientePosesionNegociada
                            .getUsuarioByPerfil("Gestor skip tracing")
                            .getNombre()
                        != null
                ? expedientePosesionNegociada.getUsuarioByPerfil("Gestor skip tracing").getNombre()
                : null);
      }
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
