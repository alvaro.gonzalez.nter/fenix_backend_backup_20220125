package com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto;

import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaInputDto;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteInputDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ExpedientePosesionNegociadaInputDTO {
  Integer idCartera;
  String carteraRem;
  String estadoExpedienteRecuperacionAmistosa;
  Boolean riesgoReputicional;

  List<BienPosesionNegociadaInputDto> bienes;
  List<OcupanteInputDto> ocupantes;
  List<String> comentarios;
}
