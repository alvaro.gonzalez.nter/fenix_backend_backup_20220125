package com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ExpedientePNSegmentacionDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  //private Integer id;
  private String idConcatenado;


  private Double valorBienes; //suma del importe ¿valor garantizado? de todos los bienes que lo componen
  private String provincia; //del bien ppal
  private String localidad; //del bien ppal
  private String tipoProcedimiento; //del bien ppal
  //private String hitoJudicial;
  private String estadoOcupacion; //estado ocupacion del bien principal
  private Boolean cargasPosteriores; // si tiene cargas posteriores el bien ppal
  private String estrategia; //estrategia que tiene bien ppal
  private Boolean rankingAgencia; //ranking de la agencia para el reparto de expedientes

}
