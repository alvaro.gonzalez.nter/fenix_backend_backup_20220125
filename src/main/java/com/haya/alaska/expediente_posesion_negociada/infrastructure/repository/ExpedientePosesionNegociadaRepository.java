package com.haya.alaska.expediente_posesion_negociada.infrastructure.repository;

import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public interface ExpedientePosesionNegociadaRepository extends JpaRepository<ExpedientePosesionNegociada, Integer> {

  Optional<ExpedientePosesionNegociada> findByIdOrigen(String stringValue);
  Optional<ExpedientePosesionNegociada> findByIdConcatenado(String concatenado);
  List<ExpedientePosesionNegociada> findByIdIn(List<Integer> ids);
  Set<ExpedientePosesionNegociada> findByAsignacionesIsEmpty();
  List<ExpedientePosesionNegociada> findByIdConcatenadoIn(List<String> ids);
  List<ExpedientePosesionNegociada> findAllByCarteraIdAndTipoEstadoId(Integer carteraId, Integer tipoEstadoId);
  List<ExpedientePosesionNegociada> findAllByCarteraId(Integer carteraId);
  List<ExpedientePosesionNegociada> findAllByAsignacionesUsuarioId(Integer usuarioId);
  List<ExpedientePosesionNegociada> findAllByCarteraIdAndAsignacionesUsuarioId(Integer carteraId, Integer usuarioId);
  List<ExpedientePosesionNegociada> findAllByIdConcatenadoIsNull();
  Page<ExpedientePosesionNegociada> findAllByCarteraId(Integer carteraId, Pageable pageable);
  Page<ExpedientePosesionNegociada> findAllByCarteraIdAndAsignacionesUsuarioId(Integer carteraId, Integer usuarioId, Pageable pageable);

  @Query(
    value =
      "SELECT rh.timestamp AS fechaAsignacion, u.ID AS gestorExpediente, u2.DES_NOMBRE AS usuarioAsignaciones\n"
        + "FROM HIST_RELA_ASIGNACION_EXPEDIENTE_POSESION_NEGOCIADA aeh\n"
        + "         left Join REGISTRO_HISTORICO rh ON aeh.REV = rh.id\n"
        + "         left Join MSTR_USUARIO u ON aeh.ID_USUARIO = u.ID\n"
        + "         left Join MSTR_USUARIO u2 ON aeh.ID_USUARIO_ASIGNACION = u2.ID\n"
        + "WHERE aeh.ID_EXPEDIENTE = ?1 and u.ID_PERFIL = 1\n"
        + "ORDER BY rh.timestamp DESC",
    nativeQuery = true)
  List<Map> historicoAsignacionGestores(Integer expediente);
  @Query(
    value =
      "select * from MSTR_EXPEDIENTE_POSESION_NEGOCIADA me where me.ID not in ( " +
        "select me.ID from MSTR_EXPEDIENTE_POSESION_NEGOCIADA me " +
        "inner join RELA_ASIGNACION_EXPEDIENTE_POSESION_NEGOCIADA raepn on raepn.ID_EXPEDIENTE = me.ID " +
        "inner join MSTR_USUARIO mu on raepn.ID_USUARIO = mu.ID " +
        "inner join MSTR_PERFIL mp on mu.ID_PERFIL = mp.ID " +
        "where mp.DES_NOMBRE = 'Gestor' " +
        "and me.ID_CARTERA  IN(:idCartera)) ",
    nativeQuery = true)
  List<ExpedientePosesionNegociada> getExpedientesPNSinGestor(Integer idCartera);

  //Métodos para segmentacion
  //Situacion
  @Query(
    value = "SELECT DISTINCT E.ID " +
      "FROM MSTR_EXPEDIENTE_POSESION_NEGOCIADA AS E " +
      "LEFT JOIN MSTR_BIEN_POSESION_NEGOCIADA AS C ON C.ID_EXPEDIENTE_POSESION_NEGOCIADA = E.ID " +
      "WHERE C.ID_SITUACION = ?2 " +
      "AND E.ID_CARTERA = ?1 ;",
    nativeQuery = true
  )
  List<Integer> findAllDistinctIdByCarteraIdAndBienesPosesionNegociadaSituacionId(Integer idCartera, Integer idSituacion);
  //Modo
  @Query(
    value = "SELECT DISTINCT E.ID " +
      "FROM MSTR_EXPEDIENTE_POSESION_NEGOCIADA AS E " +
      "WHERE E.ID_TIPO_ESTADO = ?2 " +
      "AND E.ID_CARTERA = ?1 ;",
    nativeQuery = true
  )
  List<Integer> findAllDistinctIdByCarteraIdAndTipoEstadoId(Integer carteraId, Integer tipoEstadoId);
  //Producto No Tiene
  //Judicial
  @Query(
      value =
          "SELECT DISTINCT E.ID "
              + "FROM MSTR_EXPEDIENTE_POSESION_NEGOCIADA AS E "
              + "LEFT JOIN MSTR_BIEN_POSESION_NEGOCIADA AS C ON C.ID_EXPEDIENTE_POSESION_NEGOCIADA = E.ID "
              + "WHERE C.ID IN ( "
              + "    SELECT C1.ID "
              + "    FROM MSTR_BIEN_POSESION_NEGOCIADA AS C1 "
              + "    LEFT JOIN MSTR_PROCEDIMIENTO_POSESION_NEGOCIADA AS R ON C1.ID = R.ID_BIEN "
              + "    GROUP BY C1.ID "
              + "    HAVING COUNT(R.ID) = 0 "
              + ") AND E.ID_CARTERA = ?1 ;",
      nativeQuery = true)
  List<Integer> findAllByCountProcedimientosZero(Integer idCartera);
  @Query(
      value =
          "SELECT DISTINCT E.ID "
              + "FROM MSTR_EXPEDIENTE_POSESION_NEGOCIADA AS E "
              + "LEFT JOIN MSTR_BIEN_POSESION_NEGOCIADA AS C ON C.ID_EXPEDIENTE_POSESION_NEGOCIADA = E.ID "
              + "WHERE C.ID IN ( "
              + "    SELECT C1.ID "
              + "    FROM MSTR_BIEN_POSESION_NEGOCIADA AS C1 "
              + "    LEFT JOIN MSTR_PROCEDIMIENTO_POSESION_NEGOCIADA AS R ON C1.ID = R.ID_BIEN "
              + "    GROUP BY C1.ID "
              + "    HAVING COUNT(R.ID) > 0 "
              + ") AND E.ID_CARTERA = ?1 ;",
      nativeQuery = true)
  List<Integer> findAllByCountProcedimientosMore(Integer idCartera);
}
