package com.haya.alaska.expediente_posesion_negociada.infrastructure.controller;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.expediente.infrastructure.controller.dto.AsignacionGestoresDTO;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCase;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaExcelUseCase;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaInputDTO;
import com.haya.alaska.ocupante.application.OcupanteUseCase;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteDTOToList;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.websocket.AuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("api/v1/expedientePosesionNegociada")
@Slf4j
public class ExpedientePosesionNegociadaController {
  @Autowired ExpedientePosesionNegociadaCase expedientePosesionNegociadaCase;
  @Autowired
  OcupanteUseCase ocupanteUseCase;

  @Autowired
  private ExpedientePosesionNegociadaExcelUseCase expedientePosesionNegociadaExcelUseCase;

  @ApiOperation(value = "Crear una posesión negociada partiendo de unos bienes de un expediente de negociación amistosa existente",
    notes = "Todos los bienes perteneceran a un único contrato de un expediente de negociación amistos")
  @PostMapping("addRecuperacionAmistosa/{idContrato}")
  //@Transactional(rollbackFor = Exception.class)
  public void addExpedienteSobreRecuperacionAmistosa(@PathVariable int idContrato,@RequestBody List<Integer> idContratoBienes,
                                                     @ApiIgnore CustomUserDetails principal) throws Exception
  {
    log.debug("En addexpediente recuperación amistosa {}",idContrato );
    Usuario usuario = (Usuario) principal.getPrincipal();
    ExpedientePosesionNegociadaDTO output = expedientePosesionNegociadaCase.addExpedienteSobreRecuperacionAmistosa(idContrato,idContratoBienes,usuario);
    expedientePosesionNegociadaCase.generateConcatenado(output.getId());
  }

  @ApiOperation(
    value = "Listado expedientes de posesion negociada",
    notes = "Devuelve todos los expdientes filtrados que tengan propuestas asociadas")
  @GetMapping("/all-with-propuestas")
  public ListWithCountDTO<ExpedientePosesionNegociadaDTO> getAllExpedientesWithPropuestas(
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {

    Usuario usuario = (Usuario) principal.getPrincipal();
    return expedientePosesionNegociadaCase.getAllExpedientesWithPropuestas(usuario);
  }

  @ApiOperation(
      value = "Listado expedientes filtrados",
      notes = "Devuelve todos los expdientes filtrados")
  @GetMapping
  public ListWithCountDTO<ExpedientePosesionNegociadaDTO> getAllExpedientesFiltered(
      @RequestParam(value = "idConcatenado", required = false) String id,
      @RequestParam(value = "origen", required = false) String origen,
      @RequestParam(value = "carteraTipo", required = false) String carteraTipo,
      @RequestParam(value = "gestor", required = false) String gestor,
      @RequestParam(value = "titular", required = false) String titular,
      @RequestParam(value = "cliente", required = false) String cliente,
      @RequestParam(value = "cartera", required = false) String cartera,
      @RequestParam(value = "saldo", required = false) String saldo,
      @RequestParam(value = "numeroBienes", required = false) Integer numeroBienes,
      @RequestParam(value = "estado", required = false) String estado,
      @RequestParam(value = "direccion", required = false) String direccion,
      @RequestParam(value = "localidad", required = false) String localidad,
      @RequestParam(value = "provincia", required = false) String provincia,
      @RequestParam(value = "orderField", required = false) String orderField,
      @RequestParam(value = "orderDirection", required = false) String orderDirection,
      @RequestParam(value = "size") Integer size,
      @RequestParam(value = "page") Integer page,
      @ApiIgnore CustomUserDetails principal)
    throws NotFoundException, AuthenticationException {

    Usuario usuario = (Usuario) principal.getPrincipal();
    return expedientePosesionNegociadaCase.getAllFilteredExpedientes(
        usuario,
        id,
        origen,
        carteraTipo,
        gestor,
        titular,
        cliente,
        cartera,
        saldo,
        numeroBienes,
        estado,
        orderField,
        orderDirection,
        direccion,
        localidad,
        provincia,
        size,
        page);
  }
  @ApiOperation(value = "Obtener expediente", notes = "Devuelve el expediente con id enviado")
  @GetMapping("/{expedienteId}")
  public ExpedientePosesionNegociadaDTO getExpedienteById(@PathVariable Integer expedienteId)
    throws Exception {
    var expediente= expedientePosesionNegociadaCase.getExpedienteById(expedienteId);
    return expediente;
  }

  @ApiOperation(value = "Crear una posesión negociada ",
    notes = "Crear una posesión negociada a traves de datos introducidos en el frontend")
  @PostMapping("add")
  //@Transactional(rollbackFor = Exception.class)
  public void addExpediente(@RequestBody ExpedientePosesionNegociadaInputDTO posesionNegociadaInputDTO,
                            @ApiIgnore CustomUserDetails principal) throws Exception
  {
    Usuario usuario = (Usuario) principal.getPrincipal();
    ExpedientePosesionNegociadaDTO output = expedientePosesionNegociadaCase.addExpediente(usuario, posesionNegociadaInputDTO);
    expedientePosesionNegociadaCase.generateConcatenado(output.getId());
  }

  @ApiOperation(value = "Crear posesiones negociadas con los datos de la excel tipo Importación-Posesion-Negociada.xlsx",
    notes = "La excel de ejemplo esta en el teams")
  @PostMapping("/carga")
  @Transactional(rollbackFor = Exception.class)
  public void carga(@RequestParam("file") MultipartFile file,
                    @RequestParam(value = "cartera", required = false) Integer cartera,
                    @ApiIgnore CustomUserDetails principal
                  ) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    expedientePosesionNegociadaCase.carga(usuario, file, cartera);
    expedientePosesionNegociadaCase.actualizarPostCarga();
  }

  @ApiOperation(
    value = "Descargar información vinculada a un expediente de posesión negociada",
    notes = "Descargar información en Excel vinculado a un expediente de posesión negociada")
  @GetMapping("/excelnegocio")
  public ResponseEntity<InputStreamResource> getExcelExpedientePosesion(@ApiIgnore CustomUserDetails usuario) throws Exception {
    //  List<Evento>listaeventos= expedientePosesionNegociadaExcelUseCase.findEventos();
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel =
      excelExport.create(
        expedientePosesionNegociadaExcelUseCase.findExcelExpedientePosesionNegociada((Usuario) usuario.getPrincipal()));
    return excelExport.download(
      excel, "expedientes_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(
      value = "Descargar información vinculada a un expediente de posesión negociada",
      notes = "Descargar información en Excel vinculado a un expediente de posesión negociada")
  @GetMapping("/{expedienteId}/excelnegocio")
  public ResponseEntity<InputStreamResource> getExcelExpedientePosesionById(
      @PathVariable Integer expedienteId) throws NotFoundException, IOException {

  //  List<Evento>listaeventos= expedientePosesionNegociadaExcelUseCase.findEventos();

    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel =
        excelExport.create(
            expedientePosesionNegociadaExcelUseCase.findExcelExpedientePosesionNegociadaById(
                expedienteId));


    return excelExport.download(
        excel, "expedientes_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(
    value = "Listado ocupantes by Bienes",
    notes = "Devuelve todas los ocupantes registrados a partir de los bienes.")
  @GetMapping("/{expedienteId}/ocupantes/byBienes")
  public List<OcupanteDTOToList> getOcupantesByBienes(
    @RequestParam("bienes") List<Integer> bienes) {

    return ocupanteUseCase.getOcupantesByBienes(bienes);
  }

  @ApiOperation(
    value = "Actualización masiva de estrategia para posesion negociada y gestion amistosa",
    notes = "Actualizar estrategia de los expedientes y contratos enviados.")
  @PutMapping("/gestionMasiva/masiveEstrategiaTodos")
  public void masiveEstrategiaUpdate(
    @RequestBody(required = false) BusquedaDto busquedaDto,
    @RequestParam(value = "idEstrategia") Integer idEstrategia,
    @RequestParam(value = "idsExpedientes", required = false) List<Integer> idsExpedientes,
    @RequestParam(value = "idsExpedientesPN", required = false) List<Integer> idsExpedientesPN,
    @RequestParam(value = "idsContratos", required = false) List<Integer> idsContratos,
    @RequestParam(value = "idsBienesPN", required = false) List<Integer> idsBienesPN,
    @RequestParam(required = false) boolean todos,
    @RequestParam(required = false) boolean todosPN,
    @ApiIgnore CustomUserDetails principal
  )
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    expedientePosesionNegociadaCase.masiveEstrategiaUpdate(idEstrategia, idsExpedientes, idsExpedientesPN, idsContratos, idsBienesPN, todos, todosPN, usuario, busquedaDto);
  }

  @ApiOperation(
    value = "Actualizar modo de gestión y situacion de manera masiva",
    notes = "Actualizar modo de gestión y situacion de los expedientes con ID enviado")
  @PutMapping("/gestionMasiva/modoGestionYSituacion")
  public void putModoGestionYSituacion(
    @RequestBody(required = false) BusquedaDto busquedaDto,
    @RequestParam(required = false) List<Integer> expedienteIdList,
    @RequestParam(required = false) List<Integer> expedientePNIdList,
    @RequestParam(required = false) Integer estado,
    @RequestParam(required = false) Integer subEstado,
    @RequestParam(required = false) Integer situacion,
    @RequestParam(required = false) Boolean todos,
    @RequestParam(required = false) Boolean todosPN,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    expedientePosesionNegociadaCase.gestionMasiva_ModoGestionYSituacion(
      expedienteIdList, expedientePNIdList, estado, subEstado, situacion, todos, todosPN, busquedaDto, usuario);
  }

  @ApiOperation(
    value = "Actualización masiva de estrategia (sin actualizar)",
    notes = "Actualizar estrategia de los expedientes y contratos enviados.")
  @PutMapping("/gestionMasiva/masiveEstrategia")
  @Transactional(rollbackFor = Exception.class)
  public void masiveEstrategiaUpdatePN(
    @RequestBody(required = false) BusquedaDto busquedaDto,
    @RequestParam(value = "idEstrategia") Integer idEstrategia,
    @RequestParam(value = "idsExpedientes", required = false) List<Integer> idsExpedientes,
    @RequestParam(value = "idsBienesPN", required = false) List<Integer> idsBienesPN,
    @RequestParam(required = false) boolean todos,
    @ApiIgnore CustomUserDetails principal
  )
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    expedientePosesionNegociadaCase.masiveEstrategiaUpdatePN(idEstrategia, idsExpedientes, idsBienesPN, todos, usuario, busquedaDto);
  }

  @ApiOperation(value = "Actualizar modo de gestión",
    notes = "Actualizar modo de gestión del expedientePN con ID enviado")
  @PutMapping("/{expedienteId}/modoGestion")
  @Transactional(rollbackFor = Exception.class)
  public ExpedientePosesionNegociadaDTO modificarModoGestion(@PathVariable Integer expedienteId,
                                                             @RequestParam Integer estado,
                                                             @RequestParam(required = false) Integer subEstado,
                                                             @ApiIgnore CustomUserDetails principal) throws Exception {
    return expedientePosesionNegociadaCase.updateModoGestion(expedienteId, estado, subEstado, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Actualizar id Concatenado",
    notes = "Actualiza el id concatenado de todos los expedientes")
  @PutMapping("/concatenado")
  @Transactional(rollbackFor = Exception.class)
  public void actualizarConcatenados() throws Exception {
    expedientePosesionNegociadaCase.concatenar();
  }

  @ApiOperation(
    value = "Listado expedientes filtrados",
    notes = "Devuelve todos los expdientes filtrados")
  @GetMapping("{expedienteId}/historico-gestores")
  public ListWithCountDTO<AsignacionGestoresDTO> getAllAsignacionGestoresHistorico(
    @PathVariable Integer expedienteId) throws Exception {
    return expedientePosesionNegociadaCase.getAllAsignacionGestoresHistorico(expedienteId);
  }
}
