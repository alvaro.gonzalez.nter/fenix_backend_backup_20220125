package com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto;

import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaInputDto;
import com.haya.alaska.ocupante.domain.Ocupante;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
public class ExpedientePosesionNegociadaOutputDTO  {
  int id;
  String idConcatenado;
  String cliente;
  int numeroBienes;
  String origen;
  String estado;
  double importe;
  List<String> comentarios;
}
