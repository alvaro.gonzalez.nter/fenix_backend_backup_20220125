package com.haya.alaska.expediente_posesion_negociada.infrastructure.mapper;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.comentario_expediente_posesion_negociada.domain.ComentarioExpedientePosesionNegociada;
import com.haya.alaska.estado_expediente_posesion_negociada.domain.EstadoExpedientePosesionNegociada;
import com.haya.alaska.estado_expediente_posesion_negociada.infastructure.repository.EstadoExpedientePosesionNegociadaRepository;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.expediente.infrastructure.controller.dto.AsignacionGestoresDTO;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePNSegmentacionDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaInputDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaOutputDTO;
import com.haya.alaska.grupo_cliente.domain.GrupoCliente;
import com.haya.alaska.origen_expediente_posesion_negociada.domain.OrigenExpedientePosesionNegociada;
import com.haya.alaska.origen_expediente_posesion_negociada.repository.OrigenExpedientePosesionNegociadaRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.plazo.repository.PlazoRepository;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.segmentacion_pn.infrastructure.util.SegmentacionPNUtil;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.subtipo_gestor.domain.SubtipoGestor;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ExpedientePosesionNegociadaMapper {
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  EstrategiaRepository estrategiaRepository;
  @Autowired
  UsuarioRepository usuarioRepository;

  @Autowired
  EstadoExpedientePosesionNegociadaRepository estadoExpedientePosesionNegociadaRepository;

  @Autowired
  OrigenExpedientePosesionNegociadaRepository origenExpedientePosesionNegociadaRepository;

  @Autowired
  PlazoRepository plazoRepository;

  @Autowired
  ExpedienteUtil expedienteUtil;

  @Autowired
  SegmentacionPNUtil segmentacionUtil;

  public ExpedientePosesionNegociadaDTO parse(ExpedientePosesionNegociada expedientePosesionNegociada) {
    Usuario gestor = expedientePosesionNegociada.getUsuarioByPerfil("Gestor");
    SubtipoGestor subtipoGestor = null;

    if (gestor != null) {
      subtipoGestor = gestor.getSubtipoGestor();
    }

    Usuario supervisor = expedientePosesionNegociada.getUsuarioByPerfil("Supervisor");
    Usuario responsableCartera =
      expedientePosesionNegociada.getUsuarioByPerfil("Responsable de cartera");

    Usuario gestorSkipTracing = expedientePosesionNegociada.getUsuarioByPerfil("Gestor skip tracing");
    Usuario gestorFormalizacion = expedientePosesionNegociada.getUsuarioByPerfil("Gestor formalización");
    //Usuario gestorSoporte = expedientePosesionNegociada.getUsuarioByPerfil("Soporte");

    ExpedientePosesionNegociadaDTO expedienteDTO = new ExpedientePosesionNegociadaDTO();
    expedienteDTO.setId(expedientePosesionNegociada.getId());
    expedienteDTO.setIdConcatenado(expedientePosesionNegociada.getIdConcatenado());
    expedienteDTO.setExpedienteCliente(expedientePosesionNegociada.getIdOrigen());

    //Plazo de la cartera
    //sacar el plazo de cartera y ver si existe o no
    Cartera c = carteraRepository.findById(expedientePosesionNegociada.getCartera().getId()).orElse(null);
    String pl = null;

    if (c.getPlazo() != null) {
      pl = c.getPlazo().getValor();
    }

    expedienteDTO.setCarteraPlazo(pl);

    // MODO DE GESTIÓN ¿Sacar a partir de expediente?
    if (expedientePosesionNegociada != null) {
      expedienteDTO.setModoDeGestionEstado(
        expedientePosesionNegociada.getTipoEstado() != null
          ? expedientePosesionNegociada.getTipoEstado().getValor()
          : null);

      String modoGestionEstado = expedientePosesionNegociada.getTipoEstado() != null ? expedientePosesionNegociada.getTipoEstado().getValor() : null;
      String modoGestionSubEstado = expedientePosesionNegociada.getSubEstado() != null ? expedientePosesionNegociada.getSubEstado().getValor() : null;

      if (modoGestionSubEstado != null) {
        expedienteDTO.setModoDeGestionEstado(modoGestionEstado + "/" + modoGestionSubEstado);
      } else {
        expedienteDTO.setModoDeGestionEstado(modoGestionEstado);
      }
    }

    // jUDICIALIZADO PN-BIENES-PROCEDIMIENTOS SI O NO
    Set<BienPosesionNegociada> listaBienPosesionNegociada = new HashSet<>();
    listaBienPosesionNegociada = expedientePosesionNegociada.getBienesPosesionNegociada();
    List<ProcedimientoPosesionNegociada> listaprocedimientos = new ArrayList<>();
    Double valor = 0.0;
    for (BienPosesionNegociada nuevoBien : listaBienPosesionNegociada) {
      nuevoBien.getProcedimientos();
      for (ProcedimientoPosesionNegociada pro : nuevoBien.getProcedimientos()) {
        listaprocedimientos.add(pro);
      }
      if (nuevoBien.getBien().getImporteBien() != null) {
        valor += nuevoBien.getBien().getImporteBien();
      }
    }
    if (listaprocedimientos != null && !listaprocedimientos.isEmpty()) {
      expedienteDTO.setJudicializado("SI");
    } else {
      expedienteDTO.setJudicializado("NO");
    }
    // Valor
    expedienteDTO.setValor(valor);

    // EstadoPropuesta
    EstadoPropuesta ep = expedienteUtil.getEstadoPropuestaPN(expedientePosesionNegociada);
    expedienteDTO.setEstadoPropuesta(ep != null ? new CatalogoMinInfoDTO(ep) : null);

    expedienteDTO.setOrigen(
      expedientePosesionNegociada.getOrigen() != null
        ? expedientePosesionNegociada.getOrigen().getValor()
        : null);
    // expedienteDTO.setCarteraNombre(expedientePosesionNegociada.getCartera() != null ?
    // expedientePosesionNegociada.getCartera().getNombre() : null);
    expedienteDTO.setGestor(gestor != null ? gestor.getNombre() : null);
    List<GrupoCliente> gruposClientes = expedientePosesionNegociada.getCartera().getClientes().stream().map(cc -> cc.getCliente().getGrupo()).collect(Collectors.toList());
    String grupos = gruposClientes.stream().filter(Objects::nonNull).distinct().map(GrupoCliente::getNombre).collect(Collectors.joining(", "));
    expedienteDTO.setGrupo(grupos);
    expedienteDTO.setSubtipoGestor(subtipoGestor != null ? subtipoGestor.getValor() : null);
    // CARTERA
    expedienteDTO.setCartera(
      expedientePosesionNegociada.getCartera() != null
        ? new CarteraDTOToList(expedientePosesionNegociada.getCartera())
        : null);
    // CLIENTE
    expedienteDTO.setCliente(
      expedientePosesionNegociada.getCartera() != null
        ? expedientePosesionNegociada.getCartera().getClientes().stream()
        .map(clienteCartera -> clienteCartera.getCliente().getNombre())
        .collect(Collectors.joining(", "))
        : null);

    // SUPERVISOR
    expedienteDTO.setSupervisor(supervisor != null ? supervisor.getNombre() : null);
    // RESPONSABLE CARTERA
    expedienteDTO.setResponsableCartera(
      responsableCartera != null ? responsableCartera.getNombre() : null);
    expedienteDTO.setGestorSkipTracing(gestorSkipTracing != null ? gestorSkipTracing.getNombre() : null);
    expedienteDTO.setGestorFormalizacion(gestorFormalizacion != null ? gestorFormalizacion.getNombre() : null);
    expedienteDTO.setGestorSoporte(expedientePosesionNegociada.getUsuariosByPerfil("Soporte"));

    // ACTIVOS
    expedienteDTO.setActivo(
      expedientePosesionNegociada.getCartera() != null
        ? expedientePosesionNegociada.getCartera().getTipoActivos()
        : null);

    // Area de gestor
    expedienteDTO.setAreaGestion(
      gestor != null && gestor.getAreas() != null
        ? gestor.getAreas().stream().map(AreaUsuario::getNombre).collect(Collectors.toList())
        : null);

    // Direccion
    BienPosesionNegociada valoracionalta = new BienPosesionNegociada();
    Double valor2 = null;
    if (!listaBienPosesionNegociada.isEmpty()) {
      valoracionalta = listaBienPosesionNegociada.iterator().next();
      valor2 = valoracionalta.getBien().getImporteBien() != null
          ? valoracionalta.getBien().getImporteBien()
          : null;
    }
    Double importeTotal = 0.00;
    List<Bien> listaBienes = new ArrayList<>();
    for (BienPosesionNegociada nuevoBien : listaBienPosesionNegociada) {
      listaBienes.add(nuevoBien.getBien());

      if(nuevoBien != null && nuevoBien.getBien() != null && nuevoBien.getBien().getImporteBien() != null)
        importeTotal += nuevoBien.getBien().getImporteBien();
    }
    //Nulo

    List<Bien> listabienesToList = listaBienes.stream().collect(Collectors.toList());
    List<Bien> listaAOrdenar = new ArrayList<>();
    for (Bien bien : listabienesToList) {
      if (bien.getImporteBien() != null) {
        listaAOrdenar.add(bien);
      }
      if (bien.getImporteBien() == null) {
        bien.setImporteBien(0.0);
        listaAOrdenar.add(bien);
      }
    }
    BienPosesionNegociada primeroPN = getPrincipal(expedientePosesionNegociada);
    Bien primero = null;
    if (primeroPN != null) {
      primero = primeroPN.getBien();

      expedienteDTO.setActivoPrincipal(primeroPN.getId());

      if (primeroPN.getEstrategia() != null && primeroPN.getEstrategia().getEstrategia() != null) {

        expedienteDTO.setEstrategia(
          primeroPN.getEstrategia() != null
            ? new CatalogoMinInfoDTO(primeroPN.getEstrategia().getEstrategia())
            : null);
      }
    }
    if (primero != null) {
      expedienteDTO.setDireccion(primero.getNombreVia() != null ? primero.getNombreVia() : null);
      expedienteDTO.setLocalidad(
        primero.getLocalidad() != null ? primero.getLocalidad().getValor() : null);
      expedienteDTO.setProvincia(
        primero.getProvincia() != null ? primero.getProvincia().getValor() : null);

      if (primero.getImporteBien() == null) {
        primero.setImporteBien(0.0);
        expedienteDTO.setDireccion(primero.getNombreVia() != null ? primero.getNombreVia() : null);
        expedienteDTO.setLocalidad(
          primero.getLocalidad() != null ? primero.getLocalidad().getValor() : null);
        expedienteDTO.setProvincia(
          primero.getProvincia() != null ? primero.getProvincia().getValor() : null);
      }

    }

    expedienteDTO.setGestor(gestor != null ? gestor.getNombre() : null);
    expedienteDTO.setSaldo(expedientePosesionNegociada.calculaImporteBien());
    expedienteDTO.setNumeroBienes(expedientePosesionNegociada.getBienesPosesionNegociada().size());
    expedienteDTO.setValorTotal(importeTotal);
    expedienteDTO.setEstado(
      expedientePosesionNegociada.getEstadoExpedienteRecuperacionAmistosa() != null
        ? expedientePosesionNegociada.getEstadoExpedienteRecuperacionAmistosa().getValor()
        : null);
    expedienteDTO.setComentarios(
      expedientePosesionNegociada.getComentariosPosesionNegociada().stream()
        .map(ComentarioExpedientePosesionNegociada::getComentario)
        .collect(Collectors.toList()));

    String idOrigenConcatenados = "";
    Double precioCompra = 0.0;
    for (BienPosesionNegociada bienPosesionNegociada : expedientePosesionNegociada.getBienesPosesionNegociada()) {
      Bien bien = bienPosesionNegociada.getBien();
      if (bien == null) continue;
      if (bien.getIdOrigen() != null && !idOrigenConcatenados.contains(bien.getIdOrigen()))
        idOrigenConcatenados += bien.getIdOrigen() + " ";
      precioCompra += bien.getImporteBien();
    }
    expedienteDTO.setPrecioCompra(precioCompra);
    if (idOrigenConcatenados.length() > 1)
      expedienteDTO.setOperacion(idOrigenConcatenados.substring(0, idOrigenConcatenados.length() - 1));

    //SITUACION
    List<Bien> bienes = new ArrayList<>();
    if (expedientePosesionNegociada.getBienesPosesionNegociada() != null) {
      for (var bienPN : expedientePosesionNegociada.getBienesPosesionNegociada()) {
        bienes.add(bienPN.getBien());
      }
    }

    List<Bien> bienesOrdenados = bienes.stream()
      .sorted(Comparator.comparingDouble(Bien::getImporteBien)
      .reversed())
      .collect(Collectors.toList());

    Boolean obtenido = false;
    for (var bienSacado : bienesOrdenados) {
      for (var bienPNSacado : expedientePosesionNegociada.getBienesPosesionNegociada()) {
        if (obtenido == false && bienPNSacado.getBien().equals(bienSacado)) {
          if (bienPNSacado.getSituacion() != null) {
            expedienteDTO.setSituacion(bienPNSacado.getSituacion().getValor());
            obtenido = true;
          }
        }
      }
    }

    // Añadimos la campaña al expediente
    Set<BienPosesionNegociada> setBPN = expedientePosesionNegociada.getBienesPosesionNegociada();
    if(setBPN != null && setBPN.size() > 0) {
	    var campaniaAsignadaBienPN = setBPN.stream().findFirst().get().getCampaniasAsignadasPN().stream().sorted(Comparator.comparing(CampaniaAsignadaBienPN::getFechaAsignacion).reversed()).findFirst().orElse(null);
      if (campaniaAsignadaBienPN != null) {
	      expedienteDTO.setCampania(campaniaAsignadaBienPN.getCampania().getNombre() != null ? campaniaAsignadaBienPN.getCampania().getNombre() : null);
	      expedienteDTO.setIdCampania(campaniaAsignadaBienPN.getCampania().getId() != null ? campaniaAsignadaBienPN.getCampania().getId() : null);
	    }
    }
    return expedienteDTO;
  }

  public ExpedientePosesionNegociada getExpediente(ExpedientePosesionNegociadaInputDTO expedientePosesionNegociadaDTO, Cartera cartera) throws Exception {
    ExpedientePosesionNegociada expediente = new ExpedientePosesionNegociada();
    expediente.setOrigen(origenExpedientePosesionNegociadaRepository.findById(OrigenExpedientePosesionNegociada.NEGOCIADA).orElseThrow(() -> new Exception("No encontrado origen expediente posesión negociada con valor 'Negociada'. Debería existir con id: " + OrigenExpedientePosesionNegociada.NEGOCIADA)));


    expediente.setCartera(cartera);
    String estadoExpedienteString = expedientePosesionNegociadaDTO.getEstadoExpedienteRecuperacionAmistosa() == null ? EstadoExpedientePosesionNegociada.REVISION : expedientePosesionNegociadaDTO.getEstadoExpedienteRecuperacionAmistosa();

    var estadoEspedientePN = estadoExpedientePosesionNegociadaRepository.findByCodigo(estadoExpedienteString).orElseThrow(() -> new Exception("Estado de posesión negociada: " + estadoExpedienteString + " No encontrada"));
    if (!estadoEspedientePN.getActivo())
      throw new Exception("Estado de posesión negociada: " + estadoExpedienteString + " está inactiva");
    expediente.setEstadoExpedienteRecuperacionAmistosa(estadoEspedientePN);
    expediente.setRiesgoReputacional(expedientePosesionNegociadaDTO.getRiesgoReputicional());

    return expediente;
  }

  public ExpedientePosesionNegociadaOutputDTO getExpedienteOutput(
    ExpedientePosesionNegociada expedientePosesionNegociada) throws Exception {
    ExpedientePosesionNegociadaOutputDTO expedientePosesionNegociadaOutputDTO =
      new ExpedientePosesionNegociadaOutputDTO();
    expedientePosesionNegociadaOutputDTO.setId(expedientePosesionNegociada.getId());
    expedientePosesionNegociadaOutputDTO.setIdConcatenado(expedientePosesionNegociada.getIdConcatenado());
    expedientePosesionNegociadaOutputDTO.setCliente(
      expedientePosesionNegociada.getCartera().getClientes().stream()
        .map(clienteCartera -> clienteCartera.getCliente().getNombre())
        .collect(Collectors.joining(", ")));
    expedientePosesionNegociadaOutputDTO.setNumeroBienes(
      expedientePosesionNegociada.getBienesPosesionNegociada().size());
    expedientePosesionNegociadaOutputDTO.setOrigen(
      expedientePosesionNegociada.getOrigen() != null
        ? expedientePosesionNegociada.getOrigen().getValor()
        : null);
    expedientePosesionNegociadaOutputDTO.setEstado(
      expedientePosesionNegociada.getEstadoExpedienteRecuperacionAmistosa() != null
        ? expedientePosesionNegociada.getEstadoExpedienteRecuperacionAmistosa().getValor()
        : null);
    expedientePosesionNegociadaOutputDTO.setImporte(
      expedientePosesionNegociada.calculaImporteBien());
    expedientePosesionNegociadaOutputDTO.setComentarios(
      expedientePosesionNegociada.getComentariosPosesionNegociada().stream()
        .map(v -> v.getComentario())
        .collect(Collectors.toList()));

    return expedientePosesionNegociadaOutputDTO;
  }

  public AsignacionGestoresDTO parseMap(Map map, ExpedientePosesionNegociada expediente) throws NotFoundException {
    AsignacionGestoresDTO asignacionGestoresDTO = new AsignacionGestoresDTO();

    Object timestamp = map.get("fechaAsignacion");
    Timestamp valueTimestamp = new Timestamp(Long.parseLong(String.valueOf(timestamp)));
    asignacionGestoresDTO.setFechaAsignacion(new Date(valueTimestamp.getTime()));

    var grupos = expediente.getGestor() != null ? expediente.getGestor().getGrupoUsuarios() : null;
    String concat = null;
    if (grupos != null) concat = grupos.stream().map(grupo -> grupo.getNombre()).collect(Collectors.joining(", "));
    asignacionGestoresDTO.setGrupo(concat);

    /*Object areaGestion = map.get("areaGestion");
    asignacionGestoresDTO.setAreaGestion(areaGestion != null ? String.valueOf(areaGestion) : null);*/

    Integer gestorExpediente = Integer.parseInt(map.get("gestorExpediente").toString());
    Usuario user = usuarioRepository.findById(gestorExpediente).orElseThrow(() ->
      new NotFoundException("usuario", gestorExpediente));
    asignacionGestoresDTO.setGestorExpediente(user.getNombre());

    String areas = user.getAreas().stream().map(AreaUsuario::getNombre).collect(Collectors.joining(", "));
    asignacionGestoresDTO.setAreaGestion(areas);

    Object usuarioAsignaciones = map.get("usuarioAsignaciones");
    asignacionGestoresDTO.setUsuarioAsignaciones(usuarioAsignaciones != null ? String.valueOf(usuarioAsignaciones) : null);

    Estrategia es;

    BienPosesionNegociada bpn = getPrincipal(expediente);

    if (bpn == null){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The PNC Files don't have any good.");
      else throw new NotFoundException("El ExpedientePN no tienen ningún bien.");
    }
    List<Map> estrategias = bienPosesionNegociadaRepository.findAllForEstrategiaLess(expediente.getId(), valueTimestamp, bpn.getId());
    if (estrategias.isEmpty()) {
      estrategias = bienPosesionNegociadaRepository.findAllForEstrategiaBigg(expediente.getId(), valueTimestamp, bpn.getId());
      if (estrategias.isEmpty()) {
        asignacionGestoresDTO.setEstrategia(bpn.getEstrategia() != null ? new CatalogoMinInfoDTO(bpn.getEstrategia().getEstrategia()) : null);
      } else {
        Map dato = estrategias.get(0);
        for (Map dat : estrategias) {
          if (dat.get("idEstrategia") != null) {
            dato = dat;
            break;
          }
        }
        if (dato.get("idEstrategia") != null) {
          es = estrategiaRepository.findById((Integer) dato.get("idEstrategia")).orElse(null);
          asignacionGestoresDTO.setEstrategia(es != null ? new CatalogoMinInfoDTO(es) : null);
        }
      }
    } else {
      Map dato = estrategias.get(0);
      if (dato.get("idEstrategia") != null) {
        es = estrategiaRepository.findById((Integer) dato.get("idEstrategia")).orElse(null);
        asignacionGestoresDTO.setEstrategia(es != null ? new CatalogoMinInfoDTO(es) : null);
      }
    }

    Usuario supervisor = expediente.getUsuarioByPerfil(Perfil.SUPERVISOR);
    asignacionGestoresDTO.setSupervisor(supervisor != null ? supervisor.getNombre() : "");

    return asignacionGestoresDTO;
  }

  public BienPosesionNegociada getPrincipal(ExpedientePosesionNegociada epn) {
    BienPosesionNegociada result = null;

    if (epn.getBienesPosesionNegociada().isEmpty()) return null;

    //result = new ArrayList<>(epn.getBienesPosesionNegociada()).get(0);
    for (BienPosesionNegociada b : epn.getBienesPosesionNegociada()) {
      if (result == null) result = b;
      else {
        if (result.getBien() != null && result.getBien().getImporteBien() == null) result = b;
        else if (b.getBien() != null && b.getBien().getImporteBien() != null) {
          if (result.getBien().getImporteBien() < b.getBien().getImporteBien()) result = b;
        }
      }
    }

    return result;
  }

  public List<AsignacionGestoresDTO> generarFechaFinAsignacion(List<AsignacionGestoresDTO> lista) {
    lista.sort((AsignacionGestoresDTO a1, AsignacionGestoresDTO a2) -> ObjectUtils.compare(a1.getFechaAsignacion(), (a2.getFechaAsignacion())));
    for (int i = 0; i < lista.size() - 1; i++) {
      lista.get(i).setFechaFinAsignacion(lista.get(i + 1).getFechaAsignacion());
    }
    return lista;
  }


  public ExpedientePNSegmentacionDTO parseSegmentacionPN (ExpedientePosesionNegociada expediente) {

    Double valorBienes = 0.00;
    boolean cargasPosteriores = false;

    for(BienPosesionNegociada bienProv: expediente.getBienesPosesionNegociada()) {
      valorBienes += bienProv.getBien() != null && bienProv.getBien().getImporteBien() != null ? bienProv.getBien().getImporteBien() : 0;
      if(bienProv != null && bienProv.getBien().getCargas() !=null &&bienProv.getBien().getCargas().size()>0) {
        cargasPosteriores=true;
      }
    }

    ExpedientePNSegmentacionDTO expedienteSegmentacionDTO = new ExpedientePNSegmentacionDTO();
    BeanUtils.copyProperties(expediente,expedienteSegmentacionDTO);

    BienPosesionNegociada bienPNPrincipal = expediente.getBienesPosesionNegociada() != null && expediente.getBienesPosesionNegociada().size() > 0 ? segmentacionUtil.sacarBienPNPrincipal(expediente) : null;
    ProcedimientoPosesionNegociada procedimiento = segmentacionUtil.sacarProcedimientoVivo(expediente);


    expedienteSegmentacionDTO.setIdConcatenado(expediente.getIdConcatenado());
    expedienteSegmentacionDTO.setProvincia(bienPNPrincipal != null && bienPNPrincipal.getBien().getProvincia() != null ? bienPNPrincipal.getBien().getProvincia().getValor() : null);
    expedienteSegmentacionDTO.setLocalidad(bienPNPrincipal != null && bienPNPrincipal.getBien().getLocalidad() != null ? bienPNPrincipal.getBien().getLocalidad().getValor() : null);
    expedienteSegmentacionDTO.setEstadoOcupacion(bienPNPrincipal != null && bienPNPrincipal.getEstadoOcupacion() != null ? bienPNPrincipal.getEstadoOcupacion().getValor() : null);
    expedienteSegmentacionDTO.setTipoProcedimiento(procedimiento != null && procedimiento.getTipo() != null ? procedimiento.getTipo().getValor() : null);

    expedienteSegmentacionDTO.setCargasPosteriores(cargasPosteriores);
    expedienteSegmentacionDTO.setValorBienes(valorBienes);
    expedienteSegmentacionDTO.setEstrategia(bienPNPrincipal != null && bienPNPrincipal.getEstrategia() != null ? bienPNPrincipal.getEstrategia().getEstrategia().getValor() : null);
    //TODO el ranking agencia
    expedienteSegmentacionDTO.setRankingAgencia(null); //ranking de la agencia para el reparto de expedientes

    return expedienteSegmentacionDTO;
  }
}
