package com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto;

import java.util.List;

import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.shared.dto.ContratoDTOToList;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExpedientePosesionNegociadaDTO {
  Integer id;
  String idConcatenado;
  private String expedienteCliente;
  String origen;
  //String carteraNombre;
  String gestor;
  String grupo;
  String subtipoGestor;
  private String gestorSkipTracing;
  private String gestorFormalizacion;
  String gestorSoporte;
  String cliente;
  CarteraDTOToList cartera;
  Double saldo;
  Integer numeroBienes;
  String estado;
  private String carteraPlazo;
  List<String> comentarios;
  private CatalogoMinInfoDTO estrategia;
  private String propuesta;
  private CatalogoMinInfoDTO estadoPropuesta;
  private List<ContratoDTOToList> contratos;
  private String supervisor;
  private String responsableCartera;
  private String activo;
  private List<String> areaGestion;
  private String judicializado;
  private String modoDeGestionEstado;
  private CatalogoMinInfoDTO origenEstrategia;
  private Double valor;
  private String direccion;
  private String localidad;
  private String provincia;
  private Integer activoPrincipal;
  private Double valorTotal;
  private String operacion;
  private Double precioCompra;
  private String situacion;
  private String campania;
  private Integer idCampania;
}
