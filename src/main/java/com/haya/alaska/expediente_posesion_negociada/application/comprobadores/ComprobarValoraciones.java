package com.haya.alaska.expediente_posesion_negociada.application.comprobadores;

import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.valoracion.domain.Valoracion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ComprobarValoraciones {
  /**
   * Para un mismo bien, misma valoración, importe, fecha y empresa
   * Casuística 1	Para un mismo bien, misma valoración, fecha y valorador
   * Resultado:	En la actualidad, se están cargando todos los registros
   * Sólo debería cargar un registro si los tres datos son iguales
   * Asunción 1:	Hacer un select distinct concatenando "Valorador", "Importe_valoracion" y "fecha_valoracion"
   * <p>
   * Casuística 2	Registros donde no se informa el importe de la valoración o la fecha y el importe de valoración
   * Resultado:	No se cargarán registros donde no se informe el importe de la valoración o el importe y la fecha
   * <p>
   * Casuística 3	Registros donde no se informa la fecha de valoración
   * Resultado:	Se pondrá por defecto una fecha dummy "01/01/1900"
   **/
  public static List<Valoracion> listaExcluidos  = new ArrayList<>(); ;

  public static List<Valoracion> getListaExcluidos() {
    return listaExcluidos;
  }

  private static boolean isValid(Valoracion valoracion, List<Valoracion> valoraciones) {
    if (valoraciones.contains(valoracion)) {
      listaExcluidos.add(valoracion);
      return false;
    }
    if (valoracion.getImporte() == null || (valoracion.getFecha() == null && valoracion.getImporte() == null)){
      listaExcluidos.add(valoracion);
      return false;
    }

    valoraciones.add(valoracion);
    return true;
  }

  public static List<Valoracion> comprobar(List<Valoracion> valoraciones) {
    List<Valoracion> listaYaAnadidas = new ArrayList<>();
    return valoraciones.stream().filter(valoracion -> isValid(valoracion, listaYaAnadidas)).map(valoracion -> {
      if (valoracion.getFecha() == null) {
        valoracion.setFecha(new Date("01/01/1900"));
      }
      return valoracion;
    }).collect(Collectors.toList());
  }
}
