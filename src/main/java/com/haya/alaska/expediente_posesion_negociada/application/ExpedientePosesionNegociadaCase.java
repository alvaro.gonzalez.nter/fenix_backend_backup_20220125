package com.haya.alaska.expediente_posesion_negociada.application;

import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDto;
import com.haya.alaska.busqueda.domain.BusquedaBuilderPN;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.expediente.infrastructure.controller.dto.AsignacionGestoresDTO;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaInputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.usuario.domain.Usuario;
import org.apache.tomcat.websocket.AuthenticationException;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.Predicate;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface ExpedientePosesionNegociadaCase {


  void gestionMasiva_ModoGestionYSituacion(
    List<Integer> expedienteIdList, List<Integer> expedientePNIdList, Integer estado, Integer subEstado, Integer situacion, Boolean todos, Boolean todosPN, BusquedaDto busquedaDto, Usuario usuario)
    throws Exception;

  void masiveEstrategiaUpdate(Integer idEstrategia, List<Integer> idsExpedientes, List<Integer> idsExpedientesPN, List<Integer> idsContratos, List<Integer> idsBienesPN, boolean todos, boolean todosPN, Usuario usuario, BusquedaDto busquedaDto) throws Exception;


  ExpedientePosesionNegociadaDTO addExpedienteSobreRecuperacionAmistosa(int idContrato, List<Integer> idsContratoBienes, Usuario usuario) throws Exception;

  void generateConcatenado(Integer idExpediente) throws NotFoundException;

  ListWithCountDTO<ExpedientePosesionNegociadaDTO> getAllFilteredExpedientes(
    Usuario usuario,
    String idExpediente,
    String origen, String carteraTipo, String gestor, String titular,
    String cliente, String cartera, String saldo, Integer numeroBienes, String estado,
    String orderField, String orderDirection, String direccion, String localidad, String provincia, int size, int page) throws NotFoundException, AuthenticationException;

  // Esto es un apaño rápido por el despliegue de Posesion negociada
  ListWithCountDTO<ExpedientePosesionNegociadaDTO> getAllFilteredExpedientesGestionMasiva(BusquedaDto busquedaDto, Usuario usuario, String idExpediente, String origen, String carteraTipo,
                                                                                          String gestor, String titular, String cliente, String cartera, String saldo, Integer numeroBienes, String estado, String estrategia, String supervisor, String orderField,
                                                                                          String orderDirection, int size, int page) throws NotFoundException, AuthenticationException, javax.naming.AuthenticationException;

  List<ExpedientePosesionNegociada> filtrarExpedientesPosesionNegociadaBusqueda(BusquedaDto busquedaDto, Usuario usuario, String idExpediente, String origen, String carteraTipo,
                                                                                String gestor, String cliente, String cartera, String saldo, Integer numeroBienes, String estado, String estrategia, String supervisor, String orderField, String orderDirection) throws NotFoundException;

  ExpedientePosesionNegociadaDTO addExpediente(Usuario usuario, ExpedientePosesionNegociadaInputDTO posesionNegociadaInputDTO) throws Exception;

  void carga(Usuario loggedUser, MultipartFile file, Integer cartera) throws Exception;

  void actualizarPostCarga() throws NotFoundException;

  void closeExpediente(Integer idExpediente) throws NotFoundException;

  ExpedientePosesionNegociadaDTO getExpedienteById(Integer expedienteId) throws Exception;

  void masiveEstrategiaUpdatePN(Integer idEstrategia, List<Integer> idsExpedientes, List<Integer> idsBienesPN, boolean todos, Usuario usuario, BusquedaDto busquedaDto) throws Exception;

  ExpedientePosesionNegociadaDTO updateModoGestion(Integer expedienteId, Integer estado, Integer subEstado, Usuario usuario) throws Exception;

  SubtipoEstado findByTipoSubEstado(Integer subEstado) throws Exception;

  TipoEstado findByTipoEstado(Integer estado) throws Exception;

  void concatenar();

  LinkedHashMap<String, List<Collection<?>>> getAllFilteredExpedientesPNExcel(BusquedaDto busquedaDto, Usuario usuario) throws NotFoundException, Exception;

  ListWithCountDTO<BienPosesionNegociadaDto> getAllFilteredBienesPN(BusquedaDto busquedaDto, Usuario usuario, Integer page, Integer size, String orderField, String orderDirection) throws NotFoundException;

  ListWithCountDTO<ExpedientePosesionNegociadaDTO> getAllExpedientesWithPropuestas(Usuario usuario) throws NotFoundException;

  ListWithCountDTO<AsignacionGestoresDTO> getAllAsignacionGestoresHistorico(
    Integer expedienteId) throws Exception;

  List<Predicate> getDataExpedientesPN(List<Predicate> predicates, BusquedaDto busquedaDto, Usuario usuario, BusquedaBuilderPN busquedaBuilderPN) throws NotFoundException;

}
