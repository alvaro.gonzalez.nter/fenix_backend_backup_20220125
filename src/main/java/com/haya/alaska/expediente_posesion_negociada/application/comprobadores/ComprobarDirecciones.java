package com.haya.alaska.expediente_posesion_negociada.application.comprobadores;

import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.direccion.domain.Direccion;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ComprobarDirecciones {
  /**
   * Datos de localización
   * Casuística 1	Mismo ID ocupante con datos de localización diferentes
   * Resultado:	Se carga to do lo que viene, no elimina ningún registro
   * <p>
   * Casuística 2	Mismo ID Ocupante con datos de localización iguales
   * Resultado:	Se debería cargar un único registro
   * Asunción 2:	Hacer select distinct de los campos "nombre_via", "localidad", "provincia" y "codigo_postal"
   **/

  public static List<Direccion> listaExcluidos  = new ArrayList<>(); ;

  public static List<Direccion> getListaExcluidos() {
    return listaExcluidos;
  }

  private static boolean isValid(Direccion direccion, List<Direccion> direcciones) {
    if (direcciones.contains(direccion)){
      listaExcluidos.add(direccion);
      return false;
    }
    direcciones.add(direccion);
    return true;
  }

  public static List<Direccion> comprobar(List<Direccion> direcciones) {
    List<Direccion> listaYaAnadidas = new ArrayList<>();
    return direcciones.stream().filter(direccion -> isValid(direccion, listaYaAnadidas)).collect(Collectors.toList());
  }
}
