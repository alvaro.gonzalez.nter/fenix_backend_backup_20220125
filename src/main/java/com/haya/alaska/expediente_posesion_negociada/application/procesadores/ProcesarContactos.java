package com.haya.alaska.expediente_posesion_negociada.application.procesadores;

import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.util.ExcelImport;

import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.haya.alaska.shared.util.ExcelImport.getRawStringValue;

@Service
@AllArgsConstructor
public class ProcesarContactos {
  private final int id_ocupante = 1;
  private final int tipo = 2;
  private final int valor = 3;
  private final int orden = 4;
  private final int descripcion = 5;
  private final int primera_fila_con_datos = 5;
  private final OcupanteRepository ocupanteRepository;

  public List<DatoContacto> procesar(XSSFSheet hojaContactos) throws NotFoundException {
    List<DatoContacto> dev = new ArrayList<>();
    XSSFRow filaActual;
    for (int i = primera_fila_con_datos; i <= hojaContactos.getPhysicalNumberOfRows(); i++) {
      DatoContacto contacto = new DatoContacto();
      filaActual = hojaContactos.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null != hasContent && !"".equals(hasContent)) {
        DatoContacto dc = procesarFilaContacto(contacto, filaActual);
        if (dc != null) dev.add(dc);
      }
    }
    return dev;
  }

  private DatoContacto procesarFilaContacto(DatoContacto contacto, XSSFRow filaActual) throws NotFoundException {
    Ocupante ocupante = ocupanteRepository.findByidOcupanteOrigen(getRawStringValue(filaActual, id_ocupante)).orElse(null);

    if (ocupante == null) {
      //System.out.println(getRawStringValue(filaActual, id_ocupante));
      return null;
    }
    contacto.setOcupante(ocupante);
    contacto.setOrden(Integer.parseInt(getRawStringValue(filaActual, orden)));
    if (Integer.parseInt(getRawStringValue(filaActual, tipo)) == 1) {
      //movil
      contacto.setMovil(ExcelImport.getStringValue(filaActual, valor));
    } else if (Integer.parseInt(getRawStringValue(filaActual, tipo)) == 2) {
      //fijo
      contacto.setFijo(ExcelImport.getStringValue(filaActual, valor));
    } else if (Integer.parseInt(getRawStringValue(filaActual, tipo)) == 3) {
      //email
      contacto.setEmail(ExcelImport.getStringValue(filaActual, valor));
    }
    contacto.setFechaActualizacion(new Date());
    return contacto;
  }
}
