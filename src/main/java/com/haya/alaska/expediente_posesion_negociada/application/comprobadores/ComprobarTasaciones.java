package com.haya.alaska.expediente_posesion_negociada.application.comprobadores;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.tasacion.domain.Tasacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ComprobarTasaciones {
  /**
   * Casuística 1	Para un mismo bien, misma tasación, fecha y empresa tasadora
   * Resultado:	En la actualidad, se están cargando todos los registros
   * Sólo debería cargar un registro si los tres datos son iguales
   * Asunción 1:
   * <p>
   * Casuística 2	Registros donde no se informa el importe de la tasación o la fecha y el importe de tasación
   * Resultado:	No se cargarán registros donde no se informe el importe de la tasación o el importe y la fecha
   * <p>
   * Casuística 3	Registros donde no se informa la fecha de tasación
   * Resultado:	Se pondrá por defecto una fecha dummy "01/01/1900"
   **/
  public static List<Tasacion> listaExcluidos  = new ArrayList<>(); ;

  public static List<Tasacion> getListaExcluidos() {
    return listaExcluidos;
  }

  private static boolean isValid(Tasacion tasacion, List<Tasacion> tasaciones) {
    if (tasaciones.contains(tasacion)) {
      listaExcluidos.add(tasacion);
      return false;
    }
    if (tasacion.getImporte() == null || (tasacion.getFecha() == null && tasacion.getImporte() == null)){
      listaExcluidos.add(tasacion);
      return false;
    }
    tasaciones.add(tasacion);
    return true;
  }

  public static List<Tasacion> comprobar(List<Tasacion> tasaciones) {
    List<Tasacion> listaYaAnadidas = new ArrayList<>();
    return tasaciones.stream().filter(tasacion -> isValid(tasacion, listaYaAnadidas)).map(tasacion -> {
      if (tasacion.getFecha() == null) {
        tasacion.setFecha(new Date("01/01/1900"));
      }
      return tasacion;
    }).collect(Collectors.toList());
  }
}
