package com.haya.alaska.expediente_posesion_negociada.application.procesadores;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tipo_tasacion.domain.TipoTasacion;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Service
@AllArgsConstructor
public class ProcesarTasaciones {
  private final int primera_fila_con_datos = 6;
  private final int id_bien = 2;
  private final int tasadora = 3;
  private final int tipo_tasacion = 4;
  private final int importe_tasacion = 5;
  private final int fecha_tasacion = 6;
  private final int liquidez = 7;

  private final BienRepository bienRepository;
  private HashMap<String, HashMap<String, Object>> catalogos;

  public List<Tasacion> procesar(XSSFSheet hojaTasacion, HashMap<String, HashMap<String, Object>> catalogos) throws Exception {
    this.catalogos = catalogos;
    List<Tasacion> dev = new ArrayList<>();
    XSSFRow filaActual;
    for (int i = primera_fila_con_datos; i <= hojaTasacion.getPhysicalNumberOfRows(); i++) {
      Tasacion tasacion = new Tasacion();
      filaActual = hojaTasacion.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null != hasContent && !"".equals(hasContent)) {
        dev.add(procesarFilaTasacion(tasacion, filaActual));
      }
    }
    return dev;
  }

  private Tasacion procesarFilaTasacion(Tasacion tasacion, XSSFRow filaActual) throws NotFoundException {
    Bien bien = bienRepository.findByIdCarga(getRawStringValue(filaActual, id_bien)).orElseThrow();
    tasacion.setBien(bien);
    tasacion.setTasadora(getRawStringValue(filaActual, tasadora));
    tasacion.setTipoTasacion((TipoTasacion) this.catalogos.get("tipoTasacion").get(getRawStringValue(filaActual, tipo_tasacion)));
    tasacion.setImporte(getNumericValue(filaActual, importe_tasacion));
    tasacion.setFecha(getDateValue(filaActual, fecha_tasacion));
    tasacion.setLiquidez((Liquidez) this.catalogos.get("liquidez").get(getRawStringValue(filaActual, liquidez)));
    return tasacion;
  }
}
