package com.haya.alaska.expediente_posesion_negociada.application.procesadores;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.origen_expediente_posesion_negociada.domain.OrigenExpedientePosesionNegociada;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.haya.alaska.shared.util.ExcelImport.getBooleanValue;
import static com.haya.alaska.shared.util.ExcelImport.getRawStringValue;

@Service
@AllArgsConstructor
public class ProcesarExpedientes {
  private final int id_cliente = 2;
  private final int id_cartera = 3;
  private final int id_expediente = 4;
  private final int riesgo_reputacional = 5;
  private final int primera_fila_con_datos = 6;
  private final CarteraRepository carteraRepository;
  private final ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  private HashMap<String, HashMap<String, Object>> catalogos;

  // TODO revisar este metodo porque esta sin probar y en las siguientes cargas si que vendrá informada esta hoja de excel
  public List<ExpedientePosesionNegociada> procesar(XSSFSheet hojaExpediente, HashMap<String, HashMap<String, Object>> catalogos) throws Exception {
    this.catalogos = catalogos;
    List<ExpedientePosesionNegociada> dev = new ArrayList<>();
    for (int i = primera_fila_con_datos; i < hojaExpediente.getPhysicalNumberOfRows(); i++) {
      XSSFRow filaActual = hojaExpediente.getRow(i);
      if (expedientePosesionNegociadaRepository.findByIdOrigen(getRawStringValue(filaActual, id_expediente)).isEmpty()) {
        ExpedientePosesionNegociada expPN = new ExpedientePosesionNegociada();
        //FIXME revisar esta busqueda si realmente es por esos campos por los que hay que buscar la cartera
        Cartera cartera = carteraRepository.findByIdCargaAndIdCargaSubcarteraIsNull(getRawStringValue(filaActual, id_cartera)).orElseThrow();
        expPN.setCartera(cartera);
        expPN.setRiesgoReputacional(getBooleanValue(filaActual, riesgo_reputacional));
        expPN.setIdOrigen(getRawStringValue(filaActual, id_expediente)); //FIXME revisar este mappeo
        expPN.setOrigen((OrigenExpedientePosesionNegociada) this.catalogos.get("OrigenExpedientePosesionNegociada").get("00"));
        dev.add(expPN);
      }
    }
    return dev;
  }
}
