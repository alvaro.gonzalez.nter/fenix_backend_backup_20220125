package com.haya.alaska.expediente_posesion_negociada.application.comprobadores;

import com.haya.alaska.ocupante.domain.Ocupante;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ComprobarOcupantes {
  /**
   * Datos de localización
   * Casuística 1	Mismo ID ocupante con datos de localización diferentes
   * Resultado:	Se carga to do lo que viene, no elimina ningún registro
   * <p>
   * Casuística 2	Mismo ID Ocupante con datos de localización iguales
   * Resultado:	Se debería cargar un único registro
   * Asunción 2:	Hacer select distinct de los campos "nombre_via", "localidad", "provincia" y "codigo_postal"
   * <p>
   * Datos de contacto
   * Casuística 1	Mismo ID Ocupante con datos de contacto diferentes
   * Resultado:	Se carga to do, diferenciando por tipo de contacto (email, movil, fijo) y orden. Permitiendo así existir 2 registros para el mismo interviniente y tipo de contacto
   * <p>
   * Casuística 2	Mismo ID Ocupante con datos de contacto iguales
   * Resultado:	Se debería cargar un único registro
   * Asunción 2:	Hacer select distinct de los campos "tipo" y "orden"
   **/

  private static boolean isValid(Ocupante ocupante, List<Ocupante> ocupantes) {
    ocupantes.add(ocupante);
    return true;
  }

  public static List<Ocupante> comprobar(List<Ocupante> ocupantes) {
    List<Ocupante> listaYaAnadidas = new ArrayList<>();
    return ocupantes.stream().filter(ocupante -> isValid(ocupante, listaYaAnadidas)).collect(Collectors.toList());
  }
}
