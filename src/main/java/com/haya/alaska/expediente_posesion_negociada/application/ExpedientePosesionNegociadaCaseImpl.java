package com.haya.alaska.expediente_posesion_negociada.application;

import com.haya.alaska.asignacion_expediente.application.AsignacionExpedienteUseCaseImpl;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente_;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada_;
import com.haya.alaska.asignacion_expediente_posesion_negociada.repository.AsignacionExpedientePosesionNegociadaRepository;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.Bien_;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.application.BienPosesionNegociadaUseCase;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada_;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDto;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaInputDto;
import com.haya.alaska.bien_posesion_negociada.infrastructure.mapper.BienPosesionNegociadaMapper;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.busqueda.application.BusquedaUseCaseImpl;
import com.haya.alaska.busqueda.domain.BusquedaBuilderPN;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.BusquedaAgendaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.IntervaloFecha;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.domain.Carga_;
import com.haya.alaska.carga.infrastructure.repository.CargaRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.clasificacion_contable.domain.ClasificacionContable_;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.domain.Cliente_;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera_;
import com.haya.alaska.comentario_expediente_posesion_negociada.domain.ComentarioExpedientePosesionNegociada;
import com.haya.alaska.comentario_expediente_posesion_negociada.repository.ComentarioExpedientePosesionNegociadaRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.domain.ContratoBien_;
import com.haya.alaska.contrato_bien.infrastructure.repository.ContratoBienRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.infrastructure.repository.DireccionRepository;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.espejo.bien.infrastructure.repository.BienEspejoRepository;
import com.haya.alaska.estado_checklist.domain.EstadoChecklist_;
import com.haya.alaska.estado_expediente_posesion_negociada.domain.EstadoExpedientePosesionNegociada;
import com.haya.alaska.estado_expediente_posesion_negociada.domain.EstadoExpedientePosesionNegociada_;
import com.haya.alaska.estado_expediente_posesion_negociada.infastructure.repository.EstadoExpedientePosesionNegociadaRepository;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion;
import com.haya.alaska.estado_ocupacion.infrastructure.repository.EstadoOcupacionRepository;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion.domain.Estimacion_;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.estrategia.domain.Estrategia_;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada_;
import com.haya.alaska.estrategia_asignada.infrastructure.repository.EstrategiaAsignadaRepository;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.AsignacionGestoresDTO;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.application.procesadores.*;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada_;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaInputDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.mapper.ExpedientePosesionNegociadaMapper;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.formalizacion.domain.Formalizacion_;
import com.haya.alaska.grupo_cliente.domain.GrupoCliente_;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta_;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.interviniente.infrastructure.util.IntervinienteUtil;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.domain.Movimiento_;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.domain.Ocupante_;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.origen_estrategia.infrastructure.repository.OrigenEstrategiaRepository;
import com.haya.alaska.origen_expediente_posesion_negociada.domain.OrigenExpedientePosesionNegociada;
import com.haya.alaska.origen_expediente_posesion_negociada.domain.OrigenExpedientePosesionNegociada_;
import com.haya.alaska.origen_expediente_posesion_negociada.repository.OrigenExpedientePosesionNegociadaRepository;
import com.haya.alaska.pais.domain.Pais_;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.domain.Perfil_;
import com.haya.alaska.periodo_estrategia.infrastructure.repository.PeriodoEstrategiaRepository;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.domain.Procedimiento_;
import com.haya.alaska.procedimiento.infrastructure.repository.ProcedimientoRepository;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada_;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.repository.ProcedimientoPosesionNegociadaRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.domain.Propuesta_;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.saldo.domain.Saldo_;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RuntimeException;
import com.haya.alaska.situacion.domain.Situacion;
import com.haya.alaska.situacion.domain.Situacion_;
import com.haya.alaska.situacion.infrastructure.repository.SituacionRepository;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.subtipo_estado.infrastructure.repository.SubtipoEstadoRepository;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.domain.Tasacion_;
import com.haya.alaska.tasacion.infrastructure.repository.TasacionRepository;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_carga.infrastructure.repository.TipoCargaRepository;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.tipo_estado.infrastructure.repository.TipoEstadoRepository;
import com.haya.alaska.tipo_estrategia.infrastructure.repository.TipoEstrategiaRepository;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.tipo_evento.infrastructure.repository.TipoEventoRepository;
import com.haya.alaska.tipo_tasacion.infrastructure.repository.TipoTasacionRepository;
import com.haya.alaska.tipo_valoracion.infrastructure.repository.TipoValoracionRepository;
import com.haya.alaska.uso.infrastructure.repository.UsoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.valoracion.domain.Valoracion_;
import com.haya.alaska.valoracion.infrastructure.repository.ValoracionRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class ExpedientePosesionNegociadaCaseImpl implements ExpedientePosesionNegociadaCase {

  @PersistenceContext
  private final EntityManager entityManager;
  @Autowired
  private final ProcesarBienes procesarBienes;
  @Autowired
  private final ProcesarCargas procesarCargas;
  @Autowired
  private final ProcesarContactos procesarContactos;
  @Autowired
  private final ProcesarDirecciones procesarDirecciones;
  @Autowired
  private final ProcesarExpedientes procesarExpedientes;
  @Autowired
  private final ProcesarOcupantes procesarOcupantes;
  @Autowired
  private final ProcesarTasaciones procesarTasaciones;
  @Autowired
  private final ProcesarValoraciones procesarValoraciones;
  @Autowired
  private final ProcesarCatalogos procesarCatalogos;
  @Autowired
  private final BienEspejoRepository bienEspejoRepository;
  @Autowired
  EstadoExpedientePosesionNegociadaRepository estadoExpedientePosesionNegociadaRepository;
  @Autowired
  ComentarioExpedientePosesionNegociadaRepository comentarioExpedientePosesionNegociadaRepository;
  @Autowired
  OrigenExpedientePosesionNegociadaRepository origenExpedientePosesionNegociadaRepository;
  @Autowired
  AsignacionExpedientePosesionNegociadaRepository asignacionExpedientePosesionNegociadaRepository;
  @Autowired
  ContratoBienRepository contratoBienRepository;
  @Autowired
  ContratoRepository contratoRepository;
  @Autowired
  EstadoOcupacionRepository estadoOcupacionRepository;
  @Autowired
  ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  BienRepository bienRepository;
  @Autowired
  ExpedientePosesionNegociadaMapper expedientePosesionNegociadaMapper;
  @Autowired
  BienPosesionNegociadaMapper bienPosesionNegociadaMapper;
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  BienPosesionNegociadaUseCase bienPosesionNegociadaUseCase;
  @Autowired
  AsignacionExpedienteUseCaseImpl asignacionExpedienteUseCaseImpl;
  @Autowired
  UsuarioRepository usuarioRepository;
  @Autowired
  TipoBienRepository tipoBienRepository;
  @Autowired
  EntornoRepository entornoRepository;
  @Autowired
  MunicipioRepository municipioRepository;
  @Autowired
  LocalidadRepository localidadRepository;
  @Autowired
  ProvinciaRepository provinciaRepository;
  @Autowired
  PaisRepository paisRepository;
  @Autowired
  TipoActivoRepository tipoActivoRepository;
  @Autowired
  UsoRepository usoRepository;
  @Autowired
  TipoTasacionRepository tipoTasacionRepository;
  @Autowired
  TipoValoracionRepository tipo_ValoracionRepository;
  @Autowired
  TipoCargaRepository tipoCargaRepository;
  @Autowired
  TasacionRepository tasacionRepository;
  @Autowired
  ValoracionRepository valoracionRepository;
  @Autowired
  CargaRepository cargasRepository;
  @Autowired
  OcupanteRepository ocupantesRepository;
  @Autowired
  ProcedimientoPosesionNegociadaRepository procedimientosRepository;
  @Autowired
  ServicioMaestro servicioMaestro;
  @Autowired
  DireccionRepository direccionesRepository;
  @Autowired
  DatoContactoRepository contactosRepository;
  @Autowired
  EstrategiaAsignadaRepository estrategiaAsignadaRepository;
  @Autowired
  ExpedientePosesionNegociadaExcelUseCase expedientePosesionNegociadaExcelUseCase;
  @Autowired
  TipoEstrategiaRepository tipoEstrategiaRepository;
  @Autowired
  OrigenEstrategiaRepository origenEstrategiaRepository;
  @Autowired
  PeriodoEstrategiaRepository periodoEstrategiaRepository;
  @Autowired
  EstrategiaRepository estrategiaRepository;
  @Autowired
  TipoEstadoRepository tipoEstadoRepository;
  @Autowired
  SubtipoEstadoRepository subtipoEstadoRepository;
  @Autowired
  SituacionRepository situacionRepository;
  @Autowired
  ExpedienteMapper expedienteMapper;
  @Autowired
  BusquedaUseCaseImpl busquedaUseCase;
  @Autowired
  IntervinienteUtil intervinienteUtil;
  @Autowired
  private EventoUtil eventoUtil;
  @Autowired
  private ExpedienteRepository expedienteRepository;
  @Autowired
  private IntervinienteRepository intervinienteRepository;
  @Autowired
  private ProcedimientoRepository procedimientoRepository;
  @Autowired
  private PropuestaRepository propuestaRepository;
  @Autowired
  EventoUseCase eventoUseCase;
  @Autowired
  SubtipoEventoRepository subtipoEventoRepository;
  @Autowired
  TipoEventoRepository tipoEventoRepository;

  @Override
  public void gestionMasiva_ModoGestionYSituacion(List<Integer> expedienteIdList, List<Integer> expedientePNIdList, Integer estado, Integer subEstado, Integer situacionId, Boolean todos, Boolean todosPN, BusquedaDto busquedaDto, Usuario usuario) throws Exception {

    List<Expediente> expedientes = new ArrayList<>();
    List<ExpedientePosesionNegociada> expedientesPN = new ArrayList<>();
    List<Integer> ids = new ArrayList<>();
    List<Integer> idsPN = new ArrayList<>();

    if (todos != null && todos == true) {

      if (busquedaDto == null) {
        throw new Exception("Hace falta enviar el objeto BusquedaDTO si se hace una petición para cambiar todas las estrategias");
      }
      expedientes = busquedaUseCase.getDataExpedientesFilter(busquedaDto, usuario, null, null);

    } else if (expedienteIdList != null) {
      expedientes = expedienteRepository.findByIdIn(expedienteIdList);
    }
    for (Expediente exp : expedientes) {
      for (var x : exp.getContratos()) {
        ids.add(x.getId());
      }
    }

    if (todosPN != null && todosPN == true) {

      if (busquedaDto == null) {
        throw new Exception("Hace falta enviar el objeto BusquedaDTO si se hace una petición para cambiar todas las estrategias");
      }
      expedientesPN = filtrarExpedientesPosesionNegociadaBusqueda(busquedaDto, usuario, null, null, null, null, null, null, null, null, null, null, null, null, null);

    } else if (expedientePNIdList != null) {
      expedientesPN = expedientePosesionNegociadaRepository.findByIdIn(expedientePNIdList);
    }
    for (ExpedientePosesionNegociada exp : expedientesPN) {
      for (var x : exp.getBienesPosesionNegociada()) {
        idsPN.add(x.getId());
      }
    }

    if (situacionId != null) {
      Situacion situacion = situacionRepository.findById(situacionId).orElseThrow(() -> new NotFoundException("situacion", situacionId));

      if (situacion != null) {
        List<Contrato> contratos = contratoRepository.findAllByIdIn(ids);
        Iterator<Contrato> iteratorContrato = contratos.iterator();
        while (iteratorContrato.hasNext()) {
          var contrato = iteratorContrato.next();
          contrato.setSituacion(situacion);
        }
        contratoRepository.saveAll(contratos);

        List<BienPosesionNegociada> bienesPN = bienPosesionNegociadaRepository.findAllByIdIn(idsPN);
        Iterator<BienPosesionNegociada> iteratorBienPosesionNegociada = bienesPN.iterator();
        while (iteratorBienPosesionNegociada.hasNext()) {
          var bienPN = iteratorBienPosesionNegociada.next();
          bienPN.setSituacion(situacion);
        }
        bienPosesionNegociadaRepository.saveAll(bienesPN);
      }
    }

    if (estado != null) {
      Iterator<Expediente> iteratorExpediente = expedientes.iterator();
      while (iteratorExpediente.hasNext()) {
        var expediente = iteratorExpediente.next();
        var nuevoEstado = this.findByTipoEstado(estado);
        if (nuevoEstado.getCodigo().equals("05")) expediente.setFechaCierre(new Date());
        expediente.setTipoEstado(nuevoEstado);
        expediente.setSubEstado(subEstado != null ? this.findByTipoSubEstado(subEstado) : null);
      }
      expedienteRepository.saveAll(expedientes);

      Iterator<ExpedientePosesionNegociada> iteratorExpedientePosesionNegociada = expedientesPN.iterator();
      while (iteratorExpedientePosesionNegociada.hasNext()) {
        var expedientePN = iteratorExpedientePosesionNegociada.next();
        var nuevoEstado = this.findByTipoEstado(estado);
        if (nuevoEstado.getCodigo().equals("05")) expedientePN.setFechaCierre(new Date());
        expedientePN.setTipoEstado(nuevoEstado);
        expedientePN.setSubEstado(subEstado != null ? this.findByTipoSubEstado(subEstado) : null);

        List<BienPosesionNegociada> bienPosesionNegociadaList = expedientePN.getBienesPosesionNegociada().stream().collect(Collectors.toList());
        for (BienPosesionNegociada bienPosesionNegociada : bienPosesionNegociadaList) {
          bienPosesionNegociada.setTipoEstado(nuevoEstado);
          bienPosesionNegociada.setSubEstado(subEstado != null ? this.findByTipoSubEstado(subEstado) : null);
          bienPosesionNegociadaRepository.save(bienPosesionNegociada);
        }
      }
      expedientePosesionNegociadaRepository.saveAll(expedientesPN);
    }
  }

  @Override
  public ListWithCountDTO<AsignacionGestoresDTO> getAllAsignacionGestoresHistorico(Integer expedienteId) throws Exception {
    ExpedientePosesionNegociada expediente = expedientePosesionNegociadaRepository.findById(expedienteId).orElseThrow(() -> new NotFoundException("expedientePosesionNegociada", expedienteId));
    List<Map> maps = expedientePosesionNegociadaRepository.historicoAsignacionGestores(expedienteId);
    List<AsignacionGestoresDTO> asignacionGestoresDTOS = new ArrayList<>();

    for (Map map : maps) {
      asignacionGestoresDTOS.add(expedientePosesionNegociadaMapper.parseMap(map, expediente));
    }
    expedientePosesionNegociadaMapper.generarFechaFinAsignacion(asignacionGestoresDTOS);
    return new ListWithCountDTO<>(asignacionGestoresDTOS, asignacionGestoresDTOS.size());
  }

  @Override
  public void masiveEstrategiaUpdate(Integer idEstrategia, List<Integer> idsExpedientes, List<Integer> idsExpedientesPN, List<Integer> idsContratos, List<Integer> idsBienesPN, boolean todos, boolean todosPN, Usuario usuario, BusquedaDto busquedaDto) throws Exception {

    List<Integer> ids = new ArrayList<>();
    List<Integer> idsPN = new ArrayList<>();
    Estrategia estrategia = estrategiaRepository.findById(idEstrategia).orElseThrow(() -> new NotFoundException("estrategia", idEstrategia));

    if (todos == true) {

      if (busquedaDto == null) {
        throw new Exception("Hace falta enviar el objeto BusquedaDTO si se hace una petición para cambiar todas las estrategias");
      }
      List<Expediente> expedientesSinPaginar = busquedaUseCase.getDataExpedientesFilter(busquedaDto, usuario, null, null);
      for (Expediente exp : expedientesSinPaginar) {
        for (var x : exp.getContratos()) {
          ids.add(x.getId());
        }
      }
    } else {
      ids = idsContratos;
    }

    if (todosPN == true) {

      if (busquedaDto == null) {
        throw new Exception("Hace falta enviar el objeto BusquedaDTO si se hace una petición para cambiar todas las estrategias");
      }
      List<ExpedientePosesionNegociada> expedientesSinPaginar = new ArrayList<>();
      expedientesSinPaginar = filtrarExpedientesPosesionNegociadaBusqueda(busquedaDto, usuario, null, null, null, null, null, null, null, null, null, null, null, null, null);
      for (ExpedientePosesionNegociada exp : expedientesSinPaginar) {
        for (var x : exp.getBienesPosesionNegociada()) {
          idsPN.add(x.getBien().getId());
        }
      }
    } else {
      idsPN = idsBienesPN;
    }

    List<Contrato> todosContratos = new ArrayList<>();
    if(idsExpedientes != null && idsExpedientes.size() > 0 && !todos) {
      todosContratos = contratoRepository.findAllByExpedienteIdIn(idsExpedientes);
    }
    else todosContratos = contratoRepository.findAll();

    List<BienPosesionNegociada> todosBienesPN = new ArrayList<>();
    if (idsPN != null) todosBienesPN.addAll(bienPosesionNegociadaRepository.findAllByBienIdIn(idsPN));

    if(ids != null && ids.size() > 0) {
      for (Integer idContrato : ids) {
        for (Contrato contrato : todosContratos) {
          if (contrato.getId().equals(idContrato)) {
            var ea2 = new EstrategiaAsignada();
            ea2.setOrigenEstrategia(origenEstrategiaRepository.findByCodigo("GM").orElse(null));
            ea2.setTipoEstrategia(tipoEstrategiaRepository.findByCodigo("M").orElse(null));
            ea2.setPeriodoEstrategia(periodoEstrategiaRepository.findByCodigo("M").orElse(null));
            ea2.setEstrategia(estrategia);
            ea2.setFecha(new Date());
            EstrategiaAsignada ea = estrategiaAsignadaRepository.save(ea2);
            contrato.setEstrategia(ea);
          }
        }
        contratoRepository.saveAll(todosContratos);
      }
    }
    if(todosBienesPN != null && todosBienesPN.size() > 0) {
      for (BienPosesionNegociada bienPosesionNegociada : todosBienesPN) {
        var ea2 = new EstrategiaAsignada();
        ea2.setOrigenEstrategia(origenEstrategiaRepository.findByCodigo("GM").orElse(null));
        ea2.setTipoEstrategia(tipoEstrategiaRepository.findByCodigo("M").orElse(null));
        ea2.setPeriodoEstrategia(periodoEstrategiaRepository.findByCodigo("M").orElse(null));
        ea2.setEstrategia(estrategia);
        ea2.setFecha(new Date());
        EstrategiaAsignada ea = estrategiaAsignadaRepository.save(ea2);
        bienPosesionNegociada.setEstrategia(ea);
      }
      bienPosesionNegociadaRepository.saveAll(todosBienesPN);
    }
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public ExpedientePosesionNegociadaDTO addExpedienteSobreRecuperacionAmistosa(int idContrato, List<Integer> idsContratoBienes, Usuario usuario) throws Exception {
    if (idsContratoBienes.isEmpty()) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("List of Contract of goods must not be empty");
      else throw new RuntimeException("Lista de Contrato de bienes no debe estar vacía");
    }
    Contrato contrato = contratoRepository.findById(idContrato).orElseThrow(() -> new Exception("Contrato: " + idContrato + " No encontrado"));
    if (!contrato.getActivo()) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RuntimeException("Contract: " + idContrato + " Not active");
      else throw new RuntimeException("Contrato: " + idContrato + " No esta activo");
    }

    ExpedientePosesionNegociada expedientePosesionNegociada = new ExpedientePosesionNegociada();
    expedientePosesionNegociada.setCartera(contrato.getExpediente().getCartera());
    expedientePosesionNegociada.setExpediente(contrato.getExpediente());
    expedientePosesionNegociada.setContrato(contrato);
    expedientePosesionNegociada.setOrigen(origenExpedientePosesionNegociadaRepository.findById(OrigenExpedientePosesionNegociada.AMISTOSA).orElseThrow(() -> new Exception("No econtrado origen expediente posesión negociada con valor 'Amistosa'. Debería existir con id: " + OrigenExpedientePosesionNegociada.AMISTOSA)));
    expedientePosesionNegociada.setEstadoExpedienteRecuperacionAmistosa(estadoExpedientePosesionNegociadaRepository.findByCodigo(EstadoExpedientePosesionNegociada.REVISION).get());
    expedientePosesionNegociada = expedientePosesionNegociadaRepository.saveAndFlush(expedientePosesionNegociada);

    guardaAsignacionExpediente(usuario, contrato.getExpediente().getCartera(), expedientePosesionNegociada);

    for (int idBien : idsContratoBienes) {
      ContratoBien contratoBien = contratoBienRepository.findByContratoIdAndBienId(idContrato, idBien).orElseThrow(() -> new Exception("Contrato de Bien con ID: " + idBien + " No encontrado"));
      if (!contratoBien.getActivo())
        throw new Exception("Contrato de Bien con id de bien: " + idBien + " y id de contrato: " + idContrato + "No esta activo");
      contrato.getBienes().stream().filter(c -> c.getBien().getId() == idBien && c.getActivo()).findFirst().orElseThrow(() -> new Exception("Contrato de Bien con ID: " + idBien + " No existe o esta inactivo en Contrato: " + contrato.getId()));

      Bien bien = contratoBien.getBien();

      if (bien.getPosesionNegociada() != null && bien.getPosesionNegociada()) {
        var bienPosesionNegociadaOpt = bienPosesionNegociadaRepository.findByBien(bien);

        throw new Exception("Contrato de Bien con ID: " + idBien + " Ya esta asignado a expediente de Posesión Negociada: " + (bienPosesionNegociadaOpt.isPresent() ? bienPosesionNegociadaOpt.get().getExpedientePosesionNegociada().getId() : " <DESCONOCIDA>"));
      }
      if (!bien.getTipoActivo().getCodigo().equals(TipoActivo.INMOBILIARIO))
        throw new Exception(" Bien con ID: " + idBien + " NO es del tipo Inmobiliario");

      BienPosesionNegociada bienPosesionNegociada = new BienPosesionNegociada();
      bienPosesionNegociada.setBien(bien);
      bienPosesionNegociada.setExpedientePosesionNegociada(expedientePosesionNegociada);
      bienPosesionNegociada.setEstadoOcupacion(estadoOcupacionRepository.findByCodigo(EstadoOcupacion.PENDIENTE_DE_INFORMAR).get());
      bienPosesionNegociada.setTipoGarantia(contratoBien.getTipoGarantia());
      BienPosesionNegociada bpnSaved = bienPosesionNegociadaRepository.save(bienPosesionNegociada);
      expedientePosesionNegociada.getBienesPosesionNegociada().add(bpnSaved);

      bien.setPosesionNegociada(true);
      bienRepository.save(bien);
    }
    expedientePosesionNegociada = expedientePosesionNegociadaRepository.save(expedientePosesionNegociada);

    ExpedientePosesionNegociadaDTO output = expedientePosesionNegociadaMapper.parse(expedientePosesionNegociada);
    return output;
  }

  public void generateConcatenado(Integer idExpediente) throws NotFoundException {
    ExpedientePosesionNegociada epn = expedientePosesionNegociadaRepository.findById(idExpediente).orElseThrow(() -> new NotFoundException("Expediente", idExpediente));

    String concatenado = generateConcatenado(epn);
    epn.setIdConcatenado(concatenado);
    expedientePosesionNegociadaRepository.save(epn);
  }

  @Transactional(rollbackFor = Exception.class, propagation = Propagation.MANDATORY)
  private void guardaAsignacionExpediente(Usuario usuario, Cartera cartera, ExpedientePosesionNegociada expedientePosesionNegociada) throws Exception {
    Usuario usuarioEntity = usuarioRepository.findByEmail(usuario.getEmail()).orElseThrow(() -> new Exception("Usuario: " + usuario.getEmail() + " No encontrado"));

    AsignacionExpedientePosesionNegociada asignacionExpedientePosesionNegociada = new AsignacionExpedientePosesionNegociada();
    asignacionExpedientePosesionNegociada.setActivo(true);
    asignacionExpedientePosesionNegociada.setExpediente(expedientePosesionNegociada);
    asignacionExpedientePosesionNegociada.setUsuario(usuarioEntity);
    asignacionExpedientePosesionNegociada.setUsuarioAsignacion(cartera.getResponsableCartera());
    asignacionExpedientePosesionNegociadaRepository.saveAndFlush(asignacionExpedientePosesionNegociada);
  }

  @Override
  public ListWithCountDTO<ExpedientePosesionNegociadaDTO> getAllExpedientesWithPropuestas(Usuario usuario) throws NotFoundException {

    List<ExpedientePosesionNegociada> expedientesSinPaginar = findAllFiltered(usuario, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, 1);
    List<ExpedientePosesionNegociada> expedientesPre = expedientesSinPaginar.stream().filter(exp -> exp.getPropuestas().size() > 0).collect(Collectors.toList());

    List<ExpedientePosesionNegociada> expedientes = new ArrayList<>();

    for (ExpedientePosesionNegociada expediente : expedientesPre) {
      List<Propuesta> propuestas = expediente.getPropuestas().stream().filter(pro -> pro.getBienes().size() > 0).collect(Collectors.toList());
      if (!propuestas.isEmpty()) expedientes.add(expediente);
    }

    List<ExpedientePosesionNegociadaDTO> expedientesDTOS = new ArrayList<>();
    for (ExpedientePosesionNegociada expediente : expedientes) {
      ExpedientePosesionNegociadaDTO ex = expedientePosesionNegociadaMapper.parse(expediente);
      if (ex.getEstadoPropuesta() != null) {
        ex.setPropuesta("si");
      } else {
        ex.setPropuesta("no");
      }
      expedientesDTOS.add(ex);
    }

    return new ListWithCountDTO<>(expedientesDTOS, expedientesSinPaginar.size());
  }

  @Override
  public ListWithCountDTO<ExpedientePosesionNegociadaDTO> getAllFilteredExpedientes(Usuario usuario, String idExpediente, String origen, String carteraTipo, String gestor, String titular, String cliente, String cartera, String saldo, Integer numeroBienes, String estado, String orderField, String orderDirection, String direccion, String localidad, String provincia, int size, int page) {
    List<ExpedientePosesionNegociada> expedientesSinPaginar = findAllFiltered(usuario, idExpediente, origen, carteraTipo, gestor, titular, cliente, cartera, saldo, numeroBienes, estado, orderField, orderDirection, direccion, localidad, provincia, size, page);
    List<ExpedientePosesionNegociada> expedientes = expedientesSinPaginar.stream().skip(size * page).limit(size).collect(Collectors.toList());
    List<ExpedientePosesionNegociadaDTO> expedientesDTOS = new ArrayList<>();
    for (ExpedientePosesionNegociada expediente : expedientes) {
      expedientesDTOS.add(expedientePosesionNegociadaMapper.parse(expediente));
    }
    // FILTRADO
    if (provincia != null) {
      expedientesDTOS = expedientesDTOS.stream().filter(c -> StringUtils.startsWithIgnoreCase(c.getProvincia() + "", provincia)).collect(Collectors.toList());
    }
    if (localidad != null) {
      expedientesDTOS = expedientesDTOS.stream().filter(c -> StringUtils.startsWithIgnoreCase(c.getLocalidad() + "", localidad)).collect(Collectors.toList());
    }
    if (direccion != null) {
      expedientesDTOS = expedientesDTOS.stream().filter(c -> StringUtils.startsWithIgnoreCase(c.getDireccion() + "", direccion)).collect(Collectors.toList());
    }
    // ORDENACIÓN
    if (orderField != null && orderField.equals("direccion")) {
      Comparator<ExpedientePosesionNegociadaDTO> comparator = (ExpedientePosesionNegociadaDTO o1, ExpedientePosesionNegociadaDTO o2) -> Integer.compare(o1.getId(), o2.getId());
      expedientesDTOS.sort(comparator);
      comparator = (ExpedientePosesionNegociadaDTO o1, ExpedientePosesionNegociadaDTO o2) -> StringUtils.compare(o1.getDireccion() != null ? o1.getDireccion() : null, o2.getDireccion() != null ? o2.getDireccion() : null);
      if (orderDirection == null) {
        orderDirection = "desc";
      }
      if (orderDirection.equals("asc")) expedientesDTOS.sort(comparator);
      else if (orderDirection.equals("desc")) expedientesDTOS.sort(comparator.reversed());
    }

    if (orderField != null && orderField.equals("provincia")) {
      Comparator<ExpedientePosesionNegociadaDTO> comparator = (ExpedientePosesionNegociadaDTO o1, ExpedientePosesionNegociadaDTO o2) -> Integer.compare(o1.getId(), o2.getId());
      expedientesDTOS.sort(comparator);
      comparator = (ExpedientePosesionNegociadaDTO o1, ExpedientePosesionNegociadaDTO o2) -> StringUtils.compare(o1.getProvincia() != null ? o1.getProvincia() : null, o2.getProvincia() != null ? o2.getProvincia() : null);
      if (orderDirection == null) {
        orderDirection = "desc";
      }
      if (orderDirection.equals("asc")) expedientesDTOS.sort(comparator);
      else if (orderDirection.equals("desc")) expedientesDTOS.sort(comparator.reversed());
    }

    if (orderField != null && orderField.equals("localidad")) {
      Comparator<ExpedientePosesionNegociadaDTO> comparator = (ExpedientePosesionNegociadaDTO o1, ExpedientePosesionNegociadaDTO o2) -> Integer.compare(o1.getId(), o2.getId());
      expedientesDTOS.sort(comparator);
      comparator = (ExpedientePosesionNegociadaDTO o1, ExpedientePosesionNegociadaDTO o2) -> StringUtils.compare(o1.getLocalidad() != null ? o1.getLocalidad() : null, o2.getLocalidad() != null ? o2.getLocalidad() : null);
      if (orderDirection == null) {
        orderDirection = "desc";
      }
      if (orderDirection.equals("asc")) expedientesDTOS.sort(comparator);
      else if (orderDirection.equals("desc")) expedientesDTOS.sort(comparator.reversed());
    }

    // expedientesPaged.getContent().stream().map(expedienteMapper::parse).collect(Collectors.toList());
    return new ListWithCountDTO<>(expedientesDTOS, expedientesSinPaginar.size());
  }

  // Esto es un apaño rápido por el despliegue de Posesion negociada
  @Override
  public ListWithCountDTO<ExpedientePosesionNegociadaDTO> getAllFilteredExpedientesGestionMasiva(BusquedaDto busquedaDto, Usuario usuario, String idExpediente, String origen, String carteraTipo, String gestor, String titular, String cliente, String cartera, String saldo, Integer numeroBienes, String estado, String estrategia, String supervisor, String orderField, String orderDirection, int size, int page) throws NotFoundException {

    List<ExpedientePosesionNegociada> expedientesSinPaginar = filtrarExpedientesPosesionNegociadaBusqueda(busquedaDto, usuario, idExpediente, origen, carteraTipo, gestor, cliente, cartera, saldo, numeroBienes, estado, estrategia, supervisor, orderField, orderDirection);

    List<ExpedientePosesionNegociada> expedientes = expedientesSinPaginar.stream().skip(size * page).limit(size).collect(Collectors.toList());
    List<ExpedientePosesionNegociadaDTO> expedientesDTOS = new ArrayList<>();
    for (ExpedientePosesionNegociada exp : expedientes) {
      expedientesDTOS.add(expedientePosesionNegociadaMapper.parse(exp));
    }
    return new ListWithCountDTO<>(expedientesDTOS, expedientesSinPaginar.size());
  }

  public ListWithCountDTO<BienPosesionNegociadaDto> getAllFilteredBienesPN(BusquedaDto busquedaDto, Usuario loggedUser, Integer page, Integer size, String orderField, String orderDirection) throws NotFoundException {
    List<ExpedientePosesionNegociada> expedientes = filtrarExpedientesPosesionNegociadaBusqueda(busquedaDto, loggedUser, null, null, null, null, null, null, null, null, null, null, null, orderField, orderDirection);
    List<BienPosesionNegociada> bienesSinPaginar = new ArrayList<>();
    for (ExpedientePosesionNegociada expedientePosesionNegociada : expedientes) {
      for (BienPosesionNegociada bien : expedientePosesionNegociada.getBienesPosesionNegociada()) {
        bienesSinPaginar.add(bien);
      }
    }
    List<BienPosesionNegociada> bienes = bienesSinPaginar.stream().skip(size * page).limit(size).collect(Collectors.toList());
    List<BienPosesionNegociadaDto> bienOutputDtos = new ArrayList<>();
    for (BienPosesionNegociada bien : bienes) {
      bienOutputDtos.add(bienPosesionNegociadaMapper.entityOutputDto(bien));
    }
    return new ListWithCountDTO<>(bienOutputDtos, bienesSinPaginar.size());
  }

  public LinkedHashMap<String, List<Collection<?>>> getAllFilteredExpedientesPNExcel(BusquedaDto busquedaDto, Usuario usuario) throws Exception {
    List<ExpedientePosesionNegociada> expedientesSinPaginar = filtrarExpedientesPosesionNegociadaBusqueda(busquedaDto, usuario, null, null, null, null, null, null, null, null, null, null, null, null, null);
    if (expedientesSinPaginar.size() > 5000)
      throw new Exception("La exportación seleccionada supera el límite de expedientes permitido (5000)");
    return this.expedientePosesionNegociadaExcelUseCase.findExcelExpedientePosesionNegociadaByArray(expedientesSinPaginar);
  }

  @Override
  public List<ExpedientePosesionNegociada> filtrarExpedientesPosesionNegociadaBusqueda(BusquedaDto busquedaDto, Usuario usuario, String idExpediente, String origen, String carteraTipo, String gestor, String cliente, String cartera, String saldo, Integer numeroBienes, String estado, String estrategia, String supervisor, String orderField, String orderDirection) throws NotFoundException {
    var expedienteDto = busquedaDto.getExpediente();
    var tempCriteriaBuilder = entityManager.getCriteriaBuilder();
    var query = tempCriteriaBuilder.createQuery(ExpedientePosesionNegociada.class);

    BusquedaBuilderPN busquedaBuilderPN = new BusquedaBuilderPN(query.from(ExpedientePosesionNegociada.class), tempCriteriaBuilder);
    List<Predicate> predicates = new ArrayList<>();

    predicates = getDataExpedientesPN(predicates, busquedaDto, usuario, busquedaBuilderPN);
    var cb = busquedaBuilderPN.getCb();
    var root = busquedaBuilderPN.getRoot();

    // FILTRADO DESPUES DE BUSCAR
    {
      if (idExpediente != null)
        predicates.add(cb.like(root.get(ExpedientePosesionNegociada_.idConcatenado), "%" + idExpediente + "%"));
      /*if (estrategia != null) {
      Join<ExpedientePosesionNegociada, BienPosesionNegociada> join =root.join(ExpedientePosesionNegociada_.BIENES_POSESION_NEGOCIADA, JoinType.INNER);
      Join<BienPosesionNegociada, EstrategiaAsignada> join1 = join.join(BienPosesionNegociada_.ESTRATEGIA, JoinType.INNER);
      Predicate p2 = cb.equal(join1.get(EstrategiaAsignada_.ESTRATEGIA).get(Estrategia_.VALOR), estrategia);
      predicates.add(p2);
      }*/
      if (supervisor != null) {
        Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> join = root.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
        Predicate onCond = cb.equal(join.get(AsignacionExpedientePosesionNegociada_.usuario).get(Usuario_.perfil).get(Perfil_.NOMBRE), "Supervisor");
        Join<AsignacionExpedientePosesionNegociada, Usuario> join2 = join.join(AsignacionExpedientePosesionNegociada_.usuario, JoinType.INNER);
        Predicate onCond2 = cb.like(join2.get(Usuario_.NOMBRE), "%" + supervisor + "%");
        predicates.add(cb.and(onCond, onCond2));
      }
      if (origen != null)
        predicates.add(cb.like(root.get(ExpedientePosesionNegociada_.origen).get(OrigenExpedientePosesionNegociada_.valor), "%" + origen + "%"));
      if (carteraTipo != null)
        predicates.add(cb.like(root.join(ExpedientePosesionNegociada_.cartera).get(Cartera_.tipoActivos), "%" + carteraTipo + "%"));
      if (gestor != null) {
        Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> join = root.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
        Predicate onCond = cb.equal(join.get(AsignacionExpedientePosesionNegociada_.usuario).get(Usuario_.perfil).get(Perfil_.NOMBRE), "Gestor");
        Join<AsignacionExpedientePosesionNegociada, Usuario> join2 = join.join(AsignacionExpedientePosesionNegociada_.usuario, JoinType.INNER);
        Predicate onCond2 = cb.like(join2.get(Usuario_.NOMBRE), "%" + gestor + "%");
        predicates.add(cb.and(onCond, onCond2));
      }
      if (cliente != null)
        predicates.add(cb.like(root.join(ExpedientePosesionNegociada_.cartera).join(Cartera_.clientes).join(ClienteCartera_.cliente).get(Cliente_.nombre), "%" + cliente + "%"));
      if (cartera != null)
        predicates.add(cb.like(root.get(ExpedientePosesionNegociada_.cartera).get(Cartera_.NOMBRE), "%" + cartera + "%"));
      if (estado != null)
        predicates.add(cb.like(root.join(ExpedientePosesionNegociada_.estadoExpedienteRecuperacionAmistosa).get(EstadoExpedientePosesionNegociada_.valor), "%" + estado + "%"));
      if (saldo != null) {
        Subquery<Double> subqueryI = cb.createQuery().subquery(Double.class);
        Root<BienPosesionNegociada> bienPN = subqueryI.from(BienPosesionNegociada.class);
        var joinBien = bienPN.join(BienPosesionNegociada_.bien).get(Bien_.importeBien);

        predicates.add(cb.like(subqueryI.select(cb.sum(joinBien)).where(cb.equal(bienPN.get(BienPosesionNegociada_.expedientePosesionNegociada), root.get(ExpedientePosesionNegociada_.id))).as(String.class), "%" + saldo + "%"));
      }
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    // ORDENACION
    if (orderField != null) {
      switch (orderField) {
        case "origen":
          rs.orderBy(getOrden(cb, root.get(ExpedientePosesionNegociada_.origen).get(OrigenExpedientePosesionNegociada_.valor), orderDirection));
          break;
        case "tipo":
          rs.orderBy(getOrden(cb, root.join(ExpedientePosesionNegociada_.cartera, JoinType.INNER).get(Cartera_.TIPO_ACTIVOS), orderDirection));
          break;
        case "gestor":
          Join<ExpedientePosesionNegociada_, AsignacionExpedientePosesionNegociada> join41 = root.join(ExpedientePosesionNegociada_.ASIGNACIONES, JoinType.INNER);
          Join<AsignacionExpedientePosesionNegociada, Usuario> join42 = join41.join(AsignacionExpedientePosesionNegociada_.USUARIO, JoinType.INNER);
          rs.orderBy(getOrden(cb, join42.get(Usuario_.nombre), orderDirection));
          break;
        case "cliente":
          Join<ClienteCartera, Cliente> join22 = root.join(ExpedientePosesionNegociada_.cartera, JoinType.INNER).join(Cartera_.clientes, JoinType.INNER).join(ClienteCartera_.cliente, JoinType.INNER);
          rs.orderBy(getOrden(cb, join22.get(Cliente_.NOMBRE), orderDirection));
          break;
        case "cartera":
          rs.orderBy(getOrden(cb, root.join(ExpedientePosesionNegociada_.cartera, JoinType.INNER).get(Cartera_.NOMBRE), orderDirection));
          break;
        case "usuarioId":
          var join = root.join(ExpedientePosesionNegociada_.ASIGNACIONES, JoinType.INNER).join(AsignacionExpediente_.USUARIO, JoinType.INNER);
          rs.orderBy(getOrden(cb, join.get(Usuario_.ID), orderDirection));
          break;
        case "estado":
          rs.orderBy(getOrden(cb, root.join(ExpedientePosesionNegociada_.estadoExpedienteRecuperacionAmistosa).get(EstadoExpedientePosesionNegociada_.codigo), orderDirection));
          break;
      }
    }
    log.warn(entityManager.createQuery(query.distinct(true)).unwrap(org.hibernate.query.Query.class).getQueryString());
    var listaResultados = entityManager.createQuery(rs).getResultList();

    // FILTRADO POST QUERY
    {
      // FILTRADO EXPEDIENTE :: SALDO EN GESTION
      if (expedienteDto != null && expedienteDto.getSaldoGestion() != null) {
        var saldoBusqueda = expedienteDto.getSaldoGestion();
        listaResultados = listaResultados.stream().filter((ExpedientePosesionNegociada exp) -> {
          boolean mayorQue = true;
          boolean menorQue = true;
          if (saldoBusqueda.getIgual() != null) return saldoBusqueda.getIgual().equals(exp.calculaImporteBien());
          if (saldoBusqueda.getMayor() != null)
            menorQue = Double.compare(exp.calculaImporteBien(), saldoBusqueda.getMayor()) < 0;
          if (saldoBusqueda.getMenor() != null)
            mayorQue = Double.compare(exp.calculaImporteBien(), saldoBusqueda.getMenor()) > 0;
          boolean mayorQueAndMenorQue = mayorQue && menorQue;
          return mayorQueAndMenorQue;
        }).collect(Collectors.toList());
      }

      // NUMERO ACTIVOS
      if (numeroBienes != null)
        listaResultados = listaResultados.stream().filter(exp -> exp.getBienesPosesionNegociada().size() == numeroBienes).collect(Collectors.toList());
      else {
        if ("numeroActivos".equals(orderField)) {
          if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
            listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.getBienesPosesionNegociada().size()).reversed()).collect(Collectors.toList());
          else
            listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.getBienesPosesionNegociada().size())).collect(Collectors.toList());
        } else if ("saldo".equals(orderField)) {
          if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
            listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.calculaImporteBien()).reversed()).collect(Collectors.toList());
          else
            listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.calculaImporteBien())).collect(Collectors.toList());
        }
      }

      if (estrategia != null) {
        listaResultados = listaResultados.stream().filter((var expediente) -> expedientePosesionNegociadaMapper.getPrincipal(expediente).getEstrategia() != null && StringUtils.containsIgnoreCase(expedientePosesionNegociadaMapper.getPrincipal(expediente).getEstrategia().getEstrategia().getValor(), estrategia)).collect(Collectors.toList());
      }
    }
    return listaResultados;
  }

  public List<Predicate> getDataExpedientesPN(List<Predicate> predicates, BusquedaDto busquedaDto, Usuario usuario, BusquedaBuilderPN busquedaBuilderPN) throws NotFoundException {

    CriteriaBuilder cb = busquedaBuilderPN.getCb();
    var root = busquedaBuilderPN.getRoot();

    if (busquedaDto == null) {
      busquedaDto = new BusquedaDto();
    }
    var expedienteDto = busquedaDto.getExpediente() != null ? busquedaDto.getExpediente() : null;
    var intervinienteDto = busquedaDto.getInterviniente();
    var contratoDto = busquedaDto.getContrato();
    var bienDto = busquedaDto.getBien();
    var movimientoDto = busquedaDto.getMovimientos();
    var estimacionesDto = busquedaDto.getEstimaciones();
    var propuestaDto = busquedaDto.getPropuestas();
    var agendaDto = busquedaDto.getAgenda();
    var procedimientoDto = busquedaDto.getProcedimiento();

    // COMPRUEBA USUARIO LOGGEADO
    if (usuario.getRolHaya() == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("No Haya role found for this user");
      else throw new NotFoundException("No se ha encontrado un rol de Haya para este usuario");
    }
    if (!usuario.esAdministrador() && !usuario.getRolHaya().getCodigo().equals("webservice")){
      var join = busquedaBuilderPN.getJoinAsignacionesExpediente(); // root.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
      Predicate asignaciones = cb.equal(join.get(AsignacionExpedientePosesionNegociada_.usuario).get(Usuario_.id), usuario.getId());
      Predicate asignacionesSuplente;
      Predicate predicateFinal = asignaciones;
      if (usuario.getPerfil()!=null && usuario.getPerfil().getNombre().compareTo(Perfil.GESTOR) == 0) {
        Subquery<Integer> subquerySuplente = cb.createQuery().subquery(Integer.class);
        subquerySuplente.distinct(true);
        Root<ExpedientePosesionNegociada> rootSuplentes = subquerySuplente.from(ExpedientePosesionNegociada.class);
        Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> joinSuplente = rootSuplentes.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
        Predicate predicateSuplente = cb.equal(joinSuplente.join(AsignacionExpedientePosesionNegociada_.SUPLENTE, JoinType.INNER).get(Usuario_.ID), usuario.getId());
        Subquery<Integer> subquerySuplente2 = subquerySuplente.select(rootSuplentes.get(ExpedientePosesionNegociada_.id)).distinct(true).where(predicateSuplente);
        subquerySuplente2.distinct(true);

        predicateFinal = cb.or(asignaciones, root.get(ExpedientePosesionNegociada_.ID).in(subquerySuplente2.distinct(true)));
      }
      predicates.add(predicateFinal);
    }

    // SECCIÓN EXPEDIENTES
    if (expedienteDto != null) {
      if (expedienteDto.getId() != null) {
        predicates.add(cb.like(root.get(ExpedientePosesionNegociada_.ID_CONCATENADO), "%" + expedienteDto.getId() + "%"));
      }
      // SALDO EN GESTION >> Se hace despues de ejecutar la query ya que es un campo calculado
      if (expedienteDto.getGestor() != null) {
        Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> join = root.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
        Predicate onCond = cb.equal(join.get(AsignacionExpedientePosesionNegociada_.usuario).get(Usuario_.perfil).get(Perfil_.NOMBRE), "Gestor");
        Join<AsignacionExpedientePosesionNegociada, Usuario> join2 = join.join(AsignacionExpedientePosesionNegociada_.usuario, JoinType.INNER);
        Predicate onCond2 = cb.like(join2.get(Usuario_.NOMBRE), "%" + expedienteDto.getGestor() + "%");
        predicates.add(cb.and(onCond, onCond2));
      }
      if (expedienteDto.getCartera() != null) {
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.cartera).get(Cartera_.id), expedienteDto.getCartera()));
      }
      if (expedienteDto.getResponsableCartera() != null) {
        predicates.add(cb.like(root.get(ExpedientePosesionNegociada_.cartera).get(Cartera_.responsableCartera).get(Usuario_.nombre), "%" + expedienteDto.getResponsableCartera() + "%"));
      }
      if (expedienteDto.getEstado() != null) {
        predicates.add(cb.equal(root.join(ExpedientePosesionNegociada_.estadoExpedienteRecuperacionAmistosa).get(EstadoExpedientePosesionNegociada_.id), expedienteDto.getEstado()));
      }
      if (expedienteDto.getSubestado() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (expedienteDto.getSupervisor() != null) {
        Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> ExAe = root.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
        Join<AsignacionExpedientePosesionNegociada, Usuario> AeUs = ExAe.join(AsignacionExpedientePosesionNegociada_.usuario, JoinType.INNER);
        Predicate p1 = cb.like(AeUs.get(Usuario_.nombre), "%" + expedienteDto.getSupervisor() + "%");
        Predicate p2 = cb.equal(AeUs.get(Usuario_.perfil).get(Perfil_.nombre), "Supervisor");
        predicates.add(cb.and(p1, p2));
      }
      if (expedienteDto.getEstrategia() != null) {
        Join<ExpedientePosesionNegociada, Contrato> join = root.join(ExpedientePosesionNegociada_.contrato, JoinType.INNER);
        Predicate p1 = cb.isTrue(join.get(Contrato_.esRepresentante));
        Join<Contrato, EstrategiaAsignada> join1 = join.join(Contrato_.ESTRATEGIA, JoinType.INNER);
        Predicate p2 = cb.equal(join1.get(EstrategiaAsignada_.ESTRATEGIA).get(Estrategia_.ID), expedienteDto.getEstrategia());
        predicates.add(cb.and(p1, p2));
      }
      if (expedienteDto.getSituacion() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinContratoRepresentante().get(Contrato_.situacion).get(Situacion_.id), expedienteDto.getSituacion()));
      }
      if (expedienteDto.getCliente() != null) {
        predicates.add(cb.equal(root.join(ExpedientePosesionNegociada_.cartera).join(Cartera_.clientes).join(ClienteCartera_.cliente).get(Cliente_.id), expedienteDto.getCliente()));
      }
      if (expedienteDto.getGrupoCliente() != null) {
        predicates.add(cb.equal(root.join(ExpedientePosesionNegociada_.cartera).join(Cartera_.clientes).join(ClienteCartera_.cliente).join(Cliente_.grupo).get(GrupoCliente_.id), expedienteDto.getGrupoCliente()));
      }
    }

    // SECCIÓN DE INTERVINIENTES / OCUPANTES
    if (intervinienteDto != null) {
      Join<OcupanteBien, Ocupante> joinsOcupante;
      if (intervinienteDto.getDocId() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.equal(joinsOcupante.get(Ocupante_.dni), intervinienteDto.getDocId()));
      }
      if (intervinienteDto.getId() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.equal(joinsOcupante.get(Ocupante_.id), intervinienteDto.getId()));
      }
      if (intervinienteDto.getApellidos() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.equal(joinsOcupante.get(Ocupante_.apellidos), intervinienteDto.getApellidos()));
      }
      if (intervinienteDto.getNombre() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.equal(joinsOcupante.get(Ocupante_.nombre), intervinienteDto.getNombre()));
      }
      if (intervinienteDto.getNacionalidad() != null) {
        joinsOcupante = busquedaBuilderPN.getJoinsOcupante();
        predicates.add(cb.equal(joinsOcupante.get(Ocupante_.nacionalidad).get(Pais_.id), intervinienteDto.getNacionalidad()));
      }
      if (intervinienteDto.getIdIntervinienteOrigen() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getResidencia() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getEmail() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getProvincia() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getLocalidad() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getVulnerabilidad() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getTipoIntervencion() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (intervinienteDto.getIntervaloFecha() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
    }

    // SECCIÓN DE CONTRATOS
    if (contratoDto != null) {
      Join<ExpedientePosesionNegociadaCase, Contrato> joinContrato;
      if (contratoDto.getId() != null) {
        joinContrato = busquedaBuilderPN.getJoinContrato();
        predicates.add(cb.equal(joinContrato.get(Contrato_.ID_CARGA), contratoDto.getId()));
      }
      if (contratoDto.getEstado() != null) {
        joinContrato = busquedaBuilderPN.getJoinContrato();
        predicates.add(cb.equal(joinContrato.get(Contrato_.estadoContrato), contratoDto.getEstado()));
      }
      if (contratoDto.getProducto() != null) {
        joinContrato = busquedaBuilderPN.getJoinContrato();
        predicates.add(cb.equal(joinContrato.get(Contrato_.producto.getName()), contratoDto.getProducto()));
      }
      if (contratoDto.getClasificacionContable() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinContratos().get(Contrato_.clasificacionContable).get(ClasificacionContable_.ID), contratoDto.getClasificacionContable()));
      }
      if (contratoDto.getContratoOrigen() != null) {
        predicates.add(cb.like(busquedaBuilderPN.getJoinContratos().get(Contrato_.idCarga), contratoDto.getContratoOrigen()));
      }
      if (contratoDto.getReestructurado() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinContratos().get(Contrato_.reestructurado), contratoDto.getReestructurado()));
      }
      if (contratoDto.getNumCuotasImpago() != null) {
        predicates.add(cb.equal(busquedaBuilderPN.getJoinContratos().get(Contrato_.numeroCuotasImpagadas), contratoDto.getNumCuotasImpago()));
      }
      if (contratoDto.getTipoEstimacion() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (contratoDto.getSubtipoEstimacion() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (contratoDto.getIntervaloImporte() != null) {
        switch (contratoDto.getIntervaloImporte().getDescripcion()) {
          case "capitalPendiente":
            predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinSaldos().get(Saldo_.capitalPendiente), contratoDto.getIntervaloImporte());
            break;
          case "importeInicial":
            predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.importeInicial), contratoDto.getIntervaloImporte());
            break;
          case "saldoGestion":
            predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinSaldos().get(Saldo_.saldoGestion), contratoDto.getIntervaloImporte());
            break;
          case "deudaImpagada":
            predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinSaldos().get(Saldo_.deudaImpagada), contratoDto.getIntervaloImporte());
            break;
          default:
            break;
        }
      }
      if (contratoDto.getIntervaloFechas() != null && contratoDto.getIntervaloFechas().size() > 0) {
        for (IntervaloFecha busqueda : contratoDto.getIntervaloFechas()) {
          switch (busqueda.getDescripcion()) {
            case "formalizacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaFormalizacion), busqueda);
              break;
            case "vencimientoInicial":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaVencimientoInicial), busqueda);
              break;
            case "vencimientoAnticipado":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaVencimientoAnticipado), busqueda);
              break;
            case "paseRegular":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaPaseRegular), busqueda);
              break;
            case "primerImpago":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaPrimerImpago), busqueda);
              break;
            case "primeraAsignacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaPrimeraAsignacion), busqueda);
              break;
            case "ultimaAsignacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContratos().get(Contrato_.fechaUltimaAsignacion), busqueda);
              break;
            default:
              break;
          }
        }
      }
    }

    // Si expedientePosesionNegociada no se encuentra relacionado a un bien no aparecerá en la
    // respuesta
    // SECCIÓN DE BIENES
    if (bienDto != null) {
      Join<BienPosesionNegociada, Bien> joinBien2;
      if (bienDto.getId() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.idHaya), bienDto.getId()));
      }
      if (bienDto.getIdOrigen() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.idCarga), bienDto.getIdOrigen()));
      }
      if (bienDto.getDireccion() != null) {
        predicates.add(cb.like(busquedaBuilderPN.getJoinBien().get(Bien_.nombreVia), bienDto.getDireccion()));
      }
      if (bienDto.getProvincia() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.provincia), bienDto.getProvincia()));
      }
      if (bienDto.getMunicipio() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.municipio), bienDto.getMunicipio()));
      }
      if (bienDto.getLocalidad() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.localidad), bienDto.getLocalidad()));
      }
      if (bienDto.getRegistro() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.registro), bienDto.getRegistro()));
      }
      if (bienDto.getLocalidadRegistro() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.localidadRegistro), bienDto.getLocalidadRegistro()));
      }
      if (bienDto.getFincaRegistral() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.finca), bienDto.getFincaRegistral()));
      }
      if (bienDto.getReferenciaCatastral() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.referenciaCatastral), bienDto.getReferenciaCatastral()));
      }
      if (bienDto.getIdufir() != null) {
        joinBien2 = busquedaBuilderPN.getJoinBien();
        predicates.add(cb.equal(joinBien2.get(Bien_.idufir), bienDto.getIdufir()));
      }

      if (bienDto.getIntervaloImporte() != null) {
        if (bienDto.getIntervaloImporte().getDescripcion() != null) {
          switch (bienDto.getIntervaloImporte().getDescripcion()) {
            case "tasacion":
              predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinTasacionBien().get(Tasacion_.importe), bienDto.getIntervaloImporte());
              break;
            case "valoracion":
              predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinValoracionBien().get(Valoracion_.importe), bienDto.getIntervaloImporte());
              break;
            case "adjudicacion":
              // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista
              // vacia
              predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
              break;
            default:
              break;
          }
        }
      }
      if (bienDto.getIntervaloFechas() != null && bienDto.getIntervaloFechas().size() > 0) {
        for (IntervaloFecha busqueda : bienDto.getIntervaloFechas()) {
          switch (busqueda.getDescripcion()) {
            case "inscripcion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.contratos, JoinType.INNER).get(ContratoBien_.bien).get(Bien_.fechaInscripcion), busqueda);
              break;
            case "primeraValoracion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.valoraciones, JoinType.INNER).get(Valoracion_.fecha), busqueda);
              break;
            case "primeraTasacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.tasaciones, JoinType.INNER).get(Tasacion_.fecha), busqueda);
              break;
            case "reoConversion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.contratos, JoinType.INNER).get(ContratoBien_.bien).get(Bien_.fechaReoConversion), busqueda);
              break;
            case "anotacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.cargas, JoinType.INNER).get(Carga_.fechaAnotacion), busqueda);
              break;
            case "vencimientoCarga":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinBien().join(Bien_.cargas, JoinType.INNER).get(Carga_.fechaVencimiento), busqueda);
              break;
            case "asignacionPN":
              // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista
              // vacia
              predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
              break;
            default:
              break;
          }
        }
      }
    }

    // SECCIÓN MOVIMIENTOS
    if (movimientoDto != null) {
      if (movimientoDto.getTipoMovimiento() != null) {
        Join<ExpedientePosesionNegociada, Contrato> joinContrato = root.join(ExpedientePosesionNegociada_.CONTRATO, JoinType.INNER);
        Join<Contrato, Movimiento> joinMovimiento = joinContrato.join(Contrato_.movimientos, JoinType.INNER);
        predicates.add(cb.equal(joinMovimiento.get(Movimiento_.tipo), movimientoDto.getTipoMovimiento()));
      }
      if (movimientoDto.getIntervaloImporte() != null) {
        predicates = busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinContrato().join(Contrato_.movimientos, JoinType.INNER).get(Movimiento_.importe), movimientoDto.getIntervaloImporte());
      }

      if (movimientoDto.getIntervaloFechas() != null && movimientoDto.getIntervaloFechas().size() > 0) {
        for (IntervaloFecha busqueda : movimientoDto.getIntervaloFechas()) {
          switch (busqueda.getDescripcion()) {
            case "valor":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContrato().join(Contrato_.movimientos, JoinType.INNER).get(Movimiento_.fechaValor), busqueda);
              break;
            case "contable":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinContrato().join(Contrato_.movimientos, JoinType.INNER).get(Movimiento_.fechaContable), busqueda);
              break;
            default:
              break;
          }
        }
      }
    }

    // SECCIÓN ESTIMACIONES
    if (estimacionesDto != null) {
      Join<Expediente, Estimacion> joinEstimacion;
      if (estimacionesDto.getIntervaloFechas() != null && estimacionesDto.getIntervaloFechas().size() > 0) {
        joinEstimacion = busquedaBuilderPN.getJoinEstimacion();
        for (IntervaloFecha busqueda : estimacionesDto.getIntervaloFechas()) {
          switch (busqueda.getDescripcion()) {
            case "creacion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, joinEstimacion.get(Estimacion_.fecha), busqueda);
              break;
            case "resolucion":
              predicates = busquedaBuilderPN.addIntervaloFechas(predicates, joinEstimacion.get(Estimacion_.fechaEstimada), busqueda);
              break;
            default:
              break;
          }
        }
      }
      if (estimacionesDto.getTipoPropuesta() != null) {
        joinEstimacion = busquedaBuilderPN.getJoinEstimacion();
        predicates.add(cb.equal(joinEstimacion.get(Estimacion_.tipo), estimacionesDto.getTipoPropuesta()));
      }
      if (estimacionesDto.getSubtipoPropuesta() != null) {
        joinEstimacion = busquedaBuilderPN.getJoinEstimacion();
        predicates.add(cb.equal(joinEstimacion.get(Estimacion_.subtipo), estimacionesDto.getSubtipoPropuesta()));
      }
      if (estimacionesDto.getProbalididadResolucion() != null) {
        joinEstimacion = busquedaBuilderPN.getJoinEstimacion();
        predicates.add(cb.equal(joinEstimacion.get(Estimacion_.probabilidadResolucion), estimacionesDto.getProbalididadResolucion()));
      }
    }

    // SECCION PROCEDIMIENTO
    if (procedimientoDto != null) {
      Join<BienPosesionNegociada, ProcedimientoPosesionNegociada> joinProcedimiento;
      // Revisar intervalofechas
      if (procedimientoDto.getLetrado() != null) {
        joinProcedimiento = busquedaBuilderPN.getJoinProcedimiento();
        predicates.add(cb.equal(joinProcedimiento.get(ProcedimientoPosesionNegociada_.letrado), procedimientoDto.getLetrado()));
      }
      if (procedimientoDto.getProcurador() != null) {
        joinProcedimiento = busquedaBuilderPN.getJoinProcedimiento();
        predicates.add(cb.equal(joinProcedimiento.get(ProcedimientoPosesionNegociada_.procurador), procedimientoDto.getProcurador()));
      }
      if (procedimientoDto.getJuzgado() != null) {
        joinProcedimiento = busquedaBuilderPN.getJoinProcedimiento();
        predicates.add(cb.equal(joinProcedimiento.get(ProcedimientoPosesionNegociada_.juzgado), procedimientoDto.getJuzgado()));
      }
      if (procedimientoDto.getAdministradorConcursal() != null) {
        var joinProcedimientoGestionAmistosa = busquedaBuilderPN.getJoinProcedimientoGestionAmistosa();
        predicates.add(cb.equal(joinProcedimientoGestionAmistosa.get(Procedimiento_.nombreAdminConcursal), procedimientoDto.getAdministradorConcursal()));
      }
      if (procedimientoDto.getTipoProcedimiento() != null) {
        joinProcedimiento = busquedaBuilderPN.getJoinProcedimiento();
        predicates.add(cb.equal(joinProcedimiento.get(ProcedimientoPosesionNegociada_.tipo), procedimientoDto.getTipoProcedimiento()));
      }
      if (procedimientoDto.getHitoProcedimiento() != null) {
        var joinProcedimientoGestionAmistosa = busquedaBuilderPN.getJoinProcedimientoGestionAmistosa();
        predicates.add(cb.equal(joinProcedimientoGestionAmistosa.get(Procedimiento_.hitoProcedimiento), procedimientoDto.getHitoProcedimiento()));
      }
      if (procedimientoDto.getIntervaloFecha() != null) {
        // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
        predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
      }
      if (procedimientoDto.getNumeroAutos() != null && !procedimientoDto.getNumeroAutos().equals("")) {
        joinProcedimiento = busquedaBuilderPN.getJoinProcedimiento();
        predicates.add(cb.like(joinProcedimiento.get(ProcedimientoPosesionNegociada_.AUTOS), '%' + procedimientoDto.getNumeroAutos() + '%'));
      }
    }

    // SECCION PROPUESTAS
    if (propuestaDto != null) {
      Join<ExpedientePosesionNegociadaCase, Propuesta> joinPropuesta;
      // seleccionamos sólo las propuestas activas
      var filtrarPropuestasActivas = false;
      if (propuestaDto.getFormalizacion() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.formalizaciones), propuestaDto.getFormalizacion()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getClase() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.clasePropuesta), propuestaDto.getClase()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getTipo() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.tipoPropuesta), propuestaDto.getTipo()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getSubtipo() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.subtipoPropuesta), propuestaDto.getSubtipo()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getEstado() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.estadoPropuesta), propuestaDto.getEstado()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getSancion() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.sancionPropuesta), propuestaDto.getSancion()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getResolucion() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.resolucionPropuesta), propuestaDto.getResolucion()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getGestor() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.like(joinPropuesta.get(Propuesta_.usuario).get(Usuario_.nombre), propuestaDto.getGestor()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getAnalista() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.like(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.analistaCliente), propuestaDto.getAnalista()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getGestorRecuperaciones() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.like(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.gestorRecuperacionCliente), propuestaDto.getGestorRecuperaciones()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }
      if (propuestaDto.getNotaria() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.like(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.direccionNotaria), propuestaDto.getNotaria()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }

      if (propuestaDto.getFormalizacion() != null) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        if (propuestaDto.getFormalizacion().getGestor() != null){
          predicates.add(cb.like(joinPropuesta.join(Propuesta_.EXPEDIENTE_POSESION_NEGOCIADA).get(ExpedientePosesionNegociada_.ASIGNACIONES).get(AsignacionExpediente_.USUARIO).get(Usuario_.NOMBRE), propuestaDto.getFormalizacion().getGestor()));
        }
        if (propuestaDto.getFormalizacion().getGestorCliente() != null)
          predicates.add(cb.like(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.gestorFormalizacionCliente), propuestaDto.getFormalizacion().getGestorCliente()));
        if (propuestaDto.getFormalizacion().getDocumentacionCumplimentada() != null){
          if (propuestaDto.getFormalizacion().getDocumentacionCumplimentada())
            predicates.add(cb.equal(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.ESTADO_CHECKLIST).get(EstadoChecklist_.CODIGO), "ok"));
          else
            predicates.add(cb.notEqual(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.ESTADO_CHECKLIST).get(EstadoChecklist_.CODIGO), "ok"));
        }
        if (propuestaDto.getFormalizacion().getPbc() != null)
          predicates.add(cb.equal(joinPropuesta.join(Propuesta_.formalizaciones).get(Formalizacion_.pbc), propuestaDto.getFormalizacion().getPbc()));
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
      }

      // TODO hay que treminar el refactor
      if (propuestaDto.getIntervaloImporte() != null) {
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
        switch (propuestaDto.getIntervaloImporte().getDescripcion()) {
          case "deuda":
            Subquery<Double> subqueryDeudaImpagada = cb.createQuery().subquery(Double.class);
            Root<Contrato> importesContratos = subqueryDeudaImpagada.from(Contrato.class);
            predicates = busquedaBuilderPN.addIntervaloImporte(predicates, subqueryDeudaImpagada.select(cb.sum(importesContratos.get(Contrato_.deudaImpagada))).distinct(true).where(cb.equal(busquedaBuilderPN.getJoinContratosPropuestas().get(Propuesta_.ID), importesContratos.join(Contrato_.propuestas).get(Propuesta_.id))), propuestaDto.getIntervaloImporte());
            break;
          case "propuesta":
            busquedaBuilderPN.addIntervaloImporte(predicates, busquedaBuilderPN.getJoinPropuestas().get(Propuesta_.importeTotal), propuestaDto.getIntervaloImporte());
            break;
          case "quita":
            Subquery<Double> subqueryQuita = cb.createQuery().subquery(Double.class);
            Root<ImportePropuesta> importes = subqueryQuita.from(ImportePropuesta.class);
            busquedaBuilderPN.addIntervaloImporte(predicates, subqueryQuita.select(cb.sum(importes.get(ImportePropuesta_.condonacionDeuda))).distinct(true).where(cb.equal(busquedaBuilderPN.getJoinImportesPropuestas().get(Propuesta_.ID), importes.join(ImportePropuesta_.propuestas).get(Propuesta_.id))), propuestaDto.getIntervaloImporte());
            break;
          case "prestamoInicial":
            Subquery<Double> subqueryPrestamoInicial = cb.createQuery().subquery(Double.class);
            Root<Contrato> importesContratos2 = subqueryPrestamoInicial.from(Contrato.class);
            busquedaBuilderPN.addIntervaloImporte(predicates, subqueryPrestamoInicial.select(cb.sum(importesContratos2.get(Contrato_.importeDispuesto))).distinct(true).where(cb.equal(busquedaBuilderPN.getJoinContratosPropuestas().get(Propuesta_.ID), importesContratos2.join(Contrato_.propuestas).get(Propuesta_.id))), propuestaDto.getIntervaloImporte());
            break;
          case "valoracion":
            // este campo no aplica a PN y se añade el join con -1 para que devuelva una lista vacia
            predicates.add(cb.equal(root.get(ExpedientePosesionNegociada_.id), -1));
            break;
          default:
            break;
        }
      }

      if (propuestaDto.getIntervaloFechas() != null && propuestaDto.getIntervaloFechas().size() > 0) {
        if (!filtrarPropuestasActivas) filtrarPropuestasActivas = true;
        for (IntervaloFecha busqueda : propuestaDto.getIntervaloFechas()) {
          switch (busqueda.getDescripcion()) {
            case "alta":
              busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinPropuestas().get(Propuesta_.fechaAlta), busqueda);
              break;
            case "sancion":
              busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinFormalizacionPropuesta().get(Formalizacion_.fechaSancion), busqueda);
              break;
            case "firma":
              busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinFormalizacionPropuesta().get(Formalizacion_.fechaFirma), busqueda);
              break;
            case "ultimoCambio":
              busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinPropuestas().get(Propuesta_.fechaUltimoCambio), busqueda);
              break;
            case "posesion":
              busquedaBuilderPN.addIntervaloFechas(predicates, busquedaBuilderPN.getJoinFormalizacionPropuesta().get(Formalizacion_.fechaPosesion), busqueda);
              break;
            default:
              break;
          }
        }
      }
      if (filtrarPropuestasActivas) {
        joinPropuesta = busquedaBuilderPN.getJoinPropuesta();
        predicates.add(cb.equal(joinPropuesta.get(Propuesta_.activo), true));
      }
    }

    // SECCIÓN AGENDA sería a partir de UsuarioAgenda
    if (agendaDto != null) {
      if (agendaDto.getEmisor() != null || agendaDto.getDestinatario() != null || agendaDto.getEstado() != null || agendaDto.getIntervaloFechas() != null || agendaDto.getClaseEvento() != null) {
        List<Integer> expedientesIds = getExpedienteFilteredByAgenda(usuario, agendaDto);
        predicates.add(root.get(ExpedientePosesionNegociada_.id).in(expedientesIds));
      }
    }
    return predicates;
  }

  private List<Integer> getExpedienteFilteredByAgenda(Usuario loggedUser, BusquedaAgendaDto busquedaAgendaDto) throws NotFoundException {
    List<Evento> eventos = eventoUtil.getListFilteredEventos(loggedUser.getId(), busquedaAgendaDto);
    List<Integer> expedientes = null;
    if (eventos != null) {
      expedientes = new ArrayList<>();
      for (Evento evento : eventos) {
        switch (evento.getNivel().getCodigo()) {
          case "EXP":
            Expediente expediente = expedienteRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("expediente", evento.getIdNivel()));
            if (!expedientes.contains(expediente.getId())) expedientes.add(expediente.getId());
            break;
          case "CON":
            Contrato contrato = contratoRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("contrato", evento.getIdNivel()));
            if (contrato.getExpediente() != null && !expedientes.contains(contrato.getExpediente().getId()))
              expedientes.add(contrato.getExpediente().getId());
            break;
          case "BIEN":
            Bien bien = bienRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("bien", evento.getIdNivel()));
            List<ContratoBien> contratosBien = new ArrayList<>(bien.getContratos());
            if (contratosBien != null) {
              for (ContratoBien cb : contratosBien) {
                if (bien.getId().equals(cb.getBien().getId()) && cb.getContrato().getExpediente() != null && !expedientes.contains(cb.getContrato().getExpediente().getId()))
                  expedientes.add(cb.getContrato().getExpediente().getId());
              }
            }
            break;
          case "INTE":
            Interviniente interviniente = intervinienteRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("interviniente", evento.getIdNivel()));
            List<ContratoInterviniente> contratosInterviniente = new ArrayList<>(interviniente.getContratos());
            if (contratosInterviniente != null) {
              for (ContratoInterviniente ci : contratosInterviniente) {
                if (interviniente.getId().equals(ci.getInterviniente().getId()) && ci.getContrato().getExpediente() != null && !expedientes.contains(ci.getContrato().getExpediente().getId()))
                  expedientes.add(ci.getContrato().getExpediente().getId());
              }
            }
            break;
          case "JUD":
            Procedimiento procedimiento = procedimientoRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("procedimiento", evento.getIdNivel()));
            List<Contrato> contratosProcedimiento = new ArrayList<>(procedimiento.getContratos());
            if (contratosProcedimiento != null) {
              for (Contrato c : contratosProcedimiento) {
                if (c.getExpediente() != null && !expedientes.contains(c.getExpediente().getId()))
                  expedientes.add(c.getExpediente().getId());
              }
            }
            break;
          case "PRO":
            Propuesta propuesta = propuestaRepository.findById(evento.getIdNivel()).orElseThrow(() -> new NotFoundException("propuesta", evento.getIdNivel()));
            List<Contrato> contratosPropuesta = new ArrayList<>(propuesta.getContratos());
            if (propuesta.getExpediente() != null) {
              if (!expedientes.contains(propuesta.getExpediente().getId()))
                expedientes.add(propuesta.getExpediente().getId());
            } else if (contratosPropuesta != null) {
              for (Contrato c : contratosPropuesta) {
                if (c.getExpediente() != null && !expedientes.contains(c.getExpediente().getId()))
                  expedientes.add(c.getExpediente().getId());
              }
            }
            break;
          default:
            break;
        }
      }
    }
    return expedientes;
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public ExpedientePosesionNegociadaDTO addExpediente(Usuario usuario, ExpedientePosesionNegociadaInputDTO expedientePosesionNegociadaDTO) throws Exception {
    Cartera cartera = null;
    boolean rem = false;
    if (expedientePosesionNegociadaDTO.getCarteraRem() != null) {
      cartera = carteraRepository.findByNombreAndPrincipalIsTrue(expedientePosesionNegociadaDTO.getCarteraRem()).orElseThrow(() -> new Exception("Cartera: " + expedientePosesionNegociadaDTO.getCarteraRem() + " NO encontrada"));
      rem = true;
    } else {
      cartera = carteraRepository.findById(expedientePosesionNegociadaDTO.getIdCartera()).orElseThrow(() -> new Exception("Cartera: " + expedientePosesionNegociadaDTO.getIdCartera() + " NO encontrada"));
    }
    if (!cartera.getActivo())
      throw new Exception("Cartera: " + expedientePosesionNegociadaDTO.getIdCartera() + "  establecida como inactiva");

    ExpedientePosesionNegociada expediente = expedientePosesionNegociadaMapper.getExpediente(expedientePosesionNegociadaDTO, cartera);

    List<BienPosesionNegociadaInputDto> bienesNuevos = new ArrayList<>();
    List<Integer> bienesExistentes = new ArrayList<>();
    for (var bien : expedientePosesionNegociadaDTO.getBienes()) {
      if (bien.getId() == null) bienesNuevos.add(bien);
      else bienesExistentes.add(bien.getId());
    }

    expediente = expedientePosesionNegociadaRepository.saveAndFlush(expediente);
    guardaAsignacionExpediente(usuario, cartera, expediente);
    if (expedientePosesionNegociadaDTO.getComentarios() != null) {
      for (var coment : expedientePosesionNegociadaDTO.getComentarios()) {
        ComentarioExpedientePosesionNegociada comentarioExpedientePosesionNegociada = new ComentarioExpedientePosesionNegociada();
        comentarioExpedientePosesionNegociada.setActivo(true);
        comentarioExpedientePosesionNegociada.setComentario(coment);
        comentarioExpedientePosesionNegociada.setExpediente(expediente);
        comentarioExpedientePosesionNegociada.setUsuario(usuario);
        comentarioExpedientePosesionNegociada.setFechaComentario(new Date());
        comentarioExpedientePosesionNegociadaRepository.saveAndFlush(comentarioExpedientePosesionNegociada);
      }
    }
    bienPosesionNegociadaUseCase.create(expediente, bienesNuevos, bienesExistentes,rem);

    ExpedientePosesionNegociadaDTO output = expedientePosesionNegociadaMapper.parse(expediente);
    return output;
  }

  private List<ExpedientePosesionNegociada> findAllFiltered(Usuario usuario, String idExpediente, String origen, String carteraTipo, String gestor, String titular, String cliente, String cartera, String saldo, Integer numeroBienes, String estado, String orderField, String orderDirection, String direccion, String localidad, String provincia, int size, int page) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<ExpedientePosesionNegociada> query = cb.createQuery(ExpedientePosesionNegociada.class);

    Root<ExpedientePosesionNegociada> root = query.from(ExpedientePosesionNegociada.class);
    List<Predicate> predicates = new ArrayList<>();

    // COMPRUEBA USUARIO LOGGEADO
    if (usuario != null && !usuario.esAdministrador()) {
      Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> join = root.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
      Predicate asignaciones = cb.equal(join.get(AsignacionExpedientePosesionNegociada_.usuario).get(Usuario_.id), usuario.getId());
      Predicate predicateFinal = asignaciones;
      if (usuario.getPerfil() != null && usuario.getPerfil().getNombre().compareTo(Perfil.GESTOR) == 0) {
        Subquery<Integer> subquerySuplente = cb.createQuery().subquery(Integer.class);
        subquerySuplente.distinct(true);
        Root<ExpedientePosesionNegociada> rootSuplentes = subquerySuplente.from(ExpedientePosesionNegociada.class);
        Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> joinSuplente = rootSuplentes.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
        Predicate predicateSuplente = cb.equal(joinSuplente.join(AsignacionExpedientePosesionNegociada_.SUPLENTE, JoinType.INNER).get(Usuario_.ID), usuario.getId());
        Subquery<Integer> subquerySuplente2 = subquerySuplente.select(rootSuplentes.get(ExpedientePosesionNegociada_.id)).distinct(true).where(predicateSuplente);
        subquerySuplente2.distinct(true);

        predicateFinal = cb.or(asignaciones, root.get(ExpedientePosesionNegociada_.ID).in(subquerySuplente2.distinct(true)));
      }
      predicates.add(predicateFinal);
    }

    if (idExpediente != null)
      predicates.add(cb.like(root.get(ExpedientePosesionNegociada_.ID_CONCATENADO), "%" + idExpediente + "%"));

    if (origen != null)
      predicates.add(cb.like(root.get(ExpedientePosesionNegociada_.origen).get(OrigenExpedientePosesionNegociada_.valor), "%" + origen + "%"));

    if (carteraTipo != null)
      predicates.add(cb.like(root.join(ExpedientePosesionNegociada_.cartera).get(Cartera_.tipoActivos), "%" + carteraTipo + "%"));

    if (gestor != null) {
      Join<ExpedientePosesionNegociada, AsignacionExpedientePosesionNegociada> join = root.join(ExpedientePosesionNegociada_.asignaciones, JoinType.INNER);
      Predicate onCond = cb.equal(join.get(AsignacionExpedientePosesionNegociada_.usuario).get(Usuario_.perfil).get(Perfil_.NOMBRE), "Gestor");

      Join<AsignacionExpedientePosesionNegociada, Usuario> join2 = join.join(AsignacionExpedientePosesionNegociada_.usuario, JoinType.INNER);
      Predicate onCond2 = cb.like(join2.get(Usuario_.NOMBRE), "%" + gestor + "%");

      predicates.add(cb.and(onCond, onCond2));
    }

    if (cliente != null)
      predicates.add(cb.like(root.join(ExpedientePosesionNegociada_.cartera).join(Cartera_.clientes).join(ClienteCartera_.cliente).get(Cliente_.nombre), "%" + cliente + "%"));
    if (cartera != null)
      predicates.add(cb.like(root.get(ExpedientePosesionNegociada_.cartera).get(Cartera_.NOMBRE), "%" + cartera + "%"));
    if (estado != null)
      predicates.add(cb.like(root.join(ExpedientePosesionNegociada_.estadoExpedienteRecuperacionAmistosa).get(EstadoExpedientePosesionNegociada_.VALOR), "%" + estado + "%"));

    if (saldo != null) {
      Subquery<Double> subqueryI = cb.createQuery().subquery(Double.class);
      Root<BienPosesionNegociada> bienPN = subqueryI.from(BienPosesionNegociada.class);
      var joinBien = bienPN.join(BienPosesionNegociada_.bien).get(Bien_.importeBien);

      predicates.add(cb.like(subqueryI.select(cb.sum(joinBien)).where(cb.equal(bienPN.get(BienPosesionNegociada_.expedientePosesionNegociada), root.get(ExpedientePosesionNegociada_.id))).as(String.class), "%" + saldo + "%"));
    }
    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()])).distinct(true);
    if (orderField != null) {
      switch (orderField) {
        case "id":
          rs.orderBy(getOrden(cb, root.get(ExpedientePosesionNegociada_.id), orderDirection));
          break;
        case "origen":
          rs.orderBy(getOrden(cb, root.get(ExpedientePosesionNegociada_.origen).get(OrigenExpedientePosesionNegociada_.valor), orderDirection));
          break;
        case "tipo":
          rs.orderBy(getOrden(cb, root.join(ExpedientePosesionNegociada_.cartera, JoinType.INNER).get(Cartera_.TIPO_ACTIVOS), orderDirection));
          break;
        case "gestor":
          Join<ExpedientePosesionNegociada_, AsignacionExpedientePosesionNegociada> join41 = root.join(ExpedientePosesionNegociada_.ASIGNACIONES, JoinType.INNER);
          Join<AsignacionExpedientePosesionNegociada, Usuario> join42 = join41.join(AsignacionExpedientePosesionNegociada_.USUARIO, JoinType.INNER);
          rs.orderBy(getOrden(cb, join42.get(Usuario_.nombre), orderDirection));
          break;
        case "cliente":
          Join<ClienteCartera, Cliente> join22 = root.join(ExpedientePosesionNegociada_.cartera, JoinType.INNER).join(Cartera_.clientes, JoinType.INNER).join(ClienteCartera_.cliente, JoinType.INNER);
          rs.orderBy(getOrden(cb, join22.get(Cliente_.NOMBRE), orderDirection));
          break;
        case "cartera":
          rs.orderBy(getOrden(cb, root.join(ExpedientePosesionNegociada_.cartera, JoinType.INNER).get(Cartera_.NOMBRE), orderDirection));
          break;
        case "estado":
          rs.orderBy(getOrden(cb, root.join(ExpedientePosesionNegociada_.estadoExpedienteRecuperacionAmistosa).get(EstadoExpedientePosesionNegociada_.codigo), orderDirection));
          break;
      }
    } else {
      // si no se selecciona ningún campo para ordenar, ordenamos ascendente por id por defecto
      rs.orderBy(getOrden(cb, root.get(ExpedientePosesionNegociada_.id), "asc"));
    }
    var listaResultados = entityManager.createQuery(rs).getResultList();
    if (numeroBienes != null)
      listaResultados = listaResultados.stream().filter(exp -> exp.getBienesPosesionNegociada().size() == numeroBienes).collect(Collectors.toList());
    else {
      if ("numeroBienes".equals(orderField)) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
          listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.getBienesPosesionNegociada().size()).reversed()).collect(Collectors.toList());
        else
          listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.getBienesPosesionNegociada().size())).collect(Collectors.toList());
      } else if ("saldo".equals(orderField)) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
          listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.calculaImporteBien()).reversed()).collect(Collectors.toList());
        else
          listaResultados = listaResultados.stream().sorted(Comparator.comparing((ExpedientePosesionNegociada exp) -> exp.calculaImporteBien())).collect(Collectors.toList());
      }
    }
    return listaResultados;
  }

  private Order getOrden(CriteriaBuilder cb, Expression expresion, String orderDirection) {
    if (orderDirection == null) {
      orderDirection = "desc";
    }
    return orderDirection.equalsIgnoreCase("desc") ? cb.desc(expresion) : cb.asc(expresion);
  }

  public void actualizarPostCarga() throws NotFoundException {
    List<ExpedientePosesionNegociada> sinConcats = expedientePosesionNegociadaRepository.findAllByIdConcatenadoIsNull();
    for (ExpedientePosesionNegociada exp : sinConcats) {
      for (BienPosesionNegociada bpm : exp.getBienesPosesionNegociada()) {
        Bien b = bpm.getBien();
        b.setImporteBien(b.getSaldoValoracion());
        if (b.getLatitud() == null || b.getLongitud() == null) intervinienteUtil.direccionGoogle(b);
        bienRepository.saveAndFlush(b);
        if (b.getIdHaya() != null) {
          bpm.setIdHaya(b.getIdHaya());
          bienPosesionNegociadaRepository.saveAndFlush(bpm);
        }
      }
      String epn = generateConcatenado(exp);
      exp.setIdConcatenado(epn);
      expedientePosesionNegociadaRepository.saveAndFlush(exp);
    }
  }

  @Override
  @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
  public void carga(Usuario logUser, MultipartFile file, Integer idCartera) throws Exception {
    Usuario loggedUser = usuarioRepository.findById(logUser.getId()).orElseThrow(() -> new NotFoundException("Usuario", logUser.getId()));
    Cartera defaultCartera = carteraRepository.findById(idCartera).orElse(null);
    XSSFWorkbook fichero = new XSSFWorkbook(file.getInputStream());
    XSSFSheet hojaExpediente = fichero.getSheet("Expediente PN");
    XSSFSheet hojaBienes = fichero.getSheet("Activos");
    XSSFSheet hojaTasacion = fichero.getSheet("Tasación");
    XSSFSheet hojaValoracion = fichero.getSheet("Valoración");
    XSSFSheet hojaCargas = fichero.getSheet("Cargas");
    XSSFSheet hojaOcupantes = fichero.getSheet("Ocupantes");
    XSSFSheet hojaDirecciones = fichero.getSheet("Dirección");
    XSSFSheet hojaContactos = fichero.getSheet("Contacto");
    XSSFSheet hojaProcedimientos = fichero.getSheet("Judicial");
    System.out.println("Catalogo " + new Date());
    HashMap<String, HashMap<String, Object>> catalogos = procesarCatalogos.procesar(hojaExpediente, hojaBienes, hojaTasacion, hojaValoracion, hojaCargas, hojaOcupantes, hojaDirecciones, hojaContactos, hojaProcedimientos);
    Cartera cartera = carteraRepository.findById(idCartera).orElseThrow(() -> new NotFoundException("cartera", idCartera));
    Usuario responsableCartera = cartera.getResponsableCartera();
    Usuario responsableSkipTracing = cartera.getResponsableSkipTracing();
    Usuario responsableFormalizacion = cartera.getResponsableFormalizacion();
    List<ExpedientePosesionNegociada> expedientesLista = new ArrayList<>();

    System.out.println("ExpedientePosesionNegociada" + new Date());
    List<ExpedientePosesionNegociada> expedientesPN = procesarExpedientes.procesar(hojaExpediente, catalogos);
    for (ExpedientePosesionNegociada expPN : expedientesPN) {
      try {
        System.out.println("ExpedientePosesionNegociada: " + expPN.getIdOrigen() + "," + expPN.getCartera().getId());
        expedientesLista.add(expedientePosesionNegociadaRepository.saveAndFlush(expPN));
      } catch (Exception ex) {
        throw new Exception("Error al guardar el expediente con id : " + expPN.getIdOrigen());
      }
    }
    System.out.println("BienPosesionNegociada" + new Date());
    List<BienPosesionNegociada> bienesPN = procesarBienes.procesar(hojaBienes, defaultCartera, expedientesLista, catalogos);
    List<BienPosesionNegociada> bienesSaved = new ArrayList<>();
    // bienesPN = ComprobarActivos.comprobar(bienesPN);
    for (BienPosesionNegociada bienPN : bienesPN) {
      try {
        bienesSaved.add(bienPosesionNegociadaRepository.save(bienPN));
      } catch (Exception ex) {
        throw new Exception("Error al guardar el bien con id : " + bienPN.getBien().getIdOrigen() + " _ " + bienPN.getBien().getIdHaya());
      }
    }
    // FIXME por lo visto esto no hace falta para esta carga, no se si hara falta para las
    // siguientes
    // Es necesario para posteriores cargas (al menos en PRO porque necesitamos el id_haya para que
    // funcione el gestor documental
    for (BienPosesionNegociada bienPN : bienesSaved) {
      try {
        ClienteCartera cc = bienPN.getExpedientePosesionNegociada().getCartera().getClientes().stream().findFirst().orElse(null);
        servicioMaestro.registrarBien(bienPN.getBien(), cc.getCliente().getNombre());
        bienRepository.save(bienPN.getBien());
      } catch (LockedChangeException e) {
        bienPN.getBien().setIdHaya(null);
      }
    }

    System.out.println("Tasacion" + new Date());
    List<Tasacion> tasaciones = procesarTasaciones.procesar(hojaTasacion, catalogos);
    // tasaciones = ComprobarTasaciones.comprobar(tasaciones);
    try {
      tasaciones.forEach(tas -> {
        tasacionRepository.save(tas);
      });
    } catch (Exception ex) {
      throw new Exception("Error al guardar tasacion");
    }
    System.out.println("Valoracion" + new Date());
    List<Valoracion> valoraciones = procesarValoraciones.procesar(hojaValoracion, catalogos);
    // valoraciones = ComprobarValoraciones.comprobar(valoraciones);
    try {
      valoraciones.forEach(val -> {
        valoracionRepository.save(val);
      });
    } catch (Exception ex) {
      throw new Exception("Error al guardar valoracion");
    }
    System.out.println("Carga" + new Date());
    List<Carga> cargas = procesarCargas.procesar(hojaCargas, catalogos);
    // cargas = ComprobarCargas.comprobar(cargas);
    try {
      cargas.forEach(carga -> {
        carga.setTipoCarga(tipoCargaRepository.findById(carga.getTipoCarga().getId()).orElse(null));
        cargasRepository.save(carga);
      });
    } catch (Exception ex) {
      throw new Exception("Error al guardar cargas");
    }
    System.out.println("Ocupante" + new Date());
    List<Ocupante> ocupantes = procesarOcupantes.procesar(hojaOcupantes, catalogos);
    // ocupantes = ComprobarOcupantes.comprobar(ocupantes);
    try {
      ocupantes.forEach(ocu -> {
        ocupantesRepository.save(ocu);
      });
    } catch (Exception ex) {
      throw new Exception("Error al guardar ocupantes");
    }
    System.out.println("Direccion" + new Date());
    List<Direccion> direcciones = procesarDirecciones.procesar(hojaDirecciones, catalogos);
    //  direcciones = ComprobarDirecciones.comprobar(direcciones);
    try {
      direcciones.forEach(dir -> {
        direccionesRepository.save(dir);
      });
    } catch (Exception ex) {
      throw new Exception("Error al guardar direcciones");
    }
    System.out.println("DatoContacto" + new Date());
    List<DatoContacto> contactos = procesarContactos.procesar(hojaContactos);
    // contactos = ComprobarContactos.comprobar(contactos);
    try {
      contactos.forEach(con -> {
        contactosRepository.save(con);
      });
    } catch (Exception ex) {
      throw new Exception("Error al guardar contactos");
    }

    /**
     * Esta carga no se hara esta parte segun Jesus Alcazar, no se si la siguiente sera necesario.
     * System.out.println("ProcedimientoPosesionNegociada"); List<ProcedimientoPosesionNegociada>
     * procedimientos = procesarProcedimientos.procesar(hojaProcedimientos); try {
     * procedimientos.forEach(pro -> procedimientosRepository.save(pro)); } catch (Exception ex) {
     * throw new Exception("Error al guardar procedimientos"); }*
     */

    // GenerarExcelPN.generarExcelPN(bienEspejoRepository.findId(idCartera), bienesPN, tasaciones,
    // valoraciones, cargas, ocupantes, direcciones, contactos);
    asignacionExpedienteUseCaseImpl.altaAsignacionesCargaPN(loggedUser, expedientesLista, responsableCartera, responsableSkipTracing, responsableFormalizacion);
  }

  @Override
  public ExpedientePosesionNegociadaDTO getExpedienteById(Integer expedienteId) throws Exception {
    var expediente = expedientePosesionNegociadaRepository.findById(expedienteId).orElseThrow(() -> new Exception("No econtrado Expediente posesión negociada con ID: " + expedienteId));

    if (!expediente.getActivo()) {
      throw new Exception("Expediente posesión negociada con ID: " + expedienteId + " Esta inactivo");
    }

    return expedientePosesionNegociadaMapper.parse(expediente);
  }

  public void closeExpediente(Integer idExpediente) throws NotFoundException {
    ExpedientePosesionNegociada epn = expedientePosesionNegociadaRepository.findById(idExpediente).orElse(null);
    Boolean cerrar = true;
    if (epn != null) {
      Set<BienPosesionNegociada> bienesPN = epn.getBienesPosesionNegociada();
      if (bienesPN != null) {
        for (BienPosesionNegociada bpn : bienesPN) {
          Bien b = bpn.getBien();
          if (b != null && !b.getTramitado()) {
            cerrar = false;
            break;
          }
        }
      }

      if (cerrar) {
        EstadoExpedientePosesionNegociada eepn = estadoExpedientePosesionNegociadaRepository.findByCodigo("CER").orElseThrow(() -> new NotFoundException("Estado Expediente Posesion Negociada", "código", "'CER'"));
        epn.setEstadoExpedienteRecuperacionAmistosa(eepn);
        expedientePosesionNegociadaRepository.save(epn);
      }
    }
  }

  @Override
  public void masiveEstrategiaUpdatePN(Integer idEstrategia, List<Integer> idsExpedientes, List<Integer> idsBienesPN, boolean todos, Usuario usuario, BusquedaDto busquedaDto) throws Exception {

    List<Integer> ids = new ArrayList<>();
    Estrategia estrategia = estrategiaRepository.findById(idEstrategia).orElseThrow(() -> new NotFoundException("estrategia", idEstrategia));

    var tempCriteriaBuilder = entityManager.getCriteriaBuilder();
    var query = tempCriteriaBuilder.createQuery(ExpedientePosesionNegociada.class);
    List<Predicate> predicates = new ArrayList<>();
    BusquedaBuilderPN busquedaBuilderPN = new BusquedaBuilderPN(query.from(ExpedientePosesionNegociada.class), tempCriteriaBuilder);

    predicates = getDataExpedientesPN(predicates, busquedaDto, usuario, busquedaBuilderPN);

    var root = busquedaBuilderPN.getRoot();
    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    if (todos) {
      if (busquedaDto == null) {
        throw new Exception("Hace falta enviar el objeto BusquedaDTO si se hace una petición para cambiar todas las estrategias");
      }
      // List<ExpedientePosesionNegociada>expedientePosesionNegociadaSinPaginar=query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
      List<ExpedientePosesionNegociada> expedientePosesionNegociadaSinPaginar = entityManager.createQuery(rs).getResultList();

      for (ExpedientePosesionNegociada exp : expedientePosesionNegociadaSinPaginar) {
        for (var x : exp.getBienesPosesionNegociada()) {
          ids.add(x.getId());
        }
      }
    } else {
      ids = idsBienesPN;
    }
    // Este findall cambiarlo le cuesta demasiado al menos para la prueba de editar la estrategia
    // asignada del expediente PN
    List<BienPosesionNegociada> listbienesExpediente = new ArrayList<>();
    List<BienPosesionNegociada> listaBienesPNExpedienteCompleta = new ArrayList<>();
    for (Integer idExpedientePN : idsExpedientes) {
      listbienesExpediente = bienPosesionNegociadaRepository.findAllByExpedientePosesionNegociadaId(idExpedientePN);

      System.out.println(listbienesExpediente);
      for (BienPosesionNegociada nuevoBien : listbienesExpediente) {
        listaBienesPNExpedienteCompleta.add(nuevoBien);
      }
    }
    // List<BienPosesionNegociada>bienesExpediente=bienPosesionNegociadaRepository.findAllByexpedientePosesionNegociada();

    /*List<BienPosesionNegociada> todosBienes = bienPosesionNegociadaRepository.findAll();*/
    var ea2 = new EstrategiaAsignada();
    ea2.setOrigenEstrategia(origenEstrategiaRepository.findByCodigo("GM").orElse(null));
    ea2.setTipoEstrategia(tipoEstrategiaRepository.findByCodigo("M").orElse(null));
    ea2.setPeriodoEstrategia(periodoEstrategiaRepository.findByCodigo("M").orElse(null));
    EstrategiaAsignada eaSaved = estrategiaAsignadaRepository.save(ea2);

    if (!ids.isEmpty()) {
      for (Integer idBienPN : ids) {
        for (BienPosesionNegociada nuevoBien : listaBienesPNExpedienteCompleta) {
          if (nuevoBien.getId() != null) {
            if (nuevoBien.getId().equals(idBienPN)) {
              EstrategiaAsignada ea = nuevoBien.getEstrategia();
              if (ea == null) {
                ea = eaSaved;
              }
              ea.setEstrategia(estrategia);
              if (ea.getFecha() == null) {
                ea.setFecha(new Date());
              }
              nuevoBien.setEstrategia(ea);
            }
          }
        }
      }
    }
    bienPosesionNegociadaRepository.saveAll(listbienesExpediente);
  }

  @Override
  public ExpedientePosesionNegociadaDTO updateModoGestion(Integer expedienteId, Integer estado, Integer subEstado, Usuario usuario) throws Exception {
    ExpedientePosesionNegociada expediente = expedientePosesionNegociadaRepository.findById(expedienteId).get();
    // actualizamos fecha de cierre de expediente
    TipoEstado nuevoEstado = this.findByTipoEstado(estado);
    SubtipoEstado subtipoEstado = subtipoEstadoRepository.findById(subEstado).orElse(null);
    List<BienPosesionNegociada> bienPosesionNegociadaList = expediente.getBienesPosesionNegociada().stream().collect(Collectors.toList());
    if (nuevoEstado.getCodigo().equals("05")) expediente.setFechaCierre(new Date());
    expediente.setTipoEstado(nuevoEstado);
    expediente.setSubEstado(subEstado != null ? this.findByTipoSubEstado(subEstado) : null);

    for (BienPosesionNegociada bienPosesionNegociada : bienPosesionNegociadaList) {
      bienPosesionNegociada.setTipoEstado(nuevoEstado);
      bienPosesionNegociada.setSubEstado(subEstado != null ? this.findByTipoSubEstado(subEstado) : null);
      bienPosesionNegociadaRepository.save(bienPosesionNegociada);
    }

    //crear evento
    EventoInputDTO evento = new EventoInputDTO();
    evento.setCartera(expediente.getCartera().getId());
    evento.setIdExpediente(expediente.getId());
    evento.setDestinatario(usuario.getId());
    evento.setClaseEvento(3);
    TipoEvento tipoEvento = tipoEventoRepository.findByCodigo("ACT_04").orElse(null);
    evento.setTipoEvento(tipoEvento.getId());
    evento.setFechaLimite(new Date());
    evento.setFechaAlerta(new Date());
    evento.setFechaCreacion(new Date());
    SubtipoEvento se = subtipoEventoRepository.findByCodigoAndCarteraId("MGES",expediente.getCartera().getId()).orElseThrow(() ->
      new NotFoundException("SubtipoEvento MGES","Cartera" + expediente.getCartera().getId()));
    evento.setSubtipoEvento(se.getId());
    evento.setImportancia(2);
    evento.setNivel(7);
    evento.setNivelId(expediente.getId());
    evento.setTitulo("Cambio modo gestión");
    Locale loc = LocaleContextHolder.getLocale();
    String comentario = "";
    if (loc.getLanguage().equals("en")) {
      if(subtipoEstado != null) comentario = ", "+subtipoEstado.getValorIngles();
      evento.setComentariosDestinatario(nuevoEstado.getValorIngles()+comentario);
    } else {
      if(subtipoEstado != null) comentario = ", "+subtipoEstado.getValor();
      evento.setComentariosDestinatario(nuevoEstado.getValor()+comentario);
    }
    eventoUseCase.create(evento,usuario);

    return expedientePosesionNegociadaMapper.parse(expedientePosesionNegociadaRepository.save(expediente));
  }

  @Override
  public TipoEstado findByTipoEstado(Integer estado) throws Exception {
    Optional<TipoEstado> tipoEstadoOptional = tipoEstadoRepository.findById(estado);
    if (tipoEstadoOptional.isEmpty()) throw new NotFoundException("Tipo de estado", estado);
    return tipoEstadoOptional.get();
  }

  @Override
  public SubtipoEstado findByTipoSubEstado(Integer subEstado) throws Exception {
    Optional<SubtipoEstado> tipoSubEstadoOptional = subtipoEstadoRepository.findById(subEstado);
    if (tipoSubEstadoOptional.isEmpty()) throw new NotFoundException("Subestado", subEstado);
    return tipoSubEstadoOptional.get();
  }

  public void concatenar() {
    List<ExpedientePosesionNegociada> all = expedientePosesionNegociadaRepository.findAll();
    for (ExpedientePosesionNegociada epn : all) {
      String concatenado = generateConcatenado(epn);
      epn.setIdConcatenado(concatenado);
    }
    expedientePosesionNegociadaRepository.saveAll(all);
  }

  private String generateConcatenado(ExpedientePosesionNegociada epn) {
    String result = null;
    Cartera cartera = epn.getCartera();
    List<ClienteCartera> clientes = new ArrayList<>(cartera.getClientes());
    Cliente cliente = clientes.get(0).getCliente();
    List<Bien> listaOrdenada = epn.getBienesPosesionNegociada().stream().map(BienPosesionNegociada::getBien).sorted(Comparator.comparingDouble(Bien::getSaldoValoracion).reversed()).collect(Collectors.toList());

    if (listaOrdenada.size() > 0) {
      Bien primero = listaOrdenada.get(0);
      if (primero.getIdCarga() != null)
        result = generarNoHeredado(cliente.getId(), cartera.getId(), primero.getIdCarga());
    }

    return result;
  }

  public StringBuilder generarConcatenado(Integer cliente, Integer cartera) {
    StringBuilder result = new StringBuilder();
    String clienteS = cliente.toString();
    for (Integer sice = clienteS.length(); sice < 4; sice++) {
      result.append("0");
    }
    result.append(clienteS);
    String carteraS = cartera.toString();
    for (Integer sice = carteraS.length(); sice < 4; sice++) {
      result.append("0");
    }
    result.append(carteraS);

    return result;
  }

  private String generarNoHeredado(Integer cliente, Integer cartera, String carga) {
    StringBuilder result = generarConcatenado(cliente, cartera);

    if (carga.length() == 10) result.append(carga);
    else if (carga.length() > 10) result.append(carga.substring(carga.length() - 10));
    else {
      for (Integer sice = carga.length(); sice < 10; sice++) {
        result.append("0");
      }
      result.append(carga);
      ;
    }
    return result.toString();
  }
}
