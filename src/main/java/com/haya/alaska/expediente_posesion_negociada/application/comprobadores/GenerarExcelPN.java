package com.haya.alaska.expediente_posesion_negociada.application.comprobadores;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.valoracion.domain.Valoracion;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class GenerarExcelPN {

  @Value("${service.warehouse.csvs}")
  private static String csvsFolder;

  public static void generarExcelPN(String carteraId, List<BienPosesionNegociada> bienesPN, List<Tasacion> tasaciones, List<Valoracion> valoraciones, List<Carga> cargas, List<Ocupante> ocupantes, List<Direccion> direcciones, List<DatoContacto> contactos) throws IOException {
    int e = 1;
    int duplicadas = 0;
    int importe = 0;
    int fechaNull = 0;

    List<BienPosesionNegociada> bienesPN1 = ComprobarActivos.getListaExcluidos();
    List<DatoContacto> contactoPN = ComprobarContactos.getListaExcluidos();
    List<Direccion> direccionesPN = ComprobarDirecciones.getListaExcluidos();
    List<Tasacion> tasacionPN = ComprobarTasaciones.getListaExcluidos();
    List<Valoracion> valoracionPN = ComprobarValoraciones.getListaExcluidos();
    /**
     * Borrar Ficheros Antiguos a Dos Meses
     * */
    /*Calendar fechaViejo = Calendar.getInstance();
    fechaViejo.add(fechaViejo.MONTH,-2);
    String archcsvinterviniente = "C:/Pruebas/Controles_carga_para_revisión_Informe_Resumen_PN.xls";
    File ficheroViejo = new File(archcsvinterviniente);
    if (ficheroViejo.exists()){
      ficheroViejo.delete();
    }*/

    Date fecha = new Date();
    SimpleDateFormat objSDF = new SimpleDateFormat("dd/MM/yyyy");
    String archcsvinterviniente = csvsFolder + "Controles_carga_para_revisión_Informe_Resumen_PN-" + objSDF.format(fecha) + ".xls";
    HSSFWorkbook objWB = new HSSFWorkbook();

    HSSFSheet hoja1 = objWB.createSheet("Informe_Resumen");
    HSSFSheet hoja2 = objWB.createSheet("Activos_PN");
    HSSFSheet hoja3 = objWB.createSheet("Tasación_PN");
    HSSFSheet hoja4 = objWB.createSheet("Valoración_PN");
    HSSFSheet hoja5 = objWB.createSheet("Carga_PN");
    HSSFSheet hoja6 = objWB.createSheet("Direccion_PN");
    HSSFSheet hoja7 = objWB.createSheet("Contacto_PN");

    CellStyle cellStyleCabecera = objWB.createCellStyle();
    Font cellFont = objWB.createFont();
    cellFont.setBold(true);
    cellFont.setItalic(true);
    cellStyleCabecera.setFont(cellFont);

    CellStyle cellStyleSub = objWB.createCellStyle();
    Font cellFontSub = objWB.createFont();
    cellFontSub.setUnderline(HSSFFont.U_SINGLE);
    cellStyleSub.setFont(cellFontSub);

    CellStyle cellStyleSub1 = objWB.createCellStyle();
    Font cellFontSub1 = objWB.createFont();
    cellFontSub1.setBold(true);
    cellFontSub1.setUnderline(HSSFFont.U_SINGLE);
    cellStyleSub1.setFont(cellFontSub1);

    HSSFRow fila = hoja1.createRow((short)1);
    HSSFCell celda = fila.createCell((short)0);
    celda.setCellValue("Fecha de carga ");
    celda = fila.createCell((short)1);
    celda.setCellValue(objSDF.format(fecha));

    HSSFRow fila1 = hoja1.createRow((short)2);
    celda = fila1.createCell((short)0);
    celda.setCellValue("Cartera ");
    celda = fila1.createCell((short)1);
    celda.setCellValue(carteraId);

    HSSFRow fila2 = hoja1.createRow((short)3);
    celda = fila2.createCell((short)0);
    celda.setCellValue("Cliente ");
    celda = fila2.createCell((short)1);
    celda.setCellValue(carteraId);

    HSSFRow fila3 = hoja1.createRow((short)5);
    celda = fila3.createCell((short)0);
    celda.setCellStyle(cellStyleCabecera);
    celda.setCellValue("Totales ");

    HSSFRow fila4 = hoja1.createRow((short)6);
    celda = fila4.createCell((short)0);
    celda.setCellValue("Número de registros origen ");
    celda = fila4.createCell((short)1);
    celda.setCellValue(bienesPN.size() +
      tasaciones.size() + direcciones.size() + valoraciones.size() + cargas.size() +
      ocupantes.size() + contactos.size() + bienesPN1.size() +
      contactoPN.size() + direccionesPN.size() + tasacionPN.size() +
      valoracionPN.size()
    );

    HSSFRow fila5 = hoja1.createRow((short)7);
    celda = fila5.createCell((short)0);
    celda.setCellValue("Número de registros cargados ok ");
    celda = fila5.createCell((short)1);
    celda.setCellValue(bienesPN.size() +
      tasaciones.size() + direcciones.size() + valoraciones.size() + cargas.size() +
      ocupantes.size() + contactos.size()
    );

    HSSFRow fila6 = hoja1.createRow((short)8);
    celda = fila6.createCell((short)0);
    celda.setCellValue("Número de resgitros con incidencia ");
    celda = fila6.createCell((short)1);
    celda.setCellValue(bienesPN1.size() +
      contactoPN.size() + direccionesPN.size() + tasacionPN.size() +
      valoracionPN.size()
    );

    HSSFRow fila7 = hoja1.createRow((short)9);
    celda = fila7.createCell((short)0);
    celda.setCellStyle(cellStyleCabecera);
    celda.setCellValue("Incidencias ");

    HSSFRow fila8 = hoja1.createRow((short)10);
    celda = fila8.createCell((short)0);
    celda.setCellStyle(cellStyleSub1);
    celda.setCellValue("Bienes");

    HSSFRow fila9 = hoja1.createRow((short)11);
    celda = fila9.createCell((short)0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Datos identificativos ");

    HSSFRow fila10 = hoja1.createRow((short)12);
    celda = fila10.createCell((short)0);
    celda.setCellValue("Mismo ID_Bien con datos identificativos distintos ");
    celda = fila10.createCell((short)1);
    celda.setCellValue(bienesPN1.size());

    HSSFRow filaBien = hoja2.createRow((short)0);
    celda = filaBien.createCell((short)0);
    celda.setCellValue("Incidencia ");
    celda = filaBien.createCell((short) 1);
    celda.setCellValue("id _Bien");
    celda = filaBien.createCell((short) 2);
    celda.setCellValue("id_expediente");
    celda = filaBien.createCell((short) 3);
    celda.setCellValue("ID_HAYA");
    celda = filaBien.createCell((short) 4);
    celda.setCellValue("id_origen");
    celda = filaBien.createCell((short) 5);
    celda.setCellValue("id_origen2");
    celda = filaBien.createCell((short) 6);
    celda.setCellValue("tipo_bien");
    celda = filaBien.createCell((short) 7);
    celda.setCellValue("tipo_garantia");
    celda = filaBien.createCell((short) 8);
    celda.setCellValue("estado");
    celda = filaBien.createCell((short) 9);
    celda.setCellValue("referencia_catastral");
    celda = filaBien.createCell((short) 10);
    celda.setCellValue("finca");
    celda = filaBien.createCell((short) 11);
    celda.setCellValue("localidad_registro");
    celda = filaBien.createCell((short) 12);
    celda.setCellValue("registro");
    celda = filaBien.createCell((short) 13);
    celda.setCellValue("tomo");
    celda = filaBien.createCell((short) 14);
    celda.setCellValue("libro");
    celda = filaBien.createCell((short) 15);
    celda.setCellValue("folio");
    celda = filaBien.createCell((short) 16);
    celda.setCellValue("fecha_inscripcion");
    celda = filaBien.createCell((short) 17);
    celda.setCellValue("idufir");
    celda = filaBien.createCell((short) 18);
    celda.setCellValue("reo_origen");
    celda = filaBien.createCell((short) 19);
    celda.setCellValue("fecha_reo_conversion");
    celda = filaBien.createCell((short) 20);
    celda.setCellValue("Importe_garantizado");
    celda = filaBien.createCell((short) 21);
    celda.setCellValue("responsabilidad_hipotecaria");
    celda = filaBien.createCell((short) 22);
    celda.setCellValue("posesion_negociada");
    celda = filaBien.createCell((short) 23);
    celda.setCellValue("subasta");
    celda = filaBien.createCell((short) 24);
    celda.setCellValue("estado_ocupacion");
    celda = filaBien.createCell((short) 25);
    celda.setCellValue("tipo_via");
    celda = filaBien.createCell((short) 26);
    celda.setCellValue("nombre_via");
    celda = filaBien.createCell((short) 27);
    celda.setCellValue("numero");
    celda = filaBien.createCell((short) 28);
    celda.setCellValue("portal");
    celda = filaBien.createCell((short) 29);
    celda.setCellValue("bloque");
    celda = filaBien.createCell((short) 30);
    celda.setCellValue("escalera");
    celda = filaBien.createCell((short) 31);
    celda.setCellValue("piso");
    celda = filaBien.createCell((short) 32);
    celda.setCellValue("puerta");
    celda = filaBien.createCell((short) 33);
    celda.setCellValue("entorno");
    celda = filaBien.createCell((short) 34);
    celda.setCellValue("municipio");
    celda = filaBien.createCell((short) 35);
    celda.setCellValue("localidad");
    celda = filaBien.createCell((short) 36);
    celda.setCellValue("provincia");
    celda = filaBien.createCell((short) 37);
    celda.setCellValue("comentario_direccion");
    celda = filaBien.createCell((short) 38);
    celda.setCellValue("codigo_postal");
    celda = filaBien.createCell((short) 39);
    celda.setCellValue("pais");
    celda = filaBien.createCell((short) 40);
    celda.setCellValue("longitud");
    celda = filaBien.createCell((short) 41);
    celda.setCellValue("latitud");
    celda = filaBien.createCell((short) 42);
    celda.setCellValue("tipo_activo");
    celda = filaBien.createCell((short) 43);
    celda.setCellValue("uso");
    celda = filaBien.createCell((short) 44);
    celda.setCellValue("año_construccion");
    celda = filaBien.createCell((short) 45);
    celda.setCellValue("superficie_suelo");
    celda = filaBien.createCell((short) 46);
    celda.setCellValue("superficie_construida");
    celda = filaBien.createCell((short) 46);
    celda.setCellValue("superficie_util");
    celda = filaBien.createCell((short) 47);
    celda.setCellValue("anejo_garaje");
    celda = filaBien.createCell((short) 48);
    celda.setCellValue("anejo_trastero");
    celda = filaBien.createCell((short) 49);
    celda.setCellValue("anejo_otros");
    celda = filaBien.createCell((short) 50);
    celda.setCellValue("notas1");
    celda = filaBien.createCell((short) 51);
    celda.setCellValue("notas2");

    e=1;
    for (BienPosesionNegociada activo : bienesPN1) {
      HSSFRow filaBien2 = hoja2.createRow((short) e);
      celda = filaBien2.createCell((short)0);
      celda.setCellValue("Mismo ID_Bien con datos identificativos distintos ");
      celda = filaBien2.createCell((short) 1);
      if(activo.getBien()!=null) {
        if(activo.getBien().getIdCarga()!=null) {
          celda.setCellValue(activo.getBien().getIdCarga());
        }
      }
      celda = filaBien2.createCell((short) 2);
      if(activo.getExpedientePosesionNegociada()!=null) {
        if(activo.getExpedientePosesionNegociada().getIdOrigen()!=null) {
          celda.setCellValue(activo.getExpedientePosesionNegociada().getIdOrigen());
        }
      }
      celda = filaBien2.createCell((short) 3);
      celda.setCellValue(activo.getIdHaya());
      celda = filaBien2.createCell((short) 4);
      if(activo.getBien()!=null) {
        if(activo.getBien().getIdOrigen()!=null) {
          celda.setCellValue(activo.getBien().getIdOrigen());
        }
      }
      celda = filaBien2.createCell((short) 5);
      if(activo.getBien()!=null) {
        if(activo.getBien().getIdOrigen2()!=null) {
          celda.setCellValue(activo.getBien().getIdOrigen2());
        }
      }
      celda = filaBien2.createCell((short) 6);
      if(activo.getBien()!=null) {
        if(activo.getBien().getTipoBien()!=null) {
          if(activo.getBien().getTipoBien().getCodigo()!=null) {
            celda.setCellValue(activo.getBien().getTipoBien().getCodigo());
          }
        }
      }
      celda = filaBien2.createCell((short) 7);
      if(activo.getTipoGarantia()!=null){
        if(activo.getTipoGarantia().getCodigo()!=null){
          celda.setCellValue(activo.getTipoGarantia().getCodigo());
        }
      }
      celda = filaBien2.createCell((short) 8);
      if(activo.getEstadoOcupacion()!=null) {
        if(activo.getEstadoOcupacion().getCodigo()!=null) {
          celda.setCellValue(activo.getEstadoOcupacion().getCodigo());
        }
      }
      celda = filaBien2.createCell((short) 9);
      if(activo.getBien()!=null) {
        if(activo.getBien().getReferenciaCatastral()!=null) {
          celda.setCellValue(activo.getBien().getReferenciaCatastral());
        }
      }
      celda = filaBien2.createCell((short) 10);
      if(activo.getBien()!=null) {
        if(activo.getBien().getFinca()!=null) {
          celda.setCellValue(activo.getBien().getFinca());
        }
      }
      celda = filaBien2.createCell((short) 11);
      if(activo.getBien()!=null) {
        if(activo.getBien().getLocalidadRegistro()!=null) {
          celda.setCellValue(activo.getBien().getLocalidadRegistro());
        }
      }
      celda = filaBien2.createCell((short) 12);
      if(activo.getBien()!=null) {
        if(activo.getBien().getRegistro()!=null) {
          celda.setCellValue(activo.getBien().getRegistro());
        }
      }
      celda = filaBien2.createCell((short) 13);
      if(activo.getBien()!=null) {
        if(activo.getBien().getTomo()!=null) {
          celda.setCellValue(activo.getBien().getTomo());
        }
      }
      celda = filaBien2.createCell((short) 14);
      if(activo.getBien()!=null) {
        if(activo.getBien().getLibro()!=null) {
          celda.setCellValue(activo.getBien().getLibro());
        }
      }
      celda = filaBien2.createCell((short) 15);
      if(activo.getBien()!=null) {
        if(activo.getBien().getFolio()!=null) {
          celda.setCellValue(activo.getBien().getFolio());
        }
      }
      celda = filaBien2.createCell((short) 16);
      if(activo.getBien()!=null) {
        if(activo.getBien().getFechaInscripcion()!=null) {
          celda.setCellValue(activo.getBien().getFechaInscripcion());
        }
      }
      celda = filaBien2.createCell((short) 17);
      if(activo.getBien()!=null) {
        if(activo.getBien().getIdufir()!=null) {
          celda.setCellValue(activo.getBien().getIdufir());
        }
      }
      celda = filaBien2.createCell((short) 18);
      if(activo.getBien()!=null) {
        if(activo.getBien().getReoOrigen()!=null) {
          celda.setCellValue(activo.getBien().getReoOrigen());
        }
      }
      celda = filaBien2.createCell((short) 19);
      if(activo.getBien()!=null) {
        if(activo.getBien().getFechaReoConversion()!=null) {
          celda.setCellValue(activo.getBien().getFechaReoConversion());
        }
      }
      celda = filaBien2.createCell((short) 20);
      if(activo.getBien()!=null) {
        if(activo.getBien().getImporteGarantizado()!=null) {
          celda.setCellValue(activo.getBien().getImporteGarantizado());
        }
      }
      celda = filaBien2.createCell((short) 21);
      if(activo.getBien()!=null) {
        if(activo.getBien().getResponsabilidadHipotecaria()!=null) {
          celda.setCellValue(activo.getBien().getResponsabilidadHipotecaria());
        }
      }
      celda = filaBien2.createCell((short) 22);
      if(activo.getBien()!=null) {
        if(activo.getBien().getPosesionNegociada()!=null) {
          celda.setCellValue(activo.getBien().getPosesionNegociada());
        }
      }
      celda = filaBien2.createCell((short) 23);
      if(activo.getBien()!=null) {
        if(activo.getBien().getSubasta()!=null) {
          celda.setCellValue(activo.getBien().getSubasta());
        }
      }
      celda = filaBien2.createCell((short) 24);
      if(activo.getEstadoOcupacion()!=null) {
        if(activo.getEstadoOcupacion().getCodigo()!=null) {
          celda.setCellValue(activo.getEstadoOcupacion().getCodigo());
        }
      }
      celda = filaBien2.createCell((short) 25);
      if(activo.getBien()!=null) {
        if(activo.getBien().getTipoVia()!=null) {
          if(activo.getBien().getTipoVia().getCodigo()!=null) {
            celda.setCellValue(activo.getBien().getTipoVia().getCodigo());
          }
        }
      }
      celda = filaBien2.createCell((short) 26);
      if(activo.getBien()!=null) {
        if(activo.getBien().getNombreVia()!=null) {
          celda.setCellValue(activo.getBien().getNombreVia());
        }
      }
      celda = filaBien2.createCell((short) 27);
      if(activo.getBien()!=null) {
        if(activo.getBien().getNumero()!=null) {
          celda.setCellValue(activo.getBien().getNumero());
        }
      }
      celda = filaBien2.createCell((short) 28);
      if(activo.getBien()!=null) {
        if(activo.getBien().getPortal()!=null) {
          celda.setCellValue(activo.getBien().getPortal());
        }
      }
      celda = filaBien2.createCell((short) 29);
      if(activo.getBien()!=null) {
        if(activo.getBien().getBloque()!=null) {
          celda.setCellValue(activo.getBien().getBloque());
        }
      }
      celda = filaBien2.createCell((short) 30);
      if(activo.getBien()!=null) {
        if(activo.getBien().getEscalera()!=null) {
          celda.setCellValue(activo.getBien().getEscalera());
        }
      }
      celda = filaBien2.createCell((short) 31);
      if(activo.getBien()!=null) {
        if(activo.getBien().getPiso()!=null) {
          celda.setCellValue(activo.getBien().getPiso());
        }
      }
      celda = filaBien2.createCell((short) 32);
      if(activo.getBien()!=null) {
        if(activo.getBien().getPuerta()!=null) {
          celda.setCellValue(activo.getBien().getPuerta());
        }
      }
      celda = filaBien2.createCell((short) 33);
      if(activo.getBien()!=null) {
        if(activo.getBien().getEntorno()!=null) {
          if(activo.getBien().getEntorno().getCodigo()!=null) {
            celda.setCellValue(activo.getBien().getEntorno().getCodigo());
          }
        }
      }
      celda = filaBien2.createCell((short) 34);
      if(activo.getBien()!=null) {
        if(activo.getBien().getMunicipio()!=null) {
          if(activo.getBien().getMunicipio().getCodigo()!=null) {
            celda.setCellValue(activo.getBien().getMunicipio().getCodigo());
          }
        }
      }
      celda = filaBien2.createCell((short) 35);
      if(activo.getBien()!=null) {
        if(activo.getBien().getLocalidad()!=null) {
          if(activo.getBien().getLocalidad().getCodigo()!=null) {
            celda.setCellValue(activo.getBien().getLocalidad().getCodigo());
          }
        }
      }
      celda = filaBien2.createCell((short) 36);
      if(activo.getBien()!=null) {
        if(activo.getBien().getProvincia()!=null) {
          if(activo.getBien().getProvincia().getCodigo()!=null) {
            celda.setCellValue(activo.getBien().getProvincia().getCodigo());
          }
        }
      }
      celda = filaBien2.createCell((short) 37);
      if(activo.getBien()!=null) {
        if (activo.getBien().getComentarioDireccion() != null) {
          celda.setCellValue(activo.getBien().getComentarioDireccion());
        }
      }
      celda = filaBien2.createCell((short) 38);
      if(activo.getBien()!=null) {
        if (activo.getBien().getCodigoPostal() != null) {
          celda.setCellValue(activo.getBien().getCodigoPostal());
        }
      }
      celda = filaBien2.createCell((short) 39);
      if(activo.getBien()!=null) {
        if (activo.getBien().getPais() != null) {
          if(activo.getBien().getPais().getCodigo()!=null){
            celda.setCellValue(activo.getBien().getPais().getCodigo());
          }
        }
      }
      celda = filaBien2.createCell((short) 40);
      if(activo.getBien()!=null) {
        if (activo.getBien().getLongitud() != null) {
          celda.setCellValue(activo.getBien().getLongitud());
        }
      }
      celda = filaBien2.createCell((short) 41);
      if(activo.getBien()!=null) {
        if (activo.getBien().getLatitud() != null) {
          celda.setCellValue(activo.getBien().getLatitud());
        }
      }
      celda = filaBien2.createCell((short) 42);
      if(activo.getBien()!=null) {
        if (activo.getBien().getTipoActivo() != null) {
          if(activo.getBien().getTipoActivo().getCodigo()!=null){
            celda.setCellValue(activo.getBien().getTipoActivo().getCodigo());
          }
        }
      }
      celda = filaBien2.createCell((short) 43);
      if(activo.getBien()!=null) {
        if (activo.getBien().getUso() != null) {
          if(activo.getBien().getUso().getCodigo()!=null){
            celda.setCellValue(activo.getBien().getUso().getCodigo());
          }
        }
      }
      celda = filaBien2.createCell((short) 44);
      if(activo.getBien()!=null) {
        if (activo.getBien().getAnioConstruccion() != null) {
          celda.setCellValue(activo.getBien().getAnioConstruccion());
        }
      }
      celda = filaBien2.createCell((short) 45);
      if(activo.getBien()!=null) {
        if (activo.getBien().getSuperficieSuelo() != null) {
          celda.setCellValue(activo.getBien().getSuperficieSuelo());
        }
      }
      celda = filaBien2.createCell((short) 46);
      if(activo.getBien()!=null) {
        if (activo.getBien().getSuperficieConstruida() != null) {
          celda.setCellValue(activo.getBien().getSuperficieConstruida());
        }
      }
      celda = filaBien2.createCell((short) 46);
      if(activo.getBien()!=null) {
        if (activo.getBien().getSuperficieUtil() != null) {
          celda.setCellValue(activo.getBien().getSuperficieUtil());
        }
      }
      celda = filaBien2.createCell((short) 47);
      if(activo.getBien()!=null) {
        if (activo.getBien().getAnejoGaraje() != null) {
          celda.setCellValue(activo.getBien().getAnejoGaraje());
        }
      }
      celda = filaBien2.createCell((short) 48);
      if(activo.getBien()!=null) {
        if (activo.getBien().getAnejoTrastero() != null) {
          celda.setCellValue(activo.getBien().getAnejoTrastero());
        }
      }
      celda = filaBien2.createCell((short) 49);
      if(activo.getBien()!=null) {
        if (activo.getBien().getAnejoOtros() != null) {
          celda.setCellValue(activo.getBien().getAnejoOtros());
        }
      }
      celda = filaBien2.createCell((short) 50);
      if(activo.getBien()!=null) {
        if (activo.getBien().getNotas1() != null) {
          celda.setCellValue(activo.getBien().getNotas1());
        }
      }
      celda = filaBien2.createCell((short) 51);
      if(activo.getBien()!=null) {
        if (activo.getBien().getNotas1() != null) {
          celda.setCellValue(activo.getBien().getNotas2());
        }
      }
      e++;
    }

    HSSFRow filaTasacion = hoja3.createRow((short)0);
    celda = filaTasacion.createCell((short)0);
    celda.setCellValue("Incidencia");
    celda = filaTasacion.createCell((short) 1);
    celda.setCellValue("id_bien");
    celda = filaTasacion.createCell((short) 2);
    celda.setCellValue("tasadora");
    celda = filaTasacion.createCell((short) 3);
    celda.setCellValue("tipo_tasacion");
    celda = filaTasacion.createCell((short) 4);
    celda.setCellValue("importe_tasacion");
    celda = filaTasacion.createCell((short) 5);
    celda.setCellValue("fecha_tasacion");
    celda = filaTasacion.createCell((short) 6);
    celda.setCellValue("Liquidez");

    duplicadas=0;
    importe=0;
    fechaNull=0;
    e=1;
    for (Tasacion activo : tasacionPN) {
      HSSFRow filaValidacion2 = hoja3.createRow((short) e);
      if(activo.getImporte()!=null) {
        if (tasacionPN.contains(activo)) {
          celda = filaValidacion2.createCell((short) 0);
          celda.setCellValue("Tasación duplicada");
          celda = filaValidacion2.createCell((short) 1);
          celda.setCellValue(activo.getBien().getIdCarga());
          celda = filaValidacion2.createCell((short) 2);
          celda.setCellValue(activo.getTasadora());
          celda = filaValidacion2.createCell((short) 3);
          if (activo.getTipoTasacion()!=null) {
            if (activo.getTipoTasacion().getCodigo()!=null) {
              celda.setCellValue(activo.getTipoTasacion().getCodigo());
            }
          }
          celda = filaValidacion2.createCell((short) 4);
          celda.setCellValue(activo.getImporte());
          celda = filaValidacion2.createCell((short) 5);
          celda.setCellValue(activo.getFecha());
          celda = filaValidacion2.createCell((short) 6);
          if (activo.getLiquidez()!=null) {
            if (activo.getLiquidez().getCodigo()!=null) {
              celda.setCellValue(activo.getLiquidez().getCodigo());
            }
          }
          duplicadas++;
          e++;
        }
      }
    }

    for (int i = 0; i<tasacionPN.size();i++){
      Tasacion tasacionPN1 = tasacionPN.get(i);
      if(tasacionPN1.getImporte()==null){
        HSSFRow filaValidacion2 = hoja3.createRow((short) e);
        celda = filaValidacion2.createCell((short) 0);
        celda.setCellValue("Importe de tasación nulo");
        celda = filaValidacion2.createCell((short) 1);
        celda.setCellValue(tasacionPN1.getBien().getIdCarga());
        celda = filaValidacion2.createCell((short) 2);
        celda.setCellValue(tasacionPN1.getTasadora());
        celda = filaValidacion2.createCell((short) 3);
        if (tasacionPN1.getTipoTasacion()!=null) {
          if (tasacionPN1.getTipoTasacion().getCodigo()!=null) {
            celda.setCellValue(tasacionPN1.getTipoTasacion().getCodigo());
          }
        }
        celda = filaValidacion2.createCell((short) 4);
        celda.setCellValue("");
        celda = filaValidacion2.createCell((short) 5);
        celda.setCellValue(tasacionPN1.getFecha());
        celda = filaValidacion2.createCell((short) 6);
        if (tasacionPN1.getLiquidez()!=null) {
          if (tasacionPN1.getLiquidez().getCodigo()!=null) {
            celda.setCellValue(tasacionPN1.getLiquidez().getCodigo());
          }
        }
        importe++;
        e++;
      }
      if(tasacionPN1.getFecha()==null){
        HSSFRow filaValidacion2 = hoja3.createRow((short) e);
        celda = filaValidacion2.createCell((short) 0);
        celda.setCellValue("Fecha de tasación nulo");
        celda = filaValidacion2.createCell((short) 1);
        celda.setCellValue(tasacionPN1.getBien().getIdCarga());
        celda = filaValidacion2.createCell((short) 2);
        celda.setCellValue(tasacionPN1.getTasadora());
        celda = filaValidacion2.createCell((short) 3);
        if (tasacionPN1.getTipoTasacion()!=null) {
          if (tasacionPN1.getTipoTasacion().getCodigo()!=null) {
            celda.setCellValue(tasacionPN1.getTipoTasacion().getCodigo());
          }
        }
        celda = filaValidacion2.createCell((short) 4);
        if(tasacionPN1.getImporte()!=null){
          celda.setCellValue(tasacionPN1.getImporte());
        }
        celda = filaValidacion2.createCell((short) 5);
        celda.setCellValue(tasacionPN1.getFecha());
        celda = filaValidacion2.createCell((short) 6);
        if (tasacionPN1.getLiquidez()!=null) {
          if (tasacionPN1.getLiquidez().getCodigo()!=null) {
            celda.setCellValue(tasacionPN1.getLiquidez().getCodigo());
          }
        }
        fechaNull++;
        e++;
      }
    }

    HSSFRow fila11 = hoja1.createRow((short)13);
    celda = fila11.createCell((short)0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Tasación ");

    HSSFRow fila12 = hoja1.createRow((short)14);
    celda = fila12.createCell((short)0);
    celda.setCellValue("Tasaciones duplicadas");
    celda = fila12.createCell((short)1);
    celda.setCellValue(duplicadas);

    HSSFRow fila13 = hoja1.createRow((short)15);
    celda = fila13.createCell((short)0);
    celda.setCellValue("Importe de tasación nulo");
    celda = fila13.createCell((short)1);
    celda.setCellValue(importe);

    HSSFRow fila14 = hoja1.createRow((short)16);
    celda = fila14.createCell((short)0);
    celda.setCellValue("Fecha de tasación nulo");
    celda = fila14.createCell((short)1);
    celda.setCellValue(fechaNull);

    HSSFRow filaValidacion = hoja4.createRow((short)0);
    celda = filaValidacion.createCell((short)0);
    celda.setCellValue("Incidencia");
    celda = filaValidacion.createCell((short) 1);
    celda.setCellValue("id_bien");
    celda = filaValidacion.createCell((short) 2);
    celda.setCellValue("valorador");
    celda = filaValidacion.createCell((short) 3);
    celda.setCellValue("tipo_valoracion");
    celda = filaValidacion.createCell((short) 4);
    celda.setCellValue("importe_valoracion");
    celda = filaValidacion.createCell((short) 5);
    celda.setCellValue("fecha_valoracion");
    celda = filaValidacion.createCell((short) 6);
    celda.setCellValue("Liquidez");

    duplicadas=0;
    importe=0;
    fechaNull=0;
    e=1;
    for (Valoracion activo : valoracionPN) {
        HSSFRow filaValidacion2 = hoja4.createRow((short) e);
      if(activo.getImporte()!=null) {
        if (valoracionPN.contains(activo)) {
          celda = filaValidacion2.createCell((short) 0);
          celda.setCellValue("Validaciones duplicada");
          celda = filaValidacion2.createCell((short) 1);
          celda.setCellValue(activo.getBien().getIdCarga());
          celda = filaValidacion2.createCell((short) 2);
          celda.setCellValue(activo.getValorador());
          celda = filaValidacion2.createCell((short) 3);
          if (activo.getTipoValoracion() == null) {
            celda.setCellValue("");
          } else {
            celda.setCellValue(activo.getTipoValoracion().getCodigo());
          }
          celda = filaValidacion2.createCell((short) 4);
          celda.setCellValue(activo.getImporte());
          celda = filaValidacion2.createCell((short) 5);
          celda.setCellValue(activo.getFecha());
          celda = filaValidacion2.createCell((short) 6);
          if (activo.getLiquidez() == null) {
            celda.setCellValue("");
          } else {
            celda.setCellValue(activo.getLiquidez().getCodigo());
          }
          duplicadas++;
          e++;
        }
      }
    }
    for (int i = 0; i<valoracionPN.size();i++){
      Valoracion valoracionPN1 = valoracionPN.get(i);
      if(valoracionPN1.getImporte()==null){
        HSSFRow filaValidacion2 = hoja4.createRow((short) e);
        celda = filaValidacion2.createCell((short) 0);
        celda.setCellValue("Importe de validación nulo");
        celda = filaValidacion2.createCell((short) 1);
        celda.setCellValue(valoracionPN1.getBien().getIdCarga());
        celda = filaValidacion2.createCell((short) 2);
        celda.setCellValue(valoracionPN1.getValorador());
        celda = filaValidacion2.createCell((short) 3);
        if(valoracionPN1.getTipoValoracion() == null){
          celda.setCellValue("");
        }else {
          celda.setCellValue(valoracionPN1.getTipoValoracion().getCodigo());
        }
        celda = filaValidacion2.createCell((short) 4);
        celda.setCellValue("");
        celda = filaValidacion2.createCell((short) 5);
        celda.setCellValue(valoracionPN1.getFecha());
        celda = filaValidacion2.createCell((short) 6);
        if(valoracionPN1.getLiquidez()==null){
          celda.setCellValue("");
        }else{
        celda.setCellValue(valoracionPN1.getLiquidez().getCodigo());
        }
        importe++;
        e++;
      }
      if(valoracionPN1.getFecha()==null){
        HSSFRow filaValidacion2 = hoja4.createRow((short) e);
        celda = filaValidacion2.createCell((short) 0);
        celda.setCellValue("Fecha de validación nulo");
        celda = filaValidacion2.createCell((short) 1);
        celda.setCellValue(valoracionPN1.getBien().getIdCarga());
        celda = filaValidacion2.createCell((short) 2);
        celda.setCellValue(valoracionPN1.getValorador());
        celda = filaValidacion2.createCell((short) 3);
        if(valoracionPN1.getTipoValoracion() == null){
          celda.setCellValue("");
        }else {
          celda.setCellValue(valoracionPN1.getTipoValoracion().getCodigo());
        }
        celda = filaValidacion2.createCell((short) 4);
        if(valoracionPN1.getImporte()!=null){
          celda.setCellValue(valoracionPN1.getImporte());
        }
        celda = filaValidacion2.createCell((short) 5);
        celda.setCellValue(valoracionPN1.getFecha());
        celda = filaValidacion2.createCell((short) 6);
        if(valoracionPN1.getLiquidez()==null){
          celda.setCellValue("");
        }else{
          celda.setCellValue(valoracionPN1.getLiquidez().getCodigo());
        }
        fechaNull++;
        e++;
      }
    }

    HSSFRow fila15 = hoja1.createRow((short)17);
    celda = fila15.createCell((short)0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Valoración ");

    HSSFRow fila16 = hoja1.createRow((short)18);
    celda = fila16.createCell((short)0);
    celda.setCellValue("Valoracones duplicadas");
    celda = fila16.createCell((short)1);
    celda.setCellValue(duplicadas);

    HSSFRow fila17 = hoja1.createRow((short)19);
    celda = fila17.createCell((short)0);
    celda.setCellValue("Importe de valoración nulo");
    celda = fila17.createCell((short)1);
    celda.setCellValue(importe);

    HSSFRow fila18 = hoja1.createRow((short)20);
    celda = fila18.createCell((short)0);
    celda.setCellValue("Fecha de valoración nulo");
    celda = fila18.createCell((short)1);
    celda.setCellValue(fechaNull);

    HSSFRow fila19 = hoja1.createRow((short)21);
    celda = fila19.createCell((short)0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Carga ");

    HSSFRow fila20 = hoja1.createRow((short)22);
    celda = fila20.createCell((short)0);
    celda.setCellValue("Cargas duplicadas");
    celda = fila20.createCell((short)1);
    celda.setCellValue(cargas.size());

    HSSFRow filaCarga = hoja5.createRow((short)0);
    celda = filaCarga.createCell((short)0);
    celda.setCellValue("Incidencia");
    celda = filaCarga.createCell((short) 1);
    celda.setCellValue("id_bien");
    celda = filaCarga.createCell((short) 2);
    celda.setCellValue("rango_carga");
    celda = filaCarga.createCell((short) 3);
    celda.setCellValue("tipo_carga");
    celda = filaCarga.createCell((short) 4);
    celda.setCellValue("importe_carga");
    celda = filaCarga.createCell((short) 5);
    celda.setCellValue("tipo_acreedor");
    celda = filaCarga.createCell((short) 6);
    celda.setCellValue("nombre_acreedor");
    celda = filaCarga.createCell((short) 7);
    celda.setCellValue("fecha_anotacion");
    celda = filaCarga.createCell((short) 8);
    celda.setCellValue("incluida_otros_colaterales");
    celda = filaCarga.createCell((short) 9);
    celda.setCellValue("fecha_vencimiento_carga");

    e = 1;
    for (int i = 0; i<cargas.size();i++){
      HSSFRow filaCarga2 = hoja5.createRow((short) e);
      celda = filaCarga2.createCell((short) 0);
      celda.setCellValue("Cargas duplicadas");

      Carga cargasPN = cargas.get(i);
      Set<Bien> bienesCarga = cargasPN.getBienes();
      celda = filaCarga2.createCell((short) 1);
      for (Bien bien2 : bienesCarga) {
        celda.setCellValue(bien2.getIdCarga());
      }

      celda = filaCarga2.createCell((short) 2);
      celda.setCellValue(cargasPN.getRango());
      celda = filaCarga2.createCell((short) 3);
      celda.setCellValue(cargasPN.getTipoCarga().getCodigo());
      celda = filaCarga2.createCell((short) 4);
      celda.setCellValue(cargasPN.getImporte());
      celda = filaCarga2.createCell((short) 5);
      if(cargasPN.getTipoAcreedor() == null){
        celda.setCellValue("");
      }else{
        celda.setCellValue(cargasPN.getTipoAcreedor().getCodigo());
      }
      celda = filaCarga2.createCell((short) 6);
      celda.setCellValue(cargasPN.getNombreAcreedor());
      celda = filaCarga2.createCell((short) 7);
      celda.setCellValue(cargasPN.getFechaAnotacion());
      celda = filaCarga2.createCell((short) 8);
      if(cargasPN.getIncluidaOtrosColaterales() == null){
        celda.setCellValue("");
      }else{
        celda.setCellValue(cargasPN.getIncluidaOtrosColaterales());
      }
      celda = filaCarga2.createCell((short) 9);
      celda.setCellValue(cargasPN.getFechaVencimiento());

      e++;
    }

    HSSFRow fila21 = hoja1.createRow((short)23);
    celda = fila21.createCell((short)0);
    celda.setCellStyle(cellStyleSub1);
    celda.setCellValue("Ocupantes");

    HSSFRow filaDireccion = hoja6.createRow((short)0);
    celda = filaDireccion.createCell((short)0);
    celda.setCellValue("Incidencia");
    celda = filaDireccion.createCell((short) 1);
    celda.setCellValue("id_ocupante");
    celda = filaDireccion.createCell((short) 2);
    celda.setCellValue("tipo_via");
    celda = filaDireccion.createCell((short) 3);
    celda.setCellValue("nombre_via");
    celda = filaDireccion.createCell((short) 4);
    celda.setCellValue("numero");
    celda = filaDireccion.createCell((short) 5);
    celda.setCellValue("portal");
    celda = filaDireccion.createCell((short) 6);
    celda.setCellValue("bloque");
    celda = filaDireccion.createCell((short) 7);
    celda.setCellValue("escalera");
    celda = filaDireccion.createCell((short) 8);
    celda.setCellValue("piso");
    celda = filaDireccion.createCell((short) 9);
    celda.setCellValue("puerta");
    celda = filaDireccion.createCell((short) 10);
    celda.setCellValue("entorno");
    celda = filaDireccion.createCell((short) 11);
    celda.setCellValue("municipio");
    celda = filaDireccion.createCell((short) 12);
    celda.setCellValue("localidad");
    celda = filaDireccion.createCell((short) 13);
    celda.setCellValue("provincia");
    celda = filaDireccion.createCell((short) 14);
    celda.setCellValue("comentario_direccion");
    celda = filaDireccion.createCell((short) 15);
    celda.setCellValue("codigo_postal");
    celda = filaDireccion.createCell((short) 16);
    celda.setCellValue("longitud");
    celda = filaDireccion.createCell((short) 17);
    celda.setCellValue("latitud");
    celda = filaDireccion.createCell((short) 18);
    celda.setCellValue("descripción");

    e = 1;
    for (int i = 0; i<direccionesPN.size();i++) {
      HSSFRow filaDireccion2 = hoja6.createRow((short) e);
      Direccion direccionPN1 = direccionesPN.get(i);
      celda = filaDireccion2.createCell((short) 0);
      celda.setCellValue("Mismo ID ocupante con datos de localización iguales");
      celda = filaDireccion2.createCell((short) 1);
      celda.setCellValue(direccionPN1.getOcupante().getIdOcupanteOrigen());
      celda = filaDireccion2.createCell((short) 2);
      if(direccionPN1.getTipoVia()!=null){
        celda.setCellValue(direccionPN1.getTipoVia().getCodigo());
      }
      celda = filaDireccion2.createCell((short) 3);
      celda.setCellValue(direccionPN1.getNombre());
      celda = filaDireccion2.createCell((short) 4);
      celda.setCellValue(direccionPN1.getNumero());
      celda = filaDireccion2.createCell((short) 5);
      celda.setCellValue(direccionPN1.getPortal());
      celda = filaDireccion2.createCell((short) 6);
      celda.setCellValue(direccionPN1.getBloque());
      celda = filaDireccion2.createCell((short) 7);
      celda.setCellValue(direccionPN1.getEscalera());
      celda = filaDireccion2.createCell((short) 8);
      celda.setCellValue(direccionPN1.getPiso());
      celda = filaDireccion2.createCell((short) 9);
      celda.setCellValue(direccionPN1.getPuerta());
      celda = filaDireccion2.createCell((short) 10);
      if(direccionPN1.getEntorno()!=null) {
        celda.setCellValue(direccionPN1.getEntorno().getCodigo());
      }
      celda = filaDireccion2.createCell((short) 11);
      if(direccionPN1.getMunicipio()!=null) {
        celda.setCellValue(direccionPN1.getMunicipio().getCodigo());
      }
      celda = filaDireccion2.createCell((short) 12);
      if(direccionPN1.getLocalidad()!=null) {
        celda.setCellValue(direccionPN1.getLocalidad().getCodigo());
      }
      celda = filaDireccion2.createCell((short) 13);
      if(direccionPN1.getProvincia()!=null) {
        celda.setCellValue(direccionPN1.getProvincia().getCodigo());
      }
      celda = filaDireccion2.createCell((short) 14);
      celda.setCellValue(direccionPN1.getComentario());
      celda = filaDireccion2.createCell((short) 15);
      celda.setCellValue(direccionPN1.getCodigoPostal());
      celda = filaDireccion2.createCell((short) 16);
      if(direccionPN1.getLongitud()!=null) {
        celda.setCellValue(direccionPN1.getLongitud());
      }
      celda = filaDireccion2.createCell((short) 17);
      if(direccionPN1.getLatitud()!=null) {
        celda.setCellValue(direccionPN1.getLatitud());
      }
      celda = filaDireccion2.createCell((short) 18);
      celda.setCellValue("");
      e++;
    }

    HSSFRow fila24 = hoja1.createRow((short)24);
    celda = fila24.createCell((short)0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Datos de localización ");

    HSSFRow fila25 = hoja1.createRow((short)25);
    celda = fila25.createCell((short)0);
    celda.setCellValue("Mismo ID ocupante con datos de localización iguales");
    celda = fila25.createCell((short)1);
    celda.setCellValue(direccionesPN.size());

    HSSFRow filaContacto = hoja7.createRow((short)0);
    celda = filaContacto.createCell((short)0);
    celda.setCellValue("Incidencia");
    celda = filaContacto.createCell((short) 1);
    celda.setCellValue("id_ocupante");
    celda = filaContacto.createCell((short) 2);
    celda.setCellValue("tipo");
    celda = filaContacto.createCell((short) 3);
    celda.setCellValue("valor");
    celda = filaContacto.createCell((short) 4);
    celda.setCellValue("Orden");
    celda = filaContacto.createCell((short) 5);
    celda.setCellValue("descripción");

    e = 1;
    for (int i = 0; i<contactoPN.size();i++) {
      HSSFRow filaContacto2 = hoja7.createRow((short) e);
      DatoContacto contactoPN1 = contactoPN.get(i);
      celda = filaContacto2.createCell((short) 0);
      celda.setCellValue("Mismo ID ocupante con datos de localización iguales");
      celda = filaContacto2.createCell((short) 1);
      celda.setCellValue(contactoPN1.getOcupante().getIdOcupanteOrigen());
      celda = filaContacto2.createCell((short) 2);
      if(contactoPN1.getMovil()!=null){
        celda.setCellValue("1");
      }else if(contactoPN1.getFijo()!=null){
        celda.setCellValue("2");
      }else {
        celda.setCellValue("3");
      }
      celda = filaContacto2.createCell((short) 3);
      if(contactoPN1.getMovil()!=null){
        celda.setCellValue(contactoPN1.getMovil());
      }else if(contactoPN1.getFijo()!=null){
        celda.setCellValue(contactoPN1.getFijo());
      }else {
        celda.setCellValue(contactoPN1.getEmail());
      }
      celda = filaContacto2.createCell((short) 4);
      celda.setCellValue(contactoPN1.getOrden());
      celda = filaContacto2.createCell((short) 5);
      celda.setCellValue("");
      e++;
    }

    HSSFRow fila26 = hoja1.createRow((short)28);
    celda = fila26.createCell((short)0);
    celda.setCellStyle(cellStyleSub);
    celda.setCellValue("Datos de contacto ");

    HSSFRow fila27 = hoja1.createRow((short)29);
    celda = fila27.createCell((short)0);
    celda.setCellValue("Mismo ID Inerviniente con datos de contacto diferentes");
    celda = fila27.createCell((short)1);
    celda.setCellValue(contactoPN.size());

    File objFile = new File(archcsvinterviniente);
    FileOutputStream archivoSalida = new FileOutputStream(objFile);
    objWB.write(archivoSalida);
    archivoSalida.close();
  }
}
