package com.haya.alaska.expediente_posesion_negociada.application.comprobadores;

import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.tipo_carga.domain.TipoCarga;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ComprobarCargas {
  /**
   * Casuística 1	Para un mismo bien, varias cargas con el mismo "rango_carga" y "tipo_carga"
   * Resultado:	Se cargan ambos registros, cambiando al segundo registro el valor del campo "tipo_carga" por un 3 (igualdad de rango)
   **/

  private static boolean isValid(Carga carga, List<Carga> cargas) {
    if (cargas.contains(carga)) {
      TipoCarga tipoCarga = new TipoCarga();
      tipoCarga.setId(3);
      carga.setTipoCarga(tipoCarga);
    }
    cargas.add(carga);
    return true;
  }

  public static List<Carga> comprobar(List<Carga> cargas) {
    List<Carga> listaYaAnadidas = new ArrayList<>();
    return cargas.stream().filter(carga -> isValid(carga, listaYaAnadidas)).collect(Collectors.toList());
  }
}
