package com.haya.alaska.expediente_posesion_negociada.application.procesadores;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.estado_expediente_posesion_negociada.domain.EstadoExpedientePosesionNegociada;
import com.haya.alaska.estado_expediente_posesion_negociada.infastructure.repository.EstadoExpedientePosesionNegociadaRepository;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion;
import com.haya.alaska.estado_ocupacion.infrastructure.repository.EstadoOcupacionRepository;
import com.haya.alaska.estado_procedimiento.domain.EstadoProcedimiento;
import com.haya.alaska.estado_procedimiento.infrastructure.repository.EstadoProcedimientoRepository;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.origen_expediente_posesion_negociada.domain.OrigenExpedientePosesionNegociada;
import com.haya.alaska.origen_expediente_posesion_negociada.repository.OrigenExpedientePosesionNegociadaRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.tipo_accion_procedimiento.domain.TipoAccionProcedimiento;
import com.haya.alaska.tipo_accion_procedimiento.infrastructure.repository.TipoAccionProcedimientoRepository;
import com.haya.alaska.tipo_acreedor.domain.TipoAcreedor;
import com.haya.alaska.tipo_acreedor.infrastructure.repository.TipoAcreedorRepository;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import com.haya.alaska.tipo_carga.infrastructure.repository.TipoCargaRepository;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;
import com.haya.alaska.tipo_garantia.infrastructure.repository.TipoGarantiaRepository;
import com.haya.alaska.tipo_ocupante.domain.TipoOcupante;
import com.haya.alaska.tipo_ocupante.infrastructure.repository.TipoOcupanteRepository;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import com.haya.alaska.tipo_tasacion.domain.TipoTasacion;
import com.haya.alaska.tipo_tasacion.infrastructure.repository.TipoTasacionRepository;
import com.haya.alaska.tipo_valoracion.domain.TipoValoracion;
import com.haya.alaska.tipo_valoracion.infrastructure.repository.TipoValoracionRepository;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.uso.domain.Uso;
import com.haya.alaska.uso.infrastructure.repository.UsoRepository;
import com.haya.alaska.vulnerabilidad.domain.Vulnerabilidad;
import com.haya.alaska.vulnerabilidad.infrastructure.repository.VulnerabilidadRepository;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static com.haya.alaska.shared.util.ExcelImport.getRawStringValue;

@Service
@AllArgsConstructor
public class ProcesarCatalogos {
  private OrigenExpedientePosesionNegociadaRepository origenExpedientePosesionNegociadaRepository;
  private TipoBienRepository tipoBienRepository;
  private TipoGarantiaRepository tipoGarantiaRepository;
  private TipoViaRepository tipoViaRepository;
  private MunicipioRepository municipioRepository;
  private LocalidadRepository localidadRepository;
  private EntornoRepository entornoRepository;
  private ProvinciaRepository provinciaRepository;
  private EstadoOcupacionRepository estadoOcupacionRepository;
  private EstadoExpedientePosesionNegociadaRepository estadoExpedientePosesionNegociadaRepository;
  private UsoRepository usoRepository;
  private TipoActivoRepository tipoActivoRepository;
  private PaisRepository paisRepository;
  private TipoTasacionRepository tipoTasacionRepository;
  private LiquidezRepository liquidezRepository;
  private EstadoProcedimientoRepository estadoProcedimientoRepository;
  private TipoProcedimientoRepository tipoProcedimientoRepository;
  private TipoAccionProcedimientoRepository tipoAccionProcedimientoRepository;
  private VulnerabilidadRepository vulnerabilidadRepository;
  private TipoOcupanteRepository tipoOcupanteRepository;
  private TipoAcreedorRepository tipoAcreedorRepository;
  private TipoCargaRepository tipoCargaRepository;
  private TipoValoracionRepository tipoValoracionRepository;

  public HashMap<String, HashMap<String, Object>> procesar(XSSFSheet hojaExpediente, XSSFSheet hojaBienes, XSSFSheet hojaTasacion, XSSFSheet hojaValoracion, XSSFSheet hojaCargas,
                                                           XSSFSheet hojaOcupantes, XSSFSheet hojaDirecciones, XSSFSheet hojaContactos, XSSFSheet hojaProcedimientos) throws Exception {
    HashMap<String, HashMap<String, Object>> catalogos = iniciarCatalogo();
    procesarHojaExpediente(hojaExpediente, catalogos);
    procesarHojaBienes(hojaBienes, catalogos);
    procesarHojaTasaciones(hojaTasacion, catalogos);
    procesarHojaValoraciones(hojaValoracion, catalogos);
    procesarHojaCargas(hojaCargas, catalogos);
    procesarHojaOcupantes(hojaOcupantes, catalogos);
    procesarHojaDirecciones(hojaDirecciones, catalogos);
    procesarHojaProcedimientos(hojaProcedimientos, catalogos);
    return catalogos;
  }

  private HashMap<String, HashMap<String, Object>> iniciarCatalogo() {
    HashMap<String, HashMap<String, Object>> catalogos = new HashMap<>();
    catalogos.put("OrigenExpedientePosesionNegociada", new HashMap<>());
    catalogos.put("tipoBien", new HashMap<>());
    catalogos.put("tipoGarantia", new HashMap<>());
    catalogos.put("estadoOcupacion", new HashMap<>());
    catalogos.put("tipoVia", new HashMap<>());
    catalogos.put("entorno", new HashMap<>());
    catalogos.put("municipio", new HashMap<>());
    catalogos.put("localidad", new HashMap<>());
    catalogos.put("provincia", new HashMap<>());
    catalogos.put("pais", new HashMap<>());
    catalogos.put("uso", new HashMap<>());
    catalogos.put("estadoExpedientePosesionNegociada", new HashMap<>());
    catalogos.put("tipoTasacion", new HashMap<>());
    catalogos.put("liquidez", new HashMap<>());
    catalogos.put("tipoAccionProcedimiento", new HashMap<>());
    catalogos.put("tipoProcedimiento", new HashMap<>());
    catalogos.put("estadoProcedimiento", new HashMap<>());
    catalogos.put("tipoOcupante", new HashMap<>());
    catalogos.put("vulnerabilidad", new HashMap<>());
    catalogos.put("tipoCarga", new HashMap<>());
    catalogos.put("tipoAcreedor", new HashMap<>());
    catalogos.put("tipoValoracion", new HashMap<>());
    return catalogos;
  }

  private void procesarHojaExpediente(XSSFSheet hojaExpediente, HashMap<String, HashMap<String, Object>> catalogos) throws Exception {
    OrigenExpedientePosesionNegociada origenExpedientePosesionNegociada = origenExpedientePosesionNegociadaRepository.findById(OrigenExpedientePosesionNegociada.NEGOCIADA).orElseThrow(
      () -> new Exception("No encontrado origen expediente posesión negociada con valor 'Negociada'. Debería existir con id: " + OrigenExpedientePosesionNegociada.NEGOCIADA)
    );
    HashMap<String, Object> interno = new HashMap<>();
    interno.put("00", origenExpedientePosesionNegociada);
    catalogos.put("OrigenExpedientePosesionNegociada", interno);
  }

  private void procesarHojaBienes(XSSFSheet hojaBienes, HashMap<String, HashMap<String, Object>> catalogos) throws Exception {
    XSSFRow filaActual;
    for (int i = 6; i <= hojaBienes.getPhysicalNumberOfRows(); i++) {
      filaActual = hojaBienes.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null == hasContent || "".equals(hasContent)) {
        break;
      }
      String tipoBienS = getRawStringValue(filaActual, 7);
      String tipoGarantiaS = getRawStringValue(filaActual, 8);
      String estadoOcupacionS = getRawStringValue(filaActual, 25);
      String tipoViaS = getRawStringValue(filaActual, 26);
      String entornoS = getRawStringValue(filaActual, 34);
      String municipioS = getRawStringValue(filaActual, 35);
      String localidadS = getRawStringValue(filaActual, 36);
      String provinciaS = getRawStringValue(filaActual, 37);
      String paisS = getRawStringValue(filaActual, 40);
      String tipoActivoS = getRawStringValue(filaActual, 43);
      String usoS = getRawStringValue(filaActual, 44);


      if (!comprobarMap(catalogos, tipoBienS, "tipoBien", 0)) {
        TipoBien tipoBien = tipoBienRepository.findById(Integer.valueOf(tipoBienS)).orElse(null);
        anadirMap(catalogos, "tipoBien", tipoBienS, tipoBien);
      }
      if (!comprobarMap(catalogos, tipoGarantiaS, "tipoGarantia", 2)) {
        TipoGarantia tipoGarantia = tipoGarantiaRepository.findByCodigo(tipoGarantiaS).orElse(null);
        anadirMap(catalogos, "tipoGarantia", tipoGarantiaS, tipoGarantia);
      }
      if (!comprobarMap(catalogos, estadoOcupacionS, "estadoOcupacion", 2)) {
        EstadoOcupacion estadoOcupacion = estadoOcupacionRepository.findByCodigo(estadoOcupacionS).orElse(null);
        anadirMap(catalogos, "estadoOcupacion", estadoOcupacionS, estadoOcupacion);
      }
      if (!comprobarMap(catalogos, tipoViaS, "tipoVia", 2)) {
        TipoVia tipoVia = tipoViaRepository.findByCodigo(tipoViaS).orElse(null);
        anadirMap(catalogos, "tipoVia", tipoViaS, tipoVia);
      }
      if (!comprobarMap(catalogos, entornoS, "entorno", 1)) {
        Entorno entorno = entornoRepository.findByValor(entornoS).orElse(null);
        anadirMap(catalogos, "entorno", entornoS, entorno);
      }
      if (!comprobarMap(catalogos, municipioS, "municipio", 2)) {
        Municipio municipio = municipioRepository.findByCodigo(municipioS).orElse(null);
        anadirMap(catalogos, "municipio", municipioS, municipio);
      }
      if (!comprobarMap(catalogos, localidadS, "localidad", 2)) {
        Localidad localidad = localidadRepository.findByCodigo(localidadS).orElse(null);
        anadirMap(catalogos, "localidad", localidadS, localidad);
      }
      if (!comprobarMap(catalogos, provinciaS, "provincia", 2)) {
        Provincia provincia = provinciaRepository.findByCodigo(provinciaS).orElse(null);
        anadirMap(catalogos, "provincia", provinciaS, provincia);
      }
      if (!comprobarMap(catalogos, paisS, "pais", 2)) {
        Pais pais = paisRepository.findByCodigo(paisS).orElse(null);
        anadirMap(catalogos, "pais", paisS, pais);
      }
      if (!comprobarMap(catalogos, usoS, "uso", 2)) {
        Uso uso = usoRepository.findByCodigo(usoS).orElse(null);
        anadirMap(catalogos, "uso", usoS, uso);
      }
      if (!comprobarMap(catalogos, tipoActivoS, "tipoActivo", 0)) {
        TipoActivo tipoActivo = tipoActivoRepository.findById(Integer.valueOf(tipoActivoS)).orElse(null);
        anadirMap(catalogos, "tipoActivo", tipoActivoS, tipoActivo);
      }
    }
    EstadoExpedientePosesionNegociada estadoExpedientePosesionNegociada = estadoExpedientePosesionNegociadaRepository.findByCodigo("REV").orElseThrow(
      () -> new Exception("No encontrado estado expediente posesion negociada con código 'REV'"));
    anadirMap(catalogos, "estadoExpedientePosesionNegociada", "00", estadoExpedientePosesionNegociada);
  }

  private void procesarHojaTasaciones(XSSFSheet hojaTasaciones, HashMap<String, HashMap<String, Object>> catalogos) {
    XSSFRow filaActual;
    for (int i = 6; i <= hojaTasaciones.getPhysicalNumberOfRows(); i++) {
      filaActual = hojaTasaciones.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null == hasContent || "".equals(hasContent)) {
        break;
      }

      String tipoTasacionS = getRawStringValue(filaActual, 4);
      String liquidezS = getRawStringValue(filaActual, 7);

      if (!comprobarMap(catalogos, tipoTasacionS, "tipoTasacion", 2)) {
        TipoTasacion tipoTasacion = tipoTasacionRepository.findByCodigo(tipoTasacionS).orElse(null);
        anadirMap(catalogos, "tipoTasacion", tipoTasacionS, tipoTasacion);
      }
      if (!comprobarMap(catalogos, liquidezS, "liquidez", 2)) {
        Liquidez liquidez = liquidezRepository.findByCodigo(liquidezS).orElse(null);
        anadirMap(catalogos, "liquidez", liquidezS, liquidez);
      }
    }
  }

  private void procesarHojaProcedimientos(XSSFSheet hojaProcedimientos, HashMap<String, HashMap<String, Object>> catalogos) {
    XSSFRow filaActual;
    for (int i = 6; i <= hojaProcedimientos.getPhysicalNumberOfRows(); i++) {
      filaActual = hojaProcedimientos.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null == hasContent || "".equals(hasContent)) {
        break;
      }
      String localidadS = getRawStringValue(filaActual, 6);
      String provinciaS = getRawStringValue(filaActual, 5);
      String tipoAccionProcedimientoS = getRawStringValue(filaActual, 8);
      String tipoProcedimientoS = getRawStringValue(filaActual, 9);
      String estadoProcedimientoS = getRawStringValue(filaActual, 10);

      if (!comprobarMap(catalogos, localidadS, "localidad", 1)) {
        Localidad localidad = localidadRepository.findByValor(localidadS).orElse(null);
        anadirMap(catalogos, "localidad", localidadS, localidad);
      }
      if (!comprobarMap(catalogos, provinciaS, "provincia", 1)) {
        Provincia provincia = provinciaRepository.findByValor(provinciaS).orElse(null);
        anadirMap(catalogos, "provincia", provinciaS, provincia);
      }
      if (!comprobarMap(catalogos, tipoAccionProcedimientoS, "tipoAccionProcedimiento", 2)) {
        TipoAccionProcedimiento tipoAccionProcedimiento = tipoAccionProcedimientoRepository.findByCodigo(tipoAccionProcedimientoS).orElse(null);
        anadirMap(catalogos, "tipoAccionProcedimiento", tipoAccionProcedimientoS, tipoAccionProcedimiento);
      }
      if (!comprobarMap(catalogos, tipoProcedimientoS, "tipoProcedimiento", 2)) {
        TipoProcedimiento tipoProcedimiento = tipoProcedimientoRepository.findByCodigoAndRecoveryIsFalse(tipoProcedimientoS).orElse(null);
        anadirMap(catalogos, "tipoProcedimiento", tipoProcedimientoS, tipoProcedimiento);
      }
      if (!comprobarMap(catalogos, estadoProcedimientoS, "estadoProcedimiento", 2)) {
        EstadoProcedimiento estadoProcedimiento = estadoProcedimientoRepository.findByCodigo(estadoProcedimientoS).orElse(null);
        anadirMap(catalogos, "estadoProcedimiento", estadoProcedimientoS, estadoProcedimiento);
      }
    }
  }

  private void procesarHojaDirecciones(XSSFSheet hojaDirecciones, HashMap<String, HashMap<String, Object>> catalogos) {
    XSSFRow filaActual;
    for (int i = 5; i <= hojaDirecciones.getPhysicalNumberOfRows(); i++) {
      filaActual = hojaDirecciones.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null == hasContent || "".equals(hasContent)) {
        break;
      }

      String entornoS = getRawStringValue(filaActual, 10);
      String municipioS = getRawStringValue(filaActual, 11);
      String localidadS = getRawStringValue(filaActual, 12);
      String provinciaS = getRawStringValue(filaActual, 13);
      String tipoViaS = getRawStringValue(filaActual, 2);
      if (!comprobarMap(catalogos, tipoViaS, "tipoVia", 1)) {
        TipoVia tipoVia = tipoViaRepository.findByValor(tipoViaS).orElse(null);
        anadirMap(catalogos, "tipoVia", tipoViaS, tipoVia);
      }
      if (!comprobarMap(catalogos, entornoS, "entorno", 1)) {
        Entorno entorno = entornoRepository.findByValor(entornoS).orElse(null);
        anadirMap(catalogos, "entorno", entornoS, entorno);
      }
      if (!comprobarMap(catalogos, municipioS, "municipio", 2)) {
        Municipio municipio = municipioRepository.findByCodigo(municipioS).orElse(null);
        anadirMap(catalogos, "municipio", municipioS, municipio);
      }
      if (!comprobarMap(catalogos, localidadS, "localidad", 2)) {
        Localidad localidad = localidadRepository.findByCodigo(localidadS).orElse(null);
        anadirMap(catalogos, "localidad", localidadS, localidad);
      }
      if (!comprobarMap(catalogos, provinciaS, "provincia", 2)) {
        Provincia provincia = provinciaRepository.findByCodigo(provinciaS).orElse(null);
        anadirMap(catalogos, "provincia", provinciaS, provincia);
      }
    }
  }

  private void procesarHojaOcupantes(XSSFSheet hojaOcupantes, HashMap<String, HashMap<String, Object>> catalogos) {
    XSSFRow filaActual;
    for (int i = 6; i <= hojaOcupantes.getPhysicalNumberOfRows(); i++) {
      filaActual = hojaOcupantes.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null == hasContent || "".equals(hasContent)) {
        break;
      }

      String paisS = getRawStringValue(filaActual, 7);
      String tipoOcupanteS = getRawStringValue(filaActual, 14);
      String vulnerabilidadS = getRawStringValue(filaActual, 16);

      if (!comprobarMap(catalogos, paisS, "pais", 2)) {
        Pais pais = paisRepository.findByCodigo(paisS).orElse(null);
        anadirMap(catalogos, "pais", paisS, pais);
      }
      if (!comprobarMap(catalogos, tipoOcupanteS, "tipoOcupante", 2)) {
        TipoOcupante tipoOcupante = tipoOcupanteRepository.findByCodigo(tipoOcupanteS).orElse(null);
        anadirMap(catalogos, "tipoOcupante", tipoOcupanteS, tipoOcupante);
      }
      if (!comprobarMap(catalogos, vulnerabilidadS, "vulnerabilidad", 2)) {
        Vulnerabilidad vulnerabilidad = vulnerabilidadRepository.findByCodigo(vulnerabilidadS).orElse(null);
        anadirMap(catalogos, "vulnerabilidad", vulnerabilidadS, vulnerabilidad);
      }
    }
  }

  private void procesarHojaCargas(XSSFSheet hojaCargas, HashMap<String, HashMap<String, Object>> catalogos) {
    XSSFRow filaActual;
    for (int i = 6; i <= hojaCargas.getPhysicalNumberOfRows(); i++) {
      filaActual = hojaCargas.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null == hasContent || "".equals(hasContent)) {
        break;
      }

      String tipoCargaS = getRawStringValue(filaActual, 4);
      String tipoAcreedorS = getRawStringValue(filaActual, 6);

      if (!comprobarMap(catalogos, tipoCargaS, "tipoCarga", 2)) {
        TipoCarga tipoCarga = tipoCargaRepository.findByCodigo(tipoCargaS).orElse(null);
        anadirMap(catalogos, "tipoCarga", tipoCargaS, tipoCarga);
      }
      if (!comprobarMap(catalogos, tipoAcreedorS, "tipoAcreedor", 2)) {
        TipoAcreedor tipoAcreedor = tipoAcreedorRepository.findByCodigo(tipoAcreedorS).orElse(null);
        anadirMap(catalogos, "tipoAcreedor", tipoAcreedorS, tipoAcreedor);
      }
    }
  }

  private void procesarHojaValoraciones(XSSFSheet hojaValoracion, HashMap<String, HashMap<String, Object>> catalogos) {
    XSSFRow filaActual;
    for (int i = 6; i <= hojaValoracion.getPhysicalNumberOfRows(); i++) {
      filaActual = hojaValoracion.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null == hasContent || "".equals(hasContent)) {
        break;
      }

      String tipoValoracionS = getRawStringValue(filaActual, 4);
      String liquidezS = getRawStringValue(filaActual, 7);

      if (!comprobarMap(catalogos, tipoValoracionS, "tipoValoracion", 2)) {
        TipoValoracion tipoValoracion = tipoValoracionRepository.findByCodigo(tipoValoracionS).orElse(null);
        anadirMap(catalogos, "tipoValoracion", tipoValoracionS, tipoValoracion);
      }
      if (!comprobarMap(catalogos, liquidezS, "liquidez", 2)) {
        Liquidez liquidez = liquidezRepository.findByCodigo(liquidezS).orElse(null);
        anadirMap(catalogos, "liquidez", liquidezS, liquidez);
      }
    }
  }

  //return true si ya se ha cargado
  private boolean comprobarMap(HashMap<String, HashMap<String, Object>> catalogos, String valor, String key, Integer value) {
    HashMap<String, Object> catalogo = catalogos.get(key);
    if (valor == null) return true;
    if (catalogo == null) return false;
    for (Map.Entry<String, Object> catalogoEntry : catalogo.entrySet()) {
      Catalogo catalogo1 = (Catalogo) catalogoEntry.getValue();
      if (value == 0) if (catalogo1.getId().equals(valor)) return true;
      if (value == 1) if (catalogo1.getValor().equals(valor)) return true;
      if (value == 2) if (catalogo1.getCodigo().equals(valor)) return true;
    }
    return false;
  }

  private void anadirMap(HashMap<String, HashMap<String, Object>> catalogos, String key, String value, Catalogo valor) {
    if (valor == null) return;
    HashMap<String, Object> interno = catalogos.get(key);
    if (interno == null) interno = new HashMap<>();
    interno.put(value, valor);
    catalogos.put(key, interno);
  }
}
