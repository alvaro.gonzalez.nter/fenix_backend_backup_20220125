package com.haya.alaska.expediente_posesion_negociada.application.comprobadores;

import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.tasacion.domain.Tasacion;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ComprobarContactos {
  /**
   * Datos de contacto
   * Casuística 1	Mismo ID Ocupante con datos de contacto diferentes
   * Resultado:	Se carga to do, diferenciando por tipo de contacto (email, movil, fijo) y orden. Permitiendo así existir 2 registros para el mismo interviniente y tipo de contacto
   * <p>
   * Casuística 2	Mismo ID Ocupante con datos de contacto iguales
   * Resultado:	Se debería cargar un único registro
   * Asunción 2:	Hacer select distinct de los campos "tipo" y "orden"
   **/
  public static List<DatoContacto> listaExcluidos  = new ArrayList<>(); ;

  public static List<DatoContacto> getListaExcluidos() {
    return listaExcluidos;
  }

  private static boolean isValid(DatoContacto datoContacto, List<DatoContacto> datoContactos) {
    if (datoContactos.contains(datoContacto)) {
      listaExcluidos.add(datoContacto);
      return false;
    }
    datoContactos.add(datoContacto);
    return true;
  }

  public static List<DatoContacto> comprobar(List<DatoContacto> datoContactos) {
    List<DatoContacto> listaYaAnadidas = new ArrayList<>();
    return datoContactos.stream().filter(datoContacto -> isValid(datoContacto, listaYaAnadidas)).collect(Collectors.toList());
  }
}
