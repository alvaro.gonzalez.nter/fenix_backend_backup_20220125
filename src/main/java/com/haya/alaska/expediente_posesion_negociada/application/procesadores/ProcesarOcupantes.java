package com.haya.alaska.expediente_posesion_negociada.application.procesadores;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_ocupante.domain.TipoOcupante;
import com.haya.alaska.vulnerabilidad.domain.Vulnerabilidad;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Service
@AllArgsConstructor
public class ProcesarOcupantes {
  private final int id_bien = 2;
  private final int id_origen = 3;
  private final int ocupante_ = 4;
  private final int nombre = 5;
  private final int apellidos = 6;
  private final int nacionalidad = 7;
  private final int numero_documento = 8;
  private final int menor_de_edad = 9;
  private final int discapacitado = 10;
  private final int dependiente = 11;
  private final int residencia_habitual = 12;
  private final int ingresos_Netos_Mensuales = 13;
  private final int tipo = 14;
  private final int marca_deudor_anterior = 15;
  private final int otras_situaciones_de_vulnerabilidad = 16;
  private final int primera_fila_con_datos = 6;
  private final OcupanteRepository ocupanteRepository;
  private final BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  private final BienRepository bienRepository;
  private HashMap<String, HashMap<String, Object>> catalogos;

  public List<Ocupante> procesar(XSSFSheet hojaOcupantes, HashMap<String, HashMap<String, Object>> catalogos) throws Exception {
    this.catalogos = catalogos;
    List<Ocupante> dev = new ArrayList<>();
    XSSFRow filaActual;
    Ocupante ocupante;
    Ocupante guardado;
    for (int i = primera_fila_con_datos; i <= hojaOcupantes.getPhysicalNumberOfRows(); i++) {
      filaActual = hojaOcupantes.getRow(i);
      if (filaActual == null) continue;
      String dniS = getRawStringValue(filaActual, numero_documento);
      if (dniS != null && !dniS.equals(""))
        ocupante = ocupanteRepository.findByDni(dniS).orElse(null);
      else ocupante = ocupanteRepository.findByidOcupanteOrigen(
        getRawStringValue(filaActual, id_origen)).orElse(null);

      if (null == ocupante) {
        ocupante = (procesarFilaOcupantes(new Ocupante(), filaActual));
      } else {
        Bien bien = bienRepository.findByIdCarga(getRawStringValue(filaActual, id_bien)).orElseThrow();
        BienPosesionNegociada bienPN = bienPosesionNegociadaRepository.findByBien(bien).orElseThrow();
        OcupanteBien ocupanteBien = new OcupanteBien();
        ocupanteBien.setBienPosesionNegociada(bienPN);
        ocupanteBien.setOrden(getIntegerValue(filaActual, ocupante_));
        Set<OcupanteBien> ocupantesbien = ocupante.getBienes();
        ocupantesbien.add(ocupanteBien);
        ocupante.setBienes(ocupantesbien);
      }
      guardado = ocupanteRepository.save(ocupante);
      dev.add(guardado);
    }
    return dev;
  }

  private Ocupante procesarFilaOcupantes(Ocupante ocupante, XSSFRow filaActual) throws NotFoundException {
    Bien bien = bienRepository.findByIdCarga(getRawStringValue(filaActual, id_bien)).orElseThrow();
    BienPosesionNegociada bienPN = bienPosesionNegociadaRepository.findByBien(bien).orElseThrow();
    OcupanteBien ocupanteBien = new OcupanteBien();
    ocupanteBien.setOrden(getIntegerValue(filaActual, ocupante_));
    ocupanteBien.setBienPosesionNegociada(bienPN);
    ocupante.getBienes().add(ocupanteBien);
    ocupante.setNombre(getRawStringValue(filaActual, nombre));
    ocupante.setIdOcupanteOrigen(getRawStringValue(filaActual, id_origen));
    ocupante.setApellidos(getRawStringValue(filaActual, apellidos));
    ocupante.setNacionalidad((Pais) this.catalogos.get("pais").get(getRawStringValue(filaActual, nacionalidad)));
    ocupante.setDni(getRawStringValue(filaActual, numero_documento));
    ocupante.setMenorEdad(getBooleanValue(filaActual, menor_de_edad));
    ocupante.setDiscapacitado(getBooleanValue(filaActual, discapacitado));
    ocupante.setDependencia(getBooleanValue(filaActual, dependiente));
    ocupante.setResidenciaHabitual(getBooleanValue(filaActual, residencia_habitual));
    ocupante.setIngresosNetosMensuales(getNumericValue(filaActual, ingresos_Netos_Mensuales));
    ocupanteBien.setTipoOcupante((TipoOcupante) this.catalogos.get("tipoOcupante").get(getRawStringValue(filaActual, tipo)));
    ocupante.setDeudorAnterior(getBooleanValue(filaActual, marca_deudor_anterior));
    ocupante.setOtrasSituacionesVulnerabilidad((Vulnerabilidad) this.catalogos.get("vulnerabilidad").get(getRawStringValue(filaActual, otras_situaciones_de_vulnerabilidad)));
    return ocupante;
  }
}
