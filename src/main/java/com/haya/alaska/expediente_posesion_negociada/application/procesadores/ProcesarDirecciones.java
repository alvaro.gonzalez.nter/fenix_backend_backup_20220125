package com.haya.alaska.expediente_posesion_negociada.application.procesadores;

import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_via.domain.TipoVia;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.haya.alaska.shared.util.ExcelImport.getNumericValue;
import static com.haya.alaska.shared.util.ExcelImport.getRawStringValue;

@Service
@AllArgsConstructor
public class ProcesarDirecciones {
  private final int id_ocupante = 1;
  private final int tipo_via = 2;
  private final int nombre_via = 3;
  private final int numero = 4;
  private final int portal = 5;
  private final int bloque = 6;
  private final int escalera = 7;
  private final int piso = 8;
  private final int puerta = 9;
  private final int entorno = 10;
  private final int municipio = 11;
  private final int localidad = 12;
  private final int provincia = 13;
  private final int comentario_direccion = 14;
  private final int codigo_postal = 15;
  private final int longitud = 16;
  private final int latitud = 17;
  private final int descripcion = 18;
  private final int primera_fila_con_datos = 5;
  private final OcupanteRepository ocupanteRepository;
  private HashMap<String, HashMap<String, Object>> catalogos;

  public List<Direccion> procesar(XSSFSheet hojaDirecciones, HashMap<String, HashMap<String, Object>> catalogos) throws Exception {
    this.catalogos = catalogos;
    List<Direccion> dev = new ArrayList<>();
    for (int i = primera_fila_con_datos; i <= hojaDirecciones.getPhysicalNumberOfRows(); i++) {
      XSSFRow filaActual = hojaDirecciones.getRow(i);
      Direccion direccion = new Direccion();
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, id_ocupante);
      }
      if (null != hasContent && !"".equals(hasContent)) {
        dev.add(procesarFilaDireccion(direccion, filaActual));
      }
    }
    return dev;
  }

  private Direccion procesarFilaDireccion(Direccion direccion, XSSFRow filaActual) throws NotFoundException {
    direccion.setOcupante(ocupanteRepository.findByidOcupanteOrigen(getRawStringValue(filaActual, id_ocupante)).orElseThrow());
    direccion.setTipoVia((TipoVia) this.catalogos.get("tipoVia").get(getRawStringValue(filaActual, tipo_via)));

    direccion.setNombre(getRawStringValue(filaActual, nombre_via));
    direccion.setNumero(getRawStringValue(filaActual, numero));
    direccion.setPortal(getRawStringValue(filaActual, portal));
    direccion.setBloque(getRawStringValue(filaActual, bloque));
    direccion.setEscalera(getRawStringValue(filaActual, escalera));
    direccion.setPiso(getRawStringValue(filaActual, piso));
    direccion.setPuerta(getRawStringValue(filaActual, puerta));
    direccion.setEntorno((Entorno) this.catalogos.get("entorno").get(getRawStringValue(filaActual, entorno)));
    direccion.setMunicipio((Municipio) this.catalogos.get("municipio").get(getRawStringValue(filaActual, municipio)));
    direccion.setProvincia((Provincia) this.catalogos.get("provincia").get(getRawStringValue(filaActual, provincia)));
    direccion.setLocalidad((Localidad) this.catalogos.get("localidad").get(getRawStringValue(filaActual, localidad)));
    direccion.setComentario(getRawStringValue(filaActual, comentario_direccion));
    direccion.setCodigoPostal(getRawStringValue(filaActual, codigo_postal));
    direccion.setLongitud(getNumericValue(filaActual, longitud));
    direccion.setLatitud(getNumericValue(filaActual, latitud));

    direccion.setOrigen("Manual");
    direccion.setFechaActualizacion(new Date());
    return direccion;
  }
}
