package com.haya.alaska.expediente_posesion_negociada.application.comprobadores;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;


import java.util.ArrayList;
import java.util.List;

public class ComprobarActivos {

  /**
   * Casuística 1	Mismo ID Activo con datos identificativos distintos
   * Resultado:	Se cargará un único registro, aplicando las siguientes lógicas en este orden
   * Asunción 1:	Nos quedamos con el que más información tenga por este orden:
   * 1	Referencia catastral
   * 2	Idufir
   * 3	Registro
   * 4	Fecha de inscripción
   * 5	Finca
   * 6	Tomo
   * 7	Libro
   * 8	Folio
   * Asunción 6:	Tienen los mismos campos relevantes informados, nos quedamos con el primer registro.
   * Nota: se actualizarán todos los campos con el registro seleccionado según las asunciones, excepto los campos de dirección, que se cargará siempre el más largo
   * <p>
   * Casuística 2	Mismo ID Activo con datos identificativos iguales
   * Resultado:	Se hará un select distinct por los campos identificativos de la Asunción 1 y se cargará un único registro
   **/

  public static List<BienPosesionNegociada> listaExcluidos  = new ArrayList<>();

  public static List<BienPosesionNegociada> getListaExcluidos() {
    return listaExcluidos;
  }

  public static List<BienPosesionNegociada> comprobar(List<BienPosesionNegociada> activos) {
    List<BienPosesionNegociada> listaYaAnadidas = new ArrayList<>();
    for (BienPosesionNegociada activo : activos) {
      for (BienPosesionNegociada activo1 : activos) {
        if (activo.equals(activo1)) {
          if (activo.getBien().getIdCarga().equals(activo1.getBien().getIdCarga())) {
            if (comprobarReferenciaCatastral(activo, activo1)
              && comprobarIdufir(activo, activo1)
              && comprobarRegistro(activo, activo1)
              && comprobarFechaDeInscripcion(activo, activo1)
              && comprobarFinca(activo, activo1)
              && comprobarTomo(activo, activo1)
              && comprobarLibro(activo, activo1)
              && comprobarFolio(activo, activo1)) {
              compararDirecciones(activo, activo1);
              listaYaAnadidas.add(activo);
              continue;
            } else {
              //casustica 1
              if (comprobarReferenciaCatastralNull(activo, activo1, listaYaAnadidas)) {
                if (comprobarIdufirNull(activo, activo1, listaYaAnadidas)) {
                  if (comprobarRegistroNull(activo, activo1, listaYaAnadidas)) {
                    if (comprobarFechaDeInscripcionNull(activo, activo1, listaYaAnadidas)) {
                      if (comprobarFincaNull(activo, activo1, listaYaAnadidas)) {
                        if (comprobarTomoNull(activo, activo1, listaYaAnadidas)) {
                          if (comprobarLibroNull(activo, activo1, listaYaAnadidas)) {
                            if (comprobarFolioNull(activo, activo1, listaYaAnadidas)) {
                              compararDirecciones(activo, activo1);
                              listaYaAnadidas.add(activo);
                              continue;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    for (BienPosesionNegociada activo : activos) {
      boolean exist = false;
      for (BienPosesionNegociada activo1 : listaYaAnadidas) {
        if (!activo.getBien().getIdCarga().equals(activo1.getBien().getIdCarga())) exist = true;
      }
      if (!exist) {
        listaYaAnadidas.add(activo);
      }else{
        listaExcluidos.add(activo);
      }
    }
    return listaYaAnadidas;
  }

  private static boolean comprobarReferenciaCatastralNull(BienPosesionNegociada activo, BienPosesionNegociada
    activo1, List<BienPosesionNegociada> listaYaAnadidas) {
    if (activo.getBien().getReferenciaCatastral() != null && activo1.getBien().getReferenciaCatastral() != null) {
      return true;
    } else if (activo.getBien().getReferenciaCatastral() != null && activo1.getBien().getReferenciaCatastral() == null) {
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);
      return false;
    } else if (activo.getBien().getReferenciaCatastral() == null && activo1.getBien().getReferenciaCatastral() != null) {
      return false;
    } else if (activo.getBien().getReferenciaCatastral() == null && activo1.getBien().getReferenciaCatastral() == null) {
      if (containsIdCarga(activo, listaYaAnadidas)) return false;
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    }
    return true;
  }


  private static boolean comprobarIdufirNull(BienPosesionNegociada activo, BienPosesionNegociada
    activo1, List<BienPosesionNegociada> listaYaAnadidas) {
    if (activo.getBien().getIdufir() != null && activo1.getBien().getIdufir() != null) {
      return true;
    } else if (activo.getBien().getIdufir() != null && activo1.getBien().getIdufir() == null) {
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    } else if (activo.getBien().getIdufir() == null && activo1.getBien().getIdufir() != null) {
      return false;
    } else if (activo.getBien().getIdufir() == null && activo1.getBien().getIdufir() == null) {
      if (containsIdCarga(activo, listaYaAnadidas)) return false;
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    }
    return true;
  }

  private static boolean comprobarRegistroNull(BienPosesionNegociada activo, BienPosesionNegociada
    activo1, List<BienPosesionNegociada> listaYaAnadidas) {
    if (activo.getBien().getRegistro() != null && activo1.getBien().getRegistro() != null) {
      return true;
    } else if (activo.getBien().getRegistro() != null && activo1.getBien().getRegistro() == null) {
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    } else if (activo.getBien().getRegistro() == null && activo1.getBien().getRegistro() != null) {
      return false;
    } else if (activo.getBien().getRegistro() == null && activo1.getBien().getRegistro() == null) {
      if (containsIdCarga(activo, listaYaAnadidas)) return false;
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    }
    return true;
  }

  private static boolean comprobarFechaDeInscripcionNull(BienPosesionNegociada activo, BienPosesionNegociada
    activo1, List<BienPosesionNegociada> listaYaAnadidas) {
    if (activo.getBien().getFechaInscripcion() != null && activo1.getBien().getFechaInscripcion() != null) {
      return true;
    } else if (activo.getBien().getFechaInscripcion() != null && activo1.getBien().getFechaInscripcion() == null) {
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    } else if (activo.getBien().getFechaInscripcion() == null && activo1.getBien().getFechaInscripcion() != null) {
      return false;
    } else if (activo.getBien().getFechaInscripcion() == null && activo1.getBien().getFechaInscripcion() == null) {
      if (containsIdCarga(activo, listaYaAnadidas)) return false;
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    }
    return true;
  }

  private static boolean comprobarFincaNull(BienPosesionNegociada activo, BienPosesionNegociada
    activo1, List<BienPosesionNegociada> listaYaAnadidas) {
    if (activo.getBien().getFinca() != null && activo1.getBien().getFinca() != null) {
      return true;
    } else if (activo.getBien().getFinca() != null && activo1.getBien().getFinca() == null) {
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    } else if (activo.getBien().getFinca() == null && activo1.getBien().getFinca() != null) {
      return false;
    } else if (activo.getBien().getFinca() == null && activo1.getBien().getFinca() == null) {
      if (containsIdCarga(activo, listaYaAnadidas)) return false;
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    }
    return true;
  }

  private static boolean comprobarTomoNull(BienPosesionNegociada activo, BienPosesionNegociada
    activo1, List<BienPosesionNegociada> listaYaAnadidas) {
    if (activo.getBien().getTomo() != null && activo1.getBien().getTomo() != null) {
      return true;
    } else if (activo.getBien().getTomo() != null && activo1.getBien().getTomo() == null) {
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    } else if (activo.getBien().getTomo() == null && activo1.getBien().getTomo() != null) {
      return false;
    } else if (activo.getBien().getTomo() == null && activo1.getBien().getTomo() == null) {
      if (containsIdCarga(activo, listaYaAnadidas)) return false;
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    }
    return true;
  }

  private static boolean comprobarLibroNull(BienPosesionNegociada activo, BienPosesionNegociada
    activo1, List<BienPosesionNegociada> listaYaAnadidas) {
    if (activo.getBien().getLibro() != null && activo1.getBien().getLibro() != null) {
      return true;
    } else if (activo.getBien().getLibro() != null && activo1.getBien().getLibro() == null) {
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    } else if (activo.getBien().getLibro() == null && activo1.getBien().getLibro() != null) {
      return false;
    } else if (activo.getBien().getLibro() == null && activo1.getBien().getLibro() == null) {
      if (containsIdCarga(activo, listaYaAnadidas)) return false;
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    }
    return true;
  }

  private static boolean comprobarFolioNull(BienPosesionNegociada activo, BienPosesionNegociada
    activo1, List<BienPosesionNegociada> listaYaAnadidas) {
    if (activo.getBien().getFolio() != null && activo1.getBien().getFolio() != null) {
      return true;
    } else if (activo.getBien().getFolio() != null && activo1.getBien().getFolio() == null) {
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    } else if (activo.getBien().getFolio() == null && activo1.getBien().getFolio() != null) {
      return false;
    } else if (activo.getBien().getFolio() == null && activo1.getBien().getFolio() == null) {
      if (containsIdCarga(activo, listaYaAnadidas)) return false;
      compararDirecciones(activo, activo1);
      listaYaAnadidas.add(activo);

      return false;
    }
    return true;
  }

  private static void compararDirecciones(BienPosesionNegociada activo, BienPosesionNegociada activo1) {
    if (activo.getBien().getNombreVia().length() < activo1.getBien().getNombreVia().length()) {
      activo.getBien().setNombreVia(activo1.getBien().getNombreVia());
      activo.getBien().setTipoVia(activo1.getBien().getTipoVia());
      activo.getBien().setNumero(activo1.getBien().getNumero());
      activo.getBien().setPortal(activo1.getBien().getPortal());
      activo.getBien().setBloque(activo1.getBien().getBloque());
      activo.getBien().setEscalera(activo1.getBien().getEscalera());
      activo.getBien().setPiso(activo1.getBien().getPiso());
      activo.getBien().setPuerta(activo1.getBien().getPuerta());
      activo.getBien().setEntorno(activo1.getBien().getEntorno());
      activo.getBien().setMunicipio(activo1.getBien().getMunicipio());
      activo.getBien().setLocalidad(activo1.getBien().getLocalidad());
      activo.getBien().setProvincia(activo1.getBien().getProvincia());
      activo.getBien().setComentarioDireccion(activo1.getBien().getComentarioDireccion());
      activo.getBien().setCodigoPostal(activo1.getBien().getCodigoPostal());
      activo.getBien().setPais(activo1.getBien().getPais());
      activo.getBien().setLatitud(activo1.getBien().getLatitud());
      activo.getBien().setLongitud(activo1.getBien().getLongitud());
    }
  }

  private static boolean comprobarReferenciaCatastral(BienPosesionNegociada activo, BienPosesionNegociada
    activo1) {
    return activo.getBien().getReferenciaCatastral() != null && activo.getBien().getReferenciaCatastral().equals(activo1.getBien().getReferenciaCatastral());
  }

  private static boolean comprobarIdufir(BienPosesionNegociada activo, BienPosesionNegociada
    activo1) {
    return activo.getBien().getIdufir() != null && activo.getBien().getIdufir().equals(activo1.getBien().getIdufir());
  }

  private static boolean comprobarRegistro(BienPosesionNegociada activo, BienPosesionNegociada
    activo1) {
    return activo.getBien().getRegistro() != null && activo.getBien().getRegistro().equals(activo1.getBien().getRegistro());
  }

  private static boolean comprobarFechaDeInscripcion(BienPosesionNegociada activo, BienPosesionNegociada
    activo1) {
    return activo.getBien().getFechaInscripcion() != null && activo.getBien().getFechaInscripcion().equals(activo1.getBien().getFechaInscripcion());
  }

  private static boolean comprobarFinca(BienPosesionNegociada activo, BienPosesionNegociada
    activo1) {
    return activo.getBien().getFinca() != null && activo.getBien().getFinca().equals(activo1.getBien().getFinca());
  }

  private static boolean comprobarTomo(BienPosesionNegociada activo, BienPosesionNegociada
    activo1) {
    return activo.getBien().getTomo() != null && activo.getBien().getTomo().equals(activo1.getBien().getTomo());
  }

  private static boolean comprobarLibro(BienPosesionNegociada activo, BienPosesionNegociada
    activo1) {
    return activo.getBien().getLibro() != null && activo.getBien().getLibro().equals(activo1.getBien().getLibro());
  }

  private static boolean comprobarFolio(BienPosesionNegociada activo, BienPosesionNegociada
    activo1) {
    return activo.getBien().getFolio() != null && activo.getBien().getFolio().equals(activo1.getBien().getFolio());
  }

  private static boolean containsIdCarga(BienPosesionNegociada activo, List<BienPosesionNegociada> listaYaAnadida) {
    for (BienPosesionNegociada bienPosesionNegociada : listaYaAnadida) {
      if (bienPosesionNegociada.getBien().getIdCarga().equals(activo.getBien().getIdCarga())) {
        return true;
      }
    }
    return false;
  }

}
