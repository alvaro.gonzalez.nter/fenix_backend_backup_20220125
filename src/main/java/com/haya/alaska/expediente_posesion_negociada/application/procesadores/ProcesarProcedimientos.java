package com.haya.alaska.expediente_posesion_negociada.application.procesadores;

import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.estado_procedimiento.domain.EstadoProcedimiento;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.repository.ProcedimientoPosesionNegociadaRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_accion_procedimiento.domain.TipoAccionProcedimiento;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Service
@AllArgsConstructor
public class ProcesarProcedimientos {
  private final int primera_fila_con_datos = 6;
  private final int id_bien = 1;
  private final int num_Demanda = 2;
  private final int auto = 3;
  private final int juzgado = 4;
  private final int provincia_Juzgado = 5;
  private final int ciudad_Juzgado = 6;
  private final int causa_reclamacion = 7;
  private final int tipo_accion = 8;
  private final int tipo_Procedimiento = 9;
  private final int estado_demanda = 10;
  private final int fecha_admision_tramite = 11;
  private final int importe_recuperado = 12;
  private final int importe_recuperado_costas = 13;
  private final int fecha_sentencia_1_instancia = 14;
  private final int fecha_sentencia_2_instancia = 15;
  private final int posicion = 16;
  private final int cliente_personado = 17;
  private final int demandante = 18;
  private final int demandados = 19;
  private final int otras_Posiciones = 20;
  private final int letrado = 21;
  private final int gestor = 22;
  private final int procurador = 23;
  private final ProcedimientoPosesionNegociadaRepository procedimientoPosesionNegociadaRepository;
  private final OcupanteRepository ocupanteRepository;
  private final BienRepository bienRepository;
  private final BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  private HashMap<String, HashMap<String, Object>> catalogos;

  public List<ProcedimientoPosesionNegociada> procesar(XSSFSheet hojaProcedimientos, HashMap<String, HashMap<String, Object>> catalogos) throws Exception {
    this.catalogos = catalogos;
    List<ProcedimientoPosesionNegociada> dev = new ArrayList<>();
    for (int i = primera_fila_con_datos; i <= hojaProcedimientos.getPhysicalNumberOfRows(); i++) {
      dev.add(procesarFilaProcedimientos(hojaProcedimientos.getRow(i), dev));
    }
    return dev;
  }

  private ProcedimientoPosesionNegociada procesarFilaProcedimientos(XSSFRow filaActual, List<ProcedimientoPosesionNegociada> listaProcs) throws NotFoundException {
    //Buscamos el procedimiento en bbdd y en la lista porque puede venir duplicado y solo necesitariamos añadir un nuevo Demandante
    ProcedimientoPosesionNegociada proc = procedimientoPosesionNegociadaRepository.findByNumeroDemanda(getRawStringValue(filaActual, num_Demanda)).orElse(
      listaProcs.stream().filter(l -> {
        try {
          return l.getNumeroDemanda().equals(getRawStringValue(filaActual, num_Demanda));
        } catch (Exception e) {
          return false;
        }
      }).findFirst().orElse(null)
    );
    //Si hemos encontrado el procedimiento simplemente añadimos un nuevo Demandado
    if (null != proc) {
      proc.getDemandados().add(ocupanteRepository.findByDni(getRawStringValue(filaActual, demandados)).orElseThrow());
      return proc;
    }
    proc = new ProcedimientoPosesionNegociada();
    proc.setBien(bienPosesionNegociadaRepository.findByBien(bienRepository.findByIdHaya(getRawStringValue(filaActual, id_bien)).orElseThrow()).orElseThrow());
    proc.setNumeroDemanda(getRawStringValue(filaActual, num_Demanda));
    proc.setAutos(getRawStringValue(filaActual, auto));
    proc.setJuzgado(getRawStringValue(filaActual, juzgado));
    proc.setProvinciaJuzgado((Provincia) this.catalogos.get("provincia").get(getRawStringValue(filaActual, provincia_Juzgado)));
    proc.setCiudadJuzgado((Localidad) this.catalogos.get("localidad").get(getRawStringValue(filaActual, ciudad_Juzgado)));
    proc.setCausaReclamacion(getRawStringValue(filaActual, causa_reclamacion));
    proc.setTipoAccion((TipoAccionProcedimiento) this.catalogos.get("tipoAccionProcedimiento").get(getRawStringValue(filaActual, tipo_accion)));
    proc.setTipo((TipoProcedimiento) this.catalogos.get("tipoProcedimiento").get(getRawStringValue(filaActual, tipo_Procedimiento)));
    proc.setEstado((EstadoProcedimiento) this.catalogos.get("estadoProcedimiento").get(getRawStringValue(filaActual, estado_demanda)));
    proc.setFechaAdmisionATramite(getDateValue(filaActual, fecha_admision_tramite));
    proc.setImporteRecuperado(getNumericValue(filaActual, importe_recuperado));
    proc.setImporteRecuperadoCostas(getNumericValue(filaActual, importe_recuperado_costas));
    proc.setFechaSentencia1Instancia(getDateValue(filaActual, fecha_sentencia_1_instancia));
    proc.setFechaSentencia2Instancia(getDateValue(filaActual, fecha_sentencia_2_instancia));
    proc.setPosicion(getRawStringValue(filaActual, posicion));
    proc.setClientePersonado(getRawStringValue(filaActual, cliente_personado));
    proc.setDemandante(getRawStringValue(filaActual, demandante));
    proc.getDemandados().add(ocupanteRepository.findByDni(getRawStringValue(filaActual, demandados)).orElseThrow());
    proc.setOtrasPosiciones(getRawStringValue(filaActual, otras_Posiciones));
    proc.setLetrado(getRawStringValue(filaActual, letrado));
    proc.setGestor(getRawStringValue(filaActual, gestor));
    proc.setProcurador(getRawStringValue(filaActual, procurador));
    return proc;
  }
}
