package com.haya.alaska.expediente_posesion_negociada.application;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociadaExcel;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.domain.CargaOcupanteExcel;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.domain.DatosContactoOcupanteExcel;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.direccion.domain.DireccionOcupanteExcel;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.application.ExpedienteUseCase;
import com.haya.alaska.expediente.infrastructure.controller.dto.InformeExpedienteInputDTO;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociadaExcel;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.ocupante.application.OcupanteUseCaseImpl;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.domain.OcupanteExcel;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.ocupante_bien.infrastructure.repository.OcupanteBienRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociadaExcel;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.domain.TasacionOcupanteExcel;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.valoracion.domain.ValoracionOcupanteExcel;
import com.haya.alaska.visita.domain.Visita;
import com.haya.alaska.visita.domain.VisitaExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ExpedientePosesionNegociadaExcelUseCaseImpl
    implements ExpedientePosesionNegociadaExcelUseCase {

  @Autowired private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;

  @Autowired EventoRepository eventoRepository;
  @Autowired EventoUtil eventoUtil;

  @Autowired ExpedienteUtil expedienteUtil;

  @Autowired OcupanteUseCaseImpl ocupanteUseCaseimpl;

  @Autowired OcupanteBienRepository ocupanteBienRepository;

  @Autowired ExpedienteUseCase expedienteUseCase;

  @Autowired CarteraRepository carteraRepository;

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelExpedientePosesionNegociada(
      Usuario usuario) throws Exception {
    var datos = this.createExpedientePosesionDataForExcel2();
    List<ExpedientePosesionNegociada> expedientesPosesionNegociada;
    if (usuario.esAdministrador())
      expedientesPosesionNegociada = expedientePosesionNegociadaRepository.findAll();
    else
      expedientesPosesionNegociada =
          expedientePosesionNegociadaRepository.findAllByAsignacionesUsuarioId(usuario.getId());
    if (expedientesPosesionNegociada.size() > 5000)
      throw new Exception(
          "La exportación seleccionada supera el límite de expedientes permitido (5000)");
    for (ExpedientePosesionNegociada expedientePosesionNegociada : expedientesPosesionNegociada) {
      this.addExpedientePosesionNegociadaDataForExcel(expedientePosesionNegociada, datos);
    }
    return datos;
  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelExpedientePosesionNegociadaByArray(
      List<ExpedientePosesionNegociada> expedientes) throws NotFoundException {
    var datos = this.createExpedientePosesionDataForExcel();
    for (ExpedientePosesionNegociada expedientePosesionNegociada : expedientes) {
      this.addExpedientePosesionNegociadaDataForExcel(expedientePosesionNegociada, datos);
    }
    return datos;
  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelExpedientePosesionNegociadaById(
      Integer id) throws NotFoundException {
    var datos = this.createExpedientePosesionDataForExcel2();

    // Buscamos el expediente PN por ID

    if (id != null) {
      ExpedientePosesionNegociada expedientePosesionNegociada =
          expedientePosesionNegociadaRepository
              .findById(id)
              .orElseThrow(() -> new NotFoundException("Expediente posesion negociada", id));
      this.addExpedientePosesionNegociadaDataForExcel(expedientePosesionNegociada, datos);
    } else {
      for (ExpedientePosesionNegociada expedientePosesionNegociada :
          expedientePosesionNegociadaRepository.findAll()) {
        this.addExpedientePosesionNegociadaDataForExcel(expedientePosesionNegociada, datos);
      }
    }
    return datos;
  }

  private LinkedHashMap<String, List<Collection<?>>> createExpedientePosesionDataForExcel2() {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expedientes = new ArrayList<>();
    expedientes.add(ExpedientePosesionNegociadaExcel.cabeceras);

    // Activos==BienPosesionNegociada
    List<Collection<?>> activos = new ArrayList<>();
    activos.add(BienPosesionNegociadaExcel.cabeceras);

    // tasaciones
    List<Collection<?>> tasaciones = new ArrayList<>();
    tasaciones.add(TasacionOcupanteExcel.cabeceras);

    // valoraciones
    List<Collection<?>> valoraciones = new ArrayList<>();
    valoraciones.add(ValoracionOcupanteExcel.cabeceras);

    // cargas
    List<Collection<?>> cargas = new ArrayList<>();
    cargas.add(CargaOcupanteExcel.cabeceras);

    List<Collection<?>> ocupantes = new ArrayList<>();
    ocupantes.add(OcupanteExcel.cabeceras);

    List<Collection<?>> direcciones = new ArrayList<>();
    direcciones.add(DireccionOcupanteExcel.cabeceras);

    List<Collection<?>> contactos = new ArrayList<>();
    contactos.add(DatosContactoOcupanteExcel.cabeceras);

    List<Collection<?>> visitas = new ArrayList<>();
    visitas.add(VisitaExcel.cabeceras);

    // Judicial va con procedimientos
    List<Collection<?>> judicial = new ArrayList<>();
    judicial.add(ProcedimientoPosesionNegociadaExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      datos.put("Connection", expedientes);
      datos.put("Actives", activos);
      datos.put("Appraisals", tasaciones);
      datos.put("Ratings", valoraciones);
      datos.put("Charges", cargas);
      datos.put("Occupants", ocupantes);
      datos.put("Addresses", direcciones);
      datos.put("Contact", contactos);
      datos.put("Visits", visitas);
      datos.put("Judicial", judicial);

    } else {

      datos.put("Expediente", expedientes);
      datos.put("Activos", activos);
      datos.put("Tasaciones", tasaciones);
      datos.put("Valoraciones", valoraciones);
      datos.put("Cargas", cargas);
      datos.put("Ocupantes", ocupantes);
      datos.put("Direcciones", direcciones);
      datos.put("Contacto", contactos);
      datos.put("Visitas", visitas);
      datos.put("Judicial", judicial);
    }

    return datos;
  }

  private LinkedHashMap<String, List<Collection<?>>> createExpedientePosesionDataForExcel() {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expedientes = new ArrayList<>();
    expedientes.add(ExpedientePosesionNegociadaExcel.cabeceras);

    // Activos==BienPosesionNegociada
    List<Collection<?>> activos = new ArrayList<>();
    activos.add(BienPosesionNegociadaExcel.cabeceras);

    // tasaciones
    List<Collection<?>> tasaciones = new ArrayList<>();
    tasaciones.add(TasacionOcupanteExcel.cabeceras);

    // valoraciones
    List<Collection<?>> valoraciones = new ArrayList<>();
    valoraciones.add(ValoracionOcupanteExcel.cabeceras);

    // cargas
    List<Collection<?>> cargas = new ArrayList<>();
    cargas.add(CargaOcupanteExcel.cabeceras);

    List<Collection<?>> ocupantes = new ArrayList<>();
    ocupantes.add(OcupanteExcel.cabeceras);

    List<Collection<?>> direcciones = new ArrayList<>();
    direcciones.add(DireccionOcupanteExcel.cabeceras);

    List<Collection<?>> contactos = new ArrayList<>();
    contactos.add(DatosContactoOcupanteExcel.cabeceras);

    List<Collection<?>> visitas = new ArrayList<>();
    visitas.add(VisitaExcel.cabeceras);

    // Judicial va con procedimientos
    List<Collection<?>> judicial = new ArrayList<>();
    judicial.add(ProcedimientoPosesionNegociadaExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      datos.put("Connection", expedientes);
      datos.put("Actives", activos);
      datos.put("Appraisals", tasaciones);
      datos.put("Ratings", valoraciones);
      datos.put("Charges", cargas);
      datos.put("Occupants", ocupantes);
      datos.put("Addresses", direcciones);
      datos.put("Contact", contactos);
      datos.put("Visits", visitas);
      datos.put("Judicial", judicial);

    } else {

      datos.put("Expediente", expedientes);
      datos.put("Activos", activos);
      datos.put("Tasaciones", tasaciones);
      datos.put("Valoraciones", valoraciones);
      datos.put("Cargas", cargas);
      datos.put("Ocupantes", ocupantes);
      datos.put("Direcciones", direcciones);
      datos.put("Contacto", contactos);
      datos.put("Visitas", visitas);
      datos.put("Judicial", judicial);
    }

    return datos;
  }

  private void addExpedientePosesionNegociadaDataForExcel(
      ExpedientePosesionNegociada expedientePosesionNegociada,
      LinkedHashMap<String, List<Collection<?>>> datos)
      throws NotFoundException {

    List<Collection<?>> expedientes;
    List<Collection<?>> activos;
    List<Collection<?>> tasaciones;
    List<Collection<?>> valoraciones;
    List<Collection<?>> cargas;
    List<Collection<?>> ocupantes;
    List<Collection<?>> direcciones;
    List<Collection<?>> contactos;
    List<Collection<?>> visitas;
    List<Collection<?>> judicial;

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      expedientes = datos.get("Connection");
      activos = datos.get("Actives");
      tasaciones = datos.get("Appraisals");
      valoraciones = datos.get("Ratings");
      cargas = datos.get("Charges");
      ocupantes = datos.get("Occupants");
      direcciones = datos.get("Addresses");
      contactos = datos.get("Contact");
      visitas = datos.get("Visits");
      judicial = datos.get("Judicial");

    } else {

      expedientes = datos.get("Expediente");
      activos = datos.get("Activos");
      tasaciones = datos.get("Tasaciones");
      valoraciones = datos.get("Valoraciones");
      cargas = datos.get("Cargas");
      ocupantes = datos.get("Ocupantes");
      direcciones = datos.get("Direcciones");
      contactos = datos.get("Contacto");
      visitas = datos.get("Visitas");
      judicial = datos.get("Judicial");
    }

    Evento accion = getLastEvento(expedientePosesionNegociada.getId(), 1);
    Evento alerta = getLastEvento(expedientePosesionNegociada.getId(), 2);
    Evento actividad = getLastEvento(expedientePosesionNegociada.getId(), 3);

    expedientes.add(
        new ExpedientePosesionNegociadaExcel(expedientePosesionNegociada, alerta, accion, actividad)
            .getValuesList());
    for (BienPosesionNegociada bienPosesionNegociada :
        expedientePosesionNegociada.getBienesPosesionNegociada()) {
      activos.add(new BienPosesionNegociadaExcel(bienPosesionNegociada).getValuesList());
      List<OcupanteBien> listaOcupantesBien =
          ocupanteBienRepository.findAllByBienPosesionNegociadaId(bienPosesionNegociada.getId());

      List<Tasacion> listaTasaciones =
          new ArrayList<>(bienPosesionNegociada.getBien().getTasaciones());
      List<Valoracion> listaValoraciones =
          new ArrayList<>(bienPosesionNegociada.getBien().getValoraciones());
      List<Carga> listaCargas = new ArrayList<>(bienPosesionNegociada.getBien().getCargas());
      List<Ocupante> listaOcupantes = new ArrayList<>();
      List<Direccion> listaDirecciones = new ArrayList<>();
      List<DatoContacto> listaContactos = new ArrayList<>();

      for (var x : listaOcupantesBien) {
        listaOcupantes.add(x.getOcupante());
      }
      for (var y : listaOcupantes) {
        var dirs = y.getDirecciones();
        for (var z : dirs) {
          listaDirecciones.add(z);
        }

        var conts = y.getDatosContacto();
        for (var z : conts) {
          listaContactos.add(z);
        }
      }
      for (Tasacion tas : listaTasaciones) {
        tasaciones.add(
            new TasacionOcupanteExcel(tas, expedientePosesionNegociada.getIdConcatenado())
                .getValuesList());
      }
      for (Carga car : listaCargas) {
        cargas.add(
            new CargaOcupanteExcel(
                    car,
                    bienPosesionNegociada.getId(),
                    expedientePosesionNegociada.getIdConcatenado())
                .getValuesList());
      }
      for (Valoracion valo : listaValoraciones) {
        valoraciones.add(
            new ValoracionOcupanteExcel(valo, expedientePosesionNegociada.getIdConcatenado())
                .getValuesList());
      }
      for (OcupanteBien ocupantebien : listaOcupantesBien) {
        ocupantes.add(new OcupanteExcel(ocupantebien).getValuesList());
      }
      for (Direccion dire : listaDirecciones) {
        direcciones.add(new DireccionOcupanteExcel(dire).getValuesList());
      }
      for (DatoContacto dc : listaContactos) {
        contactos.add(new DatosContactoOcupanteExcel(dc).getValuesList());
      }
      for (Visita visita : bienPosesionNegociada.getVisitas()) {
        visitas.add(new VisitaExcel(visita).getValuesList());
      }
      for (ProcedimientoPosesionNegociada procedimientoPosesionNegociada :
          bienPosesionNegociada.getProcedimientos()) {
        judicial.add(
            new ProcedimientoPosesionNegociadaExcel(procedimientoPosesionNegociada)
                .getValuesList());
      }
    }
  }

  private Evento getLastEvento(Integer idExpediente, Integer clase) throws NotFoundException {
    List<Integer> clases = new ArrayList<>();
    clases.add(clase);
    List<Evento> eventos =
        eventoUtil
            .getFilteredEventos(
                null,
                null,
                null,
                7,
                idExpediente,
                null,
                null,
                null,
                null,
                null,
                null,
                clases,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                "fechaCreacion",
                "desc",
                0,
              100);

    Evento result = null;
    if (!eventos.isEmpty()) result = eventos.get(0);
    return result;
  }

  // Informe
  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelPosesionesNegociadasById(
      Integer carteraId,
      Integer mes,
      InformeExpedienteInputDTO informeExpedienteInputDTO,
      Usuario usuario)
      throws NotFoundException {
    Perfil perfilUsu = usuario.getPerfil();

    if (perfilUsu.getNombre().equals("Responsable de cartera")
        || perfilUsu.getNombre().equals("Supervisor")) {
      var datos = this.createExpedientePosesionDataForExcel();
      if (carteraId != null) {
        Cartera cartera =
            carteraRepository
                .findById(carteraId)
                .orElseThrow(() -> new NotFoundException("Cartera", carteraId));
        this.addExpedientePosesionNegociadaDataForExcelCartera(
            cartera, datos, mes, informeExpedienteInputDTO);
      } else {
        for (Cartera cartera : carteraRepository.findAll()) {
          this.addExpedientePosesionNegociadaDataForExcelCartera(
              cartera, datos, mes, informeExpedienteInputDTO);
        }
      }
      return datos;
    } else {
      throw new IllegalArgumentException(
          "El tipo del usuario debe ser Responsable de cartera o Supervisor para poder generar el Informe");
    }
  }

  // Informe Excel
  private void addExpedientePosesionNegociadaDataForExcelCartera(
      Cartera cartera,
      LinkedHashMap<String, List<Collection<?>>> datos,
      Integer mes,
      InformeExpedienteInputDTO informeExpedienteInputDTO)
      throws NotFoundException {

    List<Collection<?>> expedientes;
    List<Collection<?>> activos;
    List<Collection<?>> ocupantes;
    List<Collection<?>> visitas;
    List<Collection<?>> judicial;
    List<Collection<?>> tasaciones;

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      expedientes = datos.get("Connection");
      activos = datos.get("Actives");
      ocupantes = datos.get("Occupants");
      visitas = datos.get("Visitas");
      judicial = datos.get("Visits");
      tasaciones = datos.get("Appraisals");

    } else {

      expedientes = datos.get("Expediente");
      activos = datos.get("Activos");
      ocupantes = datos.get("Ocupantes");
      visitas = datos.get("Visitas");
      judicial = datos.get("Judicial");
      tasaciones = datos.get("Tasaciones");
    }

    List<ExpedientePosesionNegociada> listadoExpedientesPN =
        expedientePosesionNegociadaRepository.findAllByCarteraId(cartera.getId());

    List<ExpedientePosesionNegociada> expedientesFilter =
        listadoExpedientesPN.stream()
            .filter(
                expedientePN -> {
                  Date fCE = expedientePN.getFechaCreacion();
                  Date fCI = informeExpedienteInputDTO.getFechaCreacionExpedienteInicial();
                  Date fCF = informeExpedienteInputDTO.getFechaCreacionExpedienteFinal();
                  if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;

                  Date fRE = expedientePN.getFechaCierre();
                  Date fRI = informeExpedienteInputDTO.getFechaCierreExpedienteInicial();
                  Date fRF = informeExpedienteInputDTO.getFechaCierreExpedienteFinal();
                  if (!expedienteUtil.comprobarEntre(fRE, fRI, fRF)) return false;
                  return true;
                })
            .collect(Collectors.toList());

    for (ExpedientePosesionNegociada expedientePN : expedientesFilter) {

      Boolean comprobacion =
          expedienteUseCase.filtroMesInformesPosesionNegociada(expedientePN, mes);
      var estadoExpedientePN = expedientePN.getEstadoExpedienteRecuperacionAmistosa();

      if (comprobacion) {
        if (informeExpedienteInputDTO.getEstadoExpediente() == null
            || estadoExpedientePN.getId().equals(informeExpedienteInputDTO.getEstadoExpediente())) {
          Evento accion = getLastEvento(expedientePN.getId(), 1);
          Evento alerta = getLastEvento(expedientePN.getId(), 2);
          Evento actividad = getLastEvento(expedientePN.getId(), 3);
          expedientes.add(
              new ExpedientePosesionNegociadaExcel(expedientePN, accion, alerta, actividad)
                  .getValuesList());
          for (BienPosesionNegociada bienPN : expedientePN.getBienesPosesionNegociada()) {
            activos.add(new BienPosesionNegociadaExcel(bienPN).getValuesList());
            List<OcupanteBien> listaOcupantesBien = new ArrayList<>(bienPN.getOcupantes());

            for (OcupanteBien ocupantebien : listaOcupantesBien) {
              ocupantes.add(new OcupanteExcel(ocupantebien).getValuesList());
            }
            for (Visita visita : bienPN.getVisitas()) {
              visitas.add(new VisitaExcel(visita).getValuesList());
            }

            for (Tasacion tas : bienPN.getBien().getTasaciones()) {
              tasaciones.add(
                  new TasacionOcupanteExcel(tas, expedientePN.getIdConcatenado()).getValuesList());
            }

            for (ProcedimientoPosesionNegociada procedimientoPosesionNegociada :
                bienPN.getProcedimientos()) {
              judicial.add(
                  new ProcedimientoPosesionNegociadaExcel(procedimientoPosesionNegociada)
                      .getValuesList());
            }
          }
        }
      }
    }
  }
}
