package com.haya.alaska.expediente_posesion_negociada.application;

import com.haya.alaska.expediente.infrastructure.controller.dto.InformeExpedienteInputDTO;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface ExpedientePosesionNegociadaExcelUseCase {

  LinkedHashMap<String, List<Collection<?>>> findExcelExpedientePosesionNegociada(Usuario usuario) throws Exception;

  LinkedHashMap<String, List<Collection<?>>> findExcelExpedientePosesionNegociadaById(Integer id) throws NotFoundException;


  LinkedHashMap<String, List<Collection<?>>> findExcelPosesionesNegociadasById(Integer carteraId, Integer mes, InformeExpedienteInputDTO informeExpedienteInputDTO,
                                                                               Usuario usuario) throws NotFoundException;

  // List<Evento> findEventos();
  LinkedHashMap<String, List<Collection<?>>> findExcelExpedientePosesionNegociadaByArray(List<ExpedientePosesionNegociada> expedientes) throws NotFoundException;
   // List<Evento> findEventos();
}
