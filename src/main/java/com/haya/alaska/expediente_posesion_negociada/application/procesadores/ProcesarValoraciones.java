package com.haya.alaska.expediente_posesion_negociada.application.procesadores;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_valoracion.domain.TipoValoracion;
import com.haya.alaska.valoracion.domain.Valoracion;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Service
@AllArgsConstructor
public class ProcesarValoraciones {
  private final int primera_fila_con_datos = 6;
  private final int id_bien = 2;
  private final int valorador = 3;
  private final int tipo_valoracion = 4;
  private final int importe_valoracion = 5;
  private final int fecha_valoracion = 6;
  private final int liquidez = 7;
  private final BienRepository bienRepository;
  private HashMap<String, HashMap<String, Object>> catalogos;

  public List<Valoracion> procesar(XSSFSheet hojaValoracion, HashMap<String, HashMap<String, Object>> catalogos) throws Exception {
    this.catalogos = catalogos;
    List<Valoracion> dev = new ArrayList<>();
    XSSFRow filaActual;
    Valoracion valoracion;
    for (int i = primera_fila_con_datos; i <= hojaValoracion.getPhysicalNumberOfRows(); i++) {
      valoracion = new Valoracion();
      filaActual = hojaValoracion.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null != hasContent && !"".equals(hasContent)) {
        dev.add(procesarFilaValoracion(valoracion, filaActual));
      }

    }
    return dev;
  }

  private Valoracion procesarFilaValoracion(Valoracion valoracion, XSSFRow filaActual) throws NotFoundException {
    valoracion.setValorador(getRawStringValue(filaActual, valorador));
    valoracion.setTipoValoracion((TipoValoracion) this.catalogos.get("tipoValoracion").get(getRawStringValue(filaActual, tipo_valoracion)));
    valoracion.setImporte(getNumericValue(filaActual, importe_valoracion));
    valoracion.setFecha(getDateValue(filaActual, fecha_valoracion));
    valoracion.setLiquidez((Liquidez) this.catalogos.get("liquidez").get(getRawStringValue(filaActual, liquidez)));
    Bien bien = bienRepository.findByIdCarga(getRawStringValue(filaActual, id_bien)).orElseThrow();
    valoracion.setBien(bien);
    return valoracion;
  }
}
