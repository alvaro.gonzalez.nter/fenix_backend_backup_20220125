package com.haya.alaska.expediente_posesion_negociada.application.procesadores;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tipo_acreedor.domain.TipoAcreedor;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Service
@AllArgsConstructor
public class ProcesarCargas {
  private final int primera_fila_con_datos = 6;
  private final int id_bien = 2;
  private final int rango_carga = 3;
  private final int tipo_carga = 4;
  private final int importe_carga = 5;
  private final int tipo_acreedor = 6;
  private final int nombre_acreedor = 7;
  private final int fecha_anotacion = 8;
  private final int incluida_otros_colaterales = 9;
  private final int fecha_vencimiento_carga = 10;
  private final BienRepository bienRepository;
  private HashMap<String, HashMap<String, Object>> catalogos;

  public List<Carga> procesar(XSSFSheet hojaCargas, HashMap<String, HashMap<String, Object>> catalogos) throws Exception {
    this.catalogos = catalogos;
    List<Carga> dev = new ArrayList<>();
    XSSFRow filaActual;
    Carga carga;
    for (int i = primera_fila_con_datos; i <= hojaCargas.getPhysicalNumberOfRows(); i++) {
      carga = new Carga();
      filaActual = hojaCargas.getRow(i);
      dev.add(procesarFilaCargas(carga, filaActual));
    }
    return dev;
  }

  private Carga procesarFilaCargas(Carga carga, XSSFRow filaActual) throws NotFoundException {
    Set<Bien> bienes = new HashSet<>();
    bienes.add(bienRepository.findByIdCarga(getRawStringValue(filaActual, id_bien)).orElseThrow());
    carga.setBienes(bienes);
    carga.setRango(getIntegerValue(filaActual, rango_carga));
    carga.setTipoCarga((TipoCarga) this.catalogos.get("tipoCarga").get(getRawStringValue(filaActual, tipo_carga)));
    carga.setImporte(getNumericValue(filaActual, importe_carga));
    carga.setTipoAcreedor((TipoAcreedor) this.catalogos.get("tipoAcreedor").get(getRawStringValue(filaActual, tipo_acreedor)));
    carga.setNombreAcreedor(getRawStringValue(filaActual, nombre_acreedor));
    carga.setFechaAnotacion(getDateValue(filaActual, fecha_anotacion));
    carga.setIncluidaOtrosColaterales(getBooleanValue(filaActual, incluida_otros_colaterales));
    carga.setFechaVencimiento(getDateValue(filaActual, fecha_vencimiento_carga));
    return carga;
  }
}
