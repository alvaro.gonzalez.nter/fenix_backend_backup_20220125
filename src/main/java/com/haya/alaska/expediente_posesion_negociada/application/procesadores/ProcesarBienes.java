package com.haya.alaska.expediente_posesion_negociada.application.procesadores;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.estado_expediente_posesion_negociada.domain.EstadoExpedientePosesionNegociada;
import com.haya.alaska.estado_expediente_posesion_negociada.infastructure.repository.EstadoExpedientePosesionNegociadaRepository;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion;
import com.haya.alaska.estado_ocupacion.infrastructure.repository.EstadoOcupacionRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.origen_expediente_posesion_negociada.domain.OrigenExpedientePosesionNegociada;
import com.haya.alaska.origen_expediente_posesion_negociada.repository.OrigenExpedientePosesionNegociadaRepository;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.util.ExcelImport;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;
import com.haya.alaska.tipo_garantia.infrastructure.repository.TipoGarantiaRepository;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.uso.domain.Uso;
import com.haya.alaska.uso.infrastructure.repository.UsoRepository;
import lombok.AllArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Service
@AllArgsConstructor
public class ProcesarBienes {

  private static final int primera_fila_con_datos = 6;
  private final TipoBienRepository tipoBienRepository;
  private final TipoGarantiaRepository tipoGarantiaRepository;
  private final BienRepository bienRepository;
  private final BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  private final TipoViaRepository tipoViaRepository;
  private final MunicipioRepository municipioRepository;
  private final LocalidadRepository localidadRepository;
  private final EntornoRepository entornoRepository;
  private final ProvinciaRepository provinciaRepository;
  private final EstadoOcupacionRepository estadoOcupacionRepository;
  private final OrigenExpedientePosesionNegociadaRepository origenExpedientePosesionNegociadaRepository;
  private final EstadoExpedientePosesionNegociadaRepository estadoExpedientePosesionNegociadaRepository;
  private final ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  private final UsoRepository usoRepository;
  private final TipoActivoRepository tipoActivoRepository;
  private final PaisRepository paisRepository;

  private HashMap<String, HashMap<String, Object>> catalogos;

  public List<BienPosesionNegociada> procesar(XSSFSheet hojaBienes, Cartera defaultCartera, List<ExpedientePosesionNegociada> expedientesLista, HashMap<String, HashMap<String, Object>> catalogos) throws Exception {
    this.catalogos = catalogos;
    List<BienPosesionNegociada> dev = new ArrayList<>();
    HashMap<String, Integer> map = new HashMap<>();
    fillMap(map);
    XSSFRow filaActual;
    BienPosesionNegociada bienPN;
    Bien bien = null;
    for (int i = primera_fila_con_datos; i <= hojaBienes.getPhysicalNumberOfRows(); i++) {
      filaActual = hojaBienes.getRow(i);
      String hasContent = null;
      if (null != filaActual) {
        hasContent = getRawStringValue(filaActual, 2);
      }
      if (null == hasContent || "".equals(hasContent)) {
        break;
      }
      String idCarga = getRawStringValue(filaActual, map.get("id_Bien"));
      if (null != idCarga) {
        bien = bienRepository.findByIdCarga(idCarga).orElse(null);
      }
      bienPN = bienPosesionNegociadaRepository.findByBien(bien).orElse(null);
      if (null == bien) {
        bien = procesarFilaBien(filaActual, map);
      }
      if (null == bienPN) {
        bienPN = procesarFilaBienPN(filaActual, map, defaultCartera, expedientesLista);
        bienPN.setBien(bien);
        dev.add(bienPN);
      }
    }
    return dev;
  }

  private Bien procesarFilaBien(XSSFRow filaActual, HashMap<String, Integer> map) throws NotFoundException {
    Bien bien = new Bien();
    String idCarga = getRawStringValue(filaActual, map.get("id_Bien"));
    bien.setIdCarga(idCarga);
    bien.setIdHaya(getRawStringValue(filaActual, map.get("id_haya")));
    bien.setIdOrigen(getRawStringValue(filaActual, map.get("id_origen")));
    bien.setTipoBien((TipoBien) this.catalogos.get("tipoBien").get(getRawStringValue(filaActual, map.get("tipo_bien"))));
    if (getIntegerValue(filaActual, map.get("estado")) == 1) bien.setActivo(true);
    bien.setTramitado(false);
    bien.setReferenciaCatastral(getRawStringValue(filaActual, map.get("referencia_catastral")));
    bien.setFinca(getRawStringValue(filaActual, map.get("finca")));
    bien.setLocalidadRegistro(ExcelImport.getStringValue(filaActual, map.get("localidad_registro")));
    bien.setRegistro(getRawStringValue(filaActual, map.get("registro")));
    bien.setTomo(getRawStringValue(filaActual, map.get("tomo")));
    bien.setLibro(getRawStringValue(filaActual, map.get("libro")));
    bien.setFolio(getRawStringValue(filaActual, map.get("folio")));
    bien.setFechaInscripcion(getDateValue(filaActual, map.get("fecha_inscripcion")));
    bien.setIdufir(getRawStringValue(filaActual, map.get("idufir")));

    //FIXME deben ser catalogos
    bien.setReoOrigen(getRawStringValue(filaActual, map.get("reo_origen")));
    bien.setFechaReoConversion(getDateValue(filaActual, map.get("fecha_reo_conversion")));

    bien.setImporteGarantizado(getNumericValue(filaActual, map.get("Importe_garantizado")));
    bien.setResponsabilidadHipotecaria(getNumericValue(filaActual, map.get("responsabilidad_hipotecaria")));
    bien.setPosesionNegociada(getBooleanValue(filaActual, map.get("posesion_negociada")));
    bien.setSubasta(getBooleanValue(filaActual, map.get("subasta")));
    bien.setTipoVia((TipoVia) this.catalogos.get("tipoVia").get(getRawStringValue(filaActual, map.get("tipo_via"))));
    bien.setNombreVia(getRawStringValue(filaActual, map.get("nombre_via")));
    bien.setNumero(getRawStringValue(filaActual, map.get("numero")));
    bien.setPortal((getRawStringValue(filaActual, map.get("portal"))));
    bien.setBloque(getRawStringValue(filaActual, map.get("bloque")));
    bien.setEscalera(getRawStringValue(filaActual, map.get("escalera")));
    bien.setPiso(getRawStringValue(filaActual, map.get("piso")));
    bien.setPuerta(getRawStringValue(filaActual, map.get("puerta")));
    bien.setEntorno((Entorno) this.catalogos.get("entorno").get(getRawStringValue(filaActual, map.get("entorno"))));
    bien.setMunicipio((Municipio) this.catalogos.get("municipio").get(getRawStringValue(filaActual, map.get("municipio"))));
    bien.setLocalidad((Localidad) this.catalogos.get("localidad").get(getRawStringValue(filaActual, map.get("localidad"))));
    bien.setProvincia((Provincia) this.catalogos.get("provincia").get(getRawStringValue(filaActual, map.get("provincia"))));
    bien.setComentarioDireccion(getRawStringValue(filaActual, map.get("comentario_direccion")));
    bien.setCodigoPostal(getRawStringValue(filaActual, map.get("codigo_postal")));
    bien.setPais((Pais) this.catalogos.get("pais").get(getRawStringValue(filaActual, map.get("pais"))));
    bien.setLongitud(getNumericValue(filaActual, map.get("longitud")));
    bien.setLatitud(getNumericValue(filaActual, map.get("latitud")));
    bien.setTipoActivo((TipoActivo) this.catalogos.get("tipoActivo").get(getRawStringValue(filaActual, map.get("tipo_activo"))));
    bien.setUso((Uso) this.catalogos.get("uso").get(getRawStringValue(filaActual, map.get("uso"))));
    bien.setAnioConstruccion(getIntegerValue(filaActual, map.get("año_construccion")));
    bien.setSuperficieSuelo(getNumericValue(filaActual, map.get("superficie_suelo")));
    bien.setSuperficieConstruida(getNumericValue(filaActual, map.get("superficie_construida")));
    bien.setSuperficieUtil(getNumericValue(filaActual, map.get("superficie_util")));
    bien.setAnejoGaraje(getRawStringValue(filaActual, map.get("anejo_garaje")));
    bien.setAnejoTrastero(getRawStringValue(filaActual, map.get("anejo_trastero")));
    bien.setAnejoOtros(getRawStringValue(filaActual, map.get("anejo_otros")));
    bien.setNotas1(getRawStringValue(filaActual, map.get("notas1")));
    bien.setNotas2(getRawStringValue(filaActual, map.get("notas2")));
    bien.setFechaCarga(new Date());
    return bien;
  }

  private BienPosesionNegociada procesarFilaBienPN(XSSFRow filaActual, HashMap<String, Integer> map, Cartera defaultCartera, List<ExpedientePosesionNegociada> expedientesLista) throws Exception {
    BienPosesionNegociada bienPN = new BienPosesionNegociada();
    ExpedientePosesionNegociada expedientePN = expedientePosesionNegociadaRepository.findByIdOrigen(
      getRawStringValue(filaActual, map.get("id_expediente")))
      .orElse(null);
    if (null == expedientePN && null == defaultCartera) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("The portfolio is not reported in the file or by parameter");
      else throw new NotFoundException("No se encuentra informada la cartera en el archivo ni por parametro");
    }
    if (null == expedientePN) {
      expedientePN = new ExpedientePosesionNegociada();
      expedientePN.setCartera(defaultCartera);
      expedientePN.setFechaCreacion(new Date());
      expedientePN.setIdOrigen(getRawStringValue(filaActual, map.get("id_expediente")));
      expedientePN.setOrigen((OrigenExpedientePosesionNegociada) this.catalogos.get("OrigenExpedientePosesionNegociada").get("00"));
      expedientePN.setEstadoExpedienteRecuperacionAmistosa((EstadoExpedientePosesionNegociada) this.catalogos.get("estadoExpedientePosesionNegociada").get("00"));
      //es necesario guardarlo aqui para poder encontrarlo cuando se procese el siguiente bien con el mismo expediente
      expedientesLista.add(expedientePosesionNegociadaRepository.saveAndFlush(expedientePN));
    }
    bienPN.setTipoGarantia((TipoGarantia) this.catalogos.get("tipoGarantia").get(getRawStringValue(filaActual, map.get("tipo_garantia"))));
    bienPN.setExpedientePosesionNegociada(expedientePN);
    bienPN.setEstadoOcupacion((EstadoOcupacion) this.catalogos.get("estadoOcupacion").get(getRawStringValue(filaActual, map.get("estado_ocupacion"))));
    return bienPN;
  }

  private void fillMap(HashMap<String, Integer> map) {
    map.put("id_Bien", 2);
    map.put("id_expediente", 3);
    map.put("id_haya", 4);
    map.put("id_origen", 5);
    map.put("id_origen2", 6);
    map.put("tipo_bien", 7);
    map.put("tipo_garantia", 8);
    map.put("estado", 9);
    map.put("referencia_catastral", 10);
    map.put("finca", 11);
    map.put("localidad_registro", 12);
    map.put("registro", 13);
    map.put("tomo", 14);
    map.put("libro", 15);
    map.put("folio", 16);
    map.put("fecha_inscripcion", 17);
    map.put("idufir", 18);
    map.put("reo_origen", 19);
    map.put("fecha_reo_conversion", 20);
    map.put("Importe_garantizado", 21);
    map.put("responsabilidad_hipotecaria", 22);
    map.put("posesion_negociada", 23);
    map.put("subasta", 24);
    map.put("estado_ocupacion", 25);
    map.put("tipo_via", 26);
    map.put("nombre_via", 27);
    map.put("numero", 28);
    map.put("portal", 29);
    map.put("bloque", 30);
    map.put("escalera", 31);
    map.put("piso", 32);
    map.put("puerta", 33);
    map.put("entorno", 34);
    map.put("municipio", 35);
    map.put("localidad", 36);
    map.put("provincia", 37);
    map.put("comentario_direccion", 38);
    map.put("codigo_postal", 39);
    map.put("pais", 40);
    map.put("longitud", 41);
    map.put("latitud", 42);
    map.put("tipo_activo", 43);
    map.put("uso", 44);
    map.put("año_construccion", 45);
    map.put("superficie_suelo", 46);
    map.put("superficie_construida", 47);
    map.put("superficie_util", 48);
    map.put("anejo_garaje", 49);
    map.put("anejo_trastero", 50);
    map.put("anejo_otros", 51);
    map.put("notas1", 52);
    map.put("notas2", 53);
  }
}
