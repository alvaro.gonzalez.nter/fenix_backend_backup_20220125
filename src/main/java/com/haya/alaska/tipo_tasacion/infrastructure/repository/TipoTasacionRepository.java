package com.haya.alaska.tipo_tasacion.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_tasacion.domain.TipoTasacion;

@Repository
public interface TipoTasacionRepository extends CatalogoRepository<TipoTasacion, Integer> {
	
}
