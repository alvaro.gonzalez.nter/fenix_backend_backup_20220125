package com.haya.alaska.tipo_tasacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.tasacion.domain.Tasacion;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_TASACION")
@Entity
@Table(name = "LKUP_TIPO_TASACION")
public class TipoTasacion extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_TASACION")
  @NotAudited
  private Set<Tasacion> tasaciones = new HashSet<>();
}
