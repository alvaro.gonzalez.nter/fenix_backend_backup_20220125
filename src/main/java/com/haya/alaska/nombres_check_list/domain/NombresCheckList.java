package com.haya.alaska.nombres_check_list.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.datos_check_list.domain.DatosCheckList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_NOMBRES_CHECK_LIST")
@Entity
@Getter
@Table(name = "LKUP_NOMBRES_CHECK_LIST")
@NoArgsConstructor
public class NombresCheckList extends Catalogo {
  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_NOMBRE_CHECK_LIST")
  @NotAudited
  Set<DatosCheckList> datosCheckLists;

  @Column(name = "IND_OBLIGATORIO")
  private Boolean obligatorio;
}
