package com.haya.alaska.nombres_check_list.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.nombres_check_list.domain.NombresCheckList;
import org.springframework.stereotype.Repository;

@Repository
public interface NombresCheckListRepository extends CatalogoRepository<NombresCheckList, Integer> {}
