package com.haya.alaska.estimacion_cumplida.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.estimacion.domain.Estimacion;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_ESTIMACION_CUMPLIDA")
@Entity
@Table(name = "LKUP_ESTIMACION_CUMPLIDA")
public class EstimacionCumplida extends Catalogo{

    private static final long serialVersionUID = 1L;

    @OneToMany(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_ESTIMACION")
    @NotAudited
    private Set<Estimacion> estimaciones = new HashSet<>();
}
