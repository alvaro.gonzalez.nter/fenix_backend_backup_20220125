package com.haya.alaska.estimacion_cumplida.infraestructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estimacion_cumplida.domain.EstimacionCumplida;
import org.springframework.stereotype.Repository;

@Repository
public interface EstimacionCumplidaRepository extends CatalogoRepository<EstimacionCumplida, Integer> {}
