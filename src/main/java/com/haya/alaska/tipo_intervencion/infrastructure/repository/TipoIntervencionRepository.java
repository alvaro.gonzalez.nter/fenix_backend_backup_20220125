package com.haya.alaska.tipo_intervencion.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;

@Repository
public interface TipoIntervencionRepository extends CatalogoRepository<TipoIntervencion, Integer> {
	
}
