package com.haya.alaska.tipo_via.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_via.domain.TipoVia;

@Repository
public interface TipoViaRepository extends CatalogoRepository<TipoVia, Integer> {
	
	  
}
