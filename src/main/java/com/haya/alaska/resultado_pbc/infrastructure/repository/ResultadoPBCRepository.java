package com.haya.alaska.resultado_pbc.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.resultado_pbc.domain.ResultadoPBC;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultadoPBCRepository extends CatalogoRepository<ResultadoPBC, Integer> {}
