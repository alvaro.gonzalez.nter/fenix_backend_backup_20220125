package com.haya.alaska.resultado_pbc.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Audited
@AuditTable(value = "HIST_LKUP_RESULTADO_PBC")
@Entity
@Table(name = "LKUP_RESULTADO_PBC")
@NoArgsConstructor
public class ResultadoPBC extends Catalogo {
}
