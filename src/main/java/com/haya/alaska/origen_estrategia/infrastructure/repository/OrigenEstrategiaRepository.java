package com.haya.alaska.origen_estrategia.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.origen_estrategia.domain.OrigenEstrategia;
import org.springframework.stereotype.Repository;

@Repository
public interface OrigenEstrategiaRepository extends CatalogoRepository<OrigenEstrategia, Integer> {}
