package com.haya.alaska.hito_procedimiento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HitoProcedimientoRepository extends CatalogoRepository<HitoProcedimiento, Integer> {
  List<HitoProcedimiento> findAllByTipoProcedimientoIdAndActivoIsTrueAndRecoveryIsFalse(Integer tipoId);

  List<HitoProcedimiento> findAllByActivoIsTrueAndRecoveryIsFalse();

  Optional<HitoProcedimiento> findByCodigoAndRecoveryIsFalse(String codigo);

  Optional<HitoProcedimiento> findByValorAndRecoveryIsTrue(String valor);

  List<HitoProcedimiento> findAllByValorAndRecoveryIsTrue(String valor);

  List<HitoProcedimiento> findByValorAndRecovery(String valor, boolean recovery);
}
