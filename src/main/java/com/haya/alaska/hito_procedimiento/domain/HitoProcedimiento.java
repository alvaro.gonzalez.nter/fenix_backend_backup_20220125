package com.haya.alaska.hito_procedimiento.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_HITO_PROCEDIMIENTO")
@Entity
@Getter
@Setter
@Table(name = "LKUP_HITO_PROCEDIMIENTO")
public class HitoProcedimiento extends Catalogo {
    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_TIPO_PROCEDIMIENTO")
    private TipoProcedimiento tipoProcedimiento;

    /*@ManyToMany
    @JoinTable(name = "RELA_SEGMENTACION_HITO",
      joinColumns = {@JoinColumn(name = "ID_SEGMENTACION")},
      inverseJoinColumns = {@JoinColumn(name = "ID_HITO")}
    )
    @JsonIgnore
    @AuditJoinTable(name = "HIST_RELA_SEGMENTACION_HITO")
    private Set<Segmentacion> segmentaciones = new HashSet<>();*/

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_HITO_PROCEDIMIENTO")
  @NotAudited
  private Set<Segmentacion> segmentaciones = new HashSet<>();

  @Column(name = "IND_RECOVERY", nullable = false)
  private boolean recovery = false;

  public Integer getNumero() {
    String[] partes = this.getCodigo().split("_");
    String hitoActual = partes[1];
    return Integer.parseInt(hitoActual);
  }
}
