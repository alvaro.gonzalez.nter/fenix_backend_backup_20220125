package com.haya.alaska.deposito_finanzas.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.deposito_finanzas.domain.DepositoFinanzas;
import org.springframework.stereotype.Repository;

@Repository
public interface DepositoFinanzasRepository extends CatalogoRepository<DepositoFinanzas, Integer> {
}
