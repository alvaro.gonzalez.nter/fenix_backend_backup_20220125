package com.haya.alaska.deposito_finanzas.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_DEPOSITO_FINANZAS")
@Entity
@Table(name = "LKUP_DEPOSITO_FINANZAS")
@Getter
public class DepositoFinanzas extends Catalogo {
}
