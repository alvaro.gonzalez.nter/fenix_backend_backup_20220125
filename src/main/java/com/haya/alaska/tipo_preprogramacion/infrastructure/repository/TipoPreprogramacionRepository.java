package com.haya.alaska.tipo_preprogramacion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_preprogramacion.domain.TipoPreprogramacion;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoPreprogramacionRepository extends CatalogoRepository<TipoPreprogramacion, Integer> {

}
