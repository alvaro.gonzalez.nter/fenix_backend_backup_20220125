package com.haya.alaska.tipo_preprogramacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_PREPROGRAMACION")
@Entity
@Table(name = "LKUP_TIPO_PREPROGRAMACION")
@NoArgsConstructor
public class TipoPreprogramacion extends Catalogo {

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_OCUPANTE")
  @NotAudited
  private Set<Ocupante> ocupantes = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_OCUPANTE")
  @NotAudited
  private Set<Segmentacion> segmentaciones = new HashSet<>();
}
