package com.haya.alaska.plazo.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.domain.Catalogo;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_PLAZO")
@Entity
@Table(name = "LKUP_PLAZO")
public class Plazo extends Catalogo {

  private static final long serialVersionUID = 1L;

}
