package com.haya.alaska.plazo.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.plazo.domain.Plazo;
import org.springframework.stereotype.Repository;

@Repository
public interface PlazoRepository
    extends CatalogoRepository<Plazo, Integer> {}
