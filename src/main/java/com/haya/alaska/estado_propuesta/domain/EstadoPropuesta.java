package com.haya.alaska.estado_propuesta.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * EstadoPropuesta entity
 *
 * @author agonzalez
 */
@Audited
@AuditTable(value = "HIST_LKUP_ESTADO_PROPUESTA")
@NoArgsConstructor
@Entity
@Table(name = "LKUP_ESTADO_PROPUESTA")
public class EstadoPropuesta extends Catalogo implements Serializable {
}