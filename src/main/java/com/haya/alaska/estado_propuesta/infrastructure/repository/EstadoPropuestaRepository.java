package com.haya.alaska.estado_propuesta.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoPropuestaRepository extends CatalogoRepository<EstadoPropuesta, Integer> {}
