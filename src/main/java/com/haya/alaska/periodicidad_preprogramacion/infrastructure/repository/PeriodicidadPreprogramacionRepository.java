package com.haya.alaska.periodicidad_preprogramacion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.periodicidad_preprogramacion.domain.PeriodicidadPreprogramacion;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriodicidadPreprogramacionRepository extends CatalogoRepository<PeriodicidadPreprogramacion, Integer> {}
