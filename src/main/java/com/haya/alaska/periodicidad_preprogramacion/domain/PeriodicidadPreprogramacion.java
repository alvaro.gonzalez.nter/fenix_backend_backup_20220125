package com.haya.alaska.periodicidad_preprogramacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@AuditTable(value = "HIST_LKUP_PERIODICIDAD_PREPROGRAMACION")
@NoArgsConstructor
@Entity
@Table(name = "LKUP_PERIODICIDAD_PREPROGRAMACION")
public class PeriodicidadPreprogramacion extends Catalogo {
}
