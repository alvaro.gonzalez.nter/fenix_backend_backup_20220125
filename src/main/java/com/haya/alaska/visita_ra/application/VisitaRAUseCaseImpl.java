package com.haya.alaska.visita_ra.application;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.Bien_;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.modalidad.infrastructure.repository.ModalidadRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.resultado_visita.domain.ResultadoVisita_;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.visita_ra.domain.VisitaRA;
import com.haya.alaska.visita_ra.domain.VisitaRA_;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRADTOToList;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRAInputDto;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRAOutputDto;
import com.haya.alaska.visita_ra.infraestructure.mapper.VisitaRAMapper;
import com.haya.alaska.visita_ra.infraestructure.repository.VisitaRARepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class VisitaRAUseCaseImpl implements VisitaRAUseCase {
  @PersistenceContext
  private EntityManager entityManager;

  @Autowired
  private VisitaRARepository visitaRepository;

  @Autowired
  private BienRepository bienRepository;

  @Autowired
  private ExpedientePosesionNegociadaRepository expedienteRepository;

  @Autowired
  private PropuestaRepository propuestaRepository;

  @Autowired
  private ModalidadRepository modalidadRepository;

  @Autowired
  private VisitaRAMapper visitaMapper;

  @Autowired
  private EventoUseCase eventoUseCase;

  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;

  @Autowired private GestorDocumentalRepository gestorDocumentalRepository;

  public ListWithCountDTO<VisitaRADTOToList> getAllFilteredVisitas(
    Integer idBien,
    String fecha,
    String hora,
    String resultado,
    Boolean geolocalizacion,
    Boolean fotos,
    String notas,
    String orderField,
    String orderDirection,
    Integer size,
    Integer page) {
    List<VisitaRA> visitasSinPaginar =
      this.filterVisitas(
        idBien,
        fecha,
        hora,
        resultado,
        geolocalizacion,
        fotos,
        notas,
        orderField,
        orderDirection);
    List<VisitaRA> visitas =
      visitasSinPaginar.stream().skip(size * page).limit(size).collect(Collectors.toList());

    List<VisitaRADTOToList> visitasDTOS =
      visitaMapper.listVisitaToListVisitaDTOToList(visitasSinPaginar);
    Comparator<VisitaRADTOToList> comparator =
      (var o1, var o2) -> Boolean.compare(o1.isFotos(), o2.isFotos());
    if (orderDirection != null && orderDirection.equals("desc") && orderField.equals("fotos"))
      visitasDTOS = visitasDTOS.stream().sorted(comparator).collect(Collectors.toList());
    else if (orderDirection != null && orderDirection.equals("asc") && orderField.equals("fotos"))
      visitasDTOS = visitasDTOS.stream().sorted(comparator.reversed()).collect(Collectors.toList());

    visitasDTOS = visitasDTOS.stream().skip(size * page).limit(size).collect(Collectors.toList());

    return new ListWithCountDTO<>(visitasDTOS, visitasSinPaginar.size());
  }

  private List<VisitaRA> filterVisitas(
    Integer idBien,
    String fecha,
    String hora,
    String resultado,
    Boolean geolocalizacion,
    Boolean fotos,
    String notas,
    String orderField,
    String orderDirection) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<VisitaRA> query = cb.createQuery(VisitaRA.class);
    Root<VisitaRA> root = query.from(VisitaRA.class);
    List<Predicate> predicates = new ArrayList<>();

    if (idBien != null) {
      predicates.add(
        cb.equal(root.get(VisitaRA_.bien).get(Bien_.id), idBien));
    }
    if (fecha != null) {
      predicates.add(
        cb.like(
          cb.function("date", Date.class, root.get(VisitaRA_.fecha)).as(String.class),
          "%" + fecha + "%"));
    }
    if (hora != null) {
      predicates.add(
        cb.like(
          cb.function("time", Date.class, root.get(VisitaRA_.fecha)).as(String.class),
          "%" + hora + "%"));
    }
    if (resultado != null) {
      predicates.add(
        cb.like(root.get(VisitaRA_.resultado).get(ResultadoVisita_.valor), "%" + resultado + "%"));
    }
    if (geolocalizacion != null) {
      Predicate predicate =
        cb.and(cb.isNotNull(root.get(VisitaRA_.latitud)), cb.isNotNull(root.get(VisitaRA_.longitud)));
      if (!geolocalizacion) {
        predicate = cb.not(predicate);
      }
      predicates.add(predicate);
    }
    if (fotos != null) {
      Predicate predicate = cb.isNotEmpty(root.get(VisitaRA_.fotos));
      if (!fotos) {
        predicate = cb.not(predicate);
      }
      predicates.add(predicate);
    }
    if (notas != null) {
      predicates.add(cb.like(root.get(VisitaRA_.notas), "%" + notas + "%"));
    }

    CriteriaQuery<VisitaRA> rs =
      query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
    if (orderField != null) {
      switch (orderField) {
        case "fecha":
          rs.orderBy(
            getOrden(
              cb,
              cb.function("date", Date.class, root.get(VisitaRA_.fecha)).as(String.class),
              orderDirection));
          break;
        case "hora":
          rs.orderBy(
            getOrden(
              cb,
              cb.function("time", Date.class, root.get(VisitaRA_.fecha)).as(String.class),
              orderDirection));
          break;
        case "resultado":
          rs.orderBy(
            getOrden(
              cb, (root.get(VisitaRA_.resultado).get(ResultadoVisita_.valor)), orderDirection));
          break;
        case "geolocalizacion":
          rs.orderBy(getOrden(cb, (root.get(VisitaRA_.latitud)), orderDirection));
          break;
          /*   case "fotos":
          rs.orderBy(
            getOrden(
              cb, root.get(Visita_.fotos).get(Fotos), orderDirection));
          break;*/
        /*case "notas":
          rs.orderBy(getOrden(cb, root.get(VisitaRA_.notas), orderDirection));
          break;*/
      }
    }
    return entityManager.createQuery(query).getResultList();
  }

  private Order getOrden(CriteriaBuilder cb, Expression expresion, String orderDirection) {
    if (orderDirection == null) {
      orderDirection = "desc";
    }
    return orderDirection.toLowerCase().equalsIgnoreCase("desc")
      ? cb.desc(expresion)
      : cb.asc(expresion);
  }

  @Override
  public VisitaRAOutputDto findVisitaByIdDto(Integer idVisita) throws NotFoundException {
    VisitaRA visita =
      this.visitaRepository
        .findById(idVisita)
        .orElseThrow(
          () ->
            new NotFoundException("visita", idVisita));
    return this.visitaMapper.entityOutputDto(visita);
  }

  @Override
  public VisitaRAOutputDto createVisita(
    Integer idExpediente, Integer idBien, VisitaRAInputDto visitaInputDto, List<MultipartFile> fotos, Usuario usuario) throws Exception {
    VisitaRA visita = new VisitaRA();
    visitaMapper.inputDtoToEntity(visitaInputDto, visita);
    if (isVisitaFutura(visitaInputDto)) {
      throw new Exception("No se pueden crear visitas con fechas futuras");
    }
    visita.setBien(
      this.bienRepository
        .findById(idBien)
        .orElseThrow(
          () ->
            new NotFoundException(
              "Bien de posesión negociada", idBien)));

    this.actualizarGeolocalizacionBien(visita);
    this.procesarFotos(visita, fotos,usuario);
    visita = this.visitaRepository.save(visita);
    eventoUseCase.crearEventoVisitaRA(idExpediente, visita, usuario);

    return this.visitaMapper.entityOutputDto(visita);
  }

  private boolean isVisitaFutura(VisitaRAInputDto visitaInputDto) {
    DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyy HH:mm");
    LocalDateTime dtRecibido =
      LocalDateTime.parse(visitaInputDto.getFecha() + " " + visitaInputDto.getHora(), fmt);
    LocalDateTime dtactual = LocalDateTime.now();
    Instant ahora = dtactual.toInstant(ZoneOffset.UTC);
    LocalDateTime now = LocalDateTime.now();
    Instant recibida = dtRecibido.toInstant(ZoneId.of("Europe/Madrid").getRules().getOffset(now));
    return recibida.isAfter(ahora);
  }

  @Override
  public VisitaRAOutputDto updateVisita(
    Integer idBien, Integer idVisita, VisitaRAInputDto visitaInputDto, List<MultipartFile> fotos,Usuario usuario)
    throws Exception {
    VisitaRA visita =
      this.visitaRepository
        .findById(idVisita)
        .orElseThrow(
          () -> new NotFoundException("Visita", idVisita));

    this.visitaMapper.inputDtoToEntity(visitaInputDto, visita);
    this.visitaRepository.save(visita);
    this.actualizarGeolocalizacionBien(visita);
    if (isVisitaFutura(visitaInputDto)) {
      throw new Exception("No se pueden crear visitas con fechas futuras");
    }
    this.procesarFotos(visita, fotos,usuario);
    this.visitaRepository.save(visita);

    return this.visitaMapper.entityOutputDto(visita);
  }

  private void actualizarGeolocalizacionBien(VisitaRA visita) {
    if (visita.hayGeolocalizacion()) {
      Bien bienActualizado = visita.getBien();
      bienActualizado.setLatitud(visita.getLatitud());
      bienActualizado.setLongitud(visita.getLongitud());
      visita.setBien(bienActualizado);
      this.bienRepository.save(bienActualizado);
    }
  }

  private void procesarFotos(VisitaRA visita, List<MultipartFile> fotos, Usuario usuario) throws IOException {
    if (fotos==null || fotos.isEmpty()) {
      return;
    }
    for (MultipartFile foto : fotos) {
      //Bien bien = visita.getBien();
      //String valor = visita.getBien().getTipoActivo().getValor();

      /*if (visita
        .getBien()
        .getTipoActivo()
        .getValor()
        .equals("Mobiliario(MUEBLE, DERECHOS DE CREDITO, OTROS BIENES,VALORES, etc)")) {*/
        JSONObject metadatos = new JSONObject();
        JSONObject archivoFisico = new JSONObject();
        archivoFisico.put("contenedor", "CONT");
        metadatos.put(
          "General documento",
          new MetadatoDocumentoInputDTO(foto.getOriginalFilename(), "GA-01-FOTO-04")
            .createDescripcionGeneralDocumento());
        metadatos.put("Archivo físico", archivoFisico);
        long idDocumento =
          this.servicioGestorDocumental
            .crearDocumento(
              CodigoEntidad.GARANTIA,
              visita.getBien().getIdHaya(),
              foto,
              metadatos)
            .getIdDocumento();

        Integer idgestorDocumental=(int)idDocumento;
        GestorDocumental gestorDocumental =
          new GestorDocumental(
            idgestorDocumental,
            usuario,
            null,
            new Date(),
            null,
            foto.getOriginalFilename(),
            null,
            null);
        gestorDocumentalRepository.save(gestorDocumental);
        visita.addFoto(idDocumento);
      /*} else {
        JSONObject metadatos = new JSONObject();
        JSONObject archivoFisico = new JSONObject();
        archivoFisico.put("contenedor", "CONT");
        metadatos.put(
          "General documento",
          new MetadatoDocumentoInputDTO(foto.getOriginalFilename(), "AI-01-FOTO-04")
            .createDescripcionGeneralDocumento());
        metadatos.put("Archivo físico", archivoFisico);
        long idDocumento =
          this.servicioGestorDocumental
            .crearDocumento(
              CodigoEntidad.GARANTIA,
              visita.getBien().getIdHaya(),
              foto,
              metadatos)
            .getIdDocumento();
        Integer idgestorDocumental=(int)idDocumento;
        GestorDocumental gestorDocumental =
          new GestorDocumental(
            idgestorDocumental,
            usuario,
            null,
            new Date(),
            null,
            foto.getOriginalFilename(),
            null,
            null);
        gestorDocumentalRepository.save(gestorDocumental);
        visita.addFoto(idDocumento);
      }*/
    }
  }

  private Formalizacion getFormalizacion(Propuesta propuesta) {
    return propuesta.getFormalizaciones().stream().findFirst().orElse(null);
  }

  private String getOperacion(Bien bien) {
    Set<ContratoBien> contratos = bien.getContratos();
    String des_id_origen = "";
    for (ContratoBien contrato : contratos) {
      if (contrato.getContrato().getIdOrigen() != null && !des_id_origen.contains(contrato.getContrato().getIdOrigen()))
        des_id_origen += contrato.getContrato().getIdOrigen() + " ";
    }
    return des_id_origen;
  }

  // Listo lo de garantías y tienen que coincidir con lo que tenemos registrados en la rela
  // fotos_visita
  @Override
  public List<Integer> infoFotos(List<Integer> idVisitas) throws IOException, NotFoundException {

    List<Integer> listIdFotos = new ArrayList<>();

    for (Integer idvisita : idVisitas) {
      VisitaRA visita =
        visitaRepository
          .findById(idvisita)
          .orElseThrow(() -> new NotFoundException("Visita", idvisita));
      List<DocumentoDTO> listadoIdGestor = new ArrayList<>();

      /*if (visita
        .getBien()
        .getTipoActivo()
        .getValor()
        .equals("Mobiliario(MUEBLE, DERECHOS DE CREDITO, OTROS BIENES,VALORES, etc)")) {*/
        listadoIdGestor =
          this.servicioGestorDocumental
            .listarDocumentos(
              CodigoEntidad.GARANTIA, visita.getBien().getIdHaya())
            .stream()
            .map(DocumentoDTO::new)
            .collect(Collectors.toList());
      /*} else {
        listadoIdGestor =
          this.servicioGestorDocumental
            .listarDocumentos(
              CodigoEntidad.ACTIVO_INMOBILIARIO,
              visita.getBien().getIdHaya())
            .stream()
            .map(DocumentoDTO::new)
            .collect(Collectors.toList());
      }*/
      // Conseguimos documentos de las fotos
      List<DocumentoDTO> listFinal =
        listadoIdGestor.stream()
          .filter(documentoDTO -> visita.getFotos().contains(documentoDTO.getId()))
          .collect(Collectors.toList());

      // Pasamos a id y las añadimos al listado
      for (DocumentoDTO documento : listFinal) {
        Integer idfoto = documento.getId().intValue();
        listIdFotos.add(idfoto);
      }
    }
    return listIdFotos;
  }
}
