package com.haya.alaska.visita_ra.application;

import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRADTOToList;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRAInputDto;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRAOutputDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface VisitaRAUseCase {
  ListWithCountDTO<VisitaRADTOToList> getAllFilteredVisitas(Integer idBien, String fecha, String hora, String resultado, Boolean geolocalizacion, Boolean fotos, String notas, String orderField, String orderDirection, Integer size, Integer page);

  VisitaRAOutputDto findVisitaByIdDto(Integer idVisita) throws NotFoundException;

  VisitaRAOutputDto createVisita(Integer idExpediente, Integer idBien, VisitaRAInputDto visitaInputDto, List<MultipartFile> fotos, Usuario usuario) throws Exception;

  VisitaRAOutputDto updateVisita(Integer idBien, Integer idVisita, VisitaRAInputDto visitaInputDto, List<MultipartFile> fotos, Usuario usuario) throws Exception;

  List<Integer> infoFotos(List<Integer> idVisitas) throws IOException, NotFoundException;
}

