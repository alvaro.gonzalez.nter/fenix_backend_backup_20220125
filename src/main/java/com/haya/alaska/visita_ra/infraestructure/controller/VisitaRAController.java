package com.haya.alaska.visita_ra.infraestructure.controller;

import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.visita.application.VisitaUseCase;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaDTOToList;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaInputDto;
import com.haya.alaska.visita.infraestructure.controller.dto.VisitaOutputDto;
import com.haya.alaska.visita.infraestructure.mapper.VisitaMapper;
import com.haya.alaska.visita_ra.application.VisitaRAUseCase;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRADTOToList;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRAInputDto;
import com.haya.alaska.visita_ra.infraestructure.controller.dto.VisitaRAOutputDto;
import com.haya.alaska.visita_ra.infraestructure.mapper.VisitaRAMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientes/{idExpediente}/bien/{idBien}/visitas")
public class VisitaRAController {

  @Autowired
  private VisitaRAUseCase visitaUseCase;
  @Autowired
  private VisitaRAMapper visitaMapper;

  @ApiOperation(value = "Listado visitas",
    notes = "Devuelve todas las visitas registradas con el bien enviado")
  @GetMapping
  public ListWithCountDTO<VisitaRADTOToList> getAllVisitas(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idBien") Integer idBien,
    @RequestParam(value = "fecha", required = false) String fecha,
    @RequestParam(value = "hora", required = false) String hora,
    @RequestParam(value = "resultado", required = false) String resultado,
    @RequestParam(value = "geolocalizacion", required = false) String geolocalizacion,
    @RequestParam(value = "fotos", required = false) String fotos,
    @RequestParam(value = "notas", required = false) String notas,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page
  ) {
    Boolean geolocalizacionBool = null;
    if (geolocalizacion != null) {
      geolocalizacion = geolocalizacion.toLowerCase();
      if (geolocalizacion.startsWith("s")) {
        geolocalizacionBool = true;
      } else if (geolocalizacion.startsWith("n")) {
        geolocalizacionBool = false;
      }
    }
    Boolean fotosBool = null;
    if (fotos != null) {
      fotos = fotos.toLowerCase();
      if (fotos.startsWith("s")) {
        fotosBool = true;
      } else if (fotos.startsWith("n")) {
        fotosBool = false;
      }
    }

    return visitaUseCase.getAllFilteredVisitas(
      idBien, fecha, hora, resultado, geolocalizacionBool, fotosBool, notas,
      orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Obtener visita", notes = "Devuelve la visita con id enviado")
  @GetMapping("/{idVisita}")
  public VisitaRAOutputDto getVisitaById(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idBien") Integer idBien,
    @PathVariable("idVisita") Integer idVisita
  ) throws NotFoundException {
    return visitaUseCase.findVisitaByIdDto(idVisita);
  }

  @ApiOperation(value = "Crear visita")
  @PostMapping
  public VisitaRAOutputDto createVisita(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idBien") Integer idBien,
    @RequestParam(value = "fotos", required = false) List<MultipartFile> fotos,
    @RequestPart(value = "visita") @Valid VisitaRAInputDto visitaInputDto,
    @ApiIgnore CustomUserDetails principal
  ) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return visitaUseCase.createVisita(idExpediente, idBien, visitaInputDto, fotos,usuario);
  }

  // IMPORTANTE, el PutMapping no tiene soporte para el mapeo del RequestPart que utilizamos aqui, por eso es un Post:
  // https://stackoverflow.com/questions/16230291/requestpart-with-mixed-multipart-request-spring-mvc-3-2
  @ApiOperation(value = "Actualizar visita", notes = "Actualizar la visita con id enviado")
  @PostMapping("/{idVisita}")
  public VisitaRAOutputDto updateVisita(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idBien") Integer idBien,
    @PathVariable("idVisita") Integer idVisita,
    @RequestParam(value = "fotos", required = false) List<MultipartFile> fotos,
    @RequestPart(value = "visita") @Valid VisitaRAInputDto visitaInputDto,
    @ApiIgnore CustomUserDetails principal
  ) throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return visitaUseCase.updateVisita(idBien, idVisita, visitaInputDto, fotos, usuario);
  }



  @ApiOperation(value = "Descargar documento", notes = "Obtiene un documento del gestor documental")
  @GetMapping("/info-fotos")
  public List<Integer> descargarFotos(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idBien") Integer idBien, @RequestParam List<Integer> idVisitaList) throws IOException, NotFoundException {
    return visitaUseCase.infoFotos(idVisitaList);
}
}
