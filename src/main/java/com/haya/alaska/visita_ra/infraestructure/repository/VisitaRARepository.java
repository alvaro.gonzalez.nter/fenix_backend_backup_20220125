package com.haya.alaska.visita_ra.infraestructure.repository;

import com.haya.alaska.visita.domain.Visita;
import com.haya.alaska.visita_ra.domain.VisitaRA;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitaRARepository extends JpaRepository<VisitaRA, Integer> {
}
