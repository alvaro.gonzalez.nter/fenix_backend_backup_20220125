package com.haya.alaska.visita_ra.infraestructure.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VisitaRADTOToList {
  private Integer id;
  private String fecha;
  private String hora;
  private String resultado;
  private boolean geolocalizacion;
  private boolean fotos;
  private String notas;
}
