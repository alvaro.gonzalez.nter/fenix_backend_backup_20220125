package com.haya.alaska.visita_ra.infraestructure.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VisitaRAInputDto {
  private String fecha;
  private String hora;
  private Integer resultado;
  private Double latitud;
  private Double longitud;
  private String notas;
}
