package com.haya.alaska.visita_ra.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.modalidad.domain.Modalidad;
import com.haya.alaska.resultado_visita.domain.ResultadoVisita;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Audited
@AuditTable(value = "HIST_MSTR_VISITA_RA")
@Table(name = "MSTR_VISITA_RA")
public class VisitaRA {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN", nullable = false)
  private Bien bien;

  @Column(name = "FCH_FECHA", nullable = false)
  private Date fecha;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESULTADO_VISITA")
  private ResultadoVisita resultado;

  @Column(name = "NUM_LATITUD")
  private Double latitud; // actualizar en Bien

  @Column(name = "NUM_LONGITUD")
  private Double longitud;

  @ElementCollection()
  @CollectionTable(name = "RELA_VISITA_RA_FOTOS", joinColumns = @JoinColumn(name = "ID_VISITA_RA"))
  @Column(name = "ID_FOTO_GESTOR_DOCUMENTAL")
  @NotAudited
  private Set<Long> fotos = new HashSet<>();

  @Lob
  @Column(name = "DES_NOTAS")
  private String notas;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MODALIDAD")
  private Modalidad modalidad;

  public boolean hayGeolocalizacion() {
    return this.latitud != null && this.longitud != null;
  }

  public boolean hayFotos() {
    return !this.fotos.isEmpty();
  }

  public void addFoto(long idDocumento) {
    this.fotos.add(idDocumento);
  }

  public void setFechaHora(String fecha, String hora) throws ParseException {
    if (fecha == null || hora == null) {
      return;
    }
    SimpleDateFormat format;
    Date temp = null;
    format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    try {
      temp = format.parse(fecha + " " + hora);
    } catch (ParseException ex) {
    }

    format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    try {
      temp = format.parse(fecha + " " + hora);
    } catch (ParseException ex) {
    }

    if (temp == null) throw new ParseException("No se reconoce el formato de la fecha", 0);
    this.setFecha(temp);
  }
}
