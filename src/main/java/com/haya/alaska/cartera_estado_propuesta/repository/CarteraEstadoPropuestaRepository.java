package com.haya.alaska.cartera_estado_propuesta.repository;

import com.haya.alaska.cartera_estado_propuesta.domain.CarteraEstadoPropuesta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarteraEstadoPropuestaRepository extends JpaRepository<CarteraEstadoPropuesta, Integer> {
  List<CarteraEstadoPropuesta> findAllByEstadoId(Integer estado);
  //Buscar por cartera Id obtengo todos los estados además de si están activos o no
  List<CarteraEstadoPropuesta> findAllByCarteraId(Integer cartera);
  CarteraEstadoPropuesta findByCarteraIdAndEstadoId(Integer cartera, Integer estado);
}
