package com.haya.alaska.cartera_estado_propuesta.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_RELA_CARTERA_ESTADO_PROPUESTA")
@Entity
@Getter
@Setter
@Table(name = "RELA_CARTERA_ESTADO_PROPUESTA")
public class CarteraEstadoPropuesta implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO")
  private EstadoPropuesta estado;
}
