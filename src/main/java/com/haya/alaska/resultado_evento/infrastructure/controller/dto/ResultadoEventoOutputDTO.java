package com.haya.alaska.resultado_evento.infrastructure.controller.dto;


import com.haya.alaska.resultado_evento.domain.ResultadoEvento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ResultadoEventoOutputDTO implements Serializable {

  private Integer id;
  private Boolean activo;
  private String codigo;
  private String valor;

  public ResultadoEventoOutputDTO(ResultadoEvento resultadoEvento){
    if (resultadoEvento!=null){
      this.id=resultadoEvento.getId();
      this.activo=resultadoEvento.getActivo();
      this.codigo=resultadoEvento.getCodigo();
      String valor = null;
      Locale loc = LocaleContextHolder.getLocale();
      String defaultLocal = loc.getLanguage();
      if (defaultLocal == null || defaultLocal.equals("es")) valor = resultadoEvento.getValor();
      else if (defaultLocal.equals("en")) valor = resultadoEvento.getValorIngles();
      this.valor = valor;
    }
  }

}
