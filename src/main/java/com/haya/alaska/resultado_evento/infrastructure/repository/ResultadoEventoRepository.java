package com.haya.alaska.resultado_evento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.resultado_evento.domain.ResultadoEvento;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultadoEventoRepository extends CatalogoRepository<ResultadoEvento, Integer> {
    List<ResultadoEvento> findBySubtipos_Id(Integer id);
}
