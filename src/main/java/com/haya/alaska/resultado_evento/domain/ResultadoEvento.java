package com.haya.alaska.resultado_evento.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_RESULTADO_EVENTO")
@Entity
@Table(name = "LKUP_RESULTADO_EVENTO")
@NoArgsConstructor
public class ResultadoEvento extends Catalogo {
    @ManyToMany
    @JoinTable(name = "RELA_SUBTIPO_EVENTO_RESULTADO_EVENTO",
            joinColumns = { @JoinColumn(name = "ID_RESULTADO_EVENTO") },
            inverseJoinColumns = { @JoinColumn(name = "ID_SUBTIPO_EVENTO") }
    )
    @JsonIgnore
    @AuditJoinTable(name = "HIST_RELA_SUBTIPO_EVENTO_RESULTADO_EVENTO")
    private Set<SubtipoEvento> subtipos = new HashSet<>();


  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESULTADO_EVENTO")
  @NotAudited
  private Set<Evento> eventos = new HashSet<>();
}
