package com.haya.alaska.recomendacion_no_firma.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.recomendacion_no_firma.domain.RecomendacionNoFirma;
import org.springframework.stereotype.Repository;

@Repository
public interface RecomendacionNoFirmaRepository extends CatalogoRepository<RecomendacionNoFirma, Integer> {
}
