package com.haya.alaska.procedimiento.infrastructure.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.bien_subastado.infrastructure.repository.BienSubastadoRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.estado_procedimiento.domain.EstadoProcedimiento_;
import com.haya.alaska.estado_procedimiento.infrastructure.repository.EstadoProcedimientoRepository;
import com.haya.alaska.estado_subasta.infrastructure.repository.EstadoSubastaRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento_;
import com.haya.alaska.hito_procedimiento.infrastructure.repository.HitoProcedimientoRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.procedimiento.domain.*;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.input.*;
import com.haya.alaska.procedimiento.infrastructure.repository.*;
import com.haya.alaska.provincia.domain.Provincia_;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.resultado_subasta.infrastructure.repository.ResultadoSubastaRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.subasta.domain.Subasta;
import com.haya.alaska.subasta.infrastructure.repository.SubastaRepository;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento_;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
public class ProcedimientoUtil {

  @PersistenceContext
  private EntityManager entityManager;

  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired ProcedimientoRepository procedimientoRepository;
  @Autowired ProcedimientoEtjRepository procedimientoEtjRepository;
  @Autowired ProcedimientoEtnjRepository procedimientoEtnjRepository;
  @Autowired ProcedimientoVerbalRepository procedimientoVerbalRepository;
  @Autowired ProcedimientoConcursalRepository procedimientoConcursalRepository;
  @Autowired ProcedimientoOrdinarioRepository procedimientoOrdinarioRepository;
  @Autowired ProcedimientoMonitorioRepository procedimientoMonitorioRepository;
  @Autowired ProcedimientoHipotecarioRepository procedimientoHipotecarioRepository;
  @Autowired ProcedimientoEjecucionNotarialRepository procedimientoEjecucionNotarialRepository;
  @Autowired TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired EstadoProcedimientoRepository estadoProcedimientoRepository;
  @Autowired SubastaRepository subastaRepository;
  @Autowired ContratoRepository contratoRepository;
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired BienSubastadoRepository bienSubastadoRepository;
  @Autowired EstadoSubastaRepository estadoSubastaRepository;
  @Autowired ResultadoSubastaRepository resultadoSubastaRepository;
  @Autowired BienRepository bienRepository;
  @Autowired ProvinciaRepository provinciaRepository;
  @Autowired HitoProcedimientoRepository hitoProcedimientoRepository;

  public Contrato getContratoByExpedienteAndId(Integer idExpediente, Integer idContrato)
      throws NotFoundException {
    Expediente expediente =
        expedienteRepository
            .findById(idExpediente)
            .orElseThrow(
                () -> new NotFoundException("Expediente", idExpediente));

    Contrato contrato = expediente.getContrato(idContrato);
    if (contrato == null)
      throw new NotFoundException("contrato", idContrato);
    return contrato;
  }

  public List<Procedimiento> filterProcedimientoInMemory(
      Integer idContrato,
      Integer idProcedimiento,
      String tipo,
      String provincia,
      String numeroAutos,
      String plaza,
      String juzgado,
      String importeDemanda,
      String hito,
      String estado,
      String motivo,
      String orderField,
      String orderDirection) {

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Procedimiento> query= cb.createQuery(Procedimiento.class);

    Root<Procedimiento> root = query.from(Procedimiento.class);
    List<Predicate> predicates = new ArrayList<>();

    /*predicates.add(cb.equal(root.get(Procedimiento_.contratos), idContrato));*/
    Join<Procedimiento, Contrato> destinatarioJoin = root.join(Procedimiento_.contratos, JoinType.INNER);
    Predicate onDestinatario = cb.equal(destinatarioJoin.get(Contrato_.id), idContrato);
    destinatarioJoin.on(onDestinatario);
    if (idProcedimiento != null)
      predicates.add(cb.like(root.get(Procedimiento_.id).as(String.class), "%"+ idProcedimiento + "%"));
    if (tipo != null)
      predicates.add(cb.like(root.get(Procedimiento_.tipo).get(TipoProcedimiento_.VALOR), "%" + tipo + "%"));
    if (provincia != null)
      predicates.add(cb.like(root.get(Procedimiento_.provincia).get(Provincia_.VALOR), "%" + provincia + "%"));
    if (numeroAutos != null)
      predicates.add(cb.like(root.get(Procedimiento_.numeroAutos), "%" + numeroAutos + "%"));
    if(plaza != null)
      predicates.add(cb.like(root.get(Procedimiento_.plaza),"%" + plaza + "%"));
    if (juzgado != null)
      predicates.add(cb.like(root.get(Procedimiento_.juzgado),"%" + juzgado + "%"));
    if(importeDemanda != null)
      predicates.add(cb.like(root.get(Procedimiento_.importeDemanda).as(String.class), "%" + importeDemanda.toString() + "%"));
    if (hito != null) {
      Join<Procedimiento, HitoProcedimiento> hitoJoin = root.join(Procedimiento_.HITO_PROCEDIMIENTO, JoinType.INNER);
      Predicate onHito = cb.like(hitoJoin.get(HitoProcedimiento_.valor), "%" + hito + "%");
      hitoJoin.on(onHito);
    }
    if (estado != null)
      predicates.add(cb.like(root.get(Procedimiento_.estado).get(EstadoProcedimiento_.VALOR), "%" + estado + "%"));
    if (motivo != null) {
      predicates.add(cb.like(root.get(Procedimiento_.motivo), "%" + motivo + "%"));
    }

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
    if (orderField != null) {
      if (orderField.equals("hito")){
        Join<Procedimiento, HitoProcedimiento> hitoOrderJoin = root.join(Procedimiento_.HITO_PROCEDIMIENTO, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(hitoOrderJoin.get(orderField)));
        } else {
          rs.orderBy(cb.asc(hitoOrderJoin.get(orderField)));
        }
      } else if (orderField.equals("estado") || orderField.equals("tipo") || orderField.equals("provincia")){
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(orderField).get("valor")));
        } else {
          rs.orderBy(cb.asc(root.get(orderField).get("valor")));
        }
      } else {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(orderField)));
        } else {
          rs.orderBy(cb.asc(root.get(orderField)));
        }
      }
    }
    return entityManager.createQuery(query).getResultList();
  }

  /**
   * Pasa el los datos de entrada al tipo de procedimiento correspondiente, lo guarda como nuevo
   * procedimiento y devuelve el objeto creado
   *
   * @param inputDTO Datos de entrada para el procedimiento
   * @throws IllegalArgumentException si el tipo ( tipoProcedimientoInputDTO ) es incorrecto
   * @return Procedimiento después de ser guardado en la base de datos
   */
  public Procedimiento getAndCreateProcedimiento(ProcedimientoInputDTO<?> inputDTO, Integer idContrato)
      throws NotFoundException, IOException {
    Object tipoProcedimientoId = inputDTO.getTipo();
    Integer orden = obtenerOrden(idContrato, (String) inputDTO.getNumeroAutos());

    switch (tipoProcedimientoId.toString()) {
      case "7":
        ProcedimientoEtj procedimientoEtj = new ProcedimientoEtj(orden);
        procedimientoEtj = saveProcedimientoEtj(inputDTO, procedimientoEtj);
        return procedimientoEtj;

      case "2":
        ProcedimientoEtnj procedimientoEtnj = new ProcedimientoEtnj(orden);
        procedimientoEtnj = saveProcedimientoEtnj(inputDTO, procedimientoEtnj);
        return procedimientoEtnj;

      case "5":
        ProcedimientoVerbal procedimientoVerbal = new ProcedimientoVerbal(orden);
        procedimientoVerbal = saveProcedimientoVerbal(inputDTO, procedimientoVerbal);
        return procedimientoVerbal;

      case "8":
        ProcedimientoConcursal procedimientoConcursal = new ProcedimientoConcursal(orden);
        procedimientoConcursal = saveProcedimientoConcursal(inputDTO, procedimientoConcursal);
        return procedimientoConcursal;

      case "4":
        ProcedimientoMonitorio procedimientoMonitorio = new ProcedimientoMonitorio(orden);
        procedimientoMonitorio = saveProcedimientoMonitorio(inputDTO, procedimientoMonitorio);
        return procedimientoMonitorio;

      case "6":
        ProcedimientoOrdinario procedimientoOrdinario = new ProcedimientoOrdinario(orden);
        saveProcedimientoOrdinario(inputDTO, procedimientoOrdinario);
        return procedimientoOrdinario;

      case "1":
        ProcedimientoHipotecario procedimientoHipotecario = new ProcedimientoHipotecario(orden);
        procedimientoHipotecario = saveProcedimientoHipotecario(inputDTO, procedimientoHipotecario);
        return procedimientoHipotecario;

      case "3":
        ProcedimientoEjecucionNotarial procedimientoEjecucionNotarial =
            new ProcedimientoEjecucionNotarial(orden);
        procedimientoEjecucionNotarial =
            saveProcedimientoEjecucionNotarial(inputDTO, procedimientoEjecucionNotarial);
        return procedimientoEjecucionNotarial;
      default:
        throw new IllegalArgumentException("invalid tipoProcedimientoId: " + tipoProcedimientoId);
    }
  }

  public Integer obtenerOrden(Integer idContrato, String numeroAutos) {
    return procedimientoRepository
            .findAllByContratosIdAndNumeroAutosAndOrdenGreaterThan(idContrato, numeroAutos, 0)
            .size()
        + 1;
  }

  public void copyProcedimientoInputDTOIntoProcedimiento(
      ProcedimientoInputDTO<?> procedimientoInputDTO, Procedimiento procedimiento) {
    BeanUtils.copyProperties(
        procedimientoInputDTO,
        procedimiento,
        "detalle",
        "demandados",
        "contratos",
        "subastas",
        "tipo",
        "estado");
  }

  public ProcedimientoEtj saveProcedimientoEtj(
          ProcedimientoInputDTO<?> inputDTOMap, ProcedimientoEtj procedimientoEtj) throws NotFoundException {

    ProcedimientoInputDTO<ProcedimientoEtjInputDTO> procedimientoInputDTO =
            new ObjectMapper().convertValue(inputDTOMap, ProcedimientoInputDTO.class);

    copyProcedimientoInputDTOIntoProcedimiento(procedimientoInputDTO, procedimientoEtj);

    Object detalleProcedimientoInputDTO = inputDTOMap.getDetalle();
    ProcedimientoEtjInputDTO procedimientoEtjInputDTO =
            new ObjectMapper().convertValue(detalleProcedimientoInputDTO, ProcedimientoEtjInputDTO.class);

    BeanUtils.copyProperties(procedimientoEtjInputDTO, procedimientoEtj);
    setEntidadesComunes(procedimientoEtj, procedimientoInputDTO);
    return procedimientoEtjRepository.saveAndFlush(procedimientoEtj);
  }

  public ProcedimientoEtnj saveProcedimientoEtnj(
          ProcedimientoInputDTO<?> inputDTOMap, ProcedimientoEtnj procedimientoEtnj)
          throws IOException, NotFoundException {

    ProcedimientoInputDTO<ProcedimientoEtnjInputDTO> procedimientoInputDTO =
            new ObjectMapper().convertValue(inputDTOMap, ProcedimientoInputDTO.class);

    copyProcedimientoInputDTOIntoProcedimiento(procedimientoInputDTO, procedimientoEtnj);

    Object detalleProcedimientoInputDTO = inputDTOMap.getDetalle();
    ProcedimientoEtnjInputDTO procedimientoEtnjInputDTO =
            new ObjectMapper().convertValue(detalleProcedimientoInputDTO, ProcedimientoEtnjInputDTO.class);

    BeanUtils.copyProperties(procedimientoEtnjInputDTO, procedimientoEtnj);
    setEntidadesComunes(procedimientoEtnj, procedimientoInputDTO);
    return procedimientoEtnjRepository.saveAndFlush(procedimientoEtnj);
  }

  public ProcedimientoVerbal saveProcedimientoVerbal(
          ProcedimientoInputDTO<?> inputDTOMap, ProcedimientoVerbal procedimientoVerbal)
          throws NotFoundException {

    ProcedimientoInputDTO<ProcedimientoVerbalInputDTO> procedimientoInputDTO =
            new ObjectMapper().convertValue(inputDTOMap, ProcedimientoInputDTO.class);

    copyProcedimientoInputDTOIntoProcedimiento(procedimientoInputDTO, procedimientoVerbal);

    Object detalleProcedimientoInputDTO = inputDTOMap.getDetalle();
    ProcedimientoVerbalInputDTO procedimientoVerbalInputDTO =
            new ObjectMapper()
                    .convertValue(detalleProcedimientoInputDTO, ProcedimientoVerbalInputDTO.class);

    BeanUtils.copyProperties(procedimientoVerbalInputDTO, procedimientoVerbal);
    setEntidadesComunes(procedimientoVerbal, procedimientoInputDTO);
    return procedimientoVerbalRepository.saveAndFlush(procedimientoVerbal);
  }

  public ProcedimientoConcursal saveProcedimientoConcursal(
          ProcedimientoInputDTO<?> inputDTOMap, ProcedimientoConcursal procedimientoConcursal)
          throws IOException, NotFoundException {
    ProcedimientoInputDTO<ProcedimientoConcursalInputDTO> procedimientoInputDTO =
            new ObjectMapper().convertValue(inputDTOMap, ProcedimientoInputDTO.class);

    copyProcedimientoInputDTOIntoProcedimiento(procedimientoInputDTO, procedimientoConcursal);

    Object detalleProcedimientoInputDTO = inputDTOMap.getDetalle();
    ProcedimientoConcursalInputDTO procedimientoConcursalInputDTO =
            new ObjectMapper()
                    .convertValue(detalleProcedimientoInputDTO, ProcedimientoConcursalInputDTO.class);

    BeanUtils.copyProperties(
            procedimientoConcursalInputDTO, procedimientoConcursal);
    setEntidadesComunes(procedimientoConcursal, procedimientoInputDTO);
    return procedimientoConcursalRepository.saveAndFlush(procedimientoConcursal);
  }

  public ProcedimientoOrdinario saveProcedimientoOrdinario(
          ProcedimientoInputDTO<?> inputDTOMap, ProcedimientoOrdinario procedimientoOrdinario)
          throws IOException, NotFoundException {
    ProcedimientoInputDTO<ProcedimientoOrdinarioInputDTO> procedimientoInputDTO =
            new ObjectMapper().convertValue(inputDTOMap, ProcedimientoInputDTO.class);

    copyProcedimientoInputDTOIntoProcedimiento(procedimientoInputDTO, procedimientoOrdinario);

    Object detalleProcedimientoInputDTO = inputDTOMap.getDetalle();
    ProcedimientoOrdinarioInputDTO procedimientoOrdinarioInputDTO =
            new ObjectMapper()
                    .convertValue(detalleProcedimientoInputDTO, ProcedimientoOrdinarioInputDTO.class);

    BeanUtils.copyProperties(
            procedimientoOrdinarioInputDTO, procedimientoOrdinario);
    setEntidadesComunes(procedimientoOrdinario, procedimientoInputDTO);
    return procedimientoOrdinarioRepository.saveAndFlush(procedimientoOrdinario);
  }

  public ProcedimientoEjecucionNotarial saveProcedimientoEjecucionNotarial(
          ProcedimientoInputDTO<?> inputDTOMap,
          ProcedimientoEjecucionNotarial procedimientoEjecucionNotarial)
          throws IOException, NotFoundException {

    ProcedimientoInputDTO<ProcedimientoEjecucionNotarialInputDTO> procedimientoInputDTO =
            new ObjectMapper().convertValue(inputDTOMap, ProcedimientoInputDTO.class);

    copyProcedimientoInputDTOIntoProcedimiento(
            procedimientoInputDTO, procedimientoEjecucionNotarial);

    Object detalleProcedimientoInputDTO = inputDTOMap.getDetalle();
    ProcedimientoEjecucionNotarialInputDTO procedimientoEjecucionNotarialInputDTO =
            new ObjectMapper()
                    .convertValue(
                            detalleProcedimientoInputDTO, ProcedimientoEjecucionNotarialInputDTO.class);

    BeanUtils.copyProperties(
            procedimientoEjecucionNotarialInputDTO, procedimientoEjecucionNotarial);
    setEntidadesComunes(procedimientoEjecucionNotarial, procedimientoInputDTO);
    return procedimientoEjecucionNotarialRepository.saveAndFlush(procedimientoEjecucionNotarial);
  }

  public ProcedimientoHipotecario saveProcedimientoHipotecario(
          ProcedimientoInputDTO<?> inputDTOMap, ProcedimientoHipotecario procedimientoHipotecario)
          throws IOException, NotFoundException {

    ProcedimientoInputDTO<ProcedimientoHipotecarioInputDTO> procedimientoInputDTO =
            new ObjectMapper().convertValue(inputDTOMap, ProcedimientoInputDTO.class);

    copyProcedimientoInputDTOIntoProcedimiento(procedimientoInputDTO, procedimientoHipotecario);

    Object detalleProcedimientoInputDTO = inputDTOMap.getDetalle();
    ProcedimientoHipotecarioInputDTO procedimientoHipotecarioInputDTO =
            new ObjectMapper()
                    .convertValue(detalleProcedimientoInputDTO, ProcedimientoHipotecarioInputDTO.class);

    BeanUtils.copyProperties(
            procedimientoHipotecarioInputDTO, procedimientoHipotecario);

    setEntidadesComunes(procedimientoHipotecario, procedimientoInputDTO);
    return procedimientoHipotecarioRepository.saveAndFlush(procedimientoHipotecario);
  }

  public ProcedimientoMonitorio saveProcedimientoMonitorio(
          ProcedimientoInputDTO<?> inputDTOMap, ProcedimientoMonitorio procedimientoMonitorio)
          throws IOException, NotFoundException {

    ProcedimientoInputDTO<ProcedimientoMonitorioInputDTO> procedimientoInputDTO =
            new ObjectMapper().convertValue(inputDTOMap, ProcedimientoInputDTO.class);
    copyProcedimientoInputDTOIntoProcedimiento(procedimientoInputDTO, procedimientoMonitorio);
    Object detalleProcedimientoInputDTO = inputDTOMap.getDetalle();
    ProcedimientoMonitorioInputDTO procedimientoMonitorioInputDTO =
            new ObjectMapper()
                    .convertValue(detalleProcedimientoInputDTO, ProcedimientoMonitorioInputDTO.class);

    BeanUtils.copyProperties(
            procedimientoMonitorioInputDTO, procedimientoMonitorio);
    setEntidadesComunes(procedimientoMonitorio, procedimientoInputDTO);
    return procedimientoMonitorioRepository.saveAndFlush(procedimientoMonitorio);
  }

  private void setEntidadesComunes(Procedimiento procedimiento, ProcedimientoInputDTO<?> input)
          throws NotFoundException {

    if (input.getProvincia() != null) { // provincia puede ser null
      procedimiento.setProvincia(provinciaRepository.findById(input.getProvincia()).orElse(null));
    }
    if (procedimiento.getId() == null) {
      procedimiento.setTipo(
          tipoProcedimientoRepository
              .findById(input.getTipo())
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "TipoProcedimiento", input.getTipo())));
    }
    HitoProcedimiento hito = getHitoActual(procedimiento);
    procedimiento.setHitoProcedimiento(hito);

    procedimiento.setEstado(
        estadoProcedimientoRepository
            .findById(input.getEstado())
            .orElseThrow(
                () ->
                    new NotFoundException("Estado", input.getEstado())));

    procedimientoRepository.saveAndFlush(procedimiento);
    for (ProcedimientoInputSubastaDTO subastaDto : input.getSubastas()) {
      Integer subastaId = subastaDto.getId();
      if (subastaId == null) { // crear subasta
        Subasta subasta = new Subasta();
        actualizarDatosSubasta(subastaDto, subasta, procedimiento);
        procedimiento.getSubastas().add(subasta);
      }

      if (subastaId != null) { // actualizar subasta
        Subasta subasta =
            subastaRepository
                .findById(subastaId)
                .orElseThrow(
                    () -> new NotFoundException("Subasta", subastaId));
        actualizarDatosSubasta(subastaDto, subasta, procedimiento);
        procedimiento.getSubastas().add(subasta);
      }
    }

    for (Integer id : input.getContratos()) {
      List<Contrato> contratos = procedimiento.getContratos().stream().filter(c -> {
        return c.getId().equals(id);
      }).collect(Collectors.toList());
      if (contratos.size() > 1){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Several contracts with id: " + id + " are related to the procedure of id: " + procedimiento.getId());
        else throw new NotFoundException("Varios contratos con id: " + id + " están relacionados al procedimiento de id: " + procedimiento.getId());
      }
      if (contratos.isEmpty()){
        Contrato contrato = contratoRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Contrato", id));
        procedimiento.getContratos().add(contrato);
      }
    }

    for (Integer id : input.getDemandados()) {
      List<Interviniente> intervinientes = procedimiento.getDemandados().stream().filter(c -> {
        return c.getId() == id;
      }).collect(Collectors.toList());
      if (intervinientes.size() > 1){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Several interveners with id:" + id + "are related to the procedure of id:" + procedimiento.getId());
        else throw new NotFoundException(
                "Varios intervinientes con id: " + id + " están relacionados al procedimiento de id: " + procedimiento.getId());
      }
      if (intervinientes.isEmpty()){
        Interviniente interviniente =
                intervinienteRepository
                        .findById(id)
                        .orElseThrow(
                                () -> new NotFoundException("Interviniente", id));
        procedimiento.getDemandados().add(interviniente);
      }

    }
    procedimientoRepository.saveAndFlush(procedimiento);
  }

  private void actualizarDatosSubasta(
      ProcedimientoInputSubastaDTO subastaDto, Subasta subasta, Procedimiento procedimiento)
      throws NotFoundException {
    subasta.setActivo(subastaDto.isActivo());
    subasta.setDecisionSuspension(subastaDto.getDecisionSuspension());
    subasta.setEstado(estadoSubastaRepository.findById(subastaDto.getEstado()).orElse(null));
    subasta.setFechaFinPujas(subastaDto.getFechaFinPujas());
    subasta.setFechaInicioPujas(subastaDto.getFechaInicioPujas());
    subasta.setFechaNotificacionDecreto(subastaDto.getFechaNotificacionDecreto());
    subasta.setFechaPagoTasa(subastaDto.getFechaPagoTasa());
    subasta.setMotivoSuspensionCancelacion(subastaDto.getMotivoSuspensionCancelacion());
    subasta.setOrden(subastaDto.getOrden());
    subasta.setProcedimiento(procedimiento);

    subastaRepository.saveAndFlush(subasta);

    List<ProcedimientoInputBienSubastadoDTO> bienesSubastados = subastaDto.getBienesSubastados();
    for (ProcedimientoInputBienSubastadoDTO bienSubastadoDto : bienesSubastados) {
      Integer idBienSubastado = bienSubastadoDto.getId();
      if (idBienSubastado == null) { // nuevo biensubastado
        BienSubastado bienSubastado = new BienSubastado();
        actualizarBienSubastado(bienSubastadoDto, bienSubastado, subasta);

        bienSubastadoRepository.saveAndFlush(bienSubastado);
        subasta.getBienesSubastados().add(bienSubastado);
      }
      if (idBienSubastado != null) { // actualizar biensubastado
        BienSubastado bienSubastado =
            bienSubastadoRepository
                .findById(idBienSubastado)
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "bien subastado", idBienSubastado));
        actualizarBienSubastado(bienSubastadoDto, bienSubastado, subasta);

        bienSubastadoRepository.saveAndFlush(bienSubastado);
        subasta.getBienesSubastados().add(bienSubastado);
      }
    }
  }

  private void actualizarBienSubastado(
      ProcedimientoInputBienSubastadoDTO bienSubastadoDto,
      BienSubastado bienSubastado,
      Subasta subasta)
      throws NotFoundException {
    bienSubastado.setBien(bienRepository.findById(bienSubastadoDto.getBien()).orElse(null));
    bienSubastado.setSubasta(subasta);
    bienSubastado.setDetalleResultado(bienSubastadoDto.getDetalleResultado());
    bienSubastado.setEntidadAdjudicada(bienSubastadoDto.getEntidadAdjudicada());
    bienSubastado.setFechaDecretoAdjudicacion(bienSubastadoDto.getFechaDecretoAdjudicacion());
    bienSubastado.setFechaPosteriorLanzamiento(bienSubastadoDto.getFechaPosteriorLanzamiento());
    bienSubastado.setFechaSenyalamiento(bienSubastadoDto.getFechaSenyalamiento());
    bienSubastado.setFechaTestimonio(bienSubastadoDto.getFechaTestimonio());
    bienSubastado.setImporteAdjudicacion(bienSubastadoDto.getImporteAdjudicacion());
    bienSubastado.setNumeroFinca(bienSubastadoDto.getNumeroFinca());
    bienSubastado.setPostores(bienSubastadoDto.isPostores());
    Integer resultadoSubasta = bienSubastadoDto.getResultadoSubasta();
    if (resultadoSubasta != null) {
      bienSubastado.setResultadoSubasta(
          resultadoSubastaRepository
              .findById(resultadoSubasta)
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "resultado subasta", resultadoSubasta)));
    }
  }

  public HitoProcedimiento getHitoActual(Procedimiento procedimiento) throws NotFoundException {
    String hito = procedimiento.getHito();
    if (hito == null) return null;
    String tipo = procedimiento.getTipo().getCodigo();
    HitoProcedimiento result = hitoProcedimientoRepository.findByCodigoAndRecoveryIsFalse(tipo + "_" + hito).orElse(null);
    if (result == null){
      throw new NotFoundException("hito del Procedimiento", "código", tipo + "_" + hito);
    }
    return result;
  }
}
