package com.haya.alaska.procedimiento.infrastructure.controller.dto.output;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.integraciones.recovery.model.Asunto;
import com.haya.alaska.integraciones.recovery.model.LineaProcesal;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.subasta.infrastructure.controller.dto.SubastaDTOToList;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProcedimientoOutputDTO<T> {
  private Integer id;
  private Boolean activo;
  private String demandante;
  private String despacho;
  private String despachoContrario;
  private String emailGestorJudicialHaya;
  private String emailLetrado;
  private String emailLetradoContrario;
  private String emailProcurador;
  private String emailProcuradorContrario;
  private Double entregasJudiciales;
  private Double gastosIncurridos;
  private String gestorJudicialHaya;
  private String habilitaciones;
  private Double importeDemanda;
  private String juzgado;
  private String letrado;
  private String letradoContrario;
  private Integer numero;
  private String numeroAutos;
  private String plaza;
  private String procurador;
  private String procuradorContrario;
  private String telefono2Letrado;
  private String telefono2LetradoContrario;
  private String telefonoLetrado;
  private String telefonoLetradoContrario;
  private String telefonoProcurador;
  private String telefonoProcuradorContrario;
  private CatalogoMinInfoDTO estado;
  private CatalogoMinInfoDTO provincia;
  private CatalogoMinInfoDTO tipo;
  private T detalle;

  private CatalogoMinInfoDTO hito;
  private String motivo;

  /*Campos extra concursal*/
  private String nombreAdminConcursal;
  private String direccionAdminConcursal;
  private String telefonoAdminConcursal;
  private String emailAdminConcursal;
  private String gestorConcursosHaya;
  private String mailGestorConcursosHaya;

  private List<ContratoDTOToList> contratos;
  private List<IntervinienteDTOToList> demandados;
  private List<SubastaDTOToList> subastas;

  public ProcedimientoOutputDTO(Asunto asunto) {
    this.setId(asunto.getId().intValue());
    this.setDespacho(asunto.getDespacho());
    this.setMotivo(asunto.getMotivo());
  }

  public ProcedimientoOutputDTO(Asunto asunto, LineaProcesal lineaProcesalPrincipal, List<ContratoDTOToList> contratos, List<IntervinienteDTOToList> intervinientes, List<SubastaDTOToList> subastasDto) {
    this.setId(asunto.getId().intValue());
    this.setActivo(null);//No viene informado en Recovery
    this.setDemandante(null);//No viene informado en Recovery
    this.setDespacho(asunto.getDespacho());
    this.setDespachoContrario(null);//No viene informado en Recovery
    this.setMotivo(asunto.getMotivo());
    this.setEmailGestorJudicialHaya(lineaProcesalPrincipal.getGestorLitigio() != null ? lineaProcesalPrincipal.getGestorLitigio().getEmail() : "");
    this.setEmailLetrado(lineaProcesalPrincipal.getEmailLetrado());
    this.setEmailLetradoContrario(null);//No viene informado en Recovery
    this.setEmailProcurador(lineaProcesalPrincipal.getEmailProcurador());
    this.setEmailProcuradorContrario(null);//No viene informado en Recovery
    this.setEntregasJudiciales(null);//No viene informado en Recovery
    this.setGastosIncurridos(null);//No viene informado en Recovery
    this.setGestorJudicialHaya(lineaProcesalPrincipal.getGestorLitigio() != null ? lineaProcesalPrincipal.getGestorLitigio().getNombre() : "");
    this.setHabilitaciones("");//No viene informado en Recovery
    this.setImporteDemanda(lineaProcesalPrincipal.getImporteDemanda());
    this.setJuzgado(lineaProcesalPrincipal.getJuzgado());
    this.setLetrado(lineaProcesalPrincipal.getLetrado());
    this.setLetradoContrario(asunto.getLetradoContrario());//No viene informado en Recovery
    this.setNumero(lineaProcesalPrincipal.getOrden());
    this.setNumeroAutos(lineaProcesalPrincipal.getNumeroAutos());
    this.setPlaza(lineaProcesalPrincipal.getPlaza());
    this.setProcurador(lineaProcesalPrincipal.getProcurador());
    this.setProcuradorContrario(asunto.getProcuradorContrario());//No viene informado en Recovery
    this.setTelefono2Letrado(lineaProcesalPrincipal.getTelefono2Letrado());
    this.setTelefono2LetradoContrario(null);//No viene informado en Recovery
    this.setTelefonoLetrado(lineaProcesalPrincipal.getTelefonoLetrado());
    this.setTelefonoLetradoContrario(null);//No viene informado en Recovery
    this.setNombreAdminConcursal(lineaProcesalPrincipal.getNombreAdminConcursal());
    this.setDireccionAdminConcursal(lineaProcesalPrincipal.getDireccionAdminConcursal());
    this.setTelefonoAdminConcursal(lineaProcesalPrincipal.getTelefonoAdminConcursal());
    this.setEmailAdminConcursal(lineaProcesalPrincipal.getEmailAdminConcursal());
    this.setGestorConcursosHaya(lineaProcesalPrincipal.getGestorConcursosHaya());
    this.setMailGestorConcursosHaya(lineaProcesalPrincipal.getMailGestorConcursosHaya());

    this.setSubastas(subastasDto);
    this.setContratos(contratos);
    this.setDemandados(intervinientes);
  }
}
