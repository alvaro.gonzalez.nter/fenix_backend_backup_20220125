package com.haya.alaska.procedimiento.infrastructure.controller.dto.output;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.haya.alaska.procedimiento.domain.Procedimiento;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProcedimientoEjecucionNotarialOutputDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private Date fechaFirmaActa;
  private Date fechaRecepcionCertificacionRegistral;
  private Date fechaReqPagoDeudor;
  private Date fechaAuto;
  private Date fechaSubasta;
  private Boolean celebrada;
  private Boolean resultadoSubasta;
  private String motivoSuspension;
  private Date fechaFirmaEscritura;
  private Date fechaPosesion;

  public ProcedimientoEjecucionNotarialOutputDTO(Procedimiento procedimiento) {
    BeanUtils.copyProperties(procedimiento, this);
  }
}
