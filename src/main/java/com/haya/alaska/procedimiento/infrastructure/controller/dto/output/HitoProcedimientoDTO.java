package com.haya.alaska.procedimiento.infrastructure.controller.dto.output;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoDTO;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class HitoProcedimientoDTO extends CatalogoDTO {

  Integer tipoProcedimiento;

  public HitoProcedimientoDTO(HitoProcedimiento hitoProcedimiento){
    super(hitoProcedimiento);
    this.tipoProcedimiento = hitoProcedimiento.getTipoProcedimiento().getId();
  }
}
