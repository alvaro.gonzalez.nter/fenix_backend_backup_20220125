package com.haya.alaska.procedimiento.infrastructure.controller.dto.output;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.haya.alaska.procedimiento.domain.Procedimiento;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProcedimientoOrdinarioOutputDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private Date fechaPresentacionDemanda;
  private Double principalDemanda;
  private Date fechaAdmisionDemanda;
  private Boolean resultadoNotificacionReqPago;
  private String direccionNotificacion;
  private Date fechaReqPagoDeudor;
  private Date fechaOposicion;
  private Boolean resultadoOposicion;

  public ProcedimientoOrdinarioOutputDTO(Procedimiento procedimiento) {
    BeanUtils.copyProperties(procedimiento, this);
  }
}
