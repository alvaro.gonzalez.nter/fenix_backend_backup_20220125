package com.haya.alaska.procedimiento.infrastructure.controller.dto.input;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProcedimientoEjecucionNotarialInputDTO implements Serializable{

  private static final long serialVersionUID = 1L;
  private Date fechaFirmaActa;
  private Date fechaRecepcionCertificacionRegistral;
  private Date fechaReqPagoDeudor;
  private Date fechaAuto;
  private Date fechaSubasta;
  private Boolean celebrada;
  private Boolean resultadoSubasta;
  private String motivoSuspension;
  private Date fechaFirmaEscritura;
  private Date fechaPosesion;

}
