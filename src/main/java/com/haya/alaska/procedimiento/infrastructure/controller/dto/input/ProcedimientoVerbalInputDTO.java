package com.haya.alaska.procedimiento.infrastructure.controller.dto.input;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProcedimientoVerbalInputDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private Date fechaPresentacionDemanda;
  private Double principalDemanda;
  private Date fechaAdmisionDemanda;
  private Date fechaNotificacion;
  private Boolean resultadoNotificacion;
  private Date fechaCelebracionJuicio;
  private Boolean resolucion;
}
