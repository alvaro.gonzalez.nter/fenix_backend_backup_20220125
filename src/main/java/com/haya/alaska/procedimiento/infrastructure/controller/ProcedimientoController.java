package com.haya.alaska.procedimiento.infrastructure.controller;

import com.haya.alaska.integraciones.recovery.ServicioRecovery;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTO;
import com.haya.alaska.procedimiento.application.ProcedimientoUseCase;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.ProcedimientoDTOToList;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.input.ProcedimientoInputDTO;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.output.ProcedimientoOutputDTO;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/expedientes/{idExpediente}/contratos/{idContrato}/judicial")
public class ProcedimientoController {

  @Autowired
  private ProcedimientoUseCase procedimientoUseCase;

  @Autowired
  private ServicioRecovery servicioRecovery;

  @ApiOperation(value = "Listado procedimientos", notes = "Devuelve todos los procedimientos")
  @GetMapping
  public ListWithCountDTO<ProcedimientoDTOToList> getAllProcedimientos(
    @RequestParam(value = "id", required = false) Integer id,
    @RequestParam(value = "tipo", required = false) String tipo,
    @RequestParam(value = "provincia", required = false) String provincia,
    @RequestParam(value = "numeroAutos", required = false) String numeroAutos,
    @RequestParam(value = "plaza", required = false) String plaza,
    @RequestParam(value = "juzgado", required = false) String juzgado,
      @RequestParam(value = "importeDemanda", required = false) String importeDemanda,
      @RequestParam(value = "hito", required = false) String hito,
      @RequestParam(value = "estado", required = false) String estado,
      @RequestParam(value = "motivo", required = false) String motivo,
      @RequestParam(value = "orderField", required = false) String orderField,
      @RequestParam(value = "orderDirection", required = false) String orderDirection,
      @RequestParam(value = "size") Integer size,
      @RequestParam(value = "page") Integer page,
      @PathVariable(value = "idContrato") Integer idContrato)
    throws Exception {
    return procedimientoUseCase.findAllProcedimientosDto(
        size,
        page,
        idContrato,
        orderField,
        orderDirection,
            id,
        tipo,
        provincia,
        numeroAutos,
        plaza,
        juzgado,
        importeDemanda,
        hito,
        estado,
        motivo);
  }

  /**
   * Busca un procedimiento a partir de un expediente, contrato y el id del procedimiento, lo
   * devuelve como DTO
   *
   * @param idExpediente Expediente sobre el que empezar a buscar
   * @param idContrato Contrato dentro del expediente
   * @param idProcedimiento Procedimiento dentro del contrato
   * @return ProcedimientoDTO del procedimiento que se encuentra dentro de los 2 objetos
   * @throws NotFoundException Cuando la búsqueda de alguno de los 3 objetos falla
   */
  @ApiOperation(
    value = "Obtener un procedimiento a partir d eun expediente y un contrato",
    notes = "Devuelve un procedimiento")
  @GetMapping("{idProcedimiento}")
  public ProcedimientoOutputDTO getProcedimientoById(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idContrato") Integer idContrato,
    @PathVariable("idProcedimiento") Integer idProcedimiento) throws NotFoundException, IOException {
    return procedimientoUseCase.findProcedimientoDtoById(idExpediente, idContrato, idProcedimiento);
  }

  @ApiOperation(value = "Obtener procedimiento", notes = "Devuelve el procedimiento con id Origen enviado")
  @GetMapping("/byIdOrigen/{idProcedimientoOrigen}")
  @Transactional
  public ProcedimientoDTOToList getContratoByIdCarga(
    @PathVariable("idProcedimientoOrigen") String idProcedimientoOrigen) {
    return procedimientoUseCase.findProcedimientoByIdOrigen(idProcedimientoOrigen);
  }

  /**
   * Crea un procedimiento en la base de datos
   *
   * @param idExpediente Expediente al que pertence el contrato
   * @param idContrato Contrato al que pertene el procedimiento
   * @param inputDto Datos de entrada para el procedimiento
   * @return Datos del procedimiento que se ha guardado
   * @throws NotFoundException Salta esta excepción cuando no se ha encontrado el expediente o el
   *     contrato
   */
  @ApiOperation(value = "Crea un procedimiento", notes = "Crea un procedimiento")
  @PostMapping
  public ProcedimientoOutputDTO create(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idContrato") Integer idContrato,
      @RequestBody ProcedimientoInputDTO<?> inputDto)
      throws NotFoundException, IOException {
    return procedimientoUseCase.create(inputDto, idExpediente, idContrato);
  }

  /**
   * Actualiza un procedimiento
   *
   * @param idExpediente Expediente al que pertence el contrato
   * @param idContrato Contrato al que pertene el procedimiento
   * @param idProcedimiento Id del procedimiento que se quiere actualizar
   * @param inputDto Datos de entrada para el procedimiento
   * @return Datos del procedimiento que se ha actualizado
   * @throws NotFoundException Salta esta excepción cuando no se ha encontrado el expediente, el
   *     contrato o el procedimiento
   */
  @ApiOperation(value = "Actualiza un procedimiento", notes = "Actualiza un procedimiento")
  @PutMapping("{idProcedimiento}")
  @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
  public ProcedimientoOutputDTO update(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idContrato") Integer idContrato,
      @PathVariable("idProcedimiento") Integer idProcedimiento,
      @RequestBody ProcedimientoInputDTO<?> inputDto)
      throws NotFoundException, IOException {
    return procedimientoUseCase.update(inputDto, idExpediente, idContrato, idProcedimiento);
  }

  /**
   * Borra un procedimiento
   *
   * @param idExpediente Expediente al que pertence el contrato
   * @param idContrato Contrato al que pertene el procedimiento
   * @param idProcedimiento Id del procedimiento que se quiere borrar
   * @throws NotFoundException Salta esta excepción cuando no se ha encontrado el expediente, el
   *     contrato o el procedimiento
   */
  @ApiOperation(value = "Elimina un procedimiento", notes = "Elimina un procedimiento")
  @DeleteMapping("{idProcedimiento}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idContrato") Integer idContrato,
      @PathVariable("idProcedimiento") Integer idProcedimiento)
    throws Exception {
    procedimientoUseCase.delete(idExpediente, idContrato, idProcedimiento);
  }

  @ApiOperation(value = "Descargar información de procedimiento en Excel",
          notes = "Descargar información en Excel del procedimiento con ID enviado")
  @GetMapping("{idProcedimiento}/excel")
  public ResponseEntity<InputStreamResource> getExcelProcedimeintoById(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @PathVariable("idProcedimiento") Integer idProcedimiento)
          throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport
            .create(procedimientoUseCase.findExcelProcedimeintoById(idExpediente, idContrato, idProcedimiento));

    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    return excelExport.download(excel, "Procedimiento: " + idProcedimiento + "_" + date);
  }

  @ApiOperation(value = "Descargar información de procedimiento en Excel",
          notes = "Descargar información en Excel del procedimiento con ID enviado")
  @GetMapping("/excel")
  public ResponseEntity<InputStreamResource> getExcelProcedimeintoByContratoId(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato)
          throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport
            .create(procedimientoUseCase.findExcelProcedimeintoById(idExpediente, idContrato));

    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    return excelExport.download(excel, "Procedimientos_ByContrato: " + idContrato + "_" + date);
  }

 @ApiOperation(value="Obtener Procedimientos", notes="Devuelve los procedimientos por idExpediente")
  @GetMapping("/all")
  public List<ProcedimientoDTOToList> getProcedimientosByIdExpediente(
    @PathVariable("idExpediente")Integer idExpediente)throws Exception{
    return procedimientoUseCase.getAllByExpedienteId(idExpediente);
 }

}
