package com.haya.alaska.procedimiento.infrastructure.controller.dto.input;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProcedimientoMonitorioInputDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private Date fechaPresentacionDemanda;
  private Double principalDemanda;
  private Date fechaAdmisionDemanda;
  private Date fecha;
  private Date fechaOposicion;
  private Boolean resultado;
}
