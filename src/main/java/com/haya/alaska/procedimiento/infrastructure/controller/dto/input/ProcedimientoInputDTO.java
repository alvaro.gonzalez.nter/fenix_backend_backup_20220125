package com.haya.alaska.procedimiento.infrastructure.controller.dto.input;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
@NoArgsConstructor
public class ProcedimientoInputDTO<T> implements Serializable {

  // Entidades
  private List<Integer> contratos = new ArrayList<>(); // Contrato
  private Integer tipo = 0; // TipoProcedimiento
  private Integer estado = 0; // EstadoProcedimiento
  private Integer provincia = 0; // Provincia
  private List<Integer> demandados = new ArrayList<>(); // demandados
  private List<ProcedimientoInputSubastaDTO> subastas = new ArrayList<>(); // Subasta

  // Campos
  private String numeroAutos;
  private String plaza;
  private String juzgado;
  private Double importeDemanda;
  private String demandante;
  private Double gastosIncurridos;
  private String habilitaciones;
  private Double entregasJudiciales;
  private String despacho;
  private String letrado;
  private String telefonoLetrado;
  private String telefono2Letrado;
  private String emailLetrado;
  private String gestorJudicialHaya;
  private String emailGestorJudicialHaya;
  private String procurador;
  private String telefonoProcurador;
  private String emailProcurador;
  private String despachoContrario;
  private String letradoContrario;
  private String telefonoLetradoContrario;
  private String telefono2LetradoContrario;
  private String emailLetradoContrario;
  private String procuradorContrario;
  private String telefonoProcuradorContrario;
  private String emailProcuradorContrario;
  private String motivo;
  private T detalle;

  /*Datos extra concursal*/
  private String nombreAdminConcursal;
  private String direccionAdminConcursal;
  private String telefonoAdminConcursal;
  private String emailAdminConcursal;
  private String gestorConcursosHaya;
  private String mailGestorConcursosHaya;
}
