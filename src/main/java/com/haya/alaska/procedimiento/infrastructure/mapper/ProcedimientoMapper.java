package com.haya.alaska.procedimiento.infrastructure.mapper;

import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.bien_subastado.infrastructure.controller.dto.BienSubastadoDTOToList;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estado_procedimiento.infrastructure.repository.EstadoProcedimientoRepository;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.hito_procedimiento.infrastructure.repository.HitoProcedimientoRepository;
import com.haya.alaska.integraciones.recovery.model.Asunto;
import com.haya.alaska.integraciones.recovery.model.InfoAdicional;
import com.haya.alaska.integraciones.recovery.model.LineaProcesal;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.domain.ProcedimientoEtj;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.ProcedimientoDTOToList;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.output.*;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.subasta.domain.Subasta;
import com.haya.alaska.subasta.infrastructure.controller.dto.SubastaDTOToList;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ProcedimientoMapper {

  @Autowired
  private TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired
  private HitoProcedimientoRepository hitoProcedimientoRepository;
  @Autowired
  private CatalogoMinInfoMapper catalogoMinInfoMapper;
  @Autowired
  private ProvinciaRepository provinciaRepository;
  @Autowired
  private EstadoProcedimientoRepository estadoProcedimientoRepository;

  public ProcedimientoDTOToList procedimientoDTOToList(Procedimiento procedimiento) {

    ProcedimientoDTOToList procedimientoDTOToList = new ProcedimientoDTOToList();

    BeanUtils.copyProperties(procedimiento, procedimientoDTOToList);
    procedimientoDTOToList.setGastosCostos(procedimiento.getGastosIncurridos() != null ? procedimiento.getGastosIncurridos() : null);
    procedimientoDTOToList.setTipo(
      procedimiento.getTipo() != null ? new CatalogoMinInfoDTO(procedimiento.getTipo()) : null);

    if (procedimiento.getSubastas() != null)
      procedimientoDTOToList.setProvincia(
        procedimiento.getProvincia() != null ? new CatalogoMinInfoDTO(procedimiento.getProvincia()) : null);

    procedimientoDTOToList.setEstado(
      procedimiento.getEstado() != null
        ? new CatalogoMinInfoDTO(procedimiento.getEstado())
        : null);
    procedimientoDTOToList.setHito(procedimiento.getHitoProcedimiento() != null ? new CatalogoMinInfoDTO(procedimiento.getHitoProcedimiento()) : null);

    procedimientoDTOToList.setSituacionProcesal(procedimiento.getEstado() != null ? procedimiento.getEstado().getValor() : null);
    return procedimientoDTOToList;
  }

  public ProcedimientoDTOToList procedimientoDTOToList(Asunto asunto) {
    LineaProcesal lineaProcesalPrincipal = calcularLineaProcesalPrincipal(asunto);
    if (lineaProcesalPrincipal == null) return null;
    ProcedimientoDTOToList procedimientoDTOToList = procedimientoDTOToList(asunto, lineaProcesalPrincipal);

    List<ProcedimientoDTOToList> procedimientosHijos = new LinkedList<>();
    for (LineaProcesal lineaProcesal : asunto.getItersProcesales()) {
      if (lineaProcesal.getId() != lineaProcesalPrincipal.getId() && !lineaProcesal.getTipo().getValor().equals("Preparación Expediente Judicial")) {
        procedimientosHijos.add(procedimientoDTOToList(asunto, lineaProcesal));
      }
    }
    procedimientoDTOToList.setProcedimientosHijos(procedimientosHijos);

    return procedimientoDTOToList;
  }

  public LineaProcesal calcularLineaProcesalPrincipal(Asunto asunto) {
    List<LineaProcesal> itersProcesales = asunto.getItersProcesales();
    if (itersProcesales == null) return null;
    Iterator<LineaProcesal> it = itersProcesales.iterator();
    LineaProcesal lineaProcesalAux = null;
    if (itersProcesales.size() == 1) lineaProcesalAux = itersProcesales.get(0);
    else {
      while (it.hasNext()) {
        LineaProcesal item = it.next();
        if (!item.getTipo().getValor().equals("Preparación de expediente judicial") && !item.getTipo().getValor().equals("Preparación Expediente Judicial")) {
          if (lineaProcesalAux == null) lineaProcesalAux = item;
          if (Integer.valueOf(lineaProcesalAux.getProcedimientoPrincipal()) > Integer.valueOf(item.getProcedimientoPrincipal()))
            lineaProcesalAux = item;
        }
      }
    }
    return lineaProcesalAux;
  }

  public ProcedimientoDTOToList procedimientoDTOToList(Asunto asunto, LineaProcesal lineaProcesal) {
    ProcedimientoDTOToList procedimientoDTOToList = new ProcedimientoDTOToList();

    BeanUtils.copyProperties(lineaProcesal, procedimientoDTOToList);
    procedimientoDTOToList.setId(asunto.getId().intValue());
    procedimientoDTOToList.setMotivo(asunto.getMotivo());
    procedimientoDTOToList.setTipo(catalogoMinInfoMapper.entityToDto(tipoProcedimientoRepository.findByValorAndRecoveryIsTrue(lineaProcesal.getTipo().getValor()).orElse(null)));
    procedimientoDTOToList.setProvincia(catalogoMinInfoMapper.entityToDto(tipoProcedimientoRepository.findByValorAndRecoveryIsTrue(lineaProcesal.getProvincia().getValor()).orElse(null)));
    procedimientoDTOToList.setEstado(catalogoMinInfoMapper.entityToDto(estadoProcedimientoRepository.findByValor(asunto.getEstado().getValor()).orElse(null)));
    if (lineaProcesal.getHitoRecovery().getValor() != null) {
      HitoProcedimiento hito = hitoProcedimientoRepository.findAllByValorAndRecoveryIsTrue(lineaProcesal.getHitoRecovery().getValor()).stream().findFirst().orElse(hitoProcedimientoRepository.findByValorAndRecoveryIsTrue("Hito no disponible").orElse(null));
      procedimientoDTOToList.setHito(catalogoMinInfoMapper.entityToDto(hito));
    }
    return procedimientoDTOToList;
  }

  public static ProcedimientoOutputDTO createProcedimientoOutputDTO(Procedimiento procedimiento) {
    ProcedimientoOutputDTO result;

    if (procedimiento.getTipo() != null) {
      switch (procedimiento.getTipo().getCodigo()) {
        case "CON":
          result = new ProcedimientoOutputDTO<ProcedimientoConcursalOutputDTO>();
          ProcedimientoConcursalOutputDTO pc = new ProcedimientoConcursalOutputDTO(procedimiento);
          result.setDetalle(pc);
          break;
        case "HIP":
          result = new ProcedimientoOutputDTO<ProcedimientoHipotecarioOutputDTO>();
          ProcedimientoHipotecarioOutputDTO ph =
            new ProcedimientoHipotecarioOutputDTO(procedimiento);
          result.setDetalle(ph);
          break;
        case "ETNJ":
          result = new ProcedimientoOutputDTO<ProcedimientoEtnjOutputDTO>();
          ProcedimientoEtnjOutputDTO pe1 = new ProcedimientoEtnjOutputDTO(procedimiento);
          result.setDetalle(pe1);
          break;
        case "EN":
          result = new ProcedimientoOutputDTO<ProcedimientoEjecucionNotarialOutputDTO>();
          ProcedimientoEjecucionNotarialOutputDTO pe =
            new ProcedimientoEjecucionNotarialOutputDTO(procedimiento);
          result.setDetalle(pe);
          break;
        case "MON":
          result = new ProcedimientoOutputDTO<ProcedimientoMonitorioOutputDTO>();
          ProcedimientoMonitorioOutputDTO pm = new ProcedimientoMonitorioOutputDTO(procedimiento);
          result.setDetalle(pm);
          break;
        case "VERB":
          result = new ProcedimientoOutputDTO<ProcedimientoVerbalOutputDTO>();
          ProcedimientoVerbalOutputDTO pv = new ProcedimientoVerbalOutputDTO(procedimiento);
          result.setDetalle(pv);
          break;
        case "ORD":
          result = new ProcedimientoOutputDTO<ProcedimientoOrdinarioOutputDTO>();
          ProcedimientoOrdinarioOutputDTO po = new ProcedimientoOrdinarioOutputDTO(procedimiento);
          result.setDetalle(po);
          break;
        case "ETJ":
          result = new ProcedimientoOutputDTO<ProcedimientoEtjOutputDTO>();
          ProcedimientoEtjOutputDTO pe2 = new ProcedimientoEtjOutputDTO(procedimiento);
          result.setDetalle(pe2);
          break;
        default:
          result = new ProcedimientoOutputDTO();
          break;
      }
    } else {
      result = new ProcedimientoOutputDTO();
    }
    BeanUtils.copyProperties(procedimiento, result);
    result.setEstado(
      procedimiento.getEstado() != null
        ? new CatalogoMinInfoDTO(procedimiento.getEstado())
        : null);
    result.setTipo(
      procedimiento.getTipo() != null ? new CatalogoMinInfoDTO(procedimiento.getTipo()) : null);
    result.setHito(procedimiento.getHitoProcedimiento() != null ? new CatalogoMinInfoDTO(procedimiento.getHitoProcedimiento()) : null);
    result.setProvincia(
      procedimiento.getProvincia() != null ? new CatalogoMinInfoDTO(procedimiento.getProvincia()) : null);

    List<ContratoDTOToList> contratos = new ArrayList<>();
    List<IntervinienteDTOToList> intervinientes = new ArrayList<>();
    for (Contrato c : procedimiento.getContratos()) {
      contratos.add(new ContratoDTOToList(c));
      for (Interviniente i : procedimiento.getDemandados()) {
        if (i.getContratoInterviniente(c.getId()) != null) {
          intervinientes.add(new IntervinienteDTOToList(i, c.getId()));
        }
      }
    }
    result.setContratos(contratos);
    result.setDemandados(intervinientes);

    List<SubastaDTOToList> subastas = new ArrayList<>();
    for (Subasta s : procedimiento.getSubastas()) {
      subastas.add(new SubastaDTOToList(s));
    }
    result.setSubastas(subastas);

    return result;
  }

  public ProcedimientoOutputDTO createProcedimientoOutputDTO(Asunto asunto, Set<Subasta> subastas) {
    //Se obtiene el procedimiento principal del asunto
    LineaProcesal lineaProcesalPrincipal = calcularLineaProcesalPrincipal(asunto);
    if (lineaProcesalPrincipal == null) return null;
    if (lineaProcesalPrincipal != null) {
      List<ContratoDTOToList> contratos = new ArrayList<>();
      for (com.haya.alaska.integraciones.recovery.model.Contrato c : lineaProcesalPrincipal.getContratoRecovery()) {
        ContratoDTOToList contratoDTOToList = new ContratoDTOToList();
        contratoDTOToList.setIdHaya(c.getIdHaya());
        contratos.add(contratoDTOToList);
      }


      List<IntervinienteDTOToList> intervinientes = new ArrayList<>();
      for (Interviniente i : lineaProcesalPrincipal.getDemandados()) {
        intervinientes.add(new IntervinienteDTOToList(i, null));
      }

      List<SubastaDTOToList> subastasDto = new ArrayList<>();
      for (Subasta s : subastas) {
        SubastaDTOToList subastaDTOToList = new SubastaDTOToList(s);
        subastaDTOToList.setId(s.getIdRecovery());
        List<BienSubastadoDTOToList> bienes = new ArrayList<>();
        for (BienSubastado bs : s.getBienesSubastados()) {
          bienes.add(new BienSubastadoDTOToList(bs));
        }
        subastaDTOToList.setBienesSubastados(bienes);
        subastasDto.add(subastaDTOToList);
      }

      ProcedimientoOutputDTO procedimientoOutputDTO = new ProcedimientoOutputDTO(asunto, lineaProcesalPrincipal, contratos, intervinientes, subastasDto);
      procedimientoOutputDTO.setProvincia(catalogoMinInfoMapper.entityToDto(tipoProcedimientoRepository.findByValorAndRecoveryIsTrue(lineaProcesalPrincipal.getProvincia().getValor()).orElse(null)));
      procedimientoOutputDTO.setEstado(catalogoMinInfoMapper.entityToDto(estadoProcedimientoRepository.findByValor(asunto.getEstado().getValor()).orElse(null)));
      procedimientoOutputDTO.setTipo(catalogoMinInfoMapper.entityToDto(tipoProcedimientoRepository.findByValorAndRecoveryIsTrue(lineaProcesalPrincipal.getTipo().getValor()).orElse(null)));
      if (lineaProcesalPrincipal.getHitoRecovery().getValor() != null) {
        HitoProcedimiento hito = hitoProcedimientoRepository.findAllByValorAndRecoveryIsTrue(lineaProcesalPrincipal.getHitoRecovery().getValor()).stream().findFirst().orElse(hitoProcedimientoRepository.findByValorAndRecoveryIsTrue("Hito no disponible").orElse(null));
        procedimientoOutputDTO.setHito(catalogoMinInfoMapper.entityToDto(hito));
      }
      crearDetalle(lineaProcesalPrincipal, procedimientoOutputDTO);
      return procedimientoOutputDTO;
    }
    ProcedimientoOutputDTO procedimientoOutputDTO = new ProcedimientoOutputDTO(asunto);
    procedimientoOutputDTO.setEstado(catalogoMinInfoMapper.entityToDto(estadoProcedimientoRepository.findByValor(asunto.getEstado().getValor()).orElse(null)));
    return procedimientoOutputDTO;
  }

  private void crearDetalle(LineaProcesal lineaProcesalPrincipal, ProcedimientoOutputDTO procedimientoOutputDTO) {
    if (lineaProcesalPrincipal.getInfoAdicional() != null) {
      if (lineaProcesalPrincipal.getTipo() != null) {
        if (lineaProcesalPrincipal.getTipo().getValor().equals("Procedimiento hipotecario")) {
          ProcedimientoHipotecarioOutputDTO procedimientoHipotecarioOutputDTO = new ProcedimientoHipotecarioOutputDTO();
          for (InfoAdicional info : lineaProcesalPrincipal.getInfoAdicional()) {
            if (info.getValorCampo() != null) {
              if (info.getTextoCampo().equals("Fecha presentación de la demanda"))
                procedimientoHipotecarioOutputDTO.setFechaPresentacionDemanda(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Principal demanda"))
                procedimientoHipotecarioOutputDTO.setPrincipalDemanda(Double.valueOf(info.getValorCampo()));
              if (info.getTextoCampo().equals("Demanda admitida (Si/No)"))
                procedimientoHipotecarioOutputDTO.setDemandaAdmitida(info.getValorCampo().equals("1"));
              if (info.getTextoCampo().equals("Fecha de la certificación del mandamiento de cargas"))
                procedimientoHipotecarioOutputDTO.setFechaCertificacion(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha notificación auto despachando ejecución"))
                procedimientoHipotecarioOutputDTO.setFechaNotificacionAuto(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Resultado notificación (Positivo/Negativo)"))
                procedimientoHipotecarioOutputDTO.setResultadoNotificacion(info.getValorCampo().equals("1"));
              if (info.getTextoCampo().equals("Fecha oposición"))
                procedimientoHipotecarioOutputDTO.setFechaOposicion(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Resultado (Favorable/Desfavorable) comparecencia"))
                procedimientoHipotecarioOutputDTO.setResultadoOposicion(info.getValorCampo().equals("1"));
            }
          }
          procedimientoOutputDTO.setDetalle(procedimientoHipotecarioOutputDTO);
        } else if (lineaProcesalPrincipal.getTipo().getValor().equals("Ejecución de título no judicial")) {
          ProcedimientoEtnjOutputDTO procedimientoEtnjOutputDTO = new ProcedimientoEtnjOutputDTO();
          for (InfoAdicional info : lineaProcesalPrincipal.getInfoAdicional()) {
            if (info.getValorCampo() != null) {
              if (info.getTextoCampo().equals("Fecha presentación de la demanda"))
                procedimientoEtnjOutputDTO.setFechaPresentacionDemanda(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Principal demanda"))
                procedimientoEtnjOutputDTO.setPrincipalDemanda(Double.valueOf(info.getValorCampo()));
              if (info.getTextoCampo().equals("Fecha auto despachando ejecución"))
                procedimientoEtnjOutputDTO.setFechaAutoDespachando(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha notificación requerimiento pago"))
                procedimientoEtnjOutputDTO.setFechaNotificacionReqPago(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Dirección notificación"))
                procedimientoEtnjOutputDTO.setDireccionNotificacion(info.getValorCampo());
            }
          }
          procedimientoOutputDTO.setDetalle(procedimientoEtnjOutputDTO);
        } else if (lineaProcesalPrincipal.getTipo().getValor().equals("Procedimiento Ejecución Notarial")) {
          ProcedimientoEjecucionNotarialOutputDTO procedimientoEjecucionNotarialOutputDTO = new ProcedimientoEjecucionNotarialOutputDTO();
          for (InfoAdicional info : lineaProcesalPrincipal.getInfoAdicional()) {
            if (info.getValorCampo() != null) {
              if (info.getTextoCampo().equals("Fecha firma del acta"))
                procedimientoEjecucionNotarialOutputDTO.setFechaFirmaActa(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("fecha recepción certificación registral"))
                procedimientoEjecucionNotarialOutputDTO.setFechaRecepcionCertificacionRegistral(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("fecha requerimiento de pago al deudor"))
                procedimientoEjecucionNotarialOutputDTO.setFechaReqPagoDeudor(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("fechaAuto"))
                procedimientoEjecucionNotarialOutputDTO.setFechaAuto(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("fecha subasta"))
                procedimientoEjecucionNotarialOutputDTO.setFechaSubasta(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("celebrada (Si/No)"))
                procedimientoEjecucionNotarialOutputDTO.setCelebrada(info.getValorCampo().equals("1"));
              if (info.getTextoCampo().equals("Resultado Subasta"))
                procedimientoEjecucionNotarialOutputDTO.setResultadoSubasta(info.getValorCampo().equals("1"));
              if (info.getTextoCampo().equals("Motivo suspensión"))
                procedimientoEjecucionNotarialOutputDTO.setMotivoSuspension(info.getValorCampo());
              if (info.getTextoCampo().equals("Fecha Firma escritura"))
                procedimientoEjecucionNotarialOutputDTO.setFechaFirmaEscritura(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha Posesión"))
                procedimientoEjecucionNotarialOutputDTO.setFechaPosesion(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
            }
          }
          procedimientoOutputDTO.setDetalle(procedimientoEjecucionNotarialOutputDTO);
        } else if (lineaProcesalPrincipal.getTipo().getValor().equals("Procedimiento monitorio")) {
          ProcedimientoMonitorioOutputDTO procedimientoMonitorioOutputDTO = new ProcedimientoMonitorioOutputDTO();
          for (InfoAdicional info : lineaProcesalPrincipal.getInfoAdicional()) {
            if (info.getValorCampo() != null) {
              if (info.getTextoCampo().equals("Fecha presentación de la demanda"))
                procedimientoMonitorioOutputDTO.setFechaPresentacionDemanda(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Principal de la demanda"))
                procedimientoMonitorioOutputDTO.setPrincipalDemanda(Double.valueOf(info.getValorCampo()));
              if (info.getTextoCampo().equals("Fecha Admisión demanda"))
                procedimientoMonitorioOutputDTO.setFechaAdmisionDemanda(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha"))
                procedimientoMonitorioOutputDTO.setFecha(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha oposición"))
                procedimientoMonitorioOutputDTO.setFechaOposicion(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
            }
          }
          procedimientoOutputDTO.setDetalle(procedimientoMonitorioOutputDTO);
        } else if (lineaProcesalPrincipal.getTipo().getValor().equals("Procedimiento verbal")) {
          ProcedimientoVerbalOutputDTO procedimientoVerbalOutputDTO = new ProcedimientoVerbalOutputDTO();
          for (InfoAdicional info : lineaProcesalPrincipal.getInfoAdicional()) {
            if (info.getValorCampo() != null) {
              if (info.getTextoCampo().equals("Fecha de presentación"))
                procedimientoVerbalOutputDTO.setFechaPresentacionDemanda(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Principal demanda"))
                procedimientoVerbalOutputDTO.setPrincipalDemanda(Double.valueOf(info.getValorCampo()));
              if (info.getTextoCampo().equals("Fecha admisión"))
                procedimientoVerbalOutputDTO.setFechaAdmisionDemanda(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha notificación"))
                procedimientoVerbalOutputDTO.setFechaNotificacion(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Resultado notificación (Positivo/Negativo)"))
                procedimientoVerbalOutputDTO.setResultadoNotificacion(info.getValorCampo().equals("1"));
              if (info.getTextoCampo().equals("fecha de celebración Juicio"))
                procedimientoVerbalOutputDTO.setFechaCelebracionJuicio(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("resolución"))
                procedimientoVerbalOutputDTO.setResolucion(info.getValorCampo().equals("1"));
            }
          }
          procedimientoOutputDTO.setDetalle(procedimientoVerbalOutputDTO);
        } else if (lineaProcesalPrincipal.getTipo().getValor().equals("Procedimiento ordinario")) {
          ProcedimientoOrdinarioOutputDTO procedimientoOrdinarioOutputDTO = new ProcedimientoOrdinarioOutputDTO();
          for (InfoAdicional info : lineaProcesalPrincipal.getInfoAdicional()) {
            if (info.getValorCampo() != null) {
              if (info.getTextoCampo().equals("Fecha de presentación"))
                procedimientoOrdinarioOutputDTO.setFechaPresentacionDemanda(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Principal demanda"))
                procedimientoOrdinarioOutputDTO.setPrincipalDemanda(Double.valueOf(info.getValorCampo()));
              if (info.getTextoCampo().equals("Fecha admisión demanda"))
                procedimientoOrdinarioOutputDTO.setFechaAdmisionDemanda(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Resultado notificación requerimiento de pago"))
                procedimientoOrdinarioOutputDTO.setResultadoNotificacionReqPago(info.getValorCampo().equals("1"));
              if (info.getTextoCampo().equals("Dirección notificación"))
                procedimientoOrdinarioOutputDTO.setDireccionNotificacion(info.getValorCampo());
              if (info.getTextoCampo().equals("Fecha requerimiento de pago al deudor"))
                procedimientoOrdinarioOutputDTO.setFechaReqPagoDeudor(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha oposición"))
                procedimientoOrdinarioOutputDTO.setFechaOposicion(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Resultado oposición"))
                procedimientoOrdinarioOutputDTO.setResultadoOposicion(info.getValorCampo().equals("1"));
            }
          }
          procedimientoOutputDTO.setDetalle(procedimientoOrdinarioOutputDTO);
        } else if (lineaProcesalPrincipal.getTipo().getValor().equals("Ejecución de título judicial")) {
          ProcedimientoEtj procedimientoEtj = new ProcedimientoEtj();
          for (InfoAdicional info : lineaProcesalPrincipal.getInfoAdicional()) {
            if (info.getValorCampo() != null) {
              if (info.getTextoCampo().equals("Fecha de presentación"))
                procedimientoEtj.setFechaPresentacionDemanda(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Principal demanda"))
                procedimientoEtj.setPrincipalDemanda(Double.valueOf(info.getValorCampo()));
              if (info.getTextoCampo().equals("Fecha auto despachando ejecución"))
                procedimientoEtj.setFechaAutoDespachandoEjecucion(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha notificación requerimiento pago"))
                procedimientoEtj.setFechaNotificacionReqPago(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha oposición"))
                procedimientoEtj.setFechaOposicion(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Resultado"))
                procedimientoEtj.setResultado(info.getValorCampo().equals("1"));
            }
          }
          procedimientoOutputDTO.setDetalle(procedimientoEtj);
        } else if (lineaProcesalPrincipal.getTipo().getValor().equals("Ejecución de título judicial")) {
          ProcedimientoEtj procedimientoEtj = new ProcedimientoEtj();
          for (InfoAdicional info : lineaProcesalPrincipal.getInfoAdicional()) {
            if (info.getValorCampo() != null) {
              if (info.getTextoCampo().equals("Fecha de presentación"))
                procedimientoEtj.setFechaPresentacionDemanda(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Principal demanda"))
                procedimientoEtj.setPrincipalDemanda(Double.valueOf(info.getValorCampo()));
              if (info.getTextoCampo().equals("Fecha auto despachando ejecución"))
                procedimientoEtj.setFechaAutoDespachandoEjecucion(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha notificación requerimiento pago"))
                procedimientoEtj.setFechaNotificacionReqPago(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Fecha oposición"))
                procedimientoEtj.setFechaOposicion(new Date(Integer.valueOf(info.getValorCampo().split("-")[0]) - 1900, Integer.valueOf(info.getValorCampo().split("-")[1]) - 1, Integer.valueOf(info.getValorCampo().split("-")[2])));
              if (info.getTextoCampo().equals("Resultado"))
                procedimientoEtj.setResultado(info.getValorCampo().equals("1"));
            }
          }
          procedimientoOutputDTO.setDetalle(procedimientoEtj);
        }
      }
    }
  }

}
