package com.haya.alaska.procedimiento.infrastructure.controller.dto.output;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.haya.alaska.procedimiento.domain.Procedimiento;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProcedimientoConcursalOutputDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private Date fechaPublicacion;
  private Date fechaAutoDeclarandoConcurso;
  private Date fechaAutoFaseConvenio;
  private Boolean resultadoAprobacionConvenio;
  private Date fechaApertura;
  private Date fechaResolucion;
  private Date fechaDecreto;

  public ProcedimientoConcursalOutputDTO(Procedimiento procedimiento) {
    BeanUtils.copyProperties(procedimiento, this);
  }
}
