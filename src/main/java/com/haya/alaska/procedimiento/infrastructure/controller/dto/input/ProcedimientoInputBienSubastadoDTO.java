package com.haya.alaska.procedimiento.infrastructure.controller.dto.input;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProcedimientoInputBienSubastadoDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  Integer bien; // id bien
  String detalleResultado;
  String entidadAdjudicada;
  Date fechaDecretoAdjudicacion;
  Date fechaPosteriorLanzamiento;
  Date fechaSenyalamiento;
  Date fechaTestimonio;
  Integer id; // solo para edicion
  Double importeAdjudicacion;
  String numeroFinca;
  boolean postores;
  Integer resultadoSubasta; // id resultado subasta
}
