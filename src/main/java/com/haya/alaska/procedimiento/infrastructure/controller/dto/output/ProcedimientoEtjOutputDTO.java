package com.haya.alaska.procedimiento.infrastructure.controller.dto.output;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.haya.alaska.procedimiento.domain.Procedimiento;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProcedimientoEtjOutputDTO implements Serializable {
  private static final long serialVersionUID = 1L;
  private Date fechaPresentacionDemanda;
  private Double principalDemanda;
  private Date fechaAutoDespachandoEjecucion;
  private Date fechaNotificacionReqPago;
  private Date fechaOposicion;
  private Boolean resultado;

  public ProcedimientoEtjOutputDTO(Procedimiento procedimiento) {
    BeanUtils.copyProperties(procedimiento, this);
  }
}
