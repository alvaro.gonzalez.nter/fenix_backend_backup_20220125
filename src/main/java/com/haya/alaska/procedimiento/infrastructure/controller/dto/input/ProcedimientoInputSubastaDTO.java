package com.haya.alaska.procedimiento.infrastructure.controller.dto.input;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProcedimientoInputSubastaDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private boolean activo = true;
  private List<ProcedimientoInputBienSubastadoDTO> bienesSubastados = new ArrayList<>();
  private String decisionSuspension;
  private Integer estado; // id de EstadoSubasta
  private Date fechaFinPujas;
  private Date fechaInicioPujas;
  private Date fechaNotificacionDecreto;
  private Date fechaPagoTasa;
  private Integer id; // solo aplicable en edición
  private String motivoSuspensionCancelacion;
  private int orden;
}
