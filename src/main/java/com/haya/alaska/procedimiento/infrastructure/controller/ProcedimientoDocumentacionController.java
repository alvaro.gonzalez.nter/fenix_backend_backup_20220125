package com.haya.alaska.procedimiento.infrastructure.controller;

import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.procedimiento.application.ProcedimientoUseCase;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/procedimientos")
public class ProcedimientoDocumentacionController {

  @Autowired
  ProcedimientoUseCase procedimientoUseCase;

  @ApiOperation(value="Obtener documentación de los asuntos de recovery", notes="Devuelve los documentos de recovery")
  @GetMapping("/{idProcedimiento}/documentosRecovery")
  public ListWithCountDTO<DocumentoDTO> findDocumentosAsunto(@PathVariable("idProcedimiento")Integer idProcedimiento,
                                                             @RequestParam("codigoProcedimiento")String codigoProcedimiento,
                                                             @RequestParam(value = "id", required = false) String idDocumento,
                                                             @RequestParam(value = "usuarioCreador", required = false) String usuarioCreador,
                                                             @RequestParam(value = "tipo", required = false) String tipo,
                                                             @RequestParam(value = "idEntidad", required = false) String idEntidad,
                                                             @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
                                                             @RequestParam(value = "orderField", required = false) String orderField,
                                                             @RequestParam(value = "orderDirection", required = false) String orderDirection,
                                                             @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) throws IOException {
    return procedimientoUseCase.listarDocumentosProcedimientoId(idProcedimiento,codigoProcedimiento,idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, orderDirection, orderField, size, page);
  }
}
