package com.haya.alaska.procedimiento.infrastructure.controller.dto.input;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProcedimientoConcursalInputDTO implements Serializable{

  private static final long serialVersionUID = 1L;
  private Date fechaPublicacion;
  private Date fechaAutoDeclarandoConcurso;
  private Date fechaAutoFaseConvenio;
  private Boolean resultadoAprobacionConvenio;
  private Date fechaApertura;
  private Date fechaResolucion;
  private Date fechaDecreto;
}
