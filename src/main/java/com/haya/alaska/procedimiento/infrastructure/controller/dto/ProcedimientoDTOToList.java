package com.haya.alaska.procedimiento.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProcedimientoDTOToList implements Serializable {

  private static final long serialVersionUID = 1L;
  private Integer id;
  private CatalogoMinInfoDTO tipo;
  private String numeroAutos;
  private String plaza;
  private String juzgado;
  private CatalogoMinInfoDTO provincia;
  private Double importeDemanda;
  private Double gastosCostos;
  private CatalogoMinInfoDTO hito;
  private CatalogoMinInfoDTO estado;
  // TODO qué representa este campo? no está en modelo de datos
  private String motivo;
  private String situacionProcesal;
  private List<ProcedimientoDTOToList> procedimientosHijos;
}
