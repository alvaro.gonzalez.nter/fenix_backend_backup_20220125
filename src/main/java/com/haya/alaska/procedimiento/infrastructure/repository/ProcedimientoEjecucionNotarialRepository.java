package com.haya.alaska.procedimiento.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.procedimiento.domain.ProcedimientoEjecucionNotarial;

@Repository
public interface ProcedimientoEjecucionNotarialRepository
    extends JpaRepository<ProcedimientoEjecucionNotarial, Integer> {}
