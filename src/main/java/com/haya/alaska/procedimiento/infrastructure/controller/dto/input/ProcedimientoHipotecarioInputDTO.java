package com.haya.alaska.procedimiento.infrastructure.controller.dto.input;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProcedimientoHipotecarioInputDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private Date fechaPresentacionDemanda;
  private Double principalDemanda;
  private Boolean demandaAdmitida;
  private Date fechaCertificacion;
  private Date fechaNotificacionAuto;
  private Boolean resultadoNotificacion;
  private String domicilioNotificacion;
  private Date fechaOposicion;
  private Boolean resultadoOposicion;
}
