package com.haya.alaska.procedimiento.infrastructure.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.haya.alaska.procedimiento.domain.Procedimiento;

@Repository
public interface ProcedimientoRepository extends JpaRepository<Procedimiento, Integer> {
  Optional<Procedimiento> findByIdCarga(String idCarga);

  List<Procedimiento> findAllByContratosIdAndOrden(Integer idContrato, Integer orden);

  List<Procedimiento> findAllByContratosIdCarga(String idCarga);

  List<Procedimiento> findAllByDemandadosId(Integer idInterviniente);

  List<Procedimiento> findAllByContratosIdAndNumeroAutosAndOrdenGreaterThan(
          Integer idContrato, String numeroAutos, Integer orden);

  @Query(
      value =
          "SELECT MIN(rh.timestamp) AS fecha \n"
              + "FROM HIST_MSTR_PROCEDIMIENTO ph \n"
              + "left Join REGISTRO_HISTORICO rh ON ph.REV = rh.id \n"
              + "WHERE ph.ID = ?1 ;",
      nativeQuery = true)
  BigInteger historicoProcedimiento(Integer idProcedimiento);
}
