package com.haya.alaska.procedimiento.application;

import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.ProcedimientoDTOToList;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.input.ProcedimientoInputDTO;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.output.HitoProcedimientoDTO;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.output.ProcedimientoOutputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface ProcedimientoUseCase {
  public ListWithCountDTO<ProcedimientoDTOToList> findAllProcedimientosDto(
      Integer size,
      Integer page,
      Integer idContrato,
      String orderField,
      String orderDirection,
      Integer idProcedimiento,
      String tipo,
      String provincia,
      String numeroAutos,
      String plaza,
      String juzgado,
      String importeDemanda,
      String hito,
      String estado,
      String motivo)
          throws NotFoundException, IOException, Exception;

  public ProcedimientoOutputDTO findProcedimientoDtoById(
    Integer idExpediente, Integer idContrato, Integer idProcedimiento) throws NotFoundException, IOException;
  ProcedimientoDTOToList findProcedimientoByIdOrigen(String id);

  public List<HitoProcedimientoDTO> findHitosByTipoProcedimiento(Integer tipoProcedimiento);

  public ProcedimientoOutputDTO create(
          ProcedimientoInputDTO<?> inputDto, Integer idExpediente, Integer idContrato)
      throws NotFoundException, IOException;

  public ProcedimientoOutputDTO update(
          ProcedimientoInputDTO<?> inputDto, Integer idExpediente, Integer idContrato, Integer idProcedimiento)
      throws NotFoundException, IOException;

  public void delete(Integer idExpediente, Integer idContrato, Integer idProcedimiento)
      throws NotFoundException, Exception;

  LinkedHashMap<String, List<Collection<?>>> findExcelProcedimeintoById(Integer idExpediente, Integer idContrato, Integer idProcedimiento) throws Exception;

  LinkedHashMap<String, List<Collection<?>>> findExcelProcedimeintoById(Integer idExpediente, Integer idContrato) throws Exception;

  public List<ProcedimientoDTOToList>getAllByExpedienteId(Integer idExpediente)throws Exception;
  ListWithCountDTO<DocumentoDTO>listarDocumentosProcedimientoId(Integer idProcedimiento, String codigoProcedimiento,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion,String orderDirection,String orderField,Integer size, Integer page) throws IOException;
}
