package com.haya.alaska.procedimiento.application;

import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.ContratoJudicialExcel;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato.infrastructure.util.ContratoUtil;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.estado_subasta.infrastructure.repository.EstadoSubastaRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.hito_procedimiento.infrastructure.repository.HitoProcedimientoRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.application.GestorDocumentalUseCase;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.recovery.ServicioRecovery;
import com.haya.alaska.integraciones.recovery.model.Asunto;
import com.haya.alaska.integraciones.recovery.model.BienSubastadoRecovery;
import com.haya.alaska.integraciones.recovery.model.LineaProcesal;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.procedimiento.domain.*;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.ProcedimientoDTOToList;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.input.ProcedimientoInputDTO;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.output.HitoProcedimientoDTO;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.output.ProcedimientoOutputDTO;
import com.haya.alaska.procedimiento.infrastructure.mapper.ProcedimientoMapper;
import com.haya.alaska.procedimiento.infrastructure.repository.*;
import com.haya.alaska.procedimiento.infrastructure.util.ProcedimientoUtil;
import com.haya.alaska.resultado_subasta.infrastructure.repository.ResultadoSubastaRepository;
import com.haya.alaska.shared.DemandadoJudicialExcel;
import com.haya.alaska.shared.JudicialExcel;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.subasta.domain.Subasta;
import com.haya.alaska.subasta.domain.SubastasExcel;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ProcedimientoUseCaseImpl implements ProcedimientoUseCase {

  @Autowired
  ProcedimientoMapper procedimientoMapper;
  @Autowired
  ProcedimientoUtil procedimientoUtil;
  @Autowired
  ProcedimientoRepository procedimientoRepository;
  @Autowired
  ProcedimientoEtjRepository procedimientoEtjRepository;
  @Autowired
  ProcedimientoEtnjRepository procedimientoEtnjRepository;
  @Autowired
  ProcedimientoVerbalRepository procedimientoVerbalRepository;
  @Autowired
  ProcedimientoConcursalRepository procedimientoConcursalRepository;
  @Autowired
  ProcedimientoOrdinarioRepository procedimientoOrdinarioRepository;
  @Autowired
  ProcedimientoMonitorioRepository procedimientoMonitorioRepository;
  @Autowired
  ProcedimientoHipotecarioRepository procedimientoHipotecarioRepository;
  @Autowired
  ProcedimientoEjecucionNotarialRepository procedimientoEjecucionNotarialRepository;
  @Autowired
  ContratoRepository contratoRepository;
  @Autowired
  ContratoUtil contratoUtil;
  @Autowired
  HitoProcedimientoRepository hitoProcedimientoRepository;
  @Autowired
  ExpedienteRepository expedienteRepository;
  @Autowired
  ServicioRecovery servicioRecovery;
  @Autowired
  ServicioGestorDocumental servicioGestorDocumental;
  @Autowired
  TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired
  EstadoSubastaRepository estadoSubastaRepository;
  @Autowired
  ResultadoSubastaRepository resultadoSubastaRepository;
  @Autowired
  GestorDocumentalUseCase gestorDocumentalUseCase;

  @Override
  public ListWithCountDTO<ProcedimientoDTOToList> findAllProcedimientosDto(
    Integer size,
    Integer page,
    Integer idContrato,
    String orderField,
    String orderDirection,
    Integer idProcedimiento,
    String tipo,
    String provincia,
    String numeroAutos,
    String plaza,
    String juzgado,
    String importeDemanda,
    String hito,
    String estado,
    String motivo)
    throws Exception {
    Contrato contrato = contratoRepository.findById(idContrato).orElseThrow(() -> new Exception("Contrato not found with id:" + idContrato));
    Boolean recovery = contrato.getExpediente().getCartera().getDatosJudicial();
    List<ProcedimientoDTOToList> procedimientosDTO = new ArrayList<>();
    int paginado;
    List<Asunto> asuntosPaginado = new ArrayList<>();
    if (recovery != null && recovery) {
      List<Asunto> asuntos = servicioRecovery.getAsuntos(contrato);
      if (asuntos != null) {
        paginado = asuntos.size();
        asuntosPaginado = asuntos.stream().skip(size * page).limit(size).collect(Collectors.toList());
      } else {
        paginado = 0;
        asuntosPaginado = new ArrayList<>();
      }

      for (Asunto asunto : asuntosPaginado) {
        ProcedimientoDTOToList procedimientoDTOToList = procedimientoMapper.procedimientoDTOToList(asunto);
        if (procedimientoDTOToList != null)
          procedimientosDTO.add(procedimientoDTOToList);
      }

    } else {
      List<Procedimiento> procedimientosPrincipales =
        procedimientoUtil.filterProcedimientoInMemory(
          idContrato,
          idProcedimiento,
          tipo,
          provincia,
          numeroAutos,
          plaza,
          juzgado,
          importeDemanda,
          hito,
          estado,
          motivo,
          orderField,
          orderDirection);
      paginado = procedimientosPrincipales.size();
      List<Procedimiento> procedimientos = procedimientosPrincipales.stream().skip(size * page).limit(size).collect(Collectors.toList());

      for (Procedimiento procedimiento : procedimientos) {
        procedimientosDTO.add(procedimientoMapper.procedimientoDTOToList(procedimiento));
      }
    }

    return new ListWithCountDTO<>(procedimientosDTO, paginado);
  }

  @Override
  public ProcedimientoOutputDTO findProcedimientoDtoById(
    Integer idExpediente, Integer idContrato, Integer idProcedimiento) throws NotFoundException, IOException {

    if (idExpediente == null) throw new IllegalArgumentException("idExpediente cant be null");
    if (idContrato == null) throw new IllegalArgumentException("idContrato cant be null");
    if (idProcedimiento == null) throw new IllegalArgumentException("idProcedimiento cant be null");

    Contrato contrato = contratoRepository.findById(idContrato).orElseThrow(() -> new NotFoundException("Contrato", idContrato));
    Boolean recovery = contrato.getExpediente().getCartera().getDatosJudicial();

    ProcedimientoOutputDTO procedimientoOutputDTO = null;
    if (recovery != null && recovery) {
      List<Asunto> asuntos = servicioRecovery.getAsuntos(contrato);
      Asunto asunto = servicioRecovery.getAsunto(contrato, String.valueOf(idProcedimiento));
      List<Asunto> subastasAsu = servicioRecovery.getSubastas(contrato, String.valueOf(idProcedimiento));
      LineaProcesal lineaProcesalPrincipal = procedimientoMapper.calcularLineaProcesalPrincipal(asunto);
      if (lineaProcesalPrincipal == null) return null;
      setContratoDemandados(asuntos, lineaProcesalPrincipal);
      LineaProcesal iterSubasta = null;
      Set<Subasta> subastaSet = new HashSet<>();
      for (Asunto subastaAux : subastasAsu) {
        for (LineaProcesal iterProcesal : subastaAux.getItersProcesales()) {
          if (contrato.getExpediente().getCartera().getId() == 2) {
            if (lineaProcesalPrincipal.getId().equals(iterProcesal.getId())) iterSubasta = iterProcesal;
          } else {
            subastaSet.addAll(iterProcesal.getSubastas());
          }
        }

      }
      if (subastaSet.size() == 0 && iterSubasta != null)
        subastaSet = iterSubasta.getSubastas();
      if (subastaSet != null) {
        for (Subasta subasta : subastaSet) {
          subasta.setEstado(subasta.getEstado() != null ? estadoSubastaRepository.findByValorAndRecoveryIsTrue(subasta.getEstado().getValor()).orElse(null) : null);
          List<BienSubastadoRecovery> bienesRecovery = servicioRecovery.getBienesSubasta(contrato, String.valueOf(subasta.getIdRecovery()));
          Set<BienSubastado> bienSubastados = new HashSet<>();
          for (BienSubastadoRecovery bs : bienesRecovery) {
            BienSubastado bsF = new BienSubastado();
            BeanUtils.copyProperties(bs, bsF, "id");
            bsF.setIdRecovery(bs.getId());
            bsF.setFechaPosteriorLanzamiento(bs.getFechaPosesion());
            bsF.setTipoSubasta(bs.getTipoSubasta());
            bsF.setResultadoSubasta(bsF.getResultadoSubasta() != null ? resultadoSubastaRepository.findByValor(bsF.getResultadoSubasta().getValor()).orElse(null) : null);
            bienSubastados.add(bsF);
          }
          subasta.setBienesSubastados(bienSubastados);
        }
      }

      procedimientoOutputDTO = procedimientoMapper.createProcedimientoOutputDTO(asunto, subastaSet);
    } else {
      Procedimiento procedimiento =
        this.findProcedimientoById(idExpediente, idContrato, idProcedimiento);
      procedimientoOutputDTO = ProcedimientoMapper.createProcedimientoOutputDTO(procedimiento);
    }

    return procedimientoOutputDTO;
  }

  @Override
  public ProcedimientoDTOToList findProcedimientoByIdOrigen(String id) {

    Procedimiento procedimiento = procedimientoRepository.findByIdCarga(id).orElse(null);

    ProcedimientoDTOToList proDTO = null;
    if (procedimiento != null)
      proDTO = procedimientoMapper.procedimientoDTOToList(procedimiento);

    return proDTO;
  }

  private void setContratoDemandados(List<Asunto> asuntos, LineaProcesal lineaProcesalPrincipal) {
    for (Asunto asunto : asuntos) {
      if (asunto.getItersProcesales() != null) {
        for (LineaProcesal iter : asunto.getItersProcesales()) {
          if (iter.getId().equals(lineaProcesalPrincipal.getId())) {
            lineaProcesalPrincipal.setContratos(iter.getContratoRecovery());
            lineaProcesalPrincipal.setDemandados(iter.getDemandadosRecovery());
          }
        }
      }
    }
  }

  /**
   * Busca un procedimiento a partir de un expediente, contrato y el id del procedimiento
   *
   * @param idExpediente    Expediente sobre el que empezar a buscar
   * @param idContrato      Contrato dentro del expediente
   * @param idProcedimiento Procedimiento dentro del contrato
   * @return Procedimiento que se encuentra dentro de los 2 objetos
   * @throws NotFoundException Cuando la búsqueda de alguno de los 3 objetos falla
   */
  private Procedimiento findProcedimientoById(
    Integer idExpediente, Integer idContrato, Integer idProcedimiento) throws NotFoundException {

    Contrato contrato = procedimientoUtil.getContratoByExpedienteAndId(idExpediente, idContrato);

    Procedimiento procedimiento = contratoUtil.findInContrato(contrato, idProcedimiento);
    if (procedimiento == null)
      throw new NotFoundException("procedimiento", idProcedimiento);

    return procedimiento;
  }

  /**
   * Crea un procedimiento según los datos de entrada
   *
   * @param inputDto     Datos de entrada para el procedimiento
   * @param idExpediente Id del expediente en el que estará el contrato
   * @param idContrato   Id del contrato en el que estará el procedimiento
   * @return Procedimiento guardado en la base de datos transformado en DTO
   * @throws NotFoundException Salta esta excepción cuando no se ha encontrado el Expediente, el
   *                           Contrato o el Procedimiento
   */
  @Override
  public ProcedimientoOutputDTO create(
    ProcedimientoInputDTO<?> inputDto, Integer idExpediente, Integer idContrato)
    throws NotFoundException, IOException {
    Contrato contrato = contratoRepository
      .findById(idContrato)
      .orElseThrow(() -> new NotFoundException("Contrato", idContrato));
    Boolean recovery = contrato.getExpediente().getCartera().getDatosJudicial();
    if (recovery == null || recovery == false) {
      Procedimiento procedimiento = procedimientoUtil.getAndCreateProcedimiento(inputDto, idContrato);
      List<Contrato> contratos = procedimiento.getContratos().stream().filter(c -> c.getId().equals(idContrato)).collect(Collectors.toList());
      if (contratos.size() > 1){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Several contracts with id: " + idContrato + " are related to the procedure of id: " + procedimiento.getId());
        else throw new NotFoundException("Varios contratos con id: " + idContrato + " están relacionados al procedimiento de id: " + procedimiento.getId());
      }
      if (contratos.isEmpty()) {

        procedimiento.getContratos().add(contrato);
        procedimientoRepository.save(procedimiento);
      }
      LocalDate localDateTime = LocalDateTime.now().toLocalDate();
      procedimiento.setFechaAlta(java.sql.Date.valueOf(localDateTime));
      procedimientoRepository.save(procedimiento);
      return ProcedimientoMapper.createProcedimientoOutputDTO(procedimiento);
    }
    return null;
  }

  @Override
  public ProcedimientoOutputDTO update(
    ProcedimientoInputDTO<?> inputDto, Integer idExpediente, Integer idContrato, Integer idProcedimiento)
    throws NotFoundException, IOException {
    Procedimiento procedimiento = findProcedimientoById(idExpediente, idContrato, idProcedimiento);

    switch (procedimiento.getClass().getSimpleName()) {
      case "ProcedimientoEtj":
        ProcedimientoEtj tmpEtj = procedimientoEtjRepository.findById(idProcedimiento).get();
        ProcedimientoEtj procedimientoEtj =
          procedimientoUtil.saveProcedimientoEtj(inputDto, tmpEtj);
        return ProcedimientoMapper.createProcedimientoOutputDTO(procedimientoEtj);

      case "ProcedimientoEtnj":
        ProcedimientoEtnj tmpEtnj = procedimientoEtnjRepository.findById(idProcedimiento).get();
        ProcedimientoEtnj procedimientoEtnj =
          procedimientoUtil.saveProcedimientoEtnj(inputDto, tmpEtnj);
        return ProcedimientoMapper.createProcedimientoOutputDTO(procedimientoEtnj);

      case "ProcedimientoVerbal":
        ProcedimientoVerbal tmpVerbal =
          procedimientoVerbalRepository.findById(idProcedimiento).get();
        ProcedimientoVerbal procedimientoVerbal =
          procedimientoUtil.saveProcedimientoVerbal(inputDto, tmpVerbal);
        return ProcedimientoMapper.createProcedimientoOutputDTO(procedimientoVerbal);

      case "ProcedimientoConcursal":
        ProcedimientoConcursal tmpConcursal =
          procedimientoConcursalRepository.findById(idProcedimiento).get();
        ProcedimientoConcursal procedimientoConcursal =
          procedimientoUtil.saveProcedimientoConcursal(inputDto, tmpConcursal);
        return ProcedimientoMapper.createProcedimientoOutputDTO(procedimientoConcursal);

      case "ProcedimientoMonitorio":
        ProcedimientoMonitorio tmpMonitorio =
          procedimientoMonitorioRepository.findById(idProcedimiento).get();
        ProcedimientoMonitorio procedimientoMonitorio =
          procedimientoUtil.saveProcedimientoMonitorio(inputDto, tmpMonitorio);
        return ProcedimientoMapper.createProcedimientoOutputDTO(procedimientoMonitorio);

      case "ProcedimientoOrdinario":
        ProcedimientoOrdinario tmpOrdinario =
          procedimientoOrdinarioRepository.findById(idProcedimiento).get();
        ProcedimientoOrdinario procedimientoOrdinario =
          procedimientoUtil.saveProcedimientoOrdinario(inputDto, tmpOrdinario);
        return ProcedimientoMapper.createProcedimientoOutputDTO(procedimientoOrdinario);

      case "ProcedimientoHipotecario":
        ProcedimientoHipotecario tmpHipotecario =
          procedimientoHipotecarioRepository.findById(idProcedimiento).get();
        ProcedimientoHipotecario procedimientoHipotecario =
          procedimientoUtil.saveProcedimientoHipotecario(inputDto, tmpHipotecario);
        return ProcedimientoMapper.createProcedimientoOutputDTO(procedimientoHipotecario);

      case "ProcedimientoEjecucionNotarial":
        ProcedimientoEjecucionNotarial tmpEjecucionNotarial =
          procedimientoEjecucionNotarialRepository.findById(idProcedimiento).get();
        ProcedimientoEjecucionNotarial procedimientoEjecucionNotarial =
          procedimientoUtil.saveProcedimientoEjecucionNotarial(inputDto, tmpEjecucionNotarial);
        return ProcedimientoMapper.createProcedimientoOutputDTO(procedimientoEjecucionNotarial);
    }
    return ProcedimientoMapper.createProcedimientoOutputDTO(procedimiento);
  }

  @Override
  public void delete(Integer idExpediente, Integer idContrato, Integer idProcedimiento)
    throws NotFoundException {
    Contrato contrato = procedimientoUtil.getContratoByExpedienteAndId(idExpediente, idContrato);
    Procedimiento procedimiento = contratoUtil.findInContrato(contrato, idProcedimiento);
    Boolean recovery = contrato.getExpediente().getCartera().getDatosJudicial();
    recovery = false;
    if (recovery == null || recovery == false) {
      if (procedimiento == null)
        throw new NotFoundException("procedimiento", idProcedimiento);
      contrato.removeProcedimiento(procedimiento);
      procedimiento.setActivo(false);
      procedimientoRepository.save(procedimiento);
      contratoRepository.save(contrato);
    }
  }

  public LinkedHashMap<String, List<Collection<?>>> findExcelProcedimeintoById(Integer idExpediente, Integer idContrato, Integer id) throws Exception {
    //Expediente expediente = expedienteService.findById(id);
    Procedimiento procedimiento = procedimientoRepository.findById(id).orElseThrow(() ->
      new NotFoundException("procedimiento", id));
    String tipoProcedimiento = procedimiento.getTipo().getCodigo();

    Contrato c = contratoRepository.findById(idContrato).orElseThrow(() ->
      new NotFoundException("Contrato", idContrato));

    Expediente expediente = expedienteRepository.findById(idExpediente).orElseThrow(() ->
      new NotFoundException("Expediente", idExpediente));
    String idConcatenado = expediente.getIdConcatenado();

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> judicial = new ArrayList<>();
    judicial.add(JudicialExcel.cabeceras);

    List<Collection<?>> contratosJudiciales = new ArrayList<>();
    contratosJudiciales.add(ContratoJudicialExcel.cabeceras);
    List<Collection<?>> demandadosJudiciales = new ArrayList<>();
    demandadosJudiciales.add(DemandadoJudicialExcel.cabeceras);
    List<Collection<?>> subastas = new ArrayList<>();
    subastas.add(SubastasExcel.cabeceras);

    switch (tipoProcedimiento) {
      case "HIP":
        List<Collection<?>> procedimientosHipotecario = new ArrayList<>();
        procedimientosHipotecario.add(ProcedimientoHipotecarioExcel.cabeceras);
        procedimientosHipotecario.add(new ProcedimientoHipotecarioExcel((ProcedimientoHipotecario) procedimiento, c.getIdCarga(), idConcatenado).getValuesList());

        if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")){
          datos.put("Mortgage Procedure", procedimientosHipotecario);
        } else  {
          datos.put("Procedimiento Hipotecario", procedimientosHipotecario);
        }
        break;

      case "ETNJ":
        List<Collection<?>> procedimientosETJN = new ArrayList<>();
        procedimientosETJN.add(ProcedimientoETJNExcel.cabeceras);
        procedimientosETJN.add(new ProcedimientoETJNExcel((ProcedimientoEtnj) procedimiento, c.getIdCarga(), idConcatenado).getValuesList());

        if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")){
          datos.put("ETNJ Procedure", procedimientosETJN);
        } else  {
          datos.put("Procedimiento ETNJ", procedimientosETJN);
        }
        break;

      case "EN":
        List<Collection<?>> procedimientosEjecucionNotarial = new ArrayList<>();
        procedimientosEjecucionNotarial.add(ProcedimientoEjecucionNotarialExcel.cabeceras);
        procedimientosEjecucionNotarial.add(new ProcedimientoEjecucionNotarialExcel((ProcedimientoEjecucionNotarial) procedimiento, c.getIdCarga(), idConcatenado).getValuesList());

        if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")){
          datos.put("Notarial Execution Procedure", procedimientosEjecucionNotarial);
        } else  {
          datos.put("Procedimiento Ejecucion Notarial", procedimientosEjecucionNotarial);
        }
        break;

      case "MON":
        List<Collection<?>> procedimientosMonitorio = new ArrayList<>();
        procedimientosMonitorio.add(ProcedimientoMonitorioExcel.cabeceras);
        procedimientosMonitorio.add(new ProcedimientoMonitorioExcel((ProcedimientoMonitorio) procedimiento, c.getIdCarga(), idConcatenado).getValuesList());

        if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")){
          datos.put("Monitoring Procedure", procedimientosMonitorio);
        } else  {
          datos.put("Procedimiento Monitorio", procedimientosMonitorio);
        }
        break;

      case "VERB":
        List<Collection<?>> procedimientosVerbal = new ArrayList<>();
        procedimientosVerbal.add(ProcedimientoVerbalExcel.cabeceras);
        procedimientosVerbal.add(new ProcedimientoVerbalExcel((ProcedimientoVerbal) procedimiento, c.getIdCarga(), idConcatenado).getValuesList());

        if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")){
          datos.put("Verbal Procedure", procedimientosVerbal);
        } else  {
          datos.put("Procedimiento Verbal", procedimientosVerbal);
        }
        break;

      case "ORD":
        List<Collection<?>> procedimientosOrdinario = new ArrayList<>();
        procedimientosOrdinario.add(ProcedimientoOrdinarioExcel.cabeceras);
        procedimientosOrdinario.add(new ProcedimientoOrdinarioExcel((ProcedimientoOrdinario) procedimiento, c.getIdCarga(), idConcatenado).getValuesList());

        if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")){
          datos.put("Ordinary Procedure", procedimientosOrdinario);
        } else  {
          datos.put("Procedimiento Ordinario", procedimientosOrdinario);
        }
        break;

      case "ETJ":
        List<Collection<?>> procedimientosETJ = new ArrayList<>();
        procedimientosETJ.add(ProcedimientoETJExcel.cabeceras);
        procedimientosETJ.add(new ProcedimientoETJExcel((ProcedimientoEtj) procedimiento, c.getIdCarga(), idConcatenado).getValuesList());

        if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")){
          datos.put("ETJ Procedure", procedimientosETJ);
        } else  {
          datos.put("Procedimiento ETJ", procedimientosETJ);
        }
        break;

      case "CON":
        List<Collection<?>> procedimientosConcursal = new ArrayList<>();
        procedimientosConcursal.add(ProcedimientoConcursalExcel.cabeceras);
        procedimientosConcursal.add(new ProcedimientoConcursalExcel((ProcedimientoConcursal) procedimiento, c.getIdCarga(), idConcatenado).getValuesList());

        if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")){
          datos.put("Bankruptcy Proceedings", procedimientosConcursal);
        } else  {
          datos.put("Procedimiento Concursal", procedimientosConcursal);
        }
        break;
    }

    judicial.add(new JudicialExcel(procedimiento, c.getIdCarga(), idConcatenado).getValuesList());
    for (Contrato contratoJudicial : procedimiento.getContratos()) {
      contratosJudiciales.add(new ContratoJudicialExcel(contratoJudicial, procedimiento.getId(), idConcatenado).getValuesList());
    }
    for (Interviniente demandadoJudicial : procedimiento.getDemandados()) {
      demandadosJudiciales.add(new DemandadoJudicialExcel(demandadoJudicial, procedimiento.getId(), idConcatenado).getValuesList());
    }
    for (Subasta subasta : procedimiento.getSubastas()) {
      subastas.add(new SubastasExcel(subasta, procedimiento.getId(), idConcatenado).getValuesList());
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("Judicial", judicial);
      datos.put("Loans Judicial", contratosJudiciales);
      datos.put("Defendant Judicial", demandadosJudiciales);
      datos.put("Auctions", subastas);

    } else  {
      datos.put("Judicial", judicial);
      datos.put("Contratos Judicial", contratosJudiciales);
      datos.put("Demandados Judicial", demandadosJudiciales);
      datos.put("Subastas", subastas);
    }

    return datos;
  }

  public LinkedHashMap<String, List<Collection<?>>> findExcelProcedimeintoById(Integer idExpediente, Integer idContrato) throws Exception {
    Contrato c = contratoRepository.findById(idContrato).orElseThrow(() ->
      new NotFoundException("Contrato", idContrato));
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    String idConcatenado = c.getExpediente().getIdConcatenado();

    List<Collection<?>> judicial = new ArrayList<>();
    judicial.add(JudicialExcel.cabeceras);

    List<Collection<?>> contratosJudiciales = new ArrayList<>();
    contratosJudiciales.add(ContratoJudicialExcel.cabeceras);
    List<Collection<?>> demandadosJudiciales = new ArrayList<>();
    demandadosJudiciales.add(DemandadoJudicialExcel.cabeceras);

    List<Collection<?>> procedimientosETJN = new ArrayList<>();
    procedimientosETJN.add(ProcedimientoETJNExcel.cabeceras);
    List<Collection<?>> procedimientosETJ = new ArrayList<>();
    procedimientosETJ.add(ProcedimientoETJExcel.cabeceras);
    List<Collection<?>> procedimientosHipotecario = new ArrayList<>();
    procedimientosHipotecario.add(ProcedimientoHipotecarioExcel.cabeceras);
    List<Collection<?>> procedimientosEjecucionNotarial = new ArrayList<>();
    procedimientosEjecucionNotarial.add(ProcedimientoEjecucionNotarialExcel.cabeceras);
    List<Collection<?>> procedimientosMonitorio = new ArrayList<>();
    procedimientosMonitorio.add(ProcedimientoMonitorioExcel.cabeceras);
    List<Collection<?>> procedimientosVerbal = new ArrayList<>();
    procedimientosVerbal.add(ProcedimientoVerbalExcel.cabeceras);
    List<Collection<?>> procedimientosOrdinario = new ArrayList<>();
    procedimientosOrdinario.add(ProcedimientoOrdinarioExcel.cabeceras);
    List<Collection<?>> procedimientosConcursal = new ArrayList<>();
    procedimientosConcursal.add(ProcedimientoConcursalExcel.cabeceras);
    List<Collection<?>> subastas = new ArrayList<>();
    subastas.add(SubastasExcel.cabeceras);

    for (Procedimiento procedimiento : c.getProcedimientos()) {
      if (procedimiento instanceof ProcedimientoHipotecario) {
        procedimientosHipotecario.add(
          new ProcedimientoHipotecarioExcel(
            (ProcedimientoHipotecario) procedimiento, c.getIdCarga(), idConcatenado)
            .getValuesList());
      } else if (procedimiento instanceof ProcedimientoConcursal) {
        procedimientosConcursal.add(
          new ProcedimientoConcursalExcel((ProcedimientoConcursal) procedimiento, c.getIdCarga(), idConcatenado)
            .getValuesList());
      } else if (procedimiento instanceof ProcedimientoEjecucionNotarial) {
        procedimientosEjecucionNotarial.add(
          new ProcedimientoEjecucionNotarialExcel(
            (ProcedimientoEjecucionNotarial) procedimiento, c.getIdCarga(), idConcatenado)
            .getValuesList());
      } else if (procedimiento instanceof ProcedimientoEtj) {
        procedimientosETJ.add(
          new ProcedimientoETJExcel((ProcedimientoEtj) procedimiento, c.getIdCarga(), idConcatenado)
            .getValuesList());
      } else if (procedimiento instanceof ProcedimientoEtnj) {
        procedimientosETJN.add(
          new ProcedimientoETJNExcel((ProcedimientoEtnj) procedimiento, c.getIdCarga(), idConcatenado)
            .getValuesList());
      } else if (procedimiento instanceof ProcedimientoMonitorio) {
        procedimientosMonitorio.add(
          new ProcedimientoMonitorioExcel((ProcedimientoMonitorio) procedimiento, c.getIdCarga(), idConcatenado)
            .getValuesList());
      } else if (procedimiento instanceof ProcedimientoOrdinario) {
        procedimientosOrdinario.add(
          new ProcedimientoOrdinarioExcel((ProcedimientoOrdinario) procedimiento, c.getIdCarga(), idConcatenado)
            .getValuesList());
      } else if (procedimiento instanceof ProcedimientoVerbal) {
        procedimientosVerbal.add(
          new ProcedimientoVerbalExcel((ProcedimientoVerbal) procedimiento, c.getIdCarga(), idConcatenado)
            .getValuesList());
      }

      judicial.add(new JudicialExcel(procedimiento, c.getIdCarga(), idConcatenado).getValuesList());
      for (Contrato contratoJudicial : procedimiento.getContratos()) {
        contratosJudiciales.add(
          new ContratoJudicialExcel(contratoJudicial, procedimiento.getId(), idConcatenado).getValuesList());
      }
      for (Interviniente demandadoJudicial : procedimiento.getDemandados()) {
        demandadosJudiciales.add(
          new DemandadoJudicialExcel(demandadoJudicial, procedimiento.getId(), idConcatenado).getValuesList());
      }
      for (Subasta subasta : procedimiento.getSubastas()) {
        subastas.add(new SubastasExcel(subasta, procedimiento.getId(), idConcatenado).getValuesList());
      }
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      datos.put("Judicial", judicial);
      datos.put("ETNJ Procedure", procedimientosETJN);
      datos.put("ETJ Procedure", procedimientosETJ);
      datos.put("Mortgage Procedure", procedimientosHipotecario);
      datos.put("Notarial Execution Procedure", procedimientosEjecucionNotarial);
      datos.put("Monitoring Procedure", procedimientosMonitorio);
      datos.put("Verbal Procedure", procedimientosVerbal);
      datos.put("Ordinary Procedure", procedimientosOrdinario);
      datos.put("Bankruptcy Proceedings", procedimientosConcursal);
      datos.put("Loans Judicial", contratosJudiciales);
      datos.put("Defendants Judicial", demandadosJudiciales);
      datos.put("auctions", subastas);
    } else  {

      datos.put("Judicial", judicial);
      datos.put("Procedimientos ETJN", procedimientosETJN);
      datos.put("Procedimientos ETJ", procedimientosETJ);
      datos.put("Procedimientos Hipotecario", procedimientosHipotecario);
      datos.put("Procedimientos Notarial", procedimientosEjecucionNotarial);
      datos.put("Procedimientos Monitorio", procedimientosMonitorio);
      datos.put("Procedimientos Verbal", procedimientosVerbal);
      datos.put("Procedimientos Ordinario", procedimientosOrdinario);
      datos.put("Procedimientos Concursal", procedimientosConcursal);
      datos.put("Contratos Judicial", contratosJudiciales);
      datos.put("Demandados Judicial", demandadosJudiciales);
      datos.put("Subastas", subastas);
    }

    return datos;
  }

  @Override
  public List<HitoProcedimientoDTO> findHitosByTipoProcedimiento(Integer tipoProcedimiento) {
    List<HitoProcedimiento> hitos = hitoProcedimientoRepository.findAllByTipoProcedimientoIdAndActivoIsTrueAndRecoveryIsFalse(tipoProcedimiento);
    return hitos.stream().map(HitoProcedimientoDTO::new).collect(Collectors.toList());
  }

  //Revisar como hacerlo con query
  @Override
  public List<ProcedimientoDTOToList> getAllByExpedienteId(Integer idExpediente) throws Exception {

    Expediente expediente = expedienteRepository.findById(idExpediente).orElseThrow(() ->
      new NotFoundException("expediente", idExpediente));
    List<Contrato> listaContrato = expediente.getContratos().stream().collect(Collectors.toList());
    Set<Procedimiento> listaprocedimientos = new HashSet<>();
    List<ProcedimientoDTOToList> listaProcedimientosDtoList = new ArrayList<>();
    for (Contrato contrato : listaContrato) {
      for (Procedimiento procedimiento : contrato.getProcedimientos()) {
        listaprocedimientos.add(procedimiento);
      }
    }
    List<Procedimiento> listaProcedimientosToList = listaprocedimientos.stream().collect(Collectors.toList());

    for (Procedimiento procedimiento : listaProcedimientosToList) {
      ProcedimientoDTOToList procedimientoDTOToList = procedimientoMapper.procedimientoDTOToList(procedimiento);
      listaProcedimientosDtoList.add(procedimientoDTOToList);
    }
    return listaProcedimientosDtoList;
  }


  @Override
  public ListWithCountDTO<DocumentoDTO> listarDocumentosProcedimientoId(Integer idProcedimiento, String codigoProcedimiento, String idDocumento, String usuarioCreador, String tipo, String idEntidad, String fechaActualizacion, String orderDirection, String orderField, Integer size, Integer page) throws IOException {

    List<DocumentoDTO> listadoIdGetor = new ArrayList<>();
    TipoProcedimiento tipoProcedimientoCod = tipoProcedimientoRepository.findByCodigoAndRecoveryIsTrue(codigoProcedimiento).orElse(null);
    try {
      List<DocumentoDTO> listadoIdGestor = this.servicioGestorDocumental
        .listarDocumentos(CodigoEntidad.obtenerPorTipoProcedimiento(tipoProcedimientoCod.getCodigo()), idProcedimiento.toString())
        .stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());
      List<DocumentoDTO> list = gestorDocumentalUseCase.filterOrderDocumentosDTO(listadoIdGestor, idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, null, null, orderDirection, orderField);
      List<DocumentoDTO> listaFiltradaPaginada = list.stream().skip(size * page).limit(size).collect(Collectors.toList());
      return new ListWithCountDTO<>(listaFiltradaPaginada, list.size());
    } catch (Exception ex) {
      return new ListWithCountDTO<>(new ArrayList<>(), 10);
    }
  }
}
