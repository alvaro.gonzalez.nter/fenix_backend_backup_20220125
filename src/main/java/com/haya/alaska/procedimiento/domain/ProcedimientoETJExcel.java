package com.haya.alaska.procedimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ProcedimientoETJExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Procedure ID");
      cabeceras.add("Presentation date");
      cabeceras.add("Main demand");
      cabeceras.add("Date Order Dispatching Execution");
      cabeceras.add("Notification date payment requirement");
      cabeceras.add("Opposition date");
      cabeceras.add("Result");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Procedimiento");
      cabeceras.add("Fecha presentación");
      cabeceras.add("Principal demanda");
      cabeceras.add("Fecha auto despachando ejecución");
      cabeceras.add("Fecha notificación req pago");
      cabeceras.add("Fecha oposición");
      cabeceras.add("Resultado");
    }
  }

  public ProcedimientoETJExcel(ProcedimientoEtj procedimiento, String idContrato, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      this.add("Connection ID", idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Procedure ID", procedimiento.getId());
      this.add("Presentation date", procedimiento.getFechaPresentacionDemanda());
      this.add("Main demand", procedimiento.getPrincipalDemanda());
      this.add("Date Order Dispatching Execution", procedimiento.getFechaAutoDespachandoEjecucion());
      this.add("Notification date payment requirement", procedimiento.getFechaNotificacionReqPago());
      this.add("Opposition date", procedimiento.getFechaOposicion());
      this.add("Result", procedimiento.getResultado());

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Contrato", idContrato);
      this.add("Id Procedimiento", procedimiento.getId());
      this.add("Fecha presentación", procedimiento.getFechaPresentacionDemanda());
      this.add("Principal demanda", procedimiento.getPrincipalDemanda());
      this.add("Fecha auto despachando ejecución", procedimiento.getFechaAutoDespachandoEjecucion());
      this.add("Fecha notificación req pago", procedimiento.getFechaNotificacionReqPago());
      this.add("Fecha oposición", procedimiento.getFechaOposicion());
      this.add("Resultado", procedimiento.getResultado());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
