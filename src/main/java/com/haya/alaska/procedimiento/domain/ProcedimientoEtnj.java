package com.haya.alaska.procedimiento.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_PROCEDIMIENTO_ETNJ")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_PROCEDIMIENTO_ETNJ")
public class ProcedimientoEtnj extends Procedimiento {

  private static final long serialVersionUID = 1L;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PRESENTACION_DEMANDA")
  private Date fechaPresentacionDemanda;

  @Column(name = "NUM_PRINCIPAL_DEMANDA")
  private Double principalDemanda;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_AUTO_DESPACHANDO")
  private Date fechaAutoDespachando;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_NOTIFICACION_REQ_PAGO")
  private Date fechaNotificacionReqPago;

  @Column(name = "DES_DIRECCION_NOTIFICACION")
  private String direccionNotificacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_OPOSICION")
  private Date fechaOposicion;

  @Column(name = "IND_RESULTADO")
  private Boolean resultado;

  public ProcedimientoEtnj(Integer orden) {
    this.setOrden(orden);
  }

  @Override
  public String getHito() {
    if (this.resultado != null) {
      return "01";//"Resultado Comparecencia Oposición";
    }
    if (this.fechaOposicion != null) {
      return "02";//"Fecha Oposición";
    }
    if (this.direccionNotificacion != null) {
      return "03";//"Domicilio Notificación Requerimiento de Pago";
    }
    if (this.fechaNotificacionReqPago != null) {
      return "04";//"Fecha Requerimiento de Pago";
    }
    if (this.fechaAutoDespachando != null) {
      return "05";//"Fecha Auto despachando ejecución";
    }
    if (this.principalDemanda != null) {
      return "06";//"Importe Interposición Demanda";
    }
    if (this.fechaPresentacionDemanda != null) {
      return "07";//"Fecha Interposición Demanda";
    }
    return super.getHito();
  }
}
