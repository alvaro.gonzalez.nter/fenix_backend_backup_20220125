package com.haya.alaska.procedimiento.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_PROCEDIMIENTO_HIPOTECARIO")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_PROCEDIMIENTO_HIPOTECARIO")
public class ProcedimientoHipotecario extends Procedimiento {

  private static final long serialVersionUID = 1L;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PRESTANCION_DEMANDA")
  private Date fechaPresentacionDemanda;

  @Column(name = "NUM_PRINCIPAL_DEMANDA")
  private Double principalDemanda;

  @Column(name = "IND_DEMANDA_ADMITIDA")
  private Boolean demandaAdmitida;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CERTIFICACION")
  private Date fechaCertificacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_NOTIFICACION_AUTO")
  private Date fechaNotificacionAuto;

  @Column(name = "IND_RESULTADO_NOTIFICACION")
  private Boolean resultadoNotificacion;

  @Column(name = "DES_DOMICILIO_NOTIFICACION")
  private String domicilioNotificacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_OPOSICION")
  private Date fechaOposicion;

  @Column(name = "IND_RESULT_OPOSICION")
  private Boolean resultadoOposicion;

  public ProcedimientoHipotecario(Integer orden) {
    this.setOrden(orden);
  }

  @Override
  public String getHito() {
    if (this.resultadoOposicion != null) {
      return "01";//"Resultado Comparecencia Oposición";
    }
    if (this.fechaOposicion != null) {
      return "02";//"Fecha Oposición";
    }
    if (this.domicilioNotificacion != null) {
      return "03";//"Domicilio Notificación Requerimiento de Pago";
    }
    if (this.resultadoNotificacion != null) {
      return "04";//"Resultado Requerimiento de Pago";
    }
    if (this.fechaNotificacionAuto != null) {
      return "05";//"Fecha Requerimiento de Pago";
    }
    if (this.fechaCertificacion != null) {
      return "06";//"Fecha Certificación Registral";
    }
    if (this.demandaAdmitida != null) {
      return "07";//"Auto despachando ejecución";
    }
    if (this.principalDemanda != null) {
      return "08";//"Importe Interposición Demanda";
    }
    if (this.fechaPresentacionDemanda != null) {
      return "09";//"Fecha Interposición Demanda";
    }
    return super.getHito();
  }
}
