package com.haya.alaska.procedimiento.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estado_procedimiento.domain.EstadoProcedimiento;
import com.haya.alaska.formalizacion.domain.FormalizacionBien;
import com.haya.alaska.formalizacion.domain.FormalizacionProcedimiento;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.subasta.domain.Subasta;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import lombok.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_PROCEDIMIENTO")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "MSTR_PROCEDIMIENTO")
public class Procedimiento implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "ID_CARGA")
  private String idCarga;

  @Column(name = "DES_NUM_AUTOS")
  private String numeroAutos;

  @Column(name = "DES_PLAZA")
  private String plaza;

  @Column(name = "DES_JUZGADO")
  private String juzgado;

  @Column(name = "NUM_ORDEN")
  private Integer orden;

  @Column(name = "NUM_IMPORTE_DEMANDA")
  private Double importeDemanda;

  @Column(name = "DES_DEMANDANTE")
  private String demandante;

  @Column(name = "NUM_GASTOS_INCURRIDOS")
  private Double gastosIncurridos;

  @Column(name = "DES_HABILITACIONES")
  private String habilitaciones;

  @Column(name = "NUM_ENTREGAS_JUDICIALES")
  private Double entregasJudiciales;

  @Column(name = "DES_DESPACHO")
  private String despacho;

  @Column(name = "DES_LETRADO")
  private String letrado;

  @Column(name = "DES_TFNO_LETRADO")
  private String telefonoLetrado;

  @Column(name = "DES_TFNO2_LETRADO")
  private String telefono2Letrado;

  @Column(name = "DES_EMAIL_LETRADO")
  private String emailLetrado;

  @Column(name = "DES_GESTOR_JUDICIAL_HAYA")
  private String gestorJudicialHaya;

  @Column(name = "DES_EMAIL_GESTOR_JUDICIAL_HAYA")
  private String emailGestorJudicialHaya;

  @Column(name = "DES_PROCURADOR")
  private String procurador;

  @Column(name = "DES_TFNO_PROCURADOR")
  private String telefonoProcurador;

  @Column(name = "DES_EMAIL_PROCURADOR")
  private String emailProcurador;

  @Column(name = "DES_DESPACHO_CONTRARIO")
  private String despachoContrario;

  @Column(name = "DES_LETRADO_CONTRARIO")
  private String letradoContrario;

  @Column(name = "DES_TFNO_LETRADO_CONTRARIO")
  private String telefonoLetradoContrario;

  @Column(name = "DES_TFNO2_LETRADO_CONTRARIO")
  private String telefono2LetradoContrario;

  @Column(name = "DES_EMAIL_LETRADO_CONTRARIO")
  private String emailLetradoContrario;

  @Column(name = "DES_PROCURADOR_CONTRARIO")
  private String procuradorContrario;

  @Column(name = "DES_TFNO_PROCURADOR_CONTRARIO")
  private String telefonoProcuradorContrario;

  @Column(name = "DES_EMAIL_PROCURADOR_CONTRARIO")
  private String emailProcuradorContrario;

  @Column(name = "DES_MOTIVO")
  private String motivo;

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
      name = "RELA_CONTRATO_PROCEDIMIENTO",
      joinColumns = @JoinColumn(name = "ID_PROCEDIMIENTO"),
      inverseJoinColumns = @JoinColumn(name = "ID_CONTRATO"))
  @AuditJoinTable(name = "HIST_RELA_CONTRATO_PROCEDIMIENTO")
  private Set<Contrato> contratos = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "ID_PROVINCIA", referencedColumnName = "id", updatable = true)
  private Provincia provincia;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_PROCEDIMIENTO")
  private TipoProcedimiento tipo;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_PROCEDIMIENTO")
  private EstadoProcedimiento estado;

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(
      name = "RELA_PROCEDIMIENTO_INTERVINIENTE",
      joinColumns = @JoinColumn(name = "ID_PROCEDIMIENTO"),
      inverseJoinColumns = @JoinColumn(name = "ID_DEMANDADO"))
  @AuditJoinTable(name = "HIST_RELA_PROCEDIMIENTO_INTERVINIENTE")
  private Set<Interviniente> demandados = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROCEDIMIENTO")
  @NotAudited
  private Set<Subasta> subastas = new HashSet<>();

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_HITO_PROCEDIMIENTO")
  private HitoProcedimiento hitoProcedimiento;

  /*Datos para el caso de que sea de tipo concursal*/
  @Column(name = "DES_NOMBRE_ADM_CONCURSAL")
  private String nombreAdminConcursal;

  @Column(name = "DES_DIRECCION_ADM_CONCURSAL")
  private String direccionAdminConcursal;

  @Column(name = "DES_TELEFONO_ADM_CONCURSAL")
  private String telefonoAdminConcursal;

  @Column(name = "DES_EMAIL_ADM_CONCURSAL")
  private String emailAdminConcursal;

  @Column(name = "DES_GESTOR_CONC_HAYA")
  private String gestorConcursosHaya;

  @Column(name = "DES_MAIL_GESTOR_CONC_HAYA")
  private String mailGestorConcursosHaya;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_ALTA")
  private Date fechaAlta;

  @OneToOne(mappedBy = "procedimiento")
  private FormalizacionProcedimiento formalizacionProcedimiento;

  public String getHito() {
    return null;
  }
}
