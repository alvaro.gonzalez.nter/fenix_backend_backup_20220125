package com.haya.alaska.procedimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ProcedimientoOrdinarioExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Procedure ID");
      cabeceras.add("Presentation date");
      cabeceras.add("Main demand");
      cabeceras.add("Claim admission date");
      cabeceras.add("Notification result of payment request");
      cabeceras.add("Notification address");
      cabeceras.add("Date of demand for payment debtor");
      cabeceras.add("Opposition date");
      cabeceras.add("Opposition result");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Procedimiento");
      cabeceras.add("Fecha presentación");
      cabeceras.add("Principal demanda");
      cabeceras.add("Fecha admisión demanda");
      cabeceras.add("Resultado notificación req pago");
      cabeceras.add("Dirección notificación");
      cabeceras.add("Fecha req pago deudor");
      cabeceras.add("Fecha oposición");
      cabeceras.add("Resultado oposición");
    }
  }

  public ProcedimientoOrdinarioExcel(ProcedimientoOrdinario procedimiento, String idContrato,String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Procedure ID", procedimiento.getId());
      this.add("Presentation date", procedimiento.getFechaPresentacionDemanda());
      this.add("Main demand", procedimiento.getPrincipalDemanda());
      this.add("Claim admission date", procedimiento.getFechaAdmisionDemanda());
      this.add("Notification result of payment request", procedimiento.getResultadoNotificacionReqPago());
      this.add("Notification address", procedimiento.getDireccionNotificacion());
      this.add("Date of demand for payment debtor", procedimiento.getFechaReqPagoDeudor());
      this.add("Opposition date", procedimiento.getFechaOposicion());
      this.add("Opposition result", procedimiento.getResultadoOposicion());

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Contrato", idContrato);
      this.add("Id Procedimeinto", procedimiento.getId());
      this.add("Fecha presentación", procedimiento.getFechaPresentacionDemanda());
      this.add("Principal demanda", procedimiento.getPrincipalDemanda());
      this.add("Fecha admisión demanda", procedimiento.getFechaAdmisionDemanda());
      this.add("Resultado notificación req pago", procedimiento.getResultadoNotificacionReqPago());
      this.add("Direccion notificación", procedimiento.getDireccionNotificacion());
      this.add("Fecha req pago deudor", procedimiento.getFechaReqPagoDeudor());
      this.add("Fecha oposición", procedimiento.getFechaOposicion());
      this.add("Resultado oposición", procedimiento.getResultadoOposicion());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
