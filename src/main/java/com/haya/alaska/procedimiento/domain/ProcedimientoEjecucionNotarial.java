package com.haya.alaska.procedimiento.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_PROCEDIMIENTO_EJECUCION_NOTARIAL")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_PROCEDIMIENTO_EJECUCION_NOTARIAL")
public class ProcedimientoEjecucionNotarial extends Procedimiento {

  private static final long serialVersionUID = 1L;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FIRMA_ACTA")
  private Date fechaFirmaActa;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_RECEPCION_CERTIFICACION_REGISTRAL")
  private Date fechaRecepcionCertificacionRegistral;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_REQ_PAGO_DEUDOR")
  private Date fechaReqPagoDeudor;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_AUTO")
  private Date fechaAuto;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_SUBASTA")
  private Date fechaSubasta;

  @Column(name = "IND_CELEBRADA")
  private Boolean celebrada;

  @Column(name = "IND_RESULTADO_SUBASTA")
  private Boolean resultadoSubasta;

  @Column(name = "DES_MOTIVO_SUSPENSION")
  private String motivoSuspension;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FIRMA_ESCRITURA")
  private Date fechaFirmaEscritura;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_POSESION")
  private Date fechaPosesion;

  public ProcedimientoEjecucionNotarial(Integer orden) {
    this.setOrden(orden);
  }

  @Override
  public String getHito() {
    if (this.fechaPosesion != null) {
      return "01";//"Fecha Posesión";
    }
    if (this.fechaFirmaEscritura != null) {
      return "02";//"Fecha Escritura Compraventa";
    }
    if (this.motivoSuspension != null) {
      return "03";//"Motivo suspensión Celebración Subasta";
    }
    if (this.resultadoSubasta != null) {
      return "04";//"Resultado Celebración Subasta";
    }
    if (this.celebrada != null) {
      return "05";//"Celebración Subasta";
    }
    if (this.fechaSubasta != null) {
      return "06";//"Fecha Subasta";
    }
    if (this.fechaAuto != null) {
      return "07";//"Fecha Anuncio Subasta";
    }
    if (this.fechaReqPagoDeudor != null) {
      return "08";//"Fecha Requerimiento al deudor";
    }
    if (this.fechaRecepcionCertificacionRegistral != null) {
      return "09";//"Fecha Certificación Registral";
    }
    if (this.fechaFirmaActa != null) {
      return "10";//"Fecha Entrega acta requerimiento a notario";
    }
    return super.getHito();
  }
}
