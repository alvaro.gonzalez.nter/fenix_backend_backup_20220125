package com.haya.alaska.procedimiento.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_PROCEDIMIENTO_ETJ")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_PROCEDIMIENTO_ETJ")
public class ProcedimientoEtj extends Procedimiento {

  private static final long serialVersionUID = 1L;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PRESENTACION_DEMANDA")
  private Date fechaPresentacionDemanda;

  @Column(name = "NUM_PRINCIPAL_DEMANDA")
  private Double principalDemanda;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_AUTO_DEMORA")
  private Date fechaAutoDespachandoEjecucion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_NOTIFICACION_REQ_PAGO")
  private Date fechaNotificacionReqPago;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_OPOSICION")
  private Date fechaOposicion;

  @Column(name = "IND_RESULTADO")
  private Boolean resultado;

  public ProcedimientoEtj(Integer orden) {
    this.setOrden(orden);
  }

  @Override
  public String getHito() {
    if (this.resultado != null) {
      return "01";//"Resultado Comparecencia Oposición";
    }
    if (this.fechaOposicion != null) {
      return "02";//"Fecha Oposición";
    }
    if (this.fechaNotificacionReqPago != null) {
      return "03";//"Fecha Requerimiento de Pago";
    }
    if (this.fechaAutoDespachandoEjecucion != null) {
      return "04";//"Fecha Auto despachando ejecución";
    }
    if (this.principalDemanda != null) {
      return "05";//"Importe Interposición Demanda";
    }
    if (this.fechaPresentacionDemanda != null) {
      return "06";//"Fecha Interposición Demanda";
    }
    return super.getHito();
  }
}
