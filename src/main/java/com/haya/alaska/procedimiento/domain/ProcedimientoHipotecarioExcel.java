package com.haya.alaska.procedimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ProcedimientoHipotecarioExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Procedure ID");
      cabeceras.add("Demand filing date");
      cabeceras.add("Main demand");
      cabeceras.add("Claim admitted");
      cabeceras.add("Certification date");
      cabeceras.add("Date of notification of order");
      cabeceras.add("Notification result");
      cabeceras.add("Notification address");
      cabeceras.add("Opposition date");
      cabeceras.add("Opposition result");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Procedimiento");
      cabeceras.add("Fecha presentación demanda");
      cabeceras.add("Principal demanda");
      cabeceras.add("Demanda admitida");
      cabeceras.add("Fecha certificación");
      cabeceras.add("Fecha notificación auto");
      cabeceras.add("Resultado notificación");
      cabeceras.add("Domicilio notificación");
      cabeceras.add("Fecha oposición");
      cabeceras.add("Resultado oposición");
    }
  }

  public ProcedimientoHipotecarioExcel(ProcedimientoHipotecario procedimiento, String idContrato, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Procedure ID", procedimiento.getId());
      this.add("Demand filing date", procedimiento.getFechaPresentacionDemanda());
      this.add("Main demand", procedimiento.getPrincipalDemanda());
      this.add("Claim admitted", procedimiento.getDemandaAdmitida());
      this.add("Certification date", procedimiento.getFechaCertificacion());
      this.add("Date of notification of order", procedimiento.getFechaNotificacionAuto());
      this.add("Notification result", procedimiento.getResultadoNotificacion());
      this.add("Notification address", procedimiento.getDomicilioNotificacion());
      this.add("Opposition date", procedimiento.getFechaOposicion());
      this.add("Opposition result", procedimiento.getResultadoOposicion());

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Contrato", idContrato);
      this.add("Id Procedimiento", procedimiento.getId());
      this.add("Fecha presentación demanda", procedimiento.getFechaPresentacionDemanda());
      this.add("Principal demanda", procedimiento.getPrincipalDemanda());
      this.add("Demanda admitida", procedimiento.getDemandaAdmitida());
      this.add("Fecha certificación", procedimiento.getFechaCertificacion());
      this.add("Fecha notificación auto", procedimiento.getFechaNotificacionAuto());
      this.add("Resultado notificación", procedimiento.getResultadoNotificacion());
      this.add("Domicilio notificación", procedimiento.getDomicilioNotificacion());
      this.add("Fecha oposición", procedimiento.getFechaOposicion());
      this.add("Resultado oposición", procedimiento.getResultadoOposicion());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
