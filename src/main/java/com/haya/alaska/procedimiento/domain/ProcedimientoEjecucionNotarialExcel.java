package com.haya.alaska.procedimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ProcedimientoEjecucionNotarialExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Procedure ID");
      cabeceras.add("Minute signing date");
      cabeceras.add("Date of receipt of registration certificate");
      cabeceras.add("Date of demand for payment debtor");
      cabeceras.add("Court order date");
      cabeceras.add("Auction date");
      cabeceras.add("Celebrated");
      cabeceras.add("Auction results");
      cabeceras.add("Reason suspension");
      cabeceras.add("Date signature deed");
      cabeceras.add("Possession date");

    } else {
      cabeceras.add("ID Expediente");
      cabeceras.add("ID Contrato");
      cabeceras.add("ID Procedimiento");
      cabeceras.add("Fecha firma acta");
      cabeceras.add("Fecha recepción certificado registral");
      cabeceras.add("Fecha req pago deudor");
      cabeceras.add("Fecha auto");
      cabeceras.add("Fecha subasta");
      cabeceras.add("Celebrada");
      cabeceras.add("Resultado subasta");
      cabeceras.add("Motivo suspensión");
      cabeceras.add("Fecha firma escritura");
      cabeceras.add("Fecha posesión");
    }
  }

  public ProcedimientoEjecucionNotarialExcel(ProcedimientoEjecucionNotarial procedimiento, String idContrato, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID",idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Procedure ID", procedimiento.getId());
      this.add("Minute signing date", procedimiento.getFechaFirmaActa());
      this.add("Date of receipt of registration certificate", procedimiento.getFechaRecepcionCertificacionRegistral());
      this.add("Date of demand for payment debtor", procedimiento.getFechaReqPagoDeudor());
      this.add("Court order date", procedimiento.getFechaAuto());
      this.add("Auction date", procedimiento.getFechaSubasta());
      this.add("Celebrated", procedimiento.getCelebrada());
      this.add("Auction results", procedimiento.getResultadoSubasta());
      this.add("Reason suspension", procedimiento.getMotivoSuspension());
      this.add("Date signature deed", procedimiento.getFechaFirmaEscritura());
      this.add("Possession date", procedimiento.getFechaPosesion());


    }

    else{

    this.add("ID Expediente",idExpediente);
    this.add("ID Contrato", idContrato);
    this.add("ID Procedimiento", procedimiento.getId());
    this.add("Fecha Firma Acta", procedimiento.getFechaFirmaActa());
    this.add("Fecha recepción certificado registral", procedimiento.getFechaRecepcionCertificacionRegistral());
    this.add("Fecha req pago deudor", procedimiento.getFechaReqPagoDeudor());
    this.add("Fecha auto", procedimiento.getFechaAuto());
    this.add("Fecha subasta", procedimiento.getFechaSubasta());
    this.add("Celebrada", procedimiento.getCelebrada());
    this.add("Resultado subasta", procedimiento.getResultadoSubasta());
    this.add("Motivo suspension", procedimiento.getMotivoSuspension());
    this.add("Fecha firma escritura", procedimiento.getFechaFirmaEscritura());
    this.add("Fecha posesión", procedimiento.getFechaPosesion());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
