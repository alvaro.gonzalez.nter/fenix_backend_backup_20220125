package com.haya.alaska.procedimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class ProcedimientoCarteraExcel  extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();


  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Procedure ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Status");
      cabeceras.add("Line number");
      cabeceras.add("Procedure type");
      cabeceras.add("Number Orders");
      cabeceras.add("Place");
      cabeceras.add("Court");
      cabeceras.add("Province");
      cabeceras.add("Amount demanded");
      cabeceras.add("Plaintiff");
      cabeceras.add("Expenses Incurred");
      cabeceras.add("Qualifications");
      cabeceras.add("Judicial Deliveries");
      cabeceras.add("Respondents");
      cabeceras.add("Office");
      cabeceras.add("Counsel");
      cabeceras.add("Legal Telephone");
      cabeceras.add("Legal2 Telephone");
      cabeceras.add("email Legal");
      cabeceras.add("Judicial Manager Haya");
      cabeceras.add("Email Manager Haya");
      cabeceras.add("Procurator");
      cabeceras.add("Procurator Telephone");
      cabeceras.add("Email Procurator");
      cabeceras.add("Contrary Dispatch");
      cabeceras.add("Opposing Counsel");
      cabeceras.add("Opposing Counsel Telephone");
      cabeceras.add("Opposing Counsel Telephone2");
      cabeceras.add("Opposing Counsel Email");
      cabeceras.add("Counter Procurator");
      cabeceras.add("Telephone Counter Procurator");
      cabeceras.add("Email Counter Procurator");

    } else {

      cabeceras.add("Id Procedimiento");
      cabeceras.add("Id Contrato");
      cabeceras.add("Estado");
      cabeceras.add("Numero Linea");
      cabeceras.add("Tipo Procedimiento");
      cabeceras.add("Numero Autos");
      cabeceras.add("Plaza");
      cabeceras.add("Juzgado");
      cabeceras.add("Provincia");
      cabeceras.add("Importe Demanda");
      cabeceras.add("Demandante");
      cabeceras.add("Gastos Incurridos");
      cabeceras.add("Habilitaciones");
      cabeceras.add("Entregas Judiciales");
      cabeceras.add("Demandados");
      cabeceras.add("Despacho");
      cabeceras.add("Letrado");
      cabeceras.add("Teléfono Letrado");
      cabeceras.add("Teléfono2 Letrado");
      cabeceras.add("Email Letrado");
      cabeceras.add("Gestor Judicial Haya");
      cabeceras.add("Email Gestor Haya");
      cabeceras.add("Procurador");
      cabeceras.add("Teléfono Procurador");
      cabeceras.add("Email Procurador");
      cabeceras.add("Despacho Contrario");
      cabeceras.add("Letrado Contrario");
      cabeceras.add("Teléfono Letrado Contrario");
      cabeceras.add("Teléfono2 Letrado Contrario");
      cabeceras.add("Email Letrado Contrario");
      cabeceras.add("Procurador Contrario");
      cabeceras.add("Teléfono Procurador Contrario");
      cabeceras.add("Email Procurador Contrario");
    }
  }


  public ProcedimientoCarteraExcel(Procedimiento procedimiento, String idContrato) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Procedure ID", procedimiento.getId());
      this.add("Loan ID", idContrato);
      this.add("Status", Boolean.TRUE.equals(procedimiento.getActivo()) ? "ACTIVE":"INACTIVE");
      this.add("Line number", null);
      this.add("Procedure type", procedimiento.getTipo()!=null ? procedimiento.getTipo().getValorIngles():null);
      this.add("Number Orders", procedimiento.getNumeroAutos()!=null ? procedimiento.getNumeroAutos():null);
      this.add("Place", procedimiento.getPlaza()!=null ? procedimiento.getPlaza():null);
      this.add("Court", procedimiento.getJuzgado()!=null ? procedimiento.getJuzgado():null);
      this.add("Province", procedimiento.getProvincia()!=null ? procedimiento.getProvincia().getValorIngles():null);
      this.add("Amount demanded", procedimiento.getImporteDemanda()!=null ? procedimiento.getImporteDemanda():null);
      this.add("Plaintiff", procedimiento.getDemandante()!=null ? procedimiento.getDemandante():null);
      this.add("Expenses Incurred", procedimiento.getGastosIncurridos()!=null ? procedimiento.getGastosIncurridos():null);
      this.add("Qualifications", procedimiento.getHabilitaciones()!=null ? procedimiento.getHabilitaciones():null);
      this.add("Judicial Deliveries", procedimiento.getEntregasJudiciales()!=null ? procedimiento.getEntregasJudiciales():null);
      this.add("Respondents", procedimiento.getDemandados() !=null ? procedimiento.getDemandados().stream().map(Interviniente -> Interviniente.getNombre()+" "+Interviniente.getApellidos()).collect(Collectors.joining(", ")):null);
      this.add("Office", procedimiento.getDespacho()!=null ? procedimiento.getDespacho():null);
      this.add("Counsel", procedimiento.getLetrado()!=null ? procedimiento.getLetrado():null);
      this.add("Legal Telephone", procedimiento.getTelefonoLetrado()!=null ? procedimiento.getTelefonoLetrado():null);
      this.add("Legal2 Telephone", procedimiento.getTelefono2Letrado()!=null ? procedimiento.getTelefono2Letrado():null);
      this.add("email Legal", procedimiento.getEmailLetrado()!=null ? procedimiento.getEmailLetrado():null);
      this.add("Judicial Manager Haya", procedimiento.getGestorJudicialHaya()!=null ? procedimiento.getGestorJudicialHaya():null);
      this.add("Email Manager Haya", procedimiento.getEmailGestorJudicialHaya()!=null ? procedimiento.getEmailGestorJudicialHaya():null);
      this.add("Procurator", procedimiento.getProcurador()!=null ? procedimiento.getProcurador():null);
      this.add("Procurator Telephone", procedimiento.getTelefonoProcurador()!=null ? procedimiento.getTelefonoProcurador():null);
      this.add("Email Procurator", procedimiento.getEmailProcurador()!=null ? procedimiento.getEmailProcurador():null);
      this.add("Contrary Dispatch", procedimiento.getDespachoContrario()!=null ? procedimiento.getDespachoContrario():null);
      this.add("Opposing Counsel", procedimiento.getLetradoContrario()!=null ? procedimiento.getLetradoContrario():null);
      this.add("Opposing Counsel Telephone", procedimiento.getTelefonoLetradoContrario()!=null ? procedimiento.getTelefono2LetradoContrario():null);
      this.add("Opposing Counsel Telephone2",procedimiento.getTelefono2LetradoContrario()!=null ? procedimiento.getTelefono2LetradoContrario():null);
      this.add("Opposing Counsel Email", procedimiento.getLetradoContrario() != null ? procedimiento.getLetradoContrario() : null);
      this.add("Counter Procurator", procedimiento.getProcuradorContrario() != null ? procedimiento.getProcuradorContrario() : null);
      this.add("Telephone Counter Procurator", procedimiento.getTelefonoProcuradorContrario() != null ? procedimiento.getTelefonoProcuradorContrario() : null);
      this.add("Email Counter Procurator", procedimiento.getEmailProcuradorContrario() != null ? procedimiento.getEmailProcuradorContrario() : null);

    }

    else{

    this.add("Id Procedimiento", procedimiento.getId());
    this.add("Id Contrato", idContrato);
    this.add("Estado", Boolean.TRUE.equals(procedimiento.getActivo()) ? "ACTIVO":"INACTIVO");
    this.add("Numero Linea", null);
    this.add("Tipo Procedimiento", procedimiento.getTipo()!=null ? procedimiento.getTipo().getValor():null);
    this.add("Numero Autos", procedimiento.getNumeroAutos()!=null ? procedimiento.getNumeroAutos():null);
    this.add("Plaza", procedimiento.getPlaza()!=null ? procedimiento.getPlaza():null);
    this.add("Juzgado", procedimiento.getJuzgado()!=null ? procedimiento.getJuzgado():null);
    this.add("Provincia", procedimiento.getProvincia()!=null ? procedimiento.getProvincia().getValor():null);
    this.add("Importe Demanda", procedimiento.getImporteDemanda()!=null ? procedimiento.getImporteDemanda():null);
    this.add("Demandante", procedimiento.getDemandante()!=null ? procedimiento.getDemandante():null);
    this.add("Gastos Incurridos", procedimiento.getGastosIncurridos()!=null ? procedimiento.getGastosIncurridos():null);
    this.add("Habilitaciones", procedimiento.getHabilitaciones()!=null ? procedimiento.getHabilitaciones():null);
    this.add("Entregas Judiciales", procedimiento.getEntregasJudiciales()!=null ? procedimiento.getEntregasJudiciales():null);
    this.add("Demandados", procedimiento.getDemandados() !=null ? procedimiento.getDemandados().stream().map(Interviniente -> Interviniente.getNombre()+" "+Interviniente.getApellidos()).collect(Collectors.joining(", ")):null);
    this.add("Despacho", procedimiento.getDespacho()!=null ? procedimiento.getDespacho():null);
    this.add("Letrado", procedimiento.getLetrado()!=null ? procedimiento.getLetrado():null);
    this.add("Telefono Letrado", procedimiento.getTelefonoLetrado()!=null ? procedimiento.getTelefonoLetrado():null);
    this.add("Telefono2 Letrado", procedimiento.getTelefono2Letrado()!=null ? procedimiento.getTelefono2Letrado():null);
    this.add("Email Letrado", procedimiento.getEmailLetrado()!=null ? procedimiento.getEmailLetrado():null);
    this.add("Gestor Judicial Haya", procedimiento.getGestorJudicialHaya()!=null ? procedimiento.getGestorJudicialHaya():null);
    this.add("Email Gestor Haya", procedimiento.getEmailGestorJudicialHaya()!=null ? procedimiento.getEmailGestorJudicialHaya():null);
    this.add("Procurador", procedimiento.getProcurador()!=null ? procedimiento.getProcurador():null);
    this.add("Telefono Procurador", procedimiento.getTelefonoProcurador()!=null ? procedimiento.getTelefonoProcurador():null);
    this.add("Email Procurador", procedimiento.getEmailProcurador()!=null ? procedimiento.getEmailProcurador():null);
    this.add("Despacho Contrario", procedimiento.getDespachoContrario()!=null ? procedimiento.getDespachoContrario():null);
    this.add("Letrado Contrario", procedimiento.getLetradoContrario()!=null ? procedimiento.getLetradoContrario():null);
    this.add("Telefono Letrado Contrario", procedimiento.getTelefonoLetradoContrario()!=null ? procedimiento.getTelefono2LetradoContrario():null);
    this.add("Telefono2 Letrado Contrario",procedimiento.getTelefono2LetradoContrario()!=null ? procedimiento.getTelefono2LetradoContrario():null);
    this.add("Email Letrado Contrario", procedimiento.getLetradoContrario() != null ? procedimiento.getLetradoContrario() : null);
    this.add("Procurador Contrario", procedimiento.getProcuradorContrario() != null ? procedimiento.getProcuradorContrario() : null);
    this.add("Telefono Procurador Contrario", procedimiento.getTelefonoProcuradorContrario() != null ? procedimiento.getTelefonoProcuradorContrario() : null);
    this.add("Email Procurador Contrario", procedimiento.getEmailProcuradorContrario() != null ? procedimiento.getEmailProcuradorContrario() : null);
    }
  }


  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
