package com.haya.alaska.procedimiento.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_PROCEDIMIENTO_MONITORIO")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_PROCEDIMIENTO_MONITORIO")
public class ProcedimientoMonitorio extends Procedimiento {

  private static final long serialVersionUID = 1L;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PRESENTACION_DEMANDA")
  private Date fechaPresentacionDemanda;

  @Column(name = "NUM_PRINCIPAL_DEMANDA")
  private Double principalDemanda;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ADMISION_DEMANDA")
  private Date fechaAdmisionDemanda;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA")
  private Date fecha;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_OPOSICION")
  private Date fechaOposicion;

  @Column(name = "IND_RESULTADO")
  private Boolean resultado;

  public ProcedimientoMonitorio(Integer orden) {
    this.setOrden(orden);
  }

  @Override
  public String getHito() {
    if (this.resultado != null) {
      return "01";//"Resultado Comparecencia Oposición";
    }
    if (this.fechaOposicion != null) {
      return "02";//"Fecha Oposición";
    }
    if (this.fecha != null) {
      return "03";//"Fecha Requerimiento de Pago";
    }
    if (this.fechaAdmisionDemanda != null) {
      return "04";//"Fecha Admisión Demanda";
    }
    if (this.principalDemanda != null) {
      return "05";//"Importe Interposición Demanda";
    }
    if (this.fechaPresentacionDemanda != null) {
      return "06";//"Fecha Interposición Demanda";
    }
    return super.getHito();
  }
}
