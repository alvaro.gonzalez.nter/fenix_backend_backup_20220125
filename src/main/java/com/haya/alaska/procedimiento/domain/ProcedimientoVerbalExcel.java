package com.haya.alaska.procedimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ProcedimientoVerbalExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Procedure ID");
      cabeceras.add("Presentation date");
      cabeceras.add("Main demand");
      cabeceras.add("Admission date");
      cabeceras.add("Notification date");
      cabeceras.add("Notification result");
      cabeceras.add("Date celebration trial");
      cabeceras.add("Resolution");
    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Procedimiento");
      cabeceras.add("Fecha presentación");
      cabeceras.add("Principal demanda");
      cabeceras.add("Fecha admisión");
      cabeceras.add("Fecha notificación");
      cabeceras.add("Resultado notificación");
      cabeceras.add("Fecha celebración juicio");
      cabeceras.add("Resolución");
    }
  }

  public ProcedimientoVerbalExcel(ProcedimientoVerbal procedimiento, String idContrato,String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Procedure ID", procedimiento.getId());
      this.add("Presentation date", procedimiento.getFechaPresentacionDemanda());
      this.add("Main demand", procedimiento.getPrincipalDemanda());
      this.add("Admission date", procedimiento.getFechaAdmisionDemanda());
      this.add("Notification date", procedimiento.getFechaNotificacion());
      this.add("Notification result", procedimiento.getResultadoNotificacion());
      this.add("Date celebration trial", procedimiento.getFechaCelebracionJuicio());
      this.add("Resolution", procedimiento.getResolucion());

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Contrato", idContrato);
      this.add("Id Procedimiento", procedimiento.getId());
      this.add("Fecha presentación", procedimiento.getFechaPresentacionDemanda());
      this.add("Principal demanda", procedimiento.getPrincipalDemanda());
      this.add("Fecha admisión", procedimiento.getFechaAdmisionDemanda());
      this.add("Fecha notificación", procedimiento.getFechaNotificacion());
      this.add("Resultado notificación", procedimiento.getResultadoNotificacion());
      this.add("Fecha celebración Juicio", procedimiento.getFechaCelebracionJuicio());
      this.add("Resolución", procedimiento.getResolucion());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
