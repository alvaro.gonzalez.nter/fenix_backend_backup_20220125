package com.haya.alaska.procedimiento.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_PROCEDIMIENTO_VERBAL")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_PROCEDIMIENTO_VERBAL")
public class ProcedimientoVerbal extends Procedimiento {

  private static final long serialVersionUID = 1L;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PRESENTACION_DEMANDA")
  private Date fechaPresentacionDemanda;

  @Column(name = "NUM_PRINCIPAL_DEMANDA")
  private Double principalDemanda;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ADMISION_DEMANDA")
  private Date fechaAdmisionDemanda;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_NOTIFICACION")
  private Date fechaNotificacion;

  @Column(name = "IND_RESULTADO_NOTIFICACION")
  private Boolean resultadoNotificacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CELEBRACION_JUICIO")
  private Date fechaCelebracionJuicio;

  @Column(name = "IND_RESOLUCION")
  private Boolean resolucion;

  public ProcedimientoVerbal(Integer orden) {
    this.setOrden(orden);
  }

  @Override
  public String getHito() {
    if (this.resolucion != null) {
      return "01";//"Resolución";
    }
    if (this.fechaCelebracionJuicio != null) {
      return "02";//"Fecha Juicio";
    }
    if (this.resultadoNotificacion != null) {
      return "03";//"Resultado Notificación de la Demanda";
    }
    if (this.fechaNotificacion != null) {
      return "04";//"Fecha Notificación de la Demanda";
    }
    if (this.fechaAdmisionDemanda != null) {
      return "05";//"Fecha Admisión Demanda";
    }
    if (this.principalDemanda != null) {
      return "06";//"Importe Interposición Demanda";
    }
    if (this.fechaPresentacionDemanda != null) {
      return "07";//"Fecha Interposición Demanda";
    }
    return super.getHito();
  }
}
