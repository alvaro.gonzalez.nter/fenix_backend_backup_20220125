package com.haya.alaska.procedimiento.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_PROCEDIMIENTO_CONCURSAL")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_PROCEDIMIENTO_CONCURSAL")
public class ProcedimientoConcursal extends Procedimiento {

  private static final long serialVersionUID = 1L;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PUBLICACION")
  private Date fechaPublicacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_AUTO_CONCURSO")
  private Date fechaAutoDeclarandoConcurso;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_AUTO_FASE_CONVENIO")
  private Date fechaAutoFaseConvenio;

  @Column(name = "IND_RESULTADO_APROBACION_CONVENIO")
  private Boolean resultadoAprobacionConvenio;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_APERTURA")
  private Date fechaApertura;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_RESOLUCION")
  private Date fechaResolucion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_DECRETO")
  private Date fechaDecreto;

  public ProcedimientoConcursal(Integer orden) {
    this.setOrden(orden);
  }

  @Override
  public String getHito() {
    if (this.fechaDecreto != null) {
      return "01";//"Fecha decreto convocatoria subasta";
    }
    if (this.fechaResolucion != null) {
      return "02";//"Fecha aprobación plan liquidación";
    }
    if (this.fechaApertura != null) {
      return "03";//"Fecha apertura fase liquidación";
    }
    if (this.resultadoAprobacionConvenio != null) {
      return "04";//"Fecha aprobación convenio";
    }
    if (this.fechaAutoFaseConvenio != null) {
      return "05";//"Fecha apertura fase convenio";
    }
    if (this.fechaAutoDeclarandoConcurso != null) {
      return "06";//"Fecha Auto Concurso";
    }
    if (this.fechaPublicacion != null) {
      return "07";//"Fecha publicación BOE";
    }
    return super.getHito();
  }
}
