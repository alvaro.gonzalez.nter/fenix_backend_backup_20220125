package com.haya.alaska.procedimiento.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_PROCEDIMIENTO_ORDINARIO")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_PROCEDIMIENTO_ORDINARIO")
public class ProcedimientoOrdinario extends Procedimiento {

  private static final long serialVersionUID = 1L;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_PRESENTACION_DEMANDA")
  private Date fechaPresentacionDemanda;

  @Column(name = "NUM_PRINCIPAL_DEMANDA")
  private Double principalDemanda;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ADMISION_DEMANDA")
  private Date fechaAdmisionDemanda;

  @Column(name = "IND_RESULTADO_NOTIFICACION_REQU_PAGO")
  private Boolean resultadoNotificacionReqPago;

  @Column(name = "DES_DIRECCION_NOTIFICACION")
  private String direccionNotificacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_REQ_PAGO_DEUDOR")
  private Date fechaReqPagoDeudor;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_OPOSICION")
  private Date fechaOposicion;

  @Column(name = "IND_RESULTADO_OPOSICION")
  private Boolean resultadoOposicion;

  public ProcedimientoOrdinario(Integer orden) {
    this.setOrden(orden);
  }

  @Override
  public String getHito() {
    if (this.resultadoOposicion != null) {
      return "01";//"Resultado Comparecencia Oposición";
    }
    if (this.fechaOposicion != null) {
      return "02";//"Fecha Oposición";
    }
    if (this.fechaReqPagoDeudor != null) {
      return "03";//"Fecha Notificación de la Demanda.";
    }
    if (this.direccionNotificacion != null) {
      return "04";//"Domicilio Notificación Demanda";
    }
    if (this.resultadoNotificacionReqPago != null) {
      return "05";//"Resultado Notificación de la Demanda.";
    }
    if (this.fechaAdmisionDemanda != null) {
      return "06";//"Fecha Admisión Demanda";
    }
    if (this.principalDemanda != null) {
      return "07";//"Importe Interposición Demanda";
    }
    if (this.fechaPresentacionDemanda != null) {
      return "08";//"Fecha Interposición Demanda";
    }
    return super.getHito();
  }
}
