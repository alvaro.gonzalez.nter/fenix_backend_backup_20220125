package com.haya.alaska.procedimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ProcedimientoConcursalExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Procedure ID");
      cabeceras.add("Publication date");
      cabeceras.add("Date auto dispatching execution");
      cabeceras.add("Date of self phase agreement");
      cabeceras.add("Approval");
      cabeceras.add("Opening date");
      cabeceras.add("Resolution date");
      cabeceras.add("Decree date");


    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Procedimeinto");
      cabeceras.add("Fecha Publicacion");
      cabeceras.add("Fecha Auto Despachando Ejecucion");
      cabeceras.add("Fecha Auto Fase Convenio");
      cabeceras.add("Aprobacion");
      cabeceras.add("Fecha Apertura");
      cabeceras.add("Fecha Resolucion");
      cabeceras.add("Fecha Decreto");
    }
  }

  public ProcedimientoConcursalExcel(ProcedimientoConcursal procedimiento, String idContrato, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Procedure ID", procedimiento.getId());
      this.add("Publication date", procedimiento.getFechaPublicacion());
      this.add("Date auto dispatching execution", procedimiento.getFechaAutoDeclarandoConcurso());
      this.add("Date of self phase agreement", procedimiento.getFechaAutoFaseConvenio());
      this.add("Approval", procedimiento.getResultadoAprobacionConvenio());
      this.add("Opening date", procedimiento.getFechaApertura());
      this.add("Resolution date", procedimiento.getFechaResolucion());
      this.add("Decree date", procedimiento.getFechaDecreto());

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Contrato", idContrato);
      this.add("Id Procedimeinto", procedimiento.getId());
      this.add("Fecha Publicacion", procedimiento.getFechaPublicacion());
      this.add("Fecha Auto Despachando Ejecucion", procedimiento.getFechaAutoDeclarandoConcurso());
      this.add("Fecha Auto Fase Convenio", procedimiento.getFechaAutoFaseConvenio());
      this.add("Aprobacion", procedimiento.getResultadoAprobacionConvenio());
      this.add("Fecha Apertura", procedimiento.getFechaApertura());
      this.add("Fecha Resolucion", procedimiento.getFechaResolucion());
      this.add("Fecha Decreto", procedimiento.getFechaDecreto());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
