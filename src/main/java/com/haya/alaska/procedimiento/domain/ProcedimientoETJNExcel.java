package com.haya.alaska.procedimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ProcedimientoETJNExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Procedure ID");
      cabeceras.add("Demand filing date");
      cabeceras.add("Main demand");
      cabeceras.add("Date order dispatching");
      cabeceras.add("Notification date payment requirement");
      cabeceras.add("Address notified");
      cabeceras.add("Opposition date");
      cabeceras.add("Result");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Procedimiento");
      cabeceras.add("Fecha presentación demanda");
      cabeceras.add("Principal demanda");
      cabeceras.add("Fecha auto despachando");
      cabeceras.add("Fecha notificación req pago");
      cabeceras.add("Dirección notificación");
      cabeceras.add("Fecha oposición");
      cabeceras.add("Resultado");
    }
  }

  public ProcedimientoETJNExcel(ProcedimientoEtnj procedimiento, String idContrato,String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Procedure ID", procedimiento.getId());
      this.add("Demand filing date", procedimiento.getFechaPresentacionDemanda());
      this.add("Main demand", procedimiento.getPrincipalDemanda());
      this.add("Date order dispatching", procedimiento.getFechaAutoDespachando());
      this.add("Notification date payment requirement", procedimiento.getFechaNotificacionReqPago());
      this.add("Address notified", procedimiento.getDireccionNotificacion());
      this.add("Opposition date", procedimiento.getFechaOposicion());
      this.add("Result", procedimiento.getResultado());

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Contrato", idContrato);
      this.add("Id Procedimiento", procedimiento.getId());
      this.add("Fecha presentación demanda", procedimiento.getFechaPresentacionDemanda());
      this.add("Principal demanda", procedimiento.getPrincipalDemanda());
      this.add("Fecha auto despachando", procedimiento.getFechaAutoDespachando());
      this.add("Fecha notificación req pago", procedimiento.getFechaNotificacionReqPago());
      this.add("Dirección notificación", procedimiento.getDireccionNotificacion());
      this.add("Fecha oposición", procedimiento.getFechaOposicion());
      this.add("Resultado", procedimiento.getResultado());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
