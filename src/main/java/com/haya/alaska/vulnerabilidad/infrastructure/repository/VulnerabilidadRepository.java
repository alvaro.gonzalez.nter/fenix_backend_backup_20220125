package com.haya.alaska.vulnerabilidad.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.vulnerabilidad.domain.Vulnerabilidad;

@Repository
public interface VulnerabilidadRepository extends CatalogoRepository<Vulnerabilidad, Integer> {
	  
}
