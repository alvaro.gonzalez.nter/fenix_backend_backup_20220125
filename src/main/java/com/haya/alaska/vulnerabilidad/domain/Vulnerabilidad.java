package com.haya.alaska.vulnerabilidad.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.ocupante.domain.Ocupante;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_VULNERABILIDAD")
@Entity
@Table(name = "LKUP_VULNERABILIDAD")
public class Vulnerabilidad extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_VULNERABILIDAD")
  @NotAudited
  private Set<Interviniente> intervinientes = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_VULNERABILIDAD")
  @NotAudited
  private Set<Ocupante> ocupantes = new HashSet<>();
}
