package com.haya.alaska.reo_origen.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_REO_ORIGEN")
@Entity
@Table(name = "LKUP_REO_ORIGEN")
@NoArgsConstructor
public class ReoOrigen extends Catalogo {
    @OneToMany(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_REO_ORIGEN")
    @NotAudited
    private Set<Bien> bienes = new HashSet<>();
}
