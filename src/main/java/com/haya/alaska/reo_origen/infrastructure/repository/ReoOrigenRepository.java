package com.haya.alaska.reo_origen.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.reo_origen.domain.ReoOrigen;
import org.springframework.stereotype.Repository;

@Repository
public interface ReoOrigenRepository extends CatalogoRepository<ReoOrigen, Integer> {
}
