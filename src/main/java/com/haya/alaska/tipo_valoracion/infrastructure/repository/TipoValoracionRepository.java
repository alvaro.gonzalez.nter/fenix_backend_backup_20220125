package com.haya.alaska.tipo_valoracion.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_valoracion.domain.TipoValoracion;

@Repository
public interface TipoValoracionRepository extends CatalogoRepository<TipoValoracion, Integer> {
	
	}
