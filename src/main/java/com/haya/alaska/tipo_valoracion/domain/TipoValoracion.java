package com.haya.alaska.tipo_valoracion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.valoracion.domain.Valoracion;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_VALORACION")
@Entity
@Table(name = "LKUP_TIPO_VALORACION")
public class TipoValoracion extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_VALORACION")
  @NotAudited
  private Set<Valoracion> valoraciones = new HashSet<>();
}
