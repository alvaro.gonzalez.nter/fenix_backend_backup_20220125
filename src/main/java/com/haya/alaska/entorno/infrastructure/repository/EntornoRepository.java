package com.haya.alaska.entorno.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.entorno.domain.Entorno;

@Repository
public interface EntornoRepository extends CatalogoRepository<Entorno, Integer> {
	 
}
