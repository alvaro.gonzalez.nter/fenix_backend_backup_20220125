package com.haya.alaska.tasacion.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.tasacion.domain.Tasacion;

import java.util.List;

@Repository
public interface TasacionRepository extends JpaRepository<Tasacion, Integer> {
  List<Tasacion> findAllByBienIdOrderByFechaDesc(Integer idBien);
}
