package com.haya.alaska.tasacion.infrastructure.mapper;

import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.shared.dto.TasacionDTOToList;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.infrastructure.controller.dto.TasacionDto;
import com.haya.alaska.tipo_tasacion.infrastructure.repository.TipoTasacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class TasacionMapper {
  @Autowired
  private CatalogoMinInfoMapper catalogoMinInfoMapper;
  @Autowired
  TipoTasacionRepository tipoTasacionRepository;
  @Autowired
  LiquidezRepository liquidezRepository;

  public TasacionDTOToList tasacionToTasacionDTOToList(Tasacion tasacion) {
    TasacionDTOToList tasacionDTOToList = new TasacionDTOToList();
    tasacionDTOToList.setId(tasacion.getId());
    tasacionDTOToList.setTasadora(tasacion.getTasadora());
    tasacionDTOToList.setTipoTasacion(catalogoMinInfoMapper.entityToDto(tasacion.getTipoTasacion()));
    tasacionDTOToList.setImporte(tasacion.getImporte());
    tasacionDTOToList.setFecha(tasacion.getFecha());
    tasacionDTOToList.setLiquidez(catalogoMinInfoMapper.entityToDto(tasacion.getLiquidez()));

    return tasacionDTOToList;
  }

  public List<TasacionDTOToList> listTasacionToListTasacionDTOToList(Set<Tasacion> tasaciones) {
    List<TasacionDTOToList> tasacionDTOToLists = new ArrayList<>();
    for (Tasacion tasacion : tasaciones) {
      tasacionDTOToLists.add(tasacionToTasacionDTOToList(tasacion));
    }
    return tasacionDTOToLists;
  }

  public Tasacion dtoToEntity(TasacionDto tasacionDto, boolean rem) {
    Tasacion tasacion = new Tasacion();
    tasacion.setActivo(true);
    tasacion.setId(tasacionDto.getId());
    tasacion.setTasadora(tasacionDto.getTasadora());
    tasacion.setImporte(tasacionDto.getImporte());
    tasacion.setFecha(tasacionDto.getFecha());
    if (!rem) {
      tasacion.setTipoTasacion(tasacionDto.getTipoTasacion() != null ? tipoTasacionRepository.findById(Integer.valueOf(tasacionDto.getTipoTasacion())).orElse(null) : null);
      tasacion.setLiquidez(tasacionDto.getLiquidez() != null ? liquidezRepository.findById(Integer.valueOf(tasacionDto.getLiquidez())).orElse(null) : null);
    } else {
      tasacion.setTipoTasacion(tasacionDto.getTipoTasacion() != null ? tipoTasacionRepository.findByCodigo(tasacionDto.getTipoTasacion()).orElse(null) : null);
      tasacion.setLiquidez(tasacionDto.getLiquidez() != null ? liquidezRepository.findByCodigo(tasacionDto.getLiquidez()).orElse(null) : null);
    }

    return tasacion;
  }

  public TasacionDto entityToDto(Tasacion tasacion) {
    TasacionDto tasacionDto = new TasacionDto();
    if (tasacion != null) {
      tasacionDto.setId(tasacion.getId());
      tasacionDto.setTasadora(tasacion.getTasadora());
      if (tasacion.getTipoTasacion() != null) tasacionDto.setTipoTasacion(tasacion.getTipoTasacion().getId().toString());
      tasacionDto.setImporte(tasacion.getImporte());
      tasacionDto.setFecha(tasacion.getFecha());
      if (tasacion.getLiquidez() != null) tasacionDto.setLiquidez(tasacion.getLiquidez().getId().toString());
    }
    return tasacionDto;
  }
}
