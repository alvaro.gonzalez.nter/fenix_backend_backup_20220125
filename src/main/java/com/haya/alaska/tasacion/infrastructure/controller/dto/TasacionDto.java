package com.haya.alaska.tasacion.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class TasacionDto {

  private Integer id;
  private String tasadora;
  private String tipoTasacion;
  private Double importe;
  private Date fecha;
  private String liquidez;
}
