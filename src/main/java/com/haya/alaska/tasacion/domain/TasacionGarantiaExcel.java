package com.haya.alaska.tasacion.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class TasacionGarantiaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Warranty ID");
      cabeceras.add("Appraisal Amount");
      cabeceras.add("Appraisal Date");
      cabeceras.add("Appraiser");
      cabeceras.add("Type Appraisal");
      cabeceras.add("Liquidity");

    } else {

      cabeceras.add("Id Garantia");
      cabeceras.add("Importe Tasacion");
      cabeceras.add("Fecha Tasacion");
      cabeceras.add("Tasadora");
      cabeceras.add("Tipo Tasacion");
      cabeceras.add("Liquidez");
    }
  }

  public TasacionGarantiaExcel(Tasacion tasacion, Integer idBien) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Warranty ID", idBien);
      this.add("Appraisal Amount", tasacion.getImporte());
      this.add("Appraisal Date", tasacion.getFecha());
      this.add("Appraiser", tasacion.getTasadora());
      this.add("Type Appraisal", tasacion.getTipoTasacion());
      this.add("Liquidity", tasacion.getLiquidez());

    } else {

      this.add("Id Garantia", idBien);
      this.add("Importe Tasacion", tasacion.getImporte());
      this.add("Fecha Tasacion", tasacion.getFecha());
      this.add("Tasadora", tasacion.getTasadora());
      this.add("Tipo Tasacion", tasacion.getTipoTasacion());
      this.add("Liquidez", tasacion.getLiquidez());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
