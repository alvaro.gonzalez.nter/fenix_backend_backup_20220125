package com.haya.alaska.tasacion.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class TasacionExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Warranty ID/Solvency");
      cabeceras.add("Appraisal Amount");
      cabeceras.add("Appraisal Date");
      cabeceras.add("Appraiser");
      cabeceras.add("Type Appraisal");
      cabeceras.add("Liquidity");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Garantia/Solvencia");
      cabeceras.add("Importe Tasación");
      cabeceras.add("Fecha Tasación");
      cabeceras.add("Tasadora");
      cabeceras.add("Tipo Tasación");
      cabeceras.add("Liquidez");
    }
  }

  public TasacionExcel(Tasacion tasacion, String idContrato, Integer idBien, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Warranty ID/Solvency", idBien);
      this.add("Appraisal Amount", tasacion.getImporte());
      this.add("Appraisal Date", tasacion.getFecha());
      this.add("Appraiser", tasacion.getTasadora());
      this.add(
        "Type Appraisal",
        tasacion.getTipoTasacion() != null ? tasacion.getTipoTasacion().getValorIngles() : null);
      this.add(
        "Liquidity", tasacion.getLiquidez() != null ? tasacion.getLiquidez().getValorIngles() : null);

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Contrato", idContrato);
      this.add("Id Garantia/Solvencia", idBien);
      this.add("Importe Tasación", tasacion.getImporte());
      this.add("Fecha Tasación", tasacion.getFecha());
      this.add("Tasadora", tasacion.getTasadora());
      this.add(
          "Tipo Tasación",
          tasacion.getTipoTasacion() != null ? tasacion.getTipoTasacion().getValor() : null);
      this.add(
          "Liquidez", tasacion.getLiquidez() != null ? tasacion.getLiquidez().getValor() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
