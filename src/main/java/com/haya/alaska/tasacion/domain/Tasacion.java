package com.haya.alaska.tasacion.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.tipo_tasacion.domain.TipoTasacion;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_TASACION")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_TASACION")
public class Tasacion implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  private Bien bien;

  private String tasadora;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_TASACION")
  private TipoTasacion tipoTasacion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LIQUIDEZ")
  private Liquidez liquidez;

  @Column(name = "NUM_IMPORTE")
  private Double importe;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA")
  private Date fecha;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  public long getFechaNull() {
    if (this.fecha == null) return 0;
    return this.fecha.getTime();
  }


  @Override
  public boolean equals(Object o) {
    Tasacion tasacion2 = (Tasacion) o;
    if (this.getBien().getIdCarga().equals(tasacion2.getBien().getIdCarga())
      && this.getTasadora().equals(tasacion2.getTasadora())
      && this.getFechaNull() == tasacion2.getFechaNull()
      && Double.compare(this.getImporte(), tasacion2.getImporte()) == 0) {
      return true;
    }
    return false;
  }
}
