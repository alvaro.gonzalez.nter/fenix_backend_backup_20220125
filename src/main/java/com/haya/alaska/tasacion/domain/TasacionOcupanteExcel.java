package com.haya.alaska.tasacion.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class TasacionOcupanteExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Warranty ID/Solvency");
      cabeceras.add("Appraisal Amount");
      cabeceras.add("Appraisal Date");
      cabeceras.add("Appraiser");
      cabeceras.add("Type Appraisal");
      cabeceras.add("Liquidity");

    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Garantia/Solvencia");
      cabeceras.add("Importe Tasación");
      cabeceras.add("Fecha Tasación");
      cabeceras.add("Tasadora");
      cabeceras.add("Tipo Tasación");
      cabeceras.add("Liquidez");
    }
  }

  public TasacionOcupanteExcel(Tasacion tasacion, String idExpediente) {
    super();
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Warranty ID/Solvency", tasacion.getBien().getIdHaya() != null ? tasacion.getBien().getIdHaya() : null);
      this.add("Appraisal Amount", tasacion.getImporte());
      this.add("Fecha Tasación", tasacion.getFecha());
      this.add("Tasadora", tasacion.getTasadora());
      this.add("Appraisal Date", tasacion.getTipoTasacion() != null ? tasacion.getTipoTasacion().getValorIngles() : null);
      this.add("Appraiser", tasacion.getLiquidez() != null ? tasacion.getLiquidez().getValorIngles() : null);


    }

    else{

    this.add("Id Expediente", idExpediente);
    this.add("Id Garantia/Solvencia", tasacion.getBien().getIdHaya() != null ? tasacion.getBien().getIdHaya() : null);
    this.add("Importe Tasación", tasacion.getImporte());
    this.add("Fecha Tasación", tasacion.getFecha());
    this.add("Tasadora", tasacion.getTasadora());
    this.add("Tipo Tasación", tasacion.getTipoTasacion() != null ? tasacion.getTipoTasacion().getValor() : null);
    this.add("Liquidez", tasacion.getLiquidez() != null ? tasacion.getLiquidez().getValor() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
