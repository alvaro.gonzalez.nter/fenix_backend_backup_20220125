package com.haya.alaska.documentacion_validada.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.documentacion_validada.domain.DocumentacionValidada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentacionValidadaRepository extends CatalogoRepository<DocumentacionValidada, Integer> {
}

