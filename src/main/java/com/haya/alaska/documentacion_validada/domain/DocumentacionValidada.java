package com.haya.alaska.documentacion_validada.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Audited
@AuditTable(value = "HIST_LKUP_DOCUMENTACION_VALIDADA")
@Entity
@Table(name = "LKUP_DOCUMENTACION_VALIDADA")
@Getter
public class DocumentacionValidada extends Catalogo {
}
