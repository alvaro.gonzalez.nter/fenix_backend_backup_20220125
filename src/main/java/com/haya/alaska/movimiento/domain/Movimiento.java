package com.haya.alaska.movimiento.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.descripcion_movimiento.domain.DescripcionMovimiento;
import com.haya.alaska.tipo_movimiento.domain.TipoMovimiento;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_MOVIMIENTO")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_MOVIMIENTO")
public class Movimiento implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private Contrato contrato;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_MOVIMIENTO")
  private TipoMovimiento tipo;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_DESCRIPCION_MOVIMIENTO")
  private DescripcionMovimiento descripcion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_VALOR")
  private Date fechaValor;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CONTABLE")
  private Date fechaContable;

  @Column(name = "NUM_IMPORTE")
  private Double importe;

  @Column(name = "NUM_PRINCIPAL_CAPITAL")
  private Double principalCapital;

  @Column(name = "NUM_INTERESES_ORDINARIOS")
  private Double interesesOrdinarios;

  @Column(name = "NUM_INTERESES_DEMORA")
  private Double interesesDemora;

  @Column(name = "NUM_COMISIONES")
  private Double comisiones;

  @Column(name = "NUM_GASTOS")
  private Double gastos;

  @Column(name = "NUM_IMPUESTOS")
  private Double impuestos;

  @Column(name = "NUM_PRINCIPAL_PENDIENTE_VENCER")
  private Double principalPendienteVencer;

  @Column(name = "NUM_DEUDA_IMPAGADA")
  private Double deudaImpagada;

  @Column(name = "DES_USUARIO")
  private String usuario;

  @Column(name = "NUM_IMPORTE_RECUPERADO")
  private Double importeRecuperado;

  @Column(name = "NUM_SALIDA_DUDOSO")
  private Double salidaDudoso; // TODO: por determinar que es

  @Column(name = "NUM_IMPORTE_FACTURABLE")
  private Double importeFacturable; // TODO: por determinar que es

  @Column(name = "DES_NUM_FACTURA")
  private String numeroFactura;

  @Column(name = "DES_DETALLE_GASTO")
  private String detalleGasto;

  @Column(name = "IND_AFECTA")
  private Boolean afecta;

  @Column(name = "IND_ABONADO", nullable = false, columnDefinition = "boolean default false")
  private Boolean abonado = false;

  @Column(name = "DES_SENTIDO")
  private String sentido;

  @Column(name = "DES_COMENTARIOS")
  private String comentarios;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @Column(name = "IND_CONTABILIDAD", nullable = false, columnDefinition = "boolean default false")
  private Boolean contabilidad = false;
  /* Campo de uso interno para la gestión de la carga */
  @Column(name = "DES_ID_CARGA")
  private String idCarga;

  @Column(name = "IND_ORIGEN_WEB", nullable = false, columnDefinition = "boolean default false")
  private Boolean origenWeb = false;
}
