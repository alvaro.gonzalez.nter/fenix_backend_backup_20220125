package com.haya.alaska.movimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MovimientoContabilidadExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Customer");
      cabeceras.add("Portfolio");
      cabeceras.add("Loan");
      cabeceras.add("Movement type");
      cabeceras.add("Description");
      cabeceras.add("Value date");
      cabeceras.add("Accounting date");
      cabeceras.add("Sense");
      cabeceras.add("Import");
      cabeceras.add("Principal");
      cabeceras.add("Ordinary interest");
      cabeceras.add("Interest on late payments");
      cabeceras.add("Commissions");
      cabeceras.add("Expenses");
      cabeceras.add("User");
      cabeceras.add("Amount recovered");
      cabeceras.add("Questionable output");
      cabeceras.add("Billable amount");
      cabeceras.add("Movement ID");
      cabeceras.add("Affects");
      cabeceras.add("Status");
      cabeceras.add("Comments");

    } else {

      cabeceras.add("Cliente");
      cabeceras.add("Cartera");
      cabeceras.add("Contrato");
      cabeceras.add("Tipo Movimiento");
      cabeceras.add("Descripción");
      cabeceras.add("Fecha Valor");
      cabeceras.add("Fecha Contable");
      cabeceras.add("Sentido");
      cabeceras.add("Importe");
      cabeceras.add("Principal");
      cabeceras.add("Intereses Ordinarios");
      cabeceras.add("Intereses Demora");
      cabeceras.add("Comisiones");
      cabeceras.add("Gastos");
      cabeceras.add("Usuario");
      cabeceras.add("Importe Recuperado");
      cabeceras.add("Salida Dudoso");
      cabeceras.add("Importe Facturable");
      cabeceras.add("Id Movimiento");
      cabeceras.add("Afecta");
      cabeceras.add("Estado");
      cabeceras.add("Comentarios");
    }
  }

  public MovimientoContabilidadExcel(Movimiento movimiento) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Customer", "");
      this.add("Portfolio", movimiento.getCartera() != null ? movimiento.getCartera().getId() : null);
      this.add(
        "Loan",
        movimiento.getContrato() != null ? movimiento.getContrato().getIdCarga() : null);
      this.add(
        "Movement type", movimiento.getTipo() != null ? movimiento.getTipo().getValorIngles() : null);
      this.add(
        "Description",
        movimiento.getDescripcion() != null ? movimiento.getDescripcion().getValorIngles() : null);
      this.add("Value date", movimiento.getFechaValor());
      this.add("Accounting date", movimiento.getFechaContable());
      this.add("Sense", movimiento.getSentido());
      this.add("Import", movimiento.getImporte());
      this.add("Principal", movimiento.getPrincipalCapital());
      this.add("Ordinary interest", movimiento.getInteresesOrdinarios());
      this.add("Interest on late payments", movimiento.getInteresesDemora());
      this.add("Commissions", movimiento.getComisiones());
      this.add("Expenses", movimiento.getGastos());
      this.add("User", movimiento.getUsuario());
      this.add("Amount recovered", movimiento.getImporteRecuperado());
      this.add("Questionable output", movimiento.getSalidaDudoso());
      this.add("Billable amount", movimiento.getImporteFacturable());
      this.add("Movement ID", movimiento.getId());
      this.add("Affects", Boolean.TRUE.equals(movimiento.getAfecta()) ? "YES" : "NO");
      this.add("Status", Boolean.TRUE.equals(movimiento.getActivo()) ? "YES" : "NO");
      this.add("Comments", movimiento.getComentarios());

    } else {

      this.add("Cliente", "");
      this.add("Cartera", movimiento.getCartera() != null ? movimiento.getCartera().getId() : null);
      this.add(
          "Contrato",
          movimiento.getContrato() != null ? movimiento.getContrato().getIdCarga() : null);
      this.add(
          "Tipo Movimiento", movimiento.getTipo() != null ? movimiento.getTipo().getValor() : null);
      this.add(
          "Descripción",
          movimiento.getDescripcion() != null ? movimiento.getDescripcion().getValor() : null);
      this.add("Fecha Valor", movimiento.getFechaValor());
      this.add("Fecha Contable", movimiento.getFechaContable());
      this.add("Sentido", movimiento.getSentido());
      this.add("Importe", movimiento.getImporte());
      this.add("Principal", movimiento.getPrincipalCapital());
      this.add("Intereses Ordinarios", movimiento.getInteresesOrdinarios());
      this.add("Intereses Demora", movimiento.getInteresesDemora());
      this.add("Comisiones", movimiento.getComisiones());
      this.add("Gastos", movimiento.getGastos());
      this.add("Usuario", movimiento.getUsuario());
      this.add("Importe Recuperado", movimiento.getImporteRecuperado());
      this.add("Salida Dudoso", movimiento.getSalidaDudoso());
      this.add("Importe Facturable", movimiento.getImporteFacturable());
      this.add("Id Movimiento", movimiento.getId());
      this.add("Afecta", Boolean.TRUE.equals(movimiento.getAfecta()) ? "SI" : "NO");
      this.add("Estado", Boolean.TRUE.equals(movimiento.getActivo()) ? "SI" : "NO");
      this.add("Comentarios", movimiento.getComentarios());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
