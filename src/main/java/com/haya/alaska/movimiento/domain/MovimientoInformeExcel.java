package com.haya.alaska.movimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MovimientoInformeExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Movement ID");
      cabeceras.add("Movement type");
      cabeceras.add("Date value");
      cabeceras.add("Accounting date");

    } else {

      cabeceras.add("ID Movimiento");
      cabeceras.add("Tipo Movimiento");
      cabeceras.add("Fecha Valor");
      cabeceras.add("Fecha Contable");
    }
  }

  public MovimientoInformeExcel(Movimiento movimiento) {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Movement ID", movimiento.getId());
      this.add("Movement type", movimiento.getTipo().getValorIngles());
      this.add("Date value", movimiento.getFechaValor());
      this.add("Accounting date", movimiento.getFechaContable());


    } else {

      this.add("Id Movimiento", movimiento.getId());
      this.add("Tipo Movimiento", movimiento.getTipo().getValor());
      this.add("Fecha Valor", movimiento.getFechaValor());
      this.add("Fecha Contable", movimiento.getFechaContable());
    }
  }


  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
