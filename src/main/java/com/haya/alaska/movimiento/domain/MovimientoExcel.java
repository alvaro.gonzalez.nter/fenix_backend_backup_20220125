package com.haya.alaska.movimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MovimientoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Portfolio ID");
      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Type");
      cabeceras.add("Description");
      cabeceras.add("Date Value");
      cabeceras.add("Accounting date");
      cabeceras.add("Sense");
      cabeceras.add("Import");
      cabeceras.add("Principal Capital");
      cabeceras.add("Ordinary Interest");
      cabeceras.add("Interest on late payments");
      cabeceras.add("Commissions");
      cabeceras.add("Expenses");
      cabeceras.add("Taxes");
      cabeceras.add("Principal Pending Overcoming");
      cabeceras.add("Unpaid debt");
      cabeceras.add("Balance under management");
      cabeceras.add("User");
      cabeceras.add("Amount recovered");
      cabeceras.add("Questionable output");
      cabeceras.add("Billable amount");
      cabeceras.add("Invoice number");
      cabeceras.add("Expense detail");
      cabeceras.add("Movement ID");
      cabeceras.add("Affects");
      cabeceras.add("Status");
      cabeceras.add("Comments");

    } else {

      cabeceras.add("Id Cartera");
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Tipo");
      cabeceras.add("Descripcion");
      cabeceras.add("Fecha Valor");
      cabeceras.add("Fecha Contable");
      cabeceras.add("Sentido");
      cabeceras.add("Importe");
      cabeceras.add("Principal Capital");
      cabeceras.add("Intereses Ordinarios");
      cabeceras.add("Intereses Demora");
      cabeceras.add("Comisiones");
      cabeceras.add("Gastos");
      cabeceras.add("Impuestos");
      cabeceras.add("Principal Pendiente Vencer");
      cabeceras.add("Deuda Impagada");
      cabeceras.add("Saldo en Gestión");
      cabeceras.add("Usuario");
      cabeceras.add("Importe Recuperado");
      cabeceras.add("Salida Dudoso");
      cabeceras.add("Importe Facturable");
      cabeceras.add("Numero Factura");
      cabeceras.add("Detalle Gasto");
      cabeceras.add("Id Movimiento");
      cabeceras.add("Afecta");
      cabeceras.add("Estado");
      cabeceras.add("Comentarios");
    }
  }

  public MovimientoExcel(Movimiento movimiento) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Portfolio ID", movimiento.getContrato().getExpediente().getIdConcatenado());
      this.add("Connection ID", movimiento.getContrato().getExpediente().getCartera().getNombre());
      this.add("Loan ID", movimiento.getContrato().getIdCarga());
      this.add("Type", movimiento.getTipo() != null ? movimiento.getTipo().getValorIngles() : null);
      this.add(
        "Description",
        movimiento.getDescripcion() != null ? movimiento.getDescripcion().getValorIngles() : null);
      this.add("Date Value", movimiento.getFechaValor());
      this.add("Accounting date", movimiento.getFechaContable());
      this.add("Sense", movimiento.getSentido());
      this.add("Import", movimiento.getImporte());
      this.add("Principal Capital", movimiento.getPrincipalCapital());
      this.add("Ordinary Interest", movimiento.getInteresesOrdinarios());
      this.add("Interest on late payments", movimiento.getInteresesDemora());
      this.add("Commissions", movimiento.getComisiones());
      this.add("Expenses", movimiento.getGastos());
      this.add("Taxes", movimiento.getImpuestos());
      this.add("Principal Pending Overcoming", movimiento.getPrincipalPendienteVencer());
      this.add("Unpaid debt", movimiento.getDeudaImpagada());
      this.add("Balance under management", movimiento.getContrato().getSaldoGestion());
      this.add("User", movimiento.getUsuario());
      this.add("Amount recovered", movimiento.getImporteRecuperado());
      this.add("Questionable output", movimiento.getSalidaDudoso());
      this.add("Billable amount", movimiento.getImporteFacturable());
      this.add("Invoice number", movimiento.getNumeroFactura());
      this.add("Expense detail", movimiento.getDetalleGasto());
      this.add("Movement ID", movimiento.getId());
      this.add("Affects", movimiento.getAfecta());
      this.add("Status", Boolean.TRUE.equals(movimiento.getActivo()) ? "YES" : "NO");
      this.add("Comments", movimiento.getComentarios());

    } else {

      this.add("Id Expediente", movimiento.getContrato().getExpediente().getIdConcatenado());
      this.add("Id Cartera", movimiento.getContrato().getExpediente().getCartera().getNombre());
      this.add("Id Contrato", movimiento.getContrato().getIdCarga());
      this.add("Tipo", movimiento.getTipo() != null ? movimiento.getTipo().getValor() : null);
      this.add(
          "Descripcion",
          movimiento.getDescripcion() != null ? movimiento.getDescripcion().getValor() : null);
      this.add("Fecha Valor", movimiento.getFechaValor());
      this.add("Fecha Contable", movimiento.getFechaContable());
      this.add("Sentido", movimiento.getSentido());
      this.add("Importe", movimiento.getImporte());
      this.add("Principal Capital", movimiento.getPrincipalCapital());
      this.add("Intereses Ordinarios", movimiento.getInteresesOrdinarios());
      this.add("Intereses Demora", movimiento.getInteresesDemora());
      this.add("Comisiones", movimiento.getComisiones());
      this.add("Gastos", movimiento.getGastos());
      this.add("Impuestos", movimiento.getImpuestos());
      this.add("Principal Pendiente Vencer", movimiento.getPrincipalPendienteVencer());
      this.add("Deuda Impagada", movimiento.getDeudaImpagada());
      this.add("Saldo en Gestión", movimiento.getContrato().getSaldoGestion());
      this.add("Usuario", movimiento.getUsuario());
      this.add("Importe Recuperado", movimiento.getImporteRecuperado());
      this.add("Salida Dudoso", movimiento.getSalidaDudoso());
      this.add("Importe Facturable", movimiento.getImporteFacturable());
      this.add("Numero Factura", movimiento.getNumeroFactura());
      this.add("Detalle Gasto", movimiento.getDetalleGasto());
      this.add("Id Movimiento", movimiento.getId());
      this.add("Afecta", movimiento.getAfecta());
      this.add("Estado", Boolean.TRUE.equals(movimiento.getActivo()) ? "SI" : "NO");
      this.add("Comentarios", movimiento.getComentarios());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
