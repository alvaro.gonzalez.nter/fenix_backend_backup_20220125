package com.haya.alaska.movimiento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class MovimientoNeteoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Movement ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Date value");
      cabeceras.add("Movement type");
      cabeceras.add("Import");
      cabeceras.add("Principal");
      cabeceras.add("Ordinary interest");
      cabeceras.add("Interest on late payments");
      cabeceras.add("Expense commissions");
      cabeceras.add("Status");

    } else {

      cabeceras.add("Id movimiento");
      cabeceras.add("Id contrato");
      cabeceras.add("Fecha valor");
      cabeceras.add("Tipo de movimiento");
      cabeceras.add("Importe");
      cabeceras.add("Principal");
      cabeceras.add("Intereses ordinarios");
      cabeceras.add("Intereses de demora");
      cabeceras.add("Comisiones gastos");
      cabeceras.add("Estado");
    }
  }

  public MovimientoNeteoExcel(Movimiento movimiento) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Movement ID", movimiento.getId());
      this.add(
        "Loan ID",
        movimiento.getContrato() != null ? movimiento.getContrato().getIdCarga() : null);
      this.add("Date value", movimiento.getFechaValor());
      this.add("Movement type", movimiento.getTipo());
      this.add("Import", movimiento.getImporte());
      this.add("Principal", movimiento.getPrincipalCapital());
      this.add("Ordinary interest", movimiento.getInteresesOrdinarios());
      this.add("Interest on late payments", movimiento.getInteresesDemora());
      this.add("Expense commissions", movimiento.getComisiones());
      this.add("Status", movimiento.getActivo());

    } else {

      this.add("Id movimiento", movimiento.getId());
      this.add(
          "Id contrato",
          movimiento.getContrato() != null ? movimiento.getContrato().getIdCarga() : null);
      this.add("Fecha valor", movimiento.getFechaValor());
      this.add("Tipo de movimiento", movimiento.getTipo());
      this.add("Importe", movimiento.getImporte());
      this.add("Principal", movimiento.getPrincipalCapital());
      this.add("Intereses ordinarios", movimiento.getInteresesOrdinarios());
      this.add("Intereses de demora", movimiento.getInteresesDemora());
      this.add("Comisiones gastos", movimiento.getComisiones());
      this.add("Estado", movimiento.getActivo());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
