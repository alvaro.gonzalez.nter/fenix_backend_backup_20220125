package com.haya.alaska.movimiento.application;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.descripcion_movimiento.domain.DescripcionMovimiento_;
import com.haya.alaska.descripcion_movimiento.infrastructure.repository.DescripcionMovimientoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.movimiento.domain.*;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoInputDto;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoOutputDto;
import com.haya.alaska.movimiento.infrastructure.mapper.MovimientoMapper;
import com.haya.alaska.movimiento.infrastructure.repository.MovimientoRepository;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.infrastructure.controller.dto.SaldoDto;
import com.haya.alaska.saldo.infrastructure.repository.SaldoRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MovimientoDTOToList;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.tipo_movimiento.domain.TipoMovimiento_;
import com.haya.alaska.tipo_movimiento.infrastructure.repository.TipoMovimientoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.*;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Component
public class MovimientoUseCaseImpl implements MovimientoUseCase {

  @PersistenceContext
  private EntityManager entityManager;

  @Autowired
  SaldoRepository saldoRepository;
  @Autowired
  TipoMovimientoRepository tipoMovimientoRepository;
  @Autowired
  DescripcionMovimientoRepository descripcionMovimientoRepository;
  @Autowired
  MovimientoRepository movimientoRepository;
  @Autowired
  ContratoRepository contratoRepository;
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  MovimientoMapper movimientoMapper;

  @Override
  public List<Movimiento> findAllByIdExpedienteFechaValorBetween(Integer idExpediente, Date fechaInicio, Date fechaFin) {
    return movimientoRepository.findAllByIdExpedienteFechaValorBetween(idExpediente, fechaInicio, fechaFin);
  }

  @Override
  public ListWithCountDTO<SaldoDto> getSaldosDTOByIdContrato(Integer idContrato, Integer page, Integer size) {
    Page<Saldo> saldoPage;
    saldoPage = saldoRepository.findAllByContratoIdOrderByDiaDesc(idContrato, PageRequest.of(page, size));

    List<Saldo> saldos = saldoPage.getContent();

    List<SaldoDto> saldosDTO = new ArrayList<>();
    for (Saldo saldo : saldos) {
      saldosDTO.add(new SaldoDto(saldo));
    }

    int totalRegistros = saldoRepository.countByContratoId(idContrato);
    return new ListWithCountDTO<>(saldosDTO, totalRegistros);
  }

  @Override
  public MovimientoOutputDto findMovimientoDTOById(Integer idMovimiento) throws IllegalAccessException {
    Movimiento movimiento = movimientoRepository.findById(idMovimiento).orElse(null);

    return movimientoMapper.entityToOutputDto(movimiento);
  }

  @Override
  public void createContabilidad(Integer idCartera, Boolean contratos, MovimientoInputDto movimientoInputDto, Usuario usuario) throws Exception {
    movimientoInputDto.setAfecta(false);
    if (contratos){
      Cartera cartera = carteraRepository.findById(idCartera).orElseThrow(
        () -> new com.haya.alaska.shared.exceptions.NotFoundException("cartera", idCartera));
      if (cartera.getAdministrada()) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new ForbiddenException("Wallet " + cartera.getNombre() + " is managed. You cannot do contract level accounting for managed portfolios.");
        else throw new ForbiddenException("La cartera " + cartera.getNombre() + " es administrada. No se puede hacer contabilidad a nivel contrato de carteras administradas.");
      }
      if (cartera.getExpedientes().isEmpty()){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("The portfolio" + cartera.getNombre() + "is a portfolio of Negotiated Possession. It has no contracts.");
        else throw new NotFoundException(
                "La cartera " + cartera.getNombre() + " es una cartera de Posesión Negociada. No tiene contratos.");
      }
      List<Contrato> contratoList = getContratos(cartera);
      for (Contrato c : contratoList){
        movimientoRepository.save(movimientoMapper.inputDtoToEntity(movimientoInputDto, c, cartera, true, usuario));
      }
    }
    else
      createCartera(idCartera, true, movimientoInputDto, usuario);
    //return result;
  }

  private List<Contrato> getContratos(Cartera cartera){
    List<Expediente> expedienteList = new ArrayList<>(cartera.getExpedientes());
    List<Contrato> contratoList = new ArrayList<>();
    for (Expediente ex : expedienteList){
      contratoList.addAll(ex.getContratos());
    }
    return contratoList;
  }

  @Override
  public MovimientoOutputDto create(Integer idContrato, Integer idCartera, Boolean contabilidad, MovimientoInputDto movimientoInputDto, Usuario usuario) throws Exception {

    if (idContrato != null) return createContrato(idContrato, contabilidad, movimientoInputDto, usuario);
    return createCartera(idCartera, contabilidad, movimientoInputDto, usuario);
  }

  private MovimientoOutputDto createContrato(Integer idContrato, Boolean contabilidad, MovimientoInputDto movimientoDTO, Usuario usuario) throws Exception {
    Contrato contrato = contratoRepository.findById(idContrato).orElseThrow(() -> new NotFoundException("Contrato", idContrato));
    Expediente expediente = contrato.getExpediente();
    Cartera cartera = expediente.getCartera();

    Movimiento movimiento = movimientoRepository.save(movimientoMapper.inputDtoToEntity(movimientoDTO, contrato, cartera, contabilidad, usuario));

    // actualizamos saldo
    List<Movimiento> movimientos = new ArrayList<>();
    movimientos.add(movimiento);
    if (!contabilidad)
      updateSaldoConFechaAnterior(movimientos, idContrato);
    return movimientoMapper.entityToOutputDto(movimiento);
  }

  private MovimientoOutputDto createCartera(Integer idCartera, Boolean contabilidad, MovimientoInputDto movimientoDTO, Usuario usuario) throws Exception {
    Cartera cartera = carteraRepository.findById(idCartera).orElse(null);
    Movimiento movimiento = movimientoRepository.save(movimientoMapper.inputDtoToEntity(movimientoDTO, null, cartera, contabilidad, usuario));

    // actualizamos saldo
    List<Movimiento> movimientos = new ArrayList<>();
    movimientos.add(movimiento);
    return movimientoMapper.entityToOutputDto(movimiento);
  }

  @Override
  public MovimientoOutputDto update(Integer idMovimiento, Boolean contabilidad, MovimientoInputDto movimientoDTO) throws Exception {
    Movimiento movimiento = movimientoRepository.findById(idMovimiento).orElse(null);
    if (movimiento == null) throw new NotFoundException("Movimiento", idMovimiento);
    Movimiento movimientoAux = movimientoMapper.inputDtoToEntity(movimientoDTO, movimiento.getContrato(), movimiento.getCartera(), movimiento.getUsuario());

    // si un movimiento está activo y el movimiento ha cambiado a no afecta o se ha dado de baja,
    // recalculamos el saldo restando los valores de este movimiento
    if (movimiento.getActivo() && movimiento.getAfecta()) {
      if (movimiento.getSentido().equals("-"))
        movimiento.setSentido("+");
      else
        movimiento.setSentido("-");
      List<Movimiento> movimientosEx = new ArrayList<>();
      movimientosEx.add(movimiento);
      if (movimientoAux.getContrato() != null && !contabilidad)
        updateSaldoConFechaAnterior(movimientosEx, movimientoAux.getContrato().getId());
    }
    Movimiento movimientoUpdated = movimientoRepository.save(movimientoMapper.updateFields(movimiento, movimientoDTO));
    // actualizamos saldo
    // utilizamos un movimiento auxiliar para calcular el saldo
    if (movimientoAux.getActivo() && movimientoAux.getAfecta()) {
      List<Movimiento> movimientos = new ArrayList<>();
      movimientos.add(movimientoAux);
      if (movimientoUpdated.getContrato() != null && !contabilidad)
        updateSaldoConFechaAnterior(movimientos, movimientoUpdated.getContrato().getId());
    }

    return movimientoMapper.entityToOutputDto(movimientoUpdated);
  }

  @Override
  public void delete(Integer idMovimiento, Boolean contabilidad) throws Exception {
    Movimiento movimiento = movimientoRepository.findById(idMovimiento).orElseThrow(() -> new Exception("Movimiento no encontrado con el id" + idMovimiento));
    movimiento.setActivo(false);
    Movimiento movimientoBorrado = movimientoRepository.save(movimiento);
    cambiarSentido(movimientoBorrado);
    List<Movimiento> movimientos = new ArrayList<>();
    movimientos.add(movimientoBorrado);
    if (movimientoBorrado.getContrato() != null && !contabilidad)
      updateSaldoConFechaAnterior(movimientos, movimientoBorrado.getContrato().getId());
  }

  @Override
  public ListWithCountDTO<MovimientoDTOToList> getAllFilteredMovimientos(
    String contratoCarga, Integer idContrato, Integer idCartera, Boolean contabilidad, Integer id, String fechaContable, String fechaValor, String tipoMovimiento, String descripcion,
    String importe, String principal, String interesesOrdinarios, String interesesDemora, String comisiones,
    String gastos, Boolean afecta, Boolean estado, String orderField, String orderDirection, Integer size,
    Integer page) throws com.haya.alaska.shared.exceptions.NotFoundException {

    List<Movimiento> movimientos = filterMovimientosInMemory(contratoCarga, idContrato, idCartera, contabilidad,
      id, fechaContable, fechaValor, tipoMovimiento, descripcion, importe, principal,
      interesesOrdinarios, interesesDemora, comisiones, gastos, afecta, estado, orderField, orderDirection, page, size);

    //List<Movimiento> movimientos = movimientosSinPaginar.stream().skip(size * page).limit(size).collect(Collectors.toList());

    List<MovimientoDTOToList> movimientosDTOS = movimientoMapper.listMovimientoToListMovimientoDTOToList(movimientos);

    Integer cantidad = filterCantidadMovimientosInMemory(contratoCarga, idContrato, idCartera, contabilidad,
      id, fechaContable, fechaValor, tipoMovimiento, descripcion, importe, principal,
      interesesOrdinarios, interesesDemora, comisiones, gastos, afecta, estado).intValue();

    return new ListWithCountDTO<>(movimientosDTOS, cantidad);
  }

  private List<Predicate> getPredicates(CriteriaBuilder cb, Root<Movimiento> root,
    String contratoCarga, Integer idContrato, Integer idCartera, Boolean contabilidad, Integer id, String fechaContable, String fechaValor, String tipoMovimiento, String descripcion,
    String importe, String principal, String interesesOrdinarios, String interesesDemora, String comisiones,
    String gastos, Boolean afecta, Boolean estado) throws com.haya.alaska.shared.exceptions.NotFoundException {

    List<Predicate> predicates = new ArrayList<>();

    if (contabilidad){
      predicates.add(cb.isTrue(root.get(Movimiento_.CONTABILIDAD)));
      Cartera cartera = carteraRepository.findById(idCartera).orElseThrow(
        () -> new com.haya.alaska.shared.exceptions.NotFoundException("cartera", idCartera));
      predicates.add(cb.equal(root.get(Movimiento_.cartera).get(Cartera_.id), cartera.getId()));
    }
    else
      predicates.add(cb.isFalse(root.get(Movimiento_.CONTABILIDAD)));

    /*predicates.add(cb.equal(root.get(Movimiento_.contrato), idContrato));*/
    if (idContrato != null){
      Join<Movimiento, Contrato> destinatarioJoin = root.join(Movimiento_.contrato, JoinType.INNER);
      Predicate onDestinatario = cb.equal(destinatarioJoin.get(Contrato_.id), idContrato);
      destinatarioJoin.on(onDestinatario);
    }

    if (contratoCarga != null){
      Join<Movimiento, Contrato> destinatarioJoin = root.join(Movimiento_.contrato, JoinType.INNER);
      Predicate onDestinatario = cb.like(destinatarioJoin.get(Contrato_.idCarga), "%" + contratoCarga + "%");
      destinatarioJoin.on(onDestinatario);
    }

    if (id != null)
      predicates.add(cb.like(root.get(Movimiento_.id).as(String.class), "%" + id + "%"));
    if (fechaContable != null)
      predicates.add(cb.like(root.get(Movimiento_.fechaContable).as(String.class), "%" + fechaContable + "%"));
    if (fechaValor != null)
      predicates.add(cb.like(root.get(Movimiento_.fechaValor).as(String.class), "%" + fechaValor + "%"));
    if (tipoMovimiento != null)
      predicates.add(cb.like(root.get(Movimiento_.tipo).get(TipoMovimiento_.VALOR), "%" + tipoMovimiento + "%"));
    if (descripcion != null)
      predicates.add(cb.like(root.get(Movimiento_.descripcion).get(DescripcionMovimiento_.VALOR), "%" + descripcion + "%"));
    if (importe != null)
      predicates.add(cb.like(root.get(Movimiento_.importe).as(String.class), "%" + importe + "%"));
    if (principal != null)
      predicates.add(cb.like(root.get(Movimiento_.principalCapital).as(String.class), "%" + principal + "%"));
    if (interesesOrdinarios != null)
      predicates.add(cb.like(root.get(Movimiento_.interesesOrdinarios).as(String.class), "%" + interesesOrdinarios + "%"));
    if (interesesDemora != null)
      predicates.add(cb.like(root.get(Movimiento_.interesesDemora).as(String.class), "%" + interesesDemora + "%"));
    if (comisiones != null)
      predicates.add(cb.like(root.get(Movimiento_.comisiones).as(String.class), "%" + comisiones + "%"));
    if (gastos != null)
      predicates.add(cb.like(root.get(Movimiento_.gastos).as(String.class), "%" + gastos + "%"));
    if (afecta != null)
      predicates.add(cb.equal(root.get(Movimiento_.afecta), afecta));
    if (estado != null)
      predicates.add(cb.equal(root.get(Movimiento_.activo), estado));

    return predicates;
  }

  public List<Movimiento> filterMovimientosInMemory(
    String contratoCarga, Integer idContrato, Integer idCartera, Boolean contabilidad, Integer id, String fechaContable, String fechaValor, String tipoMovimiento, String descripcion,
    String importe, String principal, String interesesOrdinarios, String interesesDemora, String comisiones,
    String gastos, Boolean afecta, Boolean estado, String orderField, String orderDirection,
    Integer page, Integer size) throws com.haya.alaska.shared.exceptions.NotFoundException {

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Movimiento> cQuery = cb.createQuery(Movimiento.class);

    Root<Movimiento> root = cQuery.from(Movimiento.class);
    List<Predicate> predicates = getPredicates(cb, root, contratoCarga, idContrato, idCartera, contabilidad,
      id, fechaContable, fechaValor, tipoMovimiento, descripcion, importe, principal,
      interesesOrdinarios, interesesDemora, comisiones, gastos, afecta, estado);

    var rs = cQuery.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    Query query = entityManager.createQuery(cQuery);

    query.setFirstResult(page * size);
    query.setMaxResults(size);

    if (orderField != null) {
      if (orderField.equals("tipoMovimiento")) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(Movimiento_.TIPO).get("valor")));
        } else {
          rs.orderBy(cb.asc(root.get(Movimiento_.TIPO).get("valor")));
        }
      } else if (orderField.equals("descripcion")) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(Movimiento_.DESCRIPCION).get("valor")));
        } else {
          rs.orderBy(cb.asc(root.get(Movimiento_.DESCRIPCION).get("valor")));
        }
      } else if (orderField.equals("periodo")) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(Movimiento_.FECHA_CONTABLE)));
        } else {
          rs.orderBy(cb.asc(root.get(Movimiento_.FECHA_CONTABLE)));
        }
      } else if (orderField.equals("cartera")) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(Movimiento_.CARTERA).get(Cartera_.NOMBRE)));
        } else {
          rs.orderBy(cb.asc(root.get(Movimiento_.CARTERA).get(Cartera_.NOMBRE)));
        }
      } else if (orderField.equals("idContrato")) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(Movimiento_.CONTRATO).get(Contrato_.ID_CARGA)));
        } else {
          rs.orderBy(cb.asc(root.get(Movimiento_.CONTRATO).get(Contrato_.ID_CARGA)));
        }
      } else {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(orderField)));
        } else {
          rs.orderBy(cb.asc(root.get(orderField)));
        }
      }
    }
    return query.getResultList();
  }

  public Long filterCantidadMovimientosInMemory(
    String contratoCarga, Integer idContrato, Integer idCartera, Boolean contabilidad, Integer id, String fechaContable, String fechaValor, String tipoMovimiento, String descripcion,
    String importe, String principal, String interesesOrdinarios, String interesesDemora, String comisiones,
    String gastos, Boolean afecta, Boolean estado) throws com.haya.alaska.shared.exceptions.NotFoundException {

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Long> cQuery = cb.createQuery(Long.class);

    Root<Movimiento> root = cQuery.from(Movimiento.class);
    List<Predicate> predicates = getPredicates(cb, root, contratoCarga, idContrato, idCartera, contabilidad,
      id, fechaContable, fechaValor, tipoMovimiento, descripcion, importe, principal,
      interesesOrdinarios, interesesDemora, comisiones, gastos, afecta, estado);

    cQuery.where(predicates.toArray(new Predicate[predicates.size()]));
    cQuery.select(cb.count(root));

    return entityManager.createQuery(cQuery).getSingleResult();
  }


  private Predicate filterContratosCartera(Cartera cartera, CriteriaBuilder cb, Root<Movimiento> root){
    Predicate onDestinatario = cb.equal(root.get(Movimiento_.cartera).get(Cartera_.id), cartera.getId());
    //destinatarioJoin.on(onDestinatario);

    List<Predicate> resultados = new ArrayList<>();
    List<Predicate> pContratos = new ArrayList<>();
    List<Contrato> contratoList = getContratos(cartera);
    if (!contratoList.isEmpty()) {
      pContratos = filterContratos(cb, root, contratoList);
    }

    resultados.addAll(pContratos);
    resultados.add(onDestinatario);

    Predicate disE = cb.disjunction();

    for (Predicate pre : resultados) {
      disE.getExpressions().add(pre);
    }

    return disE;
  }

  private List<Predicate> filterContratos(CriteriaBuilder cb, Root<Movimiento> root, List<Contrato> objetos) {
    List<Predicate> resultados = new ArrayList<>();
    for (Contrato a : objetos) {
      if (!a.getMovimientos().isEmpty())
        resultados.add(cb.equal(root.get(Movimiento_.contrato).get(Contrato_.id), a.getId()));
    }
    return resultados;
  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelMovimientoByIdContrato(Integer idContrato, Boolean contabilidad) {
    Contrato contrato = contratoRepository.findById(idContrato).orElse(null);

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> movimientos = new ArrayList<>();
    movimientos.add(MovimientoExcel.cabeceras);

    for (Movimiento movimiento : contrato.getMovimientos()) {
      if (movimiento.getContabilidad().equals(contabilidad))
      movimientos.add(new MovimientoExcel(movimiento).getValuesList());
    }
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("Movements", movimientos);

    } else  {
      datos.put("Movimientos", movimientos);
    }

    return datos;
  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelMovimientoByIdCartera(Integer idCartera) throws com.haya.alaska.shared.exceptions.NotFoundException {
    Cartera cartera = carteraRepository.findById(idCartera).orElseThrow(
      () -> new com.haya.alaska.shared.exceptions.NotFoundException("cartera", idCartera));

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> movimientos = new ArrayList<>();
    movimientos.add(MovimientoContabilidadExcel.cabeceras);

    for (Movimiento movimiento : cartera.getMovimientos()) {
      movimientos.add(new MovimientoContabilidadExcel(movimiento).getValuesList());
    }

    /*List<Contrato> contratoList = getContratos(cartera);
    for (Contrato c : contratoList){
      for (Movimiento m : c.getMovimientos()){
        if (m.getContabilidad())
          movimientos.add(new MovimientoContabilidadExcel(m).getValuesList());
      }
    }*/

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("Movements", movimientos);

    } else  {
      datos.put("Movimientos", movimientos);
    }

    return datos;
  }

  @Override
  public void carga(Integer idContrato, Integer idCartera, Boolean contabilidad, MultipartFile file, Usuario usuario) throws Exception {
    List<Movimiento> movimientos;
    if (!contabilidad) movimientos = movimientosFromExcel(file);
    else movimientos = contabilidadFromExcel(file);
    //else throw new RequiredValueException("Error, se necesita un id contrato o un id cartera");

    if (movimientos.isEmpty()){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("Error, the file does not contain movements");
      else throw new RequiredValueException("Error, el fichero no contiene movimientos");
    }

    if (contabilidad){
      for (Movimiento movimiento : movimientos){
        if (movimiento.getId() == null || movimientoRepository.findById(movimiento.getId()).isEmpty()) {
          MovimientoInputDto movimientoDTO = movimientoMapper.entityToInputDto(movimiento);
          create(idContrato, idCartera, contabilidad, movimientoDTO, usuario);
        } else {
          MovimientoInputDto movimientoDTO = movimientoMapper.entityToInputDto(movimiento);
          update(movimiento.getId(), contabilidad, movimientoDTO);
        }
      }
    } else {
      for (Movimiento movimiento : movimientos) {
        if (idContrato != null) {
          List<Saldo> saldosAnteriores = saldoRepository.findAllByContratoIdAndDiaLessThanEqualOrderByDiaDesc(idContrato, movimiento.getFechaValor());
          if (!saldosAnteriores.isEmpty()) {
            if (movimiento.getId() == null || movimientoRepository.findById(movimiento.getId()).isEmpty()) {
              MovimientoInputDto movimientoDTO = movimientoMapper.entityToInputDto(movimiento);
              create(idContrato, null, contabilidad, movimientoDTO, usuario);
            } else {
              MovimientoInputDto movimientoDTO = movimientoMapper.entityToInputDto(movimiento);
              update(movimiento.getId(), contabilidad, movimientoDTO);
            }
          }
        }
      }
    }

  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> neteo(Integer idContrato, MultipartFile file) throws Exception {
    List<Movimiento> movimientosInput = movimientosFromExcel(file);
    movimientosInput.sort((p1, p2) -> new CompareToBuilder().
      append(p1.getFechaValor(), p2.getFechaValor()).
      append(p1.getImporte(), p2.getImporte()).
      append(p1.getTipo().getId(), p2.getTipo().getId()).toComparison());

    Date inicioDate = movimientosInput.get(0).getFechaValor();
    Date finalDate = movimientosInput.get(movimientosInput.size() - 1).getFechaValor();

    Contrato contrato = contratoRepository.findById(idContrato).orElse(null);
    List<Movimiento> movimientosContrato = new ArrayList<>(contrato.getMovimientos());//movimientoRepository.findAllByContratoAndFechaValorGreaterThanEqualAndFechaValorLessThanEqual(contrato, inicioDate, finalDate);
    movimientosContrato.sort((p1, p2) -> new CompareToBuilder().
      append(p1.getFechaValor(), p2.getFechaValor()).
      append(p1.getImporte(), p2.getImporte()).
      append(p1.getTipo().getId(), p2.getTipo().getId()).toComparison());

    List<Movimiento> movimientosIguales = new ArrayList<>();
    List<Movimiento> auxIguales = new ArrayList<>();

    List<Collection<?>> movNeteoNuevos = new ArrayList<>();
    movNeteoNuevos.add(MovimientoNeteoExcel.cabeceras);
    List<Collection<?>> movNeteoIguales = new ArrayList<>();
    movNeteoIguales.add(MovimientoNeteoExcel.cabeceras);
    List<Collection<?>> movNeteoViejos = new ArrayList<>();
    movNeteoViejos.add(MovimientoNeteoExcel.cabeceras);

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    boolean igual;
    for (Movimiento movimientoFuera : movimientosInput) {
      List<Movimiento> igualesAEste = new ArrayList<>();
      igual = false;
      for (Movimiento movimientoInterno : movimientosContrato) {
        if (movimientoFuera.getFechaValor().equals(movimientoInterno.getFechaValor()) &&
          movimientoFuera.getImporte().equals(movimientoInterno.getImporte())) {
          if (movimientoFuera.getTipo().equals(movimientoInterno.getTipo())) {
            igual = true;
            movimientosIguales.add(movimientoInterno);
            movimientosContrato.remove(movimientoInterno);
            auxIguales.add(movimientoFuera);
            break;
          }
          igualesAEste.add(movimientoInterno);
        }
      }
      if (!igual && igualesAEste.size() != 0) {
        movimientosIguales.add(igualesAEste.get(0));
        movimientosContrato.remove(igualesAEste.get(0));
        auxIguales.add(movimientoFuera);
      }
    }

    for (Movimiento movimientoFuera : auxIguales) {
      movimientosInput.remove(movimientoFuera);
    }

    for (Movimiento movExcel : movimientosInput) {
      movNeteoNuevos.add(new MovimientoNeteoExcel(movExcel).getValuesList());
    }
    for (Movimiento movExcel : movimientosIguales) {
      movNeteoIguales.add(new MovimientoNeteoExcel(movExcel).getValuesList());
    }
    for (Movimiento movExcel : movimientosContrato) {
      movNeteoViejos.add(new MovimientoNeteoExcel(movExcel).getValuesList());
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("New Movements in Charge", movNeteoNuevos);
      datos.put("Older Movements not in Charge", movNeteoViejos);
      datos.put("Coincident Movements", movNeteoIguales);

    } else  {
      datos.put("Movimientos Nuevos en la Carga", movNeteoNuevos);
      datos.put("Movimientos Viejos no en la Carga", movNeteoViejos);
      datos.put("Movimientos Coincidentes", movNeteoIguales);
    }
    return datos;
  }

  /*
   * Función para actualizar el saldo en función de los movimientos recibidos. Los movimientos se
   * tendrán en cuenta para recalcular los saldos en el caso en que la fecha del saldo sea la misma
   * que la del movimiento. Para tener en cuenta el movimiento, debe tener como true el campo afecta
   */
  public void updateSaldoConFechaAnterior(List<Movimiento> movimientos, Integer id)
    throws RequiredValueException, NotFoundException, InvalidCodeException {

    // fecha para comprobar si es movimiento con fecha de futuro
    Date fechaActual = new Date();
    boolean saldoCreado = false;

    Collections.sort(movimientos, (a, b) -> a.getFechaValor().compareTo(b.getFechaValor()));
    Date fechaMovimiento = movimientos.get(0).getFechaValor();
    for (Movimiento movimiento : movimientos) {
      // fecha de movimiento anterior a la actual
      if (fechaActual.compareTo(fechaMovimiento) >= 0) {
        List<Saldo> saldosFechaIgual;
        saldosFechaIgual = saldoRepository.findAllByContratoIdAndDiaEqualsOrderByDiaAsc(id, movimiento.getFechaValor());
        if (saldosFechaIgual.size() > 1){
          Locale loc = LocaleContextHolder.getLocale();
          if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
            throw new InvalidCodeException("There are two balances for the same contract on the same day. Data error.");
          else throw new InvalidCodeException("Existen dos saldos para el mismo contrato en el mismo dia. Error de datos.");
        }
        if (saldosFechaIgual.size() == 1) {
          Saldo saldoAModificar = saldosFechaIgual.get(0);
          Saldo calculatedSaldo = updateSaldoConFechaAnterior(movimiento, saldoAModificar);
          saldoRepository.save(calculatedSaldo);
        } else {
          List<Saldo> saldosAnteriores;
          saldosAnteriores = saldoRepository.findAllByContratoIdAndDiaLessThanOrderByDiaDesc(id, movimiento.getFechaValor());
          if (!saldosAnteriores.isEmpty()) {
            Saldo nuevoSaldo = createSaldoToDate(saldosAnteriores.get(0), movimiento, movimiento.getFechaValor());
          } else {
            Locale loc = LocaleContextHolder.getLocale();
            if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
              throw new NotFoundException("You are trying to enter a movement dated before the first balance.");
            else throw new NotFoundException(
              "Estas intentando introducir un movimiento con fecha anterior al primer saldo.");
          }
        }
        List<Saldo> saldosPosteriores;
        saldosPosteriores = saldoRepository.findAllByContratoIdAndDiaGreaterThanOrderByDiaAsc(id, movimiento.getFechaValor());

        for (Saldo saldo : saldosPosteriores) {
          if (saldo != null) {
            Saldo calculatedSaldo = updateSaldoConFechaAnterior(movimiento, saldo);
            int a;
            if (calculatedSaldo.getId() == 252)
              a = 1;
            saldoRepository.saveAndFlush(calculatedSaldo);
          }
        }
      }
    }
  }

  /*
   * Función para actualizar el saldo tras la inserción o modificación de un movimiento
   */
  private Saldo updateSaldoConFechaAnterior(Movimiento movimiento, Saldo saldo)
    throws RequiredValueException {

    if (movimiento.getAfecta()) {

      // comprobamos que importe y desglose (en el caso de estar informados) suman lo mismo
      if (movimiento.getImporte() != null && (movimiento.getInteresesDemora() != null
        || movimiento.getInteresesOrdinarios() != null || movimiento.getComisiones() != null
        || movimiento.getGastos() != null)) {

        Double interesesDemora =
          movimiento.getInteresesDemora() != null ? movimiento.getInteresesDemora() : 0;
        Double interesesOrdinarios =
          movimiento.getInteresesOrdinarios() != null ? movimiento.getInteresesOrdinarios() : 0;
        Double comisiones = movimiento.getComisiones() != null ? movimiento.getComisiones() : 0;
        Double gastos = movimiento.getGastos() != null ? movimiento.getGastos() : 0;
        Double principalCapital = movimiento.getPrincipalCapital() != null ? movimiento.getPrincipalCapital() : 0;

        if ((interesesDemora + interesesOrdinarios + comisiones + gastos + principalCapital != movimiento
          .getImporte())){
          Locale loc = LocaleContextHolder.getLocale();
          if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
            throw new RequiredValueException("Error: Amount of movement does not coincide with breakdown");
          else throw new RequiredValueException("Error: Importe del movimiento no coincidente con desglose");
        }
      }
      Double impuestos = movimiento.getImpuestos() != null ? movimiento.getImpuestos() : 0;

      if (movimiento.getSentido() == null)
        throw new RequiredValueException("sentido");

      if (!movimiento.getSentido().equals("-") && !movimiento.getSentido().equals("+")){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new RequiredValueException("Format error of sense field. (+/-).");
        else throw new RequiredValueException("Error de formato del campo sentido.(+/-).");
      }

      // calculamos el saldo en función de los datos que vengan informados en el movimiento
      Double interesDem = movimiento.getInteresesDemora();
      Double interesOrd = movimiento.getInteresesOrdinarios();
      Double comi = movimiento.getComisiones();
      Double gast = movimiento.getGastos();
      Double prinCap = movimiento.getPrincipalCapital();

      if ((interesDem == null || interesDem == 0) &&
        (interesOrd == null || interesOrd == 0) &&
        (comi == null || comi == 0) &&
        (gast == null || gast == 0) &&
        (prinCap == null || prinCap == 0)) {
        return calcularSaldoMovimientoSinDesglose(saldo, movimiento);
      } else {
        return calcularSaldoMovimientoConDesglose(saldo, movimiento);
      }

    }
    return saldo;
  }

  /*
   * Creamos nuevo saldo, si no se le pasa fecha, generamos con fecha actual
   * */
  public Saldo createSaldoToDate(Saldo saldoAnterior, Movimiento movimiento, Date fecha) throws RequiredValueException {
    Saldo nuevoSaldo = new Saldo(saldoAnterior);
    Saldo saldoCalculado = updateSaldoConFechaAnterior(movimiento, nuevoSaldo);
    Date fechaSaldo = fecha == null ? new Date() : fecha;
    saldoCalculado.setDia(fechaSaldo);
    return saldoRepository.saveAndFlush(saldoCalculado);
  }

  /*
   * Función para calcular el saldo resultante al insertar o modificar un movimiento sin desglose.
   * En este caso, el movimiento viene informado con el importe y sin comisiones, gastos ni
   * intereses
   */
  public Saldo calcularSaldoMovimientoSinDesglose(Saldo saldo, Movimiento movimiento)
    throws RequiredValueException {

    if (!movimiento.getSentido().equals("-") && !movimiento.getSentido().equals("+")){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("Format error of sense field. (+/-).");
      else throw new RequiredValueException("Error de formato del campo sentido.(+/-).");
    }
    int factor = 1;
    if (movimiento.getSentido().equals("-")) {
      factor = -1;
    }
    Double restoImporte = movimiento.getImporte();

    Double deudaInpagada = saldo.getDeudaImpagada() != null ? saldo.getDeudaImpagada() : 0;

    Double interesesDemora = saldo.getInteresesDemora() != null ? saldo.getInteresesDemora() : 0;
    Double comisionesImpagados = saldo.getComisionesImpagados() != null ? saldo.getComisionesImpagados() : 0;
    Double gastosImpagados = saldo.getGastosImpagados() != null ? saldo.getGastosImpagados() : 0;
    Double interesesOrdinariosImpagados = saldo.getInteresesOrdinariosImpagados() != null ? saldo.getInteresesOrdinariosImpagados() : 0;
    Double principalCapital = saldo.getPrincipalVencidoImpagado() != null ? saldo.getPrincipalVencidoImpagado() : 0;

    if (interesesDemora == 0 && comisionesImpagados == 0 && gastosImpagados == 0 && interesesOrdinariosImpagados == 0
      && principalCapital == 0) { // Entonces no hay desglose segun Galan
      saldo.setDeudaImpagada(deudaInpagada + factor * movimiento.getImporte());
    } else {
      if (movimiento.getSentido().equals("+")) {
        saldo.setPrincipalVencidoImpagado(principalCapital + restoImporte);
        saldo.setDeudaImpagada(
          saldo.getInteresesDemora() +
            saldo.getComisionesImpagados() +
            saldo.getGastosImpagados() +
            saldo.getInteresesOrdinariosImpagados() +
            saldo.getPrincipalVencidoImpagado());
      } else {
        if (interesesDemora < restoImporte) {
          restoImporte = restoImporte - interesesDemora;
          saldo.setInteresesDemora(0.0);
        } else {
          saldo.setInteresesDemora(interesesDemora - restoImporte);
          restoImporte = 0.0;
        }

        if (gastosImpagados < restoImporte) {
          restoImporte = restoImporte - gastosImpagados;
          saldo.setGastosImpagados(0.0);
        } else {
          saldo.setGastosImpagados(gastosImpagados - restoImporte);
          restoImporte = 0.0;
        }

        if (comisionesImpagados < restoImporte) {
          restoImporte = restoImporte - comisionesImpagados;
          saldo.setComisionesImpagados(0.0);
        } else {
          saldo.setComisionesImpagados(comisionesImpagados - restoImporte);
          restoImporte = 0.0;
        }

        if (interesesOrdinariosImpagados < restoImporte) {
          restoImporte = restoImporte - interesesOrdinariosImpagados;
          saldo.setInteresesOrdinariosImpagados(0.0);
        } else {
          saldo.setInteresesOrdinariosImpagados(interesesOrdinariosImpagados - restoImporte);
          restoImporte = 0.0;
        }

        if (principalCapital < restoImporte) {
          restoImporte = restoImporte - principalCapital;
          saldo.setPrincipalVencidoImpagado(0.0);
        } else {
          saldo.setPrincipalVencidoImpagado(principalCapital - restoImporte);
          restoImporte = 0.0;
        }

        if (restoImporte > 0) {
          saldo.setDeudaImpagada(-restoImporte);
        } else {
          saldo.setDeudaImpagada(
            saldo.getInteresesDemora() +
              saldo.getComisionesImpagados() +
              saldo.getGastosImpagados() +
              saldo.getInteresesOrdinariosImpagados() +
              saldo.getPrincipalVencidoImpagado());
        }
      }
    }

    if (movimiento.getPrincipalPendienteVencer() != null)
      saldo.setCapitalPendiente(movimiento.getPrincipalPendienteVencer());

    if (factor == -1) saldo.setImporteRecuperado(saldo.getImporteRecuperado() - movimiento.getImporte());

    saldo.setSaldoGestion(saldo.getDeudaImpagada() + saldo.getCapitalPendiente());

    return saldo;
  }

  /*
   * Función para calcular el saldo resultante al insertar o modificar un movimiento con desglose.
   * En este caso, el movimiento viene informado con comisiones, gastos e intereses
   */
  @Override
  public Saldo calcularSaldoMovimientoConDesglose(Saldo saldo, Movimiento movimiento)
    throws RequiredValueException {

    if (!movimiento.getSentido().equals("-") && !movimiento.getSentido().equals("+")){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("Format error of sense field. (+/-).");
      else throw new RequiredValueException("Error de formato del campo sentido.(+/-).");
    }
    int factor = 1;
    if (movimiento.getSentido().equals("-")) {
      factor = -1;
    }

    Double interesesDemora = saldo.getInteresesDemora() != null ? saldo.getInteresesDemora() : 0;
    Double comisionesImpagados = saldo.getComisionesImpagados() != null ? saldo.getComisionesImpagados() : 0;
    Double gastosImpagados = saldo.getGastosImpagados() != null ? saldo.getGastosImpagados() : 0;
    Double interesesOrdinariosImpagados = saldo.getInteresesOrdinariosImpagados() != null ? saldo.getInteresesOrdinariosImpagados() : 0;
    Double principalVencidoImpagado = saldo.getPrincipalVencidoImpagado() != null ? saldo.getPrincipalVencidoImpagado() : 0;
    Double deudaInpagada = saldo.getDeudaImpagada() != null ? saldo.getDeudaImpagada() : 0;
    Double principalCapital = saldo.getPrincipalVencidoImpagado() != null ? saldo.getPrincipalVencidoImpagado() : 0;

    if (interesesDemora == 0 && comisionesImpagados == 0 && gastosImpagados == 0 && interesesOrdinariosImpagados == 0
      && principalCapital == 0) { // Entonces no hay desglose segun Galan
      saldo.setDeudaImpagada(deudaInpagada + factor * movimiento.getImporte());

    } else {
      if (movimiento.getComisiones() != null)
        saldo.setComisionesImpagados(comisionesImpagados + factor * movimiento.getComisiones());
      if (movimiento.getInteresesDemora() != null)
        saldo.setInteresesDemora(interesesDemora + factor * movimiento.getInteresesDemora());
      if (movimiento.getInteresesOrdinarios() != null)
        saldo.setInteresesOrdinariosImpagados(
          interesesOrdinariosImpagados + factor * movimiento.getInteresesOrdinarios());
      if (movimiento.getGastos() != null)
        saldo.setGastosImpagados(gastosImpagados + factor * movimiento.getGastos());
      if (movimiento.getPrincipalCapital() != null)
        saldo.setPrincipalVencidoImpagado(principalVencidoImpagado + factor * movimiento.getPrincipalCapital());

      saldo.setDeudaImpagada(
        saldo.getInteresesDemora() +
          saldo.getComisionesImpagados() +
          saldo.getGastosImpagados() +
          saldo.getInteresesOrdinariosImpagados() +
          saldo.getPrincipalVencidoImpagado());
    }
    if (movimiento.getPrincipalPendienteVencer() != null)
      saldo.setCapitalPendiente(movimiento.getPrincipalPendienteVencer());

    if (factor == -1) saldo.setImporteRecuperado(saldo.getImporteRecuperado() - movimiento.getImporte());

    saldo.setSaldoGestion(saldo.getDeudaImpagada() + saldo.getCapitalPendiente());
    return saldo;

  }

  private Movimiento cambiarSentido(Movimiento movimiento) {
    movimiento.setImporte(-1 * movimiento.getImporte());
    Double principalCapital = movimiento.getPrincipalCapital();
    Double interesesOrdinarios = movimiento.getInteresesOrdinarios();
    Double interesesDemora = movimiento.getInteresesDemora();
    Double comisiones = movimiento.getComisiones();
    Double gastos = movimiento.getGastos();

    if (principalCapital != null) {
      movimiento.setPrincipalCapital(-1 * principalCapital);
    }
    if (interesesOrdinarios != null) {
      movimiento.setInteresesOrdinarios(-1 * interesesOrdinarios);
    }
    if (interesesDemora != null) {
      movimiento.setInteresesDemora(-1 * interesesDemora);
    }
    if (comisiones != null) {
      movimiento.setComisiones(-1 * comisiones);
    }
    if (gastos != null) {
      movimiento.setGastos(-1 * gastos);
    }

    return movimiento;
  }

  private List<Movimiento> movimientosFromExcel(MultipartFile file) throws Exception {
    List<Movimiento> movimientos = new ArrayList<>();
    XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
    XSSFSheet worksheet = workbook.getSheetAt(0);
    for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
      Movimiento movimiento = new Movimiento();
      XSSFRow row = worksheet.getRow(i);

      // todo cliente ?? id 0
      //movimiento.setCartera(carteraRepository.findById((int) row.getCell(1).getNumericCellValue()).orElse(null));
//      movimiento.setContrato(contratoService.findByIdOrNull((int) row.getCell(2).getNumericCellValue()));
      String idCarga = getRawStringValue(row,2);
      //Integer idContato = getIntegerValue(row, 2);
      if (idCarga != null)
        movimiento.setContrato(contratoRepository.findByIdCarga(idCarga).orElse(null));
      if(movimiento.getContrato() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Contract not found in row" + i);
        else throw new NotFoundException("Contrato no encontrato en la fila " + i);
      }

      String tipo = getRawStringValue(row, 3);
      if (tipo != null)
        movimiento.setTipo(tipoMovimientoRepository.findByValor(tipo).orElse(null));
      if(movimiento.getTipo() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Tipo movimiento not found in row" + i);
        else throw new NotFoundException("Tipo movimiento no encontrato en la fila " + i);
      }

      String descripcion = getRawStringValue(row, 4);
      if (descripcion != null)
        movimiento.setDescripcion(descripcionMovimientoRepository.findByValor(descripcion).orElse(null));
      if(movimiento.getDescripcion() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Descripcion not found in row" + i);
        else throw new NotFoundException("Descripcion no encontrato en la fila "+i);
      }

      Date fValor = getDateValue(row, 5);
      if (fValor != null)
        movimiento.setFechaValor(fValor);
      if(movimiento.getFechaValor() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Fecha valor not found in row" + i);
        else throw new NotFoundException("Fecha valor no encontrato en la fila "+i);
      }

      Date fContable = getDateValue(row, 6);
      if (fContable != null)
        movimiento.setFechaContable(fContable);
      if(movimiento.getFechaContable() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Fecha contable not found in row" + i);
        else throw new NotFoundException("Fecha contable no encontrato en la fila "+i);
      }

      String sentido = getRawStringValue(row, 7);
      if (sentido != null)
        movimiento.setSentido(sentido);
      if(movimiento.getSentido() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Sentido not found in row" + i);
        else throw new NotFoundException("Sentido no encontrato en la fila "+i);
      }
      movimiento.setImporte(getNumericValue(row, 8));
      movimiento.setPrincipalCapital(getNumericValue(row, 9));
      movimiento.setInteresesOrdinarios(getNumericValue(row, 10));
      movimiento.setInteresesDemora(getNumericValue(row, 11));
      movimiento.setComisiones(getNumericValue(row, 12));
      movimiento.setGastos(getNumericValue(row, 13));
      movimiento.setUsuario(getStringValue(row, 14));
      movimiento.setImporteRecuperado(getNumericValue(row, 15));
      movimiento.setSalidaDudoso(getNumericValue(row, 16));
      movimiento.setImporteFacturable(getNumericValue(row, 17));
      movimiento.setIdCarga(getStringValue(row, 18));
      String afecta = getStringValue(row, 19);
      movimiento.setAfecta(afecta != null && afecta.equals("true"));
      String activo = getStringValue(row, 20);
      movimiento.setActivo(activo != null && activo.equals("Activo"));
      movimiento.setComentarios(getStringValue(row, 21));
      movimientos.add(movimiento);
    }

    return movimientos;
  }

  private List<Movimiento> contabilidadFromExcel(MultipartFile file) throws Exception{
    List<Movimiento> movimientos = new ArrayList<>();
    XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
    XSSFSheet worksheet = workbook.getSheetAt(0);
    for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
      Movimiento movimiento = new Movimiento();
      XSSFRow row = worksheet.getRow(i);

      Integer idCartera = getIntegerValueConGuion(row, 1);
      if (idCartera != null)
        movimiento.setCartera(carteraRepository.findById(idCartera).orElse(null));

      String idContato = getRawStringValueConGuion(row, 2);
      if (idContato != null)
        movimiento.setContrato(contratoRepository.findByIdCarga(idContato).orElse(null));

      if (movimiento.getContrato() == null && movimiento.getCartera() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Contract not found in row" + i);
        else throw new NotFoundException("Contrato no encontrato en la fila " + i);
      }

      String tipo = getRawStringValueConGuion(row, 3);
      if (tipo != null)
        movimiento.setTipo(tipoMovimientoRepository.findByValor(tipo).orElse(null));
      if (movimiento.getTipo() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Tipo movimiento not found in row" + i);
        else throw new NotFoundException("Tipo movimiento no encontrato en la fila " + i);
      }

      String descripcion = getRawStringValueConGuion(row, 4);
      if (descripcion != null)
        movimiento.setDescripcion(descripcionMovimientoRepository.findByValor(descripcion).orElse(null));
      if (movimiento.getDescripcion() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Descripcion not found in row" + i);
        else throw new NotFoundException("Descripcion no encontrato en la fila " + i);
      }

      Date fValor = getDateValue(row, 5);
      if (fValor != null)
        movimiento.setFechaValor(fValor);
      if (movimiento.getFechaValor() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Fecha valor not found in row" + i);
        else throw new NotFoundException("Fecha valor no encontrato en la fila " + i);
      }

      Date fContable = getDateValue(row, 6);
      if (fContable != null)
        movimiento.setFechaContable(fContable);
      if (movimiento.getFechaContable() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Fecha contable not found in row" + i);
        else throw new NotFoundException("Fecha contable no encontrato en la fila " + i);
      }

      String sentido = getRawStringValue(row, 7);
      if (sentido != null)
        movimiento.setSentido(sentido);
      if (movimiento.getSentido() == null) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException("Sentido not found in row" + i);
        else throw new NotFoundException("Sentido no encontrato en la fila " + i);
      }
      movimiento.setImporte(getNumericValueConGuion(row, 8));
      movimiento.setPrincipalCapital(getNumericValueConGuion(row, 9));
      movimiento.setInteresesOrdinarios(getNumericValueConGuion(row, 10));
      movimiento.setInteresesDemora(getNumericValueConGuion(row, 11));
      movimiento.setComisiones(getNumericValueConGuion(row, 12));
      movimiento.setGastos(getNumericValueConGuion(row, 13));
      movimiento.setUsuario(getRawStringValueConGuion(row, 14));
      movimiento.setImporteRecuperado(getNumericValueConGuion(row, 15));
      movimiento.setSalidaDudoso(getNumericValueConGuion(row, 16));
      movimiento.setImporteFacturable(getNumericValueConGuion(row, 17));
      movimiento.setId(getIntegerValueConGuion(row, 18));
      String afecta = getRawStringValueConGuion(row, 19);
      movimiento.setAfecta(afecta != null && afecta.equals("SI"));
      String activo = getRawStringValueConGuion(row, 20);
      movimiento.setActivo(activo != null && activo.equals("SI"));
      movimiento.setComentarios(getRawStringValueConGuion(row, 21));
      movimientos.add(movimiento);
    }
    return movimientos;
  }
}
