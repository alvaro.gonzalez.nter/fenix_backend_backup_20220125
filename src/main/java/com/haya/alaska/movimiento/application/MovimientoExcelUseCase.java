package com.haya.alaska.movimiento.application;

import com.haya.alaska.movimiento.infrastructure.controller.dto.InformeMovimientoInputDTO;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface MovimientoExcelUseCase {
  LinkedHashMap<String, List<Collection<?>>> findExcelMovimientosById(Integer carteraId, Integer mes, InformeMovimientoInputDTO informeMovimientoInputDTO,
                                                                      Usuario usuario) throws Exception;
}
