package com.haya.alaska.movimiento.application;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.ContratoMovimientoExcel;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.application.ExpedienteUseCase;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.ExpedienteCarteraExcel;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.infrastructure.controller.dto.InformeMovimientoInputDTO;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.infrastructure.repository.SaldoRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class MovimientoExcelUseCaseImpl implements MovimientoExcelUseCase {

  @Autowired
  private CarteraRepository carteraRepository;

  @Autowired
  private EventoUtil eventoUtil;
  @Autowired
  ExpedienteUtil expedienteUtil;
  @Autowired
  ExpedienteUseCase expedienteUseCase;
  @Autowired
  SaldoRepository saldoRepository;

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelMovimientosById(Integer carteraId, Integer mes, InformeMovimientoInputDTO informeMovimientoInputDTO,
                                                                             Usuario usuario) throws Exception {

    Perfil perfilUsu = usuario.getPerfil();

    if(perfilUsu.getNombre().equals("Responsable de cartera") || perfilUsu.getNombre().equals("Supervisor")) {
      var datos = this.createMovimientoDataForExcel();
      Cartera cartera = carteraRepository.findById(carteraId)
        .orElseThrow(() -> new NotFoundException("Cartera", carteraId));
      this.addMovimientoDataForExcel(cartera, datos, mes, informeMovimientoInputDTO);
      return datos;
    }
    else {
      throw new IllegalArgumentException("El tipo del usuario debe ser Responsable de cartera o Supervisor para poder generar el Informe");
    }
  }



  private LinkedHashMap<String, List<Collection<?>>> createMovimientoDataForExcel() {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expediente = new ArrayList<>();
    expediente.add(ExpedienteCarteraExcel.cabeceras);

    List<Collection<?>> contratoMovimiento = new ArrayList<>();
    contratoMovimiento.add(ContratoMovimientoExcel.cabeceras);

  /*  List<Collection<?>> movimiento = new ArrayList<>();
    movimiento.add(MovimientoExcel.cabeceras);
    datos.put("Movimiento", movimiento);*/

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      datos.put("Connection", expediente);
      datos.put("Loan-Movement", contratoMovimiento);

    } else  {

      datos.put("Expediente", expediente);
      datos.put("Contrato-Movimiento", contratoMovimiento);
    }


    return datos;
  }


  private void addMovimientoDataForExcel(Cartera cartera, LinkedHashMap<String, List<Collection<?>>> datos, Integer mes,
                                         InformeMovimientoInputDTO informeMovimientoInputDTO) throws ParseException, NotFoundException {

    List<Collection<?>> expedientes;
    List<Collection<?>>  contratosMovimientos;
  //  var movimientos = datos.get("Movimiento");

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      expedientes = datos.get("Connection");
      contratosMovimientos = datos.get("Loan-Movement");

    } else{
      expedientes = datos.get("Expediente");
      contratosMovimientos = datos.get("Contrato-Movimiento");

    }

  /*  Date fechaValorInicialD =informeMovimientoInputDTO.getFechaValorInicial()!=null ? new SimpleDateFormat("yyyy-MM-dd").parse(informeMovimientoInputDTO.getFechaValorInicial()):null;
    Date fechaValorFinalD =informeMovimientoInputDTO.getFechaValorFinal()!=null? new SimpleDateFormat("yyyy-MM-dd").parse(informeMovimientoInputDTO.getFechaValorFinal()):null;
    Date fechaContableInicialD =informeMovimientoInputDTO.getFechaContableFinal()!=null ? new SimpleDateFormat("yyyy-MM-dd").parse(informeMovimientoInputDTO.getFechaContableInicial()):null;
    Date fechaContableFinalD =informeMovimientoInputDTO.getFechaContableFinal()!=null ? new SimpleDateFormat("yyyy-MM-dd").parse(informeMovimientoInputDTO.getFechaContableFinal()):null;*/

    for (Expediente expediente : cartera.getExpedientes()) {

      boolean seguir = false;
      for(Contrato contrato : expediente.getContratos()) {
        if(contrato.getMovimientos().size()>0)
          seguir=true;
      }
      if(!seguir)
        continue;

      Boolean comprobacion = expedienteUseCase.filtroMesInformes(expediente, mes);
      var estadoExpediente = expedienteUtil.getEstado(expediente);

      if (comprobacion) {
        if (informeMovimientoInputDTO.getEstadoExpediente() == null || estadoExpediente.getId().equals(informeMovimientoInputDTO.getEstadoExpediente())) {
          Evento accion = getLastEvento(expediente.getId(), 1);
          Evento alerta = getLastEvento(expediente.getId(), 2);
          Evento actividad = getLastEvento(expediente.getId(), 3);
          Procedimiento p = expedienteUtil.getUltimoProcedimiento(expediente);
          expedientes.add(new ExpedienteCarteraExcel(expediente, accion, alerta, actividad, p).getValuesList());

          List<Contrato> expedientesSinFiltrar = expediente.getContratos().stream().collect(Collectors.toList());
        List<Contrato>expedientesFilter = expediente.getContratos().stream()
            .filter(
              contrato -> {
                if (informeMovimientoInputDTO.getProducto()!=null) {
                  if (contrato.getProducto() == null || !contrato.getProducto().getId().equals(informeMovimientoInputDTO.getProducto())) return false;
                }
                if (informeMovimientoInputDTO.getEstadoContable()!=null){
                  if (contrato.getClasificacionContable()==null || !contrato.getClasificacionContable().getId().equals(informeMovimientoInputDTO.getEstadoContable())) return false;
                }
                if (informeMovimientoInputDTO.getEstado()!=null){
                  if (contrato.getEstadoContrato()==null || !contrato.getEstadoContrato().getId().equals(informeMovimientoInputDTO.getEstado())) return false;
                }
                return true;
              })
            .collect(Collectors.toList());


       for (Contrato contrato : expedientesFilter) {

         if(contrato.getMovimientos().size()==0)
           continue;

            List<Movimiento> movimientosFilterValorTipo= contrato.getMovimientos().stream().filter(
              movimiento -> {
                if (movimiento.getContabilidad() != null && movimiento.getContabilidad()) return false;
                if (informeMovimientoInputDTO.getTipoMovimiento()!=null){
                  if (movimiento.getTipo()==null || !movimiento.getTipo().getId().equals(informeMovimientoInputDTO.getTipoMovimiento()))return false;
                }
                Date fCE = movimiento.getFechaValor();
                Date fCI = informeMovimientoInputDTO.getFechaValorInicial();
                Date fCF = informeMovimientoInputDTO.getFechaValorFinal();
                if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;

                Date fRE = movimiento.getFechaContable();
                Date fRI = informeMovimientoInputDTO.getFechaContableInicial();
                Date fRF = informeMovimientoInputDTO.getFechaContableFinal();
                if (!expedienteUtil.comprobarEntre(fRE, fRI, fRF)) return false;
                return true;
              }).collect(Collectors.toList());

       for (Movimiento movimiento : movimientosFilterValorTipo) {

         Saldo saldo = saldoRepository.findByContratoIdAndDia(contrato.getId(), movimiento.getFechaValor());
         if (saldo == null) {
           Locale loc = LocaleContextHolder.getLocale();
           if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
             throw new NotFoundException("No balance found for contract of id: " + contrato.getId() + ", dated: " +
                     movimiento.getFechaValor() + ", like move id: " + movimiento.getId());
           else throw new NotFoundException("No se ha encontrado un saldo para el contrato de id: " + contrato.getId() + ", con fecha: " +
                   movimiento.getFechaValor() + ", como el movimiento de id: " + movimiento.getId());
         } else {
           contratosMovimientos.add(new ContratoMovimientoExcel(contrato,movimiento, saldo).getValuesList());
         }

            }
          }
        }
      }
    }
  }


  public Evento getLastEvento(Integer idExpediente, Integer clase) {
    try {
      List<Integer> clases = new ArrayList<>();
      clases.add(clase);
      List<Evento> eventos = eventoUtil.getFilteredEventos(null, null, null, 1,
        idExpediente, null, null, null, null,
        null, null, clases, null, null, null, null, null, null, null, null,
        null, null, "fechaCreacion", "desc", 0, 100);

      Evento result = null;
      if (!eventos.isEmpty()) result = eventos.get(0);
      return result;
    }catch (Exception e) {
      return null;
    }
  }


}
