package com.haya.alaska.movimiento.application;

import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoInputDto;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoOutputDto;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.infrastructure.controller.dto.SaldoDto;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MovimientoDTOToList;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

public interface MovimientoUseCase {
  ListWithCountDTO<SaldoDto> getSaldosDTOByIdContrato(Integer idContrato, Integer page, Integer size);

  MovimientoOutputDto findMovimientoDTOById(Integer idMovimiento) throws IllegalAccessException;

  MovimientoOutputDto create(Integer idContrato, Integer idCartera, Boolean contabilidad, MovimientoInputDto movimientoDTO, Usuario usuario) throws Exception;
  void createContabilidad(Integer idCartera, Boolean contratos, MovimientoInputDto movimientoInputDto, Usuario usuario) throws Exception;
  MovimientoOutputDto update(Integer idMovimiento, Boolean contabilidad, MovimientoInputDto movimientoDTO) throws Exception;

  void delete(Integer idMovimiento, Boolean contabilidad) throws Exception;

  public List<Movimiento> findAllByIdExpedienteFechaValorBetween(Integer idExpediente, Date fechaInicio, Date fechaFin);

  ListWithCountDTO<MovimientoDTOToList> getAllFilteredMovimientos(String contratoCarga, Integer idContrato, Integer idCartera,
                                                                  Boolean contabilidad,
                                                                  Integer id,
                                                                  String fechaContable,
                                                                  String fechaValor,
                                                                  String tipoMovimiento,
                                                                  String descripcion,
                                                                  String importe,
                                                                  String principal,
                                                                  String interesesOrdinarios,
                                                                  String interesesDemora,
                                                                  String comisiones,
                                                                  String gastos,
                                                                  Boolean afecta,
                                                                  Boolean estado,
                                                                  String orderField, String orderDirection,
                                                                  Integer size, Integer page) throws NotFoundException;

  LinkedHashMap<String, List<Collection<?>>> findExcelMovimientoByIdContrato(Integer idContrato, Boolean contabilidad);

  LinkedHashMap<String, List<Collection<?>>> findExcelMovimientoByIdCartera(Integer idCartera) throws NotFoundException;

  void carga(Integer idContrato, Integer idCartera, Boolean contabilidad, MultipartFile file, Usuario usuario) throws Exception;

  LinkedHashMap<String, List<Collection<?>>> neteo(Integer idContrato, MultipartFile file) throws Exception;

  Saldo createSaldoToDate(Saldo saldoAnterior, Movimiento movimiento, Date fecha) throws RequiredValueException;

  Saldo calcularSaldoMovimientoConDesglose(Saldo saldo, Movimiento movimiento)
    throws RequiredValueException;

}
