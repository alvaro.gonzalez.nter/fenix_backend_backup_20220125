package com.haya.alaska.movimiento.infrastructure.controller;

import com.haya.alaska.movimiento.application.MovimientoUseCase;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoDto;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoInputDto;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoOutputDto;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.domain.Saldo_;
import com.haya.alaska.saldo.infrastructure.controller.dto.SaldoDto;
import com.haya.alaska.saldo.infrastructure.mapper.SaldoMapper;
import com.haya.alaska.saldo.infrastructure.repository.SaldoRepository;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MovimientoDTOToList;
import com.haya.alaska.shared.dto.SaldoMovimientoDto;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/contabilidad/")
public class ContabilidadController {

  @Autowired
  MovimientoUseCase movimientoUseCase;

  /*@ApiOperation(
    value = "Listado saldos",
    notes = "Devuelve el histórico de saldos para el id de contrato enviado")
  @GetMapping("/saldos")
  public ListWithCountDTO<SaldoDto> getAllSaldos(
    @RequestParam(value = "idCartera") Integer idCartera,
    @RequestParam("page") Integer page,
    @RequestParam("size") Integer size) {
    return movimientoUseCase.getSaldosDTOByIdContrato(idCartera, page, size, false);
  }*/

  @ApiOperation(
    value = "Listado movimientos filtrados",
    notes = "Devuelve todos los movimientos filtrados")
  @GetMapping
  @Transactional
  public SaldoMovimientoDto getAllMovimientosFiltered(
    @RequestParam(value = "idCartera") Integer idCartera,
    @RequestParam(value = "idContrato", required = false) String idContrato,
    @RequestParam(value = "id", required = false) Integer id,
    @RequestParam(value = "fechaContable", required = false) String fechaContable,
    @RequestParam(value = "fechaValor", required = false) String fechaValor,
    @RequestParam(value = "tipoMovimiento", required = false) String tipoMovimiento,
    @RequestParam(value = "descripcion", required = false) String descripcion,
    @RequestParam(value = "importe", required = false) String importe,
    @RequestParam(value = "principalCapital", required = false) String principalCapital,
    @RequestParam(value = "interesesOrdinarios", required = false) String interesesOrdinarios,
    @RequestParam(value = "interesesDemora", required = false) String interesesDemora,
    @RequestParam(value = "comisiones", required = false) String comisiones,
    @RequestParam(value = "gastos", required = false) String gastos,
    @RequestParam(value = "afecta", required = false) Boolean afecta,
    @RequestParam(value = "activo", required = false) Boolean estado,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page)
    throws IllegalAccessException, NotFoundException {

    // controlamos si front necesita calcular saldo o no. Lo necesitarán para la primera llamada de
    // la vista de saldos-movimientos
    SaldoDto saldoDTO = null;

    ListWithCountDTO<MovimientoDTOToList> movimientosDTO =
      movimientoUseCase.getAllFilteredMovimientos(
        idContrato,
        null,
        idCartera,
        true,
        id,
        fechaContable,
        fechaValor,
        tipoMovimiento,
        descripcion,
        importe,
        principalCapital,
        interesesOrdinarios,
        interesesDemora,
        comisiones,
        gastos,
        afecta,
        estado,
        orderField,
        orderDirection,
        size,
        page);

    return new SaldoMovimientoDto(saldoDTO, movimientosDTO);
  }

  @ApiOperation(value = "Obtener movimiento", notes = "Devuelve el movimiento con id enviado")
  @GetMapping("/{idMovimiento}")
  public MovimientoDto getMovimientoById(
    @PathVariable("idMovimiento") Integer idMovimiento) throws IllegalAccessException {
    return movimientoUseCase.findMovimientoDTOById(idMovimiento);
  }

  @ApiOperation(
    value = "Crear movimiento",
    notes = "Añadir movimiento a base de datos para contrato con el id enviado")
  @PostMapping()
  @Transactional(rollbackFor = Exception.class)
  public void createMovimiento(
    @RequestParam(value = "contratos") Boolean contratos,
    @RequestParam(value = "idCartera") Integer idCartera,
    @RequestBody @Valid MovimientoInputDto movimientoInputDTO,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    movimientoUseCase.createContabilidad(idCartera, contratos,  movimientoInputDTO, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Actualizar movimiento", notes = "Actualizar el movimiento con id enviado")
  @PutMapping("/{idMovimiento}")
  @Transactional(rollbackFor = Exception.class)
  public MovimientoOutputDto updateMovimiento(
    @PathVariable("idMovimiento") Integer idMovimiento,
    @RequestBody @Valid MovimientoInputDto movimientoInputDTO)
    throws Exception {
    return movimientoUseCase.update(idMovimiento, true, movimientoInputDTO);
  }

  @ApiOperation(value = "Eliminar movimiento", notes = "Eliminar el movimiento con id enviado")
  @DeleteMapping("/{idMovimiento}")
  public void deleteMovimiento(
    @PathVariable("idMovimiento") Integer idMovimiento) throws Exception {
    movimientoUseCase.delete(idMovimiento, true);
  }

  @ApiOperation(value = "Descargar información de movimientos en Excel", notes = "Descargar información en Excel de los movimientos por id de contrato")
  @GetMapping("/excel")
  public ResponseEntity<InputStreamResource> getExcelMovimientoById(@RequestParam(value = "idCartera", required = false) Integer idCartera)
    throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(this.movimientoUseCase.findExcelMovimientoByIdCartera(idCartera));

    return excelExport.download(excel, idCartera + "_contabilidad_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @PostMapping("/carga")
  public void carga(
    @RequestParam(value = "idCartera") Integer idCartera,
    //@RequestParam(value = "idContrato") Integer idContrato,
    @RequestParam("file") MultipartFile file,
    @ApiIgnore CustomUserDetails principal) throws Exception {
    movimientoUseCase.carga(null, idCartera, true, file, (Usuario) principal.getPrincipal());
  }
}
