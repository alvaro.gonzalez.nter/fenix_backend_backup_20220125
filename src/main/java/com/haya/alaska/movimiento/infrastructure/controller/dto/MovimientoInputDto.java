package com.haya.alaska.movimiento.infrastructure.controller.dto;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MovimientoInputDto extends MovimientoDto{
  @NotNull
  private Integer tipoMovimiento;
  @NotNull
  private Integer descripcion;
  @NotNull
  private String sentido;
}
