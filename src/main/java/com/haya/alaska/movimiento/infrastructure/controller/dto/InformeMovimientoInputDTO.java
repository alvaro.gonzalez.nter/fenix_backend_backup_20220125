package com.haya.alaska.movimiento.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InformeMovimientoInputDTO {

  private Integer estadoExpediente;
  private Integer producto;
  private Integer estado;
  private Integer estadoContable;
  private Integer tipoMovimiento;
  private Date fechaValorInicial;
  private Date fechaValorFinal;
  private Date fechaContableInicial;
  private Date fechaContableFinal;

}
