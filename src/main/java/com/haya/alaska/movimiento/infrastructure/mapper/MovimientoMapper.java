package com.haya.alaska.movimiento.infrastructure.mapper;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.descripcion_movimiento.domain.DescripcionMovimiento;
import com.haya.alaska.descripcion_movimiento.infrastructure.repository.DescripcionMovimientoRepository;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoDto;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoInputDto;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoOutputDto;
import com.haya.alaska.movimiento.infrastructure.repository.MovimientoRepository;
import com.haya.alaska.shared.dto.MovimientoDTOToList;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.tipo_movimiento.infrastructure.repository.TipoMovimientoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component
public class MovimientoMapper {

  @Autowired TipoMovimientoRepository tipoMovimientoRepository;
  @Autowired DescripcionMovimientoRepository descripcionMovimientoRepository;
  @Autowired MovimientoRepository movimientoRepository;

  @Autowired
  Map<String, CatalogoRepository<? extends Catalogo, Integer>> catalogoRepository;

  public List<MovimientoDTOToList> listMovimientoToListMovimientoDTOToList(List<Movimiento> movimientos) {
    List<MovimientoDTOToList> movimientoDto = new ArrayList<>();
    for (Movimiento movimiento : movimientos) {
      movimientoDto.add(movimientoToMovimientoDTOToList(movimiento));
    }
    return movimientoDto;
  }

  public MovimientoDTOToList movimientoToMovimientoDTOToList(Movimiento movimiento) {
    MovimientoDTOToList movimientoDTOToList = new MovimientoDTOToList();
    BeanUtils.copyProperties(movimiento, movimientoDTOToList, "tipoMovimiento", "descripcion");

    movimientoDTOToList.setCartera(movimiento.getCartera() != null ? movimiento.getCartera().getNombre() : null);
    movimientoDTOToList.setIdContrato(movimiento.getContrato() != null ? movimiento.getContrato().getIdCarga() : null);
    movimientoDTOToList.setTipoMovimiento(movimiento.getTipo() != null ? new CatalogoMinInfoDTO(movimiento.getTipo()) : null);
    movimientoDTOToList.setDescripcion(movimiento.getDescripcion() != null ? new CatalogoMinInfoDTO(movimiento.getDescripcion()) : null);

    return movimientoDTOToList;
  }

  public MovimientoOutputDto entityToOutputDto(Movimiento movimiento) throws IllegalAccessException {
    MovimientoOutputDto movimientoOutputDto = new MovimientoOutputDto();
    BeanUtils.copyProperties(movimiento, movimientoOutputDto, "tipoMovimiento","descripcion","importe");
    movimientoOutputDto.setTipoMovimiento(movimiento.getTipo() != null ? new CatalogoMinInfoDTO(movimiento.getTipo()) : null);
    movimientoOutputDto.setDescripcion(movimiento.getDescripcion() != null ? new CatalogoMinInfoDTO(movimiento.getDescripcion()) : null);
    movimientoOutputDto.setImporte(movimiento.getImporte() != null ? Math.abs(movimiento.getImporte()) : null);
    movimientoOutputDto.setProducto(movimiento.getContrato() != null ? movimiento.getContrato().getProducto() != null ? new CatalogoMinInfoDTO(movimiento.getContrato().getProducto()) : null : null);

    Map historico = movimientoRepository.findHistoricoById(movimiento.getId()).orElse(null);
    if (historico != null) movimientoOutputDto.setHistorico(mapToOutputDto(historico));

    return movimientoOutputDto;
  }

  public MovimientoInputDto entityToInputDto(Movimiento movimiento){
    MovimientoInputDto movimientoInputDto = new MovimientoInputDto();
    BeanUtils.copyProperties(movimiento, movimientoInputDto, "tipoMovimiento","descripcion","importe");
    movimientoInputDto.setTipoMovimiento(movimiento.getTipo() != null ? movimiento.getTipo().getId() : null);
    movimientoInputDto.setDescripcion(movimiento.getDescripcion() != null ? movimiento.getDescripcion().getId() : null);
    movimientoInputDto.setImporte(movimiento.getImporte() != null ? Math.abs(movimiento.getImporte()) : null);

    return movimientoInputDto;
  }

  public Movimiento inputDtoToEntity(MovimientoInputDto movimientoDTO, Contrato contrato, Cartera cartera, Boolean contabilidad, Usuario usuario) throws InvalidCodeException {

    Movimiento movimiento = new Movimiento();
    BeanUtils.copyProperties(movimientoDTO, movimiento);
    movimiento.setContabilidad(contabilidad);
    //añadimos importe si no viene informado
    movimiento.setAbonado(false);
    Double interesesDemora = movimientoDTO.getInteresesDemora() != null ? movimientoDTO.getInteresesDemora() : 0.0;
    Double comisionesImpagados = movimientoDTO.getComisiones() != null ? movimientoDTO.getComisiones() : 0.0;
    Double gastosImpagados = movimientoDTO.getGastos() != null ? movimientoDTO.getGastos() : 0.0;
    Double interesesOrdinariosImpagados = movimientoDTO.getInteresesOrdinarios() != null ? movimientoDTO.getInteresesOrdinarios() : 0.0;
    Double principalCapital = movimientoDTO.getPrincipalCapital() != null ? movimientoDTO.getPrincipalCapital() : 0.0;
    Double sumatorio = interesesDemora + comisionesImpagados + gastosImpagados + interesesOrdinariosImpagados + principalCapital;
    if(movimientoDTO.getImporte() == null) {
      movimiento.setImporte(sumatorio);
    } else {
      if (sumatorio == 0 && movimiento.getImporte() == 0){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCodeException("Both the amount and the breakdown are 0:");
        else throw new InvalidCodeException("Tanto el importe como el desglose son 0:");
      }
      if (!sumatorio.equals(movimientoDTO.getImporte()) && sumatorio != 0.0){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCodeException("The amount does not match the breakdown.");
        else throw new InvalidCodeException("El importe no coincide con el desglose.");
      }
    }
    movimiento.setPrincipalPendienteVencer(movimientoDTO.getPrincipalPendienteVender());

    movimiento.setUsuario(usuario.getEmail());
    movimiento.setActivo(true);
    movimiento.setContrato(contrato);
    movimiento.setCartera(cartera);

    if (movimientoDTO.getTipoMovimiento() != null)
      movimiento.setTipo(tipoMovimientoRepository.findById(movimientoDTO.getTipoMovimiento()).orElse(null));
    if (movimientoDTO.getDescripcion() != null)
      movimiento.setDescripcion(descripcionMovimientoRepository.findById(movimientoDTO.getDescripcion()).orElse(null));

    return movimiento;
  }

  public Movimiento inputDtoToEntity(MovimientoInputDto movimientoDTO, Contrato contrato, Cartera cartera, String usuario) throws InvalidCodeException {

    Movimiento movimiento = new Movimiento();
    BeanUtils.copyProperties(movimientoDTO, movimiento, "abonado");
    //añadimos importe si no viene informado

    Double interesesDemora = movimientoDTO.getInteresesDemora() != null ? movimientoDTO.getInteresesDemora() : 0.0;
    Double comisionesImpagados = movimientoDTO.getComisiones() != null ? movimientoDTO.getComisiones() : 0.0;
    Double gastosImpagados = movimientoDTO.getGastos() != null ? movimientoDTO.getGastos() : 0.0;
    Double interesesOrdinariosImpagados = movimientoDTO.getInteresesOrdinarios() != null ? movimientoDTO.getInteresesOrdinarios() : 0.0;
    Double principalCapital = movimientoDTO.getPrincipalCapital() != null ? movimientoDTO.getPrincipalCapital() : 0.0;
    Double sumatorio = interesesDemora + comisionesImpagados + gastosImpagados + interesesOrdinariosImpagados + principalCapital;
    if(movimientoDTO.getImporte() == null) {
      movimiento.setImporte(sumatorio);
    } else {
      if (!sumatorio.equals(movimientoDTO.getImporte()) && sumatorio != 0.0){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCodeException("The amount does not match the breakdown.");
        else throw new InvalidCodeException("El importe no coincide con el desglose.");
      }
    }
    movimiento.setPrincipalPendienteVencer(movimientoDTO.getPrincipalPendienteVender());

    movimiento.setUsuario(usuario);
    movimiento.setActivo(true);
    movimiento.setContrato(contrato);
    movimiento.setCartera(cartera);

    if (movimientoDTO.getTipoMovimiento() != null)
      movimiento.setTipo(tipoMovimientoRepository.findById(movimientoDTO.getTipoMovimiento()).orElse(null));
    if (movimientoDTO.getDescripcion() != null)
      movimiento.setDescripcion(descripcionMovimientoRepository.findById(movimientoDTO.getDescripcion()).orElse(null));

    return movimiento;
  }

  public Movimiento updateFields(Movimiento movimiento, MovimientoInputDto movimientoDTO) {

    BeanUtils.copyProperties(movimientoDTO, movimiento, "id", "abonado");
    if (movimientoDTO.getTipoMovimiento() != null)
      movimiento.setTipo(tipoMovimientoRepository.findById(movimientoDTO.getTipoMovimiento()).orElse(null));
    if (movimientoDTO.getDescripcion() != null)
      movimiento.setDescripcion(descripcionMovimientoRepository.findById(movimientoDTO.getDescripcion()).orElse(null));

    return movimiento;
  }

  public Movimiento createMovimientoAux(MovimientoInputDto movimientoDTO, Movimiento movimiento) {

    Movimiento movimientoAux = new Movimiento();
    movimientoAux.setFechaValor(movimientoDTO.getFechaValor());
    movimientoAux.setAfecta(movimientoDTO.getAfecta());
    movimientoAux.setSentido(movimientoDTO.getSentido());
    if(movimientoDTO.getComisiones()!= null && !movimientoDTO.getComisiones().equals(movimiento.getComisiones()))
      movimientoAux.setComisiones(movimientoDTO.getComisiones());
    if(movimientoDTO.getInteresesDemora() != null && !movimientoDTO.getInteresesDemora().equals(movimiento.getInteresesDemora()))
      movimientoAux.setInteresesDemora(movimientoDTO.getInteresesDemora());
    if(movimientoDTO.getInteresesOrdinarios() != null && !movimientoDTO.getInteresesOrdinarios().equals(movimiento.getInteresesOrdinarios()))
      movimientoAux.setInteresesOrdinarios(movimientoDTO.getInteresesOrdinarios());
    if(movimientoDTO.getGastos()!= null && !movimientoDTO.getGastos().equals(movimiento.getGastos()))
      movimientoAux.setGastos(movimientoDTO.getGastos());

    return movimientoAux;
  }

  public MovimientoOutputDto mapToOutputDto(Map histMov) throws IllegalAccessException {
    MovimientoOutputDto movimientoOutputDTO = new MovimientoOutputDto();
    Field[] declaredFieldsOutputDTO = MovimientoOutputDto.class.getDeclaredFields();
    //MovimientoDto movimientoDTO = new MovimientoDto();
    Field[] declaredFieldsDTO = MovimientoDto.class.getDeclaredFields();
    for (Field declaredField : declaredFieldsOutputDTO) { // non entity fields
      declaredField.setAccessible(true);
      String name = declaredField.getName();
      if (histMov.get(name) == null) continue;
      //IMPORTANTE esta comprobacion valida si es de tipo double en cuyo caso lo trata
      if(declaredField.getType() == Double.class) {
        declaredField.set(movimientoOutputDTO, Double.valueOf(histMov.get(name).toString()));
      } else if (histMov.get(name).getClass().equals(Integer.class)){
        CatalogoRepository<? extends Catalogo, Integer> repo = catalogoRepository.get(name + "Repository");
        if (repo != null){
          Catalogo catalogo = repo.findById((Integer) histMov.get(name)).orElse(null);
          if (catalogo != null)
            declaredField.set(movimientoOutputDTO, new CatalogoMinInfoDTO(catalogo));
        } else if (name.equals("descripcion")){
          DescripcionMovimiento dm = descripcionMovimientoRepository.findById((Integer) histMov.get(name)).orElse(null);
          if (dm != null)
            declaredField.set(movimientoOutputDTO, new CatalogoMinInfoDTO(dm));
        } else {
          declaredField.set(movimientoOutputDTO, histMov.get(name));
        }
      } else {
        declaredField.set(movimientoOutputDTO, histMov.get(name));
      }
    }

    for (Field declaredField : declaredFieldsDTO) { // non entity fields
      declaredField.setAccessible(true);
      String name = declaredField.getName();
      if (histMov.get(name) == null)
        continue;
      if (histMov.get(name).getClass().equals(BigDecimal.class)){
        declaredField.set(movimientoOutputDTO,Double.parseDouble(histMov.get(name).toString())) ;
      } else {
        declaredField.set(movimientoOutputDTO, histMov.get(name));
      }
    }
    if(movimientoOutputDTO.getHistorico()!=null)
      movimientoOutputDTO.getHistorico().setHistorico(null);

    if (movimientoOutputDTO.getDeudaImpagada() != null && movimientoOutputDTO.getPrincipalCapital() != null)
    movimientoOutputDTO.setSaldoGestion(movimientoOutputDTO.getDeudaImpagada() + movimientoOutputDTO.getPrincipalCapital());

    return movimientoOutputDTO;
  }

}
