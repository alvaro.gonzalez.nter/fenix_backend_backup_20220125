package com.haya.alaska.movimiento.infrastructure.repository;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.movimiento.domain.Movimiento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface MovimientoRepository extends JpaRepository<Movimiento, Integer> {
  @Query(
    value =
      "SELECT " + //ContratoInterviniente
        "i.ID as id, " +
        "i.DES_ID_CARGA as idCarga, " +
        "i.ID_TIPO_MOVIMIENTO as tipoMovimiento, " +
        "i.ID_DESCRIPCION_MOVIMIENTO as descripcion, " +
        "i.DES_SENTIDO as sentido, " +

        "i.FCH_VALOR as fechaValor, " +
        "i.FCH_CONTABLE as fechaContable, " +
        "i.NUM_IMPORTE as importe, " +
        "i.NUM_PRINCIPAL_CAPITAL as principalCapital, " +
        "i.NUM_INTERESES_ORDINARIOS as interesesOrdinarios, " +
        "i.NUM_INTERESES_DEMORA as interesesDemora, " +
        "i.NUM_COMISIONES as comisiones, " +
        "i.NUM_GASTOS as gastos, " +
        "i.NUM_PRINCIPAL_PENDIENTE_VENCER as principalPendienteVender, " +
        "i.NUM_DEUDA_IMPAGADA as deudaImpagada, " +
        "i.DES_USUARIO as usuario, " +
        "i.DES_NUM_FACTURA as numeroFactura, " +
        "i.DES_DETALLE_GASTO as detalleGasto, " +
        "i.IND_AFECTA as afecta, " +
        "i.IND_ACTIVO as activo, " +
        "i.NUM_IMPORTE_RECUPERADO as importeRecuperado, " +
        "i.NUM_SALIDA_DUDOSO as salidaDudoso, " +
        "i.NUM_IMPORTE_FACTURABLE as importeFacturable, " +
        "i.DES_COMENTARIOS as comentarios, " +
        "i.IND_ABONADO as abonado " +

        "FROM HIST_MSTR_MOVIMIENTO i JOIN REGISTRO_HISTORICO r ON i.REV = r.id " +
        "WHERE i.ID = ?1 AND r.ID_USUARIO IS NULL ORDER BY timestamp DESC LIMIT 1",
    nativeQuery = true)
  Optional<Map> findHistoricoById(Integer id);

  List<Movimiento> findAllByContratoId(Integer idContrato);

  List<Movimiento> findAllByContratoIntervinientesIntervinienteIdAndContabilidadNotAndSentidoEquals(Integer idInterviniente, Boolean credibilidad, String sentido);

  List<Movimiento> findAllByContratoAndFechaValorGreaterThanEqualAndFechaValorLessThanEqual(Contrato contrato, Date inicio, Date fin);

  Optional<Movimiento> findByIdCarga(String idCarga);

  @Query(
          value =
                  "SELECT * "
                          + "FROM MSTR_MOVIMIENTO m "
                          + "INNER JOIN MSTR_CONTRATO c ON m.ID_CONTRATO  = c.ID "
                          + "INNER JOIN MSTR_EXPEDIENTE e ON e.ID = c.ID_EXPEDIENTE "
                          + "WHERE e.ID= :idExpediente "
                          + "AND m.FCH_VALOR BETWEEN :fechaInicio AND :fechaFin "
                          + "AND m.IND_ABONADO != 1",
          nativeQuery = true)
  List<Movimiento> findAllByIdExpedienteFechaValorBetween(
          @Param("idExpediente") Integer idExpediente,
          @Param("fechaInicio") Date fechaInicio,
          @Param("fechaFin") Date fechaFin);
}
