package com.haya.alaska.movimiento.infrastructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovimientoDto {

  @NotNull
  private Date fechaValor;
  @NotNull
  private Date fechaContable;
  private Double importe;
  @NotNull
  private Double principalCapital;
  private Double interesesOrdinarios;
  private Double interesesDemora;
  private Double comisiones;
  private Double gastos;
  private Double principalPendienteVender;
  private Double deudaImpagada;
  private Double saldoGestion;
  private String usuario;
  private String numeroFactura;
  private String detalleGasto;
  @NotNull
  private Boolean afecta;
  private Boolean activo;
  private Double importeRecuperado;
  private Double salidaDudoso;
  private Double importeFacturable;
  private String comentarios;
  private Boolean abonado;
}
