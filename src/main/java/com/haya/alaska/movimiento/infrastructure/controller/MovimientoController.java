package com.haya.alaska.movimiento.infrastructure.controller;

import com.haya.alaska.movimiento.application.MovimientoExcelUseCase;
import com.haya.alaska.movimiento.application.MovimientoUseCase;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoDto;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoInputDto;
import com.haya.alaska.movimiento.infrastructure.controller.dto.MovimientoOutputDto;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.infrastructure.controller.dto.SaldoDto;
import com.haya.alaska.saldo.infrastructure.mapper.SaldoMapper;
import com.haya.alaska.saldo.infrastructure.repository.SaldoRepository;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MovimientoDTOToList;
import com.haya.alaska.shared.dto.SaldoMovimientoDto;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientes/{idExpediente}/contratos/{idContrato}/movimientos")
public class MovimientoController {

  @Autowired MovimientoUseCase movimientoUseCase;
  @Autowired SaldoRepository saldoRepository;
  @Autowired
  SaldoMapper saldoMapper;

  @Autowired
  MovimientoExcelUseCase movimientoExcelUseCase;

  @ApiOperation(
          value = "Listado saldos",
          notes = "Devuelve el histórico de saldos para el id de contrato enviado")
  @GetMapping("/saldos")
  public ListWithCountDTO<SaldoDto> getAllSaldos(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @RequestParam("page") Integer page,
          @RequestParam("size") Integer size) {
    return movimientoUseCase.getSaldosDTOByIdContrato(idContrato, page, size);
  }

  @ApiOperation(value = "Obtener movimiento", notes = "Devuelve el movimiento con id enviado")
  @GetMapping("/{idMovimiento}")
  public MovimientoDto getMovimientoById(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @PathVariable("idMovimiento") Integer idMovimiento) throws IllegalAccessException {
    return movimientoUseCase.findMovimientoDTOById(idMovimiento);
  }

  @ApiOperation(
          value = "Crear movimiento",
          notes = "Añadir movimiento a base de datos para contrato con el id enviado")
  @PostMapping
  @Transactional(rollbackFor = Exception.class)
  public MovimientoOutputDto createMovimiento(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @RequestBody @Valid MovimientoInputDto movimientoInputDTO,
          @ApiIgnore CustomUserDetails principal)
          throws Exception {
    return movimientoUseCase.create(idContrato, null, false,  movimientoInputDTO, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Actualizar movimiento", notes = "Actualizar el movimiento con id enviado")
  @PutMapping("/{idMovimiento}")
  @Transactional(rollbackFor = Exception.class)
  public MovimientoOutputDto updateMovimiento(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @PathVariable("idMovimiento") Integer idMovimiento,
          @RequestBody @Valid MovimientoInputDto movimientoInputDTO)
          throws Exception {
    return movimientoUseCase.update(idMovimiento, false, movimientoInputDTO);
  }

  @ApiOperation(value = "Eliminar movimiento", notes = "Eliminar el movimiento con id enviado")
  @DeleteMapping("/{id}")
  public void deleteMovimiento(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @PathVariable("id") Integer id) throws Exception {
    movimientoUseCase.delete(id, false);
  }

  @ApiOperation(
          value = "Listado movimientos filtrados",
          notes = "Devuelve todos los movimientos filtrados")
  @GetMapping
  @Transactional
  public SaldoMovimientoDto getAllMovimientosFiltered(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @RequestParam(value = "id", required = false) Integer id,
          @RequestParam(value = "fechaContable", required = false) String fechaContable,
          @RequestParam(value = "fechaValor", required = false) String fechaValor,
          @RequestParam(value = "tipoMovimiento", required = false) String tipoMovimiento,
          @RequestParam(value = "descripcion", required = false) String descripcion,
          @RequestParam(value = "importe", required = false) String importe,
          @RequestParam(value = "principalCapital", required = false) String principalCapital,
          @RequestParam(value = "interesesOrdinarios", required = false) String interesesOrdinarios,
          @RequestParam(value = "interesesDemora", required = false) String interesesDemora,
          @RequestParam(value = "comisiones", required = false) String comisiones,
          @RequestParam(value = "gastos", required = false) String gastos,
          @RequestParam(value = "afecta", required = false) Boolean afecta,
          @RequestParam(value = "activo", required = false) Boolean estado,
          @RequestParam(value = "orderField", required = false) String orderField,
          @RequestParam(value = "orderDirection", required = false) String orderDirection,
          @RequestParam(value = "devolverSaldo") Boolean devolverSaldo,
          @RequestParam(value = "size") Integer size,
          @RequestParam(value = "page") Integer page)
    throws IllegalAccessException, NotFoundException {

    // controlamos si front necesita calcular saldo o no. Lo necesitarán para la primera llamada de
    // la vista de saldos-movimientos
    SaldoDto saldoDTO = null;

    if (devolverSaldo) {
      Saldo saldo = saldoRepository.findTopByContratoIdOrderByDiaDesc(idContrato).orElse(null);
      if (saldo != null) {
        saldoDTO = new SaldoDto(saldo);
        Map historicoById = saldoRepository.findHistoricoBySaldoId(saldo.getId());
        saldoDTO.setHistorico(saldoMapper.parseHistorico(historicoById));
      }
    }

    ListWithCountDTO<MovimientoDTOToList> movimientosDTO =
            movimientoUseCase.getAllFilteredMovimientos(
              null,
                    idContrato,
                    null,
                    false,
                    id,
                    fechaContable,
                    fechaValor,
                    tipoMovimiento,
                    descripcion,
                    importe,
                    principalCapital,
                    interesesOrdinarios,
                    interesesDemora,
                    comisiones,
                    gastos,
                    afecta,
                    estado,
                    orderField,
                    orderDirection,
                    size,
                    page);

    return new SaldoMovimientoDto(saldoDTO, movimientosDTO);
  }

  @ApiOperation(value = "Descargar información de movimientos en Excel", notes = "Descargar información en Excel de los movimientos por id de contrato")
  @GetMapping("/excel")
  public ResponseEntity<InputStreamResource> getExcelMovimientoById(@PathVariable Integer idContrato)
          throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(this.movimientoUseCase.findExcelMovimientoByIdContrato(idContrato, false));

    return excelExport.download(excel, idContrato + "_movimientos_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @PostMapping("/carga")
  public void carga(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @RequestParam("file") MultipartFile file,
          @ApiIgnore CustomUserDetails principal) throws Exception {
    movimientoUseCase.carga(idContrato, null, false, file, (Usuario) principal.getPrincipal());
  }

  @PostMapping("/neteo")
  public ResponseEntity<InputStreamResource> neteo(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @RequestParam("file") MultipartFile file) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(movimientoUseCase.neteo(idContrato, file));

    return excelExport.download(excel, idContrato + "_movimientos_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

}
