package com.haya.alaska.movimiento.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovimientoOutputDto extends MovimientoDto{
  private Integer id;
  private String idCarga;
  private CatalogoMinInfoDTO tipoMovimiento;
  private CatalogoMinInfoDTO descripcion;
  private String sentido;
  private MovimientoOutputDto historico;
  private CatalogoMinInfoDTO producto;
}
