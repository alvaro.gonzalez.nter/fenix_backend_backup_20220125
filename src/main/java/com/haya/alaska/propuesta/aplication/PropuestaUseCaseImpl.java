package com.haya.alaska.propuesta.aplication;

import com.haya.alaska.acuerdo_pago.aplication.AcuerdoPagoUseCase;
import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoInputDTO;
import com.haya.alaska.acuerdo_pago.infrastructure.repository.AcuerdoPagoRepository;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.infrastructure.repository.AsignacionExpedienteRepository;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.asignacion_expediente_posesion_negociada.repository.AsignacionExpedientePosesionNegociadaRepository;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.controller.dto.BienOutputDto;
import com.haya.alaska.bien.infrastructure.mapper.BienMapper;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.domain.Catalogo_;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoEnlaceOutputDTO;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.comentario_propuesta.domain.ComentarioPropuesta;
import com.haya.alaska.comentario_propuesta.infrastructure.repository.ComentarioPropuestaRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estado_propuesta.infrastructure.repository.EstadoPropuestaRepository;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada_;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.formalizacion.aplication.FormalizacionUseCase;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.FormalizacionOutputDTO;
import com.haya.alaska.formalizacion.infrastructure.repository.FormalizacionRepository;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.importe_propuesta.infrastructure.repository.ImportePropuestaRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.application.GestorDocumentalUseCase;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.domain.enums.TipoEntidadEnum;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.integraciones.pbc.infrastructure.controller.dto.UpdateResultadoPBCInputDTO;
import com.haya.alaska.integraciones.pbc.infrastructure.soap.SoapClient;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteDTOToList;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.plazo.repository.PlazoRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.domain.Propuesta_;
import com.haya.alaska.propuesta.infrastructure.controller.dto.*;
import com.haya.alaska.propuesta.infrastructure.mapper.ImportePropuestaMapper;
import com.haya.alaska.propuesta.infrastructure.mapper.PropuestaMapper;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.propuesta_bien.infrastructure.PropuestaBienRepository;
import com.haya.alaska.resolucion_propuesta.infrastructure.repository.ResolucionPropuestaRepository;
import com.haya.alaska.resultado_pbc.infrastructure.repository.ResultadoPBCRepository;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.infrastructure.repository.SaldoRepository;
import com.haya.alaska.sancion_propuesta.infrastructure.repository.SancionPropuestaRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoContenedorInputDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.InvalidCatalogException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subestado_formalizacion.domain.SubestadoFormalizacion;
import com.haya.alaska.subestado_formalizacion.infrastructure.repository.SubestadoFormalizacionRepository;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import com.haya.alaska.subtipo_propuesta.infrastructure.repository.SubtipoPropuestaRepository;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.tipo_propuesta.infrastructure.repository.TipoPropuestaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class PropuestaUseCaseImpl implements PropuestaUseCase {
  @PersistenceContext
  private EntityManager entityManager;

  @Autowired
  PropuestaMapper propuestaMapper;
  @Autowired
  EventoUseCase eventoUseCase;
  @Autowired
  EventoUtil eventoUtil;
  @Autowired
  PropuestaRepository propuestaRepository;
  @Autowired
  FormalizacionRepository formalizacionRepository;
  @Autowired
  PropuestaBienRepository propuestaBienRepository;
  @Autowired
  TipoPropuestaRepository tipoPropuestaRepository;
  @Autowired
  SubtipoPropuestaRepository subtipoPropuestaRepository;
  /*@Autowired
  ServicioGestorDocumental servicioGestorDocumental;*/
  @Autowired
  EstadoPropuestaRepository estadoPropuestaRepository;
  @Autowired
  SubestadoFormalizacionRepository subestadoFormalizacionRepository;
  @Autowired
  SancionPropuestaRepository sancionPropuestaRepository;
  @Autowired
  ResolucionPropuestaRepository resolucionPropuestaRepository;
  @Autowired
  ContratoRepository contratoRepository;
  @Autowired
  BienRepository bienRepository;
  @Autowired
  BienMapper bienMapper;
  @Autowired
  BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  IntervinienteRepository intervinienteRepository;
  @Autowired
  ImportePropuestaRepository importePropuestaRepository;
  @Autowired
  FormalizacionUseCase formalizacionUseCase;
  @Autowired
  ImportePropuestaMapper importePropuestaMapper;
  @Autowired
  AcuerdoPagoRepository acuerdoPagoRepository;
  @Autowired
  AcuerdoPagoUseCase acuerdoPagoUseCase;
  @Autowired
  SaldoRepository saldoRepository;
  @Autowired
  ComentarioPropuestaRepository comentarioPropuestaRepository;
  @Autowired
  OcupanteRepository ocupanteRepository;
  @Autowired
  AsignacionExpedienteRepository asignacionExpedienteRepository;
  @Autowired
  AsignacionExpedientePosesionNegociadaRepository asignacionExpedientePosesionNegociadaRepository;
  @Autowired
  ServicioGestorDocumental servicioGestorDocumental;
  @Autowired
  private ExpedienteRepository expedienteRepository;
  @Autowired
  ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  PlazoRepository plazoRepository;
  @Autowired
  private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired
  private GestorDocumentalUseCase gestorDocumentalUseCase;
  @Autowired
  private ResultadoPBCRepository resultadoPBCRepository;
  @Autowired
  SoapClient soapClient;

  public ListWithCountDTO getFilteredPropuestas(
    Integer idExpediente,
    Boolean posesionNegociada,
    Integer idPropuesta,
    String fechaAlta,
    String usuario,
    String clase,
    String tipo,
    String subtipoPropuesta,
    String estadoPropuesta,
    String sancion,
    String resolucion,
    String fechaUltimoCambio,
    String importeTotal,
    String orderField,
    String orderDirection,
    Integer size,
    Integer page) {

    List<Propuesta> listaPropuestas =
      getFilteredPropuestas(
        idExpediente,
        posesionNegociada,
        idPropuesta,
        fechaAlta,
        usuario,
        clase,
        tipo,
        subtipoPropuesta,
        estadoPropuesta,
        sancion,
        resolucion,
        fechaUltimoCambio,
        importeTotal,
        orderField,
        orderDirection);
    List<Propuesta> propuestas =
      listaPropuestas.stream().skip(size * page).limit(size).collect(Collectors.toList());

    List<PropuestaDTOToList> propuestaDTOList = new ArrayList<>();

    propuestas.forEach(
      propuesta -> {
        PropuestaDTOToList propuestaDtoToList = new PropuestaDTOToList();
        propuestaMapper.createPropuestaDTOToList(propuesta, propuestaDtoToList, posesionNegociada);
        propuestaDTOList.add(propuestaDtoToList);
      });
    return new ListWithCountDTO(propuestaDTOList, listaPropuestas.size());
  }

  public ListWithCountDTO getAllPropuestasWithBienesToInformes(
    Integer idExpediente,
    Boolean posesionNegociada) {

    List<Propuesta> listaPropuestas =
      getFilteredPropuestas(
        idExpediente,
        posesionNegociada,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null);

    List<Propuesta> propuestas = listaPropuestas.stream().filter(
      prop -> prop.getBienes().size() > 0).collect(Collectors.toList());

    List<PropuestaDTOToList> propuestaDTOList = new ArrayList<>();

    propuestas.forEach(
      propuesta -> {
        PropuestaDTOToList propuestaDtoToList = new PropuestaDTOToList();
        propuestaMapper.createPropuestaDTOToList(propuesta, propuestaDtoToList, posesionNegociada);
        propuestaDTOList.add(propuestaDtoToList);
      });
    return new ListWithCountDTO(propuestaDTOList, propuestas.size());
  }

  public ListWithCountDTO getAllPropuestasWithBienesOcupantes(
    Integer idExpediente,
    Boolean posesionNegociada) {

    List<Propuesta> listaPropuestas =
      getFilteredPropuestas(
        idExpediente,
        posesionNegociada,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null);

    List<Propuesta> propuestas = listaPropuestas.stream().filter(
      prop -> prop.getBienes().size() > 0).collect(Collectors.toList());

    List<Propuesta> propuestas2 = new ArrayList<>();

    if (posesionNegociada == true) {
      propuestas2 = listaPropuestas.stream().filter(
        prop -> prop.getOcupantes().size() > 0).collect(Collectors.toList());
    } else {
      propuestas2 = listaPropuestas.stream().filter(
        prop -> prop.getIntervinientes().size() > 0).collect(Collectors.toList());
    }

    List<PropuestaDTOToList> propuestaDTOList = new ArrayList<>();

    propuestas2.forEach(
      propuesta -> {
        PropuestaDTOToList propuestaDtoToList = new PropuestaDTOToList();
        propuestaMapper.createPropuestaDTOToList(propuesta, propuestaDtoToList, posesionNegociada);
        propuestaDTOList.add(propuestaDtoToList);
      });
    return new ListWithCountDTO(propuestaDTOList, propuestas2.size());
  }

  public ListWithCountDTO getAllPropuestasWithBienes(
    Integer idExpediente,
    Boolean posesionNegociada) throws NotFoundException {

    List<Propuesta> listaPropuestas = propuestaRepository.findAllByExpedienteId(idExpediente);

    List<Propuesta> propuestasPre = new ArrayList<>();
    for (Propuesta p : listaPropuestas) {
      List<PropuestaBien> pbs = new ArrayList<>(p.getBienes());
      List<PropuestaBien> pbsFinal = new ArrayList<>();
      for (PropuestaBien pb : pbs) {
        Bien b = pb.getBien();
        if (!b.getTasaciones().isEmpty() && !b.getValoraciones().isEmpty() && !b.getCargas().isEmpty()) {
          pbsFinal.add(pb);
          propuestasPre.add(p);
        }
      }
      p.setBienes(new HashSet<>(pbsFinal));
    }

    List<Propuesta> propuestas = propuestasPre.stream().distinct().filter(p -> {
      if (p.getContratos().isEmpty()) return false;
      if (p.getIntervinientes().isEmpty()) return false;
      return !p.getAcuerdosPago().isEmpty();
    }).collect(Collectors.toList());

    List<PropuestaDTOToList> propuestaDTOList = new ArrayList<>();

    propuestas.forEach(
      propuesta -> {
        PropuestaDTOToList propuestaDtoToList = new PropuestaDTOToList();
        propuestaMapper.createPropuestaDTOToList(propuesta, propuestaDtoToList, posesionNegociada);
        propuestaDTOList.add(propuestaDtoToList);
      });

    for (PropuestaDTOToList p : propuestaDTOList) {
      FormalizacionOutputDTO formalizacion = formalizacionUseCase.findByPropuestaId(p.getId(), false);
      p.setFormalizacion(formalizacion);
    }

    return new ListWithCountDTO(propuestaDTOList, propuestas.size());
  }

  public PropuestaDTO getPropuesta(Integer idExpediente, Integer idPropuesta, Boolean posesionNegociada) throws NotFoundException, InvalidCatalogException {
    PropuestaDTO propuestaDto = new PropuestaDTO();
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));

    // buscamos el saldo de los contratos asignados a la propuesta
    for (Contrato contrato : propuesta.getContratos()) {
      Saldo saldo = getSaldoByIdContrato(contrato.getId());
      if (saldo == null) continue;
      Set<Saldo> saldos = new HashSet<>();
      saldos.add(saldo);
      contrato.setSaldos(saldos);
    }
    propuestaMapper.actualizaImporteTotal(propuesta, false);
    propuestaDto = propuestaMapper.createPropuestaDTO(propuesta, propuestaDto, posesionNegociada);
    return propuestaDto;
  }

  public Saldo getSaldoByIdContrato(Integer idContrato) {
    List<Saldo> saldos = saldoRepository.findAllByContratoIdOrderByDiaDesc(idContrato);
    if (saldos.isEmpty()) return null;
    // obtenemos el último saldo almacenado en bbdd que es el saldo válido para el contrato indicado
    return saldos.get(0);
  }

  public PropuestaDTO updateEstadoPropuesta(
    PropuestaInputDTO propuestaInputDTO, Integer idExpediente, Integer idPropuesta, Usuario user)
    throws NotFoundException, InvalidCatalogException, RequiredValueException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    var estadoPropuesta =
      estadoPropuestaRepository
        .findById(propuestaInputDTO.getEstadoPropuesta())
        .orElseThrow(
          () ->
            new NotFoundException("Estado Propuesta", propuestaInputDTO.getEstadoPropuesta()));
    if (propuestaInputDTO.getSancionPropuesta() != null) {
      var sancionPropuesta =
        sancionPropuestaRepository
          .findById(propuestaInputDTO.getSancionPropuesta())
          .orElseThrow(
            () ->
              new NotFoundException("Sanción Propuesta", propuestaInputDTO.getSancionPropuesta()));
      propuesta.setSancionPropuesta(sancionPropuesta);
    }

    if (propuestaInputDTO.getResolucionPropuesta() != null) {
      var resolucionPropuesta =
        resolucionPropuestaRepository
          .findById(propuestaInputDTO.getResolucionPropuesta())
          .orElseThrow(
            () ->
              new NotFoundException("Resolución Propuesta ", propuestaInputDTO.getResolucionPropuesta()));
      propuesta.setResolucionPropuesta(resolucionPropuesta);
    }

    propuesta.setEstadoPropuesta(estadoPropuesta);
    if (estadoPropuesta.getCodigo().equals("PFI"))
      eventoUseCase.alertaPteFirma(propuesta, user);
    if (Objects.nonNull(propuesta.getExpediente())
        && ((Objects.nonNull(propuesta.getExpediente().getCartera().getIdCarga())
                && propuesta.getExpediente().getCartera().getIdCarga().equals("5202"))
            || propuesta.getExpediente().getCartera().getNombre().equals("SAREB"))
        && propuesta.getSubtipoPropuesta().getPbc()) {
      if (estadoPropuesta.getCodigo().equals("PCL")
          || estadoPropuesta.getCodigo().equals("AC")
          || estadoPropuesta.getCodigo().equals("PFI")
          || estadoPropuesta.getCodigo().equals("FOR")
          || estadoPropuesta.getCodigo().equals("DEN")
          || estadoPropuesta.getCodigo().equals("DES")
          || estadoPropuesta.getCodigo().equals("ANU")
          || estadoPropuesta.getCodigo().equals("INCU")) {
        soapClient.notificarPBC(propuesta);
      }
    }
    if (getCodigosAnulado().contains(estadoPropuesta.getCodigo()))
      eventoUtil.cerrarEventosPropuesta(idPropuesta);


    propuestaMapper.actualizaImporteTotal(propuesta, false);
    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(propuesta.getId());
    return propuestaMapper.createPropuestaDTO(propuesta, new PropuestaDTO(), false);
  }

  private List<String> getCodigosAnulado(){
    List<String> codigos = new ArrayList<>();

    codigos.add("ANU");

    return codigos;
  }

  public PropuestaDTO updateCamposSareb(
      Integer idExpediente, Integer idPropuesta, Double importeColabora, Integer numColabora)
      throws NotFoundException, InvalidCatalogException {
    var propuesta =
        propuestaRepository
            .findById(idPropuesta)
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "No encontrada propuesta: "
                            + idPropuesta
                            + " para el expediente: "
                            + idExpediente));

    propuesta.setImporteColabora(importeColabora);
    propuesta.setNumColabora(numColabora);

    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(propuesta.getId());
    return propuestaMapper.createPropuestaDTO(propuesta, new PropuestaDTO(), false);
  }

  public PropuestaDTO updateCamposCerberusPBC(
      Integer idExpediente,
      Integer idPropuesta,
      Integer numAdvisoryNote,
      String fechaEnvioCES,
      String fechaEnvioWorkingGroup,
      String fechaRecepcionAprobacionAdvisoryNote)
    throws NotFoundException, InvalidCatalogException, ParseException {
    var propuesta =
        propuestaRepository
            .findById(idPropuesta)
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "No encontrada propuesta: "
                            + idPropuesta
                            + " para el expediente: "
                            + idExpediente));

    if (Objects.nonNull(numAdvisoryNote)) {
      propuesta.setNumAdvisoryNote(numAdvisoryNote);
    }
    if (Objects.nonNull(fechaEnvioCES)) {
      propuesta.setFechaEnvioCES(new SimpleDateFormat("dd/MM/yyyy").parse(fechaEnvioCES));
    }
    if (Objects.nonNull(fechaEnvioWorkingGroup)) {
      propuesta.setFechaEnvioWorkingGroup(
          new SimpleDateFormat("dd/MM/yyyy").parse(fechaEnvioWorkingGroup));
    }
    if (Objects.nonNull(fechaRecepcionAprobacionAdvisoryNote)) {
      propuesta.setFechaRecepcionAprobacionAdvisoryNote(
          new SimpleDateFormat("dd/MM/yyyy").parse(fechaRecepcionAprobacionAdvisoryNote));
    }

    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(propuesta.getId());
    return propuestaMapper.createPropuestaDTO(propuesta, new PropuestaDTO(), false);
  }

  public PropuestaDTO updateContratosPropuesta(
    PropuestaInputDTO propuestaInputDTO, Integer idExpediente, Integer idPropuesta)
    throws NotFoundException, InvalidCatalogException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    Set<Contrato> contratoNew = new HashSet<>();
    if (propuestaInputDTO.getContratos().size() > 0) {
      for (Integer contratoId : propuestaInputDTO.getContratos()) {
        contratoNew.add(
          contratoRepository
            .findById(contratoId)
            .orElseThrow(
              () -> new NotFoundException("Contrato", contratoId)));
      }
    }
    propuesta.setContratos(contratoNew);
    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(propuesta.getId());
    return propuestaMapper.createPropuestaDTO(propuesta, new PropuestaDTO(), false);
  }

  public PropuestaDTO updateBienesPropuesta(
    PropuestaInputDTO propuestaInputDTO, Integer idExpediente, Integer idPropuesta, Boolean posesionNegociada)
    throws NotFoundException, InvalidCatalogException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    List<PropuestaBien> bienesNew = new ArrayList<>();


    for (BienPrecioPropuestaInputDTO bpp : propuestaInputDTO.getBienes()) {
      Integer bienId = bpp.getIdBien();
      if (!posesionNegociada) {
        Bien bien = bienRepository.findById(bienId).orElseThrow(() -> new NotFoundException("Bien", bienId));
        for (Contrato cont : propuesta.getContratos()) {
          ContratoBien contratoBien = bien.getContratoBien(cont.getId());
          if (contratoBien != null) {
            var pb = new PropuestaBien();
            pb.setBien(bien);
            pb.setPropuesta(propuesta);
            var contrato = contratoBien.getContrato();
            pb.setContrato(contrato);
            pb.setPrecioMinimoVenta(bpp.getPrecioMinimoVenta());
            pb.setPrecioWebVenta(bpp.getPrecioWebVenta());
            bienesNew.add(pb);
            break;
          }
        }
      } else {
        BienPosesionNegociada bpn = bienPosesionNegociadaRepository.findById(bienId).orElseThrow(() ->
          new NotFoundException("Bien Posesión Negociada", bienId));
        Bien bien = bpn.getBien();
        PropuestaBien propuestaBien = new PropuestaBien();
        propuestaBien.setBien(bien);
        propuestaBien.setPropuesta(propuesta);
        bienesNew.add(propuestaBien);
      }

      propuesta.setBienes(new HashSet<>(bienesNew));
    }


    /*if (propuestaInputDTO.getBienes().size() > 0) {
      for (Integer bienId : propuestaInputDTO.getBienes()) {
        var pb = initPropuestaBien(propuesta, bienId);
        bienesNew.add(pb);
      }
    }
    propuesta.setBienes(bienesNew);*/
    propuestaRepository.saveAndFlush(propuesta);
    actualizarFechaUltimoCambio(propuesta.getId());
    return propuestaMapper.createPropuestaDTO(propuesta, new PropuestaDTO(), posesionNegociada);
  }

  private PropuestaBien initPropuestaBien(Propuesta propuesta, Integer bienId) throws NotFoundException {
    var propuestaBien = new PropuestaBien();
    var bien = bienRepository
      .findById(bienId)
      .orElseThrow(() -> new NotFoundException("Bien", bienId));
    propuestaBien.setBien(bien);
    propuestaBien.setPropuesta(propuesta);
    return propuestaBien;
  }

  public PropuestaDTO updateIntervinientesPropuesta(
    PropuestaInputDTO propuestaInputDTO, Integer idExpediente, Integer idPropuesta, Boolean posesionNegociada)
    throws NotFoundException, InvalidCatalogException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    if (!posesionNegociada) {
      Set<Interviniente> intervinienteNew = new HashSet<>();
      if (propuestaInputDTO.getIntervinientes().size() > 0) {
        for (Integer intervinienteId : propuestaInputDTO.getIntervinientes()) {
          intervinienteNew.add(
            intervinienteRepository
              .findById(intervinienteId)
              .orElseThrow(
                () ->
                  new NotFoundException("Interviniente", intervinienteId)));
        }
      }
      propuesta.setIntervinientes(intervinienteNew);
    } else {
      Set<Ocupante> ocupantesNew = new HashSet<>();
      for (Integer id : propuestaInputDTO.getIntervinientes()) {
        Ocupante o = ocupanteRepository.findById(id).orElse(null);
        if (o != null) {
          ocupantesNew.add(o);
        }
      }
      propuesta.setOcupantes(ocupantesNew);
      /*propuesta.setOcupantes(new HashSet<>());
      propuestaRepository.saveAndFlush(propuesta);
      for (Integer id : propuestaInputDTO.getIntervinientes()){
        Ocupante o = ocupanteRepository.findById(id).orElse(null);
        if (o != null){
          Set<Propuesta> propuestas = o.getPropuestas().stream().filter(p -> p.getId().equals(propuesta.getId())).collect(Collectors.toSet());
          if (propuestas.isEmpty()) o.getPropuestas().add(propuesta);
          ocupanteRepository.save(o);
        }
      }*/
    }

    Propuesta propuestaSaved = propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(idPropuesta);
    return propuestaMapper.createPropuestaDTO(propuestaSaved, new PropuestaDTO(), posesionNegociada);
  }

  public void updateResultadoPBC(Integer idPropuesta, UpdateResultadoPBCInputDTO updateResultadoPBCInputDTO, Usuario user)
    throws NotFoundException, RequiredValueException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException(
              "Propuesta"
                , idPropuesta));

    var resultadoPBC =
      resultadoPBCRepository
        .findByCodigo(updateResultadoPBCInputDTO.getResultadoPBC())
        .orElseThrow(
          () ->
            new NotFoundException(
              "Resultado PBC"
                , updateResultadoPBCInputDTO.getResultadoPBC()));

    propuesta.setResultadoPBC(resultadoPBC);
    propuesta.setFechaResolucionPBC(updateResultadoPBCInputDTO.getFechaResolucionPBC());

    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(propuesta.getId());
    eventoUseCase.crearEventoCierrePBC(idPropuesta, user);
  }

  public PropuestaDTO addImportesPagoPropuesta(
    ImportePropuestaInputDTO importePropuestaInputDTO, Integer idExpediente, Integer idPropuesta, Boolean posesionNegociada)
    throws NotFoundException, InvalidCatalogException, RequiredValueException {
    if (importePropuestaInputDTO.getContratos() == null || importePropuestaInputDTO.getContratos().size() == 0 || importePropuestaInputDTO.getImporteEntregaDineraria() == null || importePropuestaInputDTO.getImporteEntregaNoDineraria() == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("It is necessary to associate a contract with the amount");
      else throw new RequiredValueException("Es necesario asociar algún contrato al importe");
    }
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    Set<ImportePropuesta> importesPropuesta = propuesta.getImportesPropuestas();
    var importePropuesta = importePropuestaMapper.parse(importePropuestaInputDTO, posesionNegociada);
    var importeSaved = importePropuestaRepository.saveAndFlush(importePropuesta);
    importesPropuesta.add(importeSaved);
    propuesta.setImportesPropuestas(importesPropuesta);
    propuestaMapper.actualizaImporteTotal(propuesta, false);
    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(idPropuesta);
    return propuestaMapper.createPropuestaDTO(propuesta, new PropuestaDTO(), false);
  }

  private List<String> perfilesFechaFormalizacion() {
    List<String> result = new ArrayList<>();
    result.add("Responsable de cartera");
    result.add("Supervisor");
    result.add("Gestor formalización");
    result.add("Soporte");
    return result;
  }

  @Override
  public Boolean actualizarFechaFormalizacion(Integer idPropuesta, Date fecha, Usuario user) throws NotFoundException, ForbiddenException {
    if (user.esAdministrador() || (user.getPerfil() != null && perfilesFechaFormalizacion().contains(user.getPerfil().getNombre()))){
      Propuesta propuesta =
        propuestaRepository
          .findById(idPropuesta)
          .orElseThrow(
            () ->
              new NotFoundException("Propuesta", idPropuesta));
      propuesta.setFechaResolucion(fecha);
      propuestaRepository.save(propuesta);
    } else {
      String error = "El usuario no tiene permitido modificar la fecha de formalización";
      Locale loc = LocaleContextHolder.getLocale();
      String defaultLocal = loc.getLanguage();
      if (defaultLocal.equals("en"))
        error = "The user can't update the formalization date";
      throw new ForbiddenException(error);
    }

    return true;
  }

  @Override
  public void deleteAcuerdosPagoPropuesta(Integer idExpediente, Integer idPropuesta, List<Integer> acuerdos)
    throws NotFoundException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    Set<AcuerdoPago> acuerdosPropuesta = propuesta.getAcuerdosPago();
    boolean swEnc = false;
    for (AcuerdoPago acuerdo : acuerdosPropuesta) {
      for (var idAcuerdo : acuerdos) {
        if (acuerdo.getId().equals(idAcuerdo)) {
          acuerdo.setActivo(false);
          acuerdoPagoRepository.save(acuerdo);
        }
      }
    }
    propuesta.setAcuerdosPago(acuerdosPropuesta);
    propuestaMapper.actualizaImporteTotal(propuesta, false);
    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(idPropuesta);
  }

  @Override
  public void deleteImportesPagoPropuesta(Integer idExpediente, Integer idPropuesta, List<Integer> importes)
    throws NotFoundException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    Set<ImportePropuesta> importesPropuesta = propuesta.getImportesPropuestas();
    boolean swEnc = false;
    for (ImportePropuesta importePropuesta : importesPropuesta) {
      for (var idImporte : importes) {
        if (importePropuesta.getId().equals(idImporte)) {
          importePropuesta.setActivo(false);
          importePropuestaRepository.save(importePropuesta);
        }
      }
    }
    propuesta.setImportesPropuestas(importesPropuesta);
    propuestaMapper.actualizaImporteTotal(propuesta, false);
    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(idPropuesta);
  }

  public void bajaPropuesta(Integer idExpediente, Integer idPropuesta) throws NotFoundException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    propuesta.setActivo(false);
    actualizarFechaUltimoCambio(idPropuesta);
    eventoUtil.eliminarEventosPropuesta(propuesta);
    propuestaRepository.save(propuesta);
  }

  public PropuestaDTO addAcuerdoPagoPropuesta(
    AcuerdoPagoInputDTO acuerdoPagoInputDTO, Integer idExpediente, Integer idPropuesta, Boolean posesionNegociada)
    throws NotFoundException, InvalidCatalogException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    Set<AcuerdoPago> acuerdosPago = propuesta.getAcuerdosPago();

    var acuerdoPagoSaved =
      acuerdoPagoUseCase.createAcuerdoPago(
        idExpediente, acuerdoPagoInputDTO, propuesta.getUsuario(), posesionNegociada);
    acuerdosPago.add(acuerdoPagoSaved);
    propuesta.setAcuerdosPago(acuerdosPago);
    propuestaMapper.actualizaImporteTotal(propuesta, false);
    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(idPropuesta);
    return propuestaMapper.createPropuestaDTO(propuesta, new PropuestaDTO(), posesionNegociada);
  }

  private List<AcuerdoPago> getAcuerdosPagoSeleccionados(
    Set<AcuerdoPago> acuerdosPago, List<ContratoImporteAcuerdoInputDTO> contratos)
    throws NotFoundException {
    List<AcuerdoPago> acuerdosSeleccionados = new ArrayList<>();
    for (ContratoImporteAcuerdoInputDTO conImpAc : contratos) {
      var acuerdoPago = acuerdoPagoRepository.findById(conImpAc.getIdAux()).orElseThrow(() ->
        new NotFoundException("Acuerdo de Pago", conImpAc.getIdAux()));
      if (acuerdosPago.contains(acuerdoPago)) {
        acuerdosSeleccionados.add(acuerdoPago);
      }
    }
    return acuerdosSeleccionados;
  }

  private List<ImportePropuesta> getImportesPropuestaSeleccionados(
    Set<ImportePropuesta> importes, List<ContratoImporteAcuerdoInputDTO> contratos)
    throws NotFoundException {
    List<ImportePropuesta> importesSeleccionados = new ArrayList<>();
    for (ContratoImporteAcuerdoInputDTO conImpAc : contratos) {
      var importe = importePropuestaRepository.findById(conImpAc.getIdAux()).get();
      if (importes.contains(importe)) {
        importesSeleccionados.add(importe);
      }
    }
    return importesSeleccionados;
  }

  private List<Contrato> getContratosSeleccionados(List<ContratoImporteAcuerdoInputDTO> contratos)
    throws NotFoundException {
    List<Contrato> contratosSeleccionados = new ArrayList<>();
    for (ContratoImporteAcuerdoInputDTO conImpAc : contratos) {
      var contrato = contratoRepository.findById(conImpAc.getIdContrato()).orElseThrow(() ->
        new NotFoundException("Contrato", conImpAc.getIdContrato()));
      contratosSeleccionados.add(contrato);
    }
    return contratosSeleccionados;
  }

  private List<BienPosesionNegociada> getBienesSeleccionados(List<ContratoImporteAcuerdoInputDTO> contratos)
    throws NotFoundException {
    List<BienPosesionNegociada> bienesSeleccionados = new ArrayList<>();
    for (ContratoImporteAcuerdoInputDTO conImpAc : contratos) {
      var bien = bienPosesionNegociadaRepository.findById(conImpAc.getIdContrato()).orElseThrow(() ->
        new NotFoundException("Contrato", conImpAc.getIdContrato()));
      bienesSeleccionados.add(bien);
    }
    return bienesSeleccionados;
  }

  public void deleteContratoEnImportePropuesta(
    Integer idExpediente, Integer idPropuesta, List<ContratoImporteAcuerdoInputDTO> contratos, Boolean pn)
    throws NotFoundException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    Set<ImportePropuesta> importes = propuesta.getImportesPropuestas();

    // buscamos los acuerdos de pago seleccionados
    List<ImportePropuesta> importesSeleccionados = getImportesPropuestaSeleccionados(importes, contratos);

    // buscamos los contratos seleccionados
    if (pn) {
      List<BienPosesionNegociada> bienesSeleccionados = getBienesSeleccionados(contratos);

      for (ImportePropuesta importe : importesSeleccionados) {
        Set<BienPosesionNegociada> bienesImporte = importe.getBienes();
        for (BienPosesionNegociada contrato : bienesSeleccionados) {
          if (bienesImporte.contains(contrato)) {
            bienesImporte.remove(contrato);
            importe.setBienes(bienesImporte);
          }
        }
      }
    } else {
      List<Contrato> contratosSeleccionados = getContratosSeleccionados(contratos);

      for (ImportePropuesta importe : importesSeleccionados) {
        Set<Contrato> contratosAcuerdo = importe.getContratos();
        for (Contrato contrato : contratosSeleccionados) {
          if (contratosAcuerdo.contains(contrato)) {
            contratosAcuerdo.remove(contrato);
            importe.setContratos(contratosAcuerdo);
          }
        }
      }
    }

    propuestaMapper.actualizaImporteTotal(propuesta, false);
    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(idPropuesta);
  }

  public void deleteContratoEnAcuerdoPagoPropuesta(
    Integer idExpediente, Integer idPropuesta, List<ContratoImporteAcuerdoInputDTO> contratos, Boolean pn)
    throws NotFoundException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    Set<AcuerdoPago> acuerdosPago = propuesta.getAcuerdosPago();

    // buscamos los acuerdos de pago seleccionados
    List<AcuerdoPago> acuerdosSeleccionados = getAcuerdosPagoSeleccionados(acuerdosPago, contratos);

    // buscamos los contratos seleccionados
    if (pn) {
      List<BienPosesionNegociada> bienesSeleccionados = getBienesSeleccionados(contratos);

      for (AcuerdoPago acuerdoPago : acuerdosSeleccionados) {
        Set<BienPosesionNegociada> bienesAcuerdo = acuerdoPago.getBienes();
        for (BienPosesionNegociada contrato : bienesSeleccionados) {
          if (bienesAcuerdo.contains(contrato)) {
            bienesAcuerdo.remove(contrato);
            acuerdoPago.setBienes(bienesAcuerdo);
            acuerdoPagoRepository.save(acuerdoPago);
          }
        }
      }
    } else {
      List<Contrato> contratosSeleccionados = getContratosSeleccionados(contratos);

      for (AcuerdoPago acuerdoPago : acuerdosSeleccionados) {
        Set<Contrato> contratosAcuerdo = acuerdoPago.getContratos();
        for (Contrato contrato : contratosSeleccionados) {
          if (contratosAcuerdo.contains(contrato)) {
            contratosAcuerdo.remove(contrato);
            acuerdoPago.setContratos(contratosAcuerdo);
            acuerdoPagoRepository.save(acuerdoPago);
          }
        }
      }
    }

    propuestaMapper.actualizaImporteTotal(propuesta, false);
    propuestaRepository.save(propuesta);
    actualizarFechaUltimoCambio(propuesta.getId());
  }

  private void actualizarFechaUltimoCambio(int idPropuesta) throws NotFoundException {
    var propuesta = propuestaRepository.findById(idPropuesta).orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));
    propuesta.setFechaUltimoCambio(new Date());
    propuestaRepository.saveAndFlush(propuesta);
  }

  public PropuestaDTO actualizarFechaValidezConDto(Integer idPropuesta, PropuestaFechaDTO propuestaDTO) throws NotFoundException, ParseException, InvalidCatalogException {
    var propuesta = propuestaRepository.findById(idPropuesta).orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));
    propuesta.setFechaValidez(propuestaDTO.getFechaValidez());
    propuesta = propuestaRepository.save(propuesta);
    PropuestaDTO dto = new PropuestaDTO();
    var negoc = propuesta.getExpedientePosesionNegociada();
    var normal = propuesta.getExpediente();
    boolean negociada = false;
    if (normal != null && negoc == null) {
      negociada = false;
    } else if (normal == null && negoc != null) {
      negociada = true;
    }
    return propuestaMapper.createPropuestaDTO(propuesta, dto, negociada);
  }

  public Propuesta actualizarFechaValidez(Propuesta propuesta) throws RequiredValueException {
    String pl = null;
    if (propuesta.getExpediente() != null && propuesta.getExpedientePosesionNegociada() == null) {
      if (propuesta.getExpediente().getCartera().getPlazo() != null) {
        pl = propuesta.getExpediente().getCartera().getPlazo().getValor();
      }
    } else if (propuesta.getExpedientePosesionNegociada() != null) {
      if (propuesta.getExpedientePosesionNegociada().getCartera().getPlazo() != null) {
        pl = propuesta.getExpedientePosesionNegociada().getCartera().getPlazo().getValor();
      }
    }

    if (pl != null && !pl.equals("")) {
      LocalDate lt = LocalDate.now().plusDays(Integer.parseInt(pl));
      Date date = Date.from(lt.atStartOfDay(ZoneId.systemDefault()).toInstant());
      propuesta.setFechaValidez(date);
      propuesta = propuestaRepository.save(propuesta);
    }
    return propuesta;
  }

  @Override
  public PropuestaDTO findPropuestaByIdOrigen(String id) throws InvalidCatalogException {

    Propuesta prop = propuestaRepository.findByIdCarga(id).orElse(null);

    PropuestaDTO dto = null;
    if (prop != null){
      dto = new PropuestaDTO();
      propuestaMapper.createPropuestaDTO(prop, dto, prop.getExpedientePosesionNegociada() != null);
    }

    return dto;
  }

  public PropuestaDTO createPropuesta(
      PropuestaInputDTO propuestaInputDTO,
      Integer idExpediente,
      Usuario usuario,
      Boolean posesionNegociada)
      throws Exception {
    var propuestaSaved =
        propuestaMapper.createPropuesta(
            propuestaInputDTO, idExpediente, usuario, posesionNegociada);
    // creamos la formacilización de la propuesta
    formalizacionUseCase.createFormalizacion(propuestaSaved, posesionNegociada);
    // Se envia la informacion de la propuesta a PBC
    if (!posesionNegociada
        && ((Objects.nonNull(propuestaSaved.getExpediente().getCartera().getIdCarga())
                && propuestaSaved.getExpediente().getCartera().getIdCarga().equals("5202"))
            || propuestaSaved.getExpediente().getCartera().getNombre().equals("SAREB"))
        && propuestaSaved.getSubtipoPropuesta().getPbc()) {
      soapClient.notificarPBC(propuestaSaved);
    }
    return propuestaMapper.createPropuestaDTO(
        propuestaSaved, new PropuestaDTO(), posesionNegociada);
  }

  @Override
  public PropuestaDTO createPrecios(Integer idPropuesta, PreciosPropuestaDTO preciosPropuestaDTO) throws Exception {
    var propuestaSaved = propuestaMapper.createPrecios(idPropuesta, preciosPropuestaDTO);
    actualizarFechaUltimoCambio(idPropuesta);
    return propuestaMapper.createPropuestaDTO(propuestaSaved.getPropuesta(), new PropuestaDTO(), false);
  }

  @Override
  public void createListaDocumentos(Integer idExpediente, Integer idPropuesta, List<MultipartFile> files, List<String> idsMatricula, Integer idTipoPropuesta, Boolean posesionNegociada, Usuario usuario) throws Exception {
    if (files != null && files.size() > 0) {
      for (int i=0; i<files.size(); i++) {
        MultipartFile f =files.get(i);
        createDocumento(idExpediente, idPropuesta, f, idsMatricula.get(i), idTipoPropuesta, posesionNegociada, usuario);
      }
    }
  }

  @Override
  public void createDocumento(
      Integer idExpediente,
      Integer idPropuesta,
      MultipartFile file,
      String idMatricula,
      Integer idTipoPropuesta,
      Boolean posesionNegociada,
      Usuario usuario)
      throws Exception {

    Propuesta propuesta = propuestaRepository.findById(idPropuesta).orElse(null);
    // A la hora de crear el contenedor hay que pasarle un cliente que esté registrado en la base de
    // datos
    Cartera cartera;
    if (posesionNegociada) {
      ExpedientePosesionNegociada expedientePosesionNegociada =
          expedientePosesionNegociadaRepository
              .findById(idExpediente)
              .orElseThrow(
                  () ->
                      new NotFoundException("Expediente Posesión Negociada", idExpediente));
      cartera = expedientePosesionNegociada.getCartera();
    } else {
      Expediente expediente =
          expedienteRepository
              .findById(idExpediente)
              .orElseThrow(
                  () ->
                      new NotFoundException("Expediente", idExpediente));
      cartera = expediente.getCartera();
    }
    Cliente cliente = cartera.getCliente();
    if (cliente == null)
      throw new NotFoundException("Cliente", "Cartera", cartera.getNombre());
    String nombreCliente = cliente.getNombre();
    String nombreFichero = file.getOriginalFilename();
    MetadatoDocumentoInputDTO metadatoDocumentoInputDTO =
        new MetadatoDocumentoInputDTO(nombreFichero, idMatricula);
    String idPropuestaNueva = idPropuesta.toString();
    TipoPropuesta tipoPropuesta =
        tipoPropuestaRepository.findByIdAndActivoIsTrue(idTipoPropuesta).get();

    if (tipoPropuesta.getValor().equals("Refinanciación")) {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
          new MetadatoContenedorInputDTO(idPropuestaNueva, nombreCliente);
      this.servicioGestorDocumental.procesarContenedor(
          CodigoEntidad.PROPUESTAS_REESTRUCTURACION_REFINANCIACION, metadatoContenedorInputDTO);
      long id =
          servicioGestorDocumental
              .procesarDocumentoPropuesta(
                  CodigoEntidad.PROPUESTAS_REESTRUCTURACION_REFINANCIACION,
                  propuesta.getId(),
                  file,
                  metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental,
              usuario,
              null,
              new Date(),
              null,
              metadatoDocumentoInputDTO.getNombreFichero(),
            MatriculasEnum.obtenerPorMatricula(metadatoDocumentoInputDTO.getCatalogoDocumental()),
              null,
            idPropuesta.toString(),
            TipoEntidadEnum.PROPUESTA,
            idPropuesta.toString());
      gestorDocumentalRepository.save(gestorDocumental);
    }
    if (tipoPropuesta.getValor().equals("Venta consensuada")) {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
          new MetadatoContenedorInputDTO(idPropuestaNueva, nombreCliente);
      this.servicioGestorDocumental.procesarContenedor(
          CodigoEntidad.PROPUESTAS_VENTA_CONSENSUADA, metadatoContenedorInputDTO);
      long id =
          servicioGestorDocumental
              .procesarDocumentoPropuesta(
                  CodigoEntidad.PROPUESTAS_VENTA_CONSENSUADA,
                  propuesta.getId(),
                  file,
                  metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental,
              usuario,
              null,
              new Date(),
              null,
              metadatoDocumentoInputDTO.getNombreFichero(),
            MatriculasEnum.obtenerPorMatricula(metadatoDocumentoInputDTO.getCatalogoDocumental()),
              null,
            idPropuesta.toString(),
            TipoEntidadEnum.PROPUESTA,
            idPropuesta.toString());
      gestorDocumentalRepository.save(gestorDocumental);
    }

    if (tipoPropuesta.getValor().equals("Dación")) {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
          new MetadatoContenedorInputDTO(idPropuestaNueva, nombreCliente);
      this.servicioGestorDocumental.procesarContenedor(
          CodigoEntidad.PROPUESTAS_DACION, metadatoContenedorInputDTO);
      long id =
          servicioGestorDocumental
              .procesarDocumentoPropuesta(
                  CodigoEntidad.PROPUESTAS_DACION,
                  propuesta.getId(),
                  file,
                  metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental,
              usuario,
              null,
              new Date(),
              null,
              metadatoDocumentoInputDTO.getNombreFichero(),
            MatriculasEnum.obtenerPorMatricula(metadatoDocumentoInputDTO.getCatalogoDocumental()),
              null,
            idPropuesta.toString(),
            TipoEntidadEnum.PROPUESTA,
            idPropuesta.toString());
      gestorDocumentalRepository.save(gestorDocumental);
    }

    if (tipoPropuesta.getValor().equals("De posesión")) {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
          new MetadatoContenedorInputDTO(idPropuestaNueva, nombreCliente);
      this.servicioGestorDocumental.procesarContenedor(
          CodigoEntidad.PROPUESTAS_POSESION_NEGOCIADA, metadatoContenedorInputDTO);
      long id =
          servicioGestorDocumental
              .procesarDocumentoPropuesta(
                  CodigoEntidad.PROPUESTAS_POSESION_NEGOCIADA,
                  propuesta.getId(),
                  file,
                  metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental,
              usuario,
              null,
              new Date(),
              null,
              metadatoDocumentoInputDTO.getNombreFichero(),
            MatriculasEnum.obtenerPorMatricula(metadatoDocumentoInputDTO.getCatalogoDocumental()),
              null,
            idPropuesta.toString(),
            TipoEntidadEnum.PROPUESTA,
            idPropuesta.toString());
      gestorDocumentalRepository.save(gestorDocumental);
    }

    if (tipoPropuesta.getValor().equals("Cancelación")) {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
          new MetadatoContenedorInputDTO(idPropuestaNueva, nombreCliente);
      this.servicioGestorDocumental.procesarContenedor(
          CodigoEntidad.PROPUESTAS_CANCELACIONES, metadatoContenedorInputDTO);
      long id =
          servicioGestorDocumental
              .procesarDocumentoPropuesta(
                  CodigoEntidad.PROPUESTAS_CANCELACIONES,
                  propuesta.getId(),
                  file,
                  metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental,
              usuario,
              null,
              new Date(),
              null,
              metadatoDocumentoInputDTO.getNombreFichero(),
            MatriculasEnum.obtenerPorMatricula(metadatoDocumentoInputDTO.getCatalogoDocumental()),
              null,
            idPropuesta.toString(),
            TipoEntidadEnum.PROPUESTA,
            idPropuesta.toString());
      gestorDocumentalRepository.save(gestorDocumental);
    }

    if (tipoPropuesta.getValor().equals("Reactivación")) {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
        new MetadatoContenedorInputDTO(idPropuestaNueva, nombreCliente);
      this.servicioGestorDocumental.procesarContenedor(
        CodigoEntidad.PROPUESTAS_REACTIVACIONES, metadatoContenedorInputDTO);
      long id =
        servicioGestorDocumental
          .procesarDocumentoPropuesta(
            CodigoEntidad.PROPUESTAS_REACTIVACIONES,
            propuesta.getId(),
            file,
            metadatoDocumentoInputDTO)
          .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental =
        new GestorDocumental(
          idgestorDocumental,
          usuario,
          null,
          new Date(),
          null,
          metadatoDocumentoInputDTO.getNombreFichero(),
          MatriculasEnum.obtenerPorMatricula(metadatoDocumentoInputDTO.getCatalogoDocumental()),
          null,
          idPropuesta.toString(),
          TipoEntidadEnum.PROPUESTA,
          idPropuesta.toString());
      gestorDocumentalRepository.save(gestorDocumental);
    }
    if (tipoPropuesta.getValor().equals("Venta de crédito")) {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
        new MetadatoContenedorInputDTO(idPropuestaNueva, nombreCliente);
      this.servicioGestorDocumental.procesarContenedor(
        CodigoEntidad.PROPUESTAS_VENTA_DE_CREDITO, metadatoContenedorInputDTO);
      long id =
        servicioGestorDocumental
          .procesarDocumentoPropuesta(
            CodigoEntidad.PROPUESTAS_VENTA_DE_CREDITO,
            propuesta.getId(),
            file,
            metadatoDocumentoInputDTO)
          .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental =
        new GestorDocumental(
          idgestorDocumental,
          usuario,
          null,
          new Date(),
          null,
          metadatoDocumentoInputDTO.getNombreFichero(),
          MatriculasEnum.obtenerPorMatricula(metadatoDocumentoInputDTO.getCatalogoDocumental()),
          null,
          idPropuesta.toString(),
          TipoEntidadEnum.PROPUESTA,
          idPropuesta.toString());
      gestorDocumentalRepository.save(gestorDocumental);
    }
  }

  @Override
  public void createContenedor(String tipo, String clase, String idPropuesta, String nombre) throws Exception {
    //CodigoEntidad codigoEntidad = new CodigoEntidad(tipo, clase);
    MetadatoContenedorInputDTO metadatoContenedorInputDTO = new MetadatoContenedorInputDTO(idPropuesta, nombre);
   // RespuestaCrearContenedor respuesta = servicioGestorDocumental.procesarContenedor(codigoEntidad, metadatoContenedorInputDTO);
  }

  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosPropuestas(Integer idContrato, String idDocumento, String usuarioCreador, String tipo,
                                                                 String idEntidad, String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,
                                                                 String orderField, Integer size, Integer page) throws NotFoundException, IOException {
    Contrato contrato = contratoRepository.findById(idContrato)
      .orElseThrow(() -> new NotFoundException("Contrato", idContrato));
;
    List<DocumentoDTO> listaDocumentos = new ArrayList<>();
    for (Propuesta nuevaPropuesta : contrato.getPropuestas()) {
      Integer idPropuesta = nuevaPropuesta.getId();
      String idPropuestaString = idPropuesta.toString();

      List<DocumentoDTO> listaDocumentosGestion =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_VENTA_CONSENSUADA, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());

      for (DocumentoDTO nuevo : listaDocumentosGestion) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosVentas =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(
            CodigoEntidad.PROPUESTAS_REESTRUCTURACION_REFINANCIACION, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());

      for (DocumentoDTO nuevo : listaDocumentosVentas) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosDacion =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_DACION, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosDacion) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosPosesion =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_POSESION_NEGOCIADA, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosPosesion) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosCancelacion =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_CANCELACIONES, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosCancelacion) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosReactivacion =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_REACTIVACIONES, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosReactivacion) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosVentaCredito =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_VENTA_DE_CREDITO, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosVentaCredito) {
        listaDocumentos.add(nuevo);
      }
    }
    List<DocumentoDTO> list = gestorDocumentalUseCase.listarDocumentosGestorPropuesta(listaDocumentos).stream().collect(Collectors.toList());
    List<DocumentoDTO> listFinal = gestorDocumentalUseCase.filterOrderDocumentosDTO(list, idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, nombreArchivo, idHaya, orderDirection, orderField);
    List<DocumentoDTO> listaFiltradaPaginada = listFinal.stream().skip(size * page).limit(size).collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
  }


  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosByExpedienteId(Integer idExpediente, Boolean posesionNegociada, String idDocumento, String usuarioCreador, String tipo, String idEntidad, String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection, String orderField, Integer size, Integer page) throws NotFoundException, IOException {

    List<Propuesta> listadoPropuestas;
    List<DocumentoDTO> listaDocumentos = new ArrayList<>();
    if (!posesionNegociada) {
      Expediente expediente = expedienteRepository.findById(idExpediente)
        .orElseThrow(() -> new NotFoundException("Expediente", idExpediente));
      listadoPropuestas = expediente.getPropuestas().stream().collect(Collectors.toList());
    } else {
      ExpedientePosesionNegociada expedientePosesionNegociada = expedientePosesionNegociadaRepository.findById(idExpediente)
        .orElseThrow(() -> new NotFoundException("Expediente Posesión Negociada", idExpediente));
      listadoPropuestas = expedientePosesionNegociada.getPropuestas().stream().collect(Collectors.toList());
    }
    for (Propuesta propuesta : listadoPropuestas) {
      Integer idPropuesta = propuesta.getId();
      String idPropuestaString = idPropuesta.toString();
      List<DocumentoDTO> listaDocumentosGestion =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_VENTA_CONSENSUADA, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosGestion) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosVentas =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(
            CodigoEntidad.PROPUESTAS_REESTRUCTURACION_REFINANCIACION, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosVentas) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosDacion =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_DACION, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosDacion) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosPosesion =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_POSESION_NEGOCIADA, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosPosesion) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosCancelacion =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_CANCELACIONES, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosCancelacion) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosReactivacion =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_REACTIVACIONES, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosReactivacion) {
        listaDocumentos.add(nuevo);
      }
      List<DocumentoDTO> listaDocumentosVentaCredito =
        this.servicioGestorDocumental
          .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_VENTA_DE_CREDITO, idPropuestaString)
          .stream()
          .map(DocumentoDTO::new)
          .collect(Collectors.toList());
      for (DocumentoDTO nuevo : listaDocumentosVentaCredito) {
        listaDocumentos.add(nuevo);
      }
    }
    List<DocumentoDTO> list = gestorDocumentalUseCase.listarDocumentosGestorPropuesta(listaDocumentos).stream().collect(Collectors.toList());
    List<DocumentoDTO> listFinal = gestorDocumentalUseCase.filterOrderDocumentosDTO(list, idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, nombreArchivo, idHaya, orderDirection, orderField);
    List<DocumentoDTO> listaFiltradaPaginada = listFinal.stream().skip(size * page).limit(size).collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
  }

  public List<SubtipoPropuestaDTO> findSubtipoPropuestaByTipoPropuesta(Integer tipoPropuestaId) {
    List<SubtipoPropuesta> subtipoPropuestas = subtipoPropuestaRepository.findByTipoPropuestaIdAndActivoIsTrue(tipoPropuestaId);
    return subtipoPropuestas.stream().map(SubtipoPropuestaDTO::new).collect(Collectors.toList());
  }

  public List<BienOutputDto> bienesInPropuestaToInformes(Integer idPropuesta) throws NotFoundException {
    Propuesta propuesta = propuestaRepository.findById(idPropuesta).orElseThrow(
      () -> new NotFoundException("Propuesta", idPropuesta));

    Set<PropuestaBien> pbs = propuesta.getBienes();
    List<Bien> bienes = pbs.stream().map(PropuestaBien::getBien).collect(Collectors.toList());
    List<BienOutputDto> result = bienes.stream().map(bien -> bienMapper.entityOutputDto(bien, null)).collect(Collectors.toList());
    for (var bien : result) {
      var bienPN = bienPosesionNegociadaRepository.findByBienId(bien.getId()).orElse(null);
      if (bienPN != null) {
        bien.setIdBienPosesionNegociada(bienPN.getId());
      }
    }
    return result;
  }

  public List<BienOutputDto> bienesWithTasAndCar(Integer idPropuesta) throws NotFoundException {
    Propuesta propuesta = propuestaRepository.findById(idPropuesta).orElseThrow(
      () -> new NotFoundException("Propuesta", idPropuesta));

    Set<PropuestaBien> pbs = propuesta.getBienes();
    List<Bien> bienes = pbs.stream().map(PropuestaBien::getBien).collect(Collectors.toList());
    //Tal vez hay que crear otro informe
    List<Bien> bienesF = new ArrayList<>();
    for (Bien b : bienes) {
      if (!b.getTasaciones().isEmpty() && !b.getCargas().isEmpty()) bienesF.add(b);
    }
    List<BienOutputDto> result = bienesF.stream().map(bien -> bienMapper.entityOutputDto(bien, null)).collect(Collectors.toList());
    return result;
  }

  public List<BienOutputDto> bienesInPropuesta(Integer idPropuesta) throws NotFoundException {
    Propuesta propuesta = propuestaRepository.findById(idPropuesta).orElseThrow(
      () -> new NotFoundException("Propuesta", idPropuesta));

    Set<PropuestaBien> pbs = propuesta.getBienes();
    List<Bien> bienes = pbs.stream().map(PropuestaBien::getBien).collect(Collectors.toList());
    //Tal vez hay que crear otro informe
    List<Bien> bienesF = new ArrayList<>();
    for (Bien b : bienes) {
      if (!b.getTasaciones().isEmpty() && !b.getValoraciones().isEmpty() && !b.getCargas().isEmpty()) bienesF.add(b);
    }
    List<BienOutputDto> result = bienesF.stream().map(bien -> bienMapper.entityOutputDto(bien, null)).collect(Collectors.toList());
    return result;
  }

  public List<IntervinienteDTOToList> intervinientesInPropuesta(Integer idPropuesta) throws NotFoundException {
    Propuesta propuesta = propuestaRepository.findById(idPropuesta).orElseThrow(
      () -> new NotFoundException("Propuesta", idPropuesta));

    List<Interviniente> intervinientes = new ArrayList<>(propuesta.getIntervinientes());

    List<IntervinienteDTOToList> result = IntervinienteDTOToList.newList(intervinientes, null);
    return result;
  }

  public List<OcupanteDTOToList> ocupantesInPropuesta(Integer idPropuesta) throws NotFoundException {
    Propuesta propuesta = propuestaRepository.findById(idPropuesta).orElseThrow(
      () -> new NotFoundException("Propuesta", idPropuesta));

    List<Ocupante> ocupantes = new ArrayList<>(propuesta.getOcupantes());

    List<OcupanteDTOToList> result = OcupanteDTOToList.newList(ocupantes);
    return result;
  }

  public BienOutputDto getBienById(Integer idBien) throws NotFoundException {
    Bien bien = bienRepository.findById(idBien).orElseThrow(
      () -> new NotFoundException("Bien", idBien));
    BienOutputDto result = bienMapper.entityOutputDto(bien, null);
    return result;
  }

  public List<CatalogoMinInfoDTO> findTipoPropuestaByPN(Boolean posesiónNegociada) throws NotFoundException {
    List<TipoPropuesta> tipoPropuestas = new ArrayList<>();
    if (posesiónNegociada) {
      TipoPropuesta tipoPropuesta = tipoPropuestaRepository.findByActivoIsTrueAndCodigo("DP").orElseThrow(() ->
        new NotFoundException("TipoPropuesta", "Código", "'DP'"));
      tipoPropuestas.add(tipoPropuesta);
    } else {
      tipoPropuestas = tipoPropuestaRepository.findAllByActivoIsTrueAndCodigoNot("DP");
    }
    return tipoPropuestas.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
  }

  public PropuestaDTO getDetallePropuestaById(Integer propuestaId) throws NotFoundException, InvalidCatalogException {
    var propuesta =
      propuestaRepository
        .findById(propuestaId)
        .orElseThrow(
          () -> new NotFoundException("Propuesta", propuestaId));
    return propuestaMapper.createPropuestaDTO(propuesta, new PropuestaDTO(), false);
  }

  /*
   * Función para comprobar qué contratos de los que la entidad (en este caso bien o interviniente)
   * son los que han sido seleccionados en la propuesta
   * */
  public List<Contrato> getContratosRelacionados(
    List<Contrato> contratosPropuesta, List<Contrato> restoContratos) {
    List<Contrato> result =
      restoContratos.stream()
        .filter(e -> contratosPropuesta.contains(e))
        .collect(Collectors.toList());
    return result;
  }

  public ComentarioPropuestaDTO createComentario(
    Integer idPropuesta, Integer idExpediente, Usuario usuario, String comentario)
    throws NotFoundException {
    var propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(
          () ->
            new NotFoundException("Propuesta", idPropuesta, "Expediente", idExpediente));
    ComentarioPropuesta comentarioPropuesta = new ComentarioPropuesta();
    comentarioPropuesta.setFechaComentario(Calendar.getInstance().getTime());
    comentarioPropuesta.setFechaEdicion(Calendar.getInstance().getTime());
    comentarioPropuesta.setUsuario(usuario);
    comentarioPropuesta.setPropuesta(propuesta);
    comentarioPropuesta.setComentario(comentario);
    actualizarFechaUltimoCambio(idPropuesta);
    ComentarioPropuesta cp = comentarioPropuestaRepository.save(comentarioPropuesta);
    return new ComentarioPropuestaDTO(cp);
  }

  public List<Propuesta> getFilteredPropuestas(Integer idExpediente, Boolean posesionNegociada, Integer idPropuesta, String fechaAlta,
                                               String usuario, String clase, String tipo, String subtipoPropuesta, String estadoPropuesta,
                                               String sancion, String resolucion, String fechaUltimoCambio, String importeTotal,
                                               String orderField, String orderDirection) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Propuesta> query = cb.createQuery(Propuesta.class);

    Root<Propuesta> root = query.from(Propuesta.class);

    List<Predicate> predicates = new ArrayList<>();

    if (!posesionNegociada) {
      Join<Propuesta, Expediente> joinExp = root.join(Propuesta_.EXPEDIENTE, JoinType.INNER);
      Predicate onExp = cb.equal(cb.upper(joinExp.get(Expediente_.ID)), idExpediente);
      joinExp.on(onExp);
    } else {
      Join<Propuesta, ExpedientePosesionNegociada> joinExp = root.join(Propuesta_.expedientePosesionNegociada, JoinType.INNER);
      Predicate onExp = cb.equal(cb.upper(joinExp.get(ExpedientePosesionNegociada_.ID)), idExpediente);
      joinExp.on(onExp);
    }


    Predicate onCond;
    predicates.add(cb.equal(root.get(Propuesta_.activo), true));
    if (idPropuesta != null) {
      predicates.add(cb.like(root.get(Propuesta_.id).as(String.class), "%" + idPropuesta + "%"));
    } else {

      if (fechaAlta != null)
        predicates.add(cb.like(root.get(Propuesta_.fechaAlta).as(String.class), "%" + fechaAlta + "%"));
      if (usuario != null) {
        Join<Propuesta, Usuario> usuarioJoin = root.join(Propuesta_.usuario, JoinType.INNER);
        Predicate onUsuario = cb.like(cb.upper(usuarioJoin.get(Usuario_.nombre)), "%" + usuario + "%");
        usuarioJoin.on(onUsuario);
      }
      if (clase != null) {
        addOnCond(cb, root, Propuesta_.clasePropuesta, clase);
      }
      if (tipo != null) {
        addOnCond(cb, root, Propuesta_.tipoPropuesta, tipo);
      }
      if (subtipoPropuesta != null) {
        addOnCond(cb, root, Propuesta_.subtipoPropuesta, subtipoPropuesta);
      }
      if (estadoPropuesta != null) {
        addOnCond(cb, root, Propuesta_.estadoPropuesta, estadoPropuesta);
      }
      if (sancion != null) {
        addOnCond(cb, root, Propuesta_.sancionPropuesta, sancion);
      }
      if (resolucion != null) {
        addOnCond(cb, root, Propuesta_.resolucionPropuesta, resolucion);
      }
      if (fechaUltimoCambio != null)
        predicates.add(cb.like(root.get(Propuesta_.fechaUltimoCambio).as(String.class), "%" + fechaUltimoCambio + "%"));
      if (importeTotal != null)
        predicates.add(cb.like(root.get(Propuesta_.importeTotal).as(String.class), "%" + importeTotal.toString() + "%"));
    }
    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    if (orderField != null) {
      if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
        rs.orderBy(cb.desc(root.get(orderField)));
      else
        rs.orderBy(cb.asc(root.get(orderField)));
    }
    List<Propuesta> listaPropuesta = entityManager.createQuery(query).getResultList();

    return listaPropuesta;
  }

  private void addOnCond(CriteriaBuilder cb, Root<Propuesta> root, SingularAttribute<Propuesta, ?> singularAtrribute, String campo) {
    if (campo == null)
      return;
    Join<Propuesta, ?> estadoJoin = root.join(singularAtrribute, JoinType.INNER);
    Predicate onCond = cb.like(cb.upper(estadoJoin.get(Catalogo_.VALOR)), "%" + campo.toUpperCase() + "%");
    estadoJoin.on(onCond);
  }

  public void deleteComentario(Integer idExpediente, Integer idPropuesta, Integer id, Usuario user) throws NotFoundException, ForbiddenException {
    if (!user.getPerfil().getNombre().equals("Responsable de cartera")) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new ForbiddenException("The logged in user is not Portfolio Manager.");
      else throw new ForbiddenException("El usuario loggeado no es Responsable de Cartera.");
    }
    Propuesta propuesta = propuestaRepository.findById(idPropuesta).orElseThrow(
      () -> new NotFoundException("Propuesta", idPropuesta));
    if (propuesta.getExpediente() != null) {
      List<AsignacionExpediente> asignaciones = asignacionExpedienteRepository.findAllByExpedienteIdAndUsuarioId(idExpediente, user.getId());
      if (asignaciones.isEmpty()){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new ForbiddenException("The user is not assigned to this file.");
        else throw new ForbiddenException("El usuario no está asignado a este expediente.");
      }
    } else {
      List<AsignacionExpedientePosesionNegociada> asignaciones = asignacionExpedientePosesionNegociadaRepository.
        findAllByExpedienteIdAndUsuarioId(idExpediente, user.getId());
      if (asignaciones.isEmpty()){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new ForbiddenException("The user is not assigned to this filePosesionNegociada.");
        else throw new ForbiddenException("El usuario no está asignado a este expedientePosesionNegociada.");
      }
    }

    ComentarioPropuesta cp = comentarioPropuestaRepository.findById(id).orElseThrow(
      () -> new NotFoundException("ComentarioPropuesta", id));
    cp.setActivo(false);
    comentarioPropuestaRepository.save(cp);
  }

  public ComentarioPropuestaDTO updateComentario(Integer idComentario, String comentario, Usuario usuario) throws NotFoundException, ForbiddenException {
    ComentarioPropuesta cp = comentarioPropuestaRepository.findById(idComentario).orElseThrow(
      () -> new NotFoundException("comentarioPropuesta", idComentario));
    if (!usuario.getId().equals(cp.getUsuario().getId())){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new ForbiddenException("Only the user who created the comment can modify it.");
      else throw new ForbiddenException("Solo el usuario que ha creado el comentario puede modificarlo.");
    }
    cp.setComentario(comentario);
    cp.setFechaEdicion(Calendar.getInstance().getTime());
    ComentarioPropuesta cpSaved = comentarioPropuestaRepository.save(cp);
    return new ComentarioPropuestaDTO(cpSaved);
  }

  @Override
  public List<PropuestaDTOToList> findAllPropuestasByExpedienteId(Integer idExpediente, Boolean posesionNegociada) throws NotFoundException {
    if (!posesionNegociada) {
      Expediente expediente = expedienteRepository.findById(idExpediente).orElseThrow(
        () -> new NotFoundException("Expediente", idExpediente));
      List<Propuesta> listadoPropuestas =
        expediente.getPropuestas().stream().filter(propuesta ->  propuesta.getActivo()).collect(Collectors.toList());
      List<PropuestaDTOToList> listaPropuestasDTO =
        propuestaMapper.listPropuestaToListPropuestaDTOToList(listadoPropuestas);
      return listaPropuestasDTO;
    } else {
      ExpedientePosesionNegociada expedientePosesionNegociada = expedientePosesionNegociadaRepository.findById(idExpediente).orElseThrow(
        () -> new NotFoundException("Expediente Posesión Negociada", idExpediente));
      List<Propuesta> listadoPropuestas = expedientePosesionNegociada.getPropuestas().stream().filter(propuesta ->  propuesta.getActivo()).collect(Collectors.toList());
      List<PropuestaDTOToList> listaPropuestasDTO = propuestaMapper.listPropuestaToListPropuestaDTOToList(listadoPropuestas);
      return listaPropuestasDTO;
    }
  }



  public List<DocumentoDTO>findDocumentosByPropuesta(Integer idPropuesta) throws NotFoundException, IOException {
    Propuesta propuesta=propuestaRepository.findById(idPropuesta)
      .orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));
    String idPropuestaString=idPropuesta.toString();
    List<DocumentoDTO> listaDocumentos = new ArrayList<>();
    List<DocumentoDTO> listaDocumentosGestion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_VENTA_CONSENSUADA, idPropuestaString)
        .stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());
    for (DocumentoDTO nuevo : listaDocumentosGestion) {
      listaDocumentos.add(nuevo);
    }
    List<DocumentoDTO> listaDocumentosVentas =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(
          CodigoEntidad.PROPUESTAS_REESTRUCTURACION_REFINANCIACION, idPropuestaString)
        .stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());
    for (DocumentoDTO nuevo : listaDocumentosVentas) {
      listaDocumentos.add(nuevo);
    }
    List<DocumentoDTO> listaDocumentosDacion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_DACION, idPropuestaString)
        .stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());
    for (DocumentoDTO nuevo : listaDocumentosDacion) {
      listaDocumentos.add(nuevo);
    }
    List<DocumentoDTO> listaDocumentosPosesion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_POSESION_NEGOCIADA, idPropuestaString)
        .stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());
    for (DocumentoDTO nuevo : listaDocumentosPosesion) {
      listaDocumentos.add(nuevo);
    }
    List<DocumentoDTO> listaDocumentosCancelacion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_CANCELACIONES, idPropuestaString)
        .stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());
    for (DocumentoDTO nuevo : listaDocumentosCancelacion) {
      listaDocumentos.add(nuevo);
    }
    List<DocumentoDTO> listaDocumentosReactivacion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_REACTIVACIONES, idPropuestaString)
        .stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());
    for (DocumentoDTO nuevo : listaDocumentosReactivacion) {
      listaDocumentos.add(nuevo);
    }
    List<DocumentoDTO> listaDocumentoVentaCredito =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_VENTA_DE_CREDITO, idPropuestaString)
        .stream()
        .map(DocumentoDTO::new)
        .collect(Collectors.toList());
    for (DocumentoDTO nuevo : listaDocumentoVentaCredito) {
      listaDocumentos.add(nuevo);
    }

  List<DocumentoDTO> list = gestorDocumentalUseCase.listarDocumentosGestorPropuesta(listaDocumentos).stream().collect(Collectors.toList());
 // List<DocumentoDTO> listFinal = gestorDocumentalUseCase.filterOrderDocumentosDTO(list, idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, orderDirection, orderField);
  //List<DocumentoDTO> listaFiltradaPaginada = gestorDocumentalUseCase.listarDocumentosGestor(listFinal).stream().skip(size * page).limit(size).collect(Collectors.toList());
    return list;
  }

  public long countDocumentosByPropuesta(Integer idPropuesta) throws NotFoundException, IOException {
    Propuesta propuesta=propuestaRepository.findById(idPropuesta).orElseThrow(() -> new NotFoundException("Expediente", idPropuesta));
    String idPropuestaString=idPropuesta.toString();
    long n_documentos = 0;
    n_documentos += this.servicioGestorDocumental.listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_VENTA_CONSENSUADA, idPropuestaString).stream().count();
    n_documentos += this.servicioGestorDocumental.listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_REESTRUCTURACION_REFINANCIACION, idPropuestaString).stream().count();
    n_documentos += this.servicioGestorDocumental.listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_DACION, idPropuestaString).stream().count();
    n_documentos += this.servicioGestorDocumental .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_POSESION_NEGOCIADA, idPropuestaString).stream().count();
    n_documentos += this.servicioGestorDocumental.listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_CANCELACIONES, idPropuestaString).stream().count();
    n_documentos += this.servicioGestorDocumental.listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_REACTIVACIONES, idPropuestaString).stream().count();
    n_documentos += this.servicioGestorDocumental.listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_VENTA_DE_CREDITO, idPropuestaString).stream().count();

    return n_documentos;
  }

  public List<CatalogoMinInfoDTO> getEstadosPropuesta(Integer idPropuesta) throws NotFoundException {
    List<CatalogoMinInfoDTO> result = new ArrayList<>();
    Boolean formalizacion = false;

    if (idPropuesta != null){
      Propuesta p =
        propuestaRepository
          .findById(idPropuesta)
          .orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));

      Expediente e = p.getExpediente();
      if (e != null){
        Cartera c = e.getCartera();
        formalizacion = c.getFormalizacion();
      }
      if (!formalizacion){
        SubtipoPropuesta sp = p.getSubtipoPropuesta();
        if (sp != null && sp.getFormalizacion() != null)
          formalizacion = sp.getFormalizacion();
      }
    }

    List<EstadoPropuesta> estados = estadoPropuestaRepository.findAllByActivoIsTrue();
    if (!formalizacion)
      estados = estados.stream().filter(e -> !estadosFormalizacion().contains(e.getCodigo())).collect(Collectors.toList());

    result = estados.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());

    return result;
  }

  public List<CatalogoMinInfoDTO> getSubestadosPropuesta(Integer id) throws NotFoundException {
    List<CatalogoMinInfoDTO> result = new ArrayList<>();
    EstadoPropuesta ep =
        estadoPropuestaRepository
            .findById(id)
            .orElseThrow(() -> new NotFoundException("EstadoPropuesta", id));
    if (ep.getCodigo().equals("FOR"))
      ep =
          estadoPropuestaRepository
              .findByCodigo("FIR")
              .orElseThrow(() -> new NotFoundException("EstadoPropuesta", "Codigo", "FIR"));

    List<SubestadoFormalizacion> subestados = subestadoFormalizacionRepository.findAllByEstadoPropuestaId(ep.getId());
    result = subestados.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());

    return result;
  }

  private List<String> estadosFormalizacion(){
    List<String> r = new ArrayList<>();

    r.add("FIR");
    r.add("PFI");
    r.add("P0");
    r.add("CAN");
    r.add("PRO");

    return r;
  }

  @Override
  public void actualizarEstadoYSubestado(Usuario user, Integer idPropuesta, Integer estado, Integer subestado)
    throws NotFoundException, RequiredValueException {
    Propuesta p =
        propuestaRepository
            .findById(idPropuesta)
            .orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));

    EstadoPropuesta ep =
        estadoPropuestaRepository
            .findById(estado)
            .orElseThrow(() -> new NotFoundException("Estado Propuesta", estado));

    if (ep.getCodigo().equals("CAN")){
      ep =
        estadoPropuestaRepository
          .findByCodigo("ANU")
          .orElseThrow(() -> new NotFoundException("Estado Propuesta", "Codigo", "ANU"));
    } else if (ep.getCodigo().equals("FIR")){
      ep =
        estadoPropuestaRepository
          .findByCodigo("FOR")
          .orElseThrow(() -> new NotFoundException("Estado Propuesta", "Codigo", "FOR"));
      p.setResolucionPropuesta(
        resolucionPropuestaRepository.findByCodigo("F").orElse(null));

      p.setFechaResolucion(new Date());
    }

    SubestadoFormalizacion sf = null;

    if (subestado != null) {
      sf =
          subestadoFormalizacionRepository
              .findById(subestado)
              .orElseThrow(() -> new NotFoundException("Subestado Propuesta", subestado));
      /*if (sf.getCodigo().equals("P0_1") || sf.getCodigo().equals("P0_2"))
        ep =
          estadoPropuestaRepository
            .findByCodigo("ANU")
            .orElseThrow(() -> new NotFoundException("Estado Propuesta", "Codigo", "ANU"));*/
    }

    if (p.getEstadoPropuesta() != null && !p.getEstadoPropuesta().getCodigo().equals("FOR"))
      p.setEstadoPropuesta(ep);
    if (ep.getCodigo().equals("PFI"))
      eventoUseCase.alertaPteFirma(p, user);


    if (Objects.nonNull(p.getExpediente())
        && ((Objects.nonNull(p.getExpediente().getCartera().getIdCarga())
                && p.getExpediente().getCartera().getIdCarga().equals("5202"))
            || p.getExpediente().getCartera().getNombre().equals("SAREB"))
        && p.getSubtipoPropuesta().getPbc()) {
      if (ep.getCodigo().equals("PCL")
          || ep.getCodigo().equals("AC")
          || ep.getCodigo().equals("PFI")
          || ep.getCodigo().equals("FOR")
          || ep.getCodigo().equals("DEN")
          || ep.getCodigo().equals("DES")
          || ep.getCodigo().equals("ANU")
          || ep.getCodigo().equals("INCU")) {
        soapClient.notificarPBC(p);
      }
    }
    if (getCodigosAnulado().contains(ep.getCodigo()))
      eventoUtil.cerrarEventosPropuesta(idPropuesta);

    propuestaMapper.actualizaImporteTotal(p, false);
    actualizarFechaUltimoCambio(p.getId());

    Formalizacion f = formalizacionRepository.findByPropuestaId(p.getId());

    if (f.getId() == null) f.setPropuesta(p);
    f.setSubestadoFormalizacion(sf);

    if (ep.getCodigo().equals("PFI") && sf != null && sf.getCodigo().equals("PFI_8")){
      f.setFechaInicioCheckList(new Date());
      formalizacionUseCase.alertaAlGestor(user, idPropuesta);
    }

    if (subestado == null) f.setSubestadoFormalizacion(null);

    propuestaRepository.save(p);
    formalizacionRepository.save(f);

  }
}
