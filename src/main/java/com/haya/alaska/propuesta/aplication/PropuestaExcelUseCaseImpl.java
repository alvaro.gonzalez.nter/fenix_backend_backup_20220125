package com.haya.alaska.propuesta.aplication;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.application.ExpedienteUseCase;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.ExpedienteCarteraExcel;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.domain.PropuestaExcel;
import com.haya.alaska.propuesta.infrastructure.controller.dto.InformePropuestaInputDTO;
import com.haya.alaska.propuesta.infrastructure.controller.dto.IntervinientePropuestaDTO;
import com.haya.alaska.propuesta.infrastructure.mapper.PropuestaMapper;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.registro_historico.infrastructure.repository.RegistroHistoricoRepository;
import com.haya.alaska.resolucion_propuesta.infrastructure.repository.ResolucionPropuestaRepository;
import com.haya.alaska.sancion_propuesta.infrastructure.repository.SancionPropuestaRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class PropuestaExcelUseCaseImpl implements PropuestaExcelUseCase {

  @Autowired CarteraRepository carteraRepository;

  @Autowired private EventoUtil eventoUtil;

  @Autowired private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;

  @Autowired private ExpedienteRepository expedienteRepository;

  @Autowired private PropuestaRepository propuestaRepository;

  @Autowired private PropuestaMapper propuestaMapper;

  @Autowired private SancionPropuestaRepository sancionPropuestaRepository;

  @Autowired private ResolucionPropuestaRepository resolucionPropuestaRepository;

  @Autowired private RegistroHistoricoRepository registroHistoricoRepository;

  @Autowired ExpedienteUtil expedienteUtil;
  @Autowired ExpedienteUseCase expedienteUseCase;

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelPropuestas(
      Integer idCartera,
      Integer mes,
      InformePropuestaInputDTO informePropuestaInputDTO,
      Usuario usuario,
      Boolean posesionNegociada)
      throws Exception {

    Perfil perfilUsu = usuario.getPerfil();

    if (perfilUsu.getNombre().equals("Responsable de cartera")
        || perfilUsu.getNombre().equals("Supervisor")
        || usuario.esAdministrador()) {
      var datos = this.createPropuestaDataForExcel();
      if (idCartera != null) {
        Cartera cartera =
            carteraRepository
                .findById(idCartera)
                .orElseThrow(() -> new NotFoundException("Propuesta", idCartera));
        this.addPropuestaDataForExcel(
            cartera, datos, mes, informePropuestaInputDTO, posesionNegociada, usuario);
      } else {
        for (Cartera cartera : carteraRepository.findAll()) {
          this.addPropuestaDataForExcel(
              cartera, datos, mes, informePropuestaInputDTO, posesionNegociada, usuario);
        }
      }
      return datos;
    } else {
      throw new IllegalArgumentException(
          "El tipo del usuario debe ser Responsable de cartera o Supervisor para poder generar el Informe");
    }
  }

  private LinkedHashMap<String, List<Collection<?>>> createPropuestaDataForExcel() {
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expediente = new ArrayList<>();
    expediente.add(ExpedienteCarteraExcel.cabeceras);

    List<Collection<?>> propuestas = new ArrayList<>();
    propuestas.add(PropuestaExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      datos.put("Connection", expediente);
      datos.put("Proposals", propuestas);

    } else {
      datos.put("Expediente", expediente);
      datos.put("Propuestas", propuestas);
    }

    return datos;
  }

  private void addPropuestaDataForExcel(
      Cartera cartera,
      LinkedHashMap<String, List<Collection<?>>> datos,
      Integer mes,
      InformePropuestaInputDTO informePropuestaInputDTO,
      Boolean posesionNegociada,
      Usuario usuario)
      throws ParseException, NotFoundException {

    List<Collection<?>> expedientes;
    List<Collection<?>> propuestas;

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      expedientes = datos.get("Connection");
      propuestas = datos.get("Proposals");

    } else {
      expedientes = datos.get("Expediente");
      propuestas = datos.get("Propuestas");
    }

    if (posesionNegociada) {
      List<ExpedientePosesionNegociada> expedientesFilter = new ArrayList<>();
      if (usuario.esAdministrador()) {
        expedientesFilter =
            expedientePosesionNegociadaRepository.findAllByCarteraId(cartera.getId());
      } else {
        expedientesFilter =
            expedientePosesionNegociadaRepository.findAllByCarteraIdAndAsignacionesUsuarioId(
                cartera.getId(), usuario.getId());
      }
      List<ExpedientePosesionNegociada> expedientePorMes =
          expedientesFilter.stream()
              .filter(
                  expediente ->
                      expedienteUseCase.filtroMesInformesPosesionNegociada(expediente, mes))
              .collect(Collectors.toList());

      if (informePropuestaInputDTO.getEstadoExpediente() != null)
        expedientePorMes =
            expedientePorMes.stream()
                .filter(
                    expediente ->
                        expedienteUtil.getEstado((ExpedientePosesionNegociada) expediente) != null
                            ? expedienteUtil
                                .getEstado((ExpedientePosesionNegociada) expediente)
                                .getId()
                                .equals(informePropuestaInputDTO.getEstadoExpediente())
                            : true)
                .collect(Collectors.toList());

      List<ExpedientePosesionNegociada> expedientesFiltrados =
          expedientePorMes.stream()
              .filter(
                  expediente -> {
                    Date fCE = expediente.getFechaCreacion();
                    Date fCI = informePropuestaInputDTO.getFechaCreacionExpedienteInicial();
                    Date fCF = informePropuestaInputDTO.getFechaCreacionExpedienteFinal();
                    if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;

                    Date fRE = expediente.getFechaCierre();
                    Date fRI = informePropuestaInputDTO.getFechaCierreExpedienteInicial();
                    Date fRF = informePropuestaInputDTO.getFechaCierreExpedienteFinal();
                    if (!expedienteUtil.comprobarEntre(fRE, fRI, fRF)) return false;

                    return usuario.contieneExpediente(expediente.getId(), true);
                  })
              .collect(Collectors.toList());

      for (ExpedientePosesionNegociada expediente : expedientesFiltrados) {
        if (expediente.getPropuestas().size() == 0) {
          continue;
        }
        Evento accion = getLastEvento(expediente.getId(), 1);
        Evento alerta = getLastEvento(expediente.getId(), 2);
        Evento actividad = getLastEvento(expediente.getId(), 3);
        Procedimiento p = expedienteUtil.getUltimoProcedimiento(expediente);
        expedientes.add(
            new ExpedienteCarteraExcel(expediente, accion, alerta, actividad, p).getValuesList());

        List<Propuesta> ultimoCambio =
            expediente.getPropuestas().stream()
                .filter(
                    propuesta -> {
                      if (!propuesta.getActivo()) return false;
                      if (informePropuestaInputDTO.getEstadoPropuesta() != null) {
                        if (propuesta.getEstadoPropuesta() == null
                            || !propuesta
                                .getEstadoPropuesta()
                                .getId()
                                .equals(informePropuestaInputDTO.getEstadoPropuesta())) {
                          return false;
                        }
                      }

                      Date fEP = propuesta.getFechaElevacion();
                      Date fEI = informePropuestaInputDTO.getFechaElevacionPropuestaInicial();
                      Date fEF = informePropuestaInputDTO.getFechaElevacionPropuestaFinal();
                      if (!expedienteUtil.comprobarEntre(fEP, fEI, fEF)) return false;

                      Date fSP = propuesta.getFechaSancion();
                      Date fSI = informePropuestaInputDTO.getFechaSancionPropuestaInicial();
                      Date fSF = informePropuestaInputDTO.getFechaSancionPropuestaFinal();
                      if (!expedienteUtil.comprobarEntre(fSP, fSI, fSF)) return false;

                      Date fRP = propuesta.getFechaResolucion();
                      Date fRI = informePropuestaInputDTO.getFechaResolucionPropuestaInicial();
                      Date fRF = informePropuestaInputDTO.getFechaResolucionPropuestaFinal();
                      if (!expedienteUtil.comprobarEntre(fRP, fRI, fRF)) return false;

                      Date fVP = propuesta.getFechaValidez();
                      Date fVI = informePropuestaInputDTO.getFechaValidezPropuestaInicial();
                      Date fVF = informePropuestaInputDTO.getFechaValidezPropuestaFinal();
                      if (!expedienteUtil.comprobarEntre(fVP, fVI, fVF)) return false;

                      Date fUP = propuesta.getFechaUltimoCambio();
                      Date fUI = informePropuestaInputDTO.getFechaUltimoCambioPropuestaInicial();
                      Date fUF = informePropuestaInputDTO.getFechaUltimoCambioPropuestaFinal();
                      if (!expedienteUtil.comprobarEntre(fUP, fUI, fUF)) return false;

                      return true;
                    })
                .collect(Collectors.toList());

        for (Propuesta propuesta : ultimoCambio) {
          List<IntervinientePropuestaDTO> dtos =
              propuestaMapper.initIntervinientePropuestaDTO(
                  propuesta.getIntervinientes(), propuesta.getContratos());
          List<IntervinienteDTOToList> toList =
              dtos.stream()
                  .map(IntervinientePropuestaDTO::getInterviniente)
                  .collect(Collectors.toList());
          propuestas.add(new PropuestaExcel(propuesta, toList, posesionNegociada).getValuesList());
        }
      }
    } else {
      List<Expediente> expedientesFilter = new ArrayList<>();
      if (usuario.esAdministrador()) {
        expedientesFilter = expedienteRepository.findAllByCarteraId(cartera.getId());
      } else {
        expedientesFilter =
            expedienteRepository.findAllByCarteraIdAndAsignacionesUsuarioId(
                cartera.getId(), usuario.getId());
      }

      List<Expediente> expedientePorMes =
          expedientesFilter.stream()
              .filter(expediente -> expedienteUseCase.filtroMesInformes(expediente, mes))
              .collect(Collectors.toList());

      if (informePropuestaInputDTO.getEstadoExpediente() != null)
        expedientePorMes =
            expedientePorMes.stream()
                .filter(
                    expediente ->
                        expedienteUtil
                            .getEstado((Expediente) expediente)
                            .getId()
                            .equals(informePropuestaInputDTO.getEstadoExpediente()))
                .collect(Collectors.toList());

      List<Expediente> expedientesFiltrados =
          expedientePorMes.stream()
              .filter(
                  expediente -> {
                    Date fCE = expediente.getFechaCreacion();
                    Date fCI = informePropuestaInputDTO.getFechaCreacionExpedienteInicial();
                    Date fCF = informePropuestaInputDTO.getFechaCreacionExpedienteFinal();
                    if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;

                    Date fRE = expediente.getFechaCierre();
                    Date fRI = informePropuestaInputDTO.getFechaCierreExpedienteInicial();
                    Date fRF = informePropuestaInputDTO.getFechaCierreExpedienteFinal();
                    if (!expedienteUtil.comprobarEntre(fRE, fRI, fRF)) return false;

                    return usuario.contieneExpediente(expediente.getId(), false);
                  })
              .collect(Collectors.toList());

      for (Expediente expediente : expedientesFiltrados) {

        if (expediente.getPropuestas().size() == 0) {
          continue;
        }
        Evento accion = getLastEvento(expediente.getId(), 1);
        Evento alerta = getLastEvento(expediente.getId(), 2);
        Evento actividad = getLastEvento(expediente.getId(), 3);
        Procedimiento p = expedienteUtil.getUltimoProcedimiento(expediente);
        expedientes.add(
            new ExpedienteCarteraExcel(expediente, accion, alerta, actividad, p).getValuesList());

        List<Propuesta> ultimoCambio =
            expediente.getPropuestas().stream()
                .filter(
                    propuesta -> {
                      if (informePropuestaInputDTO.getEstadoPropuesta() != null) {
                        if (propuesta.getEstadoPropuesta() == null
                            || !propuesta
                                .getEstadoPropuesta()
                                .getId()
                                .equals(informePropuestaInputDTO.getEstadoPropuesta())) {
                          return false;
                        }
                      }

                      Date fEP = propuesta.getFechaElevacion();
                      Date fEI = informePropuestaInputDTO.getFechaElevacionPropuestaInicial();
                      Date fEF = informePropuestaInputDTO.getFechaElevacionPropuestaFinal();
                      if (!expedienteUtil.comprobarEntre(fEP, fEI, fEF)) return false;

                      Date fSP = propuesta.getFechaSancion();
                      Date fSI = informePropuestaInputDTO.getFechaSancionPropuestaInicial();
                      Date fSF = informePropuestaInputDTO.getFechaSancionPropuestaFinal();
                      if (!expedienteUtil.comprobarEntre(fSP, fSI, fSF)) return false;

                      Date fRP = propuesta.getFechaResolucion();
                      Date fRI = informePropuestaInputDTO.getFechaResolucionPropuestaInicial();
                      Date fRF = informePropuestaInputDTO.getFechaResolucionPropuestaFinal();
                      if (!expedienteUtil.comprobarEntre(fRP, fRI, fRF)) return false;

                      Date fVP = propuesta.getFechaValidez();
                      Date fVI = informePropuestaInputDTO.getFechaValidezPropuestaInicial();
                      Date fVF = informePropuestaInputDTO.getFechaValidezPropuestaFinal();
                      if (!expedienteUtil.comprobarEntre(fVP, fVI, fVF)) return false;

                      Date fUP = propuesta.getFechaUltimoCambio();
                      Date fUI = informePropuestaInputDTO.getFechaUltimoCambioPropuestaInicial();
                      Date fUF = informePropuestaInputDTO.getFechaUltimoCambioPropuestaFinal();
                      if (!expedienteUtil.comprobarEntre(fUP, fUI, fUF)) return false;

                      return true;
                    })
                .collect(Collectors.toList());

        for (Propuesta propuesta : ultimoCambio) {
          List<IntervinientePropuestaDTO> dtos =
              propuestaMapper.initIntervinientePropuestaDTO(
                  propuesta.getIntervinientes(), propuesta.getContratos());
          List<IntervinienteDTOToList> toList =
              dtos.stream()
                  .map(IntervinientePropuestaDTO::getInterviniente)
                  .collect(Collectors.toList());
          propuestas.add(new PropuestaExcel(propuesta, toList, posesionNegociada).getValuesList());
        }
      }
    }
  }

  public Evento getLastEvento(Integer idExpediente, Integer clase) {
    try {
      List<Integer> clases = new ArrayList<>();
      clases.add(clase);
      List<Evento> eventos =
          eventoUtil
              .getFilteredEventos(
                  null,
                  null,
                  null,
                  1,
                  idExpediente,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  clases,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  "fechaCreacion",
                  "desc",
                  0, 100);

      Evento result = null;
      if (!eventos.isEmpty()) result = eventos.get(0);
      return result;
    } catch (Exception e) {
      return null;
    }
  }
}
