package com.haya.alaska.propuesta.aplication;

import com.haya.alaska.propuesta.infrastructure.controller.dto.InformePropuestaInputDTO;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface PropuestaExcelUseCase {

  LinkedHashMap<String, List<Collection<?>>> findExcelPropuestas(Integer idCartera, Integer mes, InformePropuestaInputDTO informePropuestaInputDTO,
                                                                 Usuario usuario, Boolean posesionNegociada) throws Exception;

}
