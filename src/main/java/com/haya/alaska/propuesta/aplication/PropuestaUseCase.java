package com.haya.alaska.propuesta.aplication;

import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoInputDTO;
import com.haya.alaska.bien.infrastructure.controller.dto.BienOutputDto;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.integraciones.pbc.infrastructure.controller.dto.UpdateResultadoPBCInputDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteDTOToList;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.controller.dto.*;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.InvalidCatalogException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface PropuestaUseCase {
  ListWithCountDTO getFilteredPropuestas(
    Integer idExpediente, Boolean posesionNegociada, Integer idPropuesta, String fechaAlta, String usuario, String clase, String tipo,
    String subtipoPropuesta, String estadoPropuesta, String sancion, String resolucion, String fechaUltimoCambio,
    String importeTotal, String orderField, String orderDirection, Integer size, Integer page);

  ListWithCountDTO getAllPropuestasWithBienes(
    Integer idExpediente, Boolean posesionNegociada) throws NotFoundException;

  ListWithCountDTO getAllPropuestasWithBienesToInformes(
    Integer idExpediente, Boolean posesionNegociada) throws NotFoundException;

  ListWithCountDTO getAllPropuestasWithBienesOcupantes(
    Integer idExpediente, Boolean posesionNegociada) throws NotFoundException;

  PropuestaDTO getPropuesta(Integer idExpediente, Integer idPropuesta, Boolean posesionNegociada) throws NotFoundException, InvalidCatalogException;

  PropuestaDTO updateEstadoPropuesta(
    PropuestaInputDTO propuestaInputDTO, Integer idExpediente, Integer idPropuesta, Usuario user)
    throws NotFoundException, InvalidCatalogException, RequiredValueException;

  PropuestaDTO updateContratosPropuesta(
    PropuestaInputDTO propuestaInputDTO, Integer idExpediente, Integer idPropuesta)
    throws NotFoundException, InvalidCatalogException;

  PropuestaDTO updateBienesPropuesta(
    PropuestaInputDTO propuestaInputDTO, Integer idExpediente, Integer idPropuesta, Boolean posesionNegociada)
    throws NotFoundException, InvalidCatalogException;

  PropuestaDTO updateIntervinientesPropuesta(
    PropuestaInputDTO propuestaInputDTO, Integer idExpediente, Integer idPropuesta, Boolean posesionNegociada)
    throws NotFoundException, InvalidCatalogException;

  void updateResultadoPBC(Integer idPropuesta, UpdateResultadoPBCInputDTO updateResultadoPBCInputDTO, Usuario usuario)
    throws NotFoundException, InvalidCatalogException, RequiredValueException;

  PropuestaDTO addImportesPagoPropuesta(
    ImportePropuestaInputDTO importePropuestaInputDTO, Integer idExpediente, Integer idPropuesta, Boolean posesionNegociada)
    throws NotFoundException, InvalidCatalogException, RequiredValueException;

  Boolean actualizarFechaFormalizacion(Integer idPropuesta, Date fecha, Usuario user) throws NotFoundException, ForbiddenException;

  void deleteImportesPagoPropuesta(Integer idExpediente, Integer idPropuesta, List<Integer> importes)
    throws NotFoundException;

  void deleteAcuerdosPagoPropuesta(Integer idExpediente, Integer idPropuesta, List<Integer> acuerdos)
    throws NotFoundException;

  void bajaPropuesta(Integer idExpediente, Integer idPropuesta) throws NotFoundException;

  PropuestaDTO actualizarFechaValidezConDto(Integer idPropuesta, PropuestaFechaDTO propuesta) throws NotFoundException, ParseException, InvalidCatalogException;

  Propuesta actualizarFechaValidez(Propuesta propuesta) throws RequiredValueException;

  PropuestaDTO addAcuerdoPagoPropuesta(
    AcuerdoPagoInputDTO acuerdoPagoInputDTO, Integer idExpediente, Integer idPropuesta, Boolean posesionNegociada)
    throws NotFoundException, InvalidCatalogException;

  void deleteContratoEnImportePropuesta(
    Integer idExpediente, Integer idPropuesta, List<ContratoImporteAcuerdoInputDTO> contratos, Boolean pn)
    throws NotFoundException;

  void deleteContratoEnAcuerdoPagoPropuesta(
    Integer idExpediente, Integer idPropuesta, List<ContratoImporteAcuerdoInputDTO> contratos, Boolean pn)
    throws NotFoundException;

  PropuestaDTO findPropuestaByIdOrigen(String id) throws InvalidCatalogException;

  PropuestaDTO createPropuesta(
      PropuestaInputDTO propuestaInputDTO,
      Integer idExpediente,
      Usuario usuario,
      Boolean posesionNegociada)
      throws Exception;

  PropuestaDTO createPrecios(Integer idPropuesta, PreciosPropuestaDTO preciosPropuestaDTO) throws Exception;

  List<SubtipoPropuestaDTO> findSubtipoPropuestaByTipoPropuesta(Integer tipoPropuestaId);

  PropuestaDTO getDetallePropuestaById(Integer propuestaId) throws NotFoundException, InvalidCatalogException;

  List<Contrato> getContratosRelacionados(
    List<Contrato> contratosPropuesta, List<Contrato> restoContratos);

  ComentarioPropuestaDTO createComentario(
    Integer idPropuesta, Integer idExpediente, Usuario usuario, String comentario)
    throws NotFoundException;

  List<CatalogoMinInfoDTO> findTipoPropuestaByPN(Boolean posesiónNegociada) throws NotFoundException;

  void deleteComentario(Integer idExpediente, Integer idPropuesta, Integer id, Usuario user) throws NotFoundException, ForbiddenException;

  ComentarioPropuestaDTO updateComentario(Integer idComentario, String comentario, Usuario usuario) throws NotFoundException, ForbiddenException;

  void createDocumento(Integer idExpediente, Integer idPropuesta, MultipartFile file, String idMatricula, Integer idTipoPropuesta, Boolean posesionNegociada, Usuario usuario) throws Exception;

  void createListaDocumentos(Integer idExpediente, Integer idPropuesta, List<MultipartFile> files, List<String> idsMatricula, Integer idTipoPropuesta, Boolean posesionNegociada, Usuario usuario) throws Exception;

  void createContenedor(String tipo, String clase, String idPropuesta, String nombre) throws Exception;

  public ListWithCountDTO<DocumentoDTO> findDocumentosPropuestas(Integer idContrato,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size, Integer page) throws NotFoundException, IOException;

  public ListWithCountDTO<DocumentoDTO> findDocumentosByExpedienteId(Integer idExpediente, Boolean posesionNegociada,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size, Integer page) throws NotFoundException, IOException;

  List<PropuestaDTOToList> findAllPropuestasByExpedienteId(Integer idExpediente, Boolean posesionNegociada) throws NotFoundException;

  List<BienOutputDto> bienesInPropuesta(Integer idPropuesta) throws NotFoundException;

  List<BienOutputDto> bienesWithTasAndCar(Integer idPropuesta) throws NotFoundException;

  List<BienOutputDto> bienesInPropuestaToInformes(Integer idPropuesta) throws NotFoundException;

  List<IntervinienteDTOToList> intervinientesInPropuesta(Integer idPropuesta) throws NotFoundException;

  List<OcupanteDTOToList> ocupantesInPropuesta(Integer idPropuesta) throws NotFoundException;

  BienOutputDto getBienById(Integer idBien) throws NotFoundException;

  List<DocumentoDTO>findDocumentosByPropuesta(Integer idPropuesta) throws NotFoundException, IOException;

  long countDocumentosByPropuesta(Integer idPropuesta) throws NotFoundException, IOException;

  PropuestaDTO updateCamposSareb(
      Integer idExpediente, Integer idPropuesta, Double importeColabora, Integer numColabora)
      throws NotFoundException, InvalidCatalogException;

  PropuestaDTO updateCamposCerberusPBC(
      Integer idExpediente,
      Integer idPropuesta,
      Integer numAdvisoryNote,
      String fechaEnvioCES,
      String fechaEnvioWorkingGroup,
      String fechaRecepcionAprobacionAdvisoryNote)
    throws NotFoundException, InvalidCatalogException, ParseException;

  List<CatalogoMinInfoDTO> getEstadosPropuesta(Integer idPropuesta) throws NotFoundException;
  List<CatalogoMinInfoDTO> getSubestadosPropuesta(Integer id) throws NotFoundException;

  void actualizarEstadoYSubestado(Usuario user, Integer idPropuesta, Integer estado, Integer subestado) throws NotFoundException, RequiredValueException;
}
