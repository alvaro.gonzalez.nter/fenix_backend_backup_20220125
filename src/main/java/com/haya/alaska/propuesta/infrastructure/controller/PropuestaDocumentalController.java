package com.haya.alaska.propuesta.infrastructure.controller;

import com.haya.alaska.propuesta.aplication.PropuestaUseCase;
import com.haya.alaska.shared.dto.MetadatoContenedorInputDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/propuestaDocumental")
public class PropuestaDocumentalController {

  @Autowired
  PropuestaUseCase propuestaUseCase;

  @ApiOperation(value="NuevoContenedor")
  @PostMapping("/contenedor")
  @ResponseStatus(HttpStatus.CREATED)
  public void createContenedorPropuesta(@RequestParam("tipo_expediente") String tipo_expediente,
                                        @RequestParam("clase_expediente") String clase_expediente,
                                        @RequestParam("idPropuesta") String idPropuesta,
                                        @RequestParam ("nombre") String nombre)
    throws Exception {
    propuestaUseCase.createContenedor(tipo_expediente,clase_expediente,idPropuesta,nombre);
  }
}
