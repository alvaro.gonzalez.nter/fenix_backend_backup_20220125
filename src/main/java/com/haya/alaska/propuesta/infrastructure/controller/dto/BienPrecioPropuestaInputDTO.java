package com.haya.alaska.propuesta.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BienPrecioPropuestaInputDTO {
  Integer idBien;
  Double precioWebVenta;
  Double precioMinimoVenta;
}
