package com.haya.alaska.propuesta.infrastructure.controller;

import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoInputDTO;
import com.haya.alaska.bien.infrastructure.controller.dto.BienOutputDto;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteDTOToList;
import com.haya.alaska.propuesta.aplication.PropuestaExcelUseCase;
import com.haya.alaska.propuesta.aplication.PropuestaUseCase;
import com.haya.alaska.propuesta.infrastructure.controller.dto.*;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.*;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientes/{idExpediente}/propuesta")
public class PropuestaController {
  @Autowired
  PropuestaUseCase propuestaUseCase;
  @Autowired
  UsuarioRepository usuarioRepository;
    /*@Autowired
    ServicioGestorDocumental servicioGestorDocumental;*/

  @Autowired
  private PropuestaExcelUseCase propuestaExcelUseCase;

  @ApiOperation(value = "Obtener propuesta", notes = "Devuelve la propuesta con id Origen enviado")
  @GetMapping("/byIdOrigen/{idPropuestaOrigen}")
  @Transactional
  public PropuestaDTO getContratoByIdCarga(
    @PathVariable("idPropuestaOrigen") String idPropuestaOrigen) throws InvalidCatalogException {
    return propuestaUseCase.findPropuestaByIdOrigen(idPropuestaOrigen);
  }

  @ApiOperation(value = "Añadir una propuesta a un expediente")
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @Transactional(rollbackFor = Exception.class)
  public PropuestaDTO createPropuesta(
      @PathVariable("idExpediente") Integer idExpediente,
      @RequestBody PropuestaInputDTO propuestaInputDTO,
      @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
      @ApiIgnore CustomUserDetails principal)
      throws Exception {
    return propuestaUseCase.createPropuesta(
        propuestaInputDTO, idExpediente, (Usuario) principal.getPrincipal(), posesionNegociada);
  }

  @ApiOperation(value = "Añadir precios de web y venta a una propuesta")
  @PostMapping("/{idPropuesta}/precios")
  @ResponseStatus(HttpStatus.CREATED)
  @Transactional(rollbackFor = Exception.class)
  public PropuestaDTO createPrecios(@PathVariable("idExpediente") Integer idExpediente,
                                    @PathVariable("idPropuesta") Integer idPropuesta,
                                    @Valid @RequestBody PreciosPropuestaDTO preciosPropuestaDTO) throws Exception {
    return propuestaUseCase.createPrecios(idPropuesta, preciosPropuestaDTO);
  }

  @ApiOperation(value = "Actualizar precios de web y venta a una propuesta")
  @PutMapping("/{idPropuesta}/precios")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @Transactional(rollbackFor = Exception.class)
  public PropuestaDTO updatePrecios(@PathVariable("idExpediente") Integer idExpediente,
                                    @PathVariable("idPropuesta") Integer idPropuesta,
                                    @Valid @RequestBody PreciosPropuestaDTO preciosPropuestaDTO) throws Exception {
    return propuestaUseCase.createPrecios(idPropuesta, preciosPropuestaDTO);
  }

  @ApiOperation(value = "Modificar el estado de una propuesta de un expediente")
  @PutMapping("/{idPropuesta}/estado")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public PropuestaDTO updateEstadoPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                            @PathVariable("idPropuesta") Integer idPropuesta,
                                            @RequestBody PropuestaInputDTO propuestaInputDTO,
                                            @ApiIgnore CustomUserDetails principal) throws Exception {

    return propuestaUseCase.updateEstadoPropuesta(propuestaInputDTO, idExpediente, idPropuesta, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Modificar los contratos asignados a una propuesta de un expediente")
  @PutMapping("/{idPropuesta}/contratos")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @Transactional(rollbackFor = Exception.class)
  public PropuestaDTO updateContratosPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                               @PathVariable("idPropuesta") Integer idPropuesta,
                                               @RequestBody PropuestaInputDTO propuestaInputDTO) throws Exception {
    return propuestaUseCase.updateContratosPropuesta(propuestaInputDTO, idExpediente, idPropuesta);
  }

  @ApiOperation(value = "Modificar las bienes asignados a una propuesta de un expediente")
  @PutMapping("/{idPropuesta}/bienes")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @Transactional(rollbackFor = Exception.class)
  public PropuestaDTO updateBienesPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                            @PathVariable("idPropuesta") Integer idPropuesta,
                                            @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                            @RequestBody PropuestaInputDTO propuestaInputDTO) throws Exception {
    return propuestaUseCase.updateBienesPropuesta(propuestaInputDTO, idExpediente, idPropuesta, posesionNegociada);
  }

  @ApiOperation(value = "Modificar las intervinientes asignados a una propuesta de un expediente")
  @PutMapping("/{idPropuesta}/intervinientes")
  @ResponseStatus(HttpStatus.ACCEPTED)
  @Transactional(rollbackFor = Exception.class)
  public PropuestaDTO updateIntervinientePropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                                   @PathVariable("idPropuesta") Integer idPropuesta,
                                                   @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                                   @RequestBody PropuestaInputDTO propuestaInputDTO) throws Exception {
    return propuestaUseCase.updateIntervinientesPropuesta(propuestaInputDTO, idExpediente, idPropuesta, posesionNegociada);
  }

  @ApiOperation(value = "Añadir un pago asignado a una propuesta de un expediente")
  @PostMapping("/{idPropuesta}/importes")
  @ResponseStatus(HttpStatus.CREATED)
  @Transactional(rollbackFor = Exception.class)
  public PropuestaDTO addImportesPagoPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                               @PathVariable("idPropuesta") Integer idPropuesta,
                                               @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                               @RequestBody ImportePropuestaInputDTO importePropuestaInputDTO) throws Exception {
    return propuestaUseCase.addImportesPagoPropuesta(importePropuestaInputDTO, idExpediente, idPropuesta, posesionNegociada);
  }

  @ApiOperation(value = "Modificar las intervinientes asignados a una propuesta de un expediente")
  @PutMapping("/{idPropuesta}/fechaResolucion")
  public Boolean actualizarFechaFormalizacion(@PathVariable("idPropuesta") Integer idPropuesta,
                                              @RequestParam(value = "fecha") Date fecha,
                                              @ApiIgnore CustomUserDetails principal) throws NotFoundException, ForbiddenException {
    return propuestaUseCase.actualizarFechaFormalizacion(idPropuesta, fecha, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Borrar importes de una propuesta de un expediente")
  @DeleteMapping("/{idPropuesta}/importes")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void deleteImportesPagoPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                          @PathVariable("idPropuesta") Integer idPropuesta,
                                          @RequestBody List<Integer> importes) throws Exception {
    propuestaUseCase.deleteImportesPagoPropuesta(idExpediente, idPropuesta, importes);
  }

  @ApiOperation(value = "Añadir un acuerdo de pago asignado a una propuesta de un expediente")
  @PostMapping("/{idPropuesta}/acuerdopago")
  @ResponseStatus(HttpStatus.CREATED)
  @Transactional(rollbackFor = Exception.class)
  public PropuestaDTO addAcuerdoPagoPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                              @PathVariable("idPropuesta") Integer idPropuesta,
                                              @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                              @RequestBody AcuerdoPagoInputDTO acuerdoPagoInputDTO) throws Exception {
    return propuestaUseCase.addAcuerdoPagoPropuesta(acuerdoPagoInputDTO, idExpediente, idPropuesta, posesionNegociada);
  }

  @ApiOperation(value = "Borrar acuerdos de pago de una propuesta de un expediente")
  @DeleteMapping("/{idPropuesta}/acuerdospago")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void deleteAcuerdosPagoPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                          @PathVariable("idPropuesta") Integer idPropuesta,
                                          @RequestBody List<Integer> acuerdos) throws Exception {
    propuestaUseCase.deleteAcuerdosPagoPropuesta(idExpediente, idPropuesta, acuerdos);
  }

  @ApiOperation(value = "Borrar contratos asignados a importes de una propuesta de un expediente")
  @DeleteMapping("/{idPropuesta}/importe/contratos")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void deleteContratoImportePropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                             @PathVariable("idPropuesta") Integer idPropuesta,
                                             @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                             @RequestBody List<ContratoImporteAcuerdoInputDTO> contratos) throws Exception {
    propuestaUseCase.deleteContratoEnImportePropuesta(idExpediente, idPropuesta, contratos, posesionNegociada);
  }

  @ApiOperation(value = "Borrar contratos asignados a acuerdos de pago de una propuesta de un expediente")
  @DeleteMapping("/{idPropuesta}/acuerdopago/contratos")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void deleteContratoAcuerdoPagoPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                                 @PathVariable("idPropuesta") Integer idPropuesta,
                                                 @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                                 @RequestBody List<ContratoImporteAcuerdoInputDTO> contratos) throws Exception {
    propuestaUseCase.deleteContratoEnAcuerdoPagoPropuesta(idExpediente, idPropuesta, contratos, posesionNegociada);
  }

  @ApiOperation(value = "Añadir un documento a una propuesta")
  @PostMapping("/{idPropuesta}/contratos/{idContrato}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                       @PathVariable("idContrato") Integer idContrato,
                                       @PathVariable("idPropuesta") Integer idPropuesta,
                                       @RequestPart("file") @NotNull @NotBlank MultipartFile file,
                                       @RequestParam("idMatricula") String idMatricula,
                                       @RequestParam("idTipoPropuesta") Integer idTipoPropuesta,
                                       @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                       @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    propuestaUseCase.createDocumento(idExpediente, idPropuesta, file, idMatricula, idTipoPropuesta, posesionNegociada, usuario);
  }

  @ApiOperation(value = "Añadir documentos a una propuesta")
  @PostMapping("/{idPropuesta}/contratos/{idContrato}/lista-documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentosPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                        @PathVariable("idContrato") Integer idContrato,
                                        @PathVariable("idPropuesta") Integer idPropuesta,
                                        @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
                                        @RequestParam(value = "idsMatricula") List<String> idsMatricula,
                                        @RequestParam Integer idTipoPropuesta,
                                        @RequestPart(value="file") MultipartFile[] request,
                                        @ApiIgnore CustomUserDetails principal)
    throws Exception {

    //obtenemos los ficheros recibidos
   // String idMatricula = request.getParameter("idMatricula");
   // Integer idTipoPropuesta = 4;
    // List<MultipartFile> files = request.getFiles("file");
    List<MultipartFile> files= Arrays.asList(request);
    Usuario usuario = (Usuario) principal.getPrincipal();
    propuestaUseCase.createListaDocumentos(idExpediente, idPropuesta, files, idsMatricula, idTipoPropuesta, posesionNegociada, usuario);
  }

  @ApiOperation(value = "Obtener documentación de los bienes por expediente o contrato",
    notes = "Devuelve la documentación disponible de los bienes del expediente o contrato con id enviado")
  @GetMapping("/contratos/{idContrato}/documentos")
  @Transactional
  public ListWithCountDTO<DocumentoDTO> getDocumentosByExpediente(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idContrato") Integer idContrato,
    @RequestParam("posesionNegociada") Boolean posesionNegociada,
    @RequestParam(value = "id", required = false) String idDocumento,
    @RequestParam(value = "nombreArchivo", required = false) String nombreArchivo,
    @RequestParam(value = "idHaya", required = false) String idHaya,
    @RequestParam(value = "usuarioCreador", required = false) String usuarioCreador,
    @RequestParam(value = "tipo", required = false) String tipo,
    @RequestParam(value = "idEntidad", required = false) String idEntidad,
    @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) throws NotFoundException, IOException {
    if (idContrato == -1) {
      return propuestaUseCase.findDocumentosByExpedienteId(idExpediente, posesionNegociada, idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, nombreArchivo, idHaya, orderDirection, orderField, size, page);
    } else {
      return propuestaUseCase.findDocumentosPropuestas(idContrato, idDocumento, usuarioCreador, tipo, idEntidad, fechaActualizacion, nombreArchivo, idHaya, orderDirection, orderField, size, page);
    }
  }

 /* @ApiOperation(value="Obtener documentación de los propuestas de un expediente",
  notes="Devuelve la documentación disponible de las propuestas del contrato con id enviado")
  @GetMapping("/documentos")
  public List<DocumentoDTO> getDocumentosByExpedienteID(
    @PathVariable("idExpediente") Integer idExpediente) throws NotFoundException, IOException {
    return propuestaUseCase.findDocumentosByExpedienteId(idExpediente);
  }*/


  @ApiOperation(value = "Listado Todas las Propuestas de un expediente",
    notes = "Devuelve todas las propuestas de pago")
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public ListWithCountDTO getFilteredPropuestas(
    @PathVariable("idExpediente") Integer idExpediente,
    @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
    @RequestParam(value = "id", required = false) Integer id,
    @RequestParam(value = "fechaAlta", required = false) String fechaAlta,
    @RequestParam(value = "usuario", required = false) String usuario,
    @RequestParam(value = "clasePropuesta", required = false) String clasePropuesta,
    @RequestParam(value = "tipoPropuesta", required = false) String tipoPropuesta,
    @RequestParam(value = "subtipoPropuesta", required = false) String subtipoPropuesta,
    @RequestParam(value = "estadoPropuesta", required = false) String estadoPropuesta,
    @RequestParam(value = "sancionPropuesta", required = false) String sancionPropuesta,
    @RequestParam(value = "resolucionPropuesta", required = false) String resolucionPropuesta,
    @RequestParam(value = "fechaUltimoCambio", required = false) String fechaUltimoCambio,
    @RequestParam(value = "importeTotal", required = false) String importeTotal,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size", required = true) Integer size,
    @RequestParam(value = "page", required = true) Integer page) {

    ListWithCountDTO propuestasDTO =
      propuestaUseCase.getFilteredPropuestas(idExpediente, posesionNegociada, id, fechaAlta, usuario, clasePropuesta, tipoPropuesta,
        subtipoPropuesta, estadoPropuesta, sancionPropuesta, resolucionPropuesta, fechaUltimoCambio,
        importeTotal, orderField, orderDirection, size, page);
    return propuestasDTO;// new PageImpl<>(propuestasDTO, PageRequest.of(page, size),propuestasDTO.size());

  }

  @ApiOperation(value = "Listado todas las propuestas de un expediente",
    notes = "Devuelve todas las propuestas de un expediente sin filtrar y que tengan bienes asociados, con tasaciones, valoraciones y cargas")
  @GetMapping("/all")
  @ResponseStatus(HttpStatus.OK)
  public ListWithCountDTO getAllPropuestas(
    @PathVariable("idExpediente") Integer idExpediente,
    @RequestParam(value = "posesionNegociada") Boolean posesionNegociada) throws NotFoundException {

    ListWithCountDTO propuestasDTO =
      propuestaUseCase.getAllPropuestasWithBienes(idExpediente, posesionNegociada);
    return propuestasDTO;
  }

  @ApiOperation(value = "Listado todas las propuestas de un expediente ",
    notes = "Devuelve todas las propuestas de un expediente sin filtrar y que tengan bienes asociados")
  @GetMapping("/all-with-bienes")
  @ResponseStatus(HttpStatus.OK)
  public ListWithCountDTO getAllPropuestasToInformes(
    @PathVariable("idExpediente") Integer idExpediente,
    @RequestParam(value = "posesionNegociada") Boolean posesionNegociada) throws NotFoundException {

    ListWithCountDTO propuestasDTO =
      propuestaUseCase.getAllPropuestasWithBienesToInformes(idExpediente, posesionNegociada);
    return propuestasDTO;
  }

  @ApiOperation(value = "Listado todas las propuestas de un expediente que tengan bienes e intervinientes/ocupantes",
    notes = "Devuelve todas las propuestas de un expediente sin filtrar y que tengan bienes asociados")
  @GetMapping("/all-with-bienes-ocupantes")
  @ResponseStatus(HttpStatus.OK)
  public ListWithCountDTO getAllPropuestasWithBienesOcupantes(
    @PathVariable("idExpediente") Integer idExpediente,
    @RequestParam(value = "posesionNegociada") Boolean posesionNegociada) throws NotFoundException {

    ListWithCountDTO propuestasDTO =
      propuestaUseCase.getAllPropuestasWithBienesOcupantes(idExpediente, posesionNegociada);
    return propuestasDTO;
  }

  @ApiOperation(value = "Devuelve los datos de una propuesta ")
  @GetMapping("/{idPropuesta}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<PropuestaDTO> getPropuesta(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idPropuesta") Integer idPropuesta,
    @RequestParam(value = "posesionNegociada") Boolean posesionNegociada) {

    try {
      var propuestaDTO = propuestaUseCase.getPropuesta(idExpediente, idPropuesta, posesionNegociada);
      return ResponseEntity.ok(propuestaDTO);
    } catch (NotFoundException | InvalidCatalogException e) {
      return ResponseEntity.notFound().build();
    }
  }

  @ApiOperation(value = "Devuelve los bienes de una propuesta con tasaciones, valoraciones y cargas")
  @GetMapping("/{idPropuesta}/bienes")
  @ResponseStatus(HttpStatus.OK)
  public List<BienOutputDto> getBienesPropuestasWithTVC(
    @PathVariable("idPropuesta") Integer idPropuesta) throws NotFoundException {
    List<BienOutputDto> result = propuestaUseCase.bienesInPropuesta(idPropuesta);
    return result;
  }

  @ApiOperation(value = "Devuelve los bienes de una propuesta con tasaciones, valoraciones y cargas")
  @GetMapping("/{idPropuesta}/bienesWhitTasAndCar")
  @ResponseStatus(HttpStatus.OK)
  public List<BienOutputDto> getBienesPropuestasWithTC(
    @PathVariable("idPropuesta") Integer idPropuesta) throws NotFoundException {
    List<BienOutputDto> result = propuestaUseCase.bienesWithTasAndCar(idPropuesta);
    return result;
  }

  @ApiOperation(value = "Devuelve los bienes de una propuesta")
  @GetMapping("/{idPropuesta}/bienes-sin-tvc")
  @ResponseStatus(HttpStatus.OK)
  public List<BienOutputDto> getBienesPropuestasSinTVC(
    @PathVariable("idPropuesta") Integer idPropuesta) throws NotFoundException {
    List<BienOutputDto> result = propuestaUseCase.bienesInPropuestaToInformes(idPropuesta);
    return result;
  }

  @ApiOperation(value = "Devuelve los intervinientes de una propuesta")
  @GetMapping("/{idPropuesta}/intervinientes")
  @ResponseStatus(HttpStatus.OK)
  public List<IntervinienteDTOToList> getIntervinientesPropuestas(
    @PathVariable("idPropuesta") Integer idPropuesta) throws NotFoundException {
    List<IntervinienteDTOToList> result = propuestaUseCase.intervinientesInPropuesta(idPropuesta);
    return result;
  }

  @ApiOperation(value = "Devuelve los ocupantes de una propuesta")
  @GetMapping("/{idPropuesta}/ocupantes")
  @ResponseStatus(HttpStatus.OK)
  public List<OcupanteDTOToList> getOcupantesPropuestas(
    @PathVariable("idPropuesta") Integer idPropuesta) throws NotFoundException {
    List<OcupanteDTOToList> result = propuestaUseCase.ocupantesInPropuesta(idPropuesta);
    return result;
  }

  @ApiOperation(value = "Dar de baja una propuesta")
  @DeleteMapping("/{idPropuesta}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void bajaPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                            @PathVariable("idPropuesta") Integer idPropuesta) throws Exception {
    propuestaUseCase.bajaPropuesta(idExpediente, idPropuesta);
  }

  @ApiOperation(value = "Modificar fecha de validez propuesta")
  @PutMapping("/{idPropuesta}")
  @ResponseStatus(HttpStatus.OK)
  public PropuestaDTO actualizarFecha(@PathVariable("idPropuesta") Integer idPropuesta,
                                      @RequestBody PropuestaFechaDTO propuestaDTO) throws Exception {
    return propuestaUseCase.actualizarFechaValidezConDto(idPropuesta, propuestaDTO);
  }

//	  @ApiOperation(value = "Modificar un acuerdo de pago  de un expediente")
//	  @PutMapping("/{idAcuerdo}")
//	  @ResponseStatus(HttpStatus.ACCEPTED)
//	  public AcuerdoPagoDTO updateDocumentoContrato(@PathVariable("idExpediente") Integer idExpediente
//			  , @PathVariable("idAcuerdo") Integer idAcuerdo
//              , @RequestBody @Valid AcuerdoPagoInputDTO acuerdoPagoInputDTO)     throws NotFoundException,InvalidCodeException
//	  {
//		 return acuerdoPagoUseCase.updateAcuerdo(idExpediente,idAcuerdo,acuerdoPagoInputDTO);
//	  }
//
//	  @ApiOperation(value = "Borrar un acuerdo de pago  de un expediente")
//	  @DeleteMapping("/{idAcuerdo}")
//	  @ResponseStatus(HttpStatus.ACCEPTED)
//	  public void deleteDocumentoContrato(@PathVariable("idExpediente") Integer idExpediente  ,@PathVariable("idAcuerdo") Integer idAcuerdo,
//			  @RequestParam(value = "activo", defaultValue="false" ) Boolean activo )
//			  throws NotFoundException
//	  {
//			  acuerdoPagoUseCase.ponerAcuerdoInactivo(idExpediente,idAcuerdo,activo);
//	  }
//
//	  @ApiOperation(value = "Descargar información de un acuerdo  en Excel",
//	          notes = "Descargar información en Excel de acuerdo  con ID enviado")
//	  @GetMapping("/{idAcuerdo}/excel")
//	  public ResponseEntity<InputStreamResource> getExcelExpedienteById(@PathVariable("idExpediente") Integer idExpediente,
//			  @PathVariable("idAcuerdo") Integer idAcuerdo) throws Exception {
//	    ByteArrayInputStream excel = excelExport
//	            .create(acuerdoPagoUseCase.findExcelExpedienteById(idExpediente,idAcuerdo));
//
//	    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//	    return excelExport.download(excel, "acuerdopago_"+idExpediente + "_"+idAcuerdo+"_" + date);
//	  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ErrorResponse handleValidationExceptions(MethodArgumentNotValidException ex) {
    StringBuffer errorStr = new StringBuffer();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      String errorMessage = error.getDefaultMessage();
      errorStr.append(" Campo: '").append(fieldName).append("' ").append(errorMessage).append("   ");
    });

    return ErrorResponse.builder().status(HttpStatus.BAD_REQUEST.value()).
      error(errorStr.toString()).
      message("Error al validar JSON").
      timestamp(Instant.now().toString()).
      build();
  }

  @ApiOperation(value = "Añadir Comentario")
  @PostMapping("/{idPropuesta}/comentario")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public ComentarioPropuestaDTO createComentarioPropuesta(@PathVariable("idExpediente") Integer idExpediente,
                                                          @PathVariable("idPropuesta") Integer idPropuesta,
                                                          @RequestParam(value = "comentario") String comentario,
                                                          @ApiIgnore CustomUserDetails principal) throws Exception {
    return propuestaUseCase.createComentario(idPropuesta, idExpediente, (Usuario) principal.getPrincipal(), comentario);
  }

  @ApiOperation(value = "Borrar comentario de la propuesta")
  @DeleteMapping("/{idPropuesta}/comentarios/{idComentario}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void deleteComentario(@PathVariable("idComentario") Integer idComentario,
                               @PathVariable("idExpediente") Integer idExpediente,
                               @PathVariable("idPropuesta") Integer idPropuesta,
                               @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario user = (Usuario) principal.getPrincipal();
    propuestaUseCase.deleteComentario(idExpediente, idPropuesta, idComentario, user);
  }

  @ApiOperation(value = "Modificar comentario de la propuesta")
  @PutMapping("/{idPropuesta}/comentarios/{idComentario}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void updateComentario(@PathVariable("idComentario") Integer idComentario,
                               @RequestParam("comentario") String comentario,
                               @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario user = (Usuario) principal.getPrincipal();
    propuestaUseCase.updateComentario(idComentario, comentario, user);
  }


  @ApiOperation(value = "Descargar el Informe de propuestas en Excel",
    notes = "Descargar información en Excel de todas las propuestas")
  @PutMapping("/{idCartera}/excel")
  public ResponseEntity<InputStreamResource> getExcelPropuestas(@PathVariable("idCartera") Integer idCartera,
                                                                @RequestParam("mes") Integer mes,
                                                                @RequestBody @Valid InformePropuestaInputDTO informePropuestaInputDTO,
                                                                @ApiIgnore CustomUserDetails usuario) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(propuestaExcelUseCase.findExcelPropuestas(idCartera, mes, informePropuestaInputDTO, (Usuario) usuario.getPrincipal(), false));

    return excelExport.download(excel, "informe-propuestas-" + idCartera + "_" + new SimpleDateFormat("ddMMyyyy").format(new Date()));
  }

  @ApiOperation(
    value = "Obtiene un bien a partir de su id, sin el contrato",
    notes = "Obtiene un bien a partir de su id, sin el contrato")
  @GetMapping("/bienes/{idBien}")
  public BienOutputDto getBienById(@PathVariable("idBien") Integer idBien) throws Exception {
    return propuestaUseCase.getBienById(idBien);
  }

  @ApiOperation(
    value = "Todas las propuestas en función de IdExpediente",
    notes = "Obtiene todas las propuestas según expediente.")
  @GetMapping("/allPropuesta")
  public List<PropuestaDTOToList> findAllPropuestaByExpedienteId(@PathVariable Integer idExpediente,
                                                                 @RequestParam(value = "posesionNegociada") Boolean posesionNegociada)
    throws IOException, NotFoundException {
    return propuestaUseCase.findAllPropuestasByExpedienteId(idExpediente, posesionNegociada);
  }

  @ApiOperation(
      value = "Todas las propuestas en función de IdExpediente",
      notes = "Obtiene todas las propuestas según expediente.")
  @GetMapping("/{idPropuesta}/documento")
  public List<DocumentoDTO> findAllDocumentoByPropuestaId(
      @PathVariable Integer idPropuesta)
      throws IOException, NotFoundException {
    return propuestaUseCase.findDocumentosByPropuesta(idPropuesta);
  }

  @ApiOperation(value = "Modificar el importe colabora de una propuesta de un expediente")
  @PutMapping("/{idPropuesta}/campos-sareb-pbc")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public PropuestaDTO updateCamposSareb(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idPropuesta") Integer idPropuesta,
      @RequestParam(value = "importeColabora") Double importeColabora,
      @RequestParam(value = "numColabora") Integer numColabora)
      throws Exception {
    return propuestaUseCase.updateCamposSareb(
        idExpediente, idPropuesta, importeColabora, numColabora);
  }

  @ApiOperation(
      value =
          "Modificar los campos de una propuesta, de la cartera Cerberus para PBC, de un expediente")
  @PutMapping("/{idPropuesta}/campos-cerberus-pbc")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public PropuestaDTO updateCamposCerberusPBC(
      @PathVariable("idExpediente") Integer idExpediente,
      @PathVariable("idPropuesta") Integer idPropuesta,
      @RequestParam(value = "numAdvisoryNote", required = false) Integer numAdvisoryNote,
      @RequestParam(value = "fechaEnvioCES", required = false) String fechaEnvioCES,
      @RequestParam(value = "fechaEnvioWorkingGroup", required = false) String fechaEnvioWorkingGroup,
      @RequestParam(value = "fechaRecepcionAprobacionAdvisoryNote", required = false)
        String fechaRecepcionAprobacionAdvisoryNote)
      throws Exception {
    return propuestaUseCase.updateCamposCerberusPBC(
        idExpediente,
        idPropuesta,
        numAdvisoryNote,
        fechaEnvioCES,
        fechaEnvioWorkingGroup,
        fechaRecepcionAprobacionAdvisoryNote);
  }

  @ApiOperation(
      value = "Obtiene los estados de una propuesta",
      notes =
          "Filtra los estados disponibles para una propuesta, dependiendo de los booleans de formalizacion")
  @GetMapping("/estado")
  public List<CatalogoMinInfoDTO> getEstados(@RequestParam(value = "idPropuesta", required = false) Integer idPropuesta)
      throws NotFoundException {
    return propuestaUseCase.getEstadosPropuesta(idPropuesta);
  }

  @ApiOperation(
    value = "Obtiene los subestados de una propuesta",
    notes = "Obtiene los subestados asociados a un estado")
  @GetMapping("/subestado")
  public List<CatalogoMinInfoDTO> getSubestados(@RequestParam("idEstado") Integer idEstado) throws NotFoundException {
    return propuestaUseCase.getSubestadosPropuesta(idEstado);
  }

  @ApiOperation(
      value = "Actualiza los estados de la propuesta",
      notes = "Actualiza el estado y es subestado de la propuesta")
  @PutMapping("/{idPropuesta}/estadoYSubestado")
  public void actualizarEstados(
      @PathVariable("idPropuesta") Integer idPropuesta,
      @RequestParam("idEstado") Integer idEstado,
      @RequestParam(value = "idSubestado", required = false) Integer idSubestado,
      @ApiIgnore CustomUserDetails principal)
    throws NotFoundException, RequiredValueException {
    propuestaUseCase.actualizarEstadoYSubestado((Usuario) principal.getPrincipal(), idPropuesta, idEstado, idSubestado);
  }
}
