package com.haya.alaska.propuesta.infrastructure.controller.dto;

import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteDto;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteOutputDto;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/** @author agonzalez */

@Getter
@Setter
@ToString
@NoArgsConstructor
public class OcupantePropuestaDTO implements Serializable {

    private OcupanteOutputDto interviniente;
    private List<ContratoDTOToList> contratos;

    public OcupantePropuestaDTO(OcupanteOutputDto interviniente, List<ContratoDTOToList> contratos){
        this.interviniente = interviniente;
        this.contratos = contratos;
    }

}
