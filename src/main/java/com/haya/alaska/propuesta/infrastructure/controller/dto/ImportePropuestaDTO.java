package com.haya.alaska.propuesta.infrastructure.controller.dto;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/** @author agonzalez */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ImportePropuestaDTO {

    private Integer id;
    private List<String> contratos;
    private List<String> contratosIdOrigen;
    private Double importeEntregaDineraria;
    private Double importeEntregaNoDineraria;
    private Double condonacionDeuda;
    private String comentario;
    private Boolean activo = true;

    public ImportePropuestaDTO(ImportePropuesta importePropuesta, Boolean pN) {
        BeanUtils.copyProperties(importePropuesta, this);
        if (pN){
          Set<BienPosesionNegociada> bienes = importePropuesta.getBienes();
          this.contratos = bienes.stream().map(b -> b.getId().toString()).collect(Collectors.toList());
        } else {
          Set<Contrato> contratos = importePropuesta.getContratos();
          this.contratos = contratos.stream().map(Contrato::getIdCarga).collect(Collectors.toList());
          this.contratosIdOrigen = contratos.stream().map(Contrato::getIdCarga).collect(Collectors.toList());
        }
    }

    public static List<ImportePropuestaDTO> newList(Collection<ImportePropuesta> importesPropuesta, Boolean pN) {
        return importesPropuesta.stream().filter(importePropuesta -> importePropuesta.getActivo())
                .map(imp -> new ImportePropuestaDTO(imp, pN)).collect(Collectors.toList());
    }
}
