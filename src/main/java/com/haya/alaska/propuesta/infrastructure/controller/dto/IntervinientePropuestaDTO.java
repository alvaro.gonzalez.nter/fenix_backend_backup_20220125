package com.haya.alaska.propuesta.infrastructure.controller.dto;

import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/** @author agonzalez */

@Getter
@Setter
@ToString
@NoArgsConstructor
public class IntervinientePropuestaDTO implements Serializable {

    private IntervinienteDTOToList interviniente;
    private List<ContratoDTOToList> contratos;

    public IntervinientePropuestaDTO(IntervinienteDTOToList interviniente, List<ContratoDTOToList> contratos){
        this.interviniente = interviniente;
        this.contratos = contratos;
    }

}
