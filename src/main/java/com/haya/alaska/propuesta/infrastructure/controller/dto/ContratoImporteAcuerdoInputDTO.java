package com.haya.alaska.propuesta.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/** @author agonzalez
 * DTO para recoger información de los IDs de contratos asociados a acuerdos de pago o
 * importes de pago de una propuesta
 * */

@Getter
@ToString
@NoArgsConstructor
public class ContratoImporteAcuerdoInputDTO implements Serializable {
    Integer idContrato;
    Integer idAux;
}
