package com.haya.alaska.propuesta.infrastructure.mapper;

import com.haya.alaska.acuerdo_pago.aplication.AcuerdoPagoUseCase;
import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoDTO;
import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoInputDTO;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.mapper.BienMapper;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDTOToList;
import com.haya.alaska.bien_posesion_negociada.infrastructure.mapper.BienPosesionNegociadaMapper;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.carga.infrastructure.mapper.CargaMapper;
import com.haya.alaska.carga.infrastructure.repository.CargaRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.clase_propuesta.infrastructure.repository.ClasePropuestaRepository;
import com.haya.alaska.comentario_propuesta.domain.ComentarioPropuesta;
import com.haya.alaska.comentario_propuesta.infrastructure.repository.ComentarioPropuestaRepository;
import com.haya.alaska.contrato.application.ContratoUseCase;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.mapper.ContratoMapper;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.estado_propuesta.infrastructure.repository.EstadoPropuestaRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.FormalizacionOutputDTO;
import com.haya.alaska.importe_propuesta.aplication.ImportePropuestaUseCase;
import com.haya.alaska.importe_propuesta.infrastructure.repository.ImportePropuestaRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteOutputDto;
import com.haya.alaska.ocupante.infrastructure.mapper.OcupanteMapper;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.plazo.repository.PlazoRepository;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.ProcedimientoDTOToList;
import com.haya.alaska.procedimiento.infrastructure.mapper.ProcedimientoMapper;
import com.haya.alaska.propuesta.aplication.PropuestaUseCase;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.controller.dto.*;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.propuesta_bien.application.PropuestaBienUseCase;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.propuesta_bien.infrastructure.PropuestaBienRepository;
import com.haya.alaska.propuesta_bien.infrastructure.controller.dto.ContratoBienPropuestaDTOToList;
import com.haya.alaska.resolucion_propuesta.infrastructure.controller.ResolucionPropuestaDTO;
import com.haya.alaska.resolucion_propuesta.infrastructure.repository.ResolucionPropuestaRepository;
import com.haya.alaska.sancion_propuesta.infrastructure.controller.SancionPropuestaDTO;
import com.haya.alaska.sancion_propuesta.infrastructure.repository.SancionPropuestaRepository;
import com.haya.alaska.shared.dto.BienDTOToList;
import com.haya.alaska.shared.dto.CargaDTOToList;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.exceptions.InvalidCatalogException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_propuesta.infrastructure.repository.SubtipoPropuestaRepository;
import com.haya.alaska.tipo_propuesta.infrastructure.repository.TipoPropuestaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class PropuestaMapper {

  @Autowired PropuestaRepository propuestaRepository;
  @Autowired PropuestaUseCase propuestaUseCase;
  @Autowired PropuestaBienUseCase propuestaBienUseCase;
  @Autowired PropuestaBienRepository propuestaBienRepository;
  @Autowired ImportePropuestaUseCase importePropuestaUseCase;
  @Autowired ClasePropuestaRepository clasePropuestaRepository;
  @Autowired TipoPropuestaRepository tipoPropuestaRepository;
  @Autowired SubtipoPropuestaRepository subtipoPropuestaRepository;
  @Autowired ContratoRepository contratoRepository;
  @Autowired ContratoMapper contratoMapper;
  @Autowired ContratoUseCase contratoUseCase;
  @Autowired BienRepository bienRepository;
  @Autowired BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired BienPosesionNegociadaMapper bienPosesionNegociadaMapper;
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired OcupanteRepository ocupanteRepository;
  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired CarteraRepository carteraRepository;
  @Autowired PlazoRepository plazoRepository;
  @Autowired ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired ImportePropuestaRepository importePropuestaRepository;
  @Autowired AcuerdoPagoUseCase acuerdoPagoUseCase;
  @Autowired EstadoPropuestaRepository estadoPropuestaRepository;
  @Autowired SancionPropuestaRepository sancionPropuestaRepository;
  @Autowired ResolucionPropuestaRepository resolucionPropuestaRepository;
  @Autowired ComentarioPropuestaRepository comentarioPropuestaRepository;
  @Autowired OcupanteMapper ocupanteMapper;
  @Autowired private CargaMapper cargaMapper;
  @Autowired private CargaRepository cargaRepository;
  @Autowired private BienMapper bienMapper;
  @Autowired private ProcedimientoMapper procedimientoMapper;

  public Propuesta createPropuesta(
      PropuestaInputDTO propuestaDTO,
      Integer idExpediente,
      Usuario usuario,
      Boolean posesionNegociada)
      throws Exception {

    var propuesta = new Propuesta();
    BeanUtils.copyProperties(
        propuestaDTO,
        propuesta,
        "id",
        "contratos",
        "intervinientes",
        "bienes",
        "acuerdosPago",
        "importes");

    propuesta.setFechaAlta(new Date());
    propuesta.setFechaUltimoCambio(new Date());
    // activamos la propuesta
    propuesta.setEstadoPropuesta(
        estadoPropuestaRepository
            .findByCodigo("EST")
            .orElseThrow(() -> new NotFoundException("estado", "descripcion", "'En estudio'")));
    propuesta.setActivo(true);
    propuesta.setBorrador(false);
    propuesta.setUsuario(usuario);
    if (posesionNegociada) {
      var expedientePN =
          expedientePosesionNegociadaRepository
              .findById(idExpediente)
              .orElseThrow(() -> new NotFoundException("Expediente de poesión", idExpediente));
      propuesta.setExpedientePosesionNegociada(expedientePN);
    } else {
      var expediente =
          expedienteRepository
              .findById(idExpediente)
              .orElseThrow(() -> new NotFoundException("Expediente", idExpediente));
      propuesta.setExpediente(expediente);
    }
    // validación de campos
    Cartera c = null;
    if (posesionNegociada == false) {
      Expediente ex = expedienteRepository.findById(idExpediente).orElse(null);
      c = carteraRepository.findById(ex.getCartera().getId()).orElse(null);
    } else if (posesionNegociada == true) {
      ExpedientePosesionNegociada ex =
          expedientePosesionNegociadaRepository.findById(idExpediente).orElse(null);
      c = carteraRepository.findById(ex.getCartera().getId()).orElse(null);
    }
    if (c != null) {
      String pl = null;
      if (c.getPlazo() != null) {
        pl = c.getPlazo().getValor();
      }
      // SI LA CARTERA NO TIENE NUMERO DE DIAS, GUARDAR LA FECHA DE LA PROPUESTA
      if (pl == null || pl.equals("")) {
        if (propuestaDTO.getFechaValidez() == null) propuesta.setFechaValidez(null);
        else propuesta.setFechaValidez(propuestaDTO.getFechaValidez());
        // SI LA CARTERA TIENE NUMERO DE DIAS, NO GUARDAR LA FECHA DE LA PROPUESTA
      } else if (pl != null && !pl.equals("")) {
        propuesta.setFechaValidez(null);
      }
    } else {
      propuesta.setFechaValidez(null);
    }
    if (propuestaDTO.getClase() == null) throw new RequiredValueException("clase");
    var clase =
        clasePropuestaRepository
            .findById(propuestaDTO.getClase())
            .orElseThrow(() -> new NotFoundException("Clase", propuestaDTO.getClase()));
    propuesta.setClasePropuesta(clase);
    if (propuestaDTO.getTipo() == null) throw new RequiredValueException("tipo");
    var tipo =
        tipoPropuestaRepository
            .findById(propuestaDTO.getTipo())
            .orElseThrow(() -> new NotFoundException("Tipo Propuesta", propuestaDTO.getTipo()));
    propuesta.setTipoPropuesta(tipo);
    if (propuestaDTO.getSubtipo() == null) throw new RequiredValueException("subtipo");
    var subtipo =
        subtipoPropuestaRepository
            .findById(propuestaDTO.getSubtipo())
            .orElseThrow(() -> new NotFoundException("SubTipo Propuesta", propuestaDTO.getTipo()));
    if (subtipo.getTipoPropuesta().getId() != propuestaDTO.getTipo()) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException(
            "Proposal subtype: "
                + propuestaDTO.getSubtipo()
                + " DOES NOT belong to the type sent: "
                + propuestaDTO.getTipo());
      else
        throw new NotFoundException(
            "Subtipo Propuesta: "
                + propuestaDTO.getSubtipo()
                + " NO pertenece al tipo mandado: "
                + propuestaDTO.getTipo());
    }
    propuesta.setSubtipoPropuesta(subtipo);

    if (propuestaDTO.getVinculoPropuesta() != null) {
      if (!propuestaDTO.getVinculoPropuesta().equals("0")) {
        var vinculoPropuesta =
            propuestaRepository.findById(propuestaDTO.getVinculoPropuesta()).orElse(null);
        if (vinculoPropuesta == null)
          throw new NotFoundException("Propuesta", propuestaDTO.getVinculoPropuesta());
        propuesta.setVinculoPropuesta(vinculoPropuesta);
      }
    }

    if (propuestaDTO.getEstadoPropuesta() != null) {
      var estadoPropuesta =
          estadoPropuestaRepository.findById(propuestaDTO.getEstadoPropuesta()).orElse(null);
      if (estadoPropuesta == null)
        throw new NotFoundException("Estado de propuesta", propuestaDTO.getEstadoPropuesta());
      propuesta.setEstadoPropuesta(estadoPropuesta);
    }

    if (propuestaDTO.getSancionPropuesta() != null) {
      var sancionPropuesta =
          sancionPropuestaRepository.findById(propuestaDTO.getSancionPropuesta()).orElse(null);
      if (sancionPropuesta == null)
        throw new NotFoundException("Sancion", propuestaDTO.getSancionPropuesta());
      propuesta.setSancionPropuesta(sancionPropuesta);
    }

    if (propuestaDTO.getResolucionPropuesta() != null) {
      var resolucionPropuesta =
          resolucionPropuestaRepository
              .findById(propuestaDTO.getResolucionPropuesta())
              .orElse(null);
      if (resolucionPropuesta == null)
        throw new NotFoundException(
            "Resolucion de propuesta", propuestaDTO.getResolucionPropuesta());
      propuesta.setResolucionPropuesta(resolucionPropuesta);
    }

    List<Contrato> contratos = new ArrayList<>();
    if (propuestaDTO.getContratos() != null) {
      for (Integer contratoId : propuestaDTO.getContratos()) {
        Contrato contrato = contratoRepository.findById(contratoId).orElse(null);
        if (contrato != null) contratos.add(contrato);
      }
      propuesta.addContratos(contratos);
      /*propuestaSaved.addContratos(contratos);*/
    }

    if (posesionNegociada) {
      List<Ocupante> ocupantes = new ArrayList<>();
      if (propuestaDTO.getIntervinientes() != null) {
        for (Integer ocupanteID : propuestaDTO.getIntervinientes()) {
          Ocupante ocupante = ocupanteRepository.findById(ocupanteID).orElse(null);
          if (ocupante != null) ocupantes.add(ocupante);
        }
        propuesta.addOcupantes(ocupantes);
      }
    } else {
      List<Interviniente> intervinientes = new ArrayList<>();
      if (propuestaDTO.getIntervinientes() != null) {
        for (Integer intervinienteId : propuestaDTO.getIntervinientes()) {
          Interviniente interviniente =
              intervinienteRepository.findById(intervinienteId).orElse(null);
          if (interviniente != null) intervinientes.add(interviniente);
        }
        propuesta.addIntervinientes(intervinientes);
      }
    }

    if (propuestaDTO.getImportes() != null) {
      var importesSaved =
          importePropuestaUseCase.createImportesPropuesta(
              propuestaDTO.getImportes(), posesionNegociada);
      propuesta.addImportes(importesSaved);
    }

    if (propuestaDTO.getAcuerdosPago() != null) {
      List<AcuerdoPago> acuerdosPagoSaved = new ArrayList<>();
      for (AcuerdoPagoInputDTO acuerdoPagoInputDTO : propuestaDTO.getAcuerdosPago()) {
        var acuerdoPagoSaved =
            acuerdoPagoUseCase.createAcuerdoPago(
                idExpediente, acuerdoPagoInputDTO, usuario, posesionNegociada);
        acuerdosPagoSaved.add(acuerdoPagoSaved);
      }
      propuesta.addAcuerdosPago(acuerdosPagoSaved);
    }
    actualizaImporteTotal(propuesta, true);

    // guardamos en bbdd cada relación de propuesta-bien-contrato
    List<PropuestaBien> colaterales = getColaterales(propuestaDTO, posesionNegociada);

      // guardamos la propuesta en bbdd antes de asociarla con los bienes y contratos
    Propuesta propuestaSaved = propuestaRepository.saveAndFlush(propuesta);

    for (PropuestaBien pb : colaterales){
      pb.setPropuesta(propuestaSaved);
    }
    propuestaSaved.addBienes(colaterales);

    if (propuestaDTO.getComentario() != null) {
      ComentarioPropuesta comentarioPropuesta = new ComentarioPropuesta();
      comentarioPropuesta.setFechaComentario(Calendar.getInstance().getTime());
      comentarioPropuesta.setFechaEdicion(Calendar.getInstance().getTime());
      comentarioPropuesta.setUsuario(usuario);
      comentarioPropuesta.setPropuesta(propuestaSaved);
      comentarioPropuesta.setComentario(propuestaDTO.getComentario());
      comentarioPropuestaRepository.saveAndFlush(comentarioPropuesta);
    }

    /*Propuesta propuestaUpdated = propuestaRepository.update(propuesta);
    return propuestaUpdated;*/

    return propuestaRepository.saveAndFlush(propuestaSaved);
  }

  private List<PropuestaBien> getColaterales(PropuestaInputDTO propuestaDTO, Boolean posesionNegociada) throws NotFoundException {
    List<PropuestaBien> colaterales = new ArrayList<>();
    List<Bien> bienesErrores = new ArrayList<>();
    List<BienPosesionNegociada> bienesPNErrores = new ArrayList<>();

    for (BienPrecioPropuestaInputDTO bpp : propuestaDTO.getBienes()) {
      Bien bien;
      Integer bienId = bpp.getIdBien();
      if (posesionNegociada) {
        BienPosesionNegociada bpn =
          bienPosesionNegociadaRepository
            .findById(bienId)
            .orElseThrow(() -> new NotFoundException("bienPosesionNegociada", bienId));
        if (bpn.getTipoEstado() == null || bpn.getSubEstado() == null)
          bienesPNErrores.add(bpn);

        bien = bpn.getBien();
        PropuestaBien pb = new PropuestaBien();
        pb.setBien(bien);
        //pb.setPropuesta(propuestaSaved);
        colaterales.add(pb);
      } else {
        bien =
          bienRepository
            .findById(bienId)
            .orElseThrow(() -> new NotFoundException("bien", bienId));

        if (bien.getTipoBien() == null || bien.getSubTipoBien() == null)
          bienesErrores.add(bien);

        if (bienesErrores.isEmpty()){
          for (Integer contratoId : propuestaDTO.getContratos()) {
            ContratoBien contratoBien = bien.getContratoBien(contratoId);
            if (contratoBien != null) {
              var pb = new PropuestaBien();
              pb.setBien(bien);
              //pb.setPropuesta(propuestaSaved);
              var contrato = contratoBien.getContrato();
              pb.setContrato(contrato);
              pb.setPrecioMinimoVenta(bpp.getPrecioMinimoVenta());
              pb.setPrecioWebVenta(bpp.getPrecioWebVenta());
              colaterales.add(pb);
            }
          }
        }
      }
      //propuestaSaved.addBienes(colaterales);
    }

    if (!bienesErrores.isEmpty()){
      String ids = bienesErrores.stream().map(b -> b.getId().toString()).collect(Collectors.joining(", "));
      String error = "Los bienes de ids: " + ids + "; no tienen 'Tipo Bien' o 'Subtipo Bien'.";
      Locale loc = LocaleContextHolder.getLocale();
      String defaultLocal = loc.getLanguage();
      if (defaultLocal == null || defaultLocal.equals("en"))
        error = "The goods of ids: " + ids + "; not have 'Good Type' or 'Subtype Good'.";
      throw new NotFoundException(error);
    }
    if (!bienesPNErrores.isEmpty()){
      String ids = bienesPNErrores.stream().map(b -> b.getId().toString()).collect(Collectors.joining(", "));
      String error = "Los bienesPosesionNegociada de ids: " + ids + "; no tienen 'Tipo Estado' o 'Subtipo Estado'.";
      Locale loc = LocaleContextHolder.getLocale();
      String defaultLocal = loc.getLanguage();
      if (defaultLocal == null || defaultLocal.equals("en"))
        error = "The negotiatedPossessionGoods of ids: " + ids + "; not have 'State Type' or 'Subtype Condition'.";
      throw new NotFoundException(error);
    }

    return colaterales;
  }

  public PropuestaBien createPrecios(Integer idPropuesta, PreciosPropuestaDTO preciosPropuestaDTO)
      throws NotFoundException {
    var idBien = preciosPropuestaDTO.getIdBien();
    var idContrato = preciosPropuestaDTO.getIdContrato();
    var precioMinimoVenta =
        preciosPropuestaDTO.getPrecioMinimoVenta() != null
            ? preciosPropuestaDTO.getPrecioMinimoVenta()
            : null;
    var precioWebVenta =
        preciosPropuestaDTO.getPrecioWebVenta() != null
            ? preciosPropuestaDTO.getPrecioWebVenta()
            : null;
    var propuestaBien =
        propuestaBienUseCase.findByPropuestaIdAndBienIdAndContratoId(
            idPropuesta, idBien, idContrato);
    if (precioMinimoVenta != null) propuestaBien.setPrecioMinimoVenta(precioMinimoVenta);
    if (precioWebVenta != null) propuestaBien.setPrecioWebVenta(precioWebVenta);
    return propuestaBienRepository.saveAndFlush(propuestaBien);
  }

  /**
   * Actualiza el acumulado de importeTotal de una propuesta.
   *
   * @param propuesta
   */
  public void actualizaImporteTotal(Propuesta propuesta, Boolean creado) {
    double importeTotal = 0;
    for (var importeSaved : propuesta.getImportesPropuestas()) {
      if (importeSaved.getActivo() == true)
        importeTotal +=
            (importeSaved.getImporteEntregaDineraria() == null
                    ? 0
                    : importeSaved.getImporteEntregaDineraria())
                + +(importeSaved.getImporteEntregaNoDineraria() == null
                    ? 0
                    : importeSaved.getImporteEntregaNoDineraria());
    }
    for (var acuerdoPago : propuesta.getAcuerdosPago()) {
      // @TODO ¿ El importe total se coge del importe total o del importe de plazos?. Actualmente
      // puesto sobre importe Total.
      if (acuerdoPago.getActivo() == true) importeTotal += acuerdoPago.getImporteTotal();
    }
    propuesta.setImporteTotal(importeTotal);
    if (creado) propuesta.setImporteElevacion(importeTotal);
  }

  public PropuestaDTO createPropuestaDTO(
      Propuesta propuesta, PropuestaDTO propuestaDTO, Boolean posesionNegociada)
      throws InvalidCatalogException {
    if (propuesta == null || propuestaDTO == null) return null;

    BeanUtils.copyProperties(
        propuesta,
        propuestaDTO,
        "contratos",
        "intervinientes",
        "bienes",
        "importeColabora, numColabora, numAdvisoryNote, fechaEnvioCES, fechaEnvioWorkingGroup, fechaRecepcionAprobacionAdvisoryNote");

    if (posesionNegociada) {
      Double deudaActual = 0.00;
      for (PropuestaBien bien : propuesta.getBienes()) {
        deudaActual += bien.getBien().getImporteBien();
      }
      propuestaDTO.setDeudaActual(deudaActual);
    } else propuestaDTO.setDeudaActual(propuesta.getDeudaActual());

    if (propuesta.getExpediente() != null && propuesta.getExpediente().getCartera() != null) {
      propuestaDTO.setCartera(propuesta.getExpediente().getCartera().getNombre());
      propuestaDTO.setCliente(
          propuesta.getExpediente().getCartera().getCliente() != null
              ? propuesta.getExpediente().getCartera().getCliente().getNombre()
              : null);
      propuestaDTO.setResponsable(
          propuesta.getExpediente().getCartera().getResponsableCartera() != null
              ? propuesta.getExpediente().getCartera().getResponsableCartera().getNombre()
              : null);
      propuestaDTO.setFormalizacion(propuesta.isFormalizacion());
    } else if (propuesta.getExpedientePosesionNegociada() != null
        && propuesta.getExpedientePosesionNegociada().getCartera() != null) {
      propuestaDTO.setCartera(propuesta.getExpedientePosesionNegociada().getCartera().getNombre());
      propuestaDTO.setCliente(
          propuesta.getExpedientePosesionNegociada().getCartera().getCliente() != null
              ? propuesta.getExpedientePosesionNegociada().getCartera().getCliente().getNombre()
              : null);
      propuestaDTO.setResponsable(
          propuesta.getExpedientePosesionNegociada().getCartera().getResponsableCartera() != null
              ? propuesta
                  .getExpedientePosesionNegociada()
                  .getCartera()
                  .getResponsableCartera()
                  .getNombre()
              : null);
    }

    if (propuesta.getExpediente() != null) {
      propuestaDTO.setExpediente(propuesta.getExpediente().getId());
      propuestaDTO.setIdConcatenado(propuesta.getExpediente().getIdConcatenado());
    }
    if (propuesta.getExpedientePosesionNegociada() != null)
      propuestaDTO.setExpediente(propuesta.getExpedientePosesionNegociada().getId());

    propuestaDTO.setUsuario(new UsuarioDTO(propuesta.getUsuario()));

    if (propuesta.getFechaValidez() == null) {
      Cartera c = null;
      if (posesionNegociada || propuesta.getExpedientePosesionNegociada() != null) {
        c =
            carteraRepository
                .findById(propuesta.getExpedientePosesionNegociada().getCartera().getId())
                .orElse(null);
      } else if (!posesionNegociada || propuesta.getExpediente() != null) {
        c = carteraRepository.findById(propuesta.getExpediente().getCartera().getId()).orElse(null);
      }
      // meterle la fecha calculada, porque si la propuesta no la tiene es porque no se metió al
      // añadirla, porque no había plazo en la cartera
      if (c != null) {
        String pl = null;
        if (c.getPlazo() != null) {
          pl = c.getPlazo().getValor();
        }
        if (pl != null && !pl.equals("")) {
          Integer addDias = 0;
          try {
            addDias = Integer.parseInt(pl);
          } catch (Exception e) {
            Locale loc = LocaleContextHolder.getLocale();
            if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
              throw new InvalidCatalogException(
                  "The Term catalog value for the portfolio "
                      + c.getNombre()
                      + " is not an integer");
            else
              throw new InvalidCatalogException(
                  "El valor del catálogo Plazo para la cartera "
                      + c.getNombre()
                      + " no es un número entero");
          }
          LocalDate lt = LocalDate.now().plusDays(addDias);
          Date date = Date.from(lt.atStartOfDay(ZoneId.systemDefault()).toInstant());
          propuestaDTO.setFechaValidez(date);
        }
        if (c.getNombre().equals("SAREB")) {
          if (Objects.nonNull(propuesta.getImporteColabora())) {
            propuestaDTO.setImporteColabora(propuesta.getImporteColabora());
          }
          if (Objects.nonNull(propuesta.getNumColabora())) {
            propuestaDTO.setNumColabora(propuesta.getNumColabora());
          }
        }
        if (c.getIdCarga() != null && c.getIdCarga().equals("5202")) {
          if (Objects.nonNull(propuesta.getNumAdvisoryNote())) {
            propuestaDTO.setNumAdvisoryNote(propuesta.getNumAdvisoryNote());
          }
          if (Objects.nonNull(propuesta.getFechaEnvioCES())) {
            propuestaDTO.setFechaEnvioCES(propuesta.getFechaEnvioCES());
          }
          if (Objects.nonNull(propuesta.getFechaEnvioWorkingGroup())) {
            propuestaDTO.setFechaEnvioWorkingGroup(propuesta.getFechaEnvioWorkingGroup());
          }
          if (Objects.nonNull(propuesta.getFechaRecepcionAprobacionAdvisoryNote())) {
            propuestaDTO.setFechaRecepcionAprobacionAdvisoryNote(
                propuesta.getFechaRecepcionAprobacionAdvisoryNote());
          }
        }
      }
    } else if (propuesta.getFechaValidez() != null) {
      propuestaDTO.setFechaValidez(propuesta.getFechaValidez());
    }

    if (propuesta.getVinculoPropuesta() != null) {
      propuestaDTO.setVinculoPropuesta(propuesta.getVinculoPropuesta().getId());
    }

    if (propuesta.getClasePropuesta() != null) {
      propuestaDTO.setClasePropuesta(new CatalogoMinInfoDTO(propuesta.getClasePropuesta()));
    }

    if (propuesta.getTipoPropuesta() != null) {
      propuestaDTO.setTipoPropuesta(new CatalogoMinInfoDTO(propuesta.getTipoPropuesta()));
    }

    if (propuesta.getSubtipoPropuesta() != null) {
      propuestaDTO.setSubtipoPropuesta(new SubtipoPropuestaDTO(propuesta.getSubtipoPropuesta()));
    }

    if (propuesta.getEstadoPropuesta() != null) {
      propuestaDTO.setEstadoPropuesta(new CatalogoMinInfoDTO(propuesta.getEstadoPropuesta()));
    }

    if (propuesta.getSancionPropuesta() != null) {
      propuestaDTO.setSancionPropuesta(new CatalogoMinInfoDTO(propuesta.getSancionPropuesta()));
    }

    if (propuesta.getEstrategia() != null) {
      propuestaDTO.setEstrategia(
          new CatalogoMinInfoDTO(
              propuesta.getEstrategia() != null
                  ? propuesta.getEstrategia().getEstrategia()
                  : null));
    }

    if (propuesta.getResolucionPropuesta() != null) {
      propuestaDTO.setResolucionPropuesta(
          new CatalogoMinInfoDTO(propuesta.getResolucionPropuesta()));
    }

    propuestaDTO.setUsuario(new UsuarioDTO(propuesta.getUsuario()));

    List<ContratoDTOToList> contratos =
        contratoMapper.listContratoToListContratoDTOToList(propuesta.getContratos());
    propuestaDTO.setContratos(contratos);

    // inicializamos bienes con los contratos asociados a estos
    var bienesDTO = this.initBienPropuestaDTO(propuesta.getBienes(), propuesta.getContratos());
    if (bienesDTO == null) bienesDTO = new ArrayList<>();
    propuestaDTO.setBienes(bienesDTO);

    Set<CargaDTOToList> cargas = new HashSet<>();
    for (BienPropuestaDTO bienProp : bienesDTO) {
      if (bienProp.getBien().getId() != null){
        Bien bien = bienRepository.findById(bienProp.getBien().getId()).orElseThrow();
        var listContratos =
          bien.getContratos().stream()
            .map(c -> c.getContrato().getId())
            .collect(Collectors.toList());

        for (CargaDTOToList carga :
          cargaMapper.listCargaToListCargaDTOToList(
            Set.copyOf(cargaRepository.findByBienesId(bienProp.getBien().getId())), null)) {
          if (carga.getBienes() == null) carga.setBienes(new ArrayList<>());
          for (Integer idContrato : listContratos) {
            carga.getBienes().add(bienMapper.bienToBienDTOToList(bien, idContrato));
          }

          if (posesionNegociada) {

            BienPosesionNegociada bienPN =
              bienPosesionNegociadaRepository.findByBien(bien).orElseThrow();
            if (carga.getBienesPN() == null) carga.setBienesPN(new ArrayList<>());
            carga.getBienesPN().add(new BienPosesionNegociadaDTOToList(bienPN));
          }
          cargas.add(carga);
        }
      }
    }
    propuestaDTO.setCargas(new ArrayList<>(cargas));
    // inicializamos intervinientes con los contratos asociados a estos
    if (!posesionNegociada) {
      var intervinientesDTO =
          this.initIntervinientePropuestaDTO(
              propuesta.getIntervinientes(), propuesta.getContratos());
      propuestaDTO.setIntervinientes(intervinientesDTO);
    } else {
      List<OcupantePropuestaDTO> ocupantes = initOcupantePropuestaDTO(propuesta.getOcupantes());
      propuestaDTO.setIntervinientes(ocupantes);
    }

    if (propuesta.getContratos() != null && propuesta.getContratos().size() > 0) {
      // calculamos suma total de los importes de una propuesta
      propuestaDTO.setDeudaActual(propuesta.getDeudaActual());
      ArrayList<ProcedimientoDTOToList> procedimientosDTO = new ArrayList<>();
      for (Contrato c : propuesta.getContratos()) {
        // pasamos todos los procedimientos asociados a los contratos de la propuesta
        if (c.getProcedimientos() != null && c.getProcedimientos().size() > 0) {
          for (Procedimiento p : c.getProcedimientos()) {
            procedimientosDTO.add(procedimientoMapper.procedimientoDTOToList(p));
          }
        }
      }
      propuestaDTO.setProcedimientos(procedimientosDTO);
    }

    if (propuesta.getImportesPropuestas() != null && propuesta.getImportesPropuestas().size() > 0) {
      // calculamos suma total de los importes de una propuesta
      propuestaDTO.setImporteTotal(propuesta.getImporteTotal());
      propuestaDTO.setImporteElevacion(propuesta.getImporteElevacion());

      // obtenemos los importes creados para la propuesta
      List<ImportePropuestaDTO> importes =
          ImportePropuestaDTO.newList(propuesta.getImportesPropuestas(), posesionNegociada);
      propuestaDTO.setImportesPropuestas(importes);
    }

    if (propuesta.getAcuerdosPago() != null && propuesta.getAcuerdosPago().size() > 0) {
      List<AcuerdoPagoDTO> acuerdosPago =
          AcuerdoPagoDTO.newList(propuesta.getAcuerdosPago(), posesionNegociada);
      propuestaDTO.setAcuerdosPago(acuerdosPago);
    }

    if (propuesta.getComentarios() != null)
      propuestaDTO.setComentarios(
          propuesta.getComentarios().stream()
              .filter(ComentarioPropuesta::getActivo)
              .map(ComentarioPropuestaDTO::new)
              .sorted(Comparator.comparing(ComentarioPropuestaDTO::getFechaCreacion).reversed())
              .collect(Collectors.toList()));

    if (propuesta.getFormalizaciones() != null)
      propuestaDTO.setFormalizacionOutputDTOS(
          propuesta.getFormalizaciones().stream()
              .map(FormalizacionOutputDTO::new)
              .collect(Collectors.toList()));

    if (propuesta.getResultadoPBC() != null)
      propuestaDTO.setResultadoPBC(new CatalogoMinInfoDTO(propuesta.getResultadoPBC()));

    if (propuesta.getFechaResolucionPBC() != null)
      propuestaDTO.setFechaResolucionPBC(propuesta.getFechaResolucionPBC());

    return propuestaDTO;
  }

  public PropuestaDTOToList createPropuestaDTOToList(
      Propuesta propuesta, PropuestaDTOToList propuestaDTO, Boolean pn) {
    if (propuesta == null || propuestaDTO == null) return null;

    propuestaDTO.setPosesionNegociada(pn);

    BeanUtils.copyProperties(propuesta, propuestaDTO, "contratos", "intervinientes", "bienes");

    propuestaDTO.setUsuario(new UsuarioDTO(propuesta.getUsuario()));

    if (propuesta.getVinculoPropuesta() != null) {
      propuestaDTO.setVinculoPropuesta(propuesta.getVinculoPropuesta().getId());
    }
    if (propuesta.getClasePropuesta() != null) {
      propuestaDTO.setClasePropuesta(new CatalogoMinInfoDTO(propuesta.getClasePropuesta()));
    }
    if (propuesta.getTipoPropuesta() != null) {
      propuestaDTO.setTipoPropuesta(new CatalogoMinInfoDTO(propuesta.getTipoPropuesta()));
    }
    if (propuesta.getSubtipoPropuesta() != null) {
      propuestaDTO.setSubtipoPropuesta(new CatalogoMinInfoDTO(propuesta.getSubtipoPropuesta()));
    }
    if (propuesta.getEstadoPropuesta() != null) {
      propuestaDTO.setEstadoPropuesta(new CatalogoMinInfoDTO(propuesta.getEstadoPropuesta()));
    }
    if (propuesta.getSancionPropuesta() != null) {
      propuestaDTO.setSancionPropuesta(new CatalogoMinInfoDTO(propuesta.getSancionPropuesta()));
    }
    if (propuesta.getResolucionPropuesta() != null) {
      propuestaDTO.setResolucionPropuesta(
          new CatalogoMinInfoDTO(propuesta.getResolucionPropuesta()));
    }
    propuestaDTO.setUsuario(new UsuarioDTO(propuesta.getUsuario()));

    return propuestaDTO;
  }

  /*
   * Función para inicializar los bienes de una propuesta con los contratos de la propuesta
   * relacionados de cada bien
   * */

  /*TODO crear dto con la nueva relación de propuesta-bien-contrato*/
  public List<BienPropuestaDTO> initBienPropuestaDTO(
      Set<PropuestaBien> propuestaBienes, Set<Contrato> contratosPropuesta) {
    List<BienPropuestaDTO> result = new ArrayList<>();
    for (PropuestaBien pb : propuestaBienes) {
      var bien = pb.getBien();
      var bienPropuestaDTO = new BienPropuestaDTO();
      List<ContratoBienPropuestaDTOToList> contratosDTO = new ArrayList<>();
      for (Contrato contrato : contratosPropuesta) {
        if (pb.getContrato() != null && contrato.getId().equals(pb.getContrato().getId())) {
          var contratoDTO = new ContratoBienPropuestaDTOToList(pb);
          contratosDTO.add(contratoDTO);

          var bienDTOToList = new BienDTOToList(bien, contrato.getId());
          bienPropuestaDTO.setBien(bienDTOToList);
        }
      }
      if (contratosPropuesta.isEmpty()) {
        bienPropuestaDTO.setBien(new BienDTOToList(bien));
        BienPosesionNegociada bpn = bienPosesionNegociadaRepository.findByBien(bien).orElse(null);
        if (bpn != null) {
          bienPropuestaDTO.setBienPN(new BienPosesionNegociadaDTOToList(bpn));
        }
        bienPropuestaDTO.setContratos(new ArrayList<>());
      } else bienPropuestaDTO.setContratos(contratosDTO);
      if (bienPropuestaDTO.getBienPN() == null)
        bienPropuestaDTO.setBienPN(new BienPosesionNegociadaDTOToList());
      if (bienPropuestaDTO.getBien() == null)
        bienPropuestaDTO.setBien(new BienDTOToList());
      result.add(bienPropuestaDTO);
    }
    return result;
  }

  /*
   * Función para inicializar los intervinientes de una propuesta con los contratos de la propuesta
   * relacionados de cada interviniente
   * */
  public List<IntervinientePropuestaDTO> initIntervinientePropuestaDTO(
      Set<Interviniente> intervinientes, Set<Contrato> contratosPropuesta) {
    List<IntervinientePropuestaDTO> result = new ArrayList<>();
    for (Interviniente interviniente : intervinientes) {
      var intervinientePropuestaDTO = new IntervinientePropuestaDTO();
      for (Contrato contrato : contratosPropuesta) {
        List<Contrato> contratos = new ArrayList<>();
        ContratoInterviniente contratoInterviniente =
            interviniente.getContratoInterviniente(contrato.getId());
        if (contratoInterviniente != null) {
          contratos.add(contratoInterviniente.getContrato());

          var contratosDTO =
              contratoMapper.getContratoDTOToListToPropuesta(contratos, contratoInterviniente);
          intervinientePropuestaDTO.setContratos(contratosDTO);

          var intervinienteDTOToList =
              new IntervinienteDTOToList(
                  interviniente, contratoInterviniente.getContrato().getId());
          intervinientePropuestaDTO.setInterviniente(intervinienteDTOToList);
        }
      }
      if (contratosPropuesta.isEmpty()) {
        intervinientePropuestaDTO.setInterviniente(new IntervinienteDTOToList(interviniente, -1));
        intervinientePropuestaDTO.setContratos(new ArrayList<>());
      }

      result.add(intervinientePropuestaDTO);
    }
    return result;
  }

  public List<OcupantePropuestaDTO> initOcupantePropuestaDTO(Set<Ocupante> ocupantes) {
    List<OcupantePropuestaDTO> result = new ArrayList<>();
    for (Ocupante oc : ocupantes) {
      // pasamos información dummy para que se pueda devolver toda la info del método
      OcupanteBien falso = new OcupanteBien();
      falso.setOrden(-1);
      OcupantePropuestaDTO nuevo = new OcupantePropuestaDTO();
      OcupanteOutputDto nuevoO = ocupanteMapper.entityOutputDto(oc, falso);
      nuevo.setInterviniente(nuevoO);
      nuevo.setContratos(new ArrayList<>());
      result.add(nuevo);
    }
    return result;
  }

  public List<PropuestaDTOToList> listPropuestaToListPropuestaDTOToList(
      Collection<Propuesta> propuestas) {
    List<PropuestaDTOToList> propuestasDTO = new ArrayList<>();
    for (Propuesta propuesta : propuestas) {
      propuestasDTO.add(new PropuestaDTOToList(propuesta));
    }
    return propuestasDTO;
  }

  public SancionPropuestaDTO sancionMap(Map map) {
    SancionPropuestaDTO sancion = new SancionPropuestaDTO();

    Object rev = map.get("registro");
    sancion.setRev((Integer) rev);

    return sancion;
  }

  public ResolucionPropuestaDTO resolucionMap(Map map) {
    ResolucionPropuestaDTO resolucion = new ResolucionPropuestaDTO();

    Object rev = map.get("registro");
    resolucion.setRev((Integer) rev);

    return resolucion;
  }
}
