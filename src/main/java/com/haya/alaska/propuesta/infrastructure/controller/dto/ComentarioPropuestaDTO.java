package com.haya.alaska.propuesta.infrastructure.controller.dto;

import com.haya.alaska.comentario_propuesta.domain.ComentarioPropuesta;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ComentarioPropuestaDTO implements Serializable {
    private Integer id;
    private String comentario;
    private Date fechaComentario;
    private Date fechaCreacion;
    private UsuarioAgendaDto usuario;

    public ComentarioPropuestaDTO(ComentarioPropuesta comentarioPropuesta){
        BeanUtils.copyProperties(comentarioPropuesta, this);
        this.fechaComentario = comentarioPropuesta.getFechaEdicion();
        this.fechaCreacion = comentarioPropuesta.getFechaComentario();
        this.usuario = new UsuarioAgendaDto(comentarioPropuesta.getUsuario());
    }
}
