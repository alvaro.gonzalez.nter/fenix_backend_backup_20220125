package com.haya.alaska.propuesta.infrastructure.repository;

import com.haya.alaska.propuesta.domain.Propuesta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface PropuestaRepository extends JpaRepository<Propuesta, Integer> {
  /*@Query(value = "SELECT  a  FROM Propuesta a INNER JOIN Expediente e ON a.expediente.id = e.id and e.id = :idExpediente WHERE a.id = :idPropuesta  ")
  Optional<Propuesta> findByIdExpediente(@Param("idExpediente") Integer idExpediente, @Param("idPropuesta") Integer idPropuesta);*/

  Optional<Propuesta> findById(Integer idPropuesta);

  List<Propuesta> findAllByExpedienteId(Integer idExpediente);

  Optional<Propuesta> findByIdHaya(String idHaya);
  Optional<Propuesta> findByIdCarga(String idCarga);

  List<Propuesta> findAllByExpedienteCarteraIdOrExpedientePosesionNegociadaCarteraId(
    Integer idCarteraExp, Integer idCarteraExpPosNeg);

  List<Propuesta> findAllByEstadoPropuestaIdAndExpedienteCarteraIdOrExpedientePosesionNegociadaCarteraId(
    Integer idEstado, Integer idCarteraExp, Integer idCarteraExpPosNeg);

  List<Propuesta> findAllByTipoPropuestaIdAndExpedienteCarteraIdOrExpedientePosesionNegociadaCarteraId(
    Integer idTipo, Integer idCarteraExp, Integer idCarteraExpPosNeg);

  @Query(
    value = "SELECT * FROM MSTR_PROPUESTA WHERE ID_RESOLUCION_PROPUESTA = ?1 AND FCH_RESOLUCION >= ?2 AND FCH_RESOLUCION < ?3"
    , nativeQuery = true)
  List<Propuesta> findAllByIdResolucionPropuestaAndFechaResolucionBetween(Integer idResolucionPropuesta, String inicio, String fin);

  @Query(
    value = "SELECT * FROM MSTR_PROPUESTA WHERE FCH_SANCION >= ?1 AND FCH_SANCION < ?2"
    , nativeQuery = true)
  List<Propuesta> findAllByFechaSancionBetween(String inicio, String fin);

  //Alertas formalizacion
  List<Propuesta> findAllByFormalizacionesFechaEntradaTFGreaterThanAndFormalizacionesFechaEntradaTFLessThan(Date fIni, Date fFin);
  List<Propuesta> findAllByFormalizacionesEstadoChecklistCodigoNotAndFormalizacionesFechaOkDocumentacionGreaterThanAndFormalizacionesFechaOkDocumentacionLessThan(String ok, Date fIni, Date fFin);
  List<Propuesta> findAllByEstadoPropuestaCodigoNotAndFormalizacionesVencimientoSancionGreaterThanAndFormalizacionesVencimientoSancionLessThan(String codigo, Date fIni, Date fFin);
  List<Propuesta> findAllByFormalizacionesFechaFirmaGreaterThanAndFormalizacionesFechaFirmaLessThan(Date fIni, Date fFin);
  List<Propuesta> findAllByFechaValidezGreaterThanAndFechaValidezLessThan(Date fIni, Date fFin);
  List<Propuesta> findAllByFormalizacionesFechaEntradaGreaterThanAndFormalizacionesFechaEntradaLessThanAndFormalizacionesFechaOkDocumentacionIsNullAndFormalizacionesFechaKoDocumentacionIsNull(Date fIni, Date fFin);
  List<Propuesta> findAllByFormalizacionesFechaOkDocumentacionGreaterThanAndFormalizacionesFechaOkDocumentacionLessThanAndFormalizacionesFechaEncargoDDLIsNullAndFormalizacionesFechaEncargoDDTIsNull(Date fIni, Date fFin);
  List<Propuesta> findAllByFormalizacionesFechaEncargoDDLGreaterThanAndFormalizacionesFechaEncargoDDLLessThanAndFormalizacionesFechaRecepcionDDLIsNull(Date fIni, Date fFin);
  List<Propuesta> findAllByFormalizacionesFechaEncargoDDTGreaterThanAndFormalizacionesFechaEncargoDDTLessThanAndFormalizacionesFechaRecepcionDDTIsNull(Date fIni, Date fFin);
  List<Propuesta> findAllByFormalizacionesSubestadoFormalizacionCodigoAndFormalizacionesFechaFirmaGreaterThanAndFormalizacionesFechaFirmaLessThanAndFormalizacionesFechaComunicacionIsNull(String codigo, Date fIni, Date fFin);

  @Query(
      value =
          "SELECT * \n"
              + "FROM MSTR_PROPUESTA AS P \n"
              + "LEFT JOIN MSTR_FORMALIZACION AS F ON F.ID_PROPUESTA = P.ID \n"
              + "LEFT JOIN RELA_DATOS_CHECK_LIST AS R ON R.ID_PROPUESTA = P.ID \n"
              + "LEFT JOIN LKUP_NOMBRES_CHECK_LIST AS N ON N.ID = R.ID_NOMBRE_CHECK_LIST \n"
              + "LEFT JOIN LKUP_DOCUMENTACION_VALIDADA AS L ON L.ID = R.ID_DOCUMENTACION_VALIDADA \n"
              + "WHERE N.DES_VALOR = 'Ficha Fiscal' \n"
              + "AND L.DES_CODIGO = 'S' \n"
              + "AND ((F.FCH_RECEPCION_DDL >= ?1 AND F.FCH_RECEPCION_DDL <= ?2) OR (F.FCH_RECEPCION_DDT >= ?1 AND F.FCH_RECEPCION_DDT <= ?2));",
      nativeQuery = true)
  List<Propuesta> findAllBySiEstaSubida(Date fIni, Date fFin);
}
