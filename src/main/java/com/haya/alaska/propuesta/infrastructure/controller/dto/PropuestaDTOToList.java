package com.haya.alaska.propuesta.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.FormalizacionOutputDTO;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/** @author agonzalez */

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PropuestaDTOToList implements Serializable {

    private Integer id;
    private Boolean posesionNegociada;
    Date fechaAlta;
    private UsuarioDTO usuario;
    CatalogoMinInfoDTO clasePropuesta;
    CatalogoMinInfoDTO tipoPropuesta;
    CatalogoMinInfoDTO subtipoPropuesta;
    CatalogoMinInfoDTO estadoPropuesta;
    CatalogoMinInfoDTO sancionPropuesta;
    CatalogoMinInfoDTO resolucionPropuesta;
    Date fechaUltimoCambio;
    Double importeTotal;
    private Integer vinculoPropuesta;
    private static final long serialVersionUID = 1L;
    FormalizacionOutputDTO formalizacion;


    public PropuestaDTOToList(Propuesta propuesta){
      if (propuesta==null) propuesta=new Propuesta();
      this.id=propuesta.getId();
      this.posesionNegociada=propuesta.getExpedientePosesionNegociada()!=null ? this.posesionNegociada=true:false;
      this.fechaAlta=propuesta.getFechaAlta()!=null ? propuesta.getFechaAlta():null;
      this.usuario=propuesta.getUsuario()!=null ?new UsuarioDTO(propuesta.getUsuario()):null;
      this.clasePropuesta=propuesta.getClasePropuesta()!=null ? new CatalogoMinInfoDTO(propuesta.getClasePropuesta()):null;
      this.tipoPropuesta=propuesta.getTipoPropuesta()!=null? new CatalogoMinInfoDTO(propuesta.getTipoPropuesta()):null;
      this.subtipoPropuesta=propuesta.getSubtipoPropuesta()!=null ? new CatalogoMinInfoDTO(propuesta.getSubtipoPropuesta()):null;
      this.estadoPropuesta=propuesta.getEstadoPropuesta()!=null ? new CatalogoMinInfoDTO(propuesta.getEstadoPropuesta()):null;
      this.sancionPropuesta=propuesta.getSancionPropuesta()!=null ? new CatalogoMinInfoDTO(propuesta.getSancionPropuesta()):null;
      this.importeTotal=propuesta.getImporteTotal()!=null ? propuesta.getImporteTotal():null;
      this.fechaUltimoCambio=propuesta.getFechaUltimoCambio()!=null ? propuesta.getFechaUltimoCambio():null;
      this.vinculoPropuesta=propuesta.getVinculoPropuesta()!=null? propuesta.getVinculoPropuesta().getId():null;
    }


}
