package com.haya.alaska.propuesta.infrastructure.controller.dto;

import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDTOToList;
import com.haya.alaska.propuesta_bien.infrastructure.controller.dto.ContratoBienPropuestaDTOToList;
import com.haya.alaska.shared.dto.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/** @author agonzalez */

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BienPropuestaDTO implements Serializable {

    private BienDTOToList bien;
    private BienPosesionNegociadaDTOToList bienPN;
    private List<ContratoBienPropuestaDTOToList> contratos;

    public BienPropuestaDTO(BienDTOToList bien, List<ContratoBienPropuestaDTOToList> contratos){
        this.bien = bien;
        this.contratos = contratos;
    }

}
