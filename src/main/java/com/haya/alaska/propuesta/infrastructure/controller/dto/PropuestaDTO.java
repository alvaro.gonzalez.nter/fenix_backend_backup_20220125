package com.haya.alaska.propuesta.infrastructure.controller.dto;

import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoDTO;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.FormalizacionOutputDTO;
import com.haya.alaska.procedimiento.infrastructure.controller.dto.ProcedimientoDTOToList;
import com.haya.alaska.shared.dto.CargaDTOToList;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/** @author agonzalez */

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PropuestaDTO implements Serializable {

    private Integer id;
    private UsuarioDTO usuario;
    private Integer expediente;
    private String idConcatenado;
    private List<ContratoDTOToList> contratos;
    private Integer vinculoPropuesta;
    CatalogoMinInfoDTO clasePropuesta;
    CatalogoMinInfoDTO tipoPropuesta;
    SubtipoPropuestaDTO subtipoPropuesta;
    CatalogoMinInfoDTO estadoPropuesta;
    CatalogoMinInfoDTO sancionPropuesta;
    Date fechaSancion;
    CatalogoMinInfoDTO resolucionPropuesta;
    Date fechaResolucion;
    CatalogoMinInfoDTO estrategia;
    private List intervinientes;
    private List<BienPropuestaDTO> bienes;
    private List<ImportePropuestaDTO> importesPropuestas;
    private List<AcuerdoPagoDTO> acuerdosPago;
    Date fechaAlta;
    Date fechaUltimoCambio;
    private Date fechaValidez;
    List<ComentarioPropuestaDTO> comentarios;
    Boolean borrador;
    Double importeTotal;
    Double importeElevacion;
    Double deudaActual;
    private List<CargaDTOToList> cargas = new ArrayList<>();
    private List<ProcedimientoDTOToList> procedimientos = new ArrayList<>();
    private Boolean activo;
    private List<FormalizacionOutputDTO> formalizacionOutputDTOS = new ArrayList<>();
    private static final long serialVersionUID = 1L;

  private String cliente;
  private String cartera;
  private String responsable;
  private String oficina;
  private String dt;

  private CatalogoMinInfoDTO resultadoPBC;
  private Date fechaResolucionPBC;
  private Double importeColabora;
  private Integer numColabora;
  private Integer numAdvisoryNote;
  private Date fechaEnvioCES;
  private Date fechaEnvioWorkingGroup;
  private Date fechaRecepcionAprobacionAdvisoryNote;
  private Boolean formalizacion;
}
