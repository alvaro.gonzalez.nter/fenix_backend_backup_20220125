package com.haya.alaska.propuesta.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;


/** @author agonzalez */

@Getter
@ToString
@NoArgsConstructor
public class ImportePropuestaInputDTO implements Serializable {

    private Double importeEntregaDineraria;
    private Double importeEntregaNoDineraria;
    private Double condonacionDeuda;
    private String comentario;
    private List<Integer> contratos; // Si es posesion negociada, son ids de BienPosesionNegociada
}
