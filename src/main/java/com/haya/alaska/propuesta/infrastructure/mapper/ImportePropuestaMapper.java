package com.haya.alaska.propuesta.infrastructure.mapper;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.propuesta.infrastructure.controller.dto.ImportePropuestaDTO;
import com.haya.alaska.propuesta.infrastructure.controller.dto.ImportePropuestaInputDTO;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.exceptions.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ImportePropuestaMapper {

    @Autowired
    ContratoRepository contratoRepository;
    @Autowired
    BienPosesionNegociadaRepository bienPosesionNegociadaRepository;

    public ImportePropuestaDTO parse(ImportePropuesta importePropuesta, List<Contrato> contratos) {
        ImportePropuestaDTO importePropuestaDTO = new ImportePropuestaDTO();
        BeanUtils.copyProperties(importePropuestaDTO, importePropuesta);
        List<String> contratosDTO = contratos.stream().map(Contrato::getIdCarga).collect(Collectors.toList());
        importePropuestaDTO.setContratos(contratosDTO);
        return importePropuestaDTO;
    }

    public ImportePropuesta parse(ImportePropuestaDTO importePropuestaDTO, List<ContratoDTOToList> contratosDTO) throws NotFoundException {
        ImportePropuesta importePropuesta = new ImportePropuesta();
        BeanUtils.copyProperties(importePropuesta, importePropuestaDTO);
        List<Contrato> contratos = new ArrayList<>();
        for(ContratoDTOToList contratoDTO: contratosDTO){
            Contrato contrato = contratoRepository.findById(contratoDTO.getId()).orElseThrow(() ->
                    new NotFoundException("contrato", contratoDTO.getId()));
            if(contrato == null) throw new NotFoundException("Contrato", contratoDTO.getId());
            contratos.add(contrato);
        }
        importePropuesta.addContratos(contratos);
        return importePropuesta;
    }

    public ImportePropuesta parse(ImportePropuestaInputDTO importePropuestaDTO, Boolean posesionNegociada) throws NotFoundException{
        ImportePropuesta importePropuesta = new ImportePropuesta();
        BeanUtils.copyProperties(importePropuestaDTO, importePropuesta, "contratos");
        /*Dependiendo de si es en posesiónNegociada o nó, el array de Contratos se refiere a los ids de
        * bienesPosesionNegociada o contratos, respectivamente*/
        if (posesionNegociada){
          List<BienPosesionNegociada> bienesPN = new ArrayList<>();
          for (Integer bienPNId: importePropuestaDTO.getContratos()){
            BienPosesionNegociada bpn = bienPosesionNegociadaRepository.findById(bienPNId).orElseThrow(() ->
              new NotFoundException("bienPosesionNegociada", bienPNId));
            bienesPN.add(bpn);
          }
          importePropuesta.addBienes(bienesPN);
        } else{
          List<Contrato> contratos = new ArrayList<>();
          for(Integer contratoId: importePropuestaDTO.getContratos()){
            Contrato contrato = contratoRepository.findById(contratoId).orElseThrow(() ->
              new NotFoundException("contrato", contratoId));
            if(contrato == null) throw new NotFoundException("Contrato", contratoId);
            contratos.add(contrato);
          }
          importePropuesta.addContratos(contratos);
        }

        return importePropuesta;
    }
}
