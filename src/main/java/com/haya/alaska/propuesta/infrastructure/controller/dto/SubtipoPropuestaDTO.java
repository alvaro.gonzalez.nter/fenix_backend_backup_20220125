package com.haya.alaska.propuesta.infrastructure.controller.dto;

import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubtipoPropuestaDTO implements Serializable {
    private Integer id;
    private String codigo;
    private String valor;
    //private String valorIngles;
    private Integer tipoPropuesta;
    private Boolean pbc;
    private Boolean formalizacion;

    public SubtipoPropuestaDTO(SubtipoPropuesta subtipoPropuesta)
    {
        this.id = subtipoPropuesta.getId();
        this.codigo = subtipoPropuesta.getCodigo();
        String valor = null;
        Locale loc = LocaleContextHolder.getLocale();
        String defaultLocal = loc.getLanguage();
        if (defaultLocal == null || defaultLocal.equals("es")) valor = subtipoPropuesta.getValor();
        else if (defaultLocal.equals("en")) valor = subtipoPropuesta.getValorIngles();
        this.valor = valor;
        this.pbc = subtipoPropuesta.getPbc();
        this.formalizacion = subtipoPropuesta.getFormalizacion();
        this.tipoPropuesta = subtipoPropuesta.getTipoPropuesta() == null ? 0 : subtipoPropuesta.getTipoPropuesta().getId();
    }
}
