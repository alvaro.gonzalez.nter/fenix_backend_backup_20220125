package com.haya.alaska.propuesta.infrastructure.controller.dto;


import com.haya.alaska.acuerdo_pago.infrastructure.controller.dto.AcuerdoPagoInputDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/** @author agonzalez */

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PropuestaInputDTO implements Serializable {

    private Integer clase;
    private Integer tipo;
    private Integer subtipo;
    private List<Integer> contratos;
    private List<BienPrecioPropuestaInputDTO> bienes;
    private List<Integer> intervinientes;
    private Integer vinculoPropuesta;
    private List<ImportePropuestaInputDTO> importes;
    private List<AcuerdoPagoInputDTO> acuerdosPago;
    private Integer estadoPropuesta;
    private Integer sancionPropuesta;
    private Integer resolucionPropuesta;
    private String comentario;
    private Date fechaValidez;


    /**
     *
     */
    private static final long serialVersionUID = 1L;
}
