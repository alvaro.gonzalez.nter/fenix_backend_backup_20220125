package com.haya.alaska.propuesta.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.envers.NotAudited;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/** @author agonzalez */

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PreciosPropuestaDTO implements Serializable {
    @NotNull
    Integer idContrato;
    @NotNull
    Integer idBien;
    Double precioWebVenta;
    Double precioMinimoVenta;
}
