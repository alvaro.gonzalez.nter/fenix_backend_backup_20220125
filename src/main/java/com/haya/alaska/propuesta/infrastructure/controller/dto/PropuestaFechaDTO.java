package com.haya.alaska.propuesta.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@ToString
@NoArgsConstructor
public class PropuestaFechaDTO implements Serializable {

  private Date fechaValidez;
}
