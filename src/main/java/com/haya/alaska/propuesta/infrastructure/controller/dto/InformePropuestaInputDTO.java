package com.haya.alaska.propuesta.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InformePropuestaInputDTO {

  private Integer estadoExpediente;
  private Integer estadoPropuesta;

  private Date fechaCreacionExpedienteInicial;
  private Date fechaCreacionExpedienteFinal;

  private Date fechaCierreExpedienteInicial;
  private Date fechaCierreExpedienteFinal;

  private Date fechaElevacionPropuestaInicial;
  private Date fechaElevacionPropuestaFinal;

  private Date fechaSancionPropuestaInicial;
  private Date fechaSancionPropuestaFinal;

  private Date fechaResolucionPropuestaInicial;
  private Date fechaResolucionPropuestaFinal;

  private Date fechaValidezPropuestaInicial;
  private Date fechaValidezPropuestaFinal;

  private Date fechaUltimoCambioPropuestaInicial;
  private Date fechaUltimoCambioPropuestaFinal;

}
