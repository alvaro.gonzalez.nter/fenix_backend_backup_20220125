package com.haya.alaska.propuesta.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.comentario_propuesta.domain.ComentarioPropuesta;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTO;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteDTOToList;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class PropuestaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Proposed ID");
      cabeceras.add("Discharge date");
      cabeceras.add("User");
      cabeceras.add("Class");
      cabeceras.add("Type");
      cabeceras.add("Subtype");
      cabeceras.add("Status");
      cabeceras.add("Sanction");
      cabeceras.add("Resolution");
      cabeceras.add("Last change date");
      cabeceras.add("Total amount");
      cabeceras.add("Proposed link");
      cabeceras.add("Loan ID");
      cabeceras.add("Territorial");
      cabeceras.add("Product");
      cabeceras.add("First holder");
      cabeceras.add("Balance in management");
      cabeceras.add("Borrowers");
      cabeceras.add("Warranties");
      cabeceras.add("Judicial");
      cabeceras.add("Warranty ID");
      cabeceras.add("Warranty type");
      cabeceras.add("Finca registry");
      cabeceras.add("Location");
      cabeceras.add("Province");
      cabeceras.add("Value");
      cabeceras.add("Mortgage liability");
      cabeceras.add("Range");
      cabeceras.add("Description");
      cabeceras.add("Negotiated possession");
      cabeceras.add("Borrower ID");
      cabeceras.add("Intervention type");
      cabeceras.add("Intervention order");
      cabeceras.add("Name");
      cabeceras.add("Doc num");
      cabeceras.add("Phone");
      cabeceras.add("Vulnerability");
      cabeceras.add("Amount monetary delivery");
      cabeceras.add("Non-Monetary delivery amount");
      cabeceras.add("Condonation");
      cabeceras.add("Comment");
      cabeceras.add("Validity date");



    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id propuesta");
      cabeceras.add("Fecha Alta");
      cabeceras.add("Usuario");
      cabeceras.add("Clase");
      cabeceras.add("Tipo");
      cabeceras.add("Subtipo");
      cabeceras.add("Estado");
      cabeceras.add("Sancion");
      cabeceras.add("Resolucion");
      cabeceras.add("Fecha Ultimo Cambio");
      cabeceras.add("Importe Total");
      cabeceras.add("Vinculacion Propuesta");
      cabeceras.add("Id Contrato");
      cabeceras.add("Territorial");
      cabeceras.add("Producto");
      cabeceras.add("Primer Titular");
      cabeceras.add("Saldo En Gestion");
      cabeceras.add("Intervinientes");
      cabeceras.add("Garantias");
      cabeceras.add("Judicial");
      cabeceras.add("Id Garantia");
      cabeceras.add("Tipo Garantia");
      cabeceras.add("Finca Registral");
      cabeceras.add("Localidad");
      cabeceras.add("Provincia");
      cabeceras.add("Valor");
      cabeceras.add("Responsabilidad Hipotecaria");
      cabeceras.add("Rango");
      cabeceras.add("Descripcion");
      cabeceras.add("Posesion Negociada");
      cabeceras.add("Id Interviniente");
      cabeceras.add("Tipo Intervencion");
      cabeceras.add("Orden Intervencion");
      cabeceras.add("Nombre");
      cabeceras.add("Num Doc");
      cabeceras.add("Telefono");
      cabeceras.add("Vulnerabilidad");
      cabeceras.add("Importe Entrega Dineraria");
      cabeceras.add("Importe Entrega No Dineraria");
      cabeceras.add("Condonacion");
      cabeceras.add("Comentario");
      cabeceras.add("Fecha Validez");
    }
  }

  public PropuestaExcel(Propuesta propuesta, List<IntervinienteDTOToList> inters, Boolean posesionNegociada) {

    List<Contrato> contratos = new ArrayList<>(propuesta.getContratos());

    List<Bien> bienes = propuesta.getBienes().stream().map(PropuestaBien::getBien).collect(Collectors.toList());

    List<ContratoBien> contratoBienes = bienes.stream().map(bien -> bien.getContratos()).collect(Collectors.toList()).stream()
      .collect(ArrayList::new, List::addAll, List::addAll);

    boolean judicial = false;
    if(posesionNegociada) {

      Contrato contratoAux = propuesta.getExpedientePosesionNegociada().getContrato();
      if (contratoAux!= null) {
        if (contratoAux.getProcedimientos().size() > 0) {
          judicial = true;
        }
      }
    }
    else{
      for (Contrato contratoAux : propuesta.getExpediente().getContratos()) {
        if (contratoAux.getProcedimientos().size() > 0) {
          judicial = true;
          break;
        }
      }
    }
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      if (posesionNegociada){
        this.add("Connection ID", propuesta.getExpedientePosesionNegociada().getIdConcatenado());
        this.add("Balance in management", propuesta.getExpedientePosesionNegociada().getSaldoGestion());
        this.add("Borrowers", propuesta.getOcupantes().isEmpty() ? "NO" : "YES");
        this.add("Borrower ID", propuesta.getOcupantes().stream().map(inte -> inte.getId() != null ? inte.getId().toString() : " ").collect(Collectors.joining(", ")));
        //Para sacar la relación con el tipo Intervención
        this.add("Intervention type", null);
        this.add("Intervention order", null);
        this.add("Name", propuesta.getOcupantes().stream().map(inte -> inte.getNombre() != null ? inte.getNombre() : " ").collect(Collectors.joining(", ")));
        this.add("Doc num", propuesta.getOcupantes().stream().map(inte -> inte.getDni() != null ? inte.getDni() : " ").collect(Collectors.joining(", ")));
        this.add("Phone", propuesta.getOcupantes().stream().map(inte -> inte.getTelefonoPrincipal() != null ? inte.getTelefonoPrincipal() : " ").collect(Collectors.joining(", ")));
        this.add("Vulnerability",null);
      }
      else{
        this.add("Connection ID", propuesta.getExpediente().getIdConcatenado());
        this.add("Balance in management", propuesta.getExpediente().getSaldoGestion());
        this.add("Borrowers", inters.isEmpty() ? "NO" : "YES");
        this.add("Borrower ID", inters.stream().map(inte -> inte.getId() != null ? inte.getId().toString() : " ").collect(Collectors.joining(", ")));
        //Para sacar la relación con el tipo Intervención
        this.add("Intervention type", inters.stream().map(inte -> inte.getTipoIntervencion() != null ? inte.getTipoIntervencion().getValor() : " ").collect(Collectors.joining(", ")));
        this.add("Intervention order", inters.stream().map(inte -> inte.getOrdenIntervencion() != null ? inte.getOrdenIntervencion().toString() : " ").collect(Collectors.joining(", ")));
        this.add("Name", inters.stream().map(inte -> inte.getNombre() != null ? inte.getNombre() : " ").collect(Collectors.joining(", ")));
        this.add("Doc num", inters.stream().map(inte -> inte.getNumeroDocumento() != null ? inte.getNumeroDocumento() : " ").collect(Collectors.joining(", ")));
        this.add("Phone", inters.stream().map(inte -> inte.getTelefono() != null ? inte.getTelefono() : " ").collect(Collectors.joining(", ")));
        this.add("Vulnerability", inters.stream().map(inte -> inte.getVulnerabilidad() != null ? inte.getVulnerabilidad() ? "YES" :"NO" : " ").collect(Collectors.joining(", ")));

      }
      this.add("Proposed ID", propuesta.getId());
      this.add("discharge date", propuesta.getFechaAlta());
      this.add("User", propuesta.getUsuario() != null ? propuesta.getUsuario().getNombre() : null);
      this.add("Class",propuesta.getClasePropuesta()!=null ? propuesta.getClasePropuesta().getValorIngles() : null);
      this.add("Type", propuesta.getTipoPropuesta() != null ? propuesta.getTipoPropuesta().getValorIngles() : null);
      this.add("Subtype", propuesta.getSubtipoPropuesta() != null ? propuesta.getSubtipoPropuesta().getValorIngles() : null);
      this.add("Status", propuesta.getEstadoPropuesta() != null ? propuesta.getEstadoPropuesta().getValorIngles() : null);
      this.add("Sanction", propuesta.getSancionPropuesta() != null ? propuesta.getSancionPropuesta().getValorIngles() : null);
      this.add("Resolution", propuesta.getResolucionPropuesta() != null ? propuesta.getResolucionPropuesta().getValorIngles() : null);
      this.add("Last change date", propuesta.getFechaUltimoCambio());
      this.add("Total amount", propuesta.getImporteTotal() != null ? propuesta.getImporteTotal() : "0.0");
      this.add("Proposed link", propuesta.getVinculoPropuesta() != null ? propuesta.getVinculoPropuesta().getId() : null);
      this.add("Loan ID", contratos.stream().map(con -> con.getIdCarga()).collect(Collectors.joining(", ")));
      this.add("Territorial", contratos.stream().map(con -> con.getTerritorial() != null ? con.getTerritorial() : " ").collect(Collectors.joining(", ")));
      this.add("Product", contratos.stream().map(Contrato::getProducto).map(Producto::getValorIngles).collect(Collectors.joining(", ")));
      this.add("First holder", contratos.stream().map(Contrato::getPrimerInterviniente).map(inte ->
        (inte.getNombre() != null ? inte.getNombre() : "") + (inte.getApellidos() != null ? " " + inte.getApellidos() : ""))
        .collect(Collectors.joining(", ")));
      this.add("Warranties", bienes.isEmpty() ? "NO" : "YES");
      this.add("Judicial", judicial ? "YES" : "NO");

      this.add("Warranty ID", bienes.stream().map(bn -> bn.getId().toString()).collect(Collectors.joining(", ")));
      this.add("Warranty type", bienes.stream().map(bn -> bn.getTipoBien() != null ? bn.getTipoBien().getValorIngles() : " ").collect(Collectors.joining(", ")));
      this.add("Finca registry", bienes.stream().map(bn -> bn.getFinca() != null ? bn.getFinca() : " ").collect(Collectors.joining(", ")));
      this.add("Location", bienes.stream().map(bn -> bn.getLocalidad() != null ? bn.getLocalidad().getValorIngles() : " ").collect(Collectors.joining(", ")));
      this.add("Province", bienes.stream().map(bn -> bn.getProvincia() != null ? bn.getProvincia().getValorIngles() : " ").collect(Collectors.joining(", ")));
      if(posesionNegociada) {
        this.add("Value", bienes.stream().map(bn -> bn.getImporteGarantizado() != null ? bn.getImporteGarantizado().toString() : " ").collect(Collectors.joining(", ")));
        this.add("Mortgage liability", bienes.stream().map(bn -> bn.getResponsabilidadHipotecaria() != null ? bn.getResponsabilidadHipotecaria().toString() : " ").collect(Collectors.joining(", ")));
      } else {
        this.add("Value", contratoBienes.stream().map(bn -> bn.getImporteGarantizado() != null ? bn.getImporteGarantizado().toString() : " ").collect(Collectors.joining(", ")));
        this.add("Mortgage liability", contratos.stream().map(con -> con.getResponsabilidadHipotecaria() != null ?
          con.getResponsabilidadHipotecaria().toString() : "0,0").collect(Collectors.joining(", ")));
      }
      this.add("Negotiated possession", bienes.stream().map(bn -> bn.getPosesionNegociada() != null ? "Si" : "No").collect(Collectors.joining(", ")));
      this.add("Range", contratos.stream().map(con -> con.getRangoHipotecario() != null ? con.getRangoHipotecario() : " ").collect(Collectors.joining(", ")));
      this.add("Description", contratos.stream().map(con -> con.getDescripcionProducto() != null ? con.getDescripcionProducto() : " ").collect(Collectors.joining(", ")));

      this.add("Amount monetary delivery", propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().map(importePro -> importePro.getImporteEntregaDineraria() + " ").collect(Collectors.joining(", ")) : null); //propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().findFirst().get().getImporteEntregaDineraria() : null
      this.add("Non-Monetary delivery amount", propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().map(importePro -> importePro.getImporteEntregaNoDineraria() + " ").collect(Collectors.joining(", ")) : null); //propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().findFirst().get().getImporteEntregaNoDineraria() : null
      this.add("Condonation", propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().map(importePro -> importePro.getCondonacionDeuda() + " ").collect(Collectors.joining(", ")) : null); //propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().findFirst().get().getCondonacionDeuda() : null
      this.add("Comment", propuesta.getComentarios()  != null ? propuesta.getComentarios().stream().map(ComentarioPropuesta::getComentario).collect(Collectors.joining(", ")).replaceAll("<p>","").replaceAll("</p>","") : null);
      this.add("Validity date", propuesta.getFechaValidez() != null ? propuesta.getFechaValidez() : null);

    }

    else{

    if (posesionNegociada){
      this.add("Id Expediente", propuesta.getExpedientePosesionNegociada().getIdConcatenado());
      this.add("Saldo En Gestion", propuesta.getExpedientePosesionNegociada().getSaldoGestion());
      this.add("Intervinientes", propuesta.getOcupantes().isEmpty() ? "No" : "Si");
      this.add("Id Interviniente", propuesta.getOcupantes().stream().map(inte -> inte.getId() != null ? inte.getId().toString() : " ").collect(Collectors.joining(", ")));
      //Para sacar la relación con el tipo Intervención
      this.add("Tipo Intervencion", null);
      this.add("Orden Intervencion", null);
      this.add("Nombre", propuesta.getOcupantes().stream().map(inte -> inte.getNombre() != null ? inte.getNombre() : " ").collect(Collectors.joining(", ")));
      this.add("Num Doc", propuesta.getOcupantes().stream().map(inte -> inte.getDni() != null ? inte.getDni() : " ").collect(Collectors.joining(", ")));
      this.add("Telefono", propuesta.getOcupantes().stream().map(inte -> inte.getTelefonoPrincipal() != null ? inte.getTelefonoPrincipal() : " ").collect(Collectors.joining(", ")));
      this.add("Vulnerabilidad",null);
    }
    else{
      this.add("Id Expediente", propuesta.getExpediente().getIdConcatenado());
      this.add("Saldo En Gestion", propuesta.getExpediente().getSaldoGestion());
      this.add("Intervinientes", inters.isEmpty() ? "No" : "Si");
      this.add("Id Interviniente", inters.stream().map(inte -> inte.getId() != null ? inte.getId().toString() : " ").collect(Collectors.joining(", ")));
      //Para sacar la relación con el tipo Intervención
      this.add("Tipo Intervencion", inters.stream().map(inte -> inte.getTipoIntervencion() != null ? inte.getTipoIntervencion().getValor() : " ").collect(Collectors.joining(", ")));
      this.add("Orden Intervencion", inters.stream().map(inte -> inte.getOrdenIntervencion() != null ? inte.getOrdenIntervencion().toString() : " ").collect(Collectors.joining(", ")));
      this.add("Nombre", inters.stream().map(inte -> inte.getNombre() != null ? inte.getNombre() : " ").collect(Collectors.joining(", ")));
      this.add("Num Doc", inters.stream().map(inte -> inte.getNumeroDocumento() != null ? inte.getNumeroDocumento() : " ").collect(Collectors.joining(", ")));
      this.add("Telefono", inters.stream().map(inte -> inte.getTelefono() != null ? inte.getTelefono() : " ").collect(Collectors.joining(", ")));
      this.add("Vulnerabilidad", inters.stream().map(inte -> inte.getVulnerabilidad() != null ? inte.getVulnerabilidad() ? "Si" :"No" : " ").collect(Collectors.joining(", ")));

    }
    this.add("Id propuesta", propuesta.getId());
    this.add("Fecha Alta", propuesta.getFechaAlta());
    this.add("Usuario", propuesta.getUsuario() != null ? propuesta.getUsuario().getNombre() : null);
    this.add("Clase",propuesta.getClasePropuesta()!=null ? propuesta.getClasePropuesta().getValor() : null);
    this.add("Tipo", propuesta.getTipoPropuesta() != null ? propuesta.getTipoPropuesta().getValor() : null);
    this.add("Subtipo", propuesta.getSubtipoPropuesta() != null ? propuesta.getSubtipoPropuesta().getValor() : null);
    this.add("Estado", propuesta.getEstadoPropuesta() != null ? propuesta.getEstadoPropuesta().getValor() : null);
    this.add("Sancion", propuesta.getSancionPropuesta() != null ? propuesta.getSancionPropuesta().getValor() : null);
    this.add("Resolucion", propuesta.getResolucionPropuesta() != null ? propuesta.getResolucionPropuesta().getValor() : null);
    this.add("Fecha Ultimo Cambio", propuesta.getFechaUltimoCambio());
    this.add("Importe Total", propuesta.getImporteTotal() != null ? propuesta.getImporteTotal() : "0.0");
    this.add("Vinculacion Propuesta", propuesta.getVinculoPropuesta() != null ? propuesta.getVinculoPropuesta().getId() : null);
    this.add("Id Contrato", contratos.stream().map(con -> con.getIdCarga()).collect(Collectors.joining(", ")));
    this.add("Territorial", contratos.stream().map(con -> con.getTerritorial() != null ? con.getTerritorial() : " ").collect(Collectors.joining(", ")));
    this.add("Producto", contratos.stream().map(Contrato::getProducto).map(Producto::getValor).collect(Collectors.joining(", ")));
    this.add("Primer Titular", contratos.stream().map(Contrato::getPrimerInterviniente).map(inte ->
      (inte.getNombre() != null ? inte.getNombre() : "") + (inte.getApellidos() != null ? " " + inte.getApellidos() : ""))
      .collect(Collectors.joining(", ")));
    this.add("Garantias", bienes.isEmpty() ? "No" : "Si");
    this.add("Judicial", judicial ? "Si" : "No");

    this.add("Id Garantia", bienes.stream().map(bn -> bn.getId().toString()).collect(Collectors.joining(", ")));
    this.add("Tipo Garantia", bienes.stream().map(bn -> bn.getTipoBien() != null ? bn.getTipoBien().getValor() : " ").collect(Collectors.joining(", ")));
    this.add("Finca Registral", bienes.stream().map(bn -> bn.getFinca() != null ? bn.getFinca() : " ").collect(Collectors.joining(", ")));
    this.add("Localidad", bienes.stream().map(bn -> bn.getLocalidad() != null ? bn.getLocalidad().getValor() : " ").collect(Collectors.joining(", ")));
    this.add("Provincia", bienes.stream().map(bn -> bn.getProvincia() != null ? bn.getProvincia().getValor() : " ").collect(Collectors.joining(", ")));
      if(posesionNegociada) {
        this.add("Valor", bienes.stream().map(bn -> bn.getImporteGarantizado() != null ? bn.getImporteGarantizado().toString() : " ").collect(Collectors.joining(", ")));
        this.add("Responsabilidad Hipotecaria", bienes.stream().map(bn -> bn.getResponsabilidadHipotecaria() != null ? bn.getResponsabilidadHipotecaria().toString() : " ").collect(Collectors.joining(", ")));
      } else {
        this.add("Valor", contratoBienes.stream().map(bn -> bn.getImporteGarantizado() != null ? bn.getImporteGarantizado().toString() : " ").collect(Collectors.joining(", ")));
        this.add("Responsabilidad Hipotecaria", contratos.stream().map(con -> con.getResponsabilidadHipotecaria() != null ?
          con.getResponsabilidadHipotecaria().toString() : "0,0").collect(Collectors.joining(", ")));
      }
    this.add("Posesion Negociada", bienes.stream().map(bn -> bn.getPosesionNegociada() != null ? "Si" : "No").collect(Collectors.joining(", ")));
    this.add("Rango", contratos.stream().map(con -> con.getRangoHipotecario() != null ? con.getRangoHipotecario() : " ").collect(Collectors.joining(", ")));
    this.add("Descripcion", contratos.stream().map(con -> con.getDescripcionProducto() != null ? con.getDescripcionProducto() : " ").collect(Collectors.joining(", ")));

    this.add("Importe Entrega Dineraria", propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().map(importePro -> importePro.getImporteEntregaDineraria() + " ").collect(Collectors.joining(", ")) : null); //propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().findFirst().get().getImporteEntregaDineraria() : null
    this.add("Importe Entrega No Dineraria", propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().map(importePro -> importePro.getImporteEntregaNoDineraria() + " ").collect(Collectors.joining(", ")) : null); //propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().findFirst().get().getImporteEntregaNoDineraria() : null
    this.add("Condonacion", propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().map(importePro -> importePro.getCondonacionDeuda() + " ").collect(Collectors.joining(", ")) : null); //propuesta.getImportesPropuestas()  != null ? propuesta.getImportesPropuestas().stream().findFirst().get().getCondonacionDeuda() : null
    this.add("Comentario", propuesta.getComentarios()  != null ? propuesta.getComentarios().stream().map(ComentarioPropuesta::getComentario).collect(Collectors.joining(", ")).replaceAll("<p>","").replaceAll("</p>","") : null);
    this.add("Fecha Validez", propuesta.getFechaValidez() != null ? propuesta.getFechaValidez() : null);
    }
  }


  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
