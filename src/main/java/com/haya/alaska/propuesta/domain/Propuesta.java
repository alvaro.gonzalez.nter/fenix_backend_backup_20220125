package com.haya.alaska.propuesta.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.clase_propuesta.domain.ClasePropuesta;
import com.haya.alaska.comentario_propuesta.domain.ComentarioPropuesta;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.datos_check_list.domain.DatosCheckList;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.resolucion_propuesta.domain.ResolucionPropuesta;
import com.haya.alaska.resultado_pbc.domain.ResultadoPBC;
import com.haya.alaska.sancion_propuesta.domain.SancionPropuesta;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_PROPUESTA")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_PROPUESTA")
public class Propuesta implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;

  @Column(name = "DES_ID_CARGA")
  private String idCarga;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_USUARIO", referencedColumnName = "id", nullable = false)
  private Usuario usuario;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "id")
  private Expediente expediente;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXP_POS_NEGOCIADA")
  private ExpedientePosesionNegociada expedientePosesionNegociada;

  @ManyToMany
  @JoinTable(name = "RELA_CONTRATO_PROPUESTA",
    joinColumns = {@JoinColumn(name = "ID_PROPUESTA")},
    inverseJoinColumns = {@JoinColumn(name = "ID_CONTRATO")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_CONTRATO_PROPUESTA")
  private Set<Contrato> contratos = new HashSet<>();


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_VINCULO_PROPUESTA", nullable = true)
  private Propuesta vinculoPropuesta;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_CLASE_PROPUESTA")
  ClasePropuesta clasePropuesta;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_TIPO_PROPUESTA")
  TipoPropuesta tipoPropuesta;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_SUBTIPO_PROPUESTA")
  SubtipoPropuesta subtipoPropuesta;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_ESTADO_PROPUESTA")
  EstadoPropuesta estadoPropuesta;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_SANCION_PROPUESTA")
  SancionPropuesta sancionPropuesta;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_SANCION")
  Date fechaSancion;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_RESOLUCION_PROPUESTA")
  ResolucionPropuesta resolucionPropuesta;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_RESOLUCION")
  Date fechaResolucion;

  @ManyToMany
  @JoinTable(name = "RELA_INTERVINIENTE_PROPUESTA",
    joinColumns = {@JoinColumn(name = "ID_PROPUESTA")},
    inverseJoinColumns = {@JoinColumn(name = "ID_INTERVINIENTE")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_INTERVINIENTE_PROPUESTA")
  private Set<Interviniente> intervinientes = new HashSet<>();

  @ManyToMany
  @JoinTable(name = "RELA_OCUPANTES_PROPUESTA",
    joinColumns = {@JoinColumn(name = "ID_PROPUESTA")},
    inverseJoinColumns = {@JoinColumn(name = "ID_OCUPANTES")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_OCUPANTES_PROPUESTA")
  private Set<Ocupante> ocupantes = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTRATEGIA")
  private EstrategiaAsignada estrategia;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROPUESTA")
  @NotAudited
  private Set<PropuestaBien> bienes = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROPUESTA")
  @NotAudited
  private Set<Formalizacion> formalizaciones = new HashSet<>();

  @ManyToMany(cascade = {CascadeType.ALL})
  @JoinTable(name = "RELA_PROPUESTA_IMPORTE_PROPUESTA",
    joinColumns = {@JoinColumn(name = "ID_PROPUESTA")},
    inverseJoinColumns = {@JoinColumn(name = "ID_IMPORTE_PROPUESTA")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_PROPUESTA_IMPORTE_PROPUESTA")
  private Set<ImportePropuesta> importesPropuestas = new HashSet<>();

  @ManyToMany
  @JoinTable(name = "RELA_PROPUESTA_ACUERDO_PAGO",
    joinColumns = {@JoinColumn(name = "ID_PROPUESTA")},
    inverseJoinColumns = {@JoinColumn(name = "ID_ACUERDO_PAGO")}
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_PROPUESTA_ACUERDO_PAGO")
  private Set<AcuerdoPago> acuerdosPago = new HashSet<>();

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA_ALTA")
  Date fechaAlta;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA_ULTIMO_CAMBIO")
  Date fechaUltimoCambio;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROPUESTA")
  @NotAudited
  Set<ComentarioPropuesta> comentarios;

  @Column(name = "IND_BORRADOR", columnDefinition = "tinyint(1)")
  Boolean borrador;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @Column(name = "NUM_IMPORTE_TOTAL", nullable = false)
  private Double importeTotal;

  @Column(name = "NUM_IMPORTE_ELEVACION", nullable = false)
  private Double importeElevacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA_VALIDEZ")
  private Date fechaValidez;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA_ELEVACION")
  private Date fechaElevacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_RESOLUCION_PBC")
  Date fechaResolucionPBC;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_RESULTADO_PBC")
  ResultadoPBC resultadoPBC;

  @Column(name = "NUM_IMPORTE_COLABORA")
  private Double importeColabora;

  @Column(name = "NUM_COLABORA")
  private Integer numColabora;

  @Column(name = "NUM_ADVISORY_NOTE")
  private Integer numAdvisoryNote;

  @Column(name = "FCH_ENVIO_CES")
  private Date fechaEnvioCES;

  @Column(name = "FCH_ENVIO_WORKING_GROUP")
  private Date fechaEnvioWorkingGroup;

  @Column(name = "FCH_RECEPCION_APROBACION_ADVISORY_NOTE")
  private Date fechaRecepcionAprobacionAdvisoryNote;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROPUESTA")
  @NotAudited
  Set<DatosCheckList> datosCheckLists;

  public void addBienes(List<PropuestaBien> bienes) {
    this.bienes.addAll(bienes);
  }

  public void addIntervinientes(List<Interviniente> intervinientes) {
    this.intervinientes.addAll(intervinientes);
  }

  public void addOcupantes(List<Ocupante> ocupantes) {
    this.ocupantes.addAll(ocupantes);
  }

  public void addContratos(List<Contrato> contratos) {
    this.contratos.addAll(contratos);
  }

  public void addImportes(List<ImportePropuesta> importes) {
    this.importesPropuestas.addAll(importes);
  }

  public void addAcuerdosPago(List<AcuerdoPago> acuerdosPago) {
    this.acuerdosPago.addAll(acuerdosPago);
  }

  public Double getDeudaActual() {
    var deuda = 0.0;
    for (Contrato c : this.contratos) {
      if (c.getSaldoGestion() != null)
        deuda += c.getSaldoGestion();
    }
    return deuda;
  }

  public boolean isFormalizacion() {
    if (this.subtipoPropuesta != null
        && this.subtipoPropuesta.getFormalizacion() != null
        && this.subtipoPropuesta.getFormalizacion()
        && !this.tipoPropuesta.getCodigo().equals("DP")) return true;
    if (this.getExpediente() != null
        && this.getExpediente().getCartera().getFormalizacion() != null
        && this.getExpediente().getCartera().getFormalizacion()) return true;
    return false;
  }
}
