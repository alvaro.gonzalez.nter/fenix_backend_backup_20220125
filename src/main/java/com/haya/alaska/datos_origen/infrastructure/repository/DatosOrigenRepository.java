package com.haya.alaska.datos_origen.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.datos_origen.domain.DatosOrigen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DatosOrigenRepository extends JpaRepository<DatosOrigen, Integer> {}
