package com.haya.alaska.datos_origen.domain;

import com.haya.alaska.origen_dato.domain.OrigenDato;
import com.haya.alaska.pagina_publica.domain.PaginaPublica;
import com.haya.alaska.perfil_profesional.domain.PerfilProfesional;
import com.haya.alaska.red_social.domain.RedSocial;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_DATOS_ORIGEN")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_DATOS_ORIGEN")
public class DatosOrigen implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  //Origen de los datos
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ACTUALIZACION")
  private Date fechaActualizacion;

  @Column(name = "DES_LINK")
  private String link;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_GESTOR")
  private Usuario gestor;

  //Origen
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ORIGEN_DATO")
  private OrigenDato origenDato;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FECHA")
  private Date fecha;

  //Fuente
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PAGINA_PUBLICA")
  private PaginaPublica paginaPublica;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RED_SOCIAL")
  private RedSocial redSocial;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERFIL_PROFESIONAL")
  private PerfilProfesional perfilProfesional;
  @Column(name = "DES_OTRA_FUENTE")
  private String otraFuente;

  //Extras
  /*@ElementCollection()
  @CollectionTable(name = "RELA_VISITA_FOTOS", joinColumns = @JoinColumn(name = "ID_VISITA"))
  @Column(name = "ID_FOTO_GESTOR_DOCUMENTAL")
  @NotAudited
  private Set<Long> fotos = new HashSet<>();*/

  @Column(name = "DES_OBSERVACIONES")
  private String observaciones;
}
