package com.haya.alaska.tipo_accion_procedimiento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_accion_procedimiento.domain.TipoAccionProcedimiento;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoAccionProcedimientoRepository extends CatalogoRepository<TipoAccionProcedimiento, Integer> {

}
