package com.haya.alaska.tipo_accion_procedimiento.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_ACCION_PROCEDIMIENTO")
@Entity
@Table(name = "LKUP_TIPO_ACCION_PROCEDIMIENTO")
public class TipoAccionProcedimiento extends Catalogo {
}
