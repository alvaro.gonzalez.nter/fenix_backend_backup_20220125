package com.haya.alaska.importe_propuesta.infrastructure.repository;

import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImportePropuestaRepository extends JpaRepository<ImportePropuesta, Integer> {}
