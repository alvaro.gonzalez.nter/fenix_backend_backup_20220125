package com.haya.alaska.importe_propuesta.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.propuesta.domain.Propuesta;
import lombok.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_IMPORTE_PROPUESTA")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_IMPORTE_PROPUESTA")
public class ImportePropuesta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @ToString.Include
    private Integer id;

    @ManyToMany
    @JoinTable(name = "RELA_PROPUESTA_IMPORTE_PROPUESTA",
            joinColumns = { @JoinColumn(name = "ID_IMPORTE_PROPUESTA") },
            inverseJoinColumns = { @JoinColumn(name = "ID_PROPUESTA") }
    )
    @JsonIgnore
    @AuditJoinTable(name = "HIST_RELA_PROPUESTA_IMPORTE_PROPUESTA")
    private Set<Propuesta> propuestas = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "RELA_CONTRATO_IMPORTE_PROPUESTA",
            joinColumns = { @JoinColumn(name = "ID_IMPORTE_PROPUESTA") },
            inverseJoinColumns = { @JoinColumn(name = "ID_CONTRATO") }
    )
    @JsonIgnore
    @AuditJoinTable(name = "HIST_RELA_CONTRATO_IMPORTE_PROPUESTA")
    private Set<Contrato> contratos = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "RELA_BIEN_POSESION_NEGOCIADA_IMPORTE_PROPUESTA",
            joinColumns = { @JoinColumn(name = "ID_IMPORTE_PROPUESTA") },
            inverseJoinColumns = { @JoinColumn(name = "ID_BIEN_POSESION_NEGOCIADA") }
    )
    @JsonIgnore
    @AuditJoinTable(name = "HIST_RELA_BIEN_POSESION_NEGOCIADA_IMPORTE_PROPUESTA")
    private Set<BienPosesionNegociada> bienes = new HashSet<>();

    @Column(name = "NUM_IMPORTE_ENTREGA_DINERARIA", columnDefinition = "decimal(15,2)")
    private Double importeEntregaDineraria;
    @Column(name = "NUM_IMPORTE_ENTREGA_NO_DINERARIA", columnDefinition = "decimal(15,2)")
    private Double importeEntregaNoDineraria;
    @Column(name = "NUM_CONDONACION_DEUDA", columnDefinition = "decimal(15,2)")
    private Double condonacionDeuda;

    @Column(name = "DES_COMENTARIO")
    private String comentario;

    @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
    private Boolean activo = true;

    public void addContratos(List<Contrato> contratos) {
        this.contratos.addAll(contratos);
    }
    public void addBienes(List<BienPosesionNegociada> bienes) {
        this.bienes.addAll(bienes);
    }
}
