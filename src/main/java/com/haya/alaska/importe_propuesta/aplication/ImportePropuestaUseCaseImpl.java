package com.haya.alaska.importe_propuesta.aplication;

import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.importe_propuesta.infrastructure.repository.ImportePropuestaRepository;
import com.haya.alaska.propuesta.infrastructure.controller.dto.ImportePropuestaInputDTO;
import com.haya.alaska.propuesta.infrastructure.mapper.ImportePropuestaMapper;
import com.haya.alaska.shared.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ImportePropuestaUseCaseImpl implements ImportePropuestaUseCase{

    @Autowired
    ContratoRepository contratoRepository;

    @Autowired
    ExpedienteRepository expedienteRepository;

    @Autowired
    ImportePropuestaMapper importePropuestaMapper;

    @Autowired
    ImportePropuestaRepository importePropuestaRepository;


    public void createImportePropuesta(ImportePropuestaInputDTO importePropuestaInputDTO, Boolean posesionNegociada) throws NotFoundException
    {
        var importePropuesta = importePropuestaMapper.parse(importePropuestaInputDTO, posesionNegociada);
        importePropuestaRepository.save(importePropuesta);
    }

    public List<ImportePropuesta> createImportesPropuesta(List<ImportePropuestaInputDTO> importesPropuestaInputDTO, Boolean posesionNegociada) throws NotFoundException
    {
        List<ImportePropuesta> importesSaved = new ArrayList<>();
        for(ImportePropuestaInputDTO importePropuestaInputDTO: importesPropuestaInputDTO){
            var importePropuesta = importePropuestaMapper.parse(importePropuestaInputDTO, posesionNegociada);
            var importeSaved = importePropuestaRepository.saveAndFlush(importePropuesta);
            importesSaved.add(importeSaved);
        }
        return importesSaved;
    }

}
