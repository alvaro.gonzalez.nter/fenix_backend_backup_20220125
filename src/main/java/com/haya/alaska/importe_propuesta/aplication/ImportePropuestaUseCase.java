package com.haya.alaska.importe_propuesta.aplication;

import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.propuesta.infrastructure.controller.dto.ImportePropuestaInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;

import java.util.List;

public interface ImportePropuestaUseCase {
    void createImportePropuesta(ImportePropuestaInputDTO importePropuestaInputDTO, Boolean posesionNegociada) throws NotFoundException;
    List<ImportePropuesta> createImportesPropuesta(List<ImportePropuestaInputDTO> importesPropuestaInputDTO, Boolean posesionNegociada) throws NotFoundException;
}
