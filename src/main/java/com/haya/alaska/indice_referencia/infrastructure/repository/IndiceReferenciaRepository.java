package com.haya.alaska.indice_referencia.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.indice_referencia.domain.IndiceReferencia;

@Repository
public interface IndiceReferenciaRepository extends CatalogoRepository<IndiceReferencia, Integer> {
	
}
