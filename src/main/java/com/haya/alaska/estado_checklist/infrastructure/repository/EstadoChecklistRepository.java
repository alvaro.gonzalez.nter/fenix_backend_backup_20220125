package com.haya.alaska.estado_checklist.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estado_checklist.domain.EstadoChecklist;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoChecklistRepository extends CatalogoRepository<EstadoChecklist, Integer> {}
