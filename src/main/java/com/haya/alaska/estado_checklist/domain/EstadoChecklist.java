package com.haya.alaska.estado_checklist.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@AuditTable(value = "HIST_LKUP_ESTADO_CHECKLIST")
@Entity
@Table(name = "LKUP_ESTADO_CHECKLIST")
@NoArgsConstructor
public class EstadoChecklist extends Catalogo {

}
