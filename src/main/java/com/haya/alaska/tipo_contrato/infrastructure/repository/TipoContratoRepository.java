package com.haya.alaska.tipo_contrato.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_contrato.domain.TipoContrato;

@Repository
public interface TipoContratoRepository extends CatalogoRepository<TipoContrato, Integer> {
	 
}
