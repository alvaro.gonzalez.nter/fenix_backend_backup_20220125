package com.haya.alaska.tipo_contrato.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.interviniente.domain.Interviniente;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_CONTRATO")
@Entity
@Table(name = "LKUP_TIPO_CONTRATO")
public class TipoContrato extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_CONTRATO")
  @NotAudited
  private Set<Interviniente> intervinientes = new HashSet<>();
}
