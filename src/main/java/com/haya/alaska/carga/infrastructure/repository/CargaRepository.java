package com.haya.alaska.carga.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.carga.domain.Carga;

import java.util.List;

@Repository
public interface CargaRepository extends JpaRepository<Carga, Integer> {

  List<Carga> findByBienesId(Integer id);

}
