package com.haya.alaska.carga.infrastructure.mapper;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.mapper.BienMapper;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.infrastructure.controller.dto.CargaDto;
import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.shared.dto.BienDTOToList;
import com.haya.alaska.shared.dto.CargaDTOToList;
import com.haya.alaska.subtipo_carga_formalizacion.domain.SubtipoCargaFormalizacion;
import com.haya.alaska.subtipo_carga_formalizacion.infrastructure.repository.SubtipoCargaFormalizacionRepository;
import com.haya.alaska.tipo_acreedor.infrastructure.repository.TipoAcreedorRepository;
import com.haya.alaska.tipo_carga.infrastructure.repository.TipoCargaRepository;
import com.haya.alaska.tipo_carga_formalizacion.infrastructure.repository.TipoCargaFormalizacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class CargaMapper {

  @Autowired
  private BienMapper bienMapper;
  @Autowired
  private CatalogoMinInfoMapper catalogoMinInfoMapper;
  @Autowired
  private TipoCargaRepository tipoCargaRepository;
  @Autowired
  private TipoAcreedorRepository tipoAcreedorRepository;
  @Autowired
  private TipoCargaFormalizacionRepository tipoCargaFormalizacionRepository;
  @Autowired
  private SubtipoCargaFormalizacionRepository subtipoCargaFormalizacionRepository;
  @Autowired
  private BienRepository bienRepository;

  public List<CargaDTOToList> listCargaToListCargaDTOToList(Set<Carga> cargas, Integer idContrato) {
    List<CargaDTOToList> cargasDTO = new ArrayList<>();
    for (Carga carga : cargas) {
      cargasDTO.add(cargaToCargaDTOToList(carga, idContrato));
    }
    return cargasDTO;
  }

  public CargaDTOToList cargaToCargaDTOToList(Carga carga, Integer idContrato) {
    CargaDTOToList cargaDTOToList = new CargaDTOToList();
    cargaDTOToList.setId(carga.getId());
    if (carga.getTipoCarga() != null)
      cargaDTOToList.setTipoCarga(catalogoMinInfoMapper.entityToDto(carga.getTipoCarga()));
    if (carga.getImporte() != null) cargaDTOToList.setImporte(carga.getImporte());
    if (carga.getTipoAcreedor() != null)
      cargaDTOToList.setTipoAcreedor(catalogoMinInfoMapper.entityToDto(carga.getTipoAcreedor()));
    if (carga.getTipoCargaFormalizacion() != null)
      cargaDTOToList.setTipoCargaFormalizacion(catalogoMinInfoMapper.entityToDto(carga.getTipoCargaFormalizacion()));
    if (carga.getSubtipoCargaFormalizacion() != null)
      cargaDTOToList.setSubtipoCargaFormalizacion(catalogoMinInfoMapper.entityToDto(carga.getSubtipoCargaFormalizacion()));
    if (carga.getCargaPropia() != null) cargaDTOToList.setCargaPropia(carga.getCargaPropia());
    if (carga.getNombreAcreedor() != null) cargaDTOToList.setNombreAcreedor(carga.getNombreAcreedor());
    if (carga.getFechaAnotacion() != null) cargaDTOToList.setFechaAnotacion(carga.getFechaAnotacion());
    if (carga.getIncluidaOtrosColaterales() != null)
      cargaDTOToList.setIncluidaOtrosColaterales(carga.getIncluidaOtrosColaterales());
    if (carga.getFechaVencimiento() != null) cargaDTOToList.setFechaVencimiento(carga.getFechaVencimiento());
    if (carga.getRango() != null) cargaDTOToList.setRango(carga.getRango());

    if (idContrato != null) {
      for (Bien bien : carga.getBienes()) {
        BienDTOToList a = bienMapper.bienToBienDTOToList(bien, idContrato);
        if (cargaDTOToList.getBienes() != null) {
          cargaDTOToList.getBienes().add(a);
        } else {
          List<BienDTOToList> arrayList = new ArrayList<>();
          arrayList.add(a);
          cargaDTOToList.setBienes(arrayList);
        }
      }
    }

    return cargaDTOToList;
  }

  public Carga dtoToEntity(CargaDto cargaDto, Carga carga, boolean rem) {
    carga.setId(cargaDto.getId());
    carga.setImporte(cargaDto.getImporte());
    carga.setCargaPropia(cargaDto.getCargaPropia());
    carga.setTipoCargaFormalizacion(cargaDto.getTipoCargaFormalizacion() != null ? tipoCargaFormalizacionRepository.findById(Integer.valueOf(cargaDto.getTipoCargaFormalizacion())).orElse(null) : null);
    carga.setSubtipoCargaFormalizacion(cargaDto.getSubtipoCargaFormalizacion() != null ? subtipoCargaFormalizacionRepository.findById(Integer.valueOf(cargaDto.getSubtipoCargaFormalizacion())).orElse(null) : null);
    carga.setNombreAcreedor(cargaDto.getNombreAcreedor());
    carga.setFechaAnotacion(cargaDto.getFechaAnotacion());
    carga.setIncluidaOtrosColaterales(cargaDto.getIncluidaOtrosColaterales());
    carga.setFechaVencimiento(cargaDto.getFechaVencimiento());
    carga.setRango(cargaDto.getRango());
    if(!rem) {
      carga.setTipoCarga(cargaDto.getTipoCarga() != null ? tipoCargaRepository.findById(Integer.valueOf(cargaDto.getTipoCarga())).orElse(null) : null);
      carga.setTipoAcreedor(cargaDto.getTipoAcreedor() != null ? tipoAcreedorRepository.findById(Integer.valueOf(cargaDto.getTipoAcreedor())).orElse(null) : null);
    }else{
      carga.setTipoCarga(cargaDto.getTipoCarga() != null ? tipoCargaRepository.findByCodigo(cargaDto.getTipoCarga()).orElse(null) : null);
      carga.setTipoAcreedor(cargaDto.getTipoAcreedor() != null ? tipoAcreedorRepository.findByCodigo(cargaDto.getTipoAcreedor()).orElse(null) : null);
    }
    if (cargaDto.getBienes() != null) {
      Set<Bien> bienes = new HashSet<>();
      for (Integer bienDTOToInsert : cargaDto.getBienes()) {
        Bien bienToInsert = bienRepository.findById(bienDTOToInsert).orElse(null);
        bienes.add(bienToInsert);
      }
      carga.setBienes(bienes);
    }

    return carga;
  }

  public CargaDto entityToDto(Carga carga) {
    CargaDto cargaDto = new CargaDto();
    if (carga != null) {
      cargaDto.setId(carga.getId());
      if (carga.getTipoCarga() != null) cargaDto.setTipoCarga(carga.getTipoCarga().getId().toString());
      cargaDto.setImporte(carga.getImporte());
      if (carga.getTipoAcreedor() != null)
        cargaDto.setTipoAcreedor(carga.getTipoAcreedor().getId().toString());
      if (carga.getTipoCargaFormalizacion() != null)
        cargaDto.setTipoCargaFormalizacion(carga.getTipoCargaFormalizacion().getCodigo());
      if (carga.getSubtipoCargaFormalizacion() != null)
        cargaDto.setSubtipoCargaFormalizacion(carga.getSubtipoCargaFormalizacion().getCodigo());
      cargaDto.setNombreAcreedor(carga.getNombreAcreedor());
      cargaDto.setCargaPropia(carga.getCargaPropia());
      cargaDto.setFechaAnotacion(carga.getFechaAnotacion());
      cargaDto.setIncluidaOtrosColaterales(carga.getIncluidaOtrosColaterales());
      cargaDto.setFechaVencimiento(carga.getFechaVencimiento());
      cargaDto.setRango(carga.getRango());
    }
    return cargaDto;
  }
}
