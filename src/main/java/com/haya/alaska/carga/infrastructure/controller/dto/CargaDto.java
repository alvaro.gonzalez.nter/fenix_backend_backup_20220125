package com.haya.alaska.carga.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CargaDto {

  private Integer id;
  private String tipoCarga;
  private Double importe;
  private String tipoAcreedor;
  private Boolean cargaPropia;
  private String tipoCargaFormalizacion;
  private String subtipoCargaFormalizacion;
  private String nombreAcreedor;
  private Date fechaAnotacion;
  private Boolean incluidaOtrosColaterales;
  private Date fechaVencimiento;
  private Integer rango;
  private List<Integer> bienes;
}
