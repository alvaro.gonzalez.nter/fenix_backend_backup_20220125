package com.haya.alaska.carga.application;

import com.haya.alaska.carga.domain.Carga;

import java.util.List;

public interface CargaUseCase {

  List<Carga> findByBienesId(Integer idBien);

}
