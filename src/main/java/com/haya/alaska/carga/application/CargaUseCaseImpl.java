package com.haya.alaska.carga.application;

import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.infrastructure.repository.CargaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CargaUseCaseImpl implements CargaUseCase{

  @Autowired
  private CargaRepository cargaRepository;

  @Override
  public List<Carga> findByBienesId(Integer idBien) {
    return cargaRepository.findByBienesId(idBien);
  }
}
