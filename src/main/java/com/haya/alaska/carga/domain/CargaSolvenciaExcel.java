package com.haya.alaska.carga.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class CargaSolvenciaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Solvency Id");
      cabeceras.add("Freight Type");
      cabeceras.add("Freight Amount");
      cabeceras.add("Creditor Type");
      cabeceras.add("Creditor Name");
      cabeceras.add("Date Notation");
      cabeceras.add("Included in Other Collateral");
      cabeceras.add("Freight Expiration Date");
      cabeceras.add("Range");
    } else {
      cabeceras.add("Id Solvencia");
      cabeceras.add("Tipo Carga");
      cabeceras.add("Importe Carga");
      cabeceras.add("Tipo Acreedor");
      cabeceras.add("Nombre Acreedor");
      cabeceras.add("Fecha Notacion");
      cabeceras.add("Incluida en Otros Colaterales");
      cabeceras.add("Fecha Vencimiento de la Carga");
      cabeceras.add("Rango");
    }
  }

  public CargaSolvenciaExcel(Carga carga, Integer idSolvencia) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Solvency Id", idSolvencia);
      this.add("Freight Type", carga.getId());
      this.add("Freight Amount", carga.getImporte());
      this.add("Creditor Type", carga.getTipoAcreedor());
      this.add("Creditor Name", carga.getNombreAcreedor());
      this.add("Date Notation", carga.getFechaAnotacion());
      this.add("Included in Other Collateral", carga.getIncluidaOtrosColaterales());
      this.add("Freight Expiration Date", carga.getFechaVencimiento());
      this.add("Range", carga.getRango());
    } else {
      this.add("Id Solvencia", idSolvencia);
      this.add("Tipo Carga", carga.getId());
      this.add("Importe Carga", carga.getImporte());
      this.add("Tipo Acreedor", carga.getTipoAcreedor());
      this.add("Nombre Acreedor", carga.getNombreAcreedor());
      this.add("Fecha Notacion", carga.getFechaAnotacion());
      this.add("Incluida en Otros Colaterales", carga.getIncluidaOtrosColaterales());
      this.add("Fecha Vencimiento de la Carga", carga.getFechaVencimiento());
      this.add("Rango", carga.getRango());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
