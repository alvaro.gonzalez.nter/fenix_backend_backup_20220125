package com.haya.alaska.carga.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.formalizacion.domain.FormalizacionCarga;
import com.haya.alaska.subtipo_carga_formalizacion.domain.SubtipoCargaFormalizacion;
import com.haya.alaska.tipo_acreedor.domain.TipoAcreedor;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import com.haya.alaska.tipo_carga_formalizacion.domain.TipoCargaFormalizacion;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_CARGA")
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_CARGA")
public class Carga implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;


  @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
  @JoinTable(name = "RELA_BIEN_CARGA",
    joinColumns = @JoinColumn(name = "ID_CARGA"),
    inverseJoinColumns = @JoinColumn(name = "ID_BIEN"))
  @AuditJoinTable(name = "HIST_RELA_BIEN_CARGA")
  private Set<Bien> bienes = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_CARGA")
  private TipoCarga tipoCarga;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ACREEDOR")
  private TipoAcreedor tipoAcreedor;

  @Column(name = "NUM_RANGO")
  private Integer rango;
  @Column(name = "NUM_IMPORTE")
  private Double importe;
  @Column(name = "DES_NOMBRE_ACREEDOR")
  private String nombreAcreedor;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ANOTACION")
  private Date fechaAnotacion;
  @Column(name = "IND_INCL_OTROS_COLATERALES")
  private Boolean incluidaOtrosColaterales;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_VENCIMIENTO")
  private Date fechaVencimiento;
  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @Column(name = "IND_CARGA_PROPIA")
  private Boolean cargaPropia; //SI-NO

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_CARGA_FORMALIZACION")
  private TipoCargaFormalizacion tipoCargaFormalizacion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SUBTIPO_CARGA_FORMALIZACION")
  private SubtipoCargaFormalizacion subtipoCargaFormalizacion;

  @OneToOne(mappedBy = "carga")
  private FormalizacionCarga formalizacionCarga;

  @Override
  public boolean equals(Object o) {
    Carga carga = (Carga) o;
    Set<Bien> bienesCarga = carga.getBienes();
    for (Bien bien : this.bienes) {
      for (Bien bien2 : bienesCarga) {
        if (bien.getId().equals(bien2.getId())) {
          if (this.getTipoCarga() == null || this.getRango() == null) return false;
          if (this.getTipoCarga().getId().equals(carga.getTipoCarga().getId()) && this.getRango().equals(carga.getRango()))
            return true;
        }
      }
    }
    return false;
  }
}




