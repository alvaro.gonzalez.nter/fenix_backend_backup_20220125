package com.haya.alaska.carga.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class CargaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Loan Id");
      cabeceras.add("Warranty/Solvency Id");
      cabeceras.add("Freight Type");
      cabeceras.add("Freight Amount");
      cabeceras.add("Creditor Type");
      cabeceras.add("Creditor Name");
      cabeceras.add("Date Notation");
      cabeceras.add("Included in Other Collateral");
      cabeceras.add("Freight Expiration Date");
      cabeceras.add("Range");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Garantia/Solvencia");
      cabeceras.add("Tipo Carga");
      cabeceras.add("Importe Carga");
      cabeceras.add("Tipo Acreedor");
      cabeceras.add("Nombre Acreedor");
      cabeceras.add("Fecha Anotación");
      cabeceras.add("Incluida en Otros Colaterales");
      cabeceras.add("Fecha Vencimiento de la Carga");
      cabeceras.add("Rango");
    }
  }

  public CargaExcel(Carga carga, String inContrato, Integer idBien, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Connection Id", idExpediente);
      this.add("Loan Id", inContrato);
      this.add("Warranty/Solvency Id", idBien);
      this.add("Freight Type", carga.getId());
      this.add("Freight Amount", carga.getImporte());
      this.add(
          "Creditor Type",
          carga.getTipoAcreedor() != null ? carga.getTipoAcreedor().getValorIngles() : null);
      this.add("Creditor Name", carga.getNombreAcreedor());
      this.add("Date Notation", carga.getFechaAnotacion());
      this.add(
          "Included in Other Collateral",
          Boolean.TRUE.equals(carga.getIncluidaOtrosColaterales()) ? "YES" : "NO");
      this.add("Freight Expiration Date", carga.getFechaVencimiento());
      this.add("Range", carga.getRango());
    } else {
      this.add("Id Expediente", idExpediente);
      this.add("Id Contrato", inContrato);
      this.add("Id Garantia/Solvencia", idBien);
      this.add("Tipo Carga", carga.getId());
      this.add("Importe Carga", carga.getImporte());
      this.add(
          "Tipo Acreedor",
          carga.getTipoAcreedor() != null ? carga.getTipoAcreedor().getValor() : null);
      this.add("Nombre Acreedor", carga.getNombreAcreedor());
      this.add("Fecha Anotación", carga.getFechaAnotacion());
      this.add(
          "Incluida en Otros Colaterales",
          Boolean.TRUE.equals(carga.getIncluidaOtrosColaterales()) ? "SI" : "NO");
      this.add("Fecha Vencimiento de la Carga", carga.getFechaVencimiento());
      this.add("Rango", carga.getRango());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
