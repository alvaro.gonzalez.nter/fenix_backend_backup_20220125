package com.haya.alaska.forma_de_pago.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_LKUP_FORMA_DE_PAGO")
@NoArgsConstructor
@Entity
@Table(name = "LKUP_FORMA_DE_PAGO")
public class FormaDePago extends Catalogo{
}
