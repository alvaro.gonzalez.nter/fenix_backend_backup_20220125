package com.haya.alaska.forma_de_pago.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.forma_de_pago.domain.FormaDePago;
import org.springframework.stereotype.Repository;

@Repository
public interface FormaDePagoRepository extends CatalogoRepository<FormaDePago, Integer> {}
