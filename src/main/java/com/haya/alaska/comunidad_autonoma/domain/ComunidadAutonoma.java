package com.haya.alaska.comunidad_autonoma.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import com.haya.alaska.variable_ajd.domain.VariableAjd;
import com.haya.alaska.variable_itp.domain.VariableItp;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_COMUNIDAD_AUTONOMA")
@Entity
@Getter
@Table(name = "LKUP_COMUNIDAD_AUTONOMA")
public class ComunidadAutonoma extends Catalogo {

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_COMUNIDAD_AUTONOMA")
  @NotAudited
  private Set<Provincia> provincias = new HashSet<>();


  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_COMUNIDAD_AUTONOMA")
  @NotAudited
  private Set<VariableItp> variablesItp = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_COMUNIDAD_AUTONOMA")
  @NotAudited
  private Set<VariableAjd> variablesAjd = new HashSet<>();
}
