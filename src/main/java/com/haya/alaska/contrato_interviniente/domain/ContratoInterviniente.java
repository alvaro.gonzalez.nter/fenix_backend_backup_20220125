package com.haya.alaska.contrato_interviniente.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_RELA_CONTRATO_INTERVINIENTE")
@Entity
@Getter
@Setter
@Table(name = "RELA_CONTRATO_INTERVINIENTE")
public class ContratoInterviniente  implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private Contrato contrato;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_INTERVINIENTE")
  private Interviniente interviniente;

  @Column(name = "NUM_ORDEN_INTERVENCION")
  private Integer ordenIntervencion;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_INTERVENCION")
  private TipoIntervencion tipoIntervencion;

  //Campos de listado de skiptracing y de nivel con sus respectivas relaciones a Skiptracing y nivelskiptracing
  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO_INTERVINIENTE")
  @NotAudited
  private Set<SkipTracing> SkipTracing = new HashSet<>();


  public boolean isPrimerInterviniente() {
    if (this.getOrdenIntervencion() == null || this.getOrdenIntervencion() != 1) return false;
    //código correspondiente a TITULAR
    if (this.getTipoIntervencion()!=null && this.getTipoIntervencion().getCodigo().equals("1")){
      return true;
    }
    return false;
  }
}
