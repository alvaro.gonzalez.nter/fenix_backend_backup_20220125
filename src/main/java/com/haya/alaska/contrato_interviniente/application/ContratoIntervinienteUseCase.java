package com.haya.alaska.contrato_interviniente.application;

import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.shared.exceptions.NotFoundException;

public interface ContratoIntervinienteUseCase {

    ContratoInterviniente findByIntervinienteIdAndContratoIdAndOrdenIntervencionAndTipoIntervencionId(Integer intervinienteId, Integer contratoId, Integer ordenIntervencion, Integer tipoIntervencion) throws NotFoundException;

    ContratoInterviniente findByIntervinienteIdAndContratoId(Integer intervinienteId, Integer contratoId) throws NotFoundException;
}
