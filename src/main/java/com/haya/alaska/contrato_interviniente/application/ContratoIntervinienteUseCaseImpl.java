package com.haya.alaska.contrato_interviniente.application;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Locale;
import java.util.Optional;

@Component
public class ContratoIntervinienteUseCaseImpl implements ContratoIntervinienteUseCase {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private ContratoIntervinienteRepository contratoIntervinienteRepository;

    public ContratoInterviniente findByIntervinienteIdAndContratoIdAndOrdenIntervencionAndTipoIntervencionId(Integer intervinienteId, Integer contratoId, Integer ordenIntervencion, Integer tipoIntervencion) throws NotFoundException {
        Optional<ContratoInterviniente> contratoInterviniente = contratoIntervinienteRepository.findByIntervinienteIdAndContratoIdAndOrdenIntervencionAndTipoIntervencionId(intervinienteId, contratoId, ordenIntervencion, tipoIntervencion);
        if(contratoInterviniente.isEmpty()){
            Locale loc = LocaleContextHolder.getLocale();
            if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                throw new NotFoundException("There is no contract-intervening relationship with the following data: Intervening ID"
                        + intervinienteId + ", contract ID" + contratoId + ", intervention type ID" + tipoIntervencion + "and intervention order" + ordenIntervencion);
            else throw new NotFoundException("No existe la relación contrato-interviniente con los siguientes datos: ID interviniente "
                    + intervinienteId + ", ID contrato " + contratoId + ", ID tipo de intervención " + tipoIntervencion + " y orden de intervención " + ordenIntervencion);
        }
        return contratoInterviniente.get();
    }

    public ContratoInterviniente findByIntervinienteIdAndContratoId(Integer intervinienteId, Integer contratoId) throws NotFoundException{
        Optional<ContratoInterviniente> contratoInterviniente = contratoIntervinienteRepository.findByIntervinienteIdAndContratoId(intervinienteId, contratoId);
        if(contratoInterviniente.isEmpty()){
            Locale loc = LocaleContextHolder.getLocale();
            if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                throw new NotFoundException("There is no contract-intervening relationship with the following data: Intervening ID"
                        + intervinienteId + ", contract ID" + contratoId + ", intervention type ID");
            else throw new NotFoundException("No existe la relación contrato-interviniente con los siguientes datos: ID interviniente "
                    + intervinienteId + ", ID contrato " + contratoId + ", ID tipo de intervención");
        }
        return contratoInterviniente.get();
    }
}
