package com.haya.alaska.contrato_interviniente.infrastructure.repository;

import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.interviniente.domain.Interviniente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface ContratoIntervinienteRepository
    extends JpaRepository<ContratoInterviniente, Integer> {

  @Query(
    value =
      "SELECT " + //ContratoInterviniente
        "i.ID as id, " +
        "i.ID_TIPO_INTERVENCION as tipoIntervencion, " +
        "i.NUM_ORDEN_INTERVENCION as ordenIntervencion " +
        "FROM HIST_RELA_CONTRATO_INTERVINIENTE i JOIN REGISTRO_HISTORICO r ON i.REV = r.id " +
        "WHERE i.ID_INTERVINIENTE = ?1 AND i.ID_CONTRATO = ?2 AND r.ID_USUARIO IS NULL ORDER BY timestamp DESC LIMIT 1",
    nativeQuery = true)
  Optional<Map> findHistoricoById(Integer idInt, Integer idCon);

  Optional<ContratoInterviniente> findByContratoIdAndOrdenIntervencionAndTipoIntervencionValor(
      Integer idContrato, Integer ordenIntervencion, String valor);

  Optional<ContratoInterviniente> findByContratoIdAndOrdenIntervencionAndTipoIntervencionId(
          Integer idContrato, Integer ordenIntervencion, Integer idTipoIntervencion);

  /*Optional<ContratoInterviniente> findByContratoIdAndIntervinienteId(
          Integer idContrato, Integer idInterviniente);*/
  Optional<ContratoInterviniente> findByContratoIdAndIntervinienteIdAndTipoIntervencionId(
    Integer idContrato, Integer idInterviniente, Integer idTipoIntervencion);

  List<ContratoInterviniente> findAllByContratoIdAndIntervinienteId(
    Integer idContrato, Integer idInterviniente);

  List<ContratoInterviniente> findAllByIntervinienteIdIn(List<Integer> idIntervinientes);

  List<Interviniente> findByIntervinienteIdIn(List<ContratoInterviniente> intervinientes);

  Optional<ContratoInterviniente> findByIntervinienteIdAndContratoIdAndOrdenIntervencionAndTipoIntervencionId(Integer intervinienteId, Integer contratoId, Integer ordenIntervencion, Integer tipoIntervencion);

  Optional<ContratoInterviniente> findByIntervinienteIdAndContratoId(Integer intervinienteId, Integer contratoId);

  Optional<ContratoInterviniente> findByIntervinienteIdAndContratoIdAndTipoIntervencionId(Integer intervinienteId, Integer contratoId, Integer tipoIntervencion);

  Optional<ContratoInterviniente> findByContratoIdAndIntervinienteId(Integer idContrato, Integer intervinienteId);

  Page<ContratoInterviniente> findAllDistinctByContratoIdIn(Pageable pageable, List<Integer> contratosIds);
  int countDistinctByContratoIdIn(List<Integer> contratosIds);
  Page<ContratoInterviniente> findAllBy(Pageable page);

}
