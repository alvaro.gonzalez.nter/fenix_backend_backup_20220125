package com.haya.alaska.tipo_propuesta.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * TipoPropuesta entity
 *
 * @author agonzalez
 */
@Audited
@AuditTable(value = "HIST_LKUP_TIPO_PROPUESTA")
@NoArgsConstructor
@Entity
@Getter
@Table(name = "LKUP_TIPO_PROPUESTA")
public class TipoPropuesta extends Catalogo implements Serializable {

    @OneToMany(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_TIPO_PROPUESTA")
    @NotAudited
    private Set<SubtipoPropuesta> subtiposPropuesta = new HashSet<>();

   /* @Column(name = "MATRICULA")
     private String matricula;*/
}
