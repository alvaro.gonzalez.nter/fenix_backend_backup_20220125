package com.haya.alaska.tipo_propuesta.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TipoPropuestaRepository extends CatalogoRepository<TipoPropuesta, Integer> {
  Optional<TipoPropuesta> findByActivoIsTrueAndCodigo(String codigo);
  List<TipoPropuesta> findAllByActivoIsTrueAndCodigoNot(String codigo);
  List<TipoPropuesta> findAllDistinctByActivoIsTrueAndSubtiposPropuestaFormalizacionIsTrue();
  Optional<TipoPropuesta>findByIdAndActivoIsTrue(Integer id);
}
