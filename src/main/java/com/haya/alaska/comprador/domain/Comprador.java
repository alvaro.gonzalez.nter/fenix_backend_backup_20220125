package com.haya.alaska.comprador.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Audited
@AuditTable(value = "HIST_LKUP_COMPRADOR")
@Entity
@Table(name = "LKUP_COMPRADOR")
@NoArgsConstructor
@Getter
@Setter
public class Comprador extends Catalogo {
}
