package com.haya.alaska.comprador.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.comprador.domain.Comprador;
import org.springframework.stereotype.Repository;

@Repository
public interface CompradorRepository extends CatalogoRepository<Comprador, Integer> {
}
