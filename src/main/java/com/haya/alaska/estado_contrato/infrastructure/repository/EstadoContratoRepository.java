package com.haya.alaska.estado_contrato.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoContratoRepository extends CatalogoRepository<EstadoContrato, Integer> {}
