package com.haya.alaska.estado_contrato.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.contrato.domain.Contrato;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_ESTADO_CONTRATO")
@Entity
@Table(name = "LKUP_ESTADO_CONTRATO")
@Getter
public class EstadoContrato extends Catalogo {

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_CONTRATO")
  @NotAudited
  private Set<Contrato> contratos = new HashSet<>();
}
