package com.haya.alaska.tipo_busqueda.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_BUSQUEDA")
@Entity
@Table(name = "LKUP_TIPO_BUSQUEDA")
public class TipoBusqueda extends Catalogo {

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_BUSQUEDA")
  @NotAudited
  private Set<SkipTracing> skipTracings = new HashSet<>();
}
