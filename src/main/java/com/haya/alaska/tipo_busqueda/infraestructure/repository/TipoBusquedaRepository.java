package com.haya.alaska.tipo_busqueda.infraestructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_busqueda.domain.TipoBusqueda;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoBusquedaRepository extends CatalogoRepository<TipoBusqueda, Integer> {
}

