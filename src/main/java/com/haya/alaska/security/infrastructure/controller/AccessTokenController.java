package com.haya.alaska.security.infrastructure.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import com.haya.alaska.security.application.AccesTokenUseCase;
import com.haya.alaska.security.infrastructure.controller.dto.AccessTokenResponseDTO;
import com.haya.alaska.security.infrastructure.controller.dto.UsuarioResponseDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.security.util.TokenUtil;
import com.haya.alaska.shared.exceptions.ErrorResponse;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.application.UsuarioUseCase;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.List;

/** @author diegotobalina created on 19/08/2020 */
@RestController
@Slf4j
@RequestMapping("api/v1/oauth/access")
public class AccessTokenController {

  @Autowired private TokenUtil tokenUtil;
  @Autowired private AccesTokenUseCase accesTokenUseCase;
  @Autowired private UsuarioUseCase usuarioUseCase;

  @GetMapping
  public AccessTokenResponseDTO access(@RequestParam("code") String code)
    throws NoSuchAlgorithmException, KeyManagementException, UnirestException, KeyStoreException,
    JsonProcessingException, ForbiddenException {
    String accessToken = this.tokenUtil.getAccessToken(code);
    if (accessToken == null) {
      log.warn("ERROR no se ha podido conectar con ADFS para autenticar. REVISAR configuración de ADFS");
    } else {
      this.usuarioUseCase.actualizarUsuarioConAccessToken(this.tokenUtil.getAccessTokenInfo(accessToken), this.tokenUtil.getNombrePerfilAD());
    }
    return new AccessTokenResponseDTO(accessToken);
  }

  @GetMapping("/usuario")
  public UsuarioResponseDTO getUsuario(@ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    List<PermisoAsignado> permisoPerfiles = accesTokenUseCase.getPermisosUsuario(usuario);
    return new UsuarioResponseDTO(new UsuarioDTO(usuario), accesTokenUseCase.getValoresPermisos(permisoPerfiles, usuario));
  }

  @GetMapping("/usuario/{idCartera}")
  public UsuarioResponseDTO getUsuarioCartera(@ApiIgnore CustomUserDetails principal, @PathVariable("idCartera") Integer idCartera) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    List<PermisoAsignado> permisoPerfiles = accesTokenUseCase.getPermisosUsuarioCartera(usuario, idCartera);
    return new UsuarioResponseDTO(new UsuarioDTO(usuario), accesTokenUseCase.getValoresPermisos(permisoPerfiles, usuario));
  }

  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ExceptionHandler(ForbiddenException.class)
  public ErrorResponse handleForbiddenExceptions(ForbiddenException ex) {
    return ErrorResponse.builder()
      .status(HttpStatus.FORBIDDEN.value())
      .error(HttpStatus.FORBIDDEN.getReasonPhrase())
      .message(ex.getMessage())
      .timestamp(Instant.now().toString())
      .build();
  }
}

