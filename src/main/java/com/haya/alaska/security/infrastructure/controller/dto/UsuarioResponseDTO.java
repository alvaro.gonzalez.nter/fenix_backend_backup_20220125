package com.haya.alaska.security.infrastructure.controller.dto;

import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioResponseDTO {
  private UsuarioDTO usuarioDTO;
  private List<String> valoresPermisos;
}
