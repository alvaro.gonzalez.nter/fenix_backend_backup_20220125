package com.haya.alaska.security.infrastructure.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AccessTokenResponseDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String id_token;

  private String refresh_token;

  public AccessTokenResponseDTO(String accessToken) {
    this.id_token=accessToken;
  }
}
