package com.haya.alaska.security.application;

import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.security.util.TokenUtil;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;

@Slf4j
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private TokenUtil tokenUtil;

  @Autowired
  private UsuarioRepository usuarioRepository;

  @Value("${security.userdefault.email}")
  private String userDefaultEmail;

  /**
   * @param token Authorization header, will never be null or empty
   * @return CustomAuthentication if token verification goes ok, null if something fails
   */
  @Override
  public CustomUserDetails getByToken(String token) {
    try {
      boolean isValid = tokenUtil.verifyToken(token);
      if (!isValid) {
        if (userDefaultEmail == null || "".equals(userDefaultEmail)) {
          return null;
        }
        return createCustomUserDetails(userDefaultEmail);
      }
      HashMap<String, String> tokenInfo = tokenUtil.getAccessTokenInfo(token);
      String email = tokenInfo.get("email");
      String telefono = tokenInfo.get("telephoneNumber");
      return createCustomUserDetailsWithPhone(email, telefono);
    } catch (Exception ex) {
      log.warn(ex.getMessage());
    }
    return null;
  }

  private CustomUserDetails createCustomUserDetails(String email) throws Exception {
    Usuario usuario =
      usuarioRepository
        .findByEmail(email)
        .orElseThrow(() -> new Exception("usuario no encontrado con el email: " + email));
    return new CustomUserDetails(usuario, "", new ArrayList<>(), "", "");
  }
  
  private CustomUserDetails createCustomUserDetailsWithPhone(String email, String telefono) throws Exception {
      Usuario usuario =
	      usuarioRepository
	      .findByEmail(email)
	      .orElseThrow(() -> new Exception("usuario no encontrado con el email: " + email));
      usuario.setTelefono(telefono);
      return new CustomUserDetails(usuario, "", new ArrayList<>(), "", "");
  }
}
