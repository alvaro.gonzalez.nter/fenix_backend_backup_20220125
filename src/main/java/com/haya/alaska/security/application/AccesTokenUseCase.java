package com.haya.alaska.security.application;

import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.List;

public interface AccesTokenUseCase {
  List<String> getValoresPermisos(List<PermisoAsignado> permisoAsignados, Usuario usuario) throws NotFoundException;
  List<PermisoAsignado> getPermisosUsuario(Usuario usuario);
  List<PermisoAsignado> getPermisosUsuarioCartera(Usuario usuario, Integer idCartera);
}
