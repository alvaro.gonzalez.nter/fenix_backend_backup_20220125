package com.haya.alaska.security.application;

import java.util.List;

import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.haya.alaska.permiso_asignado.infrastructure.repository.PermisoAsignadoRepository;

@Component
public class SecurityUseCaseImpl implements SecurityUseCase {

  @Autowired
  PermisoAsignadoRepository permisoAsignadoRepository;

  @Override
  public List<PermisoAsignado> verPermisosUsuario(Integer idPerfil) {
    return permisoAsignadoRepository.findByPerfilIdAndCarteraIsNull(idPerfil);
  }

  @Override
  public List<PermisoAsignado> verTodosPermisosUsuario(Integer idPerfil) {
    return permisoAsignadoRepository.findAllByPerfilId(idPerfil);
  }

  @Override
  public List<PermisoAsignado> verPermisosUsuarioCartera(Integer idPerfil, Integer idCartera) {
    return permisoAsignadoRepository.findByPerfilIdAndCarteraId(idPerfil, idCartera);
  }
}
