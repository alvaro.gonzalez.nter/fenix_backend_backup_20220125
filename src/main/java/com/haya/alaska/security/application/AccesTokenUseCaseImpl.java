package com.haya.alaska.security.application;

import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.permiso.domain.Permiso;
import com.haya.alaska.permiso.infrastructure.repository.PermisoRepository;
import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import com.haya.alaska.permiso_disponible.domain.PermisoDisponible;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AccesTokenUseCaseImpl implements AccesTokenUseCase {
  @Autowired
  private SecurityUseCase securityUseCase;

  @Autowired
  private PermisoRepository permisoRepository;

  /*
   * Para el permiso de acceso a Dashboard, se enviará únicamente el valor "dash" no irá seguida de ninguna opción de permiso,
   * ya que los dashboards son sólo de lectura y las opciones que hay son el acceso a las distintas pestañas del dashboard
   * */
  public List<String> getValoresPermisos(List<PermisoAsignado> permisoAsignados, Usuario usuario) throws NotFoundException {
    if (permisoAsignados == null) return new ArrayList<>();
    List<String> response = new ArrayList<>();
    List<PermisoAsignado> permisosDashboard = checkPermisosDashboard(usuario);
    if(permisosDashboard != null && permisosDashboard.size() > 0){
      Permiso permisoDashboardGeneral = permisoRepository.findByCodigo("dash").orElseThrow(()-> new NotFoundException("permiso", "código", "'dash'"));
      String codigo = permisoDashboardGeneral.getCodigo();
      response.add(codigo);
      permisoAsignados.addAll(permisosDashboard);
    }
    for (PermisoAsignado permisoAsignado : permisoAsignados) {
      PermisoDisponible pd = permisoAsignado.getPermisoDisponible();
      String codigo = pd.getPermiso().getCodigo() + "." + pd.getOpcionPermiso().getCodigo();
      response.add(codigo);
    }
    return response;
  }

  private List<PermisoAsignado> checkPermisosDashboard(Usuario usuario){
    Perfil perfil = usuario.getPerfil();
    List<PermisoAsignado> permisosUsuario = null;
    if (perfil == null) return null;
    permisosUsuario = securityUseCase.verTodosPermisosUsuario(perfil.getId());
    List<PermisoAsignado> result = new ArrayList<>();
    if(permisosUsuario != null && permisosUsuario.size() > 0){
      for(PermisoAsignado p: permisosUsuario){
        if(p.getPermisoDisponible().getPermiso().getCodigo().equals("dash")) result.add(p);
      }
    }
    return result;
  }

  public List<PermisoAsignado> getPermisosUsuario(Usuario usuario) {
    if (usuario.esAdministrador() ) {
      return this.getAllPermisosAsDisponibles();
    }
    Perfil perfil = usuario.getPerfil();
    if (perfil == null) return new ArrayList<>();
    return securityUseCase.verPermisosUsuario(perfil.getId());
  }

  public List<PermisoAsignado> getPermisosUsuarioCartera(Usuario usuario, Integer idCartera) {
    if (usuario.esAdministrador() ) {
      return this.getAllPermisosAsDisponibles();
    }
    Perfil perfil = usuario.getPerfil();
    if (perfil == null) return new ArrayList<>();
    return securityUseCase.verPermisosUsuarioCartera(perfil.getId(), idCartera);
  }

  private List<PermisoAsignado> getAllPermisosAsDisponibles() {
    List<Permiso> permisos = this.permisoRepository.findAll();
    List<PermisoAsignado> asignados = new ArrayList<>();
    for (Permiso permiso : permisos) {
      for (PermisoDisponible permisoDisponible : permiso.getPermisosDisponibles()) {
        PermisoAsignado asignado = new PermisoAsignado();
        asignado.setPermisoDisponible(permisoDisponible);
        asignados.add(asignado);
      }
    }
    return asignados;
  }
}
