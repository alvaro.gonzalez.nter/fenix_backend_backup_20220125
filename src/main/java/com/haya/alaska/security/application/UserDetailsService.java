package com.haya.alaska.security.application;

import com.haya.alaska.security.util.CustomUserDetails;

/** @author diegotobalina created on 24/06/2020 */
public interface UserDetailsService {

  CustomUserDetails getByToken(String token);
}
