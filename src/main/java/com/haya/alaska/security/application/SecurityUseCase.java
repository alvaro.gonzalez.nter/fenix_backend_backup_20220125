package com.haya.alaska.security.application;

import java.util.List;

import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;


public interface SecurityUseCase {

  List<PermisoAsignado> verPermisosUsuario(Integer idPerfil);
  List<PermisoAsignado> verPermisosUsuarioCartera(Integer idPerfil, Integer idCartera);
  List<PermisoAsignado> verTodosPermisosUsuario(Integer idPerfil);
}
