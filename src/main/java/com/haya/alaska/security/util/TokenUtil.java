package com.haya.alaska.security.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import java.io.ByteArrayInputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * @author diegotobalina created on 08/07/2020
 */

@Slf4j
@Component
public class TokenUtil {
    @Value("${adfs.host}")
    private String host;

    @Value("${adfs.client_secret}")
    private String clientSecret;

    @Value("${adfs.client_id}")
    private String clientId;

    @Getter
    @Value("${adfs.nombre_perfil}")
    private String nombrePerfilAD;

    @Value("${adfs.redirect_url}")
    private String redirectUrl;

    @Value("${adfs.certificate}")
    private String certificateString;

    public String getAccessToken(String code)
            throws UnirestException, KeyStoreException, NoSuchAlgorithmException, JsonProcessingException,
            KeyManagementException {
      try {
        SSLContext sslContext =
          new SSLContextBuilder()
            .loadTrustMaterial(null, new TrustSelfSignedStrategy() {
              public boolean isTrusted(X509Certificate[] chain, String authType) {
                return true;
              }
            })
            .build();
        HttpClient customHttpClient =
          HttpClients.custom()
            .setSSLContext(sslContext)
            .setSSLHostnameVerifier(new NoopHostnameVerifier())
            .build();
        Unirest.setHttpClient(customHttpClient);
        HttpResponse<String> response =
          Unirest.post("https://" + host + "/adfs/oauth2/token")
            .header("User-Agent", "Windows-AzureAD-Authentication-Provider")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .field("grant_type", "authorization_code")
            .field("client_id", clientId)
            .field("redirect_uri", redirectUrl)
            .field("code", code)
            .field("client_secret", clientSecret)
            .asString();
        if (response.getStatus() != HttpStatus.SC_OK) {
          log.warn("ERROR respuesta adfs (código {}): {}", response.getStatus(), response.getBody());
        }
        Map<String, String> result = new ObjectMapper().readValue(response.getBody(), HashMap.class);
        return result.get("access_token");
      }catch (Exception ex) {
        log.warn("ERROR no se ha podido conectar con ADFS para autenticar. REVISAR configuración de ADFS", ex);
        throw ex;
      }
    }

    public HashMap<String, String> getAccessTokenInfo(String access_token) throws JsonProcessingException {
        String middle = access_token.split("\\.")[1]; // to get payload info
        byte[] decodedBytes = Base64.getDecoder().decode(middle);
        String decodedString = new String(decodedBytes);
        return new ObjectMapper().readValue(decodedString, HashMap.class);
    }

    public boolean verifyToken(String token) {
        try {
            X509Certificate cert = getX509Cert();
            PublicKey publicKey = cert.getPublicKey();
            Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) publicKey, null);
            JWTVerifier verifier =
                    JWT.require(algorithm)
                            // more validations if needed
                            .build();
            verifier.verify(token);
            return true;
        } catch (JWTVerificationException e) {
            if (e.getMessage().startsWith("The Token can't be used before")) {
                return true;
            }
            log.warn("Exception in verifying " + e.toString());
            return false;
        } catch (Exception e) {
            log.warn("ERROR no se ha podido verificar el certificado para el token: " + token, e);
            return false;
        }
    }

    private X509Certificate getX509Cert() throws CertificateException {
        try {
            certificateString =
                    certificateString
                            .replace("-----BEGIN CERTIFICATE-----\n", "")
                            .replace("-----END CERTIFICATE-----", ""); // NEED FOR PEM FORMAT CERT STRING
            byte[] certificateData = Base64.getMimeDecoder().decode(certificateString);
          CertificateFactory cf = CertificateFactory.getInstance("X509");
            return (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(certificateData));
        } catch (CertificateException e) {
            throw new CertificateException(e);
        }
    }
}
