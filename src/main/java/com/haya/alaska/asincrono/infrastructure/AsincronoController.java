package com.haya.alaska.asincrono.infrastructure;

import com.haya.alaska.asincrono.application.AsincronoUseCase;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/asincrono")
public class AsincronoController {

  @Lazy
  @Autowired
  AsincronoUseCase asincronoUseCase;

  @Lazy
  @ApiOperation(
    value = "Modificar los campos (catalogos y otros) de expediente y contrato a partir del excel")
  @PostMapping("/modificarCatalogos")
  public ResponseEntity modificarPorExcel(@RequestParam("file") MultipartFile file,
                                          @ApiIgnore CustomUserDetails principal,
                                          @RequestParam("pn") Boolean pn) throws Exception {
    if (file.getOriginalFilename().endsWith(".xlsx")) {
      file.transferTo(new File("src/main/java/com/haya/alaska/asincrono/infrastructure/archivo.xlsx"));
      asincronoUseCase.modificarPorExcel((Usuario) principal.getPrincipal(), pn);

    }
      return ResponseEntity.ok().build();
  }


  @ApiOperation(value = "Obtener un excel de catalogos")
  @PostMapping("/excelCatalogos")
  public ResponseEntity<InputStreamResource> crearExcel(
    @ApiIgnore CustomUserDetails principal
  ) throws NotFoundException, IOException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(asincronoUseCase.generarExcelCatalogos(usuario));
    return excelExport.download(excel, "Catalogos" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }
}
