package com.haya.alaska.asincrono.application;

import com.haya.alaska.asignacion_expediente.application.AsignacionExpedienteUseCase;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.busqueda.application.BusquedaUseCaseImpl;
import com.haya.alaska.busqueda.infrastructure.repository.BusquedaRecienteRepository;
import com.haya.alaska.campania.domain.Campania;
import com.haya.alaska.campania.infraestructure.repository.CampaniaRepository;
import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.campania_asignada.infraestructure.repository.CampaniaAsignadaRepository;
import com.haya.alaska.campania_asignada_pn.domain.CampaniaAsignadaBienPN;
import com.haya.alaska.campania_asignada_pn.infraestructure.repository.CampaniaAsignadaBienPNRepository;
import com.haya.alaska.catalogo.domain.CatalogoDobleExcel;
import com.haya.alaska.catalogo.domain.CatalogoExcel;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.estado_contrato.infrastructure.repository.EstadoContratoRepository;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.estrategia_asignada.infrastructure.repository.EstrategiaAsignadaRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.ExpedienteCatalogoExcel;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCase;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.origen_estrategia.infrastructure.repository.OrigenEstrategiaRepository;
import com.haya.alaska.periodo_estrategia.infrastructure.repository.PeriodoEstrategiaRepository;
import com.haya.alaska.shared.exceptions.InterruptedException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.situacion.domain.Situacion;
import com.haya.alaska.situacion.infrastructure.repository.SituacionRepository;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.subtipo_estado.infrastructure.repository.SubtipoEstadoRepository;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.tipo_estado.infrastructure.repository.TipoEstadoRepository;
import com.haya.alaska.tipo_estrategia.domain.TipoEstrategia;
import com.haya.alaska.tipo_estrategia.infrastructure.repository.TipoEstrategiaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.UsuarioExcel;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.haya.alaska.utilidades.comunicaciones.application.ComunicacionesUseCase;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.ContactAttributes;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SendEmailDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SubscriberAttributes;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Component
@Slf4j
public class AsincronoUseCaseImpl implements AsincronoUseCase{

  @Autowired
  BusquedaUseCaseImpl busquedaUseCaseImpl;
  @Autowired
  BusquedaRecienteRepository busquedaRecienteRepository;
  @Autowired
  ExpedienteMapper expedienteMapper;
  @Autowired
  ExpedienteRepository expedienteRepository;
  @Autowired
  ContratoRepository contratoRepository;
  @Autowired
  UsuarioRepository usuarioRepository;
  @Autowired
  EstadoContratoRepository estadoContratoRepository;
  @Autowired
  EstrategiaRepository estrategiaRepository;
  @Autowired
  TipoEstrategiaRepository tipoEstrategiaRepository;
  @Autowired
  TipoEstadoRepository tipoEstadoRepository;
  @Autowired
  SubtipoEstadoRepository subtipoEstadoRepository;
  @Autowired
  SituacionRepository situacionRepository;
  @Autowired
  CampaniaRepository campaniaRepository;
  @Autowired
  AsignacionExpedienteUseCase asignacionExpedienteUseCase;
  @Autowired
  OrigenEstrategiaRepository origenEstrategiaRepository;
  @Autowired
  EstrategiaAsignadaRepository estrategiaAsignadaRepository;
  @Autowired
  PeriodoEstrategiaRepository periodoEstrategiaRepository;
  @Autowired
  CampaniaAsignadaRepository campaniaAsignadaRepository;
  @Autowired
  CampaniaAsignadaBienPNRepository campaniaAsignadaBienPNRepository;
  @Autowired
  ComunicacionesUseCase comunicacionesUseCase;
  @Autowired
  ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  ExpedientePosesionNegociadaCase expedientePosesionNegociadaCase;

  @Async
  @Override
  public void modificarPorExcel(Usuario usuario, Boolean pn) throws Exception {

    String path = "src/main/java/com/haya/alaska/asincrono/infrastructure/archivo.xlsx";
    File file = new File(path);

    XSSFWorkbook workbook = new XSSFWorkbook(file);
    XSSFSheet worksheet = workbook.getSheetAt(0);
    if(Boolean.TRUE.equals(pn)) {
      for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
        XSSFRow row = worksheet.getRow(i);
        DataFormatter formatter = new DataFormatter();
        ExpedientePosesionNegociada expediente = null;
        BienPosesionNegociada bienPosesionNegociada = null;
        if (row.getCell(0) != null) {
          expediente = expedientePosesionNegociadaRepository.findByIdConcatenado(formatter.formatCellValue(row.getCell(0))).orElse(null);
          if (expediente != null) {
            //coger los catalogos de expediente y setearselos

            //responsable de cartera
            if (row.getCell(3) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(3))).orElse(null);
              if (usuarioSacado != null) {
                try {
                  asignacionExpedienteUseCase.modificarGestor(null, List.of(expediente.getId()), usuarioSacado.getId(), false, false, false, null, usuario);
                } catch(Exception ex) {
                  ex.printStackTrace();
                }

              }
            }

            //supervisor
             if (row.getCell(4) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(4))).orElse(null);
              if (usuarioSacado != null){
                try {
                  asignacionExpedienteUseCase.modificarGestor(null, List.of(expediente.getId()), usuarioSacado.getId(), false, false, false, null, usuario);
                } catch(Exception e) {
                  e.printStackTrace();
                  }
                }
              }


            //gestor del expediente
            if (row.getCell(5) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(5))).orElse(null);
              if (usuarioSacado != null) {
                try {
                  asignacionExpedienteUseCase.modificarGestor(null, List.of(expediente.getId()), usuarioSacado.getId(), false, false, false, null, usuario);
                } catch (Exception e) {
                  e.printStackTrace();
                }
              }
            }

            //gestor de formalizacion
            if (row.getCell(6) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(6))).orElse(null);
              if (usuarioSacado != null) {
                try {
                  asignacionExpedienteUseCase.modificarGestor(null, List.of(expediente.getId()), usuarioSacado.getId(), false, false, false, null, usuario);
                } catch(Exception e) {
                  e.printStackTrace();
                }
              }
            }

            //gestor de skip tracing
            if (row.getCell(7) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(7))).orElse(null);
              if (usuarioSacado != null) {
                try {
                  asignacionExpedienteUseCase.modificarGestor(null, List.of(expediente.getId()), usuarioSacado.getId(), false, false, false, null, usuario);

                } catch (Exception e) {
                  e.printStackTrace();
                }
              }
            }

            //perfil soporte
            if (row.getCell(8) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(8))).orElse(null);
              if (usuarioSacado != null) {
                try {
                  asignacionExpedienteUseCase.modificarGestor(null, List.of(expediente.getId()), usuarioSacado.getId(), false, false, false, null, usuario);
                } catch (Exception e) {
                  e.printStackTrace();
                }
              }
            }

            expediente=expedientePosesionNegociadaRepository.findById(expediente.getId()).orElse(null);
            if(expediente == null) continue;

            // tipo estado (modo gestion estado)
            if (row.getCell(11) != null) {
              TipoEstado tipoEstado = tipoEstadoRepository.findByCodigo(formatter.formatCellValue(row.getCell(11))).orElse(null);
              if (tipoEstado != null)
                expediente.setTipoEstado(tipoEstado);
            }

            //subtipo estado (sub estado modo gestion)
            if (row.getCell(12) != null) {
              SubtipoEstado subtipoEstado = subtipoEstadoRepository.findByCodigo(formatter.formatCellValue(row.getCell(12))).orElse(null);
              if (subtipoEstado != null)
                expediente.setSubEstado(subtipoEstado);
            }

            expediente = expedientePosesionNegociadaRepository.save(expediente);

            //coger el contrato si no es nulo y hacer lo mismo que con el expediente
            if (row.getCell(1) != null) {
              String idBienPN = formatter.formatCellValue(row.getCell(1)).replaceFirst("^0+(?!$)", "");
              bienPosesionNegociada = bienPosesionNegociadaRepository.findByIdHaya(idBienPN).orElse(null);
              if (bienPosesionNegociada != null) {

                //estrategia
                if (row.getCell(9) != null) {
                  Estrategia estrategia = estrategiaRepository.findByCodigo(formatter.formatCellValue(row.getCell(9))).orElse(null);
                  if (estrategia != null) {
                    /*List<Integer> idBienes = new ArrayList<>();
                    for(var bienTemp: expediente.getBienesPosesionNegociada()) {
                      idBienes.add(bienTemp.getId());
                    }*/
                    expedientePosesionNegociadaCase.masiveEstrategiaUpdate(estrategia.getId(),null,List.of(expediente.getId()),null,List.of(bienPosesionNegociada.getBien().getId()),false,false,usuario,null);
                    bienPosesionNegociada = bienPosesionNegociadaRepository.findById(bienPosesionNegociada.getId()).orElse(null);
                    if(bienPosesionNegociada == null) continue;
                    /*var ea2 = new EstrategiaAsignada();
                    ea2.setOrigenEstrategia(origenEstrategiaRepository.findByCodigo("GM").orElse(null));
                    ea2.setTipoEstrategia(tipoEstrategiaRepository.findByCodigo("M").orElse(null));
                    ea2.setPeriodoEstrategia(periodoEstrategiaRepository.findByCodigo("M").orElse(null));
                    ea2.setFecha(new Date());
                    ea2.setEstrategia(estrategia);
                    bienPosesionNegociada.setEstrategia(estrategiaAsignadaRepository.save(ea2));*/
                  }
                }

                //tipo de estrategia
                if (row.getCell(10) != null && bienPosesionNegociada.getEstrategia() != null) {
                  TipoEstrategia tipoEstrategia = tipoEstrategiaRepository.findByCodigo(formatter.formatCellValue(row.getCell(10))).orElse(null);
                  if (tipoEstrategia != null) {
                    var ea2 =bienPosesionNegociada.getEstrategia();
                    ea2.setTipoEstrategia(tipoEstrategia);
                    bienPosesionNegociada.setEstrategia(estrategiaAsignadaRepository.save(ea2));
                  }
                }

                //situacion
                if (row.getCell(13) != null) {
                  Situacion situacion = situacionRepository.findByCodigo(formatter.formatCellValue(row.getCell(13))).orElse(null);
                  if (situacion != null)
                    bienPosesionNegociada.setSituacion(situacion);
                }


                //campaña (añadir a las campañas asignadas?)
                if (row.getCell(14) != null) {
                  try {
                    Campania campania = campaniaRepository.findById(Integer.valueOf(formatter.formatCellValue(row.getCell(14)))).orElse(null);
                    if (campania != null) {
                      Set<CampaniaAsignadaBienPN> campaniasAsignadas = bienPosesionNegociada.getCampaniasAsignadasPN();
                      CampaniaAsignadaBienPN campaniaAsignada = new CampaniaAsignadaBienPN();
                      campaniaAsignada.setCampania(campania);
                      campaniaAsignada.setBienPosesionNegociada(bienPosesionNegociada);
                      campaniaAsignada.setFechaAsignacion(new Date());
                      campaniaAsignadaBienPNRepository.save(campaniaAsignada);

                      Set<CampaniaAsignadaBienPN> campaniasAsignadasCampania = campania.getCampaniasAsignadasPN();
                      campaniasAsignadasCampania.add(campaniaAsignada);

                      campania.setCampaniasAsignadasPN(campaniasAsignadasCampania);
                      bienPosesionNegociada.setCampaniasAsignadasPN(campaniasAsignadas);
                      //contratoRepository.save(contrato);
                      campaniaRepository.save(campania);
                    }
                  }catch(Exception e) {
                    e.printStackTrace();
                  }

                }
              }
              bienPosesionNegociadaRepository.save(bienPosesionNegociada);
            }
          }
        }
      }
      enviarEmail(usuario);

    } else if(Boolean.FALSE.equals(pn)) {
      for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
        XSSFRow row = worksheet.getRow(i);
        DataFormatter formatter = new DataFormatter();
        Expediente expediente = null;
        Contrato contrato = null;
        if (row.getCell(0) != null) {
          expediente = expedienteRepository.findByIdConcatenado(formatter.formatCellValue(row.getCell(0))).orElse(null);
          if (expediente != null) {
            //coger los catalogos de expediente y setearselos

            //responsable de cartera
            if (row.getCell(3) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(3))).orElse(null);
              if (usuarioSacado != null) {
                try{
                  asignacionExpedienteUseCase.modificarGestor(List.of(expediente.getId()), null, usuarioSacado.getId(), false, false, false, null, usuario);

                }catch(Exception e) {
                  e.printStackTrace();
                }
              }
            }

            //supervisor
            if (row.getCell(4) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(4))).orElse(null);
              if (usuarioSacado != null) {
                try {
                  asignacionExpedienteUseCase.modificarGestor(List.of(expediente.getId()), null, usuarioSacado.getId(), false, false, false, null, usuario);
                }catch(Exception e) {
                  e.printStackTrace();
                }
              }
            }

            //gestor del expediente
            if (row.getCell(5) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(5))).orElse(null);
              if (usuarioSacado != null) {
                try {
                  asignacionExpedienteUseCase.modificarGestor(List.of(expediente.getId()), null, usuarioSacado.getId(), false, false, false, null, usuario);
                } catch(Exception e) {
                  e.printStackTrace();
                }
              }
            }

            //gestor de formalizacion
            if (row.getCell(6) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(6))).orElse(null);
              if (usuarioSacado != null) {
                try {
                  asignacionExpedienteUseCase.modificarGestor(List.of(expediente.getId()), null, usuarioSacado.getId(), false, false, false, null, usuario);
                } catch(Exception e) {
                  e.printStackTrace();
                }
              }
            }

            //gestor de skip tracing
            if (row.getCell(7) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(7))).orElse(null);
              if (usuarioSacado != null) {
                try {
                  asignacionExpedienteUseCase.modificarGestor(List.of(expediente.getId()), null, usuarioSacado.getId(), false, false, false, null, usuario);
                }catch(Exception e) {
                  e.printStackTrace();
                }
              }
            }

            //perfil soporte
            if (row.getCell(8) != null) {
              Usuario usuarioSacado = usuarioRepository.findByEmail(formatter.formatCellValue(row.getCell(8))).orElse(null);
              if (usuarioSacado != null) {
                try {
                  asignacionExpedienteUseCase.modificarGestor(List.of(expediente.getId()), null, usuarioSacado.getId(), false, false, false, null, usuario);

                } catch(Exception e) {
                  e.printStackTrace();
                }
              }
            }
            expediente=expedienteRepository.findById(expediente.getId()).orElse(null);
            if(expediente == null) continue;

            // tipo estado (modo gestion estado)
            if (row.getCell(11) != null) {
              TipoEstado tipoEstado = tipoEstadoRepository.findByCodigo(formatter.formatCellValue(row.getCell(11))).orElse(null);
              if (tipoEstado != null)
                expediente.setTipoEstado(tipoEstado);
            }

            //subtipo estado (sub estado modo gestion)
            if (row.getCell(12) != null) {
              SubtipoEstado subtipoEstado = subtipoEstadoRepository.findByCodigo(formatter.formatCellValue(row.getCell(12))).orElse(null);
              if (subtipoEstado != null)
                expediente.setSubEstado(subtipoEstado);
            }

            expediente = expedienteRepository.save(expediente);

            //coger el contrato si no es nulo y hacer lo mismo que con el expediente
            if (row.getCell(1) != null) {
              contrato = contratoRepository.findByIdCarga(formatter.formatCellValue(row.getCell(1))).orElse(null);
              if (contrato != null) {
                //estrategia
                if (row.getCell(9) != null) {
                  Estrategia estrategia = estrategiaRepository.findByCodigo(formatter.formatCellValue(row.getCell(9))).orElse(null);
                  if (estrategia != null) {
                    /*List<Integer> idContratos = new ArrayList<>();
                    for(var contratoTemp: expediente.getContratos()) {
                      idContratos.add(contratoTemp.getId());
                    }*/
                    expedientePosesionNegociadaCase.masiveEstrategiaUpdate(estrategia.getId(),List.of(expediente.getId()),null,List.of(contrato.getId()),null,false,false,usuario,null);
                    contrato = contratoRepository.findById(contrato.getId()).orElse(null);
                    if(contrato == null) continue;
                    /*var ea2 = new EstrategiaAsignada();
                    ea2.setOrigenEstrategia(origenEstrategiaRepository.findByCodigo("GM").orElse(null));
                    ea2.setTipoEstrategia(tipoEstrategiaRepository.findByCodigo("M").orElse(null));
                    ea2.setPeriodoEstrategia(periodoEstrategiaRepository.findByCodigo("M").orElse(null));
                    ea2.setFecha(new Date());
                    ea2.setEstrategia(estrategia);
                    contrato.setEstrategia(estrategiaAsignadaRepository.save(ea2));*/
                  }
                }

                //estado contrato
                if (row.getCell(2) != null) {
                  EstadoContrato estadoContrato = estadoContratoRepository.findByCodigo(formatter.formatCellValue(row.getCell(2))).orElse(null);
                  if (estadoContrato != null)
                    contrato.setEstadoContrato(estadoContrato);
                }


                //tipo de estrategia
                if (row.getCell(10) != null && contrato.getEstrategia() != null) {
                  TipoEstrategia tipoEstrategia = tipoEstrategiaRepository.findByCodigo(formatter.formatCellValue(row.getCell(10))).orElse(null);
                  if (tipoEstrategia != null) {
                    var ea2 = contrato.getEstrategia();
                    ea2.setTipoEstrategia(tipoEstrategia);
                    contrato.setEstrategia(estrategiaAsignadaRepository.save(ea2));
                  }
                }

                //situacion
                if (row.getCell(13) != null) {
                  Situacion situacion = situacionRepository.findByCodigo(formatter.formatCellValue(row.getCell(13))).orElse(null);
                  if (situacion != null)
                    contrato.setSituacion(situacion);
                }


                //campaña (añadir a las campañas asignadas?)
                if (row.getCell(14) != null) {
                  try {
                    Campania campania = campaniaRepository.findById(Integer.valueOf(formatter.formatCellValue(row.getCell(14)))).orElse(null);
                    if (campania != null) {
                      Set<CampaniaAsignada> campaniasAsignadas = contrato.getCampaniasAsignadas();
                      CampaniaAsignada campaniaAsignada = new CampaniaAsignada();
                      campaniaAsignada.setCampania(campania);
                      campaniaAsignada.setContrato(contrato);
                      campaniaAsignada.setFechaAsignacion(new Date());
                      campaniaAsignadaRepository.save(campaniaAsignada);

                      Set<CampaniaAsignada> campaniasAsignadasCampania = campania.getCampaniasAsignadas();
                      campaniasAsignadasCampania.add(campaniaAsignada);

                      campania.setCampaniasAsignadas(campaniasAsignadasCampania);
                      contrato.setCampaniasAsignadas(campaniasAsignadas);
                      //contratoRepository.save(contrato);
                      campaniaRepository.save(campania);
                    }
                  }catch(Exception e) {
                    e.printStackTrace();
                  }

                }
              }
              contratoRepository.save(contrato);
            }
          }
        }
      }
      enviarEmail(usuario);
    }
  }

  private void enviarEmail(Usuario usuario) throws InterruptedException, RequiredValueException, NotFoundException, IOException, java.lang.InterruptedException {
    SendEmailDTO datosEmail = new SendEmailDTO();
    datosEmail.setAddress(usuario.getEmail());
    datosEmail.setSubscriberKey(usuario.getId());
    ContactAttributes contactAttributes = new ContactAttributes();
    SubscriberAttributes subscriberAttributes = new SubscriberAttributes();
    subscriberAttributes.setNombre("");
    subscriberAttributes.setApellido("");
    subscriberAttributes.setCliente("");
    subscriberAttributes.setIdExpediente("");
    subscriberAttributes.setCampana("");
    subscriberAttributes.setContrato("");
    subscriberAttributes.setCorreoGestor("");
    subscriberAttributes.setEmailBody("");
    subscriberAttributes.setIdInterviniente(0);
    subscriberAttributes.setPortalCliente("");
    subscriberAttributes.setSubject("Carga Correcta");
    subscriberAttributes.setTextoEmail("La carga de información ha finalizado correctamente");
    contactAttributes.setSubscriberAttributes(subscriberAttributes);
    datosEmail.setContactAttributes(contactAttributes);
    comunicacionesUseCase.envioEmailAsincrono("85200", datosEmail, usuario);
  }


  @Override
  public LinkedHashMap<String, List<Collection<?>>> generarExcelCatalogos(Usuario usuario)
    throws NotFoundException {
    var datos = this.createExpedienteDataForExcel();
    this.addExpedienteDataForExcel(datos);
    return datos;
  }

  private LinkedHashMap<String, List<Collection<?>>> createExpedienteDataForExcel() {
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expedientes = new ArrayList<>();
    expedientes.add(ExpedienteCatalogoExcel.cabeceras);

    //estado contrato
    List<Collection<?>> estadoContrato = new ArrayList<>();
    estadoContrato.add(CatalogoExcel.cabeceras);

    //responsable cartera
    List<Collection<?>> responsableCartera = new ArrayList<>();
    responsableCartera.add(UsuarioExcel.cabeceras);

    //supervisor
    List<Collection<?>> supervisor = new ArrayList<>();
    supervisor.add(UsuarioExcel.cabeceras);

    //gestor de expediente
    List<Collection<?>> gestorExpediente = new ArrayList<>();
    gestorExpediente.add(UsuarioExcel.cabeceras);

    //gestor de formalizacion
    List<Collection<?>> gestorFormalizacion = new ArrayList<>();
    gestorFormalizacion.add(UsuarioExcel.cabeceras);

    //gestor de skip tracing
    List<Collection<?>> gestorSkipTracing = new ArrayList<>();
    gestorSkipTracing.add(UsuarioExcel.cabeceras);

    //perfil soporte
    List<Collection<?>> perfilSoporte = new ArrayList<>();
    perfilSoporte.add(UsuarioExcel.cabeceras);

    //estrategia
    List<Collection<?>> estrategia = new ArrayList<>();
    estrategia.add(CatalogoExcel.cabeceras);

    //tipo estrategia
    List<Collection<?>> tipoEstrategia = new ArrayList<>();
    tipoEstrategia.add(CatalogoExcel.cabeceras);

    /*//estado modo gestion -> tipo estado
    List<Collection<?>> tipoEstado = new ArrayList<>();
    tipoEstado.add(CatalogoExcel.cabeceras);

    //sub-estado modo gestion -> subtipo estado
    List<Collection<?>> subtipoEstado = new ArrayList<>();
    subtipoEstado.add(CatalogoExcel.cabeceras);*/

    //tipo estado y subtipo estado
    List<Collection<?>> tipoSubtipoEstado = new ArrayList<>();
    tipoSubtipoEstado.add(CatalogoDobleExcel.cabeceras);

    //situacion
    List<Collection<?>> situacion = new ArrayList<>();
    situacion.add(CatalogoExcel.cabeceras);

    //campaña contrato
    List<Collection<?>> campania = new ArrayList<>();
    campania.add(CatalogoExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      datos.put("Connection", expedientes);
      datos.put("Contract situation", estadoContrato);
      datos.put("Portfolio manager", responsableCartera);
      datos.put("Supervisor", supervisor);
      datos.put("Connection manager", gestorExpediente);
      datos.put("Formalization Manager", gestorFormalizacion);
      datos.put("Skip Tracing Manager", gestorSkipTracing);
      datos.put("Support Profile", perfilSoporte);
      datos.put("Strategy", estrategia);
      datos.put("Strategy type", tipoEstrategia);
      //datos.put("Situation type", tipoEstado);
      //datos.put("Situation subtype", subtipoEstado);
      datos.put("Situation Type-Subtype", tipoSubtipoEstado);
      datos.put("Situation", situacion);
      datos.put("Campaign", campania);

    } else {
      datos.put("Expediente", expedientes);
      datos.put("Estado contrato",estadoContrato);
      datos.put("Responsable de cartera", responsableCartera);
      datos.put("Supervisor", supervisor);
      datos.put("Gestor de expediente", gestorExpediente);
      datos.put("Gestor de formalizacion", gestorFormalizacion);
      datos.put("Gestor skip tracing", gestorSkipTracing);
      datos.put("Perfil soporte", perfilSoporte);
      datos.put("Estrategia", estrategia);
      datos.put("Tipo Estrategia", tipoEstrategia);
      //datos.put("Tipo Estado", tipoEstado);
      //datos.put("Subtipo Estado", subtipoEstado);
      datos.put("Tipo-Subtipo Estado", tipoSubtipoEstado);
      datos.put("Situacion", situacion);
      datos.put("Campaña contrato", campania);

    }

    return datos;
  }

  private void addExpedienteDataForExcel(LinkedHashMap<String, List<Collection<?>>> datos)
    throws NotFoundException {
    List<Collection<?>> expedientes;
    List<Collection<?>> estadosContrato;
    List<Collection<?>> responsablesCartera;
    List<Collection<?>> supervisores;
    List<Collection<?>> gestoresExpediente;
    List<Collection<?>> gestoresFormalizacion;
    List<Collection<?>> gestoresSkipTracing;
    List<Collection<?>> perfilesSoporte;
    List<Collection<?>> estrategias;
    List<Collection<?>> tiposEstrategia;
    //List<Collection<?>> tiposEstado;
    //List<Collection<?>> subtiposEstado;
    List<Collection<?>> tiposSubtiposEstado;
    List<Collection<?>> situaciones;
    List<Collection<?>> campanas;


    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      expedientes = datos.get("Connection");
      estadosContrato = datos.get("Contract situation");
      responsablesCartera = datos.get("Portfolio manager");
      supervisores = datos.get("Supervisor");
      gestoresExpediente = datos.get("Connection manager");
      gestoresFormalizacion = datos.get("Formalization Manager");
      gestoresSkipTracing = datos.get("Skip Tracing Manager");
      perfilesSoporte = datos.get("Support Profile");
      estrategias = datos.get("Strategy");
      tiposEstrategia = datos.get("Strategy type");
      //tiposEstado = datos.get("Situation type");
      //subtiposEstado = datos.get("Situation subtype");
      tiposSubtiposEstado = datos.get("Situation Type-Subtype");
      situaciones = datos.get("Situation");
      campanas = datos.get("Campaign");
    } else {
      expedientes = datos.get("Expediente");
      estadosContrato = datos.get("Estado contrato");
      responsablesCartera = datos.get("Responsable de cartera");
      supervisores = datos.get("Supervisor");
      gestoresExpediente = datos.get("Gestor de expediente");
      gestoresFormalizacion = datos.get("Gestor de formalizacion");
      gestoresSkipTracing = datos.get("Gestor skip tracing");
      perfilesSoporte = datos.get("Perfil soporte");
      estrategias = datos.get("Estrategia");
      tiposEstrategia = datos.get("Tipo Estrategia");
      //tiposEstado = datos.get("Tipo Estado");
      //subtiposEstado = datos.get("Subtipo Estado");
      tiposSubtiposEstado = datos.get("Tipo-Subtipo Estado");
      situaciones = datos.get("Situacion");
      campanas = datos.get("Campaña contrato");
    }

    List<EstadoContrato> estadosContratoBd = estadoContratoRepository.findAllByActivoIsTrue();
    for(var estado: estadosContratoBd) {
      estadosContrato.add(new CatalogoExcel(estado).getValuesList());
    }

    List<Usuario> responsablesCarteraBd = usuarioRepository.findAllByActivoIsTrueAndPerfilNombre("Responsable de cartera");
    for(var responsable: responsablesCarteraBd) {
      responsablesCartera.add(new UsuarioExcel(responsable).getValuesList());
    }

    List<Usuario> supervisoresBd = usuarioRepository.findAllByActivoIsTrueAndPerfilNombre("Supervisor");
    for(var supervisor: supervisoresBd) {
      supervisores.add(new UsuarioExcel(supervisor).getValuesList());
    }

    List<Usuario> gestoresBd = usuarioRepository.findAllByActivoIsTrueAndPerfilNombre("Gestor");
    for(var gestor: gestoresBd) {
      gestoresExpediente.add(new UsuarioExcel(gestor).getValuesList());
    }

    List<Usuario> gestoresFormalizacionBd = usuarioRepository.findAllByActivoIsTrueAndPerfilNombre("Gestor formalización");
    for(var gestor: gestoresFormalizacionBd) {
      gestoresFormalizacion.add(new UsuarioExcel(gestor).getValuesList());
    }

    List<Usuario> gestoresSkipTracingBd = usuarioRepository.findAllByActivoIsTrueAndPerfilNombre("Gestor de skip tracing");
    for(var gestor: gestoresSkipTracingBd) {
      gestoresSkipTracing.add(new UsuarioExcel(gestor).getValuesList());
    }

    List<Usuario> soportesBd = usuarioRepository.findAllByActivoIsTrueAndPerfilNombre("Soporte");
    for(var soporte: soportesBd) {
      perfilesSoporte.add(new UsuarioExcel(soporte).getValuesList());
    }

    List<Estrategia> estrategiasBd = estrategiaRepository.findAllByActivoIsTrue();
    for(var estrategia: estrategiasBd) {
      estrategias.add(new CatalogoExcel(estrategia).getValuesList());
    }

    List<TipoEstrategia> tipoEstrategiasBd = tipoEstrategiaRepository.findAllByActivoIsTrue();
    for(var tipo: tipoEstrategiasBd) {
      tiposEstrategia.add(new CatalogoExcel(tipo).getValuesList());
    }

    /*List<TipoEstado> tiposEstadoBd = tipoEstadoRepository.findAll();
    for(var tipo: tiposEstadoBd) {
      tiposEstado.add(new CatalogoExcel(tipo).getValuesList());
    }

    List<SubtipoEstado> subtiposEstadoBd = subtipoEstadoRepository.findAll();
    for(var subtipo: subtiposEstadoBd) {
      subtiposEstado.add(new CatalogoExcel(subtipo).getValuesList());
    }*/

    List<SubtipoEstado> tiposSubtiposEstadoBd = subtipoEstadoRepository.findAllByActivoIsTrue();
    for(var subtipo: tiposSubtiposEstadoBd) {
      tiposSubtiposEstado.add(new CatalogoDobleExcel(subtipo.getEstado(),subtipo).getValuesList());
    }

    List<Situacion> situacionesBD = situacionRepository.findAllByActivoIsTrue();
    for(var situacion: situacionesBD) {
      situaciones.add(new CatalogoExcel(situacion).getValuesList());
    }

    List<Campania> campaniasBd = campaniaRepository.findAllByActivoIsTrue();
    for(var campania: campaniasBd) {
      campanas.add(new CatalogoExcel(campania).getValuesList());
    }

    //campaña contrato
    List<Collection<?>> campania = new ArrayList<>();
    campania.add(CatalogoExcel.cabeceras);
  }


}
