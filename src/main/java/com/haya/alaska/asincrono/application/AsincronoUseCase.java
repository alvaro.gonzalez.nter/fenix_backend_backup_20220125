package com.haya.alaska.asincrono.application;

import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface AsincronoUseCase {

  LinkedHashMap<String, List<Collection<?>>> generarExcelCatalogos(Usuario usuario) throws NotFoundException;
  void modificarPorExcel(Usuario usuario, Boolean pn) throws Exception;
}
