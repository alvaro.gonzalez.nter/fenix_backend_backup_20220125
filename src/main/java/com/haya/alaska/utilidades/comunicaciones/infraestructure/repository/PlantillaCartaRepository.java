package com.haya.alaska.utilidades.comunicaciones.infraestructure.repository;

import com.haya.alaska.utilidades.comunicaciones.application.domain.EnvioCarta;
import com.haya.alaska.utilidades.comunicaciones.application.domain.PlantillaCarta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlantillaCartaRepository extends JpaRepository<PlantillaCarta, Integer> {
  Optional<PlantillaCarta> findByKey(String key);
}
