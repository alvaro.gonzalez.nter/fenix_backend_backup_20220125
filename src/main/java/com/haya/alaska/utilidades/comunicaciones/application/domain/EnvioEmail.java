package com.haya.alaska.utilidades.comunicaciones.application.domain;

import javax.persistence.*;

import com.haya.alaska.usuario.domain.Usuario;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name = "MSTR_ENVIO_EMAIL")
public class EnvioEmail {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID", nullable = false)
  private Usuario usuario;

  @Column(name = "DES_REQUEST_ID")
  private String requestId;

  @Column(name = "DES_BATCH_ERROR")
  private Boolean batchHasErrors;

  @Column(name = "DES_ERRORS")
  private Boolean hasErrors;

  @Column(name = "DES_RECIPIENT_ID")
  private String recipientSendId;

  @Column(name = "DES_EMAIL")
  private String email;

  @Column(name = "DES_MESSAGES")
  private String messages;

  @Column(name = "DES_TEXTO_EMAIL")
  @Lob
  private String textoEmail;
}
