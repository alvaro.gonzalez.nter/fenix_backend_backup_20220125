package com.haya.alaska.utilidades.comunicaciones.infraestructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.utilidades.comunicaciones.application.domain.EnvioSMS;

@Repository
public interface EnvioSMSRepository extends JpaRepository<EnvioSMS, Integer> {
    
    EnvioSMS findByToken(String tokenIdSMS);
}
