package com.haya.alaska.utilidades.comunicaciones.infraestructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.utilidades.comunicaciones.application.domain.PlantillaSMS;

import java.util.Optional;

@Repository
public interface PlantillaSMSRepository extends JpaRepository<PlantillaSMS, Integer>{
  Optional<PlantillaSMS> findByCodigoAPI(String codigo);
}
