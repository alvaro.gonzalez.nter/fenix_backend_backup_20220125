package com.haya.alaska.utilidades.comunicaciones.infraestructure.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.PeticionTokenDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.RespuestaTokenSalesforceDTO;

@FeignClient(name = "salesforce-token", url = "${integraciones.sf.token}")
public interface SalesforceTokenClient {

  //TOKEN
  @PostMapping()
  ResponseEntity<RespuestaTokenSalesforceDTO> tokenSalesforce(@RequestBody PeticionTokenDTO peticionTokenDTO);
}
