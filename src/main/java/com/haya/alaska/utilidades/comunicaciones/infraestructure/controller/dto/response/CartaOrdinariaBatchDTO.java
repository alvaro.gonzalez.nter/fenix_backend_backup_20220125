package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response;

import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteBusquedaDTOToList;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteBusquedaDTOToList;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
public class CartaOrdinariaBatchDTO {

  //Integer idModelo;
  String codigoPlantilla;
  List<IntervinienteBusquedaDTOToList> intervinientes;
  List<OcupanteBusquedaDTOToList> ocupantes;
  private String texto;

}
