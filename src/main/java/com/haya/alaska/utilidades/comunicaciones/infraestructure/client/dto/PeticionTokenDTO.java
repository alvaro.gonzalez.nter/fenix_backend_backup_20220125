package com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Component
public class PeticionTokenDTO {

  @Value("${integraciones.sf.credentials.grant_type}")
  @JsonProperty("grant_type")
  private String grantType;

  @Value("${integraciones.sf.credentials.client_id}")
  @JsonProperty("client_id")
  private String clientId;

  @Value("${integraciones.sf.credentials.client_secret}")
  @JsonProperty("client_secret")
  private String clientSecret;

  @Value("${integraciones.sf.credentials.account_id}")
  @JsonProperty("account_id")
  private String accountId;

}
