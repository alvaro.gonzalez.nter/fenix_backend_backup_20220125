package com.haya.alaska.utilidades.comunicaciones.application.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name = "MSTR_PLANTILLA_SMS")
public class PlantillaSMS {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @ToString.Include
    private Integer id;
    
    @Column(name = "DES_NOMBRE_PLANTILLA")
    private String nombrePlantilla;

    @Column(name = "DES_CODIGO_API")
    private String codigoAPI;
    
    @Column(name = "DES_TEXTO_PLANTILLA")
    private String textoPlantilla;
}
