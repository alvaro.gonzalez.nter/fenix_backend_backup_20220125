package com.haya.alaska.utilidades.comunicaciones.application.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "MSTR_ENVIO_BUROFAX")
@NoArgsConstructor
public class EnvioBurofax {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "ID_MODELO_BUROFAX", referencedColumnName = "ID")
  private PlantillaBurofax modelo;

  @Column(name = "DES_NOMBRE")
  private String nombre;

  @Column(name = "DES_APELLIDOS")
  private String apellidos;

  @Column(name = "DES_DIRECCION")
  private String direccion;

  @Column(name = "DES_CODIGO_POSTAL")
  private String codigoPostal;

  @Column(name = "DES_POBLACION")
  private String poblacion;

  @Column(name = "DES_PROVINCIA")
  private String provincia;

  @Column(name = "DES_EXPEDIENTE")
  private String expediente;

  @Column(name = "DES_EXPEDIENTE_PN")
  private String expedientePN;

  @Column(name = "DES_CONTRATO")
  private String contrato;

  @Column(name = "FCH_FECHA")
  private Date fecha;

  @Column(name = "DES_CLIENTE")
  private String cliente;

  @Column(name = "DES_NOMBRE_GESTOR")
  private String nombreGestor;

  @Column(name = "DES_TELEFONO_GESTOR")
  private String telefonoGestor;

  @Column(name = "DES_CORREO_GESTOR")
  private String correoGestor;

  @Column(name = "DES_PRODUCTO")
  private String producto;

  @Column(name = "DES_IMPORTE_DEUDA_CONTRATO")
  private Double importeDeudaContrato;

  @Column(name = "DES_NUMERO_JUZGADO")
  private String numeroJuzgado;

  @Column(name = "DES_PLAZA_JUZGADO")
  private String plazaJuzgado;

  @Column(name = "DES_PROCEDIMIENTO")
  private String procedimiento;

  @Column(name = "IND_ENVIADO")
  private Boolean enviado;

  @Column(name = "DES_TEXTO")
  @Lob
  private String texto;
}
