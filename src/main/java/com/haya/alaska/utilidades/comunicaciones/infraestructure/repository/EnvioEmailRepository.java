package com.haya.alaska.utilidades.comunicaciones.infraestructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.utilidades.comunicaciones.application.domain.EnvioEmail;

@Repository
public interface EnvioEmailRepository extends JpaRepository<EnvioEmail, Integer>{
    
}
