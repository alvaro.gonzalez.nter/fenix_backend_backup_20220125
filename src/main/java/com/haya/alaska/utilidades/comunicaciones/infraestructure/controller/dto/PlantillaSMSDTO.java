package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlantillaSMSDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonIgnore
    private Integer id;
    
    private String nombrePlantilla;

    private String codigoAPI;
    
    private String textoPlantilla;
}
