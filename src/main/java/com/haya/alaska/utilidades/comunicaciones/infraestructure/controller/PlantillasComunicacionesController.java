package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller;

import java.io.IOException;
import java.util.List;

import com.haya.alaska.shared.exceptions.InterruptedException;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaBurofaxDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaCartaDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.PlantillaFormalizacionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.haya.alaska.utilidades.comunicaciones.application.PlantillasUseCaseImpl;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaEmailDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaSMSDTO;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/comunicaciones/plantillas")
public class PlantillasComunicacionesController {

    @Autowired
    PlantillasUseCaseImpl plantillasUseCase;

    @ApiOperation(value = "Obtener una Plantilla SMS", notes = "Servicio para obtener una Plantilla SMS por su id")
    @GetMapping("/sms/{idPlantillaSMS}")
    public ResponseEntity<PlantillaSMSDTO> obtenerPlantillaSMS(@PathVariable Integer idPlantillaSMS)
	    throws IOException, InterruptedException {
	PlantillaSMSDTO plantillaSMS = plantillasUseCase.obtenerPlantillaSMS(idPlantillaSMS);
	return ResponseEntity.ok(plantillaSMS);
    }

    @ApiOperation(value = "Obtención del listado completo de Plantillas SMS", notes = "Servicio para obtener el listado de plantillas de SMS de la base datos")
    @GetMapping("/sms")
    public ResponseEntity<List<PlantillaSMSDTO>> obtenerListadoPlantillasSMS()
	    throws IOException, InterruptedException {
	List<PlantillaSMSDTO> listadoPlantillaSMS = plantillasUseCase.obtenerListadoPlantillaSMS();
	return ResponseEntity.ok(listadoPlantillaSMS);
    }

    @ApiOperation(value = "Obtener una Plantilla Email", notes = "Servicio para obtener una Plantilla Email por su id")
    @GetMapping("/email/{idPlantillaSMS}")
    public ResponseEntity<PlantillaEmailDTO> obtenerPlantillaEmail(@PathVariable Integer idPlantillaEmail)
	    throws IOException, InterruptedException {
	PlantillaEmailDTO plantillaEmail= plantillasUseCase.obtenerPlantillaEmail(idPlantillaEmail);
	return ResponseEntity.ok(plantillaEmail);
    }

    @ApiOperation(value = "Obtención del listado completo de Plantillas Email", notes = "Servicio para obtener el listado de plantillas de Email de la base datos")
    @GetMapping("/email")
    public ResponseEntity<List<PlantillaEmailDTO>> obtenerListadoPlantillasEmail()
	    throws IOException, InterruptedException {
	List<PlantillaEmailDTO> listadoPlantillaEmail = plantillasUseCase.obtenerListadoPlantillaEmail();
	return ResponseEntity.ok(listadoPlantillaEmail);
    }

  @ApiOperation(
      value = "Obtener una Plantilla Formalizacion",
      notes = "Servicio para obtener una Plantilla Formalizacion por su id")
  @GetMapping("/formalizacion/{idPlantilla}")
  public ResponseEntity<PlantillaFormalizacionDTO> obtenerPlantillaFormalizacion(
      @PathVariable Integer idPlantilla) throws IOException, InterruptedException {
    PlantillaFormalizacionDTO plantillaEmail = plantillasUseCase.obtenerPlantillaFormalizacion(idPlantilla);
    return ResponseEntity.ok(plantillaEmail);
  }

  @ApiOperation(
      value = "Obtención del listado completo de Plantillas Formalizacion",
      notes = "Servicio para obtener el listado de plantillas de Formalizacion de la base datos")
  @GetMapping("/formalizacion")
  public ResponseEntity<List<PlantillaFormalizacionDTO>> obtenerListadoPlantillasFormalizacion()
      throws IOException, InterruptedException {
    List<PlantillaFormalizacionDTO> listadoPlantillaEmail =
        plantillasUseCase.obtenerListadoPlantillaFormalizacion();
    return ResponseEntity.ok(listadoPlantillaEmail);
  }

  @ApiOperation(value = "Obtener una Plantilla Carta", notes = "Servicio para obtener una Plantilla Carta por su id")
  @GetMapping("/carta/{idPlantillaCarta}")
  public ResponseEntity<PlantillaCartaDTO> obtenerPlantillaCarta(@PathVariable Integer idPlantillaCarta)
    throws IOException, InterruptedException {
    PlantillaCartaDTO plantillaCarta= plantillasUseCase.obtenerPlantillaCarta(idPlantillaCarta);
    return ResponseEntity.ok(plantillaCarta);
  }

  @ApiOperation(value = "Obtención del listado completo de Plantillas Carta", notes = "Servicio para obtener el listado de plantillas de Carta de la base datos")
  @GetMapping("/carta")
  public ResponseEntity<List<PlantillaCartaDTO>> obtenerListadoPlantillasCarta()
    throws IOException, InterruptedException {
    List<PlantillaCartaDTO> listadoPlantillaCarta = plantillasUseCase.obtenerListadoPlantillaCarta();
    return ResponseEntity.ok(listadoPlantillaCarta);
  }


  @ApiOperation(value = "Obtener una Plantilla Burofax", notes = "Servicio para obtener una Plantilla Burofax por su id")
  @GetMapping("/burofax/{idPlantillaBurofax}")
  public ResponseEntity<PlantillaBurofaxDTO> obtenerPlantillaBurofax(@PathVariable Integer idPlantillaBurofax)
    throws IOException, InterruptedException {
    PlantillaBurofaxDTO plantillaBurofax= plantillasUseCase.obtenerPlantillaBurofax(idPlantillaBurofax);
    return ResponseEntity.ok(plantillaBurofax);
  }

  @ApiOperation(value = "Obtención del listado completo de Plantillas Burofax", notes = "Servicio para obtener el listado de plantillas de Burofax de la base datos")
  @GetMapping("/burofax")
  public ResponseEntity<List<PlantillaBurofaxDTO>> obtenerListadoPlantillasBurofax()
    throws IOException, InterruptedException {
    List<PlantillaBurofaxDTO> listadoPlantillaBurofax = plantillasUseCase.obtenerListadoPlantillaBurofax();
    return ResponseEntity.ok(listadoPlantillaBurofax);
  }



}
