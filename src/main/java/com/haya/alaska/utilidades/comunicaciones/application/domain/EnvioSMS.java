package com.haya.alaska.utilidades.comunicaciones.application.domain;

import java.util.Date;

import javax.persistence.*;

import com.haya.alaska.usuario.domain.Usuario;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name = "MSTR_ENVIO_SMS")
public class EnvioSMS {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID", nullable = false)
  private Usuario usuario;

  @Column(name = "DES_TOKEN_SMS")
  private String token;

  @Column(name = "DES_MESSAGE")
  private String message;

  @Column(name = "DES_COUNTE")
  private Integer count;

  @Column(name = "DES_EXCLUDED_COUNT")
  private Integer excludedCount;

  @Column(name = "DES_CREATE_DATE")
  private Date createDate;

  @Column(name = "DES_COMPLETE_DATE")
  private Date completeDate;

  @Column(name = "DES_STATUS")
  private String status;

  @Column(name = "DES_MOBILE_NUMBER")
  private String mobileNumber;

  @Column(name = "DES_STATUS_CODE")
  private String statusCode;

  @Column(name = "DES_MESSAGE_MOBILE")
  private String messageMobile;

  @Column(name = "DES_STANDARD_STATUS_CODE")
  private String standardStatusCode;

  @Column(name = "DES_DESCRIPTION")
  private String description;

  @Column(name = "DES_TEXTO_SMS")
  @Lob
  private String textoSMS;

  @Column(name = "DES_CODIGO_PLANTILLA")
  private String codigoPlantilla;
}
