package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.ResponsesOutputDto;

import lombok.Data;

@Data
@Component
public class SendMailErrors implements Serializable {
    
  private static final long serialVersionUID = 1L;
  
  private String requestId;
  
  private List<ResponsesOutputDto> responses;
  
  private Usuario usuario;
  
}
