package com.haya.alaska.utilidades.comunicaciones.application;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.controller.dto.IntervinienteBusquedaDTOToList;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.nivel_evento.domain.NivelEvento;
import com.haya.alaska.nivel_evento.infrastructure.repository.NivelEventoRepository;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.controller.dto.OcupanteBusquedaDTOToList;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.infrastructure.repository.ProcedimientoRepository;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociada;
import com.haya.alaska.procedimiento_posesion_negociada.infrastructure.repository.ProcedimientoPosesionNegociadaRepository;
import com.haya.alaska.shared.exceptions.InterruptedException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.utilidades.comunicaciones.application.domain.*;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.SalesforceTokenClient;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.*;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.BurofaxDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.CartaOrdinariaDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.EnvioEmailErrors;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.WhatsappWebDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.*;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.mapper.ComunicacionesMapper;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.repository.EnvioCartaRepository;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.repository.EnvioEmailRepository;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.repository.EnvioSMSRepository;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.repository.PlantillaCartaRepository;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.util.ComunicacionesUtil;
import com.jcraft.jsch.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ComunicacionesUseCaseImpl implements ComunicacionesUseCase {

  @Value("${integraciones.sf.send-email}")
  private String urlEmail;

  @Value("${integraciones.sf.send-sms}")
  private String urlSMS;

  @Value("${integraciones.sf.send-sms-status}")
  private String urlSMSStatus;

  @Value("${plantilla.sms.libre}")
  private String plantillaSMSLibre;

  @Value("${plantilla.sms.gestor}")
  private String plantillaSMSGestor;

  @Autowired
  private SalesforceTokenClient salesforceTokenClient;

  @Autowired
  private IntervinienteRepository intervinienteRepository;

  @Autowired
  private OcupanteRepository ocupanteRepository;

  @Autowired
  private EnvioEmailRepository envioEmailRepository;

  @Autowired
  private EnvioSMSRepository envioSMSRepository;

  @Autowired
  private ExpedienteRepository expedienteRepository;

  @Autowired
  private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;

  @Autowired
  private ContratoRepository contratoRepository;

  @Autowired
  private CarteraRepository carteraRepository;

  @Autowired
  private PlantillaCartaRepository plantillaCartaRepository;

  @Autowired
  private ProcedimientoRepository procedimientoRepository;

  @Autowired
  private ProcedimientoPosesionNegociadaRepository procedimientoPNRepository;

  @Autowired
  private EnvioCartaRepository envioCartaRepository;

  @Autowired
  private ComunicacionesUtil comunicacionesUtil;

  @Autowired
  private ComunicacionesMapper comunicacionesMapper;

  @Autowired
  private PeticionTokenDTO peticionToken;

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private SubtipoEventoRepository subtipoEventoRepository;

  @Autowired
  private EventoUseCase eventoUseCase;

  @Autowired
  NivelEventoRepository nivelEventoRepository;

  @Override
  public RespuestaSMSDTO envioSMS(String codigoPlantilla, SendSmsDTO datosSMS, Usuario usuario) throws NotFoundException, RequiredValueException {

    if(plantillaSMSLibre.equals(codigoPlantilla)) {
	for (Subscribers subscribers : datosSMS.getSubscribers()) {
	    subscribers.getAttributes().setCliente("");
	    subscribers.getAttributes().setTelefono("");
	    subscribers.getAttributes().setNombreGestor("");
	}
    } else {
	for (Subscribers subscribers : datosSMS.getSubscribers()) {
	    subscribers.getAttributes().setTextoSMS("");

	      // Hay que coger el idExpediente (des_id_concatenado) para buscar la relacion expediente-gestor
	    String idConcatenadoExpediente = subscribers.getAttributes().getIdExpediente();

	    // En virtud del Expediente hay que coger el gestor (Nombre y telefono)
	    Expediente expediente = this.getExpediente(idConcatenadoExpediente);

	    // Hay que buscar el gestor que sea de tipo 1 idPerfil = 1
	    String telefonoGestor = "";
	    String nombreGestor = "";
	    for (AsignacionExpediente asignacion : expediente.getAsignaciones()) {
		// Cogemos el telefono del gestor
		if(asignacion.getUsuario().getPerfil().getId() != null && asignacion.getUsuario().getPerfil().getId() == 1) {
		    telefonoGestor = asignacion.getUsuario().getTelefono();
		    nombreGestor = asignacion.getUsuario().getNombre();
		    break;
		}
	    }

	    if(telefonoGestor == null || telefonoGestor.isEmpty()) {
		throw new NotFoundException("Gestor", "Teléfono");
	    }

	    subscribers.getAttributes().setTelefono(telefonoGestor);

	    if(plantillaSMSGestor.equals(codigoPlantilla)) {
		subscribers.getAttributes().setNombreGestor(nombreGestor);
	    } else {
		subscribers.getAttributes().setNombreGestor("");
	    }
	}
    }

    // Mapeamos el objeto entrante a objeto de peticion a Salesforce
    String smsTo = comunicacionesMapper.convertSMSToFormatSalesforce(datosSMS).toString();

    HttpHeaders headers = comunicacionesUtil.addTokenHeaders(getTokenSalesforce());
    HttpEntity<String> entitySendSMS = new HttpEntity<String>(smsTo, headers);
    RespuestaSMSDTO smsRespuesta = restTemplate.postForObject(urlSMS + codigoPlantilla + "/send", entitySendSMS, RespuestaSMSDTO.class);

    // Se crea la entidad de la respuesta del Envio
    HttpEntity<String> entityGetStatus = new HttpEntity<String>(headers);
    RespuestaStatusSMSDTO respuestaStatus = restTemplate.exchange(urlSMSStatus + codigoPlantilla + "/deliveries/" + smsRespuesta.getTokenId(), HttpMethod.GET, entityGetStatus, RespuestaStatusSMSDTO.class).getBody();

    if (respuestaStatus.getTracking() != null && respuestaStatus.getTracking().size() > 0) {
      Integer index = 0;
      for (DataMobileDTO dataMobile : respuestaStatus.getTracking()) {
        EnvioSMS envioSMS = generateEntityEnvioSMS(usuario, smsRespuesta.getTokenId(), respuestaStatus);
        //todo comprobar esto
        String idConcatenadoExpediente = datosSMS.getSubscribers().get(index).getAttributes().getIdExpediente();
        // En virtud del Expediente hay que coger el gestor (Nombre y telefono)
        Expediente expediente = this.getExpediente(idConcatenadoExpediente);
        envioSMS.setMobileNumber(dataMobile.getMobileNumber());
        envioSMS.setStatusCode(dataMobile.getStatusCode());
        envioSMS.setMessageMobile(dataMobile.getMessage());
        envioSMS.setStandardStatusCode(dataMobile.getStandardStatusCode());
        envioSMS.setDescription(dataMobile.getDescription());
        envioSMS.setTextoSMS(datosSMS.getSubscribers().get(index).getAttributes().getTextoSMS());
        envioSMS.setCodigoPlantilla(codigoPlantilla);
        EnvioSMS metido = envioSMSRepository.save(envioSMS);
        crearEvento(expediente.getCartera().getId(),expediente.getId(),usuario,null,null,metido,null,false);
        index++;
      }
    } else {
      for (Subscribers subscribers : datosSMS.getSubscribers()) {
        EnvioSMS envioSMS = generateEntityEnvioSMS(usuario, smsRespuesta.getTokenId(), respuestaStatus);
        String idConcatenadoExpediente = subscribers.getAttributes().getIdExpediente();
        Expediente expediente = this.getExpediente(idConcatenadoExpediente);
        envioSMS.setMobileNumber(subscribers.getMobileNumber());
        envioSMS.setStatusCode("202");
        envioSMS.setMessageMobile("Delivered");
        envioSMS.setStandardStatusCode("2000");
        envioSMS.setDescription("Message was Delivered to Aggregator");
        envioSMS.setTextoSMS(subscribers.getAttributes().getTextoSMS());
        envioSMS.setCodigoPlantilla(codigoPlantilla);
        EnvioSMS metido = envioSMSRepository.save(envioSMS);
        crearEvento(expediente.getCartera().getId(),expediente.getId(),usuario,null,null,metido,null,false);
      }
    }

    return smsRespuesta;
  }

  @Override
  public RespuestaEmailDTO envioEmailAsincrono(String key, SendEmailDTO datosEmails, Usuario usuario) throws IOException, InterruptedException, NotFoundException, RequiredValueException {

    // Mapeamos el objeto entrante a objeto de peticion a Salesforce
    JSONObject emailTo = comunicacionesMapper.convertEmailToFormatSalesforce(datosEmails);

    HttpEntity<String> emailEntity = new HttpEntity<String>(emailTo.toString(), comunicacionesUtil.addTokenHeaders(getTokenSalesforce()));
    RespuestaEmailDTO emailRespuesta = restTemplate.postForObject(urlEmail + key + "/send", emailEntity, RespuestaEmailDTO.class);

    return emailRespuesta;
  }

  @Override
  public RespuestaEmailDTO envioEmail(String key, SendEmailDTO datosEmails, Usuario usuario)
    throws IOException, InterruptedException, NotFoundException, RequiredValueException {

    // Hay que coger el idExpediente (des_id_concatenado) para buscar la relacion expediente-gestor
    String idConcatenadoExpediente =
        datosEmails.getContactAttributes().getSubscriberAttributes().getIdExpediente();

    // En virtud del Expediente hay que coger el gestor (Nombre y telefono)
    Expediente expediente = this.getExpediente(idConcatenadoExpediente);

    // Hay que buscar el gestor que sea de tipo 1 idPerfil = 1
    String telefonoGestor = "";
    for (AsignacionExpediente asignacion : expediente.getAsignaciones()) {
      // Cogemos el telefono del gestor
      if (asignacion.getUsuario().getPerfil().getId() != null
          && asignacion.getUsuario().getPerfil().getId() == 1) {
        telefonoGestor = asignacion.getUsuario().getTelefono();
        // Si la plantilla es Informativa Posesiones
        if ("16334".equals(key)) {
          datosEmails
              .getContactAttributes()
              .getSubscriberAttributes()
              .setNombreGestor(asignacion.getUsuario().getNombre());
          datosEmails
              .getContactAttributes()
              .getSubscriberAttributes()
              .setCorreoGestor(asignacion.getUsuario().getEmail());
        }
        break;
      }
    }

    if (telefonoGestor == null || telefonoGestor.isEmpty()) {
      throw new NotFoundException("Gestor", "Teléfono");
    }

    // Cogemos el telefono del Gestor para añadirlo al Email
    if (datosEmails.getContactAttributes().getSubscriberAttributes().getTelefono() == null)
      datosEmails.getContactAttributes().getSubscriberAttributes().setTelefono(telefonoGestor);

    // Coger el id Contrato o buscarlo si no nos viene informado
    String idContrato = datosEmails.getContactAttributes().getSubscriberAttributes().getContrato();
    String idCargaContrato = "";
    Contrato contrato = null;

    Integer idInterviniente =
        datosEmails.getContactAttributes().getSubscriberAttributes().getIdInterviniente();
    Interviniente interviniente = this.getInterviniente(idInterviniente);

    if (idContrato == null || idContrato.isEmpty()) {

      // Coger el contrato asociado al Expediente que sea representante
      // contrato = this.getContratoRepresentante(interviniente, idConcatenadoExpediente);
      idCargaContrato = expediente.getContratoRepresentante().getIdCarga();

      // En caso de que venga a null, al menos setear el idConcatenado del Expediente
      if (idCargaContrato == null || idCargaContrato.isEmpty()) {
        idCargaContrato = idConcatenadoExpediente;
      }

      datosEmails.getContactAttributes().getSubscriberAttributes().setContrato(idCargaContrato);
    } else {

      // Coger el contrato asociado al Expediente que sea representante
      // contrato = this.getContratoById(Integer.parseInt(idContrato));
      contrato = expediente.getContratoRepresentante();
      datosEmails
          .getContactAttributes()
          .getSubscriberAttributes()
          .setContrato(contrato.getExpediente().getIdConcatenado());
    }

    // Coger el producto y el importe para la plantilla Informativa_Importes
    if ("156781".equals(key)) {
      datosEmails
          .getContactAttributes()
          .getSubscriberAttributes()
          .setProducto(contrato.getProducto().getValor());
      datosEmails
          .getContactAttributes()
          .getSubscriberAttributes()
          .setImporte(String.valueOf(contrato.getSaldoGestion()));
    }

    // Buscar en los procedimientos para la plantilla IAJ
    if ("45908".equals(key)) {

      Procedimiento procedimiento =
          contrato.getProcedimientos().stream()
              .filter(
                  p ->
                      "Hipotecario".equals(p.getTipo().getValor())
                          || "HIP".equals(p.getTipo().getCodigo()))
              .findAny()
              .orElse(null);

      if (procedimiento != null) {
        datosEmails
            .getContactAttributes()
            .getSubscriberAttributes()
            .setJuzgado(procedimiento.getJuzgado());
        datosEmails
            .getContactAttributes()
            .getSubscriberAttributes()
            .setPlazaJuzgado(procedimiento.getPlaza());
        datosEmails
            .getContactAttributes()
            .getSubscriberAttributes()
            .setProcedimiento(String.valueOf(procedimiento.getId()));
      }
    }

    // Mapeamos el objeto entrante a objeto de peticion a Salesforce
    JSONObject emailTo = comunicacionesMapper.convertEmailToFormatSalesforce(datosEmails);

    HttpEntity<String> emailEntity =
        new HttpEntity<String>(
            emailTo.toString(), comunicacionesUtil.addTokenHeaders(getTokenSalesforce()));
    RespuestaEmailDTO emailRespuesta =
        restTemplate.postForObject(urlEmail + key + "/send", emailEntity, RespuestaEmailDTO.class);

    // Se obtiene la respuesta para mapearla a Entidad para hacer la persistencia
    EnvioEmail envioEmail = new EnvioEmail();
    envioEmail.setUsuario(usuario);
    envioEmail.setRequestId(emailRespuesta.getRequestId());
    envioEmail.setBatchHasErrors(false);
    envioEmail.setEmail(datosEmails.getAddress());
    envioEmail.setTextoEmail(datosEmails.getContactAttributes().getSubscriberAttributes().getTextoEmail());

    for (ResponsesOutputDto response : emailRespuesta.getResponses()) {
      envioEmail.setHasErrors(response.getHasErrors());
      envioEmail.setRecipientSendId(response.getRecipientSendId());
      envioEmail.setMessages(response.getMessages().toString());
      EnvioEmail metido = envioEmailRepository.save(envioEmail);
      crearEvento(expediente.getCartera().getId(),expediente.getId(),usuario,null,null,null,metido,false);
    }

    return emailRespuesta;
  }

  @Override
  public RespuestaEmailDTO envioEmailPortalDeudor(String key, SendEmailDTO datosEmails, Usuario usuario) throws IOException, NotFoundException, RequiredValueException {

    String idContrato = datosEmails.getContactAttributes().getSubscriberAttributes().getContrato();
    Integer idInterviniente = datosEmails.getContactAttributes().getSubscriberAttributes().getIdInterviniente();

    if(idContrato == null || idContrato.isEmpty()) {

	// Mediante el idInterviniente obtener el idCarga del Contrato
      Interviniente interviniente=new Interviniente();
      // Mediante el idInterviniente obtener el idCarga del Contrato
      if (idInterviniente != null) {
        interviniente = this.getInterviniente(idInterviniente);
      }

      for (ContratoInterviniente contratoInterviniente : interviniente.getContratos()) {
	    if(contratoInterviniente.getContrato().isRepresentante()) {
		datosEmails.getContactAttributes().getSubscriberAttributes().setContrato(contratoInterviniente.getContrato().getIdCarga());
	    }
	}
    }

    // Mapeamos el objeto entrante a objeto de peticion a Salesforce
    JSONObject emailTo = comunicacionesMapper.convertEmailToFormatSalesforce(datosEmails);

    HttpEntity<String> emailEntity = new HttpEntity<String>(emailTo.toString(), comunicacionesUtil.addTokenHeaders(getTokenSalesforce()));
    RespuestaEmailDTO emailRespuesta = restTemplate.postForObject(urlEmail + key + "/send", emailEntity, RespuestaEmailDTO.class);

    // Se obtiene la respuesta para mapearla a Entidad para hacer la persistencia
    EnvioEmail envioEmail = new EnvioEmail();
    envioEmail.setUsuario(usuario);
    envioEmail.setRequestId(emailRespuesta.getRequestId());
    envioEmail.setBatchHasErrors(false);
    envioEmail.setEmail(datosEmails.getAddress());
    for (ResponsesOutputDto response : emailRespuesta.getResponses()) {
      var idConcatenadoExpediente = datosEmails.getContactAttributes().getSubscriberAttributes().getIdExpediente();
      Expediente expediente = this.getExpediente(idConcatenadoExpediente);
      envioEmail.setHasErrors(response.getHasErrors());
      envioEmail.setRecipientSendId(response.getRecipientSendId());
      envioEmail.setMessages(response.getMessages().toString());
      EnvioEmail metido = envioEmailRepository.save(envioEmail);
      crearEvento(expediente.getCartera().getId(),expediente.getId(),usuario,null,null,null,metido,false);
    }

    return emailRespuesta;
  }

  @Override
  public RespuestaEmailBatchDTO envioEmailBatch(
      String key, List<SendEmailDTO> datosEmails, Usuario usuario)
      throws NotFoundException, RequiredValueException {

    // Recorremos cada email de cada interviniente
    for (SendEmailDTO sendEmailDTO : datosEmails) {
      // Obtenemos el interviniente
      Integer idInterviniente =
          sendEmailDTO.getContactAttributes().getSubscriberAttributes().getIdInterviniente();
      Interviniente interviniente = null;
      if (idInterviniente != null){
        interviniente = this.getInterviniente(idInterviniente);

        // Seteamos el nombre y el apellido (desde Gestion Masiva viene concatenado)
        sendEmailDTO
          .getContactAttributes()
          .getSubscriberAttributes()
          .setNombre(interviniente.getNombre());
        sendEmailDTO
          .getContactAttributes()
          .getSubscriberAttributes()
          .setApellido(interviniente.getApellidos());
      }

      // Hay que coger el idExpediente (des_id_concatenado) para buscar la relacion
      // expediente-gestor
      String idConcatenadoExpediente =
          sendEmailDTO.getContactAttributes().getSubscriberAttributes().getIdExpediente();

      // En virtud del Expediente hay que coger el gestor
      Expediente expediente = this.getExpediente(idConcatenadoExpediente);

      // Hay que buscar el gestor que sea de tipo 1 idPerfil = 1
      String telefonoGestor = "";
      for (AsignacionExpediente asignacion : expediente.getAsignaciones()) {
        // Cogemos el telefono del gestor
        if (asignacion.getUsuario().getPerfil().getId() != null
            && asignacion.getUsuario().getPerfil().getId() == 1) {
          telefonoGestor = asignacion.getUsuario().getTelefono();
          // Si la plantilla es Informativa Posesiones
          if ("16334".equals(key)) {
            sendEmailDTO
                .getContactAttributes()
                .getSubscriberAttributes()
                .setNombreGestor(asignacion.getUsuario().getNombre());
            sendEmailDTO
                .getContactAttributes()
                .getSubscriberAttributes()
                .setCorreoGestor(asignacion.getUsuario().getEmail());
          }
          break;
        }
      }

      if (telefonoGestor == null || telefonoGestor.isEmpty()) {
        throw new NotFoundException("Gestor", "Teléfono");
      }

      if (sendEmailDTO.getContactAttributes().getSubscriberAttributes().getTelefono() == null)
        sendEmailDTO.getContactAttributes().getSubscriberAttributes().setTelefono(telefonoGestor);

      // Coger el id Contrato o buscarlo si no nos viene informado
      String idContrato =
          sendEmailDTO.getContactAttributes().getSubscriberAttributes().getContrato();
      String idCargaContrato = "";
      Contrato contrato = null;

      if (interviniente != null && (idContrato == null || idContrato.isEmpty())) {
        // Coger el contrato asociado al Expediente que sea representante
        contrato = this.getContratoRepresentante(interviniente, idConcatenadoExpediente);
        idCargaContrato = contrato.getIdCarga();

        // En caso de que venga a null, al menos setear el idConcatenado del Expediente
        if (idCargaContrato == null || idCargaContrato.isEmpty()) {
          idCargaContrato = idConcatenadoExpediente;
        }

        sendEmailDTO.getContactAttributes().getSubscriberAttributes().setContrato(idCargaContrato);

      } else {
        // Coger el contrato asociado al Expediente que sea representante
        // contrato = this.getContratoById(Integer.parseInt(idContrato));

        sendEmailDTO.getContactAttributes().getSubscriberAttributes().setContrato(idContrato);
      }

      // Coger el producto y el importe para la plantilla Informativa_Importes
      if ("156781".equals(key)) {
        sendEmailDTO
            .getContactAttributes()
            .getSubscriberAttributes()
            .setProducto(contrato.getProducto().getValor());
        sendEmailDTO
            .getContactAttributes()
            .getSubscriberAttributes()
            .setImporte(String.valueOf(contrato.getSaldoGestion()));
      }

      // Buscar en los procedimientos para la plantilla IAJ
      if ("45908".equals(key)) {

        Procedimiento procedimiento =
            contrato.getProcedimientos().stream()
                .filter(
                    p ->
                        "Hipotecario".equals(p.getTipo().getValor())
                            || "HIP".equals(p.getTipo().getCodigo()))
                .findAny()
                .orElse(null);

        if (procedimiento != null) {
          sendEmailDTO
              .getContactAttributes()
              .getSubscriberAttributes()
              .setJuzgado(procedimiento.getJuzgado());
          sendEmailDTO
              .getContactAttributes()
              .getSubscriberAttributes()
              .setPlazaJuzgado(procedimiento.getPlaza());
          sendEmailDTO
              .getContactAttributes()
              .getSubscriberAttributes()
              .setProcedimiento(String.valueOf(procedimiento.getId()));
        }
      }
    }

    log.warn("-*-SendEmailDTO: " + datosEmails.toString());
    // Mapeamos el objeto entrante a objeto de peticion a Salesforce
    List<JSONObject> emailsTo =
        comunicacionesMapper.convertEmailBatchToFormatSalesforce(datosEmails);

    HttpEntity<String> emailEntity =
        new HttpEntity<String>(
            emailsTo.toString(), comunicacionesUtil.addTokenHeaders(getTokenSalesforce()));
    RespuestaEmailBatchDTO emailRespuestaBatch =
        restTemplate.postForObject(
            urlEmail + key + "/sendBatch", emailEntity, RespuestaEmailBatchDTO.class);

    // Se obtiene la respuesta para mapearla a Entidad para hacer la persistencia
    int index = 0;
    if (emailRespuestaBatch != null)
      log.warn("-*-RespuestaEmailBatchDTO: " + emailRespuestaBatch.toString());
    for (ResponsesOutputDto response : emailRespuestaBatch.getResponses()) {
      var idConcatenadoExpediente =
          datosEmails.get(index).getContactAttributes().getSubscriberAttributes().getIdExpediente();
      Expediente expediente = this.getExpediente(idConcatenadoExpediente);
      EnvioEmail envioEmailBatch = new EnvioEmail();
      envioEmailBatch.setUsuario(usuario);
      envioEmailBatch.setRequestId(emailRespuestaBatch.getRequestId());
      envioEmailBatch.setBatchHasErrors(emailRespuestaBatch.getBatchHasErrors());
      envioEmailBatch.setHasErrors(response.getHasErrors());
      envioEmailBatch.setRecipientSendId(response.getRecipientSendId());
      envioEmailBatch.setMessages(response.getMessages().toString());
      envioEmailBatch.setEmail(datosEmails.get(index).getAddress());
      log.warn("-*-EnvioEmail: " + envioEmailBatch.toString());
      EnvioEmail metido = envioEmailRepository.save(envioEmailBatch);
      index++;
      crearEvento(
          expediente.getCartera().getId(),
          expediente.getId(),
          usuario,
          null,
          null,
          null,
          metido,
          false);
    }

    return emailRespuestaBatch;
  }

  @Override
  public RespuestaStatusSMSDTO statusSMS(String tokenIdSms) throws IOException, InterruptedException {

    HttpEntity<String> statusSmsEntity = new HttpEntity<String>(comunicacionesUtil.addTokenHeaders(getTokenSalesforce()));
    ResponseEntity<RespuestaStatusSMSDTO> statusSmsRespuesta = restTemplate.exchange(urlSMSStatus + "/deliveries" + tokenIdSms, HttpMethod.GET, statusSmsEntity, RespuestaStatusSMSDTO.class);

    return statusSmsRespuesta.getBody();
  }

  @Override
  public List<EnvioEmailErrors> checkEmailErrors(String requestId) {
    List<EnvioEmailErrors> listErrorsEnvioEmailDTO = envioEmailRepository.findAll().stream()
      .filter(email -> requestId.equals(email.getRequestId()))
      .filter(error -> error.getBatchHasErrors() || error.getHasErrors())
      .map(emailsError -> {
        EnvioEmailErrors envioEmailErrors = new EnvioEmailErrors();
        try {
          BeanUtils.copyProperties(envioEmailErrors, emailsError);
        } catch (IllegalAccessException | InvocationTargetException e) {
          e.getMessage();
        }
        envioEmailErrors.setUsuario(emailsError.getUsuario().getId());
        return envioEmailErrors;
      })
      .collect(Collectors.toList());

    return listErrorsEnvioEmailDTO;
  }

  private LinkedHashMap<String, List<Collection<?>>> createCartaDataForExcel(
    List<EnvioCarta> envioCartas) {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> resultado = new ArrayList<>();
    resultado.add(CartaExcelDTO.cabeceras);
    for(EnvioCarta envioCarta: envioCartas) {
      resultado.add(
        new CartaExcelDTO(envioCarta).getValuesList());
    }
    datos.put("Campos Dinámicos Comunicaciones", resultado);

    return datos;
  }

  public void create(LinkedHashMap<String, List<Collection<?>>> datos) {
    Workbook workbook = new XSSFWorkbook();

    CellStyle cellStyleDate = workbook.createCellStyle();
    cellStyleDate.setDataFormat((short)14);


    CellStyle styleHeader = workbook.createCellStyle();
    Font font = workbook.createFont();
    font.setBold(true);
    font.setColor(IndexedColors.WHITE.getIndex());
    styleHeader.setFont(font);
    //styleHeader.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
    byte[] rgb = new byte[3];
    rgb[0] = (byte) 10; // red
    rgb[1] = (byte) 148; // green
    rgb[2] = (byte) 214; // blue
    //create XSSFColor
    XSSFColor color = new XSSFColor(rgb, new DefaultIndexedColorMap());
    XSSFCellStyle xssfcellcolorstyle = (XSSFCellStyle) styleHeader;
    xssfcellcolorstyle.setFillForegroundColor(color);
    xssfcellcolorstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

    for (var datosHoja : datos.entrySet()) {
      Sheet sheet = workbook.createSheet(datosHoja.getKey());
      int indiceFila = 0;
      for (Collection<?> datosFila : datosHoja.getValue()) {
        Row filaExcel = sheet.createRow(indiceFila);
        int indiceColumna = 0;
        for (Object dato : datosFila) {
          Cell celdaExcel = filaExcel.createCell(indiceColumna);
          if (indiceFila == 0) {
            celdaExcel.setCellStyle(xssfcellcolorstyle);
            sheet.setColumnWidth(indiceColumna, 15 * 256);
          }
          if (dato != null) {
            if (dato instanceof Date) {
              celdaExcel.setCellStyle(cellStyleDate);
              celdaExcel.setCellValue((Date) dato);
            } else if (dato instanceof Boolean) {
              if (dato == Boolean.TRUE) {
                if (LocaleContextHolder.getLocale().getLanguage() == null
                  || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
                  celdaExcel.setCellValue("Yes");
                } else {
                  celdaExcel.setCellValue("Sí");
                }
              } else if (dato == Boolean.FALSE) {
                celdaExcel.setCellValue("No");
              }
            } else if (dato instanceof Number) {
              celdaExcel.setCellValue(((Number) dato).doubleValue());
            } else if (dato instanceof Catalogo) {
              celdaExcel.setCellValue(((Catalogo) dato).getValor());
            } else if (dato.equals("")) {
              celdaExcel.setCellValue("-");
            } else if (dato instanceof String){
              celdaExcel.setCellValue(dato.toString());
            }
          } else {
            celdaExcel.setCellValue("-");
          }
          indiceColumna++;
        }
        indiceFila++;
      }
    }

    try{
      FileOutputStream out = new FileOutputStream(new File("src/main/java/com/haya/alaska/utilidades/comunicaciones/application/cartas.xlsx"));
      workbook.write(out);
      out.close();
    }catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  @Scheduled(cron="0 0 22 * * *")
  private void enviarCartas() throws IOException, SftpException, JSchException {

    //generar excel
    List<EnvioCarta> cartas = envioCartaRepository.findAllByEnviadoFalse();
    this.create(createCartaDataForExcel(cartas));

    //conectar con sftp
    JSch jsch = new JSch();
    Session jschSession = jsch.getSession("usr_fenix_enviospostales", "intercambio.haya.es");
    Properties config = new java.util.Properties();
    config.put("StrictHostKeyChecking", "no");
    config.put("PreferredAuthentications", "password");
    jschSession.setConfig(config);
    jschSession.setPassword("1de3301e2a");
    jschSession.connect();

    ChannelSftp channelSftp = (ChannelSftp) jschSession.openChannel("sftp");
    channelSftp.connect();

    //meter el excel
    String localFile = "src/main/java/com/haya/alaska/utilidades/comunicaciones/application/cartas.xlsx";
    String remoteDir = "Archivos/";

    LocalDate fecha = LocalDate.now();
    String mes = (fecha.getMonthValue()<10?("0"+fecha.getMonthValue()): String.valueOf((fecha.getMonthValue())));
    String dia = (fecha.getDayOfMonth()<10?("0"+fecha.getDayOfMonth()): String.valueOf((fecha.getDayOfMonth())));
    channelSftp.put(localFile, remoteDir + "cartas"+fecha.getYear()+mes+dia+".xlsx");

    channelSftp.exit();

    //poner en la bd las cartas como enviado=true
    for(EnvioCarta carta: cartas) {
      carta.setEnviado(true);
    }
    envioCartaRepository.saveAll(cartas);
  }

  private boolean generarEnvioCarta(Interviniente interviniente, IntervinienteBusquedaDTOToList intervinienteDto, CartaOrdinariaBatchDTO carta, Procedimiento procedimiento, Usuario usuario) throws NotFoundException, RequiredValueException {

    EnvioCarta envioCarta = new EnvioCarta();
    if(Boolean.TRUE.equals(interviniente.getPersonaJuridica())) {
      envioCarta.setNombre(interviniente.getRazonSocial());
    } else {
      envioCarta.setNombre(interviniente.getNombre());
      envioCarta.setApellidos(interviniente.getApellidos());
    }

    for(var direccion: interviniente.getDirecciones()) {
      if(Boolean.TRUE.equals(direccion.getPrincipal())) {
        envioCarta.setDireccion(obtenerDireccionString(direccion));
        envioCarta.setCodigoPostal(direccion.getCodigoPostal());
        envioCarta.setPoblacion(direccion.getLocalidad() != null ? direccion.getLocalidad().getValor() : null);
        envioCarta.setProvincia(direccion.getLocalidad() != null ? direccion.getProvincia().getValor() : null);
      }
    }

    Contrato contrato = contratoRepository.findByIdCarga(intervinienteDto.getIdContrato()).orElse(null);

    if(contrato != null) {
      envioCarta.setProducto(contrato.getProducto().getValor());
      envioCarta.setContrato(contrato.getIdCarga());
      envioCarta.setImporteDeudaContrato(contrato.getDeudaImpagada());
    }

    if(procedimiento != null) {
      envioCarta.setProcedimiento(procedimiento.getNumeroAutos());
      envioCarta.setPlazaJuzgado(procedimiento.getPlaza());
      envioCarta.setNumeroJuzgado(String.valueOf(procedimiento.getJuzgado()));
    }

    Expediente expediente = contrato.getExpediente();
    Cartera cartera = expediente.getCartera();
    if(cartera != null) {
      envioCarta.setCliente(cartera.getCliente().getNombre());
    }
    if(expediente != null && expediente.getGestor() != null) {
      envioCarta.setExpediente(expediente.getIdConcatenado());
      envioCarta.setNombreGestor(expediente.getGestor().getNombre());
      envioCarta.setTelefonoGestor(expediente.getGestor().getTelefono());
      envioCarta.setCorreoGestor(expediente.getGestor().getEmail());

      PlantillaCarta plantillaCarta = plantillaCartaRepository.findByKey(carta.getCodigoPlantilla()).orElse(null);
      envioCarta.setModelo(plantillaCarta);

      envioCarta.setFecha(new Date());
      envioCarta.setEnviado(false);
      envioCarta.setTexto(carta.getTexto());

      if(envioCarta.getDireccion() != null) {
        EnvioCarta metido = envioCartaRepository.save(envioCarta);
        crearEvento(cartera.getId(),expediente.getId(),usuario,metido,null,null,null,false);

        return true;
      }

    }
    return false;
  }

  private boolean crearEvento(Integer idCartera, Integer idExpediente, Usuario usuario, EnvioCarta envioCarta, EnvioBurofax envioBurofax, EnvioSMS envioSMS, EnvioEmail envioEmail,boolean posesionNegociada) throws NotFoundException, RequiredValueException {
    EventoInputDTO evento = new EventoInputDTO();
    evento.setCartera(idCartera);
    evento.setIdExpediente(idExpediente);
    evento.setDestinatario(usuario.getId());
    evento.setClaseEvento(3);
    evento.setTipoEvento(14);
    evento.setFechaLimite(new Date());
    evento.setFechaAlerta(new Date());
    evento.setFechaCreacion(new Date());
    evento.setImportancia(2);

    if(envioCarta != null) {
      NivelEvento nivel = nivelEventoRepository.findByCodigo("CEXT_CARTA").orElse(null);
      NivelEvento nivelPN = nivelEventoRepository.findByCodigo("CEXT_CARTA_PN").orElse(null);
      evento.setNivel(posesionNegociada? nivelPN.getId() : nivel.getId());
      evento.setNivelId(envioCarta.getId());
      evento.setTitulo("Envío de carta");
      SubtipoEvento se = subtipoEventoRepository.findByCodigoAndCarteraId("CEXT_CARTA",idCartera).orElseThrow(() ->
        new NotFoundException("SubtipoEvento CEXT_CARTA","Cartera" + idCartera));
      evento.setSubtipoEvento(se.getId());
    } else if(envioBurofax != null) {
      NivelEvento nivel = nivelEventoRepository.findByCodigo("CEXT_BUROFAX").orElse(null);
      NivelEvento nivelPN = nivelEventoRepository.findByCodigo("CEXT_BUROFAX_PN").orElse(null);
      evento.setNivel(posesionNegociada? nivelPN.getId() : nivel.getId());
      evento.setNivelId(envioBurofax.getId());
      evento.setTitulo("Envío de burofax");
      SubtipoEvento se = subtipoEventoRepository.findByCodigoAndCarteraId("CEXT_BUROFAX",idCartera).orElseThrow(() ->
        new NotFoundException("SubtipoEvento CEXT_BUROFAX","Cartera" + idCartera));
      evento.setSubtipoEvento(se.getId());
    } else if(envioSMS != null) {
      NivelEvento nivel = nivelEventoRepository.findByCodigo("CEXT_SMS").orElse(null);
      NivelEvento nivelPN = nivelEventoRepository.findByCodigo("CEXT_SMS_PN").orElse(null);
      evento.setNivel(posesionNegociada? nivelPN.getId() : nivel.getId());
      evento.setNivelId(envioSMS.getId());
      evento.setTitulo("Envío de SMS");
      SubtipoEvento se = subtipoEventoRepository.findByCodigoAndCarteraId("CEXT_SMS",idCartera).orElseThrow(() ->
        new NotFoundException("SubtipoEvento CEXT_SMS","Cartera" + idCartera));
      evento.setSubtipoEvento(se.getId());
      evento.setEstado(3);
    } else if(envioEmail != null) {
      NivelEvento nivel = nivelEventoRepository.findByCodigo("CEXT_EMAIL").orElse(null);
      NivelEvento nivelPN = nivelEventoRepository.findByCodigo("CEXT_EMAIL_PN").orElse(null);
      evento.setNivel(posesionNegociada? nivelPN.getId() : nivel.getId());
      evento.setNivelId(envioEmail.getId());
      evento.setTitulo("Envío de email");
      SubtipoEvento se = subtipoEventoRepository.findByCodigoAndCarteraId("CEXT_EMAIL",idCartera).orElseThrow(() ->
        new NotFoundException("SubtipoEvento CEXT_EMAIL","Cartera" + idCartera));
      evento.setSubtipoEvento(se.getId());
    }

    eventoUseCase.create(evento,usuario);
    return true;
  }

  private boolean generarEnvioCartaPN(Ocupante ocupante, OcupanteBusquedaDTOToList ocupanteDto, CartaOrdinariaBatchDTO carta, ProcedimientoPosesionNegociada procedimiento, Usuario usuario) throws NotFoundException, RequiredValueException {

    EnvioCarta envioCarta = new EnvioCarta();
    envioCarta.setNombre(ocupante.getNombre());
    envioCarta.setApellidos(ocupante.getApellidos());
    for(var direccion: ocupante.getDirecciones()) {
      if(Boolean.TRUE.equals(direccion.getPrincipal())) {

        envioCarta.setDireccion(obtenerDireccionString(direccion));
        envioCarta.setCodigoPostal(direccion.getCodigoPostal());
        envioCarta.setPoblacion(direccion.getLocalidad() != null ? direccion.getLocalidad().getValor() : null);
        envioCarta.setProvincia(direccion.getLocalidad() != null ? direccion.getProvincia().getValor() : null);
      }
    }


    ExpedientePosesionNegociada expediente = expedientePosesionNegociadaRepository.findByIdConcatenado(ocupanteDto.getIdExpediente()).orElse(null);

    Cartera cartera = expediente.getCartera();
    if(cartera != null) {
      envioCarta.setCliente(cartera.getCliente().getNombre());
    }

    if(procedimiento != null) {
      envioCarta.setProcedimiento(procedimiento.getAutos());
      envioCarta.setPlazaJuzgado(procedimiento.getCiudadJuzgado() != null ? procedimiento.getCiudadJuzgado().getValor() : null);
      envioCarta.setNumeroJuzgado(String.valueOf(procedimiento.getJuzgado()));
    }

    if(expediente != null && expediente.getGestor() != null) {
      envioCarta.setExpedientePN(expediente.getIdConcatenado());
      envioCarta.setNombreGestor(expediente.getGestor().getNombre());
      envioCarta.setTelefonoGestor(expediente.getGestor().getTelefono());
      envioCarta.setCorreoGestor(expediente.getGestor().getEmail());

      PlantillaCarta plantillaCarta = plantillaCartaRepository.findByKey(carta.getCodigoPlantilla()).orElse(null);
      envioCarta.setModelo(plantillaCarta);

      envioCarta.setFecha(new Date());
      envioCarta.setEnviado(false);
      envioCarta.setTexto(carta.getTexto());

      if (envioCarta.getDireccion() != null) {
        EnvioCarta metido = envioCartaRepository.save(envioCarta);
        //generar evento
        crearEvento(cartera.getId(),expediente.getId(),usuario,metido,null,null,null,true);
        return true;
      }
    }
    return false;
  }

  private boolean comprobarDirecciones(CartaOrdinariaBatchDTO carta) {
    boolean hayError = false;
    if(carta.getIntervinientes() != null && carta.getIntervinientes().size() != 0) {
      for(var intervinienteDto: carta.getIntervinientes()) {
        if(intervinienteDto.getId() != null) {
          Interviniente interviniente = intervinienteRepository.findById(intervinienteDto.getId()).orElse(null);
          if(interviniente != null) {
            boolean hayPrincipal = false;
            for(var direccion: interviniente.getDirecciones()) {
              if(Boolean.TRUE.equals(direccion.getPrincipal())) {
                hayPrincipal=true;
              }
            }
            if(!hayPrincipal && !hayError)
              hayError = true;
          }
        }
      }
    }
    if(carta.getOcupantes() != null && carta.getOcupantes().size() != 0) {
      for(var ocupanteDto: carta.getOcupantes()) {
          if (ocupanteDto.getId() != null) {
            Ocupante ocupante = ocupanteRepository.findById(ocupanteDto.getId()).orElse(null);
            if (ocupante != null) {
              boolean hayPrincipal = false;
              for(var direccion: ocupante.getDirecciones()) {
                if(Boolean.TRUE.equals(direccion.getPrincipal()) && !hayError) {
                  hayPrincipal=true;
                }
              }
              if(!hayPrincipal && !hayError)
                hayError = true;
            }
          }
      }
    }
    return hayError;
  }

  private boolean comprobarDireccion(CartaOrdinariaDTO carta) {
    boolean hayError = false;
    if(carta.getIdsIntervinientes() != null && carta.getIdsIntervinientes().size() != 0) {
      for(var id: carta.getIdsIntervinientes()) {
        if(id != null) {
          Interviniente interviniente = intervinienteRepository.findById(id).orElse(null);
          if(interviniente != null) {
            boolean hayPrincipal = false;
            for(var direccion: interviniente.getDirecciones()) {
              if(Boolean.TRUE.equals(direccion.getPrincipal())) {
                hayPrincipal=true;
              }
            }
            if(!hayPrincipal && !hayError)
              hayError = true;
          }
        }
      }
    }
    if(carta.getIdsOcupantes() != null && carta.getIdsOcupantes().size() != 0) {
      for(var id: carta.getIdsOcupantes()) {
          if (id != null) {
            Ocupante ocupante = ocupanteRepository.findById(id).orElse(null);
            if (ocupante != null) {
              boolean hayPrincipal = false;
              for(var direccion: ocupante.getDirecciones()) {
                if(Boolean.TRUE.equals(direccion.getPrincipal()) && !hayError) {
                  hayPrincipal=true;
                }
              }
              if(!hayPrincipal && !hayError)
                hayError = true;
            }
          }
      }
    }
    return hayError;
  }

  @Override
  public CartaOrdinariaRespuestaDTO generarEnviosCartaBatch(CartaOrdinariaBatchDTO carta, Usuario usuario) throws Exception {
    boolean hay = false;
    if(comprobarDirecciones(carta)) {
      throw new Exception("Algún interviniente u ocupante no tenía direccion principal y no se ha enviado ninguna carta");
    }
    if(carta.getIntervinientes() != null && carta.getIntervinientes().size() != 0) {
      for(var intervinienteDto: carta.getIntervinientes()) {
        if(intervinienteDto.getId() != null) {
          Interviniente interviniente = intervinienteRepository.findById(intervinienteDto.getId()).orElse(null);
          if(interviniente != null) {
            if(intervinienteDto.getProcedimientos() != null && intervinienteDto.getProcedimientos().size() > 0) {
              for(var procedimientoDto: intervinienteDto.getProcedimientos()) {
                Procedimiento procedimiento = procedimientoRepository.findById(procedimientoDto.getId()).orElse(null);
                generarEnvioCarta(interviniente, intervinienteDto, carta, procedimiento, usuario);
              }
            } else {
                generarEnvioCarta(interviniente, intervinienteDto, carta, null, usuario);
            }
          }
        }
      }
      hay = true;
    }
    if(carta.getOcupantes() != null && carta.getOcupantes().size() != 0) {
      for(var ocupanteDto: carta.getOcupantes()) {
        if(ocupanteDto.getProcedimientos() != null && ocupanteDto.getProcedimientos().size() > 0) {
          if (ocupanteDto.getId() != null) {
            Ocupante ocupante = ocupanteRepository.findById(ocupanteDto.getId()).orElse(null);
            if (ocupante != null) {
              if(ocupanteDto.getProcedimientos() != null && ocupanteDto.getProcedimientos().size() > 0) {
                for (var procedimiento : ocupanteDto.getProcedimientos()) {
                  ProcedimientoPosesionNegociada procedimientoPosesionNegociada = procedimientoPNRepository.findById(procedimiento.getId()).orElse(null);
                  generarEnvioCartaPN(ocupante, ocupanteDto, carta, procedimientoPosesionNegociada, usuario);
                }
              } else {
                generarEnvioCartaPN(ocupante, ocupanteDto, carta, null, usuario);
              }
            }
          }
        }
      }
      hay = true;
    }
    if(hay) return new CartaOrdinariaRespuestaDTO("Guardado correctamente");
    else throw new Exception("No se han generado envíos porque no se han enviado intervinientes ni ocupantes");
  }

  public String obtenerDireccionString(Direccion direccion) {
    String direccionString ="";
    if(direccion.getTipoVia() != null) direccionString += direccion.getTipoVia().getValor();
    if(direccion.getNombre() != null) direccionString += " "+direccion.getNombre();
    if(direccion.getNumero() != null) direccionString += " "+direccion.getNumero();
    if(direccion.getPortal() != null) direccionString += " "+direccion.getPortal();
    if(direccion.getBloque() != null) direccionString += " "+direccion.getBloque();
    if(direccion.getEscalera() != null) direccionString += " "+direccion.getEscalera();
    if(direccion.getPiso() != null) direccionString += " "+direccion.getPiso();
    if(direccion.getPuerta() != null) direccionString += " "+direccion.getPuerta();
    return direccionString;
  }

  private void generarEnviosCarta(Interviniente interviniente, Ocupante ocupante, Direccion direccion, CartaOrdinariaDTO cartaOrdinariaDTO, Usuario usuario) throws NotFoundException, RequiredValueException {

    EnvioCarta envioCarta = new EnvioCarta();
    Cartera cartera = null;
    Expediente expediente = null;
    ExpedientePosesionNegociada expedientePosesionNegociada = null;
    if((interviniente != null || ocupante != null) && direccion != null && cartaOrdinariaDTO != null) {
      if(interviniente != null) {
        envioCarta.setNombre(interviniente.getNombre());
        envioCarta.setApellidos(interviniente.getApellidos());
      } else if(ocupante != null) {
        envioCarta.setNombre(ocupante.getNombre());
        envioCarta.setApellidos(ocupante.getApellidos());
      }



      cartera = carteraRepository.findById(cartaOrdinariaDTO.getIdCartera()).orElse(null);
      if(cartera != null) {
        envioCarta.setCliente(cartera.getCliente().getNombre());
      }


      if(cartaOrdinariaDTO.getIdExpediente() != null && cartaOrdinariaDTO.getIdExpediente() != 0) {

        expediente = expedienteRepository.findById(cartaOrdinariaDTO.getIdExpediente()).orElse(null);

        if(expediente != null) {
          envioCarta.setExpediente(expediente.getIdConcatenado());
          if(expediente.getGestor() != null) {
            envioCarta.setNombreGestor(expediente.getGestor().getNombre());
            envioCarta.setTelefonoGestor(expediente.getGestor().getTelefono());
            envioCarta.setCorreoGestor(expediente.getGestor().getEmail());
          }
        }

        Contrato contrato = contratoRepository.findById(cartaOrdinariaDTO.getIdContrato()).orElse(null);
        if(contrato != null) {
          envioCarta.setProducto(contrato.getProducto().getValor());
          envioCarta.setContrato(contrato.getIdCarga());
          envioCarta.setImporteDeudaContrato(contrato.getDeudaImpagada());
        }

        Procedimiento procedimiento = procedimientoRepository.findById(cartaOrdinariaDTO.getIdProcedimiento()).orElse(null);
        if(procedimiento != null) {
          envioCarta.setNumeroJuzgado(procedimiento.getJuzgado());
          envioCarta.setPlazaJuzgado(procedimiento.getPlaza());
          envioCarta.setProcedimiento(procedimiento.getNumeroAutos());
        }

      } else if(cartaOrdinariaDTO.getIdExpedientePN() != null && cartaOrdinariaDTO.getIdExpedientePN() != 0) {

        expedientePosesionNegociada = expedientePosesionNegociadaRepository.findById(cartaOrdinariaDTO.getIdExpedientePN()).orElse(null);

        if(expedientePosesionNegociada  != null) {
          envioCarta.setExpedientePN(expedientePosesionNegociada .getIdConcatenado());
          if(expedientePosesionNegociada .getGestor() != null) {
            envioCarta.setNombreGestor(expedientePosesionNegociada .getGestor().getNombre());
            envioCarta.setTelefonoGestor(expedientePosesionNegociada .getGestor().getTelefono());
            envioCarta.setCorreoGestor(expedientePosesionNegociada .getGestor().getEmail());
          }
        }

        ProcedimientoPosesionNegociada procedimiento = procedimientoPNRepository.findById(cartaOrdinariaDTO.getIdProcedimiento()).orElse(null);
        if(procedimiento != null) {
          envioCarta.setNumeroJuzgado(procedimiento.getJuzgado());
          envioCarta.setPlazaJuzgado(procedimiento.getCiudadJuzgado() != null ? procedimiento.getCiudadJuzgado().getValor() : null);
          envioCarta.setProcedimiento(procedimiento.getAutos());
        }
      }

      envioCarta.setDireccion(obtenerDireccionString(direccion));
      envioCarta.setCodigoPostal(direccion.getCodigoPostal());
      envioCarta.setPoblacion(direccion.getLocalidad() != null ? direccion.getLocalidad().getValor() : null);
      envioCarta.setProvincia(direccion.getLocalidad() != null ? direccion.getProvincia().getValor() : null);



      PlantillaCarta plantillaCarta = null;
      if(cartaOrdinariaDTO.getCodigoPlantilla() != null) {

        plantillaCarta = plantillaCartaRepository.findByKey(cartaOrdinariaDTO.getCodigoPlantilla()).orElse(null);
        envioCarta.setModelo(plantillaCarta);
      } //else if(cartaOrdinariaDTO.getTexto() != null) {
        envioCarta.setTexto(cartaOrdinariaDTO.getTexto());
      //}


      envioCarta.setFecha(new Date());
      envioCarta.setEnviado(false);


    }
    EnvioCarta metido = envioCartaRepository.save(envioCarta);
    if(expediente != null)
      crearEvento(cartera.getId(),expediente.getId(),usuario,metido,null,null,null,false);
    else if(expedientePosesionNegociada != null)
      crearEvento(cartera.getId(),expedientePosesionNegociada.getId(),usuario,metido,null,null,null,true);
  }

  @Override
  public CartaOrdinariaRespuestaDTO enviarCartaOrdinaria(CartaOrdinariaDTO cartaOrdinariaDTO, Usuario usuario) throws Exception {
    if(comprobarDireccion(cartaOrdinariaDTO)) {
      throw new Exception("Algún interviniente u ocupante no tenía direccion principal y no se ha enviado ninguna carta");
    }
    if (cartaOrdinariaDTO.getIdExpediente() != null &&  cartaOrdinariaDTO.getIdExpediente() != 0) {
      if (cartaOrdinariaDTO.getIdsIntervinientes() != null && cartaOrdinariaDTO.getIdsIntervinientes().size() > 0) {
        for (var idInterviniente : cartaOrdinariaDTO.getIdsIntervinientes()) {
          Interviniente interviniente = intervinienteRepository.findById(idInterviniente).orElse(null);
          if (interviniente != null) {
            for (var direccion : interviniente.getDirecciones()) {
              if (direccion.getPrincipal() == true) {
                //enviar la carta
                generarEnviosCarta(interviniente,null,direccion,cartaOrdinariaDTO,usuario);
              }
            }
          }
        }
        return new CartaOrdinariaRespuestaDTO("Guardado correctamente");
      }
    } else if (cartaOrdinariaDTO.getIdExpedientePN() != null && cartaOrdinariaDTO.getIdExpedientePN() != 0) {
      if (cartaOrdinariaDTO.getIdsOcupantes() != null && cartaOrdinariaDTO.getIdsOcupantes().size() > 0) {
        for (var idOcupante : cartaOrdinariaDTO.getIdsOcupantes()) {
          Ocupante ocupante = ocupanteRepository.findById(idOcupante).orElse(null);
          if (ocupante != null) {
            for (var direccion : ocupante.getDirecciones()) {
              if (direccion.getPrincipal() == true) {
                generarEnviosCarta(null,ocupante,direccion,cartaOrdinariaDTO,usuario);
              }
            }
          }
        }
        return new CartaOrdinariaRespuestaDTO("Guardado correctamente");
      }
    }

    throw new Exception("No se han podido generar envíos porque no se han introducido ocupantes ni intervinientes");
  }

  @Override
  public BurofaxRespuestaDTO envioBurofax(BurofaxDTO burofaxDTO) {
    // TODO
    // Definir que y como se va a enviar
    // A que plataformas se envía el Burofax, y definir el modelo de salida.

    // Se puede hacer desde una query en intervinientes
    // O acoplando los repositorios a este servicio para hacer la busqueda en ellos de los datos necesarios
    // Se recibiría el id de Expediente desde el Front y se haría la consulta de los intervinientes para devolver el listado completo
    // de los intervinientes asociados a ese expediente
    // TODO crear el evento una vez generado
    return null;
  }

  @Override
  public WhatsappWebRespuestaDTO envioWhatsappWeb(WhatsappWebDTO burofaxDTO) {
    return null;
  }

  /**
   * Peticion y obtencion del Token de Salesforce para uso de su API
   *
   * @return Token
   */
  private String getTokenSalesforce() {
    // Obtenemos el Token desde Salesforce
    ResponseEntity<RespuestaTokenSalesforceDTO> tokenSalesforce = salesforceTokenClient.tokenSalesforce(peticionToken);

    log.debug("Token Salesforce: " + tokenSalesforce.getBody().getAccessToken());
    String token = tokenSalesforce.getBody().getTokenType() + " " + tokenSalesforce.getBody().getAccessToken();

    return token;
  }

  /**
   *
   * @param usuario
   * @param tokenId
   * @param respuestaStatus
   * @return EnvioSMS
   */
  private EnvioSMS generateEntityEnvioSMS(Usuario usuario, String tokenId, RespuestaStatusSMSDTO respuestaStatus) {
    EnvioSMS envioSMS = new EnvioSMS();
    envioSMS.setToken(tokenId);
    envioSMS.setUsuario(usuario);
    envioSMS.setMessage(respuestaStatus.getMessage());
    envioSMS.setCount(respuestaStatus.getCount());
    envioSMS.setExcludedCount(respuestaStatus.getExcludedCount());
    envioSMS.setCreateDate(respuestaStatus.getCreateDate());
    envioSMS.setCompleteDate(respuestaStatus.getCompleteDate());
    envioSMS.setStatus(respuestaStatus.getStatus());
    return envioSMS;
  }

  /**
   * Obtener un Interviniente por su ID
   *
   * @param idInterviniente
   * @return Interviniente
   * @throws NotFoundException
   */
  private Interviniente getInterviniente(Integer idInterviniente) throws NotFoundException {
    return intervinienteRepository.findById(idInterviniente)
	    .orElseThrow(() -> new NotFoundException("Interviniente", idInterviniente));
  }

  /**
   * Obtener un Expediente por el ID Concatenado
   *
   * @param idConcatenadoExpediente
   * @return Expediente
   * @throws NotFoundException
   */
  private Expediente getExpediente(String idConcatenadoExpediente) throws NotFoundException {
    return expedienteRepository.findByIdConcatenado(idConcatenadoExpediente)
	    .orElseThrow(() -> new NotFoundException("Expediente", idConcatenadoExpediente));
  }

  /**
   * Obtener el Contrato Representante del Interviniente
   *
   * @param interviniente
   * @param idConcatenadoExpediente
   * @return Contrato
   */
  private Contrato getContratoRepresentante(Interviniente interviniente, String idConcatenadoExpediente) {
    Contrato contrato = null;

    for (ContratoInterviniente contratoInterviniente : interviniente.getContratos()) {
	if(idConcatenadoExpediente.equals(contratoInterviniente.getContrato().getExpediente().getIdConcatenado()) && contratoInterviniente.getContrato().getEsRepresentante()) {
	  contrato = contratoInterviniente.getContrato();
	  break;
	}
    }

    return contrato;
  }

  private Contrato getContratoById(Integer idContrato) throws NotFoundException {
      return contratoRepository.findById(idContrato)
	      .orElseThrow(() -> new NotFoundException("Contrato", idContrato));
  }

}
