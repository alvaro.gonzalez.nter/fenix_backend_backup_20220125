package com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class DataMobileDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String mobileNumber;

  private String statusCode;

  private String message;

  private String standardStatusCode;

  private String description;

}
