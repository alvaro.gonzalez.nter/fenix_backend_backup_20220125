package com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Component
public class ContactAttributes implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("SubscriberAttributes")
  private SubscriberAttributes subscriberAttributes;

}
