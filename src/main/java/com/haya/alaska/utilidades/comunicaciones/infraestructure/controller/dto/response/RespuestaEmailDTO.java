package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class RespuestaEmailDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  public String requestId;

  public List<ResponsesOutputDto> responses;

}
