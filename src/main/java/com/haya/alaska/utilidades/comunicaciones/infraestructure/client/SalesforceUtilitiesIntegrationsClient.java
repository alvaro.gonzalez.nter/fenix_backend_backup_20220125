package com.haya.alaska.utilidades.comunicaciones.infraestructure.client;

import java.util.List;

import org.json.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.RespuestaEmailBatchDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.RespuestaSMSDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.RespuestaStatusSMSDTO;

@FeignClient(name = "salesforce-utilities-integrations", url = "${integraciones.sf.api}")
public interface SalesforceUtilitiesIntegrationsClient {

  //EMAIL
  @PostMapping("${integraciones.sf.send-email}")
  ResponseEntity<RespuestaEmailBatchDTO> envioEmailSalesforce(@RequestHeader HttpHeaders httpHeaders, @RequestBody List<JSONObject> datosEmails);

  //SMS
  @PostMapping("${integraciones.sf.send-sms}")
  ResponseEntity<RespuestaSMSDTO> envioSMSSalesforce(@RequestHeader HttpHeaders httpHeaders, @RequestBody List<JSONObject> datosSMS);

  //Status SMS
  //@RequestMapping(value = "https://mcckgv97c6cxzn2q16gxb8dvnzy1.rest.marketingcloudapis.com/sms/v1/messageContact/MzIyOjc4OjA/deliveries/{idSms}", method = RequestMethod.POST)
  @PostMapping("/sms/v1/messageContact/MzIyOjc4OjA/deliveries/{tokenIdSms}")
  ResponseEntity<RespuestaStatusSMSDTO> statusSMSSalesforce(@RequestHeader HttpHeaders httpHeaders, @PathVariable("tokenIdSms") String tokenIdSms);

}
