package com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto;

import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.utilidades.comunicaciones.application.domain.EnvioCarta;
import java.util.Collection;
import java.util.LinkedHashSet;

public class CartaExcelDTO<T> extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    cabeceras.add("Modelo");
    cabeceras.add("Nombre");
    cabeceras.add("Apellidos");
    cabeceras.add("Direccion");
    cabeceras.add("Código postal");
    cabeceras.add("Población");
    cabeceras.add("Provincia");
    cabeceras.add("Expediente");
    cabeceras.add("Contrato");
    cabeceras.add("Fecha");
    cabeceras.add("Cliente");
    cabeceras.add("Nombre gestor");
    cabeceras.add("Teléfono gestor");
    cabeceras.add("Correo gestor");
    cabeceras.add("Producto");
    cabeceras.add("Importe deuda contrato");
    cabeceras.add("Número juzgado");
    cabeceras.add("Plaza juzgado");
    cabeceras.add("Procedimiento");
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }

  public CartaExcelDTO() {
    this.add("Modelo",null);
    this.add("Nombre",null);
    this.add("Apellidos",null);
    this.add("Direccion",null);
    this.add("Código postal",null);
    this.add("Población",null);
    this.add("Provincia",null);
    this.add("Expediente",null);
    this.add("Contrato",null);
    this.add("Fecha",null);
    this.add("Cliente",null);
    this.add("Nombre gestor",null);
    this.add("Teléfono gestor",null);
    this.add("Correo gestor",null);
    this.add("Producto",null);
    this.add("Importe deuda contrato",null);
    this.add("Número juzgado",null);
    this.add("Plaza juzgado",null);
    this.add("Procedimiento",null);
  }

  public CartaExcelDTO(
    EnvioCarta envioCarta) {
    this.add("Modelo",envioCarta.getModelo().getKey());
    this.add("Nombre",envioCarta.getNombre());
    this.add("Apellidos",envioCarta.getApellidos());
    this.add("Direccion",envioCarta.getDireccion());
    this.add("Código postal",envioCarta.getCodigoPostal());
    this.add("Población",envioCarta.getPoblacion());
    this.add("Provincia",envioCarta.getProvincia());
    this.add("Expediente",envioCarta.getExpediente());
    this.add("Contrato",envioCarta.getContrato());
    this.add("Fecha",envioCarta.getFecha());
    this.add("Cliente",envioCarta.getCliente());
    this.add("Nombre gestor",envioCarta.getNombreGestor());
    this.add("Teléfono gestor",envioCarta.getTelefonoGestor());
    this.add("Correo gestor",envioCarta.getCorreoGestor());
    this.add("Producto",envioCarta.getProducto());
    this.add("Importe deuda contrato",envioCarta.getImporteDeudaContrato());
    this.add("Número juzgado",envioCarta.getNumeroJuzgado());
    this.add("Plaza juzgado",envioCarta.getPlazaJuzgado());
    this.add("Procedimiento",envioCarta.getProcedimiento());
  }

}
