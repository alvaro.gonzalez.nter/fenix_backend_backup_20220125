package com.haya.alaska.utilidades.comunicaciones.application;

import java.util.List;

import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaBurofaxDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaCartaDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaEmailDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaSMSDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.PlantillaFormalizacionDTO;

public interface PlantillasUseCase {

  PlantillaSMSDTO obtenerPlantillaSMS(Integer idPlantillaSMS);

  List<PlantillaSMSDTO> obtenerListadoPlantillaSMS();

  PlantillaFormalizacionDTO obtenerPlantillaFormalizacion(Integer idPlantillaEmail);

  List<PlantillaFormalizacionDTO> obtenerListadoPlantillaFormalizacion();

  PlantillaEmailDTO obtenerPlantillaEmail(Integer idPlantillaEmail);

  List<PlantillaEmailDTO> obtenerListadoPlantillaEmail();

  PlantillaCartaDTO obtenerPlantillaCarta(Integer idPlantillaCarta);

  List<PlantillaCartaDTO> obtenerListadoPlantillaCarta();

  PlantillaBurofaxDTO obtenerPlantillaBurofax(Integer idPlantillaBurofax);

  List<PlantillaBurofaxDTO> obtenerListadoPlantillaBurofax();

}
