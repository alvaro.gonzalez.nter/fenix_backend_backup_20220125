package com.haya.alaska.utilidades.comunicaciones.application;

import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SendEmailDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SendSmsDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.BurofaxDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.CartaOrdinariaDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.EnvioEmailErrors;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.WhatsappWebDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.*;

import java.io.IOException;
import java.util.List;

public interface ComunicacionesUseCase {

  RespuestaEmailDTO envioEmail(String key, SendEmailDTO datosEmails, Usuario usuario) throws IOException, InterruptedException, NotFoundException, NotFoundException, com.haya.alaska.shared.exceptions.InterruptedException, RequiredValueException;

  RespuestaEmailDTO envioEmailAsincrono(String key, SendEmailDTO datosEmails, Usuario usuario) throws IOException, InterruptedException, NotFoundException, NotFoundException, com.haya.alaska.shared.exceptions.InterruptedException, RequiredValueException;

  RespuestaEmailDTO envioEmailPortalDeudor(String key,SendEmailDTO datosEmails,Usuario usuario) throws IOException, InterruptedException, NotFoundException, RequiredValueException;

  RespuestaEmailBatchDTO envioEmailBatch(String key, List<SendEmailDTO> datosEmails, Usuario usuario) throws IOException, InterruptedException, NotFoundException, RequiredValueException;

  RespuestaSMSDTO envioSMS(String codigoPlantilla, SendSmsDTO datosSMS, Usuario usuario) throws IOException, NotFoundException, RequiredValueException;

  RespuestaStatusSMSDTO statusSMS(String tokenIdSms) throws IOException, InterruptedException, com.haya.alaska.shared.exceptions.InterruptedException;

  List<EnvioEmailErrors> checkEmailErrors(String requestId);

  CartaOrdinariaRespuestaDTO enviarCartaOrdinaria(CartaOrdinariaDTO cartaOrdinariaDTO, Usuario usuario) throws Exception;

  CartaOrdinariaRespuestaDTO generarEnviosCartaBatch(CartaOrdinariaBatchDTO carta, Usuario usuario) throws Exception;

  BurofaxRespuestaDTO envioBurofax(BurofaxDTO burofaxDTO);

  WhatsappWebRespuestaDTO envioWhatsappWeb(WhatsappWebDTO burofaxDTO);

}
