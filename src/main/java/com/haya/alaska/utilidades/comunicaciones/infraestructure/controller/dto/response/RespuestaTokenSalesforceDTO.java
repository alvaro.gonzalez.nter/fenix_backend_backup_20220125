package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RespuestaTokenSalesforceDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("access_token")
  private String accessToken;

  @JsonProperty("token_type")
  private String tokenType;

  @JsonProperty("expires_in")
  private Integer expiresIn;

  private String scope;

  @JsonProperty("soap_instance_url")
  private String soapInstanceUrl;

  @JsonProperty("rest_instance_url")
  private String restInstanceUrl;

}
