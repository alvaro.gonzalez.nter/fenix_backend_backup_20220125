package com.haya.alaska.utilidades.comunicaciones.infraestructure.mapper;

import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SendEmailDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SendSmsDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SubscriberAttributes;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.Subscribers;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ComunicacionesMapper {


  /**
   * Convertidor a Objeto Plantilla de Salesforce para el Envio de 1 Email
   *
   * @param datosEmails
   * @return JSONObject
   */
  public JSONObject convertEmailToFormatSalesforce(SendEmailDTO datosEmails) {
    JSONObject jsonTo = new JSONObject();
    JSONObject jsonEmail = new JSONObject();
    JSONObject jsonContactAttributes = new JSONObject();

    jsonEmail.put("Address", datosEmails.getAddress());
    jsonEmail.put("SubscriberKey", datosEmails.getSubscriberKey());

    jsonContactAttributes.put("SubscriberAttributes", checkAndParseEmailJsonContactAttributes(datosEmails.getContactAttributes().getSubscriberAttributes()));
    jsonEmail.put("ContactAttributes", jsonContactAttributes);

    jsonTo.put("To", jsonEmail);

    return jsonTo;
  }


  /**
   * Convertidor a Objeto de Plantilla de Salesforce para el envio de Emails
   *
   * @param List<SendEmailDTO>
   * @return JSONObject
   */
  public List<JSONObject> convertEmailBatchToFormatSalesforce(List<SendEmailDTO> emails) {
    List<JSONObject> listEmails = new ArrayList<JSONObject>();

    for (SendEmailDTO datos : emails) {
      JSONObject jsonTo = new JSONObject();
      JSONObject jsonEmail = new JSONObject();
      JSONObject jsonContactAttributes = new JSONObject();

      jsonEmail.put("Address", datos.getAddress());
      jsonEmail.put("SubscriberKey", datos.getSubscriberKey());

      jsonContactAttributes.put("SubscriberAttributes", checkAndParseEmailJsonContactAttributes(datos.getContactAttributes().getSubscriberAttributes()));
      jsonEmail.put("ContactAttributes", jsonContactAttributes);
      jsonTo.put("To", jsonEmail);
      listEmails.add(jsonTo);
    }

    return listEmails;
  }

  /**
   * Convertidor a Objeto de Plantilla de Salesforce de la Peticion de envio de SMS
   *
   * @param List<SendSmsDTO>
   * @return JSONObject
   */
  public JSONObject convertSMSToFormatSalesforce(SendSmsDTO sms) {

    JSONObject jsonSms = new JSONObject();
    JSONObject jsonAttributes = new JSONObject();
    List<JSONObject> listSubscribers = new ArrayList<JSONObject>();

    jsonSms.put("Subscribe", sms.isSubscribe());
    jsonSms.put("Resubscribe", sms.isResubscribe());

    for (Subscribers subscriber : sms.getSubscribers()) {
      JSONObject jsonSubscribers = new JSONObject();
      jsonSubscribers.put("MobileNumber", subscriber.getMobileNumber());
      jsonSubscribers.put("SubscriberKey", subscriber.getSubscriberKey());
      if(subscriber.getAttributes().getTextoSMS() != null && !subscriber.getAttributes().getTextoSMS().isEmpty()) {
	  jsonAttributes.put("textoSMS", subscriber.getAttributes().getTextoSMS());
      }
      if(subscriber.getAttributes().getCliente() != null && !subscriber.getAttributes().getCliente().isEmpty()) {
	  jsonAttributes.put("cliente", subscriber.getAttributes().getCliente());
      }
      if(subscriber.getAttributes().getTelefono() != null && !subscriber.getAttributes().getTelefono().isEmpty()) {
	  jsonAttributes.put("telefono", subscriber.getAttributes().getTelefono());
      }
      if(subscriber.getAttributes().getNombreGestor() != null && !subscriber.getAttributes().getNombreGestor().isEmpty()) {
	  jsonAttributes.put("nombre_gestor", subscriber.getAttributes().getNombreGestor());
      }
      jsonSubscribers.put("Attributes", jsonAttributes);
      listSubscribers.add(jsonSubscribers);
    }

    jsonSms.put("Subscribers", listSubscribers);
    jsonSms.put("keyword", sms.getKeyword());

    return jsonSms;
  }

  private JSONObject checkAndParseEmailJsonContactAttributes(SubscriberAttributes subscriberAttributes) {
      JSONObject jsonSubscriberAttributes = new JSONObject();
      // Comprobamos la existencia de Subject
      if(subscriberAttributes.getSubject() != null && !subscriberAttributes.getSubject().isEmpty()) {
	  jsonSubscriberAttributes.put("Subject", subscriberAttributes.getSubject());
      }

      // Comprobamos la existencia de textoLibre. Este Email no usa Plantilla
      if(subscriberAttributes.getTextoEmail() != null && !subscriberAttributes.getTextoEmail().isEmpty()) {
	  jsonSubscriberAttributes.put("textoEmail", subscriberAttributes.getTextoEmail());
      }

      // Comprobamos la existencia de Contrato
      if(subscriberAttributes.getContrato() != null && !subscriberAttributes.getContrato().isEmpty()) {
	  jsonSubscriberAttributes.put("Contrato", subscriberAttributes.getContrato());
      }

      // Comprobamos la existencia de Cliente
      if(subscriberAttributes.getCliente() != null && !subscriberAttributes.getCliente().isEmpty()) {
	  jsonSubscriberAttributes.put("Cliente", subscriberAttributes.getCliente());
      }

      // Comprobamos la existencia de Nombre
      if(subscriberAttributes.getNombre() != null && !subscriberAttributes.getNombre().isEmpty()) {
	  jsonSubscriberAttributes.put("Nombre", subscriberAttributes.getNombre());
      }

      // Comprobamos la existencia de Apellido
      if(subscriberAttributes.getApellido() != null && !subscriberAttributes.getApellido().isEmpty()) {
	  jsonSubscriberAttributes.put("Apellido", subscriberAttributes.getApellido());
      }

      // Comprobamos la existencia de Telefono
      if(subscriberAttributes.getTelefono() != null && !subscriberAttributes.getTelefono().isEmpty()) {
	  jsonSubscriberAttributes.put("Telefono", subscriberAttributes.getTelefono());
      }

      // Comprobamos la existencia de PortalCliente
      if(subscriberAttributes.getPortalCliente() != null && !subscriberAttributes.getPortalCliente().isEmpty()) {
	  jsonSubscriberAttributes.put("PortalCliente", subscriberAttributes.getPortalCliente());
      }

      // Comprobamos la existencia de Campana
      if(subscriberAttributes.getCampana() != null && !subscriberAttributes.getCampana().isEmpty()) {
	  jsonSubscriberAttributes.put("Campana", subscriberAttributes.getCampana());
      }

      // Comprobamos la existencia de Importe
      if(subscriberAttributes.getImporte() != null && !subscriberAttributes.getImporte().isEmpty()) {
	  jsonSubscriberAttributes.put("Importe", subscriberAttributes.getImporte());
      }

      // Comprobamos la existencia de Producto
      if(subscriberAttributes.getProducto() != null && !subscriberAttributes.getProducto().isEmpty()) {
	  jsonSubscriberAttributes.put("Producto", subscriberAttributes.getProducto());
      }

      // Comprobamos la existencia de Nombre Gestor
      if(subscriberAttributes.getNombreGestor() != null && !subscriberAttributes.getNombreGestor().isEmpty()) {
	  jsonSubscriberAttributes.put("Nombre_Gestor", subscriberAttributes.getNombreGestor());
      }

      // Comprobamos la existencia de Telefono Gestor
      if(subscriberAttributes.getTelefonoGestor() != null && !subscriberAttributes.getTelefonoGestor().isEmpty()) {
	  jsonSubscriberAttributes.put("Telefono_Gestor", subscriberAttributes.getTelefonoGestor());
      }

      // Comprobamos la existencia de Correo Gestor
      if(subscriberAttributes.getCorreoGestor() != null && !subscriberAttributes.getCorreoGestor().isEmpty()) {
	  jsonSubscriberAttributes.put("Correo_Gestor", subscriberAttributes.getCorreoGestor());
      }

      // Comprobamos la existencia de Juzgado
      if(subscriberAttributes.getJuzgado() != null && !subscriberAttributes.getJuzgado().isEmpty()) {
	  jsonSubscriberAttributes.put("Juzgado", subscriberAttributes.getJuzgado());
      }

      // Comprobamos la existencia de Plaza Juzgado
      if(subscriberAttributes.getPlazaJuzgado() != null && !subscriberAttributes.getPlazaJuzgado().isEmpty()) {
	  jsonSubscriberAttributes.put("PlazaJuzgado", subscriberAttributes.getPlazaJuzgado());
      }

      // Comprobamos la existencia de Procedimiento
      if(subscriberAttributes.getProcedimiento() != null && !subscriberAttributes.getProcedimiento().isEmpty()) {
	  jsonSubscriberAttributes.put("Procedimiento", subscriberAttributes.getProcedimiento());
      }

      // Comprobamos la existencia de FirstName
      if(subscriberAttributes.getFirstName() != null && !subscriberAttributes.getFirstName().isEmpty()) {
	  jsonSubscriberAttributes.put("FirstName", subscriberAttributes.getFirstName());
      }

      // Comprobamos la existencia de EmailBody
      if(subscriberAttributes.getEmailBody() != null && !subscriberAttributes.getEmailBody().isEmpty()) {
	  jsonSubscriberAttributes.put("EmailBody", subscriberAttributes.getEmailBody());
      }

      if (subscriberAttributes.getUsuario() != null && !subscriberAttributes.getUsuario().isEmpty()) {
          jsonSubscriberAttributes.put("User", subscriberAttributes.getUsuario());
      }

      if (subscriberAttributes.getPassword() != null && !subscriberAttributes.getPassword().isEmpty()) {
	  jsonSubscriberAttributes.put("Pass", subscriberAttributes.getPassword());
      }

      if (subscriberAttributes.getUrlPassword() != null && !subscriberAttributes.getUrlPassword().isEmpty()) {
	  jsonSubscriberAttributes.put("URLpass", subscriberAttributes.getUrlPassword());
      }

    if (subscriberAttributes.getLink() != null && !subscriberAttributes.getLink().isEmpty()) {
      jsonSubscriberAttributes.put("Link", subscriberAttributes.getLink());
    }

    if (subscriberAttributes.getGestorFormalizacion() != null && !subscriberAttributes.getGestorFormalizacion().isEmpty()) {
      jsonSubscriberAttributes.put("GestorFormalizacion", subscriberAttributes.getGestorFormalizacion());
    }
    if (subscriberAttributes.getFechaVencimientoSancion() != null && !subscriberAttributes.getFechaVencimientoSancion().isEmpty()) {
      jsonSubscriberAttributes.put("FechaVencimientoSancion", subscriberAttributes.getFechaVencimientoSancion());
    }
    if (subscriberAttributes.getIdConcatenado() != null && !subscriberAttributes.getIdConcatenado().isEmpty()) {
      jsonSubscriberAttributes.put("Des_id_concatenado", subscriberAttributes.getIdConcatenado());
    }
    if (subscriberAttributes.getIdPropuesta() != null && !subscriberAttributes.getIdPropuesta().isEmpty()) {
      jsonSubscriberAttributes.put("IdPropuesta", subscriberAttributes.getIdPropuesta());
    }
    if (subscriberAttributes.getTipoPropuesta() != null && !subscriberAttributes.getTipoPropuesta().isEmpty()) {
      jsonSubscriberAttributes.put("TipoPropuesta", subscriberAttributes.getTipoPropuesta());
    }
    if (subscriberAttributes.getApellidos() != null && !subscriberAttributes.getApellidos().isEmpty()) {
      jsonSubscriberAttributes.put("Apellidos", subscriberAttributes.getApellidos());
    }
    if (subscriberAttributes.getRazonSocial() != null && !subscriberAttributes.getRazonSocial().isEmpty()) {
      jsonSubscriberAttributes.put("RazonSocial", subscriberAttributes.getRazonSocial());
    }
    if (subscriberAttributes.getCorreo() != null && !subscriberAttributes.getCorreo().isEmpty()) {
      jsonSubscriberAttributes.put("Correo", subscriberAttributes.getCorreo());
    }
    if (subscriberAttributes.getAlquiler() != null && !subscriberAttributes.getAlquiler().isEmpty()) {
      jsonSubscriberAttributes.put("Alquiler", subscriberAttributes.getAlquiler());
    }
    if (subscriberAttributes.getFechaFirma() != null && !subscriberAttributes.getFechaFirma().isEmpty()) {
      jsonSubscriberAttributes.put("FechaFirma", subscriberAttributes.getFechaFirma());
    }
    if (subscriberAttributes.getHoraFirma() != null && !subscriberAttributes.getHoraFirma().isEmpty()) {
      jsonSubscriberAttributes.put("HoraFirma", subscriberAttributes.getHoraFirma());
    }
    if (subscriberAttributes.getNotario() != null && !subscriberAttributes.getNotario().isEmpty()) {
      jsonSubscriberAttributes.put("Notario", subscriberAttributes.getNotario());
    }
    if (subscriberAttributes.getMunicipioNotaria() != null && !subscriberAttributes.getMunicipioNotaria().isEmpty()) {
      jsonSubscriberAttributes.put("MunicipioNotaria", subscriberAttributes.getMunicipioNotaria());
    }
    if (subscriberAttributes.getDireccionNotaria() != null && !subscriberAttributes.getDireccionNotaria().isEmpty()) {
      jsonSubscriberAttributes.put("DireccionNotaria", subscriberAttributes.getDireccionNotaria());
    }
    if (subscriberAttributes.getTelefonoOficialNotaria() != null && !subscriberAttributes.getTelefonoOficialNotaria().isEmpty()) {
      jsonSubscriberAttributes.put("TelefonoOficialNotaria", subscriberAttributes.getTelefonoOficialNotaria());
    }
    if (subscriberAttributes.getEmailOficialNotaria() != null && !subscriberAttributes.getEmailOficialNotaria().isEmpty()) {
      jsonSubscriberAttributes.put("EmailOficialNotaria", subscriberAttributes.getEmailOficialNotaria());
    }
    if (subscriberAttributes.getTipoActivo() != null && !subscriberAttributes.getTipoActivo().isEmpty()) {
      jsonSubscriberAttributes.put("TipoActivo", subscriberAttributes.getTipoActivo());
    }
    if (subscriberAttributes.getDireccion() != null && !subscriberAttributes.getDireccion().isEmpty()) {
      jsonSubscriberAttributes.put("Direccion", subscriberAttributes.getDireccion());
    }
    if (subscriberAttributes.getMunicipio() != null && !subscriberAttributes.getMunicipio().isEmpty()) {
      jsonSubscriberAttributes.put("Municipio", subscriberAttributes.getMunicipio());
    }
    if (subscriberAttributes.getProvincia() != null && !subscriberAttributes.getProvincia().isEmpty()) {
      jsonSubscriberAttributes.put("Provincia", subscriberAttributes.getProvincia());
    }
    if (subscriberAttributes.getFinca() != null && !subscriberAttributes.getFinca().isEmpty()) {
      jsonSubscriberAttributes.put("Finca", subscriberAttributes.getFinca());
    }
    if (subscriberAttributes.getRegistroDeLaPropiedad() != null && !subscriberAttributes.getRegistroDeLaPropiedad().isEmpty()) {
      jsonSubscriberAttributes.put("RegistroDeLaPropiedad", subscriberAttributes.getRegistroDeLaPropiedad());
    }
    if (subscriberAttributes.getNumeroProtocolo() != null && !subscriberAttributes.getNumeroProtocolo().isEmpty()) {
      jsonSubscriberAttributes.put("NumeroProtocolo", subscriberAttributes.getNumeroProtocolo());
    }
    if (subscriberAttributes.getProvinciaNotaria() != null && !subscriberAttributes.getProvinciaNotaria().isEmpty()) {
      jsonSubscriberAttributes.put("ProvinciaNotaria", subscriberAttributes.getProvinciaNotaria());
    }

      return jsonSubscriberAttributes;
  }
}
