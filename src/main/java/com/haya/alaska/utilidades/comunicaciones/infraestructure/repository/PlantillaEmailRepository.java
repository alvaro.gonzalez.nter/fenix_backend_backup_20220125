package com.haya.alaska.utilidades.comunicaciones.infraestructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.haya.alaska.utilidades.comunicaciones.application.domain.PlantillaEmail;

public interface PlantillaEmailRepository extends JpaRepository<PlantillaEmail, Integer>{
    
}

