package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlantillaFormalizacionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String nombrePlantilla;

    private String key;

    private String textoPlantilla;
}
