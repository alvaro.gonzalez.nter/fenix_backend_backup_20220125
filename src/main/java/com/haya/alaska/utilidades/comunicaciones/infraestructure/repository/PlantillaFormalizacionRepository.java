package com.haya.alaska.utilidades.comunicaciones.infraestructure.repository;

import com.haya.alaska.utilidades.comunicaciones.application.domain.PlantillaFormalizacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlantillaFormalizacionRepository extends JpaRepository<PlantillaFormalizacion, Integer> {
  Optional<PlantillaFormalizacion> findByKey(String key);
}
