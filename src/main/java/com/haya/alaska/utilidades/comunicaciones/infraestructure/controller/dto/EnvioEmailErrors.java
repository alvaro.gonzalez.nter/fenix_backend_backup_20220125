package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class EnvioEmailErrors implements Serializable {
    
  private static final long serialVersionUID = 1L;

  @JsonIgnore
  private Integer id;

  private Integer usuario;
  
  private String email;
    
  private String requestId;
    
  private Boolean batchHasErrors;

  private String recipientSendId;
  
  private Boolean hasErrors;   
  
  private String messages;
  
}