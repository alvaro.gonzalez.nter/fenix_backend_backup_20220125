package com.haya.alaska.utilidades.comunicaciones.application;

import java.util.ArrayList;
import java.util.List;

import com.haya.alaska.utilidades.comunicaciones.application.domain.*;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaBurofaxDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaCartaDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.PlantillaFormalizacionDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.repository.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaEmailDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.PlantillaSMSDTO;

@Service
public class PlantillasUseCaseImpl implements PlantillasUseCase {

    @Autowired
    PlantillaSMSRepository plantillaSMSRepository;

    @Autowired
    PlantillaEmailRepository plantillaEmailRepository;
    @Autowired
    PlantillaFormalizacionRepository plantillaFormalizacionRepository;

    @Autowired
    PlantillaCartaRepository plantillaCartaRepository;

    @Autowired
    PlantillaBurofaxRepository plantillaBurofaxRepository;

    @Override
    public PlantillaSMSDTO obtenerPlantillaSMS(Integer idPlantillaSMS) {
	PlantillaSMS plantillaSMS = plantillaSMSRepository.findById(idPlantillaSMS).orElseThrow();

	if(plantillaSMS != null) {
	    PlantillaSMSDTO plantillaSMSDTO = new PlantillaSMSDTO();
	    BeanUtils.copyProperties(plantillaSMS, plantillaSMSDTO);
	    return plantillaSMSDTO;
	}

	return null;
    }

    @Override
    public List<PlantillaSMSDTO> obtenerListadoPlantillaSMS() {
	List<PlantillaSMS> listPlantillasSMS = plantillaSMSRepository.findAll();
	List<PlantillaSMSDTO> listPlantillasSMSDTO = new ArrayList<>();

	for (PlantillaSMS plantillaSMS : listPlantillasSMS) {
	    PlantillaSMSDTO plantillaSMSDTO = new PlantillaSMSDTO();
	    BeanUtils.copyProperties(plantillaSMS, plantillaSMSDTO);
	    listPlantillasSMSDTO.add(plantillaSMSDTO);
	}

	return listPlantillasSMSDTO;
    }

  @Override
  public PlantillaFormalizacionDTO obtenerPlantillaFormalizacion(Integer idPlantillaEmail) {
    PlantillaFormalizacion plantillaEmail = plantillaFormalizacionRepository.findById(idPlantillaEmail).orElseThrow();

    if(plantillaEmail != null) {
      PlantillaFormalizacionDTO plantillaEmailDTO = new PlantillaFormalizacionDTO();
      BeanUtils.copyProperties(plantillaEmail, plantillaEmailDTO);
      return plantillaEmailDTO;
    }

    return null;
  }

  @Override
  public List<PlantillaFormalizacionDTO> obtenerListadoPlantillaFormalizacion() {
    List<PlantillaFormalizacion> listPlantillasEmail = plantillaFormalizacionRepository.findAll();
    List<PlantillaFormalizacionDTO> listplantillasEmailDTO = new ArrayList<>();

    for (PlantillaFormalizacion plantillaEmail : listPlantillasEmail) {
      PlantillaFormalizacionDTO plantillaEmailDTO = new PlantillaFormalizacionDTO();
      BeanUtils.copyProperties(plantillaEmail, plantillaEmailDTO);
      listplantillasEmailDTO.add(plantillaEmailDTO);
    }

    return listplantillasEmailDTO;
  }

  @Override
  public PlantillaEmailDTO obtenerPlantillaEmail(Integer idPlantillaEmail) {
      PlantillaEmail plantillaEmail = plantillaEmailRepository.findById(idPlantillaEmail).orElseThrow();

	if(plantillaEmail != null) {
	    PlantillaEmailDTO plantillaEmailDTO = new PlantillaEmailDTO();
	    BeanUtils.copyProperties(plantillaEmail, plantillaEmailDTO);
	    return plantillaEmailDTO;
	}

	return null;
  }

  @Override
  public List<PlantillaEmailDTO> obtenerListadoPlantillaEmail() {
      List<PlantillaEmail> listPlantillasEmail = plantillaEmailRepository.findAll();
	List<PlantillaEmailDTO> listplantillasEmailDTO = new ArrayList<>();

	for (PlantillaEmail plantillaEmail : listPlantillasEmail) {
	    PlantillaEmailDTO plantillaEmailDTO = new PlantillaEmailDTO();
	    BeanUtils.copyProperties(plantillaEmail, plantillaEmailDTO);
	    listplantillasEmailDTO.add(plantillaEmailDTO);
	}

	return listplantillasEmailDTO;
  }

  @Override
  public PlantillaCartaDTO obtenerPlantillaCarta(Integer idPlantillaCarta) {
    PlantillaCarta plantillaCarta = plantillaCartaRepository.findById(idPlantillaCarta).orElseThrow();

    if(plantillaCarta != null) {
      PlantillaCartaDTO plantillaCartaDTO = new PlantillaCartaDTO();
      BeanUtils.copyProperties(plantillaCarta, plantillaCartaDTO);
      return plantillaCartaDTO;
    }

    return null;
  }

  @Override
  public List<PlantillaCartaDTO> obtenerListadoPlantillaCarta() {
    List<PlantillaCarta> listPlantillasCarta = plantillaCartaRepository.findAll();
    List<PlantillaCartaDTO> listplantillasCartaDTO = new ArrayList<>();

    for (PlantillaCarta plantillaCarta : listPlantillasCarta) {
      PlantillaCartaDTO plantillaCartaDTO = new PlantillaCartaDTO();
      BeanUtils.copyProperties(plantillaCarta, plantillaCartaDTO);
      listplantillasCartaDTO.add(plantillaCartaDTO);
    }

    return listplantillasCartaDTO;
  }

  @Override
  public PlantillaBurofaxDTO obtenerPlantillaBurofax(Integer idPlantillaBurofax) {
    PlantillaBurofax plantillaBurofax = plantillaBurofaxRepository.findById(idPlantillaBurofax).orElseThrow();

    if(plantillaBurofax != null) {
      PlantillaBurofaxDTO plantillaBurofaxDTO = new PlantillaBurofaxDTO();
      BeanUtils.copyProperties(plantillaBurofax, plantillaBurofaxDTO);
      return plantillaBurofaxDTO;
    }

    return null;
  }

  @Override
  public List<PlantillaBurofaxDTO> obtenerListadoPlantillaBurofax() {
    List<PlantillaBurofax> listPlantillasBurofax = plantillaBurofaxRepository.findAll();
    List<PlantillaBurofaxDTO> listplantillasBurofaxDTO = new ArrayList<>();

    for (PlantillaBurofax plantillaBurofax : listPlantillasBurofax) {
      PlantillaBurofaxDTO plantillaBurofaxDTO = new PlantillaBurofaxDTO();
      BeanUtils.copyProperties(plantillaBurofax, plantillaBurofaxDTO);
      listplantillasBurofaxDTO.add(plantillaBurofaxDTO);
    }

    return listplantillasBurofaxDTO;
  }

}
