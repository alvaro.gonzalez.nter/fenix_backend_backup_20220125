package com.haya.alaska.utilidades.comunicaciones.application.domain;

import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.Data;
import lombok.ToString;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "MSTR_PLANTILLA_CARTA")
public class PlantillaCarta {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "DES_NOMBRE_PLANTILLA")
  private String nombrePlantilla;

  @Column(name = "DES_KEY")
  private String key;

  @Column(name = "DES_TEXTO_PLANTILLA")
  private String textoPlantilla;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MODELO_CARTA")
  @NotAudited
  private Set<EnvioCarta> enviosCarta = new HashSet<>();


}
