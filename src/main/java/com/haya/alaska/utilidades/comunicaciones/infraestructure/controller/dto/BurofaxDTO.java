package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BurofaxDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer interviniente;

  private String asuntoBurofax;

  private String mensaje;

  private String nombre;
  private String apellidos;
  private String direccion;
  private String codigoPostal;
  private String poblacion;
  private String provincia;
  private String expediente;
  private String contrato;
  private Date fechaDeHoy;
  private String cliente;
  private String nombreGestor;
  private String telefonoGestor;
  private String correoGestor;
  private String producto;
  private String importeDeudaContrato;
  private String nJuzgado;
  private String plazaDelJuzgado;
  private String procedimiento;
}
