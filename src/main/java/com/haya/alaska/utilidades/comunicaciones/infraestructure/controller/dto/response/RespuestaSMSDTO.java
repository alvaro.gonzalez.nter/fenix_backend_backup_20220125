package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response;

import java.io.Serializable;

import lombok.Data;

@Data
public class RespuestaSMSDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String tokenId;

}
