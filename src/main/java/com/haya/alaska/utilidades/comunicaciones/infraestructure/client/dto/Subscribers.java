package com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Subscribers {
  
  @JsonProperty("MobileNumber")
  private String mobileNumber;
  
  @JsonProperty("SubscriberKey")
  private String subscriberKey;
  
  @JsonProperty("Attributes")
  private Attributes attributes;
  
}
