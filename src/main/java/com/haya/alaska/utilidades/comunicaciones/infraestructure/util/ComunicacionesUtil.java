package com.haya.alaska.utilidades.comunicaciones.infraestructure.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class ComunicacionesUtil {

  public HttpHeaders addTokenHeaders(String token) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.add("Authorization", token);

    return headers;
  }

}
