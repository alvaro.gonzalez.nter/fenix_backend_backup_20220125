package com.haya.alaska.utilidades.comunicaciones.application.domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "MSTR_PLANTILLA_FORMALIZACION")
public class PlantillaFormalizacion implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "DES_NOMBRE_PLANTILLA")
  private String nombrePlantilla;

  @Column(name = "DES_KEY")
  private String key;

  @Column(name = "DES_TEXTO_PLANTILLA")
  private String textoPlantilla;
}
