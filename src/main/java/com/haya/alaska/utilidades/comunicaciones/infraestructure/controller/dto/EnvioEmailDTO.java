package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.usuario.domain.Usuario;

import lombok.Data;

@Data
@Component
public class EnvioEmailDTO implements Serializable {
    
  private static final long serialVersionUID = 1L;

  @JsonIgnore
  private Integer id;

  private Usuario usuario;
  
  private String email;
    
  private String requestId;
    
  private Boolean batchHasErrors;
    
  private String recipientSendId;
  
  private Boolean hasErrors;   
  
  private String messages;
  
}
