package com.haya.alaska.utilidades.comunicaciones.infraestructure.repository;

import com.haya.alaska.utilidades.comunicaciones.application.domain.PlantillaBurofax;
import com.haya.alaska.utilidades.comunicaciones.application.domain.PlantillaCarta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlantillaBurofaxRepository extends JpaRepository<PlantillaBurofax, Integer> {
  Optional<PlantillaCarta> findByKey(String key);
}
