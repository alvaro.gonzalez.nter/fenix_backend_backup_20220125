package com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Attributes {
    
  @JsonProperty("Expediente")
  private String idExpediente;
    
  @JsonProperty("Interviniente")
  private Integer idInterviniente;
  
  @JsonProperty("TextoSMS")
  private String textoSMS;
  
  @JsonProperty("Cliente")
  private String cliente;

  @JsonProperty("Telefono")
  private String telefono;

  @JsonProperty("NombreGestor")
  private String nombreGestor;

}
