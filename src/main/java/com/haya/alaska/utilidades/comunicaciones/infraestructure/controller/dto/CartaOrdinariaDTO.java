package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class CartaOrdinariaDTO {

  //Integer idModelo;
  String codigoPlantilla;
  Integer idCartera;
  Integer idContrato;
  Integer idProcedimiento;
  Integer idExpediente;
  Integer idExpedientePN;
  Set<Integer> idsIntervinientes;
  Set<Integer> idsOcupantes;
  private String texto;

}
