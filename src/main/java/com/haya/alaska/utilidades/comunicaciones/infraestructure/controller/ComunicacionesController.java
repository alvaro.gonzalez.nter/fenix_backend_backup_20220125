package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller;

import java.io.IOException;
import java.util.List;

import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.InterruptedException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.utilidades.comunicaciones.application.ComunicacionesUseCase;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SendEmailDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SendSmsDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.BurofaxDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.CartaOrdinariaDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.EnvioEmailErrors;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.WhatsappWebDTO;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/comunicaciones")
public class ComunicacionesController {

  @Autowired
  ComunicacionesUseCase comunicacionUseCaseImpl;

  @ApiOperation(value = "Envío SMS", notes = "Envío de SMS a partir de SalesForce")
  @PostMapping("/sms/{codigoPlantilla}")
  public ResponseEntity<RespuestaSMSDTO> envioSmsSalesforce(@PathVariable("codigoPlantilla") String codigoPlantilla, @RequestBody SendSmsDTO datosSMS, @ApiIgnore CustomUserDetails principal)
    throws IOException, NotFoundException, RequiredValueException {
    RespuestaSMSDTO respuestaSMSDTO = comunicacionUseCaseImpl.envioSMS(codigoPlantilla, datosSMS, (Usuario) principal.getPrincipal());
    return ResponseEntity.ok(respuestaSMSDTO);
  }

  @ApiOperation(value = "Envío email", notes = "Envío de email a partir de SalesForce")
  @PostMapping("/email/{key}")
  public ResponseEntity<RespuestaEmailDTO> envioMailSalesforce(@PathVariable("key") String key, @RequestBody SendEmailDTO datosEmail, @ApiIgnore CustomUserDetails principal)
    throws IOException, InterruptedException, NotFoundException, java.lang.InterruptedException, RequiredValueException {
    RespuestaEmailDTO respuestaEmailDTO = comunicacionUseCaseImpl.envioEmail(key, datosEmail, (Usuario) principal.getPrincipal());
    return ResponseEntity.ok(respuestaEmailDTO);
  }

  @ApiOperation(value = "Envío de varios emails", notes = "Envío de varios emails a partir de SalesForce")
  @PostMapping("/email/batch/{key}")
  public ResponseEntity<RespuestaEmailBatchDTO> envioMailBatchSalesforce(@PathVariable("key") String key, @RequestBody List<SendEmailDTO> datosEmail, @ApiIgnore CustomUserDetails principal)
    throws IOException, InterruptedException, NotFoundException, java.lang.InterruptedException, RequiredValueException {
    RespuestaEmailBatchDTO respuestaEmailDTO = comunicacionUseCaseImpl.envioEmailBatch(key, datosEmail, (Usuario) principal.getPrincipal());
    return ResponseEntity.ok(respuestaEmailDTO);
  }

  @ApiOperation(value = "Comprobar el estado del envío de SMS", notes = "Envío de SMS a partir de SalesForce")
  @GetMapping("/sms/status/{tokenIdSms}")
  public ResponseEntity<RespuestaStatusSMSDTO> statusSMSSalesforce(@PathVariable("tokenIdSms") String tokenIdSms)
    throws IOException, InterruptedException, java.lang.InterruptedException {
    RespuestaStatusSMSDTO respuestaStatusSMSDTO = comunicacionUseCaseImpl.statusSMS(tokenIdSms);

    if (respuestaStatusSMSDTO == null) {
      return ResponseEntity.notFound().build();
    }

    return ResponseEntity.ok(respuestaStatusSMSDTO);
  }

  @ApiOperation(value = "Comprobar los errores de envíos de Email", notes = "Obtener los emails que han obtenido un error en el envío mediante el Request ID")
  @GetMapping("/email/error/{requestId}")
  public ResponseEntity<List<EnvioEmailErrors>> checkEmailErrors(@PathVariable("requestId") String requestId)
    throws IOException, InterruptedException {
    List<EnvioEmailErrors> sendMailErrors = comunicacionUseCaseImpl.checkEmailErrors(requestId);

    if (sendMailErrors == null) {
      return ResponseEntity.notFound().build();
    }

    return ResponseEntity.ok(sendMailErrors);
  }

  @ApiOperation(value = "Envío de carta ordinaria", notes = "Envío de carta ordinaria")
  @PostMapping("/carta")
  public ResponseEntity<CartaOrdinariaRespuestaDTO> envioCartaOrdinaria(@RequestBody CartaOrdinariaDTO cartaOrdinariaDTO,
                                                                        @ApiIgnore CustomUserDetails principal) throws Exception {

    return ResponseEntity.ok(comunicacionUseCaseImpl.enviarCartaOrdinaria(cartaOrdinariaDTO, (Usuario) principal.getPrincipal()));
  }

  @ApiOperation(value = "Envío de cartas ordinarias", notes = "Envío de cartas ordinarias")
  @PostMapping("/carta/batch")
  public ResponseEntity<CartaOrdinariaRespuestaDTO> envioCartaOrdinariaBatch(@RequestBody CartaOrdinariaBatchDTO cartas,
                                                                             @ApiIgnore CustomUserDetails principal) throws Exception {

    return ResponseEntity.ok(comunicacionUseCaseImpl.generarEnviosCartaBatch(cartas, (Usuario) principal.getPrincipal()));
  }

  @ApiOperation(value = "Envío de burofax", notes = "Envío de burofax")
  @PostMapping("/burofax")
  public ResponseEntity<BurofaxRespuestaDTO> envioBurofax(@RequestBody BurofaxDTO burofaxDTO) {

    return ResponseEntity.ok(new BurofaxRespuestaDTO("Envío del Burofax correcto."));
  }

  @ApiOperation(value = "Envío de whatsapp mediante Whatsapp Web", notes = "Envío de whatsapp")
  @PostMapping("/whatsappweb")
  public ResponseEntity<WhatsappWebRespuestaDTO> envioBurofax(@RequestBody WhatsappWebDTO whatsappWebDTO) {

    return ResponseEntity.ok(new WhatsappWebRespuestaDTO("Envío del Wahtaspp correcto."));
  }


  @ApiOperation(value = "Envío email", notes = "Envío de email a partir de SalesForce")
  @PostMapping("/emailPortalDeudor/{key}")
  public ResponseEntity<RespuestaEmailDTO> envioMailBienvenidaDeudorSalesforce(@PathVariable("key") String key, @RequestBody SendEmailDTO datosEmail, @ApiIgnore CustomUserDetails principal)
    throws IOException, InterruptedException, NotFoundException, java.lang.InterruptedException, RequiredValueException {
    RespuestaEmailDTO respuestaEmailDTO = comunicacionUseCaseImpl.envioEmailPortalDeudor(key, datosEmail, (Usuario) principal.getPrincipal());
    return ResponseEntity.ok(respuestaEmailDTO);
  }



}
