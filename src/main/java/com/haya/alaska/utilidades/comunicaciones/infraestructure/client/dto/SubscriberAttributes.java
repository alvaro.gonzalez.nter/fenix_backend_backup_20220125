package com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Component
public class SubscriberAttributes implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("Subject")
  private String subject;

  // Plantillas

  @JsonProperty("Expediente")
  private String idExpediente;

  @JsonProperty("Interviniente")
  private Integer idInterviniente;

  @JsonProperty("Contrato")
  private String contrato;

  @JsonProperty("Cliente")
  private String cliente;

  @JsonProperty("Nombre")
  private String nombre;

  @JsonProperty("Apellido")
  private String apellido;

  @JsonProperty("Telefono")
  private String telefono;

  @JsonProperty("PortalCliente")
  private String portalCliente;

  @JsonProperty("Campana")
  private String campana;

  @JsonProperty("Producto")
  private String producto;

  @JsonProperty("Importe")
  private String importe;

  @JsonProperty("NombreGestor")
  private String nombreGestor;

  @JsonProperty("TelefonoGestor")
  private String telefonoGestor;

  @JsonProperty("CorreoGestor")
  private String correoGestor;

  @JsonProperty("Juzgado")
  private String juzgado;

  @JsonProperty("PlazaJuzgado")
  private String plazaJuzgado;

  @JsonProperty("Procedimiento")
  private String procedimiento;

  // Plantilla de Texto Libre

  @JsonProperty("TextoEmail")
  private String textoEmail;

  // Pendiente de Verificar - Borrar

  @JsonProperty("FirstName")
  private String firstName;

  @JsonProperty("EmailBody")
  private String emailBody;

  // Bloque Emails de Login-Pass-Recuperar Contraseña

  @JsonProperty("User")
  private String usuario;

  @JsonProperty("Pass")
  private String password;

  @JsonProperty("URLpass")
  private String urlPassword;

  @JsonProperty("Link")
  private String link;

  //Plantillas de Formalizacion

  @JsonProperty("GestorFormalizacion")
  private String gestorFormalizacion;
  @JsonProperty("FechaVencimientoSancion")
  private String fechaVencimientoSancion;
  @JsonProperty("Des_id_concatenado")
  private String idConcatenado;
  @JsonProperty("IdPropuesta")
  private String idPropuesta;


  @JsonProperty("TipoPropuesta")
  private String tipoPropuesta;
  @JsonProperty("Apellidos")
  private String apellidos;
  @JsonProperty("RazonSocial")
  private String razonSocial;
  @JsonProperty("Correo")
  private String correo;
  @JsonProperty("Alquiler")
  private String alquiler;
  @JsonProperty("FechaFirma")
  private String fechaFirma;
  @JsonProperty("HoraFirma")
  private String horaFirma;
  @JsonProperty("Notario")
  private String notario;
  @JsonProperty("MunicipioNotaria")
  private String municipioNotaria;
  @JsonProperty("DireccionNotaria")
  private String direccionNotaria;
  @JsonProperty("TelefonoOficialNotaria")
  private String telefonoOficialNotaria;
  @JsonProperty("EmailOficialNotaria")
  private String emailOficialNotaria;
  @JsonProperty("TipoActivo")
  private String tipoActivo;
  @JsonProperty("Direccion")
  private String direccion;
  @JsonProperty("Municipio")
  private String municipio;
  @JsonProperty("Provincia")
  private String provincia;
  @JsonProperty("Finca")
  private String finca;
  @JsonProperty("RegistroDeLaPropiedad")
  private String registroDeLaPropiedad;


  @JsonProperty("NumeroProtocolo")
  private String numeroProtocolo;
  @JsonProperty("ProvinciaNotaria")
  private String provinciaNotaria;
}
