package com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SendSmsDTO  implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("Subscribe")
  private boolean subscribe;

  @JsonProperty("Resubscribe")
  private boolean resubscribe;

  @JsonProperty("Subscribers")
  private List<Subscribers> subscribers;

  private String keyword;

  @JsonProperty("Override")
  private boolean override;

  private String messageText;

}
