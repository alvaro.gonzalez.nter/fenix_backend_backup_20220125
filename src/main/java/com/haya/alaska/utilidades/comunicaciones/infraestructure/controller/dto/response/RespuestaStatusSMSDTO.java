package com.haya.alaska.utilidades.comunicaciones.infraestructure.controller.dto.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.DataMobileDTO;

import lombok.Data;

@Data
public class RespuestaStatusSMSDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String message;

  private Integer count;

  private Integer excludedCount;

  private Date createDate;

  private Date completeDate;

  private String status;

  private List<DataMobileDTO> tracking;

}
