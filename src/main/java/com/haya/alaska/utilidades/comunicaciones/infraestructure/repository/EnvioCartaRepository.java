package com.haya.alaska.utilidades.comunicaciones.infraestructure.repository;

import com.haya.alaska.utilidades.comunicaciones.application.domain.EnvioCarta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnvioCartaRepository extends JpaRepository<EnvioCarta, Integer> {
  List<EnvioCarta> findAllByEnviadoFalse();
}
