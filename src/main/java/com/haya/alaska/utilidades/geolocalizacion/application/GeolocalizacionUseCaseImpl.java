package com.haya.alaska.utilidades.geolocalizacion.application;

import com.haya.alaska.busqueda.application.BusquedaUseCase;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.BusquedaBienDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.BusquedaExpedienteDto;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCase;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.utilidades.geolocalizacion.infraestructure.controller.dto.GeolocalizacionOutputDTO;
import com.haya.alaska.utilidades.geolocalizacion.infraestructure.mapper.GeolocalizacionMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GeolocalizacionUseCaseImpl implements GeolocalizacionUseCase {
  @Autowired
  private BusquedaUseCase busquedaUseCase;
  @Autowired
  private ExpedientePosesionNegociadaCase expedientePosesionNegociadaCase;
  @Autowired
  private GeolocalizacionMapper geolocalizacionMapper;
  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public ListWithCountDTO<GeolocalizacionOutputDTO> filter(Integer idCliente, Integer idCartera, String idExpediente, Integer idLocalidad, Integer idProvincia, Integer idMunicipio, String tipoGarantia, String uso, Usuario usuario, Integer size, Integer page, String orderField, String orderDirection) throws NotFoundException {
    List<Expediente> expedientes = busquedaUseCase.getDataExpedientesFilter(crearBusquedaDto(idCliente, idCartera, idExpediente, idLocalidad, idProvincia, idMunicipio, tipoGarantia, uso), usuario, orderField, orderDirection);
    List<ExpedientePosesionNegociada> expedientePosesionNegociadas = expedientePosesionNegociadaCase.filtrarExpedientesPosesionNegociadaBusqueda(crearBusquedaDto(idCliente, idCartera, idExpediente, idLocalidad, idProvincia, idMunicipio, tipoGarantia, uso), usuario, null, null, null, null, null, null, null, null, null, null, null, orderField, orderDirection);
    List<GeolocalizacionOutputDTO> geolocalizacionOutputDTOS = geolocalizacionMapper.parseExpedientes(expedientes, expedientePosesionNegociadas, page, size);
    List<GeolocalizacionOutputDTO> geolocalizacionOutputDTOSPaginados = geolocalizacionOutputDTOS.stream().skip(size * page).limit(size).collect(Collectors.toList());
    Integer tablaSize = geolocalizacionMapper.getSize(expedientes, expedientePosesionNegociadas);
    return new ListWithCountDTO<>(geolocalizacionOutputDTOSPaginados, tablaSize);
  }

  @Override
  public List<GeolocalizacionOutputDTO> mapa(Integer idCliente, Integer idCartera, String idExpediente, Integer idLocalidad, Integer idProvincia, Integer idMunicipio, String tipoGarantia, String uso, Usuario usuario) throws NotFoundException {
    List<Expediente> expedientes = busquedaUseCase.getDataExpedientesFilter(crearBusquedaDto(idCliente, idCartera, idExpediente, idLocalidad, idProvincia, idMunicipio, tipoGarantia, uso), usuario, null, null);
    List<ExpedientePosesionNegociada> expedientePosesionNegociadas = expedientePosesionNegociadaCase.filtrarExpedientesPosesionNegociadaBusqueda(crearBusquedaDto(idCliente, idCartera, idExpediente, idLocalidad, idProvincia, idMunicipio, tipoGarantia, uso), usuario, null, null, null, null, null, null, null, null, null, null, null, null,null);
    return geolocalizacionMapper.parseExpedientes(expedientes, expedientePosesionNegociadas, null, null);

  }

  @Override
  public List<GeolocalizacionOutputDTO> mapaMovil(Integer idCliente, Integer idCartera, String idExpediente, Integer idLocalidad, Integer idProvincia, Integer idMunicipio, String tipoGarantia, String uso, Usuario usuario) throws NotFoundException {
    List<Expediente> expedientes = busquedaUseCase.getDataExpedientesFilter(crearBusquedaDto(idCliente, idCartera, idExpediente, idLocalidad, idProvincia, idMunicipio, tipoGarantia, uso), usuario, null, null);
    List<ExpedientePosesionNegociada> expedientePosesionNegociadas = expedientePosesionNegociadaCase.filtrarExpedientesPosesionNegociadaBusqueda(crearBusquedaDto(idCliente, idCartera, idExpediente, idLocalidad, idProvincia, idMunicipio, tipoGarantia, uso), usuario, null, null, null, null, null, null, null, null, null, null, null, null, null);
    return geolocalizacionMapper.parseExpedientes(expedientes, expedientePosesionNegociadas, true);

  }


  private BusquedaDto crearBusquedaDto(Integer idCliente, Integer idCartera, String idExpediente, Integer idLocalidad, Integer idProvincia, Integer idMunicipio, String tipoGarantia, String uso) {
    BusquedaDto busquedaDto = new BusquedaDto();
    BusquedaExpedienteDto busquedaExpedienteDto = new BusquedaExpedienteDto();
    busquedaExpedienteDto.setCartera(idCartera);
    busquedaExpedienteDto.setCliente(idCliente);
    busquedaExpedienteDto.setId(idExpediente);
    BusquedaBienDto busquedaBienDto = new BusquedaBienDto();
    busquedaBienDto.setLocalidad(idLocalidad);
    busquedaBienDto.setProvincia(idProvincia);
    busquedaBienDto.setMunicipio(idMunicipio);
    busquedaBienDto.setTipoGarantia(tipoGarantia);
    busquedaBienDto.setUso(uso);
    busquedaDto.setBien(busquedaBienDto);
    busquedaDto.setExpediente(busquedaExpedienteDto);
    return busquedaDto;

  }
}
