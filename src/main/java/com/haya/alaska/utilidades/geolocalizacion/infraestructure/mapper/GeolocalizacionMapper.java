package com.haya.alaska.utilidades.geolocalizacion.infraestructure.mapper;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.infrastructure.mapper.DatoContactoMapper;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.utilidades.geolocalizacion.infraestructure.controller.dto.GeolocalizacionOutputDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class GeolocalizacionMapper {
  @Autowired
  IntervinienteRepository intervinienteRepository;
  @Autowired
  OcupanteRepository ocupanteRepository;
  @Autowired
  BienRepository bienRepository;

  public List<GeolocalizacionOutputDTO> parseExpedientes(List<Expediente> expedientes, List<ExpedientePosesionNegociada> expedientePosesionNegociadas, Integer page, Integer size) {
    boolean mapa = false;
    if (page == null) mapa = true;
    List<GeolocalizacionOutputDTO> geolocalizacionOutputDTOS = new ArrayList<>();
    for (Expediente expediente : expedientes) {
      geolocalizacionOutputDTOS.addAll(parseExpediente(expediente, mapa));
      if (!mapa)
        if (geolocalizacionOutputDTOS.size() > (page * size + size)) return geolocalizacionOutputDTOS;
    }
    for (ExpedientePosesionNegociada expedientePosesionNegociada : expedientePosesionNegociadas) {
      geolocalizacionOutputDTOS.addAll(parseExpediente(expedientePosesionNegociada, mapa));
      if (!mapa)
        if (geolocalizacionOutputDTOS.size() > (page * size + size)) return geolocalizacionOutputDTOS;
    }
    return geolocalizacionOutputDTOS;
  }

  private List<GeolocalizacionOutputDTO> parseExpediente(Expediente expediente, boolean mapa) {
    List<GeolocalizacionOutputDTO> geolocalizacionOutputDTOS = new ArrayList<>();
    for (Contrato contrato : expediente.getContratos()) {
      Set<ContratoBien> contratoBienes = contrato.getBienes();
      Set<ContratoInterviniente> contratoIntervinientes = contrato.getIntervinientes();
      for (ContratoInterviniente contratoInterviniente : contratoIntervinientes) {
        List<GeolocalizacionOutputDTO> geolocalizacionOutputDTOIntervinientes = parseInterviniente(contratoInterviniente);
        if (mapa) {
          for (GeolocalizacionOutputDTO geolocalizacionOutputDTOInterviniente : geolocalizacionOutputDTOIntervinientes) {
            if (geolocalizacionOutputDTOInterviniente.getLatitud() != null && geolocalizacionOutputDTOInterviniente.getLongitud() != null)
              geolocalizacionOutputDTOS.add(geolocalizacionOutputDTOInterviniente);
          }

        } else {
          geolocalizacionOutputDTOS.addAll(geolocalizacionOutputDTOIntervinientes);
        }
      }
      List<GeolocalizacionOutputDTO> geolocalizacionOutputDTOContratos = new ArrayList<>();
      for (ContratoBien contratoBien : contratoBienes) {
        GeolocalizacionOutputDTO geolocalizacionOutputDTO = parseBien(contratoBien);
        if (mapa) {
          if (comprobarCoordenada(geolocalizacionOutputDTO.getLatitud(), true) && comprobarCoordenada(geolocalizacionOutputDTO.getLongitud(), false)) {
            geolocalizacionOutputDTOContratos.add(geolocalizacionOutputDTO);
          }
        } else {
          geolocalizacionOutputDTOContratos.add(geolocalizacionOutputDTO);
        }
      }
      if (contrato.getEsRepresentante()!=null && contrato.getEsRepresentante() && geolocalizacionOutputDTOContratos.size() > 0) {
        GeolocalizacionOutputDTO maxImporte = geolocalizacionOutputDTOContratos.stream()
          .filter(geolocalizacionOutputDTO -> {
            if (geolocalizacionOutputDTO.getImporte() != null) return true;
            return false;
          })
          .max(Comparator.comparing(GeolocalizacionOutputDTO::getImporte)).orElse(null);
        if (maxImporte != null) {
          maxImporte.setValidada(true);
          Collections.replaceAll(geolocalizacionOutputDTOContratos, maxImporte, maxImporte);
        }
      }
      geolocalizacionOutputDTOS.addAll(geolocalizacionOutputDTOContratos);
    }
    return geolocalizacionOutputDTOS;
  }

  public GeolocalizacionOutputDTO parseBien(ContratoBien contratoBien) {
    Contrato contrato = contratoBien.getContrato();
    Bien bien = contratoBien.getBien();
    Expediente expediente = contrato.getExpediente();
    GeolocalizacionOutputDTO geolocalizacionOutputDTO = new GeolocalizacionOutputDTO();
    geolocalizacionOutputDTO.setId(contratoBien.getBien().getId() + "/" + contratoBien.getContrato().getId() + "/" + "G");
    geolocalizacionOutputDTO.setEstado(contrato.getEstadoContrato() != null ? contrato.getEstadoContrato().getValor() : null);
    geolocalizacionOutputDTO.setIdExpediente(expediente.getIdConcatenado());
    geolocalizacionOutputDTO.setIdContrato(contrato.getIdCarga());
    geolocalizacionOutputDTO.setIdExpedienteFenix(expediente.getId());
    geolocalizacionOutputDTO.setIdContratoFenix(contrato.getId());
    geolocalizacionOutputDTO.setPosesionNegociada(false);
    geolocalizacionOutputDTO.setTipo(contratoBien.getBien().getTipoBien() != null
      ? contratoBien.getBien().getTipoBien().getValor() : null);
    String idHRE = bien.getIdCarga();
    String finca = bien.getFinca();
    String idMezclado = null;
    if (idHRE != null) idMezclado = idHRE;
    if (finca != null){
      if (idMezclado == null) idMezclado = finca;
      else idMezclado += "/" + finca;
    }

    geolocalizacionOutputDTO.setIdMezclado(idMezclado);

    String direccionString = direccionConcatBien(contratoBien.getBien());
    geolocalizacionOutputDTO.setDireccion(direccionString);

    //geolocalizacionOutputDTO.setDireccion(contratoBien.getBien().getComentarioDireccion());
    geolocalizacionOutputDTO.setLocalidad(contratoBien.getBien().getLocalidad() != null ? contratoBien.getBien().getLocalidad().getValor() : null);
    geolocalizacionOutputDTO.setMunicipio(contratoBien.getBien().getMunicipio() != null ? contratoBien.getBien().getMunicipio().getValor() : null);
    geolocalizacionOutputDTO.setCodigoGarantia(contratoBien.getBien().getId());
    geolocalizacionOutputDTO.setTipoGarantia(contratoBien.getTipoGarantia() != null ? contratoBien.getTipoGarantia().getValor() : null);
    geolocalizacionOutputDTO.setUso(contratoBien.getBien().getUso() != null ? contratoBien.getBien().getUso().getValor() : null);
    geolocalizacionOutputDTO.setImporte(contratoBien.getBien().getImporteBien());
    geolocalizacionOutputDTO.setLatitud(contratoBien.getBien().getLatitud());
    geolocalizacionOutputDTO.setLongitud(contratoBien.getBien().getLongitud());
    return geolocalizacionOutputDTO;
  }

  public List<GeolocalizacionOutputDTO> parseInterviniente(ContratoInterviniente contratoInterviniente) {
    Contrato contrato = contratoInterviniente.getContrato();
    Expediente expediente = contrato.getExpediente();
    GeolocalizacionOutputDTO geolocalizacionOutputDTO = new GeolocalizacionOutputDTO();
    List<GeolocalizacionOutputDTO> geolocalizacionOutputDTOList = new ArrayList<>();
    geolocalizacionOutputDTO.setId(contratoInterviniente.getInterviniente().getId() + "/" + contratoInterviniente.getContrato().getId());
    geolocalizacionOutputDTO.setEstado(contrato.getEstadoContrato() != null ? contrato.getEstadoContrato().getValor() : null);
    geolocalizacionOutputDTO.setIdContrato(contrato.getIdCarga());
    geolocalizacionOutputDTO.setIdExpediente(expediente.getIdConcatenado());
    geolocalizacionOutputDTO.setIdExpedienteFenix(expediente.getId());
    geolocalizacionOutputDTO.setIdContratoFenix(contrato.getId());
    geolocalizacionOutputDTO.setPosesionNegociada(false);
    String nombre = "";

    if(contratoInterviniente.getInterviniente().getPersonaJuridica() == true) {
      geolocalizacionOutputDTO.setNombre(contratoInterviniente.getInterviniente().getRazonSocial() != null ? contratoInterviniente.getInterviniente().getRazonSocial() : null);
    }else{
      if (contratoInterviniente.getInterviniente().getNombre() != null) {
        nombre += contratoInterviniente.getInterviniente().getNombre();
        if (contratoInterviniente.getInterviniente().getApellidos() != null) {
          nombre += " " + contratoInterviniente.getInterviniente().getApellidos();
        }
      }
   }

    geolocalizacionOutputDTO.setNombre(nombre);
    geolocalizacionOutputDTO.setTipoIntervencion(contratoInterviniente.getTipoIntervencion() != null ? contratoInterviniente.getTipoIntervencion().getValor() : null);
    geolocalizacionOutputDTO.setOrdenIntervencion(contratoInterviniente.getOrdenIntervencion());
    geolocalizacionOutputDTO.setTelefonoPrincipal(contratoInterviniente.getInterviniente().getTelefonoPrincipal());
    geolocalizacionOutputDTO.setDatosContacto(
      contratoInterviniente.getInterviniente()
        .getDatosContacto()
        .stream()
        .map(DatoContactoMapper::parseEntityToListDTO)
        .collect(Collectors.toList()));
    int x = 0;
    for (Direccion direccion : contratoInterviniente.getInterviniente().getDirecciones()) {
      GeolocalizacionOutputDTO geolocalizacionOutputDTOAux = new GeolocalizacionOutputDTO();
      BeanUtils.copyProperties(geolocalizacionOutputDTO, geolocalizacionOutputDTOAux);

      String direccionString = direccionConcat(direccion);
      geolocalizacionOutputDTOAux.setDireccion(direccionString);
      geolocalizacionOutputDTOAux.setId(geolocalizacionOutputDTOAux.getId() + "/" + x++ + "/I");
      if (comprobarCoordenada(direccion.getLatitud(), true) && comprobarCoordenada(direccion.getLongitud(), false)) {
        geolocalizacionOutputDTOAux.setLatitud(direccion.getLatitud());
        geolocalizacionOutputDTOAux.setLongitud(direccion.getLongitud());
       // break;
      }
      geolocalizacionOutputDTOAux.setMunicipio(direccion.getMunicipio() != null ? direccion.getMunicipio().getValor() : null);
      geolocalizacionOutputDTOAux.setLocalidad(direccion.getLocalidad() != null ? direccion.getLocalidad().getValor() : null);
    //  geolocalizacionOutputDTOAux.setDireccion(direccion.getComentario());
      geolocalizacionOutputDTOList.add(geolocalizacionOutputDTOAux);
    }
    if (contratoInterviniente.getInterviniente().getDirecciones().size() == 0)
      geolocalizacionOutputDTOList.add(geolocalizacionOutputDTO);
    return geolocalizacionOutputDTOList;
  }

  private List<GeolocalizacionOutputDTO> parseExpediente(ExpedientePosesionNegociada expedientePosesionNegociada, boolean mapa) {
    List<GeolocalizacionOutputDTO> geolocalizacionOutputDTOS = new ArrayList<>();
    for (BienPosesionNegociada bienPosesionNegociada : expedientePosesionNegociada.getBienesPosesionNegociada()) {
      Set<OcupanteBien> ocupantes = bienPosesionNegociada.getOcupantes();
      for (OcupanteBien ocupanteBien : ocupantes) {
        List<GeolocalizacionOutputDTO> geolocalizacionOutputDTOOcupantes = parseOcupante(ocupanteBien);
        if (mapa) {
          for (GeolocalizacionOutputDTO geolocalizacionOutputDTOOcupante : geolocalizacionOutputDTOOcupantes) {
            if (geolocalizacionOutputDTOOcupante.getLatitud() != null && geolocalizacionOutputDTOOcupante.getLongitud() != null)
              geolocalizacionOutputDTOS.add(geolocalizacionOutputDTOOcupante);
          }
        } else {
          geolocalizacionOutputDTOS.addAll(geolocalizacionOutputDTOOcupantes);
        }
      }
      GeolocalizacionOutputDTO geolocalizacionOutputDTO = parseBien(bienPosesionNegociada);
      if (mapa) {
        if (comprobarCoordenada(geolocalizacionOutputDTO.getLatitud(), true) && comprobarCoordenada(geolocalizacionOutputDTO.getLongitud(), false)) {
          geolocalizacionOutputDTOS.add(geolocalizacionOutputDTO);
        }
      } else {
        geolocalizacionOutputDTOS.add(geolocalizacionOutputDTO);
      }
    }
    GeolocalizacionOutputDTO maxImporte = geolocalizacionOutputDTOS.stream()
      .filter(geolocalizacionOutputDTO -> {
        if (geolocalizacionOutputDTO.getImporte() != null) return true;
        return false;
      })
      .max(Comparator.comparing(GeolocalizacionOutputDTO::getImporte)).orElse(null);
    if (maxImporte != null) {
      maxImporte.setValidada(true);
      Collections.replaceAll(geolocalizacionOutputDTOS, maxImporte, maxImporte);
    }
    return geolocalizacionOutputDTOS;
  }

  public GeolocalizacionOutputDTO parseBien(BienPosesionNegociada bienPosesionNegociada) {
    ExpedientePosesionNegociada expedientePosesionNegociada = bienPosesionNegociada.getExpedientePosesionNegociada();
    GeolocalizacionOutputDTO geolocalizacionOutputDTO = new GeolocalizacionOutputDTO();
    geolocalizacionOutputDTO.setId(bienPosesionNegociada.getId() + "/" + bienPosesionNegociada.getBien().getId() + "/" + "B");
    geolocalizacionOutputDTO.setEstado(bienPosesionNegociada.getEstadoOcupacion() != null ? bienPosesionNegociada.getEstadoOcupacion().getValor() : null);
    geolocalizacionOutputDTO.setIdExpediente(expedientePosesionNegociada.getIdConcatenado());
    geolocalizacionOutputDTO.setIdContrato(expedientePosesionNegociada.getIdOrigen());
    geolocalizacionOutputDTO.setIdExpedienteFenix(expedientePosesionNegociada.getId());
    geolocalizacionOutputDTO.setIdContratoFenix(bienPosesionNegociada.getId());
    geolocalizacionOutputDTO.setPosesionNegociada(true);
    geolocalizacionOutputDTO.setTipo(bienPosesionNegociada.getBien().getTipoBien() != null
      ? bienPosesionNegociada.getBien().getTipoBien().getValor() : null);

    String direccionString = direccionConcatBien(bienPosesionNegociada.getBien());
    geolocalizacionOutputDTO.setDireccion(direccionString);

    Bien bien = bienPosesionNegociada.getBien();

    String idHRE = bien.getIdCarga();
    String finca = bien.getFinca();
    String idMezclado = null;
    if (idHRE != null) idMezclado = idHRE;
    if (finca != null){
      if (idMezclado == null) idMezclado = finca;
      else idMezclado += "/" + finca;
    }

    geolocalizacionOutputDTO.setIdMezclado(idMezclado);

   // geolocalizacionOutputDTO.setDireccion(bienPosesionNegociada.getBien().getComentarioDireccion());
    geolocalizacionOutputDTO.setLocalidad(bienPosesionNegociada.getBien().getLocalidad() != null ? bienPosesionNegociada.getBien().getLocalidad().getValor() : null);
    geolocalizacionOutputDTO.setMunicipio(bienPosesionNegociada.getBien().getMunicipio() != null ? bienPosesionNegociada.getBien().getMunicipio().getValor() : null);
    geolocalizacionOutputDTO.setCodigoGarantia(bienPosesionNegociada.getBien().getId());
    geolocalizacionOutputDTO.setTipoGarantia(bienPosesionNegociada.getTipoGarantia() != null ? bienPosesionNegociada.getTipoGarantia().getValor() : null);
    geolocalizacionOutputDTO.setUso(bienPosesionNegociada.getBien().getUso() != null ? bienPosesionNegociada.getBien().getUso().getValor() : null);
    geolocalizacionOutputDTO.setImporte(bienPosesionNegociada.getBien().getImporteBien());
    geolocalizacionOutputDTO.setLatitud(bienPosesionNegociada.getBien().getLatitud());
    geolocalizacionOutputDTO.setLongitud(bienPosesionNegociada.getBien().getLongitud());
    return geolocalizacionOutputDTO;
  }

  public List<GeolocalizacionOutputDTO> parseOcupante(OcupanteBien ocupanteBien) {
    GeolocalizacionOutputDTO geolocalizacionOutputDTO = new GeolocalizacionOutputDTO();
    BienPosesionNegociada bienPosesionNegociada = ocupanteBien.getBienPosesionNegociada();
    ExpedientePosesionNegociada expedientePosesionNegociada = bienPosesionNegociada.getExpedientePosesionNegociada();

    List<GeolocalizacionOutputDTO> geolocalizacionOutputDTOList = new ArrayList<>();
    geolocalizacionOutputDTO.setId(ocupanteBien.getOcupante().getId() + "/" + bienPosesionNegociada.getId());
    geolocalizacionOutputDTO.setEstado(ocupanteBien.getBienPosesionNegociada().getEstadoOcupacion() != null ? ocupanteBien.getBienPosesionNegociada().getEstadoOcupacion().getValor() : null);
    geolocalizacionOutputDTO.setIdContrato(expedientePosesionNegociada.getIdOrigen());
    geolocalizacionOutputDTO.setIdExpediente(expedientePosesionNegociada.getIdConcatenado());
    geolocalizacionOutputDTO.setIdExpedienteFenix(expedientePosesionNegociada.getId());
    geolocalizacionOutputDTO.setIdContratoFenix(bienPosesionNegociada.getId());
    geolocalizacionOutputDTO.setPosesionNegociada(true);
    String nombre = "";
    if (ocupanteBien.getOcupante().getNombre() != null)
      nombre += ocupanteBien.getOcupante().getNombre();
    if (ocupanteBien.getOcupante().getApellidos() != null)
      nombre += " " + ocupanteBien.getOcupante().getApellidos();
    geolocalizacionOutputDTO.setNombre(nombre);
    geolocalizacionOutputDTO.setTipoIntervencion(ocupanteBien.getTipoOcupante() != null ? ocupanteBien.getTipoOcupante().getValor() : null);
    geolocalizacionOutputDTO.setOrdenIntervencion(ocupanteBien.getOrden());
    geolocalizacionOutputDTO.setTelefonoPrincipal(ocupanteBien.getOcupante().getTelefonoPrincipal());
    geolocalizacionOutputDTO.setDatosContacto(
      ocupanteBien.getOcupante()
        .getDatosContacto()
        .stream()
        .map(DatoContactoMapper::parseEntityToListDTO)
        .collect(Collectors.toList()));
    int x = 0;
    for (Direccion direccion : ocupanteBien.getOcupante().getDirecciones()) {
      GeolocalizacionOutputDTO geolocalizacionOutputDTOAux = new GeolocalizacionOutputDTO();
      BeanUtils.copyProperties(geolocalizacionOutputDTO, geolocalizacionOutputDTOAux);
      String direccionString = direccionConcat(direccion);
      geolocalizacionOutputDTOAux.setDireccion(direccionString);
      geolocalizacionOutputDTOAux.setId(geolocalizacionOutputDTOAux.getId() + "/" + x++ + "/O");
      if (comprobarCoordenada(direccion.getLatitud(), true) && comprobarCoordenada(direccion.getLongitud(), false)) {
        geolocalizacionOutputDTOAux.setLatitud(direccion.getLatitud());
        geolocalizacionOutputDTOAux.setLongitud(direccion.getLongitud());
       // break;
      }
      geolocalizacionOutputDTOAux.setMunicipio(direccion.getMunicipio() != null ? direccion.getMunicipio().getValor() : null);
      geolocalizacionOutputDTOAux.setLocalidad(direccion.getLocalidad() != null ? direccion.getLocalidad().getValor() : null);
    //  geolocalizacionOutputDTOAux.setDireccion(direccion.getComentario());
      geolocalizacionOutputDTOList.add(geolocalizacionOutputDTOAux);
    }
    if (ocupanteBien.getOcupante().getDirecciones().size() == 0)
      geolocalizacionOutputDTOList.add(geolocalizacionOutputDTO);
    return geolocalizacionOutputDTOList;
  }

  public Integer getSize(List<Expediente> expedientes, List<ExpedientePosesionNegociada> expedientePosesionNegociadas) {
    Integer size = 0;
    for (Expediente expediente : expedientes) {
      for (Contrato contrato : expediente.getContratos()) {
        size += contrato.getBienes().size();
        size += contrato.getIntervinientes().size();
      }
    }
    for (ExpedientePosesionNegociada expedientePosesionNegociada : expedientePosesionNegociadas) {
      for (BienPosesionNegociada bien : expedientePosesionNegociada.getBienesPosesionNegociada()) {
        size += bien.getOcupantes().size();
        size++;
      }

    }
    return size;
  }

  private boolean comprobarCoordenada(Double coordenada, boolean latitud) {
    if (latitud) {
      return coordenada != null && coordenada <= 85 && coordenada >= -85;
    } else {
      return coordenada != null && coordenada <= 180 && coordenada >= -180;
    }
  }


  private String direccionConcat(Direccion direccion){
    String direccionString = new String();
      direccionString +=
          direccion.getTipoVia() != null ? direccion.getTipoVia().getValor() + " " : "";
      direccionString += direccion.getNombre() != null ? direccion.getNombre() + " " : "";
      direccionString += direccion.getNumero() != null ? direccion.getNumero() + " " : "";
      direccionString += direccion.getPortal() != null ? direccion.getPortal() + " " : "";
      direccionString += direccion.getBloque() != null ? direccion.getBloque() + " " : "";
      direccionString += direccion.getEscalera() != null ? direccion.getEscalera() + " " : "";
      direccionString += direccion.getPiso() != null ? direccion.getPiso() + " " : "";
      direccionString += direccion.getPuerta() != null ? direccion.getPuerta() + " " : "";
      direccionString +=
          direccion.getEntorno() != null ? direccion.getEntorno().getValor() + " " : "";
      direccionString +=
          direccion.getCodigoPostal() != null ? direccion.getCodigoPostal() + " " : "";
      direccionString +=
          direccion.getMunicipio() != null ? direccion.getMunicipio().getValor() + " " : "";
      direccionString +=
          direccion.getLocalidad() != null ? direccion.getLocalidad().getValor() + " " : "";
      direccionString +=
          direccion.getProvincia() != null ? direccion.getProvincia().getValor() + " " : "";
    return direccionString;
  }




  private String direccionConcatBien(Bien bien){
    String direccionString = new String();
      direccionString += bien.getTipoVia() != null ? bien.getTipoVia().getValor() + " " : "";
      direccionString +=bien.getNombreVia() != null ?bien.getNombreVia()+ " ": "";
      direccionString +=bien.getNumero() != null ? bien.getNumero() + " ": "";
      direccionString +=bien.getPortal() != null ? bien.getPortal() + " ": "";
      direccionString +=bien.getBloque() != null ? bien.getBloque() + " ": "";
      direccionString +=bien.getEscalera() != null ? bien.getEscalera() + " ": "";
      direccionString +=bien.getPiso() != null ? bien.getPiso() + " ": "";
      direccionString +=bien.getPuerta() != null ? bien.getPuerta() + " ": "";
      direccionString +=bien.getEntorno() != null ? bien.getEntorno().getValor() + " ": "";
      direccionString +=bien.getCodigoPostal() != null ? bien.getCodigoPostal() + " ": "";
      direccionString +=bien.getMunicipio() != null ? bien.getMunicipio().getValor() + " ": "";
      direccionString +=bien.getLocalidad() != null ? bien.getLocalidad().getValor() + " ": "";
      direccionString +=bien.getProvincia() != null ? bien.getProvincia().getValor() + " ": "";
    return direccionString;
  }


 public List<GeolocalizacionOutputDTO> parseExpedientes(List<Expediente> expedientes, List<ExpedientePosesionNegociada> expedientePN, boolean mapa){
   List<GeolocalizacionOutputDTO> result = new ArrayList<>();

   List<Integer> idsRA = expedientes.stream().map(Expediente::getId).collect(Collectors.toList());
   List<Integer> idsPN = expedientePN.stream().map(ExpedientePosesionNegociada::getId).collect(Collectors.toList());
   List<Interviniente> intervinientes = intervinienteRepository.findDistinctAllByContratosContratoExpedienteIdIn(idsRA);
   List<Ocupante> ocupantes = ocupanteRepository.findDistinctAllByBienesBienPosesionNegociadaExpedientePosesionNegociadaIdIn(idsPN);
   List<Bien> bienes = bienRepository.findDistinctAllByContratosContratoExpedienteIdInOrBienPosesionNegociadaExpedientePosesionNegociadaIdIn(idsRA, idsPN);

   for (Interviniente i : intervinientes)
     result.addAll(this.parseInterviniente(i));
   for (Ocupante o : ocupantes)
     result.addAll(this.parseOcupante(o));
   for (Bien b : bienes)
     result.addAll(this.parseBien(b));

   if (mapa){
     return result.stream().filter(this::filtrarMapa).collect(Collectors.toList());
   } else {
     return result;
   }
 }

  private List<GeolocalizacionOutputDTO> parseInterviniente(Interviniente interviniente){
    ContratoInterviniente ci = interviniente.getContratos().stream().filter(c -> c.getContrato() != null).findFirst().orElse(null);
    if (ci == null) return new ArrayList<>();
    else return parseInterviniente(ci);
  }

  private List<GeolocalizacionOutputDTO> parseOcupante(Ocupante ocupante){
    OcupanteBien ob = ocupante.getBienes().stream().filter(c -> c.getBienPosesionNegociada() != null).findFirst().orElse(null);
    if (ob == null) return new ArrayList<>();
    else return parseOcupante(ob);
  }

  private List<GeolocalizacionOutputDTO> parseBien(Bien bien){
    List<GeolocalizacionOutputDTO> result = new ArrayList<>();
    BienPosesionNegociada bpn = bien.getBienPosesionNegociada();
    if (bpn != null){
      result.add(parseBien(bpn));
    } else {
      ContratoBien cb = bien.getContratos().stream().filter(c -> c.getContrato() != null).findFirst().orElse(null);
      if (cb == null) return result;
      else result.add(parseBien(cb));
    }
    return result;
  }

  private Boolean filtrarMapa(GeolocalizacionOutputDTO output){
    return output.getLatitud() != null && output.getLongitud() != null;
  }
}
