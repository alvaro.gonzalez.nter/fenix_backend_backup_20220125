package com.haya.alaska.utilidades.geolocalizacion.infraestructure.controller;

import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.utilidades.geolocalizacion.application.GeolocalizacionUseCase;
import com.haya.alaska.utilidades.geolocalizacion.infraestructure.controller.dto.GeolocalizacionOutputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/geolocalizacion")
public class GeolocalizacionController {
  @Autowired
  private GeolocalizacionUseCase geolocalizacionUseCase;

  @GetMapping("/filter")
  public ListWithCountDTO<GeolocalizacionOutputDTO> getAllFilter(
    @RequestParam(value = "cliente", required = false) Integer cliente,
    @RequestParam(value = "cartera", required = false) Integer cartera,
    @RequestParam(value = "expediente", required = false) String expediente,
    @RequestParam(value = "localidad", required = false) Integer localidad,
    @RequestParam(value = "provincia", required = false) Integer provincia,
    @RequestParam(value = "municipio", required = false) Integer municipio,
    @RequestParam(value = "tipoGarantia", required = false) String tipoGarantia,
    @RequestParam(value = "uso", required = false) String uso,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return geolocalizacionUseCase.filter(cliente, cartera, expediente, localidad, provincia, municipio, tipoGarantia, uso, usuario, size, page, orderField, orderDirection);
  }

  @GetMapping("/mapa")
  public List<GeolocalizacionOutputDTO> getMapa(
    @RequestParam(value = "cliente", required = false) Integer cliente,
    @RequestParam(value = "cartera", required = false) Integer cartera,
    @RequestParam(value = "expediente", required = false) String expediente,
    @RequestParam(value = "localidad", required = false) Integer localidad,
    @RequestParam(value = "provincia", required = false) Integer provincia,
    @RequestParam(value = "municipio", required = false) Integer municipio,
    @RequestParam(value = "tipoGarantia", required = false) String tipoGarantia,
    @RequestParam(value = "uso", required = false) String uso,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return geolocalizacionUseCase.mapa(cliente, cartera, expediente, localidad, provincia, municipio, tipoGarantia, uso, usuario);
  }

  @GetMapping("/movil/mapa")
  public List<GeolocalizacionOutputDTO> getMapaMovil(
    @RequestParam(value = "cliente", required = false) Integer cliente,
    @RequestParam(value = "cartera", required = false) Integer cartera,
    @RequestParam(value = "expediente", required = false) String expediente,
    @RequestParam(value = "localidad", required = false) Integer localidad,
    @RequestParam(value = "provincia", required = false) Integer provincia,
    @RequestParam(value = "municipio", required = false) Integer municipio,
    @RequestParam(value = "tipoGarantia", required = false) String tipoGarantia,
    @RequestParam(value = "uso", required = false) String uso,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return geolocalizacionUseCase.mapaMovil(cliente, cartera, expediente, localidad, provincia, municipio, tipoGarantia, uso, usuario);
  }
}
