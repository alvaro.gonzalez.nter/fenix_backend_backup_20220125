package com.haya.alaska.utilidades.geolocalizacion.application;

import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.utilidades.geolocalizacion.infraestructure.controller.dto.GeolocalizacionOutputDTO;

import java.util.List;

public interface GeolocalizacionUseCase {

  ListWithCountDTO<GeolocalizacionOutputDTO> filter(Integer idCliente, Integer idCartera, String idExpediente, Integer idLocalidad, Integer idProvincia, Integer idMunicipio, String tipoGarantia, String uso, Usuario usuario, Integer size, Integer page, String orderField, String orderDirection) throws NotFoundException;

  List<GeolocalizacionOutputDTO> mapa(Integer idCliente, Integer idCartera, String idExpediente, Integer idLocalidad, Integer idProvincia, Integer idMunicipio, String tipoGarantia, String uso, Usuario usuario) throws NotFoundException;
  List<GeolocalizacionOutputDTO> mapaMovil(Integer idCliente, Integer idCartera, String idExpediente, Integer idLocalidad, Integer idProvincia, Integer idMunicipio, String tipoGarantia, String uso, Usuario usuario) throws NotFoundException;
}
