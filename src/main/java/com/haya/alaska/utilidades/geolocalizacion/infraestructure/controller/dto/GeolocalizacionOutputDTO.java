package com.haya.alaska.utilidades.geolocalizacion.infraestructure.controller.dto;

import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import lombok.Data;

import java.util.List;

@Data
public class GeolocalizacionOutputDTO {
  private String nombre;
  private String id;//id del bien/interviniente/garantia/ocupante + una letra
  private String tipoIntervencion;
  private Integer ordenIntervencion;
  private String estado;
  private String direccion;
  private String localidad;
  private String telefonoPrincipal;
  private String idExpediente;
  private String idContrato;
  private Integer codigoGarantia;
  private String tipoGarantia;
  private String uso;
  private boolean validada = false;
  private Double importe;
  private String municipio;
  private Double latitud;
  private Double longitud;
  private String tipo;
  private String idMezclado;
  private List<DatosContactoDTOToList> datosContacto;

  private Integer idExpedienteFenix;
  private Integer idContratoFenix;
  private Boolean posesionNegociada;

  @Override
  public boolean equals(Object o1) {
    GeolocalizacionOutputDTO geolocalizacionOutputDTO = (GeolocalizacionOutputDTO) o1;
    if (this.getId().equals(geolocalizacionOutputDTO.getId())) return true;
    return false;
  }
}
