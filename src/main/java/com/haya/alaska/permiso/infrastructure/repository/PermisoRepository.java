package com.haya.alaska.permiso.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.permiso.domain.Permiso;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermisoRepository extends CatalogoRepository<Permiso, Integer> {
  List<Permiso> findAllByActivoIsTrue();
  List<Permiso> findAllByPermisoPadreId(Integer id);
}
