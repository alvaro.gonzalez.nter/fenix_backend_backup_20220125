package com.haya.alaska.permiso.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.permiso_disponible.domain.PermisoDisponible;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Audited
@AuditTable(value = "HIST_LKUP_PERMISO")
@Entity
@Table(name = "LKUP_PERMISO")
@Getter
@Setter
@NoArgsConstructor
public class Permiso extends Catalogo {
  @OneToOne
  @JoinColumn(name = "ID_PERMISO_PADRE")
  private Permiso permisoPadre;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERMISO")
  @NotAudited
  private List<PermisoDisponible> permisosDisponibles = new ArrayList<>();

  private static final long serialVersionUID = 1L;
}
