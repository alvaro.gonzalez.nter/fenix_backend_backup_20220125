package com.haya.alaska.enlace_interes.application;

import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresAsignadoOutputDTO;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresInputDTO;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresOutputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;

import java.util.List;

public interface EnlaceInteresUseCase {
  ListWithCountDTO<EnlaceInteresOutputDTO> getAllEnlaces(Boolean historico, Integer size, Integer page);
  EnlaceInteresOutputDTO getEnlaceById(Integer idEnlace) throws NotFoundException;
  List<EnlaceInteresOutputDTO> updateEnlaces(List<EnlaceInteresInputDTO> inputs) throws NotFoundException;
  EnlaceInteresOutputDTO borrarEnlace(Integer idEnlace, Boolean activo) throws NotFoundException;

  EnlaceInteresAsignadoOutputDTO getAllEnlaces(Integer cartera, String perfil) throws NotFoundException;
  //List<EnlaceInteresAsignadoOutputDTO> updateAllEnlaces(Integer carteraId, Integer perfilId, List<Integer> enlacesIds) throws NotFoundException;
}
