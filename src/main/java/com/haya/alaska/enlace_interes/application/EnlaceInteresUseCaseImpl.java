package com.haya.alaska.enlace_interes.application;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.enlace_interes.domain.EnlaceInteres;
import com.haya.alaska.enlace_interes.infrastructure.repository.EnlaceInteresRepository;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresAsignadoOutputDTO;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresInputDTO;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresOutputDTO;
import com.haya.alaska.enlace_interes.infrastructure.mapper.EnlaceInteresMapper;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.infrastructure.repository.PerfilRepository;
import com.haya.alaska.permiso.domain.Permiso;
import com.haya.alaska.permiso.infrastructure.repository.PermisoRepository;
import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import com.haya.alaska.permiso_asignado.infrastructure.repository.PermisoAsignadoRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
@Slf4j
public class EnlaceInteresUseCaseImpl implements EnlaceInteresUseCase {
  @Autowired
  EnlaceInteresRepository enlaceInteresRepository;
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  PerfilRepository perfilRepository;
  @Autowired
  PermisoRepository permisoRepository;
  @Autowired
  PermisoAsignadoRepository permisoAsignadoRepository;

  @Autowired
  EnlaceInteresMapper enlaceInteresMapper;

  @Override
  public ListWithCountDTO<EnlaceInteresOutputDTO> getAllEnlaces(Boolean historico, Integer size, Integer page){
    List<EnlaceInteres> enlaces;
    if (historico != null && historico) enlaces = enlaceInteresRepository.findAll();
    else enlaces = enlaceInteresRepository.findAllByActivoIsTrue();
    List<EnlaceInteresOutputDTO> result = enlaces.stream()
      .map(enlaceInteresMapper::entityToOutput).collect(Collectors.toList());
    List<EnlaceInteresOutputDTO> resultPaged = result.stream().skip(size * page).limit(size).collect(Collectors.toList());

    return new ListWithCountDTO<>(resultPaged, result.size());
  }

  @Override
  public EnlaceInteresOutputDTO getEnlaceById(Integer idEnlace) throws NotFoundException {
    EnlaceInteres ei = enlaceInteresRepository.findById(idEnlace).orElseThrow(() ->
      new NotFoundException("Enlace de Interés", idEnlace));
    EnlaceInteresOutputDTO result = enlaceInteresMapper.entityToOutput(ei);
    return result;
  }

  @Override
  public List<EnlaceInteresOutputDTO> updateEnlaces(List<EnlaceInteresInputDTO> inputs) throws NotFoundException {
    List<EnlaceInteres> enlaces = new ArrayList<>();
    for (EnlaceInteresInputDTO input : inputs){
      EnlaceInteres ei = new EnlaceInteres();
      if (input.getId() != null) ei = enlaceInteresRepository.findById(input.getId()).orElseThrow(() ->
        new NotFoundException("Enlace de Interés", input.getId()));
      enlaceInteresMapper.inputToEntity(ei, input);
      enlaces.add(enlaceInteresRepository.save(ei));
    }

    List<EnlaceInteresOutputDTO> result = enlaces.stream()
      .map(enlaceInteresMapper::entityToOutput).collect(Collectors.toList());
    return result;
  }

  @Override
  @Transactional
  public EnlaceInteresOutputDTO borrarEnlace(Integer idEnlace, Boolean activo) throws NotFoundException {
    EnlaceInteres ei = enlaceInteresRepository.findById(idEnlace).orElseThrow(() ->
      new NotFoundException("Enlace de Interés", idEnlace));
    ei.setActivo(activo);
    //if (!activo) enlaceInteresAsignadoRepository.deleteAllByEnlaceId(ei.getId());
    EnlaceInteresOutputDTO result = enlaceInteresMapper.entityToOutput(enlaceInteresRepository.save(ei));
    return result;
  }

  @Override
  public EnlaceInteresAsignadoOutputDTO getAllEnlaces(Integer cartera, String perfil) throws NotFoundException {
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();
    Perfil p = null;
    if (defaultLocal.equals("es"))
      p = perfilRepository.findByNombre(perfil).orElseThrow(() ->
      new NotFoundException("Perfil", "Nombre", perfil));
    else if (defaultLocal.equals("en"))
      p = perfilRepository.findByNombreIngles(perfil).orElseThrow(() ->
      new NotFoundException("Perfil", "Nombre", perfil));

    Cartera c = carteraRepository.findById(cartera).orElseThrow(() ->
      new NotFoundException("Cartera", cartera));

    Permiso permiso = permisoRepository.findByCodigo("util-en_in").orElseThrow(() ->
      new NotFoundException("Permiso", "Código", "en_in"));

    List<PermisoAsignado> permisosAsignados = permisoAsignadoRepository.findByPerfilIdAndCarteraIdAndPermisoDisponiblePermisoId(p.getId(), cartera, permiso.getId());
    List<String> codigos = permisosAsignados.stream().map(pa ->
      pa.getPermisoDisponible().getOpcionPermiso().getCodigo()).collect(Collectors.toList());
    List<EnlaceInteres> enlaces = enlaceInteresRepository.findAllByCodigoIn(codigos);

    EnlaceInteresAsignadoOutputDTO result = enlaceInteresMapper.entityToOutput(c, p, enlaces);
    return result;
  }

  /*@Override
  @Transactional
  public List<EnlaceInteresAsignadoOutputDTO> updateAllEnlaces(Integer carteraId, Integer perfilId, List<Integer> enlacesIds) throws NotFoundException {
    Cartera cartera = carteraRepository.findById(carteraId).orElseThrow(() ->
      new NotFoundException("No encontrada cartera con id: " + carteraId));
    Perfil perfil = perfilRepository.findById(perfilId).orElseThrow(() ->
      new NotFoundException("No encontrado perfil con id: " + perfilId));

    enlaceInteresAsignadoRepository.deleteAllByCarteraIdAndPerfilId(carteraId, perfilId);

    List<EnlaceInteres> enlaces = enlaceInteresRepository.findAllByIdIn(enlacesIds);
    List<EnlaceInteresAsignado> enlacesAsignados = new ArrayList<>();
    for (EnlaceInteres enlace : enlaces){
      EnlaceInteresAsignado eia = new EnlaceInteresAsignado();
      eia.setCartera(cartera);
      eia.setPerfil(perfil);
      eia.setEnlace(enlace);
      enlacesAsignados.add(enlaceInteresAsignadoRepository.save(eia));
    }

    List<EnlaceInteresAsignadoOutputDTO> result = enlacesAsignados.stream()
      .map(enlaceInteresMapper::entityToOutput).collect(Collectors.toList());
    return result;
  }*/
}
