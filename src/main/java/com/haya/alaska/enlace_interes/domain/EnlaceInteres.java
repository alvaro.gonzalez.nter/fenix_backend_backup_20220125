package com.haya.alaska.enlace_interes.domain;

import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_MSTR_ENLACE_INTERES")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_ENLACE_INTERES")
public class EnlaceInteres implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @Column(name = "NUM_NIVEL")
  private Integer nivel;

  @Column(name = "DES_CODIGO")
  private String codigo;

  @Column(name = "DES_NOMBRE")
  private String nombre;

  @Column(name = "DES_ENLACE")
  private String enlace;
}
