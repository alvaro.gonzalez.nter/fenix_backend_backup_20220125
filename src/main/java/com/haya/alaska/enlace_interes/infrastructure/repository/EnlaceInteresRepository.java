package com.haya.alaska.enlace_interes.infrastructure.repository;

import com.haya.alaska.enlace_interes.domain.EnlaceInteres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnlaceInteresRepository extends JpaRepository<EnlaceInteres, Integer> {
  List<EnlaceInteres> findAllByActivoIsTrue();
  List<EnlaceInteres> findAllByIdIn(List<Integer> ids);
  List<EnlaceInteres> findAllByCodigoIn(List<String> codigos);
}
