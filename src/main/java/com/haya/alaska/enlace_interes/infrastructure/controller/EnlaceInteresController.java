package com.haya.alaska.enlace_interes.infrastructure.controller;

import com.haya.alaska.enlace_interes.application.EnlaceInteresUseCase;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresAsignadoOutputDTO;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresInputDTO;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresOutputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/enlacesInteres")
public class EnlaceInteresController {
  @Autowired
  EnlaceInteresUseCase enlaceInteresUseCase;

  @ApiOperation(value = "Listado de todos los enlacesInteres",
    notes = "Devuelve todos los enlacesInteres activos o todos en la base de datos")
  @GetMapping
  public ListWithCountDTO<EnlaceInteresOutputDTO> getAll(
    @RequestParam(value = "historico") Boolean historico,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page) {
    return enlaceInteresUseCase.getAllEnlaces(historico, size, page);
  }

  @ApiOperation(value = "Obtiene el enlacesInteres por su id",
    notes = "Obtiene el enlacesInteres por su id")
  @GetMapping("/{idEnlace}")
  public EnlaceInteresOutputDTO getById(
    @PathVariable("idEnlace") Integer idEnlace) throws NotFoundException {
    return enlaceInteresUseCase.getEnlaceById(idEnlace);
  }

  @ApiOperation(value = "Actualiza los enlacesInteres",
    notes = "Actualiza o crea los enlaces a partir de los objetos enviados")
  @PutMapping
  public List<EnlaceInteresOutputDTO> updateEnlaces(
    @RequestBody List<EnlaceInteresInputDTO> enlaces) throws NotFoundException {
    return enlaceInteresUseCase.updateEnlaces(enlaces);
  }

  @ApiOperation(value = "Actualiza la activación de un enlacesInteres",
    notes = "Actualiza la activación de un enlacesInteres, y si se desactiva, se eliminarán todos los enlacesInteresAsociados al mismo.")
  @DeleteMapping("/{idEnlace}")
  public EnlaceInteresOutputDTO deleteById(
    @PathVariable("idEnlace") Integer idEnlace,
    @RequestParam(value = "activo") Boolean activo) throws NotFoundException {
    return enlaceInteresUseCase.borrarEnlace(idEnlace, activo);
  }

  @ApiOperation(value = "Listado de todos los enlacesInteresAsignados por cartera y perfil",
    notes = "Listado de todos los enlacesInteresAsignados por cartera y perfil")
  @GetMapping("/asignados")
  public EnlaceInteresAsignadoOutputDTO getAllAsignados(
    @RequestParam(value = "cartera") Integer cartera,
    @RequestParam(value = "perfil") String perfil) throws NotFoundException {
    return enlaceInteresUseCase.getAllEnlaces(cartera, perfil);
  }

  /*@ApiOperation(value = "Listado de todos los enlacesInteresAsignados por cartera y perfil",
    notes = "Listado de todos los enlacesInteresAsignados por cartera y perfil")
  @PutMapping("/asignados")
  public List<EnlaceInteresAsignadoOutputDTO> updateAllEnlaces(
    @RequestParam(value = "cartera") Integer cartera,
    @RequestParam(value = "perfil") Integer perfil,
    @RequestParam(value = "enlacesIds") List<Integer> enlacesIds) throws NotFoundException {
    return enlaceInteresUseCase.updateAllEnlaces(cartera, perfil, enlacesIds);
  }*/
}
