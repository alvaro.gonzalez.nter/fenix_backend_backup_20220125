package com.haya.alaska.enlace_interes.infrastructure.mapper;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.enlace_interes.domain.EnlaceInteres;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresAsignadoOutputDTO;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresInputDTO;
import com.haya.alaska.enlace_interes.infrastructure.controller.dto.EnlaceInteresOutputDTO;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EnlaceInteresMapper {
  public EnlaceInteresAsignadoOutputDTO entityToOutput(Cartera cartera, Perfil perfil, List<EnlaceInteres> eiList){
    EnlaceInteresAsignadoOutputDTO result = new EnlaceInteresAsignadoOutputDTO();
    if (cartera == null || perfil == null) return null;
    result.setCartera(new CarteraDTOToList(cartera));
    result.setPerfil(new PerfilSimpleOutputDTO(perfil));

    List<EnlaceInteresOutputDTO> enlaces = new ArrayList<>();
    for (EnlaceInteres ei : eiList){
      enlaces.add(entityToOutput(ei));
    }
    result.setEnlacesInteres(enlaces);
    return result;
  }

  public EnlaceInteresOutputDTO entityToOutput(EnlaceInteres ei){
    EnlaceInteresOutputDTO result = new EnlaceInteresOutputDTO();
    if (ei == null) return null;
    BeanUtils.copyProperties(ei, result);
    return result;
  }

  public EnlaceInteres inputToEntity(EnlaceInteres ei, EnlaceInteresInputDTO input){
    BeanUtils.copyProperties(input, ei, "id");
    return ei;
  }
}
