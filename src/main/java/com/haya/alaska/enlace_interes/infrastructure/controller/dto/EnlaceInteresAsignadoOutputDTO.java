package com.haya.alaska.enlace_interes.infrastructure.controller.dto;

import com.haya.alaska.cartera.infrastructure.controller.dto.CarteraDTOToList;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class EnlaceInteresAsignadoOutputDTO implements Serializable {
  private PerfilSimpleOutputDTO perfil;
  private CarteraDTOToList cartera;
  private List<EnlaceInteresOutputDTO> enlacesInteres;
}
