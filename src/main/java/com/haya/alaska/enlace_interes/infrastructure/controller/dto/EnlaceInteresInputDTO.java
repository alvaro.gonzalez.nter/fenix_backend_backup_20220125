package com.haya.alaska.enlace_interes.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class EnlaceInteresInputDTO implements Serializable {
  private Integer id;
  private Integer nivel;
  private String codigo;
  private String nombre;
  private String enlace;
}
