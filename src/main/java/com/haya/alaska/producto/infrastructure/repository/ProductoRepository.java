package com.haya.alaska.producto.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.tipo_movimiento.domain.TipoMovimiento;

@Repository
public interface ProductoRepository extends CatalogoRepository<Producto, Integer> {
	  
}
