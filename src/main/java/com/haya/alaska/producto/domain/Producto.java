package com.haya.alaska.producto.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_PRODUCTO")
@Entity
@Table(name = "LKUP_PRODUCTO")
public class Producto extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PRODUCTO")
  @NotAudited
  private Set<Contrato> contratos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PRODUCTO")
  @NotAudited
  private Set<Segmentacion> segmentaciones = new HashSet<>();
}
