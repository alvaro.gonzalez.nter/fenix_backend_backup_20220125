package com.haya.alaska.criterio_ms_deuda_impagada.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.criterio_ms_deuda_impagada.domain.CriterioMSDeudaImpagada;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CriterioMSDeudaImpagadaRepository extends CatalogoRepository<CriterioMSDeudaImpagada, Integer> {
  List<CriterioMSDeudaImpagada> findAllByCarteraId(Integer id);
  List<CriterioMSDeudaImpagada> findAllByCarteraIdAndActivoIsTrue(Integer ido);
}
