package com.haya.alaska.resolucion_propuesta.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * ResolucionPropuesta entity
 *
 * @author agonzalez
 */
@Audited
@AuditTable(value = "HIST_LKUP_RESOLUCION_PROPUESTA")
@NoArgsConstructor
@Entity
@Table(name = "LKUP_RESOLUCION_PROPUESTA")
public class ResolucionPropuesta extends Catalogo implements Serializable {
}
