package com.haya.alaska.resolucion_propuesta.infrastructure.controller;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ResolucionPropuestaDTO {
  private int rev;
  //private int revType;
  //private int indActivo;
  //private String desCodigo;
  //private String desValor;
}
