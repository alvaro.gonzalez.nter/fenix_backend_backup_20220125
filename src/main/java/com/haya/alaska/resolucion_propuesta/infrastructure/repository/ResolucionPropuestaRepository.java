package com.haya.alaska.resolucion_propuesta.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.resolucion_propuesta.domain.ResolucionPropuesta;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ResolucionPropuestaRepository extends CatalogoRepository<ResolucionPropuesta, Integer> {




  @Query(
    value =
      "SELECT REV AS registro FROM HIST_LKUP_RESOLUCION_PROPUESTA " +
        "WHERE ID = ?1",
    nativeQuery = true)
  List<Map> historicoResolucionPropuesta(Integer idResolucion);








}
