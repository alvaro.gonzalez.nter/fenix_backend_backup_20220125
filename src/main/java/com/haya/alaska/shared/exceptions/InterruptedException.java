package com.haya.alaska.shared.exceptions;

public class InterruptedException extends Exception {
  public InterruptedException(String message) {
    super(message);
  }
}
