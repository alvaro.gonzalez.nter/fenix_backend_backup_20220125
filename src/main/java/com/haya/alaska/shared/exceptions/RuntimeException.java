package com.haya.alaska.shared.exceptions;

public class RuntimeException extends Exception{
  public RuntimeException(String message) {
    super(message);
  }
}
