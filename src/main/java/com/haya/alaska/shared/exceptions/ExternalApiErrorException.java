package com.haya.alaska.shared.exceptions;

import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

public class ExternalApiErrorException extends Exception {
  public ExternalApiErrorException() {
    super(getText());
  }

  public ExternalApiErrorException(String servicio) {
    super(getText(servicio));
  }

  public ExternalApiErrorException(String mensaje, boolean aux) {
    super(mensaje);
  }

  private static String getText() {
    Locale loc = LocaleContextHolder.getLocale();

    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "An error has occurred connecting to an external service";
    else return "Ha ocurrido un error en la conexión a un servicio externo";
  }

  private static String getText(String servicio) {
    Locale loc = LocaleContextHolder.getLocale();

    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Error in the " + servicio + " service, contact your IT department";
    else return "Error en el servicio de " + servicio + ", contacte con su departamento de IT";
  }
}
