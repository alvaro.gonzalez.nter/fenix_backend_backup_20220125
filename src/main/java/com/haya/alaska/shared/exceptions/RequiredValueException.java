package com.haya.alaska.shared.exceptions;

import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

public class RequiredValueException extends Exception {

  public RequiredValueException(String entity) {
    super(getText(entity));
  }

  public RequiredValueException(Integer id) {
    super(getText(id));
  }

  private static String getText(String entity) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "The field '" + entity + "' is required";
    else return "El campo '" + entity + "' es obligatorio";
  }

  private static String getText(Integer id) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "You cannot assign files to the portfolio manager with id " + id + ". File limit exceeded.";
    else return "No se pueden asignar expedientes al responsable de cartera con id " + id + ". Límite de expedientes superado.";
  }
}
