package com.haya.alaska.shared.exceptions;

public class UnfulfilledConditionException extends Exception {

  public UnfulfilledConditionException(String message) {
    super(message);
  }
}
