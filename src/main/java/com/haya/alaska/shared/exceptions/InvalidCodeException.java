package com.haya.alaska.shared.exceptions;

public class InvalidCodeException extends Exception {

  public InvalidCodeException(String message) {
    super(message);
  }
}
