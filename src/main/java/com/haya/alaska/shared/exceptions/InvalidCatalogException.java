package com.haya.alaska.shared.exceptions;

import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

public class InvalidCatalogException extends Exception {

  public InvalidCatalogException() {
    super(getText());
  }

  public InvalidCatalogException(String message) {
    super(message);
  }

  private static String getText() {
    Locale loc = LocaleContextHolder.getLocale();

    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "There is already a catalog value with the same code";
    else return "Ya existe un valor del catálgo con el mismo código";
  }
}
