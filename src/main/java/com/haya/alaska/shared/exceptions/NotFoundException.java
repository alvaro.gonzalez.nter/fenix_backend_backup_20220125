package com.haya.alaska.shared.exceptions;

import org.springframework.context.i18n.LocaleContextHolder;

import java.util.List;
import java.util.Locale;

public class NotFoundException extends Exception {

  private static final long serialVersionUID = 1L;

  public NotFoundException(String message) {
    super(message);
  }

  // No encontrado en el maestro
  public NotFoundException(String message, Boolean maestro) {
    super(getText(message, maestro));
  }

  // No encontrado 'entity' con 'id'
  public NotFoundException(String entity, Integer id) {
    super(getText(entity, id));
  }

  // No encontrado 'entityIn' con 'idIn' en 'entityOut' con 'idOut'
  public NotFoundException(String entityIn, Integer idIn, String entityOut, Integer idOut) {
    super(getText(entityIn, idIn, entityOut, idOut));
  }

  // No encontrado 'entityIn' con 'codeIn' en 'entityOut' con 'idOut'
  public NotFoundException(String entityIn, String codeIn, String entityOut, Integer idOut) {
    super(getText(entityIn, codeIn, entityOut, idOut));
  }

  // 'Entity' con 'id' desactivada
  public NotFoundException(String entity, Integer id, Boolean activo) {
    super(getText(entity, id, activo));
  }

  // No encontrado 'entity' con campo 'field'
  public NotFoundException(String entity, String field) {
    super(getText(entity, field));
  }

  // No encontrado 'entity' con campo 'field' : 'value'
  public NotFoundException(String entity, String field, String value) {
    super(getText(entity, field, value));
  }

  // No encontrado 'entity' : 'name' para el 'field' : 'value'
  public NotFoundException(String entity, String name, String field, String value) {
    super(getText(entity, name, field, value));
  }

  // No encontradas 'entity' con campo 'field' con los valores 'codigos'
  public NotFoundException(String entity, String field, List<String> codigos) {
    super(getText(entity, field, codigos));
  }

  // No encontrado 'field' para la entidad 'entity' con id 'id'
  public NotFoundException(String field, String entity, Integer id) {
    super(getText(field, entity, id));
  }

  // No encontrada ninguna '
  public NotFoundException(String entityIn, String entityOut, Integer id, Boolean in) {
    super(getText(entityIn, entityOut, id, in));
  }

  // El 'entity' de la fila 'integer' no tiene 'field'
  public NotFoundException(String entity, Integer integer, String field) {
    super(getText(entity, integer, field));
  }

  // No encontrado 'entity' con 'filed': 'value' para 'entity2': 'value2'
  public NotFoundException(
      String entity, String filed, String value, String entity2, String value2) {
    super(getText(entity, filed, value, entity2, value2));
  }

  // No encontrado 'entity' con 'filed': 'value' para 'entity2' con id: 'value2'
  public NotFoundException(
          String entity, String filed, String value, String entity2, Integer value2) {
    super(getText(entity, filed, value, entity2, value2));
  }

  // La columna 'column' de la fila 'row' tiene un formato incorrecto (se espera 'value'). Actualmente es: 'raw'"
  public NotFoundException(Integer column, Integer row, String value, String raw) {
    super(getText(column, row, value, raw));
  }

  private static String getText(Integer column, Integer row, String value, String raw) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Column " + column + " in row " + row + " has an incorrect format (expect " + value + "). Currently it is: '" + raw + "'";
    else return "La columna " + column + " de la fila " + row + " tiene un formato incorrecto (se espera " + value + "). Actualmente es: '" + raw + "'";
  }

  private static String getText(
      String entity, String filed, String value, String entity2, String value2) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found "
          + entity
          + " with "
          + filed
          + ": '"
          + value
          + "' for"
          + entity2
          + ":"
          + value2;
    else
      return "No encontrado "
          + entity
          + " con "
          + filed
          + ": '"
          + value
          + "' para "
          + entity2
          + ": "
          + value2;
  }

  private static String getText(
          String entity, String filed, String value, String entity2, Integer value2) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found "
              + entity
              + " with "
              + filed
              + ": '"
              + value
              + "' for"
              + entity2
              + " con id: "
              + value2;
    else
      return "No encontrado "
              + entity
              + " con "
              + filed
              + ": '"
              + value
              + "' para "
              + entity2
              + ": "
              + value2;
  }

  private static String getText(String entity, Integer integer, String field) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "The " + entity + " of the row " + integer + " does not have " + field;
    else return "El " + entity + " de la fila " + integer + " no tiene " + field;
  }

  private static String getText(String entity, Boolean maestro) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found data: " + entity + " in the master";
    else return "No se ha encontrado el dato: " + entity + " en el maestro";
  }

  private static String getText(String entity, Integer id) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found data: " + entity + " with id: " + id.toString();
    else return "No se ha encontrado el dato: " + entity + " con id: " + id.toString();
  }

  private static String getText(String entityIn, Integer idIn, String entityOut, Integer idOut) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found data: "
          + entityIn
          + " with id: "
          + idIn.toString()
          + ", for the data: "
          + entityOut
          + " of id: "
          + idOut;
    else
      return "No se ha encontrado el dato: "
          + entityIn
          + " de id: "
          + idIn.toString()
          + ", para el dato: "
          + entityOut
          + " de id: "
          + idOut;
  }

  private static String getText(String entityIn, String codeIn, String entityOut, Integer idOut) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found data: "
          + entityIn
          + " with code: "
          + codeIn
          + ", for the data: "
          + entityOut
          + " of id: "
          + idOut;
    else
      return "No se ha encontrado el dato: "
          + entityIn
          + " de código: "
          + codeIn
          + ", para el dato: "
          + entityOut
          + " de id: "
          + idOut;
  }

  private static String getText(String entity, String field) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found data: " + entity + " with the field " + field;
    else return "No se ha encontrado el dato: " + entity + " con el campo " + field;
  }

  private static String getText(String entity, String field, String value) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found data: " + entity + " with the " + field + ":" + value;
    else return "No se ha encontrado la entidad: " + entity + " con el " + field + ":" + value;
  }

  private static String getText(String entity, String name, String field, String value) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found data: " + entity + ":" + name + " for the entity " + field + ":" + value;
    else
      return "No se ha encontrado la entidad: "
          + entity
          + ":"
          + name
          + " para la entidad "
          + field
          + ":"
          + value;
  }

  private static String getText(String entity, String field, List<String> codigos) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found data: "
          + entity
          + " with the field "
          + field
          + " with the values "
          + codigos.toString();
    else
      return "No se han encontrado las entidades: "
          + entity
          + " con el campo "
          + field
          + " con los valores "
          + codigos.toString();
  }

  private static String getText(String field, String entity, Integer id) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found the " + field + " in the entity " + entity + " whit the id:  " + id;
    else
      return "No se ha encontrado el " + field + " para la entidad " + entity + " con el id: " + id;
  }

  private static String getText(String entity, Integer id, Boolean activo) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Data: " + entity + " with id: " + id.toString() + " isn't active";
    else return "La entidad: " + entity + " con id: " + id.toString() + " no está activa";
  }

  private static String getText(String entityIn, String entityOut, Integer id, Boolean in) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "Not found data "
          + entityIn
          + ", in the entity "
          + entityOut
          + " with id: "
          + id.toString();
    else
      return "No se han encontrado ninguna entidad "
          + entityIn
          + ", en la entidad: "
          + entityOut
          + " de id: "
          + id.toString();
  }
}
