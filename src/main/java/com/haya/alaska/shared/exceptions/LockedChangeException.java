package com.haya.alaska.shared.exceptions;

import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

public class LockedChangeException extends Exception {

  public LockedChangeException(String message) {
    super(message);
  }

  public LockedChangeException() {
    super(getText());
  }

  public LockedChangeException(Boolean maestro) {
    super(getText(maestro));
  }

  private static String getText() {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "It is not possible to make the responsible change. To assign a new person in charge of the minutes or assistance to signature, first assign an agency to the offer.";
    else return "No es posible realizar el cambio responsable. Para asignar un nuevo responsable minuta o de asistencia a firma, asigna antes una gestoría a la oferta.";
  }

  private static String getText(Boolean maestro) {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "The contract could not be inserted into the teacher system";
    else return "No se pudo insertar el contrato en el sistema de maestros";
  }
}
