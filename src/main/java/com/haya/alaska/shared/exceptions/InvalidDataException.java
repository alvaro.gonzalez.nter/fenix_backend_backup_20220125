package com.haya.alaska.shared.exceptions;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.BindException;

import java.util.Locale;

public class InvalidDataException extends BindException {

  public InvalidDataException(Object target) {
    super(target, getText());
  }

  private static String getText() {
    Locale loc = LocaleContextHolder.getLocale();
    if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
      return "The associated contract does not exist";
    else return "El contrato asociado no existe";
  }
}
