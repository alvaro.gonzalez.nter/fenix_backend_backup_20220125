package com.haya.alaska.shared;

import com.haya.alaska.acuerdo_pago.aplication.AcuerdoPagoUseCase;
import com.haya.alaska.acuerdo_pago.domain.AcuerdoPago;
import com.haya.alaska.acuerdo_pago.infrastructure.repository.AcuerdoPagoRepository;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import com.haya.alaska.estado_evento.infrastructure.repository.EstadoEventoRepository;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.importancia_evento.domain.ImportanciaEvento;
import com.haya.alaska.importancia_evento.infrastructure.repository.ImportanciaEventoRepository;
import com.haya.alaska.movimiento.application.MovimientoUseCase;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.movimiento.infrastructure.repository.MovimientoRepository;
import com.haya.alaska.nivel_evento.domain.NivelEvento;
import com.haya.alaska.nivel_evento.infrastructure.repository.NivelEventoRepository;
import com.haya.alaska.plazo_acuerdo_pago.domain.PlazoAcuerdoPago;
import com.haya.alaska.plazo_acuerdo_pago.infrastructure.repository.PlazoAcuerdoPagoRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class ConciliacionAcuerdoPagoMovimiento {

    @Autowired AcuerdoPagoUseCase acuerdoPagoUseCase;
    @Autowired AcuerdoPagoRepository acuerdoPagoRepository;
    @Autowired PlazoAcuerdoPagoRepository plazoAcuerdoPagoRepository;
    @Autowired MovimientoUseCase movimientoUseCase;
    @Autowired MovimientoRepository movimientoRepository;
    @Autowired EventoUseCase eventoUseCase;
    @Autowired SubtipoEventoRepository subtipoEventoRepository;
    @Autowired EstadoEventoRepository estadoEventoRepository;
    @Autowired ImportanciaEventoRepository importanciaEventoRepository;
    @Autowired NivelEventoRepository nivelEventoRepository;
    @Autowired EventoRepository eventoRepository;


    @Scheduled(cron = "0 0 1 * * ?", zone = "Europe/Madrid")
    //@Scheduled(cron = "*/30 * * * * *", zone = "Europe/Madrid")
    @Transactional(rollbackFor = Exception.class)
    public void checkAcuerdosPago() throws NotFoundException {
        log.info("Iniciando tarea programada: ConciliacionAcuerdoPagoMovimiento::checkAcuerdosPago");

        //acuerdos de pago dados de alta en el sistema
        var fechaIniActualConMargen = fechaInicialConMargen(new Date());
        var fechaFinActualConMargen = fechaFinalConMargen(new Date());
        var acuerdosPago = acuerdoPagoUseCase.getAllAcuerdosDePagoByAbonadoIsFalse();

        //TODO pendiente de módulo de administración para usar los valores de "Margen tolerancia plazo"
        var importePlazoAcuerdoPago = 0.0;
        var variacionImporte = 0.0;
        var expedienteId = 0;
        List<Movimiento> movimientos = new ArrayList<>();
        for(AcuerdoPago acuerdoPago: acuerdosPago){
            //necesitamos coger los movimientos desde la fecha de inicio del acuerdo de pago hasta la fecha final con +-3 días
            Date fechaFinConMargen = fechaFinalConMargen(acuerdoPago.getFechaFinal());
            Date fechaInicio = acuerdoPago.getFechaInicio();
            var plazosAcuerdoPago = plazoAcuerdoPagoRepository.findAllByAcuerdoPagoIdAndFechaBetweenAndAbonadoIsFalse(acuerdoPago.getId(), fechaIniActualConMargen, fechaFinActualConMargen);
            for(PlazoAcuerdoPago plazoAcuerdoPago: plazosAcuerdoPago){
                importePlazoAcuerdoPago = plazoAcuerdoPago.getCantidad();
                variacionImporte = importePlazoAcuerdoPago*0.05;
                expedienteId = acuerdoPago.getExpediente().getId();

                //calculamos movimientos entre fechas con +-3 días
                movimientos = movimientoUseCase.findAllByIdExpedienteFechaValorBetween(expedienteId, fechaInicio, fechaFinConMargen);
                for(Movimiento movimiento: movimientos){
                    if(movimiento.getActivo() && movimiento.getAfecta() && movimiento.getTipo().getCodigo().equals("2")){
                        if(movimiento.getImporte() >= (importePlazoAcuerdoPago - variacionImporte)){
                            movimiento.setAbonado(true);
                            plazoAcuerdoPago.setAbonado(true);
                            movimientoRepository.saveAndFlush(movimiento);
                        }
                    }
                }
                if(!plazoAcuerdoPago.getAbonado()){
                    //crear alerta para indicar que no se ha cumplido el acuerdo de pago, puesto que no se ha abonado el plazo del acuerdo de pago
                    generarAlerta(acuerdoPago);
                }
                plazoAcuerdoPagoRepository.saveAndFlush(plazoAcuerdoPago);
            }
            //comprobamos e indicamos si se han abonado de todos los plazos del acuerdo de pago
            if(checkAbonoAcuerdoPago(acuerdoPago)) acuerdoPago.setAbonado(true);
            acuerdoPagoRepository.saveAndFlush(acuerdoPago);
        }
    }

    private void generarAlerta(AcuerdoPago acuerdoPago) throws NotFoundException {
        SubtipoEvento subtipoEvento = subtipoEventoRepository.findByCodigo("CUM01").get();
        EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("PEN").get();
        ImportanciaEvento importanciaEvento = importanciaEventoRepository.findByCodigo("MED").get();
        NivelEvento nivelEvento = nivelEventoRepository.findByCodigo("EXP").get();

        String descripcion = "Actualmente no se ha cumplido el acuerdo de pago con ID " + acuerdoPago.getId();

        Evento evento = new Evento();
        evento.setCartera(acuerdoPago.getExpediente().getCartera());
        evento.setTitulo("Acuerdo de pago incumplido");
        evento.setComentariosDestinatario(descripcion);
        evento.setFechaLimite(acuerdoPago.getFechaFinal());
        evento.setFechaCreacion(new Date());
        evento.setFechaAlerta(new Date());
        evento.setClaseEvento(subtipoEvento.getTipoEvento().getClaseEvento());
        evento.setTipoEvento(subtipoEvento.getTipoEvento());
        evento.setSubtipoEvento(subtipoEvento);
        evento.setPeriodicidad(null);
        evento.setEstado(estadoEvento);
        evento.setImportancia(importanciaEvento);
        evento.setResultado(null);
        evento.setEmisor(null);
        if(acuerdoPago.getExpediente().getGestor() == null)
            throw new NotFoundException("gestor", "expediente", acuerdoPago.getExpediente().getId());
        evento.setDestinatario(acuerdoPago.getExpediente().getGestor());
        evento.setProgramacion(true);
        evento.setCorreo(false);
        evento.setRevisado(false);
        evento.setAutogenerado(true);
        evento.setNivel(nivelEvento);
        evento.setIdNivel(1);
        eventoRepository.saveAndFlush(evento);
    }

    private Boolean checkAbonoAcuerdoPago(AcuerdoPago acuerdoPago){
        List<PlazoAcuerdoPago> plazos = plazoAcuerdoPagoRepository.findAllByAcuerdoPagoId(acuerdoPago.getId());
        for(PlazoAcuerdoPago plazo: plazos){
            if(!plazo.getAbonado()) return false;
        }
        return true;
    }

    private Date fechaInicialConMargen(Date fecha){
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        c.add(Calendar.DAY_OF_MONTH, -3);
        return c.getTime();
    }

    private Date fechaFinalConMargen(Date fecha){
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        c.add(Calendar.DAY_OF_MONTH, 3);
        return c.getTime();
    }
}
