package com.haya.alaska.shared;

import com.haya.alaska.catalogo.domain.Catalogo;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

public class ExcelExport {
  public ByteArrayInputStream create(LinkedHashMap<String, List<Collection<?>>> datos) throws IOException {
    Workbook workbook = new XSSFWorkbook();

    CellStyle cellStyleDate = workbook.createCellStyle();
    cellStyleDate.setDataFormat((short)14);


    CellStyle styleHeader = workbook.createCellStyle();
    Font font = workbook.createFont();
    font.setBold(true);
    font.setColor(IndexedColors.WHITE.getIndex());
    styleHeader.setFont(font);
    //styleHeader.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
    byte[] rgb = new byte[3];
    rgb[0] = (byte) 10; // red
    rgb[1] = (byte) 148; // green
    rgb[2] = (byte) 214; // blue
    //create XSSFColor
    XSSFColor color = new XSSFColor(rgb, new DefaultIndexedColorMap());
    XSSFCellStyle xssfcellcolorstyle = (XSSFCellStyle) styleHeader;
    xssfcellcolorstyle.setFillForegroundColor(color);
    xssfcellcolorstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

    for (var datosHoja : datos.entrySet()) {
      Sheet sheet = workbook.createSheet(datosHoja.getKey());
      int indiceFila = 0;
      for (Collection<?> datosFila : datosHoja.getValue()) {
        Row filaExcel = sheet.createRow(indiceFila);
        int indiceColumna = 0;
        for (Object dato : datosFila) {
          Cell celdaExcel = filaExcel.createCell(indiceColumna);
          if (indiceFila == 0) {
            celdaExcel.setCellStyle(xssfcellcolorstyle);
            sheet.setColumnWidth(indiceColumna, 15 * 256);
          }
          if (dato != null) {
            if (dato instanceof Date) {
              celdaExcel.setCellStyle(cellStyleDate);
              celdaExcel.setCellValue((Date) dato);
            } else if (dato instanceof Boolean) {
              if (dato == Boolean.TRUE) {
                if (LocaleContextHolder.getLocale().getLanguage() == null
                    || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
                  celdaExcel.setCellValue("Yes");
                } else {
                  celdaExcel.setCellValue("Sí");
                }
              } else if (dato == Boolean.FALSE) {
                celdaExcel.setCellValue("No");
              }
            } else if (dato instanceof Number) {
              celdaExcel.setCellValue(((Number) dato).doubleValue());
            } else if (dato instanceof Catalogo) {
              celdaExcel.setCellValue(((Catalogo) dato).getValor());
            } else if (dato.equals("")) {
              celdaExcel.setCellValue("-");
            } else {
              celdaExcel.setCellValue(dato.toString());
            }
          } else {
            celdaExcel.setCellValue("-");
          }
          indiceColumna++;
        }
        indiceFila++;
      }
    }

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      workbook.write(outputStream);
      return new ByteArrayInputStream(outputStream.toByteArray());
    }
  }

  public ResponseEntity<InputStreamResource> download(ByteArrayInputStream excel, String nombre) {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
    headers.add("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    headers.add("Content-Disposition", "attachment; filename=" + nombre + ".xlsx");
    return ResponseEntity
      .ok()
      .headers(headers)
      .body(new InputStreamResource(excel));
  }

  private class HSSFWorkbook {
  }
}
