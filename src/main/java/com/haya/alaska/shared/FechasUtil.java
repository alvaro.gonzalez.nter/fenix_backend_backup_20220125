package com.haya.alaska.shared;

import java.time.Instant;
import java.util.*;

public class FechasUtil {

  /**
   * Construye una instancia del Calendario local y devuelve la fecha actual
   *
   * @return La fecha actual
   */
  public static Date hoy() {
    Calendar calendar = Calendar.getInstance(new Locale("es", "ES"));
    Calendar.getInstance(new Locale("es", "ES"));

    return calendar.getTime();
  }

  /**
   * Construye una instancia de Calendario local y le suma el número de días recibido como parámetro
   *
   * @param fechaInicial El Date inicial
   * @param numDiasSumar El número de días a sumar
   * @return El Date resultante
   */
  public static Date sumarDias(Date fechaInicial, int numDiasSumar) {
    Calendar calendar = Calendar.getInstance(new Locale("es", "ES"));
    calendar.setTime(fechaInicial);
    calendar.add(Calendar.DATE, numDiasSumar);

    return calendar.getTime();
  }

  /**
   * Compara si dos o más fechas son iguales
   *
   * @param fechas Las fechas a comparar
   * @return True si todas las fechas son iguales
   */
  public static boolean sonIguales(Date... fechas) {

    Set<Instant> instantes = new LinkedHashSet<>();

    for (Date fecha : fechas) {

      if (Objects.isNull(fecha)) {
        return false;
      }

      instantes.add(fecha.toInstant());
    }

    return instantes.size() == 1;
  }
}
