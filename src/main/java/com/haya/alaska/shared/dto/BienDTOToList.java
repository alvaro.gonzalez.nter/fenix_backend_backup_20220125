package com.haya.alaska.shared.dto;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.valoracion.domain.Valoracion;
import lombok.*;
import org.springframework.beans.BeanUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienDTOToList implements Serializable {
  private Integer id;
  private Integer idExpediente;
  private String idOrigen;
  private String idHaya;
  private String tipoActivo;
  private Boolean estado;
  private String finca;
  private String localidad;
  private String provincia;
  private Double importeGarantizado;
  private String descripcion;
  private String referenciaCatastral;
  private Double responsabilidadHipotecaria;
  private Boolean posesionNegociada;
  private String liquidez;
  private String contratos;
  private Date fecha;
  private List<ContratoDTOToList> listaContratos;
  private Boolean solicitudAlquiler;

  public BienDTOToList(Bien bien, Integer idContrato) {

    ContratoBien contratoBien = bien.getContratoBien(idContrato);
    if (contratoBien != null) {
      BeanUtils.copyProperties(
          bien, this, "contratos", "localidad", "provincia", "descripcion", "liquidez", "importeGarantizado", "responsabilidadHipotecaria");

      this.importeGarantizado = contratoBien.getImporteGarantizado();
      this.responsabilidadHipotecaria = contratoBien.getResponsabilidadHipotecaria();

      this.idOrigen = bien.getIdCarga();
      this.localidad = bien.getLocalidad() != null ? bien.getLocalidad().getValor() : null;
      this.provincia = bien.getProvincia() != null ? bien.getProvincia().getValor() : null;
      this.liquidez =
          contratoBien.getLiquidez() != null ? contratoBien.getLiquidez().getValor() : null;

      this.posesionNegociada = bien.getPosesionNegociada() ;

      if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
        this.tipoActivo =
            bien.getTipoActivo() != null ? bien.getTipoActivo().getValorIngles() : null;
        this.descripcion = bien.getDescripcion();

      } else {
        this.tipoActivo = bien.getTipoActivo() != null ? bien.getTipoActivo().getValor() : null;
        this.descripcion = bien.getDescripcion();
      }

      List<String> contratos = new ArrayList<>();
      for (ContratoBien cb : bien.getContratos()) {
        contratos.add(cb.getContrato().getIdCarga());
      }
      this.contratos = String.join(",", contratos);
    }
    if (!bien.getTasaciones().isEmpty()) {
      List<Tasacion> tasaciones =
          bien.getTasaciones().stream()
              .sorted(Comparator.comparing(Tasacion::getFecha).reversed())
              .collect(Collectors.toList());
      Tasacion t = tasaciones.get(0);
      this.fecha = t.getFecha();
    } else if (!bien.getValoraciones().isEmpty()) {
      List<Valoracion> tasaciones =
          bien.getValoraciones().stream()
              .sorted(Comparator.comparing(Valoracion::getFecha).reversed())
              .collect(Collectors.toList());
      Valoracion t = tasaciones.get(0);
      this.fecha = t.getFecha();
    }
  }

  public BienDTOToList(Bien bien) {
    BeanUtils.copyProperties(
        bien,
        this,
        "contratos",
        "localidad",
        "provincia",
        "descripcion",
        "liquidez",
        "latitud",
        "longitud");
    this.idOrigen = bien.getIdCarga();
    this.localidad = bien.getLocalidad() != null ? bien.getLocalidad().getValor() : null;
    this.provincia = bien.getProvincia() != null ? bien.getProvincia().getValor() : null;

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.tipoActivo = bien.getTipoActivo() != null ? bien.getTipoActivo().getValorIngles() : null;
      this.descripcion = bien.getDescripcion();

    } else {
      this.tipoActivo = bien.getTipoActivo() != null ? bien.getTipoActivo().getValor() : null;
      this.descripcion = bien.getDescripcion();
    }

    List<String> contratos = new ArrayList<>();
    for (ContratoBien cb : bien.getContratos()) {
      contratos.add(cb.getContrato().getIdCarga());
    }
    this.contratos = String.join(",", contratos);

    if (!bien.getTasaciones().isEmpty()) {
      List<Tasacion> tasaciones =
          bien.getTasaciones().stream()
              .sorted(Comparator.comparing(Tasacion::getFecha).reversed())
              .collect(Collectors.toList());
      Tasacion t = tasaciones.get(0);
      this.fecha = t.getFecha();
    } else if (!bien.getValoraciones().isEmpty()) {
      List<Valoracion> tasaciones =
          bien.getValoraciones().stream()
              .sorted(Comparator.comparing(Valoracion::getFecha).reversed())
              .collect(Collectors.toList());
      Valoracion t = tasaciones.get(0);
      this.fecha = t.getFecha();
    }
    this.posesionNegociada = bien.getPosesionNegociada() ;
  }

  public static List<BienDTOToList> newList(List<Bien> bienes, Integer idContrato) {
    return bienes.stream()
        .map(bien -> new BienDTOToList(bien, idContrato))
        .collect(Collectors.toList());
  }

  public static List<BienDTOToList> newListExpediente(List<Bien> bienes, Integer idExpediente) {
    List<BienDTOToList> list = new ArrayList<>();
    for (Bien bien : bienes) {
      Integer idContrato = null;
      for (ContratoBien cb : bien.getContratos()) {
        if (cb.getContrato().getExpediente().getId().equals(idExpediente)) {
          idContrato = cb.getContrato().getId();
          break;
        }
      }
      if (idContrato != null) {
        list.add(new BienDTOToList(bien, idContrato));
      }
    }
    return list;
  }
}
