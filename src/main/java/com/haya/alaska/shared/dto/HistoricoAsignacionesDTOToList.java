package com.haya.alaska.shared.dto;

import com.haya.alaska.contrato.historico.ContratoHistoricoAsignaciones;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class HistoricoAsignacionesDTOToList implements Serializable {

  private Date fechaInicio;
  private Date fechaFin;
  private Integer revInfoInicio;
  private Integer revInfoFin;
  private String expedienteId;
  private String usuarioGestor;

  public HistoricoAsignacionesDTOToList(
          ContratoHistoricoAsignaciones contratoHistoricoAsignaciones) {
    BeanUtils.copyProperties(contratoHistoricoAsignaciones, this);
  }

  public static List<HistoricoAsignacionesDTOToList> newList(
          List<ContratoHistoricoAsignaciones> historico) {
    return historico.stream().map(HistoricoAsignacionesDTOToList::new).collect(Collectors.toList());
  }
}
