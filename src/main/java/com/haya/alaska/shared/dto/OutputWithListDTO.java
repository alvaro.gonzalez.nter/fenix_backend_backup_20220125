package com.haya.alaska.shared.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor
public class OutputWithListDTO<T> {

  private T object;
  private long count;

  public OutputWithListDTO(T object, long count) {
    this.object = object;
    this.count = count;
  }
}
