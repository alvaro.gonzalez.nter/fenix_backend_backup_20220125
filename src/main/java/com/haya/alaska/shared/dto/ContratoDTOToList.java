package com.haya.alaska.shared.dto;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.controller.dto.EstrategiaAsignadaOutputDTO;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.interviniente.domain.Interviniente;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContratoDTOToList implements Serializable {

  private static final long serialVersionUID = 1L;
  private Integer id;
  private String idOrigen;
  private Boolean esRepresentante;
  private String producto;
  private String estadoContratos;
  private String primerTitular;
  private Double saldoEnGestion;
  private Integer intervinientes;
  private Integer garantias;
  private Boolean judicial;
  private Boolean estado;
  private String situacion;
  private String rango;
  private String territorial;
  private Double responsabilidadHipotecaria;
  private EstrategiaAsignadaOutputDTO estrategia;
  private String operacion;
  private Double capitalPendiente;
  private Double quita;
  private Double precioMinimoVenta;
  private Double precioWebVenta;
  private Integer ordenIntervencion;
  private String tipoIntervencion;
  private String clasificacionContable;
  private String idHaya;



  public ContratoDTOToList(Contrato contrato) {
    if (contrato == null) contrato = new Contrato();
    this.id = contrato.getId();
    this.idOrigen = contrato.getIdCarga();
    this.operacion = contrato.getIdOrigen();
    this.esRepresentante = contrato.isRepresentante();
    this.territorial = contrato.getTerritorial();

    if (contrato.getPrimerInterviniente() != null) {
      if (contrato.getPrimerInterviniente().getRazonSocial() != null) {
        this.primerTitular = contrato.getPrimerInterviniente().getRazonSocial();
      } else {
        this.primerTitular = contrato.getPrimerInterviniente().getNombre() + " " + contrato.getPrimerInterviniente().getApellidos();
      }
    }
    if (contrato.getSaldoContrato() != null) {
      this.saldoEnGestion = contrato.getSaldoContrato().getSaldoGestion();
      this.capitalPendiente = contrato.getSaldoContrato().getCapitalPendiente();
      this.quita = 0.0;
    }
    if (contrato.getIntervinientes() != null)
      this.intervinientes = contrato.getIntervinientes().size();
    if (contrato.getBienes() != null) this.garantias = contrato.getBienes().size();
    this.judicial = !contrato.getProcedimientos().isEmpty();
    this.estado = contrato.getActivo();
    if (contrato.getSituacion() != null)
      this.situacion = contrato.getSituacion().getValor();
    this.estrategia = contrato.getEstrategia() != null ? new EstrategiaAsignadaOutputDTO(contrato.getEstrategia()) : null;
    if (contrato.getRangoHipotecario() != null) {
      this.rango = contrato.getRangoHipotecario();
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      if (contrato.getSituacion() != null) {
        this.situacion = contrato.getSituacion().getValorIngles();
      }
      if (contrato.getEstadoContrato() != null)
        this.estadoContratos = contrato.getEstadoContrato().getValorIngles();
      if (contrato.getProducto() != null) this.producto = contrato.getProducto().getValorIngles();
      if (contrato.getClasificacionContable() != null) {
        this.clasificacionContable =
          contrato.getClasificacionContable().getValorIngles() != null
            ? contrato.getClasificacionContable().getValorIngles()
            : null;
      }

    } else {
      if (contrato.getSituacion() != null) {
        this.situacion = contrato.getSituacion().getValor();
      }
      if (contrato.getEstadoContrato() != null)
        this.estadoContratos = contrato.getEstadoContrato().getValor();
      if (contrato.getProducto() != null) this.producto = contrato.getProducto().getValor();
      if (contrato.getClasificacionContable() != null) {
        this.clasificacionContable =
            contrato.getClasificacionContable().getValor() != null
                ? contrato.getClasificacionContable().getValor()
                : null;
      }
    }
  }

  public ContratoDTOToList(Contrato contrato, Bien bien) {
    this(contrato);
    for(var propuesta: contrato.getPropuestas()) {
      for(var propuestabien: propuesta.getBienes()) {
        if(propuestabien.getBien().equals(bien)) {
          this.precioMinimoVenta = propuestabien.getPrecioMinimoVenta();
          this.precioWebVenta = propuestabien.getPrecioWebVenta();
        }
      }
    }
    if(Objects.nonNull(contrato) && Objects.nonNull(contrato.getId()) && Objects.nonNull(bien)) {
      ContratoBien contratoBien = bien.getContratoBien(contrato.getId());
      if (Objects.nonNull(contratoBien) && Objects.nonNull(contratoBien.getResponsabilidadHipotecaria())) {
        this.responsabilidadHipotecaria = contratoBien.getResponsabilidadHipotecaria();
      }
    }
  }

  public ContratoDTOToList(Contrato contrato, Interviniente interviniente) {
    this(contrato);
    for(var intervinienteSacado: contrato.getIntervinientes()) {
      if(interviniente.equals(interviniente)) {
        this.ordenIntervencion = intervinienteSacado.getOrdenIntervencion();
        this.tipoIntervencion = intervinienteSacado.getTipoIntervencion().getValor();
      }
    }
  }

}
