package com.haya.alaska.shared.dto;

import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class ListWithCountDTO<T> {

  private final List<T> list;
  private final int count;

  public ListWithCountDTO(List<T> list, int count) {
    this.list = list;
    this.count = count;
  }
}
