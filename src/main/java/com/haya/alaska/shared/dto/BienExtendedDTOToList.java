package com.haya.alaska.shared.dto;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienExtendedDTOToList extends BienDTOToList implements Serializable {
  private Integer idExpediente;
  private Integer idBien;
  private Double latitud;
  private Double longitud;
  private String direccion;
  private String estadoString;
  private List<Long> fotos;

  public BienExtendedDTOToList(Bien bien, Integer idContrato) {
    super(bien);

    if (bien != null) {
      this.idBien = bien.getId();
      this.direccion = this.direccionConcatBien(bien);
      this.latitud = bien.getLatitud();
      this.longitud = bien.getLongitud();
      this.idExpediente = bien.getContratoBien(idContrato).getContrato().getExpediente().getId();
    }
  }

  public BienExtendedDTOToList(BienPosesionNegociada bienPN, Bien bien) {
    super(bien);

    if (bien != null) {
      this.idBien = bien.getId();
      this.direccion = this.direccionConcatBien(bien);
      this.latitud = bien.getLatitud();
      this.longitud = bien.getLongitud();
    }

    // a partir de aqui se rellenan los campos de PN si no es null
    if (bienPN == null) return;
    List<Long> tempFotos = new ArrayList<>();
    super.setId(bienPN.getId());
    this.setEstadoString(
        bienPN.getEstadoOcupacion() != null ? bienPN.getEstadoOcupacion().getValor() : null);
    this.idExpediente = bienPN.getExpedientePosesionNegociada().getId();

    for (var visita : bienPN.getVisitas()) {
      for (var foto : visita.getFotos()) {
        tempFotos.add(foto);
      }
    }
    this.fotos = tempFotos;
  }

  private String direccionConcatBien(Bien bien) {
    String direccionString = "";
    direccionString += bien.getTipoVia() != null ? bien.getTipoVia().getValor() + " " : "";
    direccionString += bien.getNombreVia() != null ? bien.getNombreVia() + " " : "";
    direccionString += bien.getNumero() != null ? bien.getNumero() + " " : "";
    direccionString += bien.getPortal() != null ? bien.getPortal() + " " : "";
    direccionString += bien.getBloque() != null ? bien.getBloque() + " " : "";
    direccionString += bien.getEscalera() != null ? bien.getEscalera() + " " : "";
    direccionString += bien.getPiso() != null ? bien.getPiso() + " " : "";
    direccionString += bien.getPuerta() != null ? bien.getPuerta() + " " : "";
    direccionString += bien.getEntorno() != null ? bien.getEntorno().getValor() + " " : "";
    direccionString += bien.getCodigoPostal() != null ? bien.getCodigoPostal() + " " : "";
    direccionString += bien.getMunicipio() != null ? bien.getMunicipio().getValor() + " " : "";
    direccionString += bien.getLocalidad() != null ? bien.getLocalidad().getValor() + " " : "";
    direccionString += bien.getProvincia() != null ? bien.getProvincia().getValor() + " " : "";
    return direccionString;
  }
}
