package com.haya.alaska.shared.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ValoracionDTOToList {
  private Integer id;
  private String valorador;
  private CatalogoMinInfoDTO tipoValoracion;
  private Double importe;
  private Date fecha;
  private CatalogoMinInfoDTO liquidez;
}
