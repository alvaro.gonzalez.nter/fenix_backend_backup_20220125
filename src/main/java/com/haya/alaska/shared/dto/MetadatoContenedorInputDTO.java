package com.haya.alaska.shared.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.json.JSONObject;

import java.io.Serializable;

@Getter
@NoArgsConstructor
public class MetadatoContenedorInputDTO implements Serializable {

  @Setter
  private String id;

  @Setter
  private String cliente;

  private String[] tokensCatalogoDocumental;

  public MetadatoContenedorInputDTO(String id, String cliente) {
    this.id = id;
    this.cliente = cliente;
  }
  //Para la parte de usuarios el id hace referencia al id

//Para crear el contenedor pasamos el id y el cliente
    public JSONObject createDescripcion(){
      JSONObject generalDocumento = new JSONObject();
      generalDocumento.put("ID",this.id);
      generalDocumento.put("Cliente",this.cliente);
      return generalDocumento;
    }

//Para
  public JSONObject createDescripcionGeneralDocumento() {
    JSONObject generalDocumento = new JSONObject();
    generalDocumento.put("Serie Documental", this.getSerieDocumental());
    generalDocumento.put("TDN1", this.getTdn1());
    generalDocumento.put("TDN2", this.getTdn2());
    generalDocumento.put("Proceso Carga", "WEB SERVICE ALASKA");
    return generalDocumento;
  }


  public String getSerieDocumental() {
    return this.parseCatalogoDocumental(1);
  }

  public String getTdn1() {

    return this.parseCatalogoDocumental(2);
  }

  public String getTdn2() {


    return this.parseCatalogoDocumental(3);
  }


  private String parseCatalogoDocumental(int posicion) {
    if (this.tokensCatalogoDocumental == null) {
      this.tokensCatalogoDocumental = this.id.split("-");
    }
    return this.tokensCatalogoDocumental[posicion];
  }


  }
