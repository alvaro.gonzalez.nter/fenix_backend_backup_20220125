package com.haya.alaska.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovimientoDTOToList implements Serializable {
  private Integer id;
  private String cartera;
  private String idContrato;
  private CatalogoMinInfoDTO tipoMovimiento;
  private CatalogoMinInfoDTO descripcion;
  private Date fechaContable;
  private Date fechaValor;
  private Double importe;
  private Double principalCapital;
  private Double interesesOrdinarios;
  private Double interesesDemora;
  private Double comisiones;
  private Double gastos;
  private Boolean afecta;
  private Boolean activo;
}
