package com.haya.alaska.shared.dto;


import com.haya.alaska.saldo.infrastructure.controller.dto.SaldoDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Getter
@NoArgsConstructor
public class SaldoMovimientoDto implements Serializable {
  private SaldoDto saldo;
  private ListWithCountDTO<MovimientoDTOToList> movimientos;

  /**   */
  private static final long serialVersionUID = 1L;

  public SaldoMovimientoDto(SaldoDto saldo, ListWithCountDTO<MovimientoDTOToList> movimientos) {
    this.saldo = saldo;
    this.movimientos = movimientos;
  }
}
