package com.haya.alaska.shared.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;

@Getter
@NoArgsConstructor
public class MetadatoDocumentoInputDTO implements Serializable {
  @Setter
  private String nombreFichero;

  @Setter
  private String catalogoDocumental;

  private String[] tokensCatalogoDocumental;

  @Value("${integraciones.gd.usuario}")
  private String usuario;

  public MetadatoDocumentoInputDTO(String nombreFichero, String catalogoDocumental) {
    this.nombreFichero = nombreFichero;
    this.catalogoDocumental = catalogoDocumental;
  }


  public JSONObject createDescripcionGeneralDocumento() {
    JSONObject generalDocumento = new JSONObject();
    generalDocumento.put("Serie Documental", this.getSerieDocumental());
    generalDocumento.put("TDN1", this.getTdn1());
    generalDocumento.put("TDN2", this.getTdn2());
    generalDocumento.put("Proceso Carga", "WEB SERVICE ALASKA");
    return generalDocumento;
  }

  //Usuario que hace la subida del documento
  public JSONObject createDescripcionExtraDatos(String nombreUsuario) {
    JSONObject generalDocumento = new JSONObject();
    generalDocumento.put("usuarioOperacional", nombreUsuario);
    return generalDocumento;
  }


  public JSONObject createDescripcionPropuesta(){
    JSONObject generalDocumento = new JSONObject();
    generalDocumento.put("Serie Documental", this.getSerieDocumental());
    generalDocumento.put("TDN1", this.getTdn1());
    generalDocumento.put("TDN2", this.getTdn2());
    generalDocumento.put("Proceso Carga", "WEB SERVICE ALASKA");
    return generalDocumento;
  }

  public String getSerieDocumental() {
    return this.parseCatalogoDocumental(1);
  }

  public String getTdn1() {
    return this.parseCatalogoDocumental(2);
  }

  public String getTdn2() {
    return this.parseCatalogoDocumental(3);
  }

  private String parseCatalogoDocumental(int posicion) {
    if (this.tokensCatalogoDocumental == null) {
      this.tokensCatalogoDocumental = this.catalogoDocumental.split("-");
    }
    return this.tokensCatalogoDocumental[posicion];
  }


}
