package com.haya.alaska.shared.dto;

import com.haya.alaska.bien_posesion_negociada.infrastructure.controller.dto.BienPosesionNegociadaDTOToList;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CargaDTOToList implements Serializable {
  private Integer id;
  private CatalogoMinInfoDTO tipoCarga;
  private Double importe;
  private CatalogoMinInfoDTO tipoAcreedor;
  private String nombreAcreedor;
  private Date fechaAnotacion;
  private Boolean incluidaOtrosColaterales;
  private Date fechaVencimiento;
  private Integer rango;
  private Boolean cargaPropia;
  private CatalogoMinInfoDTO tipoCargaFormalizacion;
  private CatalogoMinInfoDTO subtipoCargaFormalizacion;
  private List<BienDTOToList> bienes;
  private List<BienPosesionNegociadaDTOToList> bienesPN;

}
