package com.haya.alaska.shared;

import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class DemandadoJudicialExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Procedure Id");
      cabeceras.add("Borrower Id");
      cabeceras.add("Name");
      cabeceras.add("Document");
      cabeceras.add("Phone");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Procedimiento");
      cabeceras.add("Id Interviniente");
      cabeceras.add("Nombre");
      cabeceras.add("Documento");
      cabeceras.add("Telefono");
    }
  }

  public DemandadoJudicialExcel(
      Interviniente interviniente, Integer idProcedimiento, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      this.add("Connection Id", idExpediente);
      this.add("Procedure Id", idProcedimiento);
      this.add("Borrower Id", interviniente.getId());
      if (interviniente.getApellidos() != null) {
        this.add("Name", interviniente.getNombre() + " " + interviniente.getApellidos());
      } else {
        this.add("Name", interviniente.getNombre());
      }
      this.add("Document", interviniente.getNumeroDocumento());
      this.add("Phone", interviniente.getTelefonoPrincipal());

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Procedimiento", idProcedimiento);
      this.add("Id Interviniente", interviniente.getId());
      if (interviniente.getApellidos() != null) {
        this.add("Nombre", interviniente.getNombre() + " " + interviniente.getApellidos());
      } else {
        this.add("Nombre", interviniente.getNombre());
      }
      this.add("Documento", interviniente.getNumeroDocumento());
      this.add("Telefono", interviniente.getTelefonoPrincipal());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
