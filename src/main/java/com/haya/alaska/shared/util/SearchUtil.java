package com.haya.alaska.shared.util;


import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class SearchUtil {

  public static Example<?> getExample(Object example, List<String> wantedFields) {
    String[] notWantedFields = getNotWantedFields(example.getClass(), wantedFields);
    ExampleMatcher matcher =
        ExampleMatcher.matching()
            .withIgnorePaths(notWantedFields)
            .withIgnoreNullValues()
            .withIgnoreCase()
            .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
    return Example.of(example, matcher);
  }

  public static int compareNullSafe(String s1, String s2)
  {
    if(s1 == null && s2 != null)  return -1;
    else if(s1 != null && s2 == null)  return 1;
    else if(s1 == null && s2 == null)  return 0;
    else return s1.compareTo(s2);
  }

  public static int compareNullSafe(Date s1, Date s2)
  {
    if(s1 == null && s2 != null)  return -1;
    else if(s1 != null && s2 == null)  return 1;
    else if(s1 == null && s2 == null)  return 0;
    else return s1.compareTo(s2);
  }

  private static String[] getNotWantedFields(Class<?> objectClass, List<String> wantedFields) {
    List<String> response = new ArrayList<>();
    for (Field field : objectClass.getDeclaredFields()) {
      String fieldName = field.getName();
      if (!wantedFields.contains(fieldName)) response.add(fieldName);
    }
    String[] strings = response.toArray(new String[response.size()]);
    if (strings.length == 0) return new String[] {"_"};
    return strings;
  }
}
