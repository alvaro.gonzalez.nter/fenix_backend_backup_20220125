package com.haya.alaska.shared.util;

import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public abstract class ExcelUtils {
  private final LinkedHashMap<String, Object> datos = new LinkedHashMap<>();
  private final HashSet<String> camposSinTratar = new HashSet<>();

  public ExcelUtils() {
    for (String campo : this.getCabeceras()) {
      this.datos.put(campo, null);
      this.camposSinTratar.add(campo);
    }
  }

  public abstract Collection<String> getCabeceras();

  public List<?> getValuesList() {
    return new ArrayList<>(datos.values());
  }

  public List<String> getCamposSinTratar() {
    return new ArrayList<>(camposSinTratar);
  }

  protected void add(String campo, Object valor) {
    if (this.datos.containsKey(campo)) {
      this.datos.put(campo, valor);
      this.camposSinTratar.remove(campo);
    } else {
      log.warn("Campo no encontrado en Excel: " + campo);
    }
  }
}
