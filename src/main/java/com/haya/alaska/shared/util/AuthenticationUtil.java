package com.haya.alaska.shared.util;

import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class AuthenticationUtil {
  public static boolean isAuthenticated() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return authentication != null && authentication.isAuthenticated();
  }

  public static Usuario getAuthenticated() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return (Usuario) authentication.getPrincipal();
  }

  public static void authenticate(Usuario user) {
    List<SimpleGrantedAuthority> simpleGrantedAuthorities =
        getSimpleGrantedAuthorities(new ArrayList<>());
    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
        new UsernamePasswordAuthenticationToken(user, null, simpleGrantedAuthorities);
    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
  }

  private static List<SimpleGrantedAuthority> getSimpleGrantedAuthorities(List<String> roles) {
    return roles.stream().map(role -> new SimpleGrantedAuthority("")).collect(Collectors.toList());
  }
}
