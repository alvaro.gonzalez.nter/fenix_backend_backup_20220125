package com.haya.alaska.shared.util;

import com.haya.alaska.shared.exceptions.NotFoundException;
import io.micrometer.core.instrument.util.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExcelImport {
  public static Double getNumericValue(XSSFRow row, int column) throws NotFoundException {
    XSSFCell celda = row.getCell(column);
    if (celda == null) {
      return null;
    }
    if (null == celda.getRawValue()) {
      return null;
    }
    try{
      String s = celda.getStringCellValue();
      if (s.equals("-")) return null;
    } catch (Exception e){

    }
    if (celda.getCellType().equals(CellType.NUMERIC)) {
      return celda.getNumericCellValue();
    } else if (celda.getCellType().equals(CellType.STRING)) {
      try {
        Double result = Double.parseDouble(celda.getStringCellValue());
        return result;
      } catch (Exception e) {
        throw new NotFoundException(column, row.getRowNum(),"numérico", celda.getRawValue());
      }
    } else {
      throw new NotFoundException(column, row.getRowNum(),"numérico", celda.getRawValue());
    }
  }

  public static Double getNumericValueConGuion(XSSFRow row, int column) throws NotFoundException {
    XSSFCell celda = row.getCell(column);
    if (celda == null) {
      return null;
    }
    if (null == celda.getRawValue()) {
      return null;
    }
    if (celda.getCellType().equals(CellType.NUMERIC)) {
      return celda.getNumericCellValue();
    } else if (celda.getCellType().equals(CellType.STRING)) {
      try {
        Double result = Double.parseDouble(celda.getStringCellValue());
        return result;
      } catch (Exception e) {
        if (celda.getStringCellValue().equals("-")) return null;
        throw new NotFoundException(column, row.getRowNum(), "numérico", celda.getRawValue());
      }
    } else if (celda.getCellType().equals(CellType.FORMULA)) {
      return celda.getNumericCellValue();
    } else {
      throw new NotFoundException(column, row.getRowNum(), "numérico", celda.getRawValue());
    }
  }

  public static String getStringValue(XSSFRow row, int column) throws NotFoundException {
    XSSFCell celda = row.getCell(column);
    if (celda == null) {
      return null;
    }
    celda.setCellType(CellType.STRING);
    if (celda.getCellType().equals(CellType.STRING)) {
      return celda.getStringCellValue();
    } else {
      throw new NotFoundException(column, row.getRowNum(), "string", celda.getRawValue());
    }
  }

  public static String getRawStringValue(XSSFRow row, int column) {
    XSSFCell celda = row.getCell(column);
    if (celda == null) {
      return null;
    }
    if (celda.getCellType().equals(CellType.STRING)) {
      return celda.getStringCellValue();
    } else if (celda.getCellType().equals(CellType.NUMERIC)) {
      String temp = "" + celda.getNumericCellValue();
      if (temp.substring(temp.length() - 2).equals(".0")) temp = temp.substring(0, temp.length() - 2);
      if(temp.length()>=10 && temp.contains("E")) temp = String.valueOf(new BigDecimal(temp).longValue());
      return temp;
    } else if (celda.getCellType().equals(CellType.BOOLEAN)) {
      if (celda.getBooleanCellValue()) {
        return "1";
      } else {
        return "0";
      }
    }
    return null;
  }

  public static String getRawStringValueConGuion(XSSFRow row, int column) {
    XSSFCell celda = row.getCell(column);
    if (celda == null) {
      return null;
    }
    if (celda.getCellType().equals(CellType.STRING)) {
      if (celda.getStringCellValue().equals("-")) return null;
      return celda.getStringCellValue();
    } else if (celda.getCellType().equals(CellType.NUMERIC)) {
      String temp = "" + celda.getNumericCellValue();
      if (temp.substring(temp.length() - 2).equals(".0")) temp = temp.substring(0, temp.length() - 2);
      return temp;
    } else if (celda.getCellType().equals(CellType.BOOLEAN)) {
      if (celda.getBooleanCellValue()) {
        return "1";
      } else {
        return "0";
      }
    }
    return null;
  }

  public static Integer getIntegerValue(XSSFRow row, int column) throws NotFoundException {
    XSSFCell celda = row.getCell(column);
    if (celda == null) {
      return null;
    }
    if (null == celda.getRawValue()) {
      return null;
    }
    if (celda.getCellType().equals(CellType.NUMERIC)) {
      return (int) Math.round(celda.getNumericCellValue());
    } else if (celda.getCellType().equals(CellType.STRING)) {
      try {
        Integer result = Integer.parseInt(celda.getStringCellValue());
        return result;
      } catch (Exception e) {
        throw new NotFoundException(column, row.getRowNum(), "numérico entero", celda.getRawValue());
      }
    } else {
      throw new NotFoundException(column, row.getRowNum(), "numérico entero", celda.getRawValue());
    }
  }

  public static Integer getIntegerValueConGuion(XSSFRow row, int column) throws NotFoundException {
    XSSFCell celda = row.getCell(column);
    if (celda == null) {
      return null;
    }
    if (null == celda.getRawValue()) {
      return null;
    }
    if (celda.getCellType().equals(CellType.NUMERIC)) {
      return (int) Math.round(celda.getNumericCellValue());
    } else if (celda.getCellType().equals(CellType.STRING)) {
      try {
        Integer result = Integer.parseInt(celda.getStringCellValue());
        return result;
      } catch (Exception e) {
        if (celda.getStringCellValue().equals("-")) return null;
        throw new NotFoundException(column, row.getRowNum(), "numérico entero", celda.getRawValue());
      }
    } else {
      throw new NotFoundException(column, row.getRowNum(), "numérico entero", celda.getRawValue());
    }
  }

  public static Date getDateValue(XSSFRow row, int column) throws NotFoundException {
    XSSFCell celda = row.getCell(column);
    if (celda == null) {
      return null;
    }
    try {
      Date dev = celda.getDateCellValue();
      return dev;
    } catch (Exception ex) {
      try {
        String fecha = getRawStringValue(row, column);
        if (null == fecha) {
          return null;
        }
        fecha = fecha.trim();
        return new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
      } catch (Exception e) {
        throw new NotFoundException(column, row.getRowNum(), "fecha DD/MM/YYYY", celda.getRawValue());
      }
    }
  }

  public static Date getDateValueConGuion(XSSFRow row, int column) throws NotFoundException {
    XSSFCell celda = row.getCell(column);
    if (celda == null) {
      return null;
    }
    if (null == celda.getRawValue()) {
      return null;
    }
    try {
      Date dev = celda.getDateCellValue();
      return dev;
    } catch (Exception ex) {
      try {
        String fecha = getRawStringValue(row, column);
        if (null == fecha || fecha.equals("-")) {
          return null;
        }
        fecha = fecha.trim();
        return new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
      } catch (Exception e) {
        throw new NotFoundException(column, row.getRowNum(), "fecha DD/MM/YYYY", celda.getRawValue());
      }
    }
  }

  public static Integer[] getTimeValueConGuion(XSSFRow row, int column) throws NotFoundException {
    Integer[] lista = new Integer[4];

    XSSFCell celda = row.getCell(column);
    if (celda == null) {
      return null;
    }
    if (null == celda.getRawValue() || celda.getRawValue().equals("-")) {
      return null;
    }

    if (celda.getCellType().equals(CellType.NUMERIC)){
      double excelValue = celda.getNumericCellValue();

      int miliseconds = (int) Math.round(excelValue*86400000);
      int hour = miliseconds/( 60/*minutes*/*60/*seconds*/*1000 );
      miliseconds = miliseconds - hour*60/*minutes*/*60/*seconds*/*1000;
      int minutes = miliseconds/( 60/*seconds*/*1000 );
      miliseconds = miliseconds - minutes*60/*seconds*/*1000;
      int seconds = miliseconds/1000;

      lista[0] = hour;
      lista[1] = minutes;
      lista[2] = seconds;
      lista[3] = miliseconds;
    } else {
      throw new NotFoundException(column, row.getRowNum(), "hh:mm:ss", celda.getRawValue());
    }

    return lista;
  }

  public static Boolean getBooleanValue(XSSFRow row, int column) throws NotFoundException {
    XSSFCell celda = row.getCell(column);
    if (celda == null || null == celda.getRawValue()) {
      return null;
    }
    if (celda.getCellType().equals(CellType.NUMERIC) && (1 == (int) celda.getNumericCellValue() || 0 == (int) celda.getNumericCellValue())) {
      int num = (int) celda.getNumericCellValue();
      if (num == 1) {
        return Boolean.TRUE;
      }
      return Boolean.FALSE;
    } else {
      throw new NotFoundException(column, row.getRowNum(), "booleano (0 - 1)", celda.getRawValue());
    }
  }

  public static boolean checkIfRowIsEmpty(XSSFRow row) {
    if (row == null) {
      return true;
    }
    if (row.getLastCellNum() <= 0) {
      return true;
    }
    for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
      Cell cell = row.getCell(cellNum);
      if (cell != null && cell.getCellType() != CellType.BLANK && StringUtils.isNotBlank(cell.toString())) {
        return false;
      }
    }
    return true;
  }
}
