package com.haya.alaska.shared;

import com.haya.alaska.shared.exceptions.UnfulfilledConditionException;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

public class TelefonosUtil {

  /**
   * Comprueba y transforma el teléfono recibido como parámetro para considerarse correcto.
   *
   * @param telefono El número de teléfono a comprobar
   * @return El teléfono formateado correctamente.
   * @throws UnfulfilledConditionException Si el teléfono no tiene la longitud esperada.
   */
  public static String sanitizarTelefono(String telefono) throws UnfulfilledConditionException {
    telefono = telefono.replaceAll("\\s+", "");

    if (!telefono.contains("+34")) {
      telefono = "+34" + telefono;
    }

    if (telefono.length() > 12) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new UnfulfilledConditionException("The indicated phone does not meet the expected format");
      else throw new UnfulfilledConditionException("El teléfono indicado no cumple el formato esperado");
    }

    return telefono;
  }
}
