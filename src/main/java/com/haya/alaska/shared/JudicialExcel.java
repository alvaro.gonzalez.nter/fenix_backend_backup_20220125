package com.haya.alaska.shared;

import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class JudicialExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      cabeceras.add("Connection ID");
      cabeceras.add("Loan ID");
      cabeceras.add("Procedure ID");
      cabeceras.add("Status");
      cabeceras.add("Type");
      cabeceras.add("Number of court orders");
      cabeceras.add("Place");
      cabeceras.add("Court");
      cabeceras.add("Amount demanded");
      cabeceras.add("Applicant");
      cabeceras.add("Expenses incurred");
      cabeceras.add("Judicial authorizations");
      cabeceras.add("Judicial deliveries");
      cabeceras.add("Respondents");
      cabeceras.add("Office");
      cabeceras.add("Counsel");
      cabeceras.add("Legal Phone");
      cabeceras.add("Legal Phone 2");
      cabeceras.add("Email counsel");
      cabeceras.add("Judicial manager Haya");
      cabeceras.add("Email manager Haya");
      cabeceras.add("Procurator");
      cabeceras.add("Procurator Telephone");
      cabeceras.add("Email procurator");
      cabeceras.add("Contrary office");
      cabeceras.add("Opposing Counsel");
      cabeceras.add("Opposing Counsel Telephone");
      cabeceras.add("Opposing Counsel Telephone 2");
      cabeceras.add("Opposing Counsel Email");
      cabeceras.add("Opposing Procurator");
      cabeceras.add("Opposing Procurator Telephone");
      cabeceras.add("Opposing Counsel email");

    } else {

      cabeceras.add("Id Expediente");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Procedimiento");
      cabeceras.add("Estado");
      cabeceras.add("Tipo");
      cabeceras.add("Numero Autos");
      cabeceras.add("Plaza");
      cabeceras.add("Juzgado");
      cabeceras.add("Importe Demanda");
      cabeceras.add("Demandante");
      cabeceras.add("Gastos Incurridos");
      cabeceras.add("Habilitaciones");
      cabeceras.add("Entregas Judiciales");
      cabeceras.add("Demandados");
      cabeceras.add("Despacho");
      cabeceras.add("Letrado");
      cabeceras.add("Telefono Letrado");
      cabeceras.add("Telefono 2 Letrado");
      cabeceras.add("Email Letrado");
      cabeceras.add("Gestor Judicial Haya");
      cabeceras.add("Email Gestor Haya");
      cabeceras.add("Procurador");
      cabeceras.add("Telefono Procurador");
      cabeceras.add("Email Procurador");
      cabeceras.add("Despacho Contrario");
      cabeceras.add("Letrado Contrario");
      cabeceras.add("Telefono Letrado Contrario");
      cabeceras.add("Telefono 2 Letrado Contrario");
      cabeceras.add("Email Letrado Contrario");
      cabeceras.add("Procurador Contrario");
      cabeceras.add("Telefono Procurador Contrario");
      cabeceras.add("Email Procurador Contrario");
    }
  }

  public JudicialExcel(Procedimiento procedimiento, String idContrato, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection ID", idExpediente);
      this.add("Loan ID", idContrato);
      this.add("Procedure ID", procedimiento.getId());
      this.add("Status", procedimiento.getActivo() ? "YES" : "NO");
      this.add("Type", procedimiento.getTipo() != null ? procedimiento.getTipo().getValorIngles() : null);
      this.add("Number of court orders", procedimiento.getNumeroAutos());
      this.add("Place", procedimiento.getPlaza());
      this.add("Court", procedimiento.getJuzgado());
      this.add("Amount demanded", procedimiento.getImporteDemanda());
      this.add("Applicant", procedimiento.getDemandante());
      this.add("Expenses incurred", procedimiento.getGastosIncurridos());
      this.add("Judicial authorizations", procedimiento.getHabilitaciones());
      this.add("Judicial deliveries", procedimiento.getEntregasJudiciales());
      String demandados = "";
      for (Interviniente in : procedimiento.getDemandados()) {
        demandados += in.getNombre() + ", ";
      }
      if (procedimiento.getDemandados().size() > 0) {
        demandados = demandados.substring(0, demandados.length() - 2);
      }
      this.add("Respondents", demandados);
      this.add("Office", procedimiento.getDespacho());
      this.add("Counsel", procedimiento.getLetrado());
      this.add("Legal Phone", procedimiento.getTelefonoLetrado());
      this.add("Legal Phone 2", procedimiento.getTelefono2Letrado());
      this.add("Email Letrado", procedimiento.getEmailLetrado());
      this.add("Judicial manager Haya", procedimiento.getGestorJudicialHaya());
      this.add("Email manager Haya", procedimiento.getEmailGestorJudicialHaya());
      this.add("Procurator", procedimiento.getProcurador());
      this.add("Procurator Telephone", procedimiento.getTelefonoProcurador());
      this.add("Email counsel", procedimiento.getEmailProcurador());
      this.add("Contrary office", procedimiento.getDespachoContrario());
      this.add("Letrado Contrario", procedimiento.getLetradoContrario());
      this.add("Opposing Counsel Telephone", procedimiento.getTelefonoLetradoContrario());
      this.add("Opposing Counsel Telephone 2", procedimiento.getTelefono2LetradoContrario());
      this.add("Opposing Counsel Email", procedimiento.getEmailLetradoContrario());
      this.add("Opposing Procurator", procedimiento.getProcuradorContrario());
      this.add("Opposing Procurator Telephone", procedimiento.getTelefonoProcuradorContrario());
      this.add("Opposing Counsel email", procedimiento.getEmailProcuradorContrario());

    } else {

      this.add("Id Expediente", idExpediente);
      this.add("Id Contrato", idContrato);
      this.add("Id Procedimiento", procedimiento.getId());
      this.add("Estado", procedimiento.getActivo() ? "SI" : "NO");
      this.add("Tipo", procedimiento.getTipo() != null ? procedimiento.getTipo().getValor() : null);
      this.add("Numero Autos", procedimiento.getNumeroAutos());
      this.add("Plaza", procedimiento.getPlaza());
      this.add("Juzgado", procedimiento.getJuzgado());
      this.add("Importe Demanda", procedimiento.getImporteDemanda());
      this.add("Demandante", procedimiento.getDemandante());
      this.add("Gastos Incurridos", procedimiento.getGastosIncurridos());
      this.add("Habilitaciones", procedimiento.getHabilitaciones());
      this.add("Entregas Judiciales", procedimiento.getEntregasJudiciales());
      String demandados = "";
      for (Interviniente in : procedimiento.getDemandados()) {
        demandados += in.getNombre() + ", ";
      }
      if (procedimiento.getDemandados().size() > 0) {
        demandados = demandados.substring(0, demandados.length() - 2);
      }
      this.add("Demandados", demandados);
      this.add("Despacho", procedimiento.getDespacho());
      this.add("Letrado", procedimiento.getLetrado());
      this.add("Telefono Letrado", procedimiento.getTelefonoLetrado());
      this.add("Telefono 2 Letrado", procedimiento.getTelefono2Letrado());
      this.add("Email Letrado", procedimiento.getEmailLetrado());
      this.add("Gestor Judicial Haya", procedimiento.getGestorJudicialHaya());
      this.add("Email Gestor Haya", procedimiento.getEmailGestorJudicialHaya());
      this.add("Procurador", procedimiento.getProcurador());
      this.add("Telefono Procurador", procedimiento.getTelefonoProcurador());
      this.add("Email Procurador", procedimiento.getEmailProcurador());
      this.add("Despacho Contrario", procedimiento.getDespachoContrario());
      this.add("Letrado Contrario", procedimiento.getLetradoContrario());
      this.add("Telefono Letrado Contrario", procedimiento.getTelefonoLetradoContrario());
      this.add("Telefono 2 Letrado Contrario", procedimiento.getTelefono2LetradoContrario());
      this.add("Email Letrado Contrario", procedimiento.getEmailLetradoContrario());
      this.add("Procurador Contrario", procedimiento.getProcuradorContrario());
      this.add("Telefono Procurador Contrario", procedimiento.getTelefonoProcuradorContrario());
      this.add("Email Procurador Contrario", procedimiento.getEmailProcuradorContrario());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
