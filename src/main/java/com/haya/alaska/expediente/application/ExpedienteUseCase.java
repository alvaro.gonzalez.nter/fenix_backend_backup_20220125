package com.haya.alaska.expediente.application;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.AsignacionGestoresDTO;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTO;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteMinutaDTO;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.OutputWithListDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ExpedienteUseCase {

  ListWithCountDTO<ExpedienteDTO> findAllExpedientesDto(Integer page, Integer size) throws NotFoundException;

  ListWithCountDTO<ExpedienteDTO> getAllFilteredExpedientes(
    Usuario usuario,
    String id,
    String estado,
    String cliente,
    String cartera,
    String gestor,
    String titular,
    String estrategia,
    String saldo,
    String orderField,
    String orderDirection,
    Integer size,
    Integer page) throws NotFoundException;

  ListWithCountDTO<ExpedienteDTO> getAllExpedientesWithPropuestas(Usuario usuario) throws NotFoundException;

  ListWithCountDTO<ExpedienteDTO> getAllExpedientesMinutas(Usuario usuario) throws NotFoundException;

  List<Expediente> filterExpedientesInMemory(Usuario usuario);

  ListWithCountDTO<AsignacionGestoresDTO> getAllAsignacionGestoresHistorico(Integer expedienteId) throws Exception;

  OutputWithListDTO<ExpedienteDTO> findExpedienteDTOById(Integer id) throws Exception;

  // NO HACE FALTA
  Integer getExpedientesEnRevision(Usuario usuario);

  TipoEstado findByTipoEstado(Integer estado) throws Exception;


  SubtipoEstado findByTipoSubEstado(Integer subEstado) throws Exception;

  List<CatalogoMinInfoDTO> filtroSubEstado(Integer estado) throws Exception;

  TipoEstado estadoExpediente(Integer idExpediente) throws NotFoundException;

  Expediente findByIdCargaOrNull(String idCarga);

  List<Map> getExpedientesByUltimaValoracion();


  ExpedienteDTO updateModoGestion(Integer expedienteId, Integer estado, Integer subEstado, Usuario usuario) throws Exception;

  List<ExpedienteDTO> listaExpedientesPerfil(Integer perfilId) throws NotFoundException;

  void concatenar();
  String generarNoHeredado(Integer cliente, Integer cartera, String carga);

  ResponseEntity<InputStreamResource> minutaWord(Integer tipo, ExpedienteMinutaDTO expedienteMinutaDTO) throws NotFoundException, RequiredValueException;

  ResponseEntity<byte[]> minutaPdf(Integer tipo, ExpedienteMinutaDTO expedienteMinutaDTO) throws IOException, NotFoundException, RequiredValueException;


  Boolean filtroMesInformes(Expediente expediente, Integer mes);

  Boolean filtroMesInformesPosesionNegociada(ExpedientePosesionNegociada expedientePosesionNegociada, Integer mes);

  ResponseEntity<List<ExpedienteDTO>> expedienteFiltradoCarteraRA(Integer expedienteId, Usuario usuario, Integer size, Integer page);

  ResponseEntity<List<ExpedientePosesionNegociadaDTO>> expedienteFiltradoCarteraPN(Integer expedienteId, Usuario usuario, Integer size, Integer page);

  List<ExpedienteDTO>expedienteFiltradoCarteraSinGestor(Integer carteraID) throws NotFoundException;
}
