package com.haya.alaska.expediente.application;

import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteExcelExportDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface ExpedienteExcelUseCase {
  LinkedHashMap<String, List<Collection<?>>> findExcelExpedientes(Usuario usuario, ExpedienteExcelExportDTO expedienteExcelExportDTO) throws Exception;
  LinkedHashMap<String, List<Collection<?>>> findExcelExpedienteById(Integer id) throws Exception;
  LinkedHashMap<String, List<Collection<?>>> toExcel(List<Expediente> expedientes, ExpedienteExcelExportDTO expedienteExcelExportDTO) throws NotFoundException;
}
