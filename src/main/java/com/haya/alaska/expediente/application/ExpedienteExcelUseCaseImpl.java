package com.haya.alaska.expediente.application;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.BienExcel;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociadaExcel;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.domain.CargaExcel;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.ContratoExcel;
import com.haya.alaska.contrato.domain.ContratoJudicialExcel;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.domain.DatosContactoExcel;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.ExpedienteCarteraExcel;
import com.haya.alaska.expediente.domain.ExpedienteExcel;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteExcelExportDTO;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.domain.IntervinienteExcel;
import com.haya.alaska.localidad.domain.DatosLocalizacionExcel;
import com.haya.alaska.ocupante.domain.OcupanteInformeExcel;
import com.haya.alaska.procedimiento.domain.*;
import com.haya.alaska.procedimiento_posesion_negociada.domain.ProcedimientoPosesionNegociadaInformeExcel;
import com.haya.alaska.shared.DemandadoJudicialExcel;
import com.haya.alaska.shared.JudicialExcel;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.subasta.domain.Subasta;
import com.haya.alaska.subasta.domain.SubastasExcel;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.domain.TasacionExcel;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.valoracion.domain.ValoracionExcel;
import com.haya.alaska.visita.domain.VisitaExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

@Component
public class ExpedienteExcelUseCaseImpl implements ExpedienteExcelUseCase {
  @Autowired private ExpedienteUseCase expedienteUseCase;
  @Autowired private ExpedienteRepository expedienteRepository;
  @Autowired private EventoUtil eventoUtil;
  @Autowired private ExpedienteUtil expedienteUtil;

  @Autowired private CarteraRepository carteraRepository;

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelExpedientes(Usuario usuario, ExpedienteExcelExportDTO expedienteExcelExportDTO)
      throws Exception {
    List<Expediente> expedientes = this.expedienteUseCase.filterExpedientesInMemory(usuario);

    /*Integer limite = 500;
    if (expedientes.size() < 500) limite = expedientes.size();
    List<Expediente> exLimit = expedientes.subList(0, limite);*/

    return toExcel(expedientes, expedienteExcelExportDTO);
  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelExpedienteById(Integer id)
      throws Exception {
    var datos = this.createExpedienteDataForExcel(new ExpedienteExcelExportDTO());
    Expediente expediente =
        expedienteRepository
            .findById(id)
            .orElseThrow(() -> new NotFoundException("Expediente", id));
    this.addExpedienteDataForExcel(expediente, datos, new ExpedienteExcelExportDTO());
    return datos;
  }

  @Override
  public LinkedHashMap<String, List<Collection<?>>> toExcel(List<Expediente> expedientes, ExpedienteExcelExportDTO expedienteExcelExportDTO)
      throws NotFoundException {
    var datos = this.createExpedienteDataForExcel(expedienteExcelExportDTO);
    for (Expediente expediente : expedientes) {
      this.addExpedienteDataForExcel(expediente, datos, expedienteExcelExportDTO);
    }
    return datos;
  }

  private LinkedHashMap<String, List<Collection<?>>> createExpedienteDataForExcel(ExpedienteExcelExportDTO expedienteExcelExportDTO) {
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expedientes = new ArrayList<>();
      expedientes.add(ExpedienteExcel.cabeceras);
    List<Collection<?>> contratos = new ArrayList<>();
    contratos.add(ContratoExcel.cabeceras);
    List<Collection<?>> garantias = new ArrayList<>();
    garantias.add(BienExcel.cabeceras);
    List<Collection<?>> solvencias = new ArrayList<>();
    solvencias.add(BienExcel.cabeceras);
    List<Collection<?>> tasaciones = new ArrayList<>();
    tasaciones.add(TasacionExcel.cabeceras);
    List<Collection<?>> valoraciones = new ArrayList<>();
    valoraciones.add(ValoracionExcel.cabeceras);
    List<Collection<?>> cargas = new ArrayList<>();
    cargas.add(CargaExcel.cabeceras);
    List<Collection<?>> intervinientes = new ArrayList<>();
    intervinientes.add(IntervinienteExcel.cabeceras);
    List<Collection<?>> datosLocalizacion = new ArrayList<>();
    datosLocalizacion.add(DatosLocalizacionExcel.cabeceras);
    List<Collection<?>> datosDeContacto = new ArrayList<>();
    datosDeContacto.add(DatosContactoExcel.cabeceras);
    List<Collection<?>> judicial = new ArrayList<>();
    judicial.add(JudicialExcel.cabeceras);
    List<Collection<?>> contratosJudiciales = new ArrayList<>();
    contratosJudiciales.add(ContratoJudicialExcel.cabeceras);
    List<Collection<?>> demandadosJudiciales = new ArrayList<>();
    demandadosJudiciales.add(DemandadoJudicialExcel.cabeceras);
    List<Collection<?>> procedimientosETJN = new ArrayList<>();
    procedimientosETJN.add(ProcedimientoETJNExcel.cabeceras);
    List<Collection<?>> procedimientosETJ = new ArrayList<>();
    procedimientosETJ.add(ProcedimientoETJExcel.cabeceras);
    List<Collection<?>> procedimientosHipotecario = new ArrayList<>();
    procedimientosHipotecario.add(ProcedimientoHipotecarioExcel.cabeceras);
    List<Collection<?>> procedimientosEjecucionNotarial = new ArrayList<>();
    procedimientosEjecucionNotarial.add(ProcedimientoEjecucionNotarialExcel.cabeceras);
    List<Collection<?>> procedimientosMonitorio = new ArrayList<>();
    procedimientosMonitorio.add(ProcedimientoMonitorioExcel.cabeceras);
    List<Collection<?>> procedimientosVerbal = new ArrayList<>();
    procedimientosVerbal.add(ProcedimientoVerbalExcel.cabeceras);
    List<Collection<?>> procedimientosOrdinario = new ArrayList<>();
    procedimientosOrdinario.add(ProcedimientoOrdinarioExcel.cabeceras);
    List<Collection<?>> procedimientosConcursal = new ArrayList<>();
    procedimientosConcursal.add(ProcedimientoConcursalExcel.cabeceras);
    List<Collection<?>> subastas = new ArrayList<>();
    subastas.add(SubastasExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      if (expedienteExcelExportDTO.getExpedientes() != null && expedienteExcelExportDTO.getExpedientes() )
      datos.put("Connection", expedientes);
      if (expedienteExcelExportDTO.getContratos() != null && expedienteExcelExportDTO.getContratos() )
      datos.put("Loans", contratos);
      if (expedienteExcelExportDTO.getGarantias() != null && expedienteExcelExportDTO.getGarantias() )
      datos.put("Warranties", garantias);
      if (expedienteExcelExportDTO.getSolvencias() != null && expedienteExcelExportDTO.getSolvencias() )
      datos.put("Solvencies", solvencias);
      if (expedienteExcelExportDTO.getTasaciones() != null && expedienteExcelExportDTO.getTasaciones() )
      datos.put("Appraisals", tasaciones);
      if (expedienteExcelExportDTO.getValoraciones() != null && expedienteExcelExportDTO.getValoraciones() )
      datos.put("Ratings", valoraciones);
      if (expedienteExcelExportDTO.getCargas() != null && expedienteExcelExportDTO.getCargas() )
      datos.put("Charges", cargas);
      if (expedienteExcelExportDTO.getIntervinientes() != null && expedienteExcelExportDTO.getIntervinientes() )
      datos.put("Borrowers", intervinientes);
      if (expedienteExcelExportDTO.getDatosLocalizacion() != null && expedienteExcelExportDTO.getDatosLocalizacion() )
      datos.put("Location Data", datosLocalizacion);
      if (expedienteExcelExportDTO.getDatosDeContacto() != null && expedienteExcelExportDTO.getDatosDeContacto() )
      datos.put("Contact Data", datosDeContacto);
      if (expedienteExcelExportDTO.getJudicial() != null && expedienteExcelExportDTO.getJudicial() )
      datos.put("Judicial", judicial);
      if (expedienteExcelExportDTO.getContratosJudiciales() != null && expedienteExcelExportDTO.getContratosJudiciales() )
      datos.put("Loans Judicial", contratosJudiciales);
      if (expedienteExcelExportDTO.getDemandadosJudiciales() != null && expedienteExcelExportDTO.getDemandadosJudiciales() )
      datos.put("Defendants Judicial", demandadosJudiciales);
      if (expedienteExcelExportDTO.getProcedimientosETJN() != null && expedienteExcelExportDTO.getProcedimientosETJN() )
      datos.put("Procedures ETJN", procedimientosETJN);
      if (expedienteExcelExportDTO.getProcedimientosETJ() != null && expedienteExcelExportDTO.getProcedimientosETJ() )
      datos.put("Procedures ETJ", procedimientosETJ);
      if (expedienteExcelExportDTO.getProcedimientosHipotecario() != null && expedienteExcelExportDTO.getProcedimientosHipotecario() )
      datos.put("Mortgage Procedures", procedimientosHipotecario);
      if (expedienteExcelExportDTO.getProcedimientosEjecucionNotarial() != null && expedienteExcelExportDTO.getProcedimientosEjecucionNotarial() )
      datos.put("Notarial Procedures", procedimientosEjecucionNotarial);
      if (expedienteExcelExportDTO.getProcedimientosMonitorio() != null && expedienteExcelExportDTO.getProcedimientosMonitorio() )
      datos.put("Monitoring Procedures", procedimientosMonitorio);
      if (expedienteExcelExportDTO.getProcedimientosVerbal() != null && expedienteExcelExportDTO.getProcedimientosVerbal() )
      datos.put("Verbal Procedures", procedimientosVerbal);
      if (expedienteExcelExportDTO.getProcedimientosOrdinario() != null && expedienteExcelExportDTO.getProcedimientosOrdinario() )
      datos.put("Ordinary Procedures", procedimientosOrdinario);
      if (expedienteExcelExportDTO.getProcedimientosConcursal() != null && expedienteExcelExportDTO.getProcedimientosConcursal() )
      datos.put("Bankruptcy Proceedings", procedimientosConcursal);
      if (expedienteExcelExportDTO.getSubastas() != null && expedienteExcelExportDTO.getSubastas() )
      datos.put("Auctions", subastas);

    } else {

      if (expedienteExcelExportDTO.getExpedientes() != null && expedienteExcelExportDTO.getExpedientes() )
      datos.put("Expediente", expedientes);
      if (expedienteExcelExportDTO.getContratos() != null && expedienteExcelExportDTO.getContratos() )
      datos.put("Contratos", contratos);
      if (expedienteExcelExportDTO.getGarantias() != null && expedienteExcelExportDTO.getGarantias() )
      datos.put("Garantías", garantias);
      if (expedienteExcelExportDTO.getSolvencias() != null && expedienteExcelExportDTO.getSolvencias() )
      datos.put("Solvencias", solvencias);
      if (expedienteExcelExportDTO.getTasaciones() != null && expedienteExcelExportDTO.getTasaciones() )
      datos.put("Tasaciones", tasaciones);
      if (expedienteExcelExportDTO.getValoraciones() != null && expedienteExcelExportDTO.getValoraciones() )
      datos.put("Valoraciones", valoraciones);
      if (expedienteExcelExportDTO.getCargas() != null && expedienteExcelExportDTO.getCargas() )
      datos.put("Cargas", cargas);
      if (expedienteExcelExportDTO.getIntervinientes() != null && expedienteExcelExportDTO.getIntervinientes() )
      datos.put("Intervinientes", intervinientes);
      if (expedienteExcelExportDTO.getDatosLocalizacion() != null && expedienteExcelExportDTO.getDatosLocalizacion() )
      datos.put("Datos Localizacion", datosLocalizacion);
      if (expedienteExcelExportDTO.getDatosDeContacto() != null && expedienteExcelExportDTO.getDatosDeContacto() )
      datos.put("Datos de Contacto", datosDeContacto);
      if (expedienteExcelExportDTO.getJudicial() != null && expedienteExcelExportDTO.getJudicial() )
      datos.put("Judicial", judicial);
      if (expedienteExcelExportDTO.getContratosJudiciales() != null && expedienteExcelExportDTO.getContratosJudiciales() )
      datos.put("Contratos Judicial", contratosJudiciales);
      if (expedienteExcelExportDTO.getDemandadosJudiciales() != null && expedienteExcelExportDTO.getDemandadosJudiciales() )
      datos.put("Demandados Judicial", demandadosJudiciales);
      if (expedienteExcelExportDTO.getProcedimientosETJN() != null && expedienteExcelExportDTO.getProcedimientosETJN() )
      datos.put("Procedimientos ETJN", procedimientosETJN);
      if (expedienteExcelExportDTO.getProcedimientosETJ() != null && expedienteExcelExportDTO.getProcedimientosETJ() )
      datos.put("Procedimientos ETJ", procedimientosETJ);
      if (expedienteExcelExportDTO.getProcedimientosHipotecario() != null && expedienteExcelExportDTO.getProcedimientosHipotecario() )
      datos.put("Procedimientos Hipotecario", procedimientosHipotecario);
      if (expedienteExcelExportDTO.getProcedimientosEjecucionNotarial() != null && expedienteExcelExportDTO.getProcedimientosEjecucionNotarial() )
      datos.put("Procedimientos Notarial", procedimientosEjecucionNotarial);
      if (expedienteExcelExportDTO.getProcedimientosMonitorio() != null && expedienteExcelExportDTO.getProcedimientosMonitorio() )
      datos.put("Procedimientos Monitorio", procedimientosMonitorio);
      if (expedienteExcelExportDTO.getProcedimientosVerbal() != null && expedienteExcelExportDTO.getProcedimientosVerbal() )
      datos.put("Procedimientos Verbal", procedimientosVerbal);
      if (expedienteExcelExportDTO.getProcedimientosOrdinario() != null && expedienteExcelExportDTO.getProcedimientosOrdinario() )
      datos.put("Procedimientos Ordinario", procedimientosOrdinario);
      if (expedienteExcelExportDTO.getProcedimientosConcursal() != null && expedienteExcelExportDTO.getProcedimientosConcursal() )
      datos.put("Procedimientos Concursal", procedimientosConcursal);
      if (expedienteExcelExportDTO.getSubastas() != null && expedienteExcelExportDTO.getSubastas() )
      datos.put("Subastas", subastas);
    }

    return datos;
  }

  private void addExpedienteDataForExcel(
      Expediente expediente, LinkedHashMap<String, List<Collection<?>>> datos, ExpedienteExcelExportDTO expedienteExcelExportDTO)
      throws NotFoundException {
    List<Collection<?>> expedientes;
    List<Collection<?>> contratos;
    List<Collection<?>> garantias;
    List<Collection<?>> solvencias;
    List<Collection<?>> tasaciones;
    List<Collection<?>> valoraciones;
    List<Collection<?>> cargas;
    List<Collection<?>> intervinientes;
    List<Collection<?>> datosLocalizacion;
    List<Collection<?>> datosDeContacto;
    List<Collection<?>> judicial;
    List<Collection<?>> contratosJudiciales;
    List<Collection<?>> demandadosJudiciales;
    List<Collection<?>> procedimientosETJN;
    List<Collection<?>> procedimientosETJ;
    List<Collection<?>> procedimientosHipotecario;
    List<Collection<?>> procedimientosEjecucionNotarial;
    List<Collection<?>> procedimientosMonitorio;
    List<Collection<?>> procedimientosVerbal;
    List<Collection<?>> procedimientosOrdinario;
    List<Collection<?>> procedimientosConcursal;
    List<Collection<?>> subastas;

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      expedientes = datos.get("Connection");
      contratos = datos.get("Loans");
      garantias = datos.get("Warranties");
      solvencias = datos.get("Solvencies");
      tasaciones = datos.get("Appraisals");
      valoraciones = datos.get("Ratings");
      cargas = datos.get("Charges");
      intervinientes = datos.get("Borrowers");
      datosLocalizacion = datos.get("Location Data");
      datosDeContacto = datos.get("Contact Data");
      judicial = datos.get("Judicial");
      contratosJudiciales = datos.get("Loans Judicial");
      demandadosJudiciales = datos.get("Defendants Judicial");
      procedimientosETJN = datos.get("Procedures ETJN");
      procedimientosETJ = datos.get("Procedures ETJ");
      procedimientosHipotecario = datos.get("Mortgage Procedures");
      procedimientosEjecucionNotarial = datos.get("Notarial Procedures");
      procedimientosMonitorio = datos.get("Monitoring Procedures");
      procedimientosVerbal = datos.get("Verbal Procedures");
      procedimientosOrdinario = datos.get("Ordinary Procedures");
      procedimientosConcursal = datos.get("Bankruptcy Proceedings");
      subastas = datos.get("Auctions");

    } else {

      expedientes = datos.get("Expediente");
      contratos = datos.get("Contratos");
      garantias = datos.get("Garantías");
      solvencias = datos.get("Solvencias");
      tasaciones = datos.get("Tasaciones");
      valoraciones = datos.get("Valoraciones");
      cargas = datos.get("Cargas");
      intervinientes = datos.get("Intervinientes");
      datosLocalizacion = datos.get("Datos Localizacion");
      datosDeContacto = datos.get("Datos de Contacto");
      judicial = datos.get("Judicial");
      contratosJudiciales = datos.get("Contratos Judicial");
      demandadosJudiciales = datos.get("Demandados Judicial");
      procedimientosETJN = datos.get("Procedimientos ETJN");
      procedimientosETJ = datos.get("Procedimientos ETJ");
      procedimientosHipotecario = datos.get("Procedimientos Hipotecario");
      procedimientosEjecucionNotarial = datos.get("Procedimientos Notarial");
      procedimientosMonitorio = datos.get("Procedimientos Monitorio");
      procedimientosVerbal = datos.get("Procedimientos Verbal");
      procedimientosOrdinario = datos.get("Procedimientos Ordinario");
      procedimientosConcursal = datos.get("Procedimientos Concursal");
      subastas = datos.get("Subastas");
    }

    Evento accion = getLastEvento(expediente.getId(), 1);
    Evento alerta = getLastEvento(expediente.getId(), 2);
    Evento actividad = getLastEvento(expediente.getId(), 3);

    if (expedienteExcelExportDTO.getExpedientes() != null && expedienteExcelExportDTO.getExpedientes())
      expedientes.add(new ExpedienteExcel(expediente, accion, alerta, actividad).getValuesList());


    var idExpediente = expediente.getIdConcatenado();
    if (getInContrato(expedienteExcelExportDTO))
      for (Contrato contrato : expediente.getContratos()) {
        if (expedienteExcelExportDTO.getContratos() != null && expedienteExcelExportDTO.getContratos())
          contratos.add(new ContratoExcel(contrato).getValuesList());
        var idContrato = contrato.getIdCarga();
        var idC = contrato.getId();
        if (this.getInContratoBien(expedienteExcelExportDTO))
          for (ContratoBien cb : contrato.getBienes()) {
            Bien bien = cb.getBien();
            var idBien = bien.getId();
            if (expedienteExcelExportDTO.getTasaciones() != null && expedienteExcelExportDTO.getTasaciones())
              for (Tasacion tasacion : bien.getTasaciones()) {
                tasaciones.add(
                    new TasacionExcel(tasacion, idContrato, idBien, idExpediente).getValuesList());
              }
            if (expedienteExcelExportDTO.getValoraciones() != null && expedienteExcelExportDTO.getValoraciones())
              for (Valoracion valoracion : bien.getValoraciones()) {
                valoraciones.add(
                    new ValoracionExcel(valoracion, idContrato, idBien, idExpediente)
                        .getValuesList());
              }
            if (expedienteExcelExportDTO.getCargas() != null && expedienteExcelExportDTO.getCargas())
              for (Carga carga : bien.getCargas()) {
                cargas.add(new CargaExcel(carga, idContrato, idBien, idExpediente).getValuesList());
              }
            if (cb.getResponsabilidadHipotecaria() != null) {
              if (expedienteExcelExportDTO.getGarantias() != null && expedienteExcelExportDTO.getGarantias())
                garantias.add(new BienExcel(bien, idC).getValuesList());
            } else {
              if (expedienteExcelExportDTO.getSolvencias() != null && expedienteExcelExportDTO.getSolvencias())
                solvencias.add(new BienExcel(bien, idC).getValuesList());
            }
          }

        if (this.getInContratoInterviniente(expedienteExcelExportDTO))
          for (ContratoInterviniente contratoInterviniente : contrato.getIntervinientes()) {
            if (expedienteExcelExportDTO.getIntervinientes() != null && expedienteExcelExportDTO.getIntervinientes())
              intervinientes.add(
                  new IntervinienteExcel(
                          contratoInterviniente.getInterviniente(),
                          contrato.getIdCarga(),
                          contratoInterviniente.getOrdenIntervencion(),
                          contratoInterviniente.getTipoIntervencion(),
                          idExpediente)
                      .getValuesList());
            if (expedienteExcelExportDTO.getDatosLocalizacion() != null && expedienteExcelExportDTO.getDatosLocalizacion())
              for (Direccion direccion :
                  contratoInterviniente.getInterviniente().getDirecciones()) {
                datosLocalizacion.add(
                    new DatosLocalizacionExcel(direccion, idContrato, idExpediente)
                        .getValuesList());
              }
            if (expedienteExcelExportDTO.getDatosDeContacto() != null && expedienteExcelExportDTO.getDatosDeContacto())
              for (DatoContacto datoContacto :
                  contratoInterviniente.getInterviniente().getDatosContacto()) {
                datosDeContacto.add(
                    new DatosContactoExcel(datoContacto, idContrato, idExpediente).getValuesList());
              }
          }

        if (this.getInProcedimientos(expedienteExcelExportDTO))
          for (Procedimiento procedimiento : contrato.getProcedimientos()) {
            if (this.getProcedimientos(expedienteExcelExportDTO)) {
              if (procedimiento instanceof ProcedimientoHipotecario) {
                if (expedienteExcelExportDTO.getProcedimientosHipotecario() != null && expedienteExcelExportDTO.getProcedimientosHipotecario())
                  procedimientosHipotecario.add(
                      new ProcedimientoHipotecarioExcel(
                              (ProcedimientoHipotecario) procedimiento, idContrato, idExpediente)
                          .getValuesList());
              } else if (procedimiento instanceof ProcedimientoConcursal) {
                if (expedienteExcelExportDTO.getProcedimientosConcursal() != null && expedienteExcelExportDTO.getProcedimientosConcursal())
                  procedimientosConcursal.add(
                      new ProcedimientoConcursalExcel(
                              (ProcedimientoConcursal) procedimiento, idContrato, idExpediente)
                          .getValuesList());
              } else if (procedimiento instanceof ProcedimientoEjecucionNotarial) {
                if (expedienteExcelExportDTO.getProcedimientosEjecucionNotarial() != null && expedienteExcelExportDTO.getProcedimientosEjecucionNotarial())
                  procedimientosEjecucionNotarial.add(
                      new ProcedimientoEjecucionNotarialExcel(
                              (ProcedimientoEjecucionNotarial) procedimiento,
                              idContrato,
                              idExpediente)
                          .getValuesList());
              } else if (procedimiento instanceof ProcedimientoEtj) {
                if (expedienteExcelExportDTO.getProcedimientosETJ() != null && expedienteExcelExportDTO.getProcedimientosETJ())
                  procedimientosETJ.add(
                      new ProcedimientoETJExcel(
                              (ProcedimientoEtj) procedimiento, idContrato, idExpediente)
                          .getValuesList());
              } else if (procedimiento instanceof ProcedimientoEtnj) {
                if (expedienteExcelExportDTO.getProcedimientosETJN() != null && expedienteExcelExportDTO.getProcedimientosETJN())
                  procedimientosETJN.add(
                      new ProcedimientoETJNExcel(
                              (ProcedimientoEtnj) procedimiento, idContrato, idExpediente)
                          .getValuesList());
              } else if (procedimiento instanceof ProcedimientoMonitorio) {
                if (expedienteExcelExportDTO.getProcedimientosMonitorio() != null && expedienteExcelExportDTO.getProcedimientosMonitorio())
                  procedimientosMonitorio.add(
                      new ProcedimientoMonitorioExcel(
                              (ProcedimientoMonitorio) procedimiento, idContrato, idExpediente)
                          .getValuesList());
              } else if (procedimiento instanceof ProcedimientoOrdinario) {
                if (expedienteExcelExportDTO.getProcedimientosOrdinario() != null && expedienteExcelExportDTO.getProcedimientosOrdinario())
                  procedimientosOrdinario.add(
                      new ProcedimientoOrdinarioExcel(
                              (ProcedimientoOrdinario) procedimiento, idContrato, idExpediente)
                          .getValuesList());
              } else if (procedimiento instanceof ProcedimientoVerbal) {
                if (expedienteExcelExportDTO.getProcedimientosVerbal() != null && expedienteExcelExportDTO.getProcedimientosVerbal())
                  procedimientosVerbal.add(
                      new ProcedimientoVerbalExcel(
                              (ProcedimientoVerbal) procedimiento, idContrato, idExpediente)
                          .getValuesList());
              }
            }

            if (expedienteExcelExportDTO.getJudicial() != null && expedienteExcelExportDTO.getJudicial())
              judicial.add(
                  new JudicialExcel(procedimiento, contrato.getIdCarga(), idExpediente)
                      .getValuesList());
            if (expedienteExcelExportDTO.getContratosJudiciales() != null && expedienteExcelExportDTO.getContratosJudiciales())
              for (Contrato contratoJudicial : procedimiento.getContratos()) {
                contratosJudiciales.add(
                    new ContratoJudicialExcel(contratoJudicial, procedimiento.getId(), idExpediente)
                        .getValuesList());
              }
            if (expedienteExcelExportDTO.getDemandadosJudiciales() != null && expedienteExcelExportDTO.getDemandadosJudiciales())
              for (Interviniente demandadoJudicial : procedimiento.getDemandados()) {
                demandadosJudiciales.add(
                    new DemandadoJudicialExcel(
                            demandadoJudicial, procedimiento.getId(), idExpediente)
                        .getValuesList());
              }
            if (expedienteExcelExportDTO.getSubastas() != null && expedienteExcelExportDTO.getSubastas())
              for (Subasta subasta : procedimiento.getSubastas()) {
                subastas.add(
                    new SubastasExcel(subasta, procedimiento.getId(), idExpediente)
                        .getValuesList());
              }
          }
      }
  }

  private Boolean getInContratoInterviniente(ExpedienteExcelExportDTO exp){
    if (exp.getIntervinientes() != null && exp.getIntervinientes()) return true;
    if (exp.getDatosLocalizacion() != null && exp.getDatosLocalizacion()) return true;
    if (exp.getDatosDeContacto() != null && exp.getDatosDeContacto()) return true;

    return false;
  }

  private Boolean getInContrato(ExpedienteExcelExportDTO exp){
    if (exp.getContratos() != null && exp.getContratos()) return true;
    if (this.getInContratoBien(exp)) return true;
    if (this.getInContratoInterviniente(exp)) return true;
    if (this.getInProcedimientos(exp)) return true;

    return false;
  }

  private Boolean getInContratoBien(ExpedienteExcelExportDTO exp){
    if (exp.getTasaciones() != null && exp.getTasaciones()) return true;
    if (exp.getValoraciones() != null && exp.getValoraciones()) return true;
    if (exp.getCargas() != null && exp.getCargas()) return true;
    if ((exp.getGarantias() != null && exp.getGarantias()) || (exp.getSolvencias() != null && exp.getSolvencias())) return true;

    return false;
  }

  private Boolean getProcedimientos(ExpedienteExcelExportDTO exp){
    if (exp.getProcedimientosHipotecario() != null && exp.getProcedimientosHipotecario()) return true;
    if (exp.getProcedimientosConcursal() != null && exp.getProcedimientosConcursal()) return true;
    if (exp.getProcedimientosEjecucionNotarial() != null && exp.getProcedimientosEjecucionNotarial()) return true;
    if (exp.getProcedimientosETJ() != null && exp.getProcedimientosETJ()) return true;
    if (exp.getProcedimientosETJN() != null && exp.getProcedimientosETJN()) return true;
    if (exp.getProcedimientosMonitorio() != null && exp.getProcedimientosMonitorio()) return true;
    if (exp.getProcedimientosOrdinario() != null && exp.getProcedimientosOrdinario()) return true;
    if (exp.getProcedimientosVerbal() != null && exp.getProcedimientosVerbal()) return true;

    return false;
  }

  private Boolean getInProcedimientos(ExpedienteExcelExportDTO exp){
    if (this.getProcedimientos(exp)) return true;
    if (exp.getJudicial() != null && exp.getJudicial()) return true;
    if (exp.getContratosJudiciales() != null && exp.getContratosJudiciales()) return true;
    if (exp.getDemandadosJudiciales() != null && exp.getDemandadosJudiciales()) return true;
    if (exp.getSubastas() != null && exp.getSubastas()) return true;

    return false;
  }

  public Evento getLastEvento(Integer idExpediente, Integer clase) throws NotFoundException {
    List<Integer> clases = new ArrayList<>();
    clases.add(clase);
    List<Evento> eventos =
        eventoUtil
            .getFilteredEventos(
                null,
                null,
                null,
                1,
                idExpediente,
                null,
                null,
                null,
                null,
                null,
                null,
                clases,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                "fechaCreacion",
                "desc",
                0, 3);

    Evento result = null;
    if (!eventos.isEmpty()) result = eventos.get(0);
    return result;
  }

  private LinkedHashMap<String, List<Collection<?>>> createPosesionesDataForExcel() {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expediente = new ArrayList<>();
    expediente.add(ExpedienteCarteraExcel.cabeceras);

    List<Collection<?>> activos = new ArrayList<>();
    activos.add(BienPosesionNegociadaExcel.cabeceras);

    List<Collection<?>> ocupantes = new ArrayList<>();
    ocupantes.add(OcupanteInformeExcel.cabeceras);

    List<Collection<?>> visitas = new ArrayList<>();
    visitas.add(VisitaExcel.cabeceras);

    List<Collection<?>> judicial = new ArrayList<>();
    judicial.add(ProcedimientoPosesionNegociadaInformeExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("Connection", expediente);
      datos.put("Actives", activos);
      datos.put("Occupants", ocupantes);
      datos.put("Visits", visitas);
      datos.put("Judicial", judicial);


    } else  {
      datos.put("Expediente", expediente);
      datos.put("Activos", activos);
      datos.put("Ocupantes", ocupantes);
      datos.put("Visitas", visitas);
      datos.put("Judicial", judicial);
    }

    return datos;
  }

  public Evento getLastEvento2(Integer idExpediente, Integer clase) {
    try {
      List<Integer> clases = new ArrayList<>();
      clases.add(clase);
      List<Evento> eventos =
          eventoUtil
              .getFilteredEventos(
                  null,
                  null,
                  null,
                  1,
                  idExpediente,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  clases,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  "fechaCreacion",
                  "desc",
                  0, 100);

      Evento result = null;
      if (!eventos.isEmpty()) result = eventos.get(0);
      return result;
    } catch (Exception e) {
      return null;
    }
  }
}
