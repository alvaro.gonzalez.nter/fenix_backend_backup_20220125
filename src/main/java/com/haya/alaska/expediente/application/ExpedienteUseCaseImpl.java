package com.haya.alaska.expediente.application;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente_;
import com.haya.alaska.asignacion_expediente.infrastructure.repository.AsignacionExpedienteRepository;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.busqueda.application.BusquedaUseCaseImpl;
import com.haya.alaska.carga.infrastructure.repository.CargaRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.domain.Cliente_;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera_;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente_;
import com.haya.alaska.estrategia.domain.Estrategia_;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada_;
import com.haya.alaska.estrategia_asignada.infrastructure.repository.EstrategiaAsignadaRepository;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.expediente.infrastructure.controller.dto.AsignacionGestoresDTO;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTO;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteMinutaDTO;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.mapper.ExpedientePosesionNegociadaMapper;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.importe_propuesta.infrastructure.repository.ImportePropuestaRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.domain.Interviniente_;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.origen_estrategia.infrastructure.repository.OrigenEstrategiaRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.domain.Perfil_;
import com.haya.alaska.periodo_estrategia.infrastructure.repository.PeriodoEstrategiaRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.domain.Saldo_;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.OutputWithListDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.situacion.infrastructure.repository.SituacionRepository;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.subtipo_estado.infrastructure.repository.SubtipoEstadoRepository;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.tasacion.infrastructure.repository.TasacionRepository;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.tipo_estado.domain.TipoEstado_;
import com.haya.alaska.tipo_estado.infrastructure.repository.TipoEstadoRepository;
import com.haya.alaska.tipo_estrategia.infrastructure.repository.TipoEstrategiaRepository;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.tipo_evento.infrastructure.repository.TipoEventoRepository;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion_;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.haya.alaska.valoracion.infrastructure.repository.ValoracionRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
//anotacion para generar un fichero de log
@Slf4j
public class ExpedienteUseCaseImpl implements ExpedienteUseCase {

  @Autowired
  ExpedienteRepository expedienteRepository;
  @Autowired
  ContratoRepository contratoRepository;
  @Autowired
  ClienteRepository clienteRepository;
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  AsignacionExpedienteRepository asignacionExpedienteRepository;
  @Autowired
  ExpedienteMapper expedienteMapper;
  @Autowired
  ExpedientePosesionNegociadaMapper expedientePosesionNegociadaMapper;
  //controla la persistencia de la clase
  @PersistenceContext
  private EntityManager entityManager;
  @Autowired
  TipoEstadoRepository tipoEstadoRepository;
  @Autowired
  SubtipoEstadoRepository subtipoEstadoRepository;
  @Autowired
  EstrategiaRepository estrategiaRepository;
  @Autowired
  EstrategiaAsignadaRepository estrategiaAsignadaRepository;
  @Autowired
  TipoEstrategiaRepository tipoEstrategiaRepository;
  @Autowired
  OrigenEstrategiaRepository origenEstrategiaRepository;
  @Autowired
  PeriodoEstrategiaRepository periodoEstrategiaRepository;
  @Autowired
  SituacionRepository situacionRepository;
  @Autowired
  UsuarioRepository usuarioRepository;
  @Autowired
  BusquedaUseCaseImpl busquedaUseCase;
  @Autowired
  ExpedienteExcelUseCaseImpl expedienteExcelUseCaseImpl;
  @Autowired
  PropuestaRepository propuestaRepository;
  @Autowired
  ExpedientePosesionNegociadaRepository epnRepository;
  @Autowired
  BienRepository bienRepository;
  @Autowired
  IntervinienteRepository intervinienteRepository;
  @Autowired
  ImportePropuestaRepository importePropuestaRepository;
  @Autowired
  TasacionRepository tasacionRepository;
  @Autowired
  ValoracionRepository valoracionRepository;
  @Autowired
  CargaRepository cargaRepository;
  @Autowired
  ExpedienteUtil expedienteUtil;
  @Autowired
  BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  private EventoUseCase eventoUseCase;
  @Autowired
  SubtipoEventoRepository subtipoEventoRepository;
  @Autowired
  TipoEventoRepository tipoEventoRepository;

  //Buscamos expediente a traves de un id
  //Creamos un expediente opcional y comprobamos si el id que nos mandan esta en la bbdd
  //Si el expediente opcional esta presente lo devolvemos, si no lanzamos excepcion indicando que no se encuentra el expediente
  private Expediente findById(Integer expedienteId) throws Exception {
    Optional<Expediente> opExpediente = expedienteRepository.findById(expedienteId);
    if (opExpediente.isPresent()) {
      return opExpediente.get();
    }
    throw new Exception("No se encontró el expediente");
  }

  //Buscamos todos los expedientes
  //Pasamos al metodo la pagina y el tamaño de la pagina
  //Creamos un objeto de paginacion con los datos que nos dan
  //Creamos un objeto pagina y buscamos mediante el repositorio los expedientes que nos solicitan
  //Una vez tenemos estos expedientes creamos un listado de expedientes dto y parseamos los expedientes a la nueva lista
  //Devolvemos la lista de los expedientes pedidos y el total de expedientes que tenemos en bbdd
  @Override
  public ListWithCountDTO<ExpedienteDTO> findAllExpedientesDto(Integer page, Integer size) throws NotFoundException {
    Pageable pageable = PageRequest.of(page, size);
    Page<Expediente> paged = expedienteRepository.findAll(pageable);
    List<Expediente> expedientes = paged.getContent();

    List<ExpedienteDTO> expedienteDTOS = new ArrayList<>();
    for (Expediente e : expedientes) {
      expedienteDTOS.add(expedienteMapper.parse(e));
    }
    return new ListWithCountDTO<>(expedienteDTOS, (int) expedienteRepository.count());
  }

  //Buscamos todos los expedientes con propuestas
  //Pasamos el usuario actual conectado
  @Override
  public ListWithCountDTO<ExpedienteDTO> getAllExpedientesWithPropuestas(Usuario usuario) throws NotFoundException {
    //Creamos listado de expedientes sin paginar obteniendo los expedientes en memoria filtrados por el usuario que tenemos
    List<Expediente> expedientesSinPaginar = filterExpedientesInMemory(usuario, null, null, null, null, null, null, null, null, null, null);
    //Creamos listado de expedientesPre y recogemos los expedientes que tienen propuestas
    List<Expediente> expedientesPre = expedientesSinPaginar.stream().filter(exp -> exp.getPropuestas().size() > 0).collect(Collectors.toList());
    //Creamos listado de expedientes vacio
    List<Expediente> expedientes = new ArrayList<>();
    //Para cada expediente con propuesta, sacamos un listado de propuestas
    for (Expediente expediente : expedientesPre) {
      List<Propuesta> propuestas = expediente.getPropuestas().stream().filter(pro -> pro.getBienes().size() > 0).collect(Collectors.toList());
      //Si la propuesta no esta vacia añadimos el expediente al listado de expedientes que teniamos vacio
      if (!propuestas.isEmpty()) expedientes.add(expediente);
    }
    //Cramos listado de expedientesDTO
    List<ExpedienteDTO> expedientesDTOS = new ArrayList<>();
    //Comprobamos cada uno de los expedientes que tienen propuesta a traves de la lista que se ha llenado en el bucle anterior
    for (Expediente expediente : expedientes) {
      //Creamos un evento accion, y recogemos el ultimo evento del expediente que esta iterando en ese momento y asignamos la clase 1 al evento accion
      Evento accion = expedienteExcelUseCaseImpl.getLastEvento(expediente.getId(), 1);
      //Creamos un evento alerta, y recogemos el ultimo evento del expediente que esta iterando en ese momento y asignamos la clase 2 al evento alerta
      Evento alerta = expedienteExcelUseCaseImpl.getLastEvento(expediente.getId(), 2);
      //Creamos un evento actividad, y recogemos el ultimo evento del expediente que esta iterando en ese momento y asignamos la clase 3 al evento actividad
      Evento actividad = expedienteExcelUseCaseImpl.getLastEvento(expediente.getId(), 3);
      //Mappeamos los expedientes recogidos en un ExpedienteDTO
      ExpedienteDTO ex = expedienteMapper.parse(expediente);
      //Si el evento accion es distinto de null seteamos en el expedienteDTO el titulo y la fecha del evento
      if (accion != null) {
        ex.setAccion(accion.getTitulo());
        ex.setFechaAccion(accion.getFechaCreacion());
      }
      //Si el evento alerta es distinto de null seteamos en el expedienteDTO el titulo y la fecha del evento
      if (alerta != null) {
        ex.setAlerta(alerta.getTitulo());
        ex.setFechaAlerta(alerta.getFechaCreacion());
      }
      //Si el evento actividad es distinto de null seteamos en el expedienteDTO el titulo y la fecha del evento
      if (actividad != null) {
        ex.setActividad(actividad.getTitulo());
        ex.setFechaActividad(actividad.getFechaCreacion());
      }
      //Añadimos el expediente dto parseado y con las actividades y fechas seteadas al expedienteDTO creado antes de comprobar los eventos
      expedientesDTOS.add(ex);
    }
    //Devolvemos la lista de expedientesDTO parseada y el tamaño del listado de todos los expedientes en memoria
    return new ListWithCountDTO<>(expedientesDTOS, expedientesSinPaginar.size());
  }

  //Buscamos todos los expedientes apropiados para minutas
  public ListWithCountDTO<ExpedienteDTO> getAllExpedientesMinutas(Usuario usuario) throws NotFoundException {
    //Creamos listado de expedientes sin paginar obteniendo los expedientes en memoria filtrados por el usuario que tenemos
    List<Expediente> expedientesSinPaginar = filterExpedientesInMemory(usuario, null, null, null, null, null, null, null, null, null, null);
    //Creamos listado de expedientesPre y recogemos los expedientes que tienen propuestas
    List<Expediente> expedientesPre = expedientesSinPaginar.stream().filter(exp -> exp.getPropuestas().size() > 0).collect(Collectors.toList());
    //Creamos listado de expedientesPos vacio
    List<Expediente> expedientesPos = new ArrayList<>();

    //Para cada expedientePre que tenemos obtenemos un listado de propuestas que tengan bienes
    for (Expediente expediente : expedientesPre) {
      List<Propuesta> propuestas = expediente.getPropuestas().stream().filter(pro -> pro.getBienes().size() > 0).collect(Collectors.toList());
      //Si la propuesta de cada expediente no esta vacia recogemos el expediente en listado de expesdientesPos
      if (!propuestas.isEmpty()) expedientesPos.add(expediente);
    }
    //Creamos listado de expedientesPROCE a traves de los expedientes que tenemos del filtrado anterior
    //Si en el expediente no hay contrato o los procedimientos estan vacios no recogemos el expediente, si tiene alguno de los 2 casos lo recogemos en la lista
    List<Expediente> expedientesProce = expedientesPos.stream().filter(ex -> {
      Contrato c = ex.getContratoRepresentante();
      if (c == null || c.getProcedimientos().isEmpty()) return false;
      return true;
    }).collect(Collectors.toList());

    //Creamos listado de expedientes y le seteamos los expedientesProce que
    List<Expediente> expedientes = expedientesProce.stream().filter(ex -> {
      //Creamos listado de propuestas y le seteamos las propuestas que tenemos de expedientesProce
      List<Propuesta> props = new ArrayList<>(ex.getPropuestas());
      //Para cada propuesta
      for (Propuesta p : props) {
        //Creamos listado de bienes que tiene cada propuesta
        List<Bien> bienes = p.getBienes().stream().map(PropuestaBien::getBien).collect(Collectors.toList());
        //Si la propuesta no tiene contratos devolvemos false
        if (p.getContratos().isEmpty()) return false;
        //Si la propuesta no tiene intervinientes devolvemos false
        if (p.getIntervinientes().isEmpty()) return false;
        //Si la propuesta no tiene acuerdos de pago devolvemos false
        if (p.getAcuerdosPago().isEmpty()) return false;
        //Para cada bien
        for (Bien b : bienes) {
          //Si el bien tiene las tasaciones vacias iteramos el siguiente bien
          if (b.getTasaciones().isEmpty()) {
            continue;
          }
          //Si el bien tiene las valoraciones vacias iteramos el siguiente bien
          if (b.getValoraciones().isEmpty()) {
            continue;
          }
          //Si el bien tiene las cargas vacias iteramos el siguiente bien
          if (b.getCargas().isEmpty()) {
            continue;
          }
          return true;
        }
      }
      return false;
    }).collect(Collectors.toList());

    //Creamos listado de expedientesDTO
    List<ExpedienteDTO> expedientesDTOS = new ArrayList<>();
    //Para cada expediente procesado en la iteracion anterior
    for (Expediente expediente : expedientes) {
      //Creamos un evento accion, y recogemos el ultimo evento del expediente que esta iterando en ese momento y asignamos la clase 1 al evento accion
      Evento accion = expedienteExcelUseCaseImpl.getLastEvento(expediente.getId(), 1);
      //Creamos un evento alerta, y recogemos el ultimo evento del expediente que esta iterando en ese momento y asignamos la clase 2 al evento alerta
      Evento alerta = expedienteExcelUseCaseImpl.getLastEvento(expediente.getId(), 2);
      //Creamos un evento actividad, y recogemos el ultimo evento del expediente que esta iterando en ese momento y asignamos la clase 3 al evento actividad
      Evento actividad = expedienteExcelUseCaseImpl.getLastEvento(expediente.getId(), 3);
      //Mappeamos el expediente iterado en un expedienteDTO
      ExpedienteDTO ex = expedienteMapper.parse(expediente);
      //Si la accion es distinto de null seteamos los valores titulo y fecha de creacion
      if (accion != null) {
        ex.setAccion(accion.getTitulo());
        ex.setFechaAccion(accion.getFechaCreacion());
      }
      //Si la alerta es distinto de null seteamos los valores titulo y fecha de creacion
      if (alerta != null) {
        ex.setAlerta(alerta.getTitulo());
        ex.setFechaAlerta(alerta.getFechaCreacion());
      }
      //Si la actividad es distinto de null seteamos los valores titulo y fecha de creacion
      if (actividad != null) {
        ex.setActividad(actividad.getTitulo());
        ex.setFechaActividad(actividad.getFechaCreacion());
      }
      //Si el estado de la propuesta es distinto de null seteamos propuesta a si
      if (ex.getEstadoPropuesta() != null) {
        ex.setPropuesta("si");
        //Si no seteamos a no
      } else {
        ex.setPropuesta("no");
      }
      //añadimos el expediente a la lista de expedientesDTO
      expedientesDTOS.add(ex);
    }
    //Devolvemos listado de expedientes y tamaño total de expedientes sin paginar
    return new ListWithCountDTO<>(expedientesDTOS, expedientesSinPaginar.size());
  }

  //Buscar todos los expedientes filtrados
  //Pasamos usuario actual, y de expediente: ID, estado, cliente, cartera, gestor, titular, estrategia, saldo, orderField, orderDirection, size y page
  @Override
  public ListWithCountDTO<ExpedienteDTO> getAllFilteredExpedientes(Usuario usuario, String id, String estado, String cliente, String cartera, String gestor, String titular, String estrategia, String saldo, String orderField, String orderDirection, Integer size, Integer page) throws NotFoundException {
    //Creamos listado de expedientesSinPaginar con los expedientes en memoria
    List<Expediente> expedientesSinPaginar = filterExpedientesInMemory(usuario, id, estado, estrategia, cliente, cartera, gestor, titular, saldo, orderField, orderDirection);
    //si el orden es distinto de null y es igual a saldo
    if (orderField != null && orderField.equals("saldo")) {
      //Creamos el objeto comparator, este objeto impone un ordenamiento total a una coleccion de objetos
      Comparator<Expediente> comparator = Comparator.comparingDouble(Expediente::getSaldoGestion);
      //Si orderDirection es asc, ordenamos de esta forma
      if (orderDirection.equals("asc")) expedientesSinPaginar.sort(comparator);
      //Si no ordenamos de forma descendiente
      else if (orderDirection.equals("desc")) expedientesSinPaginar.sort(comparator.reversed());
    }
    //Creamos listado de expedientes paginados introduciendo los expedientes que nos han pedido calculandolos mediante el tamaño y la pagina solicitado
    List<Expediente> expedientesPaginados = expedientesSinPaginar.stream().skip(size * page).limit(size).collect(Collectors.toList());
    //Creamos lista de expedientesDTO
    List<ExpedienteDTO> expedientesDTOS = new ArrayList<>();
    //Para cada expediente paginado
    for (Expediente expediente : expedientesPaginados) {
      //Creamos un evento accion, y recogemos el ultimo evento del expediente que esta iterando en ese momento y asignamos la clase 1 al evento accion
      Evento accion = expedienteExcelUseCaseImpl.getLastEvento(expediente.getId(), 1);
      //Creamos un evento alerta, y recogemos el ultimo evento del expediente que esta iterando en ese momento y asignamos la clase 2 al evento alerta
      Evento alerta = expedienteExcelUseCaseImpl.getLastEvento(expediente.getId(), 2);
      //Creamos un evento actividad, y recogemos el ultimo evento del expediente que esta iterando en ese momento y asignamos la clase 3 al evento actividad
      Evento actividad = expedienteExcelUseCaseImpl.getLastEvento(expediente.getId(), 3);
      //Creamos expedienteDTO y mapeamos el expediente anterior
      ExpedienteDTO ex = expedienteMapper.parse(expediente);
      //Si la accion es distinto de null seteamos titulo y fecha de creacion
      if (accion != null) {
        ex.setAccion(accion.getTitulo());
        ex.setFechaAccion(accion.getFechaCreacion());
      }
      //Si la alerta es distinto de null seteamos titulo y fecha de creacion
      if (alerta != null) {
        ex.setAlerta(alerta.getTitulo());
        ex.setFechaAlerta(alerta.getFechaCreacion());
      }
      //Si la actividad es distinto de null seteamos titulo y fecha de creacion
      if (actividad != null) {
        ex.setActividad(actividad.getTitulo());
        ex.setFechaActividad(actividad.getFechaCreacion());
      }
      //Si el estado de la propuesta es distinto de null seteamos
      if (ex.getEstadoPropuesta() != null) {
        ex.setPropuesta("si");
      } else {
        ex.setPropuesta("no");
      }
      //Añadimos el expedienteDTO al listado de expedientes
      expedientesDTOS.add(ex);
    }
    //Si estado que nos envian es distinto de null
    if (estado != null)
      //Filtramos los expedientesDTO que tienen valor en el estado
      expedientesDTOS = expedientesDTOS.stream().filter(c -> StringUtils.startsWithIgnoreCase(StringUtils.stripAccents(c.getEstado().getValor()), estado)).collect(Collectors.toList());
    //Si el saldo que nos envian es distinto de null
    if (saldo != null)
      //Filtramos los expedientesDTO que tienen valor en el saldo
      expedientesDTOS = expedientesDTOS.stream().filter(c -> StringUtils.startsWithIgnoreCase(c.getSaldo() + "", saldo)).collect(Collectors.toList());

    //Si orderField es distinto de null y es igual a estado
    if (orderField != null && orderField.equals("estado")) {
      //Comparamos los ExpedienteDTO
      Comparator<ExpedienteDTO> comparator =
        (ExpedienteDTO o1, ExpedienteDTO o2) -> StringUtils.compare(o1.getEstado() != null ? o1.getEstado().getValor() : null, o2.getEstado() != null ? o2.getEstado().getValor() : null);
      //Si orderDirection es asc ordenamos ascendente
      if (orderDirection.equals("asc")) expedientesDTOS.sort(comparator);
      //Si orderDirection es desc ordenamos descendiente
      else if (orderDirection.equals("desc")) expedientesDTOS.sort(comparator.reversed());
    }
    //Devolvemos lista de expedientesDTO y tamaño de lista de expedientes sin paginar
    return new ListWithCountDTO<>(expedientesDTOS, expedientesSinPaginar.size());
  }

  //Buscamos expedientes por ultima valoracion
  @Override
  public List<Map> getExpedientesByUltimaValoracion() {
    return expedienteRepository.getExpedientesPorUltimaValoracion();
  }

  //Buscamos historico de asignacion de gestores por el id expediente
  @Override
  public ListWithCountDTO<AsignacionGestoresDTO> getAllAsignacionGestoresHistorico(Integer expedienteId) throws Exception {
    Expediente expediente = this.findById(expedienteId);
    List<Map> maps = expedienteRepository.historicoAsignacionGestores(expedienteId);
    List<AsignacionGestoresDTO> asignacionGestoresDTOS = new ArrayList<>();
    for (Map map : maps) {
      asignacionGestoresDTOS.add(expedienteMapper.parseMap(map, expediente));
    }
    //maps.forEach(map -> asignacionGestoresDTOS.add(expedienteMapper.parseMap(map, expediente)));
    expedienteMapper.generarFechaFinAsignacion(asignacionGestoresDTOS);
    return new ListWithCountDTO<>(asignacionGestoresDTOS, asignacionGestoresDTOS.size());
  }

  //Buscamos expedientes por id
  @Override
  public OutputWithListDTO<ExpedienteDTO> findExpedienteDTOById(Integer id) throws Exception {
    //Recuperamos el expediente mediante el id
    Expediente expediente = findById(id);
    //Lo pasamos a DTO
    var ex = expedienteMapper.parse(expediente);
    //Si hay menos de 75 contratos
    if (expediente.getContratos().size() < 75) {
     //Recogemos los diferentes eventos y si son distintos de null seteamos accion y fecha
      Evento accion = expedienteExcelUseCaseImpl.getLastEvento(id, 1);
      Evento alerta = expedienteExcelUseCaseImpl.getLastEvento(id, 2);
      Evento actividad = expedienteExcelUseCaseImpl.getLastEvento(id, 3);
      if (accion != null) {
        ex.setAccion(accion.getTitulo());
        ex.setFechaAccion(accion.getFechaCreacion());
      }
      if (alerta != null) {
        ex.setAlerta(alerta.getTitulo());
        ex.setFechaAlerta(alerta.getFechaCreacion());
      }
      if (actividad != null) {
        ex.setActividad(actividad.getTitulo());
        ex.setFechaActividad(actividad.getFechaCreacion());
      }
      if (ex.getEstadoPropuesta() != null) {
        ex.setPropuesta("si");
      } else {
        ex.setPropuesta("no");
      }
    }

    String idOrigenConcatenados = "";
    Double precioCompra = 0.0;
    Double quita = 0.0;
    for (ContratoDTOToList contrato : ex.getContratos()) {
      if (contrato.getOperacion() != null && !idOrigenConcatenados.contains(contrato.getOperacion()))
        idOrigenConcatenados += contrato.getOperacion() + " ";
      precioCompra += contrato.getCapitalPendiente();
      quita += contrato.getQuita();
    }
    ex.setPrecioCompra(precioCompra - quita);
    if (idOrigenConcatenados.length() > 1)
      ex.setOperacion(idOrigenConcatenados.substring(0, idOrigenConcatenados.length() - 1));
    return new OutputWithListDTO<>(ex, contratoRepository.count());
  }

  //Buscamos expedientes en revision para el usuario enviado
  @Override
  public Integer getExpedientesEnRevision(Usuario usuario) {
    return expedienteRepository.countContratosEnRevisionOnExpediente(usuario.getId());
  }
  //Filtramos expedientes en memoria
  @Override
  public List<Expediente> filterExpedientesInMemory(Usuario usuario) {
    return this.filterExpedientesInMemory(usuario, null, null, null, null, null, null, null, null, null, null);
  }

  private List<Expediente> filterExpedientesInMemory(Usuario usuario, String id, String estado, String estrategia, String cliente, String cartera, String gestor, String titular, String saldo, String orderField, String orderDirection) {
    //
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Expediente> query = cb.createQuery(Expediente.class);

    Root<Expediente> root = query.from(Expediente.class);
    List<Predicate> predicates = new ArrayList<>();

    if (id != null) predicates.add(cb.like(root.get(Expediente_.ID_CONCATENADO), "%" + id + "%"));

    if (cliente != null)
      predicates.add(cb.like(root.join(Expediente_.cartera).join(Cartera_.clientes).join(ClienteCartera_.cliente).get(Cliente_.nombre), "%" + cliente + "%"));
    if (cartera != null)
      predicates.add(cb.like(root.get(Expediente_.cartera).get(Cartera_.NOMBRE), "%" + cartera + "%"));
    if (gestor != null) {
      Join<Expediente, AsignacionExpediente> join = root.join(Expediente_.asignaciones, JoinType.INNER);
      Predicate onCond = cb.equal(join.get(AsignacionExpediente_.usuario).get(Usuario_.perfil).get(Perfil_.NOMBRE), "Gestor");

      Join<AsignacionExpediente, Usuario> join2 = join.join(AsignacionExpediente_.usuario, JoinType.INNER);
      Predicate onCond2 = cb.like(join2.get(Usuario_.NOMBRE), "%" + gestor + "%");

      predicates.add(cb.and(onCond, onCond2));
    }
    if (usuario != null && (!usuario.esAdministrador() && !usuario.esResponsableFormalizacion())) {
      Predicate asignaciones = cb.equal(root.join(Expediente_.asignaciones, JoinType.LEFT).get(AsignacionExpediente_.USUARIO).get(Usuario_.ID), usuario.getId());
      Predicate asignacionesSuplente;
      Predicate predicateFinal = asignaciones;
      if (usuario.getPerfil() != null && usuario.getPerfil().getNombre().compareTo(Perfil.GESTOR) == 0) {

        Subquery<Integer> subquerySuplente = cb.createQuery().subquery(Integer.class);
        subquerySuplente.distinct(true);
        Root<Expediente> rootSuplentes = subquerySuplente.from(Expediente.class);
        Join<Expediente, AsignacionExpediente> joinSuplente = rootSuplentes.join(Expediente_.asignaciones, JoinType.INNER);
        Predicate predicateSuplente = cb.equal(joinSuplente.join(AsignacionExpediente_.SUPLENTE, JoinType.INNER).get(Usuario_.ID), usuario.getId());
        Subquery<Integer> subquerySuplente2 = subquerySuplente.select(rootSuplentes.get(Expediente_.id)).distinct(true).where(predicateSuplente);
        subquerySuplente2.distinct(true);

        predicateFinal = cb.or(asignaciones, root.get(Expediente_.ID).in(subquerySuplente2.distinct(true)));
      }
      else if (usuario.getPerfil() != null && usuario.getPerfil().getNombre().equals(Perfil.GESTOR_FORMALIZACION)){
        Join<Cartera, Usuario> join1 = root.join(Expediente_.cartera, JoinType.INNER).join(Cartera_.responsableFormalizacion, JoinType.INNER);
        Predicate prCar = cb.equal(join1.get(Usuario_.id), usuario.getId());

        predicateFinal = cb.or(asignaciones, prCar);
      }
      predicates.add(predicateFinal);
    }

    if (estrategia != null) {
      Join<Expediente, Contrato> join = root.join(Expediente_.contratos, JoinType.INNER);
      Predicate p1 = cb.isTrue(join.get(Contrato_.esRepresentante));
      Join<Contrato, EstrategiaAsignada> join1 = join.join(Contrato_.estrategia, JoinType.INNER);
      Predicate p2 = cb.like(cb.upper(join1.get(EstrategiaAsignada_.estrategia).get(Estrategia_.valor)), "%" + estrategia.toUpperCase() + "%");
      predicates.add(cb.and(p1, p2));
    }

    if (titular != null) {
      Join<Expediente, Contrato> join = root.join(Expediente_.contratos, JoinType.INNER);
      Predicate onCond = cb.equal(join.get(Contrato_.esRepresentante), true);

      Join<Contrato, ContratoInterviniente> join2 = join.join(Contrato_.INTERVINIENTES, JoinType.INNER);
      Predicate conj1 = cb.conjunction();
      conj1.getExpressions().add(cb.isNotNull(join2.get(ContratoInterviniente_.ORDEN_INTERVENCION)));
      conj1.getExpressions().add(cb.equal(join2.get(ContratoInterviniente_.ORDEN_INTERVENCION), 1));
      conj1.getExpressions().add(cb.equal(join2.get(ContratoInterviniente_.TIPO_INTERVENCION).get(TipoIntervencion_.VALOR), "TITULAR"));

      conj1.getExpressions().add(cb.or(cb.like((Expression<String>) cb.upper(join2.get(ContratoInterviniente_.INTERVINIENTE).get(Interviniente_.NOMBRE)).alias("nombre"), "%" + titular.toUpperCase() + "%"), cb.like((Expression<String>) cb.upper(join2.get(ContratoInterviniente_.INTERVINIENTE).get(Interviniente_.APELLIDOS)).alias("apellidos"), "%" + titular.toUpperCase() + "%"), cb.like((Expression<String>) cb.upper(join2.get(ContratoInterviniente_.INTERVINIENTE).get(Interviniente_.RAZON_SOCIAL)).alias("razonSocial"), "%" + titular.toUpperCase() + "%")));
      Predicate pTitular = cb.conjunction();
      pTitular.getExpressions().add(onCond);
      pTitular.getExpressions().add(conj1);
      predicates.add(pTitular);
    }

    if (saldo != null) {
  /*   Join<Expediente, Contrato> join = root.join(Expediente_.contratos, JoinType.INNER);
      Predicate onCond = cb.equal(join.get(Contrato_.esRepresentante), true);

      Join<Contrato, Saldo> join2 = join.join(Contrato_.SALDOS, JoinType.INNER);
      Expression<String> filterKeyExp = join2.get(Saldo_.SALDO_GESTION).as(String.class);

      Subquery<Double> subqueryI = cb.createQuery().subquery(Double.class);
      Root<Contrato> contratoPN = subqueryI.from(Contrato.class);

      var joinContrato=contratoPN.get(Contrato_.saldoGestion);

      String saldoString =  saldo.toString().trim();
      if (saldoString.endsWith(".0")){
        saldoString = saldoString.substring(0, saldo.length() - 2);
      }
      Predicate onCond2 = cb.like(filterKeyExp, "%" + saldoString + "%");
       predicates.add(cb.and(onCond, onCond2));*/
    }

    var rs = query.distinct(true).select(root).where(predicates.toArray(new Predicate[predicates.size()]));
    if (orderField != null) {
      if (orderField.equals("id")) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(Expediente_.id)));
        } else {
          rs.orderBy(cb.asc(root.get(Expediente_.id)));
        }
      } else if (orderField.equals("idConcatenado")) {
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(Expediente_.idConcatenado)));
        } else {
          rs.orderBy(cb.asc(root.get(Expediente_.idConcatenado)));
        }
      } else if (orderField.equals("cartera")) {
        Join<Expediente, Cartera> joinCartera = root.join(Expediente_.cartera, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(joinCartera.get(Cartera_.nombre)));
        } else {
          rs.orderBy(cb.asc(joinCartera.get(Cartera_.nombre)));
        }
      } else if (orderField.equals("cliente")) {
        Join<ClienteCartera, Cliente> join22 = root.join(Expediente_.cartera, JoinType.INNER).join(Cartera_.clientes, JoinType.INNER).join(ClienteCartera_.cliente, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(join22.get(Cliente_.NOMBRE)));
        } else {
          rs.orderBy(cb.asc(join22.get(Cliente_.NOMBRE)));
        }
      } else if (orderField.equals("gestor")) {
        Join<Expediente, AsignacionExpediente> join41 = root.join(Expediente_.ASIGNACIONES, JoinType.INNER);
        Join<AsignacionExpediente, Usuario> join42 = join41.join(AsignacionExpediente_.USUARIO, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(join42.get(Usuario_.NOMBRE)));
        } else {
          rs.orderBy(cb.asc(join42.get(Usuario_.NOMBRE)));
        }
      } else if (orderField.equals("usuarioId")) {
        Join<Expediente, AsignacionExpediente> join51 = root.join(Expediente_.ASIGNACIONES, JoinType.INNER);
        Join<AsignacionExpediente, Usuario> join52 = join51.join(AsignacionExpediente_.USUARIO, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(join52.get(Usuario_.ID)));
        } else {
          rs.orderBy(cb.asc(join52.get(Usuario_.ID)));
        }
      } else if (orderField.equals("estrategia")) {
        Join<Expediente, Contrato> join = root.join(Expediente_.contratos, JoinType.INNER);
        Predicate p1 = cb.isTrue(join.get(Contrato_.esRepresentante));
        join.on(p1);
        Join<Contrato, EstrategiaAsignada> join1 = join.join(Contrato_.estrategia, JoinType.INNER);
        Predicate p2 = cb.like(join1.get(EstrategiaAsignada_.estrategia).get(Estrategia_.valor), "%" + estrategia + "%");
        predicates.add(cb.and(p1, p2));
        List<Order> orderList = new ArrayList<>();
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          orderList.add(cb.desc(join1.get(EstrategiaAsignada_.ESTRATEGIA).get(TipoEstado_.VALOR)));
        } else {
          orderList.add(cb.asc(join1.get(EstrategiaAsignada_.ESTRATEGIA).get(TipoEstado_.VALOR)));
        }
        rs.orderBy(orderList);
      } else if (orderField.equals("titular")) {
        Join<Expediente, Contrato> join61 = root.join(Expediente_.CONTRATOS, JoinType.INNER);
        Predicate onCond = cb.equal(join61.get(Contrato_.esRepresentante), true);
        join61.on(onCond);
        Join<Contrato, ContratoInterviniente> join62 = join61.join(Contrato_.INTERVINIENTES, JoinType.INNER);
        Join<ContratoInterviniente, Interviniente> join63 = join62.join(ContratoInterviniente_.INTERVINIENTE, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(join63.get(Interviniente_.NOMBRE)));
        } else {
          rs.orderBy(cb.asc(join63.get(Interviniente_.NOMBRE)));
        }
      } else if (orderField.equals("saldo")) {
        Join<Expediente, Contrato> join71 = root.join(Expediente_.CONTRATOS, JoinType.INNER);
        Predicate onCond = cb.equal(join71.get(Contrato_.esRepresentante), true);
        join71.on(onCond);
        Join<Contrato, Saldo> join72 = join71.join(Contrato_.SALDOS, JoinType.INNER);
        if (orderDirection.toLowerCase().equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(join72.get(Saldo_.SALDO_GESTION)));
        } else {
          rs.orderBy(cb.asc(join72.get(Saldo_.SALDO_GESTION)));
        }
      }
    } else {
      //si no se selecciona ningún campo para ordenar, ordenamos ascendente por id por defecto
      rs.orderBy(cb.asc(root.get(Expediente_.id)));
    }
    return entityManager.createQuery(query).getResultList();
  }

  @Override
  public TipoEstado findByTipoEstado(Integer estado) throws Exception {
    Optional<TipoEstado> tipoEstadoOptional = tipoEstadoRepository.findById(estado);
    if (tipoEstadoOptional.isEmpty()) throw new NotFoundException("Tipo de estado", estado);
    return tipoEstadoOptional.get();
  }


  @Override
  public SubtipoEstado findByTipoSubEstado(Integer subEstado) throws Exception {
    Optional<SubtipoEstado> tipoSubEstadoOptional = subtipoEstadoRepository.findById(subEstado);
    if (tipoSubEstadoOptional.isEmpty()) throw new NotFoundException("Subestado", subEstado);
    return tipoSubEstadoOptional.get();
  }

  @Override
  public ExpedienteDTO updateModoGestion(Integer expedienteId, Integer estado, Integer subEstado, Usuario usuario)
    throws Exception {
    Expediente expediente = expedienteRepository.findById(expedienteId).get();
    //actualizamos fecha de cierre de expediente
    var nuevoEstado = this.findByTipoEstado(estado);
    SubtipoEstado subtipoEstado = subtipoEstadoRepository.findById(subEstado).orElse(null);
    if (nuevoEstado.getCodigo().equals("05")) expediente.setFechaCierre(new Date());
    expediente.setTipoEstado(nuevoEstado);
    expediente.setSubEstado(subEstado != null ? this.findByTipoSubEstado(subEstado) : null);
    //crear evento
    EventoInputDTO evento = new EventoInputDTO();
    evento.setCartera(expediente.getCartera().getId());
    evento.setIdExpediente(expediente.getId());
    evento.setDestinatario(usuario.getId());
    evento.setClaseEvento(3);
    TipoEvento tipoEvento = tipoEventoRepository.findByCodigo("ACT_04").orElse(null);
    evento.setTipoEvento(tipoEvento.getId());
    evento.setFechaLimite(new Date());
    evento.setFechaAlerta(new Date());
    evento.setFechaCreacion(new Date());
    SubtipoEvento se = subtipoEventoRepository.findByCodigoAndCarteraId("MGES",expediente.getCartera().getId()).orElseThrow(() ->
      new NotFoundException("SubtipoEvento MGES","Cartera" + expediente.getCartera().getId()));
    evento.setSubtipoEvento(se.getId());
    evento.setImportancia(2);
    evento.setNivel(1);
    evento.setNivelId(expediente.getId());
    evento.setTitulo("Cambio modo gestión");
    Locale loc = LocaleContextHolder.getLocale();
    String comentario = "";
    if (loc.getLanguage().equals("en")) {
      if(subtipoEstado != null) comentario = ", "+subtipoEstado.getValorIngles();
      evento.setComentariosDestinatario(nuevoEstado.getValorIngles()+comentario);
    } else {
      if(subtipoEstado != null) comentario = ", "+subtipoEstado.getValor();
      evento.setComentariosDestinatario(nuevoEstado.getValor()+comentario);
    }

    eventoUseCase.create(evento,usuario);

    return expedienteMapper.parse(expedienteRepository.save(expediente));
  }

  @Override
  public List<CatalogoMinInfoDTO> filtroSubEstado(Integer estado) throws Exception {
    List<SubtipoEstado> a = subtipoEstadoRepository.findAllByEstadoIdAndActivoIsTrue(estado);
    return a.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
  }

  public TipoEstado estadoExpediente(Integer idExpediente) throws NotFoundException {
    Optional<Expediente> expedienteOptional = expedienteRepository.findById(idExpediente);
    if (expedienteOptional.isEmpty()) throw new NotFoundException("Expediente", idExpediente);
    Expediente expediente = expedienteOptional.get();
    return expediente.getTipoEstado();
  }

  @Override
  public Expediente findByIdCargaOrNull(String idCarga) {
    return this.expedienteRepository.findByIdCarga(idCarga).orElse(null);
  }

  private void updateMGSPN(Integer epnId, Integer estado, Integer subEstado) throws Exception {
    ExpedientePosesionNegociada expediente = epnRepository.findById(epnId).get();
    //actualizamos fecha de cierre de expediente
    var nuevoEstado = this.findByTipoEstado(estado);
    if (nuevoEstado.getCodigo().equals("05")) expediente.setFechaCierre(new Date());
    expediente.setTipoEstado(nuevoEstado);
    expediente.setSubEstado(subEstado != null ? this.findByTipoSubEstado(subEstado) : null);
    epnRepository.save(expediente);
  }


  @Override
  public List<ExpedienteDTO> listaExpedientesPerfil(Integer perfilId) throws NotFoundException {
    List<Usuario> listausuarios = new ArrayList<>();
    listausuarios = usuarioRepository.findByPerfilId(perfilId);
    Set<AsignacionExpediente> listaexpedientesAsignados = new HashSet<>();
    // List<Expediente> listaexpedientes = new ArrayList<>();
    List<ExpedienteDTO> listaexpedientesDTO = new ArrayList<>();
    for (Usuario nuevousaurio : listausuarios) {
      listaexpedientesAsignados = nuevousaurio.getAsignacionExpedientes();
      for (AsignacionExpediente nuevoexpediente : listaexpedientesAsignados) {
        Expediente expediente = nuevoexpediente.getExpediente();
        ExpedienteDTO expedienteDTO = expedienteMapper.parse(expediente);
        listaexpedientesDTO.add(expedienteDTO);
      }
    }
    return listaexpedientesDTO;
  }

  public void concatenar() {
    List<Expediente> expedientes = expedienteRepository.findAll();
    for (Expediente ex : expedientes) {
      Cartera cartera = ex.getCartera();
      String criterioExpediente = cartera.getCriterioGeneracionExpediente().getCodigo();
      List<ClienteCartera> clientes = new ArrayList<>(cartera.getClientes());
      Cliente cliente = clientes.get(0).getCliente();
      String concatenado = null;
      if (criterioExpediente.equals("cliente")) {
        Contrato contrato = ex.getContratoRepresentante();
        if (contrato == null) continue;
        Interviniente interviniente = contrato.getPrimerInterviniente();
        if (interviniente == null) continue;
        if (interviniente.getIdCarga() == null) continue;
        concatenado = generarNoHeredado(cartera.getId(), cliente.getId(), interviniente.getIdCarga());
      } else if (criterioExpediente.equals("contrato")) {
        Contrato contrato = ex.getContratoRepresentante();
        if (contrato == null) continue;
        if (contrato.getIdCarga() == null) continue;
        concatenado = generarNoHeredado(cartera.getId(), cliente.getId(), contrato.getIdCarga());
      } else if (criterioExpediente.equals("heredado")) {
        concatenado = generarHeredado(cartera.getId(), cliente.getId(), ex.getIdCarga());
      }
      ex.setIdConcatenado(concatenado);
    }
    expedienteRepository.saveAll(expedientes);
  }

  public StringBuilder generarConcatenado(Integer cliente, Integer cartera) {
    StringBuilder result = new StringBuilder();
    String clienteS = cliente.toString();
    for (Integer sice = clienteS.length(); sice < 4; sice++) {
      result.append("0");
    }
    result.append(clienteS);
    String carteraS = cartera.toString();
    for (Integer sice = carteraS.length(); sice < 4; sice++) {
      result.append("0");
    }
    result.append(carteraS);

    return result;
  }

  private String generarHeredado(Integer cliente, Integer cartera, String carga) {
    StringBuilder result = generarConcatenado(cliente, cartera);
    result.append(carga);
    return result.toString();
  }

  @Override
  public String generarNoHeredado(Integer cliente, Integer cartera, String carga) {
    StringBuilder result = generarConcatenado(cliente, cartera);

    if (carga.length() == 10) result.append(carga);
    else if (carga.length() > 10) result.append(carga.substring(carga.length() - 10));
    else {
      for (Integer sice = carga.length(); sice < 10; sice++) {
        result.append("0");
      }
      result.append(carga);
      ;
    }
    return result.toString();
  }

  public ResponseEntity<InputStreamResource> minutaWord(Integer tipo, ExpedienteMinutaDTO expedienteMinutaDTO) throws NotFoundException, RequiredValueException {
    Cliente cliente = clienteRepository.findById(expedienteMinutaDTO.getIdCliente()).orElseThrow(() -> new NotFoundException("cliente", expedienteMinutaDTO.getIdCliente()));

    Expediente expediente = expedienteRepository.findById(expedienteMinutaDTO.getIdExpediente()).orElseThrow(() -> new NotFoundException("expediente", expedienteMinutaDTO.getIdExpediente()));
    Cartera cartera = expediente.getCartera();
    Propuesta propuesta = propuestaRepository.findById(expedienteMinutaDTO.getIdPropuesta()).orElseThrow(() -> new NotFoundException("propuesta", expedienteMinutaDTO.getIdPropuesta()));

    List<Interviniente> intervinientes = intervinienteRepository.findAllById(expedienteMinutaDTO.getIdIntervinientes());
    List<ImportePropuesta> importes = importePropuestaRepository.findAllById(expedienteMinutaDTO.getIdImportes());
    List<Contrato> contratos = contratoRepository.findAllById(expedienteMinutaDTO.getIdContratos());

    ResponseEntity<InputStreamResource> result = expedienteUtil.wordExport(tipo, cliente, expedienteMinutaDTO.getFecha(), expediente, propuesta, intervinientes, importes, contratos, expedienteMinutaDTO.getIdBienes());
    return result;
  }

  public ResponseEntity<byte[]> minutaPdf(Integer tipo, ExpedienteMinutaDTO expedienteMinutaDTO) throws IOException, NotFoundException, RequiredValueException {
    Cliente cliente = clienteRepository.findById(expedienteMinutaDTO.getIdCliente()).orElseThrow(() -> new NotFoundException("cliente", expedienteMinutaDTO.getIdCliente()));

    Expediente expediente = expedienteRepository.findById(expedienteMinutaDTO.getIdExpediente()).orElseThrow(() -> new NotFoundException("expediente", expedienteMinutaDTO.getIdExpediente()));
    Cartera cartera = expediente.getCartera();
    Propuesta propuesta = propuestaRepository.findById(expedienteMinutaDTO.getIdPropuesta()).orElseThrow(() -> new NotFoundException("propuesta", expedienteMinutaDTO.getIdPropuesta()));

    List<Interviniente> intervinientes = intervinienteRepository.findAllById(expedienteMinutaDTO.getIdIntervinientes());
    List<ImportePropuesta> importes = importePropuestaRepository.findAllById(expedienteMinutaDTO.getIdImportes());
    List<Contrato> contratos = contratoRepository.findAllById(expedienteMinutaDTO.getIdContratos());


    return expedienteUtil.pdfExport(tipo, cliente, expedienteMinutaDTO.getFecha(), expediente, propuesta, intervinientes, importes, contratos, expedienteMinutaDTO.getIdBienes());
  }

  @Override
  public Boolean filtroMesInformes(Expediente expediente, Integer mes) {

    Integer mesActual = Calendar.getInstance().get(Calendar.MONTH);
    Integer mesParametro = mes - 1;

    Calendar hoy = Calendar.getInstance();
    Calendar calVariableInicio = Calendar.getInstance();
    Calendar calVariableFin = Calendar.getInstance();
    if (mesActual >= mesParametro) {
      calVariableInicio.set(hoy.get(Calendar.YEAR), mesParametro, 1, 0, 0, 0);
      calVariableFin.set(hoy.get(Calendar.YEAR), mesParametro, 1, 0, 0, 0);
      calVariableFin.add(Calendar.MONTH, 1);
    } else {
      calVariableInicio.set(hoy.get(Calendar.YEAR) - 1, mesParametro, 1, 0, 0, 0);
      calVariableFin.set(hoy.get(Calendar.YEAR) - 1, mesParametro, 1, 0, 0, 0);
      calVariableFin.add(Calendar.MONTH, 1);
    }

    Date variableInicio = calVariableInicio.getTime();
    Date variableFinal = calVariableFin.getTime();
    Calendar calEx = Calendar.getInstance();
    calEx.set(Calendar.YEAR, 3900);

    var fechaCreacionExp = expediente.getFechaCreacion();
    var fechaCierreExp = expediente.getFechaCierre() != null ? expediente.getFechaCierre() : calEx.getTime();

    boolean comprobacion = false;
    if (variableInicio.compareTo(fechaCreacionExp) * fechaCreacionExp.compareTo(variableFinal) >= 0) {
      comprobacion = true;
    } else if (variableInicio.compareTo(fechaCierreExp) * fechaCierreExp.compareTo(variableFinal) >= 0) {
      comprobacion = true;
      //Si la fecha de inicio es anterior
    } else if (fechaCreacionExp.before(variableInicio) && fechaCierreExp.after(variableFinal)) {
      comprobacion = true;
    }
    return comprobacion;
  }

  @Override
  public Boolean filtroMesInformesPosesionNegociada(ExpedientePosesionNegociada expediente, Integer mes) {

    Integer mesActual = Calendar.getInstance().get(Calendar.MONTH);
    Integer mesParametro = mes - 1;

    Calendar hoy = Calendar.getInstance();
    Calendar calVariableInicio = Calendar.getInstance();
    Calendar calVariableFin = Calendar.getInstance();
    if (mesActual >= mesParametro) {
      calVariableInicio.set(hoy.get(Calendar.YEAR), mesParametro, 1, 0, 0, 0);
      calVariableFin.set(hoy.get(Calendar.YEAR), mesParametro, 1, 0, 0, 0);
      calVariableFin.add(Calendar.MONTH, 1);
    } else {
      calVariableInicio.set(hoy.get(Calendar.YEAR) - 1, mesParametro, 1, 0, 0, 0);
      calVariableFin.set(hoy.get(Calendar.YEAR) - 1, mesParametro, 1, 0, 0, 0);
      calVariableFin.add(Calendar.MONTH, 1);
    }

    Date variableInicio = calVariableInicio.getTime();
    Date variableFinal = calVariableFin.getTime();
    Calendar calEx = Calendar.getInstance();
    calEx.set(Calendar.YEAR, 3900);

    var fechaCreacionExp = expediente.getFechaCreacion() != null ? expediente.getFechaCreacion() : calEx.getTime();
    var fechaCierreExp = expediente.getFechaCierre() != null ? expediente.getFechaCierre() : calEx.getTime();

    boolean comprobacion = false;
    if (variableInicio.compareTo(fechaCreacionExp) * fechaCreacionExp.compareTo(variableFinal) >= 0) {
      comprobacion = true;
    } else if (variableInicio.compareTo(fechaCierreExp) * fechaCierreExp.compareTo(variableFinal) >= 0) {
      comprobacion = true;
      //Si la fecha de inicio es anterior
    } else if (fechaCreacionExp.before(variableInicio) && fechaCierreExp.after(variableFinal)) {
      comprobacion = true;
    }
    return comprobacion;
  }

  @Override
  public ResponseEntity<List<ExpedienteDTO>> expedienteFiltradoCarteraRA(Integer idCartera, Usuario usuario, Integer size, Integer page) {

    Pageable pageable = PageRequest.of(page, size);
    Page<Expediente> pageExpediente = expedienteRepository.findAllByCarteraIdAndAsignacionesUsuarioId(idCartera, usuario.getId(), pageable);

    List<Expediente> listExpediente = pageExpediente.getContent().stream().collect(Collectors.toList());

    List<ExpedienteDTO> listExpedienteDTO = new ArrayList<>();

    for (Expediente expediente : listExpediente) {
      try {
        listExpedienteDTO.add(expedienteMapper.parse(expediente));
      } catch (NotFoundException e) {
        e.printStackTrace();
      }
    }

    return ResponseEntity.ok(listExpedienteDTO);
  }

  @Override
  public ResponseEntity<List<ExpedientePosesionNegociadaDTO>> expedienteFiltradoCarteraPN(Integer idCartera, Usuario usuario, Integer size, Integer page) {

    Pageable pageable = PageRequest.of(page, size);
    Page<ExpedientePosesionNegociada> pageExpedientePN = epnRepository.findAllByCarteraIdAndAsignacionesUsuarioId(idCartera, usuario.getId(), pageable);

    List<ExpedientePosesionNegociada> listExpedientePosesionNegociada = pageExpedientePN.getContent().stream().collect(Collectors.toList());

    List<ExpedientePosesionNegociadaDTO> listExpedientePosesionNegociadaDTO = new ArrayList<>();

    for (ExpedientePosesionNegociada expedientePosesionNegociada : listExpedientePosesionNegociada) {
      listExpedientePosesionNegociadaDTO.add(expedientePosesionNegociadaMapper.parse(expedientePosesionNegociada));
    }

    return ResponseEntity.ok(listExpedientePosesionNegociadaDTO);
  }

  @Override
  public List<ExpedienteDTO> expedienteFiltradoCarteraSinGestor(Integer carteraID) throws NotFoundException {

    List<Expediente> expedienteCarteraList = expedienteRepository.findAllByCarteraId(carteraID);
    List<Expediente> expedienteSinGestorList = new ArrayList<>();
    List<ExpedienteDTO> expedienteDTOList = new ArrayList<>();
    for (Expediente expediente : expedienteCarteraList) {
      Usuario gestor = expediente.getGestor();
      if (gestor != null) {
        expedienteSinGestorList.add(expediente);
      }
    }

    for (Expediente expediente : expedienteSinGestorList) {
      ExpedienteDTO expedienteDTO = expedienteMapper.parse(expediente);
      expedienteDTOList.add(expedienteDTO);
    }

    return expedienteDTOList;
  }


}
