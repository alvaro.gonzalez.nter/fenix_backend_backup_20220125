package com.haya.alaska.expediente.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.infrastructure.controller.dto.EstrategiaAsignadaOutputDTO;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ExpedienteSegmentacionDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  //private Integer id;
  private String idConcatenado;

  private String tipoInterviniente; //persona fisica o juridica?
  private String garantia; // mobiliaria o inmobiliaria?
  private String tipoProducto;
  private Double valorGarantia; //suma del importe valor garantizado de todos los contratos que lo componen
  private String provinciaPrimeraGarantia; //de la garantia de mayor valor del contrato representante
  private String localidadPrimeraGarantia; //de la garantia de mayor valor del contrato representante
  private String direccionPrimerTitular; //direccion del primer interviniente
  private String tipoProcedimiento; //del contrato representante. si hay varios, el mas reciente. //CatalogoMinInfoDTO en el otro dto
  private String hitoJudicial;
  private String situacionContable; //clasificacion contable
  private Boolean cargasPosteriores; // si tiene cargas posteriores alguno de los bienes que esten como garantia en el contrato principal
  private String estrategia; //estrategia de BP que tiene el contrato representante
  private Boolean rankingAgencia; //ranking de la agencia para el reparto de expedientes

}
