package com.haya.alaska.expediente.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InformeExpedienteInputDTO {

  private Integer estadoExpediente;
  private Date fechaCreacionExpedienteInicial;
  private Date fechaCreacionExpedienteFinal;
  private Date fechaCierreExpedienteInicial;
  private Date fechaCierreExpedienteFinal;

}
