package com.haya.alaska.expediente.infrastructure.util;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.estado_contrato.infrastructure.repository.EstadoContratoRepository;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estado_propuesta.infrastructure.repository.EstadoPropuestaRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.BienMinutaDTO;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.infrastructure.repository.ProcedimientoRepository;
import com.haya.alaska.procedimiento.infrastructure.util.ProcedimientoUtil;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.shared.util.NumberConversor;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.infrastructure.repository.TasacionRepository;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class ExpedienteUtil {
  @Autowired
  EstadoContratoRepository estadoContratoRepository;
  @Autowired
  ProcedimientoRepository procedimientoRepository;
  @Autowired
  EstadoPropuestaRepository estadoPropuestaRepository;
  @Autowired
  BienRepository bienRepository;
  @Autowired
  ProcedimientoUtil procedimientoUtil;
  @Autowired
  TasacionRepository tasacionRepository;

  /*
   * Devuelve el estado de un expediente en función del estado de sus contratos y de la gestión actual del expediente.
   *  Activo--> Expediente vivo con un gestor asignado.
   *  Pendiente gestión --> Expediente vivo sin gestor asignado.
   *  Revisión--> Expediente vivo para el cual se ha generado un warning en alguno de los asuntos AAFF/AAII) que lo componen.
   *  Cerrado--> Expediente sin ningún asunto (AAFF/AAII) activo.
   * */
  public EstadoContrato getEstado(Expediente expediente) {
    boolean enRevision = false;
    boolean hayAbiertos = false;
    String codigo;

    if (expediente.getGestor() == null) {
      for (Contrato contrato : expediente.getContratos()) {
        if (contrato.getActivo()) hayAbiertos = true;
      }
      if(!hayAbiertos)
        codigo = "CER";
      else
        codigo = "PEN";
    }
    else {
      for (Contrato contrato : expediente.getContratos()) {
        if (contrato.getEnRevision() != null && contrato.getEnRevision()) enRevision = true;
        if (contrato.getActivo()) hayAbiertos = true;
      }
      if (!hayAbiertos) codigo = "CER";
      else {
        if (enRevision) codigo = "REV";
        else codigo = "ACT";
      }
    }
    EstadoContrato estadoContrato = estadoContratoRepository.findByCodigo(codigo).orElse(null);
    return estadoContrato;
  }

  /*
   * Devuelve el estado de un expediente en función del estado de sus contratos y de la gestión actual del expediente.
   *  Activo--> Expediente vivo con un gestor asignado.
   *  Pendiente gestión --> Expediente vivo sin gestor asignado.
   *  Revisión--> Expediente vivo para el cual se ha generado un warning en alguno de los asuntos AAFF/AAII) que lo componen.
   *  Cerrado--> Expediente sin ningún asunto (AAFF/AAII) activo.
   * */
  public EstadoContrato getEstado(ExpedientePosesionNegociada expediente) {
    boolean enRevision = false;
    boolean hayAbiertos = false;
    String codigo;

    if (expediente.getGestor() == null) codigo = "PEN";
    else {
      Contrato contrato = expediente.getContrato();
      if (contrato != null) {
        if (contrato.getEnRevision() != null && contrato.getEnRevision()) enRevision = true;
        if (contrato.getActivo()) hayAbiertos = true;
        if (!hayAbiertos) codigo = "CER";
        else {
          if (enRevision) codigo = "REV";
          else codigo = "ACT";
        }
      } else {
        codigo = "";
      }
    }
    EstadoContrato estadoContrato = estadoContratoRepository.findByCodigo(codigo).orElse(null);
    return estadoContrato;
  }

  public EstadoPropuesta getEstadoPropuesta(Expediente expediente) {
    Set<Propuesta> propuestas = expediente.getPropuestas();
    Integer idMasALto = 0;
    for (Propuesta p : propuestas) {
      if(p.getActivo() == false)
        continue;
      Integer id = p.getEstadoPropuesta() != null ? p.getEstadoPropuesta().getId() : 0;
      if (idMasALto < id) idMasALto = id;
    }
    if (idMasALto != 0) return estadoPropuestaRepository.findById(idMasALto).orElse(null);
    else return null;
  }

  public Propuesta getPropuesta(Expediente expediente) {
    Set<Propuesta> propuestas = expediente.getPropuestas();
    Integer idMasALto = 0;
    Propuesta p1 = null;
    for (Propuesta p : propuestas) {
      Integer id = p.getEstadoPropuesta() != null ? p.getEstadoPropuesta().getId() : 0;
      if (idMasALto < id){
        idMasALto = id;
        p1 = p;
      }
    }
    return p1;
  }

  public EstadoPropuesta getEstadoPropuestaPN(ExpedientePosesionNegociada expediente) {
    Set<Propuesta> propuestas = expediente.getPropuestas();
    Integer idMasALto = 0;
    for (Propuesta p : propuestas) {
      if(p.getActivo() == false)
        continue;
      Integer id = p.getEstadoPropuesta() != null ? p.getEstadoPropuesta().getId() : 0;
      if (idMasALto < id) idMasALto = id;
    }
    if (idMasALto != 0) return estadoPropuestaRepository.findById(idMasALto).orElse(null);
    else return null;
  }

  public ResponseEntity<byte[]> pdfExport(
    Integer tipo,
    Cliente cliente,
    String fechaString,
    Expediente expediente,
    Propuesta propuesta,
    List<Interviniente> intervinientes,
    List<ImportePropuesta> importes,
    List<Contrato> contratos,
    List<BienMinutaDTO> bienesDTO)
    throws RequiredValueException {

    Formalizacion formalizacion = propuesta.getFormalizaciones().stream().findFirst().get();
    XWPFDocument documento;

    try {
      documento =
        procesar(
          tipo,
          cliente,
          fechaString,
          expediente,
          propuesta,
          intervinientes,
          importes,
          contratos,
          bienesDTO);

      if (documento == null) return null;

      try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
        documento.write(outputStream);
        documento.close();

        documento = new XWPFDocument(new ByteArrayInputStream(outputStream.toByteArray()));
        PdfOptions options = PdfOptions.create();
        PdfConverter converter = (PdfConverter) PdfConverter.getInstance();
        converter.convert(
          documento,
          new FileOutputStream(
            "src/main/java/com/haya/alaska/expediente/infrastructure/documents/Documento1.pdf"),
          options);

        documento.close();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        headers.add(
          "Content-Disposition",
          "attachment; filename="
            + "informe-formalizacion-"
            + formalizacion.getId()
            + "-"
            + fechaString
            + ".pdf");

        Path pdfPath =
          Paths.get(
            "src/main/java/com/haya/alaska/expediente/infrastructure/documents/Documento1.pdf");
        byte[] pdf = Files.readAllBytes(pdfPath);

        ResponseEntity<byte[]> response = new ResponseEntity<>(pdf, headers, HttpStatus.OK);
        return response;

      } catch (IOException e) {
        e.printStackTrace();
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

  public ResponseEntity<InputStreamResource> wordExport(
    Integer tipo,
    Cliente cliente,
    String fechaString,
    Expediente expediente,
    Propuesta propuesta,
    List<Interviniente> intervinientes,
    List<ImportePropuesta> importes,
    List<Contrato> contratos,
    List<BienMinutaDTO> bienesDTO)
    throws RequiredValueException {

    Formalizacion formalizacion = propuesta.getFormalizaciones().stream().findFirst().get();
    XWPFDocument documento;
    documento =
      procesar(
        tipo,
        cliente,
        fechaString,
        expediente,
        propuesta,
        intervinientes,
        importes,
        contratos,
        bienesDTO);
    if (documento == null) return null;

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      documento.write(outputStream);
      ByteArrayInputStream result = new ByteArrayInputStream(outputStream.toByteArray());
      HttpHeaders headers = new HttpHeaders();
      headers.add(
        "Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      // TODO coger la fecha
      headers.add(
        "Content-Disposition",
        "attachment; filename="
          + "informe-formalizacion-"
          + formalizacion.getId()
          + "_"
          + formalizacion.getFechaFirma()
          + ".docx");
      return ResponseEntity.ok().headers(headers).body(new InputStreamResource(result));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  private XWPFDocument procesar(
    Integer tipo,
    Cliente cliente,
    String fechaString,
    Expediente expediente,
    Propuesta propuesta,
    List<Interviniente> intervinientes,
    List<ImportePropuesta> importes,
    List<Contrato> contratos,
    List<BienMinutaDTO> bienesDTO)
    throws RequiredValueException {

    XWPFDocument documento;

    String url;
    if (tipo.equals(1))
      url =
        "src/main/java/com/haya/alaska/expediente/infrastructure/documents/plantillaMinutaCH.docx";
    else if (tipo.equals(2))
      url =
        "src/main/java/com/haya/alaska/expediente/infrastructure/documents/plantillaMinutaCV.docx";
    else{
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException("The allowed values of type are 1: Cancellation of mortgage and 2: Buy-Sell");
      else throw new RequiredValueException("Los valores permitidos de tipo son 1: Cancelación Hipoteca, y 2: Compra-Venta");
    }

    try {
      InputStream resourceAsStream = new FileInputStream(url);
      documento = new XWPFDocument(resourceAsStream);

      Map<String, String> mapaVisita =
        obtenerMap(
          cliente,
          fechaString,
          expediente,
          propuesta,
          intervinientes,
          importes,
          contratos,
          bienesDTO);

      Formalizacion formalizacion = propuesta.getFormalizaciones().stream().findFirst().get();

      if (tipo.equals(1)) {
        duplicarCHfinca(documento, bienesDTO);
      } else if (tipo.equals(2)) {
        duplicarVendedora(documento, intervinientes);
        duplicarFinca(documento, bienesDTO, contratos);
        duplicarContratos(documento, contratos);
        duplicarCargas(documento, contratos);
      }

      for (XWPFParagraph p : documento.getParagraphs()) {
        replace2(p, mapaVisita);
      }
      for (XWPFTable tbl : documento.getTables()) {
        for (XWPFTableRow row : tbl.getRows()) {
          for (XWPFTableCell cell : row.getTableCells()) {
            for (XWPFParagraph p : cell.getParagraphs()) {
              replace2(p, mapaVisita);
            }
          }
        }
      }

      for (int i = documento.getParagraphs().size() - 1; i >= 0; i--) {
        XWPFParagraph p = documento.getParagraphArray(i);
        replaceDuplicators(documento, p);
      }

      return documento;
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    } catch (NotFoundException e) {
      e.printStackTrace();
    }

    return null;
  }

  private Map<String, String> obtenerMap(
    Cliente cliente,
    String fechaString,
    Expediente expediente,
    Propuesta propuesta,
    List<Interviniente> intervinientes,
    List<ImportePropuesta> importes,
    List<Contrato> contratos,
    List<BienMinutaDTO> bienesDTO)
    throws ParseException {

    String[] meses = {
      "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    };
    String[] mesesEng = {
      "January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    };

    Cartera cartera = expediente.getCartera();
    Formalizacion formalizacion = propuesta.getFormalizaciones().stream().findFirst().get();

    Contrato contratoRepresentante = expediente.getContratoRepresentante();
    Interviniente primerInterviniente = contratoRepresentante.getPrimerInterviniente();
    Direccion direccionPI = primerInterviniente.getDirecciones().stream().findFirst().orElse(null);
    Procedimiento procedimiento =
      contratoRepresentante.getProcedimientos().stream().findFirst().orElse(null);
    for (Procedimiento p : contratoRepresentante.getProcedimientos()) {
      if (p.getTipo().getCodigo().equals("HIP")) {
        procedimiento = p;
        break;
      }
    }

    List<Interviniente> representantes = findAllTitulares(contratos);

    Date fecha = new SimpleDateFormat("yyyy-MM-dd").parse(fechaString);
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(fecha);

    Double saldoImpago = getSaldoInpago(contratos);

    String origenesContratos =
      contratos.stream().map(Contrato -> {
        if (Contrato.getIdCarga() != null) {
          return Contrato.getIdCarga();
        } else {
          return " ";
        }
      })
        .collect(Collectors.joining("y "));
    String nombresIntervinientes =
      intervinientes.stream()
        .map(inte -> inte.getNombre() + " " + inte.getApellidos())
        .collect(Collectors.joining(", "));
    String nombresTitulares = "";
    for (Interviniente inte : representantes) {
      if (inte != null) {
        nombresTitulares += inte.getNombre() != null ? inte.getNombre() + " " : "";
        nombresTitulares += inte.getApellidos() != null ? inte.getApellidos() + " " : "";
        nombresTitulares += inte.getRazonSocial() != null ? inte.getRazonSocial() + "" : "";
        nombresTitulares += ", ";
      }
    }
    String fincas = numerosFincas(bienesDTO);

    Double sumatorioTasaciones = getAllTasaciones(bienesDTO);

    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();

    Map<String, String> mapaVisita = new HashMap<>();

    mapaVisita.put("diaCabecera", calendar.get(Calendar.DAY_OF_MONTH) + "");

    String mes = "";
    if (defaultLocal.equals("es"))
      mes = meses[calendar.get(Calendar.MONTH)];
    else if (defaultLocal.equals("en"))
      mes = mesesEng[calendar.get(Calendar.MONTH)];
    mapaVisita.put("mesCabecera", mes);

    mapaVisita.put("anyoCabecera", calendar.get(Calendar.YEAR) + "");
    mapaVisita.put("notario", formalizacion.getNotario());

    mapaVisita.put(
      "nombrePrimerTitular",
      primerInterviniente.getNombre() + " " + primerInterviniente.getApellidos());
    mapaVisita.put(
      "provinciaPrimerTitular",
      direccionPI.getProvincia() != null ? direccionPI.getProvincia().getValor() : null);
    mapaVisita.put("direccionPrimerTitular", formatDirection(direccionPI));
    mapaVisita.put("dniPrimerTitular", primerInterviniente.getNumeroDocumento());

    mapaVisita.put(
      "fechaFirma",
      formalizacion.getFechaFirma() != null ? formalizacion.getFechaFirma().toString() : null);
    mapaVisita.put("origenesContratos", origenesContratos != null ? origenesContratos : " ");
    mapaVisita.put("numerosFincas", fincas);
    mapaVisita.put(
      "importeQuitaFormalizacion", String.valueOf(0.0)); // TODO: falta integracion con pbc
    mapaVisita.put("importeQuitaFormalizacionLetra", NumberConversor.parseDouble(0.0));

    mapaVisita.put("autosProcedimiento", procedimiento.getNumeroAutos());
    mapaVisita.put("numeroJuzgado", procedimiento.getJuzgado());
    mapaVisita.put("ciudadJuzgado", procedimiento.getPlaza());
    mapaVisita.put(
      "provinciaJuzgado",
      procedimiento.getProvincia() != null ? procedimiento.getProvincia().getValor() : null);

    mapaVisita.put("nombreApoderado", formalizacion.getApoderadoFirma());
    mapaVisita.put("direccionNotaria", formalizacion.getDireccionNotaria());

    mapaVisita.put("nombresIntervinientesSeleccionados", nombresIntervinientes);
    mapaVisita.put("nombreTitulares", nombresTitulares);

    mapaVisita.put("sumatorioTasaciones", sumatorioTasaciones.toString());
    mapaVisita.put("sumatorioTasacionesCent", String.valueOf(sumatorioTasaciones * 100));
    mapaVisita.put("saldoImpagoContrato", saldoImpago.toString());
    mapaVisita.put("saldoImpagoContratoCent", String.valueOf(saldoImpago * 100));
    mapaVisita.put("importeEntrega", propuesta.getImporteTotal().toString());
    mapaVisita.put("importeEntregaCent", String.valueOf(propuesta.getImporteTotal() * 100));

    return mapaVisita;
  }

  private void replace2(XWPFParagraph p, Map<String, String> data) {
    String pText = p.getText(); // complete paragraph as string
    if (pText.contains("${")) { // if paragraph does not include our pattern, ignore
      TreeMap<Integer, XWPFRun> posRuns = getPosToRuns(p);
      Pattern pat = Pattern.compile("\\$\\{(.+?)\\}");
      Matcher m = pat.matcher(pText);
      while (m.find()) { // for all patterns in the paragraph
        String g = m.group(1); // extract key start and end pos
        int s = m.start(1);
        int e = m.end(1);
        String key = g;
        String x = data.get(key);
        if (x == null) x = "";
        SortedMap<Integer, XWPFRun> range =
          posRuns.subMap(s - 2, true, e + 1, true); // get runs which contain the pattern
        boolean found1 = false; // found $
        boolean found2 = false; // found {
        boolean found3 = false; // found }
        XWPFRun prevRun = null; // previous run handled in the loop
        XWPFRun found2Run = null; // run in which { was found
        int found2Pos = -1; // pos of { within above run
        for (XWPFRun r : range.values()) {
          if (r == prevRun) continue; // this run has already been handled
          if (found3) break; // done working on current key pattern
          prevRun = r;
          for (int k = 0; ; k++) { // iterate over texts of run r
            if (found3) break;
            String txt = null;
            try {
              txt =
                r.getText(
                  k); // note: should return null, but throws exception if the text does not
              // exist
            } catch (Exception ex) {

            }
            if (txt == null) break; // no more texts in the run, exit loop
            if (txt.contains("$") && !found1) { // found $, replace it with value from data map
              txt = txt.replaceFirst("\\$", x);
              found1 = true;
            }
            if (txt.contains("{") && !found2 && found1) {
              found2Run = r; // found { replace it with empty string and remember location
              found2Pos = txt.indexOf('{');
              txt = txt.replaceFirst("\\{", "");
              found2 = true;
            }
            if (found1 && found2 && !found3) { // find } and set all chars between { and } to blank
              if (txt.contains("}")) {
                if (r == found2Run) { // complete pattern was within a single run
                  txt = txt.substring(0, found2Pos) + txt.substring(txt.indexOf('}'));
                } else // pattern spread across multiple runs
                  txt = txt.substring(txt.indexOf('}'));
              } else if (r == found2Run) // same run as { but no }, remove all text starting at {
                txt = txt.substring(0, found2Pos);
              else txt = ""; // run between { and }, set text to blank
            }
            if (txt.contains("}") && !found3) {
              txt = txt.replaceFirst("\\}", "");
              found3 = true;
            }
            r.setText(txt, k);
          }
        }
      }
    }
  }

  private TreeMap<Integer, XWPFRun> getPosToRuns(XWPFParagraph paragraph) {
    int pos = 0;
    TreeMap<Integer, XWPFRun> map = new TreeMap<Integer, XWPFRun>();
    for (XWPFRun run : paragraph.getRuns()) {
      String runText = run.text();
      if (runText != null && runText.length() > 0) {
        for (int i = 0; i < runText.length(); i++) {
          map.put(pos + i, run);
        }
        pos += runText.length();
      }
    }
    return map;
  }

  private String formatDirection(Direccion d) {
    String result = ""; // nombre_via, numero, portal, bloque, escalera, piso, puerta
    result += d.getNombre() != null ? d.getNombre() + " " : " ";
    result += d.getNumero() != null ? d.getNumero() + " " : " ";
    result += d.getPortal() != null ? d.getPortal() + " " : " ";
    result += d.getBloque() != null ? d.getBloque() + " " : " ";
    result += d.getEscalera() != null ? d.getEscalera() + " " : " ";
    result += d.getPiso() != null ? d.getPiso() + " " : " ";
    result += d.getPuerta() != null ? d.getPuerta() : "";

    return result;
  }

  private String numerosFincas(List<BienMinutaDTO> bienesDTO) {
    List<Integer> idsBienes =
      bienesDTO.stream().map(BienMinutaDTO::getIdBien).collect(Collectors.toList());
    List<Bien> bienes = bienRepository.findAllById(idsBienes);
    String result = bienes.stream().map(Bien::getFinca).collect(Collectors.joining(", "));
    return result;
  }

  private List<Interviniente> findAllTitulares(List<Contrato> contratos) {
    List<ContratoInterviniente> all = new ArrayList<>();
    for (Contrato contrato : contratos) {
      List<ContratoInterviniente> anadir = new ArrayList<>(contrato.getIntervinientes());
      all.addAll(anadir);
    }
    all =
      all.stream()
        .distinct()
        .filter(ci -> ci.getTipoIntervencion().getCodigo().equals("1"))
        .collect(Collectors.toList());
    return all.stream().map(ContratoInterviniente::getInterviniente).collect(Collectors.toList());
  }

  private Double getSaldoInpago(List<Contrato> contratos) {
    List<Saldo> saldos =
      contratos.stream().map(Contrato::getUltimoSaldo).collect(Collectors.toList());
    List<Double> doubles =
      saldos.stream().map(Saldo::getDeudaImpagada).collect(Collectors.toList());
    Double result = 0.0;
    for (Double d : doubles) {
      result += d;
    }
    return result;
  }

  private Double getAllTasaciones(List<BienMinutaDTO> bienes) {
    Double result = 0.0;
    List<Tasacion> tasaciones = new ArrayList<>();
    for (BienMinutaDTO bm : bienes) {
      tasaciones.add(tasacionRepository.findById(bm.getIdTasacion().get(0)).orElse(null));
    }
    tasaciones =
      tasaciones.stream().distinct().filter(Objects::nonNull).collect(Collectors.toList());
    for (Tasacion t : tasaciones) {
      result += t.getImporte();
    }
    return result;
  }

  private void duplicarVendedora(XWPFDocument documento, List<Interviniente> intervinientes) {
    List<XWPFParagraph> duplicables = new ArrayList<>();
    XWPFParagraph inicio = null;
    boolean comenzando = false;
    for (XWPFParagraph p : documento.getParagraphs()) {
      String pText = p.getText(); // complete paragraph as string

      if (pText.contains("$${vendedora")) {
        comenzando = true;
        inicio = p;
      }
      if (comenzando) {
        duplicables.add(p);
      }
      if (comenzando && pText.contains("vendedora}$$")) {
        comenzando = false;
        break;
      }
    }
    if (inicio != null) {
      XmlCursor cursor = inicio.getCTP().newCursor();

      for (Interviniente in : intervinientes) {
        Map<String, String> mapeo = mapVendedor(in);
        for (XWPFParagraph copyP : duplicables) {
          cursor = inicio.getCTP().newCursor();
          XWPFParagraph newP = documento.insertNewParagraph(cursor);
          cloneParagraph(newP, copyP);
          replace2(newP, mapeo);
        }
      }
      for (XWPFParagraph p : duplicables) {
        documento.removeBodyElement(documento.getPosOfParagraph(p));
        ;
      }
    }
  }

  private Map<String, String> mapVendedor(Interviniente interviniente) {
    Direccion dir = interviniente.getDirecciones().stream().findFirst().orElse(null);

    Map<String, String> mapaVisita = new HashMap<>();

    mapaVisita.put(
      "nombreVendedor",
      (interviniente.getNombre() != null ? interviniente.getNombre() : "")
        + " "
        + (interviniente.getApellidos() != null ? interviniente.getApellidos() : ""));
    mapaVisita.put(
      "estadoCivilVendedor",
      interviniente.getEstadoCivil() != null ? interviniente.getEstadoCivil().getValor() : null);
    mapaVisita.put("direccionVendedor", dir != null ? formatDirection(dir) : null);
    mapaVisita.put("dniVendedor", interviniente.getNumeroDocumento());

    return mapaVisita;
  }

  private void duplicarFinca(
    XWPFDocument documento, List<BienMinutaDTO> bienes, List<Contrato> contratos)
    throws NotFoundException {
    List<XWPFParagraph> duplicables = new ArrayList<>();
    XWPFParagraph inicio = null;
    boolean comenzando = false;
    for (XWPFParagraph p : documento.getParagraphs()) {
      String pText = p.getText(); // complete paragraph as string

      if (pText.contains("$${finca")) {
        comenzando = true;
        inicio = p;
      }
      if (comenzando) {
        duplicables.add(p);
      }
      if (comenzando && pText.contains("finca}$$")) {
        comenzando = false;
        break;
      }
    }
    if (inicio != null) {
      XmlCursor cursor = inicio.getCTP().newCursor();
      Integer i = 0;
      for (BienMinutaDTO bi : bienes) {
        i++;
        Map<String, String> mapeo = mapFinca(bi, i, contratos);
        for (XWPFParagraph copyP : duplicables) {
          cursor = inicio.getCTP().newCursor();
          XWPFParagraph newP = documento.insertNewParagraph(cursor);
          cloneParagraph(newP, copyP);
          replace2(newP, mapeo);
        }
      }
      for (XWPFParagraph p : duplicables) {
        documento.removeBodyElement(documento.getPosOfParagraph(p));
        ;
      }
    }
  }

  private void duplicarCargas(XWPFDocument documento, List<Contrato> contratos)
    throws NotFoundException {
    List<XWPFParagraph> duplicables = new ArrayList<>();
    XWPFParagraph inicio = null;
    boolean comenzando = false;
    for (XWPFParagraph p : documento.getParagraphs()) {
      String pText = p.getText(); // complete paragraph as string

      if (pText.contains("$${cargas")) {
        comenzando = true;
        inicio = p;
      }
      if (comenzando) {
        duplicables.add(p);
      }
      if (comenzando && pText.contains("cargas}$$")) {
        comenzando = false;
        break;
      }
    }
    if (inicio != null) {
      XmlCursor cursor = inicio.getCTP().newCursor();
      Integer i = 0;
      for (Contrato bi : contratos) {
        i++;
        Map<String, String> mapeo = mapCargas(bi);
        for (XWPFParagraph copyP : duplicables) {
          cursor = inicio.getCTP().newCursor();
          XWPFParagraph newP = documento.insertNewParagraph(cursor);
          cloneParagraph(newP, copyP);
          replace2(newP, mapeo);
        }
      }
      for (XWPFParagraph p : duplicables) {
        documento.removeBodyElement(documento.getPosOfParagraph(p));
        ;
      }
    }
  }

  private void duplicarContratos(XWPFDocument documento, List<Contrato> contratos)
    throws NotFoundException {
    List<XWPFParagraph> duplicables = new ArrayList<>();
    XWPFParagraph inicio = null;
    boolean comenzando = false;
    for (XWPFParagraph p : documento.getParagraphs()) {
      String pText = p.getText(); // complete paragraph as string

      if (pText.contains("$${contratos")) {
        comenzando = true;
        inicio = p;
      }
      if (comenzando) {
        duplicables.add(p);
      }
      if (comenzando && pText.contains("contratos}$$")) {
        comenzando = false;
        break;
      }
    }
    if (inicio != null) {
      XmlCursor cursor = inicio.getCTP().newCursor();
      for (Contrato bi : contratos) {
        for (XWPFParagraph copyP : duplicables) {
          cursor = inicio.getCTP().newCursor();
          XWPFParagraph newP = documento.insertNewParagraph(cursor);
          cloneParagraph(newP, copyP);
        }
      }
      for (XWPFParagraph p : duplicables) {
        documento.removeBodyElement(documento.getPosOfParagraph(p));
        ;
      }
    }
  }

  private void duplicarCHfinca(XWPFDocument documento, List<BienMinutaDTO> contratos)
    throws NotFoundException {
    List<XWPFParagraph> duplicables = new ArrayList<>();
    XWPFParagraph inicio = null;
    boolean comenzando = false;
    for (XWPFParagraph p : documento.getParagraphs()) {
      String pText = p.getText(); // complete paragraph as string

      if (pText.contains("$${CHfinca")) {
        comenzando = true;
        inicio = p;
      }
      if (comenzando) {
        duplicables.add(p);
      }
      if (comenzando && pText.contains("CHfinca}$$")) {
        comenzando = false;
        break;
      }
    }
    if (inicio != null) {
      XmlCursor cursor = inicio.getCTP().newCursor();
      for (BienMinutaDTO bi : contratos) {
        for (XWPFParagraph copyP : duplicables) {
          cursor = inicio.getCTP().newCursor();
          XWPFParagraph newP = documento.insertNewParagraph(cursor);
          cloneParagraph(newP, copyP);
        }
      }
      for (XWPFParagraph p : duplicables) {
        documento.removeBodyElement(documento.getPosOfParagraph(p));
        ;
      }
    }
  }

  private Map<String, String> mapFinca(
    BienMinutaDTO bienMinutaDTO, Integer indice, List<Contrato> contratos)
    throws NotFoundException {
    Bien bien =
      bienRepository
        .findById(bienMinutaDTO.getIdBien())
        .orElseThrow(
          () ->
            new NotFoundException(
              "bien", bienMinutaDTO.getIdBien()));

    List<Interviniente> representantes = findAllTitulares(contratos);
    String nombresTitulares = "";
    for (Interviniente inte : representantes) {
      if (inte != null) {
        nombresTitulares += inte.getNombre() != null ? inte.getNombre() + " " : "";
        nombresTitulares += inte.getApellidos() != null ? inte.getApellidos() + " " : "";
        nombresTitulares += inte.getRazonSocial() != null ? inte.getRazonSocial() + "" : "";
        nombresTitulares += ", ";
      }
    }
    Expediente expediente = contratos.get(0).getExpediente();
    Contrato representante = expediente.getContratoRepresentante();
    Saldo saldo = representante.getUltimoSaldo();

    Map<String, String> mapaVisita = new HashMap<>();

    mapaVisita.put("indiceFinca", indice.toString());
    mapaVisita.put("numeroFinca", bien.getFinca());
    mapaVisita.put("nombreDuenyo", nombresTitulares);

    if (saldo.getDeudaImpagada() != null) {
      mapaVisita.put("principalInpagado", saldo.getDeudaImpagada().toString());
      mapaVisita.put(
        "principalInpagadoLetra", NumberConversor.parseDouble(saldo.getDeudaImpagada()));
    } else {
      mapaVisita.put("principalInpagado", "0.0");
      mapaVisita.put("principalInpagadoLetra", "0.0");
    }
    if (saldo.getInteresesOrdinariosImpagados() != null) {
      mapaVisita.put("interesesOrdinarios", saldo.getInteresesOrdinariosImpagados().toString());
      mapaVisita.put(
        "interesesOrdinariosLetra",
        NumberConversor.parseDouble(saldo.getInteresesOrdinariosImpagados()));
    } else {
      mapaVisita.put("interesesOrdinarios", "0.0");
      mapaVisita.put("interesesOrdinariosLetra", "0.0");
    }
    if (saldo.getInteresesDemora() != null) {
      mapaVisita.put("interesesDemora", saldo.getInteresesDemora().toString());
      mapaVisita.put(
        "interesesDemoraLetra", NumberConversor.parseDouble(saldo.getInteresesDemora()));
    } else {
      mapaVisita.put("interesesDemora", "0.0");
      mapaVisita.put("interesesDemoraLetra", "0.0");
    }
    Double cg;
    if (saldo.getGastosImpagados() == null || saldo.getComisionesImpagados() == null) {
      cg = 0.0;
    } else {
      cg = saldo.getComisionesImpagados() + saldo.getGastosImpagados();
    }
    mapaVisita.put("comisionesYGastos", String.valueOf(cg));
    mapaVisita.put("comisionesYGastosLetra", NumberConversor.parseDouble(cg));

    return mapaVisita;
  }

  private Map<String, String> mapCargas(Contrato contrato) throws NotFoundException {
    List<Bien> bienes =
      contrato.getBienes().stream().map(ContratoBien::getBien).collect(Collectors.toList());

    String fincas = bienes.stream().map(Bien::getFinca).collect(Collectors.joining(", "));
    Map<String, String> mapaVisita = new HashMap<>();

    mapaVisita.put("numeroFincaCont", fincas);
    if (contrato.getSaldoGestion() != null) {
      DecimalFormat df = new DecimalFormat("#");
      DecimalFormat df2 = new DecimalFormat("#");
      df2.setMaximumFractionDigits(2);
      mapaVisita.put("saldoContrato", df2.format(contrato.getSaldoGestion()));
      mapaVisita.put("saldoContratoCent", df.format(contrato.getSaldoGestion() * 100));
    } else {
      mapaVisita.put("saldoContrato", "0.0");
      mapaVisita.put("saldoContratoCent", "0");
    }

    return mapaVisita;
  }

  private void cloneParagraph(XWPFParagraph clone, XWPFParagraph source) {
    CTPPr pPr = clone.getCTP().isSetPPr() ? clone.getCTP().getPPr() : clone.getCTP().addNewPPr();
    pPr.set(source.getCTP().getPPr());
    for (XWPFRun r : source.getRuns()) {
      XWPFRun nr = clone.createRun();
      cloneRun(nr, r);
    }
  }

  private void cloneRun(XWPFRun clone, XWPFRun source) {
    CTRPr rPr = clone.getCTR().isSetRPr() ? clone.getCTR().getRPr() : clone.getCTR().addNewRPr();
    rPr.set(source.getCTR().getRPr());
    clone.setText(source.getText(0));
  }

  private void replaceDuplicators(XWPFDocument doc, XWPFParagraph p) {
    String[] duplicadores = {
      "$${vendedora",
      "vendedora}$$",
      "$${finca",
      "finca}$$",
      "$${contratos",
      "contratos}$$",
      "$${cargas",
      "cargas}$$",
      "$${CHfinca",
      "CHfinca}$$"
    };
    String pText = p.getText(); // complete paragraph as string

    if (pText.contains(duplicadores[0])) doc.removeBodyElement(doc.getPosOfParagraph(p));
    if (pText.contains(duplicadores[1])) doc.removeBodyElement(doc.getPosOfParagraph(p));
    if (pText.contains(duplicadores[2])) doc.removeBodyElement(doc.getPosOfParagraph(p));
    if (pText.contains(duplicadores[3])) doc.removeBodyElement(doc.getPosOfParagraph(p));
    if (pText.contains(duplicadores[4])) doc.removeBodyElement(doc.getPosOfParagraph(p));
    if (pText.contains(duplicadores[5])) doc.removeBodyElement(doc.getPosOfParagraph(p));
    if (pText.contains(duplicadores[6])) doc.removeBodyElement(doc.getPosOfParagraph(p));
    if (pText.contains(duplicadores[7])) doc.removeBodyElement(doc.getPosOfParagraph(p));

    if (pText.contains(duplicadores[8])) doc.removeBodyElement(doc.getPosOfParagraph(p));
    if (pText.contains(duplicadores[9])) doc.removeBodyElement(doc.getPosOfParagraph(p));
  }

  public Procedimiento getUltimoProcedimiento(Expediente ex) throws NotFoundException {
    Procedimiento result = null;

    Contrato contrato = ex.getContratoRepresentante();

    List<Procedimiento> activos =
      contrato.getProcedimientos().stream()
        .filter(pro -> pro.getEstado() != null && pro.getEstado().getCodigo().equals("1"))
        .collect(Collectors.toList());
    if (activos.size() == 1) result = activos.get(0);
    else if (activos.size() > 1) {
      result =
        activos.stream().max(Comparator.comparing(this::getFechaCreacion)).orElse(activos.get(0));
    } else {
      activos = new ArrayList<>();
      for (Contrato co : ex.getContratos()) {
        activos.addAll(
          co.getProcedimientos().stream()
            .filter(pro -> pro.getEstado() != null && pro.getEstado().getCodigo().equals("1"))
            .distinct()
            .collect(Collectors.toList()));
      }
      if (!activos.isEmpty())
        result = activos.stream().min(Comparator.comparing(this::getEstado)).orElse(activos.get(0));
    }

    if (result != null) {
      HitoProcedimiento h = procedimientoUtil.getHitoActual(result);
      result.setHitoProcedimiento(h);
    }

    return result;
  }

  public Procedimiento getUltimoProcedimiento(ExpedientePosesionNegociada ex) throws NotFoundException {
    Procedimiento result = null;

    Contrato contrato = ex.getContrato();
    if (contrato == null) return null;
    List<Procedimiento> activos =
      contrato.getProcedimientos().stream()
        .filter(pro -> pro.getEstado() != null && pro.getEstado().getCodigo().equals("1"))
        .collect(Collectors.toList());
    if (activos.size() == 1) result = activos.get(0);
    else if (activos.size() > 1) {
      result =
        activos.stream().max(Comparator.comparing(this::getFechaCreacion)).orElse(activos.get(0));
    } else {
      activos = new ArrayList<>();
      Contrato co = ex.getContrato();
      activos.addAll(
        co.getProcedimientos().stream()
          .filter(pro -> pro.getEstado() != null && pro.getEstado().getCodigo().equals("1"))
          .distinct()
          .collect(Collectors.toList()));

      if (!activos.isEmpty())
        result = activos.stream().min(Comparator.comparing(this::getEstado)).orElse(activos.get(0));
    }

    if (result != null) {
      HitoProcedimiento h = procedimientoUtil.getHitoActual(result);
      result.setHitoProcedimiento(h);
    }

    return result;
  }

  public Timestamp getFechaCreacion(Procedimiento p) {
    BigInteger timestamp = procedimientoRepository.historicoProcedimiento(p.getId());
    Timestamp valueTimestamp = new Timestamp(Long.parseLong(String.valueOf(timestamp)));
    return valueTimestamp;
  }

  public Integer getEstado(Procedimiento p) {
    String hito = p.getHito();
    Integer result;
    try {
      result = Integer.parseInt(hito);
    } catch (Exception e) {
      result = 200;
    }

    return result;
  }

  public Boolean comprobarEntre(Date fechaComprobar, Date fechaInicial, Date fechaFinal) {
    fechaComprobar = simplificar(fechaComprobar);
    fechaInicial = simplificar(fechaInicial);
    fechaFinal = simplificar(fechaFinal);

    if (fechaComprobar != null) {
      if (fechaInicial != null && fechaFinal != null) {
        if (fechaInicial.compareTo(fechaComprobar) * fechaComprobar.compareTo(fechaFinal) >= 0) return true;
        return false;
      } else {
        if (fechaInicial != null) {
          return fechaInicial.before(fechaComprobar) || fechaInicial.compareTo(fechaComprobar) == 0;
        }
        if (fechaFinal != null) {
          return fechaFinal.after(fechaComprobar) || fechaFinal.compareTo(fechaComprobar) == 0;
        }
      }
    } else if (fechaInicial != null || fechaFinal != null) return false;
    return true;
  }

  private Date simplificar(Date date) {
    if (date == null) return null;
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    c.set(Calendar.HOUR, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTime();
  }

  public Integer nBienes(Expediente e){
    Integer i = 0;
    for (Contrato c : e.getContratos()){
      i += c.getBienes().size();
    }
    return i;
  }
  public Integer nIntervinientes(Expediente e){
    Integer i = 0;
    for (Contrato c : e.getContratos()){
      i += c.getIntervinientes().size();
    }
    return i;
  }
}
