package com.haya.alaska.expediente.infrastructure.controller.dto;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.HashSet;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExpedienteCSVInputDTO {

  @CsvBindByName
  private String id;
}
