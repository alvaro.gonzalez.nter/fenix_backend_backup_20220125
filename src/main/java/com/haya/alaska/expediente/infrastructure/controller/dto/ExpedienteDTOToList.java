package com.haya.alaska.expediente.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ExpedienteDTOToList {

    Integer id;
    String idConcatenado;
    Integer idCartera;
    CatalogoMinInfoDTO estado;
    CatalogoMinInfoDTO estadoPropuesta;
    String cliente;
    String cartera;
    String gestor;
    String primerTitular;
    CatalogoMinInfoDTO estrategia;
    Double saldoGestion;
    String supervisor;
    List<String> areaGestion;
    String gestor_suplente;
    String modoDeGestion;
    String situacion;

}
