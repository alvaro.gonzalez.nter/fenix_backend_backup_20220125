package com.haya.alaska.expediente.infrastructure.controller.dto;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ReasignacionGestorInputDTO {

  BusquedaDto busquedaDto;
  List<Integer> expedienteIdList;
  List<Integer> expedientePNIdList;
}
