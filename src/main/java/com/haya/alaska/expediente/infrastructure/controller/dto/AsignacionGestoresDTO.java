package com.haya.alaska.expediente.infrastructure.controller.dto;

import java.util.Date;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class AsignacionGestoresDTO {

  private Date fechaAsignacion;
  private Date fechaFinAsignacion;
  private String grupo;
  private String areaGestion;
  private String gestorExpediente;
  private String usuarioAsignaciones;
  private CatalogoMinInfoDTO estrategia;
  private String supervisor;
}
