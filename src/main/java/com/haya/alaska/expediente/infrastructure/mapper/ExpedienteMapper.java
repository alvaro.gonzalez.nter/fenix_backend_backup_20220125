package com.haya.alaska.expediente.infrastructure.mapper;

import com.haya.alaska.actividad_delegada.infrastructure.controller.dto.ExpedienteActividadDelegadaOutputDTO;
import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.campania_asignada.domain.CampaniaAsignada;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.controller.dto.ContratoBusquedaDto;
import com.haya.alaska.contrato.infrastructure.controller.dto.EstrategiaAsignadaOutputDTO;
import com.haya.alaska.contrato.infrastructure.mapper.ContratoMapper;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estrategia.domain.Estrategia;
import com.haya.alaska.estrategia.infrastructure.repository.EstrategiaRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.controller.dto.AsignacionGestoresDTO;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTO;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteDTOToList;
import com.haya.alaska.expediente.infrastructure.controller.dto.ExpedienteSegmentacionDTO;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.grupo_cliente.domain.GrupoCliente;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.segmentacion.infraestructure.util.SegmentacionUtil;
import com.haya.alaska.shared.dto.BienExtendedDTOToList;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.subtipo_gestor.domain.SubtipoGestor;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ExpedienteMapper {

  @Autowired ContratoMapper contratoMapper;
  @Autowired ContratoRepository contratoRepository;
  @Autowired CarteraRepository carteraRepository;
  @Autowired UsuarioRepository usuarioRepository;
  @Autowired EstrategiaRepository estrategiaRepository;
  @Autowired ExpedienteUtil expedienteUtil;
  // @Autowired ContratoMapper contratoMapper;
  @Autowired SegmentacionUtil segmentacionUtil;

  public ExpedienteDTO parse(Expediente expediente) throws NotFoundException {
    ExpedienteDTO expedienteDTO = new ExpedienteDTO();
    Contrato contratoRepresentante = expediente.getContratoRepresentante();
    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");
    Usuario gestorSkipTracing = expediente.getUsuarioByPerfil("Gestor skip tracing");
    Usuario gestorFormalizacion = expediente.getUsuarioByPerfil("Gestor formalización");
    // Usuario gestorSoporte = expediente.getUsuarioByPerfil("Soporte");
    SubtipoGestor subtipoGestor = null;
    if (gestor != null) {
      subtipoGestor = gestor.getSubtipoGestor();
    }
    Usuario responsableCartera = expediente.getUsuarioByPerfil("Responsable de cartera");
    Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");
    expedienteDTO.setId(expediente.getId());
    expedienteDTO.setIdConcatenado(expediente.getIdConcatenado());
    expedienteDTO.setExpedienteCliente(expediente.getIdCarga());
    expedienteDTO.setEstado(new CatalogoMinInfoDTO(expedienteUtil.getEstado(expediente)));
    EstadoPropuesta ep = expedienteUtil.getEstadoPropuesta(expediente);

    expedienteDTO.setEstadoPropuesta(ep != null ? new CatalogoMinInfoDTO(ep) : null);
    Propuesta pr = expedienteUtil.getPropuesta(expediente);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      if (pr != null && pr.getTipoPropuesta() != null)
        expedienteDTO.setPropuesta(pr.getTipoPropuesta().getValorIngles());
      expedienteDTO.setModoDeGestionEstado(
        expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValorIngles() : null);
      expedienteDTO.setModoDeGestionSubEstado(
        expediente.getSubEstado() != null ? expediente.getSubEstado().getValorIngles() : null);
      expedienteDTO.setSituacion(
        expediente.getContratoRepresentante().getSituacion() != null
          ? expediente.getContratoRepresentante().getSituacion().getValorIngles()
          : null);

    }

    else{
    if (pr != null && pr.getTipoPropuesta() != null)
      expedienteDTO.setPropuesta(pr.getTipoPropuesta().getValor());
    expedienteDTO.setModoDeGestionEstado(
        expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
    expedienteDTO.setModoDeGestionSubEstado(
        expediente.getSubEstado() != null ? expediente.getSubEstado().getValor() : null);
    expedienteDTO.setSituacion(
        expediente.getContratoRepresentante().getSituacion() != null
            ? expediente.getContratoRepresentante().getSituacion().getValor()
            : null);
    }

    expedienteDTO.setCliente(
        expediente.getCartera() != null
            ? expediente.getCartera().getClientes().stream()
                .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                .collect(Collectors.joining(", "))
            : null);
    List<GrupoCliente> griposClientes =
        expediente.getCartera().getClientes().stream()
            .map(cc -> cc.getCliente().getGrupo())
            .collect(Collectors.toList());
    String grupos =
        griposClientes.stream()
            .filter(Objects::nonNull)
            .distinct()
            .map(GrupoCliente::getNombre)
            .collect(Collectors.joining(", "));
    expedienteDTO.setGrupoCliente(grupos);

    if (expediente.getCartera() != null) {
      if (expediente.getCartera().getClientes() != null) {
        Set<ClienteCartera> clientes = expediente.getCartera().getClientes();
        List<Integer> ids = new ArrayList<>();
        for (ClienteCartera cc : clientes) {
          Cliente cli = cc.getCliente();
          ids.add(cli.getId());
        }
        expedienteDTO.setIdsCliente(ids);
      }
    }

    Procedimiento procedimiento = expedienteUtil.getUltimoProcedimiento(expediente);
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      if (procedimiento != null) {
        expedienteDTO.setJudicializado("YES");
        String hitoP = "";
        TipoProcedimiento tipoP = null;
        if (procedimiento.getHitoProcedimiento() != null) {
          hitoP = procedimiento.getHitoProcedimiento().getValor();
          expedienteDTO.setFaseProcedimiento(hitoP);
        }

        tipoP = procedimiento.getTipo();
        expedienteDTO.setTipoProcedimiento(tipoP != null ? new CatalogoMinInfoDTO(tipoP) : null);
      } else {
        expedienteDTO.setJudicializado("NO");
      }
    } else {

      if (procedimiento != null) {
        expedienteDTO.setJudicializado("SI");
        String hitoP = "";
        TipoProcedimiento tipoP = null;
        if (procedimiento.getHitoProcedimiento() != null) {
          hitoP = procedimiento.getHitoProcedimiento().getValor();
          expedienteDTO.setFaseProcedimiento(hitoP);
        }

        tipoP = procedimiento.getTipo();
        expedienteDTO.setTipoProcedimiento(tipoP != null ? new CatalogoMinInfoDTO(tipoP) : null);
      } else {
        expedienteDTO.setJudicializado("NO");
      }
    }

    expedienteDTO.setIdOrigen(expediente.getIdOrigen());
    expedienteDTO.setCartera(
        expediente.getCartera() != null ? expediente.getCartera().getNombre() : null);
    expedienteDTO.setCarteraId(
        expediente.getCartera() != null ? expediente.getCartera().getId() : null);
    // sacar el plazo de cartera y ver si existe o no
    Cartera c = carteraRepository.findById(expediente.getCartera().getId()).orElse(null);
    String pl = null;
    if (c.getPlazo() != null) {
      pl = c.getPlazo().getValor();
    }

    expedienteDTO.setFormalizacion(c.getFormalizacion());
    // SI LA CARTERA NO TIENE NUMERO DE DIAS, devolver null
    if (pl == null || pl.equals("")) {
      expedienteDTO.setCarteraPlazo(null);
      // si sí tiene numero de días, devolver ese numero
    } else if (pl != null && !pl.equals("")) {
      expedienteDTO.setCarteraPlazo(pl);
    }
    expedienteDTO.setContratoRepresentanteId(contratoRepresentante.getId());
    expedienteDTO.setContratoRepresentanteIdOrigen(contratoRepresentante.getIdCarga());
    expedienteDTO.setGestor(gestor != null ? gestor.getNombre() : null);

    if (subtipoGestor != null) {
      Locale loc = LocaleContextHolder.getLocale();
      String defaultLocal = loc.getLanguage();
      if (defaultLocal == null || defaultLocal.equals("es")) {
        expedienteDTO.setSubtipoGestor(subtipoGestor.getValor());
      } else if (defaultLocal.equals("en")) {
        expedienteDTO.setSubtipoGestor(subtipoGestor.getValorIngles());
      }
    } else {
      expedienteDTO.setSubtipoGestor(null);
    }

    expedienteDTO.setGestorSoporte(expediente.getUsuariosByPerfil("Soporte"));
    expedienteDTO.setGestorSkipTracing(
        gestorSkipTracing != null ? gestorSkipTracing.getNombre() : null);
    expedienteDTO.setGestorFormalizacion(
        gestorFormalizacion != null ? gestorFormalizacion.getNombre() : null);
    expedienteDTO.setResponsableCartera(
        responsableCartera != null ? responsableCartera.getNombre() : null);
    expedienteDTO.setSupervisor(supervisor != null ? supervisor.getNombre() : null);
    expedienteDTO.setAreaGestion(
        gestor != null && gestor.getAreas() != null
            ? gestor.getAreas().stream().map(AreaUsuario::getNombre).collect(Collectors.toList())
            : null);
    var interviniente = contratoRepresentante.getPrimerInterviniente();
    if (interviniente != null && Boolean.TRUE.equals(interviniente.getPersonaJuridica())) {
      expedienteDTO.setTitular(interviniente.getRazonSocial());
    } else {
      expedienteDTO.setTitular(interviniente.getNombre() + " " + interviniente.getApellidos());
    }
    expedienteDTO.setTitularNif(
        contratoRepresentante.getPrimerInterviniente().getNumeroDocumento());
    Contrato contrato = expediente.getContratoRepresentante();
    expedienteDTO.setEstrategiaAsignada(
        contrato.getEstrategia() != null
            ? new EstrategiaAsignadaOutputDTO(contrato.getEstrategia())
            : null);
    expedienteDTO.setEstimaciones(expediente.getEstimaciones().size());
    expedienteDTO.setSaldo(expediente.getSaldoGestion());
    expedienteDTO.setPrincipalPendiente(expediente.getPrincipalPendiente());
    expedienteDTO.setDeudaImpagada(
        expediente.getContratoRepresentante() != null
            ? expediente.getContratoRepresentante().getDeudaImpagada()
            : null);
    expedienteDTO.setImporteRecuperado(expediente.getImporteRecuperado());
    expedienteDTO.setContratos(
        contratoMapper.listContratoToListContratoDTOToList(expediente.getContratos()));
    expedienteDTO.setFechaContratoPrincipal(contratoRepresentante.getFechaPrimerImpago());

    // Cogemos la Campaña asignada del Contrato Representante
    String campaniaRepresentante = null;
    Integer idCamcampaniaRepresentante = null;

    if (contratoRepresentante.getCampaniasAsignadas() != null
        && contratoRepresentante.getCampaniasAsignadas().size() > 0) {
      var campaniaAsignada =
          contratoRepresentante.getCampaniasAsignadas().stream()
              .sorted(Comparator.comparing(CampaniaAsignada::getFechaAsignacion).reversed())
              .findFirst()
              .orElse(null);
      if (campaniaAsignada != null) {
        campaniaRepresentante = campaniaAsignada.getCampania().getNombre();
        idCamcampaniaRepresentante = campaniaAsignada.getCampania().getId();
      }
    }

    expedienteDTO.setCampania(campaniaRepresentante);
    expedienteDTO.setIdCampania(idCamcampaniaRepresentante);
    expedienteDTO.setRecovery(expediente.getCartera().getDatosJudicial());
    return expedienteDTO;
  }

  public ExpedienteSegmentacionDTO parseSegmentacion(Expediente expediente) {
    ExpedienteSegmentacionDTO expedienteSegmentacionDTO = new ExpedienteSegmentacionDTO();
    BeanUtils.copyProperties(expediente, expedienteSegmentacionDTO);

    Contrato contrato = expediente.getContratoRepresentante();
    Interviniente interviniente = contrato.getPrimerInterviniente();
    Bien bien =
        contrato.getBienes() != null && contrato.getBienes().size() > 0
            ? segmentacionUtil.sacarBienPrincipal(contrato)
            : null;
    Procedimiento procedimiento = segmentacionUtil.sacarProcedimientoVivo(contrato);
    boolean cargasPosteriores = false;
    for (ContratoBien bienProv : contrato.getBienes()) {
      if (bienProv != null
          && bienProv.getBien().getCargas() != null
          && bienProv.getBien().getCargas().size() > 0) {
        cargasPosteriores = true;
        break;
      }
    }
    Direccion direccionObj =
        interviniente != null && interviniente.getDirecciones().stream().findFirst() != null
            ? interviniente.getDirecciones().stream().findFirst().orElse(null)
            : null;
    String direccion = "";
    if (direccionObj != null) {
      direccion +=
          direccionObj.getTipoVia() != null ? direccionObj.getTipoVia().getValor() + " " : "";
      direccion += direccionObj.getNombre() != null ? direccionObj.getNombre() + " " : "";
      direccion += direccionObj.getNumero() != null ? direccionObj.getNumero() + " " : "";
      direccion += direccionObj.getBloque() != null ? direccionObj.getBloque() + " " : "";
      direccion += direccionObj.getEscalera() != null ? direccionObj.getEscalera() + " " : "";
      direccion += direccionObj.getPiso() != null ? direccionObj.getPiso() + " " : "";
      direccion += direccionObj.getPuerta() != null ? direccionObj.getPuerta() : "";
    }

    expedienteSegmentacionDTO.setIdConcatenado(expediente.getIdConcatenado());
    expedienteSegmentacionDTO.setDireccionPrimerTitular(direccion);
    expedienteSegmentacionDTO.setTipoInterviniente(
        interviniente != null
                && interviniente.getPersonaJuridica() != null
                && interviniente.getPersonaJuridica() == true
            ? "Jurídica"
            : "Física");
    expedienteSegmentacionDTO.setGarantia(
        bien != null && bien.getTipoActivo() != null && bien.getTipoActivo().getCodigo().equals("1")
            ? "Inmobiliario"
            : "Mobiliario");
    expedienteSegmentacionDTO.setTipoProducto(
        contrato != null && contrato.getProducto() != null
            ? contrato.getProducto().getValor()
            : null);
    expedienteSegmentacionDTO.setValorGarantia(
        segmentacionUtil.calcularImporteGarantizado(expediente));
    expedienteSegmentacionDTO.setProvinciaPrimeraGarantia(
        bien != null && bien.getProvincia() != null ? bien.getProvincia().getValor() : null);
    expedienteSegmentacionDTO.setLocalidadPrimeraGarantia(
        bien != null && bien.getLocalidad() != null ? bien.getLocalidad().getValor() : null);
    expedienteSegmentacionDTO.setTipoProcedimiento(
        procedimiento != null && procedimiento.getTipo() != null
            ? procedimiento.getTipo().getValor()
            : null);
    expedienteSegmentacionDTO.setHitoJudicial(
        procedimiento != null && procedimiento.getHitoProcedimiento() != null
            ? procedimiento.getHitoProcedimiento().getValor()
            : null);
    expedienteSegmentacionDTO.setSituacionContable(
        contrato != null && contrato.getClasificacionContable() != null
            ? contrato.getClasificacionContable().getValor()
            : null);
    expedienteSegmentacionDTO.setCargasPosteriores(cargasPosteriores);
    expedienteSegmentacionDTO.setEstrategia(
        contrato != null
                && contrato.getEstrategia() != null
                && contrato.getEstrategia() != null
                && contrato.getEstrategia().getEstrategia() != null
            ? contrato.getEstrategia().getEstrategia().getValor()
            : null);
    // TODO de donde saco este campo?
    expedienteSegmentacionDTO.setRankingAgencia(
        null); // ranking de la agencia para el reparto de expedientes

    return expedienteSegmentacionDTO;
  }

  public ExpedienteDTOToList parseDTOToList(Expediente expediente) {
    ExpedienteDTOToList result = new ExpedienteDTOToList();
    Contrato contratoRepresentante = expediente.getContratoRepresentante();
    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");
    result.setId(expediente.getId());
    result.setIdConcatenado(expediente.getIdConcatenado());
    result.setEstado(new CatalogoMinInfoDTO(expedienteUtil.getEstado(expediente)));
    EstadoPropuesta ep = expedienteUtil.getEstadoPropuesta(expediente);
    result.setEstadoPropuesta(ep != null ? new CatalogoMinInfoDTO(ep) : null);
    result.setCliente(
        expediente.getCartera().getClientes().stream()
            .map(clienteCartera -> clienteCartera.getCliente().getNombre())
            .collect(Collectors.joining(", ")));
    result.setCartera(expediente.getCartera().getNombre());
    result.setIdCartera(expediente.getCartera().getId());
    result.setGestor(gestor != null ? gestor.getNombre() : null);
    Interviniente primerInterviniente = contratoRepresentante.getPrimerInterviniente();
    if (primerInterviniente != null) {
      if (primerInterviniente.getPersonaJuridica() != null
          && primerInterviniente.getPersonaJuridica()) {
        String raz =
            primerInterviniente.getRazonSocial() != null
                ? primerInterviniente.getRazonSocial()
                : "";
        result.setPrimerTitular(raz);
      } else {
        String nom = primerInterviniente.getNombre() != null ? primerInterviniente.getNombre() : "";
        String ape =
            primerInterviniente.getApellidos() != null ? primerInterviniente.getApellidos() : "";
        result.setPrimerTitular(nom + " " + ape);
      }
    }
    Contrato contrato = expediente.getContratoRepresentante();
    result.setEstrategia(
        contrato.getEstrategia() != null && contrato.getEstrategia().getEstrategia() != null
            ? new CatalogoMinInfoDTO(contrato.getEstrategia().getEstrategia())
            : null);
    result.setSaldoGestion(expediente.getSaldoGestion());

    // result.setExpediente(parse(expediente));
    Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");
    String suplente = null;
    var listaAsignaciones = expediente.getAsignaciones();
    for (var asignacion : listaAsignaciones) {
      suplente =
          asignacion.getSuplente().stream()
              .map(Usuario::getNombre)
              .collect(Collectors.joining(","));
      if (suplente != null && suplente != "") break;
    }
    result.setGestor_suplente(suplente);
    result.setSupervisor(supervisor != null ? supervisor.getNombre() : null);
    result.setAreaGestion(
        gestor != null && gestor.getAreas() != null
            ? gestor.getAreas().stream().map(AreaUsuario::getNombre).collect(Collectors.toList())
            : null);
    String modoGestionEstado =
        expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null;
    String modoGestionSubEstado =
        expediente.getSubEstado() != null ? expediente.getSubEstado().getValor() : null;
    result.setModoDeGestion(modoGestionEstado + "/" + modoGestionSubEstado);
    result.setSituacion(
        expediente.getContratoRepresentante().getSituacion() != null
            ? expediente.getContratoRepresentante().getSituacion().getValor()
            : null);

    return result;
  }

  public ExpedienteActividadDelegadaOutputDTO parseActivDeleg(Expediente expediente) {
    ExpedienteActividadDelegadaOutputDTO result = new ExpedienteActividadDelegadaOutputDTO();
    Contrato contratoRepresentante = expediente.getContratoRepresentante();
    result.setId(expediente.getId());
    result.setIdConcatenado(expediente.getIdConcatenado());
    result.setEstado(new CatalogoMinInfoDTO(expedienteUtil.getEstado(expediente)));
    result.setCliente(
        expediente.getCartera().getClientes().stream()
            .map(clienteCartera -> clienteCartera.getCliente().getNombre())
            .collect(Collectors.joining(", ")));
    result.setCartera(expediente.getCartera().getNombre());
    Interviniente primerInterviniente = contratoRepresentante.getPrimerInterviniente();

    result.setTelefon1erTitular(false);
    result.setTelefonRestoIntervinientes(false);
    result.setDireccion1erTitular(false);
    result.setDireccionRestoIntervinientes(false);

    Integer nContratos = expediente.getContratos().size();
    Integer nIntervinientes = 0;
    Integer nBienes = 0;

    for (Contrato c : expediente.getContratos()) {
      nIntervinientes += c.getIntervinientes().size();
      nBienes += c.getBienes().size();
      if (c.getId().equals(contratoRepresentante.getId())) continue;
      for (ContratoInterviniente ci : c.getIntervinientes()) {
        Interviniente i = ci.getInterviniente();
        for (DatoContacto dc : i.getDatosContacto()) {
          if (dc.getMovil() != null || dc.getFijo() != null) {
            result.setTelefonRestoIntervinientes(true);
            break;
          }
        }
        if (!i.getDirecciones().isEmpty()) result.setDireccionRestoIntervinientes(true);
        if (result.getTelefonRestoIntervinientes() && result.getDireccionRestoIntervinientes())
          break;
      }
      if (result.getTelefonRestoIntervinientes() && result.getDireccionRestoIntervinientes()) break;
    }

    result.setNBienes(nBienes);
    result.setNContratos(nContratos);
    result.setNIntervinientes(nIntervinientes);

    if (primerInterviniente != null) {
      for (DatoContacto dc : primerInterviniente.getDatosContacto()) {
        if (dc.getMovil() != null || dc.getFijo() != null) {
          result.setTelefon1erTitular(true);
          break;
        }
      }
      if (!primerInterviniente.getDirecciones().isEmpty()) result.setDireccion1erTitular(true);
      if (primerInterviniente.getPersonaJuridica() != null
          && primerInterviniente.getPersonaJuridica()) {
        String raz =
            primerInterviniente.getRazonSocial() != null
                ? primerInterviniente.getRazonSocial()
                : "";
        result.setPrimerTitular(raz);
      } else {
        String nom = primerInterviniente.getNombre() != null ? primerInterviniente.getNombre() : "";
        String ape =
            primerInterviniente.getApellidos() != null ? primerInterviniente.getApellidos() : "";
        result.setPrimerTitular(nom + " " + ape);
      }
    }
    Contrato contrato = expediente.getContratoRepresentante();
    result.setEstrategia(
        contrato.getEstrategia() != null
            ? new CatalogoMinInfoDTO(contrato.getEstrategia().getEstrategia())
            : null);
    result.setSaldoGestion(expediente.getSaldoGestion());

    return result;
  }

  public AsignacionGestoresDTO parseMap(Map map, Expediente expediente) throws NotFoundException {
    AsignacionGestoresDTO asignacionGestoresDTO = new AsignacionGestoresDTO();

    Object timestamp = map.get("fechaAsignacion");
    Timestamp valueTimestamp = new Timestamp(Long.parseLong(String.valueOf(timestamp)));
    asignacionGestoresDTO.setFechaAsignacion(new Date(valueTimestamp.getTime()));

    var grupos = expediente.getGestor() != null ? expediente.getGestor().getGrupoUsuarios() : null;
    String concat = null;
    if (grupos != null)
      concat = grupos.stream().map(grupo -> grupo.getNombre()).collect(Collectors.joining(", "));
    asignacionGestoresDTO.setGrupo(concat);

    Integer gestorExpediente = Integer.parseInt(map.get("gestorExpediente").toString());
    Usuario user =
        usuarioRepository
            .findById(gestorExpediente)
            .orElseThrow(() -> new NotFoundException("usuario", gestorExpediente));
    asignacionGestoresDTO.setGestorExpediente(user.getNombre());

    String areas =
        user.getAreas().stream().map(AreaUsuario::getNombre).collect(Collectors.joining(", "));
    asignacionGestoresDTO.setAreaGestion(areas);

    Object usuarioAsignaciones = map.get("usuarioAsignaciones");
    asignacionGestoresDTO.setUsuarioAsignaciones(
        usuarioAsignaciones != null ? String.valueOf(usuarioAsignaciones) : null);

    Estrategia es;

    List<Map> estrategias =
        contratoRepository.findAllForEstrategiaLess(expediente.getId(), valueTimestamp);
    if (estrategias.isEmpty()) {
      estrategias = contratoRepository.findAllForEstrategiaBigg(expediente.getId(), valueTimestamp);
      if (estrategias.isEmpty()) {
        Contrato contrato = expediente.getContratoRepresentante();
        asignacionGestoresDTO.setEstrategia(
            contrato.getEstrategia() != null
                ? new CatalogoMinInfoDTO(contrato.getEstrategia().getEstrategia())
                : null);
      } else {
        Map dato = estrategias.get(0);
        if (dato.get("idEstrategia") != null) {
          es = estrategiaRepository.findById((Integer) dato.get("idEstrategia")).orElse(null);
          asignacionGestoresDTO.setEstrategia(es != null ? new CatalogoMinInfoDTO(es) : null);
        }
      }
    } else {
      Map dato = estrategias.get(0);
      if (dato.get("idEstrategia") != null) {
        es = estrategiaRepository.findById((Integer) dato.get("idEstrategia")).orElse(null);
        asignacionGestoresDTO.setEstrategia(es != null ? new CatalogoMinInfoDTO(es) : null);
      }
    }

    Usuario supervisor = expediente.getUsuarioByPerfil(Perfil.SUPERVISOR);
    asignacionGestoresDTO.setSupervisor(supervisor != null ? supervisor.getNombre() : "");

    return asignacionGestoresDTO;
  }

  public List<ContratoBusquedaDto> getContratoDTO(Expediente expediente) {
    List<ContratoBusquedaDto> resultList = new ArrayList<>();
    for (var contrato : expediente.getContratos()) {
      ContratoBusquedaDto contratoBusquedaDto = new ContratoBusquedaDto();
      contratoBusquedaDto.setId(contrato.getId());
      contratoBusquedaDto.setCartera(
          expediente.getCartera() != null ? expediente.getCartera().getNombre() : null);
      contratoBusquedaDto.setIdOrigen(contrato.getIdCarga());
      contratoBusquedaDto.setActivo(
          expediente.getCartera() != null ? expediente.getCartera().getTipoActivos() : null);
      contratoBusquedaDto.setOrigenEstrategia(
          contrato.getEstrategia() != null
              ? contrato.getEstrategia().getOrigenEstrategia() != null
                  ? contrato.getEstrategia().getOrigenEstrategia().getValor()
                  : null
              : null);
      contratoBusquedaDto.setFecha(
          contrato.getFechaCarga() != null ? contrato.getFechaCarga().toString() : null);
      contratoBusquedaDto.setPeriodo(
          contrato.getPeriodicidadLiquidacion() != null
              ? contrato.getPeriodicidadLiquidacion().getValor()
              : null);
      contratoBusquedaDto.setTipo(
          contrato.getTipoReestructuracion() != null
              ? contrato.getTipoReestructuracion().getValor()
              : null);
      contratoBusquedaDto.setEstrategia(
          contrato.getEstrategia() != null
              ? contrato.getEstrategia().getTipoEstrategia() != null
                  ? contrato.getEstrategia().getTipoEstrategia().getValor()
                  : null
              : null);
      resultList.add(contratoBusquedaDto);
    }
    return resultList;
  }

  public List<BienExtendedDTOToList> getBienPNDTO(ExpedientePosesionNegociada expediente) {
    List<BienExtendedDTOToList> resultList = new ArrayList<>();
    var bienesPN = expediente.getBienesPosesionNegociada();
    for (var bienPN : bienesPN) {
      BienExtendedDTOToList dtoToList = new BienExtendedDTOToList(bienPN, bienPN.getBien());
      resultList.add(dtoToList);
    }
    return resultList;
  }

  public List<BienExtendedDTOToList> getBienDTO(Expediente expediente) {
    List<BienExtendedDTOToList> resultList = new ArrayList<>();
    for (Contrato contrato: expediente.getContratos()         ) {
      for (ContratoBien bien : contrato.getBienes()) {
        BienExtendedDTOToList dtoToList = new BienExtendedDTOToList(bien.getBien(),contrato.getId());
        resultList.add(dtoToList);
      }
    }
    return resultList;
  }

  public List<AsignacionGestoresDTO> generarFechaFinAsignacion(List<AsignacionGestoresDTO> lista) {
    lista.sort(
        (AsignacionGestoresDTO a1, AsignacionGestoresDTO a2) ->
            ObjectUtils.compare(a1.getFechaAsignacion(), (a2.getFechaAsignacion())));
    for (int i = 0; i < lista.size() - 1; i++) {
      lista.get(i).setFechaFinAsignacion(lista.get(i + 1).getFechaAsignacion());
    }
    return lista;
  }
}
