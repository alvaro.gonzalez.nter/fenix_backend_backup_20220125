package com.haya.alaska.expediente.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ExpedienteExcelExportDTO {

  Boolean expedientes;
  Boolean contratos;
  Boolean garantias;
  Boolean solvencias;
  Boolean tasaciones;
  Boolean valoraciones;
  Boolean cargas;
  Boolean intervinientes;
  Boolean datosLocalizacion;
  Boolean datosDeContacto;
  Boolean judicial;
  Boolean contratosJudiciales;
  Boolean demandadosJudiciales;
  Boolean procedimientosETJN;
  Boolean procedimientosETJ;
  Boolean procedimientosHipotecario;
  Boolean procedimientosEjecucionNotarial;
  Boolean procedimientosMonitorio;
  Boolean procedimientosVerbal;
  Boolean procedimientosOrdinario;
  Boolean procedimientosConcursal;
  Boolean subastas;

  public ExpedienteExcelExportDTO(Boolean b){
    this.expedientes = b;
    this.contratos = b;
    this.garantias = b;
    this.solvencias = b;
    this.tasaciones = b;
    this.valoraciones = b;
    this.cargas = b;
    this.intervinientes = b;
    this.datosLocalizacion = b;
    this.datosDeContacto = b;
    this.judicial = b;
    this.contratosJudiciales = b;
    this.demandadosJudiciales = b;
    this.procedimientosETJN = b;
    this.procedimientosETJ = b;
    this.procedimientosHipotecario = b;
    this.procedimientosEjecucionNotarial = b;
    this.procedimientosMonitorio = b;
    this.procedimientosVerbal = b;
    this.procedimientosOrdinario = b;
    this.procedimientosConcursal = b;
    this.subastas = b;
  }
}
