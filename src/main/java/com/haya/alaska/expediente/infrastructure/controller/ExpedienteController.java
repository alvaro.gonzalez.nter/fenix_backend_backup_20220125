package com.haya.alaska.expediente.infrastructure.controller;

import com.haya.alaska.asignacion_expediente.application.AsignacionExpedienteUseCase;
import com.haya.alaska.asignacion_expediente.infrastructure.controller.dto.GestionMasivaExpedientesDTO;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.expediente.application.ExpedienteExcelUseCase;
import com.haya.alaska.expediente.application.ExpedienteUseCase;
import com.haya.alaska.expediente.infrastructure.controller.dto.*;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.controller.dto.ExpedientePosesionNegociadaDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.OutputWithListDTO;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioOutputDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.area.AreaUsuarioOutputDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientes")
public class ExpedienteController {

  @Autowired
  private ExpedienteUseCase expedienteUseCase;
  @Autowired
  private ExpedienteExcelUseCase expedienteExcelUseCase;
  @Autowired
  private AsignacionExpedienteUseCase asignacionExpedienteUseCase;

  @ApiOperation(
    value = "Listado expedientes filtrados",
    notes = "Devuelve todos los expdientes filtrados")
  @GetMapping
  public ListWithCountDTO<ExpedienteDTO> getAllExpedientesFiltered(
    @RequestParam(value = "idConcatenado", required = false) String id,
    @RequestParam(value = "estado", required = false) String estado,
    @RequestParam(value = "cliente", required = false) String cliente,
    @RequestParam(value = "cartera", required = false) String cartera,
    @RequestParam(value = "gestor", required = false) String gestor,
    @RequestParam(value = "titular", required = false) String titular,
    @RequestParam(value = "estrategia", required = false) String estrategia,
    @RequestParam(value = "saldo", required = false) String saldo,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {

    Usuario usuario = (Usuario) principal.getPrincipal();
    return expedienteUseCase.getAllFilteredExpedientes(usuario,
      id,
      estado,
      cliente,
      cartera,
      gestor,
      titular,
      estrategia,
      saldo,
      orderField,
      orderDirection,
      size,
      page);
  }

  @ApiOperation(
    value = "Listado expedientes",
    notes = "Devuelve todos los expdientes filtrados que tengan propuestas asociadas")
  @GetMapping("/all-with-propuestas")
  public ListWithCountDTO<ExpedienteDTO> getAllExpedientesWithPropuestas(
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {

    Usuario usuario = (Usuario) principal.getPrincipal();
    return expedienteUseCase.getAllExpedientesWithPropuestas(usuario);
  }

  @ApiOperation(
    value = "Listado expedientes",
    notes = "Devuelve todos los expdientes apropiados para las minutas")
  @GetMapping("/allMinutas")
  public ListWithCountDTO<ExpedienteDTO> getAllExpedientesMinutas(
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {

    Usuario usuario = (Usuario) principal.getPrincipal();
    return expedienteUseCase.getAllExpedientesMinutas(usuario);
  }

  @ApiOperation(
    value = "Listado expedientes filtrados",
    notes = "Devuelve todos los expdientes filtrados")
  @GetMapping("{expedienteId}/historico-gestores")
  public ListWithCountDTO<AsignacionGestoresDTO> getAllAsignacionGestoresHistorico(
    @PathVariable Integer expedienteId) throws Exception {
    return expedienteUseCase.getAllAsignacionGestoresHistorico(expedienteId);
  }

  @ApiOperation(value = "Obtener expediente", notes = "Devuelve el expediente con id enviado")
  @GetMapping("/{expedienteId}")
  @Transactional
  public OutputWithListDTO<ExpedienteDTO> getExpedienteById(@PathVariable Integer expedienteId)
    throws Exception {
    return expedienteUseCase.findExpedienteDTOById(expedienteId);
  }


  @ApiOperation(
    value = "Obtener numero de expedientes en revision",
    notes = "Devuelve numero de expedientes en revision con id enviado")
  @GetMapping("/en-revision")
  @Transactional
  public Integer getExpedientesEnRevision(@ApiIgnore CustomUserDetails principal) throws NotFoundException {
    return expedienteUseCase.getExpedientesEnRevision((Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Descargar información de todos los expedientes en Excel",
    notes = "Descargar información en Excel de todos los expedientes")
  @PostMapping("/excel")
  public ResponseEntity<InputStreamResource> getExcelExpedientes(@ApiIgnore CustomUserDetails principal, @RequestBody ExpedienteExcelExportDTO expedienteExcelExportDTO) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(expedienteExcelUseCase.findExcelExpedientes((Usuario) principal.getPrincipal(), expedienteExcelExportDTO));

    return excelExport.download(excel, "expedientes_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(value = "Descargar información de expediente en Excel",
    notes = "Descargar información en Excel del expediente con ID enviado")
  @GetMapping("/{expedienteId}/excel")
  public ResponseEntity<InputStreamResource> getExcelExpedienteById(
    @PathVariable Integer expedienteId) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(expedienteExcelUseCase.findExcelExpedienteById(expedienteId));

    return excelExport.download(excel, expedienteId + "_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(value = "Actualizar modo de gestión",
    notes = "Actualizar modo de gestión del expediente con ID enviado")
  @PutMapping("/{expedienteId}/modoGestion")
  public ExpedienteDTO modificarModoGestion(@PathVariable Integer expedienteId,
                                            @RequestParam Integer estado,
                                            @RequestParam(required = false) Integer subEstado,
                                            @ApiIgnore CustomUserDetails principal) throws Exception {
    return expedienteUseCase.updateModoGestion(expedienteId, estado, subEstado, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(
    value = "Actualizar modo de gestión",
    notes = "Actualizar modo de gestión del expediente con ID enviado")
  @PutMapping("/gestionMasiva/reasignacionExpedientes")
  public GestionMasivaExpedientesDTO modificarGestor(
    @RequestParam Integer usuarioId,
    @RequestParam(required = false) boolean isSuplente,
    @RequestParam boolean todos,
    @RequestParam boolean todosPN,
    @RequestBody(required = false) ReasignacionGestorInputDTO busquedaDto,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    List<Integer> expedienteIdList = new ArrayList<>();
    List<Integer> expedientePNIdList = new ArrayList<>();
    if (busquedaDto.getExpedienteIdList() != null && !busquedaDto.getExpedienteIdList().isEmpty())
      expedienteIdList.addAll(busquedaDto.getExpedienteIdList());

    if (busquedaDto.getExpedientePNIdList() != null && !busquedaDto.getExpedientePNIdList().isEmpty())
      expedientePNIdList.addAll(busquedaDto.getExpedientePNIdList());

    return asignacionExpedienteUseCase.modificarGestor(expedienteIdList, expedientePNIdList, usuarioId, isSuplente, todos, todosPN, busquedaDto.getBusquedaDto(), usuario);
  }



  @PutMapping("/gestionMasiva/desasignacionExpedientes")
  public GestionMasivaExpedientesDTO desasignar(
    @RequestParam Integer usuarioId,
    @RequestParam boolean todos,
    @RequestParam boolean todosPN,
    @RequestBody(required = false) ReasignacionGestorInputDTO busquedaDto,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    List<Integer> expedienteIdList = new ArrayList<>();
    List<Integer> expedientePNIdList = new ArrayList<>();
    if (busquedaDto.getExpedienteIdList() != null && !busquedaDto.getExpedienteIdList().isEmpty())
      expedienteIdList.addAll(busquedaDto.getExpedienteIdList());

    if (busquedaDto.getExpedientePNIdList() != null && !busquedaDto.getExpedientePNIdList().isEmpty())
      expedientePNIdList.addAll(busquedaDto.getExpedientePNIdList());

    return asignacionExpedienteUseCase.desasignar(expedienteIdList, expedientePNIdList, usuarioId, todos, todosPN, usuario);
  }

  @PutMapping("/gestionMasiva/desasignacionExpedientes/perfil")
  public List<PerfilSimpleOutputDTO> getPerfilesIncluidos(
    @RequestParam boolean todos,
    @RequestParam boolean todosPN,
    @RequestBody ReasignacionGestorInputDTO busquedaDto,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    List<Integer> expedienteIdList = new ArrayList<>();
    List<Integer> expedientePNIdList = new ArrayList<>();
    if (busquedaDto.getExpedienteIdList() != null && !busquedaDto.getExpedienteIdList().isEmpty())
      expedienteIdList.addAll(busquedaDto.getExpedienteIdList());

    if (busquedaDto.getExpedientePNIdList() != null && !busquedaDto.getExpedientePNIdList().isEmpty())
      expedientePNIdList.addAll(busquedaDto.getExpedientePNIdList());

    return asignacionExpedienteUseCase.getPerfilesIncluidos(expedienteIdList, expedientePNIdList, todos, todosPN, busquedaDto.getBusquedaDto(), usuario);
  }

  @PutMapping("/gestionMasiva/desasignacionExpedientes/area")
  public List<AreaUsuarioOutputDTO> getAreaUsuarioIncluidas(
    @RequestParam boolean todos,
    @RequestParam boolean todosPN,
    @RequestBody ReasignacionGestorInputDTO busquedaDto,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    List<Integer> expedienteIdList = new ArrayList<>();
    List<Integer> expedientePNIdList = new ArrayList<>();
    if (busquedaDto.getExpedienteIdList() != null && !busquedaDto.getExpedienteIdList().isEmpty())
      expedienteIdList.addAll(busquedaDto.getExpedienteIdList());

    if (busquedaDto.getExpedientePNIdList() != null && !busquedaDto.getExpedientePNIdList().isEmpty())
      expedientePNIdList.addAll(busquedaDto.getExpedientePNIdList());

    return asignacionExpedienteUseCase.getAreaUsuarioIncluidos(expedienteIdList, expedientePNIdList, todos, todosPN, busquedaDto.getBusquedaDto(), usuario);
  }

  @PutMapping("/gestionMasiva/desasignacionExpedientes/usuarios")
  public List<UsuarioOutputDTO> getUsuariosIncluido(
    @RequestParam boolean todos,
    @RequestParam boolean todosPN,
    @RequestParam Integer perfil,
    @RequestParam(required = false) Integer area,
    @RequestBody ReasignacionGestorInputDTO busquedaDto,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    List<Integer> expedienteIdList = new ArrayList<>();
    List<Integer> expedientePNIdList = new ArrayList<>();
    if (busquedaDto.getExpedienteIdList() != null && !busquedaDto.getExpedienteIdList().isEmpty())
      expedienteIdList.addAll(busquedaDto.getExpedienteIdList());

    if (busquedaDto.getExpedientePNIdList() != null && !busquedaDto.getExpedientePNIdList().isEmpty())
      expedientePNIdList.addAll(busquedaDto.getExpedientePNIdList());

    return asignacionExpedienteUseCase.getUsuariosIncluidos(expedienteIdList, expedientePNIdList, todos, todosPN,perfil,area, busquedaDto.getBusquedaDto(), usuario);
  }

  @GetMapping("/subestados")
  public List<CatalogoMinInfoDTO> getSubEstadosFiltrados(@RequestParam Integer estado) throws Exception {
    return expedienteUseCase.filtroSubEstado(estado);
  }

  @ApiOperation(value = "Estado expediente",
    notes = "Devuelve el estado del expediente con ID enviado")
  @GetMapping("/{expedienteId}/estado")
  public TipoEstado getEstadoExpediente(@PathVariable Integer expedienteId) throws NotFoundException {
    return expedienteUseCase.estadoExpediente(expedienteId);
  }

  @ApiOperation(
    value = "Expedientes por perfil",
    notes = "Devuelve los expedientes en función del idPerfil enviado")
  @GetMapping("/perfil")
  public List<ExpedienteDTO> getExpediente(@RequestParam Integer perfilId) throws NotFoundException {
    return expedienteUseCase.listaExpedientesPerfil(perfilId);
  }

  @ApiOperation(value = "Actualizar id Concatenado",
    notes = "Actualiza el id concatenado de todos los expedientes")
  @PutMapping("/concatenado")
  public void actualizarConcatenados() throws Exception {
    expedienteUseCase.concatenar();
  }

  @ApiOperation(value = "Minuta en Word", notes = "Genera la minuta en formato Word")
  @PostMapping("/minuta/word") //1: Cancelación Hipoteca, 2: Compra-Venta
  public ResponseEntity<InputStreamResource> minutaWord(@RequestParam Integer tipo, @RequestBody ExpedienteMinutaDTO expedienteMinutaDTO) throws NotFoundException, ForbiddenException, AccessDeniedException, RequiredValueException {

    return expedienteUseCase.minutaWord(tipo, expedienteMinutaDTO);

  }

  @ApiOperation(value = "Minuta en PDF", notes = "Genera la minuta en formato PDF")
  @PostMapping("/minuta/pdf") //1: Cancelación Hipoteca, 2: Compra-Venta
  public ResponseEntity<byte[]> minutaPdf(@RequestParam Integer tipo, @RequestBody ExpedienteMinutaDTO expedienteMinutaDTO) throws IOException, NotFoundException, ForbiddenException, RequiredValueException {

    return expedienteUseCase.minutaPdf(tipo, expedienteMinutaDTO);

  }

  @ApiOperation(value = "Devuelve un Expediente por su Cartera de tipo Recuperacion Amistosa", notes = "Devuelve un Expediente de PN por su Cartera")
  @GetMapping("/{idCartera}/recuperacionamistosa")
  public ResponseEntity<List<ExpedienteDTO>> getExpedienteByCarteraRA(@PathVariable Integer idCartera,
                                                                      @RequestParam(value = "size") Integer size,
                                                                      @RequestParam(value = "page") Integer page,
                                                                      @ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return expedienteUseCase.expedienteFiltradoCarteraRA(idCartera, usuario, size, page);
  }

  @ApiOperation(value = "Devuelve un Expediente por su Cartera de tipo Posesion Negociada", notes = "Devuelve un Expediente de RA por su Cartera")
  @GetMapping("/{idCartera}/posesionnegociada")
  public ResponseEntity<List<ExpedientePosesionNegociadaDTO>> getExpedienteByCarteraPN(@PathVariable Integer idCartera,
                                                                                       @RequestParam(value = "size") Integer size,
                                                                                       @RequestParam(value = "page") Integer page,
                                                                                       @ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return expedienteUseCase.expedienteFiltradoCarteraPN(idCartera, usuario, size, page);
  }

  @ApiOperation(
    value = "Expedientes sin Gestor",
    notes = "Devuelve los expedientes sin Gestor asignado")
  @GetMapping("/sinGestor")
  public List<ExpedienteDTO> getExpedienteSinGestor(@RequestParam Integer carteraId) throws NotFoundException {
    return expedienteUseCase.expedienteFiltradoCarteraSinGestor(carteraId);
  }
}
