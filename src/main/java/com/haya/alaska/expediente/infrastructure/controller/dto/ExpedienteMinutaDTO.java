package com.haya.alaska.expediente.infrastructure.controller.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpedienteMinutaDTO implements Serializable {

  private Integer idCliente;
  private Integer idCartera;
  private String fecha;

  private Integer idExpediente;
  private Integer idPropuesta;

  private List<BienMinutaDTO> idBienes;
  private List<Integer> idIntervinientes;
  private List<Integer> idImportes;
  private List<Integer> idContratos;
}
