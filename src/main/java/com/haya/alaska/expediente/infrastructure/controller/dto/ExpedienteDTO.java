package com.haya.alaska.expediente.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.infrastructure.controller.dto.EstrategiaAsignadaOutputDTO;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ExpedienteDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private Integer id;
  private String idConcatenado;
  private String expedienteCliente;
  private CatalogoMinInfoDTO estado;
  private List<Integer> idsCliente;
  private String cliente;
  private String grupoCliente;
  private String idOrigen;
  private String cartera;
  private Integer carteraId;
  private Integer contratoRepresentanteId;
  private String contratoRepresentanteIdOrigen;
  private String gestor;
  private String subtipoGestor;
  private String gestorSkipTracing;
  private String gestorFormalizacion;
  private String gestorSoporte;
  private String responsableCartera;
  private String supervisor;
  private List<String> areaGestion;
  private String titular;
  private String titularNif;
  private List<ContratoDTOToList> contratos;

  private String operacion;
  private Double precioCompra;
  private String carteraPlazo;

  private Boolean formalizacion;

  //Agenda
  private String accion;
  private Date fechaAccion;
  private String alerta;
  private Date fechaAlerta;
  private String actividad;
  private Date fechaActividad;
  //Modo de Gestión
  private String modoDeGestionEstado;
  private String modoDeGestionSubEstado;
  private String situacion;
  //Propuesta
  private String propuesta;
  private CatalogoMinInfoDTO estadoPropuesta;
  //Estrategia
  private EstrategiaAsignadaOutputDTO estrategiaAsignada;
  private Integer estimaciones;
  private String importeEstrategia;//TODO
  //Judicial
  private String judicializado;
  private CatalogoMinInfoDTO tipoProcedimiento;
  private String faseProcedimiento;
  //Información de la Deuda
  private Double principalPendiente;
  private Double deudaImpagada;
  private Double saldo;
  private Double importeRecuperado;
  private Date fechaContratoPrincipal;

  private String campania;
  private Integer idCampania;
  private Boolean recovery;
}
