package com.haya.alaska.expediente.infrastructure.controller.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class BienMinutaDTO implements Serializable {
  private Integer idBien;
  private List<Integer> idTasacion;
  private List<Integer> idValoracion;
  private List<Integer> idCargas;
}
