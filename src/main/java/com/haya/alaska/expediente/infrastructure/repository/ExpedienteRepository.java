package com.haya.alaska.expediente.infrastructure.repository;

import com.haya.alaska.expediente.domain.Expediente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.ws.rs.QueryParam;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface ExpedienteRepository extends JpaRepository<Expediente, Integer> {

  @Query(
    value =
      "SELECT COUNT(DISTINCT ID_EXPEDIENTE) FROM MSTR_CONTRATO c WHERE c.IND_EN_REVISION = 1 AND c.IND_ACTIVO = 1 AND c.ID_EXPEDIENTE IN (SELECT a.ID_EXPEDIENTE FROM RELA_ASIGNACION_EXPEDIENTE a WHERE a.ID_USUARIO = :usuario AND a.IND_ACTIVO = 1)",
    nativeQuery = true)
  Integer countContratosEnRevisionOnExpediente(@QueryParam(value = "usuario") Integer usuario);

  @Query(
    value =
      "SELECT rh.timestamp AS fechaAsignacion, u.ID AS gestorExpediente, u2.DES_NOMBRE AS usuarioAsignaciones\n"
        + "FROM HIST_RELA_ASIGNACION_EXPEDIENTE aeh\n"
        + "         left Join REGISTRO_HISTORICO rh ON aeh.REV = rh.id\n"
        + "         left Join MSTR_USUARIO u ON aeh.ID_USUARIO = u.ID\n"
        + "         left Join MSTR_USUARIO u2 ON aeh.ID_USUARIO_ASIGNACION = u2.ID\n"
        + "WHERE aeh.ID_EXPEDIENTE = ?1 and u.ID_PERFIL = 1\n"
        + "ORDER BY rh.timestamp DESC",
    nativeQuery = true)
  List<Map> historicoAsignacionGestores(Integer expediente);

  @Query(
    value =
      "SELECT SUM(NUM_IMPORTE) AS importe, c.ID_EXPEDIENTE AS idExpediente " +
        "FROM (SELECT ID_BIEN, MAX(FCH_FECHA) AS FCH_FECHA " +
        "FROM MSTR_VALORACION v " +
        "GROUP BY ID_BIEN) x " +
        "JOIN MSTR_VALORACION v ON x.ID_BIEN = v.ID_BIEN AND v.FCH_FECHA = x.FCH_FECHA " +
        "INNER JOIN RELA_CONTRATO_BIEN rcb ON x.ID_BIEN = rcb.ID_BIEN " +
        "INNER JOIN MSTR_CONTRATO c ON rcb.ID_CONTRATO = c.ID " +
        "WHERE c.ID_EXPEDIENTE IS NOT NULL " +
        "GROUP BY v.ID_BIEN",
    nativeQuery = true)
  List<Map> getExpedientesPorUltimaValoracion();

  Optional<Expediente> findByIdCarga(String idCarga);

  Optional<Expediente> findByIdCargaAndCarteraIdCargaAndIdOrigen(String idCarga, String carteraIdCarga, String idOrigen);

  List<Expediente> findByIdIn(List<Integer> listExpedienteId);

  List<Expediente> findByIdConcatenadoIn(List<String> listExpedienteId);

  Optional<Expediente> findByIdConcatenado(String idConcatenado);

  List<Expediente> findAllByCarteraIdAndAsignacionesUsuarioId(Integer carteraId, Integer usuarioId);

  List<Expediente> findAllByCarteraId(Integer carteraId);

  List<Expediente> findAllByCarteraIdAndAsignacionesUsuarioEmail(Integer carteraId, String email);

  List<Expediente> findDistinctAllByCarteraIdAndAsignacionesUsuarioAreasNombre(Integer carteraId, String area);

  Integer countAllByCarteraIdAndAsignacionesUsuarioAreasNombre(Integer carteraId, String area);

  Integer countDistinctAllByCarteraIdAndAsignacionesUsuarioAreasNombreNotAndAgenciaIsNotNull(Integer carteraId, String area);

  List<Expediente> findDistinctAllByCarteraIdAndAsignacionesUsuarioAreasNombreNotAndAgenciaIsNotNull(Integer carteraId, String area);

  List<Expediente> findAllByCarteraIdAndAsignacionesUsuarioEmailAndAgenciaIsNull(Integer carteraId, String email);

  List<Expediente> findAllByCarteraIdAndAsignacionesUsuarioAreasNombreAndAgenciaIsNull(Integer carteraId, String area);

  Page<Expediente> findAllByCarteraId(Integer carteraId, Pageable pageable);

  Page<Expediente> findAllByCarteraIdAndAsignacionesUsuarioId(Integer carteraId, Integer usuarioId, Pageable pageable);

  List<Expediente> findByFechaCreacionAndCarteraId(Date fechaCreacion, Integer idCartera);

  List<Expediente> findByFechaCreacion(Date fechaCreacion);

  List<Expediente> findAllByCarteraIdAndAgenciaId(Integer idCartera, Integer idAgencia);

  List<Expediente> findAllByCarteraIdAndAgenciaIsNull(Integer idCartera);

  Integer countAllByCarteraId(Integer idCartera);

  List<Expediente> findAllByCarteraIdAndTipoEstadoId(Integer carteraId, Integer situacionContableId);

  @Query(
    value =
      " select * from MSTR_EXPEDIENTE me2  where me2.ID not in ( " +
        "select me.ID as usuarioID from MSTR_EXPEDIENTE me " +
        "inner join RELA_ASIGNACION_EXPEDIENTE rae on me.ID = rae.ID_EXPEDIENTE " +
        "inner join MSTR_USUARIO mu on rae.ID_USUARIO = mu.ID " +
        "inner join MSTR_PERFIL mp on mu.ID_PERFIL = mp.ID " +
        "where mp.DES_NOMBRE = 'Gestor' " +
        "and me.ID_CARTERA =:idCartera); ",
    nativeQuery = true)
  List<Expediente> getExpedientesSinGestor(Integer idCartera);

  //Métodos para segmentacion
  //Situacion
  @Query(
    value = "SELECT DISTINCT E.ID " +
      "FROM MSTR_EXPEDIENTE AS E " +
      "LEFT JOIN MSTR_CONTRATO AS C ON C.ID_EXPEDIENTE = E.ID " +
      "WHERE C.ID_SITUACION = ?2 " +
      "AND E.ID_CARTERA = ?1 ;",
    nativeQuery = true
  )
  List<Integer> getDistinctIdByCarteraIdAndContratosSituacionId(Integer idCartera, Integer idSituacion);
  //Modo
  @Query(
    value = "SELECT DISTINCT E.ID " +
      "FROM MSTR_EXPEDIENTE AS E " +
      "WHERE E.ID_TIPO_ESTADO = ?2 " +
      "AND E.ID_CARTERA = ?1 ;",
    nativeQuery = true
  )
  List<Integer> findAllDistinctIdByCarteraIdAndTipoEstadoId(Integer carteraId, Integer situacionContableId);
  //Producto
  @Query(
    value = "SELECT DISTINCT E.ID " +
      "FROM MSTR_EXPEDIENTE AS E " +
      "LEFT JOIN MSTR_CONTRATO AS C ON C.ID_EXPEDIENTE = E.ID " +
      "WHERE C.ID_PRODUCTO = ?2 " +
      "AND E.ID_CARTERA = ?1 ;",
    nativeQuery = true
  )
  List<Integer> findAllDistinctIdByCarteraIdAndContratosProductoId(Integer carteraId, Integer situacionContableId);
  //Judicial
  @Query(
    value = "SELECT DISTINCT E.ID " +
      "FROM MSTR_EXPEDIENTE AS E " +
      "LEFT JOIN MSTR_CONTRATO AS C ON C.ID_EXPEDIENTE = E.ID " +
      "WHERE C.ID IN ( " +
      "    SELECT C1.ID " +
      "    FROM MSTR_CONTRATO AS C1 " +
      "    LEFT JOIN RELA_CONTRATO_PROCEDIMIENTO AS R ON C1.ID = R.ID_CONTRATO " +
      "    GROUP BY C1.ID " +
      "    HAVING COUNT(R.ID_PROCEDIMIENTO) = 0 " +
      ") AND E.ID_CARTERA = ?1 ;",
    nativeQuery = true
  )
  List<Integer> findAllByCountProcedimientosZero(Integer idCartera);
  @Query(
    value = "SELECT DISTINCT E.ID " +
      "FROM MSTR_EXPEDIENTE AS E " +
      "LEFT JOIN MSTR_CONTRATO AS C ON C.ID_EXPEDIENTE = E.ID " +
      "WHERE C.ID IN ( " +
      "    SELECT C1.ID " +
      "    FROM MSTR_CONTRATO AS C1 " +
      "    LEFT JOIN RELA_CONTRATO_PROCEDIMIENTO AS R ON C1.ID = R.ID_CONTRATO " +
      "    GROUP BY C1.ID " +
      "    HAVING COUNT(R.ID_PROCEDIMIENTO) > 0 " +
      ") AND E.ID_CARTERA = ?1 ;",
    nativeQuery = true
  )
  List<Integer> findAllByCountProcedimientosMore(Integer idCartera);
  //Clasificacion
  @Query(
    value = "SELECT DISTINCT E.ID " +
      "FROM MSTR_EXPEDIENTE AS E " +
      "LEFT JOIN MSTR_CONTRATO AS C ON C.ID_EXPEDIENTE = E.ID " +
      "WHERE C.ID_CLASIFICACION_CONTABLE = ?2 " +
      "AND E.ID_CARTERA = ?1 ;",
    nativeQuery = true
  )
  List<Integer> findAllDistinctIdByCarteraIdAndContratosClasificacionContableId(Integer carteraId, Integer idSituacion);
}
