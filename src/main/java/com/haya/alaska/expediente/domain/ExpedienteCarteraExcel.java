package com.haya.alaska.expediente.domain;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;
import java.util.stream.Collectors;

public class ExpedienteCarteraExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Customer Connection");
      cabeceras.add("Customer");
      cabeceras.add("Customer group");
      cabeceras.add("Portfolio");
      cabeceras.add("Representative Loan");
      cabeceras.add("First holder");
      cabeceras.add("Portfolio manager");
      cabeceras.add("Supervisor");
      cabeceras.add("Management area");
      cabeceras.add("Connection Manager");
      cabeceras.add("Formalization manager");
      cabeceras.add("Skip tracing manager");
      cabeceras.add("Status");
      cabeceras.add("Substatus");
      cabeceras.add("Situation");
      cabeceras.add("Proposal");
      cabeceras.add("Proposed Status");
      cabeceras.add("Strategy");
      cabeceras.add("Strategy Type");
      cabeceras.add("Estimate");
      cabeceras.add("Strategy amount");
      cabeceras.add("Strategy date");
      cabeceras.add("Validity date");
      cabeceras.add("Judicial");
      cabeceras.add("Procedure Type");
      cabeceras.add("Phase");
      cabeceras.add("Balance in management");
      cabeceras.add("Amount recovered");
      cabeceras.add("Outstanding principal");
      cabeceras.add("Unpaid debt");
      cabeceras.add("Date unpaid");
      cabeceras.add("Action");
      cabeceras.add("Action Date");
      cabeceras.add("Alert");
      cabeceras.add("Alert date");
      cabeceras.add("Activity");
      cabeceras.add("Activity date");
      cabeceras.add("Date");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Expediente cliente");
      cabeceras.add("Cliente");
      cabeceras.add("Grupo cliente");
      cabeceras.add("Cartera");
      cabeceras.add("Contrato representante");
      cabeceras.add("Primer titular");
      cabeceras.add("Responsable Cartera");
      cabeceras.add("Supervisor");
      cabeceras.add("Área de gestión");
      cabeceras.add("Gestor del expediente");
      cabeceras.add("Gestor de formalización");
      cabeceras.add("Gestor de skip tracing");
      cabeceras.add("Estado");
      cabeceras.add("Subestado");
      cabeceras.add("Situación");
      cabeceras.add("Propuesta");
      cabeceras.add("Estado Propuesta");
      cabeceras.add("Estrategia");
      cabeceras.add("Tipo Estrategia");
      cabeceras.add("Estimación");
      cabeceras.add("Importe Estrategia");
      cabeceras.add("Fecha Estrategia");
      cabeceras.add("Fecha Validez");
      cabeceras.add("Judicial");
      cabeceras.add("Tipo Procedimiento");
      cabeceras.add("Fase");
      cabeceras.add("Saldo en Gestión");
      cabeceras.add("Importe Recuperado");
      cabeceras.add("Principal Pendiente");
      cabeceras.add("Deuda Impagada");
      cabeceras.add("Fecha Impago");
      cabeceras.add("Acción");
      cabeceras.add("Fecha Acción");
      cabeceras.add("Alerta");
      cabeceras.add("Fecha Alerta");
      cabeceras.add("Actividad");
      cabeceras.add("Fecha Actividad");
      cabeceras.add("Fecha");
    }
  }

  public ExpedienteCarteraExcel(
      Expediente expediente, Evento accion, Evento alerta, Evento actividad, Procedimiento p) {
    super();

    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");
    Usuario responsableCartera = expediente.getUsuarioByPerfil("Responsable de cartera");
    Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      AreaUsuario areaUsuario = null;
      if (gestor != null) {
        var areasUsuario = gestor.getAreas();
        for (AreaUsuario area : areasUsuario) {
          areaUsuario = area;
        }
      }

      Propuesta propuesta = null;
      var aux = 0;
      for (Propuesta pro : expediente.getPropuestas()) {
        if (pro.getEstadoPropuesta() != null) {
          if (pro.getEstadoPropuesta().getId() > aux) {
            aux = pro.getEstadoPropuesta().getId();
            propuesta = pro;
          }
        }
      }

      this.add("Connection Id", expediente.getIdConcatenado());
      this.add(
          "Customer Connection", expediente.getIdOrigen() != null ? expediente.getIdOrigen() : null);
      this.add(
          "Customer",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add(
          "Customer group",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getGrupo().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add(
          "Portfolio",
          expediente.getCartera() != null ? expediente.getCartera().getNombre() : null);
      this.add(
          "Representative Loan",
          expediente.getContratoRepresentante() != null
              ? expediente.getContratoRepresentante().getIdCarga()
              : null);
      if (expediente.getContratoRepresentante().getPrimerInterviniente().getPersonaJuridica()
              != null
          && expediente.getContratoRepresentante().getPrimerInterviniente().getPersonaJuridica())
        this.add(
            "First holder",
            expediente.getContratoRepresentante().getPrimerInterviniente().getRazonSocial() != null
                ? expediente.getContratoRepresentante().getPrimerInterviniente().getRazonSocial()
                : null);
      else
        this.add(
            "First holder",
            expediente.getContratoRepresentante().getPrimerInterviniente().getNombre()
                + " "
                + expediente.getContratoRepresentante().getPrimerInterviniente().getApellidos());

      this.add(
          "Portfolio manager", responsableCartera != null ? responsableCartera.getNombre() : null);
      this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
      this.add(
          "Management area",
          gestor != null && areaUsuario != null ? areaUsuario.getNombre() : null);
      this.add("Connection Manager", gestor != null ? gestor.getNombre() : null);
      this.add(
          "Status",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValorIngles() : null);
      this.add(
          "Substatus",
          expediente.getSubEstado() != null ? expediente.getSubEstado().getValorIngles() : null);
      this.add(
          "Situation",
          expediente.getContratoRepresentante() != null
              ? expediente.getContratoRepresentante().getSituacion() != null
                  ? expediente.getContratoRepresentante().getSituacion().getValorIngles()
                  : null
              : null);
      if (propuesta != null) {
        this.add(
          "Proposal",
          propuesta.getTipoPropuesta() != null ? propuesta.getTipoPropuesta().getValorIngles() : null);
        this.add(
          "Proposed Status",
          propuesta.getEstadoPropuesta() != null
            ? propuesta.getEstadoPropuesta().getValorIngles()
            : null);
        this.add(
          "Validity date",
          propuesta.getFechaValidez() != null ? propuesta.getFechaValidez() : null);
        if (propuesta.getEstrategia() != null) {
          this.add(
            "Strategy",
            propuesta.getEstrategia().getEstrategia() != null
              ? propuesta.getEstrategia().getEstrategia().getValorIngles()
              : null);
          this.add(
            "Strategy Type",
            propuesta.getEstrategia().getTipoEstrategia() != null
              ? propuesta.getEstrategia().getTipoEstrategia().getValorIngles()
              : null);
          this.add("Estimate", expediente.getEstimaciones().size());
          this.add("Strategy amount", null);
          this.add(
            "Strategy date",
            propuesta.getEstrategia() != null ? propuesta.getEstrategia().getFecha() : null);
        }
      }
      this.add(
        "Judicial",
        expediente.getContratoRepresentante() != null
          ? expediente.getContratoRepresentante().getGastosJudiciales()
          : null);

      String hitoP = "";
      String tipoP = "";
      if (p != null) {
        if (p.getHitoProcedimiento() != null) hitoP = p.getHitoProcedimiento().getValorIngles();
        tipoP = p.getTipo().getValorIngles();
      }
      this.add("Phase", hitoP);
      this.add("Procedure Type", tipoP);
      this.add("Balance in management", expediente.getSaldoGestion());
      this.add("Amount recovered", expediente.getImporteRecuperado());
      this.add("Outstanding principal", expediente.getPrincipalPendiente());
      this.add(
        "Unpaid debt",
        expediente.getContratoRepresentante() != null
          ? expediente.getContratoRepresentante().getDeudaImpagada()
          : null);
      this.add(
        "Date unpaid",
        expediente.getContratoRepresentante() != null
          ? expediente.getContratoRepresentante().getFechaPrimerImpago()
          : null);
      this.add("Action", accion != null ? accion.getTitulo() : null);
      this.add("Action Date", accion != null ? accion.getFechaCreacion() : null);
      this.add("Alert", alerta != null ? alerta.getTitulo() : null);
      this.add("Alert date", alerta != null ? alerta.getFechaCreacion() : null);
      this.add("Activity", actividad != null ? actividad.getTitulo() != null : null);
      this.add("Activity date", actividad != null ? actividad.getFechaCreacion() : null);
      this.add(
        "Date", expediente.getFechaCreacion() != null ? expediente.getFechaCreacion() : null);
      this.add(
        "Formalization manager",
        expediente.getUsuarioByPerfil("Formalization manager") != null
          ? expediente.getUsuarioByPerfil("Formalization manager").getNombre()
          : null);
      this.add(
        "Skip tracing manager",
        expediente.getUsuarioByPerfil("Skip tracing manager") != null
          ? expediente.getUsuarioByPerfil("Skip tracing manager").getNombre()
          : null);

    } else {

      AreaUsuario areaUsuario = null;
      if (gestor != null) {
        var areasUsuario = gestor.getAreas();
        for (AreaUsuario area : areasUsuario) {
          areaUsuario = area;
        }
      }

      Propuesta propuesta = null;
      var aux = 0;
      for (Propuesta pro : expediente.getPropuestas()) {
        if (pro.getEstadoPropuesta() != null) {
          if (pro.getEstadoPropuesta().getId() > aux) {
            aux = pro.getEstadoPropuesta().getId();
            propuesta = pro;
          }
        }
      }

      this.add("Id Expediente", expediente.getIdConcatenado());
      this.add(
          "Expediente Cliente", expediente.getIdOrigen() != null ? expediente.getIdOrigen() : null);
      this.add(
          "Cliente",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add(
          "Grupo Cliente",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getGrupo().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add(
          "Cartera", expediente.getCartera() != null ? expediente.getCartera().getNombre() : null);
      this.add(
          "Contrato representante",
          expediente.getContratoRepresentante() != null
              ? expediente.getContratoRepresentante().getIdCarga()
              : null);
      if (expediente.getContratoRepresentante().getPrimerInterviniente().getPersonaJuridica()
              != null
          && expediente.getContratoRepresentante().getPrimerInterviniente().getPersonaJuridica())
        this.add(
            "Primer titular",
            expediente.getContratoRepresentante().getPrimerInterviniente().getRazonSocial() != null
                ? expediente.getContratoRepresentante().getPrimerInterviniente().getRazonSocial()
                : null);
      else
        this.add(
            "Primer titular",
            expediente.getContratoRepresentante().getPrimerInterviniente().getNombre()
                + " "
                + expediente.getContratoRepresentante().getPrimerInterviniente().getApellidos());
      this.add(
          "Responsable Cartera",
          responsableCartera != null ? responsableCartera.getNombre() : null);
      this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
      this.add(
          "Área de gestión",
          gestor != null && areaUsuario != null ? areaUsuario.getNombre() : null);
      this.add("Gestor del expediente", gestor != null ? gestor.getNombre() : null);
      this.add(
          "Estado",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
      this.add(
          "Subestado",
          expediente.getSubEstado() != null ? expediente.getSubEstado().getValor() : null);
      this.add(
          "Situación",
          expediente.getContratoRepresentante() != null
              ? expediente.getContratoRepresentante().getSituacion() != null
                  ? expediente.getContratoRepresentante().getSituacion().getValor()
                  : null
              : null);
      if (propuesta != null) {
        this.add(
            "Propuesta",
            propuesta.getTipoPropuesta() != null ? propuesta.getTipoPropuesta().getValor() : null);
        this.add(
            "Estado Propuesta",
            propuesta.getEstadoPropuesta() != null
                ? propuesta.getEstadoPropuesta().getValor()
                : null);
        this.add(
            "Fecha Validez",
            propuesta.getFechaValidez() != null ? propuesta.getFechaValidez() : null);
        if (propuesta.getEstrategia() != null) {
          this.add(
              "Estrategia",
              propuesta.getEstrategia().getEstrategia() != null
                  ? propuesta.getEstrategia().getEstrategia().getValor()
                  : null);
          this.add(
              "Tipo Estrategia",
              propuesta.getEstrategia().getTipoEstrategia() != null
                  ? propuesta.getEstrategia().getTipoEstrategia().getValor()
                  : null);
          this.add("Estimación", expediente.getEstimaciones().size());
          this.add("Importe Estrategia", null);
          this.add(
              "Fecha Estrategia",
              propuesta.getEstrategia() != null ? propuesta.getEstrategia().getFecha() : null);
        }
      }
      this.add(
          "Judicial",
          expediente.getContratoRepresentante() != null
              ? expediente.getContratoRepresentante().getGastosJudiciales()
              : null);

      String hitoP = "";
      String tipoP = "";
      if (p != null) {
        if (p.getHitoProcedimiento() != null) hitoP = p.getHitoProcedimiento().getValor();
        tipoP = p.getTipo().getValor();
      }

      this.add("Fase", hitoP);
      this.add("Tipo Procedimiento", tipoP);
      this.add("Saldo en Gestión", expediente.getSaldoGestion());
      this.add("Importe Recuperado", expediente.getImporteRecuperado());
      this.add("Principal Pendiente", expediente.getPrincipalPendiente());
      this.add(
          "Deuda Impagada",
          expediente.getContratoRepresentante() != null
              ? expediente.getContratoRepresentante().getDeudaImpagada()
              : null);
      this.add(
          "Fecha Impago",
          expediente.getContratoRepresentante() != null
              ? expediente.getContratoRepresentante().getFechaPrimerImpago()
              : null);
      this.add("Acción", accion != null ? accion.getTitulo() : null);
      this.add("Fecha Acción", accion != null ? accion.getFechaCreacion() : null);
      this.add("Alerta", alerta != null ? alerta.getTitulo() : null);
      this.add("Fecha Alerta", alerta != null ? alerta.getFechaCreacion() : null);
      this.add("Actividad", actividad != null ? actividad.getTitulo() != null : null);
      this.add("Fecha Actividad", actividad != null ? actividad.getFechaCreacion() : null);
      this.add(
          "Fecha", expediente.getFechaCreacion() != null ? expediente.getFechaCreacion() : null);
      this.add(
          "Gestor de formalización",
          expediente.getUsuarioByPerfil("Gestor formalización") != null
              ? expediente.getUsuarioByPerfil("Gestor formalización").getNombre()
              : null);
      this.add(
          "Gestor de skip tracing",
          expediente.getUsuarioByPerfil("Gestor skip tracing") != null
              ? expediente.getUsuarioByPerfil("Gestor skip tracing").getNombre()
              : null);
    }
  }

  public ExpedienteCarteraExcel(
      ExpedientePosesionNegociada expediente,
      Evento accion,
      Evento alerta,
      Evento actividad,
      Procedimiento p) {
    super();

    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");
    Usuario responsableCartera = expediente.getUsuarioByPerfil("Responsable de cartera");
    Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      AreaUsuario areaUsuario = null;
      if (gestor != null) {
        var areasUsuario = gestor.getAreas();
        for (AreaUsuario area : areasUsuario) {
          areaUsuario = area;
        }
      }

      Propuesta propuesta = null;
      var aux = 0;
      for (Propuesta pro : expediente.getPropuestas()) {
        if (pro.getEstadoPropuesta() != null) {
          if (pro.getEstadoPropuesta().getId() > aux) {
            aux = pro.getEstadoPropuesta().getId();
            propuesta = pro;
          }
        }
      }

      this.add("Connection Id", expediente.getIdConcatenado());
      this.add(
        "Customer Connection", expediente.getIdOrigen() != null ? expediente.getIdOrigen() : null);
      this.add(
        "Customer",
        expediente.getCartera() != null
          ? expediente.getCartera().getClientes().stream()
          .map(clienteCartera -> clienteCartera.getCliente().getNombre())
          .collect(Collectors.joining(", "))
          : null);
      this.add(
        "Customer group",
        expediente.getCartera() != null
          ? expediente.getCartera().getClientes().stream()
          .map(clienteCartera -> clienteCartera.getCliente().getGrupo().getNombre())
          .collect(Collectors.joining(", "))
          : null);
      this.add(
        "Portfolio", expediente.getCartera() != null ? expediente.getCartera().getNombre() : null);
      this.add(
        "Representative Loan",
        expediente.getContrato() != null ? expediente.getContrato().getIdCarga() : null);
      /*if(expediente.getContrato().getPrimerInterviniente().getPersonaJuridica() != null && expediente.getContrato().getPrimerInterviniente().getPersonaJuridica())
            this.add("Primer titular", expediente.getContrato().getPrimerInterviniente().getRazonSocial() != null ? expediente.getContrato().getPrimerInterviniente().getRazonSocial() : null);
          else
            this.add("Primer titular", expediente.getContrato().getPrimerInterviniente().getNombre() + " " + expediente.getContrato().getPrimerInterviniente().getApellidos());
      */
      List<Bien> bienes = new ArrayList();
      if (expediente.getBienesPosesionNegociada() != null) {
        for (var bienPN : expediente.getBienesPosesionNegociada()) {
          bienes.add(bienPN.getBien());
        }
      }
      Iterator<Bien> bienIterator = bienes.iterator();
      while (bienIterator.hasNext()) {
        Bien bienProv = bienIterator.next();
        if (bienProv.getImporteBien() == null) {
          bienIterator.remove();
        }
      }

      List<Bien> bienesOrdenados =
        bienes.stream()
          .sorted(Comparator.comparingDouble(Bien::getImporteBien).reversed())
          .collect(Collectors.toList());

      Boolean obtenido = false;
      Boolean obtenidoSituacion = false;
      for (var bienSacado : bienesOrdenados) {
        for (var bienPNSacado : expediente.getBienesPosesionNegociada()) {
          if (obtenido == false && bienPNSacado.getBien().equals(bienSacado)) {
            for (var ocupanteSacado : bienPNSacado.getOcupantes()) {
              if (obtenido == false && ocupanteSacado.getTipoOcupante() != null) {
                if (ocupanteSacado.getTipoOcupante().getCodigo().equals("TIT")) {
                  if (ocupanteSacado.getOcupante().getNombreCompleto() != null) {
                    this.add("First holder", ocupanteSacado.getOcupante().getNombreCompleto());
                    obtenido = true;
                  }
                }
              }
              if (obtenidoSituacion == false && bienPNSacado.getBien().equals(bienSacado)) {
                if (bienPNSacado.getSituacion() != null
                  && bienPNSacado.getSituacion().getValorIngles() != null) {
                  this.add("Situation", bienPNSacado.getSituacion().getValorIngles());
                  obtenidoSituacion = true;
                }
              }
            }
          }
        }
      }

      this.add(
        "Formalization manager",
        expediente.getUsuarioByPerfil("Formalization manager") != null
          && expediente.getUsuarioByPerfil("Formalization manager").getNombre() != null
          ? expediente.getUsuarioByPerfil("Formalization manager").getNombre()
          : null);
      this.add(
        "Skip tracing manager",
        expediente.getUsuarioByPerfil("Skip tracing manager") != null
          && expediente.getUsuarioByPerfil("Skip tracing manager").getNombre() != null
          ? expediente.getUsuarioByPerfil("Skip tracing manager").getNombre()
          : null);
      this.add(
        "Portfolio manager",
        responsableCartera != null ? responsableCartera.getNombre() : null);
      this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
      this.add(
        "Management area",
        gestor != null && areaUsuario != null ? areaUsuario.getNombre() : null);
      this.add("Gestor del expediente", gestor != null ? gestor.getNombre() : null);
      this.add(
        "Status",
        expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValorIngles() : null);
      this.add(
        "Substatus",
        expediente.getSubEstado() != null ? expediente.getSubEstado().getValorIngles() : null);

      if (propuesta != null) {
        this.add(
          "Proposal",
          propuesta.getTipoPropuesta() != null ? propuesta.getTipoPropuesta().getValorIngles() : null);
        this.add(
          "Proposed Status",
          propuesta.getEstadoPropuesta() != null
            ? propuesta.getEstadoPropuesta().getValorIngles()
            : null);
        this.add(
          "Validity date",
          propuesta.getFechaValidez() != null ? propuesta.getFechaValidez() : null);
        if (propuesta.getEstrategia() != null) {
          this.add(
            "Strategy",
            propuesta.getEstrategia().getEstrategia() != null
              ? propuesta.getEstrategia().getEstrategia().getValorIngles()
              : null);
          this.add(
            "Strategy Type",
            propuesta.getEstrategia().getTipoEstrategia() != null
              ? propuesta.getEstrategia().getTipoEstrategia().getValorIngles()
              : null);
          this.add("Estimate", expediente.getEstimaciones().size());
          this.add("Strategy amount", null);
        }
      }
      /**
       * this.add("Judicial", expediente.getContrato()!=null ?
       * expediente.getContrato().getGastosJudiciales():null); this.add("Saldo en Gestión",
       * expediente.getSaldoGestion()); this.add("Importe Recuperado",
       * expediente.getImporteRecuperado()); this.add("Principal Pendiente",
       * expediente.getPrincipalPendiente()); this.add("Deuda Impagada", expediente.getContrato() !=
       * null ? expediente.getContrato().getDeudaImpagada() : null); this.add("Fecha Impago",
       * expediente.getContrato() != null ? expediente.getContrato().getFechaPrimerImpago() : null);
       * this.add("Situación", expediente.getContrato() != null ?
       * expediente.getContrato().getSituacion() != null ?
       * expediente.getContrato().getSituacion().getValor() : null : null);
       */
      String hitoP = "";
      String tipoP = "";
      if (p != null) {
        if (p.getHitoProcedimiento() != null) hitoP = p.getHitoProcedimiento().getValorIngles();
        tipoP = p.getTipo().getValorIngles();
      }

      this.add("Phase", hitoP);
      this.add("Procedure Type", tipoP);
      this.add("Acción", accion != null ? accion.getTitulo() : null);
      this.add("Action Date", accion != null ? accion.getFechaCreacion() : null);
      this.add("Alert", alerta != null ? alerta.getTitulo() : null);
      this.add("Alert date", alerta != null ? alerta.getFechaCreacion() : null);
      this.add("Activity", actividad != null ? actividad.getTitulo() != null : null);
      this.add("Activity date", actividad != null ? actividad.getFechaCreacion() : null);
      this.add(
        "Date", expediente.getFechaCreacion() != null ? expediente.getFechaCreacion() : null);


    } else {

      AreaUsuario areaUsuario = null;
      if (gestor != null) {
        var areasUsuario = gestor.getAreas();
        for (AreaUsuario area : areasUsuario) {
          areaUsuario = area;
        }
      }

      Propuesta propuesta = null;
      var aux = 0;
      for (Propuesta pro : expediente.getPropuestas()) {
        if (pro.getEstadoPropuesta() != null) {
          if (pro.getEstadoPropuesta().getId() > aux) {
            aux = pro.getEstadoPropuesta().getId();
            propuesta = pro;
          }
        }
      }

      this.add("Id Expediente", expediente.getIdConcatenado());
      this.add(
          "Expediente Cliente", expediente.getIdOrigen() != null ? expediente.getIdOrigen() : null);
      this.add(
          "Cliente",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add(
          "Grupo Cliente",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getGrupo().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add(
          "Cartera", expediente.getCartera() != null ? expediente.getCartera().getNombre() : null);
      this.add(
          "Contrato representante",
          expediente.getContrato() != null ? expediente.getContrato().getIdCarga() : null);
      /*if(expediente.getContrato().getPrimerInterviniente().getPersonaJuridica() != null && expediente.getContrato().getPrimerInterviniente().getPersonaJuridica())
            this.add("Primer titular", expediente.getContrato().getPrimerInterviniente().getRazonSocial() != null ? expediente.getContrato().getPrimerInterviniente().getRazonSocial() : null);
          else
            this.add("Primer titular", expediente.getContrato().getPrimerInterviniente().getNombre() + " " + expediente.getContrato().getPrimerInterviniente().getApellidos());
      */
      List<Bien> bienes = new ArrayList();
      if (expediente.getBienesPosesionNegociada() != null) {
        for (var bienPN : expediente.getBienesPosesionNegociada()) {
          bienes.add(bienPN.getBien());
        }
      }
      Iterator<Bien> bienIterator = bienes.iterator();
      while (bienIterator.hasNext()) {
        Bien bienProv = bienIterator.next();
        if (bienProv.getImporteBien() == null) {
          bienIterator.remove();
        }
      }

      List<Bien> bienesOrdenados =
          bienes.stream()
              .sorted(Comparator.comparingDouble(Bien::getImporteBien).reversed())
              .collect(Collectors.toList());

      Boolean obtenido = false;
      Boolean obtenidoSituacion = false;
      for (var bienSacado : bienesOrdenados) {
        for (var bienPNSacado : expediente.getBienesPosesionNegociada()) {
          if (obtenido == false && bienPNSacado.getBien().equals(bienSacado)) {
            for (var ocupanteSacado : bienPNSacado.getOcupantes()) {
              if (obtenido == false && ocupanteSacado.getTipoOcupante() != null) {
                if (ocupanteSacado.getTipoOcupante().getCodigo().equals("TIT")) {
                  if (ocupanteSacado.getOcupante().getNombreCompleto() != null) {
                    this.add("Primer titular", ocupanteSacado.getOcupante().getNombreCompleto());
                    obtenido = true;
                  }
                }
              }
              if (obtenidoSituacion == false && bienPNSacado.getBien().equals(bienSacado)) {
                if (bienPNSacado.getSituacion() != null
                    && bienPNSacado.getSituacion().getValor() != null) {
                  this.add("Situación", bienPNSacado.getSituacion().getValor());
                  obtenidoSituacion = true;
                }
              }
            }
          }
        }
      }

      this.add(
          "Gestor de formalización",
          expediente.getUsuarioByPerfil("Gestor formalización") != null
                  && expediente.getUsuarioByPerfil("Gestor formalización").getNombre() != null
              ? expediente.getUsuarioByPerfil("Gestor formalización").getNombre()
              : null);
      this.add(
          "Gestor de skip tracing",
          expediente.getUsuarioByPerfil("Gestor skip tracing") != null
                  && expediente.getUsuarioByPerfil("Gestor skip tracing").getNombre() != null
              ? expediente.getUsuarioByPerfil("Gestor skip tracing").getNombre()
              : null);
      this.add(
          "Responsable Cartera",
          responsableCartera != null ? responsableCartera.getNombre() : null);
      this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
      this.add(
          "Área de gestión",
          gestor != null && areaUsuario != null ? areaUsuario.getNombre() : null);
      this.add("Gestor del expediente", gestor != null ? gestor.getNombre() : null);
      this.add(
          "Estado",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
      this.add(
          "Subestado",
          expediente.getSubEstado() != null ? expediente.getSubEstado().getValor() : null);

      if (propuesta != null) {
        this.add(
            "Propuesta",
            propuesta.getTipoPropuesta() != null ? propuesta.getTipoPropuesta().getValor() : null);
        this.add(
            "Estado Propuesta",
            propuesta.getEstadoPropuesta() != null
                ? propuesta.getEstadoPropuesta().getValor()
                : null);
        this.add(
            "Fecha Validez",
            propuesta.getFechaValidez() != null ? propuesta.getFechaValidez() : null);
        if (propuesta.getEstrategia() != null) {
          this.add(
              "Estrategia",
              propuesta.getEstrategia().getEstrategia() != null
                  ? propuesta.getEstrategia().getEstrategia().getValor()
                  : null);
          this.add(
              "Tipo Estrategia",
              propuesta.getEstrategia().getTipoEstrategia() != null
                  ? propuesta.getEstrategia().getTipoEstrategia().getValor()
                  : null);
          this.add("Estimación", expediente.getEstimaciones().size());
          this.add("Importe Estrategia", null);
        }
      }
      /**
       * this.add("Judicial", expediente.getContrato()!=null ?
       * expediente.getContrato().getGastosJudiciales():null); this.add("Saldo en Gestión",
       * expediente.getSaldoGestion()); this.add("Importe Recuperado",
       * expediente.getImporteRecuperado()); this.add("Principal Pendiente",
       * expediente.getPrincipalPendiente()); this.add("Deuda Impagada", expediente.getContrato() !=
       * null ? expediente.getContrato().getDeudaImpagada() : null); this.add("Fecha Impago",
       * expediente.getContrato() != null ? expediente.getContrato().getFechaPrimerImpago() : null);
       * this.add("Situación", expediente.getContrato() != null ?
       * expediente.getContrato().getSituacion() != null ?
       * expediente.getContrato().getSituacion().getValor() : null : null);
       */
      String hitoP = "";
      String tipoP = "";
      if (p != null) {
        if (p.getHitoProcedimiento() != null) hitoP = p.getHitoProcedimiento().getValor();
        tipoP = p.getTipo().getValor();
      }

      this.add("Fase", hitoP);
      this.add("Tipo Procedimiento", tipoP);
      this.add("Acción", accion != null ? accion.getTitulo() : null);
      this.add("Fecha Acción", accion != null ? accion.getFechaCreacion() : null);
      this.add("Alerta", alerta != null ? alerta.getTitulo() : null);
      this.add("Fecha Alerta", alerta != null ? alerta.getFechaCreacion() : null);
      this.add("Actividad", actividad != null ? actividad.getTitulo() != null : null);
      this.add("Fecha Actividad", actividad != null ? actividad.getFechaCreacion() : null);
      this.add(
          "Fecha", expediente.getFechaCreacion() != null ? expediente.getFechaCreacion() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
