package com.haya.alaska.expediente.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ExpedienteMovimientoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Status");
    } else {
      cabeceras.add("Connection Id");
      cabeceras.add("Estado");
    }
  }

  public ExpedienteMovimientoExcel(Expediente expediente) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      this.add("Connection Id", expediente.getIdConcatenado());
      this.add(
        "Status",
        expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValorIngles() : null);

    } else {
      this.add("Id Expediente", expediente.getIdConcatenado());
      this.add(
          "Estado",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
