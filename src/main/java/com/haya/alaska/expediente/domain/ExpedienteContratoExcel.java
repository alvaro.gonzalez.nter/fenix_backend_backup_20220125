package com.haya.alaska.expediente.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ExpedienteContratoExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Connection Status");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Estado Expediente");
    }
  }

  public ExpedienteContratoExcel(Expediente expediente) {

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Connection Id", expediente.getIdConcatenado());
      this.add(
          "Connection Status",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValorIngles() : null);
    } else {
      this.add("Id Expediente", expediente.getIdConcatenado());
      this.add(
          "Estado Expediente",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
