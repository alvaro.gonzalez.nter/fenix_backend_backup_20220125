package com.haya.alaska.expediente.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;

public class ExpedienteCatalogoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      cabeceras.add("Connection Id");
      cabeceras.add("Contract");
      cabeceras.add("Contract situation");
      cabeceras.add("Portfolio manager");
      cabeceras.add("Supervisor");
      cabeceras.add("Connection Manager");
      cabeceras.add("Formalization manager");
      cabeceras.add("Skip tracing manager");
      cabeceras.add("Support profile");
      cabeceras.add("Strategy");
      cabeceras.add("Strategy Type");
      cabeceras.add("Management mode status");
      cabeceras.add("Management mode substatus");
      cabeceras.add("Situation");
      cabeceras.add("Contract campaign");

    } else {
      cabeceras.add("Id expediente");
      cabeceras.add("Contrato");
      cabeceras.add("Estado Contrato");
      cabeceras.add("Responsable de cartera");
      cabeceras.add("Supervisor");
      cabeceras.add("Gestor del expediente");
      cabeceras.add("Gestor de Formalización");
      cabeceras.add("Gestor de Skip Tracing");
      cabeceras.add("Perfil soporte");
      cabeceras.add("Estrategia");
      cabeceras.add("Tipo estrategia");
      cabeceras.add("Estado Modo Gestión");
      cabeceras.add("Sub-estado Modo Gestión");
      cabeceras.add("Situación");
      cabeceras.add("Campaña contrato");
    }
  }


  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
