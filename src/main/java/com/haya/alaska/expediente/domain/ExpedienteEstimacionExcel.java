package com.haya.alaska.expediente.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ExpedienteEstimacionExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Connection Status");
      cabeceras.add("Strategy Type");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Estado expediente");
      cabeceras.add("Tipo Estrategia");
    }
  }

  public ExpedienteEstimacionExcel(Expediente expediente) {

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Connection Id", expediente.getIdConcatenado());
      this.add(
          "Connection Status",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValorIngles() : null);
      this.add("Strategy Type", null);
    } else {
      this.add("Id Expediente", expediente.getIdConcatenado());
      this.add(
          "Estado expediente",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
      this.add("Tipo Estrategia", null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
