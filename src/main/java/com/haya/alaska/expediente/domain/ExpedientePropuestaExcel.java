package com.haya.alaska.expediente.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ExpedientePropuestaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Status");
      cabeceras.add("Date created");
      cabeceras.add("Closing date");

    } else {
      cabeceras.add("Id expediente");
      cabeceras.add("Estado");
      cabeceras.add("Fecha creacion");
      cabeceras.add("Fecha cierre");
    }
  }

  public ExpedientePropuestaExcel(Expediente expediente) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      this.add("Connection Id", expediente.getIdConcatenado());
      this.add(
        "Status",
        expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValorIngles() : null);
      this.add("Date created", expediente.getFechaCreacion());
      this.add("FClosing date", expediente.getFechaCierre());

      // cartera!=null ? expediente.getCartera().getNombre():null

    } else {

      this.add("Id expediente", expediente.getIdConcatenado());
      this.add(
          "Estado",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
      this.add("Fecha creacion", expediente.getFechaCreacion());
      this.add("Fecha cierre", expediente.getFechaCierre());

      // cartera!=null ? expediente.getCartera().getNombre():null
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
