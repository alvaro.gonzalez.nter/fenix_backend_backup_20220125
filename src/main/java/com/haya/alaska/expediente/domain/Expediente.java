package com.haya.alaska.expediente.domain;

import com.haya.alaska.agencia_master_servicing.domain.AgenciaMasterServicing;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.simulacion.domain.Simulacion;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Audited
@AuditTable(value = "HIST_MSTR_EXPEDIENTE")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_EXPEDIENTE")
public class Expediente implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "ID_ORIGEN")
  private String idOrigen;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE")
  @NotAudited
  private Set<Contrato> contratos = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE")
  @NotAudited
  private Set<AsignacionExpediente> asignaciones = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE_RECUPERACION_AMISTOSA")
  @NotAudited
  private Set<ExpedientePosesionNegociada> expedientesPosesionNegociada = new HashSet<>();

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;
  @Column(name = "ID_CARGA")
  private String idCarga;
  @Column(name = "DES_ID_CONCATENADO", unique = true)
  private String idConcatenado;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ESTADO")
  private TipoEstado tipoEstado;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_SUBESTADO")
  private SubtipoEstado subEstado;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE")
  @NotAudited
  private Set<Propuesta> propuestas = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE")
  @NotAudited
  private Set<Estimacion> estimaciones = new HashSet<>();

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CREACION")
  private Date fechaCreacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CIERRE")
  private Date fechaCierre;


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AGENCIA")
  private AgenciaMasterServicing agencia;


  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_EXPEDIENTE")
  @NotAudited
  private Set<Simulacion> simulaciones = new HashSet<>();

  public Expediente(String idOrigen, Cartera cartera) {
    this.idOrigen = idOrigen;
    this.cartera = cartera;
  }

  public void addContratos(List<Contrato> contratos) {
    if (this.contratos == null) {
      this.contratos = new HashSet<>();
    }
    this.contratos.addAll(contratos);
  }

  public Contrato getContratoRepresentante() {
    if (this.contratos == null || this.contratos.isEmpty()) {
      return new Contrato();
    }
    Set<Contrato> contratosExpendiente = this.getContratos();
    return contratosExpendiente
      .stream()
      .filter(Contrato::isRepresentante)
      .findFirst()
      .orElse(new Contrato());
  }

  public Usuario getGestor() {
    return this.getUsuarioByPerfil("Gestor");
  }

  public Usuario getSupervisor() {
    return this.getUsuarioByPerfil("Supervisor");
  }

  public String getUsuariosByPerfil(String perfil) {
    List<AsignacionExpediente> asignaciones = new ArrayList<>();
    for (AsignacionExpediente asignacion : this.asignaciones) {
      if (Boolean.TRUE.equals(asignacion.getActivo())) {
        Perfil perfilUsuario = asignacion.getUsuario().getPerfil();
        if (perfilUsuario != null && perfilUsuario.getNombre().equals(perfil)) {
          asignaciones.add(asignacion);
        }
      }
    }
    if(asignaciones != null && asignaciones.size() != 0) {
      String nombresConcatenados = asignaciones.stream().map(asignacionExpediente->asignacionExpediente.getUsuario().getNombre()).collect(Collectors.joining(", "));
      return nombresConcatenados;
    } else {
      return null;
    }
  }

  public Usuario getUsuarioByPerfil(String perfil) {
    List<AsignacionExpediente> asignaciones = new ArrayList<>();
    for (AsignacionExpediente asignacion : this.asignaciones) {
      if (Boolean.TRUE.equals(asignacion.getActivo())) {
        Perfil perfilUsuario = asignacion.getUsuario().getPerfil();
        if (perfilUsuario != null && perfilUsuario.getNombre().equals(perfil)) {
          asignaciones.add(asignacion);
        }
      }
    }

    AsignacionExpediente ae = asignaciones.stream().max(Comparator.comparing(AsignacionExpediente::getId)).orElse(new AsignacionExpediente());

    return ae.getUsuario();
  }

  /*
   * Cálculo del saldo en gestión de un expediente: Sumatorio del capital pendiente de vencer más
   * saldo en impago de los contratos que conforman el expediente. Información extraída del SALDO
   * del contrato (literal del funcional)
   * Si tenemos calculado el saldo en gestión para el saldo de un contrato, obtenemos este saldo en gestión.
   * En caso contrario, comprobamos los valores de capital pendiente de vencer y principal vencido impagado
   */
  public double getSaldoGestion() {
    double total = 0;
    for (Contrato contrato : this.contratos) {
      Saldo saldo = contrato.getSaldoContrato();
      if (saldo != null) {
        if (saldo.getSaldoGestion() != null) {
          total += saldo.getSaldoGestion();
        } else {
          if (saldo.getCapitalPendiente() != null) {
            total += saldo.getCapitalPendiente();
          }
          if (saldo.getPrincipalVencidoImpagado() != null) {
            total += saldo.getPrincipalVencidoImpagado();
          }
        }
      }
    }
    return total;
  }

  public double getPrincipalPendiente() {
    double total = 0;
    for (Contrato contrato : this.contratos) {
      Saldo saldo = contrato.getSaldoContrato();
      if (saldo != null) {
        if (saldo.getPrincipalVencidoImpagado() != null) {
          total += saldo.getPrincipalVencidoImpagado();
        }
      }
    }
    return total;
  }

  /**
   * Cálculo del importe recuperado de un expediente: Sumatorio de todos los movimientos de cobro
   * (movimientos de tipo abono) de todos los contratos que conforman el expediente. (literal del
   * funcional)
   */
  public double getImporteRecuperado() {
    double total = 0;
    for (Contrato contrato : this.contratos) {
      for (Movimiento movimiento : contrato.getMovimientos()) {
        if (movimiento.getTipo() != null && movimiento.getTipo().getCodigo().equals("2")) {
          if (movimiento.getImporte() != null && movimiento.getActivo() && movimiento.getAfecta() != null && movimiento.getAfecta()) {
            if(movimiento.getSentido().compareTo("-") == 0)
              total += movimiento.getImporte();
          }
        }
      }
    }
    return total;
  }

  /**
   * Devuelve un contrato por id o devuelve null si no lo encuentra
   *
   * @param idContrato Id del contrato que se quiere encontrar
   * @return Contrato encontrado dentro del expediente
   */
  public Contrato getContrato(Integer idContrato) {
    if (idContrato == null) {
      return null;
    }
    return this.contratos.stream().filter(c -> c.getId().equals(idContrato)).findFirst().orElse(null);
  }
}
