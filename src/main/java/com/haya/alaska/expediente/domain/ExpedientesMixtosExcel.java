package com.haya.alaska.expediente.domain;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.hito_procedimiento.domain.HitoProcedimiento;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.*;
import java.util.stream.Collectors;

public class ExpedientesMixtosExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  //private Evento alerta

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      cabeceras.add("Connection Id");
      cabeceras.add("Customer");
      cabeceras.add("Portfolio");
      cabeceras.add("Representative Loan");
      cabeceras.add("First holder");
      cabeceras.add("Portfolio manager");
      cabeceras.add("Supervisor");
      cabeceras.add("Management area");
      cabeceras.add("Connection Manager");
      cabeceras.add("Formalization manager");
      cabeceras.add("Skip tracing manager");
      cabeceras.add("Action");
      cabeceras.add("Date Action");
      cabeceras.add("Alert");
      cabeceras.add("Alert date");
      cabeceras.add("Activity");
      cabeceras.add("Activity date");
      cabeceras.add("Judicial");
      cabeceras.add("Judicial phase");
      cabeceras.add("Proposal");
      cabeceras.add("Proposed Status");
      cabeceras.add("Strategy");
      cabeceras.add("Strategy Type");
      cabeceras.add("Management mode status");
      cabeceras.add("Management mode substatus");
      cabeceras.add("Situation");
      cabeceras.add("Amount recovered");
      cabeceras.add("Outstanding principal");
      cabeceras.add("Unpaid debt");
      cabeceras.add("Balance in management");
      cabeceras.add("Recovered");

    } else {
      cabeceras.add("Id expediente");
      cabeceras.add("Cliente");
      cabeceras.add("Cartera");
      cabeceras.add("Contrato representante");
      cabeceras.add("Primer titular");
      cabeceras.add("Responsable de cartera");
      cabeceras.add("Supervisor");
      cabeceras.add("Área de gestión");
      cabeceras.add("Gestor del expediente");
      cabeceras.add("Gestor de Formalización");
      cabeceras.add("Gestor de Skip Tracing");
      cabeceras.add("Acción");
      cabeceras.add("Fecha acción");
      cabeceras.add("Alerta");
      cabeceras.add("Fecha alerta");
      cabeceras.add("Actividad");
      cabeceras.add("Fecha actividad");
      cabeceras.add("Judicializado");
      cabeceras.add("Fase judicialización");
      cabeceras.add("Propuesta");
      cabeceras.add("Propuesta estado");
      cabeceras.add("Estrategia");
      cabeceras.add("Tipo estrategia");
      cabeceras.add("Estado Modo Gestión");
      cabeceras.add("Sub-estado Modo Gestión");
      cabeceras.add("Situación");
      cabeceras.add("Importe recuperado");
      cabeceras.add("Principal pendiente");
      cabeceras.add("Deuda impagada");
      cabeceras.add("Saldo en gestión");
      cabeceras.add("Recuperado");
    }
  }

  public ExpedientesMixtosExcel(Expediente expediente, Evento alerta, Evento accion, Evento actividad) {
    super();

    Contrato contratoRepresentante = expediente.getContratoRepresentante();
    Cartera cartera = expediente.getCartera();
    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");
    Usuario gestorFormalizacion = expediente.getUsuarioByPerfil("Gestor formalización");
    Usuario gestorSkipTracing = expediente.getUsuarioByPerfil("Gestor skip tracing");
    Usuario responsableCartera = expediente.getUsuarioByPerfil("Responsable de cartera");
    Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      this.add("Connection Id", expediente.getIdConcatenado());
      this.add(
        "Customer",
        expediente.getCartera() != null
          ? expediente.getCartera().getClientes().stream()
          .map(clienteCartera -> clienteCartera.getCliente().getNombre())
          .collect(Collectors.joining(", "))
          : null);
      this.add("Portfolio", cartera != null ? expediente.getCartera().getNombre() : null);
      this.add("Representative Loan", contratoRepresentante.getIdCarga());
      this.add(
        "First holder",
        contratoRepresentante.getPrimerInterviniente().getNombre()
          + " "
          + contratoRepresentante.getPrimerInterviniente().getApellidos());
      this.add(
        "Portfolio manager",
        responsableCartera != null ? responsableCartera.getNombre() : null);
      this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
      String nombres = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          nombres += au.getNombre() + ", ";
        }
      }
      this.add("Management area", nombres);
      this.add("Connection Manager", gestor != null ? gestor.getNombre() : null);
      this.add(
        "Formalization manager",
        gestorFormalizacion != null ? gestorFormalizacion.getNombre() : null);
      this.add(
        "Skip tracing manager",
        gestorSkipTracing != null ? gestorSkipTracing.getNombre() : null);
      this.add("Action", accion != null ? accion.getTitulo() : null);
      this.add("Date Action", accion != null ? accion.getFechaCreacion() : null);
      this.add("Alert", alerta != null ? alerta.getTitulo() : null);
      this.add("Alert date", alerta != null ? alerta.getFechaCreacion() : null);
      this.add("Activity", actividad != null ? actividad.getTitulo() : null);
      this.add("Activity date", actividad != null ? actividad.getFechaCreacion() : null);

      String procedimientojudicial = "";
      List<Procedimiento> procedimientos = new ArrayList<>();
      for (Contrato c : expediente.getContratos()) {
        procedimientos.addAll(c.getProcedimientos());
      }
      if (!procedimientos.isEmpty()) {
        procedimientojudicial = "YES";
      } else {
        procedimientojudicial = "NO";
      }

      HitoProcedimiento hitoMax = null;

      for (Procedimiento procedimiento : procedimientos) {
        HitoProcedimiento hp = procedimiento.getHitoProcedimiento();
        if (hp != null) {
          String[] partes = hp.getCodigo().split("_");
          Integer hitoActual = Integer.parseInt(partes[1]);
          if (hitoMax == null || hitoMax.getNumero() < hitoActual) {
            hitoMax = hp;
          }
        }
      }

      this.add("Judicial", procedimientojudicial);
      this.add("Judicial phase", hitoMax != null ? hitoMax.getValorIngles() : null);

      List<Propuesta> propuestas = new ArrayList<>(expediente.getPropuestas());
      String propuestasB = "NO";
      EstadoPropuesta estadoPropuesta = new EstadoPropuesta();
      if (!propuestas.isEmpty()) {
        propuestasB = "YES";
        Date fecha = propuestas.get(0).getFechaAlta();
        for (Propuesta propuesta : propuestas) {
          estadoPropuesta = propuesta.getEstadoPropuesta();
          if (propuesta.getFechaAlta().after(fecha)) {
            estadoPropuesta = propuesta.getEstadoPropuesta();
            fecha = propuesta.getFechaAlta();
          }
        }
      }

      this.add(
        "Proposal",
        propuestasB); // Comprobar si el expediente Posesión negociada tiene propuestas SI/NO
      this.add(
        "Proposed Status",
        estadoPropuesta
          .getValorIngles()); // De todas las propuestas el estado más avanzado a partir de la fecha
      EstrategiaAsignada ea = contratoRepresentante.getEstrategia();
      this.add(
        "Strategy",
        ea != null ? ea.getEstrategia() != null ? ea.getEstrategia().getValorIngles() : null : null);
      this.add(
        "Strategy Type",
        ea != null
          ? ea.getTipoEstrategia() != null ? ea.getTipoEstrategia().getValorIngles() : null
          : null);
      this.add(
        "Management mode status",
        expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValorIngles() : null);
      this.add(
        "Management mode substatus",
        expediente.getSubEstado() != null ? expediente.getSubEstado().getValorIngles() : null);
      this.add(
        "Situation",
        contratoRepresentante.getSituacion() != null
          ? contratoRepresentante.getSituacion().getValorIngles()
          : null);
      this.add("Balance in management", expediente.getSaldoGestion());
      this.add("Amount recovered", expediente.getImporteRecuperado());
      this.add("Principal pendiente", expediente.getPrincipalPendiente());
      this.add("Unpaid debt", contratoRepresentante.getDeudaImpagada());
      this.add(
        "Balance in management",
        contratoRepresentante.getSaldoContrato() != null
          ? contratoRepresentante.getSaldoContrato().getSaldoGestion()
          : null);
      this.add("Recovered", expediente.getImporteRecuperado());

    } else {

      this.add("Id expediente", expediente.getIdConcatenado());
      this.add(
          "Cliente",
          expediente.getCartera() != null
              ? expediente.getCartera().getClientes().stream()
                  .map(clienteCartera -> clienteCartera.getCliente().getNombre())
                  .collect(Collectors.joining(", "))
              : null);
      this.add("Cartera", cartera != null ? expediente.getCartera().getNombre() : null);
      this.add("Contrato representante", contratoRepresentante.getIdCarga());
      this.add(
          "Primer titular",
          contratoRepresentante.getPrimerInterviniente().getNombre()
              + " "
              + contratoRepresentante.getPrimerInterviniente().getApellidos());
      this.add(
          "Responsable de cartera",
          responsableCartera != null ? responsableCartera.getNombre() : null);
      this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
      String nombres = "";
      if (gestor != null && gestor.getAreas() != null) {
        for (AreaUsuario au : gestor.getAreas()) {
          nombres += au.getNombre() + ", ";
        }
      }
      this.add("Área de gestión", nombres);
      this.add("Gestor del expediente", gestor != null ? gestor.getNombre() : null);
      this.add(
          "Gestor de Formalización",
          gestorFormalizacion != null ? gestorFormalizacion.getNombre() : null);
      this.add(
          "Gestor de Skip Tracing",
          gestorSkipTracing != null ? gestorSkipTracing.getNombre() : null);
      this.add("Acción", accion != null ? accion.getTitulo() : null);
      this.add("Fecha acción", accion != null ? accion.getFechaCreacion() : null);
      this.add("Alerta", alerta != null ? alerta.getTitulo() : null);
      this.add("Fecha alerta", alerta != null ? alerta.getFechaCreacion() : null);
      this.add("Actividad", actividad != null ? actividad.getTitulo() : null);
      this.add("Fecha actividad", actividad != null ? actividad.getFechaCreacion() : null);

      String procedimientojudicial = "";
      List<Procedimiento> procedimientos = new ArrayList<>();
      for (Contrato c : expediente.getContratos()) {
        procedimientos.addAll(c.getProcedimientos());
      }
      if (!procedimientos.isEmpty()) {
        procedimientojudicial = "SI";
      } else {
        procedimientojudicial = "NO";
      }

      HitoProcedimiento hitoMax = null;

      for (Procedimiento procedimiento : procedimientos) {
        HitoProcedimiento hp = procedimiento.getHitoProcedimiento();
        if (hp != null) {
          String[] partes = hp.getCodigo().split("_");
          Integer hitoActual = Integer.parseInt(partes[1]);
          if (hitoMax == null || hitoMax.getNumero() < hitoActual) {
            hitoMax = hp;
          }
        }
      }

      this.add("Judicializado", procedimientojudicial);
      this.add("Fase judicialización", hitoMax != null ? hitoMax.getValor() : null);

      List<Propuesta> propuestas = new ArrayList<>(expediente.getPropuestas());
      String propuestasB = "NO";
      EstadoPropuesta estadoPropuesta = new EstadoPropuesta();
      if (!propuestas.isEmpty()) {
        propuestasB = "SI";
        Date fecha = propuestas.get(0).getFechaAlta();
        for (Propuesta propuesta : propuestas) {
          estadoPropuesta = propuesta.getEstadoPropuesta();
          if (propuesta.getFechaAlta().after(fecha)) {
            estadoPropuesta = propuesta.getEstadoPropuesta();
            fecha = propuesta.getFechaAlta();
          }
        }
      }

      this.add(
          "Propuesta",
          propuestasB); // Comprobar si el expediente Posesión negociada tiene propuestas SI/NO
      this.add(
          "Propuesta estado",
          estadoPropuesta
              .getValor()); // De todas las propuestas el estado más avanzado a partir de la fecha
      EstrategiaAsignada ea = contratoRepresentante.getEstrategia();
      this.add(
          "Estrategia",
          ea != null ? ea.getEstrategia() != null ? ea.getEstrategia().getValor() : null : null);
      this.add(
          "Tipo estrategia",
          ea != null
              ? ea.getTipoEstrategia() != null ? ea.getTipoEstrategia().getValor() : null
              : null);
      this.add(
          "Estado Modo Gestión",
          expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
      this.add(
          "Sub-estado Modo Gestión",
          expediente.getSubEstado() != null ? expediente.getSubEstado().getValor() : null);
      this.add(
          "Situación",
          contratoRepresentante.getSituacion() != null
              ? contratoRepresentante.getSituacion().getValor()
              : null);
      this.add("Saldo en gestión", expediente.getSaldoGestion());
      this.add("Importe recuperado", expediente.getImporteRecuperado());
      this.add("Principal pendiente", expediente.getPrincipalPendiente());
      this.add("Deuda impagada", contratoRepresentante.getDeudaImpagada());
      this.add(
          "Saldo en gestión",
          contratoRepresentante.getSaldoContrato() != null
              ? contratoRepresentante.getSaldoContrato().getSaldoGestion()
              : null);
      this.add("Recuperado", expediente.getImporteRecuperado());
    }
  }

  /*public ExpedientesMixtosExcel(ExpedientePosesionNegociada expediente, Evento alerta, Evento accion, Evento actividad) {
    super();

    Contrato contratoRepresentante = expediente.getContratoRepresentante();
    Cartera cartera = expediente.getCartera();
    Usuario gestor = expediente.getUsuarioByPerfil("Gestor");
    Usuario gestorFormalizacion = expediente.getUsuarioByPerfil("Gestor formalización");
    Usuario gestorSkipTracing = expediente.getUsuarioByPerfil("Gestor skip tracing");
    Usuario responsableCartera = expediente.getUsuarioByPerfil("Responsable de cartera");
    Usuario supervisor = expediente.getUsuarioByPerfil("Supervisor");



    this.add("Id expediente", expediente.getIdConcatenado());
    this.add("Cliente", expediente.getCartera() !=null ? expediente.getCartera().getClientes().stream().map(clienteCartera -> clienteCartera.getCliente().getNombre()).collect(Collectors.joining(", ")):null);
    this.add("Cartera",cartera!=null ? expediente.getCartera().getNombre():null);
    this.add("Contrato representante", contratoRepresentante.getId());
    this.add("Primer titular", contratoRepresentante.getPrimerInterviniente().getNombre() + " " + contratoRepresentante.getPrimerInterviniente().getApellidos());
    this.add("Responsable de cartera", responsableCartera != null ? responsableCartera.getNombre() : null);
    this.add("Supervisor", supervisor != null ? supervisor.getNombre() : null);
    String nombres = "";
    if (gestor != null && gestor.getAreas() != null){
      for (AreaUsuario au : gestor.getAreas()){
        nombres += au.getNombre() + ", ";

      }
    }
    this.add(
      "Área de gestión",
      nombres);
    this.add("Gestor del expediente", gestor != null ? gestor.getNombre() : null);
    this.add("Gestor de Formalización", gestorFormalizacion != null ? gestorFormalizacion.getNombre() : null);
    this.add("Gestor de Skip Tracing", gestorSkipTracing != null ? gestorSkipTracing.getNombre() : null);
    this.add("Acción", accion != null ? accion.getTitulo() : null);
    this.add("Fecha acción", accion != null ? accion.getFechaCreacion() : null);
    this.add("Alerta", alerta != null ? alerta.getTitulo() : null);
    this.add("Fecha alerta", alerta != null ? alerta.getFechaCreacion() : null);
    this.add("Actividad", actividad != null ? actividad.getTitulo() : null);
    this.add("Fecha actividad", actividad != null ? actividad.getFechaCreacion() : null);

    String procedimientojudicial = "";
    List<Procedimiento> procedimientos = new ArrayList<>();
    for (Contrato c : expediente.getContratos()){
      procedimientos.addAll(c.getProcedimientos());
    }
    if (!procedimientos.isEmpty()) {
      procedimientojudicial = "SI";
    } else {
      procedimientojudicial = "NO";
    }

    HitoProcedimiento hitoMax = null;

    for (Procedimiento procedimiento : procedimientos) {
      HitoProcedimiento hp = procedimiento.getHitoProcedimiento();
      if (hp != null){
        String[] partes = hp.getCodigo().split("_");
        Integer hitoActual = Integer.parseInt(partes[1]);
        if (hitoMax == null || hitoMax.getNumero() < hitoActual){
          hitoMax = hp;
        }
      }
    }

    this.add("Judicializado", procedimientojudicial);
    this.add("Fase judicialización", hitoMax != null ? hitoMax.getValor() : null);

    List<Propuesta> propuestas = new ArrayList<>(expediente.getPropuestas());
    String propuestasB = "NO";
    EstadoPropuesta estadoPropuesta = new EstadoPropuesta();
    if (!propuestas.isEmpty()){
      propuestasB = "SI";
      Date fecha = propuestas.get(0).getFechaAlta();
      for (Propuesta propuesta : propuestas) {
        estadoPropuesta = propuesta.getEstadoPropuesta();
        if (propuesta.getFechaAlta().after(fecha)) {
          estadoPropuesta = propuesta.getEstadoPropuesta();
          fecha = propuesta.getFechaAlta();
        }
      }
    }

    this.add(
      "Propuesta",
      propuestasB); // Comprobar si el expediente Posesión negociada tiene propuestas SI/NO
    this.add(
      "Propuesta estado",
      estadoPropuesta.getValor()); // De todas las propuestas el estado más avanzado a partir de la fecha
    EstrategiaAsignada ea = contratoRepresentante.getEstrategia();
    this.add("Estrategia", ea != null ? ea.getEstrategia()!= null ? ea.getEstrategia().getValor() :null:null);
    this.add("Tipo estrategia", ea!= null ? ea.getTipoEstrategia() != null ? ea.getTipoEstrategia().getValor():null:null);
    this.add("Estado Modo Gestión", expediente.getTipoEstado() != null ? expediente.getTipoEstado().getValor() : null);
    this.add("Sub-estado Modo Gestión", expediente.getSubEstado() != null ? expediente.getSubEstado().getValor() : null);
    this.add("Situación", contratoRepresentante.getSituacion() != null ? contratoRepresentante.getSituacion().getValor() : null);
    this.add("Saldo en gestión", expediente.getSaldoGestion());
    this.add("Importe recuperado", expediente.getImporteRecuperado());
    this.add("Principal pendiente", expediente.getPrincipalPendiente());
    this.add("Deuda impagada", contratoRepresentante.getDeudaImpagada());
    this.add("Saldo en gestión", contratoRepresentante.getSaldoContrato() != null ? contratoRepresentante.getSaldoContrato().getSaldoGestion() : null);
    this.add("Recuperado", expediente.getImporteRecuperado());
  }*/

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
