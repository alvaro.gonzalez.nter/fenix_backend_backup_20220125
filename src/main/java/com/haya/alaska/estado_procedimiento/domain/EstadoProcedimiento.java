package com.haya.alaska.estado_procedimiento.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_ESTADO_PROCEDIMIENTO")
@Entity
@Table(name = "LKUP_ESTADO_PROCEDIMIENTO")
public class EstadoProcedimiento extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_PROCEDIMIENTO")
  @NotAudited
  private Set<Procedimiento> procedimientos = new HashSet<>();
}
