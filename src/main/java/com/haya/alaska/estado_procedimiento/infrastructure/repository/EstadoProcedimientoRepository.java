package com.haya.alaska.estado_procedimiento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.estado_procedimiento.domain.EstadoProcedimiento;

@Repository
public interface EstadoProcedimientoRepository extends CatalogoRepository<EstadoProcedimiento, Integer> {}
