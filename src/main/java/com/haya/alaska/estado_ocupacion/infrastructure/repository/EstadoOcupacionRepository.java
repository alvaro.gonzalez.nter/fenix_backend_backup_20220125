package com.haya.alaska.estado_ocupacion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoOcupacionRepository extends CatalogoRepository<EstadoOcupacion, Integer> {}
