package com.haya.alaska.estado_ocupacion.domain;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_ESTADO_OCUPACION")
@Entity
@Table(name = "LKUP_ESTADO_OCUPACION")
@NoArgsConstructor
public class EstadoOcupacion extends Catalogo {
  public final static String PENDIENTE_DE_INFORMAR="PEN";
  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_OCUPACION")
  @NotAudited
  private Set<BienPosesionNegociada> bienes = new HashSet<>();

}
