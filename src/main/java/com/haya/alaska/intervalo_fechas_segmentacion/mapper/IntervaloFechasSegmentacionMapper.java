package com.haya.alaska.intervalo_fechas_segmentacion.mapper;

import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.IntervaloFecha;
import com.haya.alaska.intervalo_fechas_segmentacion.controller.dto.IntervaloFechasSegmentacionDTO;
import com.haya.alaska.intervalo_fechas_segmentacion.domain.IntervaloFechasSegmentacion;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class IntervaloFechasSegmentacionMapper {

  public Set<IntervaloFechasSegmentacion> listaDtoAListaEntidad (List<IntervaloFechasSegmentacionDTO> input) {

    if(input == null)
      return null;

    Set<IntervaloFechasSegmentacion> fechas = new HashSet<>();
    for(IntervaloFechasSegmentacionDTO fecha : input) {
      if(fecha == null) continue;
      IntervaloFechasSegmentacion intervalo = new IntervaloFechasSegmentacion();
      BeanUtils.copyProperties(fecha,intervalo);
      fechas.add(intervalo);
    }
    return fechas;
  }

  public List<IntervaloFechasSegmentacionDTO> listaEntidadAListaDto(Set<IntervaloFechasSegmentacion> entidades) {

    if(entidades == null)
      return null;

    List<IntervaloFechasSegmentacionDTO> dtoList = new ArrayList<>();
    for(IntervaloFechasSegmentacion fecha : entidades) {
      if(fecha == null) continue;
      IntervaloFechasSegmentacionDTO dto = new IntervaloFechasSegmentacionDTO();
      BeanUtils.copyProperties(fecha,dto);
      dtoList.add(dto);
    }
    return dtoList;
  }

  public IntervaloFechasSegmentacion dtoAEntidad(IntervaloFechasSegmentacionDTO dto) {
    IntervaloFechasSegmentacion entidad = new IntervaloFechasSegmentacion();
    if(dto!=null)
      BeanUtils.copyProperties(dto,entidad);
    else
      entidad=null;

    return entidad;
  }

  public IntervaloFechasSegmentacionDTO entidadADto(IntervaloFechasSegmentacion entidad) {
    IntervaloFechasSegmentacionDTO dto = new IntervaloFechasSegmentacionDTO();
    if(entidad!=null)
      BeanUtils.copyProperties(entidad,dto);
    else
      dto=null;

    return dto;
  }
}
