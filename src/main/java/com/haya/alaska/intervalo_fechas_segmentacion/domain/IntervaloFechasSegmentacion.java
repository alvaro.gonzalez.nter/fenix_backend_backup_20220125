package com.haya.alaska.intervalo_fechas_segmentacion.domain;

import com.haya.alaska.segmentacion.domain.Segmentacion;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_INTERVALO_FECHAS_SEGMENTACION")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_INTERVALO_FECHAS_SEGMENTACION")
public class IntervaloFechasSegmentacion {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name="DES_DESCRIPCION")
  private String descripcion;

  @Column(name="FCH_INICIO")
  @Temporal(TemporalType.DATE)
  private Date fechaInicio;

  @Column(name="FCH_FIN")
  @Temporal(TemporalType.DATE)
  private Date fechaFin;

  /*@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_EXPEDIENTE")
  private Segmentacion segmentacionExpediente;*/

  /*@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_CONTRATO")
  private Segmentacion segmentacionContrato;*/

  /*@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_INTERVINIENTE")
  private Segmentacion segmentacionInterviniente;*/

  /*@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_GARANTIA")
  private Segmentacion segmentacionGarantia;*/

  /*@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SEGMENTACION_PROCEDIMIENTO")
  private Segmentacion segmentacionProcedimiento;*/
}
