package com.haya.alaska.intervalo_fechas_segmentacion.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class IntervaloFechasSegmentacionDTO {

  private String descripcion;
  private Date fechaInicio;
  private Date fechaFin;

}
