package com.haya.alaska.intervalo_estimacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

/** @author agonzalez */
@Audited
@AuditTable(value = "HIST_LKUP_INTERVALO_ESTIMACION")
@Entity
@Table(name = "LKUP_INTERVALO_ESTIMACION")
@NoArgsConstructor
public class IntervaloEstimacion extends Catalogo {
}
