package com.haya.alaska.intervalo_estimacion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.intervalo_estimacion.domain.IntervaloEstimacion;
import org.springframework.stereotype.Repository;

@Repository
public interface IntervaloEstimacionRepository extends CatalogoRepository<IntervaloEstimacion, Integer> {}
