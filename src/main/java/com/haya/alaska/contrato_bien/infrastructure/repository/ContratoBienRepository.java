package com.haya.alaska.contrato_bien.infrastructure.repository;

import com.haya.alaska.contrato_bien.domain.ContratoBien;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ContratoBienRepository extends JpaRepository<ContratoBien, Integer> {
    ContratoBien findByBienIdAndContratoId(Integer idBien, Integer idContrato);

	  Optional<ContratoBien> findByContratoIdAndBienId(
	          Integer idContrato, Integer idBien);

	Page<ContratoBien> findAllDistinctByContratoIdInAndBienTramitadoIsFalse(Pageable pageable, List<Integer> contratosIds);
	int countDistinctByContratoIdInAndBienTramitadoIsFalse(List<Integer> contratosIds);
	Page<ContratoBien> findAllBy(Pageable page);
}
