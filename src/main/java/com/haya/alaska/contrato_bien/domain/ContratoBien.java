package com.haya.alaska.contrato_bien.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.liquidez.domain.Liquidez;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_RELA_CONTRATO_BIEN")
@Entity
@Getter
@Setter
@Table(name = "RELA_CONTRATO_BIEN")
public class ContratoBien  implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;


  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO")
  private Contrato contrato;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  private Bien bien;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_GARANTIA")
  private TipoGarantia tipoGarantia;

  @Column(name = "IMP_GARANTIZADO")
  private Double importeGarantizado;

  @Column(name = "NUM_RESP_HIPOTECARIA")
  private Double responsabilidadHipotecaria;

  @Column(name = "NUM_RANGO")
  private Double rango;

  @Column(name = "IND_ESTADO")
  private Boolean estado; //true = activo, false = liberado

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LIQUIDEZ")
  private Liquidez liquidez;
}
