package com.haya.alaska.subestado_formalizacion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.subestado_formalizacion.domain.SubestadoFormalizacion;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubestadoFormalizacionRepository extends CatalogoRepository<SubestadoFormalizacion, Integer> {
  List<SubestadoFormalizacion> findAllByEstadoPropuestaId(Integer id);
  List<SubestadoFormalizacion> findAllByEstadoPropuestaIdAndActivoIsTrue(Integer id);
}
