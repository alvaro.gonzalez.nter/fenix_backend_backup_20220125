package com.haya.alaska.subestado_formalizacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Audited
@AuditTable(value = "HIST_LKUP_SUBESTADO_FORMALIZACION")
@Entity
@Getter
@Setter
@Table(name = "LKUP_SUBESTADO_FORMALIZACION")
public class SubestadoFormalizacion extends Catalogo {
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_PROPUESTA", nullable = false)
  EstadoPropuesta estadoPropuesta;
}
