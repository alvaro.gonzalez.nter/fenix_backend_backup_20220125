package com.haya.alaska.tipo_contacto.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.dato_contacto_skip_tracing.domain.DatoContactoST;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_CONTACTO")
@Entity
@Table(name = "LKUP_TIPO_CONTACTO")
public class TipoContacto extends Catalogo {

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_CONTACTO")
  @NotAudited
  private Set<DatoContactoST> datoContactoSTSet = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_CONTACTO")
  @NotAudited
  private Set<IntervinienteST> intervinienteSTSet = new HashSet<>();
}
