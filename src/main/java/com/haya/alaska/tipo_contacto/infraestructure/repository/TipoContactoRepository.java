package com.haya.alaska.tipo_contacto.infraestructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_contacto.domain.TipoContacto;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoContactoRepository extends CatalogoRepository<TipoContacto,Integer> {
}
