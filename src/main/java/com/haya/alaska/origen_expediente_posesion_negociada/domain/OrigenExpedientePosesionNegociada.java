package com.haya.alaska.origen_expediente_posesion_negociada.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.estrategia_asignada.domain.EstrategiaAsignada;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_ORIGEN_EXPEDIENTE_POSESION_NEGOCIADA")
@Entity
@Getter
@Table(name = "LKUP_ORIGEN_EXPEDIENTE_POSESION_NEGOCIADA")
public class OrigenExpedientePosesionNegociada extends Catalogo {
  public static final int AMISTOSA=1;
  public static final int NEGOCIADA=2;
}
