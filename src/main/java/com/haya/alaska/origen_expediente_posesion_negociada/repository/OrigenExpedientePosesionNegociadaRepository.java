package com.haya.alaska.origen_expediente_posesion_negociada.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.origen_expediente_posesion_negociada.domain.OrigenExpedientePosesionNegociada;
import org.springframework.stereotype.Repository;

@Repository
public interface OrigenExpedientePosesionNegociadaRepository extends CatalogoRepository<OrigenExpedientePosesionNegociada, Integer> {}
