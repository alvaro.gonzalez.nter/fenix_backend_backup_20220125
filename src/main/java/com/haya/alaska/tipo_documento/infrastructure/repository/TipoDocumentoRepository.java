package com.haya.alaska.tipo_documento.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_documento.domain.TipoDocumento;

@Repository
public interface TipoDocumentoRepository extends CatalogoRepository<TipoDocumento, Integer> {
	  
}
