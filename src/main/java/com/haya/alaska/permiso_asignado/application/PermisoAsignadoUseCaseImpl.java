package com.haya.alaska.permiso_asignado.application;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.expediente.infrastructure.controller.dto.ReasignacionGestorInputDTO;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.infrastructure.repository.PerfilRepository;
import com.haya.alaska.permiso.domain.Permiso;
import com.haya.alaska.permiso.infrastructure.repository.PermisoRepository;
import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.*;
import com.haya.alaska.permiso_asignado.infrastructure.mapper.PermisoAsignadoMapper;
import com.haya.alaska.permiso_asignado.infrastructure.repository.PermisoAsignadoRepository;
import com.haya.alaska.permiso_disponible.domain.PermisoDisponible;
import com.haya.alaska.permiso_disponible.infrastructure.repository.PermisoDisponibleRepository;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.application.UsuarioUseCase;
import com.haya.alaska.usuario.application.UsuarioUseCaseImpl;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class PermisoAsignadoUseCaseImpl implements PermisoAsignadoUseCase {
  @Autowired
  PerfilRepository perfilRepository;
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  PermisoAsignadoRepository permisoAsignadoRepository;
  @Autowired
  PermisoRepository permisoRepository;
  @Autowired
  PermisoAsignadoMapper permisoAsignadoMapper;
  @Autowired
  PermisoDisponibleRepository permisoDisponibleRepository;
  @Autowired
  UsuarioUseCaseImpl usuarioUseCase;
  @Autowired
  UsuarioRepository usuarioRepository;
  //CRUD Perfil

  @Override
  public List<PermisoDisponibleOutputDTO> getPermisosDisponibles(){
    List<Permiso> permisos = permisoRepository.findAll();
    List<PermisoDisponibleOutputDTO> result = new ArrayList<>();
    for (Permiso permiso : permisos){
      List<PermisoDisponible> pds = permiso.getPermisosDisponibles();
      List<OpcionOutputDTO> opDTO = pds.stream().map(pd ->
        permisoAsignadoMapper.opcionToDTO(pd)
      ).collect(Collectors.toList());
      result.add(permisoAsignadoMapper.permisoDisponibleToDTO(permiso, opDTO));
    }
    return result;
  }

  @Override
  public List<PerfilCompletoDTO> getAll(boolean todos, boolean todosPN, ReasignacionGestorInputDTO busquedaDto, CustomUserDetails principal) throws NotFoundException {

    List<Perfil> perfiles = new ArrayList<>();
    if(busquedaDto != null || todos || todosPN) {
      List<Usuario> usuarios = usuarioRepository.findAll();
      usuarios = usuarioUseCase.getUsuarios(todos,todosPN,busquedaDto,principal,usuarios);
      for(var usuario: usuarios) {
        if(!perfiles.contains(usuario.getPerfil()) && usuario.getPerfil().getActivo()) perfiles.add(usuario.getPerfil());
      }
    } else {
      perfiles = perfilRepository.findAll();
    }

    List<PerfilCompletoDTO> result = perfiles.stream().map(PerfilCompletoDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public PerfilOutputDTO getPermisosAsignados(Integer idPerfil, Integer idCartera){
    List<PermisoAsignado> pa = permisoAsignadoRepository.findByPerfilIdAndCarteraId(idPerfil, idCartera);
    if (pa.size() == 0) return new PerfilOutputDTO();
    List<PermisoDisponible> permisosDisponibles = pa.stream().map(PermisoAsignado::getPermisoDisponible).collect(Collectors.toList());
    List<PermisoDisponibleOutputDTO> arrayPDODTO = new ArrayList<>();
    Map<Permiso, List<OpcionOutputDTO>> groupedPermisos = new HashMap<>();
    for (PermisoDisponible pd : permisosDisponibles){
      List<OpcionOutputDTO> prueba = groupedPermisos.get(pd.getPermiso());
      if (prueba == null){
        List<OpcionOutputDTO> insert = new ArrayList<>();
        insert.add(permisoAsignadoMapper.opcionToDTO(pd));
        groupedPermisos.put(pd.getPermiso(), insert);
      } else {
        prueba.add(permisoAsignadoMapper.opcionToDTO(pd));
        groupedPermisos.replace(pd.getPermiso(), prueba);
      }
    }

    for (Map.Entry<Permiso, List<OpcionOutputDTO>> entry : groupedPermisos.entrySet()) {
      Permiso key = entry.getKey();
      List<OpcionOutputDTO> value = entry.getValue();
      arrayPDODTO.add(permisoAsignadoMapper.permisoDisponibleToDTO(key, value));
    }

    PerfilOutputDTO result = permisoAsignadoMapper.perfilToDTO(pa.get(0), arrayPDODTO);

    return result;
  }

  @Override
  public PerfilOutputDTO getPermisosAsignadosSinCartera(Integer idPerfil){
    List<PermisoAsignado> pa = permisoAsignadoRepository.findByPerfilIdAndCarteraIsNull(idPerfil);
    if (pa.size() == 0) return new PerfilOutputDTO();
    List<PermisoDisponible> permisosDisponibles = pa.stream().map(PermisoAsignado::getPermisoDisponible).collect(Collectors.toList());
    List<PermisoDisponibleOutputDTO> arrayPDODTO = new ArrayList<>();
    Map<Permiso, List<OpcionOutputDTO>> groupedPermisos = new HashMap<>();
    for (PermisoDisponible pd : permisosDisponibles){
      List<OpcionOutputDTO> prueba = groupedPermisos.get(pd.getPermiso());
      if (prueba == null){
        List<OpcionOutputDTO> insert = new ArrayList<>();
        insert.add(permisoAsignadoMapper.opcionToDTO(pd));
        groupedPermisos.put(pd.getPermiso(), insert);
      } else {
        prueba.add(permisoAsignadoMapper.opcionToDTO(pd));
        groupedPermisos.replace(pd.getPermiso(), prueba);
      }
    }

    for (Map.Entry<Permiso, List<OpcionOutputDTO>> entry : groupedPermisos.entrySet()) {
      Permiso key = entry.getKey();
      List<OpcionOutputDTO> value = entry.getValue();
      arrayPDODTO.add(permisoAsignadoMapper.permisoDisponibleToDTO(key, value));
    }

    PerfilOutputDTO result = permisoAsignadoMapper.perfilToDTO(pa.get(0), arrayPDODTO);

    return result;
  }

  @Override
  public PerfilOutputDTO updatePermisosAsignados(PerfilInputDTO input) throws NotFoundException {
    Cartera cartera;
    Perfil perfil;
    if (input.getCartera() != null){
      cartera = carteraRepository.findById(input.getCartera()).orElseThrow(() ->
        new NotFoundException("Cartera", input.getCartera()));
      perfil = perfilRepository.findById(input.getPerfil()).orElseThrow(() ->
        new NotFoundException("Perfil", input.getPerfil()));
      permisoAsignadoRepository.deleteByPerfilIdAndCarteraId(input.getPerfil(), input.getCartera());
    } else {
      cartera = null;
      perfil = perfilRepository.findById(input.getPerfil()).orElseThrow(() ->
        new NotFoundException("Perfil", input.getPerfil()));
      permisoAsignadoRepository.deleteByPerfilIdAndCarteraIsNull(input.getPerfil());
    }

    for (Integer pdId : input.getPermisos()){
      PermisoDisponible pd = permisoDisponibleRepository.findById(pdId).orElseThrow(() ->
        new NotFoundException("PermisoDisponible", pdId));
      PermisoAsignado pa = new PermisoAsignado();
      pa.setCartera(cartera);
      pa.setPerfil(perfil);
      pa.setPermisoDisponible(pd);
      permisoAsignadoRepository.saveAndFlush(pa);
    }

    return getPermisosAsignados(input.getPerfil(), input.getCartera());
  }

  @Override
  public void updateCarteraPerfiles(Integer idCartera, List<Integer> idsPerfiles) throws NotFoundException {
    Cartera cartera = carteraRepository.findById(idCartera).orElseThrow(() ->
      new NotFoundException("Cartera", idCartera));
    Set<Perfil> perfiles = new HashSet<>();
    for (Integer id : idsPerfiles) {
      perfiles.add(perfilRepository.findById(id).orElseThrow(() ->
        new NotFoundException("Perfil", id)));
    }
    cartera.setPerfiles(new HashSet<>());
    carteraRepository.saveAndFlush(cartera);
    cartera.setPerfiles(perfiles);
    carteraRepository.saveAndFlush(cartera);
  }

  @Override
  public List<PerfilSimpleOutputDTO> getPerfiles(Integer idCartera) throws NotFoundException {
    Cartera cartera = carteraRepository.findById(idCartera).orElseThrow(() ->
      new NotFoundException("Cartera", idCartera));

    Set<Perfil> perfiles = cartera.getPerfiles();

    return perfiles.stream().map(PerfilSimpleOutputDTO::new).collect(Collectors.toList());
  }
}
