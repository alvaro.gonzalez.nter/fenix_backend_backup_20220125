package com.haya.alaska.permiso_asignado.application;

import com.haya.alaska.expediente.infrastructure.controller.dto.ReasignacionGestorInputDTO;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PerfilCompletoDTO;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PerfilInputDTO;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PerfilOutputDTO;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PermisoDisponibleOutputDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;

import java.util.List;

public interface PermisoAsignadoUseCase {
  List<PermisoDisponibleOutputDTO> getPermisosDisponibles();
  PerfilOutputDTO getPermisosAsignados(Integer idPerfil, Integer idCartera);
  PerfilOutputDTO getPermisosAsignadosSinCartera(Integer idPerfil);
  PerfilOutputDTO updatePermisosAsignados(PerfilInputDTO input) throws NotFoundException;
  void updateCarteraPerfiles(Integer idCartera, List<Integer> idsPerfiles) throws NotFoundException;
  List<PerfilSimpleOutputDTO> getPerfiles(Integer idCartera) throws NotFoundException;
  List<PerfilCompletoDTO> getAll(boolean todos, boolean todosPN, ReasignacionGestorInputDTO busquedaDto, CustomUserDetails principal) throws NotFoundException;
}
