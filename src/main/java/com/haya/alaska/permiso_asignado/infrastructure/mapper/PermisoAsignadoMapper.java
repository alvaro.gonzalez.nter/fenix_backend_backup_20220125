package com.haya.alaska.permiso_asignado.infrastructure.mapper;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.permiso.domain.Permiso;
import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.OpcionOutputDTO;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PerfilOutputDTO;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PermisoDisponibleOutputDTO;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PermisoOutputDTO;
import com.haya.alaska.permiso_disponible.domain.PermisoDisponible;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

@Component
public class PermisoAsignadoMapper {
  public PerfilOutputDTO perfilToDTO(PermisoAsignado permisoAsignado, List<PermisoDisponibleOutputDTO> permisos){
    PerfilOutputDTO result = new PerfilOutputDTO();
    Locale loc = LocaleContextHolder.getLocale();
    result.setPerfil(permisoAsignado.getPerfil().getNombre());
    if (loc.getLanguage().equals("en"))
      result.setPerfil(permisoAsignado.getPerfil().getNombreIngles());
    result.setCartera(permisoAsignado.getCartera() != null ? permisoAsignado.getCartera().getNombre() : null);
    result.setPermisos(permisos);
    return result;
  }

  public PermisoDisponibleOutputDTO permisoDisponibleToDTO(Permiso permiso, List<OpcionOutputDTO> opDTO){
    PermisoDisponibleOutputDTO result = new PermisoDisponibleOutputDTO();
    result.setPermiso(permisoToDTO(permiso));
    result.setOpciones(opDTO);
    return result;
  }

  public PermisoOutputDTO permisoToDTO(Permiso permiso){
    PermisoOutputDTO result = new PermisoOutputDTO();
    result.setPermiso(new CatalogoMinInfoDTO(permiso));
    result.setPermisoPadre(permiso.getPermisoPadre() != null ? new CatalogoMinInfoDTO(permiso.getPermisoPadre()) : null);
    return result;
  }

  public OpcionOutputDTO opcionToDTO(PermisoDisponible pd){
    OpcionOutputDTO result = new OpcionOutputDTO();
    result.setId(pd.getId());
    result.setOpcion(new CatalogoMinInfoDTO(pd.getOpcionPermiso()));
    return result;
  }
}
