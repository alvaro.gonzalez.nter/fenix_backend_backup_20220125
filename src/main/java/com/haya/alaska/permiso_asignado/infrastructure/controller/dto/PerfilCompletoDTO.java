package com.haya.alaska.permiso_asignado.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.perfil.domain.Perfil;
import lombok.*;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PerfilCompletoDTO implements Serializable {
  Integer id;
  String nombre;
  CatalogoMinInfoDTO areaPerfil;
  Boolean activo;

  public PerfilCompletoDTO(Perfil perfil){
    this.id = perfil.getId();
    Locale loc = LocaleContextHolder.getLocale();
    this.nombre = perfil.getNombre();
    if (loc.getLanguage().equals("en"))
      this.nombre = perfil.getNombreIngles();
    this.areaPerfil = perfil.getAreaPerfil() != null ? new CatalogoMinInfoDTO(perfil.getAreaPerfil()) : null;
    this.activo = perfil.getActivo();
  }
}
