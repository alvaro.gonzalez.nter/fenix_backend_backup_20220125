package com.haya.alaska.permiso_asignado.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OpcionOutputDTO implements Serializable {
  Integer id;
  CatalogoMinInfoDTO opcion;
}
