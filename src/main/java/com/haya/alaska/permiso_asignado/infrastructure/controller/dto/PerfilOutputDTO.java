package com.haya.alaska.permiso_asignado.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PerfilOutputDTO implements Serializable{
  String perfil;
  String cartera;
  List<PermisoDisponibleOutputDTO> permisos = new ArrayList<>();
}
