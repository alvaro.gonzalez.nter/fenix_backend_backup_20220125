package com.haya.alaska.permiso_asignado.infrastructure.repository;

import java.util.List;

import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface PermisoAsignadoRepository extends JpaRepository<PermisoAsignado, Integer> {
  List<PermisoAsignado> findByPerfilId(Integer perfil);
  List<PermisoAsignado> findAllByPerfilId(Integer perfil);
  List<PermisoAsignado> findByPerfilIdAndCarteraId(Integer perfil, Integer cartera);
  List<PermisoAsignado> findByPerfilIdAndCarteraIdAndPermisoDisponiblePermisoId(Integer perfil, Integer cartera, Integer permiso);
  List<PermisoAsignado> findByPerfilIdAndCarteraIsNull(Integer perfil);

  @Transactional
  void deleteByPerfilIdAndCarteraId(Integer idPerfil, Integer idContrato);
  @Transactional
  void deleteByPerfilIdAndCarteraIsNull(Integer idPerfil);
}
