package com.haya.alaska.permiso_asignado.infrastructure.controller;

import com.haya.alaska.expediente.infrastructure.controller.dto.ReasignacionGestorInputDTO;
import com.haya.alaska.permiso_asignado.application.PermisoAsignadoUseCase;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PerfilCompletoDTO;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PerfilInputDTO;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PerfilOutputDTO;
import com.haya.alaska.permiso_asignado.infrastructure.controller.dto.PermisoDisponibleOutputDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.infrastructure.controller.dto.PerfilSimpleOutputDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/roles")
public class PermisoAsignadoController {
  @Autowired
  PermisoAsignadoUseCase permisoAsignadoUseCase;

  @ApiOperation(value = "Obtener permisos asignados generales", notes = "Devuelve todos los permisos del perfil enviado")
  @GetMapping("/asignados")
  public PerfilOutputDTO getPerfilesAsignados(@RequestParam(value = "idPerfil") Integer idPerfil) throws NotFoundException {
    return permisoAsignadoUseCase.getPermisosAsignadosSinCartera(idPerfil);
  }

  @ApiOperation(value = "Obtener permisos asignados de la cartera", notes = "Devuelve todos los permisos del perfil enviado en la cartera enviada")
  @GetMapping("/asignados/{idCartera}")
  public PerfilOutputDTO getPerfilesAsignadosCartera(@RequestParam(value = "idPerfil") Integer idPerfil,
                                              @PathVariable("idCartera") Integer idCartera) throws NotFoundException {
    return permisoAsignadoUseCase.getPermisosAsignados(idPerfil, idCartera);
  }

  @ApiOperation(value = "Obtener permisos disponibles", notes = "Obtiene una lista de todos los permisos ")
  @GetMapping("/disponibles")
  public List<PermisoDisponibleOutputDTO> getPermisosDisponibles(){
    return permisoAsignadoUseCase.getPermisosDisponibles();
  }

  @ApiOperation(value = "Obtener todos los permisos", notes = "Obtiene todos los permiso.")
  @GetMapping("/all")
  public List<PerfilCompletoDTO> getAllPermisos(@ApiIgnore CustomUserDetails principal) throws NotFoundException {
    return permisoAsignadoUseCase.getAll(false, false, null, principal);
  }

  @ApiOperation(value = "Obtener todos los permisos", notes = "Obtiene todos los permiso.")
  @PostMapping("/all")
  public List<PerfilCompletoDTO> getAllPermisosBusqueda(
    @RequestParam boolean todos,
    @RequestParam boolean todosPN,
    @RequestBody ReasignacionGestorInputDTO busquedaDto,
    @ApiIgnore CustomUserDetails principal
  ) throws NotFoundException {
    return permisoAsignadoUseCase.getAll(todos, todosPN, busquedaDto, principal);
  }

  @ApiOperation(value = "Actualizar asignados", notes = "Actualiza los Permisos asignados del perfil y la cartera pasados en el input")
  @PutMapping("/asignados")
  public PerfilOutputDTO updatePermitido(@RequestBody @Valid PerfilInputDTO perfilInputDTO) throws NotFoundException {
    return permisoAsignadoUseCase.updatePermisosAsignados(perfilInputDTO);
  }

  @ApiOperation(value = "Actualizar perfiles", notes = "Actualiza los perfiles relacionados con la cartera")
  @PutMapping("/perfil")
  public void updatePermitido(@RequestParam(value = "idCartera") Integer idCartera,
                                         @RequestParam(value = "idsPerfiles") List<Integer> idsPerfiles) throws NotFoundException {
    permisoAsignadoUseCase.updateCarteraPerfiles(idCartera, idsPerfiles);
  }

  @ApiOperation(value = "Obtener perfiles de la Cartera", notes = "Obtiene una lista de los perfiles asignados a la cartera.")
  @GetMapping("/perfil/{idCartera}")
  public List<PerfilSimpleOutputDTO> getPerfiles(@PathVariable("idCartera") Integer idCartera) throws NotFoundException {
    return permisoAsignadoUseCase.getPerfiles(idCartera);
  }
}
