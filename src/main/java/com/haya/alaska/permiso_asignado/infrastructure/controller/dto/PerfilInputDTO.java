package com.haya.alaska.permiso_asignado.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PerfilInputDTO implements Serializable {
  Integer perfil;
  Integer cartera;
  List<Integer> permisos;
}
