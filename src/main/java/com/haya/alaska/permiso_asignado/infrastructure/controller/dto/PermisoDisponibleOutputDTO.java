package com.haya.alaska.permiso_asignado.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PermisoDisponibleOutputDTO implements Serializable {
  PermisoOutputDTO permiso;
  List<OpcionOutputDTO> opciones;
}
