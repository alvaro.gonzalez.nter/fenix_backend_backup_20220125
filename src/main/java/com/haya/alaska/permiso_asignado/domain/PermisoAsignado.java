package com.haya.alaska.permiso_asignado.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.permiso_disponible.domain.PermisoDisponible;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_RELA_PERMISO_ASIGNADO")
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "RELA_PERMISO_ASIGNADO",
  uniqueConstraints = {@UniqueConstraint(columnNames = {
    "ID_PERFIL", "ID_CARTERA", "ID_PERMISO_DISPONIBLE"
  })})
public class PermisoAsignado implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID", length = 50)
  private Integer id;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERFIL")
  private Perfil perfil;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PERMISO_DISPONIBLE")
  private PermisoDisponible permisoDisponible;
}
