package com.haya.alaska.sancion_propuesta.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.sancion_propuesta.domain.SancionPropuesta;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SancionPropuestaRepository extends CatalogoRepository<SancionPropuesta, Integer> {



  @Query(
    value =
      "SELECT REV AS registro FROM HIST_LKUP_SANCION_PROPUESTA " +
        "WHERE ID = ?1",
    nativeQuery = true)
  List<Map> historicoSancionPropuesta(Integer idSancion);





}
