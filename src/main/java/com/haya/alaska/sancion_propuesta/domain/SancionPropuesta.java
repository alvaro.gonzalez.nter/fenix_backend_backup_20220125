package com.haya.alaska.sancion_propuesta.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * SancionPropuesta entity
 *
 * @author agonzalez
 */
@Audited
@AuditTable(value = "HIST_LKUP_SANCION_PROPUESTA")
@NoArgsConstructor
@Entity
@Table(name = "LKUP_SANCION_PROPUESTA")
public class SancionPropuesta extends Catalogo implements Serializable {
}