package com.haya.alaska.tipo_carga.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_carga.domain.TipoCarga;

@Repository
public interface TipoCargaRepository extends CatalogoRepository<TipoCarga, Integer> {
	 
}
