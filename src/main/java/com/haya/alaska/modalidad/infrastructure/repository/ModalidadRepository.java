package com.haya.alaska.modalidad.infrastructure.repository;


import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.modalidad.domain.Modalidad;
import org.springframework.stereotype.Repository;

@Repository
public interface ModalidadRepository extends CatalogoRepository<Modalidad, Integer> {

}
