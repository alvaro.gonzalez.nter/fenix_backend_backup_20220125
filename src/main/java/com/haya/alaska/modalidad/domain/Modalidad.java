package com.haya.alaska.modalidad.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.visita.domain.Visita;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_MODALIDAD")
@Entity
@Table(name = "LKUP_MODALIDAD")
public class Modalidad extends Catalogo {
  public final static String INMOBILIARIO = "1";
  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MODALIDAD")
  @NotAudited
  private Set<Visita> visitas = new HashSet<>();
}
