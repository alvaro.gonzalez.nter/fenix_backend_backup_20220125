package com.haya.alaska.subtipo_gestor.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.subtipo_gestor.domain.SubtipoGestor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubtipoGestorRepository extends CatalogoRepository<SubtipoGestor, Integer> {
  List<SubtipoGestor> findAllById(Integer id);
}
