package com.haya.alaska.subtipo_gestor.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_SUBTIPO_GESTOR")
@Entity
@Table(name = "LKUP_SUBTIPO_GESTOR")
@Getter
@Setter
@NoArgsConstructor
public class SubtipoGestor extends Catalogo{

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SUBTIPO_GESTOR")
  @NotAudited
  private Set<Usuario> usuario = new HashSet<>();
}
