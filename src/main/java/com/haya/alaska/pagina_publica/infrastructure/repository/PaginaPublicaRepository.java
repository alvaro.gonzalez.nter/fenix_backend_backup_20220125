package com.haya.alaska.pagina_publica.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.pagina_publica.domain.PaginaPublica;
import org.springframework.stereotype.Repository;

@Repository
public interface PaginaPublicaRepository extends CatalogoRepository<PaginaPublica, Integer> {}
