package com.haya.alaska.dato_contacto.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class DatosContactoOcupanteExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Occupant Id");
      cabeceras.add("Asset Id");
      cabeceras.add("Contact Data Identification");
      cabeceras.add("Fixed Phone");
      cabeceras.add("Fixed Phone Validated");
      cabeceras.add("Mobile Phone");
      cabeceras.add("Mobile Phone Validated");
      cabeceras.add("Email");
      cabeceras.add("Email Validated");
      cabeceras.add("Origin");
      cabeceras.add("Update Date");
    } else {
      cabeceras.add("Id Ocupante");
      cabeceras.add("Id Activo");
      cabeceras.add("Identificacion de Datos de Contacto");
      cabeceras.add("Telefono Fijo");
      cabeceras.add("Fijo Validado");
      cabeceras.add("Telefono Movil");
      cabeceras.add("Movil Validado");
      cabeceras.add("Email");
      cabeceras.add("Email Validado");
      cabeceras.add("Origen");
      cabeceras.add("Fecha Actualizacion");
    }
  }

  public DatosContactoOcupanteExcel(DatoContacto datoContacto) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Occupant Id", datoContacto.getOcupante().getId());
      this.add("Asset Id", datoContacto.getActivo());
      this.add("Contact Data Identification", datoContacto.getId());
      this.add("Fixed Phone", datoContacto.getFijo());
      this.add(
          "Fixed Phone Validated",
          Boolean.TRUE.equals(datoContacto.getFijoValidado()) ? "YES" : "NO");
      this.add("Mobile Phone", datoContacto.getMovil());
      this.add(
          "Mobile Phone Validated",
          Boolean.TRUE.equals(datoContacto.getMovilValidado()) ? "YES" : "NO");
      this.add("Email", datoContacto.getEmail());
      this.add(
          "Email Validated", Boolean.TRUE.equals(datoContacto.getEmailValidado()) ? "YES" : "NO");
      this.add("Origin", datoContacto.getOrigen());
      this.add("Update Date", datoContacto.getFechaActualizacion());
    } else {
      this.add("Id Ocupante", datoContacto.getOcupante().getId());
      this.add("Id Activo", datoContacto.getActivo());
      this.add("Identificacion de Datos de Contacto", datoContacto.getId());
      this.add("Telefono Fijo", datoContacto.getFijo());
      this.add("Fijo Validado", Boolean.TRUE.equals(datoContacto.getFijoValidado()) ? "SI" : "NO");
      this.add("Telefono Movil", datoContacto.getMovil());
      this.add(
          "Movil Validado", Boolean.TRUE.equals(datoContacto.getMovilValidado()) ? "SI" : "NO");
      this.add("Email", datoContacto.getEmail());
      this.add(
          "Email Validado", Boolean.TRUE.equals(datoContacto.getEmailValidado()) ? "SI" : "NO");
      this.add("Origen", datoContacto.getOrigen());
      this.add("Fecha Actualizacion", datoContacto.getFechaActualizacion());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
