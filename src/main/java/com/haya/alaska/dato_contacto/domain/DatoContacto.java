package com.haya.alaska.dato_contacto.domain;

import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.ocupante.domain.Ocupante;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_DATO_CONTACTO")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_DATO_CONTACTO")
public class DatoContacto implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "DES_FIJO")
  private String fijo;
  @Column(name = "IND_FIJO_VALIDADO")
  private Boolean fijoValidado;
  @Column(name = "DES_MOVIL")
  private String movil;
  @Column(name = "IND_MOVIL_VALIDADO")
  private Boolean movilValidado;
  @Column(name = "DES_EMAIL")
  private String email;
  @Column(name = "IND_EMAIL_VALIDADO")
  private Boolean emailValidado;
  @Column(name = "DES_ORIGEN")
  private String origen;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ACTUALIZACION")
  private Date fechaActualizacion;
  @Column(name = "NUM_ORDEN")
  private Integer orden;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ALTA")
  private Date fechaAlta;

  @ManyToOne
  @JoinColumn(name = "ID_INTERVINIENTE")
  private Interviniente interviniente;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_OCUPANTE")
  private Ocupante ocupante;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;


  @Override
  public boolean equals(Object o) {
    DatoContacto datoContacto = (DatoContacto) o;
    if (this.getOcupante().getId().equals(datoContacto.getOcupante().getId())) {
      if (this.getEmailNull().equals(datoContacto.getEmailNull())
        && this.getMovilNull().equals(datoContacto.getMovilNull())
        && this.getFijoNull().equals(datoContacto.getFijoNull())) {
        return true;
      }
      return false;
    }
    return false;
  }

  private String getEmailNull() {
    if (this.email == null) return "";
    return this.email;
  }

  private String getMovilNull() {
    if (this.movil == null) return "";
    return this.movil;
  }

  private String getFijoNull() {
    if (this.fijo == null) return "";
    return this.fijo;
  }
}
