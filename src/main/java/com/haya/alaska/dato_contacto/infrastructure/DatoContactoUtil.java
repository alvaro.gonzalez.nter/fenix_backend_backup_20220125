package com.haya.alaska.dato_contacto.infrastructure;

import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.dato_contacto.infrastructure.mapper.DatoContactoMapper;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DatoContactoUtil {

  @Autowired private DatoContactoRepository datoContactoRepository;
  @Autowired private DatoContactoMapper datoContactoMapper;

  public List<DatoContacto> createDatosContactoToInterviniente(
      List<DatosContactoDTOToList> datosContactoDto, Interviniente interviniente)
      throws NotFoundException, RequiredValueException {
    List<DatoContacto> datosContacto =
        datoContactoMapper.dTOListToEntityList(datosContactoDto, interviniente);
    List<DatoContacto> datosContactoSaved = new ArrayList<>();
    for (DatoContacto datoContacto : datosContacto) {
      datoContacto.setInterviniente(interviniente);
      datosContactoSaved.add(datoContactoRepository.saveAndFlush(datoContacto));
    }
    interviniente.addDatosContactos(datosContactoSaved);
    return datosContactoSaved;
  }

  public List<DatoContacto> createDatosContactoToOcupante(
    List<DatosContactoDTOToList> datosContactoDto, Ocupante ocupante)
    throws NotFoundException, RequiredValueException {
    List<DatoContacto> datosContacto =
      datoContactoMapper.dTOListToEntityListOcupante(datosContactoDto, ocupante);
    List<DatoContacto> datosContactoSaved = new ArrayList<>();
    for (DatoContacto datoContacto : datosContacto) {
      datoContacto.setOcupante(ocupante);
      datosContactoSaved.add(datoContactoRepository.saveAndFlush(datoContacto));
    }
    ocupante.addDatosContactos(datosContactoSaved);
    return datosContactoSaved;
  }

  public boolean checkDatoContactoOrder(
      DatosContactoDTOToList datosContacto, Integer idInterviniente) {
    List<DatoContacto> datosContactoList =
        datoContactoRepository.findByIntervinienteId(idInterviniente);
    if (datosContactoList.isEmpty()) {
      return true;
    }
    for (DatoContacto datoContacto : datosContactoList) {
      if (datosContacto.getId() != datosContacto.getId()
          && datoContacto.getOrden() == datosContacto.getOrden()) return false;
    }
    return true;
  }

  public boolean checkDatoContactoOrderOcupante(
    DatosContactoDTOToList datosContacto, Integer idOcupante) {
    List<DatoContacto> datosContactoList =
      datoContactoRepository.findByOcupanteId(idOcupante);
    if (datosContactoList.isEmpty()) {
      return true;
    }
    for (DatoContacto datoContacto : datosContactoList) {
      if (datosContacto.getId() != datosContacto.getId()
        && datoContacto.getOrden() == datosContacto.getOrden()) return false;
    }
    return true;
  }

  public DatoContacto createDatoContactoToInterviniente(
      DatosContactoDTOToList datoContactoDto, Interviniente interviniente)
      throws NotFoundException {

    DatoContacto datoContacto = datoContactoMapper.datoContactoDTOToListToEntity(datoContactoDto, interviniente);

    datoContacto.setInterviniente(interviniente);
    DatoContacto datoContactoSaved = datoContactoRepository.saveAndFlush(datoContacto);
    interviniente.addDatoContacto(datoContactoSaved);

    return datoContactoSaved;
  }

  public DatoContacto createDatoContactoToOcupante(
    DatosContactoDTOToList datoContactoDto, Ocupante ocupante)
    throws NotFoundException {

    DatoContacto datoContacto = datoContactoMapper.datoContactoDTOToListToEntityOcupante(datoContactoDto, ocupante);

    datoContacto.setOcupante(ocupante);
    DatoContacto datoContactoSaved = datoContactoRepository.saveAndFlush(datoContacto);
    ocupante.addDatoContacto(datoContactoSaved);

    return datoContactoSaved;
  }

  public DatoContacto updateFields(DatoContacto datosContacto, Interviniente interviniente) {
    DatoContacto datoContactoUpdated = new DatoContacto();
    BeanUtils.copyProperties(datosContacto, datoContactoUpdated);
    datoContactoUpdated.setInterviniente(interviniente);
    datoContactoUpdated.setFechaActualizacion(new Date());
    return datoContactoRepository.save(datoContactoUpdated);
  }

  public DatoContacto updateFieldsOcupante(DatoContacto datosContacto, Ocupante ocupante) {
    DatoContacto datoContactoUpdated = new DatoContacto();
    BeanUtils.copyProperties(datosContacto, datoContactoUpdated);
    datoContactoUpdated.setOcupante(ocupante);
    datoContactoUpdated.setFechaActualizacion(new Date());
    return datoContactoRepository.save(datoContactoUpdated);
  }
}
