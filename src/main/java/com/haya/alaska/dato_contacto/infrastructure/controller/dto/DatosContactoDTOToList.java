package com.haya.alaska.dato_contacto.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class DatosContactoDTOToList implements Serializable {

  private static final long serialVersionUID = 1L;
  private Integer id;
  private String fijo;
  private Boolean fijoValidado;
  private String movil;
  private Boolean movilValidado;
  private String email;
  private Boolean emailValidado;
  private String origen;
  private Date fechaActualizacion;
  private Integer orden;
  private Date fechaAlta;

}
