package com.haya.alaska.dato_contacto.infrastructure.mapper;

import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.DatoContactoUtil;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component
public class DatoContactoMapper {

  @Autowired DatoContactoUtil datoContactoUtil;

  public static DatosContactoDTOToList parseEntityToListDTO(DatoContacto datoContacto) {
    DatosContactoDTOToList datosContactoDTOToList = new DatosContactoDTOToList();
    BeanUtils.copyProperties(datoContacto, datosContactoDTOToList);
    datosContactoDTOToList.setFechaAlta(datoContacto.getFechaAlta());
/*    datosContactoDTOToList.setId(datoContacto.getId());
    datosContactoDTOToList.setFijo(datoContacto.getFijo());
    datosContactoDTOToList.setFijoValidado(datoContacto.getFijoValidado());
    datosContactoDTOToList.setMovil(datoContacto.getMovil());
    datosContactoDTOToList.setMovilValidado(datoContacto.getMovilValidado());
    datosContactoDTOToList.setEmail(datoContacto.getEmail());
    datosContactoDTOToList.setEmailValidado(datoContacto.getEmailValidado());
    datosContactoDTOToList.setOrigen(datoContacto.getOrigen());
    datosContactoDTOToList.setFechaActualizacion(datoContacto.getFechaActualizacion());
    datosContactoDTOToList.setOrden(datoContacto.getOrden());*/

    return datosContactoDTOToList;
  }

  public DatoContacto datoContactoDTOToListToEntity(DatosContactoDTOToList datosContactoDTOToList, Interviniente interviniente) {
    DatoContacto datoContacto = new DatoContacto();

    BeanUtils.copyProperties(datosContactoDTOToList, datoContacto);
    datoContacto.setOrigen("Manual");
    datoContacto.setFechaActualizacion(new Date());
    datoContacto.setFechaAlta(new Date());
    datoContacto.setInterviniente(interviniente);
    return datoContacto;
  }

  public DatoContacto datoContactoDTOToListToEntityOcupante(DatosContactoDTOToList datosContactoDTOToList, Ocupante ocupante) {
    DatoContacto datoContacto = new DatoContacto();
    datoContacto.setOrigen("Manual");
    datoContacto.setFechaActualizacion(new Date());

    BeanUtils.copyProperties(datosContactoDTOToList, datoContacto);
    datoContacto.setFechaAlta(new Date());
    datoContacto.setOcupante(ocupante);
    return datoContacto;
  }

  public DatoContacto parse(DatosContactoDTOToList datosContactoDTOToList) {
    DatoContacto datoContacto = new DatoContacto();
    datoContacto.setOrigen("Manual");
    datoContacto.setFechaActualizacion(new Date());
    //datoContacto.setFechaAlta(datosContactoDTOToList.getFechaAlta());
    //datoContacto.set
    BeanUtils.copyProperties(datosContactoDTOToList, datoContacto);
    datoContacto.setFechaAlta(datosContactoDTOToList.getFechaAlta());

    return datoContacto;
  }

  public List<DatoContacto> dTOListToEntityList(
      List<DatosContactoDTOToList> datosContacto, Interviniente interviniente)
      throws RequiredValueException {
    List<DatoContacto> list = new ArrayList<>();
    for (DatosContactoDTOToList datosContactoDTOToList : datosContacto) {
      if (!datoContactoUtil.checkDatoContactoOrder(datosContactoDTOToList, interviniente.getId())){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new RequiredValueException("There is already a contact data for the order:" + datosContactoDTOToList.getOrden());
        else throw new RequiredValueException("Ya existe un dato contacto para el orden: " + datosContactoDTOToList.getOrden());
      }
      DatoContacto parse = datoContactoDTOToListToEntity(datosContactoDTOToList, interviniente);
      list.add(parse);
    }
    return list;
  }

  public List<DatoContacto> dTOListToEntityListOcupante(
    List<DatosContactoDTOToList> datosContacto, Ocupante ocupante)
    throws RequiredValueException {
    List<DatoContacto> list = new ArrayList<>();
    for (DatosContactoDTOToList datosContactoDTOToList : datosContacto) {
      if (!datoContactoUtil.checkDatoContactoOrder(datosContactoDTOToList, ocupante.getId())){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new RequiredValueException("There is already a contact data for the order:" + datosContactoDTOToList.getOrden());
        else throw new RequiredValueException("Ya existe un dato contacto para el orden: " + datosContactoDTOToList.getOrden());
      }
      DatoContacto parse = datoContactoDTOToListToEntityOcupante(datosContactoDTOToList, ocupante);
      list.add(parse);
    }
    return list;
  }
}
