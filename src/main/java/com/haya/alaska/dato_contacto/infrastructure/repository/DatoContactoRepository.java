package com.haya.alaska.dato_contacto.infrastructure.repository;

import com.haya.alaska.dato_contacto.domain.DatoContacto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DatoContactoRepository extends JpaRepository<DatoContacto, Integer> {

  List<DatoContacto> findByIntervinienteId(Integer idInterviniente);
  List<DatoContacto> findByOcupanteId(Integer idOcupante);
  List<DatoContacto> findByOcupanteIdNotNull();
  DatoContacto findByEmail(String email);
  List<DatoContacto> findAllByIntervinienteIdAndOrigen(Integer idInterviniente, String origen);
}
