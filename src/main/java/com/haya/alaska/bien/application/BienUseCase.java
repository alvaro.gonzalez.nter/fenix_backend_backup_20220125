package com.haya.alaska.bien.application;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.controller.dto.*;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.shared.dto.BienDTOToList;
import com.haya.alaska.shared.dto.BienExtendedDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;


public interface BienUseCase {

  ListWithCountDTO<BienDTOToList> getAllFilteredBienes(
          boolean esGarantia,
          Integer idContrato,
          Integer id,
          String idOrigen,
          String tipoActivo,
          Boolean estado,
          String fincaRegistral,
          String localidad,
          String provincia,
          String valor,
          String descripcion,
          String responsabilidad,
          String liquidez,
          Boolean posesionNegociada,
          String orderField,
          String orderDirection,
          Integer size,
          Integer page);

  ListWithCountDTO<BienExtendedDTOToList> getAllFilteredBienesExtended(boolean esGarantia, Integer idContrato, Integer id, String idOrigen, String tipoActivo, Boolean estado, String fincaRegistral, String localidad, String provincia, String valor, String descripcion, String responsabilidad, String liquidez, Boolean posesionNegociada, String orderField, String orderDirection, Integer size, Integer page);

  List<Bien> filterBienesInMemory(
          Integer id,
          String idOrigen,
          Boolean esGarantia,
          Integer idContrato,
          String tipoActivo,
          Boolean estado,
          String fincaRegistral,
          String localidad,
          String provincia,
          String valor,
          String descripcion,
          String responsabilidad,
          String liquidez,
          Boolean posesionNegociada,
          String orderField,
          String orderDirection
  );

  BienOutputDto findBienByIdDto(Integer id, Integer idContrato) throws IllegalAccessException;

  BienOutputDto findBienByIdOrigenOrIdOrigen2Dto(String idOrigen) throws IllegalAccessException;

  BienOutputDto findBienByIdCargaDto(String idHaya) throws IllegalAccessException;

  BienOutputDto update(Integer idBien, Integer idContrato, BienInputDto bienDTO) throws Exception;

  BienGeolocalizacionOutputDto updateGeolocalizacion(Integer idBien, BienGeolocalizacionInputDto bienDTO) throws Exception;

  BienOutputDto create(Integer idContrato, BienInputDto bienDTO) throws RequiredValueException, NotFoundException, InvalidCodeException;

  void delete(Integer idBien, Integer idContrato);

  LinkedHashMap<String, List<Collection<?>>> findExcelBienById(Integer idContrato);

  LinkedHashMap<String, List<Collection<?>>> findExcelBienByIdBien(Integer idContrato, Integer idBien);

  void createDocumento(Integer idBien, MultipartFile file, MetadatoDocumentoInputDTO metadatoDocumento, Usuario usuario,String tipoDocumento) throws NotFoundException, IOException;

  ListWithCountDTO<DocumentoDTO> findDocumentosByContratoId(Integer idContrato,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size, Integer page) throws NotFoundException, IOException;

  ListWithCountDTO<DocumentoDTO> findDocumentosByExpedienteId(Integer id,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion, String nombreArchivo, String idHaya, String orderDirection,String orderField,Integer size, Integer page) throws NotFoundException, IOException;

  ListWithCountDTO<DocumentoDTO> findDocumentosByBienId(Integer idBien,String idDocumento,String usuarioCreador,String tipo, String idEntidad,String fechaActualizacion,String orderDirection,String orderField,Integer size,Integer page) throws NotFoundException,IOException;

  ListWithCountDTO<BienDTOToList> getAll(Integer size, Integer page, List<Integer> contratos, String referencia);

  ListWithCountDTO<BienVinculacionDto> findVinculadosBienById(Integer idBien) throws NotFoundException;

  List<BienDTOToList> getAllByExpedienteId(Integer idExpediente);
}
