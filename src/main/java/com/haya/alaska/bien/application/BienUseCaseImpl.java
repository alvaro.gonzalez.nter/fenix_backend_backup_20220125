package com.haya.alaska.bien.application;

import com.haya.alaska.bien.domain.*;
import com.haya.alaska.bien.infrastructure.controller.dto.*;
import com.haya.alaska.bien.infrastructure.mapper.BienMapper;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.domain.CargaGarantiaExcel;
import com.haya.alaska.carga.domain.CargaSolvenciaExcel;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.Contrato_;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.domain.ContratoBien_;
import com.haya.alaska.contrato_bien.infrastructure.repository.ContratoBienRepository;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.application.GestorDocumentalUseCase;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.domain.enums.TipoEntidadEnum;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.liquidez.domain.Liquidez_;
import com.haya.alaska.localidad.domain.Localidad_;
import com.haya.alaska.provincia.domain.Provincia_;
import com.haya.alaska.shared.dto.BienDTOToList;
import com.haya.alaska.shared.dto.BienExtendedDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.domain.TasacionGarantiaExcel;
import com.haya.alaska.tasacion.domain.TasacionSolvenciaExcel;
import com.haya.alaska.tipo_activo.domain.TipoActivo_;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.valoracion.domain.ValoracionGarantiaExcel;
import com.haya.alaska.valoracion.domain.ValoracionSolvenciaExcel;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.StringUtil;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class BienUseCaseImpl implements BienUseCase {

  @PersistenceContext private EntityManager entityManager;

  @Autowired private BienMapper bienMapper;
  @Autowired private ServicioGestorDocumental servicioGestorDocumental;

  @Autowired private BienRepository bienRepository;
  @Autowired private ContratoRepository contratoRepository;
  @Autowired private ContratoBienRepository contratoBienRepository;
  @Autowired private ExpedienteRepository expedienteRepository;
  @Autowired private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired private GestorDocumentalUseCase gestorDocumentalUseCase;

  @Override
  public ListWithCountDTO<BienDTOToList> getAllFilteredBienes(
      boolean esGarantia,
      Integer idContrato,
      Integer id,
      String idOrigen,
      String tipoActivo,
      Boolean estado,
      String fincaRegistral,
      String localidad,
      String provincia,
      String valor,
      String descripcion,
      String responsabilidad,
      String liquidez,
      Boolean posesionNegociada,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page) {
    List<Bien> bienesSinPaginar =
        filterBienesInMemory(
            id,
            idOrigen,
            esGarantia,
            idContrato,
            tipoActivo,
            estado,
            fincaRegistral,
            localidad,
            provincia,
            valor,
            descripcion,
            responsabilidad,
            liquidez,
            posesionNegociada,
            orderField,
            orderDirection);

    List<BienDTOToList> bienesDTOS =
        bienMapper.listBienToListBienDTOToList(bienesSinPaginar, idContrato);

    if (valor != null)
      bienesDTOS =
          bienesDTOS.stream()
              .filter(
                  (var o1) ->
                      StringUtil.startsWithIgnoreCase(o1.getImporteGarantizado() + "", valor))
              .collect(Collectors.toList());

    bienesDTOS = bienesDTOS.stream().skip(size * page).limit(size).collect(Collectors.toList());

    // expedientesPaged.getContent().stream().map(expedienteMapper::parse).collect(Collectors.toList());
    return new ListWithCountDTO<>(bienesDTOS, bienesSinPaginar.size());
  }

  @Override
  public ListWithCountDTO<BienExtendedDTOToList> getAllFilteredBienesExtended(
      boolean esGarantia,
      Integer idContrato,
      Integer id,
      String idOrigen,
      String tipoActivo,
      Boolean estado,
      String fincaRegistral,
      String localidad,
      String provincia,
      String valor,
      String descripcion,
      String responsabilidad,
      String liquidez,
      Boolean posesionNegociada,
      String orderField,
      String orderDirection,
      Integer size,
      Integer page) {
    List<Bien> bienesSinPaginar =
        filterBienesInMemory(
            id,
            idOrigen,
            esGarantia,
            idContrato,
            tipoActivo,
            estado,
            fincaRegistral,
            localidad,
            provincia,
            valor,
            descripcion,
            responsabilidad,
            liquidez,
            posesionNegociada,
            orderField,
            orderDirection);

    List<BienExtendedDTOToList> bienesDTOS =
        bienMapper.listBienToListBienExtendedDTOToList(bienesSinPaginar, idContrato);

    if (valor != null)
      bienesDTOS =
          bienesDTOS.stream()
              .filter(
                  (var o1) ->
                      StringUtil.startsWithIgnoreCase(o1.getImporteGarantizado() + "", valor))
              .collect(Collectors.toList());

    bienesDTOS = bienesDTOS.stream().skip(size * page).limit(size).collect(Collectors.toList());

    // expedientesPaged.getContent().stream().map(expedienteMapper::parse).collect(Collectors.toList());
    return new ListWithCountDTO<>(bienesDTOS, bienesSinPaginar.size());
  }

  @Override
  public List<Bien> filterBienesInMemory(
      Integer id,
      String idOrigen,
      Boolean esGarantia,
      Integer idContrato,
      String tipoActivo,
      Boolean estado,
      String fincaRegistral,
      String localidad,
      String provincia,
      String valor,
      String descripcion,
      String responsabilidad,
      String liquidez,
      Boolean posesionNegociada,
      String orderField,
      String orderDirection) {

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Bien> query = cb.createQuery(Bien.class);

    Root<Bien> root = query.from(Bien.class);
    List<Predicate> predicates = new ArrayList<>();

    Join<Bien, ContratoBien> cbJoin = root.join(Bien_.contratos, JoinType.INNER);
    Predicate onContratoBien =
        cb.equal(cbJoin.get(ContratoBien_.contrato).get(Contrato_.id), idContrato);
    cbJoin.on(onContratoBien);

    if (id != null) predicates.add(cb.like(root.get(Bien_.id).as(String.class), "%" + id + "%"));
    if (esGarantia != null) {
      if (esGarantia) {
        predicates.add(cb.isNotNull(cbJoin.get(ContratoBien_.responsabilidadHipotecaria)));
      } else {
        predicates.add(cb.isNull(cbJoin.get(ContratoBien_.responsabilidadHipotecaria)));
      }
    }
    if (idOrigen != null) predicates.add(cb.like(root.get(Bien_.idOrigen), "%" + idOrigen + "%"));
    if (tipoActivo != null)
      predicates.add(
          cb.like(root.get(Bien_.tipoActivo).get(TipoActivo_.VALOR), "%" + tipoActivo + "%"));
    if (estado != null) predicates.add(cb.equal(root.get(Bien_.activo), estado));
    if (fincaRegistral != null)
      predicates.add(cb.like(root.get(Bien_.finca), "%" + fincaRegistral + "%"));
    if (localidad != null)
      predicates.add(
          cb.like(root.get(Bien_.localidad).get(Localidad_.VALOR), "%" + localidad + "%"));
    if (provincia != null)
      predicates.add(
          cb.like(root.get(Bien_.provincia).get(Provincia_.VALOR), "%" + provincia + "%"));
    if (valor != null) {
      /*Join<Bien, Tasacion> tasJoin = root.join(Bien_.tasaciones, JoinType.INNER);
      Subquery<Date> sqMaxT = cb.createQuery().subquery(Date.class);
      Root<Tasacion> rootMaxTas = sqMaxT.from(Tasacion.class);
      sqMaxT.select(cb.greatest(rootMaxTas.get(Tasacion_.fecha)));
      Subquery<Integer> sqT = cb.createQuery().subquery(Integer.class);
      Root<Tasacion> rootTas = sqT.from(Tasacion.class);
      sqT.select(tasJoin.get(Tasacion_.id)).where(cb.equal(tasJoin.get(Tasacion_.fecha), sqMaxT));
      Predicate pTasJoin = cb.and(
              cb.equal(tasJoin.get(Tasacion_.id), sqT),
              cb.like(tasJoin.get(Tasacion_.IMPORTE).as(String.class), "%" + valor + "%"));*/

      /*Join<Bien, Valoracion> valJoin = root.join(Bien_.valoraciones, JoinType.INNER);
      Subquery<Integer> sqV = cb.createQuery().subquery(Integer.class);
      Root<Valoracion> rootVal = sqV.from(Valoracion.class);
      sqV.select(rootVal.get(Valoracion_.ID)).where(cb.equal(rootVal.get(Valoracion_.FECHA), cb.greatest(rootVal.get(Valoracion_.fecha))));
      Predicate pValJoin = cb.and(
              cb.equal(valJoin.get(Valoracion_.id), sqV),
              cb.like(valJoin.get(Valoracion_.IMPORTE).as(String.class), "%" + valor + "%"));*/

      // Join<Bien, TipoActivo> taJoin = root.join(Bien_.tipoActivo, JoinType.INNER);
      // cb.selectCase().when(cb.equal(taJoin.get(TipoActivo_.CODIGO), "1"),
      // predicates.add(pTasJoin)).otherwise(predicates.add(pValJoin));
      // predicates.add(pTasJoin);
      // predicates.add(cb.equal(root.get(Bien_.ID), c));
    }
    if (descripcion != null)
      predicates.add(cb.like(root.get(Bien_.descripcion), "%" + descripcion + "%"));
    if (responsabilidad != null)
      predicates.add(
          cb.like(
              cbJoin.get(ContratoBien_.responsabilidadHipotecaria).as(String.class),
              "%" + responsabilidad.toString() + "%"));
    if (liquidez != null)
      predicates.add(
          cb.like(cbJoin.get(ContratoBien_.liquidez).get(Liquidez_.VALOR), "%" + liquidez + "%"));
    if (posesionNegociada != null)
      predicates.add(cb.equal(root.get(Bien_.posesionNegociada), posesionNegociada));

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
    if (orderField != null) {
      if (orderField.equals("estado")
          || orderField.equals("responsabilidadHipotecaria")
          || orderField.equals("liquidez")) {
        if (orderDirection.equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(cbJoin.get(orderField)));
        } else {
          rs.orderBy(cb.asc(cbJoin.get(orderField)));
        }
      } else {
        if (orderDirection.equalsIgnoreCase("desc")) {
          rs.orderBy(cb.desc(root.get(orderField)));
        } else {
          rs.orderBy(cb.asc(root.get(orderField)));
        }
      }
    }
    return entityManager.createQuery(query).getResultList();
  }

  public BienOutputDto findBienByIdDto(Integer id, Integer idContrato)
      throws IllegalAccessException {
    Bien bien = bienRepository.findById(id).orElse(null);
    BienOutputDto result = null;
    if (bien != null) {
      result = bienMapper.entityOutputDto(bien, idContrato);
      result.setHistorico(bienMapper.mapToOutputDto(bienRepository.historicoById(bien.getId())));
    }

    return result;
  }

  public BienOutputDto findBienByIdOrigenOrIdOrigen2Dto(String idOrigen)
      throws IllegalAccessException {
    Bien bien = bienRepository.findByIdOrigenEquals(idOrigen).orElse(null);
    if (bien == null) bien = bienRepository.findByIdOrigen2Equals(idOrigen).orElse(null);

    BienOutputDto result = null;
    if (bien != null) {
      result = bienMapper.entityOutputDto(bien, null);
      result.setHistorico(bienMapper.mapToOutputDto(bienRepository.historicoById(bien.getId())));
    }

    return result;
  }

  public BienOutputDto findBienByIdCargaDto(String idCarga) throws IllegalAccessException {
    Bien bien = bienRepository.findByIdCargaEquals(idCarga).orElse(null);

    BienOutputDto result = null;
    if (bien != null) {
      result = bienMapper.entityOutputDto(bien, null);
      result.setHistorico(bienMapper.mapToOutputDto(bienRepository.historicoById(bien.getId())));
    }

    return result;
  }

  public BienOutputDto update(Integer idBien, Integer idContrato, BienInputDto bienDTO)
      throws Exception {
    // validamos que no existan ya en bbdd los campos idOrigen y idOrigen2
    var bienAux =
        bienRepository
            .findById(bienDTO.getId())
            .orElseThrow(
                () ->
                    new NotFoundException("bien", bienDTO.getId()));
    if (bienAux.getIdOrigen() != null
        && bienDTO.getIdOrigen() != null
        && (!bienDTO.getIdOrigen().equals(bienAux.getIdOrigen()))) {
      Bien bienToCheck = new Bien();
      bienToCheck.setIdOrigen(bienDTO.getIdOrigen());
      checkIdOrigenAndIdOrigen2(bienToCheck);
    }
    if (bienAux.getIdOrigen2() != null
        && bienDTO.getIdOrigen2() != null
        && (!bienDTO.getIdOrigen2().equals(bienAux.getIdOrigen2()))) {
      Bien bienToCheck = new Bien();
      bienToCheck.setIdOrigen2(bienDTO.getIdOrigen2());
      checkIdOrigenAndIdOrigen2(bienToCheck);
    }

    Bien bien = bienMapper.inputDtoToEntity(idBien, idContrato, bienDTO);

    return bienMapper.entityOutputDto(bien, idContrato);
  }

  public BienGeolocalizacionOutputDto updateGeolocalizacion(
      Integer idBien, BienGeolocalizacionInputDto bienDTO) throws Exception {
    Bien bienAux =
        bienRepository
            .findById(idBien)
            .orElseThrow(
                () -> new NotFoundException("Bien", idBien));
    bienAux.setLatitud(bienDTO.getLatitud());
    bienAux.setLongitud(bienDTO.getLongitud());
    return bienMapper.entityExtendedOutputDto(bienAux, null);
  }

  private void checkIdOrigenAndIdOrigen2(Bien b) throws InvalidCodeException {
    Bien bien = null;
    if (b.getIdOrigen() != null) {
      bien = bienRepository.findByIdOrigenEquals(b.getIdOrigen()).orElse(null);
      if (bien != null){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCodeException("The asset cannot be created, there is already an asset with the idOrigen with the value " + b.getIdOrigen());
        else throw new InvalidCodeException("No se puede crear el bien, ya existe un bien con el idOrigen con el valor " + b.getIdOrigen());
      }
    }
    if (b.getIdOrigen2() != null) {
      bien = bienRepository.findByIdOrigen2Equals(b.getIdOrigen2()).orElse(null);
      if (bien != null){
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCodeException("The asset cannot be created, there is already an asset with the idOrigen with the value " + b.getIdOrigen());
        else throw new InvalidCodeException("No se puede crear el bien, ya existe un bien con el idOrigen con el valor " + b.getIdOrigen());
      }
    }
  }

  public BienOutputDto create(Integer idContrato, BienInputDto bienDTO)
      throws NotFoundException, InvalidCodeException {
    // validamos que no existan ya en bbdd los campos idOrigen y idOrigen2
    if (bienDTO.getIdOrigen() != null) {
      Bien bienToCheck = new Bien();
      bienToCheck.setIdOrigen(bienDTO.getIdOrigen());
      checkIdOrigenAndIdOrigen2(bienToCheck);
    }
    if (bienDTO.getIdOrigen2() != null) {
      Bien bienToCheck = new Bien();
      bienToCheck.setIdOrigen2(bienDTO.getIdOrigen2());
      checkIdOrigenAndIdOrigen2(bienToCheck);
    }

    Bien bien_nuevo = bienMapper.inputDtoToEntity(null, idContrato, bienDTO);

    ContratoBien cb =
        contratoBienRepository.findByBienIdAndContratoId(bien_nuevo.getId(), idContrato);
    if (cb == null)
      throw new NotFoundException("Contrato", idContrato, "Bien", bien_nuevo.getId());
    if (bien_nuevo.getContratos().isEmpty()) {
      bien_nuevo.getContratos().add(cb);
    }
    /*ContratoBien cb = bien_nuevo.getContratoBien(idContrato);
    if(cb == null) throw new NotFoundException("No se ha encontrato el contrado con ID: "+idContrato);
    Contrato contrato = contratoRepository.findById(idContrato).orElse(null);
    List<String> bienes = new ArrayList<>();
    bienes.add(bien_nuevo.getId());
    List<Bien> listBienes = bienRepository.findAllById(bienes);

    listBienes.forEach(bien -> contrato.getBienes().add(cb));

    contratoRepository.save(contrato);*/

    return bienMapper.entityOutputDto(bien_nuevo, idContrato);
  }

  public void delete(Integer idBien, Integer idContrato) {
    Bien bien = bienRepository.findById(idBien).orElse(null);
    ContratoBien cb = bien.getContratoBien(idContrato);
    cb.setEstado(false);
    contratoBienRepository.save(cb);
    /*bien.setActivo(false);
    bienRepository.save(bien);*/
  }

  public LinkedHashMap<String, List<Collection<?>>> findExcelBienById(Integer idContrato) {
    Contrato contrato = contratoRepository.findById(idContrato).orElse(null);

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> garantiaInmobiliaria = new ArrayList<>();
    garantiaInmobiliaria.add(GarantiaInmobiliariaExcel.cabeceras);
    List<Collection<?>> garantiaMobiliaria = new ArrayList<>();
    garantiaMobiliaria.add(GarantiaMobiliariaExcel.cabeceras);
    List<Collection<?>> solvenciaInmobiliaria = new ArrayList<>();
    solvenciaInmobiliaria.add(SolvenciaInmobiliariaExcel.cabeceras);
    List<Collection<?>> solvenciaMobiliaria = new ArrayList<>();
    solvenciaMobiliaria.add(SolvenciaMobiliariaExcel.cabeceras);

    List<Collection<?>> tasacionesGarantias = new ArrayList<>();
    tasacionesGarantias.add(TasacionGarantiaExcel.cabeceras);
    List<Collection<?>> tasacionesSolvencias = new ArrayList<>();
    tasacionesSolvencias.add(TasacionSolvenciaExcel.cabeceras);
    List<Collection<?>> valoracionesGarantias = new ArrayList<>();
    valoracionesGarantias.add(ValoracionGarantiaExcel.cabeceras);
    List<Collection<?>> valoracionesSolvencias = new ArrayList<>();
    valoracionesSolvencias.add(ValoracionSolvenciaExcel.cabeceras);
    List<Collection<?>> cargasGarantias = new ArrayList<>();
    cargasGarantias.add(CargaGarantiaExcel.cabeceras);
    List<Collection<?>> cargasSolvencias = new ArrayList<>();
    cargasSolvencias.add(CargaSolvenciaExcel.cabeceras);

    for (ContratoBien cb : contrato.getBienes()) {
      Bien bien = cb.getBien();
      var bienId = bien.getId();
      if (cb.getResponsabilidadHipotecaria() != null) {
        for (Tasacion tasacion : bien.getTasaciones()) {
          tasacionesGarantias.add(new TasacionGarantiaExcel(tasacion, bienId).getValuesList());
        }
        for (Valoracion valoracion : bien.getValoraciones()) {
          valoracionesGarantias.add(
              new ValoracionGarantiaExcel(valoracion, bienId).getValuesList());
        }
        for (Carga carga : bien.getCargas()) {
          cargasGarantias.add(new CargaGarantiaExcel(carga, bienId).getValuesList());
        }
        if (bien.getTipoActivo().getCodigo().equals("0")) {
          garantiaMobiliaria.add(new GarantiaMobiliariaExcel(bien, contrato).getValuesList());
        } else {
          garantiaInmobiliaria.add(new GarantiaInmobiliariaExcel(bien, contrato).getValuesList());
        }
      } else {
        for (Tasacion tasacion : bien.getTasaciones()) {
          tasacionesSolvencias.add(new TasacionSolvenciaExcel(tasacion, bienId).getValuesList());
        }
        for (Valoracion valoracion : bien.getValoraciones()) {
          valoracionesSolvencias.add(
              new ValoracionSolvenciaExcel(valoracion, bienId).getValuesList());
        }
        for (Carga carga : bien.getCargas()) {
          cargasSolvencias.add(new CargaSolvenciaExcel(carga, bienId).getValuesList());
        }
        if (bien.getTipoActivo().getCodigo().equals("0")) {
          solvenciaMobiliaria.add(new SolvenciaMobiliariaExcel(bien, contrato).getValuesList());
        } else {
          solvenciaInmobiliaria.add(new SolvenciaInmobiliariaExcel(bien, contrato).getValuesList());
        }
      }
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      datos.put("Real estate warranty", garantiaInmobiliaria);
      datos.put("Furniture warranty", garantiaMobiliaria);
      datos.put("Real Estate Solvency", solvenciaInmobiliaria);
      datos.put("Furniture Solvency", solvenciaMobiliaria);
      datos.put("Appraisals Warranties", tasacionesGarantias);
      datos.put("Solvency appraisals", tasacionesSolvencias);
      datos.put("Warranty Ratings", valoracionesGarantias);
      datos.put("Solvency Valuations", valoracionesSolvencias);
      datos.put("Charges Warranties", cargasGarantias);
      datos.put("Solvenient Loads", cargasSolvencias);

    } else  {

      datos.put("Garantia Inmobiliaria", garantiaInmobiliaria);
      datos.put("Garantia Mobiliaria", garantiaMobiliaria);
      datos.put("Solvencia Inmobiliaria", solvenciaInmobiliaria);
      datos.put("Solvencia Mobiliaria", solvenciaMobiliaria);
      datos.put("Tasaciones Garantias", tasacionesGarantias);
      datos.put("Tasaciones Solvencias", tasacionesSolvencias);
      datos.put("Valoraciones Garantias", valoracionesGarantias);
      datos.put("Valoraciones Solvencias", valoracionesSolvencias);
      datos.put("Cargas Garantias", cargasGarantias);
      datos.put("Cargas Solvencias", cargasSolvencias);
    }

    return datos;
  }

  public LinkedHashMap<String, List<Collection<?>>> findExcelBienByIdBien(
      Integer idContrato, Integer idBien) {
    Bien bien = bienRepository.findById(idBien).orElse(null);
    ContratoBien cb = bien.getContratoBien(idContrato);

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> garantiaInmobiliaria = new ArrayList<>();
    garantiaInmobiliaria.add(GarantiaInmobiliariaExcel.cabeceras);
    List<Collection<?>> garantiaMobiliaria = new ArrayList<>();
    garantiaMobiliaria.add(GarantiaMobiliariaExcel.cabeceras);
    List<Collection<?>> solvenciaInmobiliaria = new ArrayList<>();
    solvenciaInmobiliaria.add(SolvenciaInmobiliariaExcel.cabeceras);
    List<Collection<?>> solvenciaMobiliaria = new ArrayList<>();
    solvenciaMobiliaria.add(SolvenciaMobiliariaExcel.cabeceras);

    List<Collection<?>> tasacionesGarantias = new ArrayList<>();
    tasacionesGarantias.add(TasacionGarantiaExcel.cabeceras);
    List<Collection<?>> tasacionesSolvencias = new ArrayList<>();
    tasacionesSolvencias.add(TasacionSolvenciaExcel.cabeceras);
    List<Collection<?>> valoracionesGarantias = new ArrayList<>();
    valoracionesGarantias.add(ValoracionGarantiaExcel.cabeceras);
    List<Collection<?>> valoracionesSolvencias = new ArrayList<>();
    valoracionesSolvencias.add(ValoracionSolvenciaExcel.cabeceras);
    List<Collection<?>> cargasGarantias = new ArrayList<>();
    cargasGarantias.add(CargaGarantiaExcel.cabeceras);
    List<Collection<?>> cargasSolvencias = new ArrayList<>();
    cargasSolvencias.add(CargaSolvenciaExcel.cabeceras);

    var bienId = bien.getId();
    if (cb.getResponsabilidadHipotecaria() != null) {
      for (Tasacion tasacion : bien.getTasaciones()) {
        tasacionesGarantias.add(new TasacionGarantiaExcel(tasacion, bienId).getValuesList());
      }
      for (Valoracion valoracion : bien.getValoraciones()) {
        valoracionesGarantias.add(new ValoracionGarantiaExcel(valoracion, bienId).getValuesList());
      }
      for (Carga carga : bien.getCargas()) {
        cargasGarantias.add(new CargaGarantiaExcel(carga, bienId).getValuesList());
      }
      if (bien.getTipoActivo().getCodigo().equals("0")) {
        garantiaMobiliaria.add(new GarantiaMobiliariaExcel(bien, cb.getContrato()).getValuesList());
      } else {
        garantiaInmobiliaria.add(
            new GarantiaInmobiliariaExcel(bien, cb.getContrato()).getValuesList());
      }
    } else {
      for (Tasacion tasacion : bien.getTasaciones()) {
        tasacionesSolvencias.add(new TasacionSolvenciaExcel(tasacion, bienId).getValuesList());
      }
      for (Valoracion valoracion : bien.getValoraciones()) {
        valoracionesSolvencias.add(
            new ValoracionSolvenciaExcel(valoracion, bienId).getValuesList());
      }
      for (Carga carga : bien.getCargas()) {
        cargasSolvencias.add(new CargaSolvenciaExcel(carga, bienId).getValuesList());
      }
      if (bien.getTipoActivo().getCodigo().equals("0")) {
        solvenciaMobiliaria.add(
            new SolvenciaMobiliariaExcel(bien, cb.getContrato()).getValuesList());
      } else {
        solvenciaInmobiliaria.add(
            new SolvenciaInmobiliariaExcel(bien, cb.getContrato()).getValuesList());
      }
    }

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){

      datos.put("Real estate warranty", garantiaInmobiliaria);
      datos.put("Furniture warranty", garantiaMobiliaria);
      datos.put("Real Estate Solvency", solvenciaInmobiliaria);
      datos.put("Furniture Solvency", solvenciaMobiliaria);
      datos.put("Appraisals Warranties", tasacionesGarantias);
      datos.put("Solvency appraisals", tasacionesSolvencias);
      datos.put("Warranty Ratings", valoracionesGarantias);
      datos.put("Solvency Valuations", valoracionesSolvencias);
      datos.put("Charges Warranties", cargasGarantias);
      datos.put("Solvenient Loads", cargasSolvencias);

    } else  {

      datos.put("Garantia Inmobiliaria", garantiaInmobiliaria);
      datos.put("Garantia Mobiliaria", garantiaMobiliaria);
      datos.put("Solvencia Inmobiliaria", solvenciaInmobiliaria);
      datos.put("Solvencia Mobiliaria", solvenciaMobiliaria);
      datos.put("Tasaciones Garantias", tasacionesGarantias);
      datos.put("Tasaciones Solvencias", tasacionesSolvencias);
      datos.put("Valoraciones Garantias", valoracionesGarantias);
      datos.put("Valoraciones Solvencias", valoracionesSolvencias);
      datos.put("Cargas Garantias", cargasGarantias);
      datos.put("Cargas Solvencias", cargasSolvencias);
    }

    return datos;
  }

  public void createDocumento(
      Integer idBien,
      MultipartFile file,
      MetadatoDocumentoInputDTO metadatoDocumento,
      Usuario usuario,
      String tipoDocumento)
      throws NotFoundException, IOException {
    Bien bien = bienRepository.findById(idBien).orElse(null);
    if (bien.getIdHaya() == null) {
      throw new NotFoundException("Bien", true);
    }
    JSONObject metadatos = new JSONObject();
    JSONObject archivoFisico = new JSONObject();
    archivoFisico.put("contenedor", "CONT");
    metadatos.put("General documento", metadatoDocumento.createDescripcionGeneralDocumento());
    metadatos.put("Archivo físico", archivoFisico);
    long id =
        servicioGestorDocumental
            .crearDocumento(CodigoEntidad.GARANTIA, bien.getIdHaya(), file, metadatos)
            .getIdDocumento();
    Integer idgestorDocumental = (int) id;
    GestorDocumental gestorDocumental =
        new GestorDocumental(
            idgestorDocumental,
            usuario,
            null,
            new Date(),
            null,
            metadatoDocumento.getNombreFichero(),
          MatriculasEnum.obtenerPorMatricula(metadatoDocumento.getCatalogoDocumental()),
            null,
            idBien.toString(),
            TipoEntidadEnum.GARANTIA,
            bien.getIdHaya());
    gestorDocumentalRepository.save(gestorDocumental);
  }

  private MetadatoDocumentoInputDTO crearMetadatoDocumento(
      MultipartFile file, String CatalogoDocumental) {
    return new MetadatoDocumentoInputDTO(file.getName(), CatalogoDocumental);
  }

  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosByContratoId(
      Integer idContrato,
      String idDocumento,
      String usuarioCreador,
      String tipo,
      String idEntidad,
      String fechaActualizacion,
      String nombreArchivo,
      String idHaya,
      String orderDirection,
      String orderField,
      Integer size,
      Integer page)
      throws NotFoundException, IOException {
    Contrato contrato =
        contratoRepository
            .findById(idContrato)
            .orElseThrow(() -> new NotFoundException("Contrato", idContrato));
    List<String> idsBienes = new ArrayList<>();
    List<DocumentoDTO> listavacia = new ArrayList<>();
    for (ContratoBien cb : contrato.getBienes()) {
      Bien bien = cb.getBien();
      String idHayaT = bien.getIdHaya();
      if (idHayaT != null) {
        idsBienes.add(idHayaT);
      }
    }
    if (idsBienes.size() == 0) {
      if (contrato.getBienes().size() != 0) {
        log.warn("Ningún bien del contrado " + idContrato + " tiene idHaya");
      }
      return new ListWithCountDTO<>(listavacia, listavacia.size());
    }

    List<DocumentoDTO> listadoIdGestor =
        this.servicioGestorDocumental
            .listarDocumentosMulti(CodigoEntidad.GARANTIA, idsBienes)
            .stream()
            .map(DocumentoDTO::new)
            .collect(Collectors.toList());

    List<DocumentoDTO> list =
        gestorDocumentalUseCase.listarDocumentosGestorBienes(listadoIdGestor).stream()
            .collect(Collectors.toList());
    List<DocumentoDTO> listFinal =
        gestorDocumentalUseCase.filterOrderDocumentosDTO(
            list,
            idDocumento,
            usuarioCreador,
            tipo,
            idEntidad,
            fechaActualizacion,
            nombreArchivo,
            idHaya,
            orderDirection,
            orderField);
    List<DocumentoDTO> listaFiltradaPaginada =
        listFinal.stream().skip(size * page).limit(size).collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
  }

  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosByExpedienteId(
      Integer id,
      String idDocumento,
      String usuarioCreador,
      String tipo,
      String idEntidad,
      String fechaActualizacion,
      String nombreArchivo,
      String idHaya,
      String orderDirection,
      String orderField,
      Integer size,
      Integer page)
      throws NotFoundException, IOException {
    Expediente expediente = expedienteRepository.findById(id).orElse(null);
    Set<Contrato> listadoContratos = expediente.getContratos();
    Set<ContratoBien> listadoContratosBien = new HashSet<>();
    List<Bien> listaBienes = new ArrayList<>();
    for (Contrato contrato : listadoContratos) {
      Set<ContratoBien> listaContratoBien = contrato.getBienes();
      for (ContratoBien cb : listaContratoBien) {
        listaBienes.add(cb.getBien());
      }
    }
    List<String> listaActivosContrato = new ArrayList<>();
    for (Bien bien : listaBienes) {
      if (bien.getIdHaya() != null) listaActivosContrato.add(bien.getIdHaya());
    }

    List<DocumentoDTO> listadoIdGestor =
        this.servicioGestorDocumental
            .listarDocumentosMulti(CodigoEntidad.GARANTIA, listaActivosContrato)
            .stream()
            .map(DocumentoDTO::new)
            .collect(Collectors.toList());

    List<DocumentoDTO> list =
        gestorDocumentalUseCase.listarDocumentosGestorBienes(listadoIdGestor).stream()
            .collect(Collectors.toList());
    List<DocumentoDTO> listFinal =
        gestorDocumentalUseCase.filterOrderDocumentosDTO(
            list,
            idDocumento,
            usuarioCreador,
            tipo,
            idEntidad,
            fechaActualizacion,
            nombreArchivo,
            idHaya,
            orderDirection,
            orderField);
    List<DocumentoDTO> listaFiltradaPaginada =
        listFinal.stream().skip(size * page).limit(size).collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
  }

  public ListWithCountDTO<BienDTOToList> getAll(
      Integer size, Integer page, List<Integer> contratos, String referencia) {

    List<Bien> bienes = new ArrayList<>();
    List<BienDTOToList> garantiasDTOS = new ArrayList<>();

    if(Objects.isNull(contratos) || contratos.isEmpty()) {
      CriteriaBuilder cb = entityManager.getCriteriaBuilder();
      CriteriaQuery<Bien> cQuery = cb.createQuery(Bien.class);

      Root<Bien> root = cQuery.from(Bien.class);
      List<Predicate> predicates = new ArrayList<>();

      // predicates.add(cb.isNotNull(cbJoin.get(ContratoBien_.CONTRATO)));
      predicates.add(cb.isTrue(root.get(Bien_.ACTIVO)));

      if (contratos != null && !contratos.isEmpty()) {
        Join<Bien, ContratoBien> cbJoin = root.join(Bien_.contratos, JoinType.INNER);
        CriteriaBuilder.In<Integer> inClause =
          cb.in(cbJoin.get(ContratoBien_.CONTRATO).get(Contrato_.ID));
        for (Integer idC : contratos) {
          inClause.value(idC);
        }
        predicates.add(inClause);
        predicates.add(cb.isFalse(root.get(Bien_.tramitado)));
      }

      if (referencia != null)
        predicates.add(cb.like(root.get(Bien_.REFERENCIA_CATASTRAL), "%" + referencia + "%"));

      var rs = cQuery.select(root).distinct(true).where(predicates.toArray(new Predicate[predicates.size()]));

      Query query = entityManager.createQuery(cQuery);

      query.setFirstResult(page * size);
      query.setMaxResults(size);

      bienes = query.getResultList();

    } else {
      for(Integer idContrato: contratos) {
        var garantias = filterBienesInMemory(
          null, null, true, idContrato, null, null, null, null,
          null, null, null, null, null, null, null, null);
        var solvencias = filterBienesInMemory(
          null, null, false, idContrato, null, null, null, null,
          null, null, null, null, null, null, null, null);

        for(var garantia: garantias) {
          if(bienes.isEmpty()) {
            bienes.addAll(garantias);
          }else {
            boolean contiene = false;
            for(var bien: bienes) {
              if (bien.getId().equals(garantia.getId()))
                contiene = true;
            }
            if(!contiene)
              bienes.add(garantia);
          }
        }
        for(var solvencia: solvencias) {
          if(bienes.isEmpty()) {
            bienes.addAll(solvencias);
          }else {
            boolean contiene = false;
            for(var bien: bienes) {
              if (bien.getId().equals(solvencia.getId()))
                contiene = true;
            }
            if(!contiene)
              bienes.add(solvencia);
          }
        }
      }
    }

    garantiasDTOS = bienMapper.listBienToListBienDTOToList(bienes, null);
    List<BienDTOToList> listaFiltradaPaginada =
      garantiasDTOS.stream()
        .skip(size * page)
        .limit(size)
        .collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada, garantiasDTOS.size());
  }

  public ListWithCountDTO<BienVinculacionDto> findVinculadosBienById(Integer idBien)
      throws NotFoundException {
    Bien bien = bienRepository.findById(idBien).orElse(null);
    List<BienVinculacionDto> bienVinculacionDTOS = new ArrayList<>();
    String nombreInterviniente = "";
    for (ContratoBien cb : bien.getContratos()) {
      Contrato contrato = cb.getContrato();
      if (contrato.getExpediente().getCartera().getResponsableCartera() != null) {
        if (contrato.getPrimerInterviniente() != null) {
          if (contrato.getPrimerInterviniente().getPersonaJuridica() != null
              && contrato.getPrimerInterviniente().getRazonSocial() != null)
            nombreInterviniente = contrato.getPrimerInterviniente().getRazonSocial();
          else if (contrato.getPrimerInterviniente().getNombre() != null
              && contrato.getPrimerInterviniente().getApellidos() != null)
            nombreInterviniente =
                contrato.getPrimerInterviniente().getNombre()
                    + " "
                    + contrato.getPrimerInterviniente().getApellidos();
          else nombreInterviniente = contrato.getPrimerInterviniente().getNombre();
        }
        bienVinculacionDTOS.add(
            new BienVinculacionDto(
                contrato.getExpediente().getId(),
                contrato.getExpediente().getIdConcatenado(),
                contrato.getExpediente().getGestor() != null
                    ? new UsuarioDTO(contrato.getExpediente().getGestor())
                    : new UsuarioDTO(),
                contrato.getIdCarga() != null ? contrato.getIdCarga() : "",
                nombreInterviniente));
      } else {
        throw new NotFoundException(
            "Cartera", "Responsable", contrato.getExpediente().getCartera().getNombre());
      }
    }
    return new ListWithCountDTO<>(bienVinculacionDTOS, bienVinculacionDTOS.size());
  }

  @Override
  public List<BienDTOToList> getAllByExpedienteId(Integer idExpediente) {
    List<Bien> bienesProv = bienRepository.findAllByExpedienteId(idExpediente);
    List<Bien> bienes = new ArrayList<>();
    for (var bienTemp : bienesProv) {
      if (!bienes.contains(bienTemp)) bienes.add(bienTemp);
    }
    List<BienDTOToList> bienesDto = new ArrayList<>();
    for (var bien : bienes) {
      bienesDto.add(bienMapper.bienToBienDTOToList(bien, null));
    }
    return bienesDto;
    // return BienDTOToList.newListExpediente(bienes, idExpediente);
  }

  @Override
  public ListWithCountDTO<DocumentoDTO> findDocumentosByBienId(
      Integer idBien,
      String idDocumento,
      String usuarioCreador,
      String tipo,
      String idEntidad,
      String fechaActualizacion,
      String orderDirection,
      String orderField,
      Integer size,
      Integer page)
      throws NotFoundException, IOException {
    Bien bien =
        bienRepository
            .findById(idBien)
            .orElseThrow(() -> new NotFoundException("Bien", idBien));
    String idHaya = bien.getIdHaya();
    List<DocumentoDTO> listavacia = new ArrayList<>();
    if (idHaya == null) {
      return new ListWithCountDTO<>(listavacia, listavacia.size());
    }

    List<DocumentoDTO> listadoIdGestor =
        this.servicioGestorDocumental.listarDocumentos(CodigoEntidad.GARANTIA, idHaya).stream()
            .map(DocumentoDTO::new)
            .collect(Collectors.toList());

    List<DocumentoDTO> list =
        gestorDocumentalUseCase.listarDocumentosGestorBienes(listadoIdGestor).stream()
            .collect(Collectors.toList());
    List<DocumentoDTO> listFinal =
        gestorDocumentalUseCase.filterOrderDocumentosDTO(
            list,
            idDocumento,
            usuarioCreador,
            tipo,
            idEntidad,
            fechaActualizacion,
            null,
            null,
            orderDirection,
            orderField);
    List<DocumentoDTO> listaFiltradaPaginada =
        gestorDocumentalUseCase.listarDocumentosGestor(listFinal).stream()
            .skip(size * page)
            .limit(size)
            .collect(Collectors.toList());
    return new ListWithCountDTO<>(listaFiltradaPaginada, listFinal.size());
  }
}
