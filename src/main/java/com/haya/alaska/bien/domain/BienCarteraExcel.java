package com.haya.alaska.bien.domain;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;

public class BienCarteraExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Asset Id");
      cabeceras.add("Loan Id");
      cabeceras.add("Haya Id");
      cabeceras.add("Origin Id");
      cabeceras.add("Customer Id");
      cabeceras.add("Customer2 Id");
      cabeceras.add("Asset Type");
      cabeceras.add("Asset Subtype");
      cabeceras.add("Warranty Type");
      cabeceras.add("Status");
      cabeceras.add("Cadastral Reference");
      cabeceras.add("Finca");
      cabeceras.add("Location Registration");
      cabeceras.add("Registration");
      cabeceras.add("Volume");
      cabeceras.add("Book");
      cabeceras.add("Folio");
      cabeceras.add("Registration Date");
      cabeceras.add("Idufir");
      cabeceras.add("Reo Origin");
      cabeceras.add("Reo Conversion Date");
      cabeceras.add("Guaranteed Amount");
      cabeceras.add("Mortgage Liability");
      cabeceras.add("Negotiated Possession");
      cabeceras.add("Auction");
      cabeceras.add("Via Type");
      cabeceras.add("Via Name");
      cabeceras.add("Number");
      cabeceras.add("Portal");
      cabeceras.add("Block");
      cabeceras.add("Stairs");
      cabeceras.add("Floor");
      cabeceras.add("Door");
      cabeceras.add("Environment");
      cabeceras.add("Municipality");
      cabeceras.add("Location");
      cabeceras.add("Province");
      cabeceras.add("Comment Address");
      cabeceras.add("Postal Code");
      cabeceras.add("Country");
      cabeceras.add("Length");
      cabeceras.add("Latitude");
      cabeceras.add("Active Type");
      cabeceras.add("Use");
      cabeceras.add("Year Construction");
      cabeceras.add("Floor area");
      cabeceras.add("Built-up area");
      cabeceras.add("Useful surface");
      cabeceras.add("Garage Annex");
      cabeceras.add("Storage Room Annex");
      cabeceras.add("Other Annex");
    } else {
      cabeceras.add("Id Bien");
      cabeceras.add("Id Contrato");
      cabeceras.add("Id Haya");
      cabeceras.add("Id Origen");
      cabeceras.add("Id Cliente");
      cabeceras.add("Id Cliente2");
      cabeceras.add("Tipo Bien");
      cabeceras.add("Subtipo Bien");
      cabeceras.add("Tipo Garantia");
      cabeceras.add("Estado");
      cabeceras.add("Refenrencia Catastral");
      cabeceras.add("Finca");
      cabeceras.add("Localidad Registro");
      cabeceras.add("Registro");
      cabeceras.add("Tomo");
      cabeceras.add("Libro");
      cabeceras.add("Folio");
      cabeceras.add("Fecha Inscripcion");
      cabeceras.add("Idufir");
      cabeceras.add("Reo Origen");
      cabeceras.add("Fecha Reo Conversion");
      cabeceras.add("Importe Garantizado");
      cabeceras.add("Responsabilidad Hipotecaria");
      cabeceras.add("Posesion Negociada");
      cabeceras.add("Subasta");
      cabeceras.add("Tipo Via");
      cabeceras.add("Nombre Via");
      cabeceras.add("Numero");
      cabeceras.add("Portal");
      cabeceras.add("Bloque");
      cabeceras.add("Escalera");
      cabeceras.add("Piso");
      cabeceras.add("Puerta");
      cabeceras.add("Entorno");
      cabeceras.add("Municipio");
      cabeceras.add("Localidad");
      cabeceras.add("Provincia");
      cabeceras.add("Comentario Direccion");
      cabeceras.add("Codigo Postal");
      cabeceras.add("Pais");
      cabeceras.add("Longitud");
      cabeceras.add("Latitud");
      cabeceras.add("Tipo Activo");
      cabeceras.add("Uso");
      cabeceras.add("Año Construccion");
      cabeceras.add("Superficie Suelo");
      cabeceras.add("Superficie Construida");
      cabeceras.add("Superficie Util");
      cabeceras.add("Anejo Garaje");
      cabeceras.add("Anejo Trastero");
      cabeceras.add("Anejo Otros");
    }
  }

  public BienCarteraExcel(Bien bien, Integer idContrato, BienPosesionNegociada bienPN) {
    super();

    ContratoBien cb = bien.getContratoBien(idContrato);

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Asset Id", bien.getId());
      if (cb != null)
        this.add("Loan Id", cb.getContrato() != null ? cb.getContrato().getIdCarga() : null);
      this.add("Haya Id", bien.getIdHaya() != null ? bien.getIdHaya() : null);
      this.add("Origin Id", bien.getIdCarga() != null ? bien.getIdCarga() : null);
      this.add("Customer Id", bien.getIdOrigen() != null ? bien.getIdOrigen() : null);
      this.add("Customer2 Id", bien.getIdOrigen2() != null ? bien.getIdOrigen2() : null);
      this.add(
          "Asset Type", bien.getTipoBien() != null ? bien.getTipoBien().getValorIngles() : null);
      this.add(
          "Asset Subtype",
          bien.getSubTipoBien() != null ? bien.getSubTipoBien().getValorIngles() : null);
      this.add("Status", Boolean.TRUE.equals(bien.getActivo()) ? "Activated" : "Released");
      if (cb != null) {
        this.add(
            "Warranty Type",
            cb.getTipoGarantia() != null ? cb.getTipoGarantia().getValorIngles() : null);
      } else if (bienPN != null) {
        this.add(
            "Warranty Type",
            bienPN.getTipoGarantia() != null ? bienPN.getTipoGarantia().getValorIngles() : null);
      }
      this.add(
          "Cadastral Reference",
          bien.getReferenciaCatastral() != null ? bien.getReferenciaCatastral() : null);
      this.add("Finca", bien.getFinca() != null ? bien.getFinca() : null);
      this.add(
          "Location Registration",
          bien.getLocalidadRegistro() != null ? bien.getLocalidadRegistro() : null);
      this.add("Registration", bien.getRegistro() != null ? bien.getRegistro() : null);
      this.add("Volume", bien.getTomo() != null ? bien.getTomo() : null);
      this.add("Book", bien.getLibro() != null ? bien.getTomo() : null);
      this.add("Folio", bien.getFolio() != null ? bien.getFolio() : null);
      this.add(
          "Registration Date",
          bien.getFechaInscripcion() != null ? bien.getFechaInscripcion() : null);
      this.add("Idufir", bien.getIdufir() != null ? bien.getIdufir() : null);
      this.add("Reo Origin", bien.getReoOrigen() != null ? bien.getReoOrigen() : null);
      this.add(
          "Reo Conversion Date",
          bien.getFechaReoConversion() != null ? bien.getFechaReoConversion() : null);

      if(Objects.nonNull(bienPN)) {
        this.add(
          "Mortgage Liability",
          bien.getResponsabilidadHipotecaria() != null
            ? bien.getResponsabilidadHipotecaria()
            : null);
        this.add(
          "Guaranteed Amount",
          bien.getImporteGarantizado() != null ? bien.getImporteGarantizado() : null);
      } else {
        this.add(
          "Mortgage Liability",
          cb.getResponsabilidadHipotecaria() != null
            ? cb.getResponsabilidadHipotecaria()
            : null);
        this.add(
          "Guaranteed Amount",
          cb.getImporteGarantizado() != null ? cb.getImporteGarantizado() : null);
      }

      this.add(
          "Negotiated Possession", Boolean.TRUE.equals(bien.getPosesionNegociada()) ? "YES" : "NO");
      this.add("Auction", Boolean.TRUE.equals(bien.getSubasta()) ? "YES" : "NO");
      this.add("Via Type", bien.getTipoVia() != null ? bien.getTipoVia().getValorIngles() : null);
      this.add("Via Name", bien.getNombreVia() != null ? bien.getNombreVia() : null);
      this.add("Number", bien.getNumero() != null ? bien.getNumero() : null);
      this.add("Portal", bien.getPortal() != null ? bien.getPortal() : null);
      this.add("Block", bien.getBloque() != null ? bien.getBloque() : null);
      this.add("Stairs", bien.getEscalera() != null ? bien.getEscalera() : null);
      this.add("Floor", bien.getPiso() != null ? bien.getPiso() : null);
      this.add("Door", bien.getPuerta() != null ? bien.getPuerta() : null);
      this.add(
          "Environment", bien.getEntorno() != null ? bien.getEntorno().getValorIngles() : null);
      this.add(
          "Municipality",
          bien.getMunicipio() != null ? bien.getMunicipio().getValorIngles() : null);
      this.add(
          "Location", bien.getLocalidad() != null ? bien.getLocalidad().getValorIngles() : null);
      this.add(
          "Province", bien.getProvincia() != null ? bien.getProvincia().getValorIngles() : null);
      this.add(
          "Comment Address",
          bien.getComentarioDireccion() != null ? bien.getComentarioDireccion() : null);
      this.add("Postal Code", bien.getCodigoPostal() != null ? bien.getCodigoPostal() : null);
      this.add("Country", bien.getPais() != null ? bien.getPais().getValorIngles() : null);
      this.add("Length", bien.getLongitud() != null ? bien.getLongitud() : null);
      this.add("Latitude", bien.getLatitud() != null ? bien.getLatitud() : null);
      this.add(
          "Active Type",
          bien.getTipoActivo() != null ? bien.getTipoActivo().getValorIngles() : null);
      this.add("Use", bien.getUso() != null ? bien.getUso().getValorIngles() : null);
      this.add(
          "Year Construction",
          bien.getAnioConstruccion() != null ? bien.getAnioConstruccion() : null);
      this.add("Floor area", bien.getSuperficieSuelo() != null ? bien.getSuperficieSuelo() : null);
      this.add(
          "Built-up area",
          bien.getSuperficieConstruida() != null ? bien.getSuperficieConstruida() : null);
      this.add(
          "Useful surface", bien.getSuperficieUtil() != null ? bien.getSuperficieUtil() : null);
      this.add("Garage Annex", bien.getAnejoGaraje() != null ? "YES" : "-");
      this.add("Storage Room Annex", bien.getAnejoTrastero() != null ? "YES" : "-");
      this.add("Other Annex", bien.getAnejoOtros() != null ? "YES" : "-");
    } else {
      this.add("Id Bien", bien.getId());
      if (cb != null)
        this.add("Id Contrato", cb.getContrato() != null ? cb.getContrato().getIdCarga() : null);
      this.add("Id Haya", bien.getIdHaya() != null ? bien.getIdHaya() : null);
      this.add("Id Origen", bien.getIdCarga() != null ? bien.getIdCarga() : null);
      this.add("Id Cliente", bien.getIdOrigen() != null ? bien.getIdOrigen() : null);
      this.add("Id Cliente2", bien.getIdOrigen2() != null ? bien.getIdOrigen2() : null);
      this.add("Tipo Bien", bien.getTipoBien() != null ? bien.getTipoBien().getValor() : null);
      this.add(
          "Subtipo Bien", bien.getSubTipoBien() != null ? bien.getSubTipoBien().getValor() : null);
      this.add("Estado", Boolean.TRUE.equals(bien.getActivo()) ? "Activa" : "Liberada");
      if (cb != null) {
        this.add(
            "Tipo Garantia", cb.getTipoGarantia() != null ? cb.getTipoGarantia().getValor() : null);
      } else if (bienPN != null) {
        this.add(
            "Tipo Garantia",
            bienPN.getTipoGarantia() != null ? bienPN.getTipoGarantia().getValor() : null);
      }
      this.add(
          "Refenrencia Catastral",
          bien.getReferenciaCatastral() != null ? bien.getReferenciaCatastral() : null);
      this.add("Finca", bien.getFinca() != null ? bien.getFinca() : null);
      this.add(
          "Localidad Registro",
          bien.getLocalidadRegistro() != null ? bien.getLocalidadRegistro() : null);
      this.add("Registro", bien.getRegistro() != null ? bien.getRegistro() : null);
      this.add("Tomo", bien.getTomo() != null ? bien.getTomo() : null);
      this.add("Libro", bien.getLibro() != null ? bien.getTomo() : null);
      this.add("Folio", bien.getFolio() != null ? bien.getFolio() : null);
      this.add(
          "Fecha Inscripcion",
          bien.getFechaInscripcion() != null ? bien.getFechaInscripcion() : null);
      this.add("Idufir", bien.getIdufir() != null ? bien.getIdufir() : null);
      this.add("Reo Origen", bien.getReoOrigen() != null ? bien.getReoOrigen() : null);
      this.add(
          "Fecha Reo Conversion",
          bien.getFechaReoConversion() != null ? bien.getFechaReoConversion() : null);
      if(Objects.nonNull(bienPN)) {
        this.add(
          "Responsabilidad Hipotecaria",
          bien.getResponsabilidadHipotecaria() != null
            ? bien.getResponsabilidadHipotecaria()
            : null);
        this.add(
          "Importe Garantizado",
          bien.getImporteGarantizado() != null ? bien.getImporteGarantizado() : null);
      } else {
        this.add(
          "Responsabilidad Hipotecaria",
          cb.getResponsabilidadHipotecaria() != null
            ? cb.getResponsabilidadHipotecaria()
            : null);
        this.add(
          "Importe Garantizado",
          cb.getImporteGarantizado() != null ? cb.getImporteGarantizado() : null);
      }
      this.add(
          "Posesion Negociada", Boolean.TRUE.equals(bien.getPosesionNegociada()) ? "SÍ" : "NO");
      this.add("Subasta", Boolean.TRUE.equals(bien.getSubasta()) ? "SÍ" : "NO");
      this.add("Tipo Via", bien.getTipoVia() != null ? bien.getTipoVia().getValor() : null);
      this.add("Nombre Via", bien.getNombreVia() != null ? bien.getNombreVia() : null);
      this.add("Numero", bien.getNumero() != null ? bien.getNumero() : null);
      this.add("Portal", bien.getPortal() != null ? bien.getPortal() : null);
      this.add("Bloque", bien.getBloque() != null ? bien.getBloque() : null);
      this.add("Escalera", bien.getEscalera() != null ? bien.getEscalera() : null);
      this.add("Piso", bien.getPiso() != null ? bien.getPiso() : null);
      this.add("Puerta", bien.getPuerta() != null ? bien.getPuerta() : null);
      this.add("Entorno", bien.getEntorno() != null ? bien.getEntorno().getValor() : null);
      this.add("Municipio", bien.getMunicipio() != null ? bien.getMunicipio().getValor() : null);
      this.add("Localidad", bien.getLocalidad() != null ? bien.getLocalidad().getValor() : null);
      this.add("Provincia", bien.getProvincia() != null ? bien.getProvincia().getValor() : null);
      this.add(
          "Comentario Direccion",
          bien.getComentarioDireccion() != null ? bien.getComentarioDireccion() : null);
      this.add("Codigo Postal", bien.getCodigoPostal() != null ? bien.getCodigoPostal() : null);
      this.add("Pais", bien.getPais() != null ? bien.getPais().getValor() : null);
      this.add("Longitud", bien.getLongitud() != null ? bien.getLongitud() : null);
      this.add("Latitud", bien.getLatitud() != null ? bien.getLatitud() : null);
      this.add(
          "Tipo Activo", bien.getTipoActivo() != null ? bien.getTipoActivo().getValor() : null);
      this.add("Uso", bien.getUso() != null ? bien.getUso().getValor() : null);
      this.add(
          "Año Construccion",
          bien.getAnioConstruccion() != null ? bien.getAnioConstruccion() : null);
      this.add(
          "Superficie Suelo", bien.getSuperficieSuelo() != null ? bien.getSuperficieSuelo() : null);
      this.add(
          "Superficie Construida",
          bien.getSuperficieConstruida() != null ? bien.getSuperficieConstruida() : null);
      this.add(
          "Superficie Util", bien.getSuperficieUtil() != null ? bien.getSuperficieUtil() : null);
      this.add("Anejo Garaje", bien.getAnejoGaraje() != null ? "SÍ" : "-");
      this.add("Anejo Trastero", bien.getAnejoTrastero() != null ? "SÍ" : "-");
      this.add("Anejo Otros", bien.getAnejoOtros() != null ? "SÍ" : "-");
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
