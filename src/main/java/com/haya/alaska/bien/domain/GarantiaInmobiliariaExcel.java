package com.haya.alaska.bien.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class GarantiaInmobiliariaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      cabeceras.add("Asset Id");
      cabeceras.add("Loan");
      cabeceras.add("Connection Id");
      cabeceras.add("Haya Id");
      cabeceras.add("Origin");
      cabeceras.add("Origin 2");
      cabeceras.add("Asset Type");
      cabeceras.add("Asset Subtype");
      cabeceras.add("Warranty/Solvency Type");
      cabeceras.add("Status");
      cabeceras.add("Cadastral Reference");
      cabeceras.add("Finca");
      cabeceras.add("Location Registration");
      cabeceras.add("Registration");
      cabeceras.add("Volume");
      cabeceras.add("Book");
      cabeceras.add("Folio");
      cabeceras.add("Registration Date");
      cabeceras.add("Idufir");
      cabeceras.add("Reo Origin");
      cabeceras.add("Reo Conversion Date");
      cabeceras.add("Guaranteed Amount");
      cabeceras.add("Mortgage Liability");
      cabeceras.add("Negotiated Possession");
      cabeceras.add("Auction");
      cabeceras.add("Via Type");
      cabeceras.add("Via Name");
      cabeceras.add("Number");
      cabeceras.add("Portal");
      cabeceras.add("Block");
      cabeceras.add("Stairs");
      cabeceras.add("Floor");
      cabeceras.add("Door");
      cabeceras.add("Environment");
      cabeceras.add("Municipality");
      cabeceras.add("Location");
      cabeceras.add("Province");
      cabeceras.add("Comment Address");
      cabeceras.add("Postal Code");
      cabeceras.add("Country");
      cabeceras.add("Length");
      cabeceras.add("Latitude");
      cabeceras.add("Active Type");
      cabeceras.add("Use");
      cabeceras.add("Year Construction");
      cabeceras.add("Floor area");
      cabeceras.add("Built-up area");
      cabeceras.add("Useful surface");
      cabeceras.add("Garage Annex");
      cabeceras.add("Storage Room Annex");
      cabeceras.add("Other Annex");
      cabeceras.add("Judicial Sanitation Id");
      cabeceras.add("Awarded Amount");
      cabeceras.add("Awarded");
      cabeceras.add("Decree Date");
      cabeceras.add("Testimonial Date");
      cabeceras.add("Ownership/Release Date");
      cabeceras.add("Additional Data");
      cabeceras.add("Additional Data 2");

    } else {
      cabeceras.add("Id Bien");
      cabeceras.add("Contrato");
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Haya");
      cabeceras.add("Origen");
      cabeceras.add("Origen 2");
      cabeceras.add("Tipo Bien");
      cabeceras.add("Subtipo Bien");
      cabeceras.add("Tipo Garantia");
      cabeceras.add("Estado");
      cabeceras.add("Referencia Catastral");
      cabeceras.add("Finca");
      cabeceras.add("Localidad Registro");
      cabeceras.add("Registro");
      cabeceras.add("Tomo");
      cabeceras.add("Libro");
      cabeceras.add("Folio");
      cabeceras.add("Fecha Inscripcion");
      cabeceras.add("Idufir");
      cabeceras.add("Reo Origen");
      cabeceras.add("Fecha Reo Conversion");
      cabeceras.add("Importe Garantizado");
      cabeceras.add("Responsabilidad Hipotecaria");
      cabeceras.add("Posesion Negociada");
      cabeceras.add("Subasta");
      cabeceras.add("Tipo Via");
      cabeceras.add("Nombre Via");
      cabeceras.add("Numero");
      cabeceras.add("Portal");
      cabeceras.add("Bloque");
      cabeceras.add("Escalera");
      cabeceras.add("Piso");
      cabeceras.add("Puerta");
      cabeceras.add("Entorno");
      cabeceras.add("Municipio");
      cabeceras.add("Localidad");
      cabeceras.add("Provincia");
      cabeceras.add("Comentario Direccion");
      cabeceras.add("Codigo Postal");
      cabeceras.add("Pais");
      cabeceras.add("Longitud");
      cabeceras.add("Latitud");
      cabeceras.add("Tipo Activo");
      cabeceras.add("Uso");
      cabeceras.add("Anio Construccion");
      cabeceras.add("Superficie Suelo");
      cabeceras.add("Superficie Construida");
      cabeceras.add("Superficie Util");
      cabeceras.add("Anejo Garaje");
      cabeceras.add("Anejo Trastero");
      cabeceras.add("Anejo Otros");
      cabeceras.add("Id Saneamiento Judicial");
      cabeceras.add("Importe Adjudicado");
      cabeceras.add("Adjudicado");
      cabeceras.add("Fecha Decreto");
      cabeceras.add("Fecha Testimonio");
      cabeceras.add("Fecha Posesion/Lanzamiento");
      cabeceras.add("Notas1");
      cabeceras.add("Notas2");
    }
  }

  public GarantiaInmobiliariaExcel(Bien bien, Contrato contrato) {
    super();

    ContratoBien cb = bien.getContratoBien(contrato.getId());

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      this.add("Asset Id", bien.getId());
      this.add("Loan", contrato.getIdCarga());
      this.add(
        "Connection Id",
        contrato.getExpediente() != null ? contrato.getExpediente().getIdConcatenado() : null);
      this.add("Haya Id", bien.getIdHaya());
      this.add("Origin", bien.getIdCarga());
      this.add("Origin 2", bien.getIdOrigen());
      this.add("Asset Type", bien.getTipoBien());
      this.add("Asset Subtype", bien.getSubTipoBien());
      this.add("Warranty Type", cb.getTipoGarantia());
      this.add("Status", bien.getActivo() ? "Activated" : "Released");
      this.add("Cadastral Reference", bien.getReferenciaCatastral());
      this.add("Finca", bien.getFinca());
      this.add("Location Registration", bien.getLocalidadRegistro());
      this.add("Registration", bien.getRegistro());
      this.add("Volume", bien.getTomo());
      this.add("Book", bien.getLibro());
      this.add("Folio", bien.getFolio());
      this.add("Registration Date", bien.getFechaInscripcion());
      this.add("Idufir", bien.getIdufir());
      this.add("Reo Origin", bien.getReoOrigen());
      this.add("Reo Conversion Date", bien.getFechaReoConversion());
      this.add("Guaranteed Amount", cb.getImporteGarantizado());
      this.add("Mortgage Liability", cb.getResponsabilidadHipotecaria());
      this.add("Negotiated Possession", bien.getPosesionNegociada());
      this.add("Auction", bien.getSubasta());
      this.add("Via Type", bien.getTipoVia());
      this.add("Via Name", bien.getNombreVia());
      this.add("Number", bien.getNumero());
      this.add("Portal", bien.getPortal());
      this.add("Block", bien.getBloque());
      this.add("Stairs", bien.getEscalera());
      this.add("Floor", bien.getPiso());
      this.add("Door", bien.getPuerta());
      this.add("Environment", bien.getEntorno());
      this.add("Municipality", bien.getMunicipio());
      this.add("Location", bien.getLocalidad());
      this.add("Province", bien.getProvincia());
      this.add("Comment Address", bien.getComentarioDireccion());
      this.add("Postal Code", bien.getCodigoPostal());
      this.add("Country", bien.getPais());
      this.add("Length", bien.getLongitud());
      this.add("Active Type", bien.getTipoActivo());
      this.add("Use", bien.getUso());
      this.add("Year Construction", bien.getAnioConstruccion());
      this.add("Floor area", bien.getSuperficieSuelo());
      this.add("Built-up area", bien.getSuperficieConstruida());
      this.add("Useful surface", bien.getSuperficieUtil());
      this.add("Garage Annex", bien.getAnejoGaraje());
      this.add("Storage Room Annex", bien.getAnejoTrastero());
      this.add("Other Annex", bien.getAnejoOtros());
      this.add("Judicial Sanitation Id", null);
      this.add("Awarded Amount", null);
      this.add("Awarded", null);
      this.add("Decree Date", null);
      this.add("Testimonial Date", null);
      this.add("Ownership/Release Date", null);
      this.add("Additional Data 1", bien.getNotas1());
      this.add("Additional Data 2", bien.getNotas2());

    } else {

      this.add("Id Bien", bien.getId());
      this.add("Contrato", contrato.getIdCarga());
      this.add(
          "Id Expediente",
          contrato.getExpediente() != null ? contrato.getExpediente().getIdConcatenado() : null);
      this.add("Id Haya", bien.getIdHaya());
      this.add("Origen", bien.getIdCarga());
      this.add("Origen 2", bien.getIdOrigen());
      this.add("Tipo Bien", bien.getTipoBien());
      this.add("Subtipo Bien", bien.getSubTipoBien());
      this.add("Tipo Garantia", cb.getTipoGarantia());
      this.add("Estado", bien.getActivo() ? "Activa" : "Liberada");
      this.add("Referencia Catastral", bien.getReferenciaCatastral());
      this.add("Finca", bien.getFinca());
      this.add("Localidad Registro", bien.getLocalidadRegistro());
      this.add("Registro", bien.getRegistro());
      this.add("Tomo", bien.getTomo());
      this.add("Libro", bien.getLibro());
      this.add("Folio", bien.getFolio());
      this.add("Fecha Inscripcion", bien.getFechaInscripcion());
      this.add("Idufir", bien.getIdufir());
      this.add("Reo Origen", bien.getReoOrigen());
      this.add("Fecha Reo Conversion", bien.getFechaReoConversion());
      this.add("Importe Garantizado", cb.getImporteGarantizado());
      this.add("Responsabilidad Hipotecaria", cb.getResponsabilidadHipotecaria());
      this.add("Posesion Negociada", bien.getPosesionNegociada());
      this.add("Subasta", bien.getSubasta());
      this.add("Tipo Via", bien.getTipoVia());
      this.add("Nombre Via", bien.getNombreVia());
      this.add("Numero", bien.getNumero());
      this.add("Portal", bien.getPortal());
      this.add("Bloque", bien.getBloque());
      this.add("Escalera", bien.getEscalera());
      this.add("Piso", bien.getPiso());
      this.add("Puerta", bien.getPuerta());
      this.add("Entorno", bien.getEntorno());
      this.add("Municipio", bien.getMunicipio());
      this.add("Localidad", bien.getLocalidad());
      this.add("Provincia", bien.getProvincia());
      this.add("Comentario Direccion", bien.getComentarioDireccion());
      this.add("Codigo Postal", bien.getCodigoPostal());
      this.add("Pais", bien.getPais());
      this.add("Longitud", bien.getLongitud());
      this.add("Latitud", bien.getLatitud());
      this.add("Tipo Activo", bien.getTipoActivo());
      this.add("Uso", bien.getUso());
      this.add("Anio Construccion", bien.getAnioConstruccion());
      this.add("Superficie Suelo", bien.getSuperficieSuelo());
      this.add("Superficie Construida", bien.getSuperficieConstruida());
      this.add("Superficie Util", bien.getSuperficieUtil());
      this.add("Anejo Garaje", bien.getAnejoGaraje());
      this.add("Anejo Trastero", bien.getAnejoTrastero());
      this.add("Anejo Otros", bien.getAnejoOtros());
      this.add("Id Saneamiento Judicial", null);
      this.add("Importe Adjudicado", null);
      this.add("Adjudicado", null);
      this.add("Fecha Decreto", null);
      this.add("Fecha Testimonio", null);
      this.add("Fecha Posesion/Lanzamiento", null);
      this.add("Notas1", bien.getNotas1());
      this.add("Notas2", bien.getNotas2());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
