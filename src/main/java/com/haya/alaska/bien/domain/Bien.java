package com.haya.alaska.bien.domain;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion;
import com.haya.alaska.formalizacion.domain.FormalizacionBien;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_via.domain.TipoVia;
import com.haya.alaska.uso.domain.Uso;
import com.haya.alaska.valoracion.domain.Valoracion;
import lombok.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;


@Audited
@AuditTable(value = "HIST_MSTR_BIEN")
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_BIEN")
public class Bien implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;
  @Column(name = "DES_ID_DATATAPE")
  private String idDatatape;
  @Column(name = "DES_ID_ORIGEN")
  private String idOrigen;
  @Column(name = "DES_ID_ORIGEN_2")
  private String idOrigen2;
//
//  @ManyToMany(cascade = {CascadeType.ALL})
//  @JoinTable(
//      name = "RELA_CONTRATO_BIEN",
//      joinColumns = @JoinColumn(name = "ID_BIEN"),
//      inverseJoinColumns = @JoinColumn(name = "ID_CONTRATO"))
//  private Set<Contrato> contratos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  private Set<ContratoBien> contratos = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_BIEN")
  private TipoBien tipoBien;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SUB_TIPO_BIEN")
  private SubTipoBien subTipoBien;

//  @ManyToOne(fetch = FetchType.LAZY,
//      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
//  @JoinColumn(name = "ID_TIPO_GARANTIA")
//  private TipoGarantia tipoGarantia;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  private Set<Tasacion> tasaciones = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  private Set<Valoracion> valoraciones = new HashSet<>();

  @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
  @JoinTable(
    name = "RELA_BIEN_CARGA",
    joinColumns = @JoinColumn(name = "ID_BIEN"),
    inverseJoinColumns = @JoinColumn(name = "ID_CARGA"))
  @AuditJoinTable(name = "HIST_RELA_BIEN_CARGA")
  private Set<Carga> cargas = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  private Set<PropuestaBien> bienes = new HashSet<>();

  @Column(name = "DES_REF_CATASTRAL")
  private String referenciaCatastral;
  @Column(name = "DES_FINCA")
  private String finca;
  @Column(name = "DES_LOC_REGISTRO")
  private String localidadRegistro;
  @Column(name = "DES_REGISTRO")
  private String registro;
  @Column(name = "DES_TOMO")
  private String tomo;
  @Column(name = "DES_LIBRO")
  private String libro;
  @Column(name = "DES_FOLIO")
  private String folio;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_INSCRIPCION")
  private Date fechaInscripcion;
  @Column(name = "DES_IDUFIR")
  private String idufir;
  @Column(name = "DES_REO_ORIGEN")
  private String reoOrigen;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_REO_COVERSION")
  private Date fechaReoConversion;
  /*@ManyToOne(fetch = FetchType.LAZY,
          cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_REO_ORIGEN")
  private ReoOrigen reoOrigen;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_FCH_REO_COVERSION")
  private FechaReoConversion fechaReoConversion;*/
  @Column(name = "IMP_GARANTIZADO")
  private Double importeGarantizado;
  @Column(name = "NUM_RESP_HIPOTECARIA")
  private Double responsabilidadHipotecaria;
  @Column(name = "IND_POS_NEGOCIADA")
  private Boolean posesionNegociada;
  @Column(name = "IND_TRAMITADO", nullable = false, columnDefinition = "boolean default false")
  private Boolean tramitado = false;
  @Column(name = "IND_SUBASTA")
  private Boolean subasta;
  @Column(name = "NUM_RANGO")
  private Double rango;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  private Set<BienSubastado> bienesSubastados = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_VIA")
  private TipoVia tipoVia;

  @Column(name = "DES_NOMBRE_VIA")
  private String nombreVia;
  @Column(name = "DES_NUMERO")
  private String numero;
  @Column(name = "DES_PORTAL")
  private String portal;
  @Column(name = "DES_BLOQUE")
  private String bloque;
  @Column(name = "DES_ESCALERA")
  private String escalera;
  @Column(name = "DES_PISO")
  private String piso;
  @Column(name = "DES_PUERTA")
  private String puerta;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ENTORNO")
  private Entorno entorno;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO")
  private Municipio municipio;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LOCALIDAD")
  private Localidad localidad;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA")
  private Provincia provincia;

  @Column(name = "DES_COMENT_DIRECC")
  private String comentarioDireccion;
  @Column(name = "DES_COD_POSTAL")
  private String codigoPostal;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PAIS")
  private Pais pais;

  @Column(name = "NUM_LATITUD")
  private Double latitud;
  @Column(name = "ID_LONGITUD")
  private Double longitud;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ACTIVO")
  private TipoActivo tipoActivo;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_USO")
  private Uso uso;

  @Column(name = "NUM_ANIO_CONTRUCT")
  private Integer anioConstruccion;
  @Column(name = "NUM_SUPERF_SUELO")
  private Double superficieSuelo;
  @Column(name = "NUM_SUPERF_CONTRUIDA")
  private Double superficieConstruida;
  @Column(name = "NUM_SUPERF_UTIL")
  private Double superficieUtil;
  @Column(name = "DES_ANEJO_GARAJE")
  private String anejoGaraje;
  @Column(name = "DES_ANEJO_TRASTERO")
  private String anejoTrastero;
  @Column(name = "DES_ANEJO_OTROS")
  private String anejoOtros;
  @Column(name = "DES_NOTAS_1")
  private String notas1;
  @Column(name = "DES_NOTAS_2")
  private String notas2;
  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;
  @Column(name = "ID_CARGA")
  private String idCarga;
  @Column(name = "DES_CARTERA_REM")
  private String carteraREM;
  @Column(name = "DES_SUBCARTERA_REM")
  private String subcarteraREM;
  @Column(name = "DES_DESCRIPCION")
  private String descripcion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CARGA")
  private Date fechaCarga;

  // Se actualizara automaticamente sobre los campos de Valoración y Tasacion
  @Getter(AccessLevel.NONE)
  @Column(name = "IMPORTE_BIEN")
  private Double importeBien;

  @OneToOne(mappedBy = "bien")
  private FormalizacionBien formalizacionBien;

  public Double getImporteBien()
  {
    long count = tasaciones.stream().count();
    if(count == 0) return 0.00;
    return tasaciones.stream().max(Comparator.comparing(Tasacion::getFecha)).get().getImporte();
  }

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN")
  @NotAudited
  private Set<BusinessPlanEstrategia> businessPlanEstrategias = new HashSet<>();


  @OneToOne(mappedBy = "bien")
  private BienPosesionNegociada bienPosesionNegociada;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_OCUPACION")
  private EstadoOcupacion estadoOcupacion;

  // TODO Descomentar en caso de que sea necesario enviar la lista ordenada

  //  public List<Tasacion> getSortedTasaciones() {
  //    List<Tasacion> tasacionesOrdenadas = new ArrayList<>(this.tasaciones);
  //    // TODO comprobar si está ordenado descendentemente
  //    tasacionesOrdenadas.sort((a, b) -> b.getFecha().compareTo(a.getFecha()));
  //    return tasacionesOrdenadas;
  //  }
  //
  //  public List<Valoracion> getSortedValoraciones() {
  //    List<Valoracion> valoracionesOrdenadas = new ArrayList<>(this.valoraciones);
  //    // TODO comprobar si está ordenado descendentemente
  //    valoracionesOrdenadas.sort((a, b) -> b.getFecha().compareTo(a.getFecha()));
  //    return valoracionesOrdenadas;
  //  }

  public String getDescripcion() {
    if (this.descripcion != null) {
      return this.descripcion;
    }
    if (this.getTipoBien() != null && this.getSubTipoBien() != null) {
      return this.getTipoBien().getValor().concat(" ").concat(this.getSubTipoBien().getValor());
    } else if (this.getTipoBien() != null) {
      return this.getTipoBien().getValor();
    } else if (this.getSubTipoBien() != null) { // TODO no tiene sentido
      return this.getSubTipoBien().getValor();
    }
    return "";
  }

  public double getSaldoValoracion() {
    var valoracion = getValoraciones().stream().max(Comparator.comparing(Valoracion::getFecha));
    var tasacion = getTasaciones().stream().max(Comparator.comparing(Tasacion::getFecha));
    if (valoracion.isPresent() && tasacion.isPresent()) {
      int cp = tasacion.get().getFecha().compareTo(valoracion.get().getFecha());
      return cp > 0 ?
        valoracion.get().getImporte() :
        cp < 0 ? tasacion.get().getImporte() :
          tasacion.get().getImporte() < valoracion.get().getImporte() ? tasacion.get().getImporte() : valoracion.get().getImporte();
    }
    if (valoracion.isPresent())
      return valoracion.get().getImporte();
    if (tasacion.isPresent())
      return tasacion.get().getImporte();
    return 0;
  }

  public boolean isMismaDireccion(Direccion d) {
    if (this.latitud != null
      && this.longitud != null
      && d.getLatitud() != null
      && d.getLongitud() != null) {
      if (this.latitud.equals(d.getLatitud()) && this.longitud.equals(d.getLongitud())) {
        return true;
      }
    }
    Boolean isNombreVia = compareIfNotNull(this.nombreVia, d.getNombre());
    Boolean isMunicipio = compareIfNotNull(this.municipio, d.getMunicipio());
    Boolean isProvincia = compareIfNotNull(this.provincia, d.getProvincia());

    if (isNombreVia != null && isMunicipio != null && isProvincia != null) {
      if (!isNombreVia || !isMunicipio || !isProvincia) {
        return false;
      }
    } else {
      return false;
    }

    if (!Objects.equals(this.numero, d.getNumero())) {
      return false;
    }
    if (!Objects.equals(this.portal, d.getPortal())) {
      return false;
    }
    if (!Objects.equals(this.bloque, d.getBloque())) {
      return false;
    }
    if (!Objects.equals(this.escalera, d.getEscalera())) {
      return false;
    }
    if (!Objects.equals(this.piso, d.getPiso())) {
      return false;
    }
    if (!Objects.equals(this.puerta, d.getPuerta())) {
      return false;
    }
    return Objects.equals(this.pais, d.getPais());
  }

  private static Boolean compareIfNotNull(Object a, Object b) {
    if (a == null || b == null) return null;
    return Objects.equals(a, b);
  }

  public ContratoBien getContratoBien(Integer contrato) {
    for (ContratoBien cb : this.getContratos()) {
      if (cb.getContrato() != null) {
        if (cb.getContrato().getId().equals(contrato)) {
          return cb;
        }
      }
      }
    return null;
  }

  public Tasacion getUltimaTasacion(){
    if (this.getTasaciones().isEmpty()) return null;
    return this.tasaciones.stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
  }
  public Valoracion getUltimaValoracion(){
    if (this.getValoraciones().isEmpty()) return null;
    return this.valoraciones.stream().max(Comparator.comparing(Valoracion::getFecha)).orElse(null);
  }

  public String getReferenciaCatastralNull() {
    if (this.referenciaCatastral == null) return "";
    return this.referenciaCatastral;
  }

  public String getIdufirNull() {
    if (this.idufir == null) return "";
    return this.idufir;
  }

  public String getRegistroNull() {
    if (this.registro == null) return "";
    return this.registro;
  }

  public Date getFechaInscripcionNull() {
    if (this.fechaInscripcion == null) return null;
    return this.fechaInscripcion;
  }

  public String getFincaNull() {
    if (this.finca == null) return "";
    return this.finca;
  }

  public String getTomoNull() {
    if (this.tomo == null) return "";
    return this.tomo;
  }

  public String getLibroNull() {
    if (this.libro == null) return "";
    return this.libro;
  }

  public String getFolioNull() {
    if (this.folio == null) return "";
    return this.folio;
  }

  public String getCodigoPostalNull() {
    if (this.codigoPostal == null) return "";
    return this.codigoPostal;
  }

  public String getDireccionCalle(){
    String result = "";

    if (this.nombreVia != null)
      result += this.nombreVia + " ";
    if (this.numero != null)
      result += this.numero + " ";
    if (this.portal != null)
      result += ", " + this.portal + " ";
    if (this.escalera != null)
      result += ", " + this.escalera + " ";
    if (this.piso != null)
      result += ", " + this.piso + " ";
    if (this.puerta != null)
      result += this.puerta + " ";

    return result;
  }
}
