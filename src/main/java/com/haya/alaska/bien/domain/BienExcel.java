package com.haya.alaska.bien.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class BienExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Collateral Id");
      cabeceras.add("Loan");
      cabeceras.add("Haya Id");
      cabeceras.add("Origin 1");
      cabeceras.add("Origin 2");
      cabeceras.add("Collateral Type");
      cabeceras.add("Collateral Subtype");
      cabeceras.add("Warranty/Solvency Type");
      cabeceras.add("Status");
      cabeceras.add("Cadastral Reference");
      cabeceras.add("Finca");
      cabeceras.add("Location Registration");
      cabeceras.add("Registration");
      cabeceras.add("Volume");
      cabeceras.add("Book");
      cabeceras.add("Folio");
      cabeceras.add("Registration Date");
      cabeceras.add("Idufir");
      cabeceras.add("Reo Origin");
      cabeceras.add("Reo Conversion Date");
      cabeceras.add("Guaranteed Amount");
      cabeceras.add("Mortgage Liability");
      cabeceras.add("Negotiated Possession");
      cabeceras.add("Auction");
      cabeceras.add("Via Type");
      cabeceras.add("Via Name");
      cabeceras.add("Number");
      cabeceras.add("Portal");
      cabeceras.add("Block");
      cabeceras.add("Stairs");
      cabeceras.add("Floor");
      cabeceras.add("Door");
      cabeceras.add("Environment");
      cabeceras.add("Municipality");
      cabeceras.add("Location");
      cabeceras.add("Province");
      cabeceras.add("Comment Address");
      cabeceras.add("Postal Code");
      cabeceras.add("Country");
      cabeceras.add("Length");
      cabeceras.add("Latitude");
      cabeceras.add("Active Type");
      cabeceras.add("Use");
      cabeceras.add("Year Construction");
      cabeceras.add("Floor area");
      cabeceras.add("Built-up area");
      cabeceras.add("Useful surface");
      cabeceras.add("Garage Annex");
      cabeceras.add("Storage Room Annex");
      cabeceras.add("Other Annex");
      cabeceras.add("Judicial Sanitation Id");
      cabeceras.add("Awarded Amount");
      cabeceras.add("Awarded");
      cabeceras.add("Decree Date");
      cabeceras.add("Testimonial Date");
      cabeceras.add("Ownership/Release Date");
      cabeceras.add("Additional Data");
      cabeceras.add("Additional Data 2");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Bien");
      cabeceras.add("Contrato");
      cabeceras.add("Id Haya");
      cabeceras.add("Origen 1");
      cabeceras.add("Origen 2");
      cabeceras.add("Tipo Bien");
      cabeceras.add("Subtipo Bien");
      cabeceras.add("Tipo Garantia/Solvencia");
      cabeceras.add("Estado");
      cabeceras.add("Referencia Catastral");
      cabeceras.add("Finca");
      cabeceras.add("Localidad Registro");
      cabeceras.add("Registro");
      cabeceras.add("Tomo");
      cabeceras.add("Libro");
      cabeceras.add("Folio");
      cabeceras.add("Fecha Inscripcion");
      cabeceras.add("Idufir");
      cabeceras.add("Reo Origen");
      cabeceras.add("Fecha Reo Conversion");
      cabeceras.add("Importe Garantizado");
      cabeceras.add("Responsabilidad Hipotecaria");
      cabeceras.add("Posesion Negociada");
      cabeceras.add("Subasta");
      cabeceras.add("Tipo Via");
      cabeceras.add("Nombre Via");
      cabeceras.add("Numero");
      cabeceras.add("Portal");
      cabeceras.add("Bloque");
      cabeceras.add("Escalera");
      cabeceras.add("Piso");
      cabeceras.add("Puerta");
      cabeceras.add("Entorno");
      cabeceras.add("Municipio");
      cabeceras.add("Localidad");
      cabeceras.add("Provincia");
      cabeceras.add("Comentario Direccion");
      cabeceras.add("Codigo Postal");
      cabeceras.add("Pais");
      cabeceras.add("Longitud");
      cabeceras.add("Latitud");
      cabeceras.add("Tipo Activo");
      cabeceras.add("Uso");
      cabeceras.add("Año Construccion");
      cabeceras.add("Superficie Suelo");
      cabeceras.add("Superficie Construida");
      cabeceras.add("Superficie Util");
      cabeceras.add("Anejo Garaje");
      cabeceras.add("Anejo Trastero");
      cabeceras.add("Anejo Otros");
      cabeceras.add("Id Saneamiento Judicial");
      cabeceras.add("Importe Adjudicado");
      cabeceras.add("Adjudicado");
      cabeceras.add("Fecha Decreto");
      cabeceras.add("Fecha Testimonio");
      cabeceras.add("Fecha Posesion/Lanzamiento");
      cabeceras.add("Datos adicionales");
      cabeceras.add("Datos adicionales 2");
    }
  }

  public BienExcel(Bien bien, Integer idContrato) {
    super();

    ContratoBien cb = bien.getContratoBien(idContrato);
    Contrato c = cb.getContrato();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Connection Id", c.getExpediente().getIdConcatenado());
      this.add("Collateral Id", bien.getId());
      this.add("Loan", c.getIdCarga());
      this.add("Haya Id", bien.getIdHaya());
      this.add("Origin 1", bien.getIdCarga());
      this.add("Origin 2", bien.getIdOrigen());
      this.add(
          "Collateral Type",
          bien.getTipoBien() != null ? bien.getTipoBien().getValorIngles() : null);
      this.add(
          "Collateral Subtype",
          bien.getSubTipoBien() != null ? bien.getSubTipoBien().getValorIngles() : null);
      this.add(
          "Warranty/Solvency Type",
          cb.getTipoGarantia() != null ? cb.getTipoGarantia().getValorIngles() : null);
      this.add("Status", Boolean.TRUE.equals(bien.getActivo()) ? "Activated" : "Released");
      this.add("Cadastral Reference", bien.getReferenciaCatastral());
      this.add("Finca", bien.getFinca());
      this.add("Location Registration", bien.getLocalidadRegistro());
      this.add("Registration", bien.getRegistro());
      this.add("Volume", bien.getTomo());
      this.add("Book", bien.getLibro());
      this.add("Folio", bien.getFolio());
      this.add("Registration Date", bien.getFechaInscripcion());
      this.add("Idufir", bien.getIdufir());
      this.add("Reo Origin", bien.getReoOrigen());
      this.add("Reo Conversion Date", bien.getFechaReoConversion());
      this.add("Guaranteed Amount", cb.getImporteGarantizado());
      this.add("Mortgage Liability", cb.getResponsabilidadHipotecaria());
      this.add(
          "Negotiated Possession", Boolean.TRUE.equals(bien.getPosesionNegociada()) ? "YES" : "NO");
      this.add("Auction", Boolean.TRUE.equals(bien.getSubasta()) ? "YES" : "NO");
      this.add("Via Type", bien.getTipoVia() != null ? bien.getTipoVia().getValorIngles() : null);
      this.add("Via Name", bien.getNombreVia());
      this.add("Number", bien.getNumero());
      this.add("Portal", bien.getPortal());
      this.add("Block", bien.getBloque());
      this.add("Stairs", bien.getEscalera());
      this.add("Floor", bien.getPiso());
      this.add("Door", bien.getPuerta());
      this.add(
          "Environment", bien.getEntorno() != null ? bien.getEntorno().getValorIngles() : null);
      this.add(
          "Municipality",
          bien.getMunicipio() != null ? bien.getMunicipio().getValorIngles() : null);
      this.add(
          "Location", bien.getLocalidad() != null ? bien.getLocalidad().getValorIngles() : null);
      this.add(
          "Province", bien.getProvincia() != null ? bien.getProvincia().getValorIngles() : null);
      this.add("Comment Address", bien.getComentarioDireccion());
      this.add("Postal Code", bien.getCodigoPostal());
      this.add("Country", bien.getPais() != null ? bien.getPais().getValorIngles() : null);
      this.add("Length", bien.getLongitud());
      this.add("Latitude", bien.getLatitud());
      this.add(
          "Active Type",
          bien.getTipoActivo() != null ? bien.getTipoActivo().getValorIngles() : null);
      this.add("Use", bien.getUso() != null ? bien.getUso().getValorIngles() : null);
      this.add("Year Construction", bien.getAnioConstruccion());
      this.add("Floor area", bien.getSuperficieSuelo());
      this.add("Built-up area", bien.getSuperficieConstruida());
      this.add("Useful surface", bien.getSuperficieUtil());
      this.add("Garage Annex", bien.getAnejoGaraje());
      this.add("Storage Room Annex", bien.getAnejoTrastero());
      this.add("Other Annex", bien.getAnejoOtros());
      this.add("Judicial Sanitation Id", null);
      this.add("Awarded Amount", null);
      this.add("Awarded", null);
      this.add("Decree Date", null);
      this.add("Testimonial Date", null);
      this.add("Ownership/Release Date", null);
      this.add("Additional Data", bien.getNotas1());
      this.add("Additional Data 2", bien.getNotas2());
    } else {
      this.add("Id Expediente", c.getExpediente().getIdConcatenado());
      this.add("Id Bien", bien.getId());
      this.add("Contrato", c.getIdCarga());
      this.add("Id Haya", bien.getIdHaya());
      this.add("Origen 1", bien.getIdCarga());
      this.add("Origen 2", bien.getIdOrigen());
      this.add("Tipo Bien", bien.getTipoBien() != null ? bien.getTipoBien().getValor() : null);
      this.add(
          "Subtipo Bien", bien.getSubTipoBien() != null ? bien.getSubTipoBien().getValor() : null);
      this.add(
          "Tipo Garantia/Solvencia",
          cb.getTipoGarantia() != null ? cb.getTipoGarantia().getValor() : null);
      this.add("Estado", Boolean.TRUE.equals(bien.getActivo()) ? "Activa" : "Liberada");
      this.add("Referencia Catastral", bien.getReferenciaCatastral());
      this.add("Finca", bien.getFinca());
      this.add("Localidad Registro", bien.getLocalidadRegistro());
      this.add("Registro", bien.getRegistro());
      this.add("Tomo", bien.getTomo());
      this.add("Libro", bien.getLibro());
      this.add("Folio", bien.getFolio());
      this.add("Fecha Inscripcion", bien.getFechaInscripcion());
      this.add("Idufir", bien.getIdufir());
      this.add("Reo Origen", bien.getReoOrigen());
      this.add("Fecha Reo Conversion", bien.getFechaReoConversion());
      this.add("Importe Garantizado", cb.getImporteGarantizado());
      this.add("Responsabilidad Hipotecaria", cb.getResponsabilidadHipotecaria());
      this.add(
          "Posesion Negociada", Boolean.TRUE.equals(bien.getPosesionNegociada()) ? "Si" : "NO");
      this.add("Subasta", Boolean.TRUE.equals(bien.getSubasta()) ? "Si" : "NO");
      this.add("Tipo Via", bien.getTipoVia() != null ? bien.getTipoVia().getValor() : null);
      this.add("Nombre Via", bien.getNombreVia());
      this.add("Numero", bien.getNumero());
      this.add("Portal", bien.getPortal());
      this.add("Bloque", bien.getBloque());
      this.add("Escalera", bien.getEscalera());
      this.add("Piso", bien.getPiso());
      this.add("Puerta", bien.getPuerta());
      this.add("Entorno", bien.getEntorno() != null ? bien.getEntorno().getValor() : null);
      this.add("Municipio", bien.getMunicipio() != null ? bien.getMunicipio().getValor() : null);
      this.add("Localidad", bien.getLocalidad() != null ? bien.getLocalidad().getValor() : null);
      this.add("Provincia", bien.getProvincia() != null ? bien.getProvincia().getValor() : null);
      this.add("Comentario Direccion", bien.getComentarioDireccion());
      this.add("Codigo Postal", bien.getCodigoPostal());
      this.add("Pais", bien.getPais() != null ? bien.getPais().getValor() : null);
      this.add("Longitud", bien.getLongitud());
      this.add("Latitud", bien.getLatitud());
      this.add(
          "Tipo Activo", bien.getTipoActivo() != null ? bien.getTipoActivo().getValor() : null);
      this.add("Uso", bien.getUso() != null ? bien.getUso().getValor() : null);
      this.add("Año Construccion", bien.getAnioConstruccion());
      this.add("Superficie Suelo", bien.getSuperficieSuelo());
      this.add("Superficie Construida", bien.getSuperficieConstruida());
      this.add("Superficie Util", bien.getSuperficieUtil());
      this.add("Anejo Garaje", bien.getAnejoGaraje());
      this.add("Anejo Trastero", bien.getAnejoTrastero());
      this.add("Anejo Otros", bien.getAnejoOtros());
      this.add("Id Saneamiento Judicial", null);
      this.add("Importe Adjudicado", null);
      this.add("Adjudicado", null);
      this.add("Fecha Decreto", null);
      this.add("Fecha Testimonio", null);
      this.add("Fecha Posesion/Lanzamiento", null);
      this.add("Datos adicionales", bien.getNotas1());
      this.add("Datos adicionales 2", bien.getNotas2());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
