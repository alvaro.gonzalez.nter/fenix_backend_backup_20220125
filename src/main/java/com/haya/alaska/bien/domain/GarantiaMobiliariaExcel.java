package com.haya.alaska.bien.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class GarantiaMobiliariaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      cabeceras.add("Asset Id");
      cabeceras.add("Loan");
      cabeceras.add("Connection Id");
      cabeceras.add("Origin");
      cabeceras.add("Origin 2");
      cabeceras.add("Warranty/Solvency Type");
      cabeceras.add("Status");
      cabeceras.add("Value");
      cabeceras.add("Description");
      cabeceras.add("Mortgage liability");
      cabeceras.add("Negotiated Possession");
      cabeceras.add("Auction");
      cabeceras.add("Judicial Sanitation Id");
      cabeceras.add("Awarded amount");
      cabeceras.add("Awarded");
      cabeceras.add("Decree Date");
      cabeceras.add("Testimonial Date");
      cabeceras.add("Ownership/Release Date");
      cabeceras.add("Additional Data1");
      cabeceras.add("Additional Data2");

    } else {

      cabeceras.add("Id Bien");
      cabeceras.add("Contrato");
      cabeceras.add("Id Expediente");
      cabeceras.add("Origen");
      cabeceras.add("Origen 2");
      cabeceras.add("Tipo Garantia");
      cabeceras.add("Estado");
      cabeceras.add("Valor");
      cabeceras.add("Descripcion");
      cabeceras.add("Responsabilidad Hipotecaria");
      cabeceras.add("Posesion Negociada");
      cabeceras.add("Subasta");
      cabeceras.add("Id Saneamiento Judicial");
      cabeceras.add("Importe Adjudicado");
      cabeceras.add("Adjudicado");
      cabeceras.add("Fecha Decreto");
      cabeceras.add("Fecha Testimonio");
      cabeceras.add("Fecha Posesion/Lanzamiento");
      cabeceras.add("Notas1");
      cabeceras.add("Notas2");
    }
  }

  public GarantiaMobiliariaExcel(Bien bien, Contrato contrato) {
    super();

    ContratoBien cb = bien.getContratoBien(contrato.getId());

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      this.add("Asset Id", bien.getId());
      this.add("Loan", contrato.getIdCarga());
      this.add(
        "Connection Id",
        contrato.getExpediente() != null ? contrato.getExpediente().getIdConcatenado() : null);
      this.add("Origin", bien.getIdCarga());
      this.add("Origin 2", bien.getIdOrigen());
      this.add("Warranty/Solvency Type", cb.getTipoGarantia());
      this.add("Status", bien.getActivo() ? "Activate" : "Released");
      this.add("Value", cb.getImporteGarantizado());
      this.add("Description", bien.getDescripcion());
      this.add("Mortgage liability", cb.getResponsabilidadHipotecaria());
      this.add("Negotiated Possession", bien.getPosesionNegociada());
      this.add("Auction", bien.getSubasta());
      this.add("Judicial Sanitation Id", null);
      this.add("Awarded amount", null);
      this.add("Awarded", null);
      this.add("Decree Date", null);
      this.add("Testimonial Date", null);
      this.add("Ownership/Release Date", null);
      this.add("Additional Data1", bien.getNotas1());
      this.add("Additional Data2", bien.getNotas2());

    }

    else{

      this.add("Id Bien", bien.getId());
      this.add("Contrato", contrato.getIdCarga());
      this.add(
        "Id Expediente",
        contrato.getExpediente() != null ? contrato.getExpediente().getIdConcatenado() : null);
      this.add("Origen", bien.getIdCarga());
      this.add("Origen 2", bien.getIdOrigen());
      this.add("Tipo Garantia", cb.getTipoGarantia());
      this.add("Estado", bien.getActivo() ? "Activa" : "Liberada");
      this.add("Valor", cb.getImporteGarantizado());
      this.add("Descripcion", bien.getDescripcion());
      this.add("Responsabilidad Hipotecaria", cb.getResponsabilidadHipotecaria());
      this.add("Posesion Negociada", bien.getPosesionNegociada());
      this.add("Subasta", bien.getSubasta());
      this.add("Id Saneamiento Judicial", null);
      this.add("Importe Adjudicado", null);
      this.add("Adjudicado", null);
      this.add("Fecha Decreto", null);
      this.add("Fecha Testimonio", null);
      this.add("Fecha Posesion/Lanzamiento", null);
      this.add("Notas1", bien.getNotas1());
      this.add("Notas2", bien.getNotas2());

    }


  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
