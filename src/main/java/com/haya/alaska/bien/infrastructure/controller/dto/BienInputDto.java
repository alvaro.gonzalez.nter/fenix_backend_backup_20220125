package com.haya.alaska.bien.infrastructure.controller.dto;

import com.haya.alaska.carga.infrastructure.controller.dto.CargaDto;
import com.haya.alaska.tasacion.infrastructure.controller.dto.TasacionDto;
import com.haya.alaska.valoracion.controller.dto.ValoracionDto;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienInputDto extends BienDto {
  private Integer id;
  private String tipoGarantia;
  private String liquidez;
  private String tipoVia;
  private String entorno;
  private String municipio;
  private String localidad;
  private String provincia;
  private String pais;
  private String tipoActivo;
  private String tipoBien;
  private String subTipoBien;
  private String uso;
  private String estadoOcupacion;
  private List<TasacionDto> tasaciones;

  private List<ValoracionDto> valoraciones;
  private List<CargaDto> cargas;
}
