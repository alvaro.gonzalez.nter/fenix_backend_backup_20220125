package com.haya.alaska.bien.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienDto implements Serializable {

  private String idHaya;
  private String idOrigen;
  private String idOrigen2;
  private String idCarga;
  private Boolean activo;
  private String referenciaCatastral;
  private String finca;
  private String localidadRegistro;
  private String registro;
  private String tomo;
  private String libro;
  private String folio;
  private Date fechaInscripcion;
  private String idufir;
  private Integer reoOrigen;
  private Date fechaReoConversion;
  private Double responsabilidadHipotecaria;
  private Boolean posesionNegociada;
  private Boolean subasta;
  private Double rango;
  private Double importeGarantizado;
  private String descripcion;

  private String nombreVia;
  private String numero;
  private String portal;
  private String bloque;
  private String escalera;
  private String piso;
  private String puerta;
  private String comentarioDireccion;
  private String codigoPostal;
  private Double longitud;
  private Double latitud;
  private Integer anioConstruccion;
  private Double superficieSuelo;
  private Double superficieUtil;
  private Double superficieConstruida;
  private String anejoGaraje;
  private String anejoTrastero;
  private String anejoOtros;
  private Boolean estado;

  private String notas1;
  private String notas2;
}
