package com.haya.alaska.bien.infrastructure.controller;

import com.haya.alaska.bien.application.BienUseCase;
import com.haya.alaska.bien.infrastructure.controller.dto.BienInputDto;
import com.haya.alaska.bien.infrastructure.controller.dto.BienOutputDto;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.dto.BienDTOToList;
import com.haya.alaska.shared.dto.BienExtendedDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/expedientes/{idExpediente}/contratos/{idContrato}/bienes")
public class BienController {

  @Autowired private BienUseCase bienUseCase;

  @ApiOperation(value = "Listado garantías",
          notes = "Devuelve todas las garantías del contrato registradas en base de datos")
  @GetMapping("/garantias")
  public ListWithCountDTO<BienDTOToList> getAllGarantias(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @RequestParam(value = "id", required = false) Integer id,
          @RequestParam(value = "idOrigen", required = false) String idOrigen,
          @RequestParam(value = "tipoActivo", required = false) String tipoActivo,
          @RequestParam(value = "estado", required = false) Boolean estado,
          @RequestParam(value = "finca", required = false) String fincaRegistral,
          @RequestParam(value = "localidad", required = false) String localidad,
          @RequestParam(value = "provincia", required = false) String provincia,
          @RequestParam(value = "importeGarantizado", required = false) String importeGarantizado,
          @RequestParam(value = "descripcion", required = false) String descripcion,
          @RequestParam(value = "responsabilidadHipotecaria", required = false) String responsabilidad,
          @RequestParam(value = "liquidez", required = false) String liquidez,
          @RequestParam(value = "posesionNegociada", required = false) Boolean posesionNegociada,
          @RequestParam(value = "orderField", required = false) String orderField,
          @RequestParam(value = "orderDirection", required = false) String orderDirection,
          @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) {
    boolean esGarantia = true;

    return bienUseCase.getAllFilteredBienes(
            esGarantia, idContrato, id,idOrigen, tipoActivo, estado, fincaRegistral, localidad, provincia, importeGarantizado, descripcion,
            responsabilidad, liquidez, posesionNegociada, orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Listado garantías",
    notes = "Devuelve todas las garantías del contrato registradas en base de datos")
  @GetMapping("/movil/garantias")
  public ListWithCountDTO<BienExtendedDTOToList> getAllGarantiasMovil(
    @PathVariable("idExpediente") Integer idExpediente,
    @PathVariable("idContrato") Integer idContrato,
    @RequestParam(value = "id", required = false) Integer id,
    @RequestParam(value = "idOrigen", required = false) String idOrigen,
    @RequestParam(value = "tipoActivo", required = false) String tipoActivo,
    @RequestParam(value = "estado", required = false) Boolean estado,
    @RequestParam(value = "finca", required = false) String fincaRegistral,
    @RequestParam(value = "localidad", required = false) String localidad,
    @RequestParam(value = "provincia", required = false) String provincia,
    @RequestParam(value = "importeGarantizado", required = false) String importeGarantizado,
    @RequestParam(value = "descripcion", required = false) String descripcion,
    @RequestParam(value = "responsabilidadHipotecaria", required = false) String responsabilidad,
    @RequestParam(value = "liquidez", required = false) String liquidez,
    @RequestParam(value = "posesionNegociada", required = false) Boolean posesionNegociada,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) {
    boolean esGarantia = true;

    return bienUseCase.getAllFilteredBienesExtended(
      esGarantia, idContrato, id,idOrigen, tipoActivo, estado, fincaRegistral, localidad, provincia, importeGarantizado, descripcion,
      responsabilidad, liquidez, posesionNegociada, orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Listado solvencias",
          notes = "Devuelve todas las solvencias del contrato registradas en base de datos")
  @GetMapping("/solvencias")
  public ListWithCountDTO<BienDTOToList> getAllSolvencias(
          @PathVariable("idExpediente") Integer idExpediente,
          @PathVariable("idContrato") Integer idContrato,
          @RequestParam(value = "id", required = false) Integer id,
          @RequestParam(value = "tipoActivo", required = false) String tipoActivo,
          @RequestParam(value = "estado", required = false) Boolean estado,
          @RequestParam(value = "fincaRegistral", required = false) String fincaRegistral,
          @RequestParam(value = "localidad", required = false) String localidad,
          @RequestParam(value = "provincia", required = false) String provincia,
          @RequestParam(value = "importeGarantizado", required = false) String importeGarantizado,
          @RequestParam(value = "descripcion", required = false) String descripcion,
          @RequestParam(value = "responsabilidadHipotecaria", required = false) String responsabilidad,
          @RequestParam(value = "liquidez", required = false) String liquidez,
          @RequestParam(value = "posesionNegociada", required = false) Boolean posesionNegociada,
          @RequestParam(value = "orderField", required = false) String orderField,
          @RequestParam(value = "orderDirection", required = false) String orderDirection,
          @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page) {
    boolean esGarantia = false;

    return bienUseCase.getAllFilteredBienes(
            esGarantia, idContrato, id,null, tipoActivo, estado, fincaRegistral, localidad, provincia, importeGarantizado, descripcion,
            responsabilidad, liquidez, posesionNegociada, orderField, orderDirection, size, page);
  }

  @ApiOperation(value = "Obtener bien", notes = "Devuelve el bien con id enviado")
  @GetMapping("/{idBien}")
  public BienOutputDto getBienById(@PathVariable("idExpediente") Integer idExpediente, @PathVariable("idContrato") Integer idContrato, @PathVariable("idBien") Integer idBien) throws IllegalAccessException {
    return bienUseCase.findBienByIdDto(idBien, idContrato);
  }

  @ApiOperation(value = "Actualizar bien con latitud y longitud", notes = "Actualizar el bien con id enviado")
  @PutMapping("/{idBien}")
  @Transactional(rollbackFor = Exception.class)
  public BienOutputDto updateBienExtended(@PathVariable("idExpediente") Integer idExpediente,
                                  @PathVariable("idContrato") Integer idContrato, @PathVariable("idBien") Integer idBien,
                                  @RequestBody @Valid BienInputDto bienInputDTO) throws Exception {
    return bienUseCase.update(idBien, idContrato, bienInputDTO);
  }

  @ApiOperation(value = "Crear bien", notes = "Añadir un nuevo bien al contrato con el id enviado")
  @PostMapping
  @Transactional(rollbackFor = Exception.class)
  public BienOutputDto createBien(@PathVariable("idExpediente") Integer idExpediente,
                            @PathVariable("idContrato") Integer idContrato, @RequestBody @Valid BienInputDto bienInputDTO)
          throws Exception {
    return bienUseCase.create(idContrato, bienInputDTO);
  }

  @ApiOperation(value = "Eliminar bien", notes = "Eliminar el contrato con id enviado")
  @DeleteMapping("/{id}")
  public void deleteBien(@PathVariable("idExpediente") Integer idExpediente,
                         @PathVariable("idContrato") Integer idContrato, @PathVariable("id") Integer idBien) {
    bienUseCase.delete(idBien, idContrato);
  }

  @ApiOperation(value = "Obtener documentación de los bienes a nivel contrato o a nivel expediente",
          notes = "Devuelve la documentación disponible de los bienes del expediente o del contrato con id enviado")
  @GetMapping("/documentos")
  @Transactional
  public ListWithCountDTO<DocumentoDTO> getDocumentosByExpediente(
          @PathVariable("idContrato") Integer idContrato,
          @PathVariable(value = "idExpediente", required = false)Integer idExpediente,
          @RequestParam(value="id",required = false) String idDocumento,
          @RequestParam(value = "nombreArchivo", required = false) String nombreArchivo,
          @RequestParam(value = "idHaya", required = false) String idHaya,
          @RequestParam(value="usuarioCreador",required = false) String usuarioCreador,
          @RequestParam(value="tipo",required = false) String tipo,
          @RequestParam(value = "idEntidad", required = false) String idEntidad,
          @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
          @RequestParam(value = "orderField", required = false) String orderField,
          @RequestParam(value = "orderDirection", required = false) String orderDirection,
          @RequestParam(value = "size") Integer size,
          @RequestParam(value = "page") Integer page) throws NotFoundException, IOException {
   if (idContrato==-1){
     return bienUseCase.findDocumentosByExpedienteId(idExpediente,idDocumento,usuarioCreador,tipo,idEntidad,fechaActualizacion,nombreArchivo,idHaya,orderDirection,orderField,size,page);
   }else{
     return bienUseCase.findDocumentosByContratoId(idContrato,idDocumento,usuarioCreador,tipo,idEntidad,fechaActualizacion,nombreArchivo,idHaya,orderDirection,orderField,size,page);
   }
  }

  @ApiOperation(value = "Añadir un documento a un bien")
  @PostMapping("/{idBien}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoBien(@PathVariable("idExpediente") Integer idExpediente,
                                  @PathVariable("idContrato") Integer idContrato,
                                  @PathVariable("idBien") Integer idBien,
                                  @RequestPart("file") @NotNull @NotBlank MultipartFile file,
                                  @RequestPart("metadatos") MetadatoDocumentoInputDTO metadatoDocumento,
                                  @RequestParam(value = "tipoDocumento", required = false) String tipoDocumento,
                                  @ApiIgnore CustomUserDetails principal)
          throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    bienUseCase.createDocumento(idBien, file,metadatoDocumento,usuario,tipoDocumento);
  }

  @ApiOperation(value = "Descargar información de bienes en Excel", notes = "Descargar información en Excel de los bienes por id de contrato")
  @GetMapping("/excel")
  public ResponseEntity<InputStreamResource> getExcelBienById(@PathVariable Integer idContrato) throws IOException {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(this.bienUseCase.findExcelBienById(idContrato));

    return excelExport.download(excel, idContrato + "_garantias_solvencias_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(value = "Descargar información del  bien con el id recibido en Excel", notes = "Descargar información en Excel del bien por id de bien")
  @GetMapping("/{idBien}/excel")
  public ResponseEntity<InputStreamResource> getExcelBienById(@PathVariable Integer idContrato,
                                                              @PathVariable("idBien") Integer idBien) throws IOException {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(this.bienUseCase.findExcelBienByIdBien(idContrato, idBien));

    return excelExport.download(excel, idContrato + "_garantia_solvencia_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(value = "Devuelve los bienes asociados a un expediente")
  @GetMapping("/all")
  @ResponseStatus(HttpStatus.OK)
  public List<BienDTOToList> getAllBienes(
          @PathVariable("idExpediente") Integer idExpediente) {
    List<BienDTOToList> bienes = bienUseCase.getAllByExpedienteId(idExpediente);
    return bienes;
  }

  @ApiOperation(
    value = "Obtener documentación de un bien",
    notes =
      "Devuelve la documentación disponible del bien con id enviado")
  @GetMapping("/{idBien}/documentosBienes")
  @Transactional
  public ListWithCountDTO<DocumentoDTO> getDocumentosByBien(
    @PathVariable("idBien") Integer idBien,
    @RequestParam(value="id",required = false) String idDocumento,
    @RequestParam(value="usuarioCreador",required = false) String usuarioCreador,
    @RequestParam(value="tipo",required = false) String tipo,
    @RequestParam(value = "idEntidad", required = false) String idEntidad,
    @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page)
    throws NotFoundException, IOException {
    return bienUseCase.findDocumentosByBienId(idBien,idDocumento,usuarioCreador,tipo,idEntidad,fechaActualizacion,orderDirection,orderField,size,page);
  }
}
