package com.haya.alaska.bien.infrastructure.controller.dto;

import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BienVinculacionDto {
  private Integer idExpediente;
  private String idConcatenado;
  private UsuarioDTO nombreGestor;
  private String contrato;
  private String primerTitular;
}
