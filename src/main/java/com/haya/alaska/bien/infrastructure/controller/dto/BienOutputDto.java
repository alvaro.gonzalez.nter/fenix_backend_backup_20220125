package com.haya.alaska.bien.infrastructure.controller.dto;

import com.haya.alaska.bien_subastado.infrastructure.controller.dto.BienSubastadoDTOToList;
import com.haya.alaska.carga.infrastructure.controller.dto.CargaDto;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.shared.dto.CargaDTOToList;
import com.haya.alaska.shared.dto.TasacionDTOToList;
import com.haya.alaska.shared.dto.ValoracionDTOToList;
import com.haya.alaska.tasacion.infrastructure.controller.dto.TasacionDto;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienOutputDto extends BienDto{
  private Integer id;
  private Integer idBienPosesionNegociada;
  private CatalogoMinInfoDTO tipoGarantia;
  private CatalogoMinInfoDTO tipoBien;
  private CatalogoMinInfoDTO subTipoBien;
  private CatalogoMinInfoDTO liquidez;
  private boolean esGarantia;
  private CatalogoMinInfoDTO tipoVia;
  private CatalogoMinInfoDTO entorno;
  private CatalogoMinInfoDTO municipio;
  private CatalogoMinInfoDTO localidad;
  private CatalogoMinInfoDTO provincia;
  private CatalogoMinInfoDTO pais;
  private CatalogoMinInfoDTO tipoActivo;
  private CatalogoMinInfoDTO uso;
  private CatalogoMinInfoDTO estadoOcupacion; // EstadoOcupacion

  private List<TasacionDTOToList> tasaciones = new ArrayList<>();
  private List<ValoracionDTOToList> valoraciones = new ArrayList<>();
  private List<CargaDTOToList> cargas = new ArrayList<>();

  private BienOutputDto historico;

  private List<BienSubastadoDTOToList> saneamiento;

  private Boolean asociadoPN;
  private TasacionDto tasacionMayorImporte;
  private CargaDto cargaMayorImporte;
}
