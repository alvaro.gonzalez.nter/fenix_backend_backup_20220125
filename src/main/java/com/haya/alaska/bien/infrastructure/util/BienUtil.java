package com.haya.alaska.bien.infrastructure.util;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.controller.dto.BienInputDto;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.infrastructure.controller.dto.CargaDto;
import com.haya.alaska.carga.infrastructure.mapper.CargaMapper;
import com.haya.alaska.carga.infrastructure.repository.CargaRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.infrastructure.repository.ContratoBienRepository;
import com.haya.alaska.entorno.infrastructure.repository.EntornoRepository;
import com.haya.alaska.estado_ocupacion.infrastructure.repository.EstadoOcupacionRepository;
import com.haya.alaska.integraciones.maestro.ServicioMaestro;
import com.haya.alaska.interviniente.infrastructure.util.IntervinienteUtil;
import com.haya.alaska.liquidez.infrastructure.repository.LiquidezRepository;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.pais.infrastructure.repository.PaisRepository;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.shared.exceptions.LockedChangeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.sub_tipo_bien.infrastructure.repository.SubTipoBienRepository;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.infrastructure.controller.dto.TasacionDto;
import com.haya.alaska.tasacion.infrastructure.mapper.TasacionMapper;
import com.haya.alaska.tasacion.infrastructure.repository.TasacionRepository;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_garantia.infrastructure.repository.TipoGarantiaRepository;
import com.haya.alaska.tipo_via.infrastructure.repository.TipoViaRepository;
import com.haya.alaska.uso.infrastructure.repository.UsoRepository;
import com.haya.alaska.valoracion.controller.dto.ValoracionDto;
import com.haya.alaska.valoracion.domain.Valoracion;
import com.haya.alaska.valoracion.infrastructure.mapper.ValoracionMapper;
import com.haya.alaska.valoracion.infrastructure.repository.ValoracionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class BienUtil {

    @Autowired
    private TasacionMapper tasacionMapper;
    @Autowired
    private ValoracionMapper valoracionMapper;
    @Autowired
    private CargaMapper cargaMapper;

    @Autowired private TipoGarantiaRepository tipoGarantiaRepository;
    @Autowired private LiquidezRepository liquidezRepository;
    @Autowired private TipoViaRepository tipoViaRepository;
    @Autowired private EntornoRepository entornoRepository;
    @Autowired private MunicipioRepository municipioRepository;
    @Autowired private LocalidadRepository localidadRepository;
    @Autowired private ProvinciaRepository provinciaRepository;
    @Autowired private PaisRepository paisRepository;
    @Autowired private TipoActivoRepository tipoActivoRepository;
    @Autowired private TipoBienRepository tipoBienRepository;
    @Autowired private SubTipoBienRepository subTipoBienRepository;
    @Autowired private UsoRepository usoRepository;
    @Autowired private EstadoOcupacionRepository estadoOcupacionRepository;
    @Autowired private TasacionRepository tasacionRepository;
    @Autowired private ValoracionRepository valoracionRepository;
    @Autowired private CargaRepository cargaRepository;
    @Autowired private BienRepository bienRepository;
    @Autowired private ContratoRepository contratoRepository;
    @Autowired private ContratoBienRepository contratoBienRepository;
    @Autowired private IntervinienteUtil intervinienteUtil;
    @Autowired ServicioMaestro servicioMaestro;

    public Bien createBien(Bien bien, Cartera cartera, BienInputDto bienInputDto, Integer idContrato, boolean rem) throws NotFoundException {

        BeanUtils.copyProperties(bienInputDto, bien, "tasaciones", "valoraciones", "cargas", "responsabilidadHipotecaria", "importeGarantizado");

        bien.setActivo(true);
        bien.setTramitado(false);

        bien = bienRepository.saveAndFlush(bien);

        if(idContrato != null){
          ContratoBien newCb = new ContratoBien();
          newCb.setContrato(contratoRepository.findById(idContrato).orElseThrow(()->
            new NotFoundException("contrato", idContrato)));
          newCb.setBien(bien);
          if (bienInputDto.getTipoGarantia() != null)
            newCb.setTipoGarantia(tipoGarantiaRepository.findById(Integer.valueOf(bienInputDto.getTipoGarantia())).orElse(null));
          if (bienInputDto.getLiquidez() != null)
            newCb.setLiquidez(liquidezRepository.findById(Integer.valueOf(bienInputDto.getLiquidez())).orElse(null));
          if (bienInputDto.getResponsabilidadHipotecaria() != null)
            newCb.setResponsabilidadHipotecaria(bienInputDto.getResponsabilidadHipotecaria());
          if (bienInputDto.getImporteGarantizado() != null)
            newCb.setImporteGarantizado(bienInputDto.getImporteGarantizado());
          if (bienInputDto.getRango() != null)
            newCb.setRango(bienInputDto.getRango());
          if (bienInputDto.getEstado() != null)
            newCb.setEstado(bienInputDto.getEstado());
          contratoBienRepository.saveAndFlush(newCb);
        } else {
          bien.setImporteGarantizado(bienInputDto.getImporteGarantizado());
          bien.setResponsabilidadHipotecaria(bienInputDto.getResponsabilidadHipotecaria());
        }

        if(!rem) {
          if (bienInputDto.getEntorno() != null)
            bien.setEntorno(entornoRepository.findById(Integer.valueOf(bienInputDto.getEntorno())).orElse(null));
          if (bienInputDto.getTipoVia() != null)
            bien.setTipoVia(tipoViaRepository.findById(Integer.valueOf(bienInputDto.getTipoVia())).orElse(null));
          if (bienInputDto.getMunicipio() != null)
            bien.setMunicipio(municipioRepository.findById(Integer.valueOf(bienInputDto.getMunicipio())).orElse(null));
          if (bienInputDto.getLocalidad() != null)
            bien.setLocalidad(localidadRepository.findById(Integer.valueOf(bienInputDto.getLocalidad())).orElse(null));
          if (bienInputDto.getProvincia() != null)
            bien.setProvincia(provinciaRepository.findById(Integer.valueOf(bienInputDto.getProvincia())).orElse(null));
          if (bienInputDto.getPais() != null)
            bien.setPais(paisRepository.findById(Integer.valueOf(bienInputDto.getPais())).orElse(null));
          if (bienInputDto.getTipoActivo() != null)
            bien.setTipoActivo(tipoActivoRepository.findById(Integer.valueOf(bienInputDto.getTipoActivo())).orElse(null));
          if (bienInputDto.getTipoBien() != null)
            bien.setTipoBien(tipoBienRepository.findById(Integer.valueOf(bienInputDto.getTipoBien())).orElse(null));
          if (bienInputDto.getSubTipoBien() != null)
            bien.setSubTipoBien(subTipoBienRepository.findById(Integer.valueOf(bienInputDto.getSubTipoBien())).orElse(null));
          if (bienInputDto.getUso() != null) bien.setUso(usoRepository.findById(Integer.valueOf(bienInputDto.getUso())).orElse(null));
        }else{
          if (bienInputDto.getEntorno() != null)
            bien.setEntorno(entornoRepository.findByCodigo(bienInputDto.getEntorno()).orElse(null));
          if (bienInputDto.getTipoVia() != null)
            bien.setTipoVia(tipoViaRepository.findByCodigo(bienInputDto.getTipoVia()).orElse(null));
          if (bienInputDto.getMunicipio() != null)
            bien.setMunicipio(municipioRepository.findByCodigo(bienInputDto.getMunicipio()).orElse(null));
          if (bienInputDto.getLocalidad() != null)
            bien.setLocalidad(localidadRepository.findByCodigo(bienInputDto.getLocalidad()).orElse(null));
          if (bienInputDto.getProvincia() != null)
            bien.setProvincia(provinciaRepository.findByCodigo(bienInputDto.getProvincia()).orElse(null));
          if (bienInputDto.getPais() != null)
            bien.setPais(paisRepository.findByCodigo(bienInputDto.getPais()).orElse(null));
          if (bienInputDto.getTipoActivo() != null)
            bien.setTipoActivo(tipoActivoRepository.findByCodigo(bienInputDto.getTipoActivo()).orElse(null));
          if (bienInputDto.getTipoBien() != null)
            bien.setTipoBien(tipoBienRepository.findByCodigo(bienInputDto.getTipoBien()).orElse(null));
          if (bienInputDto.getSubTipoBien() != null)
            bien.setSubTipoBien(subTipoBienRepository.findByCodigo(bienInputDto.getSubTipoBien()).orElse(null));
          if (bienInputDto.getUso() != null) bien.setUso(usoRepository.findByCodigo(bienInputDto.getUso()).orElse(null));
        }
        /*TODO descomentar para subida a pre*/
        Contrato contrato = contratoRepository.findById(idContrato).orElseThrow( () -> new NotFoundException("No se ha encontrato el contrato con ID: "+idContrato) );
      try {
        servicioMaestro.registrarBien(bien, contrato.getExpediente().getCartera().getCliente().getNombre());
      } catch (LockedChangeException e) {
        e.printStackTrace();
      }
      intervinienteUtil.direccionGoogle(bien);
        Bien bienSaved = bienRepository.saveAndFlush(bien);


        checkTasacionesValoracionesYCargas(bienSaved, bienInputDto,rem);
        return bienSaved;
    }

    public Bien updateBien(Bien bien, BienInputDto bienInputDto, ContratoBien cb) throws NotFoundException {

        BeanUtils.copyProperties(bienInputDto, bien, "tasaciones", "valoraciones", "cargas", "importeGarantizado", "responsabilidadHipotecaria");

        bien.setActivo(true);

        if (bienInputDto.getEntorno() != null)
            bien.setEntorno(entornoRepository.findById(Integer.valueOf(bienInputDto.getEntorno())).orElse(null));
        if (bienInputDto.getTipoVia() != null)
            bien.setTipoVia(tipoViaRepository.findById(Integer.valueOf(bienInputDto.getTipoVia())).orElse(null));
        if (bienInputDto.getMunicipio() != null)
            bien.setMunicipio(municipioRepository.findById(Integer.valueOf(bienInputDto.getMunicipio())).orElse(null));
        if (bienInputDto.getLocalidad() != null)
            bien.setLocalidad(localidadRepository.findById(Integer.valueOf(bienInputDto.getLocalidad())).orElse(null));
        if (bienInputDto.getProvincia() != null)
            bien.setProvincia(provinciaRepository.findById(Integer.valueOf(bienInputDto.getProvincia())).orElse(null));
        if (bienInputDto.getPais() != null)
            bien.setPais(paisRepository.findById(Integer.valueOf(bienInputDto.getPais())).orElse(null));
        if (bienInputDto.getTipoActivo() != null)
            bien.setTipoActivo(tipoActivoRepository.findById(Integer.valueOf(bienInputDto.getTipoActivo())).orElse(null));
        if (bienInputDto.getTipoBien() != null)
            bien.setTipoBien(tipoBienRepository.findById(Integer.valueOf(bienInputDto.getTipoBien())).orElse(null));
        if (bienInputDto.getSubTipoBien() != null)
            bien.setSubTipoBien(subTipoBienRepository.findById(Integer.valueOf(bienInputDto.getSubTipoBien())).orElse(null));
        if (bienInputDto.getUso() != null)
            bien.setUso(usoRepository.findById(Integer.valueOf(bienInputDto.getUso())).orElse(null));
        if (bienInputDto.getEstadoOcupacion() != null)
          bien.setEstadoOcupacion(estadoOcupacionRepository.findById(Integer.valueOf(bienInputDto.getEstadoOcupacion())).orElse(null));

        if(cb != null){
          if (bienInputDto.getTipoGarantia() != null)
            cb.setTipoGarantia(tipoGarantiaRepository.findById(Integer.valueOf(bienInputDto.getTipoGarantia())).orElse(null));
          if (bienInputDto.getLiquidez() != null)
            cb.setLiquidez(liquidezRepository.findById(Integer.valueOf(bienInputDto.getLiquidez())).orElse(null));
          if (bienInputDto.getResponsabilidadHipotecaria() != null)
            cb.setResponsabilidadHipotecaria(bienInputDto.getResponsabilidadHipotecaria());
          if (bienInputDto.getImporteGarantizado() != null)
            cb.setImporteGarantizado(bienInputDto.getImporteGarantizado());
          if (bienInputDto.getRango() != null)
            cb.setRango(bienInputDto.getRango());
          if (bienInputDto.getEstado() != null)
            cb.setEstado(bienInputDto.getEstado());
        } else {
          bien.setImporteGarantizado(bienInputDto.getImporteGarantizado());
          bien.setResponsabilidadHipotecaria(bienInputDto.getResponsabilidadHipotecaria());
        }

        //TODO qué son estado e importe garantizado?
        intervinienteUtil.direccionGoogle(bien);
        Bien bienSaved = bienRepository.saveAndFlush(bien);
        checkTasacionesValoracionesYCargas(bienSaved, bienInputDto,false);

        return bien;
    }

    public Bien checkTasacionesValoracionesYCargas(Bien bienSaved, BienInputDto bienInputDto, boolean rem) throws NotFoundException {
        if (bienInputDto.getTasaciones() != null) {
            Set<Tasacion> tasaciones = new HashSet<>();
            for (TasacionDto tasacionDTO : bienInputDto.getTasaciones()) {
                Tasacion tasacion = tasacionMapper.dtoToEntity(tasacionDTO,rem);
                if (tasacionDTO.getId() != null) {
                    tasacion.setId(tasacionDTO.getId());
                }
                tasacion.setBien(bienSaved);
                tasaciones.add(tasacionRepository.saveAndFlush(tasacion));
            }
            bienSaved.setTasaciones(tasaciones);
        }

        if (bienInputDto.getValoraciones() != null) {
            Set<Valoracion> valoraciones = new HashSet<>();
            for (ValoracionDto valoracionDTO : bienInputDto.getValoraciones()) {
                Valoracion valoracion = valoracionMapper.dtoToEntity(valoracionDTO,rem);
                if (valoracionDTO.getId() != null) {
                    valoracion.setId(valoracionDTO.getId());
                }
                valoracion.setBien(bienSaved);
                valoraciones.add(valoracionRepository.saveAndFlush(valoracion));
            }
            bienSaved.setValoraciones(valoraciones);
        }

        if (bienInputDto.getCargas() != null) {
            Set<Carga> cargas = new HashSet<>();
            for (CargaDto cargaDTO : bienInputDto.getCargas()) {
                if (cargaDTO.getId() != null) {
                    Carga carga = cargaRepository.findById(cargaDTO.getId()).orElseThrow( ()-> new NotFoundException("Carga", cargaDTO.getId()));
                    var cargaUpdated = cargaMapper.dtoToEntity(cargaDTO, carga,rem);
                    cargas.add(cargaRepository.saveAndFlush(cargaUpdated));
                } else {
                    Carga nuevaCarga = new Carga();
                    Carga carga = cargaMapper.dtoToEntity(cargaDTO, nuevaCarga,rem);
                    cargas.add(cargaRepository.saveAndFlush(carga));
                }
            }
            bienSaved.setCargas(cargas);
        }
        bienSaved.setImporteBien(bienSaved.getSaldoValoracion());
        return bienRepository.saveAndFlush(bienSaved);
    }
}
