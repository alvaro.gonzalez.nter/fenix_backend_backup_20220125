package com.haya.alaska.bien.infrastructure.repository;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato.domain.Contrato;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface BienRepository extends JpaRepository<Bien, Integer> {

  List<Bien> findByContratosId(Integer idContrato);

  List<Bien> findByContratosContratoExpedienteCarteraIdCargaAndContratosContratoExpedienteCarteraIdCargaSubcarteraIsNotNull(String carteraId);

  Page<Bien> findAllDistinctByContratosIdIn(Pageable pageable, Iterable<Integer> contratos);

  Page<Bien> findAllBy(Pageable pageable);

  List<Bien> findAllByIdIn(List<Integer> idsBien);
  List<Bien> findAllByIdCargaIn(List<String> idsBien);

  @Query(value =
    "SELECT bh.IND_ACTIVO as activo, " +
      "bh.DES_ANEJO_GARAJE as anejoGaraje, " +
      "bh.DES_ANEJO_OTROS as anejoOtros, " +
      "bh.DES_ANEJO_TRASTERO as anejoTrastero, " +
      "bh.NUM_ANIO_CONTRUCT as anioConstruccion, " +
      "bh.DES_BLOQUE as bloque, " +
      "bh.DES_COD_POSTAL as codigoPostal, " +
      "bh.DES_COMENT_DIRECC as comentarioDireccion, " +
      "bh.DES_DESCRIPCION as descripcion, " +
      "bh.DES_ESCALERA as escalera, " +
      "bh.FCH_INSCRIPCION as fechaInscripcion, " +
      "bh.FCH_REO_COVERSION as fechaReoConversion, " +
      "bh.DES_FINCA as finca, " +
      "bh.DES_FOLIO as folio, " +
      "bh.DES_ID_HAYA as idHaya, " +
      "bh.DES_ID_ORIGEN as idOrigen, " +
      "bh.DES_ID_ORIGEN_2 as irOrigen2, " +
      "bh.DES_IDUFIR as idufir, " +
      "bh.IMP_GARANTIZADO as importeGarantizado, " +
      "bh.NUM_LATITUD as latitud, " +
      "bh.DES_LIBRO as libro, " +
      "bh.DES_LOC_REGISTRO as localidadRegistro, " +
      "bh.ID_LONGITUD as longitud, " +
      "bh.DES_NOMBRE_VIA as nombreVia, " +
      "bh.DES_NOTAS_1 as notas1, " +
      "bh.DES_NOTAS_2 as notas2, " +
      "bh.DES_NUMERO as numero, " +
      "bh.DES_PISO as piso, " +
      "bh.DES_PUERTA as puerta, " +
      "bh.DES_PORTAL as portal, " +
      "bh.NUM_RANGO as rango, " +
      "bh.IND_POS_NEGOCIADA as posesionNegociada, " +
      "bh.DES_REF_CATASTRAL as referenciaCatastral, " +
      "bh.DES_REGISTRO as registro, " +
      "bh.DES_TOMO as tomo, " +
      "bh.DES_REO_ORIGEN as reoOrigen, " +
      "bh.NUM_RESP_HIPOTECARIA as responsabilidadHipotecaria, " +
      "bh.IND_SUBASTA as subasta, " +
      "bh.NUM_SUPERF_SUELO as superficieSuelo, " +
      "bh.NUM_SUPERF_UTIL as superficieUtil, " +
      "bh.NUM_SUPERF_CONTRUIDA as superficieConstruida, " +

      "bh.DES_ID_DATATAPE as idDatatape, " +
      "bh.ID_TIPO_BIEN as tipoBien, " +
      "bh.ID_SUB_TIPO_BIEN as subTipoBien, " +
      "bh.ID_TIPO_VIA as tipoVia, " +
      "bh.ID_ENTORNO as entorno, " +
      "bh.ID_MUNICIPIO as municipio, " +
      "bh.ID_LOCALIDAD as localidad, " +
      "bh.ID_PROVINCIA as provincia, " +
      "bh.ID_PAIS as pais, " +
      "bh.ID_TIPO_ACTIVO as tipoActivo, " +
      "bh.ID_USO as uso, " +
      "bh.ID_CARGA as idCarga, " +
      "bh.DES_CARTERA_REM as carteraREM, " +
      "bh.DES_SUBCARTERA_REM as subcarteraREM, " +
      "bh.FCH_CARGA as fechaCarga " +
      "FROM HIST_MSTR_BIEN bh JOIN REGISTRO_HISTORICO r ON bh.REV = r.id WHERE bh.ID = ?1 AND r.ID_USUARIO IS NULL ORDER BY timestamp DESC LIMIT 1",
    nativeQuery = true)
  Map historicoById(Integer id);

  @Query(
    value =
      "SELECT DISTINCT *"
        + "FROM MSTR_BIEN b "
        + "INNER join RELA_CONTRATO_BIEN cb on b.id = cb.ID_BIEN "
        + "INNER join MSTR_CONTRATO c on c.id = cb.ID_CONTRATO "
        + "INNER JOIN MSTR_EXPEDIENTE e ON e.id = c.ID_EXPEDIENTE "
        + "where e.id= :idExpediente ",
    nativeQuery = true)
  List<Bien> findAllByExpedienteId(@Param("idExpediente") Integer idExpediente);

  Optional<Bien> findByIdCarga(String idCarga);

  Optional<Bien> findByIdOrigen(String stringValue);

  Optional<Bien> findByIdOrigenEquals(String idOrigen);

  Optional<Bien> findByIdHayaEquals(String idHaya);

  Optional<Bien> findByIdCargaEquals(String idCarga);

  Optional<Bien> findByIdOrigen2Equals(String idOrigen2);

  Optional<Bien> findByIdOrigenEqualsOrIdOrigen2Equals(String idOrigen, String idOrigen2);

  Optional<Bien> findByIdHaya(String idOrigen);

  @Query(value = "SELECT * FROM MSTR_BIEN WHERE ID_LONGITUD =0 OR ID_LONGITUD IS NULL OR NUM_LATITUD = 0 OR NUM_LATITUD  IS NULL",
    nativeQuery = true)
  List<Bien> findAllByLongitudIsNullAndLatitudIsNull();

  Optional<Bien> findAllByIdHaya(String idHaya);

  List<Bien> findAllByFechaCargaAndIdHayaIsNull(Date date);
  List<Bien> findAllDistinctByContratosContratoIn(List<Contrato> contratos);


  List<Bien> findDistinctAllByContratosContratoExpedienteIdInOrBienPosesionNegociadaExpedientePosesionNegociadaIdIn(List<Integer> idsRA, List<Integer> idsPN);
}
