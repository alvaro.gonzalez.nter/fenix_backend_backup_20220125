package com.haya.alaska.bien.infrastructure.controller.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienGeolocalizacionInputDto {
  private Double latitud;
  private Double longitud;
}
