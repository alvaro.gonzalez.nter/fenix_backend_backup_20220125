package com.haya.alaska.bien.infrastructure.controller;

import com.haya.alaska.bien.application.BienUseCase;
import com.haya.alaska.bien.infrastructure.controller.dto.BienGeolocalizacionInputDto;
import com.haya.alaska.bien.infrastructure.controller.dto.BienGeolocalizacionOutputDto;
import com.haya.alaska.bien.infrastructure.controller.dto.BienOutputDto;
import com.haya.alaska.bien.infrastructure.controller.dto.BienVinculacionDto;
import com.haya.alaska.shared.dto.BienDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/bienes")
public class BienAllController {

  @Autowired BienUseCase bienUseCase;

  @ApiOperation(value = "Listado de todos los bienes",
          notes = "Devuelve todos los bienes registrados en la base de datos")
  @GetMapping
  public ListWithCountDTO<BienDTOToList> getAll(
          @RequestParam(value = "referencia", required = false) String referencia,
          @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page,
          @RequestParam(value = "contratos", required = false) List<Integer> contratos) {
    return bienUseCase.getAll(size, page, contratos, referencia);
  }

  @ApiOperation(
          value = "Obtener bien y contratos de los bienes",
          notes = "Devuelve el bien y los contratos relacionados")
  @GetMapping("/{id}/contratos")
  @Transactional
  public ListWithCountDTO<BienVinculacionDto> getBienContratosById(
          @PathVariable("id") Integer id) throws Exception {
    return bienUseCase.findVinculadosBienById(id);
  }

  @ApiOperation(value = "Actualizar localizacion", notes = "Actualiza la localizacion del bien con id enviado")
  @PutMapping("{idBien}/movil/localizacion")
  @Transactional(rollbackFor = Exception.class)
  public BienGeolocalizacionOutputDto updateBien(@PathVariable("idBien") Integer idBien,
                                                 @RequestBody @Valid BienGeolocalizacionInputDto bienInputDTO) throws Exception {
    return bienUseCase.updateGeolocalizacion(idBien, bienInputDTO);
  }

  @ApiOperation(value = "Obtener bien", notes = "Devuelve el bien con id enviado")
  @GetMapping("/{idBien}")
  public BienOutputDto getBienById( @PathVariable("idBien") Integer idBien) throws IllegalAccessException {
    return bienUseCase.findBienByIdDto(idBien, null);
  }

  @ApiOperation(value = "Obtener bien", notes = "Devuelve el bien con idOrigen o idOrigen2 enviado")
  @GetMapping("/byIdOrigen/{idBienOrigen}")
  public BienOutputDto getBienByIdOrigen( @PathVariable("idBienOrigen") String idBienOrigen) throws IllegalAccessException {
    return bienUseCase.findBienByIdCargaDto(idBienOrigen);
  }
}
