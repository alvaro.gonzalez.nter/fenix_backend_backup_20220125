package com.haya.alaska.bien.infrastructure.mapper;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.controller.dto.BienDto;
import com.haya.alaska.bien.infrastructure.controller.dto.BienGeolocalizacionOutputDto;
import com.haya.alaska.bien.infrastructure.controller.dto.BienInputDto;
import com.haya.alaska.bien.infrastructure.controller.dto.BienOutputDto;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien.infrastructure.util.BienUtil;
import com.haya.alaska.bien_subastado.domain.BienSubastado;
import com.haya.alaska.bien_subastado.infrastructure.controller.dto.BienSubastadoDTOToList;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.infrastructure.mapper.CargaMapper;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.catalogo.infrastructure.mapper.CatalogoMinInfoMapper;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.mapper.ContratoMapper;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.shared.dto.BienDTOToList;
import com.haya.alaska.shared.dto.BienExtendedDTOToList;
import com.haya.alaska.shared.dto.CargaDTOToList;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.infrastructure.mapper.TasacionMapper;
import com.haya.alaska.valoracion.infrastructure.mapper.ValoracionMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class BienMapper {

  @Autowired
  private CatalogoMinInfoMapper catalogoMinInfoMapper;
  @Autowired
  private TasacionMapper tasacionMapper;
  @Autowired
  private ValoracionMapper valoracionMapper;
  @Autowired
  private CargaMapper cargaMapper;
  @Autowired
  private ContratoMapper contratoMapper;
  @Autowired
  private BienRepository bienRepository;
  @Autowired
  private BienUtil bienUtil;
  @Autowired
  private ContratoRepository contratoRepository;
  @Autowired
  Map<String, CatalogoRepository<? extends Catalogo, Integer>> catalogoRepository;

  public List<BienDTOToList> listBienToListBienDTOToList(List<Bien> bienes, Integer idContrato) {
    List<BienDTOToList> bienDTO = new ArrayList<>();
    for (Bien bien : bienes) {
      bienDTO.add(bienToBienDTOToList(bien, idContrato));
    }
    return bienDTO;
  }

  public List<BienExtendedDTOToList> listBienToListBienExtendedDTOToList(List<Bien> bienes, Integer idContrato) {
    List<BienExtendedDTOToList> bienDTO = new ArrayList<>();
    for (Bien bien : bienes) {
      bienDTO.add(new BienExtendedDTOToList(bien,idContrato));
    }
    return bienDTO;
  }

  public BienDTOToList bienToBienDTOToList(Bien bien, Integer idContrato) {
    ContratoBien cb = null;
    if(bien.getContratos() != null && bien.getContratos().size() >0)
      cb = idContrato != null ? bien.getContratoBien(idContrato) : bien.getContratos().stream().findFirst().get();

    BienDTOToList bienDTOToList = new BienDTOToList(bien, idContrato);
    bienDTOToList.setIdOrigen(bien.getIdCarga());
    bienDTOToList.setId(bien.getId());
    bienDTOToList.setIdHaya(bien.getIdHaya() != null ? bien.getIdHaya() : null);
    bienDTOToList.setTipoActivo(bien.getTipoActivo() != null ? bien.getTipoActivo().getValor() : null);
    bienDTOToList.setEstado(bien.getActivo());
    if (bien.getFinca() != null) bienDTOToList.setFinca(bien.getFinca());
    bienDTOToList.setLocalidad(bien.getLocalidad() != null ? bien.getLocalidad().getValor() : null);
    bienDTOToList.setProvincia(bien.getProvincia() != null ? bien.getProvincia().getValor() : null);
    bienDTOToList.setImporteGarantizado(cb != null ? cb.getImporteGarantizado() : null);
    bienDTOToList.setDescripcion(bien.getDescripcion());
    bienDTOToList.setReferenciaCatastral(bien.getReferenciaCatastral());
    if (cb != null && cb.getResponsabilidadHipotecaria() != null)
      bienDTOToList.setResponsabilidadHipotecaria(cb.getResponsabilidadHipotecaria());
    bienDTOToList.setPosesionNegociada(bien.getPosesionNegociada());
    bienDTOToList.setLiquidez(cb != null && cb.getLiquidez() != null ? cb.getLiquidez().getValor() : null);

    List<String> contratos = new ArrayList<>();
    List<Contrato> listaContratos = new ArrayList<>();
    for (ContratoBien x : bien.getContratos()) {
      listaContratos.add(x.getContrato());
      if (x.getContrato() != null)
        contratos.add(x.getContrato().getIdCarga());
    }
    bienDTOToList.setContratos(String.join(",", contratos));
    List<ContratoDTOToList> listaContratosDtos = contratoMapper.listContratoToListContratoDTOToList(listaContratos,bien);

    bienDTOToList.setListaContratos(listaContratosDtos);
    return bienDTOToList;
  }

  public BienDTOToList bienToBienDTOToListAgenda(Bien bien) {
    BienDTOToList bienDTOToList = new BienDTOToList(bien);
    bienDTOToList.setId(bien.getId());
    bienDTOToList.setTipoActivo(bien.getTipoActivo() != null ? bien.getTipoActivo().getValor() : null);
    bienDTOToList.setEstado(bien.getActivo());
    bienDTOToList.setFinca(bien.getFinca());
    bienDTOToList.setLocalidad(bien.getLocalidad() != null ? bien.getLocalidad().getValor() : null);
    bienDTOToList.setProvincia(bien.getProvincia() != null ? bien.getProvincia().getValor() : null);
    bienDTOToList.setImporteGarantizado(bien.getImporteGarantizado());
    bienDTOToList.setDescripcion(bien.getDescripcion());
    bienDTOToList.setReferenciaCatastral(bien.getReferenciaCatastral());
    bienDTOToList.setResponsabilidadHipotecaria(null);
    bienDTOToList.setPosesionNegociada(bien.getPosesionNegociada());
    bienDTOToList.setLiquidez(null);
    bienDTOToList.setIdOrigen(bien.getIdCarga());

    List<String> contratos = new ArrayList<>();
    for (ContratoBien x : bien.getContratos()) {
      contratos.add(x.getContrato().getIdCarga());
    }
    bienDTOToList.setContratos(String.join(",", contratos));

    return bienDTOToList;
  }

  public BienOutputDto entityOutputDto(Bien bien, Integer idContrato) {
    BienOutputDto bienOutputDto = new BienOutputDto();
    ContratoBien cb = null;
    if (idContrato != null) {
      cb = bien.getContratoBien(idContrato);
    }

    BeanUtils.copyProperties(bien, bienOutputDto);
    bienOutputDto.setIdOrigen(bien.getIdOrigen());
    String tipo = bien.getTipoBien() != null ? bien.getTipoBien().getValor() + " " : "";
    String subtipo = bien.getSubTipoBien() != null ? bien.getSubTipoBien().getValor() : "";
    bienOutputDto.setDescripcion(tipo + subtipo);

    if (cb != null) {
      bienOutputDto.setEstado(cb.getEstado());
      if (cb.getTipoGarantia() != null)
        bienOutputDto.setTipoGarantia(catalogoMinInfoMapper.entityToDto(cb.getTipoGarantia()));
      if (bien.getTipoBien() != null)
        bienOutputDto.setTipoBien(catalogoMinInfoMapper.entityToDto(bien.getTipoBien()));
      if (bien.getSubTipoBien() != null)
        bienOutputDto.setSubTipoBien(catalogoMinInfoMapper.entityToDto(bien.getSubTipoBien()));
      if (cb.getLiquidez() != null)
        bienOutputDto.setLiquidez(catalogoMinInfoMapper.entityToDto(cb.getLiquidez()));
      bienOutputDto.setEsGarantia(cb.getResponsabilidadHipotecaria() != null);
      if (cb.getImporteGarantizado() != null)
        bienOutputDto.setImporteGarantizado(cb.getImporteGarantizado());
      if(cb.getResponsabilidadHipotecaria() != null)
        bienOutputDto.setResponsabilidadHipotecaria(cb.getResponsabilidadHipotecaria());
    }

    if (bien.getTipoVia() != null)
      bienOutputDto.setTipoVia(catalogoMinInfoMapper.entityToDto(bien.getTipoVia()));
    if (bien.getEntorno() != null)
      bienOutputDto.setEntorno(catalogoMinInfoMapper.entityToDto(bien.getEntorno()));
    if (bien.getMunicipio() != null)
      bienOutputDto.setMunicipio(catalogoMinInfoMapper.entityToDto(bien.getMunicipio()));
    if (bien.getLocalidad() != null)
      bienOutputDto.setLocalidad(catalogoMinInfoMapper.entityToDto(bien.getLocalidad()));
    if (bien.getProvincia() != null)
      bienOutputDto.setProvincia(catalogoMinInfoMapper.entityToDto(bien.getProvincia()));
    if (bien.getPais() != null)
      bienOutputDto.setPais(catalogoMinInfoMapper.entityToDto(bien.getPais()));
    if (bien.getTipoActivo() != null)
      bienOutputDto.setTipoActivo(catalogoMinInfoMapper.entityToDto(bien.getTipoActivo()));
    if (bien.getUso() != null)
      bienOutputDto.setUso(catalogoMinInfoMapper.entityToDto(bien.getUso()));
    if (bien.getEstadoOcupacion() != null)
      bienOutputDto.setEstadoOcupacion(catalogoMinInfoMapper.entityToDto(bien.getEstadoOcupacion()));

    if (bien.getTasaciones() != null) {
      bienOutputDto.setTasaciones(tasacionMapper.listTasacionToListTasacionDTOToList(bien.getTasaciones()));
      Tasacion devolver = null;
      for (Tasacion tasacion : bien.getTasaciones()) {
        if (devolver == null) devolver = tasacion;
        if (Double.compare(devolver.getImporte(), tasacion.getImporte()) < 0) devolver = tasacion;
      }
      bienOutputDto.setTasacionMayorImporte(tasacionMapper.entityToDto(devolver));
    }

    if (bien.getValoraciones() != null)
      bienOutputDto.setValoraciones(valoracionMapper.listValoracionToListValoracionDTOToList(bien.getValoraciones()));

    if (bien.getCargas() != null) {
      Set<Carga> a = bien.getCargas();
      List<CargaDTOToList> b = cargaMapper.listCargaToListCargaDTOToList(a, idContrato);
      bienOutputDto.setCargas(b);
      Carga devolver = null;
      for (Carga carga : bien.getCargas()) {
        if (devolver == null) devolver = carga;
        if (carga.getImporte() != null)
          if (Double.compare(devolver.getImporte(), carga.getImporte()) < 0) devolver = carga;
      }
      bienOutputDto.setCargaMayorImporte(cargaMapper.entityToDto(devolver));
    }

    List<BienSubastado> subastas = new ArrayList<>(bien.getBienesSubastados());
    if (!subastas.isEmpty())
      bienOutputDto.setSaneamiento(subastas.stream().map(BienSubastadoDTOToList::new).collect(Collectors.toList()));
    else bienOutputDto.setSaneamiento(new ArrayList<>());

    return bienOutputDto;
  }

  public BienGeolocalizacionOutputDto entityExtendedOutputDto(Bien bien, Integer idContrato) {
    BienGeolocalizacionOutputDto out = new BienGeolocalizacionOutputDto();
    var temp = entityOutputDto(bien, idContrato);
    BeanUtils.copyProperties(temp, out);
    out.setLatitud(bien.getLatitud());
    out.setLongitud(bien.getLongitud());
    return out;
  }

  public Bien inputDtoToEntity(Integer id, Integer idContrato, BienInputDto bienInputDto) throws NotFoundException {
    Bien bien = new Bien();
    if (id != null) {
      bien = bienRepository.findById(id).orElse(null);
      ContratoBien contratoBien = bien.getContratoBien(idContrato);
      bienUtil.updateBien(bien, bienInputDto, contratoBien);
    } else {
      Contrato contrato = contratoRepository.findById(idContrato).orElseThrow(() -> new NotFoundException("contrato", idContrato));
      if (!contrato.getActivo()){
        throw new NotFoundException("Contrato", idContrato, false);
      }
      bienUtil.createBien(bien, contrato.getExpediente().getCartera(), bienInputDto, idContrato,false);
    }
    return bien;
  }

  public BienOutputDto mapToOutputDto(Map bien) throws IllegalAccessException {
    BienOutputDto bienOutputDTO = new BienOutputDto();
    Field[] bienOutputDTODeclaredFields = BienOutputDto.class.getDeclaredFields();
    Field[] bienDTODeclaredFields = BienDto.class.getDeclaredFields();
    //BienOutputDto bienDTO = new BienOutputDto();
    for (Field declaredField : bienOutputDTODeclaredFields) { // non entity fields
      declaredField.setAccessible(true);
      String name = declaredField.getName();
      if (bien.get(name) == null)
        continue;
      if (bien.get(name).getClass().equals(BigDecimal.class)) {
        declaredField.set(bienOutputDTO, Double.parseDouble(bien.get(name).toString()));
      } else if (bien.get(name).getClass().equals(Integer.class)) {
        CatalogoRepository<? extends Catalogo, Integer> repo = catalogoRepository.get(name + "Repository");
        if (repo != null) {
          Catalogo catalogo = repo.findById((Integer) bien.get(name)).orElse(null);
          if (catalogo != null)
            declaredField.set(bienOutputDTO, new CatalogoMinInfoDTO(catalogo));
        } else {
          declaredField.set(bienOutputDTO, bien.get(name));
        }
      } else {
        declaredField.set(bienOutputDTO, bien.get(name));
      }
    }
    for (Field declaredField : bienDTODeclaredFields) { // non entity fields
      declaredField.setAccessible(true);
      String name = declaredField.getName();
      if (bien.get(name) == null)
        continue;
      if (bien.get(name).getClass().equals(BigDecimal.class)) {
        declaredField.set(bienOutputDTO, Double.parseDouble(bien.get(name).toString()));
      } else {
        declaredField.set(bienOutputDTO, bien.get(name));
      }
    }
    if (bienOutputDTO.getHistorico() != null)
      bienOutputDTO.getHistorico().setHistorico(null);
    return bienOutputDTO;
  }
}
