package com.haya.alaska.bien.infrastructure.controller.dto;

import com.haya.alaska.bien_subastado.infrastructure.controller.dto.BienSubastadoDTOToList;
import com.haya.alaska.carga.infrastructure.controller.dto.CargaDto;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.shared.dto.CargaDTOToList;
import com.haya.alaska.shared.dto.TasacionDTOToList;
import com.haya.alaska.shared.dto.ValoracionDTOToList;
import com.haya.alaska.tasacion.infrastructure.controller.dto.TasacionDto;
import lombok.*;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BienGeolocalizacionOutputDto extends BienOutputDto{
  private Double latitud;
  private Double longitud;
}
