package com.haya.alaska.formalizacion.aplication;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.CheckListOutputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.FormalizacionInputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.FormalizacionOutputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.InformeInputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.*;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.InformacionCheckListDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input.FormalizacionCompletaInputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output.FormalizacionCompletaOutputDTO;
import com.haya.alaska.informes.infrastructure.controller.dto.BombinDTO;
import com.haya.alaska.informes.infrastructure.controller.dto.FormalizacionDTO;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.controller.dto.SubtipoPropuestaDTO;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface FormalizacionUseCase {
  FormalizacionOutputDTO findByPropuestaId(Integer propuestaId, Boolean posesionNegociada) throws NotFoundException;

  FormalizacionOutputDTO updateFormalicacionByInput(FormalizacionInputDTO input, Integer propuestaId, Boolean posesionNegociada) throws NotFoundException, InvalidCodeException;

  void updatePbcFormalizacion(Integer idPropuesta, String estado) throws NotFoundException, InvalidCodeException;

  FormalizacionOutputDTO createFormalizacion(Propuesta propuesta, Boolean posesionNegociada) throws NotFoundException;


  ResponseEntity<InputStreamResource> autorizaciónCambioBombínWord(BombinDTO bombinDTO) throws Exception;

  ResponseEntity<byte[]> autorizaciónCambioBombínPdf(BombinDTO bombinDTO) throws Exception;

  ResponseEntity<InputStreamResource> comunicacionFormalizacionWord(FormalizacionDTO formalizacionDTO, boolean posesionNegociada) throws Exception;

  ResponseEntity<InputStreamResource> comunicacionFormalizacionPDF(FormalizacionDTO formalizacionDTO, boolean posesionNegociada) throws Exception;

  void cargaFormalizacion(
    Integer idCliente, Integer idCartera, Usuario logged, MultipartFile file) throws RequiredValueException, NotFoundException, IOException, InvalidCodeException, ForbiddenException;

  void cargaManual(Integer idCliente, Integer idCartera, Usuario logged, FormalizacionCompletaInputDTO input, List<MultipartFile> documentos) throws Exception;

  FormalizacionCompletaOutputDTO getFormalizacionCompleta(Integer idProp) throws NotFoundException;

  LinkedHashMap<String, List<Collection<?>>> findExcelFormalizacionById(Integer id) throws Exception;

  LinkedHashMap<String, List<Collection<?>>> findExcelBusquedaById(
    List<Integer> ids,
    Boolean todos,
    BusquedaDto busquedaDto,
    Usuario loggedUser,
    String id,
    String titular,
    String nif,
    String estado,
    String detalleEstado,
    String fechaFirma,
    String horaFirma,
    String formalizacionHaya,
    String gestorHaya,
    String formalizacionCliente,
    String fechaSancion,
    String importeDacion,
    String importeDeuda,
    String provincia,
    String alquiler);

  ResponseEntity<byte[]> generarDocumento(InformeInputDTO inport, MultipartFile[] files, Usuario logged) throws IOException, NotFoundException, RequiredValueException, InterruptedException;

  List<CheckListOutputDTO> getCheckListOutput(Integer idPropuesta) throws NotFoundException, IOException;

  List<CheckListOutputDTO> getCheckList(Integer idPropuesta, List<CheckListInputDTO> checkListInputDTOS, List<MultipartFile> documentos, Usuario usuario) throws Exception;

  void okCheckList(Integer idPropuesta) throws NotFoundException;

  InformacionCheckListDTO getInformacionCheckList(Integer idPropuesta) throws NotFoundException;
  void updateInformacionCheckList(InformacionCheckListDTO dto) throws NotFoundException;

  LinkedHashMap<String, List<Collection<?>>> findExcelCheckList(Integer id) throws Exception;

  void alertaAlGestor(Usuario logged, Integer idPropuesta) throws NotFoundException, RequiredValueException;
  ByteArrayInputStream createExcelCheckList(Integer id) throws Exception;

  List<CatalogoMinInfoDTO> getTiposFormalizacion();
  List<SubtipoPropuestaDTO> getSubtiposFormalizacion(Integer tipoPropuesta);

  String getNombrePrimerInterviniente(Integer idPropuesta) throws NotFoundException;
}
