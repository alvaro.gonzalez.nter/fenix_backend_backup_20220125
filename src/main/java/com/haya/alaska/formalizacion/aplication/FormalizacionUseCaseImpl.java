package com.haya.alaska.formalizacion.aplication;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.busqueda.application.BusquedaUseCase;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.datos_check_list.domain.DatosCheckList;
import com.haya.alaska.datos_check_list.infrastructure.repository.DatosCheckListRepository;
import com.haya.alaska.documentacion_validada.domain.DocumentacionValidada;
import com.haya.alaska.documentacion_validada.infrastructure.repository.DocumentacionValidadaRepository;
import com.haya.alaska.estado_checklist.domain.EstadoChecklist;
import com.haya.alaska.estado_checklist.infrastructure.repository.EstadoChecklistRepository;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.*;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.InformacionCheckListDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input.FormalizacionCompletaInputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output.FormalizacionBusquedaOutputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output.FormalizacionCompletaOutputDTO;
import com.haya.alaska.formalizacion.infrastructure.mapper.FormalizacionMapper;
import com.haya.alaska.formalizacion.infrastructure.repository.FormalizacionRepository;
import com.haya.alaska.formalizacion.infrastructure.util.FormalizacionExport;
import com.haya.alaska.formalizacion.infrastructure.util.FormalizacionImport;
import com.haya.alaska.informacion_check_list.domain.InformacionCheckList;
import com.haya.alaska.informacion_check_list.infrastructure.repository.InformacionCheckListRepository;
import com.haya.alaska.informes.infrastructure.controller.dto.BombinDTO;
import com.haya.alaska.informes.infrastructure.controller.dto.CertificadoDTO;
import com.haya.alaska.informes.infrastructure.controller.dto.FormalizacionDTO;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.domain.enums.TipoEntidadEnum;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.integraciones.gd.model.Documento;
import com.haya.alaska.integraciones.gd.model.MatriculasEnum;
import com.haya.alaska.integraciones.smtp.application.SMTPUseCase;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.nombres_check_list.domain.NombresCheckList;
import com.haya.alaska.nombres_check_list.infrastructure.repository.NombresCheckListRepository;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.infrastructure.repository.ProcedimientoRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.controller.dto.SubtipoPropuestaDTO;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoContenedorInputDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import com.haya.alaska.subtipo_propuesta.infrastructure.repository.SubtipoPropuestaRepository;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.infrastructure.repository.TasacionRepository;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.tipo_propuesta.infrastructure.repository.TipoPropuestaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.haya.alaska.utilidades.comunicaciones.application.ComunicacionesUseCase;
import com.haya.alaska.utilidades.comunicaciones.application.domain.PlantillaFormalizacion;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SendEmailDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.repository.PlantillaFormalizacionRepository;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Time;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class FormalizacionUseCaseImpl implements FormalizacionUseCase {
  @Autowired
  FormalizacionRepository formalizacionRepository;

  @Autowired
  ClienteRepository clienteRepository;
  @Autowired
  CarteraRepository carteraRepository;
  @Autowired
  UsuarioRepository usuarioRepository;
  @Autowired
  EventoUseCase eventoUseCase;
  @Autowired
  TipoPropuestaRepository tipoPropuestaRepository;
  @Autowired
  SubtipoPropuestaRepository subtipoPropuestaRepository;

  @Autowired
  private FormalizacionExport formalizacionExport;
  @Autowired private EstadoChecklistRepository estadoChecklistRepository;

  @Autowired
  private ExpedienteRepository expedienteRepository;

  @Autowired
  private PropuestaRepository propuestaRepository;
  @Autowired
  private BienRepository bienRepository;
  @Autowired
  private BienPosesionNegociadaRepository bienPosesionNegociadaRepository;
  @Autowired
  private TasacionRepository tasacionRepository;
  @Autowired
  private IntervinienteRepository intervinienteRepository;

  @Autowired
  private ProcedimientoRepository procedimientoRepository;
  @Autowired
  private ExpedienteUtil expedienteUtil;
  @Autowired
  private FormalizacionImport formalizacionImport;
  @Autowired
  private FormalizacionMapper formalizacionMapper;
  @Autowired
  private BusquedaUseCase busquedaUseCase;

  @Autowired
  private SMTPUseCase smtpUseCase;
  @Autowired
  private PlantillaFormalizacionRepository plantillaFormalizacionRepository;
  @Autowired
  private ComunicacionesUseCase comunicacionesUseCase;
  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;
  @Autowired
  private NombresCheckListRepository nombresCheckListRepository;
  @Autowired
  private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired
  private InformacionCheckListRepository informacionCheckListRepository;

  @Autowired private DatosCheckListRepository datosCheckListRepository;
  @Autowired private DocumentacionValidadaRepository documentacionValidadaRepository;

  public FormalizacionOutputDTO findByPropuestaId(Integer propuestaId, Boolean posesionNegociada)
      throws NotFoundException {
    Formalizacion formalizacion = this.formalizacionRepository.findByPropuestaId(propuestaId);
    if (formalizacion == null) throw new NotFoundException("formalizacion", "propuesta", propuestaId);
    FormalizacionOutputDTO output = new FormalizacionOutputDTO(formalizacion, posesionNegociada);
    return output;
  }

  public FormalizacionOutputDTO updateFormalicacionByInput(FormalizacionInputDTO input, Integer propuestaId, Boolean posesionNegociada) throws NotFoundException, InvalidCodeException {
    Formalizacion formalizacion = this.formalizacionRepository.findByPropuestaId(propuestaId);
    if (formalizacion == null) throw new NotFoundException("formalizacion", "propuesta", propuestaId);
    /*Propuesta propuesta = formalizacion.getPropuesta();
    if (!propuesta.getSubtipoPropuesta().getFormalizacion()) throw new InvalidCodeException(
      "No se puede formalizar la propuesta: " + propuestaId);*/
    Formalizacion fSaved = addInputoToFormalizacion(input, formalizacion);
    FormalizacionOutputDTO output = new FormalizacionOutputDTO(fSaved, posesionNegociada);
    return output;
  }

  public void updatePbcFormalizacion(Integer idPropuesta, String estado) throws NotFoundException, InvalidCodeException {
    if (!estado.equals("aprobado") && !estado.equals("denegado")) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en")) throw new NotFoundException("Invalid status");
      else throw new NotFoundException("Estado no válido");
    }
    Formalizacion formalizacion = this.formalizacionRepository.findByPropuestaId(idPropuesta);
    if (formalizacion == null) throw new NotFoundException("formalizacion", "propuesta", idPropuesta);
    Propuesta propuesta = formalizacion.getPropuesta();
    if (!propuesta.getSubtipoPropuesta().getFormalizacion()) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCodeException("The proposal cannot be formalized: " + idPropuesta);
      else throw new InvalidCodeException("No se puede formalizar la propuesta: " + idPropuesta);
    }
    formalizacion.setPbc(estado.equals("aprobado"));
    this.formalizacionRepository.save(formalizacion);
  }

  public FormalizacionOutputDTO createFormalizacion(Propuesta propuesta, Boolean posesionNegociada) {
    Integer propuestaId = propuesta.getId();
    Formalizacion formalizacion = formalizacionRepository.findByPropuestaId(propuestaId);
    if (formalizacion == null) formalizacion = new Formalizacion();
    formalizacion.setPropuesta(propuesta);
    Formalizacion fSaved = formalizacionRepository.save(formalizacion);
    FormalizacionOutputDTO output = new FormalizacionOutputDTO(fSaved, posesionNegociada);
    return output;
  }

  private Formalizacion addInputoToFormalizacion(FormalizacionInputDTO input, Formalizacion formalizacion) {
    BeanUtils.copyProperties(input, formalizacion);
    if (input.getHoraFirma() != null) {
      String[] tiempo = input.getHoraFirma().split(":");
      if (tiempo.length != 2) {
        throw new IllegalArgumentException("El formato de la Hora Firma debe ser: 'hh:mm'");
      }
      Time hora = Time.valueOf(input.getHoraFirma());
      formalizacion.setHoraFirma(hora);
    }
    return formalizacionRepository.save(formalizacion);
  }

  private static String[] getNullPropertyNames(Object source) {
    final BeanWrapper src = new BeanWrapperImpl(source);
    java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();
    Set<String> emptyNames = new HashSet<String>();
    for (java.beans.PropertyDescriptor pd : pds) {
      Object srcValue = src.getPropertyValue(pd.getName());
      if (srcValue == null) emptyNames.add(pd.getName());
    }
    String[] result = new String[emptyNames.size()];
    return emptyNames.toArray(result);
  }

  @Override
  public ResponseEntity<InputStreamResource> autorizaciónCambioBombínWord(BombinDTO bombinDTO) throws Exception {
    return formalizacionExport.wordExportBombin(getMapBombin(bombinDTO));
  }

  @Override
  public ResponseEntity<byte[]> autorizaciónCambioBombínPdf(BombinDTO bombinDTO) throws Exception {
    return formalizacionExport.pdfExportBombin(getMapBombin(bombinDTO));
  }

  private Map<String, String> getMapBombin(BombinDTO bombinDTO) throws Exception {
    Bien bien = bienRepository.findById(bombinDTO.getIdBien()).orElseThrow(() -> new Exception("Bien no encontrado con el id:" + bombinDTO.getIdBien()));
    Map<String, String> mapaBombin = new HashMap<>();
    mapaBombin.put("interviniente", bombinDTO.getNombreInterviniente() != null ? bombinDTO.getNombreInterviniente().toUpperCase() : null);
    mapaBombin.put("interDNI", bombinDTO.getDniInterviniente() != null ? bombinDTO.getDniInterviniente().toUpperCase() : null);
    mapaBombin.put("empresa", bombinDTO.getEmpresa() != null ? bombinDTO.getEmpresa().toUpperCase() : null);
    mapaBombin.put("llaves", bombinDTO.getLlaves() != null ? bombinDTO.getLlaves().toString() : null);
    mapaBombin.put("fechaEntrega", bombinDTO.getFechaEntrega());
    mapaBombin.put("cif", bombinDTO.getCif());

    mapaBombin.put("inmuebleTipo", bien.getTipoBien() != null ? bien.getTipoBien().getValor().toUpperCase() : null);
    String direccion = "";
    if (bien.getNombreVia() != null) direccion += bien.getNombreVia();
    if (bien.getNumero() != null) direccion += " " + bien.getNumero();
    if (bien.getPortal() != null) direccion += " " + bien.getPortal();
    if (bien.getBloque() != null) direccion += " " + bien.getBloque();
    if (bien.getEscalera() != null) direccion += " " + bien.getEscalera();
    if (bien.getPiso() != null) direccion += " " + bien.getPiso();
    if (bien.getPuerta() != null) direccion += " " + bien.getPuerta();
    mapaBombin.put("inmuebleCalle", direccion != null ? direccion.toUpperCase() : null);
    mapaBombin.put("inmuebleLocalidad", bien.getMunicipio() != null ? bien.getMunicipio().getValor().toUpperCase() : null);
    mapaBombin.put("inmuebleProvincia", bien.getProvincia() != null ? bien.getProvincia().getValor().toUpperCase() : null);
    mapaBombin.put("finca", bien.getFinca() != null ? bien.getFinca().toUpperCase() : null);
    mapaBombin.put("inmuRegPro", bien.getLocalidadRegistro() != null ? bien.getLocalidadRegistro().toUpperCase() : null);
    mapaBombin.put("inmuRegN", bien.getReferenciaCatastral() != null ? bien.getReferenciaCatastral().toUpperCase() : null);
    mapaBombin.put("idBien", String.valueOf(bien.getId()));
    return mapaBombin;
  }

  @Override
  public ResponseEntity<InputStreamResource> comunicacionFormalizacionWord(FormalizacionDTO formalizacionDTO, boolean posesionNegociada) throws Exception {
    return formalizacionExport.wordExportFormalizacion(getMapFormalizacion(formalizacionDTO, posesionNegociada));
  }

  @Override
  public ResponseEntity<InputStreamResource> comunicacionFormalizacionPDF(FormalizacionDTO formalizacionDTO, boolean posesionNegociada) throws Exception {
    return formalizacionExport.pdfExportFormalizacion(getMapFormalizacion(formalizacionDTO, posesionNegociada));
  }

  private Map<String, String> getMapFormalizacion(FormalizacionDTO formalizacionDTO, Boolean posesionNegociada) throws Exception {
    Bien bien;
    Procedimiento procedimiento;
    Map<String, String> mapaFormalizacion = new HashMap<>();
    Usuario gestorForm = null;
    Propuesta propuesta = propuestaRepository.findById(formalizacionDTO.getIdPropuesta()).orElseThrow(() -> new Exception("Propuesta no encontrada con el id:" + formalizacionDTO.getIdPropuesta()));
    if (posesionNegociada) {
      BienPosesionNegociada bienPosesionNegociada = bienPosesionNegociadaRepository.findById(formalizacionDTO.getIdBien()).orElseThrow(() -> new Exception("Bien posesion negociada no encontrado con el id:" + formalizacionDTO.getIdBien()));
      bien = bienPosesionNegociada.getBien();
      mapaFormalizacion.put("idBien", String.valueOf(bienPosesionNegociada.getId()));
      if (propuesta.getExpedientePosesionNegociada() != null)
        mapaFormalizacion.put("gestorHayaRecuperacion", propuesta.getExpedientePosesionNegociada().getGestor() != null ? propuesta.getExpedientePosesionNegociada().getGestor().getNombre() : "");
      procedimiento = expedienteUtil.getUltimoProcedimiento(propuesta.getExpedientePosesionNegociada());
      mapaFormalizacion.put("operacion", bien.getIdOrigen());
      ExpedientePosesionNegociada epn = propuesta.getExpedientePosesionNegociada();
      gestorForm = epn.getUsuarioByPerfil("Gestor formalización");
    } else {
      bien = bienRepository.findById(formalizacionDTO.getIdBien()).orElseThrow(() -> new Exception("Bien no encontrado con el id:" + formalizacionDTO.getIdBien()));
      procedimiento = expedienteUtil.getUltimoProcedimiento(propuesta.getExpediente());
      Map<String, String> mapDatosContrato = getContrato(propuesta);
      mapaFormalizacion.put("operacion", mapDatosContrato.get("operacion"));
      mapaFormalizacion.put("numPrestamo", mapDatosContrato.get("numPrestamo"));
      mapaFormalizacion.put("nombreTitular1", mapDatosContrato.get("titular1"));
      mapaFormalizacion.put("nifTitular1", mapDatosContrato.get("dni1"));
      mapaFormalizacion.put("nombreTitular2", mapDatosContrato.get("titular2"));
      mapaFormalizacion.put("nifTitular2", mapDatosContrato.get("dni2"));
      mapaFormalizacion.put("importeDeuda", mapDatosContrato.get("importeDeuda"));
      mapaFormalizacion.put("quita", mapDatosContrato.get("quita"));
      mapaFormalizacion.put("precioCompra", mapDatosContrato.get("precioCompra"));
      mapaFormalizacion.put("remanente", mapDatosContrato.get("remanente"));
      if (propuesta.getExpediente() != null)
        mapaFormalizacion.put("gestorHayaRecuperacion", propuesta.getExpediente().getGestor() != null ? propuesta.getExpediente().getGestor().getNombre() : "");
      mapaFormalizacion.put("idBien", String.valueOf(bien.getId()));
      Expediente epn = propuesta.getExpediente();
      gestorForm = epn.getUsuarioByPerfil("Gestor formalización");
    }

    Formalizacion formalizacion = getFormalizacion(propuesta);
    Tasacion tasacion = getTasacion(bien);
    Carga carga = getCarga(bien);
    mapaFormalizacion.put("cliente", formalizacionDTO.getCliente());
    mapaFormalizacion.put("contacto", formalizacionDTO.getContacto());
    mapaFormalizacion.put("comprador", formalizacionDTO.getComprador());

    if (formalizacion != null) {
      mapaFormalizacion.put("alquiler", formalizacion.getAlquiler() != null && formalizacion.getAlquiler() ? "SI" : "NO");
      mapaFormalizacion.put("gestorClienteFormalizacion", formalizacion.getGestorFormalizacionCliente());
      mapaFormalizacion.put("gestorClienteRecuperacion", formalizacion.getGestorRecuperacionCliente());
      mapaFormalizacion.put("gestorHayaFormalizacion", gestorForm != null ? gestorForm.getNombre() : null);

      mapaFormalizacion.put("fechaFormalizacion", formalizacion.getFechaFirma() != null ? String.valueOf(formalizacion.getFechaFirma()) : "");
      mapaFormalizacion.put("numProtocolo", formalizacion.getNumeroProtocolo() != null ? String.valueOf(formalizacion.getNumeroProtocolo()) : "");
      mapaFormalizacion.put("nombreNotaria", formalizacion.getNotario() != null ? formalizacion.getNotario() : "");
      mapaFormalizacion.put("contactoNotaria", formalizacion.getTelefonoNotaria() != null ? formalizacion.getTelefonoNotaria() : "");
      mapaFormalizacion.put("direccionNotaria", formalizacion.getDireccionNotaria() != null ? formalizacion.getDireccionNotaria() : "");
    }
    mapaFormalizacion.put("oficina", formalizacionDTO.getOficina());
    mapaFormalizacion.put("gestorIncumplimiento", formalizacionDTO.getGestorInclumplimiento());
    mapaFormalizacion.put("tipologia", bien.getTipoBien() != null ? bien.getTipoBien().getValor() : "");

    String direccion = "";
    if (bien.getNombreVia() != null) direccion += bien.getNombreVia();
    if (bien.getNumero() != null) direccion += ": " + bien.getNumero();
    if (bien.getPortal() != null) direccion += " " + bien.getPortal();
    if (bien.getBloque() != null) direccion += " " + bien.getBloque();
    if (bien.getEscalera() != null) direccion += " " + bien.getEscalera();
    if (bien.getPiso() != null) direccion += " " + bien.getPiso();
    if (bien.getPuerta() != null) direccion += " " + bien.getPuerta();
    mapaFormalizacion.put("direccion", direccion);

    mapaFormalizacion.put("municipio", bien.getMunicipio() != null ? bien.getMunicipio().getValor() : "");
    mapaFormalizacion.put("provincia", bien.getProvincia() != null ? bien.getProvincia().getValor() : "");
    mapaFormalizacion.put("refCatastral", bien.getReferenciaCatastral());
    mapaFormalizacion.put("fincaRegistral", bien.getFinca());
    mapaFormalizacion.put("registro", bien.getLocalidadRegistro());

    mapaFormalizacion.put("tasacion", tasacion != null && tasacion.getImporte() != null ? String.valueOf(tasacion.getImporte()) : "");

    if (procedimiento != null) {
      mapaFormalizacion.put("numAuto", procedimiento.getNumeroAutos());
      mapaFormalizacion.put("juzgado", procedimiento.getJuzgado());
      mapaFormalizacion.put("plaza", procedimiento.getPlaza());
    }
    mapaFormalizacion.put("gestoria", formalizacionDTO.getGestoria() != null ? formalizacionDTO.getGestoria() : "");
    mapaFormalizacion.put("ratificacion", formalizacionDTO.getNecesitaRatificacion() != null ? formalizacionDTO.getNecesitaRatificacion().equalsIgnoreCase("true") ? "SI" : "NO" : null);
    mapaFormalizacion.put("cargas", carga != null ? carga.getNombreAcreedor() : "");

    String deudas = "";
    String situaciones = "";
    String certificados = "";
    for (int i = 0; i < formalizacionDTO.getCertificados().size(); i++) {
      CertificadoDTO certificadoDTO = formalizacionDTO.getCertificados().get(i);
      deudas += certificadoDTO.getDeudas() != null ? certificadoDTO.getDeudas() : "";
      deudas += "\r";
      situaciones += certificadoDTO.getSituacion() != null ? certificadoDTO.getSituacion() : "";
      situaciones += "\r";
      certificados += certificadoDTO.getNumReferencia() != null ? "SI" : "NO";
      certificados += "\r";
    }
    // borrar fila de mas
    if (deudas.length() > 2) deudas.substring(0, deudas.length() - 2);
    if (situaciones.length() > 2) situaciones.substring(0, situaciones.length() - 2);
    if (certificados.length() > 2) certificados.substring(0, certificados.length() - 2);
    mapaFormalizacion.put("deudas", deudas);
    mapaFormalizacion.put("situaciones", situaciones);
    mapaFormalizacion.put("certificados", certificados);
    //todo
    mapaFormalizacion.put("fechaVisita", formalizacionDTO.getFechaVisita());
    mapaFormalizacion.put("llaves", formalizacionDTO.getLlaves());
    mapaFormalizacion.put("observaciones", formalizacionDTO.getObservaciones());

    return mapaFormalizacion;
  }

  private Tasacion getTasacion(Bien bien) {
    Tasacion devolver = null;
    for (Tasacion tasacion : bien.getTasaciones()) {
      if (devolver == null) devolver = tasacion;
      if (Double.compare(devolver.getImporte(), tasacion.getImporte()) < 0) devolver = tasacion;
    }
    return devolver;
  }

  private Carga getCarga(Bien bien) {
    Carga devolver = null;
    for (Carga carga : bien.getCargas()) {
      if (devolver == null) devolver = carga;
      if (Double.compare(devolver.getImporte(), carga.getImporte()) < 0) devolver = carga;
    }
    return devolver;
  }

  private Formalizacion getFormalizacion(Propuesta propuesta) {
    return propuesta.getFormalizaciones().stream().findFirst().orElse(null);
  }

  private Map<String, String> getContrato(Propuesta propuesta) {
    HashMap<String, String> mapContrato = new HashMap<>();
    Set<Contrato> contratos = propuesta.getContratos();
    String des_id_origen = "";
    String numPrestamo = "";
    String titular1 = "";
    String titular2 = "";
    String dni1 = "";
    String dni2 = "";
    Double importeDeuda = 0.0;
    Double quita = 0.0;
    Double remanente = 0.0;
    Double precioCompra = 0.0;
    for (Contrato contrato : contratos) {
      if (contrato.getIdOrigen() != null && !des_id_origen.contains(contrato.getIdOrigen()))
        des_id_origen += contrato.getIdOrigen() + " ";
      if (contrato.getIdCarga() != null && !numPrestamo.contains(contrato.getIdCarga()))
        numPrestamo += contrato.getIdCarga() + " ";
      for (ContratoInterviniente contratoInterviniente : contrato.getIntervinientes()) {
        if (contratoInterviniente.getTipoIntervencion() != null && contratoInterviniente.getTipoIntervencion().getValor().equals("TITULAR")) {
          if (contratoInterviniente.getOrdenIntervencion() == 1) {
            if (contratoInterviniente.getInterviniente() != null && !dni1.contains(contratoInterviniente.getInterviniente().getNumeroDocumento())) {
              dni1 += contratoInterviniente.getInterviniente().getNumeroDocumento() + " ";
              titular1 += contratoInterviniente.getInterviniente().getNombre() + " ";
            }
          } else if (contratoInterviniente.getOrdenIntervencion() == 2) {
            if (contratoInterviniente.getInterviniente() != null && !dni2.contains(contratoInterviniente.getInterviniente().getNumeroDocumento())) {
              dni2 += contratoInterviniente.getInterviniente().getNumeroDocumento() + " ";
              titular2 += contratoInterviniente.getInterviniente().getNombre() + " ";
            }
          }
        }
      }
      if (contrato.getSaldoContrato() != null) {
        importeDeuda += contrato.getSaldoContrato().getCapitalPendiente();
      }
    }
    //TODO Cuando se implemente quita y importeDacion habra que cambiar esto
    //remanente = importeDeuda - importeDacion - quita;
    precioCompra = importeDeuda - quita;
    mapContrato.put("numPrestamo", numPrestamo);
    mapContrato.put("operacion", des_id_origen);
    mapContrato.put("titular1", titular1);
    mapContrato.put("titular2", titular2);
    mapContrato.put("dn1", dni1);
    mapContrato.put("dni2", dni2);
    mapContrato.put("importeDeuda", String.valueOf(importeDeuda));
    mapContrato.put("quita", String.valueOf(quita));
    mapContrato.put("remanente", String.valueOf(remanente));
    mapContrato.put("precioCompra", String.valueOf(precioCompra));
    return mapContrato;
  }

  public void cargaFormalizacion(Integer idCliente, Integer idCartera, Usuario logged, MultipartFile file) throws RequiredValueException, NotFoundException, IOException, InvalidCodeException, ForbiddenException {
    formalizacionImport.parseCarga(idCliente, idCartera, logged, file);
  }

  public void cargaManual(Integer idCliente, Integer idCartera, Usuario logged, FormalizacionCompletaInputDTO input, List<MultipartFile> documentos) throws Exception {
    Cliente cli = null;
    if (idCliente != null)
      cli = clienteRepository.findById(idCliente).orElseThrow(() -> new NotFoundException("Cliente", idCliente));
    Cartera car = null;
    if (idCartera != null)
      car = carteraRepository.findById(idCartera).orElseThrow(() -> new NotFoundException("Cartera", idCartera));
    Usuario user = usuarioRepository.findById(logged.getId()).orElseThrow(() -> new NotFoundException("Usuario", logged.getId()));

    Propuesta p = formalizacionMapper.parseImputToEntity(cli, car, input, user);
    if (input.getCheckList() != null && !input.getCheckList().isEmpty())
      getCheckList(p.getId(), input.getCheckList(), documentos, logged);
    if (input.getInformacionCheckList() != null){
      input.getInformacionCheckList().setIdPropuesta(p.getId());
      updateInformacionCheckList(input.getInformacionCheckList());
    }
  }

  public FormalizacionCompletaOutputDTO getFormalizacionCompleta(Integer idProp) throws NotFoundException {
    Propuesta p = propuestaRepository.findById(idProp).orElseThrow(() -> new NotFoundException("Propuesta", idProp));

    FormalizacionCompletaOutputDTO result = formalizacionMapper.createFormalizacionDTO(p);
    return result;
  }

  public LinkedHashMap<String, List<Collection<?>>> findExcelFormalizacionById(Integer id) throws Exception {
    LinkedHashMap<String, List<Collection<?>>> datos = formalizacionExport.createFormalizaciónCompletaForExcel();
    Propuesta p = propuestaRepository.findById(id).orElseThrow(() -> new NotFoundException("Propuesta", id));
    formalizacionExport.addPropuestaForExcel(p, datos);
    return datos;
  }

  public LinkedHashMap<String, List<Collection<?>>> findExcelBusquedaById(
      List<Integer> ids,
      Boolean todos,
      BusquedaDto busquedaDto,
      Usuario loggedUser,
      String id,
      String titular,
      String nif,
      String estado,
      String detalleEstado,
      String fechaFirma,
      String horaFirma,
      String formalizacionHaya,
      String gestorHaya,
      String formalizacionCliente,
      String fechaSancion,
      String importeDacion,
      String importeDeuda,
      String provincia,
      String alquiler) {
    if (todos != null && todos){
      ListWithCountDTO<FormalizacionBusquedaOutputDTO> dtos = busquedaUseCase.getBusquedaFormalizacion(busquedaDto, loggedUser,
        id,
        titular,
        nif,
        estado,
        detalleEstado,
        fechaFirma,
        horaFirma,
        formalizacionHaya,
        gestorHaya,
        formalizacionCliente,
        fechaSancion,
        importeDacion,
        importeDeuda,
        provincia,
        alquiler,
        "id", "desc", null, 1);
      ids = dtos.getList().stream().map(FormalizacionBusquedaOutputDTO::getIdExpediente).collect(Collectors.toList());
    }

    List<Expediente> exps = expedienteRepository.findByIdIn(ids);
    return formalizacionExport.excelBusquedaFormalizacion(exps);
  }

  public ResponseEntity<byte[]> generarDocumento(InformeInputDTO inport, MultipartFile[] files, Usuario logged) throws IOException, NotFoundException, RequiredValueException, InterruptedException {
    Usuario user = usuarioRepository.findById(logged.getId()).orElse(logged);

    if (files == null) files = new MultipartFile[0];
    if (inport.getTipoPlantilla() == null) throw new RequiredValueException("tipoPlantilla");
    if (inport.getIdPropuesta() == null) throw new RequiredValueException("idPropuesta");

    PlantillaFormalizacion pf = plantillaFormalizacionRepository.findById(inport.getTipoPlantilla()).orElseThrow(() -> new NotFoundException("PlantillaFormalizacion", inport.getTipoPlantilla()));

    ResponseEntity<byte[]> documento = null;

    Propuesta p = propuestaRepository.findById(inport.getIdPropuesta()).orElseThrow(() -> new NotFoundException("Propuesta", inport.getIdPropuesta()));
    Expediente e = p.getExpediente();

    if (e == null) throw new NotFoundException("Propuesta", "Espediente");

    String[] emails;

    switch (pf.getKey()) {
      case "3NC61":
      case "61CT4":
      case "V3961N":
        List<Bien> bienes = p.getBienes().stream().map(PropuestaBien::getBien).distinct().collect(Collectors.toList());

        emails = formalizacionMapper.getEmails(inport.getDestinatarios(), inport.getDestinatariosList());
        List<SendEmailDTO> emailsDTOs = formalizacionMapper.obtenerSendEmail(p, bienes, pf.getKey(), emails);
        comunicacionesUseCase.envioEmailBatch(pf.getKey(), emailsDTOs, user);
        break;
      case "CCS":
        documento = formalizacionExport.pdfCCD(p, inport, files);
        break;
      case "CCR":
        documento = formalizacionExport.pdfCCR(e, p);
        break;
      case "CAE":
        documento = formalizacionExport.pdfCAE(p, inport, files);
        emails = formalizacionMapper.getEmails(inport.getDestinatarios(), inport.getDestinatariosList());
        smtpUseCase.sendEmail(emails, pf.getNombrePlantilla(), pf.getTextoPlantilla(), documento, "Informe Aviso Encargo DDL.zip");
        break;
      case "CAA":
        documento = formalizacionExport.pdfCAA(p, inport, files);
        emails = formalizacionMapper.getEmails(inport.getDestinatarios(), inport.getDestinatariosList());
        smtpUseCase.sendEmail(emails, pf.getNombrePlantilla(), pf.getTextoPlantilla(), documento, "Informe Aviso Asistencia.zip");
        break;
    }

    return documento;
  }

  public List<CheckListOutputDTO> getCheckListOutput(Integer idPropuesta) throws NotFoundException, IOException {
    List<CheckListOutputDTO> result = new ArrayList<>();

    Propuesta p =
        propuestaRepository
            .findById(idPropuesta)
            .orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));
    List<Bien> bienes =
        p.getBienes().stream().map(PropuestaBien::getBien).distinct().collect(Collectors.toList());

    DocumentacionValidada dvDesc =
        documentacionValidadaRepository
            .findByCodigo("N")
            .orElseThrow(() -> new NotFoundException("DocumentacionValidada", "Codigo", "N"));

    List<NombresCheckList> nombres = nombresCheckListRepository.findAll();

    List<Documento> listaDocumentos = getDocumentos(idPropuesta);

    List<String> matriculas = listaDocumentos.stream().map(this::obtenerMatricula).collect(Collectors.toList());

    for (NombresCheckList ncl : nombres) {
      Boolean documentado = matriculas.contains(ncl.getCodigo());

      DatosCheckList dclFinal = null;
      DatosCheckList dcl =
          datosCheckListRepository
              .findByPropuestaIdAndNombreId(idPropuesta, ncl.getId())
              .orElse(new DatosCheckList());
      if (dcl.getId() == null) {
        dcl.setDocumentacionValidada(dvDesc);
        dcl.setPropuesta(p);
        dcl.setNombre(ncl);
        dcl.setBienes(new HashSet<>(bienes));

        dclFinal = datosCheckListRepository.save(dcl);
      } else {
        dclFinal = dcl;
      }

      result.add(formalizacionMapper.entityToDTO(dclFinal, documentado));
    }

    return result;
  }

  private List<Documento> getDocumentos(Integer idPropuesta) throws IOException {
    List<Documento> listaDocumentos = new ArrayList<>();

    String idPropuestaString = idPropuesta.toString();

    List<Documento> listaDocumentosGestion;
    listaDocumentosGestion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_REESTRUCTURACION_REFINANCIACION, idPropuestaString);
    listaDocumentos.addAll(listaDocumentosGestion);
    listaDocumentosGestion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_VENTA_CONSENSUADA, idPropuestaString);
    listaDocumentos.addAll(listaDocumentosGestion);
    listaDocumentosGestion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_DACION, idPropuestaString);
    listaDocumentos.addAll(listaDocumentosGestion);
    listaDocumentosGestion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_POSESION_NEGOCIADA, idPropuestaString);
    listaDocumentos.addAll(listaDocumentosGestion);
    listaDocumentosGestion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_CANCELACIONES, idPropuestaString);
    listaDocumentos.addAll(listaDocumentosGestion);
    listaDocumentosGestion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_REACTIVACIONES, idPropuestaString);
    listaDocumentos.addAll(listaDocumentosGestion);
    listaDocumentosGestion =
      this.servicioGestorDocumental
        .listarDocumetosPropuesta(CodigoEntidad.PROPUESTAS_VENTA_DE_CREDITO, idPropuestaString);
    listaDocumentos.addAll(listaDocumentosGestion);

    return listaDocumentos;
  }

  private String obtenerMatricula(Documento d){
    String result = "";

    result += d.getTipoExpediente() + "-";
    result += d.getSerieDocumental() + "-";
    result += d.getTdn1() + "-";
    result += d.getTdn2();

    return result;
  }

  public List<CheckListOutputDTO> getCheckList(Integer idPropuesta, List<CheckListInputDTO> checkListInputDTOS, List<MultipartFile> documentos, Usuario usuario) throws Exception {
    Propuesta propuesta = propuestaRepository.findById(idPropuesta).orElseThrow();
    HashMap<CheckListInputDTO, MultipartFile> unionCheckDocumentos = new HashMap<>();
    for (CheckListInputDTO checkListInputDTO : checkListInputDTOS) {
      if (checkListInputDTO.getNuevoDocumento()) {
        unionCheckDocumentos.put(checkListInputDTO, documentos.get(0));
        documentos.remove(0);
      }
    }
    for (Map.Entry<CheckListInputDTO, MultipartFile> entry : unionCheckDocumentos.entrySet()) {
      CheckListInputDTO checkListInputDTO = entry.getKey();
      gestorDocumentalCheckList(propuesta, checkListInputDTO, entry, usuario);
    }

    for (CheckListInputDTO checkListInputDTO : checkListInputDTOS){
      DatosCheckList dcl =
        datosCheckListRepository
          .findByPropuestaIdAndNombreId(idPropuesta, checkListInputDTO.getNombre())
          .orElse(new DatosCheckList());
      if (dcl.getPropuesta() == null) dcl.setPropuesta(propuesta);
      formalizacionMapper.dtoToEntity(checkListInputDTO, dcl);
      datosCheckListRepository.save(dcl);
    }

    return null;
  }

  private void gestorDocumentalCheckList(Propuesta propuesta, CheckListInputDTO checkListInputDTO, Map.Entry<CheckListInputDTO, MultipartFile> entry, Usuario usuario) throws Exception {
    MultipartFile fichero = entry.getValue();

    NombresCheckList nombresCheckList =
      nombresCheckListRepository.findById(checkListInputDTO.getNombre()).orElseThrow();

    MetadatoDocumentoInputDTO metadatoDocumentoInputDTO =
      new MetadatoDocumentoInputDTO(
        fichero.getOriginalFilename(), nombresCheckList.getCodigo());

    MetadatoContenedorInputDTO metadatoContenedorInputDTO =
      new MetadatoContenedorInputDTO(
        String.valueOf(propuesta.getId()),
        propuesta.getExpediente().getCartera().getCliente().getNombre());
    JSONObject metadatos = new JSONObject();
    JSONObject archivoFisico = new JSONObject();
    archivoFisico.put("contenedor", "CONT");
    metadatos.put("Operación", metadatoContenedorInputDTO.createDescripcion());

    TipoPropuesta tipoPropuesta = propuesta.getTipoPropuesta();
    CodigoEntidad codigoEntidad = null;
    if (tipoPropuesta.getValor().equals("Refinanciación")) {
      codigoEntidad = CodigoEntidad.PROPUESTAS_REESTRUCTURACION_REFINANCIACION;
    } else if (tipoPropuesta.getValor().equals("Venta consensuada")) {
      codigoEntidad = CodigoEntidad.PROPUESTAS_VENTA_CONSENSUADA;
    } else if (tipoPropuesta.getValor().equals("Dación")) {
      codigoEntidad = CodigoEntidad.PROPUESTAS_DACION;
    } else if (tipoPropuesta.getValor().equals("De posesión")) {
      codigoEntidad = CodigoEntidad.PROPUESTAS_POSESION_NEGOCIADA;
    } else if (tipoPropuesta.getValor().equals("Cancelación")) {
      codigoEntidad = CodigoEntidad.PROPUESTAS_CANCELACIONES;
    } else if (tipoPropuesta.getValor().equals("Reactivación")) {
      codigoEntidad = CodigoEntidad.PROPUESTAS_REACTIVACIONES;
    } else if (tipoPropuesta.getValor().equals("Venta de crédito")) {
      codigoEntidad = CodigoEntidad.PROPUESTAS_VENTA_DE_CREDITO;
    }
    if (codigoEntidad != null) {
      servicioGestorDocumental.procesarContenedor(codigoEntidad, metadatoContenedorInputDTO);
      long id =
        servicioGestorDocumental
          .procesarDocumentoPropuesta(
            codigoEntidad, propuesta.getId(), fichero, metadatoDocumentoInputDTO)
          .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      GestorDocumental gestorDocumental =
        new GestorDocumental(
          idgestorDocumental,
          usuario,
          null,
          new Date(),
          null,
          metadatoDocumentoInputDTO.getNombreFichero(),
          MatriculasEnum.obtenerPorMatricula(
            metadatoDocumentoInputDTO.getCatalogoDocumental()),
          null,
          propuesta.getId().toString(),
          TipoEntidadEnum.PROPUESTA,
          propuesta.getId().toString());
      gestorDocumentalRepository.save(gestorDocumental);
    }
  }

  public void okCheckList(Integer idPropuesta) throws NotFoundException {
    Formalizacion f = formalizacionRepository.findByPropuestaId(idPropuesta);
    EstadoChecklist ecl =
        estadoChecklistRepository
            .findByCodigo("ok")
            .orElseThrow(() -> new NotFoundException("EstadoChecklist", "Codigo", "ok"));
    f.setEstadoChecklist(ecl);
    f.setFechaFinCheckList(new Date());

    formalizacionRepository.save(f);
  }

  public InformacionCheckListDTO getInformacionCheckList(Integer idPropuesta) throws NotFoundException {
    InformacionCheckList icl =
        informacionCheckListRepository
            .findByFormalizacionPropuestaId(idPropuesta)
            .orElse(new InformacionCheckList());
    if (icl.getId() == null){
      Formalizacion f = formalizacionRepository.findByPropuestaId(idPropuesta);
      if (f == null || f.getPropuesta() == null){
        Propuesta p =
          propuestaRepository
            .findById(idPropuesta)
            .orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));
        if (p.getFormalizaciones().isEmpty()){
          Formalizacion fOld = new Formalizacion();
          fOld.setPropuesta(p);
          f = formalizacionRepository.save(fOld);
        }
      }
      icl.setFormalizacion(f);
    }
    InformacionCheckListDTO dto = new InformacionCheckListDTO();
    formalizacionMapper.entityToDto(icl, dto);
    dto.setIdPropuesta(idPropuesta);

    return dto;
  }

  public void updateInformacionCheckList(InformacionCheckListDTO dto) throws NotFoundException {
    InformacionCheckList icl =
        informacionCheckListRepository
            .findByFormalizacionPropuestaId(dto.getIdPropuesta())
            .orElse(new InformacionCheckList());
    if (icl.getId() == null){
      Formalizacion f = formalizacionRepository.findByPropuestaId(dto.getIdPropuesta());
      if (f == null || f.getPropuesta() == null){
        Propuesta p =
            propuestaRepository
                .findById(dto.getIdPropuesta())
                .orElseThrow(() -> new NotFoundException("Propuesta", dto.getIdPropuesta()));
        if (p.getFormalizaciones().isEmpty()){
          Formalizacion fOld = new Formalizacion();
          fOld.setPropuesta(p);
          f = formalizacionRepository.save(fOld);
        }
      }
      icl.setFormalizacion(f);
    }
    formalizacionMapper.dtoToEntity(icl, dto);
    informacionCheckListRepository.save(icl);
  }

  public String getNombrePrimerInterviniente(Integer idPropuesta) throws NotFoundException {
    Propuesta p =
        propuestaRepository
            .findById(idPropuesta)
            .orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));
    Expediente ex = p.getExpediente();
    if (ex == null) return null;
    Contrato cR = ex.getContratoRepresentante();
    if (cR == null) return null;
    Interviniente iP = cR.getPrimerInterviniente();
    if (iP == null) return null;
    return iP.getNombreReal();
  }

  public LinkedHashMap<String, List<Collection<?>>> findExcelCheckList(Integer id) throws Exception {
    LinkedHashMap<String, List<Collection<?>>> datos = formalizacionExport.checkListExcel();

    Propuesta p =
      propuestaRepository
        .findById(id)
        .orElseThrow(() -> new NotFoundException("Propuesta", id));
    List<Bien> bienes =
      p.getBienes().stream().map(PropuestaBien::getBien).distinct().collect(Collectors.toList());

    DocumentacionValidada dvDesc =
      documentacionValidadaRepository
        .findByCodigo("N")
        .orElseThrow(() -> new NotFoundException("DocumentacionValidada", "Codigo", "N"));

    List<NombresCheckList> nombres = nombresCheckListRepository.findAll();

    List<DatosCheckList> datosCL = new ArrayList<>();

    for (NombresCheckList ncl : nombres) {
      DatosCheckList dclFinal = null;
      DatosCheckList dcl =
        datosCheckListRepository
          .findByPropuestaIdAndNombreId(id, ncl.getId())
          .orElse(new DatosCheckList());
      if (dcl.getId() == null) {
        dcl.setDocumentacionValidada(dvDesc);
        dcl.setPropuesta(p);
        dcl.setNombre(ncl);
        dcl.setBienes(new HashSet<>(bienes));

        dclFinal = dcl;
      } else {
        dclFinal = dcl;
      }

      datosCL.add(dclFinal);
    }

    formalizacionExport.addCheckListExcel(datosCL, datos);
    return datos;
  }

  public void alertaAlGestor(Usuario logged, Integer idPropuesta) throws NotFoundException, RequiredValueException {
    Propuesta propuesta =
      propuestaRepository
        .findById(idPropuesta)
        .orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));

    Usuario gestor = null;
    Integer idExp = 0;
    String expNombre = "";
    if (propuesta.getExpediente() != null) {
      Expediente ex = propuesta.getExpediente();
      idExp = ex.getId();
      expNombre = "Espediente RA";
      gestor = ex.getGestor();
    } else if (propuesta.getExpedientePosesionNegociada() != null) {
      ExpedientePosesionNegociada ex = propuesta.getExpedientePosesionNegociada();
      idExp = ex.getId();
      expNombre = "Espediente PN";
      gestor = ex.getGestor();
    } else {
      throw new NotFoundException("Propuesta", "Expediente");
    }

    if (gestor == null) throw new NotFoundException("Gestor", expNombre, idExp);

    String titulo = "Revisión de Documentos";
    String descripcion = "Falta documentación/Información, por favor revisar check list.";

    eventoUseCase.alertaFormalizacion(propuesta, logged, gestor.getId(), titulo, descripcion);
  }

  private void alertasMúltiples(List<Propuesta> propuestas, String perfil, String titulo, String descripcion) {
    //Map<Usuario, List<Propuesta>> map = new HashMap<>();

    for (Propuesta p : propuestas){
      Usuario gestor = null;
      Integer idExp = 0;
      String expNombre = "";
      if (p.getExpediente() != null) {
        Expediente ex = p.getExpediente();
        idExp = ex.getId();
        expNombre = "Espediente RA";
        gestor = ex.getUsuarioByPerfil(perfil);
      } else if (p.getExpedientePosesionNegociada() != null) {
        ExpedientePosesionNegociada ex = p.getExpedientePosesionNegociada();
        idExp = ex.getId();
        expNombre = "Espediente PN";
        gestor = ex.getUsuarioByPerfil(perfil);
      } else {
        //throw new NotFoundException("Propuesta", "Expediente");
      }

      //if (gestor == null) throw new NotFoundException("Gestor", expNombre, idExp);
      /*if (gestor != null) {
        List<Propuesta> propIn = map.get(gestor);
        if (propIn == null){
          propIn = new ArrayList<>();
          propIn.add(p);
          map.put(gestor, propIn);
        } else {
          propIn.add(p);
          map.put(gestor, propIn);
        }
      }*/
      if (gestor != null) {
        try {
          eventoUseCase.alertaFormalizacion(p, null, gestor.getId(), titulo, descripcion);
        } catch (RequiredValueException | NotFoundException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private void emailMultiple(List<Propuesta> pS) throws InterruptedException, NotFoundException, IOException, RequiredValueException {
    for (Propuesta p : pS){
      String email = null;
      Usuario user = null;
      if (p.getExpediente() != null){
        Expediente ex = p.getExpediente();
        Usuario u = ex.getUsuarioByPerfil("Gestor formalización");
        user = u;
        if (u == null) throw new NotFoundException("Expediente", "Gestor formalización");
        email = u.getEmail();
      } else if (p.getExpedientePosesionNegociada() != null){
        ExpedientePosesionNegociada ex = p.getExpedientePosesionNegociada();
        Usuario u = ex.getUsuarioByPerfil("Gestor formalización");
        user = u;
        if (u == null) throw new NotFoundException("Expediente", "Gestor formalización");
        email = u.getEmail();
      }
      if (email == null) throw new NotFoundException("Propuesta", "Gestor formalización", "Email");
      String[] emails = new String[1];
      emails[0] = email;
      List<Bien> bienes = p.getBienes().stream().map(PropuestaBien::getBien).collect(Collectors.toList());
      List<SendEmailDTO> emailsDTOs = formalizacionMapper.obtenerSendEmail(p, bienes, "V3961N", emails);
      comunicacionesUseCase.envioEmailBatch("V3961N", emailsDTOs, user);

    }
  }

  public ByteArrayInputStream createExcelCheckList(Integer id) throws Exception {
    return formalizacionExport.createCheckList(findExcelCheckList(id));
  }

  public List<CatalogoMinInfoDTO> getTiposFormalizacion(){
    List<TipoPropuesta> estados = tipoPropuestaRepository.findAllDistinctByActivoIsTrueAndSubtiposPropuestaFormalizacionIsTrue();
    List<CatalogoMinInfoDTO> result = estados.stream().filter(tp -> !tp.getCodigo().equals("DP")).map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
    return result;
  }

  public List<SubtipoPropuestaDTO> getSubtiposFormalizacion(Integer tipoPropuesta){
    List<SubtipoPropuesta> estados = subtipoPropuestaRepository.findByTipoPropuestaIdAndActivoIsTrueAndFormalizacionIsTrue(tipoPropuesta);
    List<SubtipoPropuestaDTO> result = estados.stream().filter(tp -> !tp.getTipoPropuesta().getCodigo().equals("DP")).map(SubtipoPropuestaDTO::new).collect(Collectors.toList());
    return result;
  }

  @Scheduled(cron="1 0 0 * * *")
  private void comprobacionesFormalizacion(){
    Date hoy = new Date();
    Calendar c = Calendar.getInstance();

    c.setTime(hoy);
    c.add(Calendar.DATE, 2);
    Date fFinDoc = c.getTime();
    List<Propuesta> pFechaDoc =
        propuestaRepository
            .findAllByFormalizacionesEstadoChecklistCodigoNotAndFormalizacionesFechaOkDocumentacionGreaterThanAndFormalizacionesFechaOkDocumentacionLessThan(
                "ok", hoy, fFinDoc);
    alertasMúltiples(
        pFechaDoc,
        "Gestor",
        "Alerta Ok Documentación",
        "La propuesta está a 1 dia de llegar a la Fecha Ok Documentación, y aun no están ok los documentos de la misma.");

    c.setTime(hoy);
    c.add(Calendar.DATE, 9);
    Date fIniTF = c.getTime();
    c.add(Calendar.DATE, 2);
    Date fFinTF = c.getTime();
    List<Propuesta> pFechaTF =
        propuestaRepository
            .findAllByFormalizacionesFechaEntradaTFGreaterThanAndFormalizacionesFechaEntradaTFLessThan(
                fIniTF, fFinTF);
    alertasMúltiples(
        pFechaTF,
        "Gestor",
        "Alerta Fecha TF",
        "La propuesta está a 10 dias de llegar a la Fecha TF.");

    List<Propuesta> pFechaSanc =
        propuestaRepository
            .findAllByEstadoPropuestaCodigoNotAndFormalizacionesVencimientoSancionGreaterThanAndFormalizacionesVencimientoSancionLessThan(
                "FOR", fIniTF, fFinTF);

    try{
      emailMultiple(pFechaSanc);
    } catch (Exception e){

    }

    c.setTime(hoy);
    c.add(Calendar.DATE, 1);
    Date fIniFirm = c.getTime();
    c.add(Calendar.DATE, 2);
    Date fFinFirm = c.getTime();
    List<Propuesta> pFechaFirm =
        propuestaRepository
            .findAllByFormalizacionesFechaFirmaGreaterThanAndFormalizacionesFechaFirmaLessThan(
                fIniFirm, fFinFirm);
    alertasMúltiples(
        pFechaFirm,
        "Gestor formalización",
        "Alerta Fecha Firma",
        "Faltan 2 dias para que la propuesta llegue a la fecha firma.");

    c.setTime(hoy);
    c.add(Calendar.DATE, 9);
    Date fIniCad = c.getTime();
    c.add(Calendar.DATE, 2);
    Date fFinCad = c.getTime();
    List<Propuesta> pFechaCad =
        propuestaRepository
            .findAllByFechaValidezGreaterThanAndFechaValidezLessThan(
              fIniCad, fFinCad);
    alertasMúltiples(
      pFechaCad,
        "Gestor formalización",
        "Alerta Fecha Caducidad",
        "Faltan 10 dias para que la propuesta llegue a la fecha Validez.");


    c.setTime(hoy);
    c.add(Calendar.DATE, -11);
    Date fIniSub = c.getTime();
    c.add(Calendar.DATE, 2);
    Date fFinSub = c.getTime();
    List<Propuesta> pFechaSub =
        propuestaRepository
            .findAllBySiEstaSubida(
              fIniSub, fFinSub);
    alertasMúltiples(
      pFechaSub,
        "Gestor formalización",
        "Alerta Fecha No Subida",
        "Han pasado 10 dias desde la Fecha Recepción DDL o la Fecha Recepción DDT, y aún no se ha subido.");

    c.setTime(hoy);
    c.add(Calendar.DATE, -5);
    Date fIniEnt = c.getTime();
    c.add(Calendar.DATE, 2);
    Date fFinEnt = c.getTime();
    List<Propuesta> pFechaEnt =
        propuestaRepository
            .findAllByFormalizacionesFechaEntradaGreaterThanAndFormalizacionesFechaEntradaLessThanAndFormalizacionesFechaOkDocumentacionIsNullAndFormalizacionesFechaKoDocumentacionIsNull(
              fIniEnt, fFinEnt);
    alertasMúltiples(
        pFechaEnt,
        "Gestor formalización",
        "Alerta Fecha Entrada",
        "Han pasado 4 dias desde la fecha entrada, pero no se han informado las fechas OK/KO documentación.");

    c.setTime(hoy);
    c.add(Calendar.DATE, -4);
    Date fIniOk = c.getTime();
    c.add(Calendar.DATE, 2);
    Date fFinOk = c.getTime();
    List<Propuesta> pFechaOk =
        propuestaRepository
            .findAllByFormalizacionesFechaOkDocumentacionGreaterThanAndFormalizacionesFechaOkDocumentacionLessThanAndFormalizacionesFechaEncargoDDLIsNullAndFormalizacionesFechaEncargoDDTIsNull(
              fIniOk, fFinOk);
    alertasMúltiples(
      pFechaOk,
        "Gestor formalización",
        "Alerta Fecha Ok Documentación",
        "Han pasado 3 dias desde la fecha Ok Documentación, pero no se ha encargado el DDL ni el DDT.");

    c.setTime(hoy);
    c.add(Calendar.DATE, -31);
    Date fIniDD = c.getTime();
    c.add(Calendar.DATE, 1);
    Date fFinDD = c.getTime();
    List<Propuesta> pFechaDDL =
        propuestaRepository
            .findAllByFormalizacionesFechaEncargoDDLGreaterThanAndFormalizacionesFechaEncargoDDLLessThanAndFormalizacionesFechaRecepcionDDLIsNull(
              fIniDD, fFinDD);
    alertasMúltiples(
      pFechaDDL,
        "Gestor formalización",
        "Alerta Fecha Encargo DDL",
        "Han pasado 30 dias desde la fecha Encargo DDL, pero pero aún no se ha recibido.");
    List<Propuesta> pFechaDDT =
        propuestaRepository
            .findAllByFormalizacionesFechaEncargoDDTGreaterThanAndFormalizacionesFechaEncargoDDTLessThanAndFormalizacionesFechaRecepcionDDTIsNull(
              fIniDD, fFinDD);
    alertasMúltiples(
      pFechaDDT,
        "Gestor formalización",
        "Alerta Fecha Encargo DDT",
        "Han pasado 30 dias desde la fecha Encargo DDT, pero pero aún no se ha recibido.");

    c.setTime(hoy);
    c.add(Calendar.DATE, -3);
    Date fIniFir = c.getTime();
    c.add(Calendar.DATE, 2);
    Date fFinFir = c.getTime();
    List<Propuesta> pFechaFir =
      propuestaRepository
        .findAllByFormalizacionesSubestadoFormalizacionCodigoAndFormalizacionesFechaFirmaGreaterThanAndFormalizacionesFechaFirmaLessThanAndFormalizacionesFechaComunicacionIsNull(
          "FIR_2", fIniFir, fFinFir);
    alertasMúltiples(
      pFechaFir,
      "Gestor",
      "Alerta Fecha Firma",
      "La propuesta está firmada desde hace 2 dias, pero aun no se ha comunicado.");
  }
}
