package com.haya.alaska.formalizacion.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionInputDTO {
    
    // 1- Datos del Gestor
    String gestorRecuperacionCliente;
    String telefono1GesRecCli;
    String analistaCliente;
    String telefono1Cliente;
    String gestorFormalizacion;
    String telefono1GesFormalizacion;
    String gestorFormalizacionCliente;
    String telefono1GesFormalizacionCliente;

    // 2- Datos Estado
    Date fechaSancion;
    Boolean documentacionCompletada;
    Boolean solicitudGestionActivosVisita;
    Date fEnvioMinutaANotaria;
    Date fEnvioMinutaAClienteFormalizacion;
    Date fOkMinutaANotaria;
    Boolean informeVisitaOk;
    Date fechaFirma;
    String horaFirma;
    Date fechaPosesion;
    Boolean alquiler;
    Date fechaVigencia;
    Boolean pbc;
    String comentarios;

    // 3- Notaria
    String direccionNotaria;
    String notario;
    Integer numeroProtocolo;
    String contactoOficinaCliente;
    String apoderadoFirma;
    String gestoriaTramitadora;
    String oficinaNotaria;
    String telefonoNotaria;
    String telefonoOficialNotaria;
    String emailOficialNotaria;
}
