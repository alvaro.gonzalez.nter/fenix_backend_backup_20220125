package com.haya.alaska.formalizacion.infrastructure.controller;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.formalizacion.aplication.FormalizacionUseCase;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.*;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.InformacionCheckListDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input.FormalizacionCompletaInputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output.FormalizacionCompletaOutputDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/formalizacion")
public class FormalizacionController {
  @Autowired
  FormalizacionUseCase formalizacionUseCase;

  @ApiOperation(value = "Obtener formalizacion", notes = "Devuelve la formalizacion con el id de la propuesta enviado")
  @GetMapping("/{idPropuesta}")
  public FormalizacionOutputDTO getFormalizacionByIdPropuesta(@PathVariable("idPropuesta") Integer idPropuesta,
                                                              @RequestParam("posesionNegociada") Boolean posesionNegociada)
    throws NotFoundException {
    return formalizacionUseCase.findByPropuestaId(idPropuesta, posesionNegociada);
  }

  @ApiOperation(value = "Modificar formalizacion", notes = "Modificar el formalizacion con id enviado")
  @PutMapping("/{idPropuesta}")
  public FormalizacionOutputDTO updateFormalizacion(@PathVariable("idPropuesta") Integer idPropuesta, @RequestBody @Valid FormalizacionInputDTO formalizacionInputDTO, @RequestParam("posesionNegociada") Boolean posesionNegociada)
    throws NotFoundException, InvalidCodeException {
    return formalizacionUseCase.updateFormalicacionByInput(formalizacionInputDTO, idPropuesta, posesionNegociada);
  }

  @ApiOperation(value = "Marcar estado PBC de formalización")
  @PostMapping("/{idPropuesta}/pbc/{estado}")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void updatePbcFormalizacion(@PathVariable("idPropuesta") Integer idPropuesta,
                                     @PathVariable("estado") String estado) throws NotFoundException, InvalidCodeException {
    formalizacionUseCase.updatePbcFormalizacion(idPropuesta, estado);
  }

  @PostMapping("/carga")
  public void carga(
    @RequestParam(value = "cliente") Integer cliente,
    @RequestParam(value = "cartera") Integer cartera,
    @RequestParam("file") MultipartFile file,
    @ApiIgnore CustomUserDetails principal) throws Exception {

    formalizacionUseCase.cargaFormalizacion(cliente, cartera, (Usuario) principal.getPrincipal(), file);
  }

  @PostMapping()
  public void cargaManual(
    @RequestParam(value = "cliente", required = false) Integer cliente,
    @RequestParam(value = "cartera", required = false) Integer cartera,
    @RequestPart FormalizacionCompletaInputDTO input,
    @RequestParam(value = "documentos", required = false) List<MultipartFile> documentos,
    @ApiIgnore CustomUserDetails principal) throws Exception {

    formalizacionUseCase.cargaManual(cliente, cartera, (Usuario) principal.getPrincipal(), input, documentos);
  }

  @ApiOperation(value = "Obtener formalizacion completa", notes = "Devuelve la formalizacion completa con el id de la propuesta enviado")
  @GetMapping("/completa/{idPropuesta}")
  public FormalizacionCompletaOutputDTO getFormalizacionCompletaByIdPropuesta(
    @PathVariable("idPropuesta") Integer idPropuesta)
    throws NotFoundException {
    return formalizacionUseCase.getFormalizacionCompleta(idPropuesta);
  }

  @ApiOperation(value = "Descargar Formalización Completa en Excel",
    notes = "Descargar todos los datos de Formalización Completa en Excel")
  @GetMapping("/completa/{idPropuesta}/excel")
  public ResponseEntity<InputStreamResource> getExcelFormalizaciónCompleta(@PathVariable("idPropuesta") Integer idPropuesta) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(formalizacionUseCase.findExcelFormalizacionById(idPropuesta));

    String nombre = formalizacionUseCase.getNombrePrimerInterviniente(idPropuesta);
    String nombreFinal = "FormalizacionCompleta_" + (nombre != null ? nombre : idPropuesta + "_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

    return excelExport.download(excel, nombreFinal);
  }

  @ApiOperation(value = "Descargar Busqueda Formalización en Excel",
    notes = "Descargar todos los datos de Busqueda Formalización en Excel")
  @PostMapping("/busqueda/excel")
  public ResponseEntity<InputStreamResource> getExcelBusqueda(
    @RequestParam("idsExpedientes") List<Integer> idsExpedientes,
    @RequestParam("todos") Boolean todos,
    @RequestParam(value = "idConcatenado", required = false) String idConcatenado,
    @RequestParam(value = "titular", required = false) String titular,
    @RequestParam(value = "nif", required = false) String nif,
    @RequestParam(value = "estado", required = false) String estado,
    @RequestParam(value = "detalleEstado", required = false) String detalleEstado,
    @RequestParam(value = "fechaFirma", required = false) String fechaFirma,
    @RequestParam(value = "horaFirma", required = false) String horaFirma,
    @RequestParam(value = "formalizacionHaya", required = false) String formalizacionHaya,
    @RequestParam(value = "gestorHaya", required = false) String gestorHaya,
    @RequestParam(value = "formalizacionCliente", required = false) String formalizacionCliente,
    @RequestParam(value = "fechaSancion", required = false) String fechaSancion,
    @RequestParam(value = "importeDacion", required = false) String importeDacion,
    @RequestParam(value = "importeDeuda", required = false) String importeDeuda,
    @RequestParam(value = "provincia", required = false) String provincia,
    @RequestParam(value = "alquiler", required = false) String alquiler,
    @RequestBody BusquedaDto busqueda,
    @ApiIgnore CustomUserDetails principal
    ) throws IOException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(formalizacionUseCase.findExcelBusquedaById(
      idsExpedientes, todos,
      busqueda, usuario,
      idConcatenado,
      titular,
      nif,
      estado,
      detalleEstado,
      fechaFirma,
      horaFirma,
      formalizacionHaya,
      gestorHaya,
      formalizacionCliente,
      fechaSancion,
      importeDacion,
      importeDeuda,
      provincia,
      alquiler));

    return excelExport.download(excel, "BusquedaFormalizacion_" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
  }

  @ApiOperation(value = "Obtienes los tipos de Formalizacion",
    notes = "Obtienes los tipos cuyos subtipos tienen el indicador de formalización a true.")
  @GetMapping("/tipos")
  public List<CatalogoMinInfoDTO> getTiposFormalizacion() throws Exception {

    return formalizacionUseCase.getTiposFormalizacion();
  }

  @ApiOperation(value = "Descargar Check List en Excel",
    notes = "Descargar todos los datos de Check List en Excel")
  @GetMapping("/checklist/{idPropuesta}/excel")
  public ResponseEntity<InputStreamResource> getExcelCheckList(@RequestParam("idPropuesta") Integer idPropuesta) throws Exception {
    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = formalizacionUseCase.createExcelCheckList(idPropuesta);

    String nombre = formalizacionUseCase.getNombrePrimerInterviniente(idPropuesta);
    String nombreFinal = "CheckList_" + (nombre != null ? nombre : new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

    return excelExport.download(excel, nombreFinal);
  }

  @ApiOperation(value = "Comunicacion Formalizacion en pdf")
  @PostMapping("/plantilla/pdf")
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<byte[]> comunicacionFormalizacionPDF(
      @RequestPart(value = "files", required = false) MultipartFile[] request,
      @RequestPart(value = "datos") InformeInputDTO inport,
      @ApiIgnore CustomUserDetails principal)
      throws Exception {

    return formalizacionUseCase.generarDocumento(inport, request, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Checklist para la documentacion de Propuestas",
    notes = "Checklist para la documentacion de Propuestas")
  @PostMapping("/checklist/{idPropuesta}")
  public List<CheckListOutputDTO> getCheckList(@PathVariable("idPropuesta") Integer idPropuesta,
                                                @RequestPart(value = "checkList") List<CheckListInputDTO> checkListInputDTOS,
                                               @RequestParam(value = "documentos", required = false) List<MultipartFile> documentos,
                                               @ApiIgnore CustomUserDetails principal) throws Exception {
    return formalizacionUseCase.getCheckList(idPropuesta,checkListInputDTOS,documentos, (Usuario) principal.getPrincipal());
  }


  @ApiOperation(
      value = "Obtiene los datos de la check list de la propuesta",
      notes = "Obtiene los datos de la check list de la propuesta")
  @GetMapping("/{idPropuesta}/checkList")
  public List<CheckListOutputDTO> getCheckList(@PathVariable("idPropuesta") Integer idPropuesta)
      throws Exception {
    return formalizacionUseCase.getCheckListOutput(idPropuesta);
  }
  @ApiOperation(
      value = "Actualiza el estado de la checkList a OK",
      notes = "Actualiza el estado de la checkList a OK")
  @PutMapping("/{idPropuesta}/okCheckList")
  public void okCheckList(@PathVariable("idPropuesta") Integer idPropuesta)
      throws Exception {
    formalizacionUseCase.okCheckList(idPropuesta);
  }

  @ApiOperation(
      value = "Actualiza la informacion de la checkList",
      notes = "Actualiza la informacion de la checkList")
  @PutMapping("/informacionCheckList")
  public void updateInformacionCheckList(@RequestBody InformacionCheckListDTO dto)
      throws Exception {
    formalizacionUseCase.updateInformacionCheckList(dto);
  }

  @ApiOperation(
      value = "Obtencion de la informacion de la checkList",
      notes = "Obtencion de la informacion de la checkList")
  @GetMapping("/informacionCheckList/{idPropuesta}")
  public InformacionCheckListDTO getInformacionCheckList(@PathVariable("idPropuesta") Integer idPropuesta)
      throws Exception {
    return formalizacionUseCase.getInformacionCheckList(idPropuesta);
  }

  @ApiOperation(
      value = "Crea la alerta del gestor",
      notes = "Crea la alerta de revisión de documentos para el gestor de la propuesta")
  @PostMapping("/alertaGestor/{idPropuesta}")
  public void createEventoDocumentacion(@PathVariable("idPropuesta") Integer idPropuesta,
                                                           @ApiIgnore CustomUserDetails principal)
      throws Exception {
    formalizacionUseCase.alertaAlGestor((Usuario) principal.getPrincipal(), idPropuesta);
  }
}
