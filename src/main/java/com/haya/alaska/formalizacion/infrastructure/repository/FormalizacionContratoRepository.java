package com.haya.alaska.formalizacion.infrastructure.repository;

import com.haya.alaska.formalizacion.domain.FormalizacionContrato;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FormalizacionContratoRepository extends JpaRepository<FormalizacionContrato, Integer> {
    Optional<FormalizacionContrato> findByContratoId(Integer id);
}
