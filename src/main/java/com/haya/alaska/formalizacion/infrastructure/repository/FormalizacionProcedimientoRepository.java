package com.haya.alaska.formalizacion.infrastructure.repository;

import com.haya.alaska.formalizacion.domain.FormalizacionProcedimiento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FormalizacionProcedimientoRepository extends JpaRepository<FormalizacionProcedimiento, Integer> {
    Optional<FormalizacionProcedimiento> findByProcedimientoId(Integer id);
}
