package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionJudicialInputDTO implements Serializable {
  private Integer id;
  private List<String> contratos;
  private List<String> intervinientes;

  private Integer estadoProcedimiento; // EstadoProcedimiento
  private Integer tipoProcedimiento; // TipoProcedimiento

  private Boolean concurso;
  private Boolean litigio;
  private Boolean auto;
  private Boolean testimonio;

  private String nAuto;
  private String nJuzgado;
  private String plaza;
  private Boolean dispone;
}
