package com.haya.alaska.formalizacion.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.shared.dto.BienDTOToList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CheckListOutputDTO implements Serializable {
  private CatalogoMinInfoDTO nombre;
  private Boolean documentado;
  private Boolean obligatorio;
  private CatalogoMinInfoDTO validado;
  private List<BienDTOToList> bienes;
}
