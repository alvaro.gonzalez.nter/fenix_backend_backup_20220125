package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;


import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.formalizacion.domain.FormalizacionBien;
import com.haya.alaska.tasacion.domain.Tasacion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionBienOutputDTO implements Serializable {
  private Integer id;
  private String idCarga;

  private CatalogoMinInfoDTO riesgoDetectado; // RiesgosDetectados
  private String idHaya;
  private Date fechaAlta;
  private Double responsabilidadHipotecaria;

  private CatalogoMinInfoDTO tipoGarantia; // TipoGarantia
  private CatalogoMinInfoDTO tipoActivo; // TipoActivo
  private Boolean colateral;
  private CatalogoMinInfoDTO tipoBien; // TipoBien
  private CatalogoMinInfoDTO subtipoBien; // SubTipoBien
  private String idifur;

  private CatalogoMinInfoDTO municipio; // Municipio
  private CatalogoMinInfoDTO provincia; // Provincia
  private CatalogoMinInfoDTO localidad; // Localida

  private String finca;
  private String registro;
  private String direccion;
  private String referencia;

  private String localidadRegistro;

  private CatalogoMinInfoDTO alquiler; // Alquiler
  private CatalogoMinInfoDTO estadoOcupacion; // EstadoOcupacion

  private CatalogoMinInfoDTO depositoFinanzas;

  private Double deuOri_pri;
  private Double deuOri_iO;
  private Double deuOri_iD;

  private Double resHip_pri;
  private Double resHip_iO;
  private Double resHip_iD;

  private Boolean riesgo;

  private List<FormalizacionCargaOutputDTO> cargas = new ArrayList<>();
  private List<FormalizacionTasacionOutputDTO> tasaciones = new ArrayList<>();
  private List<FormalizacionContratoBienOutputDTO> contratos = new ArrayList<>();

  public FormalizacionBienOutputDTO(Bien b, FormalizacionBien fb){
    this.id = b.getId();
    this.idCarga = b.getIdCarga();

    this.riesgoDetectado = fb.getRiesgosDetectados() != null ? new CatalogoMinInfoDTO(fb.getRiesgosDetectados()) : null;
    this.idHaya = b.getIdHaya();
    this.fechaAlta = b.getFechaCarga();
    this.responsabilidadHipotecaria = b.getResponsabilidadHipotecaria();

    this.tipoActivo = b.getTipoActivo() != null ? new CatalogoMinInfoDTO(b.getTipoActivo()) : null;
    this.tipoBien = b.getTipoBien() != null ? new CatalogoMinInfoDTO(b.getTipoBien()) : null;
    this.subtipoBien = b.getSubTipoBien() != null ? new CatalogoMinInfoDTO(b.getSubTipoBien()) : null;
    this.idifur = b.getIdufir();

    this.municipio = b.getMunicipio() != null ? new CatalogoMinInfoDTO(b.getMunicipio()) : null;
    this.provincia = b.getProvincia() != null ? new CatalogoMinInfoDTO(b.getProvincia()) : null;
    this.localidad = b.getLocalidad() != null ? new CatalogoMinInfoDTO(b.getLocalidad()) : null;

    this.finca = b.getFinca();
    this.registro = b.getRegistro();
    this.direccion = b.getNombreVia();
    this.referencia = b.getReferenciaCatastral();

    this.localidadRegistro = b.getLocalidadRegistro();

    this.alquiler = fb.getAlquiler() != null ? new CatalogoMinInfoDTO(fb.getAlquiler()) : null;
    this.estadoOcupacion = b.getEstadoOcupacion() != null ? new CatalogoMinInfoDTO(b.getEstadoOcupacion()) : null;

    this.depositoFinanzas = fb.getDepositoFinanzas() != null ? new CatalogoMinInfoDTO(fb.getDepositoFinanzas()) : null;

    this.deuOri_pri = fb.getDeudaOriginal_principal();
    this.deuOri_iO = fb.getDeudaOriginal_interesesOrdinarios();
    this.deuOri_iD = fb.getDeudaOriginal_interesesDeDemora();

    this.resHip_pri = fb.getResponsabilidadHipotecaria_principal();
    this.resHip_iO = fb.getResponsabilidadHipotecaria_interesesOrdinarios();
    this.resHip_iD = fb.getResponsabilidadHipotecaria_interesesDeDemora();

    this.riesgo = fb.getRiesgoConsignacion();

    List<FormalizacionTasacionOutputDTO> tass = new ArrayList<>();
    for (Tasacion t : b.getTasaciones()){
      tass.add(new FormalizacionTasacionOutputDTO(t));
    }
    if (!tass.isEmpty())
      this.tasaciones = tass.stream().sorted(Comparator.comparing(FormalizacionTasacionOutputDTO::getFechaTasacion)).collect(Collectors.toList());

    for (ContratoBien cb : b.getContratos()){
      this.contratos.add(new FormalizacionContratoBienOutputDTO(cb));
    }
  }
}
