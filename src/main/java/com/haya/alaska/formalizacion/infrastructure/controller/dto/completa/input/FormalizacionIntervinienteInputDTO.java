package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionIntervinienteInputDTO implements Serializable {
  private Integer id;
  private List<FormalizacionContratoIntervinienteInputDTO> contratos;

  private String idCarga;
  private Boolean estado;
  private String nombre;
  private String apellidos;
  private String razonSocial;

  private Date fechaNacimiento;
  private Date fechaConstitucion;
  private Integer tipoDocumento; // TipoDocumento
  private String dni_cif;
}
