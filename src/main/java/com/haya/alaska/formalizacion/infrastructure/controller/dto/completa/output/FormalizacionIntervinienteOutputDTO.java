package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.interviniente.domain.Interviniente;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionIntervinienteOutputDTO implements Serializable {
  private Integer id;

  private String idCarga;
  private Boolean estado;
  private String nombre;
  private String apellidos;
  private String razonSocial;

  private Date fechaNacimiento;
  private Date fechaConstitucion;
  private CatalogoMinInfoDTO tipoDocumento; // TipoDocumento
  private String dni_cif;

  List<FormalizacionContactoOutputDTO> contactos = new ArrayList<>();

  public FormalizacionIntervinienteOutputDTO(Interviniente i){
    this.id = i.getId();

    this.idCarga = i.getIdCarga();
    this.estado = i.getActivo();
    this.nombre = i.getNombre();
    this.apellidos = i.getApellidos();
    this.razonSocial = i.getRazonSocial();

    this.fechaNacimiento = i.getFechaNacimiento();
    this.fechaConstitucion = i.getFechaConstitucion();
    this.tipoDocumento = i.getTipoDocumento() != null ? new CatalogoMinInfoDTO(i.getTipoDocumento()) : null;
    this.dni_cif = i.getNumeroDocumento();

    for (DatoContacto dc : i.getDatosContacto()){
      Integer id = dc.getId();
      Integer orden = dc.getOrden();
      if (dc.getMovil() != null){
        this.contactos.add(new FormalizacionContactoOutputDTO(id, "Movil", dc.getMovil(), orden));
      }
      if (dc.getFijo() != null){
        this.contactos.add(new FormalizacionContactoOutputDTO(id, "Fijo", dc.getFijo(), orden));
      }
      if (dc.getEmail() != null){
        this.contactos.add(new FormalizacionContactoOutputDTO(id, "Email", dc.getEmail(), orden));
      }
    }
  }
}
