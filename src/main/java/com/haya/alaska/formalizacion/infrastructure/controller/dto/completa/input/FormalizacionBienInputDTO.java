package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionBienInputDTO implements Serializable {
  private Integer id;
  private List<String> contratos;

  private String idCarga;
  private Integer riesgoDetectado; // RiesgosDetectados
  private String idHaya;
  private Date fechaAlta;
  private Double responsabilidadHipotecaria;

  private Integer tipoGarantia; // TipoGarantia
  private Integer tipoActivo; // TipoActivo
  private Boolean colateral;
  private Integer tipoBien; // TipoBien
  private Integer subtipoBien; // SubTipoBien
  private String idifur;

  private Integer municipio; // Municipio
  private Integer provincia; // Provincia
  private Integer localidad; // Localida

  private String finca;
  private String registro;
  private String direccion;
  private String referencia;

  private String tasadora;
  private Double valorTasacion;
  private Date fechaTasacion;

  private Integer alquiler; // Alquiler
  private Integer estadoOcupacion; // EstadoOcupacion

  private Integer depositoFinanzas;

  private Double deuOri_pri;
  private Double deuOri_iO;
  private Double deuOri_iD;

  private Double resHip_pri;
  private Double resHip_iO;
  private Double resHip_iD;

  private Boolean riesgo;
}
