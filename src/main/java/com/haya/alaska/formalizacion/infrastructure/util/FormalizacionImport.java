package com.haya.alaska.formalizacion.infrastructure.util;

import com.haya.alaska.alquiler.domain.Alquiler;
import com.haya.alaska.alquiler.infrastructure.repository.AlquilerRepository;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.infrastructure.repository.AsignacionExpedienteRepository;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.infrastructure.repository.CargaRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.clase_propuesta.domain.ClasePropuesta;
import com.haya.alaska.clase_propuesta.infrastructure.repository.ClasePropuestaRepository;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.comentario_propuesta.domain.ComentarioPropuesta;
import com.haya.alaska.comentario_propuesta.infrastructure.repository.ComentarioPropuestaRepository;
import com.haya.alaska.comprador.domain.Comprador;
import com.haya.alaska.comprador.infrastructure.repository.CompradorRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.infrastructure.repository.ContratoBienRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.deposito_finanzas.domain.DepositoFinanzas;
import com.haya.alaska.deposito_finanzas.infrastructure.repository.DepositoFinanzasRepository;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.estado_contrato.infrastructure.repository.EstadoContratoRepository;
import com.haya.alaska.estado_ddl_ddt.domain.EstadoDdlDdt;
import com.haya.alaska.estado_ddl_ddt.infrastructure.repository.EstadoDdlDdtRepository;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion;
import com.haya.alaska.estado_ocupacion.infrastructure.repository.EstadoOcupacionRepository;
import com.haya.alaska.estado_procedimiento.domain.EstadoProcedimiento;
import com.haya.alaska.estado_procedimiento.infrastructure.repository.EstadoProcedimientoRepository;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estado_propuesta.infrastructure.repository.EstadoPropuestaRepository;
import com.haya.alaska.expediente.application.ExpedienteUseCase;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.forma_de_pago.domain.FormaDePago;
import com.haya.alaska.forma_de_pago.infrastructure.repository.FormaDePagoRepository;
import com.haya.alaska.formalizacion.domain.*;
import com.haya.alaska.formalizacion.infrastructure.repository.*;
import com.haya.alaska.gestor_formalizacion.domain.GestorConcursos;
import com.haya.alaska.gestor_formalizacion.domain.GestorGestoria;
import com.haya.alaska.gestor_formalizacion.infrastructure.repository.GestorConcursosRepository;
import com.haya.alaska.gestor_formalizacion.infrastructure.repository.GestorGestoriaRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.minuta_revisada_fiscal.domain.MinutaRevisadaFiscal;
import com.haya.alaska.minuta_revisada_fiscal.infrastructure.repository.MinutaRevisadaFiscalRepository;
import com.haya.alaska.motivo_recomendacion_no_firma.domain.MotivoRecomendacionNoFirma;
import com.haya.alaska.motivo_recomendacion_no_firma.infrastructure.repository.MotivoRecomendacionNoFirmaRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.procedimiento.domain.*;
import com.haya.alaska.procedimiento.infrastructure.repository.*;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.producto.infrastructure.repository.ProductoRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.propuesta_bien.infrastructure.PropuestaBienRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.recomendacion_no_firma.domain.RecomendacionNoFirma;
import com.haya.alaska.recomendacion_no_firma.infrastructure.repository.RecomendacionNoFirmaRepository;
import com.haya.alaska.riesgos_detectados.domain.RiesgosDetectados;
import com.haya.alaska.riesgos_detectados.infrastructutr.repository.RiesgosDetectadosRepository;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.infrastructure.repository.SaldoRepository;
import com.haya.alaska.sancion_propuesta.domain.SancionPropuesta;
import com.haya.alaska.sancion_propuesta.infrastructure.repository.SancionPropuestaRepository;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien;
import com.haya.alaska.sub_tipo_bien.infrastructure.repository.SubTipoBienRepository;
import com.haya.alaska.subestado_formalizacion.domain.SubestadoFormalizacion;
import com.haya.alaska.subestado_formalizacion.infrastructure.repository.SubestadoFormalizacionRepository;
import com.haya.alaska.subtipo_carga_formalizacion.domain.SubtipoCargaFormalizacion;
import com.haya.alaska.subtipo_carga_formalizacion.infrastructure.repository.SubtipoCargaFormalizacionRepository;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.subtipo_estado.infrastructure.repository.SubtipoEstadoRepository;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import com.haya.alaska.subtipo_propuesta.infrastructure.repository.SubtipoPropuestaRepository;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.infrastructure.repository.TasacionRepository;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import com.haya.alaska.tipo_carga.infrastructure.repository.TipoCargaRepository;
import com.haya.alaska.tipo_carga_formalizacion.domain.TipoCargaFormalizacion;
import com.haya.alaska.tipo_carga_formalizacion.infrastructure.repository.TipoCargaFormalizacionRepository;
import com.haya.alaska.tipo_documento.domain.TipoDocumento;
import com.haya.alaska.tipo_documento.infrastructure.repository.TipoDocumentoRepository;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.tipo_estado.infrastructure.repository.TipoEstadoRepository;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;
import com.haya.alaska.tipo_garantia.infrastructure.repository.TipoGarantiaRepository;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.tipo_propuesta.infrastructure.repository.TipoPropuestaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Time;
import java.util.*;
import java.util.stream.Collectors;

import static com.haya.alaska.shared.util.ExcelImport.*;

@Component
public class FormalizacionImport {
  @Autowired UsuarioRepository usuarioRepository;
  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired AsignacionExpedienteRepository asignacionExpedienteRepository;
  @Autowired ClienteRepository clienteRepository;
  @Autowired CarteraRepository carteraRepository;
  @Autowired TipoEstadoRepository tipoEstadoRepository;
  @Autowired SubtipoEstadoRepository subtipoEstadoRepository;
  @Autowired ExpedienteUseCase expedienteUseCase;

  // Propuesta
  @Autowired PropuestaRepository propuestaRepository;
  @Autowired PropuestaBienRepository propuestaBienRepository;
  @Autowired ComentarioPropuestaRepository comentarioPropuestaRepository;
  @Autowired FormalizacionRepository formalizacionRepository;
  @Autowired ClasePropuestaRepository clasePropuestaRepository;
  @Autowired TipoPropuestaRepository tipoPropuestaRepository;
  @Autowired SubtipoPropuestaRepository subtipoPropuestaRepository;
  @Autowired EstadoPropuestaRepository estadoPropuestaRepository;
  @Autowired SubestadoFormalizacionRepository subestadoFormalizacionRepository;
  @Autowired SancionPropuestaRepository sancionPropuestaRepository;
  @Autowired RecomendacionNoFirmaRepository recomendacionNoFirmaRepository;
  @Autowired MotivoRecomendacionNoFirmaRepository motivoRecomendacionNoFirmaRepository;
  @Autowired CompradorRepository compradorRepository;
  @Autowired EstadoDdlDdtRepository estadoDdlDdtRepository;
  @Autowired GestorConcursosRepository gestorConcursosRepository;
  @Autowired GestorGestoriaRepository gestorGestoriaRepository;
  @Autowired MinutaRevisadaFiscalRepository minutaRevisadaFiscalRepository;

  // Contratos
  @Autowired ContratoRepository contratoRepository;
  @Autowired SaldoRepository saldoRepository;
  @Autowired FormalizacionContratoRepository formalizacionContratoRepository;
  @Autowired EstadoContratoRepository estadoContratoRepository;
  @Autowired ProductoRepository productoRepository;
  @Autowired FormaDePagoRepository formaDePagoRepository;

  // Bienes
  @Autowired BienRepository bienRepository;
  @Autowired ContratoBienRepository contratoBienRepository;
  @Autowired FormalizacionBienRepository formalizacionBienRepository;
  @Autowired TasacionRepository tasacionRepository;
  @Autowired RiesgosDetectadosRepository riesgosDetectadosRepository;
  @Autowired TipoGarantiaRepository tipoGarantiaRepository;
  @Autowired TipoActivoRepository tipoActivoRepository;
  @Autowired TipoBienRepository tipoBienRepository;
  @Autowired SubTipoBienRepository subTipoBienRepository;
  @Autowired MunicipioRepository municipioRepository;
  @Autowired ProvinciaRepository provinciaRepository;
  @Autowired LocalidadRepository localidadRepository;
  @Autowired AlquilerRepository alquilerRepository;
  @Autowired EstadoOcupacionRepository estadoOcupacionRepository;
  @Autowired DepositoFinanzasRepository depositoFinanzasRepository;

  // Cargas
  @Autowired CargaRepository cargaRepository;
  @Autowired FormalizacionCargaRepository formalizacionCargaRepository;
  @Autowired TipoCargaRepository tipoCargaRepository;
  @Autowired TipoCargaFormalizacionRepository tipoCargaFormalizacionRepository;
  @Autowired SubtipoCargaFormalizacionRepository subtipoCargaFormalizacionRepository;

  // Intervinientes
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired ContratoIntervinienteRepository contratoIntervinienteRepository;
  @Autowired TipoDocumentoRepository tipoDocumentoRepository;
  @Autowired TipoIntervencionRepository tipoIntervencionRepository;

  // Contactos Intervinientes
  @Autowired DatoContactoRepository datoContactoRepository;

  // Procedimiento
  @Autowired FormalizacionProcedimientoRepository formalizacionProcedimientoRepository;
  @Autowired ProcedimientoHipotecarioRepository procedimientoHipotecarioRepository;
  @Autowired ProcedimientoEtnjRepository procedimientoEtnjRepository;
  @Autowired ProcedimientoEjecucionNotarialRepository procedimientoEjecucionNotarialRepository;
  @Autowired ProcedimientoMonitorioRepository procedimientoMonitorioRepository;
  @Autowired ProcedimientoVerbalRepository procedimientoVerbalRepository;
  @Autowired ProcedimientoOrdinarioRepository procedimientoOrdinarioRepository;
  @Autowired ProcedimientoEtjRepository procedimientoEtjRepository;
  @Autowired ProcedimientoConcursalRepository procedimientoConcursalRepository;
  @Autowired TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired EstadoProcedimientoRepository estadoProcedimientoRepository;

  private static Integer firstRow = 3;

  @Transactional(rollbackFor = Exception.class)
  public void parseCarga(
      Integer idCliente, Integer idCartera, Usuario logged, MultipartFile file) // cliente cartera
      throws IOException, NotFoundException, RequiredValueException, InvalidCodeException,
          ForbiddenException {
    Usuario user =
        usuarioRepository
            .findById(logged.getId())
            .orElseThrow(() -> new NotFoundException("Usuario", logged.getId()));

    XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
    XSSFSheet wsAG = workbook.getSheet("Avance de Gestión");
    XSSFSheet wsP = workbook.getSheet("Condiciones Generales Operacion");
    XSSFSheet wsDD = workbook.getSheet("DD");
    XSSFSheet wsCo = workbook.getSheet("Contactos");
    XSSFSheet wsLit = workbook.getSheet("Litigio");
    XSSFSheet wsDE_C = workbook.getSheet("Datos Económicos - Contrato");
    XSSFSheet wsB = workbook.getSheet("Bienes");
    XSSFSheet wsCD = workbook.getSheet("CargasDeudas");
    XSSFSheet wsI = workbook.getSheet("Intervinientes");
    XSSFSheet wsCI = workbook.getSheet("Contacto Interviniente");
    XSSFSheet wsS = workbook.getSheet("Sareb");

    List<Contrato> cnts = parseExcelToEntityContrato(wsDE_C);
    List<Bien> bienes = parseExcelToEntityBien(wsB);
    List<Carga> cargas = parseExcelToEntityCarga(wsCD);
    List<ContratoInterviniente> intervinientes = parseExcelToEntityInterviniente(wsI);
    List<DatoContacto> datoContactos = parseExcelToEntityContacto(wsCI);
    List<Procedimiento> procedimientos = parseExcelToEntityProcedimiento(wsLit);

    List<Propuesta> props = parseExcelToEntityAG(wsAG, user);
    parseExcelToEntityPrincipal(wsP);
    parseExcelToEntityDD(wsDD);
    parseExcelToEntityContactos(wsCo, user);
    parseExcelToEntitySareb(wsS);

    Cliente cli =
        clienteRepository
            .findById(idCliente)
            .orElseThrow(() -> new NotFoundException("Cliente", idCliente));
    Cartera car =
        carteraRepository
            .findById(idCartera)
            .orElseThrow(() -> new NotFoundException("Cartera", idCartera));

    List<Contrato> contratos =
        contratoRepository.findAllById(
            cnts.stream().map(Contrato::getId).collect(Collectors.toList()));

    agruparContratosPorCriterioClienteInter(cli, car, intervinientes);

    List<Propuesta> propuestas =
        propuestaRepository.findAllById(
            props.stream().map(Propuesta::getId).collect(Collectors.toList()));
    asignarPropuestas(cli, car, propuestas);
  }

  private List<Propuesta> parseExcelToEntityAG(XSSFSheet worksheet, Usuario user)
      throws RequiredValueException, NotFoundException, InvalidCodeException {
    Integer ci = 1; // Columna Inicial
    List<Propuesta> propuestas = new ArrayList<>();

    if (worksheet.getPhysicalNumberOfRows() < 4) {
      String error = "";
      throw new RequiredValueException("PROPUESTA ORIGEN"); // TODO
    }

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);

      String idProp = getRawStringValueConGuion(row, ci);
      if (idProp == null) throw new RequiredValueException("PROPUESTA ORIGEN");

      Propuesta p = propuestaRepository.findByIdCarga(idProp).orElse(null);
      if (p != null) {
        String error = "Ya existe una propuesta con idCarga: " + idProp;
        Locale loc = LocaleContextHolder.getLocale();
        String defaultLocal = loc.getLanguage();
        if (defaultLocal == null || defaultLocal.equals("en"))
          error = "There is already a proposal with idCarga: " + idProp;
        throw new InvalidCodeException(error);
      }

      p = new Propuesta();
      p.setFechaAlta(new Date());
      p.setFechaUltimoCambio(new Date());
      p.setImporteTotal(0.0);
      p.setImporteElevacion(0.0);
      p.setUsuario(user);
      p.setIdCarga(idProp);
      Formalizacion f = new Formalizacion();

      String clasePropCod = getRawStringValueConGuion(row, ci + 1);
      if (clasePropCod != null) {
        ClasePropuesta cp =
            clasePropuestaRepository
                .findByActivoIsTrueAndCodigo(clasePropCod)
                .orElseThrow(() -> new NotFoundException("ClasePropuesta", "Codigo", clasePropCod));
        p.setClasePropuesta(cp);
      } else throw new RequiredValueException("Clase Propuesta");

      String tipoPropCod = getRawStringValueConGuion(row, ci + 2);
      if (tipoPropCod != null) {
        TipoPropuesta cp =
            tipoPropuestaRepository
                .findByActivoIsTrueAndCodigo(tipoPropCod)
                .orElseThrow(() -> new NotFoundException("TipoPropuesta", "Codigo", tipoPropCod));
        p.setTipoPropuesta(cp);
      } else throw new RequiredValueException("Tipo Operacion");

      String subPropCod = getRawStringValueConGuion(row, ci + 3);
      if (subPropCod != null) {
        SubtipoPropuesta cp =
            subtipoPropuestaRepository
                .findByActivoIsTrueAndCodigo(subPropCod)
                .orElseThrow(() -> new NotFoundException("SubtipoPropuesta", "Codigo", subPropCod));
        p.setSubtipoPropuesta(cp);
      } else throw new RequiredValueException("Subtipo Operacion");

      String estPropCod = getRawStringValueConGuion(row, ci + 4);
      if (estPropCod != null) {
        EstadoPropuesta cp =
            estadoPropuestaRepository
                .findByActivoIsTrueAndCodigo(estPropCod)
                .orElseThrow(() -> new NotFoundException("EstadoPropuesta", "Codigo", estPropCod));
        p.setEstadoPropuesta(cp);
      } else throw new RequiredValueException("ESTADO");

      String subestPropCod = getRawStringValueConGuion(row, ci + 5);
      if (subestPropCod != null) {
        SubestadoFormalizacion cp =
            subestadoFormalizacionRepository
                .findByActivoIsTrueAndCodigo(subestPropCod)
                .orElseThrow(
                    () -> new NotFoundException("SubestadoFormalizacion", "Codigo", subestPropCod));
        f.setSubestadoFormalizacion(cp);
      }

      String sancPropCod = getRawStringValueConGuion(row, ci + 6);
      if (sancPropCod != null) {
        SancionPropuesta cp =
          sancionPropuestaRepository
            .findByActivoIsTrueAndCodigo(sancPropCod)
            .orElseThrow(
              () -> new NotFoundException("SancionPropuesta", "Codigo", sancPropCod));
        p.setSancionPropuesta(cp);
      }

      Date fechaSan = getDateValueConGuion(row, ci + 7);
      if (fechaSan != null) f.setFechaSancion(fechaSan);

      Date vencSanc = getDateValueConGuion(row, ci + 8);
      if (vencSanc != null) f.setVencimientoSancion(vencSanc);

      Date fechaEnt = getDateValueConGuion(row, ci + 9);
      if (fechaEnt != null) f.setFechaEntrada(fechaEnt);

      Date fOKDocu = getDateValueConGuion(row, ci + 10);
      if (fOKDocu != null) f.setFechaOkDocumentacion(fOKDocu);
      Date fKODocu = getDateValueConGuion(row, ci + 11);
      if (fKODocu != null) f.setFechaKoDocumentacion(fKODocu);

      Date soliMin = getDateValueConGuion(row, ci + 12);
      if (soliMin != null) f.setSolicitudMinuta(soliMin);
      Date receMin = getDateValueConGuion(row, ci + 13);
      if (receMin != null) f.setRecepcionMinuta(receMin);
      Date minRevFis = getDateValueConGuion(row, ci + 14);
      if (minRevFis != null) f.setMinutaRevisadaFiscal(minRevFis);
      Date revCon = getDateValueConGuion(row, ci + 15);
      if (revCon != null) f.setFechaRevisionConcursos(revCon);
      Date revMinHRE = getDateValueConGuion(row, ci + 16);
      if (revMinHRE != null) f.setRevisionMinutaHRE(revMinHRE);
      Date revEnvMinNot = getDateValueConGuion(row, ci + 17);
      if (revEnvMinNot != null) f.setFEnvioMinutaANotaria(revEnvMinNot);
      Date receNotNot = getDateValueConGuion(row, ci + 18);
      if (receNotNot != null) f.setRecepcionMinutaDeNotaria(receNotNot);

      Date fEMCV = getDateValueConGuion(row, ci + 19);
      if (fEMCV != null) f.setFEnvioMinutaAClienteFormalizacion(fEMCV);
      Date fOkMin = getDateValueConGuion(row, ci + 20);
      if (fOkMin != null) f.setFOkMinutaANotaria(fOkMin);

      Date solGAV = getDateValueConGuion(row, ci + 21);
      if (solGAV != null) f.setSolicitudGestionActivosVisita(solGAV);
      Boolean infVisOk = getBooleanValue(row, ci + 22);
      if (infVisOk != null) f.setInformeVisitaOk(infVisOk);

      Date fechaFirmaEst = getDateValueConGuion(row, ci + 23);
      if (fechaFirmaEst != null) f.setFechaFirmaEstimada(fechaFirmaEst);

      Date fechaFirma = getDateValueConGuion(row, ci + 24);
      if (fechaFirma != null) f.setFechaFirma(fechaFirma);

      String horaFirma = getRawStringValueConGuion(row, ci + 25);
      if (horaFirma != null) {
        String[] tiempo = horaFirma.split(":");
        if (tiempo.length != 3) {
          throw new IllegalArgumentException("El formato de la Hora Firma debe ser: 'hh:mm:ss'");
        }
        Time hora = Time.valueOf(horaFirma);
        f.setHoraFirma(hora);
      }

      Date fechaComuni = getDateValueConGuion(row, ci + 26);
      if (fechaComuni != null) f.setFechaComunicacion(fechaComuni);

      Integer nProt = getIntegerValueConGuion(row, ci + 27);
      if (nProt != null) f.setNumeroProtocolo(nProt);

      /*Boolean conFechHor = getBooleanValue(row, ci + 9);
      if (conFechHor != null) f.setConfirmadaFechaYHora(conFechHor);*/

      /*Boolean docCom = getBooleanValue(row, ci + 14);
      if (docCom != null) f.setDocumentacionCompletada(docCom);*/
      Date fEntTF = getDateValueConGuion(row, ci + 28);
      if (fEntTF != null) f.setFechaEntradaTF(fEntTF);
      Date fAvaTTF = getDateValueConGuion(row, ci + 29);
      if (fAvaTTF != null) f.setFechaAvanceTareaTF(fAvaTTF);

      String observ = getRawStringValueConGuion(row, ci + 30);
      if (observ != null) f.setObservaciones(observ);

      Propuesta pSaved = propuestaRepository.save(p);
      propuestas.add(pSaved);

      f.setPropuesta(pSaved);
      formalizacionRepository.save(f);
    }
    return propuestas;
  }

  private List<Propuesta> parseExcelToEntityPrincipal(XSSFSheet worksheet)
      throws RequiredValueException, NotFoundException {
    Integer ci = 1; // Columna Inicial
    List<Propuesta> propuestas = new ArrayList<>();

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      if (!checkIfRowIsEmpty(row)) {

        String idProp = getRawStringValueConGuion(row, ci + 1);
        if (idProp == null) throw new RequiredValueException("PROPUESTA ORIGEN");
        Propuesta p =
            propuestaRepository
                .findByIdCarga(idProp)
                .orElseThrow(() -> new NotFoundException("Propuesta", "IdHaya", idProp));

        Formalizacion f = formalizacionRepository.findByPropuestaId(p.getId());
        String expOr = getRawStringValueConGuion(row, ci);

        Boolean dacSing = getBooleanValue(row, ci + 2);
        if (dacSing != null) f.setDacionSingular(dacSing);

        String comp = getRawStringValueConGuion(row, ci + 3);
        if (comp != null) {
          Comprador c =
            compradorRepository
              .findByActivoIsTrueAndCodigo(comp)
              .orElseThrow(() -> new NotFoundException("Comprador", "Codigo", comp));
          f.setComprador(c);
        }

        Integer tFFRR = getIntegerValueConGuion(row, ci + 4);
        if (tFFRR != null) f.setTotalFFRR(tFFRR);

        String formaPagCod = getRawStringValueConGuion(row, ci + 5);
        if (formaPagCod != null) {
          FormaDePago fp =
            formaDePagoRepository
              .findByActivoIsTrueAndCodigo(formaPagCod)
              .orElseThrow(() -> new NotFoundException("FormaDePago", "Codigo", formaPagCod));
          f.setFormaDePago(fp);
        }

        Boolean gastosAsume = getBooleanValue(row, ci + 6);
        if (gastosAsume != null) f.setGastosAsume(gastosAsume);
        Double gastosImporte = getNumericValueConGuion(row, ci + 7);
        if (gastosImporte != null) f.setGastosImporte(gastosImporte);

        Boolean plusAsume = getBooleanValue(row, ci + 8);
        if (plusAsume != null) f.setPlusvaliaAsume(plusAsume);
        Double plusImporte = getNumericValueConGuion(row, ci + 9);
        if (plusImporte != null) f.setPlusvaliaImporte(plusImporte);

        Boolean ibiAsume = getBooleanValue(row, ci + 10);
        if (ibiAsume != null) f.setIbiAsume(ibiAsume);
        Double ibiImporte = getNumericValueConGuion(row, ci + 11);
        if (ibiImporte != null) f.setIbiImporte(ibiImporte);

        Boolean cpAsume = getBooleanValue(row, ci + 12);
        if (cpAsume != null) f.setCpAsume(cpAsume);
        Double cpImporte = getNumericValueConGuion(row, ci + 13);
        if (cpImporte != null) f.setCpImporte(cpImporte);

        Boolean urbAsume = getBooleanValue(row, ci + 14);
        if (urbAsume != null) f.setUrbanizacionAsume(urbAsume);
        Double urbImporte = getNumericValueConGuion(row, ci + 15);
        if (urbImporte != null) f.setUrbanizacionImporte(urbImporte);

        Boolean embAsu = getBooleanValue(row, ci + 16);
        if (embAsu != null) f.setEmbargoOtrosAsume(embAsu);
        Double embImp = getNumericValueConGuion(row, ci + 17);
        if (embImp != null) f.setEmbargoOtrosImporte(embImp);

        Double importeEE = getNumericValueConGuion(row, ci + 18);
        if (importeEE != null) f.setImporteEntregaEfectivo(importeEE);

        Boolean pbc = getBooleanValue(row, ci + 19);
        if (pbc != null) f.setPbc(pbc);
        Date vigPbc = getDateValueConGuion(row, ci + 20);
        if (vigPbc != null) f.setFechaVigencia(vigPbc);

        Boolean entBL = getBooleanValue(row, ci + 21);
        if (entBL != null) f.setEntregaDeBienesLibres(entBL);

        Boolean firmCon = getBooleanValue(row, ci + 22);
        if (firmCon != null) f.setFirmaCondicionada(firmCon);

        Boolean penRatCom = getBooleanValue(row, ci + 23);
        if (penRatCom != null) f.setPendienteRatificacionComprador(penRatCom);
        Boolean penRatVen = getBooleanValue(row, ci + 24);
        if (penRatVen != null) f.setPendienteRatificacionVendedor(penRatVen);
        Boolean penRatBan = getBooleanValue(row, ci + 25);
        if (penRatBan != null) f.setPendienteRatificacionBanco(penRatBan);

        String alerCom = getRawStringValueConGuion(row, ci + 26);
        if (alerCom != null) f.setAlertaEnComunicacion(alerCom);

        String recNoFir = getRawStringValueConGuion(row, ci + 27);
        if (recNoFir != null) {
          RecomendacionNoFirma cp =
            recomendacionNoFirmaRepository
              .findByActivoIsTrueAndCodigo(recNoFir)
              .orElseThrow(
                () -> new NotFoundException("RecomendacionNoFirma", "Codigo", recNoFir));
          f.setRecomendacionNoFirma(cp);
        }

        String motRecNoFir = getRawStringValueConGuion(row, ci + 28);
        if (motRecNoFir != null) {
          MotivoRecomendacionNoFirma cp =
            motivoRecomendacionNoFirmaRepository
              .findByActivoIsTrueAndCodigo(motRecNoFir)
              .orElseThrow(
                () -> new NotFoundException("RecomendacionNoFirma", "Codigo", motRecNoFir));
          f.setMotivoRecomendacionNoFirma(cp);
        }

        String observ = getRawStringValueConGuion(row, ci + 29);
        if (observ != null) f.setObservaciones(observ);

        String nPres = getRawStringValueConGuion(row, ci + 30);
        if (nPres != null) asignarContratos(p, nPres);

        String nBien = getRawStringValueConGuion(row, ci + 31);
        if (nBien != null) asignarBienes(p, nBien);

        String nInte = getRawStringValueConGuion(row, ci + 32);
        if (nInte != null) asignarIntervinientes(p, nInte);

        Propuesta pSaved = propuestaRepository.save(p);
        propuestas.add(pSaved);
      }
    }
    return propuestas;
  }

  private void asignarContratos(Propuesta p, String codigos) throws NotFoundException {
    String[] contratos = codigos.split(",");
    for (String cod : contratos) {
      Contrato c =
          contratoRepository
              .findByIdCarga(cod)
              .orElseThrow(() -> new NotFoundException("Contrato", "IdCarga", cod));
      p.getContratos().add(c);
    }
  }

  private void asignarIntervinientes(Propuesta p, String codigos) throws NotFoundException {
    String[] intervinientes = codigos.split(",");
    for (String cod : intervinientes) {
      Interviniente c =
          intervinienteRepository
              .findByIdCarga(cod)
              .orElseThrow(() -> new NotFoundException("Interviniente", "IdCarga", cod));
      p.getIntervinientes().add(c);
    }
  }

  private void asignarBienes(Propuesta p, String codigos) throws NotFoundException {
    String[] bienes = codigos.split(",");
    for (String cod : bienes) {
      Bien b =
          bienRepository
              .findByIdCarga(cod)
              .orElseThrow(() -> new NotFoundException("Contrato", "IdCarga", cod));
      PropuestaBien pb = new PropuestaBien();
      pb.setPropuesta(p);
      pb.setBien(b);
      if (!b.getContratos().isEmpty()) {
        Contrato c =
            b.getContratos().stream().map(ContratoBien::getContrato).findFirst().orElse(null);
        pb.setContrato(c);
      }
      propuestaBienRepository.save(pb);
    }
  }

  private List<Propuesta> parseExcelToEntityDD(XSSFSheet worksheet)
      throws RequiredValueException, NotFoundException {
    Integer ci = 1; // Columna Inicial
    List<Propuesta> propuestas = new ArrayList<>();

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      if (!checkIfRowIsEmpty(row)) {
        String idProp = getRawStringValueConGuion(row, ci);
        if (idProp == null) throw new RequiredValueException("PROPUESTA ORIGEN");
        Propuesta p =
            propuestaRepository
                .findByIdCarga(idProp)
                .orElseThrow(() -> new NotFoundException("Propuesta", "IdHaya", idProp));

        Formalizacion f = formalizacionRepository.findByPropuestaId(p.getId());

        Date fEncDDL = getDateValueConGuion(row, ci + 1);
        if (fEncDDL != null) f.setFechaEncargoDDL(fEncDDL);
        Date fRecDDL = getDateValueConGuion(row, ci + 2);
        if (fRecDDL != null) f.setFechaRecepcionDDL(fRecDDL);

        String estdDDLCod = getRawStringValueConGuion(row, ci + 3);
        if (estdDDLCod != null) {
          EstadoDdlDdt c =
              estadoDdlDdtRepository
                  .findByActivoIsTrueAndCodigo(estdDDLCod)
                  .orElseThrow(() -> new NotFoundException("EstadoDdlDdt", "Codigo", estdDDLCod));
          f.setEstadoDDL(c);
        }

        Date fEncDDT = getDateValueConGuion(row, ci + 4);
        if (fEncDDT != null) f.setFechaEncargoDDT(fEncDDT);
        Date fRecDDT = getDateValueConGuion(row, ci + 5);
        if (fRecDDT != null) f.setFechaRecepcionDDT(fRecDDT);

        String estdDDTCod = getRawStringValueConGuion(row, ci + 6);
        if (estdDDTCod != null) {
          EstadoDdlDdt c =
              estadoDdlDdtRepository
                  .findByActivoIsTrueAndCodigo(estdDDTCod)
                  .orElseThrow(() -> new NotFoundException("EstadoDdlDdt", "Codigo", estdDDTCod));
          f.setEstadoDDT(c);
        }

        Boolean ddReSu = getBooleanValue(row, ci + 7);
        if (ddReSu != null) f.setDdRevisadaYSubida(ddReSu);
        Date fSubDD = getDateValueConGuion(row, ci + 8);
        if (fSubDD != null) f.setFechaSubidaDD(fSubDD);
        String valDD = getRawStringValueConGuion(row, ci + 9);
        if (valDD != null) f.setValoracionDDCliente(valDD);

        String despLeg = getRawStringValueConGuion(row, ci + 10);
        if (despLeg != null) f.setDespachoLegal(despLeg);
        Double honDD = getNumericValueConGuion(row, ci + 11);
        if (honDD != null) f.setHonorariosDD(honDD);
        Double honAF = getNumericValueConGuion(row, ci + 12);
        if (honAF != null) f.setHonorariosAsistenciaFirma(honAF);

        String despTec = getRawStringValueConGuion(row, ci + 13);
        if (despTec != null) f.setDespachoTecnico(despTec);

        Double honoDD = getNumericValueConGuion(row, ci + 14);
        if (honoDD != null) f.setHonorariosDD(honoDD);

        /*Boolean ddRec = getBooleanValue(row, ci + 9);
        if (ddRec != null) f.setDdRecibida(ddRec);*/


        propuestas.add(p);
        formalizacionRepository.save(f);
      }
    }
    return propuestas;
  }

  private List<Propuesta> parseExcelToEntityContactos(XSSFSheet worksheet, Usuario user)
      throws RequiredValueException, NotFoundException {
    Integer ci = 1; // Columna Inicial
    List<Propuesta> propuestas = new ArrayList<>();

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      if (!checkIfRowIsEmpty(row)) {
        String idProp = getRawStringValueConGuion(row, ci);
        if (idProp == null) throw new RequiredValueException("PROPUESTA ORIGEN");
        Propuesta p =
            propuestaRepository
                .findByIdCarga(idProp)
                .orElseThrow(() -> new NotFoundException("Propuesta", "IdHaya", idProp));

        Formalizacion f = formalizacionRepository.findByPropuestaId(p.getId());

        String terri = getRawStringValueConGuion(row, ci + 1);
        if (terri != null) f.setTerritorial(terri);

        String gesForLeg = getRawStringValueConGuion(row, ci + 2);
        if (gesForLeg != null) {
          Usuario c =
              usuarioRepository
                  .findByEmail(gesForLeg)
                  .orElseThrow(() -> new NotFoundException("Usuario", "Email", gesForLeg));
          if (c.getPerfil() != null && c.getPerfil().getNombre().equals("Soporte formalización")){
            Expediente e = p.getExpediente();
            AsignacionExpediente ae = new AsignacionExpediente();
            ae.setExpediente(e);
            ae.setUsuario(c);
            ae.setUsuarioAsignacion(user);
            asignacionExpedienteRepository.save(ae);
          }
        }

        String gesFormHaya = getRawStringValueConGuion(row, ci + 3);
        if (gesFormHaya != null) {
          Usuario c =
            usuarioRepository
              .findByEmail(gesFormHaya)
              .orElseThrow(() -> new NotFoundException("Usuario", "Email", gesFormHaya));
          Expediente e = p.getExpediente();
          AsignacionExpediente ae = new AsignacionExpediente();
          ae.setExpediente(e);
          ae.setUsuario(c);
          ae.setUsuarioAsignacion(user);
          asignacionExpedienteRepository.save(ae);
        }

        String gesRecHaya = getRawStringValueConGuion(row, ci + 4);
        if (gesRecHaya != null) {
          Usuario c =
            usuarioRepository
              .findByEmail(gesRecHaya)
              .orElseThrow(() -> new NotFoundException("Usuario", "Email", gesRecHaya));
          Expediente e = p.getExpediente();
          AsignacionExpediente ae = new AsignacionExpediente();
          ae.setExpediente(e);
          ae.setUsuario(c);
          ae.setUsuarioAsignacion(user);
          asignacionExpedienteRepository.save(ae);
        }

        String gestConc = getRawStringValueConGuion(row, ci + 5);
        if (gestConc != null) {
          GestorConcursos c =
              gestorConcursosRepository
                  .findByActivoIsTrueAndCodigo(gestConc)
                  .orElseThrow(() -> new NotFoundException("GestorConcursos", "Codigo", gestConc));
          f.setGestorConcursos(c);
        }

        String gesRecCli = getRawStringValueConGuion(row, ci + 6);
        if (gesRecCli != null) f.setGestorRecuperacionCliente(gesRecCli);

        String telGesRecCli = getRawStringValueConGuion(row, ci + 7);
        if (telGesRecCli != null) f.setTelefono1GesRecCli(telGesRecCli);

        String alaCli = getRawStringValueConGuion(row, ci + 8);
        if (alaCli != null) f.setAnalistaCliente(alaCli);

        String telAnaCli = getRawStringValueConGuion(row, ci + 9);
        if (telAnaCli != null) f.setTelefono1Cliente(telAnaCli);

        String gesForLetCli = getRawStringValueConGuion(row, ci + 10);
        if (gesForLetCli != null) f.setGestorFormalizacionCliente(gesForLetCli);

        String telGesFor = getRawStringValueConGuion(row, ci + 11);
        if (telGesFor != null) f.setTelefono1GesFormalizacionCliente(telGesFor);

        String gestoria = getRawStringValueConGuion(row, ci + 12);
        if (gestoria != null) f.setGestoriaTramitadora(gestoria);

        String gestGest = getRawStringValueConGuion(row, ci + 13);
        if (gestGest != null) {
          GestorGestoria c =
              gestorGestoriaRepository
                  .findByActivoIsTrueAndCodigo(gestGest)
                  .orElseThrow(() -> new NotFoundException("Gestor Gestoria", "Codigo", gestGest));
          f.setGestorGestoria(c);
        }

        String notario = getRawStringValueConGuion(row, ci + 14);
        if (notario != null) f.setNotario(notario);

        String dirNot = getRawStringValueConGuion(row, ci + 15);
        if (dirNot != null) f.setDireccionNotaria(dirNot);

        String proNot = getRawStringValueConGuion(row, ci + 16);
        if (proNot != null) {
          Provincia c =
              provinciaRepository
                  .findByActivoIsTrueAndCodigo(proNot)
                  .orElseThrow(() -> new NotFoundException("Provincia", "Codigo", proNot));
          f.setProvinciaNotaria(c);
        }

        String munNot = getRawStringValueConGuion(row, ci + 17);
        if (munNot != null) {
          Municipio c =
              municipioRepository
                  .findByActivoIsTrueAndCodigo(munNot)
                  .orElseThrow(() -> new NotFoundException("Municipio", "Codigo", munNot));
          f.setMunicipioNotaria(c);
        }

        String ofiNot = getRawStringValueConGuion(row, ci + 18);
        if (ofiNot != null) f.setOficinaNotaria(ofiNot);
        String telNot = getRawStringValueConGuion(row, ci + 19);
        if (telNot != null) f.setTelefonoNotaria(telNot);
        String telOfiNot = getRawStringValueConGuion(row, ci + 20);
        if (telOfiNot != null) f.setTelefonoOficialNotaria(telOfiNot);
        String emaOfiNot = getRawStringValueConGuion(row, ci + 21);
        if (emaOfiNot != null) f.setEmailOficialNotaria(emaOfiNot);
        String apodFirm = getRawStringValueConGuion(row, ci + 22);
        if (apodFirm != null) f.setApoderadoFirma(apodFirm);
        String conOfi = getRawStringValueConGuion(row, ci + 23);
        if (conOfi != null) f.setContactoOficinaCliente(conOfi);

        propuestas.add(p);
        formalizacionRepository.save(f);
      }
    }
    return propuestas;
  }

  private List<Propuesta> parseExcelToEntitySareb(XSSFSheet worksheet)
      throws RequiredValueException, NotFoundException {
    Integer ci = 1; // Columna Inicial
    List<Propuesta> propuestas = new ArrayList<>();

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      if (!checkIfRowIsEmpty(row)) {
        String idProp = getRawStringValueConGuion(row, ci);
        if (idProp == null) throw new RequiredValueException("PROPUESTA ORIGEN");
        Propuesta p =
            propuestaRepository
                .findByIdCarga(idProp)
                .orElseThrow(() -> new NotFoundException("Propuesta", "IdHaya", idProp));

        Formalizacion f = formalizacionRepository.findByPropuestaId(p.getId());

        Double nbv = getNumericValueConGuion(row, ci + 1);
        if (nbv != null) f.setNbv(nbv);

        Date fTP = getDateValueConGuion(row, ci + 2);
        if (fTP != null) f.setFechaTecleoEnPrisma(fTP);
        Date fEN = getDateValueConGuion(row, ci + 3);
        if (fEN != null) f.setFechaElevacionNILO(fEN);
        Date fCN = getDateValueConGuion(row, ci + 4);
        if (fCN != null) f.setFechaCierreNILO(fCN);

        propuestas.add(p);
        formalizacionRepository.save(f);
      }
    }
    return propuestas;
  }

  private List<Contrato> parseExcelToEntityContrato(XSSFSheet worksheet)
      throws NotFoundException, RequiredValueException, InvalidCodeException {
    Integer ci = 1; // Columna Inicial
    List<Contrato> contratos = new ArrayList<>();

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      if (!checkIfRowIsEmpty(row)) {
        String idContrato = getRawStringValueConGuion(row, ci);
        if (idContrato == null) throw new RequiredValueException("Nº PRESTAMO");
        Contrato c = contratoRepository.findByIdCarga(idContrato).orElse(null);
        if (c != null) {
          String error = "Ya existe un contrato con idCarga: " + idContrato;
          Locale loc = LocaleContextHolder.getLocale();
          String defaultLocal = loc.getLanguage();
          if (defaultLocal == null || defaultLocal.equals("en"))
            error = "There is already a contract with idCarga: " + idContrato;
          throw new InvalidCodeException(error);
        }
        c = new Contrato();
        c.setIdCarga(idContrato);
        c.setFechaCarga(new Date());

        FormalizacionContrato fc =
            formalizacionContratoRepository
                .findByContratoId(c.getId())
                .orElse(new FormalizacionContrato());
        if (fc.getContrato() == null) fc.setContrato(c);

        String estadoCod = getRawStringValueConGuion(row, ci + 1);
        if (estadoCod != null) {
          EstadoContrato ec =
              estadoContratoRepository
                  .findByActivoIsTrueAndCodigo(estadoCod)
                  .orElseThrow(() -> new NotFoundException("EstadoContrato", "Codigo", estadoCod));
          c.setEstadoContrato(ec);
        } else throw new RequiredValueException("estado");

        String prodCod = getRawStringValueConGuion(row, ci + 2);
        if (prodCod != null) {
          Producto p =
              productoRepository
                  .findByActivoIsTrueAndCodigo(prodCod)
                  .orElseThrow(() -> new NotFoundException("Producto", "Codigo", prodCod));
          c.setProducto(p);
        } else throw new RequiredValueException("producto");

        Boolean titulizado = getBooleanValue(row, ci + 3);
        if (titulizado != null) fc.setTitulizado(titulizado);

        Double saldoGestion = getNumericValueConGuion(row, ci + 4);
        if (saldoGestion != null) c.setSaldoGestion(saldoGestion);

        Double precioCompra = getNumericValueConGuion(row, ci + 5);
        if (precioCompra != null) fc.setPrecioCompra(precioCompra);

        Double importeDeuda = getNumericValueConGuion(row, ci + 6);
        if (importeDeuda != null) c.setDeudaImpagada(importeDeuda);
        else throw new RequiredValueException("IMPORTE DE LA DEUDA");

        Double quita = getNumericValueConGuion(row, ci + 7);
        if (quita != null) c.setRestoHipotecario(quita);

        Double importePrestamoInicial = getNumericValueConGuion(row, ci + 9);
        if (importePrestamoInicial != null) c.setImporteInicial(importePrestamoInicial);

        Double importeRetro = getNumericValueConGuion(row, ci + 10);
        if (importeRetro != null) fc.setImporteRetroactividad(importeRetro);

        Contrato cSaved = contratoRepository.save(c);

        Double restoDeuda = getNumericValueConGuion(row, ci + 8);
        if (restoDeuda == null) restoDeuda = 0.0;
        Saldo s = new Saldo();
        s.setCapitalPendiente(restoDeuda);
        s.setContrato(cSaved);
        s.setDia(new Date());
        s.setSaldoGestion(restoDeuda);
        saldoRepository.save(s);

        contratos.add(cSaved);
      }
    }
    return contratos;
  }

  private List<Bien> parseExcelToEntityBien(XSSFSheet worksheet)
      throws NotFoundException, RequiredValueException, InvalidCodeException {
    Integer ci = 1; // Columna Inicial
    List<Bien> bienes = new ArrayList<>();

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      if (!checkIfRowIsEmpty(row)) {
        String idContrato = getRawStringValueConGuion(row, ci);
        if (idContrato == null) throw new RequiredValueException("Nº PRESTAMO");
        Contrato c =
            contratoRepository
                .findByIdCarga(idContrato)
                .orElseThrow(() -> new NotFoundException("Contrato", "IdCarga", idContrato));

        String idBien = getRawStringValueConGuion(row, ci + 2);
        if (idBien == null) throw new RequiredValueException("ID_ORIGEN BIEN");
        Bien bien = bienRepository.findByIdCarga(idBien).orElse(null);
        if (bien != null) {
          String error = "Ya existe un bien con idCarga: " + idBien;
          Locale loc = LocaleContextHolder.getLocale();
          String defaultLocal = loc.getLanguage();
          if (defaultLocal == null || defaultLocal.equals("en"))
            error = "There is already a good with idCarga: " + idBien;
          throw new InvalidCodeException(error);
        }
        bien = new Bien();
        bien.setIdCarga(idBien);
        bien.setFechaCarga(new Date());

        ContratoBien cb = new ContratoBien();
        cb.setContrato(c);

        FormalizacionBien fb = new FormalizacionBien();

        String riesgoDetectadoCod = getRawStringValueConGuion(row, ci + 1);
        if (riesgoDetectadoCod != null) {
          RiesgosDetectados rd =
              riesgosDetectadosRepository
                  .findByActivoIsTrueAndCodigo(riesgoDetectadoCod)
                  .orElseThrow(
                      () ->
                          new NotFoundException("RiesgosDetectados", "Codigo", riesgoDetectadoCod));
          fb.setRiesgosDetectados(rd);
        }

        String nActivoHaya = getRawStringValueConGuion(row, ci + 3);
        if (nActivoHaya != null) bien.setIdHaya(nActivoHaya);

        Date fechaAlta = getDateValueConGuion(row, ci + 4);
        if (fechaAlta != null) bien.setFechaCarga(fechaAlta);

        Double responsabilidadHipotecaria = getNumericValueConGuion(row, ci + 5);
        if (responsabilidadHipotecaria != null) {
          bien.setResponsabilidadHipotecaria(responsabilidadHipotecaria);
          cb.setResponsabilidadHipotecaria(responsabilidadHipotecaria);
        }

        String tipoGarantiaCod = getRawStringValueConGuion(row, ci + 6);
        if (tipoGarantiaCod != null) {
          TipoGarantia tg =
              tipoGarantiaRepository
                  .findByActivoIsTrueAndCodigo(tipoGarantiaCod)
                  .orElseThrow(
                      () -> new NotFoundException("TipoGarantia", "Codigo", tipoGarantiaCod));
          cb.setTipoGarantia(tg);
        } else throw new RequiredValueException("TIPO GARANTIA");

        String tipoActivoCod = getRawStringValueConGuion(row, ci + 7);
        if (tipoActivoCod != null) {
          TipoActivo ta =
              tipoActivoRepository
                  .findByActivoIsTrueAndCodigo(tipoActivoCod)
                  .orElseThrow(() -> new NotFoundException("TipoActivo", "Codigo", tipoActivoCod));
          bien.setTipoActivo(ta);
        } else throw new RequiredValueException("TIPO ACTIVO");

        Boolean colateral = getBooleanValue(row, ci + 8);
        if (colateral != null) cb.setEstado(colateral);

        String tipoBienCod = getRawStringValueConGuion(row, ci + 9);
        if (tipoBienCod != null) {
          TipoBien tb =
              tipoBienRepository
                  .findByActivoIsTrueAndCodigo(tipoBienCod)
                  .orElseThrow(() -> new NotFoundException("TipoBien", "Codigo", tipoActivoCod));
          bien.setTipoBien(tb);
        } else throw new RequiredValueException("TIPOLOGIA");

        String subtipoBienCod = getRawStringValueConGuion(row, ci + 10);
        if (subtipoBienCod != null) {
          SubTipoBien stb =
              subTipoBienRepository
                  .findByActivoIsTrueAndCodigo(tipoBienCod)
                  .orElseThrow(
                      () -> new NotFoundException("SubTipoBien", "Codigo", subtipoBienCod));
          bien.setSubTipoBien(stb);
        } else throw new RequiredValueException("SUBTIPO ACTIVO");

        String idifur = getRawStringValueConGuion(row, ci + 11);
        if (idifur != null) bien.setIdufir(idifur);
        else throw new RequiredValueException("IDUFIR");

        String municipioCod = getRawStringValueConGuion(row, ci + 12);
        if (municipioCod != null) {
          Municipio mun =
              municipioRepository
                  .findByActivoIsTrueAndCodigo(municipioCod)
                  .orElseThrow(() -> new NotFoundException("Municipio", "Codigo", municipioCod));
          bien.setMunicipio(mun);
        }

        String provinciaCod = getRawStringValueConGuion(row, ci + 13);
        if (provinciaCod != null) {
          Provincia pro =
              provinciaRepository
                  .findByActivoIsTrueAndCodigo(provinciaCod)
                  .orElseThrow(() -> new NotFoundException("Provincia", "Codigo", provinciaCod));
          bien.setProvincia(pro);
        } else throw new RequiredValueException("PROVINCIA");

        String localidadCod = getRawStringValueConGuion(row, ci + 14);
        if (localidadCod != null) {
          Localidad loc =
              localidadRepository
                  .findByActivoIsTrueAndCodigo(localidadCod)
                  .orElseThrow(() -> new NotFoundException("Localidad", "Codigo", localidadCod));
          bien.setLocalidad(loc);
        } else throw new RequiredValueException("LOCALIDA");

        String finca = getRawStringValueConGuion(row, ci + 15);
        if (finca != null) bien.setFinca(finca);

        String registro = getRawStringValueConGuion(row, ci + 16);
        if (registro != null) bien.setRegistro(registro);

        String direccion = getRawStringValueConGuion(row, ci + 17);
        if (direccion != null) bien.setNombreVia(direccion);

        String referencia = getRawStringValueConGuion(row, ci + 18);
        if (referencia != null) bien.setReferenciaCatastral(referencia);

        Tasacion t = null;

        String tasadora = getRawStringValueConGuion(row, ci + 19);
        if (tasadora != null) {
          if (t == null) t = new Tasacion();
          t.setTasadora(tasadora);
        }

        Double valorTasacion = getNumericValueConGuion(row, ci + 20);
        if (valorTasacion != null) {
          if (t == null) t = new Tasacion();
          t.setImporte(valorTasacion);
        }

        Date fechaTasacion = getDateValueConGuion(row, ci + 21);
        if (fechaTasacion != null) {
          if (t == null) t = new Tasacion();
          t.setFecha(fechaTasacion);
        }

        String alquilerCod = getRawStringValueConGuion(row, ci + 22);
        if (alquilerCod != null) {
          Alquiler alq =
              alquilerRepository
                  .findByActivoIsTrueAndCodigo(alquilerCod)
                  .orElseThrow(() -> new NotFoundException("Alquiler", "Codigo", alquilerCod));
          fb.setAlquiler(alq);
        } else throw new RequiredValueException("ALQUILER");

        String estadoOcCod = getRawStringValueConGuion(row, ci + 23);
        if (estadoOcCod != null) {
          EstadoOcupacion estO =
              estadoOcupacionRepository
                  .findByActivoIsTrueAndCodigo(estadoOcCod)
                  .orElseThrow(
                      () -> new NotFoundException("EstadoOcupacion", "Codigo", estadoOcCod));
          bien.setEstadoOcupacion(estO);
        }

        String depFin = getRawStringValueConGuion(row, ci + 24);
        if (depFin != null) {
          DepositoFinanzas estO =
              depositoFinanzasRepository
                  .findByActivoIsTrueAndCodigo(depFin)
                  .orElseThrow(() -> new NotFoundException("DepositoFinanzas", "Codigo", depFin));
          fb.setDepositoFinanzas(estO);
        }

        Double deuOri_pri = getNumericValueConGuion(row, ci + 25);
        if (deuOri_pri != null) fb.setDeudaOriginal_principal(deuOri_pri);
        Double deuOri_iO = getNumericValueConGuion(row, ci + 26);
        if (deuOri_iO != null) fb.setDeudaOriginal_interesesOrdinarios(deuOri_iO);
        Double deuOri_iD = getNumericValueConGuion(row, ci + 27);
        if (deuOri_iD != null) fb.setDeudaOriginal_interesesDeDemora(deuOri_iD);

        Double resHip_pri = getNumericValueConGuion(row, ci + 28);
        if (resHip_pri != null) fb.setResponsabilidadHipotecaria_principal(resHip_pri);
        Double resHip_iO = getNumericValueConGuion(row, ci + 29);
        if (resHip_iO != null) fb.setResponsabilidadHipotecaria_interesesOrdinarios(resHip_iO);
        Double resHip_iD = getNumericValueConGuion(row, ci + 30);
        if (resHip_iD != null) fb.setResponsabilidadHipotecaria_interesesDeDemora(resHip_iD);

        Boolean riesgo = getBooleanValue(row, ci + 31);
        if (riesgo != null) fb.setRiesgoConsignacion(riesgo);

        Bien bienSaved = bienRepository.save(bien);
        bienes.add(bienSaved);
        if (t != null) {
          t.setBien(bienSaved);
          if (t.getImporte() == null) t.setImporte(0.0);
          tasacionRepository.save(t);
        }
        fb.setBien(bienSaved);
        formalizacionBienRepository.save(fb);
        cb.setBien(bienSaved);
        contratoBienRepository.save(cb);
      }
    }
    return bienes;
  }

  private List<Carga> parseExcelToEntityCarga(XSSFSheet worksheet)
      throws NotFoundException, RequiredValueException {
    Integer ci = 1; // Columna Inicial
    List<Carga> cargas = new ArrayList<>();

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      if (!checkIfRowIsEmpty(row)) {
        String idBien = getRawStringValueConGuion(row, ci);
        if (idBien == null) throw new RequiredValueException("ID_ORIGEN BIEN");
        List<String> bienes = new ArrayList<>();
        if (idBien.contains(",")) {
          bienes = Arrays.asList(idBien.split(","));
        } else bienes.add(idBien);

        Carga c = new Carga();

        for (String bienCod : bienes) {
          Bien b =
              bienRepository
                  .findByIdCarga(idBien)
                  .orElseThrow(() -> new NotFoundException("Bien", "IdCarga", bienCod));
          c.getBienes().add(b);
        }

        FormalizacionCarga fc = new FormalizacionCarga();

        String tipCarCod = getRawStringValueConGuion(row, ci + 1);
        if (tipCarCod != null) {
          TipoCarga tc =
              tipoCargaRepository
                  .findByActivoIsTrueAndCodigo(tipCarCod)
                  .orElseThrow(() -> new NotFoundException("TipoCarga", "Codigo", tipCarCod));
          c.setTipoCarga(tc);
        } else throw new RequiredValueException("TIPO CARGA");

        Double impCar = getNumericValueConGuion(row, ci + 2);
        if (impCar != null) c.setImporte(impCar);

        Boolean cargaProp = getBooleanValue(row, ci + 3);
        if (cargaProp != null) c.setCargaPropia(cargaProp);

        String tipForCod = getRawStringValueConGuion(row, ci + 4);
        if (tipForCod != null) {
          TipoCargaFormalizacion tc =
              tipoCargaFormalizacionRepository
                  .findByActivoIsTrueAndCodigo(tipCarCod)
                  .orElseThrow(
                      () -> new NotFoundException("TipoCargaFormalizacion", "Codigo", tipForCod));
          c.setTipoCargaFormalizacion(tc);
        }

        String stipForCod = getRawStringValueConGuion(row, ci + 5);
        if (stipForCod != null) {
          SubtipoCargaFormalizacion tc =
              subtipoCargaFormalizacionRepository
                  .findByActivoIsTrueAndCodigo(tipCarCod)
                  .orElseThrow(
                      () -> new NotFoundException("TipoCargaFormalizacion", "Codigo", stipForCod));
          c.setSubtipoCargaFormalizacion(tc);
        }

        Double deudaIbi = getNumericValueConGuion(row, ci + 6);
        if (deudaIbi != null) fc.setDeudaIBI(deudaIbi);
        Boolean certiIbi = getBooleanValue(row, ci + 7);
        if (certiIbi != null) fc.setCertificadoIBI(certiIbi);

        Double deudaCP = getNumericValueConGuion(row, ci + 8);
        if (deudaCP != null) fc.setDeudaCP(deudaCP);
        Boolean certiCP = getBooleanValue(row, ci + 9);
        if (certiCP != null) fc.setCertificadoCP(certiCP);

        String contactoCP = getRawStringValueConGuion(row, ci + 10);
        if (contactoCP != null) fc.setContactoCP(contactoCP);

        Double deudaUrba = getNumericValueConGuion(row, ci + 11);
        if (deudaUrba != null) fc.setDeudaUrbanizacion(deudaUrba);
        Boolean certiUrba = getBooleanValue(row, ci + 12);
        if (certiUrba != null) fc.setCertificadoUrbanizacion(certiUrba);

        Carga cSaved = cargaRepository.save(c);
        cargas.add(cSaved);
        fc.setCarga(cSaved);
        formalizacionCargaRepository.save(fc);
      }
    }
    return cargas;
  }

  private List<ContratoInterviniente> parseExcelToEntityInterviniente(XSSFSheet worksheet)
      throws NotFoundException, RequiredValueException, ForbiddenException {
    Integer ci = 1; // Columna Inicial
    List<ContratoInterviniente> intervinientes = new ArrayList<>();

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      if (!checkIfRowIsEmpty(row)) {
        String idContrato = getRawStringValueConGuion(row, ci);
        if (idContrato == null) throw new RequiredValueException("Nº PRESTAMO");
        List<String> contratos = new ArrayList<>();
        if (idContrato.contains(",")) {
          contratos = Arrays.asList(idContrato.split(","));
        } else contratos.add(idContrato);

        String inCod = getRawStringValueConGuion(row, ci + 1);
        if (inCod == null) throw new RequiredValueException("ID INTERVINIENTE ORIGEN");
        Interviniente in = intervinienteRepository.findByIdCarga(inCod).orElse(null);
        if (in == null) {
          in = new Interviniente();

          in.setIdCarga(inCod);
          in.setFechaCarga(new Date());

          Boolean estado = getBooleanValue(row, ci + 2);
          if (estado != null) in.setActivo(estado);
          else throw new RequiredValueException("estado");

          String razSoc = getRawStringValueConGuion(row, ci + 5);
          if (razSoc != null) {
            in.setRazonSocial(razSoc);

            in.setPersonaJuridica(true);
          } else {
            String nombre = getRawStringValueConGuion(row, ci + 3);
            if (nombre != null) in.setNombre(nombre);
            // else throw new RequiredValueException("nombre");

            String apellidos = getRawStringValueConGuion(row, ci + 4);
            if (apellidos != null) in.setApellidos(apellidos);
            // else throw new RequiredValueException("apellidos");

            in.setPersonaJuridica(false);
          }

          Date fechaNac = getDateValueConGuion(row, ci + 6);
          if (fechaNac != null) in.setFechaNacimiento(fechaNac);
          Date fechaCos = getDateValueConGuion(row, ci + 7);
          if (fechaCos != null) in.setFechaConstitucion(fechaCos);

          String tipoDoc = getRawStringValueConGuion(row, ci + 8);
          if (tipoDoc != null) {
            TipoDocumento td =
                tipoDocumentoRepository
                    .findByActivoIsTrueAndCodigo(tipoDoc)
                    .orElseThrow(() -> new NotFoundException("TipoDocumento", "Codigo", tipoDoc));
            in.setTipoDocumento(td);
          } else throw new RequiredValueException("Tipo Documento");

          String dni = getRawStringValueConGuion(row, ci + 9);
          if (dni != null) in.setNumeroDocumento(dni);
          // else throw new RequiredValueException("DNI/NIF");
        }

        Interviniente iSaved = intervinienteRepository.save(in);

        for (String cod : contratos) {
          Contrato c =
              contratoRepository
                  .findByIdCarga(cod)
                  .orElseThrow(() -> new NotFoundException("Contrato", "IdCarga", cod));

          ContratoInterviniente cI = new ContratoInterviniente();
          cI.setContrato(c);

          String tipoInt = getRawStringValueConGuion(row, ci + 10);
          if (tipoInt != null) {
            TipoIntervencion td =
                tipoIntervencionRepository
                    .findByActivoIsTrueAndCodigo(tipoInt)
                    .orElseThrow(
                        () -> new NotFoundException("TipoIntervencion", "Codigo", tipoInt));
            cI.setTipoIntervencion(td);
          } else throw new RequiredValueException("tipo_intervencion");

          Integer orInt = getIntegerValueConGuion(row, ci + 11);
          if (orInt != null) cI.setOrdenIntervencion(orInt);
          else throw new RequiredValueException("Tipo Documento");
          cI.setInterviniente(iSaved);
          ContratoInterviniente ciSaved = contratoIntervinienteRepository.save(cI);

          comprobarIntervinientes(cI, intervinientes, row.getRowNum());

          intervinientes.add(ciSaved);
        }
      }
    }
    return intervinientes;
  }

  private void comprobarIntervinientes(
      ContratoInterviniente ci, List<ContratoInterviniente> cis, Integer linea)
      throws ForbiddenException {
    Interviniente i = ci.getInterviniente();
    Contrato c = ci.getContrato();
    TipoIntervencion ti = ci.getTipoIntervencion();
    Integer oi = ci.getOrdenIntervencion();

    for (ContratoInterviniente ciT : cis) {
      if (c.getIdCarga().equals(ciT.getContrato().getIdCarga())) {

        if (i.getIdCarga().equals(ciT.getInterviniente().getIdCarga())) {
          String error =
              "Ya existe una relación entre el interviniente: "
                  + i.getIdCarga()
                  + " y el contrato: "
                  + c.getIdCarga()
                  + " que se duplica en la línea: "
                  + linea;
          Locale loc = LocaleContextHolder.getLocale();
          String defaultLocal = loc.getLanguage();
          if (defaultLocal == null || defaultLocal.equals("en"))
            error =
                "There is already a relationship between the participant: "
                    + i.getIdCarga()
                    + " and the contract: "
                    + c.getIdCarga()
                    + " which is duplicated in the line: "
                    + linea;

          throw new ForbiddenException(error);
        }

        if (!ti.getCodigo().equals(ciT.getTipoIntervencion().getCodigo())) continue;
        if (!oi.equals(ciT.getOrdenIntervencion())) continue;

        String error =
            "El contrato: "
                + c.getIdCarga()
                + " tiene un interviniente con Tipo Intervención: "
                + ti.getValor()
                + " y orden intervención: "
                + oi
                + " duplicada en la línea: "
                + linea;
        Locale loc = LocaleContextHolder.getLocale();
        String defaultLocal = loc.getLanguage();
        if (defaultLocal == null || defaultLocal.equals("en"))
          error =
              "The contract: "
                  + c.getIdCarga()
                  + " has a participant with Intervention Type: "
                  + ti.getValorIngles()
                  + " and intervention order: "
                  + oi
                  + " duplicated in the line: "
                  + linea;

        throw new ForbiddenException(error);
      }
    }
  }

  private List<DatoContacto> parseExcelToEntityContacto(XSSFSheet worksheet)
      throws NotFoundException, RequiredValueException {
    Integer ci = 1; // Columna Inicial
    List<DatoContacto> datoContactos = new ArrayList<>();

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      if (!checkIfRowIsEmpty(row)) {
        String idInt = getRawStringValueConGuion(row, ci);
        if (idInt == null) throw new RequiredValueException("ID INTERVINIENTE ORIGEN");
        Interviniente in =
            intervinienteRepository
                .findByIdCarga(idInt)
                .orElseThrow(() -> new NotFoundException("Interviniente", "IdCarga", idInt));

        DatoContacto dc = new DatoContacto();
        dc.setInterviniente(in);
        dc.setFechaAlta(new Date());
        dc.setFechaActualizacion(new Date());

        String valor = getRawStringValueConGuion(row, ci + 2);
        if (valor == null) throw new RequiredValueException("valor");

        Integer orden = getIntegerValueConGuion(row, ci + 3);
        if (orden == null) dc.setOrden(orden);
        else throw new RequiredValueException("orden");

        String tipoCon = getRawStringValueConGuion(row, ci + 1);
        if (tipoCon != null) {
          switch (tipoCon) {
            case "TEL_MOV":
              dc.setMovil(valor);
              break;
            case "TEL_FIJ":
              dc.setFijo(valor);
              break;
            case "MAIL":
              dc.setEmail(valor);
              break;
            default:
              break;
          }
        } else throw new RequiredValueException("tipo");

        DatoContacto dcSaved = datoContactoRepository.save(dc);
        datoContactos.add(dcSaved);
      }
    }
    return datoContactos;
  }

  private List<Procedimiento> parseExcelToEntityProcedimiento(XSSFSheet worksheet)
      throws NotFoundException, RequiredValueException {
    Integer ci = 1; // Columna Inicial
    List<Procedimiento> procedimientos = new ArrayList<>();

    for (int i = firstRow; i < worksheet.getPhysicalNumberOfRows(); i++) {
      XSSFRow row = worksheet.getRow(i);
      if (!checkIfRowIsEmpty(row)) {
        String idCont = getRawStringValueConGuion(row, ci);
        if (idCont == null) throw new RequiredValueException("Nº PRESTAMO");
        List<String> contratos = new ArrayList<>();
        if (idCont.contains(",")) {
          contratos = Arrays.asList(idCont.split(","));
        } else contratos.add(idCont);

        List<Contrato> conList = new ArrayList<>();
        for (String intCod : contratos) {
          Contrato c =
              contratoRepository
                  .findByIdCarga(idCont)
                  .orElseThrow(() -> new NotFoundException("Contrato", "IdCarga", intCod));
          conList.add(c);
        }

        String idInt = getRawStringValueConGuion(row, ci + 1);
        if (idInt == null) throw new RequiredValueException("ID INTERVINIENTE ORIGEN");
        List<String> intervinientes = new ArrayList<>();
        if (idInt.contains(",")) {
          intervinientes = Arrays.asList(idInt.split(","));
        } else intervinientes.add(idInt);

        List<Interviniente> intList = new ArrayList<>();
        for (String intCod : intervinientes) {
          Interviniente in =
              intervinienteRepository
                  .findByIdCarga(idInt)
                  .orElseThrow(() -> new NotFoundException("Interviniente", "IdCarga", intCod));
          intList.add(in);
        }

        FormalizacionProcedimiento fp = new FormalizacionProcedimiento();

        EstadoProcedimiento ep = null;
        String estadoCod = getRawStringValueConGuion(row, ci + 2);
        if (estadoCod != null) {
          ep =
              estadoProcedimientoRepository
                  .findByActivoIsTrueAndCodigo(estadoCod)
                  .orElseThrow(
                      () -> new NotFoundException("EstadoProcedimiento", "Codigo", estadoCod));
        } else throw new RequiredValueException("Estado");

        Boolean conc = getBooleanValue(row, ci + 4);
        Boolean liti = getBooleanValue(row, ci + 5);
        Boolean auto = getBooleanValue(row, ci + 6);
        Boolean test = getBooleanValue(row, ci + 7);

        String nAuto = getRawStringValueConGuion(row, ci + 8);
        if (nAuto == null) throw new RequiredValueException("Nº Auto");
        String nJuzg = getRawStringValueConGuion(row, ci + 9);
        String plaza = getRawStringValueConGuion(row, ci + 10);

        Boolean disp = getBooleanValue(row, ci + 11);

        String tiProCod = getRawStringValueConGuion(row, ci + 3);
        if (tiProCod == null) throw new RequiredValueException("Tipo Procedimiento");
        TipoProcedimiento tp =
            tipoProcedimientoRepository
                .findByActivoIsTrueAndCodigo(tiProCod)
                .orElseThrow(() -> new NotFoundException("TipoProcedimiento", "Codigo", tiProCod));
        switch (tiProCod) {
          case "HIP":
            ProcedimientoHipotecario pHip = new ProcedimientoHipotecario();
            pHip.getContratos().addAll(conList);
            pHip.getDemandados().addAll(intList);

            pHip.setTipo(tp);
            pHip.setEstado(ep);

            pHip.setNumeroAutos(nAuto);
            pHip.setJuzgado(nJuzg);
            pHip.setPlaza(plaza);

            ProcedimientoHipotecario pHipSaved = procedimientoHipotecarioRepository.save(pHip);
            procedimientos.add(pHipSaved);

            fp.setProcedimiento(pHipSaved);

            fp.setConcurso(conc);
            fp.setLitigio(liti);
            fp.setAuto(auto);
            fp.setTestimonio(test);
            fp.setDisponible(disp);

            formalizacionProcedimientoRepository.save(fp);
            break;

          case "ETNJ":
            ProcedimientoEtnj pEtnj = new ProcedimientoEtnj();
            pEtnj.getContratos().addAll(conList);
            pEtnj.getDemandados().addAll(intList);

            pEtnj.setTipo(tp);
            pEtnj.setEstado(ep);

            pEtnj.setNumeroAutos(nAuto);
            pEtnj.setJuzgado(nJuzg);
            pEtnj.setPlaza(plaza);

            ProcedimientoEtnj pEntjSaved = procedimientoEtnjRepository.save(pEtnj);
            procedimientos.add(pEntjSaved);

            fp.setProcedimiento(pEntjSaved);

            fp.setConcurso(conc);
            fp.setLitigio(liti);
            fp.setAuto(auto);
            fp.setTestimonio(test);
            fp.setDisponible(disp);

            formalizacionProcedimientoRepository.save(fp);
            break;

          case "EN":
            ProcedimientoEjecucionNotarial pEN = new ProcedimientoEjecucionNotarial();
            pEN.getContratos().addAll(conList);
            pEN.getDemandados().addAll(intList);

            pEN.setTipo(tp);
            pEN.setEstado(ep);

            pEN.setNumeroAutos(nAuto);
            pEN.setJuzgado(nJuzg);
            pEN.setPlaza(plaza);

            ProcedimientoEjecucionNotarial pENSaved =
                procedimientoEjecucionNotarialRepository.save(pEN);
            procedimientos.add(pENSaved);

            fp.setProcedimiento(pENSaved);

            fp.setConcurso(conc);
            fp.setLitigio(liti);
            fp.setAuto(auto);
            fp.setTestimonio(test);
            fp.setDisponible(disp);

            formalizacionProcedimientoRepository.save(fp);
            break;

          case "MON":
            ProcedimientoMonitorio pMon = new ProcedimientoMonitorio();
            pMon.getContratos().addAll(conList);
            pMon.getDemandados().addAll(intList);

            pMon.setTipo(tp);
            pMon.setEstado(ep);

            pMon.setNumeroAutos(nAuto);
            pMon.setJuzgado(nJuzg);
            pMon.setPlaza(plaza);

            ProcedimientoMonitorio pMonSaved = procedimientoMonitorioRepository.save(pMon);
            procedimientos.add(pMonSaved);

            fp.setProcedimiento(pMonSaved);

            fp.setConcurso(conc);
            fp.setLitigio(liti);
            fp.setAuto(auto);
            fp.setTestimonio(test);
            fp.setDisponible(disp);

            formalizacionProcedimientoRepository.save(fp);
            break;

          case "VERB":
            ProcedimientoVerbal pVer = new ProcedimientoVerbal();
            pVer.getContratos().addAll(conList);
            pVer.getDemandados().addAll(intList);

            pVer.setTipo(tp);
            pVer.setEstado(ep);

            pVer.setNumeroAutos(nAuto);
            pVer.setJuzgado(nJuzg);
            pVer.setPlaza(plaza);

            ProcedimientoVerbal pVerSaved = procedimientoVerbalRepository.save(pVer);
            procedimientos.add(pVerSaved);

            fp.setProcedimiento(pVerSaved);

            fp.setConcurso(conc);
            fp.setLitigio(liti);
            fp.setAuto(auto);
            fp.setTestimonio(test);
            fp.setDisponible(disp);

            formalizacionProcedimientoRepository.save(fp);
            break;

          case "ORD":
            ProcedimientoOrdinario pOrd = new ProcedimientoOrdinario();
            pOrd.getContratos().addAll(conList);
            pOrd.getDemandados().addAll(intList);

            pOrd.setTipo(tp);
            pOrd.setEstado(ep);

            pOrd.setNumeroAutos(nAuto);
            pOrd.setJuzgado(nJuzg);
            pOrd.setPlaza(plaza);

            ProcedimientoOrdinario pOrdSaved = procedimientoOrdinarioRepository.save(pOrd);
            procedimientos.add(pOrdSaved);

            fp.setProcedimiento(pOrdSaved);

            fp.setConcurso(conc);
            fp.setLitigio(liti);
            fp.setAuto(auto);
            fp.setTestimonio(test);
            fp.setDisponible(disp);

            formalizacionProcedimientoRepository.save(fp);
            break;

          case "ETJ":
            ProcedimientoEtj pEtj = new ProcedimientoEtj();
            pEtj.getContratos().addAll(conList);
            pEtj.getDemandados().addAll(intList);

            pEtj.setTipo(tp);
            pEtj.setEstado(ep);

            pEtj.setNumeroAutos(nAuto);
            pEtj.setJuzgado(nJuzg);
            pEtj.setPlaza(plaza);

            ProcedimientoEtj pEtjSaved = procedimientoEtjRepository.save(pEtj);
            procedimientos.add(pEtjSaved);

            fp.setProcedimiento(pEtjSaved);

            fp.setConcurso(conc);
            fp.setLitigio(liti);
            fp.setAuto(auto);
            fp.setTestimonio(test);
            fp.setDisponible(disp);

            formalizacionProcedimientoRepository.save(fp);
            break;

          case "CON":
            ProcedimientoConcursal pCon = new ProcedimientoConcursal();
            pCon.getContratos().addAll(conList);
            pCon.getDemandados().addAll(intList);

            pCon.setTipo(tp);
            pCon.setEstado(ep);

            pCon.setNumeroAutos(nAuto);
            pCon.setJuzgado(nJuzg);
            pCon.setPlaza(plaza);

            ProcedimientoConcursal pConSaved = procedimientoConcursalRepository.save(pCon);
            procedimientos.add(pConSaved);

            fp.setProcedimiento(pConSaved);

            fp.setConcurso(conc);
            fp.setLitigio(liti);
            fp.setAuto(auto);
            fp.setTestimonio(test);
            fp.setDisponible(disp);

            formalizacionProcedimientoRepository.save(fp);
            break;
        }
      }
    }
    return procedimientos;
  }

  private void agruparContratosPorCriterioClienteInter(
      Cliente cliente, Cartera cartera, List<ContratoInterviniente> inter) {
    TipoEstado estadoPendiente =
        tipoEstadoRepository.findByActivoIsTrueAndCodigo("1").orElse(null); // pendiente de gestión
    SubtipoEstado subEstadoPendiente =
        subtipoEstadoRepository
            .findByActivoIsTrueAndCodigo("11")
            .orElse(null); // pendiente de gestión

    Map<Interviniente, List<Contrato>> contratosOrdenados =
        new HashMap<Interviniente, List<Contrato>>();
    Map<Interviniente, List<Contrato>> contratosSinOrdenar =
        new HashMap<Interviniente, List<Contrato>>();

    for (ContratoInterviniente ci : inter) {
      List<Contrato> contratosPorInterviniente = new ArrayList<Contrato>();
      if (ci.getOrdenIntervencion() == 1 && ci.getTipoIntervencion().getValor().equals("TITULAR")) {
        contratosPorInterviniente.add(ci.getContrato());
        if (contratosOrdenados.containsKey(ci.getInterviniente())) {
          contratosOrdenados.get(ci.getInterviniente()).add(ci.getContrato());
        } else {
          contratosOrdenados.put(ci.getInterviniente(), contratosPorInterviniente);
        }
      } else {
        contratosPorInterviniente.add(ci.getContrato());
        if (contratosSinOrdenar.containsKey(ci.getInterviniente())) {
          contratosSinOrdenar.get(ci.getInterviniente()).add(ci.getContrato());
        } else {
          contratosSinOrdenar.put(ci.getInterviniente(), contratosPorInterviniente);
        }
      }
    }

    for (Interviniente i : contratosSinOrdenar.keySet()) {
      if (contratosOrdenados.containsKey(i)) {
        List<Contrato> contratosFinales = contratosSinOrdenar.get(i);
        contratosFinales.addAll(contratosOrdenados.get(i));
        contratosSinOrdenar.put(
            i, contratosFinales.stream().distinct().collect(Collectors.toList()));
      }
    }

    Set<Interviniente> intervinientes = contratosOrdenados.keySet();
    for (Interviniente interviniente : intervinientes) {
      Expediente expediente = new Expediente();

      expediente.setFechaCreacion(new Date());
      expediente.setCartera(cartera);
      expediente.setTipoEstado(estadoPendiente);
      expediente.setSubEstado(subEstadoPendiente);
      Expediente eSaved = expedienteRepository.save(expediente);
      if (interviniente.getIdCarga() != null) {
        String concatenado =
            expedienteUseCase.generarNoHeredado(
                cliente.getId(), cartera.getId(), interviniente.getIdCarga());
        eSaved.setIdConcatenado(concatenado);
      } else {
        String concatenado =
            expedienteUseCase.generarNoHeredado(
                cliente.getId(), cartera.getId(), eSaved.getId().toString());
        eSaved.setIdConcatenado(concatenado);
      }
      eSaved = expedienteRepository.save(eSaved);

      Contrato cRep = null;
      for (Contrato c : contratosOrdenados.get(interviniente)) {
        c.setExpediente(eSaved);
        c.setEsRepresentante(false);
        c.setCalculado(false);
        Contrato cSaved = contratoRepository.save(c);

        if (cRep == null) cRep = cSaved;
        else if (c.getSaldoGestion() > cRep.getSaldoGestion()) cRep = cSaved;
      }
      if (cRep != null) {
        cRep.setEsRepresentante(true);
        contratoRepository.save(cRep);
      }
    }
  }

  private void asignarPropuestas(Cliente cli, Cartera car, List<Propuesta> propuestas) {
    List<Propuesta> propuestasVacias = new ArrayList<>();
    for (Propuesta p : propuestas) {
      if (p.getExpediente() != null) continue;
      if (p.getContratos().isEmpty()) {
        propuestasVacias.add(p);
      } else {
        Contrato c = p.getContratos().stream().findFirst().orElse(null);
        FormalizacionContrato fc =
            formalizacionContratoRepository
                .findByContratoId(c.getId())
                .orElse(new FormalizacionContrato());
        if (fc.getPrecioCompra() != null) p.setImporteTotal(fc.getPrecioCompra());
        Expediente e = c.getExpediente();
        if (e == null) propuestasVacias.add(p);
        else p.setExpediente(e);
      }
    }
    propuestaRepository.saveAll(propuestas);
    List<Contrato> contratos = new ArrayList<>();
    for (Propuesta p : propuestas) contratos.addAll(p.getContratos());

    List<Contrato> cVacios =
        contratos.stream()
            .filter(c -> c.getExpediente() == null)
            .distinct()
            .collect(Collectors.toList());

    if (!propuestasVacias.isEmpty() || !cVacios.isEmpty()) {
      TipoEstado estadoPendiente =
          tipoEstadoRepository
              .findByActivoIsTrueAndCodigo("1")
              .orElse(null); // pendiente de gestión
      SubtipoEstado subEstadoPendiente =
          subtipoEstadoRepository
              .findByActivoIsTrueAndCodigo("11")
              .orElse(null); // pendiente de gestión

      Expediente e = new Expediente();
      e.setFechaCreacion(new Date());
      e.setCartera(car);
      e.setTipoEstado(estadoPendiente);
      e.setSubEstado(subEstadoPendiente);

      Expediente eSaved = expedienteRepository.save(e);
      String concatenado =
          expedienteUseCase.generarNoHeredado(cli.getId(), car.getId(), eSaved.getId().toString());
      eSaved.setIdConcatenado(concatenado);
      eSaved = expedienteRepository.save(eSaved);

      for (Propuesta p : propuestasVacias) p.setExpediente(eSaved);
      Boolean rep = true;
      for (Contrato c : cVacios) {
        c.setExpediente(eSaved);
        if (rep && c.getPrimerInterviniente() != null) {
          c.setEsRepresentante(rep);
          rep = false;
        }
      }

      if (rep && !cVacios.isEmpty()) {
        Contrato c = cVacios.stream().findFirst().orElse(null);
        c.setEsRepresentante(true);
      }

      propuestaRepository.saveAll(propuestasVacias);
      contratoRepository.saveAll(cVacios);
    }
  }
}
