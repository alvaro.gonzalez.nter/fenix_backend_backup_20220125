package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.formalizacion.domain.FormalizacionContrato;
import com.haya.alaska.saldo.domain.Saldo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionContratoOutputDTO implements Serializable {
  private Integer id;
  private String idCarga;
  private Boolean esRepresentante;

  private CatalogoMinInfoDTO estado; // EstadoContrato
  private CatalogoMinInfoDTO producto; // Producto
  private Boolean titulizado;
  private Double saldoGestion;
  private Double precioCompra;
  private Double importeDeuda;
  private Double quita;
  private Double restoDeudaReclamable;
  private Double importePrestamoInicial;
  private Double importeRetroactividad;

  private List<FormalizacionJudicialOutputDTO> proccedimientos = new ArrayList<>();
  private List<FormalizacionContratoBienOutputDTO> bienes = new ArrayList<>();

  public FormalizacionContratoOutputDTO(Contrato c, FormalizacionContrato fc){
    this.id = c.getId();
    this.idCarga = c.getIdCarga();
    this.esRepresentante = c.getEsRepresentante();

    this.estado = c.getEstadoContrato() != null ? new CatalogoMinInfoDTO(c.getEstadoContrato()) : null;
    this.producto = c.getProducto() != null ? new CatalogoMinInfoDTO(c.getProducto()) : null;
    this.titulizado = fc.getTitulizado();
    Saldo s = c.getUltimoSaldo();
    if (s != null){
      this.saldoGestion = s.getSaldoGestion();
      this.importeDeuda = s.getDeudaImpagada();
      this.restoDeudaReclamable = s.getCapitalPendiente();
    }
    this.quita = c.getRestoHipotecario();
    this.importePrestamoInicial = c.getImporteInicial();
    this.precioCompra = fc.getPrecioCompra();
    this.importeRetroactividad = fc.getImporteRetroactividad();

    for (ContratoBien cb : c.getBienes()){
      this.bienes.add(new FormalizacionContratoBienOutputDTO(cb));
    }
  }
}
