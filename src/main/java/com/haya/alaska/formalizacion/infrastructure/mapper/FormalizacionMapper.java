package com.haya.alaska.formalizacion.infrastructure.mapper;

import com.haya.alaska.alquiler.domain.Alquiler;
import com.haya.alaska.alquiler.infrastructure.repository.AlquilerRepository;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.infrastructure.repository.AsignacionExpedienteRepository;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.mapper.BienMapper;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.carga.infrastructure.repository.CargaRepository;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.clase_propuesta.domain.ClasePropuesta;
import com.haya.alaska.clase_propuesta.infrastructure.repository.ClasePropuestaRepository;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.comentario_propuesta.domain.ComentarioPropuesta;
import com.haya.alaska.comentario_propuesta.infrastructure.repository.ComentarioPropuestaRepository;
import com.haya.alaska.comprador.domain.Comprador;
import com.haya.alaska.comprador.infrastructure.repository.CompradorRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_bien.infrastructure.repository.ContratoBienRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.contrato_interviniente.infrastructure.repository.ContratoIntervinienteRepository;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.datos_check_list.domain.DatosCheckList;
import com.haya.alaska.datos_check_list.infrastructure.repository.DatosCheckListRepository;
import com.haya.alaska.deposito_finanzas.domain.DepositoFinanzas;
import com.haya.alaska.deposito_finanzas.infrastructure.repository.DepositoFinanzasRepository;
import com.haya.alaska.documentacion_validada.domain.DocumentacionValidada;
import com.haya.alaska.documentacion_validada.infrastructure.repository.DocumentacionValidadaRepository;
import com.haya.alaska.estado_checklist.domain.EstadoChecklist;
import com.haya.alaska.estado_checklist.infrastructure.repository.EstadoChecklistRepository;
import com.haya.alaska.estado_contrato.domain.EstadoContrato;
import com.haya.alaska.estado_contrato.infrastructure.repository.EstadoContratoRepository;
import com.haya.alaska.estado_ddl_ddt.domain.EstadoDdlDdt;
import com.haya.alaska.estado_ddl_ddt.infrastructure.repository.EstadoDdlDdtRepository;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion;
import com.haya.alaska.estado_ocupacion.infrastructure.repository.EstadoOcupacionRepository;
import com.haya.alaska.estado_procedimiento.domain.EstadoProcedimiento;
import com.haya.alaska.estado_procedimiento.infrastructure.repository.EstadoProcedimientoRepository;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estado_propuesta.infrastructure.repository.EstadoPropuestaRepository;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.expediente.application.ExpedienteUseCase;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.forma_de_pago.domain.FormaDePago;
import com.haya.alaska.forma_de_pago.infrastructure.repository.FormaDePagoRepository;
import com.haya.alaska.formalizacion.domain.*;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.CheckListInputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.CheckListOutputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.InformacionCheckListDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input.*;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output.*;
import com.haya.alaska.formalizacion.infrastructure.repository.*;
import com.haya.alaska.gestor_formalizacion.domain.GestorConcursos;
import com.haya.alaska.gestor_formalizacion.domain.GestorGestoria;
import com.haya.alaska.gestor_formalizacion.infrastructure.repository.GestorConcursosRepository;
import com.haya.alaska.gestor_formalizacion.infrastructure.repository.GestorGestoriaRepository;
import com.haya.alaska.informacion_check_list.domain.CheckListDouble;
import com.haya.alaska.informacion_check_list.domain.CheckListInteger;
import com.haya.alaska.informacion_check_list.domain.CheckListString;
import com.haya.alaska.informacion_check_list.domain.InformacionCheckList;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.localidad.infrastructure.repository.LocalidadRepository;
import com.haya.alaska.minuta_revisada_fiscal.domain.MinutaRevisadaFiscal;
import com.haya.alaska.minuta_revisada_fiscal.infrastructure.repository.MinutaRevisadaFiscalRepository;
import com.haya.alaska.motivo_recomendacion_no_firma.domain.MotivoRecomendacionNoFirma;
import com.haya.alaska.motivo_recomendacion_no_firma.infrastructure.repository.MotivoRecomendacionNoFirmaRepository;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.municipio.infrastructure.repository.MunicipioRepository;
import com.haya.alaska.nombres_check_list.domain.NombresCheckList;
import com.haya.alaska.nombres_check_list.infrastructure.repository.NombresCheckListRepository;
import com.haya.alaska.procedimiento.domain.*;
import com.haya.alaska.procedimiento.infrastructure.repository.*;
import com.haya.alaska.producto.domain.Producto;
import com.haya.alaska.producto.infrastructure.repository.ProductoRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.propuesta_bien.infrastructure.PropuestaBienRepository;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.provincia.infrastructure.repository.ProvinciaRepository;
import com.haya.alaska.recomendacion_no_firma.domain.RecomendacionNoFirma;
import com.haya.alaska.recomendacion_no_firma.infrastructure.repository.RecomendacionNoFirmaRepository;
import com.haya.alaska.riesgos_detectados.domain.RiesgosDetectados;
import com.haya.alaska.riesgos_detectados.infrastructutr.repository.RiesgosDetectadosRepository;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.saldo.infrastructure.repository.SaldoRepository;
import com.haya.alaska.sancion_propuesta.domain.SancionPropuesta;
import com.haya.alaska.sancion_propuesta.infrastructure.repository.SancionPropuestaRepository;
import com.haya.alaska.shared.dto.BienDTOToList;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien;
import com.haya.alaska.sub_tipo_bien.infrastructure.repository.SubTipoBienRepository;
import com.haya.alaska.subestado_formalizacion.domain.SubestadoFormalizacion;
import com.haya.alaska.subestado_formalizacion.infrastructure.repository.SubestadoFormalizacionRepository;
import com.haya.alaska.subtipo_carga_formalizacion.domain.SubtipoCargaFormalizacion;
import com.haya.alaska.subtipo_carga_formalizacion.infrastructure.repository.SubtipoCargaFormalizacionRepository;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import com.haya.alaska.subtipo_estado.infrastructure.repository.SubtipoEstadoRepository;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import com.haya.alaska.subtipo_propuesta.infrastructure.repository.SubtipoPropuestaRepository;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tasacion.infrastructure.repository.TasacionRepository;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_activo.infrastructure.repository.TipoActivoRepository;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_bien.infrastructure.repository.TipoBienRepository;
import com.haya.alaska.tipo_carga.domain.TipoCarga;
import com.haya.alaska.tipo_carga.infrastructure.repository.TipoCargaRepository;
import com.haya.alaska.tipo_carga_formalizacion.domain.TipoCargaFormalizacion;
import com.haya.alaska.tipo_carga_formalizacion.infrastructure.repository.TipoCargaFormalizacionRepository;
import com.haya.alaska.tipo_contacto.domain.TipoContacto;
import com.haya.alaska.tipo_contacto.infraestructure.repository.TipoContactoRepository;
import com.haya.alaska.tipo_documento.domain.TipoDocumento;
import com.haya.alaska.tipo_documento.infrastructure.repository.TipoDocumentoRepository;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import com.haya.alaska.tipo_estado.infrastructure.repository.TipoEstadoRepository;
import com.haya.alaska.tipo_garantia.domain.TipoGarantia;
import com.haya.alaska.tipo_garantia.infrastructure.repository.TipoGarantiaRepository;
import com.haya.alaska.tipo_intervencion.domain.TipoIntervencion;
import com.haya.alaska.tipo_intervencion.infrastructure.repository.TipoIntervencionRepository;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import com.haya.alaska.tipo_procedimiento.infrastructure.repository.TipoProcedimientoRepository;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.tipo_propuesta.infrastructure.repository.TipoPropuestaRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.haya.alaska.utilidades.comunicaciones.application.domain.PlantillaFormalizacion;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.ContactAttributes;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SendEmailDTO;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.client.dto.SubscriberAttributes;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.repository.PlantillaFormalizacionRepository;
import io.jsonwebtoken.lang.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class FormalizacionMapper {
  @Autowired UsuarioRepository usuarioRepository;
  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired AsignacionExpedienteRepository asignacionExpedienteRepository;
  @Autowired TipoEstadoRepository tipoEstadoRepository;
  @Autowired SubtipoEstadoRepository subtipoEstadoRepository;
  @Autowired ExpedienteUseCase expedienteUseCase;
  @Autowired ExpedienteUtil expedienteUtil;
  @Autowired EventoUseCase eventoUseCase;

  // Propuesta
  @Autowired PropuestaRepository propuestaRepository;
  @Autowired PropuestaBienRepository propuestaBienRepository;
  @Autowired ComentarioPropuestaRepository comentarioPropuestaRepository;
  @Autowired FormalizacionRepository formalizacionRepository;
  @Autowired ClasePropuestaRepository clasePropuestaRepository;
  @Autowired TipoPropuestaRepository tipoPropuestaRepository;
  @Autowired SubtipoPropuestaRepository subtipoPropuestaRepository;
  @Autowired EstadoPropuestaRepository estadoPropuestaRepository;
  @Autowired SubestadoFormalizacionRepository subestadoFormalizacionRepository;
  @Autowired SancionPropuestaRepository sancionPropuestaRepository;
  @Autowired RecomendacionNoFirmaRepository recomendacionNoFirmaRepository;
  @Autowired MotivoRecomendacionNoFirmaRepository motivoRecomendacionNoFirmaRepository;
  @Autowired CompradorRepository compradorRepository;
  @Autowired EstadoDdlDdtRepository estadoDdlDdtRepository;
  @Autowired GestorConcursosRepository gestorConcursosRepository;
  @Autowired GestorGestoriaRepository gestorGestoriaRepository;
  @Autowired MinutaRevisadaFiscalRepository minutaRevisadaFiscalRepository;
  @Autowired PlantillaFormalizacionRepository plantillaFormalizacionRepository;

  @Autowired private DatosCheckListRepository datosCheckListRepository;
  @Autowired private DocumentacionValidadaRepository documentacionValidadaRepository;
  @Autowired private NombresCheckListRepository nombresCheckListRepository;
  @Autowired private EstadoChecklistRepository estadoChecklistRepository;

  // Contratos
  @Autowired ContratoRepository contratoRepository;
  @Autowired SaldoRepository saldoRepository;
  @Autowired FormalizacionContratoRepository formalizacionContratoRepository;
  @Autowired EstadoContratoRepository estadoContratoRepository;
  @Autowired ProductoRepository productoRepository;
  @Autowired FormaDePagoRepository formaDePagoRepository;

  // Bienes
  @Autowired BienRepository bienRepository;
  @Autowired
  BienMapper bienMapper;
  @Autowired ContratoBienRepository contratoBienRepository;
  @Autowired FormalizacionBienRepository formalizacionBienRepository;
  @Autowired TasacionRepository tasacionRepository;
  @Autowired RiesgosDetectadosRepository riesgosDetectadosRepository;
  @Autowired TipoGarantiaRepository tipoGarantiaRepository;
  @Autowired TipoActivoRepository tipoActivoRepository;
  @Autowired TipoBienRepository tipoBienRepository;
  @Autowired SubTipoBienRepository subTipoBienRepository;
  @Autowired MunicipioRepository municipioRepository;
  @Autowired ProvinciaRepository provinciaRepository;
  @Autowired LocalidadRepository localidadRepository;
  @Autowired AlquilerRepository alquilerRepository;
  @Autowired EstadoOcupacionRepository estadoOcupacionRepository;
  @Autowired DepositoFinanzasRepository depositoFinanzasRepository;

  // Cargas
  @Autowired CargaRepository cargaRepository;
  @Autowired FormalizacionCargaRepository formalizacionCargaRepository;
  @Autowired TipoCargaRepository tipoCargaRepository;
  @Autowired TipoCargaFormalizacionRepository tipoCargaFormalizacionRepository;
  @Autowired SubtipoCargaFormalizacionRepository subtipoCargaFormalizacionRepository;

  // Intervinientes
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired ContratoIntervinienteRepository contratoIntervinienteRepository;
  @Autowired TipoDocumentoRepository tipoDocumentoRepository;
  @Autowired TipoIntervencionRepository tipoIntervencionRepository;

  // Contactos Intervinientes
  @Autowired DatoContactoRepository datoContactoRepository;
  @Autowired TipoContactoRepository tipoContactoRepository;

  // Procedimiento
  @Autowired FormalizacionProcedimientoRepository formalizacionProcedimientoRepository;
  @Autowired ProcedimientoHipotecarioRepository procedimientoHipotecarioRepository;
  @Autowired ProcedimientoEtnjRepository procedimientoEtnjRepository;
  @Autowired ProcedimientoEjecucionNotarialRepository procedimientoEjecucionNotarialRepository;
  @Autowired ProcedimientoMonitorioRepository procedimientoMonitorioRepository;
  @Autowired ProcedimientoVerbalRepository procedimientoVerbalRepository;
  @Autowired ProcedimientoOrdinarioRepository procedimientoOrdinarioRepository;
  @Autowired ProcedimientoEtjRepository procedimientoEtjRepository;
  @Autowired ProcedimientoConcursalRepository procedimientoConcursalRepository;
  @Autowired ProcedimientoRepository procedimientoRepository;
  @Autowired TipoProcedimientoRepository tipoProcedimientoRepository;
  @Autowired EstadoProcedimientoRepository estadoProcedimientoRepository;

  @Transactional(rollbackFor = Exception.class)
  public Propuesta parseImputToEntity(
      Cliente cli, Cartera car, FormalizacionCompletaInputDTO completa, Usuario user)
      throws RequiredValueException, NotFoundException, ForbiddenException {
    FormalizacionPropuestaInputDTO input = completa.getPropuesta();
    Integer id = input.getId();
    Propuesta p;
    Formalizacion f;

    List<Integer> contratos = new ArrayList<>();
    for (FormalizacionContratoInputDTO in : completa.getContratos()) {
      contratos.add(parseInputToEntity(in));
    }

    List<ContratoBien> bienes = new ArrayList<>();
    for (FormalizacionBienInputDTO in : completa.getBienes()) {
      bienes.addAll(parseInputToEntity(in));
    }
    for (FormalizacionCargaInputDTO in : completa.getCargas()) {
      parseInputToEntity(in);
    }

    List<Integer> intervinientes = new ArrayList<>();
    List<ContratoInterviniente> cos = new ArrayList<>();
    for (FormalizacionIntervinienteInputDTO in : completa.getIntervinientes()) {
      if (in.getId() == null) intervinientes.add(parseInputToEntity(in, cos));
    }
    for (FormalizacionContactoInputDTO in : completa.getContactos()) {
      if (in.getId() == null) parseInputToEntity(in);
    }

    List<Integer> judiciales = new ArrayList<>();
    for (FormalizacionJudicialInputDTO in : completa.getProcedimientos()) {
      judiciales.add(parseInputToEntity(in));
    }

    if (id == null) {
      p = new Propuesta();
      p.setFechaAlta(new Date());
      p.setFechaUltimoCambio(new Date());
      p.setImporteTotal(0.0);
      p.setImporteElevacion(0.0);
      p.setUsuario(user);
      if (input.getIdCarga() == null) throw new RequiredValueException("idCarga");
      p.setIdCarga(input.getIdCarga());
      f = new Formalizacion();
    } else {
      p =
          propuestaRepository
              .findById(id)
              .orElseThrow(() -> new NotFoundException("Propuesta", id));

      f = formalizacionRepository.findByPropuestaId(id);
      if (f == null) throw new NotFoundException("Formalizacion", "Propuesta", id);
    }
    parseAvanceDeGestion(p, f, input, user);
    parsePrincipal(f, input);
    parseDD(f, input);
    parseSarev(f, input);

    Propuesta pSaved = propuestaRepository.save(p);
    if (f.getPropuesta() == null) f.setPropuesta(pSaved);
    formalizacionRepository.save(f);

    String observ = input.getObservaciones();
    if (observ != null) f.setObservaciones(observ);

    String comen = input.getComentario();
    if (comen != null) {
      ComentarioPropuesta comP = new ComentarioPropuesta();
      comP.setFechaComentario(new Date());
      comP.setComentario(comen);
      comP.setUsuario(user);
      comP.setPropuesta(pSaved);
      comentarioPropuestaRepository.save(comP);
    }

    if (id == null) anadirRelaciones(pSaved, contratos, bienes, intervinientes);
    pSaved = propuestaRepository.save(pSaved);

    if (pSaved.getExpediente() == null && cli != null && car != null) {
      List<Propuesta> propuestas = new ArrayList<>();
      propuestas.add(pSaved);
      asignarPropuestas(cli, car, propuestas);
    }

    parseContactos(p, f, input, user);

    return propuestaRepository.save(pSaved);
  }

  private void asignarPropuestas(Cliente cli, Cartera car, List<Propuesta> propuestas) {
    List<Propuesta> propuestasVacias = new ArrayList<>();
    for (Propuesta p : propuestas) {
      if (p.getContratos().isEmpty()) {
        propuestasVacias.add(p);
      } else {
        Contrato c = p.getContratos().stream().findFirst().orElse(null);
        FormalizacionContrato fc =
            formalizacionContratoRepository
                .findByContratoId(c.getId())
                .orElse(new FormalizacionContrato());
        if (fc.getPrecioCompra() != null) p.setImporteTotal(fc.getPrecioCompra());
        Expediente e = c.getExpediente();
        if (e == null) propuestasVacias.add(p);
        else p.setExpediente(e);
      }
    }
    propuestaRepository.saveAll(propuestas);
    List<Contrato> contratos = new ArrayList<>();
    for (Propuesta p : propuestas) contratos.addAll(p.getContratos());

    List<Contrato> cVacios =
        contratos.stream()
            .filter(c -> c.getExpediente() == null)
            .distinct()
            .collect(Collectors.toList());

    if (!propuestasVacias.isEmpty() || !cVacios.isEmpty()) {
      TipoEstado estadoPendiente =
          tipoEstadoRepository
              .findByActivoIsTrueAndCodigo("1")
              .orElse(null); // pendiente de gestión
      SubtipoEstado subEstadoPendiente =
          subtipoEstadoRepository
              .findByActivoIsTrueAndCodigo("11")
              .orElse(null); // pendiente de gestión

      Expediente e = new Expediente();
      e.setFechaCreacion(new Date());
      e.setCartera(car);
      e.setTipoEstado(estadoPendiente);
      e.setSubEstado(subEstadoPendiente);

      Expediente eSaved = expedienteRepository.save(e);
      String concatenado =
          expedienteUseCase.generarNoHeredado(cli.getId(), car.getId(), eSaved.getId().toString());
      eSaved.setIdConcatenado(concatenado);
      eSaved = expedienteRepository.save(eSaved);

      for (Propuesta p : propuestasVacias){
        Formalizacion f = p.getFormalizaciones().stream().findFirst().orElse(new Formalizacion());
        if (f.getExpedienteOrigen() != null) {
          eSaved.setIdCarga(f.getExpedienteOrigen());
          eSaved = expedienteRepository.save(eSaved);
        }
        p.setExpediente(eSaved);
      }
      Boolean rep = true;
      for (Contrato c : cVacios) {
        c.setExpediente(eSaved);
        if (rep && c.getPrimerInterviniente() != null) {
          c.setEsRepresentante(rep);
          rep = false;
        }
      }

      if (rep && !cVacios.isEmpty()) {
        Contrato c = cVacios.stream().findFirst().orElse(null);
        c.setEsRepresentante(true);
      }

      propuestaRepository.saveAll(propuestasVacias);
      contratoRepository.saveAll(cVacios);
    }
  }

  private void anadirRelaciones(
      Propuesta p,
      List<Integer> contratos,
      List<ContratoBien> bienes,
      List<Integer> intervinientes) {
    List<Contrato> cs = contratoRepository.findAllById(contratos);
    List<Interviniente> is = intervinienteRepository.findAllById(intervinientes);

    p.setContratos(new HashSet<>(cs));
    p.setIntervinientes(new HashSet<>(is));

    List<Bien> bs = bienes.stream().map(ContratoBien::getBien).collect(Collectors.toList());

    for (Bien b : bs) {
      PropuestaBien pb = new PropuestaBien();
      pb.setPropuesta(p);
      pb.setBien(b);
      List<Contrato> contBien =
          bienes.stream()
              .filter(cb -> cb.getBien().getId().equals(b.getId()))
              .map(ContratoBien::getContrato)
              .collect(Collectors.toList());

      if (!contBien.isEmpty()) {
        Contrato c = contBien.stream().findFirst().orElse(null);
        pb.setContrato(c);
      }
      propuestaBienRepository.save(pb);
    }
  }

  private void parseAvanceDeGestion(
      Propuesta p, Formalizacion f, FormalizacionPropuestaInputDTO input, Usuario user)
      throws RequiredValueException, NotFoundException {

    if (input.getId() == null) {
      if (input.getClasePropuesta() != null) {
        ClasePropuesta cp =
            clasePropuestaRepository
                .findById(input.getClasePropuesta())
                .orElseThrow(
                    () -> new NotFoundException("ClasePropuesta", input.getClasePropuesta()));
        p.setClasePropuesta(cp);
      } else throw new RequiredValueException("Clase Propuesta");

      if (input.getTipoPropuesta() != null) {
        TipoPropuesta cp =
            tipoPropuestaRepository
                .findById(input.getTipoPropuesta())
                .orElseThrow(
                    () -> new NotFoundException("TipoPropuesta", input.getTipoPropuesta()));
        p.setTipoPropuesta(cp);
      } else throw new RequiredValueException("Tipo Propuesta");

      if (input.getSubtipoPropuesta() != null) {
        SubtipoPropuesta cp =
            subtipoPropuestaRepository
                .findById(input.getSubtipoPropuesta())
                .orElseThrow(
                    () -> new NotFoundException("SubtipoPropuesta", input.getSubtipoPropuesta()));
        p.setSubtipoPropuesta(cp);
      } else throw new RequiredValueException("Subtipo Propuesta");

      EstadoPropuesta ep =
          estadoPropuestaRepository
              .findByCodigo("PFI")
              .orElseThrow(() -> new NotFoundException("EstadoPropuesta", "Codigo", "PFI"));
      p.setEstadoPropuesta(ep);
      if (ep.getCodigo().equals("PFI"))
        eventoUseCase.alertaPteFirma(p, user);

      if (input.getTipoSancion() != null) {
        SancionPropuesta cp =
            sancionPropuestaRepository
                .findById(input.getTipoSancion())
                .orElseThrow(
                    () -> new NotFoundException("SancionPropuesta", input.getTipoSancion()));
        p.setSancionPropuesta(cp);
      }
    }

    SubestadoFormalizacion sf =
        subestadoFormalizacionRepository
            .findByCodigo("PFI_9")
            .orElseThrow(() -> new NotFoundException("SubestadoFormalizacion", "Codigo", "PFI_9"));
    f.setSubestadoFormalizacion(sf);

    Date fechaFirma = input.getFechaFirma();
    if (fechaFirma != null) f.setFechaFirma(fechaFirma);
    String horaFirma = input.getHoraFirma();
    if (horaFirma != null) {
      String[] tiempo = horaFirma.split(":");
      if (tiempo.length != 3) {
        throw new IllegalArgumentException("El formato de la Hora Firma debe ser: 'hh:mm:ss'");
      }
      Time hora = Time.valueOf(horaFirma);
      f.setHoraFirma(hora);
    }
    Date fechaFirmaEst = input.getFechaFirmaEstimada();
    if (fechaFirmaEst != null) f.setFechaFirmaEstimada(fechaFirmaEst);
    /*Boolean conFechHor = input.getConfirmadaFechaYHora();
    if (conFechHor != null) f.setConfirmadaFechaYHora(conFechHor);*/

    Date fechaEnt = input.getFechaEntrada();
    if (fechaEnt != null) f.setFechaEntrada(fechaEnt);
    Date fechaSan = input.getFechaSancion();
    if (fechaSan != null) f.setFechaSancion(fechaSan);

    Date vencSanc = input.getVencimientoSancion();
    if (vencSanc != null) f.setVencimientoSancion(vencSanc);

    /*Boolean docCom = input.getDocumentacionCompleta();
    if (docCom != null) f.setDocumentacionCompletada(docCom);*/

    Date fOKDocu = input.getFechaOKDocumentacion();
    if (fOKDocu != null) f.setFechaOkDocumentacion(fOKDocu);
    Date fKODocu = input.getFechaKODocumentacion();
    if (fKODocu != null) f.setFechaKoDocumentacion(fKODocu);

    Date soliMin = input.getSolicitudMinuta();
    if (soliMin != null) f.setSolicitudMinuta(soliMin);
    Date receMin = input.getRecepcionMinuta();
    if (receMin != null) f.setRecepcionMinuta(receMin);
    Date revMinHRE = input.getRevisionMinuta();
    if (revMinHRE != null) f.setRevisionMinutaHRE(revMinHRE);
    Date revEnvMinNot = input.getEnvioMinutaNotaria();
    if (revEnvMinNot != null) f.setFEnvioMinutaANotaria(revEnvMinNot);
    Date receNotNot = input.getRecepcionMinutaNotaria();
    if (receNotNot != null) f.setRecepcionMinutaDeNotaria(receNotNot);

    Date fEMCV = input.getEnvioMinutaCliente();
    if (fEMCV != null) f.setFEnvioMinutaAClienteFormalizacion(fEMCV);
    Date fOkMin = input.getOkMinuta();
    if (fOkMin != null) f.setFOkMinutaANotaria(fOkMin);

    Date solGAV = input.getSolicitudGestion();
    if (solGAV != null) f.setSolicitudGestionActivosVisita(solGAV);
    Boolean infVisOk = input.getInformeVisitaOk();
    if (infVisOk != null) f.setInformeVisitaOk(infVisOk);

    Date fechaComuni = input.getFechaComunicacion();
    if (fechaComuni != null) f.setFechaComunicacion(fechaComuni);
    Date fEntTF = input.getFechaEntradaTF();
    if (fEntTF != null) f.setFechaEntradaTF(fEntTF);
    Date fAvaTTF = input.getFechaAvanceTareaTF();
    if (fAvaTTF != null) f.setFechaAvanceTareaTF(fAvaTTF);

    Integer nProt = input.getNumeroProtocolo();
    if (nProt != null) f.setNumeroProtocolo(nProt);

    String apodFirm = input.getApoderadosFirma();
    if (apodFirm != null) f.setApoderadoFirma(apodFirm);

    if (input.getRecomendacionNoFirma() != null) {
      RecomendacionNoFirma cp =
          recomendacionNoFirmaRepository
              .findById(input.getRecomendacionNoFirma())
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "RecomendacionNoFirma", input.getRecomendacionNoFirma()));
      f.setRecomendacionNoFirma(cp);
    }

    if (input.getMotivoRecomendacionNoFirma() != null) {
      MotivoRecomendacionNoFirma cp =
          motivoRecomendacionNoFirmaRepository
              .findById(input.getMotivoRecomendacionNoFirma())
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "RecomendacionNoFirma", input.getMotivoRecomendacionNoFirma()));
      f.setMotivoRecomendacionNoFirma(cp);
    }

    /*if (input.getMinutaRevisadaFiscal() != null) {
      MinutaRevisadaFiscal cp =
          minutaRevisadaFiscalRepository
              .findById(input.getMinutaRevisadaFiscal())
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "MinutaRevisadaFiscal", input.getMinutaRevisadaFiscal()));
      f.setMinutaRevisadaFiscal(cp);
    }*/

    Date minutaRevisadaFiscal = input.getMinutaRevisadaFiscal();
    if (minutaRevisadaFiscal != null) f.setMinutaRevisadaFiscal(minutaRevisadaFiscal);

    Date fechaRevisionConcursos = input.getFechaRevisionConcursos();
    if (fechaRevisionConcursos != null) f.setFechaRevisionConcursos(fechaRevisionConcursos);
  }

  private void parsePrincipal(Formalizacion f, FormalizacionPropuestaInputDTO input)
      throws NotFoundException {

    String expedienteOrigen = input.getExpedienteOrigen();
    if (expedienteOrigen != null) f.setExpedienteOrigen(expedienteOrigen);

    Boolean dacSing = input.getDacionSingular();
    if (dacSing != null) f.setDacionSingular(dacSing);

    String gestoria = input.getGestoria();
    if (gestoria != null) f.setGestoriaTramitadora(gestoria);

    Integer tFFRR = input.getTotalFFRR();
    if (tFFRR != null) f.setTotalFFRR(tFFRR);

    String alerCom = input.getAlertaComunicacion();
    if (alerCom != null) f.setAlertaEnComunicacion(alerCom);

    if (input.getComprador() != null) {
      Comprador c =
          compradorRepository
              .findById(input.getComprador())
              .orElseThrow(() -> new NotFoundException("Comprador", input.getComprador()));
      f.setComprador(c);
    }

    Boolean penRatVen = input.getPenRatVen();
    if (penRatVen != null) f.setPendienteRatificacionVendedor(penRatVen);
    Boolean penRatCom = input.getPenRatCom();
    if (penRatCom != null) f.setPendienteRatificacionComprador(penRatCom);
    Boolean penRatBan = input.getPenRatBan();
    if (penRatBan != null) f.setPendienteRatificacionBanco(penRatBan);
    Boolean firmCon = input.getFirmCon();
    if (firmCon != null) f.setFirmaCondicionada(firmCon);

    Boolean pbc = input.getPbc();
    if (pbc != null) f.setPbc(pbc);
    Date fechaVigencia = input.getFechaVigencia();
    if (fechaVigencia != null) f.setFechaVigencia(fechaVigencia);

    Boolean embargoOtrosAsume = input.getEmbargoOtrosAsime();
    if (embargoOtrosAsume != null) f.setEmbargoOtrosAsume(embargoOtrosAsume);
    Double embargoOtrosImporte = input.getEmbargoOtrosImporte();
    if (embargoOtrosImporte != null) f.setEmbargoOtrosImporte(embargoOtrosImporte);

    String observacionesPrincipal = input.getObservacionesPrincipal();
    if (observacionesPrincipal != null) f.setObservacionesPrincipal(observacionesPrincipal);


    f.setGastosAsume(input.getGastosAsume());
    f.setGastosImporte(input.getGastosImporte());
    f.setPlusvaliaAsume(input.getPlusvaliaAsume());
    f.setPlusvaliaImporte(input.getPlusvaliaImporte());
    f.setIbiAsume(input.getIbiAsume());
    f.setIbiImporte(input.getIbiImporte());
    f.setCpAsume(input.getCpAsume());
    f.setCpImporte(input.getCpImporte());
    f.setUrbanizacionAsume(input.getUrbanizacionAsume());
    f.setUrbanizacionImporte(input.getUrbanizacionImporte());
    f.setImporteEntregaEfectivo(input.getImporteEntregaEfectivo());
    f.setEntregaDeBienesLibres(input.getEntregaBienesLibres());

    if (input.getFormaDePago() != null) {
      FormaDePago fdp =
        formaDePagoRepository
          .findById(input.getFormaDePago())
          .orElseThrow(() -> new NotFoundException("FormaDePago", input.getFormaDePago()));
      f.setFormaDePago(fdp);
    }
  }

  private void parseDD(Formalizacion f, FormalizacionPropuestaInputDTO input)
      throws NotFoundException {
    Date fEncDDL = input.getEncargoDDL();
    if (fEncDDL != null) f.setFechaEncargoDDL(fEncDDL);
    Date fRecDDL = input.getRecepcionDDL();
    if (fRecDDL != null) f.setFechaRecepcionDDL(fRecDDL);

    if (input.getEstadoDDL() != null) {
      EstadoDdlDdt c =
          estadoDdlDdtRepository
              .findById(input.getEstadoDDL())
              .orElseThrow(() -> new NotFoundException("EstadoDdlDdt", input.getEstadoDDL()));
      f.setEstadoDDL(c);
    }

    Date fEncDDT = input.getEncargoDDT();
    if (fEncDDT != null) f.setFechaEncargoDDT(fEncDDT);
    Date fRecDDT = input.getRecepcionDDT();
    if (fRecDDT != null) f.setFechaRecepcionDDT(fRecDDT);

    if (input.getEstadoDDT() != null) {
      EstadoDdlDdt c =
          estadoDdlDdtRepository
              .findById(input.getEstadoDDT())
              .orElseThrow(() -> new NotFoundException("EstadoDdlDdt", input.getEstadoDDT()));
      f.setEstadoDDT(c);
    }

    String despLeg = input.getDespachoLegal();
    if (despLeg != null) f.setDespachoLegal(despLeg);
    String despTec = input.getDespachoTecnico();
    if (despTec != null) f.setDespachoTecnico(despTec);

    /*Boolean ddRec = input.getDdRecibida();
    if (ddRec != null) f.setDdRecibida(ddRec);*/
    Boolean ddReSu = input.getDdRecibidaYSubida();
    if (ddReSu != null) f.setDdRevisadaYSubida(ddReSu);

    Date fSubDD = input.getSubidaDD();
    if (fSubDD != null) f.setFechaSubidaDD(fSubDD);

    Double honDD = input.getHonorariosDD();
    if (honDD != null) f.setHonorariosDD(honDD);
    Double honAF = input.getHonorariosAsistenciaFirma();
    if (honAF != null) f.setHonorariosAsistenciaFirma(honAF);

    String valoracionDDCliente = input.getValoracionDDCliente();
    if (valoracionDDCliente != null) f.setValoracionDDCliente(valoracionDDCliente);
    String horariosDDTecnica = input.getHorariosDDTecnica();
    if (horariosDDTecnica != null) f.setHorariosDDTecnica(horariosDDTecnica);
  }

  private void parseContactos(
      Propuesta p, Formalizacion f, FormalizacionPropuestaInputDTO input, Usuario user)
      throws NotFoundException {
    /*if (input.getId() == null) {
      Integer gesRecHaya = input.getGestorRecuperacionHaya();
      if (gesRecHaya != null) {
        Usuario c =
            usuarioRepository
                .findById(gesRecHaya)
                .orElseThrow(() -> new NotFoundException("Usuario", gesRecHaya));
        Expediente e = p.getExpediente();
        AsignacionExpediente ae = new AsignacionExpediente();
        ae.setExpediente(e);
        ae.setUsuario(c);
        ae.setUsuarioAsignacion(user);
        asignacionExpedienteRepository.save(ae);
      }

      if (input.getGestorFormalizacionPrincipal() != null) {
        Usuario c =
          usuarioRepository
            .findById(input.getGestorFormalizacionPrincipal())
            .orElseThrow(
              () -> new NotFoundException("Usuario", input.getGestorFormalizacionPrincipal()));
        Expediente e = p.getExpediente();
        AsignacionExpediente ae = new AsignacionExpediente();
        ae.setExpediente(e);
        ae.setUsuario(c);
        ae.setUsuarioAsignacion(user);
        asignacionExpedienteRepository.save(ae);
      }

      if (input.getGestorFormalizacionLegal() != null) {
        Usuario c =
          usuarioRepository
            .findById(input.getGestorFormalizacionLegal())
            .orElseThrow(
              () -> new NotFoundException("Usuario", input.getGestorFormalizacionLegal()));

        AsignacionExpediente ae = new AsignacionExpediente();
        Expediente e = p.getExpediente();
        ae.setExpediente(e);
        ae.setUsuario(c);
        ae.setUsuarioAsignacion(user);
        asignacionExpedienteRepository.save(ae);
      }
    }*/

    if (input.getGestorConcursos() != null) {
      GestorConcursos c =
          gestorConcursosRepository
              .findById(input.getGestorConcursos())
              .orElseThrow(
                  () -> new NotFoundException("GestorConcursos", input.getGestorConcursos()));
      f.setGestorConcursos(c);
    }

    if (input.getGestorGestoria() != null) {
      GestorGestoria c =
          gestorGestoriaRepository
              .findById(input.getGestorGestoria())
              .orElseThrow(() -> new NotFoundException("EstadoDdlDdt", input.getGestorGestoria()));
      f.setGestorGestoria(c);
    }

    String gesRecCli = input.getGestorRecuperacionCliente();
    if (gesRecCli != null) f.setGestorRecuperacionCliente(gesRecCli);
    String tel1GesRecCli = input.getTel1GestorRecuperacionCliente();
    if (tel1GesRecCli != null) f.setTelefono1GesRecCli(tel1GesRecCli);

    String terri = input.getTerritorial();
    if (terri != null) f.setTerritorial(terri);

    String alaCli = input.getAnalistaCliente();
    if (alaCli != null) f.setAnalistaCliente(alaCli);
    String tel1AlaCli = input.getTel1AnalistaCliente();
    if (tel1AlaCli != null) f.setTelefono1Cliente(tel1AlaCli);

    String gesForLetCli = input.getGestorFormalizacionLetradoCliente();
    if (gesForLetCli != null) f.setGestorFormalizacionCliente(gesForLetCli);
    String tel1GesForLetCli = input.getTel1GestorFormalizacionLetradoCliente();
    if (tel1GesForLetCli != null) f.setTelefono1GesFormalizacionCliente(tel1GesForLetCli);

    String notario = input.getNotario();
    if (notario != null) f.setNotario(notario);

    String dirNot = input.getDireccionNotaria();
    if (dirNot != null) f.setDireccionNotaria(dirNot);

    Integer proNot = input.getProvinciaNotaria();
    if (proNot != null) {
      Provincia c =
          provinciaRepository
              .findById(proNot)
              .orElseThrow(() -> new NotFoundException("Provincia", proNot));
      f.setProvinciaNotaria(c);
    }

    Integer munNot = input.getMunicipioNotaria();
    if (munNot != null) {
      Municipio c =
          municipioRepository
              .findById(munNot)
              .orElseThrow(() -> new NotFoundException("Municipio", munNot));
      f.setMunicipioNotaria(c);
    }

    String ofiNot = input.getOficialNotaria();
    if (ofiNot != null) f.setOficinaNotaria(ofiNot);
    String telNot = input.getTelefonoNotaria();
    if (telNot != null) f.setTelefonoNotaria(telNot);
    String telOfiNot = input.getTelefonoOficialNotaria();
    if (telOfiNot != null) f.setTelefonoOficialNotaria(telOfiNot);
    String emaOfiNot = input.getEmailOficialNotaria();
    if (emaOfiNot != null) f.setEmailOficialNotaria(emaOfiNot);
    String conOfi = input.getContactoOficina();
    if (conOfi != null) f.setContactoOficinaCliente(conOfi);
  }

  private void parseSarev(Formalizacion f, FormalizacionPropuestaInputDTO input) {

    Double nbv = input.getNbv();
    if (nbv != null) f.setNbv(nbv);

    Date fTP = input.getTecleoEnPrisma();
    if (fTP != null) f.setFechaTecleoEnPrisma(fTP);
    Date fEN = input.getElevacionNilo();
    if (fEN != null) f.setFechaElevacionNILO(fEN);
    Date fCN = input.getCierreNilo();
    if (fCN != null) f.setFechaCierreNILO(fCN);
  }

  public Integer parseInputToEntity(FormalizacionContratoInputDTO input)
      throws NotFoundException, RequiredValueException {
    Integer id = input.getId();
    Contrato c;
    FormalizacionContrato fc;
    if (id == null) {
      c = new Contrato();
      fc = new FormalizacionContrato();

      if (input.getIdCarga() == null) throw new RequiredValueException("idCarga");
      if (input.getImporteDeuda() == null) throw new RequiredValueException("importeDeuda");

      c.setIdCarga(input.getIdCarga());
      c.setSaldoGestion(input.getSaldoGestion());
      c.setDeudaImpagada(input.getImporteDeuda());
      c.setRestoHipotecario(input.getQuita());
      c.setImporteInicial(input.getImportePrestamoInicial());

      if (input.getEstado() != null) {
        EstadoContrato ec =
            estadoContratoRepository
                .findById(input.getEstado())
                .orElseThrow(() -> new NotFoundException("EstadoContrato", input.getEstado()));
        c.setEstadoContrato(ec);
      }

      if (input.getProducto() != null) {
        Producto pro =
            productoRepository
                .findById(input.getProducto())
                .orElseThrow(() -> new NotFoundException("Producto", input.getProducto()));
        c.setProducto(pro);
      }
    } else {
      c = contratoRepository.findById(id).orElseThrow(() -> new NotFoundException("Contrato", id));
      fc = formalizacionContratoRepository.findByContratoId(id).orElse(new FormalizacionContrato());
    }

    fc.setTitulizado(input.getTitulizado());
    fc.setPrecioCompra(input.getPrecioCompra());
    fc.setImporteRetroactividad(input.getImporteRetroactividad());

    Contrato cSaved = contratoRepository.save(c);
    if (fc.getContrato() == null) fc.setContrato(cSaved);
    formalizacionContratoRepository.save(fc);

    if (id == null) {
      Double restoDeuda = input.getRestoDeudaReclamable();
      if (restoDeuda == null) restoDeuda = 0.0;

      Saldo s = new Saldo();
      s.setCapitalPendiente(restoDeuda);
      s.setContrato(cSaved);
      s.setDia(new Date());
      s.setSaldoGestion(restoDeuda);
      saldoRepository.save(s);
    }
    return cSaved.getId();
  }

  private List<ContratoBien> parseInputToEntity(FormalizacionBienInputDTO input)
      throws RequiredValueException, NotFoundException {
    Integer id = input.getId();
    Bien b;
    FormalizacionBien fb;
    Tasacion t = null;
    if (input.getId() != null) {
      List<Tasacion> tasaciones = tasacionRepository.findAllByBienIdOrderByFechaDesc(input.getId());
      if (!tasaciones.isEmpty()) t = tasaciones.get(0);
    }
    if (id == null) {
      b = new Bien();
      fb = new FormalizacionBien();

      if (input.getIdCarga() == null) throw new RequiredValueException("idCarga");
      if (input.getTipoActivo() == null) throw new RequiredValueException("tipoActivo");

      b.setIdCarga(input.getIdCarga());
      b.setIdHaya(input.getIdHaya());
      b.setFechaCarga(input.getFechaAlta());
      b.setFechaCarga(new Date());
      b.setResponsabilidadHipotecaria(input.getResponsabilidadHipotecaria());
      b.setIdufir(input.getIdifur());
      b.setFinca(input.getFinca());
      b.setRegistro(input.getRegistro());
      b.setNombreVia(input.getDireccion());
      b.setReferenciaCatastral(input.getReferencia());

      if (input.getTipoActivo() != null) {
        TipoActivo ta =
            tipoActivoRepository
                .findById(input.getTipoActivo())
                .orElseThrow(() -> new NotFoundException("TipoActivo", input.getTipoActivo()));
        b.setTipoActivo(ta);
      } else throw new RequiredValueException("TipoActivo");

      if (input.getTipoBien() != null) {
        TipoBien tb =
            tipoBienRepository
                .findById(input.getTipoBien())
                .orElseThrow(() -> new NotFoundException("TipoBien", input.getTipoActivo()));
        b.setTipoBien(tb);
      } else throw new RequiredValueException("TipoBien");

      if (input.getSubtipoBien() != null) {
        SubTipoBien tb =
            subTipoBienRepository
                .findById(input.getSubtipoBien())
                .orElseThrow(() -> new NotFoundException("SubTipoBien", input.getSubtipoBien()));
        b.setSubTipoBien(tb);
      } else throw new RequiredValueException("SubtipoBien");

      if (input.getMunicipio() != null) {
        Municipio m =
            municipioRepository
                .findById(input.getMunicipio())
                .orElseThrow(() -> new NotFoundException("Municipio", input.getMunicipio()));
        b.setMunicipio(m);
      }

      if (input.getProvincia() != null) {
        Provincia p =
            provinciaRepository
                .findById(input.getProvincia())
                .orElseThrow(() -> new NotFoundException("Provincia", input.getProvincia()));
        b.setProvincia(p);
      }

      if (input.getLocalidad() != null) {
        Localidad l =
            localidadRepository
                .findById(input.getLocalidad())
                .orElseThrow(() -> new NotFoundException("Localidad", input.getLocalidad()));
        b.setLocalidad(l);
      }

      String tasadora = input.getTasadora();
      if (tasadora != null) {
        if (t == null) t = new Tasacion();
        t.setTasadora(tasadora);
      }

      Double valorTasacion = input.getValorTasacion();
      if (valorTasacion != null) {
        if (t == null) t = new Tasacion();
        t.setImporte(valorTasacion);
      }

      Date fechaTasacion = input.getFechaTasacion();
      if (fechaTasacion != null) {
        if (t == null) t = new Tasacion();
        t.setFecha(fechaTasacion);
      }

      if (input.getEstadoOcupacion() != null) {
        EstadoOcupacion eo =
            estadoOcupacionRepository
                .findById(input.getEstadoOcupacion())
                .orElseThrow(
                    () -> new NotFoundException("EstadoOcupacion", input.getEstadoOcupacion()));
        b.setEstadoOcupacion(eo);
      }
    } else {
      b = bienRepository.findById(id).orElseThrow(() -> new NotFoundException("Bien", id));
      fb = formalizacionBienRepository.findByBienId(id).orElse(new FormalizacionBien());
    }

    if (input.getDepositoFinanzas() != null) {
      DepositoFinanzas cp =
          depositoFinanzasRepository
              .findById(input.getDepositoFinanzas())
              .orElseThrow(
                  () -> new NotFoundException("MinutaRevisadaFiscal", input.getDepositoFinanzas()));
      fb.setDepositoFinanzas(cp);
    }

    fb.setDeudaOriginal_principal(input.getDeuOri_pri());
    fb.setDeudaOriginal_interesesOrdinarios(input.getDeuOri_iO());
    fb.setDeudaOriginal_interesesDeDemora(input.getDeuOri_iD());
    fb.setResponsabilidadHipotecaria_principal(input.getResHip_pri());
    fb.setResponsabilidadHipotecaria_interesesOrdinarios(input.getResHip_iO());
    fb.setResponsabilidadHipotecaria_interesesDeDemora(input.getResHip_iD());
    fb.setRiesgoConsignacion(input.getRiesgo());

    if (input.getRiesgoDetectado() != null) {
      RiesgosDetectados rd =
          riesgosDetectadosRepository
              .findById(input.getRiesgoDetectado())
              .orElseThrow(
                  () -> new NotFoundException("RiesgosDetectados", input.getRiesgoDetectado()));
      fb.setRiesgosDetectados(rd);
    }

    if (input.getAlquiler() != null) {
      Alquiler a =
          alquilerRepository
              .findById(input.getAlquiler())
              .orElseThrow(() -> new NotFoundException("Alquiler", input.getAlquiler()));
      fb.setAlquiler(a);
    }

    TipoGarantia tg = null;
    if (input.getTipoGarantia() != null) {
      tg =
          tipoGarantiaRepository
              .findById(input.getTipoGarantia())
              .orElseThrow(() -> new NotFoundException("TipoGarantia", input.getTipoGarantia()));
    }

    Bien bSaved = bienRepository.save(b);
    if (fb.getBien() == null) fb.setBien(bSaved);
    formalizacionBienRepository.save(fb);

    if (t != null) {
      t.setBien(bSaved);
      if (t.getImporte() == null) t.setImporte(0.0);
      tasacionRepository.save(t);
    }

    List<ContratoBien> result = new ArrayList<>();

    if (id == null) {
      for (String conCod : input.getContratos()) {
        Contrato c =
            contratoRepository
                .findByIdCarga(conCod)
                .orElseThrow(() -> new NotFoundException("Contrato", "Codigo", conCod));
        ContratoBien cb = b.getContratoBien(c.getId());
        if (cb == null) {
          cb = new ContratoBien();
          cb.setBien(bSaved);
          cb.setContrato(c);
        }

        cb.setTipoGarantia(tg);
        cb.setResponsabilidadHipotecaria(input.getResponsabilidadHipotecaria());
        cb.setEstado(input.getColateral());
        if (cb.getEstado() == null) cb.setEstado(true);
        result.add(contratoBienRepository.save(cb));
      }
    }
    return result;
  }

  private Integer parseInputToEntity(FormalizacionCargaInputDTO input) throws NotFoundException {
    Integer id = input.getId();
    Carga c;
    FormalizacionCarga fc;
    if (id == null) {
      c = new Carga();
      fc = new FormalizacionCarga();

      c.setFechaAnotacion(new Date());
      c.setImporte(input.getImporteCarga());
      c.setCargaPropia(input.getCargaPropia());

      if (input.getTipoCarga() != null) {
        TipoCarga rd =
            tipoCargaRepository
                .findById(input.getTipoCarga())
                .orElseThrow(() -> new NotFoundException("TipoCarga", input.getTipoCarga()));
        c.setTipoCarga(rd);
      }

      if (input.getTipoCargaFormalizacion() != null) {
        TipoCargaFormalizacion rd =
            tipoCargaFormalizacionRepository
                .findById(input.getTipoCargaFormalizacion())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "TipoCargaFormalizacion", input.getTipoCargaFormalizacion()));
        c.setTipoCargaFormalizacion(rd);
      }

      if (input.getSubtipoCargaFormalizacion() != null) {
        SubtipoCargaFormalizacion rd =
            subtipoCargaFormalizacionRepository
                .findById(input.getSubtipoCargaFormalizacion())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "TipoCargaFormalizacion", input.getSubtipoCargaFormalizacion()));
        c.setSubtipoCargaFormalizacion(rd);
      }
    } else {
      c = cargaRepository.findById(id).orElseThrow(() -> new NotFoundException("Carga", id));
      fc = formalizacionCargaRepository.findByCargaId(id).orElse(new FormalizacionCarga());
    }

    fc.setDeudaIBI(input.getDeudaIbi());
    fc.setCertificadoIBI(input.getCertificadoIbi());
    fc.setDeudaCP(input.getDeudaCp());
    fc.setCertificadoCP(input.getCertificadoCp());
    fc.setContactoCP(input.getContactoCp());
    fc.setDeudaUrbanizacion(input.getDeudaUrbanizacion());
    fc.setCertificadoUrbanizacion(input.getCertificadoUrbanizacion());

    if (id == null) {
      List<Bien> bienes = new ArrayList<>();
      if (input.getBienes() != null)
        for (String conCod : input.getBienes()) {
          Bien b =
              bienRepository
                  .findByIdCarga(conCod)
                  .orElseThrow(() -> new NotFoundException("Bien", "Codigo", conCod));

          bienes.add(b);
        }
      if (input.getBienesIds() != null)
        for (Integer idB : input.getBienesIds()) {
          Bien b =
              bienRepository.findById(idB).orElseThrow(() -> new NotFoundException("Bien", idB));

          bienes.add(b);
        }
      c.setBienes(new HashSet<>(bienes.stream().distinct().collect(Collectors.toList())));
    }

    Carga cSaved = cargaRepository.save(c);
    if (fc.getCarga() == null) fc.setCarga(cSaved);
    formalizacionCargaRepository.save(fc);
    return cSaved.getId();
  }

  private Integer parseInputToEntity(
      FormalizacionIntervinienteInputDTO input,
      List<ContratoInterviniente> cis) // no entrar si el id es != null
      throws NotFoundException, RequiredValueException, ForbiddenException {
    Interviniente i = new Interviniente();

    if (input.getIdCarga() == null) throw new RequiredValueException("idCarga");
    i.setIdCarga(input.getIdCarga());
    if (input.getEstado() != null) i.setActivo(input.getEstado());

    if (input.getRazonSocial() != null) {
      i.setRazonSocial(input.getRazonSocial());

      i.setPersonaJuridica(true);
    } else {
      i.setNombre(input.getNombre());
      i.setApellidos(input.getApellidos());

      i.setPersonaJuridica(false);
    }

    i.setFechaNacimiento(input.getFechaNacimiento());
    i.setFechaConstitucion(input.getFechaConstitucion());
    i.setNumeroDocumento(input.getDni_cif());

    if (input.getTipoDocumento() != null) {
      TipoDocumento td =
          tipoDocumentoRepository
              .findById(input.getTipoDocumento())
              .orElseThrow(() -> new NotFoundException("TipoDocumento", input.getTipoDocumento()));
      i.setTipoDocumento(td);
    }

    Interviniente iSaved = intervinienteRepository.save(i);

    for (FormalizacionContratoIntervinienteInputDTO conObj : input.getContratos()) {
      TipoIntervencion ti = null;
      if (conObj.getTipoIntervencion() != null) {
        ti =
            tipoIntervencionRepository
                .findById(conObj.getTipoIntervencion())
                .orElseThrow(
                    () -> new NotFoundException("TipoDocumento", conObj.getTipoIntervencion()));
      }

      if (conObj.getTipoIntervencion() == null)
        throw new RequiredValueException("tipoIntervencion");
      if (conObj.getOrdenIntervencion() == null)
        throw new RequiredValueException("ordenIntervencion");
      if (conObj.getId() == null) throw new RequiredValueException("id");
      Contrato c =
          contratoRepository
              .findByIdCarga(conObj.getId())
              .orElseThrow(() -> new NotFoundException("Contrato", "Codigo", conObj.getId()));
      ContratoInterviniente ci = i.getContratoInterviniente(c.getId());
      if (ci == null) {
        ci = new ContratoInterviniente();
        ci.setInterviniente(iSaved);
        ci.setContrato(c);
      }

      ci.setTipoIntervencion(ti);
      ci.setOrdenIntervencion(conObj.getOrdenIntervencion());
      ContratoInterviniente ciSaved = contratoIntervinienteRepository.save(ci);
      comprobarIntervinientes(ciSaved, cis);
      cis.add(ciSaved);
    }
    return iSaved.getId();
  }

  private void comprobarIntervinientes(ContratoInterviniente ci, List<ContratoInterviniente> cis)
      throws ForbiddenException {
    Interviniente i = ci.getInterviniente();
    Contrato c = ci.getContrato();
    TipoIntervencion ti = ci.getTipoIntervencion();
    Integer oi = ci.getOrdenIntervencion();

    for (ContratoInterviniente ciT : cis) {
      if (c.getIdCarga().equals(ciT.getContrato().getIdCarga())) {

        if (i.getIdCarga().equals(ciT.getInterviniente().getIdCarga())) {
          String error =
              "Ya existe una relación entre el interviniente: "
                  + i.getIdCarga()
                  + " y el contrato: "
                  + c.getIdCarga();
          Locale loc = LocaleContextHolder.getLocale();
          String defaultLocal = loc.getLanguage();
          if (defaultLocal == null || defaultLocal.equals("en"))
            error =
                "There is already a relationship between the participant: "
                    + i.getIdCarga()
                    + " and the contract: "
                    + c.getIdCarga();

          throw new ForbiddenException(error);
        }

        if (!ti.getCodigo().equals(ciT.getTipoIntervencion().getCodigo())) continue;
        if (!oi.equals(ciT.getOrdenIntervencion())) continue;

        String error =
            "El contrato: "
                + c.getIdCarga()
                + " tiene un interviniente con Tipo Intervención: "
                + ti.getValor()
                + " y orden intervención: "
                + oi
                + " duplicada.";
        Locale loc = LocaleContextHolder.getLocale();
        String defaultLocal = loc.getLanguage();
        if (defaultLocal == null || defaultLocal.equals("en"))
          error =
              "The contract: "
                  + c.getIdCarga()
                  + " has a participant with Intervention Type: "
                  + ti.getValorIngles()
                  + " and intervention order: "
                  + oi
                  + " duplicated.";

        throw new ForbiddenException(error);
      }
    }
  }

  private void parseInputToEntity(
      FormalizacionContactoInputDTO input) // no entrar si el id es != null
      throws NotFoundException, RequiredValueException {
    DatoContacto dc = new DatoContacto();

    if (input.getIdInterviniente() == null) throw new RequiredValueException("idInterviniente");
    Interviniente i =
        intervinienteRepository
            .findByIdCarga(input.getIdInterviniente())
            .orElseThrow(
                () -> new NotFoundException("Interviniente", "Codigo", input.getIdInterviniente()));

    dc.setInterviniente(i);
    dc.setFechaAlta(new Date());
    dc.setFechaActualizacion(new Date());
    dc.setOrden(input.getOrden());

    TipoContacto tc =
        tipoContactoRepository
            .findById(input.getTipo())
            .orElseThrow(() -> new NotFoundException("TipoContacto", input.getTipo()));

    String valor = input.getValor();

    String tipoCon = tc.getCodigo();
    if (tipoCon != null) {
      switch (tipoCon) {
        case "TEL_MOV":
          dc.setMovil(valor);
          break;
        case "TEL_FIJ":
          dc.setFijo(valor);
          break;
        case "MAIL":
          dc.setEmail(valor);
          break;
        default:
          break;
      }
    }

    datoContactoRepository.save(dc);
  }

  private Integer parseInputToEntity(FormalizacionJudicialInputDTO input)
      throws RequiredValueException, NotFoundException {
    Integer id = input.getId();
    Integer idFinal = id;
    FormalizacionProcedimiento fp;
    List<Contrato> conList = new ArrayList<>();
    List<Interviniente> intList = new ArrayList<>();

    if (input.getContratos() != null) {
      for (String cod : input.getContratos()) {
        Contrato c =
            contratoRepository
                .findByIdCarga(cod)
                .orElseThrow(() -> new NotFoundException("Contrato", "Codigo", cod));
        conList.add(c);
      }
    }

    if (input.getIntervinientes() != null) {
      for (String cod : input.getIntervinientes()) {
        Interviniente i =
            intervinienteRepository
                .findByIdCarga(cod)
                .orElseThrow(() -> new NotFoundException("Interviniente", "Codigo", cod));
        intList.add(i);
      }
    }

    EstadoProcedimiento ep = null;
    if (input.getEstadoProcedimiento() != null) {
      ep =
          estadoProcedimientoRepository
              .findById(input.getEstadoProcedimiento())
              .orElseThrow(
                  () ->
                      new NotFoundException("EstadoProcedimiento", input.getEstadoProcedimiento()));
    } else throw new RequiredValueException("EstadoProcedimiento");

    Boolean conc = input.getConcurso();
    Boolean liti = input.getLitigio();
    Boolean auto = input.getAuto();
    Boolean test = input.getTestimonio();

    String nAuto = input.getNAuto();
    if (nAuto == null) throw new RequiredValueException("Nº Auto");
    String nJuzg = input.getNJuzgado();
    String plaza = input.getPlaza();

    Boolean disp = input.getDispone();

    if (id == null) {
      fp = new FormalizacionProcedimiento();
      String tiProCod;
      if (input.getTipoProcedimiento() == null)
        throw new RequiredValueException("Tipo Procedimiento");
      TipoProcedimiento tp =
          tipoProcedimientoRepository
              .findById(input.getTipoProcedimiento())
              .orElseThrow(
                  () -> new NotFoundException("TipoProcedimiento", input.getTipoProcedimiento()));
      tiProCod = tp.getCodigo();
      switch (tiProCod) {
        case "HIP":
          ProcedimientoHipotecario pHip = new ProcedimientoHipotecario();
          pHip.getContratos().addAll(conList);
          pHip.getDemandados().addAll(intList);

          pHip.setTipo(tp);
          pHip.setEstado(ep);

          pHip.setNumeroAutos(nAuto);
          pHip.setJuzgado(nJuzg);
          pHip.setPlaza(plaza);

          ProcedimientoHipotecario pHipSaved = procedimientoHipotecarioRepository.save(pHip);
          fp.setProcedimiento(pHipSaved);
          idFinal = pHipSaved.getId();

          break;

        case "ETNJ":
          ProcedimientoEtnj pEtnj = new ProcedimientoEtnj();
          pEtnj.getContratos().addAll(conList);
          pEtnj.getDemandados().addAll(intList);

          pEtnj.setTipo(tp);
          pEtnj.setEstado(ep);

          pEtnj.setNumeroAutos(nAuto);
          pEtnj.setJuzgado(nJuzg);
          pEtnj.setPlaza(plaza);

          ProcedimientoEtnj pEntjSaved = procedimientoEtnjRepository.save(pEtnj);
          fp.setProcedimiento(pEntjSaved);
          idFinal = pEntjSaved.getId();

          break;

        case "EN":
          ProcedimientoEjecucionNotarial pEN = new ProcedimientoEjecucionNotarial();
          pEN.getContratos().addAll(conList);
          pEN.getDemandados().addAll(intList);

          pEN.setTipo(tp);
          pEN.setEstado(ep);

          pEN.setNumeroAutos(nAuto);
          pEN.setJuzgado(nJuzg);
          pEN.setPlaza(plaza);

          ProcedimientoEjecucionNotarial pENSaved =
              procedimientoEjecucionNotarialRepository.save(pEN);
          fp.setProcedimiento(pENSaved);
          idFinal = pENSaved.getId();

          break;

        case "MON":
          ProcedimientoMonitorio pMon = new ProcedimientoMonitorio();
          pMon.getContratos().addAll(conList);
          pMon.getDemandados().addAll(intList);

          pMon.setTipo(tp);
          pMon.setEstado(ep);

          pMon.setNumeroAutos(nAuto);
          pMon.setJuzgado(nJuzg);
          pMon.setPlaza(plaza);

          ProcedimientoMonitorio pMonSaved = procedimientoMonitorioRepository.save(pMon);
          fp.setProcedimiento(pMonSaved);
          idFinal = pMonSaved.getId();

          break;

        case "VERB":
          ProcedimientoVerbal pVer = new ProcedimientoVerbal();
          pVer.getContratos().addAll(conList);
          pVer.getDemandados().addAll(intList);

          pVer.setTipo(tp);
          pVer.setEstado(ep);

          pVer.setNumeroAutos(nAuto);
          pVer.setJuzgado(nJuzg);
          pVer.setPlaza(plaza);

          ProcedimientoVerbal pVerSaved = procedimientoVerbalRepository.save(pVer);
          fp.setProcedimiento(pVerSaved);
          idFinal = pVerSaved.getId();

          break;

        case "ORD":
          ProcedimientoOrdinario pOrd = new ProcedimientoOrdinario();
          pOrd.getContratos().addAll(conList);
          pOrd.getDemandados().addAll(intList);

          pOrd.setTipo(tp);
          pOrd.setEstado(ep);

          pOrd.setNumeroAutos(nAuto);
          pOrd.setJuzgado(nJuzg);
          pOrd.setPlaza(plaza);

          ProcedimientoOrdinario pOrdSaved = procedimientoOrdinarioRepository.save(pOrd);
          fp.setProcedimiento(pOrdSaved);
          idFinal = pOrdSaved.getId();

          break;

        case "ETJ":
          ProcedimientoEtj pEtj = new ProcedimientoEtj();
          pEtj.getContratos().addAll(conList);
          pEtj.getDemandados().addAll(intList);

          pEtj.setTipo(tp);
          pEtj.setEstado(ep);

          pEtj.setNumeroAutos(nAuto);
          pEtj.setJuzgado(nJuzg);
          pEtj.setPlaza(plaza);

          ProcedimientoEtj pEtjSaved = procedimientoEtjRepository.save(pEtj);
          fp.setProcedimiento(pEtjSaved);
          idFinal = pEtjSaved.getId();

          break;

        case "CON":
          ProcedimientoConcursal pCon = new ProcedimientoConcursal();
          pCon.getContratos().addAll(conList);
          pCon.getDemandados().addAll(intList);

          pCon.setTipo(tp);
          pCon.setEstado(ep);

          pCon.setNumeroAutos(nAuto);
          pCon.setJuzgado(nJuzg);
          pCon.setPlaza(plaza);

          ProcedimientoConcursal pConSaved = procedimientoConcursalRepository.save(pCon);
          fp.setProcedimiento(pConSaved);
          idFinal = pConSaved.getId();

          break;

        default:
          Procedimiento p = new Procedimiento();
          p.getContratos().addAll(conList);
          p.getDemandados().addAll(intList);

          p.setTipo(tp);
          p.setEstado(ep);

          p.setNumeroAutos(nAuto);
          p.setJuzgado(nJuzg);
          p.setPlaza(plaza);

          Procedimiento pSaved = procedimientoRepository.save(p);
          fp.setProcedimiento(pSaved);
          idFinal = pSaved.getId();

          break;
      }

      fp.setConcurso(conc);
      fp.setLitigio(liti);
      fp.setAuto(auto);
      fp.setTestimonio(test);
      fp.setDisponible(disp);

      formalizacionProcedimientoRepository.save(fp);
    } else {
      fp =
          formalizacionProcedimientoRepository
              .findByProcedimientoId(id)
              .orElse(new FormalizacionProcedimiento());

      if (fp.getProcedimiento() == null) {
        Procedimiento p =
            procedimientoRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Propuesta", id));
        fp.setProcedimiento(p);
      }

      fp.setConcurso(conc);
      fp.setLitigio(liti);
      fp.setAuto(auto);
      fp.setTestimonio(test);
      fp.setDisponible(disp);

      formalizacionProcedimientoRepository.save(fp);
    }
    return idFinal;
  }

  public FormalizacionCompletaOutputDTO createFormalizacionDTO(Propuesta p) throws NotFoundException {
    FormalizacionCompletaOutputDTO result = new FormalizacionCompletaOutputDTO();

    Formalizacion f = p.getFormalizaciones().stream().findFirst().orElse(new Formalizacion());

    FormalizacionPropuestaOutputDTO fpo = new FormalizacionPropuestaOutputDTO(p, f);

    EstadoChecklist ecl = getEstadoChecklist(p, f);
    fpo.setEstadoCheckList(new CatalogoMinInfoDTO(ecl));

    Date inicio = f.getFechaInicioCheckList();
    if (inicio != null){
      Date fin = f.getFechaFinCheckList();
      if (fin == null) fin = new Date();
      Long dias = getDiasCheckList(inicio, fin);
      fpo.setDiasActivo(dias);
    }

    result.setPropuesta(fpo);

    List<FormalizacionContratoOutputDTO> contratos = new ArrayList<>();
    for (Contrato c : p.getContratos()) {
      FormalizacionContrato fc =
          formalizacionContratoRepository
              .findByContratoId(c.getId())
              .orElse(new FormalizacionContrato());

      FormalizacionContratoOutputDTO fcO = new FormalizacionContratoOutputDTO(c, fc);

      List<FormalizacionJudicialOutputDTO> procedimientos = new ArrayList<>();
      for (Procedimiento pr : c.getProcedimientos()) {
        FormalizacionProcedimiento fp =
            formalizacionProcedimientoRepository
                .findByProcedimientoId(pr.getId())
                .orElse(new FormalizacionProcedimiento());
        procedimientos.add(new FormalizacionJudicialOutputDTO(pr, fp));
      }
      fcO.setProccedimientos(procedimientos);

      contratos.add(fcO);
    }
    result.setContratos(contratos);

    Expediente ex = p.getExpediente();
    if (ex != null){
      Procedimiento pr = expedienteUtil.getUltimoProcedimiento(ex);
      if (pr != null) {
        FormalizacionProcedimiento fp =
            pr.getFormalizacionProcedimiento() != null
                ? pr.getFormalizacionProcedimiento()
                : new FormalizacionProcedimiento();
        result.setMayorHito(new FormalizacionJudicialOutputDTO(pr, fp));
      }
    }

    List<FormalizacionIntervinienteOutputDTO> intervinientes = new ArrayList<>();
    for (Interviniente i : p.getIntervinientes()) {
      intervinientes.add(new FormalizacionIntervinienteOutputDTO(i));
    }
    result.setIntervinientes(intervinientes);

    List<FormalizacionBienOutputDTO> bienes = new ArrayList<>();
    List<Integer> idsBien = new ArrayList<>();
    for (PropuestaBien pb : p.getBienes()) {
      Bien b = pb.getBien();
      if (idsBien.contains(b.getId())) continue;

      FormalizacionBien fb =
          formalizacionBienRepository.findByBienId(b.getId()).orElse(new FormalizacionBien());
      FormalizacionBienOutputDTO fbO = new FormalizacionBienOutputDTO(b, fb);

      List<FormalizacionCargaOutputDTO> cargas = new ArrayList<>();
      List<Carga> cargasBien = b.getCargas().stream().sorted(Comparator.comparing(Carga::getFechaAnotacion)).collect(Collectors.toList());
      for (Carga c : cargasBien) {
        FormalizacionCarga fc =
            formalizacionCargaRepository.findByCargaId(c.getId()).orElse(new FormalizacionCarga());
        cargas.add(new FormalizacionCargaOutputDTO(c, fc));
      }
      fbO.setCargas(cargas);

      idsBien.add(fbO.getId());

      bienes.add(fbO);
    }
    result.setBienes(bienes);

    result.setIntervinientePrincipal(null);
    Expediente e = p.getExpediente();
    if (e != null) {
      Contrato c = e.getContratoRepresentante();
      if (c != null) {
        Interviniente i = c.getPrimerInterviniente();
        if (i != null) result.setIntervinientePrincipal(new FormalizacionIntervinienteOutputDTO(i));
      }
    }

    return result;
  }

  public String[] getEmails(String concat, List<String> emails) throws RequiredValueException {
    if (emails != null && !emails.isEmpty()) return Strings.toStringArray(emails);
    String[] result = concat.split(", ");

    if (result.length == 0) result = concat.split(",");
    if (result.length == 0) throw new RequiredValueException("Emails");

    return result;
  }

  public List<SendEmailDTO> obtenerSendEmail(Propuesta p, List<Bien> bienes, String codigo, String[] direcciones) throws NotFoundException {
    List<SendEmailDTO> result = new ArrayList<>();
    for (String dir : direcciones){
      SendEmailDTO se = new SendEmailDTO();

      ContactAttributes ca = new ContactAttributes();

      SubscriberAttributes sa = obtenerSubscriberAttributes(p, bienes, codigo);

      ca.setSubscriberAttributes(sa);
      se.setContactAttributes(ca);
      se.setAddress(dir);
      se.setSubscriberKey(1);

      result.add(se);
    }

    return result;
  }

  public SubscriberAttributes obtenerSubscriberAttributes(Propuesta p, List<Bien> bienes, String codigo) throws NotFoundException {
    PlantillaFormalizacion pf = plantillaFormalizacionRepository.findByKey(codigo).orElse(null);

    SubscriberAttributes sa = new SubscriberAttributes();

    Expediente e = p.getExpediente();
    sa.setIdExpediente(e.getIdConcatenado());
    if (pf != null){
      sa.setTextoEmail(pf.getTextoPlantilla());
      sa.setSubject(pf.getNombrePlantilla());
    } else {
      sa.setTextoEmail(" ");
      sa.setSubject("Email Formalización");
    }

    Contrato cRep = null;
    Interviniente iRep = null;
    Cartera ca = null;
    Cliente cl = null;
    Usuario gForm = null;
    Formalizacion f = p.getFormalizaciones().stream().findFirst().orElse(new Formalizacion());

    if (e != null){
      cRep = e.getContratoRepresentante();
      if (cRep != null) iRep = cRep.getPrimerInterviniente();

      gForm = e.getUsuarioByPerfil("Gestor formalización");
      if (gForm == null)
        throw new NotFoundException("Expediente", "Gestor Formalización");

      ca = e.getCartera();
      if (ca != null) cl = ca.getClientes().stream().map(ClienteCartera::getCliente).findFirst().orElse(null);
    } else
      throw new NotFoundException("Propuesta", "Expediente");

    if (gForm.getTelefono() != null && !gForm.getTelefono().isEmpty())
      sa.setTelefono(gForm.getTelefono());
    if (gForm.getEmail() != null && !gForm.getEmail().isEmpty())
      sa.setCorreo(gForm.getEmail());

    switch (codigo){
      case "3NC61": // Encargo Gestoria
      case "61CT4": // Solicitud Visita
        sa.setCliente(cl.getNombre());
        sa.setTipoPropuesta(p.getTipoPropuesta() != null ? p.getTipoPropuesta().getValor() : null);
        if (iRep != null){
          if (iRep.getNombre() != null) sa.setNombre(iRep.getNombre());
          if (iRep.getApellidos() != null) sa.setApellidos(iRep.getApellidos());
          if (iRep.getRazonSocial() != null) sa.setRazonSocial(iRep.getRazonSocial());
        }
        sa.setAlquiler(f.getAlquiler() != null ? f.getAlquiler().toString() : null);
        sa.setFechaFirma(f.getFechaFirma() != null ? f.getFechaFirma().toString() : null);
        sa.setHoraFirma(f.getHoraFirma() != null ? f.getHoraFirma().toString() : null);
        sa.setNotario(f.getNotario());
        sa.setMunicipioNotaria(f.getMunicipioNotaria() != null ? f.getMunicipioNotaria().getValor() : null);
        sa.setProvinciaNotaria(f.getProvinciaNotaria() != null ? f.getProvinciaNotaria().getValor() : null);
        sa.setDireccionNotaria(f.getDireccionNotaria());
        sa.setTelefonoOficialNotaria(f.getTelefonoOficialNotaria());
        sa.setEmailOficialNotaria(f.getEmailOficialNotaria());

        mapeoVisitas(sa, bienes);
        break;
      case "V3961N":// Vencimiento Sancion
        sa.setGestorFormalizacion(gForm.getNombre());
        sa.setFechaVencimientoSancion(f.getVencimientoSancion() != null ? f.getVencimientoSancion().toString() : null);
        sa.setIdConcatenado(e.getIdConcatenado());
        sa.setIdPropuesta(p.getIdCarga());
        break;
    }

    return sa;
  }

  private void mapeoVisitas(SubscriberAttributes sa, List<Bien> bienes){
    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();

    Map<SubTipoBien, Integer> tiposActivos = new HashMap<>();
    Map<Municipio, Integer> municipios = new HashMap<>();
    Map<Provincia, Integer> provincias = new HashMap<>();
    Map<Alquiler, Integer> alquileres = new HashMap<>();

    String direc = bienes.stream().map(Bien::getDireccionCalle).collect(Collectors.joining("; "));
    String finca = bienes.stream().map(Bien::getFinca).collect(Collectors.joining("; "));
    String regis = bienes.stream().map(Bien::getRegistro).collect(Collectors.joining("; "));

    for (Bien b : bienes){
      FormalizacionBien fb = b.getFormalizacionBien();
      if (fb != null){
        if (fb.getAlquiler() != null){
          if (alquileres.containsKey(fb.getAlquiler())){
            Integer cant = alquileres.get(fb.getAlquiler());
            cant ++;
            alquileres.put(fb.getAlquiler(), cant);
          } else {
            alquileres.put(fb.getAlquiler(), 1);
          }
        }
      }

      if (b.getSubTipoBien() != null){
        if (tiposActivos.containsKey(b.getSubTipoBien())){
          Integer cant = tiposActivos.get(b.getSubTipoBien());
          cant ++;
          tiposActivos.put(b.getSubTipoBien(), cant);
        } else {
          tiposActivos.put(b.getSubTipoBien(), 1);
        }
      }

      if (b.getMunicipio() != null){
        if (municipios.containsKey(b.getMunicipio())){
          Integer cant = municipios.get(b.getMunicipio());
          cant ++;
          municipios.put(b.getMunicipio(), cant);
        } else {
          municipios.put(b.getMunicipio(), 1);
        }
      }

      if (b.getProvincia() != null){
        if (provincias.containsKey(b.getProvincia())){
          Integer cant = provincias.get(b.getProvincia());
          cant ++;
          provincias.put(b.getProvincia(), cant);
        } else {
          provincias.put(b.getProvincia(), 1);
        }
      }
    }

    String alqui = "";
    for (Map.Entry<Alquiler, Integer> o : alquileres.entrySet()){
      Alquiler ta = o.getKey();
      if (defaultLocal == null || defaultLocal.equals("es")) alqui += ta.getValor();
      else if (defaultLocal.equals("en")) alqui += ta.getValorIngles();
      alqui += ": " + o.getValue() + ", ";
    }
    sa.setAlquiler(alqui);

    String tipoAct = "";
    for (Map.Entry<SubTipoBien, Integer> o : tiposActivos.entrySet()){
      SubTipoBien sb = o.getKey();
      TipoBien tb = sb.getTipoBien();
      if (defaultLocal == null || defaultLocal.equals("es")) tipoAct += tb.getValor() + "-" + sb.getValor();
      else if (defaultLocal.equals("en")) tipoAct += tb.getValorIngles() + "-" + sb.getValorIngles();
      tipoAct += ": " + o.getValue() + ", ";
    }
    sa.setTipoActivo(tipoAct);

    String munic = "";
    for (Map.Entry<Municipio, Integer> o : municipios.entrySet()){
      Municipio ta = o.getKey();
      if (defaultLocal == null || defaultLocal.equals("es")) munic += ta.getValor();
      else if (defaultLocal.equals("en")) munic += ta.getValorIngles();
      munic += ": " + o.getValue() + ", ";
    }
    sa.setMunicipio(munic);

    String provi = "";
    for (Map.Entry<Provincia, Integer> o : provincias.entrySet()){
      Provincia ta = o.getKey();
      if (defaultLocal == null || defaultLocal.equals("es")) provi += ta.getValor();
      else if (defaultLocal.equals("en")) provi += ta.getValorIngles();
      provi += ": " + o.getValue() + ", ";
    }
    sa.setProvincia(provi);

    sa.setDireccion(direc);
    sa.setFinca(finca);
    sa.setRegistroDeLaPropiedad(regis);
  }

  public CheckListOutputDTO entityToDTO(DatosCheckList dcl, Boolean documentado){
    CheckListOutputDTO result = new CheckListOutputDTO();

    result.setDocumentado(documentado);
    result.setObligatorio(dcl.getNombre().getObligatorio());
    result.setNombre(new CatalogoMinInfoDTO(dcl.getNombre()));
    result.setValidado(dcl.getDocumentacionValidada() != null ? new CatalogoMinInfoDTO(dcl.getDocumentacionValidada()) : null);
    List<BienDTOToList> bList = bienMapper.listBienToListBienDTOToList(new ArrayList<>(dcl.getBienes()), null);
    result.setBienes(bList);

    return result;
  }

  public void dtoToEntity(CheckListInputDTO in, DatosCheckList dcl) throws NotFoundException {
    if (in.getValidado() != null){
      DocumentacionValidada dv =
        documentacionValidadaRepository
          .findById(in.getValidado())
          .orElseThrow(() -> new NotFoundException("DocumentacionValidada", in.getValidado()));
      dcl.setDocumentacionValidada(dv);
    }
    if (in.getNombre() != null){
      NombresCheckList ncl =
        nombresCheckListRepository
          .findById(in.getNombre())
          .orElseThrow(() -> new NotFoundException("NombresCheckList", in.getNombre()));
      dcl.setNombre(ncl);
    }
    List<Bien> bienes = new ArrayList<>();
    if (in.getBienes() != null && !in.getBienes().isEmpty()){
      bienes = bienRepository.findAllById(in.getBienes());
    } else if (in.getBienesCodigos() != null && !in.getBienesCodigos().isEmpty())
      bienes = bienRepository.findAllByIdCargaIn(in.getBienesCodigos());
    dcl.setBienes(new HashSet<>(bienes));
  }

  public long getDiasCheckList(Date fIni, Date fFin) {
    long diffInMillies = Math.abs(fFin.getTime() - fIni.getTime());
    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
    return diff;
  }

  public EstadoChecklist getEstadoChecklist(Propuesta p, Formalizacion f) throws NotFoundException {
    List<Bien> bienes =
      p.getBienes().stream().map(PropuestaBien::getBien).distinct().collect(Collectors.toList());

    DocumentacionValidada dvDesc =
      documentacionValidadaRepository
        .findByCodigo("N")
        .orElseThrow(() -> new NotFoundException("DocumentacionValidada", "Codigo", "N"));

    List<NombresCheckList> nombres = nombresCheckListRepository.findAll();

    Boolean comprobar = true;
    EstadoChecklist estadoGeneral = null;
    if (f.getEstadoChecklist() != null && f.getEstadoChecklist().getCodigo().equals("ok")){
      comprobar = false;
    }

    for (NombresCheckList ncl : nombres) {
      DatosCheckList dclFinal = null;
      DatosCheckList dcl =
        datosCheckListRepository
          .findByPropuestaIdAndNombreId(p.getId(), ncl.getId())
          .orElse(new DatosCheckList());
      if (dcl.getId() == null) {
        dcl.setDocumentacionValidada(dvDesc);
        dcl.setPropuesta(p);
        dcl.setNombre(ncl);
        dcl.setBienes(new HashSet<>(bienes));

        dclFinal = datosCheckListRepository.save(dcl);
      } else {
        dclFinal = dcl;
      }

      if (comprobar && ncl.getObligatorio()) {
        if (dclFinal.getDocumentacionValidada() != null
          && (dclFinal.getDocumentacionValidada().getCodigo().equals("S")
          || dclFinal.getDocumentacionValidada().getCodigo().equals("NA")
          || dclFinal.getDocumentacionValidada().getCodigo().equals("A"))) {
          estadoGeneral =
            estadoChecklistRepository
              .findByCodigo("PTE")
              .orElseThrow(() -> new NotFoundException("EstadoChecklist", "Codigo", "PTE"));
        } else if (dclFinal.getDocumentacionValidada() != null
          && (dclFinal.getDocumentacionValidada().getCodigo().equals("N")
          || dclFinal.getDocumentacionValidada().getCodigo().equals("DES"))) {
          estadoGeneral =
            estadoChecklistRepository
              .findByCodigo("ko")
              .orElseThrow(() -> new NotFoundException("EstadoChecklist", "Codigo", "ko"));
          comprobar = false;
        }
      }
    }

    if (estadoGeneral != null){
      f.setEstadoChecklist(estadoGeneral);
      formalizacionRepository.save(f);
    } else {
      estadoGeneral =
        estadoChecklistRepository
          .findByCodigo("ok")
          .orElseThrow(() -> new NotFoundException("EstadoChecklist", "Codigo", "ok"));
    }

    return estadoGeneral;
  }

  public void entityToDto(InformacionCheckList icl, InformacionCheckListDTO dto){
    if (icl.getSancionAceptadaDeudor() != null)
      BeanUtils.copyProperties(icl.getSancionAceptadaDeudor(), dto.getSancionAceptadaDeudor());
    if (icl.getLpo() != null)
      BeanUtils.copyProperties(icl.getLpo(), dto.getLpo());
    if (icl.getCfo() != null)
      BeanUtils.copyProperties(icl.getCfo(), dto.getCfo());
    if (icl.getArrendamientos() != null)
      BeanUtils.copyProperties(icl.getArrendamientos(), dto.getArrendamientos());
    if (icl.getFinanzasArrendamientosDepositadas() != null)
      BeanUtils.copyProperties(
          icl.getFinanzasArrendamientosDepositadas(), dto.getFinanzasArrendamientosDepositadas());
    if (icl.getOcupaciones() != null)
      BeanUtils.copyProperties(icl.getOcupaciones(), dto.getOcupaciones());
    if (icl.getDeudaPendienteEnComPropietarios() != null)
      BeanUtils.copyProperties(
          icl.getDeudaPendienteEnComPropietarios(), dto.getDeudaPendienteEnComPropietarios());
    if (icl.getDeudaPendienteEUC() != null)
      BeanUtils.copyProperties(icl.getDeudaPendienteEUC(), dto.getDeudaPendienteEUC());
    if (icl.getDeudaPendienteEnImpuestosYTasasMunicipales() != null)
      BeanUtils.copyProperties(
          icl.getDeudaPendienteEnImpuestosYTasasMunicipales(),
          dto.getDeudaPendienteEnImpuestosYTasasMunicipales());
    if (icl.getCargasUrbanisticas() != null)
      BeanUtils.copyProperties(icl.getCargasUrbanisticas(), dto.getCargasUrbanisticas());
    if (icl.getDeudaPorCargasUrbanisticas() != null)
      BeanUtils.copyProperties(
          icl.getDeudaPorCargasUrbanisticas(), dto.getDeudaPorCargasUrbanisticas());
    if (icl.getEmbargos() != null)
      BeanUtils.copyProperties(icl.getEmbargos(), dto.getEmbargos());
    if (icl.getHipotecas() != null)
      BeanUtils.copyProperties(icl.getHipotecas(), dto.getHipotecas());
    if (icl.getPlusvaliaCargoDelAdquirente() != null)
      BeanUtils.copyProperties(
          icl.getPlusvaliaCargoDelAdquirente(), dto.getPlusvaliaCargoDelAdquirente());
    if (icl.getGastosDeFormalizacion() != null)
      BeanUtils.copyProperties(icl.getGastosDeFormalizacion(), dto.getGastosDeFormalizacion());
    if (icl.getPrevisionDeGastos() != null)
      BeanUtils.copyProperties(icl.getPrevisionDeGastos(), dto.getPrevisionDeGastos());
    if (icl.getAportacionDeEfectivo() != null)
      BeanUtils.copyProperties(icl.getAportacionDeEfectivo(), dto.getAportacionDeEfectivo());
    if (icl.getAportacionDeActivos() != null)
      BeanUtils.copyProperties(icl.getAportacionDeActivos(), dto.getAportacionDeActivos());
    if (icl.getCondonacionDeRemanente() != null)
      BeanUtils.copyProperties(icl.getCondonacionDeRemanente(), dto.getCondonacionDeRemanente());
    if (icl.getOperacionConcursal() != null)
      BeanUtils.copyProperties(icl.getOperacionConcursal(), dto.getOperacionConcursal());
    if (icl.getOperacionConcursalRiesgo() != null)
      BeanUtils.copyProperties(
          icl.getOperacionConcursalRiesgo(), dto.getOperacionConcursalRiesgo());
    if (icl.getOperacionConcursalDispone() != null)
      BeanUtils.copyProperties(
          icl.getOperacionConcursalDispone(), dto.getOperacionConcursalDispone());
    if (icl.getAltaEnPrisma() != null)
      BeanUtils.copyProperties(icl.getAltaEnPrisma(), dto.getAltaEnPrisma());
    if (icl.getCheckListSustituta() != null)
      BeanUtils.copyProperties(icl.getCheckListSustituta(), dto.getCheckListSustituta());
    if (icl.getListaDeContraste() != null)
      BeanUtils.copyProperties(icl.getListaDeContraste(), dto.getListaDeContraste());
  }

  public void dtoToEntity(InformacionCheckList icl, InformacionCheckListDTO dto){
    if (icl.getSancionAceptadaDeudor() == null){
      CheckListString a = new CheckListString();
      icl.setSancionAceptadaDeudor(a);
    }
    BeanUtils.copyProperties(dto.getSancionAceptadaDeudor(), icl.getSancionAceptadaDeudor());

    if (icl.getLpo() == null){
      CheckListString a = new CheckListString();
      icl.setLpo(a);
    }
    BeanUtils.copyProperties(dto.getLpo(), icl.getLpo());

    if (icl.getCfo() == null) {
      CheckListString a = new CheckListString();
      icl.setCfo(a);
    }
    BeanUtils.copyProperties(dto.getCfo(), icl.getCfo());

    if (icl.getArrendamientos() == null) {
      CheckListString a = new CheckListString();
      icl.setArrendamientos(a);
    }
    BeanUtils.copyProperties(dto.getArrendamientos(), icl.getArrendamientos());

    if (icl.getFinanzasArrendamientosDepositadas() == null) {
      CheckListString a = new CheckListString();
      icl.setFinanzasArrendamientosDepositadas(a);
    }
    BeanUtils.copyProperties(dto.getFinanzasArrendamientosDepositadas(), icl.getFinanzasArrendamientosDepositadas());

    if (icl.getOcupaciones() == null) {
      CheckListString a = new CheckListString();
      icl.setOcupaciones(a);
    }
    BeanUtils.copyProperties(dto.getOcupaciones(), icl.getOcupaciones());

    if (icl.getDeudaPendienteEnComPropietarios() == null) {
      CheckListDouble a = new CheckListDouble();
      icl.setDeudaPendienteEnComPropietarios(a);
    }
    BeanUtils.copyProperties(dto.getDeudaPendienteEnComPropietarios(), icl.getDeudaPendienteEnComPropietarios());

    if (icl.getDeudaPendienteEUC() == null) {
      CheckListDouble a = new CheckListDouble();
      icl.setDeudaPendienteEUC(a);
    }
    BeanUtils.copyProperties(dto.getDeudaPendienteEUC(), icl.getDeudaPendienteEUC());

    if (icl.getDeudaPendienteEnImpuestosYTasasMunicipales() == null) {
      CheckListDouble a = new CheckListDouble();
      icl.setDeudaPendienteEnImpuestosYTasasMunicipales(a);
    }
    BeanUtils.copyProperties(dto.getDeudaPendienteEnImpuestosYTasasMunicipales(), icl.getDeudaPendienteEnImpuestosYTasasMunicipales());

    if (icl.getCargasUrbanisticas() == null) {
      CheckListInteger a = new CheckListInteger();
      icl.setCargasUrbanisticas(a);
    }
    BeanUtils.copyProperties(dto.getCargasUrbanisticas(), icl.getCargasUrbanisticas());

    if (icl.getDeudaPorCargasUrbanisticas() == null) {
      CheckListDouble a = new CheckListDouble();
      icl.setDeudaPorCargasUrbanisticas(a);
    }
    BeanUtils.copyProperties(dto.getDeudaPorCargasUrbanisticas(), icl.getDeudaPorCargasUrbanisticas());

    if (icl.getEmbargos() == null) {
      CheckListString a = new CheckListString();
      icl.setEmbargos(a);
    }
    BeanUtils.copyProperties(dto.getEmbargos(), icl.getEmbargos());

    if (icl.getHipotecas() == null) {
      CheckListString a = new CheckListString();
      icl.setHipotecas(a);
    }
    BeanUtils.copyProperties(dto.getHipotecas(), icl.getHipotecas());

    if (icl.getPlusvaliaCargoDelAdquirente() == null) {
      CheckListString a = new CheckListString();
      icl.setPlusvaliaCargoDelAdquirente(a);
    }
    BeanUtils.copyProperties(dto.getPlusvaliaCargoDelAdquirente(), icl.getPlusvaliaCargoDelAdquirente());

    if (icl.getGastosDeFormalizacion() == null) {
      CheckListDouble a = new CheckListDouble();
      icl.setGastosDeFormalizacion(a);
    }
    BeanUtils.copyProperties(dto.getGastosDeFormalizacion(), icl.getGastosDeFormalizacion());

    if (icl.getPrevisionDeGastos() == null) {
      CheckListDouble a = new CheckListDouble();
      icl.setPrevisionDeGastos(a);
    }
    BeanUtils.copyProperties(dto.getPrevisionDeGastos(), icl.getPrevisionDeGastos());

    if (icl.getAportacionDeEfectivo() == null) {
      CheckListDouble a = new CheckListDouble();
      icl.setAportacionDeEfectivo(a);
    }
    BeanUtils.copyProperties(dto.getAportacionDeEfectivo(), icl.getAportacionDeEfectivo());

    if (icl.getAportacionDeActivos() == null) {
      CheckListDouble a = new CheckListDouble();
      icl.setAportacionDeActivos(a);
    }
    BeanUtils.copyProperties(dto.getAportacionDeActivos(), icl.getAportacionDeActivos());

    if (icl.getCondonacionDeRemanente() == null) {
      CheckListDouble a = new CheckListDouble();
      icl.setCondonacionDeRemanente(a);
    }
    BeanUtils.copyProperties(dto.getCondonacionDeRemanente(), icl.getCondonacionDeRemanente());

    if (icl.getOperacionConcursal() == null) {
      CheckListString a = new CheckListString();
      icl.setOperacionConcursal(a);
    }
    BeanUtils.copyProperties(dto.getOperacionConcursal(), icl.getOperacionConcursal());

    if (icl.getOperacionConcursalRiesgo() == null) {
      CheckListDouble a = new CheckListDouble();
      icl.setOperacionConcursalRiesgo(a);
    }
    BeanUtils.copyProperties(dto.getOperacionConcursalRiesgo(), icl.getOperacionConcursalRiesgo());

    if (icl.getOperacionConcursalDispone() == null) {
      CheckListString a = new CheckListString();
      icl.setOperacionConcursalDispone(a);
    }
    BeanUtils.copyProperties(dto.getOperacionConcursalDispone(), icl.getOperacionConcursalDispone());

    if (icl.getAltaEnPrisma() == null) {
      CheckListString a = new CheckListString();
      icl.setAltaEnPrisma(a);
    }
    BeanUtils.copyProperties(dto.getAltaEnPrisma(), icl.getAltaEnPrisma());

    if (icl.getCheckListSustituta() == null) {
      CheckListString a = new CheckListString();
      icl.setCheckListSustituta(a);
    }
    BeanUtils.copyProperties(dto.getCheckListSustituta(), icl.getCheckListSustituta());

    if (icl.getListaDeContraste() == null) {
      CheckListString a = new CheckListString();
      icl.setListaDeContraste(a);
    }
    BeanUtils.copyProperties(dto.getListaDeContraste(), icl.getListaDeContraste());
  }
}
