package com.haya.alaska.formalizacion.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CheckListInputDTO implements Serializable {
  private Integer nombre;
  private Boolean nuevoDocumento;
  private Integer validado;
  private List<Integer> bienes;
  private List<String> bienesCodigos;
}
