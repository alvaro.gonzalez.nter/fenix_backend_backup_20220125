package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.tipo_contacto.domain.TipoContacto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionContactoOutputDTO implements Serializable {
  private Integer id;

  private String tipo; // TipoIntervencion
  private String valor;
  private Integer orden;

  public FormalizacionContactoOutputDTO(DatoContacto dc){
    this.id = dc.getId();

    this.orden = dc.getOrden();

    if (dc.getMovil() != null){
      this.valor = dc.getMovil();
      this.tipo = "Movil";
    }
    else if (dc.getFijo() != null){
      this.valor = dc.getFijo();
      this.tipo = "Fijo";
    }
    else if (dc.getEmail() != null){
      this.valor = dc.getEmail();
      this.tipo = "Email";
    }
  }

  public FormalizacionContactoOutputDTO(Integer id, String tipo, String valor, Integer orden){
    this.id = id;
    this.tipo = tipo;
    this.valor = valor;
    this.orden = orden;
  }
}
