package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input;

import com.haya.alaska.formalizacion.infrastructure.controller.dto.CheckListInputDTO;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.InformacionCheckListDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionCompletaInputDTO implements Serializable {
  FormalizacionPropuestaInputDTO propuesta;
  List<FormalizacionContratoInputDTO> contratos;
  List<FormalizacionBienInputDTO> bienes;
  List<FormalizacionCargaInputDTO> cargas;
  List<FormalizacionIntervinienteInputDTO> intervinientes;
  List<FormalizacionContactoInputDTO> contactos;
  List<FormalizacionJudicialInputDTO> procedimientos;
  List<CheckListInputDTO> checkList;
  InformacionCheckListDTO informacionCheckList;
}
