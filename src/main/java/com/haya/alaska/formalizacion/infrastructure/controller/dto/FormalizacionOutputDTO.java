package com.haya.alaska.formalizacion.infrastructure.controller.dto;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.controller.dto.DatosContactoDTOToList;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionOutputDTO {
  private Integer id;
  private Integer propuestaId;

  // 1- Datos del Gestor
  private String gestorRecuperacionCliente;
  private String telefono1GesRecCli;
  private String analistaCliente;
  private String telefono1Cliente;
  private UsuarioAgendaDto gestorFormalizacion;
  private String telefono1GesFormalizacion;
  private String gestorFormalizacionCliente;
  private String telefono1GesFormalizacionCliente;

  // 2- Datos Estado
  private Date fechaSancion;
  private Boolean documentacionCompletada;
  private Boolean solicitudGestionActivosVisita;
  private Date fEnvioMinutaANotaria;
  private Date fEnvioMinutaAClienteFormalizacion;
  private Date fOkMinutaANotaria;
  private Boolean informeVisitaOk;
  private Date fechaFirma;
  private Time horaFirma;
  private Date fechaPosesion;
  private Boolean alquiler;
  private Date fechaVigencia;
  private Boolean pbc;
  private String comentarios;

  // 3- Notaria
  private String direccionNotaria;
  private String notario;
  private Integer numeroProtocolo;
  private String contactoOficinaCliente;
  private String apoderadoFirma;
  private String gestoriaTramitadora;
  private String oficinaNotaria;
  private String telefonoNotaria;
  private String telefonoOficialNotaria;
  private String emailOficialNotaria;

  private List<DatosContratosFormalizacionOutputDTO> datosContratosFormalizacion;
  private List<DatosIntervinientesFormalizacionOutputDTO> datosIntervinientesFormalizacion;
  private List<DatosColateralesFormalizacionOutputDTO> datosColateralesFormalizacion;

  public FormalizacionOutputDTO(Formalizacion formalizacion, Boolean posesionNegociada) {
    BeanUtils.copyProperties(formalizacion, this);
    Propuesta p = formalizacion.getPropuesta();
    Usuario gestorForm = null;

    this.propuestaId = formalizacion.getPropuesta().getId();

    this.datosContratosFormalizacion = new ArrayList<>();
    for (Contrato contrato : formalizacion.getPropuesta().getContratos()) {
      this.datosContratosFormalizacion.add(new DatosContratosFormalizacionOutputDTO(contrato));
    }

    this.datosIntervinientesFormalizacion = new ArrayList<>();
    if (posesionNegociada) {
      for (Ocupante ocupante : formalizacion.getPropuesta().getOcupantes()) {
        this.datosIntervinientesFormalizacion.add(new DatosIntervinientesFormalizacionOutputDTO(ocupante));
        ExpedientePosesionNegociada epn = p.getExpedientePosesionNegociada();
        gestorForm = epn.getUsuarioByPerfil("Gestor formalización");
      }
    } else {
      for (Interviniente interviniente : formalizacion.getPropuesta().getIntervinientes()) {
        this.datosIntervinientesFormalizacion.add(new DatosIntervinientesFormalizacionOutputDTO(interviniente));
        Expediente epn = p.getExpediente();
        gestorForm = epn.getUsuarioByPerfil("Gestor formalización");
      }
    }
    if (gestorForm != null) this.gestorFormalizacion = new UsuarioAgendaDto(gestorForm);

    this.datosColateralesFormalizacion = new ArrayList<>();
    for (PropuestaBien bien : formalizacion.getPropuesta().getBienes()) {
      this.datosColateralesFormalizacion.add(new DatosColateralesFormalizacionOutputDTO(bien));
    }
  }

  public FormalizacionOutputDTO(Formalizacion formalizacion) {
    this(formalizacion, false);
  }
}
