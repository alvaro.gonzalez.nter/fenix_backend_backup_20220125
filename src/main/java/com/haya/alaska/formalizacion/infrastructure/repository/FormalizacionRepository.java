package com.haya.alaska.formalizacion.infrastructure.repository;

import com.haya.alaska.formalizacion.domain.Formalizacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormalizacionRepository extends JpaRepository<Formalizacion, Integer> {
    Formalizacion findByPropuestaId(Integer id);
}
