package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionPropuestaOutputDTO implements Serializable {
  private Integer id;
  // Avance de Gestion
  private String idCarga;
  private String idExpedienteOrigen;

  private CatalogoMinInfoDTO clasePropuesta;
  private CatalogoMinInfoDTO tipoPropuesta;
  private CatalogoMinInfoDTO subtipoPropuesta;
  private CatalogoMinInfoDTO estadoPropuesta; // EstadoFormalizacion
  private CatalogoMinInfoDTO subestadoPropuesta; // SubestadoFormalizacion // DEPRECATED

  private Date fechaFirma;
  private String horaFirma;
  private Date fechaFirmaEstimada;
  //private Boolean confirmadaFechaYHora;

  private Date fechaEntrada;
  private Date fechaSancion;

  private CatalogoMinInfoDTO tipoSancion; // SancionPropuesta
  private Date vencimientoSancion;

  private Boolean documentacionCompleta;
  private Date fechaKODocumentacion;
  private Date fechaOKDocumentacion;

  private Date solicitudMinuta;
  private Date recepcionMinuta;
  private Date revisionMinuta;
  private Date envioMinutaNotaria;
  private Date recepcionMinutaNotaria;
  private Date envioMinutaCliente;
  private Date okMinuta;

  private Date solicitudGestion;
  private Boolean informeVisitaOk;

  private Date fechaComunicacion;
  private Date fechaEntradaTF;
  private Date fechaAvanceTareaTF;

  private Integer numeroProtocolo;
  private String apoderadosFirma;
  private String observaciones;

  private CatalogoMinInfoDTO recomendacionNoFirma; // RecomendacionNoFirma
  private CatalogoMinInfoDTO motivoRecomendacionNoFirma; // MotivoRecomendacionNoFirma

  private Date minutaRevisadaFiscal;
  private Date fechaRevisionConcursos;

  // Principal
  private Boolean dacionSingular;

  private Integer totalFFRR;

  private String alertaComunicacion;
  private CatalogoMinInfoDTO comprador; // Compradoe

  private Boolean penRatVen;
  private Boolean penRatCom;
  private Boolean penRatBan;
  private Boolean firmCon;

  private Boolean pbc;
  private Date fechaVigencia;

  private Boolean embargoOtrosAsime;
  private Double embargoOtrosImporte;

  private String observacionesPrincipal;

  private CatalogoMinInfoDTO formaDePago; // FormaDePago

  private Boolean gastosAsume;
  private Double gastosImporte;
  private Boolean plusvaliaAsume;
  private Double plusvaliaImporte;
  private Boolean ibiAsume;
  private Double ibiImporte;
  private Boolean cpAsume;
  private Double cpImporte;
  private Boolean urbanizacionAsume;
  private Double urbanizacionImporte;

  private Double importeEntregaEfectivo;
  private Boolean entregaBienesLibres;

  // DD
  private Date encargoDDL;
  private Date recepcionDDL;
  private CatalogoMinInfoDTO estadoDDL; // EstadoDdlDdt

  private Date encargoDDT;
  private Date recepcionDDT;
  private CatalogoMinInfoDTO estadoDDT; // EstadoDdlDdt

  private String despachoLegal;
  private String despachoTecnico;

  private Boolean ddRecibidaYSubida;
  private Date subidaDD;
  private Double honorariosDD;
  private Double honorariosAsistenciaFirma;

  private String valoracionDDCliente;
  private String horariosDDTecnica;

  // Contactos
  private String gestorFormalizacionLegal; // Usuario
  private UsuarioAgendaDto gestorFormalizacionPrincipal;
  private CatalogoMinInfoDTO gestorConcursos; // GestorConcursos
  private CatalogoMinInfoDTO gestorGestoria; // GestorGestoria
  private UsuarioAgendaDto gestorRecuperacionHaya; // Usuario
  private String gestorRecuperacionCliente;
  private String tel1GestorRecuperacionCliente;

  private String territorial;
  private String analistaCliente;
  private String tel1AnalistaCliente;
  private String gestorFormalizacionLetradoCliente;
  private String tel1GestorFormalizacionLetradoCliente;

  private String notario;
  private String direccionNotaria;
  private CatalogoMinInfoDTO provinciaNotaria; // Provincia
  private CatalogoMinInfoDTO municipioNotaria; // Municipio
  private String oficialNotaria;
  private String telefonoNotaria;
  private String telefonoOficialNotaria;
  private String emailOficialNotaria;
  private String contactoOficina;

  private String gestoria;

  //Sareb
  private Double nbv;
  private Date tecleoEnPrisma;
  private Date elevacionNilo;
  private Date cierreNilo;

  //CheckList
  private CatalogoMinInfoDTO estadoCheckList;
  private Long diasActivo;

  public FormalizacionPropuestaOutputDTO(Propuesta p, Formalizacion fp){
    this.id = p.getId();
    this.idCarga = p.getIdCarga();

    this.clasePropuesta = p.getClasePropuesta() != null ? new CatalogoMinInfoDTO(p.getClasePropuesta()) : null;
    this.tipoPropuesta = p.getTipoPropuesta() != null ? new CatalogoMinInfoDTO(p.getTipoPropuesta()) : null;
    this.subtipoPropuesta = p.getSubtipoPropuesta() != null ? new CatalogoMinInfoDTO(p.getSubtipoPropuesta()) : null;
    this.estadoPropuesta = p.getEstadoPropuesta() != null ? new CatalogoMinInfoDTO(p.getEstadoPropuesta()) : null;
    this.subestadoPropuesta = fp.getSubestadoFormalizacion() != null ? new CatalogoMinInfoDTO(fp.getSubestadoFormalizacion()) : null;

    this.fechaFirma = fp.getFechaFirma();
    this.horaFirma = fp.getHoraFirma() != null ? fp.getHoraFirma().toString() : null;
    this.fechaFirmaEstimada = fp.getFechaFirmaEstimada();
    //this.confirmadaFechaYHora = fp.getConfirmadaFechaYHora();

    this.fechaEntrada = fp.getFechaEntrada();
    this.fechaSancion = fp.getFechaSancion();

    this.tipoSancion = p.getSancionPropuesta() != null ? new CatalogoMinInfoDTO(p.getSancionPropuesta()) : null;
    this.vencimientoSancion = fp.getVencimientoSancion();

    this.documentacionCompleta = fp.getEstadoChecklist() != null ? fp.getEstadoChecklist().getCodigo().equals("ok") : null;
    this.fechaKODocumentacion = fp.getFechaKoDocumentacion();
    this.fechaOKDocumentacion = fp.getFechaOkDocumentacion();

    this.solicitudMinuta = fp.getSolicitudMinuta();
    this.recepcionMinuta = fp.getRecepcionMinuta();
    this.revisionMinuta = fp.getRevisionMinutaHRE();
    this.envioMinutaNotaria = fp.getFEnvioMinutaANotaria();
    this.recepcionMinutaNotaria = fp.getRecepcionMinutaDeNotaria();
    this.envioMinutaCliente = fp.getFEnvioMinutaAClienteFormalizacion();
    this.okMinuta = fp.getFOkMinutaANotaria();

    this.solicitudGestion = fp.getSolicitudGestionActivosVisita();
    this.informeVisitaOk = fp.getInformeVisitaOk();

    this.fechaComunicacion = fp.getFechaComunicacion();
    this.fechaEntradaTF = fp.getFechaEntradaTF();
    this.fechaAvanceTareaTF = fp.getFechaAvanceTareaTF();

    this.numeroProtocolo = fp.getNumeroProtocolo();
    this.apoderadosFirma = fp.getApoderadoFirma();
    this.observaciones = fp.getObservaciones();

    this.recomendacionNoFirma = fp.getRecomendacionNoFirma() != null ? new CatalogoMinInfoDTO(fp.getRecomendacionNoFirma()) : null;
    this.motivoRecomendacionNoFirma = fp.getMotivoRecomendacionNoFirma() != null ? new CatalogoMinInfoDTO(fp.getMotivoRecomendacionNoFirma()) : null;

    this.minutaRevisadaFiscal = fp.getMinutaRevisadaFiscal();
    this.fechaRevisionConcursos = fp.getFechaRevisionConcursos();

    // Principal
    this.dacionSingular = fp.getDacionSingular();

    if(p.getBienes() != null)
      this.totalFFRR = p.getBienes().size();
    else
      this.totalFFRR = 0;


    this.alertaComunicacion = fp.getAlertaEnComunicacion();
    this.comprador = fp.getComprador() != null ? new CatalogoMinInfoDTO(fp.getComprador()) : null;

    this.penRatVen = fp.getPendienteRatificacionVendedor();
    this.penRatCom = fp.getPendienteRatificacionComprador();
    this.penRatBan = fp.getPendienteRatificacionBanco();
    this.firmCon = fp.getFirmaCondicionada();

    this.pbc = fp.getPbc();
    this.fechaVigencia = fp.getFechaVigencia();

    this.embargoOtrosAsime = fp.getEmbargoOtrosAsume();
    this.embargoOtrosImporte = fp.getEmbargoOtrosImporte();

    this.observacionesPrincipal = fp.getObservacionesPrincipal();

    this.formaDePago = fp.getFormaDePago() != null ? new CatalogoMinInfoDTO(fp.getFormaDePago()) : null;

    this.gastosAsume = fp.getGastosAsume();
    this.gastosImporte = fp.getGastosImporte();
    this.plusvaliaAsume = fp.getPlusvaliaAsume();
    this.plusvaliaImporte = fp.getPlusvaliaImporte();
    this.ibiAsume = fp.getIbiAsume();
    this.ibiImporte = fp.getIbiImporte();
    this.cpAsume = fp.getCpAsume();
    this.cpImporte = fp.getCpImporte();
    this.urbanizacionAsume = fp.getUrbanizacionAsume();
    this.urbanizacionImporte = fp.getUrbanizacionImporte();

    this.importeEntregaEfectivo = fp.getImporteEntregaEfectivo();
    this.entregaBienesLibres = fp.getEntregaDeBienesLibres();

    // DD
    this.encargoDDL = fp.getFechaEncargoDDL();
    this.recepcionDDL = fp.getFechaRecepcionDDL();
    this.estadoDDL = fp.getEstadoDDL() != null ? new CatalogoMinInfoDTO(fp.getEstadoDDL()) : null;

    this.encargoDDT = fp.getFechaEncargoDDT();
    this.recepcionDDT = fp.getFechaRecepcionDDT();
    this.estadoDDT = fp.getEstadoDDT() != null ? new CatalogoMinInfoDTO(fp.getEstadoDDT()) : null;

    this.despachoLegal = fp.getDespachoLegal();
    this.despachoTecnico = fp.getDespachoTecnico();

    this.ddRecibidaYSubida = fp.getDdRevisadaYSubida();
    this.subidaDD = fp.getFechaSubidaDD();
    this.honorariosDD = fp.getHonorariosDD();
    this.honorariosAsistenciaFirma = fp.getHonorariosAsistenciaFirma();

    this.valoracionDDCliente = fp.getValoracionDDCliente();
    this.horariosDDTecnica = fp.getHorariosDDTecnica();

    // Contactos
    Usuario gestorForm = null;
    String gestorFormLeg = null;
    if (p.getExpediente() != null){
      Expediente ex = p.getExpediente();
      gestorForm = ex.getUsuarioByPerfil("Gestor formalización");
      gestorFormLeg = ex.getUsuariosByPerfil("Soporte formalización");
    } else if (p.getExpedientePosesionNegociada() != null){
      ExpedientePosesionNegociada ex = p.getExpedientePosesionNegociada();
      gestorForm = ex.getUsuarioByPerfil("Gestor formalización");
      gestorFormLeg = ex.getUsuariosByPerfil("Soporte formalización");
    }
    if (gestorFormLeg != null)
      this.gestorFormalizacionLegal = gestorFormLeg;
    if (gestorForm != null)
      this.gestorFormalizacionPrincipal = new UsuarioAgendaDto(gestorForm);
    this.gestorConcursos = fp.getGestorConcursos() != null ? new CatalogoMinInfoDTO(fp.getGestorConcursos()) : null;
    this.gestorGestoria = fp.getGestorGestoria() != null ? new CatalogoMinInfoDTO(fp.getGestorGestoria()) : null;
    if (p.getExpediente() != null){
      Expediente e = p.getExpediente();
      this.idExpedienteOrigen = e.getIdCarga();
      Usuario u = e.getGestor();
      if (u != null)
        this.gestorRecuperacionHaya = new UsuarioAgendaDto(u);
    }
    this.gestorRecuperacionCliente = fp.getGestorRecuperacionCliente();
    this.tel1GestorRecuperacionCliente = fp.getTelefono1GesRecCli();

    this.territorial = fp.getTerritorial();
    this.analistaCliente = fp.getAnalistaCliente();
    this.tel1AnalistaCliente = fp.getAnalistaCliente();
    this.gestorFormalizacionLetradoCliente = fp.getGestorFormalizacionCliente();
    this.tel1GestorFormalizacionLetradoCliente = fp.getTelefono1GesFormalizacionCliente();

    this.notario = fp.getNotario();
    this.direccionNotaria = fp.getDireccionNotaria();
    this.provinciaNotaria = fp.getProvinciaNotaria() != null ? new CatalogoMinInfoDTO(fp.getProvinciaNotaria()) : null;
    this.municipioNotaria = fp.getMunicipioNotaria() != null ? new CatalogoMinInfoDTO(fp.getMunicipioNotaria()) : null;
    this.oficialNotaria = fp.getOficinaNotaria();
    this.telefonoNotaria = fp.getTelefonoNotaria();
    this.telefonoOficialNotaria = fp.getTelefonoOficialNotaria();
    this.emailOficialNotaria = fp.getEmailOficialNotaria();
    this.contactoOficina = fp.getContactoOficinaCliente();

    this.gestoria = fp.getGestoriaTramitadora();

    //Sareb
    this.nbv = fp.getNbv();
    this.tecleoEnPrisma = fp.getFechaTecleoEnPrisma();
    this.elevacionNilo = fp.getFechaElevacionNILO();
    this.cierreNilo = fp.getFechaCierreNILO();
  }
}
