package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;

import com.haya.alaska.tasacion.domain.Tasacion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionTasacionOutputDTO implements Serializable {
  private Integer id;

  private String tasadora;
  private Double importeTasacion;
  private Date fechaTasacion;

  public FormalizacionTasacionOutputDTO(Tasacion t){
    this.id = t.getId();

    this.tasadora = t.getTasadora();
    this.importeTasacion = t.getImporte();
    this.fechaTasacion = t.getFecha();
  }
}
