package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionContratoIntervinienteInputDTO implements Serializable {
  private String id;

  private Integer tipoIntervencion; // TipoIntervencion
  private Integer ordenIntervencion;
}
