package com.haya.alaska.formalizacion.infrastructure.controller.dto;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.ocupante.domain.Ocupante;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DatosIntervinientesFormalizacionOutputDTO implements Serializable {
  private Integer idInterviniente;
  private String nombre;
  private Boolean contactado;
  private String numeroDocumento;
  private String telefono;
  private Boolean vulnerabilidad;
  private CatalogoMinInfoDTO otrasSituacionesDeVulnerabilidad;
  //TODO: Mirar como se usan ahora
    /*private CatalogoMinInfoDTO tipoIntervencion;
    private Integer ordenIntervencion;*/

  public DatosIntervinientesFormalizacionOutputDTO(Interviniente interviniente) {
    String nombre = "";
    if (interviniente.getPersonaJuridica()) {
      nombre = interviniente.getRazonSocial() != null ? interviniente.getNombre() : "";
    } else {
      String nom = interviniente.getNombre() != null ? interviniente.getNombre() : "";
      String ape = interviniente.getApellidos() != null ? interviniente.getApellidos() : "";
      nombre = nom + " " + ape;
    }
    this.setNombre(nombre);
    this.setContactado(interviniente.getContactado());
    this.setNumeroDocumento(interviniente.getNumeroDocumento());
    this.setTelefono(interviniente.getTelefonoPrincipal());
    this.setVulnerabilidad(interviniente.getVulnerabilidad() != null ? interviniente.getVulnerabilidad() : null);
    this.setOtrasSituacionesDeVulnerabilidad(interviniente.getOtrasSituacionesVulnerabilidad() != null ? new CatalogoMinInfoDTO(interviniente.getOtrasSituacionesVulnerabilidad()) : null);
        /*this.tipoIntervencion = interviniente.getTipoIntervencion() != null ?
                new CatalogoMinInfoDTO(interviniente.getTipoIntervencion()) : null;
        this.setOrdenIntervencion(interviniente.getOrdenIntervencion());*/
    this.setIdInterviniente(interviniente.getId());
  }

  public DatosIntervinientesFormalizacionOutputDTO(Ocupante ocupante) {
    this.setNombre(ocupante.getNombreCompleto());
    this.setNumeroDocumento(ocupante.getDni());
    //meterle al dto un telefono, sacado de la lista de los datos de contacto
    for (DatoContacto dc : ocupante.getDatosContacto()) {
      if (dc.getOrden().equals(1) && dc.getMovil() != null)
        this.setTelefono(dc.getMovil());
      else if (dc.getOrden().equals(1) && dc.getFijo() != null)
        this.setTelefono(dc.getFijo());
    }
    this.setOtrasSituacionesDeVulnerabilidad(ocupante.getOtrasSituacionesVulnerabilidad() != null ? new CatalogoMinInfoDTO(ocupante.getOtrasSituacionesVulnerabilidad()) : null);
        /*this.tipoIntervencion = interviniente.getTipoIntervencion() != null ?
                new CatalogoMinInfoDTO(interviniente.getTipoIntervencion()) : null;
        this.setOrdenIntervencion(interviniente.getOrdenIntervencion());*/
    this.setIdInterviniente(ocupante.getId());
  }
}
