package com.haya.alaska.formalizacion.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class InformeInputDTO implements Serializable {
  private Integer idPropuesta;
  private Integer tipoPlantilla;

  private List<Integer> idsBienes;

  private List<String> destinatariosList;
  private String destinatarios;

  private String comentario;

  private String tramiteDacion;
  private String hipotecasSareb;
  private String otrasCargas;
  private String arrendamientos;
  private String informeOcupacional;

  private String ibi;
  private Boolean ibiB;
  private String comunidad;
  private Boolean comunidadB;
  private String urbanisticas;
  private Boolean urbanisticasB;
  private String otrasTasas;
  private Boolean otrasTasasB;
  private String gastos;
  private Boolean gastosB;

  private String comunicacionElecnor;
  private String otrasInformaciones;
  private String observacionesTexto;

  private Boolean escrituraCopia;
  private Boolean certificadoTasacion;
  private Boolean diligenciaEmbargos;
  private Boolean lpo;
  private Boolean facturas;
  private Boolean llaves;

  private Double presupuestoDDL;
  private Double presupuestoAsistFirma;
}
