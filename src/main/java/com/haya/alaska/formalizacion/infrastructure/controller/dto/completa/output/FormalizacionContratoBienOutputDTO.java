package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionContratoBienOutputDTO implements Serializable {
  private Integer idBien;
  private Integer idContrato;

  private CatalogoMinInfoDTO tipoGarantia;
  private Boolean colateral;

  public FormalizacionContratoBienOutputDTO(ContratoBien cb){
    this.idBien = cb.getBien() != null ? cb.getBien().getId() : null;
    this.idContrato = cb.getContrato() != null ? cb.getContrato().getId() : null;

    this.tipoGarantia = cb.getTipoGarantia() != null ? new CatalogoMinInfoDTO(cb.getTipoGarantia()) : null;
    this.colateral = cb.getEstado();
  }
}
