package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.formalizacion.domain.FormalizacionBien;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionBusquedaOutputDTO implements Serializable {
  private Integer idExpediente;
  private String idConcatenado;
  private String titular;
  private String nif;

  private CatalogoMinInfoDTO estado;
  private CatalogoMinInfoDTO detalleEstado;

  private Date fechaFirma;
  private String horaFirma;
  private String formalizacionHaya;
  private UsuarioAgendaDto gestorHaya;
  private String formalizacionCliente;

  private Date fechaSancion;

  private Double importeDacion;
  private Double importeDeuda;

  private CatalogoMinInfoDTO provincia;
  private CatalogoMinInfoDTO alquiler;

  public FormalizacionBusquedaOutputDTO(Expediente e){
    Contrato c = e.getContratoRepresentante();
    Interviniente i = null;
    if (c != null) i = c.getPrimerInterviniente();
    idExpediente = e.getId();
    idConcatenado = e.getIdConcatenado();
    gestorHaya =  e.getGestor() != null ? new UsuarioAgendaDto(e.getGestor()) : null;
    if (i != null){
      titular = i.getNombreReal();
      nif = i.getNumeroDocumento();
    }
    Propuesta p = null;
    for (Propuesta pr : e.getPropuestas()){
      if (p == null) {
        p = pr;
        continue;
      }
      if (p.getDeudaActual() < pr.getDeudaActual())
        p = pr;
    }

    if (p != null){
      Formalizacion f = p.getFormalizaciones().stream().findFirst().orElse(new Formalizacion());
      estado = p.getEstadoPropuesta() != null ? new CatalogoMinInfoDTO(p.getEstadoPropuesta()) : null;
      detalleEstado = f.getSubestadoFormalizacion() != null ? new CatalogoMinInfoDTO(f.getSubestadoFormalizacion()) : null;
      fechaFirma = f.getFechaFirma();
      horaFirma = f.getHoraFirma() != null ? f.getHoraFirma().toString() : null;
      Usuario form = e.getUsuarioByPerfil("Gestor formalización");
      formalizacionHaya = form != null ? form.getNombre() : null;
      formalizacionCliente = f.getGestorFormalizacionCliente();
      fechaSancion = f.getFechaSancion();
      provincia = f.getProvinciaNotaria() != null ? new CatalogoMinInfoDTO(f.getProvinciaNotaria()) : null;

      Double dacion = 0.0;
      Double deuda = 0.0;
      for (Contrato co : p.getContratos()){
        Saldo s = co.getUltimoSaldo();
        if (s != null){
          if (s.getDeudaImpagada() != null)
            deuda += s.getDeudaImpagada();
          if (s.getSaldoGestion() != null)
            dacion += s.getSaldoGestion();
        }
      }
      importeDacion = dacion;  //sumatorio saldo de los contratos de la propuesta
      importeDeuda = deuda;  //sumatorio importe deuda de los contratos de la propuesta

      Bien b = null;
      for (PropuestaBien pb : p.getBienes()){
        Bien bi = pb.getBien();
        if (bi != null){
          if (b == null){
            b = bi;
            continue;
          }
          if (b.getImporteBien() < bi.getImporteBien())
            b = bi;
        }
      }

      if (b != null){
        FormalizacionBien fb = b.getFormalizacionBien();
        if (fb != null)
          alquiler = fb.getAlquiler() != null ? new CatalogoMinInfoDTO(fb.getAlquiler()) : null;  //bien de mayor valor
      }
    }
  }
}
