package com.haya.alaska.formalizacion.infrastructure.repository;

import com.haya.alaska.formalizacion.domain.FormalizacionBien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FormalizacionBienRepository extends JpaRepository<FormalizacionBien, Integer> {
  Optional<FormalizacionBien> findByBienId(Integer id);
}
