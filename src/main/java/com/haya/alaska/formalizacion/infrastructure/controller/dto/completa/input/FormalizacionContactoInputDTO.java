package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionContactoInputDTO implements Serializable {
  private Integer id;
  private String idInterviniente;

  private Integer tipo; // TipoIntervencion
  private String valor;
  private Integer orden;
}
