package com.haya.alaska.formalizacion.infrastructure.controller.dto;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.tasacion.domain.Tasacion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DatosColateralesFormalizacionOutputDTO implements Serializable {
    private Integer idBien;
    private CatalogoMinInfoDTO tipoActivo;
    private String fincaRegistral;
    private String registroPropiedad;
    private Integer cargas;
    private String direccion;
    private CatalogoMinInfoDTO poblacion;
    private CatalogoMinInfoDTO provincia;
    private String tasadora;
    private Date fechaTasacion;
    private Double valorTasacion;

    public DatosColateralesFormalizacionOutputDTO(PropuestaBien propuestaBien){
        var bien = propuestaBien.getBien();
        this.tipoActivo = bien.getTipoActivo() != null ?
                new CatalogoMinInfoDTO(bien.getTipoActivo()) : null;
        this.setFincaRegistral(bien.getFinca());
        this.setRegistroPropiedad(bien.getRegistro());
        this.setCargas(bien.getCargas() != null ? bien.getCargas().size() : null);
        this.setDireccion(bien.getComentarioDireccion());
        this.poblacion = bien.getMunicipio() != null ?
                new CatalogoMinInfoDTO(bien.getMunicipio()) : null;
        this.provincia = bien.getProvincia() != null ?
                new CatalogoMinInfoDTO(bien.getProvincia()) : null;
        List<Tasacion> tasaciones = new ArrayList<>(bien.getTasaciones());
        Tasacion tas = tasaciones.stream().max(Comparator.comparing(Tasacion::getFecha)).orElse(null);
        if (tas != null){
            this.setTasadora(tas.getTasadora());
            this.setFechaTasacion(tas.getFecha());
            this.setValorTasacion(tas.getImporte());
        }

        this.setIdBien(bien.getId());
    }
}
