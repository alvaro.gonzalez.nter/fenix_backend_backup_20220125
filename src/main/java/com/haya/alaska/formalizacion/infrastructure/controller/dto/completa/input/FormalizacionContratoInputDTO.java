package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionContratoInputDTO implements Serializable {
  private Integer id;

  private String idCarga;
  private Integer estado; // EstadoContrato
  private Integer producto; // Producto
  private Boolean titulizado;
  private Double saldoGestion;
  private Double precioCompra;
  private Double importeDeuda;
  private Double quita;
  private Double restoDeudaReclamable;
  private Double importePrestamoInicial;
  private Double importeRetroactividad;
}
