package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.formalizacion.domain.FormalizacionProcedimiento;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionJudicialOutputDTO implements Serializable {
  private Integer id;

  private CatalogoMinInfoDTO estadoProcedimiento; // EstadoProcedimiento
  private CatalogoMinInfoDTO tipoProcedimiento; // TipoProcedimiento
  private CatalogoMinInfoDTO hitoProcedimiento; // HitoProcedimiento

  private Boolean concurso;
  private Boolean litigio;
  private Boolean auto;
  private Boolean testimonio;

  private String nAuto;
  private String nJuzgado;
  private String plaza;
  private Boolean dispone;

  public FormalizacionJudicialOutputDTO(Procedimiento p, FormalizacionProcedimiento fp){
    this.id = p.getId();

    this.estadoProcedimiento = p.getEstado() != null ? new CatalogoMinInfoDTO(p.getEstado()) : null;
    this.tipoProcedimiento = p.getTipo() != null ? new CatalogoMinInfoDTO(p.getTipo()) : null;
    this.hitoProcedimiento = p.getHitoProcedimiento() != null ? new CatalogoMinInfoDTO(p.getHitoProcedimiento()) : null;

    this.concurso = fp.getConcurso();
    this.litigio = fp.getLitigio();
    this.auto = fp.getAuto();
    this.testimonio = fp.getTestimonio();

    this.nAuto = p.getNumeroAutos();
    this.nJuzgado = p.getJuzgado();
    this.plaza = p.getPlaza();
    this.dispone = fp.getDisponible();
  }
}
