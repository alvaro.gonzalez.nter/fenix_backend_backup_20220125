package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionPropuestaInputDTO implements Serializable {
  private Integer id;

  // Avance de Gestion
  private String idCarga;

  private Integer clasePropuesta; // ClasePropuesta
  private Integer tipoPropuesta; // TopoPropuesta
  private Integer subtipoPropuesta; // SubtipoPropuesta
  private Integer estadoPropuesta; // EstadoPropuesta // DEPRECATED
  private Integer subestadoPropuesta; // SubestadoFormalizacion // DEPRECATED

  private Date fechaFirma;
  private String horaFirma;
  private Date fechaFirmaEstimada;
  private Boolean confirmadaFechaYHora;

  private Date fechaEntrada;
  private Date fechaSancion;

  private Integer tipoSancion; // SancionPropuesta
  private Date vencimientoSancion;

  private Boolean documentacionCompleta;
  private Date fechaKODocumentacion;
  private Date fechaOKDocumentacion;

  private Date solicitudMinuta;
  private Date recepcionMinuta;
  private Date revisionMinuta;
  private Date envioMinutaNotaria;
  private Date recepcionMinutaNotaria;
  private Date envioMinutaCliente;
  private Date okMinuta;

  private Date solicitudGestion;
  private Boolean informeVisitaOk;

  private Date fechaComunicacion;
  private Date fechaEntradaTF;
  private Date fechaAvanceTareaTF;

  private Integer numeroProtocolo;
  private String apoderadosFirma;
  private String observaciones;

  private Integer recomendacionNoFirma; // RecomendacionNoFirma
  private Integer motivoRecomendacionNoFirma; // MotivoRecomendacionNoFirma

  private Date minutaRevisadaFiscal;

  private Date fechaRevisionConcursos;

  // Principal
  private String expedienteOrigen;
  private Boolean dacionSingular;

  private Integer totalFFRR;

  private String alertaComunicacion;
  private Integer comprador; // Compradoe

  private Boolean penRatVen;
  private Boolean penRatCom;
  private Boolean penRatBan;
  private Boolean firmCon;

  private Boolean pbc;
  private Date fechaVigencia;

  private Boolean embargoOtrosAsime;
  private Double embargoOtrosImporte;

  private String observacionesPrincipal;

  private Integer formaDePago; // FormaDePago

  private Boolean gastosAsume;
  private Double gastosImporte;
  private Boolean plusvaliaAsume;
  private Double plusvaliaImporte;
  private Boolean ibiAsume;
  private Double ibiImporte;
  private Boolean cpAsume;
  private Double cpImporte;
  private Boolean urbanizacionAsume;
  private Double urbanizacionImporte;

  private Double importeEntregaEfectivo;
  private Boolean entregaBienesLibres;

  // DD
  private Date encargoDDL;
  private Date recepcionDDL;
  private Integer estadoDDL; // EstadoDdlDdt

  private Date encargoDDT;
  private Date recepcionDDT;
  private Integer estadoDDT; // EstadoDdlDdt

  private String despachoLegal;
  private String despachoTecnico;

  private Boolean ddRecibidaYSubida;
  private Date subidaDD;
  private Double honorariosDD;
  private Double honorariosAsistenciaFirma;

  private String valoracionDDCliente;
  private String horariosDDTecnica;

  // Contactos
  //private Integer gestorFormalizacionLegal; // Usuario
  //private Integer gestorFormalizacionPrincipal;
  private Integer gestorConcursos; // GestorConcursos
  private Integer gestorGestoria; // GestorGestoria
  //private Integer gestorRecuperacionHaya; // Usuario
  private String gestorRecuperacionCliente;
  private String tel1GestorRecuperacionCliente;

  private String territorial;
  private String analistaCliente;
  private String tel1AnalistaCliente;
  private String gestorFormalizacionLetradoCliente;
  private String tel1GestorFormalizacionLetradoCliente;

  private String notario;
  private String direccionNotaria;
  private Integer provinciaNotaria; // Provincia
  private Integer municipioNotaria; // Municipio
  private String oficialNotaria;
  private String telefonoNotaria;
  private String telefonoOficialNotaria;
  private String emailOficialNotaria;
  private String contactoOficina;

  private String gestoria; // GestorConcursos

  //Sareb
  private Double nbv;
  private Date tecleoEnPrisma;
  private Date elevacionNilo;
  private Date cierreNilo;

  //Comentario
  private String comentario;
}
