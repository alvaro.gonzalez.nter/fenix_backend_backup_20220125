package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.input;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionCargaInputDTO implements Serializable {
  private Integer id;
  private List<Integer> bienesIds;
  private List<String> bienes;

  private Integer tipoCarga; // TipoCarga

  private Double importeCarga;
  private Boolean cargaPropia;
  private Integer tipoCargaFormalizacion; // TipoCargaFormalizacion
  private Integer subtipoCargaFormalizacion; // SubtipoCargaFormalizacion

  private Double deudaIbi;
  private Boolean certificadoIbi;

  private Double deudaCp;
  private Boolean certificadoCp;
  private String contactoCp;

  private Double deudaUrbanizacion;
  private Boolean certificadoUrbanizacion;
}
