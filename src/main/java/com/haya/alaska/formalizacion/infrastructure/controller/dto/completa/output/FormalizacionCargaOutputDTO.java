package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;

import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.formalizacion.domain.FormalizacionCarga;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionCargaOutputDTO implements Serializable {
  private Integer id;

  private CatalogoMinInfoDTO tipoCarga; // TipoCarga

  private Double importeCarga;
  private Boolean cargaPropia;
  private CatalogoMinInfoDTO tipoCargaFormalizacion; // TipoCargaFormalizacion
  private CatalogoMinInfoDTO subtipoCargaFormalizacion; // SubtipoCargaFormalizacion

  private Double deudaIbi;
  private Boolean certificadoIbi;

  private Double deudaCp;
  private Boolean certificadoCp;
  private String contactoCp;

  private Double deudaUrbanizacion;
  private Boolean certificadoUrbanizacion;

  public FormalizacionCargaOutputDTO(Carga c, FormalizacionCarga fc){
    this.id = c.getId();

    this.tipoCarga = c.getTipoCarga() != null ? new CatalogoMinInfoDTO(c.getTipoCarga()) : null;

    this.importeCarga = c.getImporte();
    this.cargaPropia = c.getCargaPropia();
    this.tipoCargaFormalizacion = c.getTipoCargaFormalizacion() != null ? new CatalogoMinInfoDTO(c.getTipoCargaFormalizacion()) : null;
    this.subtipoCargaFormalizacion = c.getSubtipoCargaFormalizacion() != null ? new CatalogoMinInfoDTO(c.getSubtipoCargaFormalizacion()) : null;

    this.deudaIbi = fc.getDeudaIBI();
    this.certificadoIbi = fc.getCertificadoIBI();

    this.deudaCp = fc.getDeudaCP();
    this.certificadoCp = fc.getCertificadoCP();
    this.contactoCp = fc.getContactoCP();

    this.deudaUrbanizacion = fc.getDeudaUrbanizacion();
    this.certificadoUrbanizacion = fc.getCertificadoUrbanizacion();
  }
}
