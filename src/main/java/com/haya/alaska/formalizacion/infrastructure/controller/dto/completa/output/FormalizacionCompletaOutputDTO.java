package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FormalizacionCompletaOutputDTO implements Serializable {
  FormalizacionPropuestaOutputDTO propuesta;
  List<FormalizacionContratoOutputDTO> contratos;
  List<FormalizacionIntervinienteOutputDTO> intervinientes;
  List<FormalizacionBienOutputDTO> bienes;
  FormalizacionIntervinienteOutputDTO intervinientePrincipal;
  FormalizacionJudicialOutputDTO mayorHito;
}
