package com.haya.alaska.formalizacion.infrastructure.repository;

import com.haya.alaska.formalizacion.domain.FormalizacionCarga;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FormalizacionCargaRepository extends JpaRepository<FormalizacionCarga, Integer> {
    Optional<FormalizacionCarga> findByCargaId(Integer id);
}
