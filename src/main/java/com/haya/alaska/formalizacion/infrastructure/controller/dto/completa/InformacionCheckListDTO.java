package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa;

import com.haya.alaska.informacion_check_list.domain.CheckListDouble;
import com.haya.alaska.informacion_check_list.domain.CheckListInteger;
import com.haya.alaska.informacion_check_list.domain.CheckListString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class InformacionCheckListDTO implements Serializable {
  Integer idPropuesta;

  CheckListString sancionAceptadaDeudor = new CheckListString();
  CheckListString lpo = new CheckListString();
  CheckListString cfo = new CheckListString();
  CheckListString arrendamientos = new CheckListString();
  CheckListString finanzasArrendamientosDepositadas = new CheckListString();
  CheckListString ocupaciones = new CheckListString();
  CheckListDouble deudaPendienteEnComPropietarios = new CheckListDouble();
  CheckListDouble deudaPendienteEUC = new CheckListDouble();
  CheckListDouble deudaPendienteEnImpuestosYTasasMunicipales = new CheckListDouble();
  CheckListInteger cargasUrbanisticas = new CheckListInteger();
  CheckListDouble deudaPorCargasUrbanisticas = new CheckListDouble();
  CheckListString embargos = new CheckListString();
  CheckListString hipotecas = new CheckListString();
  CheckListString plusvaliaCargoDelAdquirente = new CheckListString();
  CheckListDouble gastosDeFormalizacion = new CheckListDouble();
  CheckListDouble previsionDeGastos = new CheckListDouble();
  CheckListDouble aportacionDeEfectivo = new CheckListDouble();
  CheckListDouble aportacionDeActivos = new CheckListDouble();
  CheckListDouble condonacionDeRemanente = new CheckListDouble();
  CheckListString operacionConcursal = new CheckListString();
  CheckListDouble operacionConcursalRiesgo = new CheckListDouble();
  CheckListString operacionConcursalDispone = new CheckListString();
  CheckListString altaEnPrisma = new CheckListString();
  CheckListString checkListSustituta = new CheckListString();
  CheckListString listaDeContraste = new CheckListString();
}
