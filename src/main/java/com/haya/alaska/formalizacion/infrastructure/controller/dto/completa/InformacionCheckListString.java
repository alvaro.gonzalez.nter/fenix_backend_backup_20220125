package com.haya.alaska.formalizacion.infrastructure.controller.dto.completa;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class InformacionCheckListString implements Serializable {
  private String sancionDelExpediente;
  private String importeEstimado;
  private String documentoCertificadoDisponible;
}
