package com.haya.alaska.formalizacion.infrastructure.util;

import com.haya.alaska.alquiler.domain.Alquiler;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.datos_check_list.domain.DatosCheckList;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.formalizacion.domain.*;
import com.haya.alaska.formalizacion.domain.excel.*;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.InformeInputDTO;
import com.haya.alaska.formalizacion.infrastructure.repository.FormalizacionBienRepository;
import com.haya.alaska.formalizacion.infrastructure.repository.FormalizacionCargaRepository;
import com.haya.alaska.formalizacion.infrastructure.repository.FormalizacionContratoRepository;
import com.haya.alaska.formalizacion.infrastructure.repository.FormalizacionProcedimientoRepository;
import com.haya.alaska.importe_propuesta.domain.ImportePropuesta;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.shared.ExcelExport;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import com.haya.alaska.tasacion.domain.Tasacion;
import com.haya.alaska.tipo_activo.domain.TipoActivo;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import com.haya.alaska.tipo_propuesta.domain.TipoPropuesta;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.valoracion.domain.Valoracion;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Component
public class FormalizacionExport {


  private static final SimpleDateFormat dateFormatSave = new SimpleDateFormat("ddMMyyyy");
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


  @Autowired
  FormalizacionContratoRepository formalizacionContratoRepository;
  @Autowired
  FormalizacionBienRepository formalizacionBienRepository;
  @Autowired
  FormalizacionCargaRepository formalizacionCargaRepository;
  @Autowired
  FormalizacionProcedimientoRepository formalizacionProcedimientoRepository;
  @Autowired
  ExpedienteUtil expedienteUtil;
  @Autowired
  BienRepository bienRepository;


  public ResponseEntity<InputStreamResource> wordExportBombin(Map<String, String> mapaBombin) throws IOException {
    File file = new File("src/main/java/com/haya/alaska/formalizacion/infrastructure/documents/PlantillaInformeCambioBombin.docx");
    FileInputStream fileInputStream = new FileInputStream(file);
    XWPFDocument plantilla = new XWPFDocument(fileInputStream);


    for (XWPFParagraph p : plantilla.getParagraphs()) {
      replace2(p, mapaBombin);
    }
    for (XWPFTable tbl : plantilla.getTables()) {
      for (XWPFTableRow row : tbl.getRows()) {
        for (XWPFTableCell cell : row.getTableCells()) {
          for (XWPFParagraph p : cell.getParagraphs()) {
            replace2(p, mapaBombin);
          }
        }
      }
    }

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      plantilla.write(outputStream);
      ByteArrayInputStream result = new ByteArrayInputStream(outputStream.toByteArray());
      HttpHeaders headers = new HttpHeaders();
      headers.add("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      headers.add("Content-Disposition", "attachment; filename=" + "informe-cambio-bombin-" + mapaBombin.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".docx");
      return ResponseEntity
        .ok()
        .headers(headers)
        .body(new InputStreamResource(result));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }


  public ResponseEntity<byte[]> pdfExportBombin(Map<String, String> mapaBombin) throws IOException {
    File file = new File("src/main/java/com/haya/alaska/formalizacion/infrastructure/documents/PlantillaInformeCambioBombin.docx");
    FileInputStream fileInputStream = new FileInputStream(file);
    XWPFDocument plantilla = new XWPFDocument(fileInputStream);

    for (XWPFParagraph p : plantilla.getParagraphs()) {
      replace2(p, mapaBombin);
    }
    for (XWPFTable tbl : plantilla.getTables()) {
      for (XWPFTableRow row : tbl.getRows()) {
        for (XWPFTableCell cell : row.getTableCells()) {
          for (XWPFParagraph p : cell.getParagraphs()) {
            replace2(p, mapaBombin);
          }
        }
      }
    }

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      plantilla.write(outputStream);

      plantilla = new XWPFDocument(new ByteArrayInputStream(outputStream.toByteArray()));
      PdfOptions options = PdfOptions.create();
      PdfConverter converter = (PdfConverter) PdfConverter.getInstance();
      converter.convert(plantilla, new FileOutputStream("pdf/informe-cambio-bombin-" + mapaBombin.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".pdf"), options);
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_PDF);
      headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
      headers.add("Content-Disposition", "attachment; filename=" + "informe-cambio-bombin-" + mapaBombin.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".pdf");
      Path pdfPath = Paths.get("pdf/informe-cambio-bombin-" + mapaBombin.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".pdf");
      byte[] pdf = Files.readAllBytes(pdfPath);

      ResponseEntity<byte[]> response = new ResponseEntity<>(pdf, headers, HttpStatus.OK);
      return response;

    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public ResponseEntity<InputStreamResource> wordExportFormalizacion(Map<String, String> mapaFormalizacion) throws IOException {
    File file = new File("src/main/java/com/haya/alaska/formalizacion/infrastructure/documents/PlantillaComunicacionFormalizacionWORD.docx");
    FileInputStream fileInputStream = new FileInputStream(file);
    XWPFDocument plantilla = new XWPFDocument(fileInputStream);


    for (XWPFParagraph p : plantilla.getParagraphs()) {
      replace2(p, mapaFormalizacion);
    }
    for (XWPFTable tbl : plantilla.getTables()) {
      for (XWPFTableRow row : tbl.getRows()) {
        for (XWPFTableCell cell : row.getTableCells()) {
          for (XWPFTable tbl1 : cell.getTables()) {
            for (XWPFTableRow row1 : tbl1.getRows()) {
              for (XWPFTableCell cell1 : row1.getTableCells()) {
                for (XWPFTable tbl2 : cell1.getTables()) {
                  for (XWPFTableRow row2 : tbl2.getRows()) {
                    for (XWPFTableCell cell2 : row2.getTableCells()) {
                      for (XWPFParagraph p : cell2.getParagraphs()) {
                        replace2(p, mapaFormalizacion);
                      }
                    }
                  }
                }
                for (XWPFParagraph p : cell1.getParagraphs()) {
                  replace2(p, mapaFormalizacion);
                }
              }
            }
          }
          for (XWPFParagraph p : cell.getParagraphs()) {
            replace2(p, mapaFormalizacion);
          }
        }
      }
    }

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      plantilla.write(outputStream);

      FileOutputStream fos=new FileOutputStream("arhivo.zip");
      ZipOutputStream zipOutputStream=new ZipOutputStream(fos);


      ByteArrayInputStream result = new ByteArrayInputStream(outputStream.toByteArray());
      HttpHeaders headers = new HttpHeaders();
      headers.add("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      headers.add("Content-Disposition", "attachment; filename=" + "informe-formalizacion-" + mapaFormalizacion.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".docx");
      headers.add("nombre","informe-formalizacion-" + mapaFormalizacion.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".docx");
      return ResponseEntity
        .ok()
        .headers(headers)
        .body(new InputStreamResource(result));


    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public ResponseEntity<InputStreamResource> pdfExportFormalizacion(Map<String, String> mapaFormalizacion) throws IOException {
    File file = new File("src/main/java/com/haya/alaska/formalizacion/infrastructure/documents/PlantillaComunicacionFormalizacionPDF.docx");
    FileInputStream fileInputStream = new FileInputStream(file);
    XWPFDocument plantilla = new XWPFDocument(fileInputStream);

    for (XWPFParagraph p : plantilla.getParagraphs()) {
      replace2(p, mapaFormalizacion);
    }
    for (XWPFTable tbl : plantilla.getTables()) {
      for (XWPFTableRow row : tbl.getRows()) {
        for (XWPFTableCell cell : row.getTableCells()) {
          for (XWPFTable tbl1 : cell.getTables()) {
            for (XWPFTableRow row1 : tbl1.getRows()) {
              for (XWPFTableCell cell1 : row1.getTableCells()) {
                for (XWPFTable tbl2 : cell1.getTables()) {
                  for (XWPFTableRow row2 : tbl2.getRows()) {
                    for (XWPFTableCell cell2 : row2.getTableCells()) {
                      for (XWPFParagraph p : cell2.getParagraphs()) {
                        replace2(p, mapaFormalizacion);
                      }
                    }
                  }
                }
                for (XWPFParagraph p : cell1.getParagraphs()) {
                  replace2(p, mapaFormalizacion);
                }
              }
            }
          }
          for (XWPFParagraph p : cell.getParagraphs()) {
            replace2(p, mapaFormalizacion);
          }
        }
      }
    }

   // FileOutputStream fos=new FileOutputStream("arhivo.zip");
    //ZipOutputStream zipOutputStream=new ZipOutputStream(fos);


    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      plantilla.write(outputStream);

      plantilla = new XWPFDocument(new ByteArrayInputStream(outputStream.toByteArray()));
      PdfOptions options = PdfOptions.create();
      PdfConverter converter = (PdfConverter) PdfConverter.getInstance();
      converter.convert(plantilla, new FileOutputStream("pdf/informe-formalizacion-" + mapaFormalizacion.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".pdf"), options);
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_PDF);
      headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
      headers.add("Content-Disposition", "attachment; filename=" + "informe-formalizacion-" + mapaFormalizacion.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".pdf");
      headers.add("nombre","informe-formalizacion-" + mapaFormalizacion.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".pdf");
      Path pdfPath = Paths.get("pdf/informe-formalizacion-" + mapaFormalizacion.get("idBien") + "-" + dateFormatSave.format(System.currentTimeMillis()) + ".pdf");
      byte[] pdf = Files.readAllBytes(pdfPath);
      InputStream input=new ByteArrayInputStream(pdf);


      return ResponseEntity
        .ok()
        .headers(headers)
        .body(new InputStreamResource(input));

    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  private void replace2(XWPFParagraph p, Map<String, String> data) {
    String pText = p.getText(); // complete paragraph as string
    if (pText.contains("${")) { // if paragraph does not include our pattern, ignore
      TreeMap<Integer, XWPFRun> posRuns = getPosToRuns(p);
      Pattern pat = Pattern.compile("\\$\\{(.+?)\\}");
      Matcher m = pat.matcher(pText);
      while (m.find()) { // for all patterns in the paragraph
        String g = m.group(1);  // extract key start and end pos
        int s = m.start(1);
        int e = m.end(1);
        String key = g;
        String x = data.get(key);
        if (x == null)
          x = "";
        SortedMap<Integer, XWPFRun> range = posRuns.subMap(s - 2, true, e + 1, true); // get runs which contain the pattern
        boolean found1 = false; // found $
        boolean found2 = false; // found {
        boolean found3 = false; // found }
        XWPFRun prevRun = null; // previous run handled in the loop
        XWPFRun found2Run = null; // run in which { was found
        int found2Pos = -1; // pos of { within above run
        for (XWPFRun r : range.values()) {
          if (r == prevRun)
            continue; // this run has already been handled
          if (found3)
            break; // done working on current key pattern
          prevRun = r;
          for (int k = 0; ; k++) { // iterate over texts of run r
            if (found3)
              break;
            String txt = null;
            try {
              txt = r.getText(k); // note: should return null, but throws exception if the text does not exist
            } catch (Exception ex) {

            }
            if (txt == null)
              break; // no more texts in the run, exit loop
            if (txt.contains("$") && !found1) {  // found $, replace it with value from data map
              txt = txt.replaceFirst("\\$", x);
              found1 = true;
            }
            if (txt.contains("{") && !found2 && found1) {
              found2Run = r; // found { replace it with empty string and remember location
              found2Pos = txt.indexOf('{');
              txt = txt.replaceFirst("\\{", "");
              found2 = true;
            }
            if (found1 && found2 && !found3) { // find } and set all chars between { and } to blank
              if (txt.contains("}")) {
                if (r == found2Run) { // complete pattern was within a single run
                  txt = txt.substring(0, found2Pos) + txt.substring(txt.indexOf('}'));
                } else // pattern spread across multiple runs
                  txt = txt.substring(txt.indexOf('}'));
              } else if (r == found2Run) // same run as { but no }, remove all text starting at {
                txt = txt.substring(0, found2Pos);
              else
                txt = ""; // run between { and }, set text to blank
            }
            if (txt.contains("}") && !found3) {
              txt = txt.replaceFirst("\\}", "");
              found3 = true;
            }
            r.setText(txt, k);
          }
        }
      }
      System.out.println(p.getText());

    }

  }


  private TreeMap<Integer, XWPFRun> getPosToRuns(XWPFParagraph paragraph) {
    int pos = 0;
    TreeMap<Integer, XWPFRun> map = new TreeMap<Integer, XWPFRun>();
    for (XWPFRun run : paragraph.getRuns()) {
      String runText = run.text();
      if (runText != null && runText.length() > 0) {
        for (int i = 0; i < runText.length(); i++) {
          map.put(pos + i, run);
        }
        pos += runText.length();
      }

    }
    return map;
  }

  public LinkedHashMap<String, List<Collection<?>>> createFormalizaciónCompletaForExcel(){
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> avanceGestion = new ArrayList<>();
    avanceGestion.add(AvanceDeGestionExcel.cabeceras);
    List<Collection<?>> principal = new ArrayList<>();
    principal.add(PrincipalExcel.cabeceras);
    List<Collection<?>> dd = new ArrayList<>();
    dd.add(DDExcel.cabeceras);
    List<Collection<?>> contactos = new ArrayList<>();
    contactos.add(ContactosExcel.cabeceras);
    List<Collection<?>> litigio = new ArrayList<>();
    litigio.add(FormalizacionProcedimientoExcel.cabeceras);
    List<Collection<?>> contratos = new ArrayList<>();
    contratos.add(FormalizacionContratoExcel.cabeceras);
    List<Collection<?>> bienes = new ArrayList<>();
    bienes.add(FormalizacionBienExcel.cabeceras);
    List<Collection<?>> cargas = new ArrayList<>();
    cargas.add(FormalizacionCargaExcel.cabeceras);
    List<Collection<?>> intervinientes = new ArrayList<>();
    intervinientes.add(FormalizacionIntervinienteExcel.cabeceras);
    List<Collection<?>> contactosInter = new ArrayList<>();
    contactosInter.add(FormalizacionContactoExcel.cabeceras);
    List<Collection<?>> sareb = new ArrayList<>();
    sareb.add(SarebExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("Advance Managment", avanceGestion);
      datos.put("General Conditions of Operation", principal);
      datos.put("DD", dd);
      datos.put("Contacts", contactos);
      datos.put("Litigation", litigio);
      datos.put("Economic Data - Contracts", contratos);
      datos.put("Goods", bienes);
      datos.put("Loads", cargas);
      datos.put("Interveners", intervinientes);
      datos.put("Intervener Contact", contactosInter);
      datos.put("Sareb", sareb);
    } else {
      datos.put("Avance de Gestión", avanceGestion);
      datos.put("Condiciones Generales Operacion", principal);
      datos.put("DD", dd);
      datos.put("Contactos", contactos);
      datos.put("Litigio", litigio);
      datos.put("Datos Económicos - Contratos", contratos);
      datos.put("Bienes", bienes);
      datos.put("Cargas", cargas);
      datos.put("Intervinientes", intervinientes);
      datos.put("Contacto Interviniente", contactosInter);
      datos.put("Sareb", sareb);
    }

    return datos;
  }

  public LinkedHashMap<String, List<Collection<?>>> ccsExcel(){
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> datosGeneral = new ArrayList<>();
    datosGeneral.add(SarebActivosExcel.cabeceras);
    List<Collection<?>> detalleDeuda = new ArrayList<>();
    detalleDeuda.add(DetalleDeudaExcel.sobreCabecera());
    detalleDeuda.add(DetalleDeudaExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("General Data of Assets", datosGeneral);
      datos.put("Debt Detail", detalleDeuda);
    } else {
      datos.put("Datos Generales de los Activos", datosGeneral);
      datos.put("Detalle Deuda", detalleDeuda);
    }

    return datos;
  }

  public LinkedHashMap<String, List<Collection<?>>> checkListExcel(){
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> datosGeneral = new ArrayList<>();
    datosGeneral.add(CheckListExcel.cabeceras);

    datos.put("CheckList", datosGeneral);

    return datos;
  }

  public LinkedHashMap<String, List<Collection<?>>> caExcel(){
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> datosGeneral = new ArrayList<>();
    datosGeneral.add(SarebActivosExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("General Data of Assets", datosGeneral);
    } else {
      datos.put("Datos Generales de los Activos", datosGeneral);
    }

    return datos;
  }

  public ByteArrayInputStream mapearExcelCcs(Propuesta p) throws IOException {
    LinkedHashMap<String, List<Collection<?>>> datos = ccsExcel();
    addBienExcel(p, datos);

    ByteArrayInputStream excel = this.createCCS(datos);

    return excel;
  }

  public ByteArrayInputStream mapearExcelCA(Propuesta p) throws IOException {
    LinkedHashMap<String, List<Collection<?>>> datos = caExcel();
    addBienExcel(p, datos);

    ExcelExport excelExport = new ExcelExport();
    ByteArrayInputStream excel = excelExport.create(datos);

    return excel;
  }

  private void addBienExcel(Propuesta p, LinkedHashMap<String, List<Collection<?>>> datos){
    List<Collection<?>> datosGeneral;
    List<Collection<?>> detalleDeuda;

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datosGeneral = datos.get("General Data of Assets");
      detalleDeuda = datos.get("Debt Detail");
    } else {
      datosGeneral = datos.get("Datos Generales de los Activos");
      detalleDeuda = datos.get("Detalle Deuda");
    }

    Double valTas = 0.0;
    Double impSan = 0.0;
    Double entrDin = 0.0;
    Double imporQui = 0.0;
    Double restoHip = 0.0;

    for (PropuestaBien pb : p.getBienes()){
      Bien b = pb.getBien();
      if (b != null){
        FormalizacionBien fb = b.getFormalizacionBien() != null ? b.getFormalizacionBien() : new FormalizacionBien();
        datosGeneral.add(new SarebActivosExcel(b, fb, pb.getContrato()).getValuesList());

        if (!b.getTasaciones().isEmpty() || !b.getValoraciones().isEmpty()){
          Tasacion t = b.getUltimaTasacion();
          Valoracion v = b.getUltimaValoracion();
          if (t != null && v != null){
            if (t.getFecha().after(v.getFecha())) valTas += t.getImporte();
            if (t.getFecha().before(v.getFecha())) valTas += v.getImporte();
          } else if (t != null){
            valTas += t.getImporte();
          } else if (v != null){
            valTas += v.getImporte();
          }
        }
      }
    }

    for (ImportePropuesta ip : p.getImportesPropuestas()){
      if (ip.getImporteEntregaDineraria() != null) entrDin += ip.getImporteEntregaDineraria();
      if (ip.getCondonacionDeuda() != null) imporQui += ip.getCondonacionDeuda();
    }

    if (detalleDeuda != null){
      for (Contrato c : p.getContratos()){
        detalleDeuda.add(new DetalleDeudaExcel(c).getValuesList());
        if (c.getRestoHipotecario() != null) restoHip += c.getRestoHipotecario();
      }

      detalleDeuda.add(new DetalleDeudaExcel("VALOR DE TASACIÓN", elNotCie(valTas)).getValuesList());
      detalleDeuda.add(new DetalleDeudaExcel("PRECIO DE COMPRA", elNotCie(impSan)).getValuesList());
      detalleDeuda.add(new DetalleDeudaExcel("APORTACIÓN MONETARIA", elNotCie(entrDin)).getValuesList());
      detalleDeuda.add(new DetalleDeudaExcel("CONDONACIÓN", elNotCie(imporQui)).getValuesList());
      detalleDeuda.add(new DetalleDeudaExcel("REMANENTE DE DEUDA", elNotCie(restoHip)).getValuesList());
    }

  }

  public static String elNotCie(double Número) {

    return new DecimalFormat("###,###.####################################").format(Número);

  }

  public void addPropuestaForExcel(Propuesta p, LinkedHashMap<String, List<Collection<?>>> datos){
    List<Collection<?>> avanceGestion;
    List<Collection<?>> principal;
    List<Collection<?>> dd;
    List<Collection<?>> contactos;
    List<Collection<?>> litigio;
    List<Collection<?>> contratos;
    List<Collection<?>> bienes;
    List<Collection<?>> cargas;
    List<Collection<?>> intervinientes;
    List<Collection<?>> contactosInter;
    List<Collection<?>> sareb;

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      avanceGestion = datos.get("Advance Managment");
      principal = datos.get("General Conditions of Operation");
      dd = datos.get("DD");
      contactos = datos.get("Contacts");
      litigio = datos.get("Litigation");
      contratos = datos.get("Economic Data - Contracts");
      bienes = datos.get("Goods");
      cargas = datos.get("Loads");
      intervinientes = datos.get("Interveners");
      contactosInter = datos.get("Intervener Contact");
      sareb = datos.get("Sareb");
    } else {
      avanceGestion = datos.get("Avance de Gestión");
      principal = datos.get("Condiciones Generales Operacion");
      dd = datos.get("DD");
      contactos = datos.get("Contactos");
      litigio = datos.get("Litigio");
      contratos = datos.get("Datos Económicos - Contratos");
      bienes = datos.get("Bienes");
      cargas = datos.get("Cargas");
      intervinientes = datos.get("Intervinientes");
      contactosInter = datos.get("Contacto Interviniente");
      sareb = datos.get("Sareb");
    }

    Formalizacion f = p.getFormalizaciones().stream().findFirst().orElse(new Formalizacion());
    avanceGestion.add(new AvanceDeGestionExcel(p, f).getValuesList());
    principal.add(new PrincipalExcel(p, f).getValuesList());
    dd.add(new DDExcel(p, f).getValuesList());
    contactos.add(new ContactosExcel(p, f).getValuesList());
    sareb.add(new SarebExcel(p, f).getValuesList());

    for (Contrato c : p.getContratos()){
      FormalizacionContrato fc = formalizacionContratoRepository.findByContratoId(c.getId()).orElse(new FormalizacionContrato());
      contratos.add(new FormalizacionContratoExcel(c, fc).getValuesList());
      for (Procedimiento pr : c.getProcedimientos()){
        FormalizacionProcedimiento fpr = formalizacionProcedimientoRepository.findByProcedimientoId(pr.getId()).orElse(new FormalizacionProcedimiento());
        litigio.add(new FormalizacionProcedimientoExcel(pr, fpr).getValuesList());
      }
    }

    for (PropuestaBien pb : p.getBienes()){
      Bien b = pb.getBien();
      if (b != null){
        FormalizacionBien fb = formalizacionBienRepository.findByBienId(b.getId()).orElse(new FormalizacionBien());
        for (ContratoBien cb : b.getContratos())
          bienes.add(new FormalizacionBienExcel(b, cb, fb).getValuesList());
        for (Carga c : b.getCargas()){
          FormalizacionCarga fc = formalizacionCargaRepository.findByCargaId(c.getId()).orElse(new FormalizacionCarga());
          cargas.add(new FormalizacionCargaExcel(b, c, fc).getValuesList());
        }
      }
    }

    for (Interviniente i : p.getIntervinientes()){
      for (ContratoInterviniente ci : i.getContratos())
        intervinientes.add(new FormalizacionIntervinienteExcel(i, ci).getValuesList());
      for (DatoContacto dc : i.getDatosContacto()){
        if (dc.getEmail() != null){
          contactosInter.add(new FormalizacionContactoExcel(i, dc).getValuesList());
          dc.setEmail(null);
        }
        if (dc.getFijo() != null){
          contactosInter.add(new FormalizacionContactoExcel(i, dc).getValuesList());
          dc.setFijo(null);
        }
        if (dc.getMovil() != null){
          contactosInter.add(new FormalizacionContactoExcel(i, dc).getValuesList());
          dc.setMovil(null);
        }
      }
    }
  }

  public void addCheckListExcel(List<DatosCheckList> checkLists, LinkedHashMap<String, List<Collection<?>>> datos){
    List<Collection<?>> avanceGestion;

    avanceGestion = datos.get("CheckList");

    for (int i = 0; i < checkLists.size(); i++){
      DatosCheckList dcl = checkLists.get(i);
      if (i == 0) {
        avanceGestion.add(new CheckListExcel("Respuestas: SI / NO / Autorizado/ No aplica").getValuesList());
        avanceGestion.add(new CheckListExcel("DOCUMENTACIÓN OPERACIÓN / CLIENTE", "Observaciones Formalización", "Observaciones Entradas").getValuesList());
      } else if (i == 7){
        avanceGestion.add(new CheckListExcel("DOCUMENTACIÓN INMUEBLE").getValuesList());
      } else if (i == 21){
        avanceGestion.add(new CheckListExcel("RESIDENCIAL (edificio terminado):").getValuesList());
      } else if (i == 29){
        avanceGestion.add(new CheckListExcel("LOCAL COMERCIAL / NAVE INDUSTRIAL").getValuesList());
      } else if (i == 31){
        avanceGestion.add(new CheckListExcel("OBRA EN CONSTRUCCIÓN").getValuesList());
      } else if (i == 39){
        avanceGestion.add(new CheckListExcel("SUELO URBANO / URBANIZABLE").getValuesList());
      } else if (i == 42){
        avanceGestion.add(new CheckListExcel("SUELO RÚSTICO").getValuesList());
      }
      avanceGestion.add(new CheckListExcel(dcl).getValuesList());
    }
  }

  public LinkedHashMap<String, List<Collection<?>>> excelBusquedaFormalizacion(List<Expediente> esps){
    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expedientes = new ArrayList<>();
    expedientes.add(FormalizacionBusquedaExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("Files", expedientes);
    } else {
      datos.put("Expedientes", expedientes);
    }

    for (Expediente e : esps){
      expedientes.add(new FormalizacionBusquedaExcel(e).getValuesList());
    }

    return datos;
  }

  public Map<String, String> getMapCA(Propuesta p, InformeInputDTO inport){
    Map<String, String> map = new HashMap<>();

    Expediente e = p.getExpediente();
    Cartera ca = null;
    Cliente cl = null;
    Contrato c = null;
    Interviniente i = null;

    Usuario gestor = null;
    if (e != null){
      gestor = e.getGestor();

      ca = e.getCartera();
      if (ca != null)
        cl = ca.getCliente();

      c = e.getContratoRepresentante();
      if (c != null) {
        i = c.getPrimerInterviniente();
      }
    }

    Formalizacion f = p.getFormalizaciones().stream().findFirst().orElse(new Formalizacion());
    TipoPropuesta tp = p.getTipoPropuesta();

    map.put("idPropuesta",  p.getIdCarga() != null ? p.getIdCarga() : p.getId().toString());

    map.put("cliente", cl != null ? cl.getNombre() : null);
    map.put("deudor", i != null ? i.getNombreReal() : null);  //primer titular del expediente
    if (tp != null){
      if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en"))
        map.put("tipoProp", tp.getValorIngles());
      else
        map.put("tipoProp", tp.getValor());
    }
    map.put("numColabora", p.getNumColabora() != null ? p.getNumColabora().toString() : null);
    map.put("fechaFirmaPrevista", f.getFechaFirmaEstimada() != null ? f.getFechaFirmaEstimada().toString() : null);
    map.put("notaria", f.getNotario());
    map.put("direccion", f.getDireccionNotaria());
    map.put("horaFirma", f.getHoraFirma() != null ? f.getHoraFirma().toString() : null);
    if (gestor != null){
      String total = "";
      if (gestor.getNombre() != null) total += gestor.getNombre();
      if (gestor.getTelefono() != null) total += "-" + gestor.getTelefono();
      if (gestor.getEmail() != null) total += "-" + gestor.getEmail();

      map.put("gestorEmailTelefono", total);
    }

    map.put("presDDLAMano", inport.getPresupuestoDDL() != null ? inport.getPresupuestoDDL().toString() : null);
    map.put("presFirmaAMano", inport.getPresupuestoAsistFirma() != null ? inport.getPresupuestoAsistFirma().toString() : null);

    return map;
  }

  public Map<String, String> getMapCCS(Propuesta p, InformeInputDTO inport) throws NotFoundException {
    Map<String, String> map = new HashMap<>();

    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();

    Expediente e = p.getExpediente();
    Contrato c = null;
    Interviniente i = null;
    Procedimiento pr = null;

    Usuario gestor = null;
    Usuario gForm = null;
    if (e != null){
      gestor = e.getGestor();
      gForm = e.getUsuarioByPerfil("Gestor formalización");

      pr = expedienteUtil.getUltimoProcedimiento(e);

      c = e.getContratoRepresentante();
      if (c != null) {
        i = c.getPrimerInterviniente();
      }
    }

    Formalizacion f = p.getFormalizaciones().stream().findFirst().orElse(new Formalizacion());

    TipoPropuesta tp = p.getTipoPropuesta();
    SubtipoPropuesta sp = p.getSubtipoPropuesta();
    String tipSubProp = "";
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      if (tp != null) tipSubProp += tp.getValorIngles();
      if (sp != null) tipSubProp += "/" + sp.getValorIngles();
    } else {
      if (tp != null) tipSubProp += tp.getValor();
      if (sp != null) tipSubProp += "/" + sp.getValor();
    }

    map.put("tipSubProp", tipSubProp);
    if (i != null)
      map.put("primTitRep", i.getNombreReal());
    if (pr != null){
      if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en"))
        map.put(
            "hitoProc",
            pr.getHitoProcedimiento() != null ? pr.getHitoProcedimiento().getValorIngles() : null);
      else
        map.put(
            "hitoProc",
            pr.getHitoProcedimiento() != null ? pr.getHitoProcedimiento().getValor() : null);
    }
    map.put("gestoriaTram", f.getGestoriaTramitadora());
    map.put("numColabora", p.getNumColabora() != null ? p.getNumColabora().toString() : null);
    map.put("tramiteAMano", inport.getTramiteDacion());

    if (gestor != null)
      map.put("gestor", gestor.getNombre());
    if (gForm != null)
      map.put("gestorForm", gForm.getNombre());

    map.put("fechForm", f.getFechaFirma() != null ? f.getFechaFirma().toString() : null);
    map.put("nProtocol", e.getIdCarga());
    map.put("notaria", f.getNotario());
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en"))
        map.put("localidadNotaria", f.getMunicipioNotaria() != null ? f.getMunicipioNotaria().getValorIngles() : null);
    else
        map.put("localidadNotaria", f.getMunicipioNotaria() != null ? f.getMunicipioNotaria().getValor() : null);

    Map<SubTipoBien, Integer> tiposActivos = new HashMap<>();
    Map<Municipio, Integer> municipios = new HashMap<>();
    Map<Provincia, Integer> provincias = new HashMap<>();
    Map<Alquiler, Integer> alquileres = new HashMap<>();

    Integer coutCarg = 0;
    Integer notCoutCarg = 0;

    Double sumDeudaImpContr = 0.0;
    Double sumImpEntreDinProp = 0.0;
    Double sumRestHipContr = 0.0;
    Double sumRemanContr = 0.0;
    Double sumUltTasValBien = 0.0;

    List<Bien> bienes = new ArrayList<>();
    if (inport.getIdsBienes() != null && !inport.getIdsBienes().isEmpty())
      bienes = bienRepository.findAllById(inport.getIdsBienes());
    else
      bienes = p.getBienes().stream().map(PropuestaBien::getBien).distinct().collect(Collectors.toList());

    for (Bien b : bienes){

      FormalizacionBien fb = b.getFormalizacionBien();
      if (fb != null){
        if (fb.getAlquiler() != null){
          if (alquileres.containsKey(fb.getAlquiler())){
            Integer cant = alquileres.get(fb.getAlquiler());
            cant ++;
            alquileres.put(fb.getAlquiler(), cant);
          } else {
            alquileres.put(fb.getAlquiler(), 1);
          }
        }
      }

      if (b.getCargas().isEmpty()) notCoutCarg++;
      else coutCarg++;

      if (!b.getTasaciones().isEmpty() || !b.getValoraciones().isEmpty()){
        Tasacion t = b.getUltimaTasacion();
        Valoracion v = b.getUltimaValoracion();
        if (t != null && v != null){
          if (t.getFecha().after(v.getFecha())) sumUltTasValBien += t.getImporte();
          if (t.getFecha().before(v.getFecha())) sumUltTasValBien += v.getImporte();
        } else if (t != null){
          sumUltTasValBien += t.getImporte();
        } else if (v != null){
          sumUltTasValBien += v.getImporte();
        }
      }

      if (b.getSubTipoBien() != null){
        if (tiposActivos.containsKey(b.getSubTipoBien())){
          Integer cant = tiposActivos.get(b.getSubTipoBien());
          cant ++;
          tiposActivos.put(b.getSubTipoBien(), cant);
        } else {
          tiposActivos.put(b.getSubTipoBien(), 1);
        }
      }

      if (b.getMunicipio() != null){
        if (municipios.containsKey(b.getMunicipio())){
          Integer cant = municipios.get(b.getMunicipio());
          cant ++;
          municipios.put(b.getMunicipio(), cant);
        } else {
          municipios.put(b.getMunicipio(), 1);
        }
      }

      if (b.getProvincia() != null){
        if (provincias.containsKey(b.getProvincia())){
          Integer cant = provincias.get(b.getProvincia());
          cant ++;
          provincias.put(b.getProvincia(), cant);
        } else {
          provincias.put(b.getProvincia(), 1);
        }
      }
    }

    for (Contrato co : p.getContratos()){
      Saldo sa = co.getUltimoSaldo();
      if (co.getRestoHipotecario() != null)
        sumRestHipContr += co.getRestoHipotecario();

      if (sa != null){
        if (sa.getDeudaImpagada() != null)
          sumDeudaImpContr += sa.getDeudaImpagada();
        if (sa.getCapitalPendiente() != null)
          sumRemanContr += sa.getCapitalPendiente();
      }
    }

    for (ImportePropuesta ip : p.getImportesPropuestas()){
      if (ip.getImporteEntregaDineraria() != null) sumImpEntreDinProp += ip.getImporteEntregaDineraria();
    }

    String tipoAct = "";
    for (Map.Entry<SubTipoBien, Integer> o : tiposActivos.entrySet()){
      SubTipoBien sb = o.getKey();
      TipoBien tb = sb.getTipoBien();
      if (defaultLocal == null || defaultLocal.equals("es")) tipoAct += tb.getValor() + "-" + sb.getValor();
      else if (defaultLocal.equals("en")) tipoAct += tb.getValorIngles() + "-" + sb.getValorIngles();
      tipoAct += ": " + o.getValue() + ", ";
    }
    map.put("countTipos", tipoAct);

    String munic = "";
    for (Map.Entry<Municipio, Integer> o : municipios.entrySet()){
      Municipio ta = o.getKey();
      if (defaultLocal == null || defaultLocal.equals("es")) munic += ta.getValor();
      else if (defaultLocal.equals("en")) munic += ta.getValorIngles();
      munic += ": " + o.getValue() + ", ";
    }
    map.put("countMuni", munic);

    String provi = "";
    for (Map.Entry<Provincia, Integer> o : provincias.entrySet()){
      Provincia ta = o.getKey();
      if (defaultLocal == null || defaultLocal.equals("es")) provi += ta.getValor();
      else if (defaultLocal.equals("en")) provi += ta.getValorIngles();
      provi += ": " + o.getValue() + ", ";
    }
    map.put("countProv", provi);

    map.put("countBien", String.valueOf(p.getBienes().size()));
    map.put("countConCarga", coutCarg.toString());
    map.put("countSinCarga", notCoutCarg.toString());

    map.put("sumDeudaImpContr", elNotCie(sumDeudaImpContr));
    map.put("impSancProp", p.getImporteTotal() != null ? elNotCie(p.getImporteTotal()) : null);
    map.put("sumImpEntreDinProp", elNotCie(sumImpEntreDinProp));
    map.put("sumRestHipContr", elNotCie(sumRestHipContr));
    map.put("sumRemanContr", elNotCie(sumRemanContr));
    map.put("sumUltTasValBien", elNotCie(sumUltTasValBien));

    map.put("hipotAMano", inport.getHipotecasSareb());
    map.put("otrasAMano", inport.getOtrasCargas());


    String alqui = "";
    for (Map.Entry<Alquiler, Integer> o : alquileres.entrySet()){
      Alquiler ta = o.getKey();
      if (defaultLocal == null || defaultLocal.equals("es")) alqui += ta.getValor();
      else if (defaultLocal.equals("en")) alqui += ta.getValorIngles();
      alqui += ": " + o.getValue() + ", ";
    }
    map.put("counAlquileres", alqui);
    map.put("arendAMano", inport.getArrendamientos());
    map.put("infOcuAMano", inport.getInformeOcupacional());

    map.put("ibiAMano", inport.getIbi());
    map.put("autoFich", inport.getIbiB() != null ? inport.getIbiB() ? "Si" : "No Aplica" : null);
    map.put("comuAMano", inport.getComunidad());
    map.put("comuFich", inport.getComunidadB() != null ? inport.getComunidadB() ? "Si" : "No Aplica" : null);
    map.put("urbaAMano", inport.getUrbanisticas());
    map.put("urbaFich", inport.getUrbanisticasB() != null ? inport.getUrbanisticasB() ? "Si" : "No Aplica" : null);
    map.put("otraAMano", inport.getOtrasTasas());
    map.put("otraFich", inport.getOtrasTasasB() != null ? inport.getOtrasTasasB() ? "Si" : "No Aplica" : null);
    map.put("gastAMano", inport.getGastos());
    map.put("gastFich", inport.getGastosB() != null ? inport.getGastosB() ? "Si" : "No Aplica" : null);

    if (pr != null && pr.getTipo() != null){
      if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en"))
        map.put("tipoProced", pr.getTipo() != null ? pr.getTipo().getValorIngles() : null);
      else
        map.put("tipoProced", pr.getTipo() != null ? pr.getTipo().getValor() : null);
    }

    map.put("comAMano", inport.getComunicacionElecnor());
    map.put("otraInfoAMano", inport.getOtrasInformaciones());

    map.put("correoAMano", inport.getObservacionesTexto());

    map.put("escCopSimBoolAMano", inport.getEscrituraCopia() != null ? inport.getEscrituraCopia() ? "Si" : "No" : null);
    map.put("certTasBoolAMano", inport.getCertificadoTasacion() != null ? inport.getCertificadoTasacion() ? "Si" : "No" : null);
    map.put("DiliEmbBoolAMano", inport.getDiligenciaEmbargos() != null ? inport.getDiligenciaEmbargos() ? "Si" : "No" : null);
    map.put("lpoBoolAMano", inport.getLpo() != null ? inport.getLpo() ? "Si" : "No" : null);
    map.put("factuBoolAMano", inport.getFacturas() != null ? inport.getFacturas() ? "Si" : "No" : null);
    map.put("llavesBoolAMano", inport.getLlaves() != null ? inport.getLlaves() ? "Si" : "No" : null);

    return map;
  }

  //Deprecated
  public Map<String, String> getMapCCR(Expediente e, Propuesta p){
    Map<String, String> map = new HashMap<>();

    Formalizacion f = p.getFormalizaciones().stream().findFirst().orElse(new Formalizacion());

    Cartera ca = e.getCartera();
    List<Cliente> cliS = ca.getClientes().stream().map(ClienteCartera::getCliente).collect(Collectors.toList());

    Locale loc = LocaleContextHolder.getLocale();
    String defaultLocal = loc.getLanguage();

    map.put("cliente", cliS.stream().map(Cliente::getNombre).collect(Collectors.joining(", ")));
    map.put("cartera", ca.getNombre());
    map.put("expediente", e.getIdCarga());
    map.put("idPropuesta", p.getIdCarga() != null ? p.getIdCarga() : p.getId().toString());

    String tipoProp = null;
    if (p.getTipoPropuesta() != null){
      if (defaultLocal == null || defaultLocal.equals("es")) tipoProp = p.getTipoPropuesta().getValor();
      else if (defaultLocal.equals("en")) tipoProp = p.getTipoPropuesta().getValorIngles();
    }
    map.put("tipoProp", tipoProp);

    String subtipoProp = null;
    if (p.getSubtipoPropuesta() != null){
      if (defaultLocal == null || defaultLocal.equals("es")) subtipoProp = p.getSubtipoPropuesta().getValor();
      else if (defaultLocal.equals("en")) subtipoProp = p.getSubtipoPropuesta().getValorIngles();
    }
    map.put("subtipoProp", subtipoProp);

    Interviniente in = null;
    for (Interviniente i : p.getIntervinientes()){
      for (Contrato co : p.getContratos()){
        ContratoInterviniente ci = i.getContratoInterviniente(co.getId());
        if (ci != null && ci.getTipoIntervencion().getCodigo().equals("1") && ci.getOrdenIntervencion().equals(1)){
          in = i;
          break;
        }
      }
      if (in != null) break;
    }

    if (in != null){
      map.put("titular", in.getNombreReal());
      DatoContacto dc = in.getDatoPrincipal();
      if (dc != null){
        String tipo = "";
        if (dc.getEmail() != null){
          tipo = "Email";
          map.put("valorContacto", dc.getEmail());
        }
        else if (dc.getMovil() != null){
          tipo = "Teléfono Móvil";
          map.put("valorContacto", dc.getMovil());
        }
        else if (dc.getFijo() != null){
          tipo = "Teléfono Fijo";
          map.put("valorContacto", dc.getFijo());
        }
        map.put("orderTipo", dc.getOrigen() + "/" + tipo);
      }
    }


    String comprador = null;
    if (f.getComprador() != null){
      if (defaultLocal == null || defaultLocal.equals("es")) comprador = f.getComprador().getValor();
      else if (defaultLocal.equals("en")) comprador = f.getComprador().getValorIngles();
    }
    map.put("comprador", comprador);

    String alquiler = null;
    if (f.getAlquiler() == null) alquiler = "Subrogado";
    else if (f.getAlquiler()) alquiler = "Si";
    else if (!f.getAlquiler()) alquiler = "No";
    map.put("alquiler", alquiler);

    Usuario gestor = e.getGestor();
    map.put("gestor", gestor != null ? gestor.getNombre() : null);

    Usuario form = e.getUsuarioByPerfil("Gestor formalización");
    map.put("gestorForm", form != null ? form.getNombre() : null);
    map.put("gestorRecCli", f.getGestorRecuperacionCliente());
    map.put("gestorFormCli", f.getGestorFormalizacionCliente());

    map.put("anaCli", f.getAnalistaCliente());
    map.put("contOfiCli", f.getContactoOficinaCliente());

    map.put("fechaFirm", f.getFechaFirma() != null ? f.getFechaFirma().toString() : "");
    map.put("nProtocol", e.getIdCarga());

    map.put("notario", f.getNotario());

    String provincia = "";
    if (f.getProvinciaNotaria() != null){
      if (defaultLocal == null || defaultLocal.equals("es")) provincia = f.getProvinciaNotaria().getValor();
      else if (defaultLocal.equals("en")) provincia = f.getProvinciaNotaria().getValorIngles();
    }
    String municipio = "";
    if (f.getMunicipioNotaria() != null){
      if (defaultLocal == null || defaultLocal.equals("es")) municipio = f.getMunicipioNotaria().getValor();
      else if (defaultLocal.equals("en")) municipio = f.getMunicipioNotaria().getValorIngles();
    }
    map.put("provinciaMunicipio", municipio + "(" + provincia + ")");

    map.put("contNota", f.getTelefonoNotaria() + "/" + f.getEmailOficialNotaria());
    map.put("totalFFRR", f.getTotalFFRR() != null ? f.getTotalFFRR().toString() : "");

    Map<TipoActivo, Integer> tiposActivos = new HashMap<>();
    Map<Municipio, Integer> municipios = new HashMap<>();
    Map<Provincia, Integer> provincias = new HashMap<>();
    Map<String, Integer> direcciones = new HashMap<>();

    List<String> referencias = new ArrayList<>();
    List<String> fincas = new ArrayList<>();
    List<String> registros = new ArrayList<>();

    List<String> tasVal = new ArrayList<>();

    for (PropuestaBien pb : p.getBienes()){
      Bien b = pb.getBien();

      if (!b.getTasaciones().isEmpty() || !b.getValoraciones().isEmpty()){
        Tasacion t = b.getUltimaTasacion();
        if (t == null){
          Valoracion v = b.getUltimaValoracion();
          if (v != null && v.getImporte() != null) tasVal.add(v.getImporte().toString());
        } else {
          if (t.getImporte() != null) tasVal.add(t.getImporte().toString());
        }
      }

      if (b.getTipoActivo() != null){
        if (tiposActivos.containsKey(b.getTipoActivo())){
          Integer cant = tiposActivos.get(b.getTipoActivo());
          cant ++;
          tiposActivos.put(b.getTipoActivo(), cant);
        } else {
          tiposActivos.put(b.getTipoActivo(), 1);
        }
      }

      if (b.getMunicipio() != null){
        if (municipios.containsKey(b.getMunicipio())){
          Integer cant = municipios.get(b.getMunicipio());
          cant ++;
          municipios.put(b.getMunicipio(), cant);
        } else {
          municipios.put(b.getMunicipio(), 1);
        }
      }

      if (b.getProvincia() != null){
        if (provincias.containsKey(b.getProvincia())){
          Integer cant = provincias.get(b.getProvincia());
          cant ++;
          provincias.put(b.getProvincia(), cant);
        } else {
          provincias.put(b.getProvincia(), 1);
        }
      }

      if (b.getNombreVia() != null){
        if (direcciones.containsKey(b.getNombreVia())){
          Integer cant = direcciones.get(b.getNombreVia());
          cant ++;
          direcciones.put(b.getNombreVia(), cant);
        } else {
          direcciones.put(b.getNombreVia(), 1);
        }
      }

      if (b.getReferenciaCatastral() != null) referencias.add(b.getReferenciaCatastral());
      if (b.getFinca() != null) fincas.add(b.getFinca());
      if (b.getRegistro() != null) registros.add(b.getRegistro());
    }

    String tipoAct = "";
    for (Map.Entry<TipoActivo, Integer> o : tiposActivos.entrySet()){
      TipoActivo ta = o.getKey();
      if (defaultLocal == null || defaultLocal.equals("es")) tipoAct += ta.getValor();
      else if (defaultLocal.equals("en")) tipoAct += ta.getValorIngles();
      tipoAct += ": " + o.getValue() + ", ";
    }
    map.put("tipoAct", tipoAct);

    String munic = "";
    for (Map.Entry<Municipio, Integer> o : municipios.entrySet()){
      Municipio ta = o.getKey();
      if (defaultLocal == null || defaultLocal.equals("es")) munic += ta.getValor();
      else if (defaultLocal.equals("en")) munic += ta.getValorIngles();
      munic += ": " + o.getValue() + ", ";
    }
    map.put("municipioBien", munic);

    String provi = "";
    for (Map.Entry<Provincia, Integer> o : provincias.entrySet()){
      Provincia ta = o.getKey();
      if (defaultLocal == null || defaultLocal.equals("es")) provi += ta.getValor();
      else if (defaultLocal.equals("en")) provi += ta.getValorIngles();
      provi += ": " + o.getValue() + ", ";
    }
    map.put("provinciaBien", provi);

    String direc = "";
    for (Map.Entry<String, Integer> o : direcciones.entrySet()){
      String ta = o.getKey();
      direc += ta + ": " + o.getValue() + ", ";
    }
    map.put("direccionesBien", direc);

    map.put("referencias", String.join(", ", referencias));
    map.put("fincas", String.join(", ", fincas));
    map.put("registros", String.join(", ", registros));
    map.put("tasVal", String.join(", ", tasVal));

    List<String> idContratos = new ArrayList<>();
    List<String> impDeu = new ArrayList<>();
    List<String> preCom = new ArrayList<>();
    List<String> impEnt = new ArrayList<>();

    //Map<String, Integer> gAsume = new HashMap<>();
    Map<String, Integer> pAsume = new HashMap<>();

    Contrato cP = null;
    Saldo sP = null;

    for (Contrato c : p.getContratos()){
      idContratos.add(c.getIdCarga());

      //FormalizacionContrato fc = c.getFormalizacionContrato();

      if (c.getImporteInicial() != null) impEnt.add(c.getImporteInicial().toString());

      Saldo s = c.getUltimoSaldo();
      if (s != null && s.getDeudaImpagada() != null)
        impDeu.add(s.getDeudaImpagada().toString());

      if (cP == null && s != null) {
        cP = c;
        sP = s;
      } else {
        if (s != null && s.getDeudaImpagada() != null && s.getDeudaImpagada() > sP.getDeudaImpagada()){
          cP = c;
          sP = s;
        }
      }

      /*if (fc != null){
        if (fc.getGastosAsume() != null){
          String key = fc.getGastosAsume() ? "Si" : "No";
          if (gAsume.containsKey(key)){
            Integer cant = gAsume.get(key);
            cant ++;
            gAsume.put(key, cant);
          } else {
            gAsume.put(key, 1);
          }
        }

        if (fc.getPlusvaliaAsume() != null){
          String key = fc.getPlusvaliaAsume() ? "Si" : "No";
          if (pAsume.containsKey(key)){
            Integer cant = pAsume.get(key);
            cant ++;
            pAsume.put(key, cant);
          } else {
            pAsume.put(key, 1);
          }
        }
      }*/

    }
    map.put("idsCont", String.join(", ", idContratos));
    map.put("impDeu", String.join(", ", impDeu));
    map.put("preCom", ""/*TODO*/);
    map.put("impEnt", String.join(", ", impEnt));

    Procedimiento prP = null;
    if (cP != null){
      if (!cP.getProcedimientos().isEmpty()){
        if (cP.getProcedimientos().size() == 1)
          prP = cP.getProcedimientos().stream().findFirst().orElse(null);
        else
          prP = cP.getProcedimientos().stream().min(Comparator.comparing(expedienteUtil::getEstado)).orElse(null);
      }
    }


    if (prP == null){
      map.put("nAutos", null);
      map.put("juzgados", null);
      map.put("plazas", null);
    } else {
      map.put("nAutos", prP.getNumeroAutos());
      map.put("judgados", prP.getJuzgado());
      map.put("plazas", prP.getPlaza());
    }

    map.put("gestoria", f.getGestoriaTramitadora());

    /*String gestorAsume = "";
    for (Map.Entry<String, Integer> o : gAsume.entrySet()){
      String ta = o.getKey();
      gestorAsume += ta + ": " + o.getValue() + ", ";
    }*/
    if (f.getGastosAsume() != null)
      map.put("gestorAsume", f.getGastosAsume() ? "Si" : "No");

    String plusvaliaAsume = "";
    for (Map.Entry<String, Integer> o : pAsume.entrySet()){
      String ta = o.getKey();
      plusvaliaAsume += ta + ": " + o.getValue() + ", ";
    }
    map.put("plusvaliaAsume", plusvaliaAsume);

    map.put("ratiComp", f.getPendienteRatificacionComprador() != null ? f.getPendienteRatificacionComprador() ? "Si" : "No" : null);
    map.put("ratiBank", f.getPendienteRatificacionBanco() != null ? f.getPendienteRatificacionBanco() ? "Si" : "No" : null);

    //TODO cargas y firma condicionada

    //FormalizacionContrato fc = cP.getFormalizacionContrato();
    //if (fc != null){
    map.put("ibiImp", f.getIbiAsume() ? "Si" : "No");
    map.put("ibiAsu", f.getIbiImporte() != null ? f.getIbiImporte().toString() : null);

    map.put("cpImp", f.getCpAsume() ? "Si" : "No");
    map.put("cpAsu", f.getCpImporte() != null ? f.getCpImporte().toString() : null);

    map.put("urbImp", f.getUrbanizacionAsume() ? "Si" : "No");
    map.put("urbAsu", f.getUrbanizacionImporte() != null ? f.getUrbanizacionImporte().toString() : null);

    map.put("contCP", ""/*TODO*/);
    //}

    map.put("fechaVisita", f.getFechaVigencia() != null ? f.getFechaVigencia().toString() : null);
    map.put("ems", ""/*TODO*/);
    map.put("ocupacion", ""/*TODO*/);

    map.put("observacion", ""/*TODO*/);

    return map;
  }

  public Map<String, String> getMapCAE(){
    Map<String, String> map = new HashMap<>();

    return map;
  }

  public Map<String, String> getMapCAA(){
    Map<String, String> map = new HashMap<>();

    return map;
  }

  public XWPFDocument obtenerDocumento(){
    return null;
  }

  private XWPFDocument formateoDocumentos(Map<String, String> mapaFormalizacion, String path) throws IOException {
    File file = new File(path);
    FileInputStream fileInputStream = new FileInputStream(file);
    XWPFDocument plantilla = new XWPFDocument(fileInputStream);


    for (XWPFParagraph p : plantilla.getParagraphs()) {
      replace2(p, mapaFormalizacion);
    }
    for (XWPFTable tbl : plantilla.getTables()) {
      for (XWPFTableRow row : tbl.getRows()) {
        for (XWPFTableCell cell : row.getTableCells()) {
          for (XWPFTable tbl1 : cell.getTables()) {
            for (XWPFTableRow row1 : tbl1.getRows()) {
              for (XWPFTableCell cell1 : row1.getTableCells()) {
                for (XWPFTable tbl2 : cell1.getTables()) {
                  for (XWPFTableRow row2 : tbl2.getRows()) {
                    for (XWPFTableCell cell2 : row2.getTableCells()) {
                      for (XWPFParagraph p : cell2.getParagraphs()) {
                        replace2(p, mapaFormalizacion);
                      }
                    }
                  }
                }
                for (XWPFParagraph p : cell1.getParagraphs()) {
                  replace2(p, mapaFormalizacion);
                }
              }
            }
          }
          for (XWPFParagraph p : cell.getParagraphs()) {
            replace2(p, mapaFormalizacion);
          }
        }
      }
    }

    return plantilla;
  }

  public ResponseEntity<byte[]> pdfCCD(Propuesta p, InformeInputDTO inport, MultipartFile[] files) throws IOException, NotFoundException {
    Map<String, String> mapaFormalizacion = getMapCCS(p, inport);

    XWPFDocument plantilla = formateoDocumentos(mapaFormalizacion, "src/main/java/com/haya/alaska/formalizacion/infrastructure/documents/plantillaComComSareb.docx");

    ResponseEntity<byte[]> pdf = convertirEnPDF(plantilla, mapaFormalizacion.get("idPropuesta"));

    ByteArrayInputStream excel = mapearExcelCcs(p);

    ResponseEntity<byte[]> result = getZip(pdf, excel, files, p.getIdCarga());

    return result;
  }

  public ResponseEntity<byte[]> pdfCAA(Propuesta p, InformeInputDTO inport, MultipartFile[] files) throws IOException, NotFoundException {
    Map<String, String> mapaFormalizacion = getMapCA(p, inport);

    XWPFDocument plantilla = formateoDocumentos(mapaFormalizacion, "src/main/java/com/haya/alaska/formalizacion/infrastructure/documents/plantillaCorAviAsis.docx");

    ResponseEntity<byte[]> pdf = convertirEnPDF(plantilla, mapaFormalizacion.get("idPropuesta"));

    ByteArrayInputStream excel = mapearExcelCA(p);

    ResponseEntity<byte[]> result = getZip(pdf, excel, files, p.getIdCarga());

    return result;
  }

  public ResponseEntity<byte[]> pdfCAE(Propuesta p, InformeInputDTO inport, MultipartFile[] files) throws IOException, NotFoundException {
    Map<String, String> mapaFormalizacion = getMapCA(p, inport);

    XWPFDocument plantilla = formateoDocumentos(mapaFormalizacion, "src/main/java/com/haya/alaska/formalizacion/infrastructure/documents/plantillaCorAviEncar.docx");

    ResponseEntity<byte[]> pdf = convertirEnPDF(plantilla, mapaFormalizacion.get("idPropuesta"));

    ByteArrayInputStream excel = mapearExcelCA(p);

    ResponseEntity<byte[]> result = getZip(pdf, excel, files, p.getIdCarga());

    return result;
  }

  public ResponseEntity<byte[]> pdfCCR(Expediente e, Propuesta p) throws IOException {
    Map<String, String> mapaFormalizacion = getMapCCR(e, p);

    XWPFDocument plantilla = formateoDocumentos(mapaFormalizacion, "src/main/java/com/haya/alaska/formalizacion/infrastructure/documents/plantillaComComForm.docx");

    return convertirEnPDF(plantilla, mapaFormalizacion.get("idPropuesta"));
  }

  public ResponseEntity<byte[]> convertirEnPDF(XWPFDocument plantilla, String nombre) throws IOException {
    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      plantilla.write(outputStream);
      plantilla.close();

      plantilla = new XWPFDocument(new ByteArrayInputStream(outputStream.toByteArray()));

      PdfOptions options = PdfOptions.create();
      PdfConverter converter = (PdfConverter) PdfConverter.getInstance();
      converter.convert(
        plantilla,
        new FileOutputStream(
          "src/main/java/com/haya/alaska/formalizacion/infrastructure/documents/Documento1.pdf"),
        options);
      plantilla.close();

      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_PDF);
      headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

      headers.add(
        "Content-Disposition",
        "attachment; filename="
          + "informe-formalizacion-"
          + nombre
          + "-"
          + new Date().toString()
          + ".pdf");

      Path pdfPath =
        Paths.get(
          "src/main/java/com/haya/alaska/formalizacion/infrastructure/documents/Documento1.pdf");
      byte[] pdf = Files.readAllBytes(pdfPath);

      ResponseEntity<byte[]> response = new ResponseEntity<>(pdf, headers, HttpStatus.OK);
      return response;

    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public ResponseEntity<byte[]> getZip(ResponseEntity<byte[]> pdf, ByteArrayInputStream excel, MultipartFile[] files, String nombre) throws IOException {
    String nBase = "Cimunicacion-Adquisicion-Formalizada_" + nombre + "_" + dateFormatSave.format(System.currentTimeMillis());

    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ZipOutputStream zos = new ZipOutputStream(bos);

    int count;
    byte data[] = new byte[2048];

    ZipEntry zip = new ZipEntry(nBase + ".pdf");
    zos.putNextEntry(zip);

    ByteArrayInputStream in = new ByteArrayInputStream(pdf.getBody());
    BufferedInputStream entrysteamPDF =
      new BufferedInputStream(in);
    while ((count = entrysteamPDF.read(data, 0, 2048)) != -1) {
      zos.write(data, 0, count);
    }
    entrysteamPDF.close();
    zos.closeEntry();

    ZipEntry zip2 = new ZipEntry(nBase + ".xlsx");
    zos.putNextEntry(zip2);

    BufferedInputStream entrysteamExcel =
      new BufferedInputStream(excel);
    while ((count = entrysteamExcel.read(data, 0, 2048)) != -1) {
      zos.write(data, 0, count);
    }
    entrysteamExcel.close();
    zos.closeEntry();

    for (MultipartFile f : files){
      ZipEntry zipF = new ZipEntry(f.getOriginalFilename());
      zos.putNextEntry(zipF);

      BufferedInputStream entrysteamF =
        new BufferedInputStream(f.getInputStream());
      while ((count = entrysteamF.read(data, 0, 2048)) != -1) {
        zos.write(data, 0, count);
      }
      entrysteamF.close();
      zos.closeEntry();
    }

    zos.close();
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
    headers.add("Content-Disposition", "attachment; filename=" + nBase + ".zip");
    return ResponseEntity.ok().headers(headers).body(bos.toByteArray());
  }

  public ByteArrayInputStream createCCS(LinkedHashMap<String, List<Collection<?>>> datos) throws IOException {
    Workbook workbook = new XSSFWorkbook();

    CellStyle cellStyleDate = workbook.createCellStyle();
    cellStyleDate.setDataFormat((short)14);


    CellStyle styleHeader = workbook.createCellStyle();
    Font font = workbook.createFont();
    font.setBold(true);
    font.setColor(IndexedColors.WHITE.getIndex());
    styleHeader.setFont(font);
    //styleHeader.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
    byte[] rgb = new byte[3];
    rgb[0] = (byte) 10; // red
    rgb[1] = (byte) 148; // green
    rgb[2] = (byte) 214; // blue
    //create XSSFColor
    XSSFColor color = new XSSFColor(rgb, new DefaultIndexedColorMap());
    XSSFCellStyle xssfcellcolorstyle = (XSSFCellStyle) styleHeader;
    xssfcellcolorstyle.setFillForegroundColor(color);
    xssfcellcolorstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

    for (var datosHoja : datos.entrySet()) {
      Sheet sheet = workbook.createSheet(datosHoja.getKey());
      int indiceFila = 0;
      for (Collection<?> datosFila : datosHoja.getValue()) {
        Row filaExcel = sheet.createRow(indiceFila);
        int indiceColumna = 0;
        for (Object dato : datosFila) {
          Cell celdaExcel = filaExcel.createCell(indiceColumna);
          if (indiceFila == 0) {
            celdaExcel.setCellStyle(xssfcellcolorstyle);
            sheet.setColumnWidth(indiceColumna, 15 * 256);
          }
          if ((sheet.getSheetName().equals("Debt Detail") || sheet.getSheetName().equals("Detalle Deuda")) && indiceFila == 1){
            celdaExcel.setCellStyle(xssfcellcolorstyle);
            sheet.setColumnWidth(indiceColumna, 15 * 256);
          }
          if (dato != null) {
            if (dato instanceof Date) {
              celdaExcel.setCellStyle(cellStyleDate);
              celdaExcel.setCellValue((Date) dato);
            } else if (dato instanceof Boolean) {
              if (dato == Boolean.TRUE) {
                if (LocaleContextHolder.getLocale().getLanguage() == null
                  || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
                  celdaExcel.setCellValue("Yes");
                } else {
                  celdaExcel.setCellValue("Sí");
                }
              } else if (dato == Boolean.FALSE) {
                celdaExcel.setCellValue("No");
              }
            } else if (dato instanceof Number) {
              celdaExcel.setCellValue(((Number) dato).doubleValue());
            } else if (dato instanceof Catalogo) {
              celdaExcel.setCellValue(((Catalogo) dato).getValor());
            } else if (dato.equals("")) {
              celdaExcel.setCellValue("-");
            } else {
              celdaExcel.setCellValue(dato.toString());
            }
          } else {
            celdaExcel.setCellValue("-");
          }
          indiceColumna++;
        }
        indiceFila++;
      }
    }

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      workbook.write(outputStream);
      return new ByteArrayInputStream(outputStream.toByteArray());
    }
  }

  public ByteArrayInputStream createCheckList(LinkedHashMap<String, List<Collection<?>>> datos) throws IOException {
    Workbook workbook = new XSSFWorkbook();

    CellStyle cellStyleDate = workbook.createCellStyle();
    cellStyleDate.setDataFormat((short)14);

    for (var datosHoja : datos.entrySet()) {
      Sheet sheet = workbook.createSheet(datosHoja.getKey());
      sheet.setColumnWidth(1, 24 * 256);
      sheet.setColumnWidth(2, 24 * 256);
      sheet.setColumnWidth(3, 24 * 256);
      sheet.setColumnWidth(4, 24 * 256);
      int indiceFila = 0;
      for (Collection<?> datosFila : datosHoja.getValue()) {
        Row filaExcel = sheet.createRow(indiceFila);
        int indiceColumna = 0;
        for (Object dato : datosFila) {
          Cell celdaExcel = filaExcel.createCell(indiceColumna);
          if (indiceFila == 0 && (indiceColumna == 1)) {
            XSSFCellStyle stile = getColor(workbook, 41, 65, 29, IndexedColors.WHITE.getIndex());
            celdaExcel.setCellStyle(stile);
            sheet.addMergedRegion(new CellRangeAddress(
              0, //first row (0-based)
              0, //last row  (0-based)
              1, //first column (0-based)
              2  //last column  (0-based)
            ));
          }
          if (indiceFila == 1 && indiceColumna == 1) {
            XSSFCellStyle stile = getColor(workbook, 196, 6, 6, IndexedColors.WHITE.getIndex());
            stile.setWrapText(true);
            celdaExcel.setCellStyle(stile);
            filaExcel.setHeight((short) 600);
          }
          if (indiceColumna == 1 && (indiceFila == 2 || indiceFila == 10 || indiceFila == 25 || indiceFila == 34 || indiceFila == 37 || indiceFila == 46 || indiceFila == 50)) {
            celdaExcel.setCellStyle(getColor(workbook, 84, 132, 53, IndexedColors.WHITE.getIndex()));
            sheet.addMergedRegion(new CellRangeAddress(
              indiceFila, //first row (0-based)
              indiceFila, //last row  (0-based)
              1, //first column (0-based)
              2  //last column  (0-based)
            ));
          }
          if (indiceColumna == 3 && (indiceFila == 0 || indiceFila == 2)) {
            celdaExcel.setCellStyle(getColor(workbook, 0, 0, 0, IndexedColors.WHITE.getIndex()));
          }
          if (indiceColumna == 4 && indiceFila == 2) {
            celdaExcel.setCellStyle(getColor(workbook, 0, 0, 0, IndexedColors.WHITE.getIndex()));
          }
          if (dato != null) {
            if (dato instanceof Date) {
              celdaExcel.setCellStyle(cellStyleDate);
              celdaExcel.setCellValue((Date) dato);
            } else if (dato instanceof Boolean) {
              if (dato == Boolean.TRUE) {
                if (LocaleContextHolder.getLocale().getLanguage() == null
                  || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
                  celdaExcel.setCellValue("Yes");
                } else {
                  celdaExcel.setCellValue("Sí");
                }
              } else if (dato == Boolean.FALSE) {
                celdaExcel.setCellValue("No");
              }
            } else if (dato instanceof Number) {
              celdaExcel.setCellValue(((Number) dato).doubleValue());
            } else if (dato instanceof Catalogo) {
              celdaExcel.setCellValue(((Catalogo) dato).getValor());
            } else if (dato.equals("")) {
              celdaExcel.setCellValue("-");
            } else {
              celdaExcel.setCellValue(dato.toString());
            }
          } else {
            celdaExcel.setCellValue("-");
          }
          indiceColumna++;
        }
        indiceFila++;
      }
    }

    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      workbook.write(outputStream);
      return new ByteArrayInputStream(outputStream.toByteArray());
    }
  }

  private XSSFCellStyle getColor(Workbook workbook, int red, int green, int blue, short fuente){
    CellStyle styleHeader = workbook.createCellStyle();
    Font font = workbook.createFont();
    font.setBold(true);
    font.setColor(fuente);
    styleHeader.setFont(font);

    byte[] rgb = new byte[3];
    rgb[0] = (byte) red; // red
    rgb[1] = (byte) green; // green
    rgb[2] = (byte) blue; // blue
    //create XSSFColor
    XSSFColor color = new XSSFColor(rgb, new DefaultIndexedColorMap());
    XSSFCellStyle xssfcellcolorstyle = (XSSFCellStyle) styleHeader;
    xssfcellcolorstyle.setFillForegroundColor(color);
    xssfcellcolorstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

    return xssfcellcolorstyle;
  }
}
