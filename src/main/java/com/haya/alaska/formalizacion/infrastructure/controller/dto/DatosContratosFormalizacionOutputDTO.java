package com.haya.alaska.formalizacion.infrastructure.controller.dto;


import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.formalizacion.infrastructure.controller.dto.completa.output.FormalizacionContratoOutputDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DatosContratosFormalizacionOutputDTO implements Serializable {
    private String idContrato;
    private Double importeDeuda;
    private Double importeDacion;
    private Double importeQuita;
    private Double restoDeudaReclamable;
    private Double importePrestamoInicial;
    private FormalizacionContratoOutputDTO formalizacion;

    public DatosContratosFormalizacionOutputDTO(Contrato contrato){
        this.setImporteDeuda(contrato.getSaldoContrato().getCapitalPendiente());
        // TODO: falta integracion con pipeline
        this.setImporteDacion(0.0);
        this.setImporteQuita(0.0);
        if (this.importeDeuda != null){
            Double restoDeudaReclamable = this.getImporteDeuda() - this.getImporteDacion() - this.getImporteQuita();
            this.setRestoDeudaReclamable(restoDeudaReclamable);
        } else {
            this.setRestoDeudaReclamable(null);
        }

        this.setImportePrestamoInicial(contrato.getImporteInicial());
        this.setIdContrato(contrato.getIdCarga());
    }
}
