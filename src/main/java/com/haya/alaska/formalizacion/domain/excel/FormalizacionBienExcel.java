package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.formalizacion.domain.FormalizacionBien;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.tasacion.domain.Tasacion;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class FormalizacionBienExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Loan No.");
      cabeceras.add("Detected Risks");
      cabeceras.add("Origin Id Good");
      cabeceras.add("Active Number Beech");
      cabeceras.add("Date Added");
      cabeceras.add("Mortgage Liability");
      cabeceras.add("Warranty Type");
      cabeceras.add("Active Type");
      cabeceras.add("Collateral / Free");
      cabeceras.add("Typology");
      cabeceras.add("Active Subtype");
      cabeceras.add("Idufir");
      cabeceras.add("Municipality");
      cabeceras.add("Province");
      cabeceras.add("Locality");
      cabeceras.add("Registry Property");
      cabeceras.add("Property Registry");
      cabeceras.add("Address");
      cabeceras.add("Cadastral Reference");
      cabeceras.add("Appraiser");
      cabeceras.add("Appraisal Value");
      cabeceras.add("Valuation Date");
      cabeceras.add("Rent");
      cabeceras.add("Occupation Status");
      cabeceras.add("Deposit Finance");
      cabeceras.add("Original Debt-Principal");
      cabeceras.add("Original Debt-Ordinary Interest");
      cabeceras.add("Original Debt-Default Interest");
      cabeceras.add("Mortgage Liability-Principal");
      cabeceras.add("Mortgage Liability-Ordinary Interests");
      cabeceras.add("Mortgage Liability-Default Interest");
      cabeceras.add("Consignment Risk");
    } else {
      cabeceras.add("Nº Préstamo");
      cabeceras.add("Riesgos Detectados");
      cabeceras.add("Id Origen Bien");
      cabeceras.add("Nº Activo Haya");
      cabeceras.add("Fecha Alta");
      cabeceras.add("Responsabilidad Hipotecaria");
      cabeceras.add("Tipo Garantía");
      cabeceras.add("Tipo Activo");
      cabeceras.add("Colateral/Libre");
      cabeceras.add("Tipología");
      cabeceras.add("Subtipo Activo");
      cabeceras.add("Idufir");
      cabeceras.add("Municipio");
      cabeceras.add("Provincia");
      cabeceras.add("Localidad");
      cabeceras.add("Finca Registral");
      cabeceras.add("Registro de la Propiedad");
      cabeceras.add("Dirección");
      cabeceras.add("Referencia Catastral");
      cabeceras.add("Tasadora");
      cabeceras.add("Valor de Tasación");
      cabeceras.add("Fecha Tasación");
      cabeceras.add("Alquiler");
      cabeceras.add("Estado Ocupación");
      cabeceras.add("Deposito Finanzas");
      cabeceras.add("Deuda Originaria-Principal");
      cabeceras.add("Deuda Originaria-Intereses Ordinarios");
      cabeceras.add("Deuda Originaria-Intereses de Demora");
      cabeceras.add("Responsabilidad Hipotecaria-Principal");
      cabeceras.add("Responsabilidad Hipotecaria-Intereses Ordinarios");
      cabeceras.add("Responsabilidad Hipotecaria-Intereses de Demora");
      cabeceras.add("Riesgo Consignación");
    }
  }

  public FormalizacionBienExcel(Bien b, ContratoBien cb, FormalizacionBien fb) {
    Contrato c = null;
    if (cb != null) c = cb.getContrato();
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      if (c != null) this.add("Loan No.", c.getIdCarga());
      this.add(
          "Detected Risks",
          fb.getRiesgosDetectados() != null ? fb.getRiesgosDetectados().getValorIngles() : null);
      this.add("Origin Id Good", b.getIdCarga());
      this.add("Active Number Beech", b.getIdHaya());
      this.add("Date Added", b.getFechaCarga());
      this.add("Mortgage Liability", b.getResponsabilidadHipotecaria());
      this.add(
          "Active Type", b.getTipoActivo() != null ? b.getTipoActivo().getValorIngles() : null);
      if (cb != null) {
        if (cb.getEstado() != null)
          this.add("Collateral / Free", cb.getEstado() ? "Active" : "Released");
        this.add(
            "Warranty Type",
            cb.getTipoGarantia() != null ? cb.getTipoGarantia().getValorIngles() : null);
      }
      this.add("Typology", b.getTipoBien() != null ? b.getTipoBien().getValorIngles() : null);
      this.add(
          "Active Subtype",
          b.getSubTipoBien() != null ? b.getSubTipoBien().getValorIngles() : null);
      this.add("Idufir", b.getIdufir());
      this.add("Municipality", b.getMunicipio() != null ? b.getMunicipio().getValorIngles() : null);
      this.add("Province", b.getProvincia() != null ? b.getProvincia().getValorIngles() : null);
      this.add("Locality", b.getLocalidad() != null ? b.getLocalidad().getValorIngles() : null);
      this.add("Registry Property", b.getFinca());
      this.add("Property Registry", b.getRegistro());
      this.add("Address", b.getNombreVia());
      this.add("Cadastral Reference", b.getReferenciaCatastral());
      if (b.getTasaciones() != null && !b.getTasaciones().isEmpty()) {
        List<Tasacion> tasaciones =
            b.getTasaciones().stream()
                .sorted(Comparator.comparing(Tasacion::getFecha).reversed())
                .collect(Collectors.toList());
        Tasacion t = tasaciones.get(0);
        this.add("Appraiser", t.getTasadora());
        this.add("Appraisal Value", t.getImporte());
        this.add("Valuation Date", t.getFecha());
      }
      this.add("Rent", fb.getAlquiler() != null ? fb.getAlquiler().getValorIngles() : null);
      this.add(
          "Occupation Status",
          b.getEstadoOcupacion() != null ? b.getEstadoOcupacion().getValorIngles() : null);
      this.add("Deposit Finance", fb.getDepositoFinanzas());
      this.add("Original Debt-Principal", fb.getDeudaOriginal_principal());
      this.add("Original Debt-Ordinary Interest", fb.getDeudaOriginal_interesesOrdinarios());
      this.add("Original Debt-Default Interest", fb.getDeudaOriginal_interesesDeDemora());
      this.add("Mortgage Liability-Principal", fb.getResponsabilidadHipotecaria_principal());
      this.add(
          "Mortgage Liability-Ordinary Interests",
          fb.getResponsabilidadHipotecaria_interesesOrdinarios());
      this.add("Mortgage Liability-Default Interest", fb.getDeudaOriginal_interesesDeDemora());
      this.add("Consignment Risk", fb.getRiesgoConsignacion());
    } else {
      if (c != null) this.add("Nº Préstamo", c.getIdCarga());
      this.add(
          "Riesgos Detectados",
          fb.getRiesgosDetectados() != null ? fb.getRiesgosDetectados().getValor() : null);
      this.add("Id Origen Bien", b.getIdCarga());
      this.add("Nº Activo Haya", b.getIdHaya());
      this.add("Fecha Alta", b.getFechaCarga());
      this.add("Responsabilidad Hipotecaria", b.getResponsabilidadHipotecaria());
      this.add("Tipo Activo", b.getTipoActivo() != null ? b.getTipoActivo().getValor() : null);
      if (cb != null) {
        if (cb.getEstado() != null)
          this.add("Colateral/Libre", cb.getEstado() ? "Activo" : "Liberado");
        this.add(
            "Tipo Garantía", cb.getTipoGarantia() != null ? cb.getTipoGarantia().getValor() : null);
      }
      this.add("Colateral/Libre", cb.getEstado() ? "Activo" : "Liberado");
      this.add("Tipología", b.getTipoBien() != null ? b.getTipoBien().getValor() : null);
      this.add("Subtipo Activo", b.getSubTipoBien() != null ? b.getSubTipoBien().getValor() : null);
      this.add("Idufir", b.getIdufir());
      this.add("Municipio", b.getMunicipio() != null ? b.getMunicipio().getValor() : null);
      this.add("Provincia", b.getProvincia() != null ? b.getProvincia().getValor() : null);
      this.add("Localidad", b.getLocalidad() != null ? b.getLocalidad().getValor() : null);
      this.add("Finca Registral", b.getFinca());
      this.add("Registro de la Propiedad", b.getRegistro());
      this.add("Dirección", b.getNombreVia());
      this.add("Referencia Catastral", b.getReferenciaCatastral());
      if (b.getTasaciones() != null && !b.getTasaciones().isEmpty()) {
        List<Tasacion> tasaciones =
            b.getTasaciones().stream()
                .sorted(Comparator.comparing(Tasacion::getFecha).reversed())
                .collect(Collectors.toList());
        Tasacion t = tasaciones.get(0);
        this.add("Tasadora", t.getTasadora());
        this.add("Valor de Tasación", t.getImporte());
        this.add("Fecha Tasación", t.getFecha());
      }
      this.add("Alquiler", fb.getAlquiler() != null ? fb.getAlquiler().getValor() : null);
      this.add(
          "Estado Ocupación",
          b.getEstadoOcupacion() != null ? b.getEstadoOcupacion().getValor() : null);
      this.add("Deposito Finanzas", fb.getDepositoFinanzas());
      this.add("Deuda Originaria-Principal", fb.getDeudaOriginal_principal());
      this.add("Deuda Originaria-Intereses Ordinarios", fb.getDeudaOriginal_interesesOrdinarios());
      this.add("Deuda Originaria-Intereses de Demora", fb.getDeudaOriginal_interesesDeDemora());
      this.add(
          "Responsabilidad Hipotecaria-Principal", fb.getResponsabilidadHipotecaria_principal());
      this.add(
          "Responsabilidad Hipotecaria-Intereses Ordinarios",
          fb.getResponsabilidadHipotecaria_interesesOrdinarios());
      this.add(
          "Responsabilidad Hipotecaria-Intereses de Demora",
          fb.getDeudaOriginal_interesesDeDemora());
      this.add("Riesgo Consignación", fb.getRiesgoConsignacion());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
