package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class AvanceDeGestionExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Origin Proposal");
      cabeceras.add("Class Proposal");
      cabeceras.add("Operation Type");
      cabeceras.add("Subtype Proposal");
      cabeceras.add("State");
      cabeceras.add("State Detail");
      cabeceras.add("Penalty Type");
      cabeceras.add("Date Sanction");
      cabeceras.add("Expiry Penalty");
      cabeceras.add("Entry Date");
      cabeceras.add("Full Documentation");
      cabeceras.add("OK Date Complete Documentation");
      cabeceras.add("KO Date Complete Documentation");
      cabeceras.add("Minute Request");
      cabeceras.add("Minute Reception Date");
      cabeceras.add("Minute Reviewed by Prosecutor");
      cabeceras.add("Contest Review Date");
      cabeceras.add("HRE Minute Revision Date");
      cabeceras.add("Minute Submission Date to Notary Public");
      cabeceras.add("Minute Reception Date (from Notary Public)");
      cabeceras.add("Minute Sending Date to Client for Validation");
      cabeceras.add("Date OK Minute");
      cabeceras.add("Asset Management Request View");
      cabeceras.add("Report Visit Ok");
      cabeceras.add("Estimated Signature Date");
      cabeceras.add("Signature Date");
      cabeceras.add("Signature Time");
      cabeceras.add("Communication Date");
      cabeceras.add("Protocol No.");
      cabeceras.add("TF Entry Date");
      cabeceras.add("Date Progress Task TF");
      cabeceras.add("Observations");
    } else {
      cabeceras.add("Propuesta Origen");
      cabeceras.add("Clase Propuesta");
      cabeceras.add("Tipo Operación");
      cabeceras.add("Subtipo Propuesta");
      cabeceras.add("Estado");
      cabeceras.add("Detalle Estado");
      cabeceras.add("Tipo Sanción");
      cabeceras.add("Fecha Sanción");
      cabeceras.add("Vencimiento Sanción");
      cabeceras.add("Fecha Entrada");
      cabeceras.add("Documentación Completa");
      cabeceras.add("Fecha OK Documentación Completa");
      cabeceras.add("Fecha KO Documentación Completa");
      cabeceras.add("Solicitud Minuta");
      cabeceras.add("Fecha Recepción Minuta");
      cabeceras.add("Minuta Revisada por Fiscal");
      cabeceras.add("Fecha Revision Concursos");
      cabeceras.add("Fecha Revision Minuta HRE");
      cabeceras.add("Fecha de Envío Minuta a Notaria");
      cabeceras.add("Fecha Recepción Minuta (de Notaria)");
      cabeceras.add("Fecha Envío Minuta a Cliente para Validación");
      cabeceras.add("Fecha OK Minuta");
      cabeceras.add("Solicitud Gestión Activos Vista");
      cabeceras.add("Informe Visita Ok");
      cabeceras.add("Fecha Firma Estimada");
      cabeceras.add("Fecha de Firma");
      cabeceras.add("Hora de Firma");
      cabeceras.add("Fecha Comunicación");
      cabeceras.add("Nº Protocolo");
      cabeceras.add("Fecha Entrada TF");
      cabeceras.add("Fecha Avance Tarea TF");
      cabeceras.add("Observaciones");
    }
  }

  public AvanceDeGestionExcel(Propuesta p, Formalizacion fp) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Origin Proposal", p.getIdCarga());

      this.add(
          "Class Proposal",
          p.getClasePropuesta() != null ? p.getClasePropuesta().getValor() : null);
      this.add(
          "Operation Type", p.getTipoPropuesta() != null ? p.getTipoPropuesta().getValor() : null);
      this.add(
          "Subtype Proposal",
          p.getSubtipoPropuesta() != null ? p.getSubtipoPropuesta().getValor() : null);
      this.add("State", p.getEstadoPropuesta() != null ? p.getEstadoPropuesta().getValor() : null);
      this.add(
          "State Detail",
          fp.getSubestadoFormalizacion() != null
              ? fp.getSubestadoFormalizacion().getValor()
              : null);

      this.add("Signature Date", fp.getFechaFirma());
      this.add("Signature Time", fp.getHoraFirma());
      this.add("Estimated Signature Date", fp.getFechaFirmaEstimada());
      /*if (fp.getConfirmadaFechaYHora() != null)
        this.add("Confirmed Date and Time", fp.getConfirmadaFechaYHora() ? "YES" : "NO");*/

      this.add("Entry Date", fp.getFechaEntrada());
      this.add("Date Sanction", p.getFechaSancion());
      this.add("Contest Review Date", fp.getFechaRevisionConcursos());
      this.add("HRE Minute Revision Date", fp.getRevisionMinutaHRE());

      this.add(
          "Penalty Type",
          p.getSancionPropuesta() != null ? p.getSancionPropuesta().getValor() : null);
      this.add("Expiry Penalty", fp.getVencimientoSancion());

      this.add("Full Documentation", fp.getEstadoChecklist() != null ? fp.getEstadoChecklist().getCodigo().equals("ok") ? "YES" : "NO" : "NO");
      this.add("KO Date Complete Documentation", fp.getFechaKoDocumentacion());
      this.add("OK Date Complete Documentation", fp.getFechaOkDocumentacion());

      this.add("Minute Request", fp.getSolicitudMinuta());
      this.add("Minute Reception Date", fp.getRecepcionMinuta());
      this.add("HRE Minute Review Date", fp.getRevisionMinutaHRE());
      this.add("Minute Submission Date to Notary Public", fp.getFEnvioMinutaANotaria());
      this.add("Minute Reception Date (from Notary Public)", fp.getRecepcionMinutaDeNotaria());
      this.add(
          "Minute Sending Date to Client for Validation",
          fp.getFEnvioMinutaAClienteFormalizacion());
      this.add("Date OK Minute", fp.getFOkMinutaANotaria());

      if (fp.getSolicitudGestionActivosVisita() != null)
        this.add(
            "Asset Management Request View", fp.getSolicitudGestionActivosVisita());
      if (fp.getInformeVisitaOk() != null)
        this.add("Report Visit Ok", fp.getInformeVisitaOk() ? "YES" : "NO");

      this.add("Communication Date", fp.getFechaComunicacion());
      this.add("TF Entry Date", fp.getFechaEntradaTF());
      this.add("Date Progress Task TF", fp.getFechaAvanceTareaTF());

      this.add("Protocol No.", fp.getNumeroProtocolo());
      this.add("Attorneys Signature", fp.getApoderadoFirma());
      this.add("Observations", fp.getObservaciones());

      this.add(
          "Minute Reviewed by Prosecutor",
          fp.getMinutaRevisadaFiscal());
    } else {
      this.add("Propuesta Origen", p.getIdCarga());

      this.add(
          "Clase Propuesta",
          p.getClasePropuesta() != null ? p.getClasePropuesta().getValor() : null);
      this.add(
          "Tipo Operación", p.getTipoPropuesta() != null ? p.getTipoPropuesta().getValor() : null);
      this.add(
          "Subtipo Propuesta",
          p.getSubtipoPropuesta() != null ? p.getSubtipoPropuesta().getValor() : null);
      this.add("Estado", p.getEstadoPropuesta() != null ? p.getEstadoPropuesta().getValor() : null);
      this.add(
          "Detalle Estado",
          fp.getSubestadoFormalizacion() != null
              ? fp.getSubestadoFormalizacion().getValor()
              : null);

      this.add("Fecha de Firma", fp.getFechaFirma());
      this.add("Hora de Firma", fp.getHoraFirma());
      this.add("Fecha Firma Estimada", fp.getFechaFirmaEstimada());
      /*if (fp.getConfirmadaFechaYHora() != null)
        this.add("Confirmada Fecha y Hora", fp.getConfirmadaFechaYHora() ? "SI" : "NO");*/

      this.add("Fecha Entrada", fp.getFechaEntrada());
      this.add("Fecha Sanción", p.getFechaSancion());
      this.add("Fecha Revisión Concursos", fp.getFechaRevisionConcursos());
      this.add("Fecha Revision Minuta HRE", fp.getFechaRevisionConcursos());

      this.add(
          "Tipo Sanción",
          p.getSancionPropuesta() != null ? p.getSancionPropuesta().getValor() : null);
      this.add("Vencimiento Sanción", fp.getVencimientoSancion());

      this.add("Documentación Completa", fp.getEstadoChecklist() != null ? fp.getEstadoChecklist().getCodigo().equals("ok") ? "SI" : "NO" : "NO");
      this.add("Fecha KO Documentación Completa", fp.getFechaKoDocumentacion());
      this.add("Fecha OK Documentación Completa", fp.getFechaOkDocumentacion());

      this.add("Solicitud Minuta", fp.getSolicitudMinuta());
      this.add("Fecha Recepción Minuta", fp.getRecepcionMinuta());
      this.add("Fecha Revisión Minuta HRE", fp.getRevisionMinutaHRE());
      this.add("Fecha de Envío Minuta a Notaria", fp.getFEnvioMinutaANotaria());
      this.add("Fecha Recepción Minuta (de Notaria)", fp.getRecepcionMinutaDeNotaria());
      this.add(
          "Fecha Envío Minuta a Cliente para Validación",
          fp.getFEnvioMinutaAClienteFormalizacion());
      this.add("Fecha OK Minuta", fp.getFOkMinutaANotaria());

      if (fp.getSolicitudGestionActivosVisita() != null)
        this.add(
            "Solicitud Gestión Activos Vista", fp.getSolicitudGestionActivosVisita());
      if (fp.getInformeVisitaOk() != null)
        this.add("Informe Visita Ok", fp.getInformeVisitaOk() ? "SI" : "NO");

      this.add("Fecha Comunicación", fp.getFechaComunicacion());
      this.add("Fecha Entrada TF", fp.getFechaEntradaTF());
      this.add("Fecha Avance Tarea TF", fp.getFechaAvanceTareaTF());

      this.add("Nº Protocolo", fp.getNumeroProtocolo());
      this.add("Apoderados Firma", fp.getApoderadoFirma());
      this.add("Observaciones", fp.getObservaciones());

      this.add(
          "Minuta Revisada por Fiscal",
          fp.getMinutaRevisadaFiscal());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
