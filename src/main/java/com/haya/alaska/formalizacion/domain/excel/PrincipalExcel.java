package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class PrincipalExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Origin File");
      cabeceras.add("Origin Proposal");
      cabeceras.add("Singular Dation");
      cabeceras.add("Buyer");
      cabeceras.add("Total FFRR");
      cabeceras.add("Way to pay");
      cabeceras.add("Expenses Assume");
      cabeceras.add("Expenses Amount");
      cabeceras.add("Capital Gains Assume");
      cabeceras.add("Capital Gain Amount");
      cabeceras.add("Ibi Assume");
      cabeceras.add("Ibi Amount");
      cabeceras.add("CP Assume");
      cabeceras.add("CP Amount");
      cabeceras.add("Urbanization Assume");
      cabeceras.add("Urbanization Amount");
      cabeceras.add("Cash Delivery Amount");
      cabeceras.add("PBC");
      cabeceras.add("PBC Effective Date");
      cabeceras.add("Free Goods Delivery");
      cabeceras.add("Conditioned Signature");
      cabeceras.add("Pending Ratification Seller");
      cabeceras.add("Buyer Ratification Pending");
      cabeceras.add("Pending Bank Ratification");
      cabeceras.add("Communication Alert");
      cabeceras.add("Recomendación No Firma");
      cabeceras.add("Motivo Recomendación No Firma");
      cabeceras.add("Observations");
      cabeceras.add("Loan No.");
      cabeceras.add("Origin ID Good");
      cabeceras.add("Origin Intervening Id");
    } else {
      cabeceras.add("Expediente Origen");
      cabeceras.add("Propuesta Origen");
      cabeceras.add("Dación Singular");
      cabeceras.add("Comprador");
      cabeceras.add("Total FFRR");
      cabeceras.add("Forma de Pago");
      cabeceras.add("Gastos Asume");
      cabeceras.add("Gastos Importe");
      cabeceras.add("Plusvalía Asume");
      cabeceras.add("Plusvalía Importe");
      cabeceras.add("Ibi Asume");
      cabeceras.add("Ibi Importe");
      cabeceras.add("CP Asume");
      cabeceras.add("CP Importe");
      cabeceras.add("Urbanización Asume");
      cabeceras.add("Urbanización Importe");
      cabeceras.add("Embargos/Otros Asume");
      cabeceras.add("Embargos/Otros Importe");
      cabeceras.add("Importe Entrega de Efectivo");
      cabeceras.add("PBC");
      cabeceras.add("Fecha Vigencia PBC");
      cabeceras.add("Entrega de Bienes Libres");
      cabeceras.add("Firma Condicionada");
      cabeceras.add("Pendiente Ratificación Vendedor");
      cabeceras.add("Pendiente Ratificación Comprador");
      cabeceras.add("Pendiente Ratificación Banco");
      cabeceras.add("Alerta Comunicación");
      cabeceras.add("Recommendation No Signature");
      cabeceras.add("Reason Recommendation No Signature");
      cabeceras.add("Observaciones");
      cabeceras.add("Nº Préstamo");
      cabeceras.add("ID Origen Bien");
      cabeceras.add("Id Interviniente Origen");
    }
  }

  public PrincipalExcel(Propuesta p, Formalizacion fp) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add(
          "Origin File", p.getExpediente() != null ? p.getExpediente().getIdConcatenado() : null);
      this.add("Origin Proposal", p.getIdCarga());
      if (fp.getDacionSingular() != null)
        this.add("Singular Dation", fp.getDacionSingular() ? "Yes" : "No");
      this.add("Observations", fp.getObservacionesPrincipal());
      this.add("Total FFRR", fp.getTotalFFRR());
      this.add("Communication Alert", fp.getAlertaEnComunicacion());
      this.add("Buyer", fp.getComprador() != null ? fp.getComprador().getValor() : null);
      if (fp.getPendienteRatificacionVendedor() != null)
        this.add(
            "Pending Ratification Seller", fp.getPendienteRatificacionVendedor() ? "Yes" : "No");
      if (fp.getPendienteRatificacionComprador() != null)
        this.add(
            "Buyer Ratification Pending", fp.getPendienteRatificacionComprador() ? "Yes" : "No");
      if (fp.getPendienteRatificacionBanco() != null)
        this.add("Pending Bank Ratification", fp.getPendienteRatificacionBanco() ? "Yes" : "No");
      if (fp.getFirmaCondicionada() != null)
        this.add("Conditioned Signature", fp.getFirmaCondicionada() ? "Yes" : "No");
      String codigos = "-";
      if (!p.getContratos().isEmpty())
        codigos =
            p.getContratos().stream().map(Contrato::getIdCarga).collect(Collectors.joining(", "));
      this.add("Loan No.", codigos);
      codigos = "-";
      if (!p.getBienes().isEmpty())
        codigos =
            p.getBienes().stream()
                .map(PropuestaBien::getBien)
                .map(Bien::getIdCarga)
                .collect(Collectors.joining(", "));
      this.add("Origin ID Good", codigos);
      codigos = "-";
      if (!p.getIntervinientes().isEmpty())
        codigos =
            p.getIntervinientes().stream()
                .map(Interviniente::getIdCarga)
                .collect(Collectors.joining(", "));
      this.add("Origin Intervening Id", codigos);
      if (fp.getGastosAsume() != null)
        this.add("Expenses Assume", fp.getGastosAsume() ? "Yes" : "No");
      this.add("Expenses Amount", fp.getGastosImporte());
      if (fp.getPlusvaliaAsume() != null)
        this.add("Capital Gains Assume", fp.getPlusvaliaAsume() ? "Yes" : "No");
      this.add("Capital Gain Amount", fp.getPlusvaliaImporte());
      if (fp.getIbiAsume() != null) this.add("Ibi Assume", fp.getIbiAsume() ? "Yes" : "No");
      this.add("Ibi Amount", fp.getIbiImporte());
      if (fp.getCpAsume() != null) this.add("CP Assume", fp.getCpAsume() ? "Yes" : "No");
      this.add("CP Amount", fp.getCpImporte());
      if (fp.getUrbanizacionAsume() != null)
        this.add("Urbanization Assume", fp.getUrbanizacionAsume() ? "Yes" : "No");
      this.add("Urbanization Amount", fp.getUrbanizacionImporte());this.add("Cash Delivery Amount", fp.getImporteEntregaEfectivo());
      if (fp.getEntregaDeBienesLibres() != null)
        this.add("Free Goods Delivery", fp.getEntregaDeBienesLibres() ? "Yes" : "No");
      this.add("Way to pay", fp.getFormaDePago() != null ? fp.getFormaDePago().getValorIngles() : null);
      if (fp.getPbc() != null)
        this.add("PBC", fp.getPbc() ? "Si" : "No");
      this.add("PBC Effective Date", fp.getFechaVigencia() != null);

      this.add(
        "Recommendation No Signature",
        fp.getRecomendacionNoFirma() != null ? fp.getRecomendacionNoFirma().getValor() : null);
      this.add(
        "Reason Recommendation No Signature",
        fp.getMotivoRecomendacionNoFirma() != null
          ? fp.getMotivoRecomendacionNoFirma().getValor()
          : null);
    } else {
      this.add(
          "Expediente Origen",
          p.getExpediente() != null ? p.getExpediente().getIdConcatenado() : null);
      this.add("Propuesta Origen", p.getIdCarga());
      if (fp.getDacionSingular() != null)
        this.add("Dación Singular", fp.getDacionSingular() ? "SI" : "No");
      this.add("Observaciones", fp.getObservacionesPrincipal());
      this.add("Total FFRR", fp.getTotalFFRR());
      this.add("Alerta Comunicación", fp.getAlertaEnComunicacion());
      this.add("Comprador", fp.getComprador() != null ? fp.getComprador().getValor() : null);
      if (fp.getPendienteRatificacionVendedor() != null)
        this.add(
            "Pendiente Ratificación Vendedor", fp.getPendienteRatificacionVendedor() ? "SI" : "No");
      if (fp.getPendienteRatificacionComprador() != null)
        this.add(
            "Pendiente Ratificación Comprador",
            fp.getPendienteRatificacionComprador() ? "SI" : "No");
      if (fp.getPendienteRatificacionBanco() != null)
        this.add("Pendiente Ratificación Banco", fp.getPendienteRatificacionBanco() ? "SI" : "No");
      if (fp.getFirmaCondicionada() != null)
        this.add("Firma Condicionada", fp.getFirmaCondicionada() ? "SI" : "No");
      String codigos = "-";
      if (!p.getContratos().isEmpty())
        codigos =
            p.getContratos().stream().map(Contrato::getIdCarga).collect(Collectors.joining(", "));
      this.add("Nº Préstamo", codigos);
      codigos = "-";
      if (!p.getBienes().isEmpty())
        codigos =
            p.getBienes().stream()
                .map(PropuestaBien::getBien)
                .map(Bien::getIdCarga)
                .collect(Collectors.joining(", "));
      this.add("ID Origen Bien", codigos);
      codigos = "-";
      if (!p.getIntervinientes().isEmpty())
        codigos =
            p.getIntervinientes().stream()
                .map(Interviniente::getIdCarga)
                .collect(Collectors.joining(", "));
      this.add("Id Interviniente Origen", codigos);
      if (fp.getGastosAsume() != null) this.add("Gastos Asume", fp.getGastosAsume() ? "Si" : "No");
      this.add("Gastos Importe", fp.getGastosImporte());
      if (fp.getPlusvaliaAsume() != null)
        this.add("Plusvalía Asume", fp.getPlusvaliaAsume() ? "Si" : "No");
      this.add("Plusvalía Importe", fp.getPlusvaliaImporte());
      if (fp.getIbiAsume() != null) this.add("Ibi Asume", fp.getIbiAsume() ? "Si" : "No");
      this.add("Ibi Importe", fp.getIbiImporte());
      if (fp.getCpAsume() != null) this.add("CP Asume", fp.getCpAsume() ? "Si" : "No");
      this.add("CP Importe", fp.getCpImporte());
      if (fp.getUrbanizacionAsume() != null)
        this.add("Urbanización Asume", fp.getUrbanizacionAsume() ? "Si" : "No");
      this.add("Urbanización Importe", fp.getUrbanizacionImporte());
      this.add("Importe Entrega de Efectivo", fp.getImporteEntregaEfectivo());
      if (fp.getEntregaDeBienesLibres() != null)
        this.add("Entrega de Bienes Libres", fp.getEntregaDeBienesLibres() ? "Si" : "No");
      this.add("Forma de Pago", fp.getFormaDePago() != null ? fp.getFormaDePago().getValor() : null);
      if (fp.getPbc() != null)
        this.add("PBC", fp.getPbc() ? "Si" : "No");
      this.add("Fecha Vigencia PBC", fp.getFechaVigencia() != null);this.add(
        "Recomendación No Firma",
        fp.getRecomendacionNoFirma() != null ? fp.getRecomendacionNoFirma().getValor() : null);
      this.add(
        "Motivo Recomendación No Firma",
        fp.getMotivoRecomendacionNoFirma() != null
          ? fp.getMotivoRecomendacionNoFirma().getValor()
          : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
