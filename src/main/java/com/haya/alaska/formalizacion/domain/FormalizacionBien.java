package com.haya.alaska.formalizacion.domain;

import com.haya.alaska.alquiler.domain.Alquiler;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.deposito_finanzas.domain.DepositoFinanzas;
import com.haya.alaska.estado_ocupacion.domain.EstadoOcupacion;
import com.haya.alaska.riesgos_detectados.domain.RiesgosDetectados;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_FORMALIZACION_BIEN")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_FORMALIZACION_BIEN")
public class FormalizacionBien implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_BIEN", nullable = false)
  private Bien bien;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RIESGOS_DETECTADOS")
  private RiesgosDetectados riesgosDetectados;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;

  @Column(name = "FCH_ALTA_REM")
  private Date fechaAltaRem;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ALQUILER")
  private Alquiler alquiler;


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_DEPOSITO_FINANZAS")
  private DepositoFinanzas depositoFinanzas;

  @Column(name = "NUM_DEUDA_ORIGINAL_PRINCIPAL")
  private Double deudaOriginal_principal;
  @Column(name = "NUM_DEUDA_ORIGINAL_INTERESES_ORDINARIOS")
  private Double deudaOriginal_interesesOrdinarios;
  @Column(name = "NUM_DEUDA_ORIGINAL_INTERESES_DE_DEMORA")
  private Double deudaOriginal_interesesDeDemora;

  @Column(name = "NUM_RESPONSABILIDAD_HIPOTECARIA_PRINCIPAL")
  private Double responsabilidadHipotecaria_principal;
  @Column(name = "NUM_RESPONSABILIDAD_HIPOTECARIA_INTERESES_ORDINARIOS")
  private Double responsabilidadHipotecaria_interesesOrdinarios;
  @Column(name = "NUM_RESPONSABILIDAD_HIPOTECARIA_INTERESES_DE_DEMORA")
  private Double responsabilidadHipotecaria_interesesDeDemora;

  @Column(name = "IND_RIESGO_CONSIGNACION")
  private Boolean riesgoConsignacion; //SI-NO
}
