package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class DDExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Origin Proposal");
      cabeceras.add("DDL Custom Date");
      cabeceras.add("DDL Reception Date");
      cabeceras.add("DDL Status");
      cabeceras.add("DDT Custom Date");
      cabeceras.add("DDT Reception Date");
      cabeceras.add("DDT Status");
      cabeceras.add("DD Received and Uploaded");
      cabeceras.add("Date Uploaded DD");
      cabeceras.add("DD Customer Rating");
      cabeceras.add("Legal Office");
      cabeceras.add("DD Fees");
      cabeceras.add("Signature Assistance Fees");
      cabeceras.add("Technical Dispatch");
      cabeceras.add("DD Technical Technical Fee");
      //cabeceras.add("DD Received");
    } else {
      cabeceras.add("Propuesta Origen");
      cabeceras.add("Fecha Encargo DDL");
      cabeceras.add("Fecha Recepción DDL");
      cabeceras.add("Estado DDL");
      cabeceras.add("Fecha Encargo DDT");
      cabeceras.add("Fecha Recepción DDT");
      cabeceras.add("Estado DDT");
      cabeceras.add("DD recibida y Subida");
      cabeceras.add("Fecha Subida DD");
      cabeceras.add("Valoración DD Cliente");
      cabeceras.add("Despacho Legal");
      cabeceras.add("Honorarios DD");
      cabeceras.add("Honorarios Asistencia Firma");
      cabeceras.add("Despacho Técnico");
      cabeceras.add("Honorarios DD Técnica Técnico");
      //cabeceras.add("DD recibida");
    }
  }

  public DDExcel (Propuesta p, Formalizacion fp){
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      this.add("Origin Proposal", p.getIdCarga());
      this.add("DDL Custom Date", fp.getFechaEncargoDDL());
      this.add("DDL Reception Date", fp.getFechaRecepcionDDL());
      this.add("DDL Status", fp.getEstadoDDL() != null ? fp.getEstadoDDL().getValor() : null);
      this.add("DDT Custom Date", fp.getFechaEncargoDDT());
      this.add("DDT Reception Date", fp.getFechaRecepcionDDT());
      this.add("DDT Status", fp.getEstadoDDT() != null ? fp.getEstadoDDT().getValor() : null);
      this.add("Legal Office", fp.getDespachoLegal());
      this.add("Technical Dispatch", fp.getDespachoTecnico());
      /*if (fp.getDdRecibida() != null)
        this.add("DD Received", fp.getDdRecibida() ? "Yes" : "No");*/
      if (fp.getDdRevisadaYSubida() != null)
        this.add("DD Received and Uploaded", fp.getDdRevisadaYSubida() ? "Yes" : "No");
      this.add("Date Uploaded DD", fp.getFechaSubidaDD());
      this.add("DD Fees", fp.getHonorariosDD());
      this.add("Signature Assistance Fees", fp.getHonorariosAsistenciaFirma());
      this.add("DD Customer Rating", fp.getValoracionDDCliente());
      this.add("DD Technical Technical Fee", fp.getHonorariosDD());
    } else {
      this.add("Propuesta Origen", p.getIdCarga());
      this.add("Fecha Encargo DDL", fp.getFechaEncargoDDL());
      this.add("Fecha Recepción DDL", fp.getFechaRecepcionDDL());
      this.add("Estado DDL", fp.getEstadoDDL() != null ? fp.getEstadoDDL().getValor() : null);
      this.add("Fecha Encargo DDT", fp.getFechaEncargoDDT());
      this.add("Fecha Recepción DDT", fp.getFechaRecepcionDDT());
      this.add("Estado DDT", fp.getEstadoDDT() != null ? fp.getEstadoDDT().getValor() : null);
      this.add("Despacho Legal", fp.getDespachoLegal());
      this.add("Despacho Técnico", fp.getDespachoTecnico());
      /*if (fp.getDdRecibida() != null)
        this.add("DD recibida", fp.getDdRecibida() ? "Si" : "No");*/
      if (fp.getDdRevisadaYSubida() != null)
        this.add("DD recibida y Subida", fp.getDdRevisadaYSubida() ? "Si" : "No");
      this.add("Fecha Subida DD", fp.getFechaSubidaDD());
      this.add("Honorarios DD", fp.getHonorariosDD());
      this.add("Honorarios Asistencia Firma", fp.getHonorariosAsistenciaFirma());
      this.add("Valoración DD Cliente", fp.getValoracionDDCliente());
      this.add("Honorarios DD Técnica Técnico", fp.getHonorariosDD());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
