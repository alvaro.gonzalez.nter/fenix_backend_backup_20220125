package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.LinkedHashSet;

public class SarebExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add ("Origin Proposal");
      cabeceras.add ("NFV");
      cabeceras.add ("Date Click Prism");
      cabeceras.add ("NILO Elevation Date");
      cabeceras.add ("NILO Close Date");
    } else {
      cabeceras.add("Propuesta Origen");
      cabeceras.add("NFV");
      cabeceras.add("Fecha Tecleo Prisma");
      cabeceras.add("Fecha Elevación NILO");
      cabeceras.add("Fecha Cierre NILO");
    }
  }

  public SarebExcel(Propuesta p, Formalizacion fp) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Origin Proposal", p.getIdCarga());
      this.add("NFV", fp.getNbv() != null ? elNotCie(fp.getNbv()) : null);
      this.add("Date Click Prism", fp.getFechaTecleoEnPrisma());
      this.add("NILO Elevation Date", fp.getFechaElevacionNILO());
      this.add("NILO Close Date", fp.getFechaCierreNILO());
    } else {
      this.add("Propuesta Origen", p.getIdCarga());
      this.add("NFV", fp.getNbv() != null ? elNotCie(fp.getNbv()) : null);
      this.add("Fecha Tecleo Prisma", fp.getFechaTecleoEnPrisma());
      this.add("Fecha Elevación NILO", fp.getFechaElevacionNILO());
      this.add("Fecha Cierre NILO", fp.getFechaCierreNILO());
    }
  }

  public static String elNotCie(double numero) {

    return new DecimalFormat("###,###.####################################").format(numero);

  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
