package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class ContactosExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Origin Proposal");
      cabeceras.add("Territorial");
      cabeceras.add("Formalization / Legal Manager");
      cabeceras.add("Main formalization manager");
      cabeceras.add("Haya Recovery Manager");
      cabeceras.add("Contest Manager (EDH / Client)");
      cabeceras.add("Client Recovery Manager");
      cabeceras.add("Customer Recovery Manager Phone");
      cabeceras.add("Analyst Client");
      cabeceras.add("Customer Analyst Phone");
      cabeceras.add("Formalization Manager / Client Lawyer");
      cabeceras.add("Telephone Manager Formalization / Client Lawyer");
      cabeceras.add("Gestoria");
      cabeceras.add("Gestor Gestoria");
      cabeceras.add("Notary");
      cabeceras.add("Notary Address");
      cabeceras.add("Notary Province");
      cabeceras.add("Notary Municipality");
      cabeceras.add("Notary Official");
      cabeceras.add("Notary Phone");
      cabeceras.add("Official Notary Phone");
      cabeceras.add("Official Notary Email");
      cabeceras.add("Signature Proxy");
      cabeceras.add("Office Contact");
    } else {
      cabeceras.add("Propuesta Origen");
      cabeceras.add("Territorial");
      cabeceras.add("Gestor Formalización / Legal");
      cabeceras.add("Gestor Formalización Principal");
      cabeceras.add("Gestor Recuperaciones Haya");
      cabeceras.add("Gestor Concursos (HRE / Cliente)");
      cabeceras.add("Gestor Recuperaciones Cliente");
      cabeceras.add("Teléfono Gestor Recuperación Cliente");
      cabeceras.add("Analista Cliente");
      cabeceras.add("Teléfono Analista Cliente");
      cabeceras.add("Gestor Formalización / Letrado Cliente");
      cabeceras.add("Teléfono Gestor Formalización / Letrado Cliente");
      cabeceras.add("Gestoria");
      cabeceras.add("Gestor Gestoria");
      cabeceras.add("Notario");
      cabeceras.add("Dirección Notaria");
      cabeceras.add("Provincia Notaria");
      cabeceras.add("Municipio Notaria");
      cabeceras.add("Oficial Notaria");
      cabeceras.add("Teléfono Notaria");
      cabeceras.add("Teléfono Oficial Notaria");
      cabeceras.add("Email Oficial Notaria");
      cabeceras.add("Apoderados Firma");
      cabeceras.add("Contacto Oficina");
    }
  }

  public ContactosExcel(Propuesta p, Formalizacion fp) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Origin Proposal", p.getIdCarga());
      Usuario gestorForm = null;
      String gestorFormLeg = null;
      if (p.getExpediente() != null){
        Expediente ex = p.getExpediente();
        gestorForm = ex.getUsuarioByPerfil("Gestor formalización");
        gestorFormLeg = ex.getUsuariosByPerfil("Soporte formalización");
      } else if (p.getExpedientePosesionNegociada() != null){
        ExpedientePosesionNegociada ex = p.getExpedientePosesionNegociada();
        gestorForm = ex.getUsuarioByPerfil("Gestor formalización");
        gestorFormLeg = ex.getUsuariosByPerfil("Soporte formalización");
      }
      this.add("Formalization / Legal Manager", gestorFormLeg);
      this.add("Principal Formalization Manager", gestorForm != null ? gestorForm.getNombre() : null);
      this.add(
          "Contest Manager (HRE / Client)",
          fp.getGestorConcursos() != null ? fp.getGestorConcursos().getValorIngles() : null);
      this.add(
          "Management Manager",
          fp.getGestorGestoria() != null ? fp.getGestorGestoria().getValorIngles() : null);
      if (p.getExpediente() != null) {
        String nombre = null;
        Expediente e = p.getExpediente();
        Usuario gestor = e.getGestor();
        if (gestor != null) nombre = gestor.getNombre();
        this.add("Haya Recovery Manager", nombre);
      }
      this.add("Client Recovery Manager", fp.getGestorRecuperacionCliente());
      this.add("Customer Recovery Manager Phone", fp.getTelefono1GesRecCli());
      this.add("Customer Analyst Phone", fp.getTelefono1Cliente());
      this.add("Gestoria", fp.getGestoriaTramitadora());
      this.add("Signature Proxy", fp.getApoderadoFirma());
      this.add("Territorial", fp.getTerritorial());
      this.add("Customer Analyst", fp.getAnalistaCliente());
      this.add("Formalization Manager / Client Lawyer", fp.getGestorFormalizacionCliente());
      this.add("Notary", fp.getNotario());
      this.add("Notary Address", fp.getDireccionNotaria());
      this.add(
          "Notary Province",
          fp.getProvinciaNotaria() != null ? fp.getProvinciaNotaria().getValorIngles() : null);
      this.add(
          "Notary Municipality",
          fp.getMunicipioNotaria() != null ? fp.getMunicipioNotaria().getValorIngles() : null);
      this.add("Notary Official", fp.getOficinaNotaria());
      this.add("Notary Phone", fp.getTelefonoNotaria());
      this.add("Official Notary Phone", fp.getTelefonoOficialNotaria());
      this.add("Official Notary Email", fp.getEmailOficialNotaria());
      this.add("Office Contact", fp.getContactoOficinaCliente());
    } else {
      this.add("Propuesta Origen", p.getIdCarga());
      Usuario gestorForm = null;
      String gestorFormLeg = null;
      if (p.getExpediente() != null){
        Expediente ex = p.getExpediente();
        gestorForm = ex.getUsuarioByPerfil("Gestor formalización");
        gestorFormLeg = ex.getUsuariosByPerfil("Soporte formalización");
      } else if (p.getExpedientePosesionNegociada() != null){
        ExpedientePosesionNegociada ex = p.getExpedientePosesionNegociada();
        gestorForm = ex.getUsuarioByPerfil("Gestor formalización");
        gestorFormLeg = ex.getUsuariosByPerfil("Soporte formalización");
      }
      this.add("Gestor Formalización Principal", gestorForm != null ? gestorForm.getNombre() : null);
      this.add("Gestor Formalización / Legal", gestorFormLeg);
      this.add(
          "Gestor Concursos (HRE / Cliente)",
          fp.getGestorConcursos() != null ? fp.getGestorConcursos().getValor() : null);
      this.add(
          "Gestor Gestoria",
          fp.getGestorGestoria() != null ? fp.getGestorGestoria().getValor() : null);
      if (p.getExpediente() != null) {
        String nombre = null;
        Expediente e = p.getExpediente();
        Usuario gestor = e.getGestor();
        if (gestor != null) nombre = gestor.getNombre();
        this.add("Gestor Recuperaciones Haya", nombre);
      }
      this.add("Gestor Recuperaciones Cliente", fp.getGestorRecuperacionCliente());
      this.add("Teléfono Gestor Recuperación Cliente", fp.getTelefono1GesRecCli());
      this.add("Teléfono Analista Cliente", fp.getTelefono1Cliente());
      this.add("Gestoria", fp.getGestoriaTramitadora());
      this.add("Apoderados Firma", fp.getApoderadoFirma());
      this.add("Territorial", fp.getTerritorial());
      this.add("Analista Cliente", fp.getAnalistaCliente());
      this.add("Gestor Formalización / Letrado Cliente", fp.getGestorFormalizacionCliente());
      this.add("Notario", fp.getNotario());
      this.add("Dirección Notaria", fp.getDireccionNotaria());
      this.add(
          "Provincia Notaria",
          fp.getProvinciaNotaria() != null ? fp.getProvinciaNotaria().getValor() : null);
      this.add(
          "Municipio Notaria",
          fp.getMunicipioNotaria() != null ? fp.getMunicipioNotaria().getValor() : null);
      this.add("Oficial Notaria", fp.getOficinaNotaria());
      this.add("Teléfono Notaria", fp.getTelefonoNotaria());
      this.add("Teléfono Oficial Notaria", fp.getTelefonoOficialNotaria());
      this.add("Email Oficial Notaria", fp.getEmailOficialNotaria());
      this.add("Contacto Oficina", fp.getContactoOficinaCliente());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
