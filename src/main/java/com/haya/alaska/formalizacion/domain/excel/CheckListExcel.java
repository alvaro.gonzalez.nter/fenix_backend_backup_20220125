package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.datos_check_list.domain.DatosCheckList;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class CheckListExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add (" ");
      cabeceras.add ("APPENDIX - RECEIVED DOCUMENTATION");
      cabeceras.add ("  ");
      cabeceras.add ("Result Tab (Admission):");
      cabeceras.add ("   ");
    } else {
      cabeceras.add (" ");
      cabeceras.add ("ANEXO - DOCUMENTACIÓN RECIBIDA");
      cabeceras.add ("  ");
      cabeceras.add ("Resultado Ficha (Admisión):");
      cabeceras.add ("   ");
    }
  }

  public CheckListExcel(DatosCheckList dcl) {
    this.add(" ", " ");
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("APPENDIX - RECEIVED DOCUMENTATION", dcl.getDocumentacionValidada() != null ? dcl.getDocumentacionValidada().getValorIngles() : null);
      this.add("  ", dcl.getNombre() != null ? dcl.getNombre().getValorIngles() : null);
      this.add("Result Tab (Admission):", " ");
    } else {
      this.add("ANEXO - DOCUMENTACIÓN RECIBIDA", dcl.getDocumentacionValidada() != null ? dcl.getDocumentacionValidada().getValor() : null);
      this.add("  ", dcl.getNombre() != null ? dcl.getNombre().getValor() : null);
      this.add("Resultado Ficha (Admisión):", " ");
    }
    this.add("   ", " ");
  }

  public CheckListExcel(String nombre){
    this.add(" ", " ");
    this.add("  ", " ");
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("APPENDIX - RECEIVED DOCUMENTATION", nombre);
      this.add("Result Tab (Admission):", " ");
    } else {
      this.add("ANEXO - DOCUMENTACIÓN RECIBIDA", nombre);
      this.add("Resultado Ficha (Admisión):", " ");
    }
    this.add("   ", " ");
  }

  public CheckListExcel(String uno, String dos, String tres){
    this.add(" ", " ");
    this.add("  ", " ");
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("APPENDIX - RECEIVED DOCUMENTATION", uno);
      this.add("Result Tab (Admission):", dos);
    } else {
      this.add("ANEXO - DOCUMENTACIÓN RECIBIDA", uno);
      this.add("Resultado Ficha (Admisión):", dos);
    }
    this.add("   ", tres);
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
