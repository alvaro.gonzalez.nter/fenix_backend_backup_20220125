package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.formalizacion.domain.FormalizacionBien;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.shared.util.ExcelUtils;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class FormalizacionBusquedaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      cabeceras.add ("File No.");
      cabeceras.add ("Headline");
      cabeceras.add ("NIF");
      cabeceras.add ("Status");
      cabeceras.add ("State Detail");
      cabeceras.add ("Signature Date");
      cabeceras.add ("Signature Time");
      cabeceras.add ("Beech Formalization");
      cabeceras.add ("Haya Recovery Manager");
      cabeceras.add ("Client Formalization Manager");
      cabeceras.add ("Penalty / Entry Date");
      cabeceras.add ("Dation Amount");
      cabeceras.add ("Debt Amount");
      cabeceras.add ("Province");
      cabeceras.add ("Rent");
    } else {
      cabeceras.add("Nº Expediente");
      cabeceras.add("Titular");
      cabeceras.add("NIF");
      cabeceras.add("Estado");
      cabeceras.add("Detalle Estado");
      cabeceras.add("Fecha de Firma");
      cabeceras.add("Hora de Firma");
      cabeceras.add("Formalización Haya");
      cabeceras.add("Gestor Recuperaciones Haya");
      cabeceras.add("Gestor Formalización Cliente");
      cabeceras.add("Fecha Sanción/Entrada");
      cabeceras.add("Importe Dación");
      cabeceras.add("Importe de la Deuda");
      cabeceras.add("Provincia");
      cabeceras.add("Alquiler");
    }
  }

  public FormalizacionBusquedaExcel(Expediente e) {
    Contrato c = e.getContratoRepresentante();
    Interviniente i = null;
    if (c != null) i = c.getPrimerInterviniente();
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      this.add("File No.", e.getIdConcatenado());
      this.add("Haya Recovery Manager", e.getGestor() != null ? e.getGestor().getNombre() : null);
      if (i != null){
        this.add("Headline", i.getNombreReal());
        this.add("NIF", i.getNumeroDocumento());
      }
      Propuesta p = null;
      for (Propuesta pr : e.getPropuestas()){
        if (p == null) {
          p = pr;
          continue;
        }
        if (p.getDeudaActual() < pr.getDeudaActual())
          p = pr;
      }

      if (p != null){
        Formalizacion f = p.getFormalizaciones().stream().findFirst().orElse(new Formalizacion());
        this.add("Status", p.getEstadoPropuesta() != null ? p.getEstadoPropuesta().getValorIngles() : null);
        this.add("State Detail", f.getSubestadoFormalizacion() != null ? f.getSubestadoFormalizacion().getValorIngles() : null);
        this.add("Signature Date", f.getFechaFirma());
        this.add("Signature Time", f.getHoraFirma());
        Usuario gestorForm = null;
        if (p.getExpediente() != null){
          Expediente ex = p.getExpediente();
          gestorForm = ex.getUsuarioByPerfil("Gestor formalización");
        } else if (p.getExpedientePosesionNegociada() != null){
          ExpedientePosesionNegociada ex = p.getExpedientePosesionNegociada();
          gestorForm = ex.getUsuarioByPerfil("Gestor formalización");
        }
        this.add("Beech Formalization", gestorForm != null ? gestorForm.getNombre() : null); //gestor formalización
        this.add("Client Formalization Manager", f.getGestorFormalizacionCliente());
        this.add("Penalty / Entry Date", f.getFechaSancion());
        this.add("Province", f.getProvinciaNotaria() != null ? f.getProvinciaNotaria().getValorIngles() : null);

        Double dacion = 0.0;
        Double deuda = 0.0;
        for (Contrato co : p.getContratos()){
          dacion += co.getSaldoGestion();
          deuda += co.getDeudaImpagada();
        }
        this.add("Dation Amount", dacion);  //sumatorio saldo de los contratos de la propuesta
        this.add("Debt Amount", deuda);  //sumatorio importe deuda de los contratos de la propuesta

        Bien b = null;
        for (PropuestaBien pb : p.getBienes()){
          Bien bi = pb.getBien();
          if (bi != null){
            if (b == null){
              b = bi;
              continue;
            }
            if (b.getImporteBien() < bi.getImporteBien())
              b = bi;
          }
        }

        if (b != null){
          FormalizacionBien fb = b.getFormalizacionBien();
          if (fb != null)
            this.add("Rent", fb.getAlquiler() != null ? fb.getAlquiler().getValor() : null);  //bien de mayor valor
        }
      }
    } else {
      this.add("Nº Expediente", e.getIdConcatenado());
      this.add("Gestor Recuperaciones Haya", e.getGestor() != null ? e.getGestor().getNombre() : null);
      if (i != null){
        this.add("Titular", i.getNombreReal());
        this.add("NIF", i.getNumeroDocumento());
      }
      Propuesta p = null;
      for (Propuesta pr : e.getPropuestas()){
        if (p == null) {
          p = pr;
          continue;
        }
        if (p.getDeudaActual() < pr.getDeudaActual())
          p = pr;
      }

      if (p != null){
        Formalizacion f = p.getFormalizaciones().stream().findFirst().orElse(new Formalizacion());
        this.add("Estado", p.getEstadoPropuesta() != null ? p.getEstadoPropuesta().getValor() : null);
        this.add("Detalle Estado", f.getSubestadoFormalizacion() != null ? f.getSubestadoFormalizacion().getValor() : null);
        this.add("Fecha de Firma", f.getFechaFirma());
        this.add("Hora de Firma", f.getHoraFirma());
        Usuario gestorForm = null;
        if (p.getExpediente() != null){
          Expediente ex = p.getExpediente();
          gestorForm = ex.getUsuarioByPerfil("Gestor formalización");
        } else if (p.getExpedientePosesionNegociada() != null){
          ExpedientePosesionNegociada ex = p.getExpedientePosesionNegociada();
          gestorForm = ex.getUsuarioByPerfil("Gestor formalización");
        }
        this.add("Formalización Haya", gestorForm != null ? gestorForm.getNombre() : null); //gestor formalización
        this.add("Gestor Formalización Cliente", f.getGestorFormalizacionCliente());
        this.add("Fecha Sanción/Entrada", f.getFechaSancion());
        this.add("Provincia", f.getProvinciaNotaria() != null ? f.getProvinciaNotaria().getValor() : null);

        Double dacion = 0.0;
        Double deuda = 0.0;
        for (Contrato co : p.getContratos()){
          Saldo s = co.getUltimoSaldo();
          if (s != null){
            if (s.getDeudaImpagada() != null)
              deuda += s.getDeudaImpagada();
            if (s.getSaldoGestion() != null)
              dacion += s.getSaldoGestion();
          }
        }
        this.add("Importe Dación", dacion);  //sumatorio saldo de los contratos de la propuesta
        this.add("Importe de la Deuda", deuda);  //sumatorio importe deuda de los contratos de la propuesta

        Bien b = null;
        for (PropuestaBien pb : p.getBienes()){
          Bien bi = pb.getBien();
          if (bi != null){
            if (b == null){
              b = bi;
              continue;
            }
            if (b.getImporteBien() < bi.getImporteBien())
              b = bi;
          }
        }

        if (b != null){
          FormalizacionBien fb = b.getFormalizacionBien();
          if (fb != null)
            this.add("Alquiler", fb.getAlquiler() != null ? fb.getAlquiler().getValor() : null);  //bien de mayor valor
        }
      }
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
