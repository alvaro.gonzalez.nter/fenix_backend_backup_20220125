package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.formalizacion.domain.FormalizacionCarga;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class FormalizacionCargaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Origin Id Good");
      cabeceras.add("Load Type");
      cabeceras.add("Loading Amount");
      cabeceras.add("Own Loading");
      cabeceras.add("Load Typology");
      cabeceras.add("Subtype Load");
      cabeceras.add("IBI Debt");
      cabeceras.add("IBI Certificate");
      cabeceras.add("Debt CP");
      cabeceras.add("CP Certificate");
      cabeceras.add("Contact CP");
      cabeceras.add("Debt Urbanization");
      cabeceras.add("Urbanization Certificate");
    } else {
      cabeceras.add("Id Origen Bien");
      cabeceras.add("Tipo Carga");
      cabeceras.add("Importe Carga");
      cabeceras.add("Carga Propia");
      cabeceras.add("Tipología Carga");
      cabeceras.add("Subtipo Carga");
      cabeceras.add("Deuda IBI");
      cabeceras.add("Certificado IBI");
      cabeceras.add("Deuda CP");
      cabeceras.add("Certificado CP");
      cabeceras.add("Contacto CP");
      cabeceras.add("Deuda Urbanización");
      cabeceras.add("Certificado Urbanización");
    }
  }

  public FormalizacionCargaExcel(Bien b, Carga c, FormalizacionCarga fc) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Origin Id Good", b.getIdCarga());
      this.add("Load Type", c.getTipoCarga() != null ? c.getTipoCarga().getValor() : null);
      this.add("Loading Amount", c.getImporte());
      if (c.getCargaPropia() != null) this.add("Own Loading", c.getCargaPropia() ? "Yes" : "No");
      this.add(
          "Load Typology",
          c.getTipoCargaFormalizacion() != null ? c.getTipoCargaFormalizacion().getValor() : null);
      this.add(
          "Subtype Load",
          c.getSubtipoCargaFormalizacion() != null
              ? c.getSubtipoCargaFormalizacion().getValor()
              : null);
      this.add("IBI Debt", fc.getDeudaIBI());
      if (fc.getCertificadoIBI() != null)
        this.add("IBI Certificate", fc.getCertificadoIBI() ? "Yes" : "No");
      this.add("Debt CP", fc.getDeudaCP());
      if (fc.getCertificadoCP() != null)
        this.add("CP Certificate", fc.getCertificadoCP() ? "Yes" : "No");
      this.add("Contact CP", fc.getContactoCP());
      this.add("Debt Urbanization", fc.getDeudaUrbanizacion());
      if (fc.getCertificadoUrbanizacion() != null)
        this.add("Urbanization Certificate", fc.getCertificadoUrbanizacion() ? "Yes" : "No");
    } else {
      this.add("Id Origen Bien", b.getIdCarga());
      this.add("Tipo Carga", c.getTipoCarga() != null ? c.getTipoCarga().getValor() : null);
      this.add("Importe Carga", c.getImporte());
      if (c.getCargaPropia() != null) this.add("Carga Propia", c.getCargaPropia() ? "Si" : "No");
      this.add(
          "Tipología Carga",
          c.getTipoCargaFormalizacion() != null ? c.getTipoCargaFormalizacion().getValor() : null);
      this.add(
          "Subtipo Carga",
          c.getSubtipoCargaFormalizacion() != null
              ? c.getSubtipoCargaFormalizacion().getValor()
              : null);
      this.add("Deuda IBI", fc.getDeudaIBI());
      if (fc.getCertificadoIBI() != null)
        this.add("Certificado IBI", fc.getCertificadoIBI() ? "Si" : "No");
      this.add("Deuda CP", fc.getDeudaCP());
      if (fc.getCertificadoCP() != null)
        this.add("Certificado CP", fc.getCertificadoCP() ? "Si" : "No");
      this.add("Contacto CP", fc.getContactoCP());
      this.add("Deuda Urbanización", fc.getDeudaUrbanizacion());
      if (fc.getCertificadoUrbanizacion() != null)
        this.add("Certificado Urbanización", fc.getCertificadoUrbanizacion() ? "Si" : "No");
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
