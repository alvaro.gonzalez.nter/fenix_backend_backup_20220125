package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.formalizacion.domain.FormalizacionProcedimiento;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class FormalizacionProcedimientoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Id Loan");
      cabeceras.add("Intervener Origin Id");
      cabeceras.add("Status");
      cabeceras.add("Procedure Type");
      cabeceras.add("Contest");
      cabeceras.add("Litigation");
      cabeceras.add("Auto");
      cabeceras.add("Testimony");
      cabeceras.add("Auto No.");
      cabeceras.add("Court No.");
      cabeceras.add("Square");
      cabeceras.add("Do you have the rest of the Cars and Testimonies for the signature?");
    } else {
      cabeceras.add("Id Prestamo");
      cabeceras.add("Id Interviniente Origen");
      cabeceras.add("Estado");
      cabeceras.add("Tipo Procedimiento");
      cabeceras.add("Concurso");
      cabeceras.add("Litigio");
      cabeceras.add("Auto");
      cabeceras.add("Testimonio");
      cabeceras.add("Nº Auto");
      cabeceras.add("Nº Juzgado");
      cabeceras.add("Plaza");
      cabeceras.add("¿Dispone del resto de Autos y Testimonios para la firma?");
    }
  }

  public FormalizacionProcedimientoExcel(Procedimiento p, FormalizacionProcedimiento fp) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add(
          "Id Loan",
          p.getContratos().stream().map(Contrato::getIdCarga).collect(Collectors.joining(", ")));
      this.add(
          "Intervener Origin Id",
          p.getDemandados().stream()
              .map(Interviniente::getIdCarga)
              .collect(Collectors.joining(", ")));
      this.add("Status", p.getEstado() != null ? p.getEstado().getValorIngles() : null);
      this.add("Procedure Type", p.getTipo() != null ? p.getTipo().getValorIngles() : null);
      if (fp.getConcurso() != null) this.add("Contest", fp.getConcurso() ? "Yes" : "No");
      if (fp.getLitigio() != null) this.add("Litigation", fp.getLitigio() ? "Yes" : "No");
      if (fp.getAuto() != null) this.add("Auto", fp.getAuto() ? "Yes" : "No");
      if (fp.getTestimonio() != null) this.add("Testimony", fp.getTestimonio() ? "Yes" : "No");
      this.add("Auto No.", p.getNumeroAutos());
      this.add("Court No.", p.getJuzgado());
      this.add("Square", p.getPlaza());
      if (fp.getDisponible() != null)
        this.add(
            "Do you have the rest of the Cars and Testimonies for the signature?",
            fp.getDisponible() ? "Yes" : "No");
    } else {
      this.add(
          "Id Prestamo",
          p.getContratos().stream().map(Contrato::getIdCarga).collect(Collectors.joining(", ")));
      this.add(
          "Id Interviniente Origen",
          p.getDemandados().stream()
              .map(Interviniente::getIdCarga)
              .collect(Collectors.joining(", ")));
      this.add("Estado", p.getEstado() != null ? p.getEstado().getValor() : null);
      this.add("Tipo Procedimiento", p.getTipo() != null ? p.getTipo().getValor() : null);
      if (fp.getConcurso() != null) this.add("Concurso", fp.getConcurso() ? "Si" : "No");
      if (fp.getLitigio() != null) this.add("Litigio", fp.getLitigio() ? "Si" : "No");
      if (fp.getAuto() != null) this.add("Auto", fp.getAuto() ? "Si" : "No");
      if (fp.getTestimonio() != null) this.add("Testimonio", fp.getTestimonio() ? "Si" : "No");
      this.add("Nº Auto", p.getNumeroAutos());
      this.add("Nº Juzgado", p.getJuzgado());
      this.add("Plaza", p.getPlaza());
      if (fp.getDisponible() != null)
        this.add(
            "¿Dispone del resto de Autos y Testimonios para la firma?",
            fp.getDisponible() ? "Si" : "No");
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
