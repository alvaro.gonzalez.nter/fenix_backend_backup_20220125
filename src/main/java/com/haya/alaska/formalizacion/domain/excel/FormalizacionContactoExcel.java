package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class FormalizacionContactoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      cabeceras.add ("Origin Intervening Id");
      cabeceras.add ("Type");
      cabeceras.add ("Value");
      cabeceras.add ("Order");
    } else {
      cabeceras.add("Id Interviniente Origen");
      cabeceras.add("Tipo");
      cabeceras.add("Valor");
      cabeceras.add("Orden");
    }
  }

  public FormalizacionContactoExcel(Interviniente i, DatoContacto dc) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add ("Origin Intervening Id", i.getIdCarga());
      String type = "";
      String value = "";
      if (dc.getEmail() != null) {
        type = "Email";
        value = dc.getEmail ();
      }
       else if (dc.getFijo() != null) {
        type = "Landline Phone";
        value = dc.getFijo();
      }
       else if (dc.getMovil() != null) {
        type = "Mobile Phone";
        value = dc.getMovil ();
      }
      this.add ("Type", type);
      this.add ("Value", value);
      this.add ("Order", dc.getOrden());
    } else {
      this.add("Id Interviniente Origen", i.getIdCarga());
      String tipo = "";
      String valor = "";
      if (dc.getEmail() != null){
        tipo = "Email";
        valor = dc.getEmail();
      }
      else if (dc.getFijo() != null){
        tipo = "Teléfono Fijo";
        valor = dc.getFijo();
      }
      else if (dc.getMovil() != null){
        tipo = "Teléfono Móvil";
        valor = dc.getMovil();
      }
      this.add("Tipo", tipo);
      this.add("Valor", valor);
      this.add("Orden", dc.getOrden());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
