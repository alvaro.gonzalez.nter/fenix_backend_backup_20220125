package com.haya.alaska.formalizacion.domain;

import com.haya.alaska.carga.domain.Carga;
import com.haya.alaska.subtipo_carga_formalizacion.domain.SubtipoCargaFormalizacion;
import com.haya.alaska.tipo_carga_formalizacion.domain.TipoCargaFormalizacion;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_MSTR_FORMALIZACION_CARGA")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_FORMALIZACION_CARGA")
public class FormalizacionCarga implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARGA", nullable = false)
  private Carga carga;


  //CARGAS DEUDAS
  @Column(name = "NUM_DEUDA_IBI")
  private Double deudaIBI;
  @Column(name = "IND_CERTIFICADO_IBI")
  private Boolean certificadoIBI;

  @Column(name = "NUM_DEUDA_CP")
  private Double deudaCP;
  @Column(name = "IND_CERTIFICADO_CP")
  private Boolean certificadoCP;

  @Column(name = "DES_CONTACTO_CP")
  private String contactoCP;

  @Column(name = "NUM_DEUDA_URBANIZACION")
  private Double deudaUrbanizacion;
  @Column(name = "IND_CERTIFICADO_URBANIZACION")
  private Boolean certificadoUrbanizacion;
}
