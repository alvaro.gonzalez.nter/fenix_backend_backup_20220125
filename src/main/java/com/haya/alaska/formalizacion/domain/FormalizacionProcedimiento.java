package com.haya.alaska.formalizacion.domain;

import com.haya.alaska.procedimiento.domain.Procedimiento;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_MSTR_FORMALIZACION_PROCEDIMEINTO")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_FORMALIZACION_PROCEDIMEINTO")
public class FormalizacionProcedimiento implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROCEDIMIENTO", nullable = false)
  private Procedimiento procedimiento;

  //LITIGIO
  @Column(name = "IND_CONCURSO")
  private Boolean concurso;
  @Column(name = "IND_LITIGIO")
  private Boolean litigio;
  @Column(name = "IND_AUTO")
  private Boolean auto;
  @Column(name = "IND_TESTIMONIO")
  private Boolean testimonio;

  @Column(name = "IND_DISPONIBLE")
  private Boolean disponible;
}
