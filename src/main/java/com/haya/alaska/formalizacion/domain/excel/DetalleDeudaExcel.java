package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.text.DecimalFormat;
import java.util.*;

public class DetalleDeudaExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add ("Loan");
      cabeceras.add ("Id Agreement");
      cabeceras.add ("Principal / Privilege");
      cabeceras.add ("Interest and expenses / Ordinary");
      cabeceras.add ("Total Debt");
    } else {
      cabeceras.add("Préstamo");
      cabeceras.add("Id Acuerdo");
      cabeceras.add("Principal/Privilegio");
      cabeceras.add("Intereses y gastos/Ordinario");
      cabeceras.add("Total Deuda");
    }
  }

  public DetalleDeudaExcel(Contrato c) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Loan", elNotCie(c.getImporteInicial()));
      this.add("Id Agreement", c.getImporteInicial() != null ? elNotCie(c.getImporteInicial()) : null);
      Saldo s = c.getUltimoSaldo();
      if (s != null){
        this.add("Principal / Privilege", s.getInteresesOrdinariosImpagados() != null ? elNotCie(s.getInteresesOrdinariosImpagados()) : null);
        this.add("Interest and expenses / Ordinary", s.getInteresesOrdinariosImpagados() != null ? elNotCie(s.getInteresesOrdinariosImpagados()) : null);
        this.add("Total Debt", s.getSaldoGestion() != null ? elNotCie(s.getSaldoGestion()) : null);
      }
    } else {
      this.add("Préstamo", c.getImporteInicial() != null ? elNotCie(c.getImporteInicial()) : null);
      this.add("Id Acuerdo", c.getIdCarga());
      Saldo s = c.getUltimoSaldo();
      if (s != null){
        this.add("Principal/Privilegio",  s.getInteresesOrdinariosImpagados() != null ? elNotCie(s.getInteresesOrdinariosImpagados()) : null);
        this.add("Intereses y gastos/Ordinario", s.getPrincipalVencidoImpagado() != null ? elNotCie(s.getPrincipalVencidoImpagado()) : null);
        this.add("Total Deuda", s.getSaldoGestion() != null ? elNotCie(s.getSaldoGestion()) : null);
      }
    }
  }

  public static String elNotCie(Double numero) {
    return new DecimalFormat("###,###.####################################").format(numero);
  }

  public DetalleDeudaExcel(String nombre, String cantidad) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Loan", nombre);
      this.add("Id Agreement", "");
      this.add("Privilege", "");
      this.add("Ordinary", "");
      this.add("Total Debt", cantidad);
    } else {
      this.add("Préstamo", nombre);
      this.add("Id Acuerdo", "");
      this.add("Privilegio", "");
      this.add("Ordinario", "");
      this.add("Total Deuda", cantidad);
    }
  }

  public static final LinkedHashSet<String> sobreCabecera(){
    LinkedHashSet<String> s = new LinkedHashSet<>();

    s.add("SALDO DEUDA A FECHA DE FIRMA");
    s.add(" ");
    s.add("  ");
    s.add("   ");
    Calendar c = Calendar.getInstance();
    s.add(c.getTime().toString());

    return s;
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
