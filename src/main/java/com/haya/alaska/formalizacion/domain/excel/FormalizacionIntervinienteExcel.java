package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class FormalizacionIntervinienteExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Loan No.");
      cabeceras.add("Origin Intervening Id");
      cabeceras.add("Status");
      cabeceras.add("Name");
      cabeceras.add("Surname");
      cabeceras.add("Company Name");
      cabeceras.add("Date of Birth");
      cabeceras.add("Date of Incorporation");
      cabeceras.add("Document Type");
      cabeceras.add("DNI/CIF");
      cabeceras.add("Intervention Type");
      cabeceras.add("Intervention Order");
    } else {
      cabeceras.add("Nº Prestamo");
      cabeceras.add("Id Interviniente Origen");
      cabeceras.add("Estado");
      cabeceras.add("Nombre");
      cabeceras.add("Apellidos");
      cabeceras.add("Razón Social");
      cabeceras.add("Fecha Nacimiento");
      cabeceras.add("Fecha Constitución");
      cabeceras.add("Tipo Documento");
      cabeceras.add("DNI/CIF");
      cabeceras.add("Tipo Intervención");
      cabeceras.add("Orden Intervención");
    }
  }

  public FormalizacionIntervinienteExcel(Interviniente i, ContratoInterviniente ci) {
    Contrato c = null;
    if (ci != null) c = ci.getContrato();
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      if (c != null) this.add("Loan No.", c.getIdCarga());
      this.add("Origin Intervening Id", i.getIdOrigen());
      this.add("Status", i.getActivo() ? "Active" : "Inactive");
      this.add("Name", i.getNombre());
      this.add("Surname", i.getApellidos());
      this.add("Company Name", i.getRazonSocial());
      this.add("Date of Birth", i.getFechaNacimiento());
      this.add("Date of Incorporation", i.getFechaConstitucion());
      this.add(
          "Document Type",
          i.getTipoDocumento() != null ? i.getTipoDocumento().getValorIngles() : null);
      this.add("DNI/CIF", i.getNumeroDocumento());
      if (ci != null) {
        this.add(
            "Intervention Type",
            ci.getTipoIntervencion() != null ? ci.getTipoIntervencion().getValorIngles() : null);
        this.add("Intervention Order", ci.getOrdenIntervencion());
      }
    } else {
      if (c != null) this.add("Nº Prestamo", c.getIdCarga());
      this.add("Id Interviniente Origen", i.getIdOrigen());
      this.add("Estado", i.getActivo() ? "Activo" : "Inactivo");
      this.add("Nombre", i.getNombre());
      this.add("Apellidos", i.getApellidos());
      this.add("Razón Social", i.getRazonSocial());
      this.add("Fecha Nacimiento", i.getFechaNacimiento());
      this.add("Fecha Constitución", i.getFechaConstitucion());
      this.add(
          "Tipo Documento", i.getTipoDocumento() != null ? i.getTipoDocumento().getValor() : null);
      this.add("DNI/CIF", i.getNumeroDocumento());
      if (ci != null) {
        this.add(
            "Tipo Intervención",
            ci.getTipoIntervencion() != null ? ci.getTipoIntervencion().getValor() : null);
        this.add("Orden Intervención", ci.getOrdenIntervencion());
      }
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
