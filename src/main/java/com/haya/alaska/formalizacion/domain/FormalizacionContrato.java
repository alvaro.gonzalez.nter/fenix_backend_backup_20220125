package com.haya.alaska.formalizacion.domain;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.forma_de_pago.domain.FormaDePago;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_MSTR_FORMALIZACION_CONTRATO")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_FORMALIZACION_CONTRATO")
public class FormalizacionContrato implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CONTRATO", nullable = false)
  private Contrato contrato;

  //DATOS ECONOMICOS
  @Column(name = "IND_TITULIZADO")
  private Boolean titulizado;

  @Column(name = "NUM_IMPORTE_RETROACTIVIDAD")
  private Double importeRetroactividad;

  @Column(name = "NUM_PRECIO_COMPRA")
  private Double precioCompra; // precioCompra para la propuesta
}
