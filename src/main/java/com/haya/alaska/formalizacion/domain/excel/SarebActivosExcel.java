package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.formalizacion.domain.Formalizacion;
import com.haya.alaska.formalizacion.domain.FormalizacionBien;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class SarebActivosExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add ("Active Sareb Id");
      cabeceras.add ("Registry Property");
      cabeceras.add ("Property Registry");
      cabeceras.add ("Property Registry Number");
      cabeceras.add ("Collateral Loads/Free of charge");
      cabeceras.add ("Assignor Loan Id");
      cabeceras.add ("Id Sareb Loan");
      cabeceras.add ("Active Typology");
      cabeceras.add ("Municipality");
      cabeceras.add ("Province");
      cabeceras.add ("Postal Code");
      cabeceras.add ("Type Via");
      cabeceras.add ("Name Address");
      cabeceras.add ("Address Number");
      cabeceras.add ("Plant Address");
      cabeceras.add ("Door Address");
      cabeceras.add ("Cadastral Reference");
      cabeceras.add ("Purchase Price");
      cabeceras.add ("Valuation Appraisal");
      cabeceras.add ("Lease");
      cabeceras.add ("Occupation");
    } else {
      cabeceras.add("Id Activo Sareb");
      cabeceras.add("Finca Registral");
      cabeceras.add("Registro de la Propiedad");
      cabeceras.add("Numero Registro de la Propiedad");
      cabeceras.add("Colateral Cargas/Libre de Cargas");
      cabeceras.add("Id Préstamo Cedente");
      cabeceras.add("Id Préstamo Sareb");
      cabeceras.add("Tipología Activo");
      cabeceras.add("Municipio");
      cabeceras.add("Provincia");
      cabeceras.add("Código Postal");
      cabeceras.add("Tipo Via");
      cabeceras.add("Nombre Dirección");
      cabeceras.add("Número Dirección");
      cabeceras.add("Dirección Planta");
      cabeceras.add("Dirección Puerta");
      cabeceras.add("Referencia Catastral");
      cabeceras.add("Precio Compra");
      cabeceras.add("Valoración Tasación");
      cabeceras.add("Arrendamiento");
      cabeceras.add("Ocupación");
    }
  }

  public SarebActivosExcel(Bien b, FormalizacionBien fb, Contrato c){
    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      this.add("Active Sareb Id", b.getIdCarga());
      this.add("Registry Property", b.getFinca());
      this.add("Property Registry", b.getLocalidadRegistro());
      this.add("Property Registry Number", b.getRegistro());
      this.add("Collateral Loads/Free of charge", b.getCargas().isEmpty() ? "Libre de Cargas" : "Carga");
      if (c != null){
        this.add("Assignor Loan Id", c.getIdOrigen());
        this.add("Id Sareb Loan", c.getIdCarga());
      }
      this.add("Active Typology", b.getTipoActivo() != null ? b.getTipoActivo().getValor() : null);
      this.add("Municipality", b.getMunicipio() != null ? b.getMunicipio().getValor() : null);
      this.add("Province", b.getProvincia() != null ? b.getProvincia().getValor() : null);
      this.add("Postal Code", b.getCodigoPostal());
      this.add("Type Via", b.getTipoVia() != null ? b.getTipoVia().getValor() : null);
      this.add("Name Address", b.getNombreVia());
      this.add("Address Number", b.getNumero());
      this.add("Plant Address", b.getPiso());
      this.add("Door Address", b.getPuerta());
      this.add("Cadastral Reference", b.getReferenciaCatastral());
      this.add("Purchase Price", b.getImporteBien());
      this.add("Valuation Appraisal", b.getSaldoValoracion());
      this.add("Lease", fb.getAlquiler() != null ? fb.getAlquiler().getValor() : null);
      this.add("Occupation", b.getEstadoOcupacion() != null ? b.getEstadoOcupacion().getValor() : null);
    } else {
      this.add("Id Activo Sareb", b.getIdCarga());
      this.add("Finca Registral", b.getFinca());
      this.add("Registro de la Propiedad", b.getLocalidadRegistro());
      this.add("Numero Registro de la Propiedad", b.getRegistro());
      this.add("Colateral Cargas/Libre de Cargas", b.getCargas().isEmpty() ? "Libre de Cargas" : "Carga");
      if (c != null){
        this.add("Id Préstamo Cedente", c.getIdOrigen());
        this.add("Id Préstamo Sareb", c.getIdCarga());
      }
      this.add("Tipología Activo", b.getTipoActivo() != null ? b.getTipoActivo().getValor() : null);
      this.add("Municipio", b.getMunicipio() != null ? b.getMunicipio().getValor() : null);
      this.add("Provincia", b.getProvincia() != null ? b.getProvincia().getValor() : null);
      this.add("Código Postal", b.getCodigoPostal());
      this.add("Tipo Via", b.getTipoVia() != null ? b.getTipoVia().getValor() : null);
      this.add("Nombre Dirección", b.getNombreVia());
      this.add("Número Dirección", b.getNumero());
      this.add("Dirección Planta", b.getPiso());
      this.add("Dirección Puerta", b.getPuerta());
      this.add("Referencia Catastral", b.getReferenciaCatastral());
      this.add("Precio Compra", b.getImporteBien());
      this.add("Valoración Tasación", b.getSaldoValoracion());
      this.add("Arrendamiento", fb.getAlquiler() != null ? fb.getAlquiler().getValor() : null);
      this.add("Ocupación", b.getEstadoOcupacion() != null ? b.getEstadoOcupacion().getValor() : null);
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
