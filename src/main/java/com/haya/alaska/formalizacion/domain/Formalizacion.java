package com.haya.alaska.formalizacion.domain;

import com.haya.alaska.comprador.domain.Comprador;
import com.haya.alaska.estado_checklist.domain.EstadoChecklist;
import com.haya.alaska.estado_ddl_ddt.domain.EstadoDdlDdt;
import com.haya.alaska.forma_de_pago.domain.FormaDePago;
import com.haya.alaska.gestor_formalizacion.domain.GestorConcursos;
import com.haya.alaska.gestor_formalizacion.domain.GestorGestoria;
import com.haya.alaska.informacion_check_list.domain.InformacionCheckList;
import com.haya.alaska.minuta_revisada_fiscal.domain.MinutaRevisadaFiscal;
import com.haya.alaska.motivo_recomendacion_no_firma.domain.MotivoRecomendacionNoFirma;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.recomendacion_no_firma.domain.RecomendacionNoFirma;
import com.haya.alaska.subestado_formalizacion.domain.SubestadoFormalizacion;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_FORMALIZACION")
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "MSTR_FORMALIZACION")
public class Formalizacion implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @ToString.Include
    private Integer id;

    @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
    private Boolean activo = true;

    @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_PROPUESTA", referencedColumnName = "id", nullable = false)
    Propuesta propuesta;

  @OneToOne(mappedBy = "formalizacion")
  private InformacionCheckList informacionCheckList;

    // 1- Datos del Gestor
    @Column(name = "DES_GESTOR_RECUPERACION_CLIENTE", columnDefinition = "varchar(255)")
    private String gestorRecuperacionCliente;
    @Column(name = "DES_TELEFONO_1_GESTOR_RECUPERACION_CLIENTE", columnDefinition = "varchar(255)")
    private String telefono1GesRecCli;
    @Column(name = "DES_ANALISTA_CLIENTE", columnDefinition = "varchar(255)")
    private String analistaCliente;
    @Column(name = "DES_TELEFONO_1_CLIENTE", columnDefinition = "varchar(255)")
    private String telefono1Cliente;

    @Column(name = "DES_TELEFONO_1_GESTOR_FORMALIZACION", columnDefinition = "varchar(255)")
    private String telefono1GesFormalizacion;
    @Column(name = "DES_GESTOR_FORMALIZACION_CLIENTE", columnDefinition = "varchar(255)")
    private String gestorFormalizacionCliente;
    @Column(name = "DES_TELEFONO_1_GESTOR_FORMALIZACION_CLIENTE", columnDefinition = "varchar(255)")
    private String telefono1GesFormalizacionCliente;

    // 2- Datos Estado
    @Column(name = "FCH_FECHA_SANCION")
    private Date fechaSancion;
    /*@Column(name = "IND_DOCUMENTACION_COMPLETADA")
    private Boolean documentacionCompletada;//Estado checkList*/
    @Column(name = "FCH_SOLICITUD_GESTION_ACTIVOS_VISITA")
    private Date solicitudGestionActivosVisita;
    @Column(name = "FCH_ENVIO_MINUTA_NOTARIA")
    private Date fEnvioMinutaANotaria;
    @Column(name = "FCH_ENVIO_MINUTA_FORM")
    private Date fEnvioMinutaAClienteFormalizacion;
    @Column(name = "FCH_OK_MINUTA_NOTARIA")
    private Date fOkMinutaANotaria;
    @Column(name = "IND_INFORME_VISITA_OK")
    private Boolean informeVisitaOk;
    @Column(name = "FCH_FECHA_FIRMA")
    private Date fechaFirma;
    @Column(name = "FCH_HORA_FIRMA")
    private Time horaFirma;
    @Column(name = "FCH_FECHA_POSESION")
    private Date fechaPosesion;
    @Column(name = "IND_ALQUILER")
    private Boolean alquiler;
    @Column(name = "FCH_FECHA_VIGENCIA")
    private Date fechaVigencia;
    @Column(name = "IND_PBC")
    private Boolean pbc;
    @Column(name = "DES_COMENTARIOS", columnDefinition = "varchar(280)")
    private String comentarios;

    // 3- Notaria
    @Column(name = "DES_DIRECCION_NOTARIA", columnDefinition = "varchar(280)")
    private String direccionNotaria;
    @Column(name = "DES_NOTARIO", columnDefinition = "varchar(255)")
    private String notario;
    @Column(name = "NUM_NUMERO_PROTOCOLO")
    private Integer numeroProtocolo;
    @Column(name = "DES_CONTACTO_OFICINA_CLIENTE", columnDefinition = "varchar(255)")
    private String contactoOficinaCliente;
    @Column(name = "DES_APODERADO_FIRMA", columnDefinition = "varchar(255)")
    private String apoderadoFirma;
    @Column(name = "DES_GESTORIA_TRAMITADORA", columnDefinition = "varchar(255)")
    private String gestoriaTramitadora;
    @Column(name = "DES_OFICINA_NOTARIA", columnDefinition = "varchar(255)")
    private String oficinaNotaria;
    @Column(name = "DES_TELEFONO_NOTARIA", columnDefinition = "varchar(255)")
    private String telefonoNotaria;
    @Column(name = "DES_TELEFONO_OFICIAL_NOTARIA", columnDefinition = "varchar(255)")
    private String telefonoOficialNotaria;
    @Column(name = "DES_EMAIL_OFICIAL_NOTARIA")
    private String emailOficialNotaria;

  // AVANCE DE GESTION
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SUBESTADO_PROPUESTA")
  private SubestadoFormalizacion subestadoFormalizacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FIRMA_ESTIMADA")
  private Date fechaFirmaEstimada;

  /*@Column(name = "IND_CONFIRMADA_FECHA_Y_HORA")
  private Boolean confirmadaFechaYHora;*/

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ENTRADA")
  private Date fechaEntrada;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_VENCIMIENTO_SANCION")
  private Date vencimientoSancion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_OK_DOCUMENTACION")
  private Date fechaOkDocumentacion;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_KO_DOCUMENTACION")
  private Date fechaKoDocumentacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_SOICITUD_MINUTA")
  private Date solicitudMinuta;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_RECEPCION_MINUTA")
  private Date recepcionMinuta;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_REVISION_MINUTA_HRE")
  private Date revisionMinutaHRE;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_RECEPCION_MINUTA_NOTARIA")
  private Date recepcionMinutaDeNotaria;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_COMUNICACION")
  private Date fechaComunicacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ENTRADA_TF")
  private Date fechaEntradaTF;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_AVANCE_TAREA_TF")
  private Date fechaAvanceTareaTF;

  @Column(name = "DES_OBSERVACIONES")
  @Lob
  private String observaciones;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RECOMENDACION_NO_FIRMA")
  private RecomendacionNoFirma recomendacionNoFirma;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MOTIVO_RECOMENDACION_NO_FIRMA")
  private MotivoRecomendacionNoFirma motivoRecomendacionNoFirma;

  /*@ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MINUTA_REVISADA_FISCAL")
  private MinutaRevisadaFiscal minutaRevisadaFiscal;*/

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_REVISADA_FISCAL")
  private Date minutaRevisadaFiscal;


  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_REVISION_CONCURSOS")
  private Date fechaRevisionConcursos;


  //PRINCIPAL
  @Column(name = "IND_DACION_SINGULAR")
  private Boolean dacionSingular;

  @Column(name = "NUM_TOTAL_FFRR")
  private Integer totalFFRR; //Cantidad dedeposito bienes de la propuesta

  @Column(name = "DES_ALERTA_EN_COMUNICACION")
  private String alertaEnComunicacion;


  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_COMPRADOR")
  private Comprador comprador;

  @Column(name = "IND_PENDIENTE_RATIFICACION_VENDEDOR")
  private Boolean pendienteRatificacionVendedor;

  @Column(name = "IND_PENDIENTE_RATIFICACION_COMPRADOR")
  private Boolean pendienteRatificacionComprador;

  @Column(name = "IND_PENDIENTE_RATIFICACION_BANCO")
  private Boolean pendienteRatificacionBanco;

  @Column(name = "IND_FIRMA_CONDICIONADA")
  private Boolean firmaCondicionada; //true: suspensiva, false: resolutoria

  @Column(name = "IND_EMBARGO_OTROS_ASUME")
  private Boolean embargoOtrosAsume;
  @Column(name = "NUM_EMBARGO_OTROS_IMPORTE")
  private Double embargoOtrosImporte;

  @Column(name = "DES_OBSERVACIONES_PRINCIPAL")
  @Lob
  private String observacionesPrincipal;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_FORMA_DE_PAGO")
  private FormaDePago formaDePago;

  @Column(name = "IND_GASTOS_ASUME")
  private Boolean gastosAsume;
  @Column(name = "NUM_GASTOS_IMPORTE")
  private Double gastosImporte;

  @Column(name = "IND_PLUSVALIA_ASUME")
  private Boolean plusvaliaAsume;
  @Column(name = "NUM_PLUSVALIA_IMPORTE")
  private Double plusvaliaImporte;

  @Column(name = "IND_IBI_ASUME")
  private Boolean ibiAsume;
  @Column(name = "NUM_IBI_IMPORTE")
  private Double ibiImporte; // Calculado Para las viejas

  @Column(name = "IND_CP_ASUME")
  private Boolean cpAsume;
  @Column(name = "NUM_CP_IMPORTE")
  private Double cpImporte; // Calculado Para las viejas

  @Column(name = "IND_URBANIZACION_ASUME")
  private Boolean urbanizacionAsume;
  @Column(name = "NUM_URBANIZACION_IMPORTE")
  private Double urbanizacionImporte; // Calculado Para las viejas

  @Column(name = "NUM_IMPORTE_ENTREGA_DE_EFECTIVO")
  private Double importeEntregaEfectivo; // Calculado Para las viejas

  @Column(name = "IND_ENTREGA_DE_BIENES_LIBRES")
  private Boolean entregaDeBienesLibres;

  //DD
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ENCARGO_DDL")
  private Date fechaEncargoDDL;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_RECEPCION_DDL")
  private Date fechaRecepcionDDL;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_DDL")
  private EstadoDdlDdt estadoDDL;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ENCARGO_DDT")
  private Date fechaEncargoDDT;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_RECEPCION_DDT")
  private Date fechaRecepcionDDT;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_DDT")
  private EstadoDdlDdt estadoDDT;

  @Column(name = "DES_DESPACHO_LEGAL")
  private String despachoLegal;

  @Column(name = "DES_DESPACHO_TECNICO")
  private String despachoTecnico;

 /* @Column(name = "IND_DD_RECIBIDA")
  private Boolean ddRecibida;*/

  @Column(name = "IND_DD_REVISADA_Y_SUBIDA")
  private Boolean ddRevisadaYSubida;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_SUBIDA_DD")
  private Date fechaSubidaDD;

  @Column(name = "NUM_HONORARIOS_DD")
  private Double honorariosDD;
  @Column(name = "NUM_HONORARIOS_ASISTENCIA_FIRMA")
  private Double honorariosAsistenciaFirma;

  @Column(name = "DES_VALORACION_DD_CLIENTE")
  private String valoracionDDCliente;
  @Column(name = "DES_HORARIOS_DD_TECNICA")
  private String horariosDDTecnica;

  //CONTACTOS

  /*@ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_GESTOR_FORMALIZACION_LEGAL")
  private Usuario gestorFormalizacionLegal; // Usuarios con perfil 'por definir'*/

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_GESTOR_CONCURSOS")
  private GestorConcursos gestorConcursos;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_GESTOR_GESTORIA")
  private GestorGestoria gestorGestoria;

  @Column(name = "DES_TERRITORIAL")
  private String territorial;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA_NOTARIA")
  private Provincia provinciaNotaria;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO_NOTARIA")
  private Municipio municipioNotaria;

  //SAREB
  @Column(name = "NUM_NBV")
  private Double nbv;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_TECLEO_EN_PRISMA")
  private Date fechaTecleoEnPrisma;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_ELEVACION_NILO")
  private Date fechaElevacionNILO;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_CIERRE_NILO")
  private Date fechaCierreNILO;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ESTADO_CHECK_LIST")
  private EstadoChecklist estadoChecklist;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_INICIO_CHECK_LIST")
  private Date fechaInicioCheckList;
  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_FIN_CHECK_LIST")
  private Date fechaFinCheckList;

  @Column(name = "DES_ID_CARGA_EXPEDIENTE")
  private String expedienteOrigen;
}
