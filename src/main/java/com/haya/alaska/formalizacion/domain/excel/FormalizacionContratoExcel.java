package com.haya.alaska.formalizacion.domain.excel;

import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.formalizacion.domain.FormalizacionContrato;
import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class FormalizacionContratoExcel extends ExcelUtils {
  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Loan No.");
      cabeceras.add("Status");
      cabeceras.add("Product");
      cabeceras.add("Titled");
      cabeceras.add("Balance in Management");
      cabeceras.add("Purchase Price");
      cabeceras.add("Debt Amount");
      cabeceras.add("Remove");
      cabeceras.add("Rest of Claimable Debt");
      cabeceras.add("Initial Loan Amount");
      cabeceras.add("Retroactive Amount");
    } else {
      cabeceras.add("Nº Préstamo");
      cabeceras.add("Estado");
      cabeceras.add("Producto");
      cabeceras.add("Titulizado");
      cabeceras.add("Saldo en Gestión");
      cabeceras.add("Precio de Compra");
      cabeceras.add("Importe de la Deuda");
      cabeceras.add("Quita");
      cabeceras.add("Resto Deuda Reclamable");
      cabeceras.add("Importe del Préstamo Inicial");
      cabeceras.add("Importe Retroactividad");
    }
  }

  public FormalizacionContratoExcel(Contrato c, FormalizacionContrato fc) {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Loan No.", c.getIdCarga());
      this.add(
          "Status", c.getEstadoContrato() != null ? c.getEstadoContrato().getValorIngles() : null);
      this.add("Product", c.getProducto() != null ? c.getProducto().getValorIngles() : null);
      if (fc.getTitulizado() != null) this.add("Titled", fc.getTitulizado() ? "Yes" : "No");
      this.add("Balance in Management", c.getSaldoGestion());
      this.add("Purchase Price", fc.getPrecioCompra());
      this.add("Debt Amount", c.getDeudaImpagada());
      this.add("Remove", c.getRestoHipotecario());
      this.add(
          "Rest of Claimable Debt",
          c.getUltimoSaldo() != null ? c.getUltimoSaldo().getCapitalPendiente() : null);
      this.add("Initial Loan Amount", c.getImporteInicial());
      this.add("Retroactive Amount", fc.getImporteRetroactividad());
    } else {
      this.add("Nº Préstamo", c.getIdCarga());
      this.add("Estado", c.getEstadoContrato() != null ? c.getEstadoContrato().getValor() : null);
      this.add("Producto", c.getProducto() != null ? c.getProducto().getValor() : null);
      if (fc.getTitulizado() != null) this.add("Tituizado", fc.getTitulizado() ? "Si" : "No");
      this.add("Saldo en Gestión", c.getSaldoGestion());
      this.add("Precio de Compra", fc.getPrecioCompra());
      this.add("Importe de la Deuda", c.getDeudaImpagada());
      this.add("Quita", c.getRestoHipotecario());
      this.add(
          "Resto Deuda Reclamable",
          c.getUltimoSaldo() != null ? c.getUltimoSaldo().getCapitalPendiente() : null);
      this.add("Importe del Préstamo Inicial", c.getImporteInicial());
      this.add("Importe Retroactividad", fc.getImporteRetroactividad());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
