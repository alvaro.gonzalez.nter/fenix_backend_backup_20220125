package com.haya.alaska.informacion_check_list.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@Setter
public class CheckListString {
  private String sancionDelExpediente;
  private String importeEstimado;
  private String documentoCertificadoDisponible;
}
