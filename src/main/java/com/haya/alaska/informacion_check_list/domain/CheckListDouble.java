package com.haya.alaska.informacion_check_list.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@Setter
public class CheckListDouble {
  private String sancionDelExpediente;
  private Double importeEstimado;
  private String documentoCertificadoDisponible;
}
