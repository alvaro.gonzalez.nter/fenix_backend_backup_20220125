package com.haya.alaska.informacion_check_list.domain;

import com.haya.alaska.formalizacion.domain.Formalizacion;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_MSTR_INFORMACION_CHECK_LIST")
@NoArgsConstructor
@Entity
@Setter
@Getter
@AllArgsConstructor
@Table(name = "MSTR_INFORMACION_CHECK_LIST")
public class InformacionCheckList implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_FORMALIZACION", nullable = false)
  private Formalizacion formalizacion;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_SAD_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_SAD_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_SAD_DCD") )
  } )
  @Embedded
  private CheckListString sancionAceptadaDeudor;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_LPO_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_LPO_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_LPO_DCD") )
  } )
  @Embedded
  private CheckListString lpo;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_CFO_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_CFO_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_CFO_DCD") )
  } )
  @Embedded
  private CheckListString cfo;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_ARR_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_ARR_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_ARR_DCD") )
  } )
  @Embedded
  private CheckListString arrendamientos;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_FAR_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_FAR_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_FAR_DCD") )
  } )
  @Embedded
  private CheckListString finanzasArrendamientosDepositadas;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_OCU_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_OCU_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_OCU_DCD") )
  } )
  @Embedded
  private CheckListString ocupaciones;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_DPP_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_DPP_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_DPP_DCD") )
  } )
  @Embedded
  private CheckListDouble deudaPendienteEnComPropietarios;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_DPE_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_DPE_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_DPE_DCD") )
  } )
  @Embedded
  private CheckListDouble deudaPendienteEUC;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_DPI_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_DPI_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_DPI_DCD") )
  } )
  @Embedded
  private CheckListDouble deudaPendienteEnImpuestosYTasasMunicipales;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_CUR_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_CUR_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_CUR_DCD") )
  } )
  @Embedded
  private CheckListInteger cargasUrbanisticas;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_DCU_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_DCU_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_DCU_DCD") )
  } )
  @Embedded
  private CheckListDouble deudaPorCargasUrbanisticas;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_EMB_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_EMB_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_EMB_DCD") )
  } )
  @Embedded
  private CheckListString embargos;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_HIP_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_HIP_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_HIP_DCD") )
  } )
  @Embedded
  private CheckListString hipotecas;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_PCA_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_PCA_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_PCA_DCD") )
  } )
  @Embedded
  private CheckListString plusvaliaCargoDelAdquirente;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_GFO_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_GFO_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_GFO_DCD") )
  } )
  @Embedded
  private CheckListDouble gastosDeFormalizacion;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_PGA_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_PGA_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_PGA_DCD") )
  } )
  @Embedded
  private CheckListDouble previsionDeGastos;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_AEF_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_AEF_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_AEF_DCD") )
  } )
  @Embedded
  private CheckListDouble aportacionDeEfectivo;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_AAC_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_AAC_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_AAC_DCD") )
  } )
  @Embedded
  private CheckListDouble aportacionDeActivos;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_CRE_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_CRE_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_CRE_DCD") )
  } )
  @Embedded
  private CheckListDouble condonacionDeRemanente;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_OCO_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_OCO_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_OCO_DCD") )
  } )
  @Embedded
  private CheckListString operacionConcursal;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_OCR_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_OCR_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_OCR_DCD") )
  } )
  @Embedded
  private CheckListDouble operacionConcursalRiesgo;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_OCD_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_OCD_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_OCD_DCD") )
  } )
  @Embedded
  private CheckListString operacionConcursalDispone;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_APR_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_APR_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_APR_DCD") )
  } )
  @Embedded
  private CheckListString altaEnPrisma;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_CLS_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_CLS_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_CLS_DCD") )
  } )
  @Embedded
  private CheckListString checkListSustituta;

  @AttributeOverrides( {
    @AttributeOverride(name="sancionDelExpediente", column = @Column(name="DES_LCO_SDE") ),
    @AttributeOverride(name="importeEstimado", column = @Column(name="DES_LCO_IE") ),
    @AttributeOverride(name="documentoCertificadoDisponible", column = @Column(name="DES_LCO_DCD") )
  } )
  @Embedded
  private CheckListString listaDeContraste;
}
