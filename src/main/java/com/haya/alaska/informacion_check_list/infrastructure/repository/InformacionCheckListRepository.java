package com.haya.alaska.informacion_check_list.infrastructure.repository;

import com.haya.alaska.informacion_check_list.domain.InformacionCheckList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InformacionCheckListRepository extends JpaRepository<InformacionCheckList, Integer> {
  Optional<InformacionCheckList> findByFormalizacionPropuestaId(Integer idPropuesta);
}
