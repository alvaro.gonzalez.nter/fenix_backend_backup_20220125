package com.haya.alaska.opcion_permiso.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.opcion_permiso.domain.OpcionPermiso;
import org.springframework.stereotype.Repository;

@Repository
public interface OpcionPermisoRepository extends CatalogoRepository<OpcionPermiso, Integer> {}
