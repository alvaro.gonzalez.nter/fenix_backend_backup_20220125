package com.haya.alaska.opcion_permiso.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.permiso_disponible.domain.PermisoDisponible;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Audited
@AuditTable(value = "HIST_LKUP_OPCION_PERMISO")
@Entity
@Table(name = "LKUP_OPCION_PERMISO")
public class OpcionPermiso extends Catalogo {
  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_OPCION_PERMISO")
  @NotAudited
  private List<PermisoDisponible> permisosDisponibles = new ArrayList<>();
}
