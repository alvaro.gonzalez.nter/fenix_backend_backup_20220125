package com.haya.alaska.busqueda_skip_tracing.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_BUSQUEDA_SKIP_TRACING")
@Entity
@Table(name = "LKUP_BUSQUEDA_SKIP_TRACING")
@NoArgsConstructor
public class BusquedaSkipTracing extends Catalogo {
  @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
  @JoinTable(
    name = "RELA_SKIP_TRACING_BUSQUEDA_SKIP_TRACING",
    joinColumns = @JoinColumn(name = "ID_BUSQUEDA"),
    inverseJoinColumns = @JoinColumn(name = "ID_SKIP_TRACING"))
  @AuditJoinTable(name = "HIST_RELA_SKIP_TRACING_BUSQUEDA_SKIP_TRACING")
  private Set<SkipTracing> skipTracings = new HashSet<>();
}
