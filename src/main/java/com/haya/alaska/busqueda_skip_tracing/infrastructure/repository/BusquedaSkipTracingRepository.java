package com.haya.alaska.busqueda_skip_tracing.infrastructure.repository;

import com.haya.alaska.busqueda_skip_tracing.domain.BusquedaSkipTracing;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusquedaSkipTracingRepository extends CatalogoRepository<BusquedaSkipTracing, Integer> {}
