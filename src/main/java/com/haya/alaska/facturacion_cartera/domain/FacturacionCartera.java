package com.haya.alaska.facturacion_cartera.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.domain.Catalogo;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_FACTURACION_CARTERA")
@Entity
@Table(name = "LKUP_FACTURACION_CARTERA")
public class FacturacionCartera extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_FACTURACION_CARTERA")
  @NotAudited
  private Set<Cartera> carteras = new HashSet<>();
}
