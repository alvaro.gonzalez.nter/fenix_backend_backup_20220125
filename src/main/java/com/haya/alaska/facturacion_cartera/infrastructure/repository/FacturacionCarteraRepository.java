package com.haya.alaska.facturacion_cartera.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.facturacion_cartera.domain.FacturacionCartera;

@Repository
public interface FacturacionCarteraRepository extends CatalogoRepository<FacturacionCartera, Integer> {}
