package com.haya.alaska.tratamiento_reo.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.domain.Catalogo;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TRATAMIENTO_REO")
@Entity
@Table(name = "LKUP_TRATAMIENTO_REO")
public class TratamientoReo extends Catalogo {
    @OneToMany(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_TRATAMIENTO_REO")
    @NotAudited
    private Set<Cartera> carteras = new HashSet<>();
}
