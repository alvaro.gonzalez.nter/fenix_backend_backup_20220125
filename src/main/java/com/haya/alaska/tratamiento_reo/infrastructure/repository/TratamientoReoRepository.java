package com.haya.alaska.tratamiento_reo.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tratamiento_reo.domain.TratamientoReo;
import org.springframework.stereotype.Repository;

@Repository
public interface TratamientoReoRepository extends CatalogoRepository<TratamientoReo, Integer> {}
