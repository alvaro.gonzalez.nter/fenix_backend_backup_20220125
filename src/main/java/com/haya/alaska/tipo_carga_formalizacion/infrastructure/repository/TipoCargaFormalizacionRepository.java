package com.haya.alaska.tipo_carga_formalizacion.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_carga_formalizacion.domain.TipoCargaFormalizacion;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoCargaFormalizacionRepository extends CatalogoRepository<TipoCargaFormalizacion, Integer> {
}
