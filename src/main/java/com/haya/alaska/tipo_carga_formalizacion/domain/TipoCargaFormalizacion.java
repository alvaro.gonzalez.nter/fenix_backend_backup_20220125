package com.haya.alaska.tipo_carga_formalizacion.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_CARGA_FORMALIZACION")
@Entity
@Table(name = "LKUP_TIPO_CARGA_FORMALIZACION")
public class TipoCargaFormalizacion extends Catalogo {

}
