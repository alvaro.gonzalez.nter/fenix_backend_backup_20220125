package com.haya.alaska.dato_direccion_skip_tracing.domain;

import com.haya.alaska.datos_origen.domain.DatosOrigen;
import com.haya.alaska.direccion.domain.Direccion;
import com.haya.alaska.entorno.domain.Entorno;
import com.haya.alaska.interviniente_skip_tracing.domain.IntervinienteST;
import com.haya.alaska.localidad.domain.Localidad;
import com.haya.alaska.municipio.domain.Municipio;
import com.haya.alaska.pais.domain.Pais;
import com.haya.alaska.provincia.domain.Provincia;
import com.haya.alaska.tipo_via.domain.TipoVia;
import lombok.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_DATO_DIRECCION_SKIP_TRACING")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_DATO_DIRECCION_SKIP_TRACING")
public class DatoDireccionST implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  @ToString.Include
  private Integer id;

  @Column(name = "IND_PRINCIPAL")
  private Boolean principal = false;

  @Column(name = "IND_VALIDADO")
  private Boolean validado;

  @Temporal(TemporalType.DATE)
  @Column(name = "FCH_VALIDACION")
  private Date fechaValidacion;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_VIA")
  private TipoVia tipoVia;
//Revisar campos

  @Column(name = "DES_NOMBRE")
  private String nombre;

  @Column(name = "DES_BLOQUE")
  private String bloque;

  @Column(name = "DES_ESCALERA")
  private String escalera;

  @Column(name = "DES_PISO")
  private String piso;

  @Column(name = "DES_PUERTA")
  private String puerta;

  @Column(name = "DES_PORTAL")
  private String portal;

  @Column(name = "DES_NUMERO")
  private String numero;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_ENTORNO")
  private Entorno entorno;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_MUNICIPIO")
  private Municipio municipio;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_LOCALIDAD")
  private Localidad localidad;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PROVINCIA")
  private Provincia provincia;

  @Column(name = "DES_COMENTARIO")
  private String comentario;

  @Column(name = "DES_CODIGO_POSTAL")
  private String codigoPostal;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PAIS")
  private Pais pais;

  @Column(name = "NUM_LONGITUD")
  private Double longitud;
  @Column(name = "NUM_LATITUD")
  private Double latitud;

  //Campo imagen no añadido revisar como hacer con gestor documental

  /*@Temporal(TemporalType.DATE)
  @Column(name = "FCH_ALTA")
  private Date fechaAlta;*/

  @Column(name = "DES_OBSERVACIONES")
  private String observaciones;

  @OneToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_DIRECCION")
  private Direccion datoDireccion;

  @OneToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_DATOS_ORIGEN")
  private DatosOrigen datosOrigen;

  @ManyToOne
  @JoinColumn(name = "ID_INTERVINIENTE_SKIP_TRACING")
  private IntervinienteST intervinienteST;

  @ElementCollection()
  @CollectionTable(name = "RELA_DATO_DIRECCION_ST_FOTOS", joinColumns = @JoinColumn(name = "ID_DATO_DIRECCION_ST"))
  @Column(name = "ID_FOTO_GESTOR_DOCUMENTAL")
  @NotAudited
  private Set<Long> fotos=new HashSet<>();

  @Column(name = "IND_FINCA_HIPOTECADA")
  private Boolean fincaHipotecada;

  public DatoDireccionST(Direccion direccion) {
    BeanUtils.copyProperties(direccion,this);
  }

  public void addFoto(long idDocumento) {
    this.fotos.add(idDocumento);
  }

  public String verEnGoogle() {
    if ((this.latitud != null) && (this.longitud != null)) {
      return "https://www.google.com/maps/search/?api=1&query="
        + this.latitud
        + " ,"
        + this.longitud;
    } else {
      return null;
    }
  }
}
