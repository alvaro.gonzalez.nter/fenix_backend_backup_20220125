package com.haya.alaska.dato_direccion_skip_tracing.infraestructure.repository;

import com.haya.alaska.dato_direccion_skip_tracing.domain.DatoDireccionST;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DatoDireccionSTRepository extends JpaRepository<DatoDireccionST, Integer> {
  Optional<DatoDireccionST> findByDatoDireccionId(Integer direccionId);
}

