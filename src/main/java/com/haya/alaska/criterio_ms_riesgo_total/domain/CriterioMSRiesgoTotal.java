package com.haya.alaska.criterio_ms_riesgo_total.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Audited
@AuditTable(value = "HIST_LKUP_CRITERIO_MS_RIESGO_TOTAL")
@Entity
@Table(name = "LKUP_CRITERIO_MS_RIESGO_TOTAL")
@NoArgsConstructor
@Getter
@Setter
public class CriterioMSRiesgoTotal extends Catalogo {
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  private Cartera cartera;
}
