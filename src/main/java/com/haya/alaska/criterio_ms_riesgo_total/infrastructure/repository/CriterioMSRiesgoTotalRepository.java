package com.haya.alaska.criterio_ms_riesgo_total.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.criterio_ms_riesgo_total.domain.CriterioMSRiesgoTotal;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CriterioMSRiesgoTotalRepository extends CatalogoRepository<CriterioMSRiesgoTotal, Integer> {
  List<CriterioMSRiesgoTotal> findAllByCarteraId(Integer id);
  List<CriterioMSRiesgoTotal> findAllByCarteraIdAndActivoIsTrue(Integer ido);
}
