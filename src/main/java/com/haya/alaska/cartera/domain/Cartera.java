package com.haya.alaska.cartera.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.asignacion_usuario_cartera.domain.AsignacionUsuarioCartera;
import com.haya.alaska.business_plan.domain.BusinessPlan;
import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;
import com.haya.alaska.cartera_fecha_excepcion.domain.CarteraFechaExcepcion;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.criterio_generacion_expediente.domain.CriterioGeneracionExpediente;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.facturacion_cartera.domain.FacturacionCartera;
import com.haya.alaska.movimiento.domain.Movimiento;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.permiso_asignado.domain.PermisoAsignado;
import com.haya.alaska.plazo.domain.Plazo;
import com.haya.alaska.saldo.domain.Saldo;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import com.haya.alaska.tratamiento_reo.domain.TratamientoReo;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_CARTERA")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MSTR_CARTERA")
public class Cartera implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  //indicador de cartera para parametrizar una sola cartera, puesto que la parametrización es común
  @Column(name = "IND_PRINCIPAL")
  private Boolean principal;

  @Column(name = "DES_ID_HAYA")
  private String idHaya;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<ClienteCartera> clientes = new HashSet<>();

  @Column(name = "DES_NOMBRE")
  private String nombre;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESPONSABLE_CARTERA")
  private Usuario responsableCartera;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESPONSABLE_SKIP_TRACING")
  private Usuario responsableSkipTracing;
  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_RESPONSABLE_FORMALIZACION")
  private Usuario responsableFormalizacion;

  @Column(name = "IND_DATOS_CORE_BANKING")
  private Boolean datosCoreBanking = false;

  @Column(name = "IND_DATOS_JUDICIAL")
  private Boolean datosJudicial = false;

  @Column(name = "IND_DATOS_CONTABILIDAD")
  private Boolean datosContabilidad = false;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CRITERIO_GENERACION_EXPEDIENTE")
  private CriterioGeneracionExpediente criterioGeneracionExpediente;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_FACTURACION_CARTERA")
  private FacturacionCartera facturacion;

  @Column(name = "IND_ADMINISTRADA")
  private Boolean administrada = false;

  @Column(name = "NUM_TOLERANCIA")
  private Integer tolerancia;

  @Column(name = "DES_EMAIL")
  private String email;

  @Column(name = "NUM_DIAS_RETRASO_CANCELACION")
  private Integer diasRestrasoCancelacion;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<Expediente> expedientes = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<ExpedientePosesionNegociada> expedientesPosesionNegociada = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<CarteraFechaExcepcion> carteraFechaExcepciones = new HashSet<>();

  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @Column(name = "DES_ID_CARGA")
  private String idCarga;

  @Column(name = "DES_ID_CARGA_SUBCARTERA")
  private String idCargaSubcartera;

  @Column(name = "DES_TIPO_ACTIVOS")
  private String tipoActivos;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @OrderBy("orden")
  @NotAudited
  private Set<CarteraContratoRepresentante> criterioContratoRepresentante = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TRATAMIENTO_REO")
  private TratamientoReo tratamientoReo;

  @ManyToMany
  @JoinTable(name = "RELA_CARTERA_PERFIL",
    joinColumns = { @JoinColumn(name = "ID_CARTERA") },
    inverseJoinColumns = { @JoinColumn(name = "ID_PERFIL") }
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_CARTERA_PERFIL")
  private Set<Perfil> perfiles = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<PermisoAsignado> permisos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<BusinessPlan> businessPlans = new HashSet<>();

  @OneToOne(
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_PLAZO")
  private Plazo plazo;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<Movimiento> movimientos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<Saldo> saldos = new HashSet<>();

  @Column(name = "DES_ID_CARTERA_HAYA")
  private Integer idCarteraHaya;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<Segmentacion> segmentacion = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<SegmentacionPN> segmentacionPN = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_CARTERA")
  @NotAudited
  private Set<AsignacionUsuarioCartera> asignacionesUsuarioCartera = new HashSet<>();

  @Column(name = "IND_FORMALIZACION", nullable = false, columnDefinition = "boolean default false")
  private Boolean formalizacion = false;

  public Cliente getCliente() {
    ClienteCartera cc = this.getClientes().stream().findFirst().orElse(null);
    return cc != null ? cc.getCliente() : null;
  }
}
