package com.haya.alaska.cartera.infrastructure.controller;

import com.haya.alaska.cartera.application.CarteraUseCase;
import com.haya.alaska.cartera.infrastructure.controller.dto.*;
import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/carteras")
public class CarteraController {

  @Autowired
  private CarteraUseCase carteraUseCase;

  @ApiOperation(value = "Listado de contratos de la cartera",
          notes = "Devuelve todos los contratos perteneciantes a los expedientes de la cartera.")
  @GetMapping("/{id}/contratos")
  public List<ContratoDTOToList> getContratosByCartera(@PathVariable("id") Integer id) {
    return carteraUseCase.getContratosByCartera(id);
  }

  @ApiOperation(value = "Actualizar cartera", notes = "Actualiza una cartera con el id enviado.")
  @PutMapping("/{idCartera}")
  public CarteraOutputDTO createCartera(@PathVariable("idCartera") Integer idCartera, @RequestBody @Valid CarteraInputDTO carteraInputDTO)
          throws Exception {
    CarteraOutputDTO result = carteraUseCase.generateCartera(carteraInputDTO, idCartera);
    return result;

  }

  @ApiOperation(value = "Listado de carteras por cliente",
    notes = "Devuelve todas las carteras registradas en función del id del cliente enviado.")
  @GetMapping("/{idCliente}")
  public List<CarteraDTOToList> getCarteraByCliente(
    @PathVariable("idCliente") Integer idCliente,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return carteraUseCase.getCarterasByCliente(idCliente, usuario, false);
  }

  @ApiOperation(value = "Listado de carteras por cliente con asignaciones",
    notes = "Devuelve todas las carteras registradas en función del id del cliente enviado.")
  @GetMapping("/asignadas/{idCliente}")
  public List<CarteraDTOToList> getCarteraByClienteAsignado(
    @PathVariable("idCliente") Integer idCliente,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return carteraUseCase.getCarterasByCliente(idCliente, usuario, true);
  }

  @ApiOperation(value = "Listado de carteras por lista de clientes",
    notes = "Devuelve todas las carteras registradas en función de los ids de los clientes enviados.")
  @GetMapping("/carterasCliente")
  public List<CarteraDTOToList> getCarterasByClientes(
    @RequestParam(value="idsClientes") List<Integer> idsClientes,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return carteraUseCase.getCarterasByClientes(idsClientes, usuario);
  }

  @ApiOperation(value = "Listado de carteras de formalizacion por cliente",
    notes = "Devuelve todas las carteras registradas con el boolean de formalizacion a true.")
  @GetMapping("/formalizacion")
  public List<CarteraDTOToList> getCarteraFormalizacionByCliente(
    @RequestParam(value = "idCliente", required = false) Integer idCliente,
    @ApiIgnore CustomUserDetails principal) {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return carteraUseCase.getCarterasFormalizacion(idCliente);
  }

  @ApiOperation(value = "Listado de carteras por usuario y cliente",
    notes = "Devuelve todas las carteras registradas en función del usuario que ha iniciado sesion")
  @GetMapping("/usuario/{idCliente}")
  public List<CarteraDTOToList> getCarteraByUsuario(
    @PathVariable("idCliente") Integer idCliente,
    @ApiIgnore CustomUserDetails principal) {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return carteraUseCase.getCarterasByUserAlways(idCliente, usuario);
  }

  @ApiOperation(value = "Listado de carteras",
    notes = "Devuelve todas las carteras registradas en bbdd.")
  @GetMapping
  public List<CarteraDTOToList> getAll() {
    return carteraUseCase.getAll();
  }

  @ApiOperation(value = "Listado de carteras",
    notes = "Devuelve todas las carteras registradas en bbdd.")
  @GetMapping("/nombre/{nombre}")
  public CarteraOutputDTO getCartera(@RequestParam(value = "idCliente") Integer idCliente,
                                     @PathVariable(value = "nombre") String nombre) throws NotFoundException {
    return carteraUseCase.getCarterasByClienteAndNombre(idCliente, nombre);
  }

  /*@ApiOperation(value = "Listado de grupos cliente",
    notes = "Devuelve todos los grupos cliente registradas en bbdd.")
  @GetMapping("/grupoCliente")
  public List<GrupoClienteOutputDTO> getAllGrupoCliente(){
    return carteraUseCase.getAllGrupoCliente();
  }*/

  @ApiOperation(value = "Listado de grupos cliente de un usuario",
    notes = "Devuelve todos los grupos cliente asociados a un usuario.")
  @GetMapping("/grupoCliente")
  public List<GrupoClienteOutputDTO> getAllGrupoClienteByUsuario(@ApiIgnore CustomUserDetails principal){
    Usuario usuario = (Usuario) principal.getPrincipal();
    return carteraUseCase.getGrupoClienteByUsuario(usuario);
  }

  @ApiOperation(value = "Listado de grupos cliente de un usuario",
    notes = "Devuelve todos los grupos cliente asociados a un usuario.")
  @GetMapping("/grupoCliente/asignados")
  public List<GrupoClienteOutputDTO> getAllGrupoClienteAsignadosByUsuario(@ApiIgnore CustomUserDetails principal) throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return carteraUseCase.getGrupoClienteAsignadosByUsuario(usuario);
  }

  @ApiOperation(value = "Listado de clientes",
    notes = "Devuelve todos los clientes registrados en bbdd.")
  @GetMapping("/grupoCliente/{idGrupo}")
  public List<ClienteOutputDTO> getClientes(@PathVariable(value = "idGrupo") Integer idGrupo) throws NotFoundException{
    return carteraUseCase.getClientes(idGrupo);
  }

  @ApiOperation(value = "Listado de clientes",
    notes = "Devuelve todos los clientes registrados en bbdd.")
  @GetMapping("/grupoCliente/asignados/{idGrupo}")
  public List<ClienteOutputDTO> getClientesAsignados(@PathVariable(value = "idGrupo") Integer idGrupo, @ApiIgnore CustomUserDetails principal) throws NotFoundException{
    Usuario usuario = (Usuario) principal.getPrincipal();
    return carteraUseCase.getClientesAsignados(idGrupo, usuario);
  }

  @ApiOperation(value = "Listado de clientes de un usuario",
    notes = "Devuelve todos los clientes asociados a un usuario.")
  @GetMapping("/grupoClienteUsuario/{idGrupo}")
  public List<ClienteOutputDTO> getClientesByUsuario(@PathVariable(value = "idGrupo") Integer idGrupo,
                                                     @ApiIgnore CustomUserDetails principal) throws NotFoundException{
    Usuario usuario = (Usuario) principal.getPrincipal();
    return carteraUseCase.getClientesByUsuario(idGrupo,usuario);
  }


  @ApiOperation(value = "Añadir un documento a una cartera")
  @PostMapping("/{idCartera}/documentos")
  @ResponseStatus(HttpStatus.CREATED)
  public void createDocumentoCartera(
    @PathVariable("idCartera") Integer idCartera,
    @RequestPart("file") @NotNull @NotBlank MultipartFile file,
    @RequestParam("idMatricula") String idMatricula,
    @RequestParam("tipoModeloCartera") String tipoModeloCartera,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    carteraUseCase.createDocumento(idCartera, idMatricula,file, usuario,tipoModeloCartera);
  }

  @ApiOperation(
    value = "Listado de carteras asignadas",
    notes = "Listado de carteras asociadas al usuario logeado.")
  @GetMapping("/byUser")
  public List<CarteraDTOToList> getCarteras(@ApiIgnore CustomUserDetails principal)
    throws NotFoundException {
    Usuario usuario = (Usuario) principal.getPrincipal();
    return carteraUseCase.getCarteras(usuario);
  }

  @ApiOperation(value = "Obtener documentación de contrato", notes = "Devuelve la documentación disponible del contrato con id enviado")
  @PostMapping("/{idCartera}/documentosObt")
  @Transactional
  public ListWithCountDTO<DocumentoRepositorioDTO> getDocumentosByContrato(
    @PathVariable(value = "idCartera",required = false) Integer idCartera,
    @RequestBody(required = false) BusquedaRepositorioDTO busquedaRepositorioDTO,
    @RequestParam(value="idDocumento",required = false) String idDocumento,
    @RequestParam(value="usuarioCreador",required = false) String usuarioCreador,
    @RequestParam(value="tipoDocumento",required = false) String tipoDocumento,
    @RequestParam(value="tipoModeloCartera",required = false) String tipoModeloCartera,
    @RequestParam(value="nombreDocumento",required = false) String nombreDocumento,
    @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
    @RequestParam(value = "fechaAlta", required = false) String fechaAlta,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page) throws Exception {
 return carteraUseCase.findDocumentosByCartera(busquedaRepositorioDTO,idCartera,idDocumento,usuarioCreador,tipoDocumento,tipoModeloCartera,nombreDocumento,fechaActualizacion,fechaAlta,orderDirection,orderField,size,page);
  }





  @ApiOperation(value = "Añadir un documento a un contenedor general")
  @PostMapping("/documentosGeneral")
  @ResponseStatus(HttpStatus.CREATED)
    public void createDocumentoGeneral(
    @RequestPart("file") @NotNull @NotBlank MultipartFile file,
    @RequestParam("idMatricula") String idMatricula,
   // @RequestParam("tipoDocumento") String tipoDocumento,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    Usuario usuario = (Usuario) principal.getPrincipal();
    carteraUseCase.createDocumentoGeneral(idMatricula, file, usuario);
  }


  @ApiOperation(value = "Obtener documentación de contrato", notes = "Devuelve la documentación disponible del contrato con id enviado")
  @PostMapping("/documentosGeneralObtener")
  @Transactional
  public ListWithCountDTO<DocumentoRepositorioDTO> getDocumentosByGeneral(
    @RequestBody(required = false) BusquedaRepositorioDTO busquedaRepositorioDTO,
    @RequestParam(value="id",required = false) String idDocumento,
    @RequestParam(value="usuarioCreador",required = false) String usuarioCreador,
    @RequestParam(value="tipoDocumento",required = false) String tipoDocumento,
    @RequestParam(value="tipoModeloCartera",required = false) String tipoModeloCartera,
    @RequestParam(value="nombreArchivo",required = false) String nombreDocumento,
    @RequestParam(value = "fechaActualizacion", required = false) String fechaActualizacion,
    @RequestParam(value = "fechaAlta", required = false) String fechaAlta,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size,
    @RequestParam(value = "page") Integer page) throws IOException {
    return carteraUseCase.findDocumentosByGeneral(busquedaRepositorioDTO,idDocumento,usuarioCreador,tipoDocumento,tipoModeloCartera,nombreDocumento,fechaActualizacion,fechaAlta,orderDirection,orderField,size,page);
  }


}
