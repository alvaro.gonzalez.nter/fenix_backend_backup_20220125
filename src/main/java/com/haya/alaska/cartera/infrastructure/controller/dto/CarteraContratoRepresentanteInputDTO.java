package com.haya.alaska.cartera.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CarteraContratoRepresentanteInputDTO implements Serializable {
    Integer criterioContratoRep;
    Integer orden;
    Boolean ordenAsc;
}
