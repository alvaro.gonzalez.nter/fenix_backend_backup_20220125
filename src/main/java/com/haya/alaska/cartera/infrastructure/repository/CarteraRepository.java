package com.haya.alaska.cartera.infrastructure.repository;

import com.haya.alaska.cartera.domain.Cartera;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarteraRepository extends JpaRepository<Cartera, Integer> {

  Optional<Cartera> findByIdCargaAndIdCargaSubcarteraIsNull(String idCarga);

  List<Cartera> findByIdCargaAndIdCargaSubcarteraIsNotNull(String idCarga);

  List<Cartera> findAllByPrincipalIsTrueAndFormalizacionIsTrue();
  List<Cartera> findAllByClientesClienteIdAndPrincipalIsTrueAndFormalizacionIsTrue(Integer clienteId);

  List<Cartera> findAllByPrincipalIsTrue();

  List<Cartera> findAllByExpedientesNotNull();

  List<Cartera> findAllByExpedientesPosesionNegociadaNotNull();

  List<Cartera> findAllByClientesClienteId(Integer clienteId);

  List<Cartera> findAllByClientesClienteIdAndPrincipalIsTrue(Integer clienteId);

  List<Cartera> findAllByClientesClienteIdAndNombre(Integer clienteId, String nombre);

  List<Cartera> findAllByClientesClienteIdAndNombreAndPrincipalIsTrue(Integer clienteId, String nombre);

  Optional<Cartera> findByIdHaya(String integerValue);

  Optional<Cartera> findByNombreAndPrincipalIsTrue(String nombre);
}
