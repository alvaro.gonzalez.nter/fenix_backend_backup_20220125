package com.haya.alaska.cartera.infrastructure.mapper;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.controller.dto.*;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;
import com.haya.alaska.cartera_contrato_representante.infrastructure.repository.CarteraContratoRepresentanteRepository;
import com.haya.alaska.cartera_fecha_excepcion.domain.CarteraFechaExcepcion;
import com.haya.alaska.cartera_fecha_excepcion.infrastructure.repository.CarteraFechaExcepcionRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.cliente_cartera.infrastructure.repository.ClienteCarteraRepository;
import com.haya.alaska.criterio_contrato_representante.infrastructure.repository.CriterioContratoRepresentanteRepository;
import com.haya.alaska.criterio_generacion_expediente.infrastructure.repository.CriterioGeneracionExpedienteRepository;
import com.haya.alaska.facturacion_cartera.infrastructure.repository.FacturacionCarteraRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.tratamiento_reo.infrastructure.repository.TratamientoReoRepository;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class CarteraMapper {
  @Autowired private CarteraRepository carteraRepository;
  @Autowired private ClienteRepository clienteRepository;
  @Autowired private FacturacionCarteraRepository facturacionCarteraRepository;
  @Autowired private UsuarioRepository usuarioRepository;
  @Autowired private TratamientoReoRepository tratamientoReoRepository;
  @Autowired private CriterioGeneracionExpedienteRepository criterioGeneracionExpedienteRepository;

  @Autowired
  private CriterioContratoRepresentanteRepository criterioContratoRepresentanteRepository;

  @Autowired private CarteraFechaExcepcionRepository carteraFechaExcepcionRepository;
  @Autowired private CarteraContratoRepresentanteRepository carteraContratoRepresentanteRepository;
  @Autowired private ClienteCarteraRepository clienteCarteraRepository;

  public Cartera inputToEntityAndSave(CarteraInputDTO input, Cartera cartera)
      throws NotFoundException, RequiredValueException {

    BeanUtils.copyProperties(input, cartera, "carteraFechaExcepciones");

    if (input.getCliente() != null) {
      Cliente cliente =
          clienteRepository
              .findById(input.getCliente())
              .orElseThrow(() -> new NotFoundException("Cliente", input.getCliente()));
      List<ClienteCartera> rela =
          clienteCarteraRepository.findAllByCarteraIdAndClienteId(cartera.getId(), cliente.getId());
      ClienteCartera cc;
      if (rela.size() != 1) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException(
              "Database error. There are several or no relationships between clients and portfolio.");
        else
          throw new NotFoundException(
              "Error en base de datos. Existen varias o ninguna relación entre clientes y cartera.");
      } else cc = rela.get(0);

      cc.setCartera(cartera);
      cc.setCliente(cliente);
      cartera.getClientes().add(cc);
    } else {
      throw new RequiredValueException("cliente");
    }

    if (input.getResponsableFormalizacion() != null)
      cartera.setResponsableFormalizacion(
          usuarioRepository
              .findById(input.getResponsableFormalizacion())
              .orElseThrow(
                  () -> new NotFoundException("Usuario", input.getResponsableFormalizacion())));
    else throw new RequiredValueException("responsableFormalizacion");

    Cartera carteraSaved;

    if (input.getFormalizacion() != null && input.getFormalizacion()) {

      carteraSaved = carteraRepository.save(cartera);
    } else {
      if (input.getIdHaya() == null) throw new RequiredValueException("idHaya");

      if (input.getFacturacion() != null)
        cartera.setFacturacion(
            facturacionCarteraRepository
                .findById(input.getFacturacion())
                .orElseThrow(() -> new NotFoundException("Facturación", input.getCliente())));
      else throw new RequiredValueException("facturación");

      if (input.getAdministrada() == null) throw new RequiredValueException("administrada");

      if (input.getResponsableCartera() != null)
        cartera.setResponsableCartera(
            usuarioRepository
                .findById(input.getResponsableCartera())
                .orElseThrow(
                    () -> new NotFoundException("Usuario", input.getResponsableCartera())));
      else throw new RequiredValueException("responsableCartera");
      if (input.getResponsableSkipTracing() != null)
        cartera.setResponsableSkipTracing(
            usuarioRepository
                .findById(input.getResponsableSkipTracing())
                .orElseThrow(
                    () -> new NotFoundException("Usuario", input.getResponsableSkipTracing())));
      else throw new RequiredValueException("responsableSkipTracing");

      if (input.getDatosCoreBanking() == null) throw new RequiredValueException("datosCoreBanking");
      if (input.getDatosJudicial() == null) throw new RequiredValueException("datosCoreBanking");
      if (input.getDatosContabilidad() == null)
        throw new RequiredValueException("datosCoreBanking");

      if (input.getTratamientoReo() != null)
        cartera.setTratamientoReo(
            tratamientoReoRepository
                .findById(input.getTratamientoReo())
                .orElseThrow(
                    () -> new NotFoundException("Tratamiento REO", input.getTratamientoReo())));
      else throw new RequiredValueException("tratamientoReo");

      if (input.getCriterioGeneracionExpediente() != null)
        cartera.setCriterioGeneracionExpediente(
            criterioGeneracionExpedienteRepository
                .findById(input.getCriterioGeneracionExpediente())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "Criterio de generación de expediente",
                            input.getCriterioGeneracionExpediente())));
      else throw new RequiredValueException("criterioGeneracionExpediente");

      carteraSaved = carteraRepository.save(cartera);

      Set<CarteraFechaExcepcion> cfeL = new HashSet<>();
      if (input.getCarteraFechaExcepciones() != null) {
        for (Date date : input.getCarteraFechaExcepciones()) {
          CarteraFechaExcepcion cfe = new CarteraFechaExcepcion();
          cfe.setFecha(date);
          cfe.setCartera(carteraSaved);
          cfeL.add(carteraFechaExcepcionRepository.saveAndFlush(cfe));
        }
      }
      carteraSaved.setCarteraFechaExcepciones(cfeL);
    }

    List<CarteraContratoRepresentanteInputDTO> ccrA = input.getCarteraContratoRepresentantes();
    if (ccrA == null || ccrA.size() < 1) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new RequiredValueException(
            "The field 'carteraContratoRepresentantes' is necessary and must have at least 1 value.");
      else
        throw new RequiredValueException(
            "El campo 'carteraContratoRepresentantes' es necesario y tiene que tener al menos 1 valor.");
    }

    Set<CarteraContratoRepresentante> ccrL = new HashSet<>();

    for (CarteraContratoRepresentanteInputDTO ccrI : input.getCarteraContratoRepresentantes()) {
      CarteraContratoRepresentante ccr = new CarteraContratoRepresentante();
      BeanUtils.copyProperties(ccrI, ccr);
      if (ccrI.getCriterioContratoRep() != null)
        ccr.setCriterioContratoCartera(
            criterioContratoRepresentanteRepository
                .findById(ccrI.getCriterioContratoRep())
                .orElseThrow(
                    () ->
                        new NotFoundException(
                            "Criterio de generación de contrato", ccrI.getCriterioContratoRep())));
      else throw new RequiredValueException("criterioGeneracionExpediente");
      if (ccrI.getOrden() == null) throw new RequiredValueException("orden");
      if (ccrI.getOrdenAsc() == null) throw new RequiredValueException("ordenAsc");
      ccr.setCartera(carteraSaved);
      ccrL.add(carteraContratoRepresentanteRepository.saveAndFlush(ccr));
    }
    carteraSaved.setCriterioContratoRepresentante(ccrL);

    return carteraRepository.save(carteraSaved);
  }

  public CarteraOutputDTO entityToOutput(Cartera cartera) {
    CarteraOutputDTO output = new CarteraOutputDTO();

    BeanUtils.copyProperties(cartera, output);
    output.setCliente(
        cartera.getCliente() != null ? new ClienteOutputDTO(cartera.getCliente()) : null);
    output.setFacturacion(
        cartera.getFacturacion() != null ? new CatalogoMinInfoDTO(cartera.getFacturacion()) : null);
    output.setResponsableCartera(
        cartera.getResponsableCartera() != null
            ? new UsuarioAgendaDto(cartera.getResponsableCartera())
            : null);
    output.setResponsableSkipTracing(
        cartera.getResponsableSkipTracing() != null
            ? new UsuarioAgendaDto(cartera.getResponsableSkipTracing())
            : null);
    output.setResponsableFormalizacion(
        cartera.getResponsableFormalizacion() != null
            ? new UsuarioAgendaDto(cartera.getResponsableFormalizacion())
            : null);
    output.setTratamientoReo(
        cartera.getTratamientoReo() != null
            ? new CatalogoMinInfoDTO(cartera.getTratamientoReo())
            : null);
    output.setCriterioGeneracionExpediente(
        cartera.getCriterioGeneracionExpediente() != null
            ? new CatalogoMinInfoDTO(cartera.getCriterioGeneracionExpediente())
            : null);

    output.setCriterioGeneracionContratoRep(
        cartera.getCriterioContratoRepresentante().stream()
            .map(CarteraContratoRepresentanteOutputDTO::new)
            .collect(Collectors.toList()));
    output.setCarteraFechaExcepciones(
        cartera.getCarteraFechaExcepciones().stream()
            .map(CarteraFechaExcepcion::getFecha)
            .collect(Collectors.toList()));

    return output;
  }
}
