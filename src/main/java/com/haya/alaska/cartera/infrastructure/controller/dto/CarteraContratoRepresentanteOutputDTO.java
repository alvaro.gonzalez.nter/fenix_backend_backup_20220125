package com.haya.alaska.cartera.infrastructure.controller.dto;

import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.*;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CarteraContratoRepresentanteOutputDTO {
    Integer cartera;
    CatalogoMinInfoDTO criterioContratoCartera;
    Integer orden;
    Boolean ordenAsc;

    public CarteraContratoRepresentanteOutputDTO(CarteraContratoRepresentante ccr){
        BeanUtils.copyProperties(ccr, this);
        this.cartera = ccr.getCartera().getId();
        this.criterioContratoCartera = new CatalogoMinInfoDTO(ccr.getCriterioContratoCartera());
    }
}
