package com.haya.alaska.cartera.infrastructure.controller.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CarteraInputDTO implements Serializable {
    //Atributos obligatorios
    Integer Cliente;
    String idHaya; //Cartera maestro
    //Tipologoa
    Integer facturacion;
    Boolean administrada;
    //Responsables
    Integer responsableCartera;
    Integer responsableSkipTracing;
    Integer responsableFormalizacion;
    //Aplicaciones origen
    Boolean datosCoreBanking;
    Boolean datosJudicial;
    Boolean datosContabilidad;
    Integer tratamientoReo;
    //Criterios de generación del Expediente
    Integer criterioGeneracionExpediente;
    //Criterios de generación del contrato Representante.
    List<CarteraContratoRepresentanteInputDTO> carteraContratoRepresentantes;
    //Atributos opcionales?
    String idOrigen;

    Integer tolerancia;
    String email;
    Integer diasRestrasoCancelacion;
    List<Date> carteraFechaExcepciones;

    Boolean formalizacion;
}
