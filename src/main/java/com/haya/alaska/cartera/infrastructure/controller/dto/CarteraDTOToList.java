package com.haya.alaska.cartera.infrastructure.controller.dto;

import com.haya.alaska.cartera.domain.Cartera;
import lombok.*;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CarteraDTOToList implements Serializable {

  Integer id;
  Boolean activo;
  Boolean administrada;
  String idHaya;
  String idCarga;
  String nombre;
  Integer cliente;
  Integer criterio;
  Integer facturacion;
  Integer responsable;
  Boolean formalizacion;

  public CarteraDTOToList(Cartera cartera){
    this.id = cartera.getId();
    this.activo = cartera.getActivo();
    this.administrada = cartera.getAdministrada() != null ? cartera.getAdministrada() : null;
    this.idHaya = cartera.getIdHaya() != null ? cartera.getIdHaya() : null;
    this.idCarga = cartera.getIdCarga();
    this.nombre = cartera.getNombre() != null ? cartera.getNombre() : null;
    this.cliente = cartera.getCliente() != null ? cartera.getCliente().getId() : null;
    this.facturacion = cartera.getFacturacion() != null ? cartera.getFacturacion().getId() : null;
    this.responsable = cartera.getResponsableCartera() != null ? cartera.getResponsableCartera().getId() : null;
    this.formalizacion = cartera.getFormalizacion();
  }

  public static List<CarteraDTOToList> newList(List<Cartera> carteras) {
    return carteras.stream().map(CarteraDTOToList::new).collect(Collectors.toList());
  }
}
