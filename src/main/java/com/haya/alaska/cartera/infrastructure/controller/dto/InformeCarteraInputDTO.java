package com.haya.alaska.cartera.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InformeCarteraInputDTO {

  private Integer estado;
  private Date fechaCreacionInicial;
  private Date fechaCreacionFinal;
  private Date fechaCierreInicial;
  private Date fechaCierreFinal;

}
