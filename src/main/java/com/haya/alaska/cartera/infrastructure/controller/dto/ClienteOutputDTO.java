package com.haya.alaska.cartera.infrastructure.controller.dto;

import com.haya.alaska.cliente.domain.Cliente;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ClienteOutputDTO {
  Integer id;
  String nombre;
  String grupoCliente;

  public ClienteOutputDTO(Cliente cliente) {
    this.id = cliente.getId();
    this.nombre = cliente.getNombre();
    this.grupoCliente = cliente.getGrupo() != null ? cliente.getGrupo().getNombre() : null;
  }

  public static List<ClienteOutputDTO> newList(List<Cliente> clientes) {
    return clientes.stream().map(ClienteOutputDTO::new).collect(Collectors.toList());
  }
}
