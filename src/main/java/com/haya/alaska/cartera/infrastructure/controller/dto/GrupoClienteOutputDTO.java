package com.haya.alaska.cartera.infrastructure.controller.dto;

import com.haya.alaska.grupo_cliente.domain.GrupoCliente;
import lombok.*;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GrupoClienteOutputDTO {
  Integer id;
  Boolean activo;
  String nombre;

  public GrupoClienteOutputDTO(GrupoCliente grupoCliente){
    BeanUtils.copyProperties(grupoCliente, this);
  }
}
