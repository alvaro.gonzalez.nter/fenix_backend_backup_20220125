package com.haya.alaska.cartera.infrastructure.controller.dto;

import com.haya.alaska.cartera_fecha_excepcion.domain.CarteraFechaExcepcion;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioDTO;
import lombok.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CarteraOutputDTO {
    Integer id;
    ClienteOutputDTO Cliente;
    String idHaya;
    CatalogoMinInfoDTO facturacion;
    Boolean administrada;
    UsuarioAgendaDto responsableCartera;
    UsuarioAgendaDto responsableSkipTracing;
    UsuarioAgendaDto responsableFormalizacion;
    Boolean datosCoreBanking;
    Boolean datosJudicial;
    Boolean datosContabilidad;
    CatalogoMinInfoDTO tratamientoReo;
    CatalogoMinInfoDTO criterioGeneracionExpediente;
    List<CarteraContratoRepresentanteOutputDTO> criterioGeneracionContratoRep;
    String idOrigen;
    String nombre;
    Integer tolerancia;
    String email;
    Integer diasRestrasoCancelacion;
    String idCarga;
    List<Date> carteraFechaExcepciones;
    Boolean formalizacion;
}
