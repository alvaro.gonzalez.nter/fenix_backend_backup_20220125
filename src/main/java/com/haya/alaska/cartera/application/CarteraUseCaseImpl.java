package com.haya.alaska.cartera.application;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.controller.dto.*;
import com.haya.alaska.cartera.infrastructure.mapper.CarteraMapper;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.cliente.application.ClienteUseCase;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.mapper.ContratoMapper;
import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.grupo_cliente.domain.GrupoCliente;
import com.haya.alaska.grupo_cliente.infrastructure.repository.GrupoClienteRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.integraciones.gd.application.GestorDocumentalUseCase;
import com.haya.alaska.integraciones.gd.domain.GestorDocumental;
import com.haya.alaska.integraciones.gd.infraestructure.repository.GestorDocumentalRepository;
import com.haya.alaska.integraciones.gd.model.CodigoEntidad;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.dto.MetadatoContenedorInputDTO;
import com.haya.alaska.shared.dto.MetadatoDocumentoInputDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CarteraUseCaseImpl implements CarteraUseCase{

  @Autowired
  private CarteraRepository carteraRepository;
  @Autowired
  private ContratoMapper contratoMapper;
  @Autowired
  private CarteraMapper carteraMapper;
  @Autowired
  private GrupoClienteRepository grupoClienteRepository;
  @Autowired
  private ServicioGestorDocumental servicioGestorDocumental;
  @Autowired
  private GestorDocumentalRepository gestorDocumentalRepository;
  @Autowired
  private GestorDocumentalUseCase gestorDocumentalUseCase;

  @Autowired
  private UsuarioRepository usuarioRepository;
  @Autowired
  private ClienteUseCase clienteUseCase;


  @Override
  public List<ContratoDTOToList> getContratosByCartera(Integer idCartera) {
    Cartera cartera = carteraRepository.findById(idCartera).orElse(null);
    Set<Contrato> contratos = new HashSet<>();
    for (Expediente e : cartera.getExpedientes()){
      contratos.addAll(new ArrayList<>(e.getContratos()));
    }

    return contratoMapper.listContratoToListContratoDTOToList(contratos);
  }

  public CarteraOutputDTO generateCartera(CarteraInputDTO input, Integer id) throws NotFoundException, RequiredValueException {
    Cartera oldCartera = carteraRepository.findById(id).orElseThrow(() ->
      new NotFoundException("Cartera", id));
    Cartera cartera = carteraMapper.inputToEntityAndSave(input, oldCartera);
    //TODO:  generación de expedientes y contratos
    return carteraMapper.entityToOutput(cartera);
  }

  @Override
  public List<CarteraDTOToList> getCarterasByCliente(Integer clienteId, Usuario logged, Boolean asignados) throws NotFoundException {
    if(asignados) {
      List<CarteraDTOToList> carteras = new ArrayList<>();
      for(var asignacionUsuarioCartera: logged.getAsignacionesUsuarioCartera()) {
        var cartera = new CarteraDTOToList(asignacionUsuarioCartera.getCartera());
        if(cartera.getCliente().equals(clienteId)) {
          boolean meter = true;
          for(var carteraDentro: carteras) {
            if(carteraDentro.getId().equals(cartera.getId())) meter = false;
          }
          if(meter) carteras.add(cartera);
        }
      }
      return carteras;
    }
    List<CarteraDTOToList> carterasByUser = getCarteras(logged);
    List<Integer> ids = carterasByUser.stream().map(CarteraDTOToList::getId).collect(Collectors.toList());
    List<CarteraDTOToList> carterasByCliente = CarteraDTOToList.newList(carteraRepository.findAllByClientesClienteIdAndPrincipalIsTrue(clienteId));
    return carterasByCliente.stream().filter(c -> ids.contains(c.getId())).collect(Collectors.toList());
  }

  @Override
  public List<CarteraDTOToList> getCarterasByClientes(List<Integer> clientes, Usuario logged) throws NotFoundException {
    List<CarteraDTOToList> carteras = new ArrayList<>();
    for(var cliente: clientes) {
      var carterasUsuario = carteraRepository.findAllByClientesClienteId(cliente);
      for(var cartera: carterasUsuario) {
        carteras.add(new CarteraDTOToList(cartera));
      }

    }
    return carteras;
  }

  @Override
  public List<CarteraDTOToList> getAll(){
    //Seleccionamos todas las carteras marcadas como principal.
    //Las carteras con más de un registro en bbdd indican diferentes tipos de activos que manejan y distintos clientes
    return CarteraDTOToList.newList(carteraRepository.findAllByPrincipalIsTrue());
  }

  @Override
  public CarteraOutputDTO getCarterasByClienteAndNombre(Integer clienteId, String nombre) throws NotFoundException {
    List<Cartera> carteras = carteraRepository.findAllByClientesClienteIdAndNombreAndPrincipalIsTrue(clienteId, nombre);
    if (carteras.size() != 1){
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("Error with the number of wallets");
      else throw new NotFoundException("Error con el número de carteras");
    }
    return carteraMapper.entityToOutput(carteras.get(0));
  }

  @Override
  public List<GrupoClienteOutputDTO> getAllGrupoCliente(){
    List<GrupoCliente> grupos = grupoClienteRepository.findAll();
    return grupos.stream().map(GrupoClienteOutputDTO::new).collect(Collectors.toList());
  }

  @Override
  public List<GrupoClienteOutputDTO> getGrupoClienteByUsuario(Usuario user){
    List<ClienteOutputDTO> clientes = clienteUseCase.getClientesByUser(user);
    List<Integer> idClientes = clientes.stream().map(clienteOutputDTO -> clienteOutputDTO.getId()).collect(Collectors.toList());
    List<GrupoCliente> grupos = grupoClienteRepository.findAllByClientesIdIn(idClientes);
    return grupos.stream().distinct().map(GrupoClienteOutputDTO::new).collect(Collectors.toList());
  }

  @Override
  public List<GrupoClienteOutputDTO> getGrupoClienteAsignadosByUsuario(Usuario user) throws NotFoundException {
    Usuario logged = usuarioRepository.findById(user.getId()).orElse(new Usuario());
    List<ClienteOutputDTO> clientes = clienteUseCase.getAll(logged, true);
    List<Integer> idClientes = clientes.stream().map(clienteOutputDTO -> clienteOutputDTO.getId()).collect(Collectors.toList());
    List<GrupoCliente> grupos = grupoClienteRepository.findAllByClientesIdIn(idClientes);
    return grupos.stream().distinct().map(GrupoClienteOutputDTO::new).collect(Collectors.toList());
  }

  @Override
  public List<ClienteOutputDTO> getClientes(Integer idGrupo) throws NotFoundException {
    GrupoCliente grupo = grupoClienteRepository.findById(idGrupo).orElse(null);
    if (grupo == null) throw new NotFoundException("GrupoCliente", idGrupo);
    return grupo.getClientes().stream().map(ClienteOutputDTO::new).collect(Collectors.toList());
  }

  @Override
  public List<ClienteOutputDTO> getClientesAsignados(Integer idGrupo, Usuario user) throws NotFoundException {
    GrupoCliente grupo = grupoClienteRepository.findById(idGrupo).orElse(null);
    if (grupo == null) throw new NotFoundException("GrupoCliente", idGrupo);
    List<Integer> clientesAsignados = clienteUseCase.getAll(user, true).stream().map(ClienteOutputDTO::getId).collect(Collectors.toList());

    return grupo.getClientes().stream().filter(cl -> clientesAsignados.contains(cl.getId())).map(ClienteOutputDTO::new).collect(Collectors.toList());
  }

  @Override
  public List<ClienteOutputDTO> getClientesByUsuario(Integer idGrupo, Usuario usuario) throws NotFoundException {
    GrupoCliente grupo = grupoClienteRepository.findById(idGrupo).orElse(null);
    if (grupo == null) throw new NotFoundException("GrupoCliente", idGrupo);
    return clienteUseCase.getClientesByGrupoAndUserAlways(grupo, usuario);
  }

  @Override
  public List<CarteraDTOToList> getCarteras(Usuario logged) throws NotFoundException {
    Usuario user =
      usuarioRepository
        .findById(logged.getId())
        .orElseThrow(
          () ->
            new NotFoundException("Usuario", logged.getId()));

    return getCarteraByUser(user);
  }

  @Override
  public List<CarteraDTOToList> getCarterasByUserAlways(Usuario user){
    List<Cartera> carterasList;
    List<AsignacionExpediente> asigRA = new ArrayList<>(user.getAsignacionExpedientes());
    List<AsignacionExpedientePosesionNegociada> asigPN =
      new ArrayList<>(user.getAsignacionExpedientesPN());
    List<Cartera> carteras =
      asigRA.stream()
        .map(a -> a.getExpediente().getCartera())
        .filter(Objects::nonNull)
        .distinct()
        .collect(Collectors.toList());
    carteras.addAll(
      asigPN.stream()
        .map(a -> a.getExpediente().getCartera())
        .filter(Objects::nonNull)
        .distinct()
        .collect(Collectors.toList()));
    carterasList = carteras.stream().distinct().collect(Collectors.toList());

    List<CarteraDTOToList> result =
      carterasList.stream().map(CarteraDTOToList::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public List<CarteraDTOToList> getCarterasByUserAlways(Integer idCliente, Usuario user){
    List<Cartera> carterasList;
    List<AsignacionExpediente> asigRA = new ArrayList<>(user.getAsignacionExpedientes());
    List<AsignacionExpedientePosesionNegociada> asigPN =
      new ArrayList<>(user.getAsignacionExpedientesPN());
    List<Cartera> carteras =
      asigRA.stream()
        .map(a -> a.getExpediente().getCartera())
        .filter(Objects::nonNull)
        .filter(cartera -> cartera.getCliente().getId().equals(idCliente))
        .distinct()
        .collect(Collectors.toList());
    carteras.addAll(
      asigPN.stream()
        .map(a -> a.getExpediente().getCartera())
        .filter(Objects::nonNull)
        .distinct()
        .collect(Collectors.toList()));
    carterasList = carteras.stream().distinct().collect(Collectors.toList());

    List<CarteraDTOToList> result =
      carterasList.stream().map(CarteraDTOToList::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public List<CarteraDTOToList> getCarteraByUser(Usuario user){
    List<Cartera> carterasList;
    if (user.esAdministrador()) {
      carterasList = carteraRepository.findAll();
    } else {
      List<AsignacionExpediente> asigRA = new ArrayList<>(user.getAsignacionExpedientes());
      List<AsignacionExpedientePosesionNegociada> asigPN =
        new ArrayList<>(user.getAsignacionExpedientesPN());
      List<Cartera> carteras =
        asigRA.stream()
          .map(a -> a.getExpediente().getCartera())
          .filter(Objects::nonNull)
          .distinct()
          .collect(Collectors.toList());
      carteras.addAll(
        asigPN.stream()
          .map(a -> a.getExpediente().getCartera())
          .filter(Objects::nonNull)
          .distinct()
          .collect(Collectors.toList()));
      carterasList = carteras.stream().distinct().collect(Collectors.toList());
    }

    List<CarteraDTOToList> result =
      carterasList.stream().map(CarteraDTOToList::new).collect(Collectors.toList());
    return result;
  }

  //Realizar try catch se realiza la parte de crear el contenedor en caso que falle que haga el insertar el documento
  @Override
  public void createDocumento(Integer idCartera,String idMatricula, MultipartFile file, Usuario usuario,String tipoModeloCartera) throws Exception {
    Cartera cartera=carteraRepository.findById(idCartera).orElse(null);
    Cliente cliente=cartera.getCliente();
    String cartera1=cartera.getIdHaya();
    String nombreCartera=cartera.getNombre().replaceAll(" ","");
    if (cliente == null)
      throw new NotFoundException("Cliente", "Cartera ", cartera.getNombre());
  String nombreCliente = cliente.getNombre();
  String nombreFichero = file.getOriginalFilename();
    MetadatoDocumentoInputDTO metadatoDocumentoInputDTO = new MetadatoDocumentoInputDTO(nombreFichero, idMatricula);
 try{
    MetadatoContenedorInputDTO metadatoContenedorInputDTO = new MetadatoContenedorInputDTO(nombreCartera,nombreCliente);
    this.servicioGestorDocumental.procesarContenedorCartera(CodigoEntidad.CARTERA, metadatoContenedorInputDTO);
    long id = servicioGestorDocumental.procesarDocumento(CodigoEntidad.CARTERA,nombreCartera, file, metadatoDocumentoInputDTO).getIdDocumento();
    Integer idgestorDocumental = (int) id;
    Date fechaAlta=new Date();
    //TipoDocumento Revisar a la hora de guardar
    guardarDocumentosCartera(idgestorDocumental,idMatricula,usuario,fechaAlta,nombreFichero,null,tipoModeloCartera);
    }catch(Exception e){
   long id = servicioGestorDocumental.procesarDocumento(CodigoEntidad.CARTERA,nombreCartera, file, metadatoDocumentoInputDTO).getIdDocumento();
   Integer idgestorDocumental = (int) id;
   Date fechaAlta=new Date();
   guardarDocumentosCartera(idgestorDocumental,idMatricula,usuario,fechaAlta,nombreFichero,null,tipoModeloCartera);
 }
}

  @Override
  public ListWithCountDTO<DocumentoRepositorioDTO> findDocumentosByCartera(
      BusquedaRepositorioDTO busquedaRepositorioDTO,
      Integer idCartera,
      String idDocumento,
      String usuarioCreador,
      String tipoDocumento,
      String tipoModeloCartera,
      String nombreDocumento,
      String fechaActualizacion,
      String fechaAlta,
      String orderDirection,
      String orderField,
      Integer size,
      Integer page)
      throws Exception {
    Cartera cartera = carteraRepository.findById(idCartera).orElse(null);
    String nombreCliente = cartera.getCliente().getNombre();
    String nombreCartera = cartera.getNombre().replaceAll(" ", "");
    String cartera1 = cartera.getIdHaya();

    try {
      List<DocumentoRepositorioDTO> listadoIdGestor =
          this.servicioGestorDocumental
              .listarDocumentos(CodigoEntidad.CARTERA, nombreCartera)
              .stream()
              .map(DocumentoRepositorioDTO::new)
              .collect(Collectors.toList());

      List<DocumentoRepositorioDTO> list =
          gestorDocumentalUseCase.listarDocumentosRepositorioCarteras(listadoIdGestor);
      List<DocumentoRepositorioDTO> listbusqueda =
          gestorDocumentalUseCase.busquedaDocumentoRepositorio(busquedaRepositorioDTO, list);
      List<DocumentoRepositorioDTO> listfiltrada =
          gestorDocumentalUseCase.filterDocumentoRepositorioDTO(
              listbusqueda,
              idDocumento,
              usuarioCreador,
              tipoDocumento,
              tipoModeloCartera,
              nombreDocumento,
              fechaActualizacion,
              fechaAlta,
              orderDirection,
              orderField);
      List<DocumentoRepositorioDTO> listaFiltradaPaginada =
          gestorDocumentalUseCase.listarDocumentosRepositorioCarteras(listfiltrada).stream()
              .skip(size * page)
              .limit(size)
              .collect(Collectors.toList());
      return new ListWithCountDTO<>(listaFiltradaPaginada, listfiltrada.size());
    } catch (Exception ex) {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
          new MetadatoContenedorInputDTO(nombreCartera, nombreCliente);
      this.servicioGestorDocumental.procesarContenedorCartera(
          CodigoEntidad.CARTERA, metadatoContenedorInputDTO);
      return new ListWithCountDTO<>(new ArrayList<>(),10);
    }

  }

  public void guardarDocumentosCartera(Integer idgestorDocumental, String idMatricula, Usuario usuario, Date fechaAlta, String nombreFichero,String tipoDocumento,String tipoModeloCartera) {

    if (idMatricula.equals("HA-02-LICM-14")) {
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental, usuario, null, fechaAlta, null, nombreFichero, null,tipoModeloCartera);
      gestorDocumentalRepository.save(gestorDocumental);
    }
    if (idMatricula.contains("OTRO")
        || idMatricula.contains("NORM")
        || idMatricula.contains("FACT")
        || idMatricula.contains("HA-02-LICM-22")) {
      GestorDocumental gestorDocumental =
        new GestorDocumental(
          idgestorDocumental, usuario, null, fechaAlta, null, nombreFichero, null,tipoModeloCartera);
      gestorDocumentalRepository.save(gestorDocumental);
    }
    if (!idMatricula.equals("HA-02-LICM-14")) {
     // String tipoDocumento = "Modelos";
      GestorDocumental gestorDocumental =
        new GestorDocumental(
          idgestorDocumental, usuario, null, fechaAlta, null, nombreFichero, null,tipoModeloCartera);
      gestorDocumentalRepository.save(gestorDocumental);
    }
   }

  @Override
  public void createDocumentoGeneral(String idMatricula, MultipartFile file, Usuario usuario)
      throws IOException {
    Integer idContenedor = 9999;
    String nombreFichero = file.getOriginalFilename();
    MetadatoDocumentoInputDTO metadatoDocumentoInputDTO =
        new MetadatoDocumentoInputDTO(nombreFichero, idMatricula);
    try {
      MetadatoContenedorInputDTO metadatoContenedorInputDTO =
          new MetadatoContenedorInputDTO(idContenedor.toString(), "");
      this.servicioGestorDocumental.procesarContenedorCartera(
          CodigoEntidad.CARTERA, metadatoContenedorInputDTO);
      long id =
          servicioGestorDocumental
              .procesarDocumento(
                  CodigoEntidad.CARTERA, idContenedor.toString(), file, metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      Date fechaAlta = new Date();
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental, usuario, null, fechaAlta, null, nombreFichero, null, null);
      gestorDocumentalRepository.save(gestorDocumental);
      // TipoDocumento Revisar a la hora de guardar
      // guardarDocumentosCartera(idgestorDocumental,idMatricula,usuario,fechaAlta,nombreFichero,null,null);
    } catch (Exception e) {
      long id =
          servicioGestorDocumental
              .procesarDocumento(
                  CodigoEntidad.CARTERA, idContenedor.toString(), file, metadatoDocumentoInputDTO)
              .getIdDocumento();
      Integer idgestorDocumental = (int) id;
      Date fechaAlta = new Date();
      GestorDocumental gestorDocumental =
          new GestorDocumental(
              idgestorDocumental, usuario, null, fechaAlta, null, nombreFichero, null, null);
      gestorDocumentalRepository.save(gestorDocumental);
    }
  }


//Se estableció que el contenedor sería uno con el valor de 9999 en un correo
  @Override
  public ListWithCountDTO<DocumentoRepositorioDTO> findDocumentosByGeneral(
    BusquedaRepositorioDTO busquedaRepositorioDTO,
    String idDocumento,
    String usuarioCreador,
    String tipoDocumento,
    String tipoModeloCartera,
    String nombreDocumento,
    String fechaActualizacion,
    String fechaAlta,
    String orderDirection,
    String orderField,
    Integer size,
    Integer page)
    throws IOException {
    Integer idContenedor=9999;


    List<DocumentoRepositorioDTO> listadoIdGestor =
      this.servicioGestorDocumental
        .listarDocumentos(CodigoEntidad.CARTERA, idContenedor.toString())
        .stream()
        .map(DocumentoRepositorioDTO::new)
        .collect(Collectors.toList());

    List<DocumentoRepositorioDTO> list =
      gestorDocumentalUseCase.listarDocumentosRepositorioCarteras(listadoIdGestor);
    List<DocumentoRepositorioDTO> listbusqueda =
      gestorDocumentalUseCase.busquedaDocumentoRepositorio(busquedaRepositorioDTO, list);
    List<DocumentoRepositorioDTO> listfiltrada =
      gestorDocumentalUseCase.filterDocumentoRepositorioDTO(
        listbusqueda,
        idDocumento,
        usuarioCreador,
        tipoDocumento,
        tipoModeloCartera,
        nombreDocumento,
        fechaActualizacion,
        fechaAlta,
        orderDirection,
        orderField);
    List<DocumentoRepositorioDTO> listaFiltradaPaginada =
      gestorDocumentalUseCase.listarDocumentosRepositorioCarteras(listfiltrada).stream()
        .skip(size * page)
        .limit(size)
        .collect(Collectors.toList());

    return new ListWithCountDTO<>(listaFiltradaPaginada, listfiltrada.size());
  }

  @Override
  public List<CarteraDTOToList> getCarterasFormalizacion(Integer idCliente){
    List<Cartera> carteras;
    if (idCliente == null) carteras = carteraRepository.findAllByPrincipalIsTrueAndFormalizacionIsTrue();
    else carteras = carteraRepository.findAllByClientesClienteIdAndPrincipalIsTrueAndFormalizacionIsTrue(idCliente);


    List<CarteraDTOToList> result = carteras.stream().map(CarteraDTOToList::new).collect(Collectors.toList());
    return result;
  }
}
