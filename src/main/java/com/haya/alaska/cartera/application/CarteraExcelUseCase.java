package com.haya.alaska.cartera.application;

import com.haya.alaska.cartera.infrastructure.controller.dto.InformeCarteraInputDTO;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;


public interface CarteraExcelUseCase {


  LinkedHashMap<String, List<Collection<?>>> findExcelCarteraById(Integer carteraId, InformeCarteraInputDTO informeCarteraInputDTO, Usuario usuario, Boolean posesionNegociada) throws Exception;


}

