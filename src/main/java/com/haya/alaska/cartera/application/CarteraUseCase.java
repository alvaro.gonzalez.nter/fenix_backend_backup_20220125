package com.haya.alaska.cartera.application;

import com.haya.alaska.cartera.infrastructure.controller.dto.*;
import com.haya.alaska.documento.infrastructure.controller.dto.BusquedaRepositorioDTO;
import com.haya.alaska.documento.infrastructure.controller.dto.DocumentoRepositorioDTO;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface CarteraUseCase {
  List<ContratoDTOToList> getContratosByCartera(Integer idCartera);
  CarteraOutputDTO generateCartera(CarteraInputDTO input, Integer id) throws NotFoundException, RequiredValueException;
  List<CarteraDTOToList> getCarterasByCliente(Integer clienteId, Usuario logged, Boolean asignados) throws NotFoundException;
  List<CarteraDTOToList> getCarterasByClientes(List<Integer> clientes, Usuario logged) throws NotFoundException;
  List<CarteraDTOToList> getAll();
  CarteraOutputDTO getCarterasByClienteAndNombre(Integer clienteId, String nombre) throws NotFoundException;
  List<GrupoClienteOutputDTO> getAllGrupoCliente();
  List<GrupoClienteOutputDTO> getGrupoClienteByUsuario(Usuario user);
  List<GrupoClienteOutputDTO> getGrupoClienteAsignadosByUsuario(Usuario user) throws NotFoundException;
  List<ClienteOutputDTO> getClientes(Integer idGrupo) throws NotFoundException;
  List<ClienteOutputDTO> getClientesAsignados(Integer idGrupo, Usuario user) throws NotFoundException;
  List<ClienteOutputDTO> getClientesByUsuario(Integer idGrupo, Usuario usuario) throws NotFoundException;
  void createDocumento(Integer idCartera,String idMatricula, MultipartFile file, Usuario usuario,String tipoDocumento) throws Exception;

  List<CarteraDTOToList> getCarteraByUser(Usuario user);
  List<CarteraDTOToList> getCarterasByUserAlways(Usuario user);
  List<CarteraDTOToList> getCarterasByUserAlways(Integer idCliente, Usuario user);
  List<CarteraDTOToList> getCarteras(Usuario logged) throws NotFoundException;

  ListWithCountDTO<DocumentoRepositorioDTO> findDocumentosByCartera(
      BusquedaRepositorioDTO busquedaRepositorioDTO,
      Integer idCartera,
      String idDocumento,
      String usuarioCreador,
      String tipoDocumento,
      String tipoModeloCartera,
      String nombreDocumento,
      String fechaActualizacion,
      String fechaAlta,
      String orderDirection,
      String orderField,
      Integer size,
      Integer page)
    throws Exception;

  void createDocumentoGeneral(String idMatricula, MultipartFile file, Usuario usuario) throws IOException;

  ListWithCountDTO<DocumentoRepositorioDTO> findDocumentosByGeneral(
    BusquedaRepositorioDTO busquedaRepositorioDTO,
    String idDocumento,
    String usuarioCreador,
    String tipoDocumento,
    String tipoModeloCartera,
    String nombreDocumento,
    String fechaActualizacion,
    String fechaAlta,
    String orderDirection,
    String orderField,
    Integer size,
    Integer page)
    throws IOException;

  List<CarteraDTOToList> getCarterasFormalizacion(Integer idCliente);
}
