package com.haya.alaska.cartera.application;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.domain.BienCarteraExcel;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.controller.dto.InformeCarteraInputDTO;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.domain.ContratoCarteraExcel;
import com.haya.alaska.contrato_bien.domain.ContratoBien;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.ExpedienteCarteraExcel;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociadaExcel;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.interviniente.domain.IntervinienteCarteraExcel;
import com.haya.alaska.ocupante.domain.OcupanteInformeExcel;
import com.haya.alaska.ocupante_bien.domain.OcupanteBien;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.domain.ProcedimientoCarteraExcel;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CarteraExcelUseCaseImpl implements CarteraExcelUseCase {

  @Autowired private CarteraRepository carteraRepository;
  @Autowired private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired private ExpedienteRepository expedienteRepository;
  @Autowired private EventoUtil eventoUtil;
  @Autowired ExpedienteUtil expedienteUtil;

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelCarteraById(
      Integer carteraId,
      InformeCarteraInputDTO informeCarteraInputDTO,
      Usuario usuario,
      Boolean posesionNegociada)
      throws Exception {

    Perfil perfilUsu = usuario.getPerfil();

    if (perfilUsu.getNombre().equals("Responsable de cartera")
        || perfilUsu.getNombre().equals("Supervisor")
        || usuario.esAdministrador()) {
      var datos = this.createCarteraDataForExcel(posesionNegociada);
      if (carteraId != null) {
        Cartera cartera =
            carteraRepository
                .findById(carteraId)
                .orElseThrow(() -> new NotFoundException("Cartera", carteraId));
        if (posesionNegociada == false)
          this.addCarteraDataForExcel(cartera, datos, informeCarteraInputDTO, usuario);
        else this.addCarteraDataForExcelPN(cartera, datos, informeCarteraInputDTO, usuario);
      } else {
        for (Cartera cartera : carteraRepository.findAll()) {
          if (posesionNegociada == false)
            this.addCarteraDataForExcel(cartera, datos, informeCarteraInputDTO, usuario);
          else this.addCarteraDataForExcelPN(cartera, datos, informeCarteraInputDTO, usuario);
        }
      }
      return datos;
    } else {
      throw new IllegalArgumentException(
          "El tipo del usuario debe ser Responsable de cartera o Supervisor para poder generar el Informe");
    }
  }

  private LinkedHashMap<String, List<Collection<?>>> createCarteraDataForExcel(
      Boolean posesionNegociada) {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();
    if (posesionNegociada == true) {
      List<Collection<?>> expediente = new ArrayList<>();
      expediente.add(ExpedientePosesionNegociadaExcel.cabeceras);

      List<Collection<?>> ocupantes = new ArrayList<>();
      ocupantes.add(OcupanteInformeExcel.cabeceras);

      List<Collection<?>> bienes = new ArrayList<>();
      bienes.add(BienCarteraExcel.cabeceras);

      if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
        datos.put("Connection", expediente);
        datos.put("Occupants", ocupantes);
        datos.put("Assets", bienes);

      } else {
        datos.put("Expediente", expediente);
        datos.put("Ocupantes", ocupantes);
        datos.put("Bienes", bienes);
      }

    } else if (posesionNegociada == false) {
      List<Collection<?>> expediente = new ArrayList<>();
      expediente.add(ExpedienteCarteraExcel.cabeceras);

      List<Collection<?>> contrato = new ArrayList<>();
      contrato.add(ContratoCarteraExcel.cabeceras);

      List<Collection<?>> intervinientes = new ArrayList<>();
      intervinientes.add(IntervinienteCarteraExcel.cabeceras);

      List<Collection<?>> bienes = new ArrayList<>();
      bienes.add(BienCarteraExcel.cabeceras);

      List<Collection<?>> judicial = new ArrayList<>();
      judicial.add(ProcedimientoCarteraExcel.cabeceras);

      if (LocaleContextHolder.getLocale().getLanguage() == null
          || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
        datos.put("Connection", expediente);
        datos.put("Loan", contrato);
        datos.put("Borrowers", intervinientes);
        datos.put("Assets", bienes);
        datos.put("Judicial", judicial);

      } else {
        datos.put("Expediente", expediente);
        datos.put("Contrato", contrato);
        datos.put("Intervinientes", intervinientes);
        datos.put("Bienes", bienes);
        datos.put("Judicial", judicial);
      }
    }

    return datos;
  }

  private void addCarteraDataForExcelPN(
      Cartera cartera,
      LinkedHashMap<String, List<Collection<?>>> datos,
      InformeCarteraInputDTO informeCarteraInputDTO,
      Usuario usuario)
      throws ParseException, NotFoundException {

    List<Collection<?>> expedientes = null;
    List<Collection<?>> ocupantes = null;
    List<Collection<?>> bienes = null;

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      expedientes = datos.get("Connection");
      ocupantes = datos.get("Occupants");
      bienes = datos.get("Assets");

    } else {
      expedientes = datos.get("Expediente");
      ocupantes = datos.get("Ocupantes");
      bienes = datos.get("Bienes");
    }

    List<ExpedientePosesionNegociada> expedientesPN = null;
    if (usuario.esAdministrador()) {
      expedientesPN = expedientePosesionNegociadaRepository.findAllByCarteraId(cartera.getId());
    } else {
      expedientesPN =
          expedientePosesionNegociadaRepository.findAllByCarteraIdAndAsignacionesUsuarioId(
              cartera.getId(), usuario.getId());
    }

    if (expedientesPN == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("There is no data for the selected user");
      else throw new NotFoundException("No existe ningún dato para el usuario seleccionado");
    }

    List<ExpedientePosesionNegociada> expedientesFilter =
        expedientesPN.stream()
            .filter(
                expediente -> {
                  Date fCE = expediente.getFechaCreacion();
                  Date fCI = informeCarteraInputDTO.getFechaCreacionInicial();
                  Date fCF = informeCarteraInputDTO.getFechaCreacionFinal();
                  if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;

                  Date fRE = expediente.getFechaCierre();
                  Date fRI = informeCarteraInputDTO.getFechaCierreInicial();
                  Date fRF = informeCarteraInputDTO.getFechaCierreFinal();
                  if (!expedienteUtil.comprobarEntre(fRE, fRI, fRF)) return false;
                  return usuario.contieneExpediente(expediente.getId(), true);
                })
            .collect(Collectors.toList());

    for (ExpedientePosesionNegociada expediente : expedientesFilter) {

      var estadoExpediente = expediente.getEstadoExpedienteRecuperacionAmistosa();

      if (informeCarteraInputDTO.getEstado() == null
          || estadoExpediente.getId().equals(informeCarteraInputDTO.getEstado())) {
        Evento accion = getLastEventoPN(expediente.getId(), 1);
        Evento alerta = getLastEventoPN(expediente.getId(), 2);
        Evento actividad = getLastEventoPN(expediente.getId(), 3);
        expedientes.add(
            new ExpedientePosesionNegociadaExcel(expediente, accion, alerta, actividad)
                .getValuesList());

        for (BienPosesionNegociada bienPN : expediente.getBienesPosesionNegociada()) {
          Bien bien = bienPN.getBien();
          bienes.add(new BienCarteraExcel(bien, null, bienPN).getValuesList());
          if (bienPN.getOcupantes() != null) {
            for (OcupanteBien ocupanteBien : bienPN.getOcupantes()) {
              ocupantes.add(new OcupanteInformeExcel(ocupanteBien).getValuesList());
            }
          }
        }
      }
    }
  }

  private void addCarteraDataForExcel(
      Cartera cartera,
      LinkedHashMap<String, List<Collection<?>>> datos,
      InformeCarteraInputDTO informeCarteraInputDTO,
      Usuario usuario)
      throws ParseException, NotFoundException {

    List<Collection<?>> expedientes;
    List<Collection<?>> contratos;
    List<Collection<?>> intervinientes;
    List<Collection<?>> bienes;
    List<Collection<?>> judicial;

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {

      expedientes = datos.get("Expediente");
      contratos = datos.get("Contrato");
      intervinientes = datos.get("Intervinientes");
      bienes = datos.get("Bienes");
      judicial = datos.get("Judicial");

    } else {

      expedientes = datos.get("Expediente");
      contratos = datos.get("Contrato");
      intervinientes = datos.get("Intervinientes");
      bienes = datos.get("Bienes");
      judicial = datos.get("Judicial");
    }

    List<Expediente> expedientesFilter = null;
    if (usuario.esAdministrador() == true) {
      expedientesFilter = expedienteRepository.findAllByCarteraId(cartera.getId());
    } else {
      expedientesFilter =
          expedienteRepository.findAllByCarteraIdAndAsignacionesUsuarioId(
              cartera.getId(), usuario.getId());
    }

    if (expedientesFilter == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("There is no data for the selected user");
      else throw new NotFoundException("No existe ningún dato para el usuario seleccionado");
    }

    expedientesFilter.stream()
        .filter(
            expediente -> {
              Date fCE = expediente.getFechaCreacion();
              Date fCI = informeCarteraInputDTO.getFechaCreacionInicial();
              Date fCF = informeCarteraInputDTO.getFechaCreacionFinal();
              if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;

              Date fRE = expediente.getFechaCierre();
              Date fRI = informeCarteraInputDTO.getFechaCierreInicial();
              Date fRF = informeCarteraInputDTO.getFechaCierreFinal();
              if (!expedienteUtil.comprobarEntre(fRE, fRI, fRF)) return false;

              return usuario.contieneExpediente(expediente.getId(), false);
            })
        .collect(Collectors.toList());

    for (Expediente expediente : expedientesFilter) {
      var estadoExpediente = expedienteUtil.getEstado(expediente);
      if (informeCarteraInputDTO.getEstado() == null
          || estadoExpediente.getId().equals(informeCarteraInputDTO.getEstado())) {
        Evento accion = getLastEvento(expediente.getId(), 1);
        Evento alerta = getLastEvento(expediente.getId(), 2);
        Evento actividad = getLastEvento(expediente.getId(), 3);
        Procedimiento p = expedienteUtil.getUltimoProcedimiento(expediente);
        expedientes.add(
            new ExpedienteCarteraExcel(expediente, accion, alerta, actividad, p).getValuesList());
        for (Contrato contrato : expediente.getContratos()) {
          contratos.add(
              new ContratoCarteraExcel(contrato, accion, alerta, actividad).getValuesList());
          if (contrato.getIntervinientes() != null) {
            for (ContratoInterviniente interviniente : contrato.getIntervinientes()) {
              intervinientes.add(
                  new IntervinienteCarteraExcel(
                          interviniente.getInterviniente(),
                          contrato.getIdCarga(),
                          interviniente.getOrdenIntervencion(),
                          interviniente.getTipoIntervencion())
                      .getValuesList());
            }
          }
          if (contrato.getBienes() != null) {
            for (ContratoBien bien : contrato.getBienes()) {
              bienes.add(
                  new BienCarteraExcel(bien.getBien(), contrato.getId(), null).getValuesList());
            }
          }
          if (contrato.getProcedimientos() != null) {
            for (Procedimiento procedimiento : contrato.getProcedimientos()) {
              judicial.add(
                  new ProcedimientoCarteraExcel(procedimiento, contrato.getIdCarga())
                      .getValuesList());
            }
          }
        }
      }
    }
  }

  public Evento getLastEventoPN(Integer idExpediente, Integer clase) {
    try {
      List<Integer> clases = new ArrayList<>();
      clases.add(clase);
      List<Evento> eventos =
          eventoUtil
              .getFilteredEventos(
                  null,
                  null,
                  null,
                  7,
                  idExpediente,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  clases,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  "fechaCreacion",
                  "desc",
                  0,
                100);

      Evento result = null;
      if (!eventos.isEmpty()) result = eventos.get(0);
      return result;
    } catch (Exception e) {
      return null;
    }
  }

  public Evento getLastEvento(Integer idExpediente, Integer clase) {
    try {
      List<Integer> clases = new ArrayList<>();
      clases.add(clase);
      List<Evento> eventos =
          eventoUtil
              .getFilteredEventos(
                  null,
                  null,
                  null,
                  1,
                  idExpediente,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  clases,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  "fechaCreacion",
                  "desc",
                0,
                100);

      Evento result = null;
      if (!eventos.isEmpty()) result = eventos.get(0);
      return result;
    } catch (Exception e) {
      return null;
    }
  }
}
