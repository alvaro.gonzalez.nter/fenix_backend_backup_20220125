package com.haya.alaska.modelo_sms.infraestructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.modelo_sms.domain.ModeloSMS;

public interface ModeloSMSRepository extends CatalogoRepository<ModeloSMS, Integer>{}
