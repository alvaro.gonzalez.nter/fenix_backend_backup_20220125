package com.haya.alaska.modelo_sms.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.haya.alaska.catalogo.domain.Catalogo;

import lombok.Getter;
import lombok.Setter;

@Audited
@AuditTable(value = "HIST_LKUP_MODELO_SMS")
@Entity
@Table(name = "LKUP_MODELO_SMS")
@Getter
@Setter
public class ModeloSMS extends Catalogo {

  private static final long serialVersionUID = 1L;

  @Column(name = "DES_MOMBRE_MODELO")
  private String nombreModelo;
}
