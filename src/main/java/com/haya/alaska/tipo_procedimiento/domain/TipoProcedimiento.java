package com.haya.alaska.tipo_procedimiento.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import lombok.Getter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_PROCEDIMIENTO")
@Entity
@Getter
@Table(name = "LKUP_TIPO_PROCEDIMIENTO")
public class TipoProcedimiento extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_PROCEDIMIENTO")
  @NotAudited
  private Set<Procedimiento> procedimientos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_PROCEDIMIENTO")
  @NotAudited
  private Set<Segmentacion> segmentaciones = new HashSet<>();

  @Column(name = "IND_RECOVERY", nullable = false)
  private boolean recovery = false;
}
