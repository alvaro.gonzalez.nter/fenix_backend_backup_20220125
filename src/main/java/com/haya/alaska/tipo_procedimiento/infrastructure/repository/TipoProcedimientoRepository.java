package com.haya.alaska.tipo_procedimiento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_procedimiento.domain.TipoProcedimiento;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TipoProcedimientoRepository extends CatalogoRepository<TipoProcedimiento, Integer> {
  Optional<TipoProcedimiento> findByCodigoAndRecoveryIsFalse(String idCodigo);

  Optional<TipoProcedimiento> findByCodigoAndRecoveryIsTrue(String idCodigo);

  Optional<TipoProcedimiento> findByValorAndRecoveryIsTrue(String valor);

  Optional<TipoProcedimiento> findByValorAndRecoveryIsFalse(String valor);
}
