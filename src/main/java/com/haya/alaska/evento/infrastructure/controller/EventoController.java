package com.haya.alaska.evento.infrastructure.controller;

import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.BusquedaAgendaDto;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.evento.application.EventoExcelUseCase;
import com.haya.alaska.evento.application.EventoUseCase;
import com.haya.alaska.evento.infrastructure.controller.dto.*;
import com.haya.alaska.resultado_evento.infrastructure.controller.dto.ResultadoEventoOutputDTO;
import com.haya.alaska.security.util.CustomUserDetails;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/agenda")
public class EventoController {
  @Autowired
  EventoUseCase eventoUseCase;
  @Autowired
  private UsuarioRepository usuarioRepository;

  @Autowired
  private EventoExcelUseCase eventoExcelUseCase;

  @ApiOperation(value = "Listado de todos los eventoes",
    notes = "Devuelve todos los eventoes registrados en la base de datos")
  @GetMapping
  public ListWithCountDTO<EventoOutputDTO> getFilteredEvents(
    @RequestParam(value = "emisorId", required = false) Integer emisorId,
    @RequestParam(value = "destinatarioId", required = false) Integer destinatarioId,
    @RequestParam(value = "nivelId", required = false) Integer nivelId,
    @RequestParam(value = "nivelObjectId", required = false) Integer nivelObjectId,
    @RequestParam(value = "carteraId", required = false) Integer carteraId,

    @RequestParam(value = "fCreaMin", required = false) Date fCreaMin,
    @RequestParam(value = "fCreaMax", required = false) Date fCreaMax,
    @RequestParam(value = "fAlerMin", required = false) Date fAlerMin,
    @RequestParam(value = "fAlerMax", required = false) Date fAlerMax,
    @RequestParam(value = "fLimiMin", required = false) Date fLimiMin,
    @RequestParam(value = "fLimiMax", required = false) Date fLimiMax,

    @RequestParam(value = "clase", required = false) List<Integer> clase,
    @RequestParam(value = "tipo", required = false) Integer tipo,
    @RequestParam(value = "subtipo", required = false) Integer subtipo,
    @RequestParam(value = "subtipoList", required = false) List<Integer> subtipoList,
    @RequestParam(value = "estado", required = false) List<Integer> estado,
    @RequestParam(value = "importancia", required = false) Integer importancia,
    @RequestParam(value = "resultado", required = false) Integer resultado,

    @RequestParam(value = "titulo", required = false) String titulo,

    @RequestParam(value = "activo", required = false) Boolean activo,
    @RequestParam(value = "revisado", required = false) Boolean revisado,

    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "size") Integer size, @RequestParam(value = "page") Integer page,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {

    Usuario usuario = (Usuario) principal.getPrincipal();
    return eventoUseCase.findAll(usuario.getId(), emisorId, destinatarioId, nivelId,
      nivelObjectId, fCreaMin, fCreaMax, fAlerMin, fAlerMax,
      fLimiMin, fLimiMax, clase, tipo, subtipo, subtipoList, carteraId, estado, importancia, resultado, titulo, activo,
      revisado, orderField, orderDirection, page, size);
  }

  @ApiOperation(value = "Obtener evento", notes = "Devuelve el evento con id enviado")
  @GetMapping("/{idEvento}")
  public EventoOutputDTO getEventoById(@PathVariable("idEvento") Integer idEvento)
    throws NotFoundException {
    return eventoUseCase.findEventoById(idEvento);
  }

  @ApiOperation(value = "Crear evento", notes = "Añadir un nuevo evento al contrato con el id enviado")
  @PostMapping
  public EventoOutputDTO createEvento(@RequestBody @Valid EventoInputDTO eventoInputDTO,
                                      @ApiIgnore CustomUserDetails principal)
    throws Exception {
    return eventoUseCase.create(eventoInputDTO, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Eliminar evento", notes = "Eliminar el evento con id enviado")
  @DeleteMapping("/{idEvento}")
  public void deleteEvento(@PathVariable("idEvento") Integer idEvento)
    throws NotFoundException {
    eventoUseCase.delete(idEvento);
  }

  @ApiOperation(value = "Modificar evento", notes = "Modificar el evento con id enviado")
  @PutMapping("/{idEvento}")
  public EventoOutputDTO updateEvento(@PathVariable("idEvento") Integer idEvento,
                                      @RequestBody @Valid EventoInputDTO eventoInputDTO,
                                      @ApiIgnore CustomUserDetails principal)
    throws NotFoundException, RequiredValueException, InvalidCodeException, IOException {
    return eventoUseCase.update(idEvento, eventoInputDTO, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Obtener tipos", notes = "Devuelve los Tipos de evento dependiendo del id de la Clase")
  @GetMapping("/tipos/{idClase}")
  public List<CatalogoMinInfoDTO> getTipos(@PathVariable("idClase") Integer idClase) throws NotFoundException {
    return eventoUseCase.findTipo(idClase);
  }

  @ApiOperation(value = "Obtener tipos por lista de clases", notes = "Devuelve los Tipos de evento por una lista de Clases")
  @GetMapping("/tipos")
  public List<CatalogoMinInfoDTO> getTipos(@RequestParam("clases") List<Integer> clases) throws NotFoundException {
    return eventoUseCase.findTipos(clases);
  }

  @ApiOperation(value = "Obtener clases filtradas", notes = "Devuelve los Clases de evento dependiendo del id de la Clase y de la Cartera")
  @GetMapping("/claseFiltrado")
  public List<CatalogoMinInfoDTO> getClasesFiltrados(
    @RequestParam(value = "carteraId", required = false) Integer carteraId) throws NotFoundException {
    return eventoUseCase.findClaseFiltrado(carteraId);

  }

  @ApiOperation(value = "Obtener tipos filtrados", notes = "Devuelve los Tipos de evento dependiendo del id de la Clase")
  @GetMapping("/tiposFiltrado/{idClase}")
  public List<CatalogoMinInfoDTO> getTiposFiltrados(
    @PathVariable("idClase") Integer idClase,
    @RequestParam(value = "idCartera", required = false) Integer idCartera) throws NotFoundException {
    return eventoUseCase.findTipoFiltrado(idClase, idCartera);
  }

  @ApiOperation(value = "Obtener subtipos", notes = "Devuelve los Subtipos de evento dependiendo del id del Tipo")
  @GetMapping("/subtipos/{idTipo}")
  public List<SubtipoEventoOutputDTO> getSubtipos(
    @PathVariable("idTipo") Integer idTipo,
    @ApiIgnore CustomUserDetails principal,
    @RequestParam(value = "carteraId", required = false) Integer carteraId) throws NotFoundException {
    return eventoUseCase.findSubtipo(idTipo, (Usuario) principal.getPrincipal(), carteraId);
  }

  @ApiOperation(value = "Obtener subtipos por tipos evento", notes = "Devuelve los Subtipos de evento dependiendo de la lista de Tipos")
  @GetMapping("/subtipos")
  public List<SubtipoEventoOutputDTO> getSubtiposPorLista(
    @RequestParam(value = "tipoEvento", required = false) List<Integer> tipoEvento,
    @ApiIgnore CustomUserDetails principal,
    @RequestParam(value = "carteraId", required = false) Integer carteraId) throws NotFoundException {
    return eventoUseCase.findSubtipos(tipoEvento, (Usuario) principal.getPrincipal(), carteraId);
  }

  @ApiOperation(value = "Obtener resultados", notes = "Devuelve los Resultados de evento dependiendo del id del Subtipo")
  @GetMapping("/resultados/{idSubtipo}")
  public List<CatalogoMinInfoDTO> getResultados(@PathVariable("idSubtipo") Integer idSubtipo,
                                                @RequestParam(value = "idEvento", required = false) Integer idEvento) throws NotFoundException {
    return eventoUseCase.findResultados(idSubtipo, idEvento);
  }

  @ApiOperation(value = "Creacion masiva de eventos", notes = "Crea el mismo evento para cada usuario en la array enviada.")
  @PostMapping("/masivo")
  public ListWithCountDTO<EventoOutputDTO> createMasivo(
    @RequestParam(value = "objetivos") List<Integer> objetivos,
    @RequestBody @Valid EventoInputDTO eventoInputDTO,
    @ApiIgnore CustomUserDetails principal)
    throws Exception {
    return eventoUseCase.masiveCreate(eventoInputDTO, objetivos, (Usuario) principal.getPrincipal());
  }

  @ApiOperation(value = "Eliminar evento", notes = "Eliminar el evento con id enviado")
  @DeleteMapping("/masivo")
  public void deleteMasivo(
    @RequestParam(value = "eventos") List<Integer> eventos
  ) throws NotFoundException {
    eventoUseCase.masiveDelete(eventos);
  }

  @ApiOperation(value = "Elevar Propuesta", notes = "Elevacion de la Propuesta con id dado.")
  @PostMapping("/elevacion")
  public void elevarPropuesta(@RequestParam(value = "idPropuesta") Integer idPropuesta,
                              @RequestParam(value = "idCartera") Integer idCartera,
                              @ApiIgnore CustomUserDetails principal)
    throws Exception {
    eventoUseCase.elevarPropuesta(idPropuesta, (Usuario) principal.getPrincipal(), idCartera);
  }

  @ApiOperation(value = "Obtener Destinatario", notes = "Devuelve el usuario objetivo para el SubtipoEvento")
  @GetMapping("/destinatario")
  public UsuarioAgendaDto getDestinatario(@RequestParam(value = "idExpediente") Integer idExpediente,
                                          @RequestParam(value = "idPerfil") Integer idPerfil,
                                          @RequestParam(value = "posesionNegociada") Boolean posesionNegociada) throws NotFoundException {
    return eventoUseCase.userByExpedienteAndPerfil(idExpediente, idPerfil, posesionNegociada);
  }

  @ApiOperation(value = "Obtener Destinatario", notes = "Devuelve el usuario objetivo para el SubtipoEvento")
  @GetMapping("/asignados")
  public List<UsuarioAgendaDto> getAsignados(@RequestParam(value = "idExpediente") Integer idExpediente,
                                             @RequestParam(value = "posesionNegociada") Boolean posesionNegociada) throws NotFoundException {
    return eventoUseCase.getExpedientUsers(idExpediente, posesionNegociada);
  }

  @ApiOperation(value = "Obtener autorización de subtipos", notes = "Devuelve true si el usuario tiene permisos para crear el subtipoEvento.")
  @GetMapping("/authorization/{idSubtipo}")
  public Boolean getAuthorization(
    @PathVariable("idSubtipo") Integer idSubtipo,
    @ApiIgnore CustomUserDetails principal) throws NotFoundException {
    return eventoUseCase.getAuthorization(idSubtipo, (Usuario) principal.getPrincipal());
  }


  @ApiOperation(value = "Modificar evento", notes = "Modificar el evento con id enviado")
  @PutMapping("/workflows")
  public void updateWorkflows(@RequestBody @Valid WorkflowInputDTO workflowInputDTO)
    throws Exception {
    eventoUseCase.updateWorkflow(workflowInputDTO);
  }


  @ApiOperation(value = "Obtener Estados por cartera", notes = "Devuelve los estados activos de la cartera")
  @GetMapping("/fases")
  public List<CarteraEstadoPropuestaOutputDTO> getActivosEstadoCartera(@RequestParam(value = "carteraId") Integer carteraId)
    throws NotFoundException {
    return eventoUseCase.getEstadoActivo(carteraId);
  }


  @ApiOperation(value = "Obtener workflow", notes = "Devuelve el workflow por cartera y estadoPropuesta")
  @GetMapping("/eventos")
  public List<SubtipoEventoOutputDTO> getworkflowCartera(@RequestParam(value = "carteraId") Integer carteraId,
                                                         @RequestParam(value = "estadoPropuestaId") Integer estadoPropuestaId)
    throws NotFoundException {
    return eventoUseCase.findByCartera(carteraId, estadoPropuestaId);
  }


  @ApiOperation(value = "Obtener Evento Destino", notes = "Devuelve los eventosDestino")
  @GetMapping("/eventoSucesor")
  public List<SubtipoEventoOutputDTO> getWorkByCarteraAndResult(@RequestParam(value = "carteraId") Integer carteraId,
                                                                @RequestParam(value = "resultadoEventoId") Integer resultadoEventoId,
                                                                @RequestParam(value = "eventoOrigenId") Integer eventoOrigenId)
    throws NotFoundException {
    return eventoUseCase.findWorkflowCarteraResultadoEventoAndSuBtipoEvento(carteraId, resultadoEventoId, eventoOrigenId);
  }


  @ApiOperation(value = "Obtener Resultado Evento", notes = "Devuelve los resultadoEvento a partir del subtipo")
  @GetMapping("/resultadoEvento")
  public List<ResultadoEventoOutputDTO> getResultadoEventoBySubtipo(@RequestParam(value = "subtipoId") Integer subtipoId)
    throws NotFoundException {
    return eventoUseCase.findResultadoEventoBySubTipoEvento(subtipoId);
  }

  @ApiOperation(value = "Solicitar SkipTracing", notes = "Crear Evento para Solicitar el SkipTracing de un Interviniente.")
  @PostMapping("/skipTracing")
  public void solicitarSkipTracing(@RequestParam(value = "idInterviniente") Integer idInterviniente,
                                   @RequestParam(value = "idContrato") Integer idContrato,
                                   @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario creador = (Usuario) principal.getPrincipal();
    eventoUseCase.solicitarSkipTracing(idInterviniente, idContrato, creador);
  }

  @ApiOperation(
      value = "Solicitar SkipTracing",
      notes = "Crear Evento para Solicitar el SkipTracing de un Interviniente.")
  @PostMapping("/llamadaAbierta")
  public void llamadaAbierta(
      @RequestParam(value = "idInterviniente") Integer idInterviniente,
      @RequestParam(value = "idExpediente") Integer idExpediente,
      @RequestParam(value = "telefono") String telefono,
      @RequestParam(value = "posesionNegociada") Boolean posesionNegociada,
      @ApiIgnore CustomUserDetails principal)
      throws Exception {
    Usuario creador = (Usuario) principal.getPrincipal();
    eventoUseCase.llamadaAbierta(
        idExpediente, idInterviniente, telefono, posesionNegociada, creador);
  }

  @ApiOperation(value = "Solicitad comunicación interna", notes = "Solicita una comunicación o una alerta a usuarios internos.")
  @PostMapping("/interno")
  public void crearEventoInterno(
    @RequestBody(required = false) EventoMasBusquedaInputDTO mezcla,
    @RequestParam(required = false) List<Integer> expedienteIdList,
    @RequestParam(required = false) List<Integer> expedientePNIdList,
    @RequestParam(required = false) Boolean todos,
    @RequestParam(required = false) Boolean todosPN,
    @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario logUser = (Usuario) principal.getPrincipal();
    eventoUseCase.createEventoUtilidades(mezcla.getInput(), expedienteIdList, expedientePNIdList,
      todos, todosPN, mezcla.getBusqueda(), logUser);
  }

  @ApiOperation(value = "Obten el último Evento de WF", notes = "Solicita una comunicación o una alerta a usuarios internos.")
  @GetMapping("/{idPropuesta}/wf")
  public Integer getEventoWF(@PathVariable("idPropuesta") Integer idPropuesta) throws Exception {
    return eventoUseCase.eventoFromPropuesta(idPropuesta);
  }

  @ApiOperation(value = "Obten el último Evento de WF", notes = "Solicita una comunicación o una alerta a usuarios internos.")
  @GetMapping("/{idPropuesta}/wfOutput")
  public EventoOutputDTO getEventoWFOutput(@PathVariable("idPropuesta") Integer idPropuesta) throws Exception {
    return eventoUseCase.getLastWF(idPropuesta);
  }

  @ApiOperation(value = "Lista para pestaña Actividad", notes = "Lista de eventos filtrados para la pestaña de actividad de la agenda.")
  @PostMapping("/listaActividad")
  public ListWithCountDTO<EventoOutputDTO> getListActividad(
    @RequestParam(value = "nivelId", required = false) Integer nivelId,
    @RequestParam(value = "nivelObjectId", required = false) Integer nivelObjectId,
    @RequestParam(value = "orderField", required = false) String orderField,
    @RequestParam(value = "orderDirection", required = false) String orderDirection,
    @RequestParam(value = "page") Integer page,
    @RequestParam(value = "size") Integer size,
    @RequestBody BusquedaAgendaDto busquedaAgendaDto,
    @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario creador = (Usuario) principal.getPrincipal();
    if (orderField == null) orderField = "fechaAlerta";
    if (orderDirection == null) orderDirection = "desc";
    return eventoUseCase.getListActividad(creador.getId(), busquedaAgendaDto, nivelId, nivelObjectId, orderField, orderDirection, page, size);
  }

  @ApiOperation(value = "Solicitar Judilicialización", notes = "Solicitar evento de Judicialización para el procedimiento con el id enviado")
  @PostMapping("/judicializar")
  public void judicializar(@RequestParam(value = "idContrato") Integer idContrato,
                           @RequestParam(value = "idInterviniente") Integer idInterviniente,
                           @RequestParam(value = "fechaPeticion") Date fechaPeticion,
                           @ApiIgnore CustomUserDetails principal) throws Exception {
    Usuario creador = (Usuario) principal.getPrincipal();
    eventoUseCase.eventoWFJudicial(idContrato, idInterviniente, fechaPeticion, creador);
  }
}
