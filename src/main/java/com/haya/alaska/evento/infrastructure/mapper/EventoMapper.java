package com.haya.alaska.evento.infrastructure.mapper;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien.infrastructure.mapper.BienMapper;
import com.haya.alaska.bien.infrastructure.repository.BienRepository;
import com.haya.alaska.bien_posesion_negociada.infrastructure.mapper.BienPosesionNegociadaMapper;
import com.haya.alaska.bien_posesion_negociada.infrastructure.repository.BienPosesionNegociadaRepository;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.clase_evento.infrastructure.repository.ClaseEventoRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import com.haya.alaska.estado_evento.infrastructure.repository.EstadoEventoRepository;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion.infrastructure.controller.dto.EstimacionDTO;
import com.haya.alaska.estimacion.infrastructure.repository.EstimacionRepository;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoOutputDTO;
import com.haya.alaska.evento.infrastructure.controller.dto.SubtipoEventoOutputDTO;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.mapper.ExpedienteMapper;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.mapper.ExpedientePosesionNegociadaMapper;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.importancia_evento.infrastructure.repository.ImportanciaEventoRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.mapper.IntervinienteMapper;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.nivel_evento.infrastructure.repository.NivelEventoRepository;
import com.haya.alaska.periodicidad_evento.infrastructure.repository.PeriodicidadEventoRepository;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.procedimiento.infrastructure.mapper.ProcedimientoMapper;
import com.haya.alaska.procedimiento.infrastructure.repository.ProcedimientoRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.controller.dto.PropuestaDTOToList;
import com.haya.alaska.propuesta.infrastructure.mapper.PropuestaMapper;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.resultado_evento.infrastructure.repository.ResultadoEventoRepository;
import com.haya.alaska.shared.dto.ContratoDTOToList;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.tipo_evento.infrastructure.repository.TipoEventoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.haya.alaska.utilidades.comunicaciones.application.domain.*;
import com.haya.alaska.utilidades.comunicaciones.infraestructure.repository.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.haya.alaska.shared.Utils.getNullPropertyNames;

@Component
public class EventoMapper {

  @Autowired EventoRepository eventoRepository;
  @Autowired EventoUtil eventoUtil;
  @Autowired ClaseEventoRepository claseEventoRepository;
  @Autowired TipoEventoRepository tipoEventoRepository;
  @Autowired SubtipoEventoRepository subtipoEventoRepository;
  @Autowired PeriodicidadEventoRepository periodicidadEventoRepository;
  @Autowired EstadoEventoRepository estadoEventoRepository;
  @Autowired ImportanciaEventoRepository importanciaEventoRepository;
  @Autowired ResultadoEventoRepository resultadoEventoRepository;
  @Autowired UsuarioRepository usuarioRepository;
  @Autowired NivelEventoRepository nivelEventoRepository;
  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired ExpedienteMapper expedienteMapper;
  @Autowired ContratoRepository contratoRepository;
  @Autowired BienRepository bienRepository;
  @Autowired BienMapper bienMapper;
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired IntervinienteMapper intervinienteMapper;
  @Autowired ProcedimientoRepository procedimientoRepository;
  @Autowired ProcedimientoMapper procedimientoMapper;
  @Autowired PropuestaRepository propuestaRepository;
  @Autowired PropuestaMapper propuestaMapper;
  @Autowired ExpedientePosesionNegociadaRepository ePNRepository;
  @Autowired BienPosesionNegociadaRepository bPNRepository;
  @Autowired ExpedientePosesionNegociadaMapper ePNMapper;
  @Autowired BienPosesionNegociadaMapper bPNMapper;
  @Autowired EstimacionRepository estimacionRepository;
  @Autowired CarteraRepository carteraRepository;
  @Autowired
  EnvioEmailRepository envioEmailRepository;
  @Autowired
  EnvioCartaRepository envioCartaRepository;
  @Autowired
  EnvioSMSRepository envioSMSRepository;
  @Autowired
  EnvioBurofaxRepository envioBurofaxRepository;
  @Autowired
  PlantillaCartaRepository plantillaCartaRepository;
  @Autowired
  PlantillaBurofaxRepository plantillaBurofaxRepository;
  @Autowired
  PlantillaSMSRepository plantillaSMSRepository;

  public Evento createEvento(EventoInputDTO input, Usuario emisor)
      throws RequiredValueException, NotFoundException {
    Evento evento = new Evento();
    BeanUtils.copyProperties(input, evento, "revisado");

    if (input.getFechaCreacion() == null) evento.setFechaCreacion(Calendar.getInstance().getTime());
    else evento.setFechaCreacion(input.getFechaCreacion());

    evento.setComentariosDestinatario(input.getComentariosDestinatario());

    Integer idObjeto;
    idObjeto = input.getIdEventoPadre();
    evento.setEventoPadre(
        idObjeto != null ? eventoRepository.findById(idObjeto).orElse(null) : null);

    idObjeto = input.getClaseEvento();
    if (idObjeto == null) throw new RequiredValueException("Clase");
    evento.setClaseEvento(
        claseEventoRepository
            .findById(idObjeto)
            .orElseThrow(() -> new NotFoundException("claseEvento", input.getClaseEvento())));

    idObjeto = input.getCartera();
    if (idObjeto != null) evento.setCartera(carteraRepository.findById(idObjeto).orElse(null));

    idObjeto = input.getImportancia();
    if (idObjeto == null) throw new RequiredValueException("Importancia");
    evento.setImportancia(
        importanciaEventoRepository
            .findById(idObjeto)
            .orElseThrow(() -> new NotFoundException("importanciaEvento", input.getImportancia())));

    if (input.getEstado() == null) {
      evento.setEstado(
          estadoEventoRepository
              .findByCodigo("PEN")
              .orElseThrow(() -> new NotFoundException("estadoEvento", "código", "PEN")));
    } else {
      idObjeto = input.getEstado();
      evento.setEstado(
          estadoEventoRepository
              .findById(idObjeto)
              .orElseThrow(() -> new NotFoundException("estadoEvento", input.getEstado())));
    }

    Boolean variarEstado = true;
    if (!evento.getClaseEvento().getCodigo().equals("COM")) {
      idObjeto = input.getTipoEvento();
      if (idObjeto == null) throw new RequiredValueException("Tipo");
      TipoEvento te =
          tipoEventoRepository
              .findById(idObjeto)
              .orElseThrow(() -> new NotFoundException("tipoEvento", input.getTipoEvento()));
      evento.setTipoEvento(te);

      idObjeto = input.getSubtipoEvento();
      if (idObjeto == null) throw new RequiredValueException("Subtipo");
      SubtipoEvento subtipoEvento =
          subtipoEventoRepository
              .findById(idObjeto)
              .orElseThrow(() -> new NotFoundException("subtipoEvento", input.getSubtipoEvento()));
      evento.setSubtipoEvento(subtipoEvento);

      if (input.getFechaLimite() != null) evento.setFechaLimite(input.getFechaLimite());
      if (input.getFechaCreacion() != null) evento.setFechaCreacion(input.getFechaCreacion());
      if (input.getFechaAlerta() != null) evento.setFechaAlerta(input.getFechaAlerta());

      switch (evento.getClaseEvento().getCodigo()) {
        case "ACC":
          if (input.getDestinatario() != null)
            evento.setDestinatario(
                usuarioRepository.findById(input.getDestinatario()).orElse(null));
          else evento.setDestinatario(emisor);

          if (input.getFechaLimite() == null) {
            Integer dias = subtipoEvento.getFechaLimite();
            if (dias != null) {
              Calendar c = Calendar.getInstance();
              c.setTime(evento.getFechaCreacion());
              c.add(Calendar.DATE, dias);
              evento.setFechaLimite(c.getTime());
            } else {
              evento.setFechaLimite(evento.getFechaCreacion());
              if (input.getFechaAlerta() == null) {
                evento.setEstado(estadoEventoRepository.findByCodigo("REA").orElse(null));
              }
            }
          }
          if (input.getFechaAlerta() == null) {
            evento.setFechaAlerta(evento.getFechaCreacion());
          }
          break;
        case "ALE":
          if (input.getDestinatario() == null) throw new RequiredValueException("Destinatario");
          evento.setDestinatario(usuarioRepository.findById(input.getDestinatario()).orElse(null));
          if (te.getCodigo().equals("ALE_02") && !subtipoEvento.getCodigo().equals("A_FORM")) {
            evento.setEstado(estadoEventoRepository.findByCodigo("REA").orElse(null));
            variarEstado = false;
          }

          if (input.getFechaLimite() == null) {
            if (!evento.getImportancia().getCodigo().equals("ALT")) {
              Integer dias = subtipoEvento.getFechaLimite();
              if (dias != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(evento.getFechaCreacion());
                c.add(Calendar.DATE, dias);
                evento.setFechaLimite(c.getTime());
              } else {
                throw new RequiredValueException("FechaLimite");
              }
            }
          }
          if (input.getFechaAlerta() == null) throw new RequiredValueException("FechaAlerta");
          break;
        case "ACT":
          if (input.getDestinatario() != null)
            evento.setDestinatario(
                usuarioRepository.findById(input.getDestinatario()).orElse(null));
          else evento.setDestinatario(emisor);

          if (input.getFechaCreacion() != null) {
            evento.setFechaCreacion(input.getFechaCreacion());
            Calendar cInput = Calendar.getInstance();
            cInput.setTime(input.getFechaCreacion());
            cInput.set(Calendar.MILLISECOND, 0);
            cInput.set(Calendar.SECOND, 0);
            cInput.set(Calendar.MINUTE, 0);
            cInput.set(Calendar.HOUR, 0);
            Date dInput = cInput.getTime();
            Calendar cToday = Calendar.getInstance();
            cToday.set(Calendar.MILLISECOND, 0);
            cToday.set(Calendar.SECOND, 0);
            cToday.set(Calendar.MINUTE, 0);
            cToday.set(Calendar.HOUR, 0);
            Date dToday = cToday.getTime();
            if (dInput.compareTo(dToday) < 0)
              evento.setEstado(estadoEventoRepository.findByCodigo("REA").orElse(null));
          }
          if (input.getFechaLimite() == null) {
            Integer dias = subtipoEvento.getFechaLimite();
            if (dias != null) {
              Calendar c = Calendar.getInstance();
              c.setTime(evento.getFechaCreacion());
              c.add(Calendar.DATE, dias);
              evento.setFechaLimite(c.getTime());
            } else {
              evento.setFechaLimite(evento.getFechaCreacion());
              if (input.getFechaAlerta() == null) {
                evento.setEstado(estadoEventoRepository.findByCodigo("REA").orElse(null));
              }
            }
          }
          if (input.getFechaAlerta() == null) {
            evento.setFechaAlerta(evento.getFechaCreacion());
          }
          break;
      }
    } else {
      evento.setTipoEvento(
          tipoEventoRepository
              .findByCodigo("COM_01")
              .orElseThrow(() -> new NotFoundException("tipoEvento", "código", "COM_01")));

      evento.setSubtipoEvento(
          subtipoEventoRepository
              .findByCodigoAndCarteraId("COMU01", input.getCartera())
              .orElseThrow(() -> new NotFoundException("subtipoEvento", "código", "COMU01")));

      if (input.getDestinatario() == null) throw new RequiredValueException("Destinatario");
      evento.setDestinatario(usuarioRepository.findById(input.getDestinatario()).orElse(null));

      evento.setFechaAlerta(new Date());
      evento.setFechaCreacion(new Date());
      evento.setFechaLimite(new Date());

      if (input.getIdEventoPadre() != null) {
        Evento comuPadre =
            eventoRepository
                .findById(input.getIdEventoPadre())
                .orElseThrow(() -> new NotFoundException("Evento", input.getIdEventoPadre()));
        if (comuPadre.getClaseEvento().getCodigo().equals("COM")) {
          EstadoEvento re =
              estadoEventoRepository
                  .findByCodigo("REA")
                  .orElseThrow(() -> new NotFoundException("ResultadoEvento", "Código", "REA"));
          comuPadre.setEstado(re);
          eventoRepository.save(comuPadre);
        }
      }
    }

    if (input.getCorreo() == null) evento.setCorreo(evento.getSubtipoEvento().getCorreo());
    if (input.getProgramacion() == null)
      evento.setProgramacion(evento.getSubtipoEvento().getProgramacion());

    idObjeto = input.getPeriodicidad();
    evento.setPeriodicidad(
        idObjeto != null
            ? periodicidadEventoRepository
                .findById(idObjeto)
                .orElseThrow(
                    () -> new NotFoundException("periodicidadEvento", input.getPeriodicidad()))
            : evento.getSubtipoEvento().getPeriodicidad());

    if (variarEstado && input.getRevisado() != null && input.getRevisado()) {
      evento.setEstado(estadoEventoRepository.findByCodigo("PRO").orElse(null));
    }
    idObjeto = input.getResultado();
    evento.setResultado(
        idObjeto != null
            ? resultadoEventoRepository
                .findById(idObjeto)
                .orElseThrow(() -> new NotFoundException("resultadoEvento", input.getResultado()))
            : null);
    if (variarEstado && input.getResultado() != null) {
      evento.setEstado(estadoEventoRepository.findByCodigo("REA").orElse(null));
    }

    evento.setEmisor(emisor);

    if (input.getNivel() == null && input.getNivelId() == null) {
      evento.setNivel(
          nivelEventoRepository
              .findByCodigo("GEN")
              .orElseThrow(() -> new NotFoundException("nuvelEvento", "código", "'GEN'")));
      evento.setIdNivel(0);
    } else {
      idObjeto = input.getNivel();
      if (idObjeto == null) throw new RequiredValueException("Nivel");
      evento.setNivel(
          nivelEventoRepository
              .findById(idObjeto)
              .orElseThrow(() -> new NotFoundException("nivelEvento", input.getNivel())));

      idObjeto = input.getNivelId();
      if (idObjeto == null) throw new RequiredValueException("Nivel Id");
      evento.setIdNivel(idObjeto);
    }

    return evento;
  }

  public EventoOutputDTO createEventoOutputDTO(Evento evento) throws NotFoundException {
    EventoOutputDTO outputDTO = new EventoOutputDTO();
    BeanUtils.copyProperties(evento, outputDTO);

    outputDTO.setEditable(true);

    Expediente expediente = null;
    ExpedientePosesionNegociada expedientePN = null;

    if (evento.getIdExpediente() != null) {
      String codigo = evento.getNivel().getCodigo();
      if (!codigo.equals("EXP_PN") && !codigo.equals("BIEN_PN")
        && !codigo.equals("CEXT_SMS_PN") && !codigo.equals("CEXT_EMAIL_PN") && !codigo.equals("CEXT_CARTA_PN") && !codigo.equals("CEXT_BUROFAX_PN")) {
        if (codigo.equals("PRO")) {
          Propuesta pro =
              propuestaRepository
                  .findById(evento.getIdNivel())
                  .orElseThrow(() -> new NotFoundException("propuesta", evento.getIdNivel()));
          if (pro.getExpediente() != null) expediente = pro.getExpediente();
          else if (pro.getExpedientePosesionNegociada() != null)
            expedientePN = pro.getExpedientePosesionNegociada();
        } else if (codigo.equals("EST")) {
          Estimacion est =
              estimacionRepository
                  .findById(evento.getIdNivel())
                  .orElseThrow(() -> new NotFoundException("estimacion", evento.getIdNivel()));
          if (est.getExpediente() != null) expediente = est.getExpediente();
          else if (est.getExpedientePosesionNegociada() != null)
            expedientePN = est.getExpedientePosesionNegociada();
        } else {
          expediente =
              expedienteRepository
                  .findById(evento.getIdExpediente())
                  .orElseThrow(() -> new NotFoundException("expediente", evento.getIdExpediente()));
        }
      } else {
        expedientePN =
            expedientePosesionNegociadaRepository
                .findById(evento.getIdExpediente())
                .orElseThrow(() -> new NotFoundException("expedientePN", evento.getIdExpediente()));
      }
    }

    if (expediente != null) {
      Contrato c = expediente.getContratoRepresentante();
      if (c != null) {
        Interviniente tit = c.getPrimerInterviniente();
        if (tit != null && tit.getPersonaJuridica()!=null && !tit.getPersonaJuridica()) {
          outputDTO.setPrimerTitular(tit.getNombre() + " " + tit.getApellidos());
        } else if (tit != null && tit.getPersonaJuridica()!=null && tit.getPersonaJuridica()) {
          outputDTO.setPrimerTitular(tit.getRazonSocial());
        } else outputDTO.setPrimerTitular("");
      } else outputDTO.setPrimerTitular("");
      outputDTO.setIdConcatenado(expediente.getIdConcatenado());
    }

    if (expedientePN != null) {
      outputDTO.setIdConcatenado(expedientePN.getIdConcatenado());
    }

    outputDTO.setIdEventoPadre(
        evento.getEventoPadre() != null ? evento.getEventoPadre().getId() : null);

    outputDTO.setEmisor(
        evento.getEmisor() != null ? new UsuarioAgendaDto(evento.getEmisor()) : null);
    if(evento.getSubtipoEvento().getCodigo().contains("CEXT")) {
      outputDTO.setUsuario("Nota del Sistema");
      if(evento.getNivel().getCodigo().equals("CEXT_EMAIL") || evento.getNivel().getCodigo().equals("CEXT_EMAIL_PN")) {
        EnvioEmail envioEmail = envioEmailRepository.findById(evento.getIdNivel()).orElse(null);
        outputDTO.setDireccion(envioEmail.getEmail());
        outputDTO.setPlantilla(envioEmail.getTextoEmail());

      } else if(evento.getNivel().getCodigo().equals("CEXT_CARTA") || evento.getNivel().getCodigo().equals("CEXT_CARTA_PN")) {
        EnvioCarta envioCarta = envioCartaRepository.findById(evento.getIdNivel()).orElse(null);
        outputDTO.setDireccion(envioCarta.getDireccion());
        outputDTO.setIdPlantilla(envioCarta.getModelo().getId());
        PlantillaCarta plantillaCarta = plantillaCartaRepository.findById(envioCarta.getModelo().getId()).orElse(null);
        if(plantillaCarta != null) {
          if(plantillaCarta.getNombrePlantilla().equalsIgnoreCase("Carta Texto Libre")) {
            outputDTO.setPlantilla(envioCarta.getTexto());
          } else {
            outputDTO.setPlantilla(plantillaCarta.getTextoPlantilla());
          }
        }

      } else if(evento.getNivel().getCodigo().equals("CEXT_SMS") || evento.getNivel().getCodigo().equals("CEXT_SMS_PN")) {
        EnvioSMS envioSMS = envioSMSRepository.findById(evento.getIdNivel()).orElse(null);
        PlantillaSMS plantillaSMS = plantillaSMSRepository.findByCodigoAPI(envioSMS.getCodigoPlantilla()).orElse(null);
        outputDTO.setDireccion(envioSMS.getMobileNumber());
        if(plantillaSMS != null) {
          if(plantillaSMS.getNombrePlantilla().equalsIgnoreCase("Sms Texto Libre"))
            outputDTO.setPlantilla(envioSMS.getTextoSMS());
          else
            outputDTO.setPlantilla(plantillaSMS.getTextoPlantilla());
        }

      } else if(evento.getNivel().getCodigo().equals("CEXT_BUROFAX") || evento.getNivel().getCodigo().equals("CEXT_BUROFAX_PN") ) {
        //todo esto cuando se implemente habra que cambiarlo probablemente
        EnvioBurofax envioBurofax = envioBurofaxRepository.findById(evento.getIdNivel()).orElse(null);
        outputDTO.setDireccion(envioBurofax.getDireccion());
        outputDTO.setIdPlantilla(envioBurofax.getModelo().getId());
        PlantillaBurofax plantillaBurofax = plantillaBurofaxRepository.findById(envioBurofax.getModelo().getId()).orElse(null);
        if(plantillaBurofax != null) outputDTO.setPlantilla(plantillaBurofax.getTextoPlantilla());
      }
    } else {
      if (evento.getEmisor() != null)
        outputDTO.setUsuario(evento.getEmisor().getNombre());
    }
    outputDTO.setDestinatario(
        evento.getDestinatario() != null ? new UsuarioAgendaDto(evento.getDestinatario()) : null);

    outputDTO.setClaseEvento(
        evento.getClaseEvento() != null ? new CatalogoMinInfoDTO(evento.getClaseEvento()) : null);
    outputDTO.setTipoEvento(
        evento.getTipoEvento() != null ? new CatalogoMinInfoDTO(evento.getTipoEvento()) : null);
    outputDTO.setSubtipoEvento(
        evento.getSubtipoEvento() != null
            ? new SubtipoEventoOutputDTO(evento.getSubtipoEvento())
            : null);
    outputDTO.setPeriodicidad(
        evento.getPeriodicidad() != null ? new CatalogoMinInfoDTO(evento.getPeriodicidad()) : null);
    outputDTO.setEstado(
        evento.getEstado() != null ? new CatalogoMinInfoDTO(evento.getEstado()) : null);
    outputDTO.setImportancia(
        evento.getImportancia() != null ? new CatalogoMinInfoDTO(evento.getImportancia()) : null);
    outputDTO.setResultado(
        evento.getResultado() != null ? new CatalogoMinInfoDTO(evento.getResultado()) : null);

    if (evento.getNivel() != null) {
      outputDTO.setNivel(new CatalogoMinInfoDTO(evento.getNivel()));
      if (evento.getIdNivel() != null) {
        Integer idObject = evento.getIdNivel();
        switch (evento.getNivel().getCodigo().toUpperCase()) {
          case "EXP":
            outputDTO.setNivelObject(
                expedienteMapper.parse(
                    expedienteRepository
                        .findById(idObject)
                        .orElseThrow(() -> new NotFoundException("expediente", idObject))));
            break;
          case "CON":
            outputDTO.setNivelObject(
                new ContratoDTOToList(
                    contratoRepository
                        .findById(idObject)
                        .orElseThrow(() -> new NotFoundException("contrato", idObject))));
            break;
          case "BIEN":
            Bien bien =
                bienRepository
                    .findById(idObject)
                    .orElseThrow(() -> new NotFoundException("bien", idObject));
            outputDTO.setNivelObject(bienMapper.bienToBienDTOToListAgenda(bien));
            break;
          case "INTE":
            outputDTO.setNivelObject(
                intervinienteMapper.parseEntityToDTOListAgenda(
                    intervinienteRepository
                        .findById(idObject)
                        .orElseThrow(() -> new NotFoundException("interviniente", idObject))));
            break;
          case "JUD":
            if (!evento.getEmisor().getEmail().startsWith("recovery")
                && evento.getIdRecovery() == null
                && !(evento.getTitulo() != null
                    && evento.getTitulo().startsWith("Judicialización"))) {
              Procedimiento procedimiento =
                  procedimientoRepository
                      .findById(idObject)
                      .orElseThrow(() -> new NotFoundException("procedimiento", idObject));
              outputDTO.setNivelObject(procedimientoMapper.procedimientoDTOToList(procedimiento));
            } else {
              outputDTO.setIdRecovery(evento.getIdRecovery());
              outputDTO.setNivelObject(evento.getIdNivel());
            }
            break;
          case "PRO":
            Boolean pn = false;
            Propuesta prop =
                propuestaRepository
                    .findById(idObject)
                    .orElseThrow(() -> new NotFoundException("propuesta", idObject));
            Integer ultimoWF = eventoUtil.eventoFromPropuesta(prop.getId());
            if (ultimoWF != null && evento.getId().equals(ultimoWF))
              outputDTO.setEditable(eventoUtil.comprobarEditable(prop));
            if (prop.getExpediente() != null) pn = false;
            else if (prop.getExpedientePosesionNegociada() != null) pn = true;
            else {
              Locale loc = LocaleContextHolder.getLocale();
              if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                throw new NotFoundException(
                    "The proposal of id: "
                        + idObject
                        + " is not associated to any file or filePosesionNegociada.");
              else
                throw new NotFoundException(
                    "La propuesta de id: "
                        + idObject
                        + " no está asociada a ningún expediente ni expedientePosesionNegociada.");
            }

            outputDTO.setNivelObject(
                propuestaMapper.createPropuestaDTOToList(prop, new PropuestaDTOToList(), pn));
            break;
          case "EXP_PN":
            outputDTO.setNivelObject(
                ePNMapper.parse(
                    ePNRepository
                        .findById(idObject)
                        .orElseThrow(
                            () ->
                                new NotFoundException("expediente posesion negociada", idObject))));
            break;
          case "BIEN_PN":
            outputDTO.setNivelObject(
                bPNMapper.entityOutputDto(
                    bPNRepository
                        .findById(idObject)
                        .orElseThrow(
                            () -> new NotFoundException("bien posesion negociada", idObject))));
            break;
          case "EST":
            outputDTO.setNivelObject(
                new EstimacionDTO(
                    estimacionRepository
                        .findById(idObject)
                        .orElseThrow(
                            () ->
                                new NotFoundException("estimación posesion negociada", idObject))));
            break;
          default:
            CatalogoMinInfoDTO c = new CatalogoMinInfoDTO();
            c.setId(idObject);
            outputDTO.setNivelObject(c);
            break;
        }
      } else {
        outputDTO.setNivel(null);
        outputDTO.setNivelObject(null);
      }
    } else {
      outputDTO.setNivel(null);
      outputDTO.setNivelObject(null);
    }

    return outputDTO;
  }

  public Evento modificarEvento(EventoInputDTO input, Evento evento) throws NotFoundException {
    BeanUtils.copyProperties(input, evento, getNullPropertyNames(input));
    Integer idObjeto;
    idObjeto = input.getIdEventoPadre();
    evento.setEventoPadre(
        idObjeto != null ? eventoRepository.findById(idObjeto).orElse(null) : null);

    idObjeto = input.getClaseEvento();
    evento.setClaseEvento(
        claseEventoRepository
            .findById(idObjeto)
            .orElseThrow(() -> new NotFoundException("claseEvento", input.getClaseEvento())));

    idObjeto = input.getTipoEvento();
    evento.setTipoEvento(
        tipoEventoRepository
            .findById(idObjeto)
            .orElseThrow(() -> new NotFoundException("tipoEvento", input.getTipoEvento())));

    idObjeto = input.getSubtipoEvento();
    evento.setSubtipoEvento(
        subtipoEventoRepository
            .findById(idObjeto)
            .orElseThrow(() -> new NotFoundException("subtipoEvento", input.getSubtipoEvento())));

    evento.setComentariosDestinatario(input.getComentariosDestinatario());

    idObjeto = input.getPeriodicidad();
    evento.setPeriodicidad(
        idObjeto != null
            ? periodicidadEventoRepository
                .findById(idObjeto)
                .orElseThrow(
                    () -> new NotFoundException("periodicidadEvento", input.getPeriodicidad()))
            : null);
    idObjeto = input.getImportancia();
    evento.setImportancia(
        importanciaEventoRepository
            .findById(idObjeto)
            .orElseThrow(() -> new NotFoundException("importanciaEvento", input.getImportancia())));
    idObjeto = input.getEstado();
    evento.setEstado(
        estadoEventoRepository
            .findById(idObjeto)
            .orElseThrow(() -> new NotFoundException("estadoEvento", input.getEstado())));
    if (input.getRevisado()) {
      evento.setEstado(estadoEventoRepository.findByCodigo("PRO").orElse(null));
    }
    idObjeto = input.getResultado();
    evento.setResultado(
        idObjeto != null
            ? resultadoEventoRepository
                .findById(idObjeto)
                .orElseThrow(() -> new NotFoundException("resultadoEvento", input.getResultado()))
            : null);
    if (input.getResultado() != null) {
      evento.setEstado(estadoEventoRepository.findByCodigo("REA").orElse(null));
    }

    idObjeto = input.getDestinatario();
    evento.setDestinatario(
        idObjeto != null ? usuarioRepository.findById(idObjeto).orElse(null) : null);

    idObjeto = input.getNivel();
    evento.setNivel(
        nivelEventoRepository
            .findById(idObjeto)
            .orElseThrow(() -> new NotFoundException("nivelEvento", input.getImportancia())));

    idObjeto = input.getNivelId();
    evento.setIdNivel(idObjeto);

    return evento;
  }

  public EventoInputDTO parseInput(Evento e) {
    EventoInputDTO result = new EventoInputDTO();

    result.setIdEventoPadre(e.getEventoPadre() != null ? e.getEventoPadre().getId() : null);
    result.setCartera(e.getCartera() != null ? e.getCartera().getId() : null);

    result.setTitulo(e.getTitulo());
    result.setComentariosDestinatario(e.getComentariosDestinatario());

    result.setFechaCreacion(e.getFechaCreacion());
    result.setFechaLimite(e.getFechaLimite());
    result.setFechaAlerta(e.getFechaAlerta());

    result.setClaseEvento(e.getClaseEvento() != null ? e.getClaseEvento().getId() : null);
    result.setTipoEvento(e.getTipoEvento() != null ? e.getTipoEvento().getId() : null);
    result.setSubtipoEvento(e.getSubtipoEvento() != null ? e.getSubtipoEvento().getId() : null);
    result.setPeriodicidad(e.getPeriodicidad() != null ? e.getPeriodicidad().getId() : null);
    result.setEstado(e.getEstado() != null ? e.getEstado().getId() : null);
    result.setImportancia(e.getImportancia() != null ? e.getImportancia().getId() : null);
    result.setResultado(e.getResultado() != null ? e.getResultado().getId() : null);

    result.setDestinatario(e.getDestinatario() != null ? e.getDestinatario().getId() : null);

    result.setProgramacion(e.getProgramacion());
    result.setCorreo(e.getCorreo());
    result.setAutogenerado(e.getAutogenerado());
    result.setRevisado(e.getRevisado());

    result.setNivel(e.getNivel() != null ? e.getNivel().getId() : null);
    result.setNivelId(e.getIdNivel());
    result.setIdExpediente(e.getIdExpediente());

    return result;
  }
}
