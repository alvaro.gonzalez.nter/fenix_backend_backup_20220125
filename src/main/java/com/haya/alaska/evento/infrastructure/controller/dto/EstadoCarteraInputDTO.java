package com.haya.alaska.evento.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class EstadoCarteraInputDTO implements Serializable {
  private Integer id;
  private Boolean activo;
}
