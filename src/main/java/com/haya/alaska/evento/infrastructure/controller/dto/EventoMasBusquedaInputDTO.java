package com.haya.alaska.evento.infrastructure.controller.dto;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class EventoMasBusquedaInputDTO implements Serializable {
  private EventoInputDTO input;
  private BusquedaDto busqueda;
}
