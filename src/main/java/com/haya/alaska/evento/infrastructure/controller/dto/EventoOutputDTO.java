package com.haya.alaska.evento.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class EventoOutputDTO implements Serializable {
  private Integer id;
  private Integer idEventoPadre;
  private String titulo;
  private String descripcion = "";
  private String comentariosDestinatario;

  private Date fechaLimite;
  private Date fechaCreacion; //fecha hora
  private Date fechaAlerta;
  private CatalogoMinInfoDTO claseEvento;
  private CatalogoMinInfoDTO tipoEvento;
  private SubtipoEventoOutputDTO subtipoEvento;
  private CatalogoMinInfoDTO periodicidad;
  private CatalogoMinInfoDTO estado;
  private CatalogoMinInfoDTO importancia;
  private CatalogoMinInfoDTO resultado;

  private UsuarioAgendaDto emisor; //usuario
  private UsuarioAgendaDto destinatario;

  private Boolean programacion;
  private Boolean correo;
  private Boolean activo;
  private Boolean revisado;
  private Boolean autogenerado;

  private Boolean editable;

  private CatalogoMinInfoDTO nivel;
  private Object nivelObject;
  private Long idRecovery;
  private Integer idExpediente;
  private String idConcatenado;
  private String primerTitular;

  private String usuario;
  private String direccion;
  private String plantilla;
  private Integer idPlantilla;

}
