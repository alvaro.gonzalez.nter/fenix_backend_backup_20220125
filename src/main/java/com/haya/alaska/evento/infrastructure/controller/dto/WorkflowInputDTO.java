package com.haya.alaska.evento.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class WorkflowInputDTO implements Serializable {
  Integer idCartera;
  List<EstadoCarteraInputDTO> estados;
}
