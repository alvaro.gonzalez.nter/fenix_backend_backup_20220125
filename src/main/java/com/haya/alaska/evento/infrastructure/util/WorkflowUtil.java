package com.haya.alaska.evento.infrastructure.util;

import com.haya.alaska.bien_posesion_negociada.application.BienPosesionNegociadaUseCaseImpl;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.cliente.domain.Cliente;
import com.haya.alaska.cliente.infrastructure.repository.ClienteRepository;
import com.haya.alaska.cliente_cartera.domain.ClienteCartera;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.dato_contacto.domain.DatoContacto;
import com.haya.alaska.dato_contacto.infrastructure.repository.DatoContactoRepository;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import com.haya.alaska.estado_evento.infrastructure.repository.EstadoEventoRepository;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estado_propuesta.infrastructure.repository.EstadoPropuestaRepository;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.controller.dto.EventoInputDTO;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.importancia_evento.domain.ImportanciaEvento;
import com.haya.alaska.importancia_evento.infrastructure.repository.ImportanciaEventoRepository;
import com.haya.alaska.integraciones.pbc.infrastructure.soap.SoapClient;
import com.haya.alaska.integraciones.portal_deudor.client.PortalDeudorFeignClient;
import com.haya.alaska.integraciones.portal_deudor.infrastructure.util.PortalDeudorUtil;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.nivel_evento.domain.NivelEvento;
import com.haya.alaska.nivel_evento.infrastructure.repository.NivelEventoRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.infrastructure.repository.PerfilRepository;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.propuesta_bien.domain.PropuestaBien;
import com.haya.alaska.resolucion_propuesta.infrastructure.repository.ResolucionPropuestaRepository;
import com.haya.alaska.resultado_evento.domain.ResultadoEvento;
import com.haya.alaska.resultado_evento.infrastructure.repository.ResultadoEventoRepository;
import com.haya.alaska.sancion_propuesta.infrastructure.repository.SancionPropuestaRepository;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.subtipo_propuesta.domain.SubtipoPropuesta;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.haya.alaska.workflow_propuesta.domain.WorkflowElevacionPropuesta;
import com.haya.alaska.workflow_propuesta.infrastructure.repository.WorkflowPropuestaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class WorkflowUtil {
  @Autowired SubtipoEventoRepository subtipoEventoRepository;
  @Autowired EstadoEventoRepository estadoEventoRepository;
  @Autowired NivelEventoRepository nivelEventoRepository;
  @Autowired ImportanciaEventoRepository importanciaEventoRepository;
  @Autowired ResultadoEventoRepository resultadoEventoRepository;
  @Autowired WorkflowPropuestaRepository workflowPropuestaRepository;
  @Autowired UsuarioRepository usuarioRepository;
  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired ContratoRepository contratoRepository;
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired DatoContactoRepository datoContactoRepository;
  @Autowired ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired PerfilRepository perfilRepository;
  @Autowired PropuestaRepository propuestaRepository;
  @Autowired EstadoPropuestaRepository estadoPropuestaRepository;
  @Autowired SancionPropuestaRepository sancionPropuestaRepository;
  @Autowired ResolucionPropuestaRepository resolucionPropuestaRepository;
  @Autowired BienPosesionNegociadaUseCaseImpl bPNUseCase;
  @Autowired ClienteRepository clienteRepository;
  @Autowired SoapClient soapClient;
  @Autowired PortalDeudorFeignClient portalDeudorFeignClient;

  // Workflow Propuesta
  public EventoInputDTO workflowPropuesta(Evento eventoInicial, Boolean gestor)
      throws NotFoundException, RequiredValueException {
    Evento evento = new Evento();
    BeanUtils.copyProperties(eventoInicial, evento);
    EventoInputDTO newInput = new EventoInputDTO();
    String[] ingnoredProperties = {"id", "revisado"};
    BeanUtils.copyProperties(evento, newInput, ingnoredProperties);
    Propuesta propuesta =
        propuestaRepository
            .findById(evento.getIdNivel())
            .orElseThrow(() -> new NotFoundException("Propuesta", evento.getIdNivel()));
    newInput.setCartera(evento.getCartera().getId());
    ResultadoEvento original = evento.getResultado();
    if (propuesta.getExpediente() != null && getEspecial(evento) && !gestor) {
      original =
          resultadoEventoRepository
              .findByCodigo("AU_CO")
              .orElseThrow(() -> new NotFoundException("Resultado Evento", "código", "'AU_CO'"));
    }
    List<WorkflowElevacionPropuesta> wepList =
        workflowPropuestaRepository.findAllByEventoOrigenIdAndResultadoEventoIdAndCarteraId(
            evento.getSubtipoEvento().getId(), original.getId(), evento.getCartera().getId());
    if (evento.getSubtipoEvento().getCodigo().equals("REV09")
        && evento.getResultado().getCodigo().equals("COMP")) {
      SubtipoEvento newS = evento.getEventoPadre().getSubtipoEvento();
      Usuario newDest = evento.getEmisor();
      EstadoPropuesta newEstado =
          estadoPropuestaRepository
              .findByCodigo("PFI")
              .orElseThrow(() -> new NotFoundException("ResultadoEvento", "Código", "PFI"));
      createWithDestinatario(newInput, evento, newS, newDest, newEstado, gestor);
      return newInput;
    } else if (wepList.size() != 0) {
      // Pillamos el subtipoevento de la lista
      WorkflowElevacionPropuesta wep = wepList.get(0);
      wep = comprovarWorkflow(wep, evento);
      SubtipoEvento newSubtipo = wep.getEventoDestino();
      // Seleccionamos el limite destinatario del subtipo del destino
      // Aquí metodo para comprobar el workflow si está activo o no
      if (this.getFirmaToGestor(evento, evento.getResultado())) {
        SubtipoEvento newS =
            subtipoEventoRepository.findByActivoIsTrueAndCodigoAndCarteraId(
                "REV09", wep.getCartera().getId());
        Usuario newDest = propuesta.getExpediente().getGestor();
        if (newDest == null)
          throw new NotFoundException(
              "El expediente de id: "
                  + (propuesta.getExpediente().getIdConcatenado() != null
                      ? propuesta.getExpediente().getIdConcatenado()
                      : propuesta.getExpediente().getId() + " (no tiene concatenado)")
                  + " no tiene gestor");
        EstadoPropuesta newEstado =
            estadoPropuestaRepository
                .findByCodigo("PGE")
                .orElseThrow(() -> new NotFoundException("EstadoPropuesta", "Codigo", "PGE"));
        createWithDestinatario(newInput, evento, newS, newDest, newEstado, gestor);
        return newInput;
      } else if (newSubtipo != null) {
        Perfil perfilDestinatario = newSubtipo.getLimiteDestinatario();

        SubtipoPropuesta subtipoPropuesta = propuesta.getSubtipoPropuesta();

        Usuario dest = null;
        Expediente ex = null;
        ExpedientePosesionNegociada exPN = null;
        if (propuesta.getExpediente() != null) ex = propuesta.getExpediente();
        else exPN = propuesta.getExpedientePosesionNegociada();

        // Obtenemos el destinatario
        if (perfilDestinatario == null) {
          Locale loc = LocaleContextHolder.getLocale();
          if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
            throw new NotFoundException(
                "The subtype: " + newSubtipo.getValor() + " must have a recipient profile.");
          else
            throw new NotFoundException(
                "El subtipo: " + newSubtipo.getValor() + " debe tener un perfil destinatario.");
        } else if (wep.getEstadoPropuesta() != null && wep.getEstadoPropuesta().getCodigo().equals("PFI")) {
          if (subtipoPropuesta.getFormalizacion() != null && subtipoPropuesta.getFormalizacion() && !propuesta.getTipoPropuesta().getCodigo().equals("DP")) {
            if (ex != null) {
              dest = ex.getUsuarioByPerfil(Perfil.GESTOR_FORMALIZACION);
              if (propuesta.isFormalizacion() && dest == null){
                Cartera c = ex.getCartera();
                dest = c.getResponsableFormalizacion();
              }
              if (dest == null)
                throw new NotFoundException(
                    "Gestor formalización", "Id expediente", ex.getIdConcatenado());
            } else if (exPN != null) {
              dest = exPN.getUsuarioByPerfil(perfilDestinatario.getNombre());
              if (dest == null)
                throw new NotFoundException(
                    "Gestor formalización", "Id expediente", exPN.getIdConcatenado());
            } else {
              Locale loc = LocaleContextHolder.getLocale();
              if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                throw new NotFoundException(
                    "The proposal of id: "
                        + propuesta.getId()
                        + " is not associated with any file.");
              else
                throw new NotFoundException(
                    "La propuesta de id: "
                        + propuesta.getId()
                        + " no está asociada a ningun expediente.");
            }
          } else {
            Perfil p = newSubtipo.getLimiteDestinatario();
            if (ex != null) {
              dest = ex.getUsuarioByPerfil(p.getNombre());
              if (dest == null){
                p = getPerfilSupervisor(evento.getCartera());
                dest = ex.getUsuarioByPerfil(p.getNombre());
              }
              if (dest == null) {
                Locale loc = LocaleContextHolder.getLocale();
                if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                  throw new NotFoundException(
                      "No user with profile found "
                          + p.getNombre()
                          + " for the record "
                          + (ex.getIdConcatenado() != null ? ex.getIdConcatenado() : ex.getId()));
                else
                  throw new NotFoundException(
                      "No se ha encontrado un usuario con perfil "
                          + p.getNombre()
                          + " para el expediente "
                          + (ex.getIdConcatenado() != null ? ex.getIdConcatenado() : ex.getId()));
              }
            } else if (exPN != null) {
              dest = exPN.getUsuarioByPerfil(p.getNombre());
              if (dest == null && !propuesta.getTipoPropuesta().getCodigo().equals("DP")){
                p = getPerfilSupervisor(evento.getCartera());
                dest = exPN.getUsuarioByPerfil(p.getNombre());
              }
              if (dest == null) {
                Locale loc = LocaleContextHolder.getLocale();
                if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                  throw new NotFoundException(
                      "No user with profile found "
                          + p.getNombre()
                          + " for the record "
                          + (exPN.getIdConcatenado() != null
                              ? exPN.getIdConcatenado()
                              : exPN.getId()));
                else
                  throw new NotFoundException(
                      "No se ha encontrado un usuario con perfil "
                          + p.getNombre()
                          + " para el expediente "
                          + (exPN.getIdConcatenado() != null
                              ? exPN.getIdConcatenado()
                              : exPN.getId()));
              }
            } else {
              Locale loc = LocaleContextHolder.getLocale();
              if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
                throw new NotFoundException(
                    "The proposal of id: "
                        + propuesta.getId()
                        + " is not associated with any file.");
              else
                throw new NotFoundException(
                    "La propuesta de id: "
                        + propuesta.getId()
                        + " no está asociada a ningun expediente.");
            }
          }
        } else {
          if (ex != null){
            dest = userByPerfil(propuesta.getExpediente().getId(), perfilDestinatario.getId());
            if (propuesta.isFormalizacion() && perfilDestinatario.getNombre().equals(Perfil.GESTOR_FORMALIZACION) && dest == null){
              Cartera c = ex.getCartera();
              dest = c.getResponsableFormalizacion();
            }
          }
          else if (exPN != null)
            dest =
                userByPerfilPN(
                    propuesta.getExpedientePosesionNegociada().getId(), perfilDestinatario.getId());
          else {
            Locale loc = LocaleContextHolder.getLocale();
            if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
              throw new NotFoundException(
                  "The proposal of id: " + propuesta.getId() + " is not associated with any file.");
            else
              throw new NotFoundException(
                  "La propuesta de id: "
                      + propuesta.getId()
                      + " no está asociada a ningun expediente.");
          }
        }

        // Si el evento origen y el subtipo y no hay formzalizacion y no hay pbc
        // Si el codigo es rev07 busco dos subtipos rev06 y rev10
        if (evento.getSubtipoEvento().getCodigo().equals("REV07")
            && newSubtipo.getCodigo().equals("REV06")) {
          // Pasamos el evento y la actualizamos

          EstadoPropuesta estadoPropuesta = wep.getEstadoPropuesta();
          if (subtipoPropuesta.getPbc() != null && subtipoPropuesta.getPbc()) {
            estadoPropuesta = estadoPropuestaRepository.findByCodigo("PDFP").orElse(null);
          }

          createWithDestinatario(newInput, evento, newSubtipo, dest, estadoPropuesta, gestor);
          return newInput;
          // Terminada formalización no se crea evento
        } else {
          // Busca usuario en función de si es Pn o no;

          // Si es null el userDestinatario se crea
          if (dest != null) {
            createWithDestinatario(
                newInput, evento, newSubtipo, dest, wep.getEstadoPropuesta(), gestor);
            return newInput;
          } else {
            Locale loc = LocaleContextHolder.getLocale();
            if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
              throw new NotFoundException(
                  "A user assigned to the file of the proposal with profile has not been found: "
                      + perfilDestinatario.getNombre());
            else
              throw new NotFoundException(
                  "No se ha encontrado un usuario asignado al expediente de la propuesta con perfil: "
                      + perfilDestinatario.getNombre());
          }
        }
      } else {
        if (wep.getEventoOrigen()
            .getCodigo()
            .equals("REV09")) { // Subtipo 'Revision Gestor' //Suelen no tener destinatario
          EstadoPropuesta estadoPropuesta = null;
          // Si el resultado
          switch (wep.getResultadoEvento().getCodigo()) {
            case "ACEP":
              SubtipoEvento seG = evento.getEventoPadre().getSubtipoEvento();
              ResultadoEvento reG = resultadoEventoRepository.findByCodigo("AUTO").orElse(null);
              evento.setSubtipoEvento(seG);
              evento.setResultado(reG);
              return workflowPropuesta(evento, true);
            case "DESI":
              estadoPropuesta = estadoPropuestaRepository.findByCodigo("DES").orElse(null);
              updatearPropuesta(evento, estadoPropuesta, null, gestor);
              break;
            case "RECO":
              SubtipoEvento newS =
                  subtipoEventoRepository.findByActivoIsTrueAndCodigoAndCarteraId(
                      "REV05", wep.getCartera().getId());
              createWithDestinatario(
                  newInput, evento, newS, evento.getEventoPadre().getEmisor(), null, gestor);
              return newInput;
          }
        } else {
          EstadoPropuesta estadoPropuesta = null; // Si esta formalizada,negado o desistida
          switch (wep.getResultadoEvento().getCodigo()) {
            default:
              estadoPropuesta = wep.getEstadoPropuesta();
              updatearPropuesta(evento, estadoPropuesta, null, gestor);
          }
        }
      }
    }
    return null;
  }

  public void createWithDestinatario(
      EventoInputDTO newInput,
      Evento evento,
      SubtipoEvento newSubtipo,
      Usuario userDestinatario,
      EstadoPropuesta estadoPropuesta,
      Boolean gestor)
      throws NotFoundException {
    // Creación del evento
    newInput.setIdEventoPadre(evento.getId());
    newInput.setSubtipoEvento(newSubtipo.getId());
    TipoEvento newTipo = newSubtipo.getTipoEvento();
    newInput.setTipoEvento(newTipo.getId());
    ClaseEvento claseEvento = newTipo.getClaseEvento();
    newInput.setClaseEvento(claseEvento.getId());
    newInput.setImportancia(3); // Alta importancia
    newInput.setPeriodicidad(
        evento.getPeriodicidad() != null ? evento.getPeriodicidad().getId() : null);
    // Revisamos el estado evento pendiente
    EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("PEN").orElse(null);
    if (estadoEvento == null) throw new NotFoundException("estado evento", "código", "PEN");
    newInput.setEstado(estadoEvento.getId());
    newInput.setResultado(null);
    newInput.setComentariosDestinatario("Flujo Propuestas");

    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.YEAR, 100);
    newInput.setFechaLimite(c.getTime());

    newInput.setNivel(6); // Nivel de propuesta
    newInput.setNivelId(evento.getIdNivel());
    newInput.setFechaCreacion(new Date());
    newInput.setFechaAlerta(new Date());

    updatearPropuesta(evento, estadoPropuesta, newInput, gestor);

    newInput.setDestinatario(userDestinatario.getId());
  }

  private void updatearPropuesta(
      Evento evento, EstadoPropuesta estadoPropuesta, EventoInputDTO nuevo, Boolean gestor)
      throws NotFoundException {
    Propuesta propuesta =
        propuestaRepository
            .findById(evento.getIdNivel())
            .orElseThrow(() -> new NotFoundException("Propuesta", evento.getIdNivel()));

    Date hoy = new Date();

    if (propuesta.getEstadoPropuesta() != null
        && propuesta.getEstadoPropuesta().getCodigo().equals("EST"))
      propuesta.setFechaElevacion(hoy);

    if (estadoPropuesta != null) {
      propuesta.setEstadoPropuesta(estadoPropuesta);
      if (estadoPropuesta.getCodigo().equals("CER")
          && propuesta.getExpedientePosesionNegociada() != null) {
        bPNUseCase.tramitarBien(
            propuesta.getBienes().stream()
                .map(PropuestaBien::getBien)
                .collect(Collectors.toList()));
      }
    } else {
      if (evento.getSubtipoEvento().getCodigo().equals("REV09")) {
        ResultadoEvento res = resultadoEventoRepository.findByCodigo("AUTO").orElse(null);
        SubtipoEvento subPadre = evento.getEventoPadre().getSubtipoEvento();
        if (subPadre.getCodigo().equals("REV05"))
          res = resultadoEventoRepository.findByCodigo("ELEV").orElse(null);
        List<WorkflowElevacionPropuesta> wfs =
            workflowPropuestaRepository.findAllByEventoDestinoIdAndResultadoEventoIdAndCarteraId(
                subPadre.getId(), res.getId(), subPadre.getCartera().getId());
        propuesta.setEstadoPropuesta(wfs.get(0).getEstadoPropuesta());
      }
    }

    if (nuevo != null) {
      SubtipoEvento subNuevo =
          subtipoEventoRepository.findById(nuevo.getSubtipoEvento()).orElse(null);
      if (subNuevo.getCodigo().equals("REV06") /*+PBC*/) { // Control de +PBC
        if (propuesta.getSubtipoPropuesta().getPbc()) {
          EstadoPropuesta estado = estadoPropuestaRepository.findByCodigo("PDFP").orElse(null);
          propuesta.setEstadoPropuesta(estado);
        } else {
          EstadoPropuesta estado = estadoPropuestaRepository.findByCodigo("PDF").orElse(null);
          propuesta.setEstadoPropuesta(estado);
        }
      }
    }

    // gestor es un boolean que solo es true cuando el evento viene de Revisión gestor
    if (!gestor
        && evento.getSubtipoEvento() != null
        && evento.getSubtipoEvento().getCodigo().equals("REV07")) {
      switch (evento.getResultado().getCodigo()) {
        case "AUTO":
          propuesta.setSancionPropuesta(
              sancionPropuestaRepository.findByCodigo("AUT").orElse(null));
          propuesta.setFechaSancion(hoy);
          break;
        case "AU_CO":
          propuesta.setSancionPropuesta(sancionPropuestaRepository.findByCodigo("AC").orElse(null));
          propuesta.setFechaSancion(hoy);
          break;
        case "DENE":
          propuesta.setSancionPropuesta(
              sancionPropuestaRepository.findByCodigo("DEN").orElse(null));
          propuesta.setFechaSancion(hoy);
          break;
        case "DEVU":
          propuesta.setSancionPropuesta(
              sancionPropuestaRepository.findByCodigo("DEV").orElse(null));
          propuesta.setFechaSancion(hoy);
          break;
      }
    }

    if (evento.getSubtipoEvento().getCodigo().equals("REV10")) {
      switch (evento.getResultado().getCodigo()) {
        case "FORM":
          propuesta.setResolucionPropuesta(
              resolucionPropuestaRepository.findByCodigo("F").orElse(null));
          propuesta.setFechaResolucion(hoy);
          break;
        case "RECH":
          propuesta.setResolucionPropuesta(
              resolucionPropuestaRepository.findByCodigo("R").orElse(null));
          // propuesta.setFechaResolucion(hoy);
          break;
        case "INCI":
          propuesta.setResolucionPropuesta(
              resolucionPropuestaRepository.findByCodigo("I").orElse(null));
          // propuesta.setFechaResolucion(hoy);
          break;
        case "PARA":
          propuesta.setResolucionPropuesta(
              resolucionPropuestaRepository.findByCodigo("P").orElse(null));
          // propuesta.setFechaResolucion(hoy);
          break;
        case "INCU":
          propuesta.setResolucionPropuesta(
              resolucionPropuestaRepository.findByCodigo("IN").orElse(null));
          // propuesta.setFechaResolucion(hoy);
          break;
      }
    }

    // Se envia la informacion de la propuesta a PBC
    if (Objects.nonNull(propuesta.getExpediente())
        && ((Objects.nonNull(propuesta.getExpediente().getCartera().getIdCarga())
                && propuesta.getExpediente().getCartera().getIdCarga().equals("5202"))
            || propuesta.getExpediente().getCartera().getNombre().equals("SAREB"))
        && propuesta.getSubtipoPropuesta().getPbc()) {
      if (propuesta.getEstadoPropuesta().getCodigo().equals("PCL")
          || (Objects.nonNull(propuesta.getSancionPropuesta())
              && (propuesta.getSancionPropuesta().getCodigo().equals("AUT")
                  || propuesta.getSancionPropuesta().getCodigo().equals("AC")))
          || propuesta.getEstadoPropuesta().getCodigo().equals("PFI")
          || propuesta.getEstadoPropuesta().getCodigo().equals("FOR")
          || propuesta.getEstadoPropuesta().getCodigo().equals("DEN")
          || propuesta.getEstadoPropuesta().getCodigo().equals("DES")
          || propuesta.getEstadoPropuesta().getCodigo().equals("ANU")
          || propuesta.getEstadoPropuesta().getCodigo().equals("INCU")) {
        soapClient.notificarPBC(propuesta);
      }
    }

    propuestaRepository.save(propuesta);
  }

  private WorkflowElevacionPropuesta comprovarWorkflow(WorkflowElevacionPropuesta wep, Evento e) {
    if (wep.getActivo()) return wep;
    if (wep.getEventoDestino() == null) return wep;
    List<WorkflowElevacionPropuesta> wepList =
        workflowPropuestaRepository.findAllByEventoOrigenIdAndResultadoEventoIdAndCarteraId(
            wep.getEventoDestino().getId(),
            wep.getResultadoEvento().getId(),
            wep.getCartera().getId());

    ResultadoEvento re = null;
    SubtipoEvento se = null;

    if (wepList.size() != 0) {
      WorkflowElevacionPropuesta newWep = wepList.get(0);
      return comprovarWorkflow(newWep, e);
    } else if (wep.getEventoDestino().getCodigo().equals("REV09")) {
      if (wep.getResultadoEvento().getCodigo().equals("AU_CO")) {
        re = resultadoEventoRepository.findByCodigo("ACEP").orElse(null);
        se =
            subtipoEventoRepository
                .findByCodigoAndCarteraId("REV09", wep.getCartera().getId())
                .orElse(null);
        List<WorkflowElevacionPropuesta> wepListComp =
            workflowPropuestaRepository.findAllByEventoOrigenIdAndResultadoEventoIdAndCarteraId(
                se.getId(), re.getId(), wep.getCartera().getId());
        e.setEventoPadre(e);
        return wepListComp.get(0);
      } else /* if (wep.getResultadoEvento().getCodigo().equals("DEVU"))*/ {
        re = resultadoEventoRepository.findByCodigo("RECO").orElse(null);
        se = subtipoEventoRepository.findByCodigoAndCarteraId("REV09", wep.getId()).orElse(null);
        List<WorkflowElevacionPropuesta> wepListComp =
            workflowPropuestaRepository.findAllByEventoOrigenIdAndResultadoEventoIdAndCarteraId(
                se.getId(), re.getId(), wep.getCartera().getId());
        e.setEventoPadre(e);
        return wepListComp.get(0);
      }
    } else {
      switch (wep.getResultadoEvento().getCodigo()) {
        case "PARA":
          // Esto solo deberia pasar si se cumple "getFirmaToGestor(e, e.getResultadoEvento())"
          return wep;
        case "AUTO":
          re = resultadoEventoRepository.findByCodigo("COMP").orElse(null);
          se =
              subtipoEventoRepository
                  .findByCodigoAndCarteraId("REV06", wep.getCartera().getId())
                  .orElse(null);
          List<WorkflowElevacionPropuesta> wepListAcep =
              workflowPropuestaRepository.findAllByEventoOrigenIdAndResultadoEventoIdAndCarteraId(
                  se.getId(), re.getId(), wep.getCartera().getId());
          return comprovarWorkflow(wepListAcep.get(0), e);
        case "ELEV":
          re = resultadoEventoRepository.findByCodigo("AUTO").orElse(null);
          se =
              subtipoEventoRepository
                  .findByCodigoAndCarteraId("REV05", wep.getCartera().getId())
                  .orElse(null);
          List<WorkflowElevacionPropuesta> wepListElev =
              workflowPropuestaRepository.findAllByEventoOrigenIdAndResultadoEventoIdAndCarteraId(
                  se.getId(), re.getId(), wep.getCartera().getId());
          return comprovarWorkflow(wepListElev.get(0), e);
        case "COMP":
          re = resultadoEventoRepository.findByCodigo("FORM").orElse(null);
          se =
              subtipoEventoRepository
                  .findByCodigoAndCarteraId("REV10", wep.getCartera().getId())
                  .orElse(null);
          List<WorkflowElevacionPropuesta> wepListComp =
              workflowPropuestaRepository.findAllByEventoOrigenIdAndResultadoEventoIdAndCarteraId(
                  se.getId(), re.getId(), wep.getCartera().getId());
          return wepListComp.get(0);
      }
    }
    return null;
  }

  public Boolean getFirmaToGestor(Evento e, ResultadoEvento re) throws NotFoundException {
    Propuesta p =
        propuestaRepository
            .findById(e.getIdNivel())
            .orElseThrow(() -> new NotFoundException("Propuesta", e.getIdNivel()));
    return p.getExpediente() != null
        && e.getSubtipoEvento().getCodigo().equals("REV10")
        && (re.getCodigo().equals("PARA") || re.getCodigo().equals("INCI"));
  }

  public Boolean getEspecial(Evento evento) {
    return (getCarterasCajamar().contains(evento.getCartera())
        && getEventosWF().contains(evento.getSubtipoEvento().getCodigo())
        && evento.getResultado().getCodigo().equals("AUTO"));
  }

  public List<Cartera> getCarterasCajamar() {
    Cliente cl = clienteRepository.findByNombre("CAJAMAR");
    List<Cartera> carteras =
        cl.getCarteras().stream()
            .map(ClienteCartera::getCartera)
            .distinct()
            .collect(Collectors.toList());
    return carteras;
  }

  private List<String> getEventosWF() {
    List<String> wf = new ArrayList<>();

    // wf.add("REV04");
    // wf.add("REV05");
    wf.add("REV07");

    return wf;
  }

  public Usuario userByPerfil(Integer idExpediente, Integer idPerfil) throws NotFoundException {
    Expediente expediente =
        expedienteRepository
            .findById(idExpediente)
            .orElseThrow(() -> new NotFoundException("expediente", idExpediente));
    Perfil perfil =
        perfilRepository
            .findById(idPerfil)
            .orElseThrow(() -> new NotFoundException("Perfil", idPerfil));
    Usuario user = expediente.getUsuarioByPerfil(perfil.getNombre());
    return user;
  }

  public Usuario userByPerfilPN(Integer idExpediente, Integer idPerfil) throws NotFoundException {
    ExpedientePosesionNegociada expediente =
        expedientePosesionNegociadaRepository
            .findById(idExpediente)
            .orElseThrow(() -> new NotFoundException("expedientePN", idExpediente));
    Perfil perfil =
        perfilRepository
            .findById(idPerfil)
            .orElseThrow(() -> new NotFoundException("Perfil", idPerfil));
    Usuario user = expediente.getUsuarioByPerfil(perfil.getNombre());
    return user;
  }

  private Perfil getPerfilSupervisor(Cartera cartera) throws NotFoundException {
    EstadoPropuesta ep =
        estadoPropuestaRepository
            .findByCodigo("PSU")
            .orElseThrow(() -> new NotFoundException("EstadoPropuesta", "código", "'PSU'"));
    List<WorkflowElevacionPropuesta> wepNoForm =
        workflowPropuestaRepository.findAllByCarteraIdAndEstadoPropuestaId(
            cartera.getId(), ep.getId());
    if (wepNoForm.isEmpty()) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException(
            "The portfolio "
                + cartera.getNombre()
                + " does not have workflows that lead to the Proposal Status 'Pending Supervisor'");
      else
        throw new NotFoundException(
            "La cartera "
                + cartera.getNombre()
                + " no tiene workflows que lleven al EstadoPropuesta 'Pendiente Supervisor'");
    }
    SubtipoEvento dest = wepNoForm.get(0).getEventoDestino();
    Perfil perfil = dest.getLimiteDestinatario();
    if (perfil == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException(
            "The subtypeEvent: "
                + dest.getValor()
                + " from the portfolio "
                + cartera.getNombre()
                + " does not have a profile assigned for the recipient.");
      else
        throw new NotFoundException(
            "El subtipoEvento: "
                + dest.getValor()
                + " de la cartera "
                + cartera.getNombre()
                + " no tiene un perfil asignado para el destinatario.");
    }
    return perfil;
  }

  // Workflow Judicial
  public EventoInputDTO wfJudicial(Evento e, Usuario logged)
      throws NotFoundException, RequiredValueException {
    Boolean b = true;
    EventoInputDTO newIn = new EventoInputDTO();
    if (e.getResultado() != null
        && e.getResultado().getCodigo().equals("OK")
        && e.getSubtipoEvento().getCodigo().equals("JU_PR")) {
      newIn = eventoWFJudicial(e, e.getIdNivel());
    }
    if (newIn == null) {
      Contrato contrato = contratoRepository.findById(e.getIdNivel()).orElse(null);
      Expediente ex = contrato.getExpediente();
      Cartera ca = ex.getCartera();
      SubtipoEvento se =
          subtipoEventoRepository
              .findByCodigoAndCarteraId("REV03", ca.getId())
              .orElseThrow(() -> new NotFoundException("subtipoEvento", "código", "'REV03'"));
      ImportanciaEvento ie =
          importanciaEventoRepository
              .findByCodigo("ALT")
              .orElseThrow(() -> new NotFoundException("importanciaEvento", "código", "'ALT'"));
      NivelEvento ne =
          nivelEventoRepository
              .findByCodigo("JUD")
              .orElseThrow(() -> new NotFoundException("Nivel Evento", "código", "'JUD'"));
      Usuario creador = usuarioRepository.findById(logged.getId()).orElse(null);
      Usuario destinatario = usuarioRepository.findByEmail("recovery@haya.es").orElse(null);
      ResultadoEvento resultadoEvento =
          resultadoEventoRepository
              .findByCodigo("AUTO")
              .orElseThrow(() -> new NotFoundException("Resultado Evento", "código", "'AUTO'"));

      TipoEvento te = se.getTipoEvento();
      ClaseEvento ce = te.getClaseEvento();
      newIn.setTitulo(e.getTitulo());
      newIn.setComentariosDestinatario(e.getComentariosDestinatario());
      newIn.setIdEventoPadre(e.getId());
      newIn.setCartera(ca.getId());

      newIn.setFechaCreacion(new Date());
      newIn.setFechaAlerta(new Date());
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.DATE, 1);
      newIn.setFechaLimite(calendar.getTime());

      newIn.setClaseEvento(ce.getId());
      newIn.setTipoEvento(te.getId());
      newIn.setSubtipoEvento(se.getId());

      newIn.setDestinatario(destinatario.getId());
      newIn.setImportancia(ie.getId());
      newIn.setAutogenerado(true);

      newIn.setResultado(resultadoEvento.getId());
      newIn.setNivel(ne.getId());
      newIn.setNivelId(contrato.getId());
      newIn.setIdRecovery(e.getIdRecovery());
      newIn.setIdExpediente(e.getIdExpediente());

      return newIn;
    }
    return null;
  }

  private EventoInputDTO eventoWFJudicial(Evento padre, Integer idContrato)
      throws NotFoundException, RequiredValueException {
    Contrato contrato = contratoRepository.findById(idContrato).orElse(null);
    Expediente e = contrato.getExpediente();
    Cartera ca = e.getCartera();
    Calendar calendar = Calendar.getInstance();
    EventoInputDTO newIn = new EventoInputDTO();

    newIn.setTitulo(padre.getTitulo());
    newIn.setComentariosDestinatario(padre.getComentariosDestinatario());
    newIn.setIdEventoPadre(padre.getId());
    newIn.setCartera(ca.getId());
    newIn.setFechaCreacion(calendar.getTime());
    newIn.setFechaAlerta(calendar.getTime());
    calendar.add(Calendar.DATE, 1);
    newIn.setFechaLimite(calendar.getTime());
    newIn.setClaseEvento(padre.getClaseEvento().getId());
    newIn.setTipoEvento(padre.getTipoEvento().getId());
    newIn.setSubtipoEvento(padre.getSubtipoEvento().getId());
    newIn.setImportancia(padre.getImportancia().getId());
    newIn.setAutogenerado(true);
    newIn.setNivel(padre.getNivel().getId());
    newIn.setNivelId(idContrato);
    newIn.setIdExpediente(e.getId());

    Usuario dest;
    if (padre.getDestinatario().getPerfil().getNombre().equals("Supervisor"))
      dest = e.getUsuarioByPerfil("Responsable de cartera");
    else if (padre.getDestinatario().getPerfil().getNombre().equals("Responsable de cartera"))
      return null;
    else {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException(
            "The event of judicialization of the procedure is not assigned neither to a 'Supervisor', nor to a 'Head of portfolio'");
      else
        throw new NotFoundException(
            "El evento de judicialización del procedimiento no está asignado ni a un 'Supervisor', ni a un 'Responsable de cartera'");
    }
    if (dest == null)
      throw new NotFoundException("Responsable de cartera", "expediente", e.getId());

    newIn.setDestinatario(dest.getId());

    return newIn;
  }

  // Workflow Clientes
  public void llamarContrasena(Evento e) throws NotFoundException {
    // Genero password
    String password = String.valueOf(PortalDeudorUtil.generadorPassword(8, 12, 2, 2, 2));
    Integer intervinienteId = e.getIdNivel(); // este es el intervinienteId*/
    Interviniente interviniente =
        intervinienteRepository
            .findById(intervinienteId)
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "No se ha encontrado el interviniente con id " + intervinienteId));
    String username = e.getTitulo();
    Integer idGestor = e.getDestinatario().getId();
    log.warn("01 Obtenemos id Destinatario y comenzamos proceso de guardar Contraseña en fenix");
    String respuestaGuardado =
        portalDeudorFeignClient.guardarContrasena(username, password, intervinienteId, idGestor);
    log.warn("02 Finaliza proceso de guardado de contraseña en fenix");

    // Obtenemos el datoContacto 1 que pasará a ser el secundario el que introducimos
    // en portal deudor durante el registro pasará a ser el principal
    String[] parts = username.split(": ");
    String nombreObt = parts[1];
    String nombreObt2 = nombreObt.replace(" con el numero de telefono de", "");
    String numeroTelefono = parts[2];

    List<DatoContacto> datoContactoList =
        interviniente.getDatosContacto().stream().collect(Collectors.toList());
    DatoContacto datoContacto =
        datoContactoList.stream()
            .sorted(Comparator.comparing(DatoContacto::getOrden))
            .findFirst()
            .orElse(null);

    DatoContacto dc = new DatoContacto();
    dc.setEmail(nombreObt2);
    dc.setMovil(numeroTelefono);
    dc.setInterviniente(interviniente);
    dc.setOrden(1);
    dc.setFijoValidado(false);
    dc.setEmailValidado(false);
    dc.setMovilValidado(false);
    dc.setOrigen("Portal Deudor");
    dc.setFechaAlta(new Date());
    datoContactoRepository.save(dc);

    datoContacto.setOrden(2);
    datoContactoRepository.save(datoContacto);

    // Crear usuario y controlar si devuelve null //Revisar guardar contraseña
    // TODO: en este método, hay que generar la contraseña, hay que guardarla en el portal deudor, y
    // hay que mandar el correo al interviniente
    // FeignClient para sacar la peticion Creada (Y aquí es donde pasamos el password que se va a
    // meter en portal deudor)
  }
}
