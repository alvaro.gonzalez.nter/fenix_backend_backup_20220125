package com.haya.alaska.evento.infrastructure.repository;

import com.haya.alaska.evento.domain.Evento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EventoRepository extends JpaRepository<Evento, Integer> {
  List<Evento> findAllByFechaLimiteLessThanAndEstadoCodigo(Date limite, String estado);

  List<Evento> findAllByDestinatarioId(Integer idDestinatario);

  List<Evento> findAllByDestinatarioIdAndIdNivelAndNivelId(Integer idDestinatario, Integer idObjeto, Integer idNivel);

  Evento findTopByNivelIdAndClaseEventoIdOrderByFechaCreacionDesc(Integer idNivel, Integer idclase);

  List<Evento> findAllByNivelIdAndIdNivelOrderByFechaCreacionDesc(Integer idNivel, Integer idObject);

  List<Evento> findByIdExpediente(Integer idExpediente);

  List<Evento> findAllByIdNivelAndCarteraId(Integer idNive, Integer idCartera);

  List<Evento> findAllByNivelCodigoAndResultadoIsNullAndIdNivel(String codigo, Integer idNivel);

  List<Evento> findAllByIdNivelAndNivelCodigoAndEstadoCodigo(Integer idObjeto, String codigoNivel, String estado);


  @Query(
    value = "SELECT * FROM MSTR_EVENTO WHERE ID_SUBTIPO_EVENTO = ?1 AND FCH_FECHA_CREACION >= ?2 AND FCH_FECHA_CREACION < ?3"
    , nativeQuery = true)
  List<Evento> findAllBySubtipoEventoIdAndFechaCreacionBetween(Integer subTipoId, String ayer, String hoy);

  @Query(
    value = "SELECT * FROM MSTR_EVENTO WHERE FCH_FECHA_CREACION >= ?1 AND FCH_FECHA_CREACION < ?2"
    , nativeQuery = true)
  List<Evento> findAllByFechaCreacionBetween(String ayer, String hoy);

}
