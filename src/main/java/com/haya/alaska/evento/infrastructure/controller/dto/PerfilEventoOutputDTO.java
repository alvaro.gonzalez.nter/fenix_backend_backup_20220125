package com.haya.alaska.evento.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.perfil.domain.Perfil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.Locale;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PerfilEventoOutputDTO implements Serializable {
    private Integer id;
    private String nombre;
    private CatalogoMinInfoDTO area;

    public PerfilEventoOutputDTO(Perfil perfil){
        this.id = perfil.getId();
      Locale loc = LocaleContextHolder.getLocale();
        this.nombre = perfil.getNombre();
      if (loc.getLanguage().equals("en"))
        this.nombre = perfil.getNombreIngles();
    }
}
