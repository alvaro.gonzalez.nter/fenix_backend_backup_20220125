package com.haya.alaska.evento.infrastructure.controller.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InformeEventoInputDTO {

  private Integer estadoExpediente;
  private Date fechaCreacionExpedienteInicial;
  private Date fechaCreacionExpedienteFinal;
  private Date fechaCierreExpedienteInicial;
  private Date fechaCierreExpedienteFinal;
  private Integer tipoEvento;
  private Date fechaCreacionEventoInicial;
  private Date fechaCreacionEventoFinal;
  private Date fechaLimiteEventoInicial;
  private Date fechaLimiteEventoFinal;

}
