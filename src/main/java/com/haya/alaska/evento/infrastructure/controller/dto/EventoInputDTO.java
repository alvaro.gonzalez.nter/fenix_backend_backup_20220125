package com.haya.alaska.evento.infrastructure.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class EventoInputDTO implements Serializable {
  private Integer idEventoPadre;
  private Integer cartera;
  private String titulo;
  //private String descripcion;
  private String comentariosDestinatario;

  private Date fechaCreacion;
  private Date fechaLimite;
  private Date fechaAlerta;
  private Integer claseEvento;
  private Integer tipoEvento;
  private Integer subtipoEvento;
  private Integer periodicidad;
  private Integer estado;
  private Integer importancia;
  private Integer resultado;

  private Integer destinatario;

  private Boolean programacion;
  private Boolean correo;
  private Boolean autogenerado;
  private Boolean revisado;

  private Integer nivel;
  private Integer nivelId;
  private Long idRecovery;
  private Integer idExpediente;
}
