package com.haya.alaska.evento.infrastructure.controller.dto;

import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SubtipoEventoOutputDTO implements Serializable {
    private Integer id;
    private Integer idCartera;
    private Boolean activo;
    private String codigo;
    private String valor;
    //private String valorIngles;
    private CatalogoMinInfoDTO tipoEvento;
    private PerfilEventoOutputDTO emisor;
    private PerfilEventoOutputDTO destinatario;
    private CatalogoMinInfoDTO importancia;
    private Integer fechaLimite;
    private CatalogoMinInfoDTO periodicidad;
    private String descripcion;
    private Boolean correo;
    private Boolean programacion;
    private List<CatalogoMinInfoDTO> resultados;

    public SubtipoEventoOutputDTO (SubtipoEvento subtipoEvento){
      if (subtipoEvento!=null){
        this.id = subtipoEvento.getId();
        this.activo = subtipoEvento.getActivo()!=null ? subtipoEvento.getActivo():null;
        this.idCartera = subtipoEvento.getCartera() != null ? subtipoEvento.getCartera().getId() : null;
        this.codigo = subtipoEvento.getCodigo();
        String valor = null;
        Locale loc = LocaleContextHolder.getLocale();
        String defaultLocal = loc.getLanguage();
        if (defaultLocal == null || defaultLocal.equals("es")) valor = subtipoEvento.getValor();
        else if (defaultLocal.equals("en")) valor = subtipoEvento.getValorIngles();
        this.valor = valor;
        //this.valorIngles = subtipoEvento.getValorIngles();
        this.tipoEvento = subtipoEvento.getTipoEvento() != null ? new CatalogoMinInfoDTO(subtipoEvento.getTipoEvento()) : null;
        this.emisor = subtipoEvento.getLimiteEmisor() != null ? new PerfilEventoOutputDTO(subtipoEvento.getLimiteEmisor()) : null;
        this.destinatario = subtipoEvento.getLimiteDestinatario() != null ? new PerfilEventoOutputDTO(subtipoEvento.getLimiteDestinatario()) : null;
        this.importancia = subtipoEvento.getImportancia() != null ? new CatalogoMinInfoDTO(subtipoEvento.getImportancia()) : null;
        this.fechaLimite = subtipoEvento.getFechaLimite();
        this.periodicidad = subtipoEvento.getPeriodicidad() != null ? new CatalogoMinInfoDTO(subtipoEvento.getPeriodicidad()) : null;
        this.descripcion = subtipoEvento.getDescripcion();
        this.correo = subtipoEvento.getCorreo();
        this.programacion = subtipoEvento.getProgramacion();
        this.resultados = subtipoEvento.getResultados().stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
    }
      }
}
