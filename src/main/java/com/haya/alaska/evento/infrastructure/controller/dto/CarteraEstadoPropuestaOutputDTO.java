package com.haya.alaska.evento.infrastructure.controller.dto;

import com.haya.alaska.cartera_estado_propuesta.domain.CarteraEstadoPropuesta;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CarteraEstadoPropuestaOutputDTO implements Serializable {

  private Integer id;
  private Boolean activo;
  private CatalogoMinInfoDTO estado;

  public CarteraEstadoPropuestaOutputDTO(CarteraEstadoPropuesta carteraEstadoPropuesta){
    if (carteraEstadoPropuesta!=null){
      this.id=carteraEstadoPropuesta.getId();
      this.activo=carteraEstadoPropuesta.getActivo();
      this.estado=carteraEstadoPropuesta.getEstado()!=null ? new CatalogoMinInfoDTO(carteraEstadoPropuesta.getEstado()):null;
    }
  }
}
