package com.haya.alaska.evento.infrastructure.util;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente_;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada_;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.BusquedaAgendaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.IntervaloFecha;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.domain.Cartera_;
import com.haya.alaska.catalogo.domain.Catalogo_;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.clase_evento.domain.ClaseEvento_;
import com.haya.alaska.clase_evento.infrastructure.repository.ClaseEventoRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import com.haya.alaska.estado_evento.domain.EstadoEvento_;
import com.haya.alaska.estado_evento.infrastructure.repository.EstadoEventoRepository;
import com.haya.alaska.estimacion.domain.Estimacion;
import com.haya.alaska.estimacion.infrastructure.repository.EstimacionRepository;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.domain.Evento_;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.Expediente_;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.nivel_evento.domain.NivelEvento;
import com.haya.alaska.nivel_evento.domain.NivelEvento_;
import com.haya.alaska.nivel_evento.infrastructure.repository.NivelEventoRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.perfil.domain.Perfil_;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.resultado_pbc.domain.ResultadoPBC;
import com.haya.alaska.sancion_propuesta.domain.SancionPropuesta;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento_;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.tipo_evento.domain.TipoEvento_;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.domain.Usuario_;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class EventoUtil {
  @PersistenceContext private EntityManager entityManager;
  @Autowired EventoRepository eventoRepository;
  @Autowired EstimacionRepository estimacionRepository;
  @Autowired ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired UsuarioRepository usuarioRepository;
  @Autowired EstadoEventoRepository estadoEventoRepository;
  @Autowired ClaseEventoRepository claseEventoRepository;
  @Autowired NivelEventoRepository nivelEventoRepository;
  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired ContratoRepository contratoRepository;
  @Autowired ExpedientePosesionNegociadaRepository ePNRepository;
  @Autowired PropuestaRepository propuestaRepository;

  // Filtrados
  public List<Evento> getFilteredEventos(
      Integer loggedUser,
      Integer emisorId,
      Integer destinatarioId,
      Integer nivelId,
      Integer nivelObjectId,
      Date fCreaMin,
      Date fCreaMax,
      Date fAlerMin,
      Date fAlerMax,
      Date fLimiMin,
      Date fLimiMax,
      List<Integer> clase,
      Integer tipo,
      List<Integer> subtipoList,
      Integer cartera,
      List<Integer> estado,
      Integer importancia,
      Integer resultado,
      String titulo,
      Boolean activo,
      Boolean revisado,
      String perfil,
      String orderField,
      String orderDirection,
      Integer page,
      Integer size)
      throws NotFoundException {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Evento> query = cb.createQuery(Evento.class);

    Root<Evento> root = query.from(Evento.class);
    List<Predicate> predicates =
        getPredicates(
            cb,
            root,
            loggedUser,
            emisorId,
            destinatarioId,
            nivelId,
            nivelObjectId,
            fCreaMin,
            fCreaMax,
            fAlerMin,
            fAlerMax,
            fLimiMin,
            fLimiMax,
            clase,
            tipo,
            subtipoList,
            cartera,
            estado,
            importancia,
            resultado,
            titulo,
            activo,
            revisado,
            perfil);

    var rs = query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));

    if (orderField != null) {
      if (orderDirection.toLowerCase().equalsIgnoreCase("desc"))
        rs.orderBy(cb.desc(root.get(orderField)));
      else rs.orderBy(cb.asc(root.get(orderField)));
    }
    Set<ParameterExpression<?>> q = query.getParameters();

    Query queryResult = entityManager.createQuery(query);

    if (page != null && size != null) {
      queryResult.setFirstResult(page * size);
      queryResult.setMaxResults(size);
    }

    List<Evento> listaPropuesta = queryResult.getResultList();

    if(nivelId != null && nivelId.equals(1) && nivelObjectId!=null) {
      Expediente expediente = expedienteRepository.findById(nivelObjectId).orElse(null);
      if(expediente!=null) {
        for(var contrato: expediente.getContratos()) {
          listaPropuesta.addAll(getFilteredEventos(loggedUser,emisorId,destinatarioId,2,contrato.getId(),fCreaMin,fCreaMax,fAlerMin,
            fAlerMax,fLimiMin,fLimiMax,clase,tipo,subtipoList,cartera,estado,importancia,resultado,titulo,activo,revisado,
            perfil,orderField,orderDirection,page,size));
        }
        for(var propuesta: expediente.getPropuestas()) {
          listaPropuesta.addAll(getFilteredEventos(loggedUser,emisorId,destinatarioId,6,propuesta.getId(),fCreaMin,fCreaMax,fAlerMin,
            fAlerMax,fLimiMin,fLimiMax,clase,tipo,subtipoList,cartera,estado,importancia,resultado,titulo,activo,revisado,
            perfil,orderField,orderDirection,page,size));
        }
        for(var estimacion: expediente.getEstimaciones()) {
          listaPropuesta.addAll(getFilteredEventos(loggedUser,emisorId,destinatarioId,9,estimacion.getId(),fCreaMin,fCreaMax,fAlerMin,
            fAlerMax,fLimiMin,fLimiMax,clase,tipo,subtipoList,cartera,estado,importancia,resultado,titulo,activo,revisado,
            perfil,orderField,orderDirection,page,size));
        }
        for(var expedientePN: expediente.getExpedientesPosesionNegociada()) {
          listaPropuesta.addAll(getFilteredEventos(loggedUser,emisorId,destinatarioId,7,expedientePN.getId(),fCreaMin,fCreaMax,fAlerMin,
            fAlerMax,fLimiMin,fLimiMax,clase,tipo,subtipoList,cartera,estado,importancia,resultado,titulo,activo,revisado,
            perfil,orderField,orderDirection,page,size));
        }
      }

    }

    return listaPropuesta;
  }

  public List<Evento> filterListActividad(
      Integer loggedUser,
      BusquedaAgendaDto busquedaAgendaDto,
      Integer nivelId,
      Integer nivelObjectId,
      String orderField,
      String orderDirection)
      throws NotFoundException {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Evento> query = cb.createQuery(Evento.class);

    Root<Evento> root = query.from(Evento.class);
    List<Predicate> predicates = getPredicatesByObject(null, busquedaAgendaDto, cb, root);

    Join<Evento, Usuario> emisorJoin = root.join(Evento_.emisor, JoinType.INNER);
    Join<Evento, Usuario> destinatarioJoin = root.join(Evento_.destinatario, JoinType.INNER);
    Predicate log =
        cb.or(
            cb.like(emisorJoin.get(Usuario_.id).as(String.class), "%" + loggedUser + "%"),
            cb.like(destinatarioJoin.get(Usuario_.id).as(String.class), "%" + loggedUser + "%"));

    if (nivelId == null) {
      if (loggedUser != null) {
        predicates.add(log);
      }
    } else if (nivelObjectId != null) {
      NivelEvento nivelEvento =
          nivelEventoRepository
              .findById(nivelId)
              .orElseThrow(() -> new NotFoundException("NivelEvento", nivelId));

      Predicate pNotCom =
          cb.notLike(root.get(Evento_.claseEvento).get(ClaseEvento_.CODIGO), "%COM%");
      Predicate pCom = cb.like(root.get(Evento_.claseEvento).get(ClaseEvento_.CODIGO), "%COM%");

      switch (nivelEvento.getCodigo()) {
        case "EXP":
          Expediente expediente =
              expedienteRepository
                  .findById(nivelObjectId)
                  .orElseThrow(() -> new NotFoundException("Expediente", nivelObjectId));
          Predicate disE = filterExpedientes(expediente, cb, root, true);
          if (disE != null) {
            Predicate notFinal = cb.and(disE, pNotCom);
            Predicate comFinal = cb.and(log, cb.and(pCom, disE));
            predicates.add(cb.or(notFinal, comFinal));
          }
          break;
        case "CON":
          Contrato contrato =
              contratoRepository
                  .findById(nivelObjectId)
                  .orElseThrow(() -> new NotFoundException("Contrato", nivelId));
          Predicate pCont =
              cb.and(
                  cb.equal(root.get(Evento_.idNivel), contrato.getId()),
                  cb.like(
                      root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
                      "%" + nivelId + "%"));
          List<Predicate> subPredicates = filterContrato(contrato, cb, root);
          subPredicates.add(pCont);
          Predicate disC = cb.disjunction();
          for (Predicate pre : subPredicates) {
            disC.getExpressions().add(pre);
          }

          Predicate notFinalC = cb.and(disC, pNotCom);
          Predicate comFinalC = cb.and(log, cb.and(pCom, disC));
          predicates.add(cb.or(notFinalC, comFinalC));
          break;
        case "EXP_PN":
          ExpedientePosesionNegociada expedientePN =
              ePNRepository
                  .findById(nivelObjectId)
                  .orElseThrow(
                      () -> new NotFoundException("ExpedientePosesionNegociada", nivelObjectId));
          Predicate disEPN = filterExpedientesPN(expedientePN, cb, root, true);
          if (disEPN != null) {

            Predicate notFinal = cb.and(disEPN, pNotCom);
            Predicate comFinal = cb.and(log, cb.and(pCom, disEPN));
            predicates.add(cb.or(notFinal, comFinal));
          }
          break;
        default:
          Predicate ext =
              cb.and(
                  cb.equal(root.get(Evento_.idNivel).as(String.class), nivelObjectId),
                  cb.equal(root.get(Evento_.nivel).get(NivelEvento_.ID), nivelId));
          Predicate notFinal = cb.and(ext, pNotCom);
          Predicate comFinal = cb.and(log, cb.and(pCom, ext));
          predicates.add(cb.or(notFinal, comFinal));
          break;
      }
    }

    var rs =
        query
            .select(root)
            .distinct(true)
            .where(predicates.toArray(new Predicate[predicates.size()]));

    if (orderField != null) {
      if (orderDirection.toLowerCase().equalsIgnoreCase("asc"))
        rs.orderBy(cb.asc(root.get(orderField)));
      else rs.orderBy(cb.desc(root.get(orderField)));
    }

    List<Evento> eventos = entityManager.createQuery(rs).getResultList();


    if(nivelId != null && nivelId.equals(1) && nivelObjectId!=null ){
      Expediente expediente = expedienteRepository.findById(nivelObjectId).orElse(null);
      if(expediente != null){
        for(var contrato: expediente.getContratos()) {
          eventos.addAll(filterListActividad(loggedUser, busquedaAgendaDto, 2, contrato.getId(), orderField, orderDirection));
        }
        for(var propuesta: expediente.getPropuestas()) {
          eventos.addAll(filterListActividad(loggedUser, busquedaAgendaDto, 6, propuesta.getId(), orderField, orderDirection));
        }
        for(var estimacion: expediente.getEstimaciones()) {
          eventos.addAll(filterListActividad(loggedUser, busquedaAgendaDto, 9, estimacion.getId(), orderField, orderDirection));
        }
        for(var expedientePN: expediente.getExpedientesPosesionNegociada()) {
          eventos.addAll(filterListActividad(loggedUser, busquedaAgendaDto, 7, expedientePN.getId(), orderField, orderDirection));
        }
      }
    }


    /*if(orderField.equalsIgnoreCase("fecha")) {
      if(orderDirection.equals("asc"))
        eventos.sort(Comparator.comparing(Evento::getFechaCreacion));
      else
        eventos.sort(Comparator.comparing(Evento::getFechaCreacion).reversed());
    }*/


    return eventos;
  }

  public Long getCantidadEvents(
      Integer loggedUser,
      Integer emisorId,
      Integer destinatarioId,
      Integer nivelId,
      Integer nivelObjectId,
      Date fCreaMin,
      Date fCreaMax,
      Date fAlerMin,
      Date fAlerMax,
      Date fLimiMin,
      Date fLimiMax,
      List<Integer> clase,
      Integer tipo,
      List<Integer> subtipoList,
      Integer cartera,
      List<Integer> estado,
      Integer importancia,
      Integer resultado,
      String titulo,
      Boolean activo,
      Boolean revisado,
      String perfil)
      throws NotFoundException {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Long> cQuery = cb.createQuery(Long.class);

    Root<Evento> root = cQuery.from(Evento.class);

    List<Predicate> predicates =
        getPredicates(
            cb,
            root,
            loggedUser,
            emisorId,
            destinatarioId,
            nivelId,
            nivelObjectId,
            fCreaMin,
            fCreaMax,
            fAlerMin,
            fAlerMax,
            fLimiMin,
            fLimiMax,
            clase,
            tipo,
            subtipoList,
            cartera,
            estado,
            importancia,
            resultado,
            titulo,
            activo,
            revisado,
            perfil);

    cQuery.where(predicates.toArray(new Predicate[predicates.size()]));
    cQuery.select(cb.count(root));

    Long count = entityManager.createQuery(cQuery).getSingleResult();

    if(nivelId != null && nivelId.equals(1) && nivelObjectId!=null ){
      Expediente expediente = expedienteRepository.findById(nivelObjectId).orElse(null);
      if(expediente != null){
        for(var contrato: expediente.getContratos()) {
          count += getFilteredEventos(loggedUser,emisorId,destinatarioId,2,contrato.getId(),fCreaMin,fCreaMax,fAlerMin,
            fAlerMax,fLimiMin,fLimiMax,clase,tipo,subtipoList,cartera,estado,importancia,resultado,titulo,activo,revisado,
            perfil,null,null,null,null).size();
        }
        for(var propuesta: expediente.getPropuestas()) {
          count += getFilteredEventos(loggedUser,emisorId,destinatarioId,6,propuesta.getId(),fCreaMin,fCreaMax,fAlerMin,
            fAlerMax,fLimiMin,fLimiMax,clase,tipo,subtipoList,cartera,estado,importancia,resultado,titulo,activo,revisado,
            perfil,null,null,null,null).size();
        }
        for(var estimacion: expediente.getEstimaciones()) {
          count += getFilteredEventos(loggedUser,emisorId,destinatarioId,9,estimacion.getId(),fCreaMin,fCreaMax,fAlerMin,
            fAlerMax,fLimiMin,fLimiMax,clase,tipo,subtipoList,cartera,estado,importancia,resultado,titulo,activo,revisado,
            perfil,null,null,null,null).size();
        }
        for(var expedientePN: expediente.getExpedientesPosesionNegociada()) {
          count += getFilteredEventos(loggedUser,emisorId,destinatarioId,7,expedientePN.getId(),fCreaMin,fCreaMax,fAlerMin,
            fAlerMax,fLimiMin,fLimiMax,clase,tipo,subtipoList,cartera,estado,importancia,resultado,titulo,activo,revisado,
            perfil,null,null,null,null).size();
        }
      }
    }
    return count;
  }

  private List<Predicate> getPredicates(
      CriteriaBuilder cb,
      Root<Evento> root,
      Integer loggedUser,
      Integer emisorId,
      Integer destinatarioId,
      Integer nivelId,
      Integer nivelObjectId,
      Date fCreaMin,
      Date fCreaMax,
      Date fAlerMin,
      Date fAlerMax,
      Date fLimiMin,
      Date fLimiMax,
      List<Integer> clase,
      Integer tipo,
      List<Integer> subtipoList,
      Integer cartera,
      List<Integer> estado,
      Integer importancia,
      Integer resultado,
      String titulo,
      Boolean activo,
      Boolean revisado,
      String perfil)
      throws NotFoundException {
    List<Predicate> predicates = new ArrayList<>();

    predicates.add(cb.isTrue(root.get(Evento_.ACTIVO)));

    Join<Evento, Usuario> emisorJoin = root.join(Evento_.emisor, JoinType.INNER);
    Join<Evento, Usuario> destinatarioJoin = root.join(Evento_.destinatario, JoinType.INNER);
    Predicate log = cb.isTrue(root.get(Evento_.ACTIVO));
    if (loggedUser != null) {
      log =
          cb.or(
              cb.equal(emisorJoin.get(Usuario_.id).as(String.class), loggedUser),
              cb.equal(destinatarioJoin.get(Usuario_.id).as(String.class), loggedUser));
    }

    if (perfil != null) {
      Subquery<Usuario> subqueryI = cb.createQuery().subquery(Usuario.class);
      List<Predicate> suPredicates = new ArrayList<>();
      if (nivelId == 1) {
        Root<AsignacionExpediente> fromAE = subqueryI.from(AsignacionExpediente.class);
        Join<AsignacionExpediente, Expediente> joinExpe =
            fromAE.join(AsignacionExpediente_.expediente, JoinType.INNER);
        suPredicates.add(cb.equal(joinExpe.get(Expediente_.ID), nivelObjectId));
        subqueryI
            .select(fromAE.get(AsignacionExpediente_.usuario))
            .distinct(true)
            .where(suPredicates.toArray(new Predicate[predicates.size()]));
      } else if (nivelId == 7) {
        Root<AsignacionExpedientePosesionNegociada> fromAE =
            subqueryI.from(AsignacionExpedientePosesionNegociada.class);
        Join<AsignacionExpedientePosesionNegociada, ExpedientePosesionNegociada> joinExpe =
            fromAE.join(AsignacionExpedientePosesionNegociada_.expediente, JoinType.INNER);
        suPredicates.add(cb.equal(joinExpe.get(Expediente_.ID), nivelObjectId));
        subqueryI
            .select(fromAE.get(AsignacionExpedientePosesionNegociada_.usuario))
            .distinct(true)
            .where(suPredicates.toArray(new Predicate[predicates.size()]));
      }
      Predicate p1 = cb.not(root.get(Evento_.destinatario).in(subqueryI));
      predicates.add(p1);
      predicates.add(cb.equal(destinatarioJoin.get(Usuario_.perfil).get(Perfil_.nombre), perfil));
    }

    if (nivelId == null) {
      if (loggedUser != null) {
        predicates.add(log);
      }
    } else if (nivelObjectId != null) {
      NivelEvento nivelEvento =
          nivelEventoRepository
              .findById(nivelId)
              .orElseThrow(() -> new NotFoundException("NivelEvento", nivelId));

      Predicate pNotCom =
          cb.notLike(root.get(Evento_.claseEvento).get(ClaseEvento_.CODIGO), "%COM%");
      Predicate pCom = cb.like(root.get(Evento_.claseEvento).get(ClaseEvento_.CODIGO), "%COM%");

      switch (nivelEvento.getCodigo()) {
        case "EXP":
          Expediente expediente =
              expedienteRepository
                  .findById(nivelObjectId)
                  .orElseThrow(() -> new NotFoundException("Expediente", nivelObjectId));
          Predicate disE = filterExpedientes(expediente, cb, root, true);
          if (disE != null) {
            Predicate notFinal = cb.and(disE, pNotCom);
            Predicate comFinal = cb.and(log, cb.and(pCom, disE));
            predicates.add(cb.or(notFinal, comFinal));
          }
          break;
        case "CON":
          Contrato contrato =
              contratoRepository
                  .findById(nivelObjectId)
                  .orElseThrow(() -> new NotFoundException("Contrato", nivelId));
          Predicate pCont =
              cb.and(
                  cb.equal(root.get(Evento_.idNivel), contrato.getId()),
                  cb.like(
                      root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
                      "%" + nivelId + "%"));
          List<Predicate> subPredicates = filterContrato(contrato, cb, root);
          subPredicates.add(pCont);
          Predicate disC = cb.disjunction();
          for (Predicate pre : subPredicates) {
            disC.getExpressions().add(pre);
          }

          Predicate notFinalC = cb.and(disC, pNotCom);
          Predicate comFinalC = cb.and(log, cb.and(pCom, disC));
          predicates.add(cb.or(notFinalC, comFinalC));
          break;
        case "EXP_PN":
          ExpedientePosesionNegociada expedientePN =
              ePNRepository
                  .findById(nivelObjectId)
                  .orElseThrow(
                      () -> new NotFoundException("ExpedientePosesionNegociada", nivelObjectId));
          Predicate disEPN = filterExpedientesPN(expedientePN, cb, root, true);
          if (disEPN != null) {

            Predicate notFinal = cb.and(disEPN, pNotCom);
            Predicate comFinal = cb.and(log, cb.and(pCom, disEPN));
            predicates.add(cb.or(notFinal, comFinal));
          }
          break;
        default:
          Predicate ext =
              cb.and(
                  cb.equal(root.get(Evento_.idNivel).as(String.class), nivelObjectId),
                  cb.equal(root.get(Evento_.nivel).get(NivelEvento_.ID), nivelId));
          Predicate notFinal = cb.and(ext, pNotCom);
          Predicate comFinal = cb.and(log, cb.and(pCom, ext));
          predicates.add(cb.or(notFinal, comFinal));
          break;
      }
    }

    if (cartera != null) {
      Join<Evento, Cartera> carteraJoin = root.join(Evento_.cartera, JoinType.INNER);
      predicates.add(cb.equal(carteraJoin.get(Cartera_.ID), cartera));
    }

    if (emisorId != null) {
      Predicate onEmisor =
          cb.equal(emisorJoin.get(Usuario_.id).as(String.class), emisorId);
      emisorJoin.on(onEmisor);
    }
    if (destinatarioId != null) {
      Predicate onDestinatario =
          cb.equal(destinatarioJoin.get(Usuario_.id).as(String.class), destinatarioId);
      destinatarioJoin.on(onDestinatario);
    }
    // Dates
    if (fCreaMin != null && fCreaMax != null) {
      Calendar c = Calendar.getInstance();
      c.setTime(fCreaMax);
      c.add(Calendar.DATE, 1);
      c.add(Calendar.SECOND, -1);
      fCreaMax = c.getTime();
      predicates.add(cb.between(root.get(Evento_.fechaCreacion), fCreaMin, fCreaMax));
    } else {
      if (fCreaMin != null)
        predicates.add(cb.greaterThanOrEqualTo(root.get(Evento_.fechaCreacion), fCreaMin));
      if (fCreaMax != null)
        predicates.add(cb.lessThanOrEqualTo(root.get(Evento_.fechaCreacion), fCreaMax));
    }

    if (fAlerMin != null && fAlerMax != null) {
      Calendar c = Calendar.getInstance();
      c.setTime(fAlerMax);
      c.add(Calendar.DATE, 1);
      c.add(Calendar.SECOND, -1);
      fAlerMax = c.getTime();
      Predicate dis = getAlerts(cb, root, fAlerMin, fAlerMax);
      Predicate dat = cb.between(root.get(Evento_.fechaAlerta), fAlerMin, fAlerMax);
      dis.getExpressions().add(dat);
      predicates.add(dis);
    } else {
      if (fAlerMin != null)
        predicates.add(cb.greaterThanOrEqualTo(root.get(Evento_.fechaAlerta), fAlerMin));
      if (fAlerMax != null)
        predicates.add(cb.lessThanOrEqualTo(root.get(Evento_.fechaAlerta), fAlerMax));
    }

    if (fLimiMin != null && fLimiMax != null) {
      Calendar c = Calendar.getInstance();
      c.setTime(fLimiMax);
      c.add(Calendar.DATE, 1);
      c.add(Calendar.SECOND, -1);
      fLimiMax = c.getTime();
      predicates.add(cb.between(root.get(Evento_.fechaLimite), fLimiMin, fLimiMax));
    } else {
      if (fLimiMin != null)
        predicates.add(cb.greaterThanOrEqualTo(root.get(Evento_.fechaLimite), fLimiMin));
      if (fLimiMax != null)
        predicates.add(cb.lessThanOrEqualTo(root.get(Evento_.fechaLimite), fLimiMax));
    }

    if (titulo != null) {
      predicates.add(cb.like(root.get(Evento_.titulo), "%" + titulo + "%"));
    }

    if (activo != null) {
      predicates.add(cb.equal(root.get(Evento_.activo), activo));
    }
    if (revisado != null) {
      predicates.add(cb.equal(root.get(Evento_.revisado), revisado));
    }

    if (clase != null && !clase.isEmpty()) {
      Predicate disClase = cb.disjunction();
      Join<Evento, ClaseEvento> claseJoin = root.join(Evento_.claseEvento, JoinType.INNER);
      for (Integer claseId : clase) {
        Predicate onClase = cb.equal(claseJoin.get(ClaseEvento_.ID), claseId);
        disClase.getExpressions().add(onClase);
      }
      predicates.add(disClase);
    }

    if (estado != null && !estado.isEmpty()) {
      Predicate disEstado = cb.disjunction();
      Join<Evento, EstadoEvento> claseJoin = root.join(Evento_.estado, JoinType.INNER);
      for (Integer estadoId : estado) {
        Predicate onEstado = cb.equal(claseJoin.get(EstadoEvento_.ID), estadoId);
        disEstado.getExpressions().add(onEstado);
      }
      predicates.add(disEstado);
    }
    // addOnCond(cb, root, Evento_.claseEvento, clase);
    addOnCond(cb, root, Evento_.tipoEvento, tipo);
    if (subtipoList != null && !subtipoList.isEmpty()) {
      Predicate disSubt = cb.disjunction();
      Join<Evento, SubtipoEvento> subJoin = root.join(Evento_.subtipoEvento, JoinType.INNER);
      for (Integer subtipo : subtipoList) {
        Predicate onSub = cb.equal(subJoin.get(SubtipoEvento_.ID), subtipo);
        disSubt.getExpressions().add(onSub);
      }
      predicates.add(disSubt);
    }

    // addOnCond(cb, root, Evento_.estado, estado);
    addOnCond(cb, root, Evento_.importancia, importancia);
    addOnCond(cb, root, Evento_.resultado, resultado);

    return predicates;
  }

  public List<Predicate> getPredicatesByObject(
      Integer loggedUser,
      BusquedaAgendaDto busquedaAgendaDto,
      CriteriaBuilder cb,
      Root<Evento> root) {
    List<Predicate> predicates = new ArrayList<>();

    if (loggedUser != null
        && busquedaAgendaDto.getClaseEvento() != null
        && busquedaAgendaDto.getClaseEvento().equals(4)) {
      Join<Evento, Usuario> emisorJoin = root.join(Evento_.emisor, JoinType.INNER);
      Join<Evento, Usuario> destinatarioJoin = root.join(Evento_.destinatario, JoinType.INNER);
      predicates.add(
          cb.or(
              cb.like(emisorJoin.get(Usuario_.id).as(String.class), "%" + loggedUser + "%"),
              cb.like(destinatarioJoin.get(Usuario_.id).as(String.class), "%" + loggedUser + "%")));
    }

    if (busquedaAgendaDto.getTipoEvento() != null
    && (busquedaAgendaDto.getTiposEvento() == null || busquedaAgendaDto.getTiposEvento().isEmpty())) {
      Join<Evento, TipoEvento> claseJoin = root.join(Evento_.tipoEvento, JoinType.INNER);
      Predicate onClase =
          cb.equal(claseJoin.get(TipoEvento_.id), busquedaAgendaDto.getTipoEvento());
      claseJoin.on(onClase);
    }

    if (busquedaAgendaDto.getEmisor() != null) {
      Join<Evento, Usuario> emisorJoin = root.join(Evento_.emisor, JoinType.INNER);
      Predicate onEmisor =
          cb.like(
              emisorJoin.get(Usuario_.nombre).as(String.class),
              "%" + busquedaAgendaDto.getEmisor() + "%");
      emisorJoin.on(onEmisor);
    }
    if (busquedaAgendaDto.getDestinatario() != null) {
      Join<Evento, Usuario> destinatarioJoin = root.join(Evento_.destinatario, JoinType.INNER);
      Predicate onDestinatario =
          cb.like(
              destinatarioJoin.get(Usuario_.nombre).as(String.class),
              "%" + busquedaAgendaDto.getDestinatario() + "%");
      destinatarioJoin.on(onDestinatario);
    }
    if (busquedaAgendaDto.getIntervaloFechas() != null) {
      for (IntervaloFecha busqueda : busquedaAgendaDto.getIntervaloFechas()) {
        if (busqueda.getDescripcion() != null) {
          if (busqueda.getDescripcion().equals("creacion")) {
            if (busqueda.getFechaInicio() != null) {
              predicates.add(
                  cb.greaterThanOrEqualTo(
                      root.get(Evento_.fechaCreacion), busqueda.getFechaInicio()));
            }
            if (busqueda.getFechaFin() != null) {
              predicates.add(
                  cb.lessThanOrEqualTo(root.get(Evento_.fechaCreacion), busqueda.getFechaFin()));
            }
            if (predicates.size() != 0) {
              predicates.add(cb.and(predicates.toArray(new Predicate[0])));
            }
          } else {
            if (busqueda.getFechaInicio() != null) {
              predicates.add(
                  cb.greaterThanOrEqualTo(
                      root.get(Evento_.fechaLimite), busqueda.getFechaInicio()));
            }
            if (busqueda.getFechaFin() != null) {
              predicates.add(
                  cb.lessThanOrEqualTo(root.get(Evento_.fechaLimite), busqueda.getFechaFin()));
            }
            if (predicates.size() != 0) {
              predicates.add(cb.and(predicates.toArray(new Predicate[0])));
            }
          }
        }
      }
    }

    if (busquedaAgendaDto.getSubtiposEvento() != null
      && !busquedaAgendaDto.getSubtiposEvento().isEmpty()) {
      Predicate disSubtipo = cb.disjunction();
      Join<Evento, SubtipoEvento> subtipoJoin = root.join(Evento_.subtipoEvento, JoinType.INNER);
      for (Integer subtipoId : busquedaAgendaDto.getSubtiposEvento()) {
        Predicate onSubtipo = cb.equal(subtipoJoin.get(SubtipoEvento_.ID), subtipoId);
        disSubtipo.getExpressions().add(onSubtipo);
      }
      predicates.add(disSubtipo);
    } else if (busquedaAgendaDto.getTiposEvento() != null
      && !busquedaAgendaDto.getTiposEvento().isEmpty()) {
      Predicate disTipo = cb.disjunction();
      Join<Evento, TipoEvento> tipoJoin = root.join(Evento_.tipoEvento, JoinType.INNER);
      for (Integer tipoId : busquedaAgendaDto.getTiposEvento()) {
        Predicate onTipo = cb.equal(tipoJoin.get(TipoEvento_.ID), tipoId);
        disTipo.getExpressions().add(onTipo);
      }
      predicates.add(disTipo);
    } else if (busquedaAgendaDto.getClaseEvento() != null
        && !busquedaAgendaDto.getClaseEvento().isEmpty()) {
      Predicate disClase = cb.disjunction();
      Join<Evento, ClaseEvento> claseJoin = root.join(Evento_.claseEvento, JoinType.INNER);
      for (Integer claseId : busquedaAgendaDto.getClaseEvento()) {
        Predicate onClase = cb.equal(claseJoin.get(ClaseEvento_.ID), claseId);
        disClase.getExpressions().add(onClase);
      }
      predicates.add(disClase);
    }

    if (busquedaAgendaDto.getEstado() != null && !busquedaAgendaDto.getEstado().isEmpty()) {
      Predicate disEstado = cb.disjunction();
      Join<Evento, EstadoEvento> claseJoin = root.join(Evento_.estado, JoinType.INNER);
      for (Integer estadoId : busquedaAgendaDto.getEstado()) {
        Predicate onEstado = cb.equal(claseJoin.get(EstadoEvento_.ID), estadoId);
        disEstado.getExpressions().add(onEstado);
      }
      predicates.add(disEstado);
    }

    return predicates;
  }

  public List<Evento> getListFilteredEventos(
      Integer loggedUser, BusquedaAgendaDto busquedaAgendaDto) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Evento> query = cb.createQuery(Evento.class);

    Root<Evento> root = query.from(Evento.class);
    List<Predicate> predicates = getPredicatesByObject(loggedUser, busquedaAgendaDto, cb, root);

    var rs =
        query
            .select(root)
            .distinct(true)
            .where(predicates.toArray(new Predicate[predicates.size()]));

    List<Evento> eventos = entityManager.createQuery(rs).getResultList();

    return eventos;
  }

  private Predicate getAlerts(CriteriaBuilder cb, Root<Evento> root, Date fIni, Date fFin)
      throws NotFoundException {
    ClaseEvento niv =
        claseEventoRepository
            .findByCodigo("ALE")
            .orElseThrow(() -> new NotFoundException("NivelEvento", "código", "'ALE'"));
    Predicate pA = cb.equal(root.get(Evento_.claseEvento), niv.getId());
    Predicate fa = cb.between(root.get(Evento_.fechaAlerta), fIni, fFin);
    Predicate fc = cb.between(root.get(Evento_.fechaCreacion), fIni, fFin);
    Predicate fl = cb.between(root.get(Evento_.fechaLimite), fIni, fFin);

    Predicate disE = cb.disjunction();
    disE.getExpressions().add(cb.and(fa, pA));
    disE.getExpressions().add(cb.and(fc, pA));
    disE.getExpressions().add(cb.and(fl, pA));

    return disE;
  }

  private Predicate filterExpedientes(
      Expediente expediente, CriteriaBuilder cb, Root<Evento> root, Boolean profundizar) {
    Integer nivel =
        Objects.requireNonNull(nivelEventoRepository.findByCodigo("EXP").orElse(null)).getId();
    Integer nivelSMS =
      Objects.requireNonNull(nivelEventoRepository.findByCodigo("CEXT_SMS").orElse(null)).getId();
    Integer nivelBurofax =
      Objects.requireNonNull(nivelEventoRepository.findByCodigo("CEXT_BUROFAX").orElse(null)).getId();
    Integer nivelCarta=
      Objects.requireNonNull(nivelEventoRepository.findByCodigo("CEXT_CARTA").orElse(null)).getId();
    Integer nivelEmail =
      Objects.requireNonNull(nivelEventoRepository.findByCodigo("CEXT_EMAIL").orElse(null)).getId();

    if (expediente != null) {
      Predicate pExp =
        cb.or(
          cb.and(
              cb.equal(root.get(Evento_.idNivel), expediente.getId()),
              cb.like(
                  root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
                  "%" + nivel + "%")),
          cb.and(
            cb.equal(root.get(Evento_.idExpediente), expediente.getId()),
            cb.like(
              root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
              nivelSMS.toString())),
          cb.and(
            cb.equal(root.get(Evento_.idExpediente), expediente.getId()),
            cb.like(
              root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
              nivelBurofax.toString())),
          cb.and(
            cb.equal(root.get(Evento_.idExpediente), expediente.getId()),
            cb.like(
              root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
              nivelCarta.toString())),
          cb.and(
            cb.equal(root.get(Evento_.idExpediente), expediente.getId()),
            cb.like(
              root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
              nivelEmail.toString()))
        );

      List<Predicate> resultados = new ArrayList<>();
      /*nivel =
          Objects.requireNonNull(nivelEventoRepository.findByCodigo("CON").orElse(null)).getId();

      List<Contrato> contratos = new ArrayList<>(expediente.getContratos());
      List<Predicate> pContratos = new ArrayList<>();

      if (!contratos.isEmpty()) {
        for (Contrato con : contratos) {
          pContratos.addAll(filterContrato(con, cb, root));
          pContratos.add(
              cb.and(
                  cb.equal(root.get(Evento_.idNivel), con.getId()),
                  cb.equal(root.get(Evento_.nivel).get(NivelEvento_.id), nivel)));
        }
      }

      nivel =
          Objects.requireNonNull(nivelEventoRepository.findByCodigo("PRO").orElse(null)).getId();
      Set<Propuesta> propuestas = expediente.getPropuestas();
      List<Predicate> pPropuestas = new ArrayList<>();

      if (propuestas != null && !propuestas.isEmpty()) {
        pPropuestas = filterPropuestas(cb, root, nivel, propuestas);
      }

      nivel =
          Objects.requireNonNull(nivelEventoRepository.findByCodigo("EST").orElse(null)).getId();
      Set<Estimacion> estimaciones = expediente.getEstimaciones();
      List<Predicate> pEstimaciones = new ArrayList<>();

      if (estimaciones != null && !estimaciones.isEmpty()) {
        pEstimaciones = filterEstimaciones(cb, root, nivel, estimaciones);
      }

      if (profundizar) {
        List<ExpedientePosesionNegociada> expPN =
            new ArrayList<>(expediente.getExpedientesPosesionNegociada());
        for (ExpedientePosesionNegociada ePN : expPN) {
          Predicate pEPN = filterExpedientesPN(ePN, cb, root, false);
          if (pEPN != null) resultados.add(pEPN);
        }
      }*/

      resultados.add(pExp);
      /*resultados.addAll(pContratos);
      resultados.addAll(pPropuestas);
      resultados.addAll(pEstimaciones);*/

      Predicate disE = cb.disjunction();
      disE.getExpressions().add(pExp);

      for (Predicate pre : resultados) {
        disE.getExpressions().add(pre);
      }

      return disE;
    } else {
      return null;
    }
  }

  private Predicate filterExpedientesPN(
      ExpedientePosesionNegociada expediente,
      CriteriaBuilder cb,
      Root<Evento> root,
      Boolean profundizar) {
    Integer nivel =
        Objects.requireNonNull(nivelEventoRepository.findByCodigo("EXP_PN").orElse(null)).getId();
    Integer nivelSMS =
      Objects.requireNonNull(nivelEventoRepository.findByCodigo("CEXT_SMS_PN").orElse(null)).getId();
    Integer nivelBurofax =
      Objects.requireNonNull(nivelEventoRepository.findByCodigo("CEXT_BUROFAX_PN").orElse(null)).getId();
    Integer nivelCarta=
      Objects.requireNonNull(nivelEventoRepository.findByCodigo("CEXT_CARTA_PN").orElse(null)).getId();
    Integer nivelEmail =
      Objects.requireNonNull(nivelEventoRepository.findByCodigo("CEXT_EMAIL_PN").orElse(null)).getId();


    if (expediente != null) {
      Predicate pExp =
        cb.or(
          cb.and(
              cb.equal(root.get(Evento_.idNivel), expediente.getId()),
              cb.like(
                  root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
                  "%" + nivel + "%")),
          cb.and(
            cb.equal(root.get(Evento_.idExpediente), expediente.getId()),
            cb.like(
              root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
              nivelSMS.toString())),
          cb.and(
            cb.equal(root.get(Evento_.idExpediente), expediente.getId()),
            cb.like(
              root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
              nivelCarta.toString())),
          cb.and(
            cb.equal(root.get(Evento_.idExpediente), expediente.getId()),
            cb.like(
              root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
              nivelBurofax.toString())),
          cb.and(
            cb.equal(root.get(Evento_.idExpediente), expediente.getId()),
            cb.like(
              root.get(Evento_.nivel).get(NivelEvento_.id).as(String.class),
              nivelEmail.toString()))
        );

      List<Predicate> resultados = new ArrayList<>();
      nivel =
          Objects.requireNonNull(nivelEventoRepository.findByCodigo("BIEN_PN").orElse(null))
              .getId();


      List<BienPosesionNegociada> bienes = new ArrayList<>(expediente.getBienesPosesionNegociada());
      List<Predicate> pBienes = new ArrayList<>();
      if (!bienes.isEmpty()) {
        pBienes = filterBienesPN(cb, root, nivel, bienes);
      }

      nivel =
          Objects.requireNonNull(nivelEventoRepository.findByCodigo("PRO").orElse(null)).getId();
      Set<Propuesta> propuestas = expediente.getPropuestas();
      List<Predicate> pPropuestas = new ArrayList<>();

      if (propuestas != null && !propuestas.isEmpty()) {
        pPropuestas = filterPropuestas(cb, root, nivel, propuestas);
      }

      if (profundizar) {
        Expediente ex = expediente.getExpediente();
        Predicate pEx = filterExpedientes(ex, cb, root, false);
        if (pEx != null) resultados.add(pEx);
      }

      resultados.add(pExp);
      resultados.addAll(pBienes);
      resultados.addAll(pPropuestas);

      Predicate disE = cb.disjunction();
      disE.getExpressions().add(pExp);

      for (Predicate pre : resultados) {
        disE.getExpressions().add(pre);
      }

      return disE;
    } else return null;
  }

  private List<Predicate> filterPropuestas(
      CriteriaBuilder cb, Root<Evento> root, Integer nivel, Set<Propuesta> objetos) {
    List<Predicate> resultados = new ArrayList<>();
    Join<Evento, ?> estadoJoin = root.join(Evento_.nivel, JoinType.INNER);
    for (Propuesta a : objetos) {
      Predicate onCond = cb.equal(estadoJoin.get(NivelEvento_.ID), nivel);
      resultados.add(cb.and(onCond, cb.equal(root.get(Evento_.idNivel), a.getId())));
    }
    return resultados;
  }

  private List<Predicate> filterEstimaciones(
      CriteriaBuilder cb, Root<Evento> root, Integer nivel, Set<Estimacion> objetos) {
    List<Predicate> resultados = new ArrayList<>();
    Join<Evento, ?> estadoJoin = root.join(Evento_.nivel, JoinType.INNER);
    for (Estimacion a : objetos) {
      Predicate onCond = cb.equal(estadoJoin.get(NivelEvento_.ID), nivel);
      resultados.add(cb.and(onCond, cb.equal(root.get(Evento_.idNivel), a.getId())));
    }
    return resultados;
  }

  private List<Predicate> filterBienes(
      CriteriaBuilder cb, Root<Evento> root, Integer nivel, List<Bien> objetos) {
    List<Predicate> resultados = new ArrayList<>();
    //  Join<Evento, ?> estadoJoin = root.join(Evento_.nivel, JoinType.INNER);
    for (Bien a : objetos) {
      Predicate onCond = cb.equal(root.get(Evento_.nivel).get(NivelEvento_.ID), nivel);
      resultados.add(cb.and(onCond, cb.equal(root.get(Evento_.idNivel), a.getId())));
    }
    return resultados;
  }

  private List<Predicate> filterIntervinientes(
      CriteriaBuilder cb, Root<Evento> root, Integer nivel, List<Interviniente> objetos) {
    List<Predicate> resultados = new ArrayList<>();
    // Join<Evento, ?> estadoJoin = root.join(Evento_.nivel, JoinType.INNER);
    for (Interviniente a : objetos) {
      Predicate onCond = cb.equal(root.get(Evento_.nivel).get(NivelEvento_.ID), nivel);
      resultados.add(cb.and(onCond, cb.equal(root.get(Evento_.idNivel), a.getId())));
    }
    return resultados;
  }

  private List<Predicate> filterProcedimientos(
      CriteriaBuilder cb, Root<Evento> root, Integer nivel, List<Procedimiento> objetos) {
    List<Predicate> resultados = new ArrayList<>();
    Join<Evento, ?> estadoJoin = root.join(Evento_.nivel, JoinType.INNER);
    for (Procedimiento a : objetos) {
      Predicate onCond = cb.equal(estadoJoin.get(NivelEvento_.ID), nivel);
      resultados.add(cb.and(onCond, cb.equal(root.get(Evento_.idNivel), a.getId())));
    }
    return resultados;
  }

  private List<Predicate> filterSkipTracing(
      CriteriaBuilder cb, Root<Evento> root, Integer nivel, List<SkipTracing> objetos) {
    List<Predicate> resultados = new ArrayList<>();
    Join<Evento, ?> estadoJoin = root.join(Evento_.nivel, JoinType.INNER);
    for (SkipTracing a : objetos) {
      Predicate onCond = cb.equal(estadoJoin.get(NivelEvento_.ID), nivel);
      resultados.add(cb.and(onCond, cb.equal(root.get(Evento_.idNivel), a.getId())));
    }
    return resultados;
  }

  private List<Predicate> filterContrato(Contrato contrato, CriteriaBuilder cb, Root<Evento> root) {
    Integer nivel =
        Objects.requireNonNull(nivelEventoRepository.findByCodigo("BIEN").orElse(null)).getId();
    List<Bien> bienes =
        contrato.getBienes().stream().map(c -> c.getBien()).collect(Collectors.toList());
    List<Predicate> pBienes = new ArrayList<>();
    if (bienes != null && !bienes.isEmpty()) pBienes = filterBienes(cb, root, nivel, bienes);

    nivel = Objects.requireNonNull(nivelEventoRepository.findByCodigo("INTE").orElse(null)).getId();
    List<Interviniente> intervinientes =
        new ArrayList<>(
            contrato.getIntervinientes().stream()
                .map(ContratoInterviniente::getInterviniente)
                .collect(Collectors.toList()));
    List<Predicate> pIntervinientes = new ArrayList<>();
    if (intervinientes != null && !intervinientes.isEmpty())
      pIntervinientes = filterIntervinientes(cb, root, nivel, intervinientes);

    nivel = Objects.requireNonNull(nivelEventoRepository.findByCodigo("JUD").orElse(null)).getId();
    List<Procedimiento> procedimientos = new ArrayList<>(contrato.getProcedimientos());
    List<Predicate> pProcedimientos = new ArrayList<>();
    if (procedimientos != null && !procedimientos.isEmpty())
      pProcedimientos = filterProcedimientos(cb, root, nivel, procedimientos);

    nivel =
        Objects.requireNonNull(nivelEventoRepository.findByCodigo("SK_TR").orElse(null)).getId();
    List<SkipTracing> skipTracings = new ArrayList<>();
    for (ContratoInterviniente ci : contrato.getIntervinientes()) {
      skipTracings.addAll(ci.getSkipTracing());
    }
    List<Predicate> pSkipTracing = new ArrayList<>();
    if (skipTracings != null && !skipTracings.isEmpty())
      pSkipTracing = filterSkipTracing(cb, root, nivel, skipTracings);

    List<Predicate> result = new ArrayList<>();
    result.addAll(pBienes);
    result.addAll(pIntervinientes);
    result.addAll(pProcedimientos);
    result.addAll(pSkipTracing);
    return result;
  }

  private List<Predicate> filterBienesPN(
      CriteriaBuilder cb, Root<Evento> root, Integer nivel, List<BienPosesionNegociada> objetos) {
    List<Predicate> resultados = new ArrayList<>();
    Join<Evento, ?> estadoJoin = root.join(Evento_.nivel, JoinType.INNER);
    for (BienPosesionNegociada a : objetos) {
      Predicate onCond = cb.equal(estadoJoin.get(NivelEvento_.ID), nivel);
      resultados.add(cb.and(onCond, cb.equal(root.get(Evento_.idNivel), a.getId())));
    }
    return resultados;
  }

  private void addOnCond(
      CriteriaBuilder cb,
      Root<Evento> root,
      SingularAttribute<Evento, ?> singularAtrribute,
      Integer campo) {
    if (campo == null) return;
    Join<Evento, ?> estadoJoin = root.join(singularAtrribute, JoinType.INNER);
    Predicate onCond = cb.equal(estadoJoin.get(Catalogo_.ID), campo);
    estadoJoin.on(onCond);
  }

  // Otros
  public List<String> inborrables() {
    List<String> result = new ArrayList<>();
    result.add("EST");
    result.add("ANU");
    result.add("CAD");
    result.add("DES");
    result.add("DEN");
    result.add("FOR");
    result.add("PGE");
    result.add("PCL");
    result.add("PFI");
    return result;
  }

  public List<String> codigosDeWF() {
    List<String> result = new ArrayList<>();
    result.add("REV04");
    result.add("REV05");
    result.add("REV06");
    result.add("REV07");
    result.add("REV09");
    result.add("REV10");
    result.add("PROP06");
    return result;
  }

  public List<Integer> getEstadosReasignacion() throws NotFoundException {
    List<Integer> estados = new ArrayList<>();

    estados.add(
        estadoEventoRepository
            .findByCodigo("PEN")
            .orElseThrow(() -> new NotFoundException("EstadoEvento", "código", "'PEN'"))
            .getId());
    estados.add(
        estadoEventoRepository
            .findByCodigo("PRO")
            .orElseThrow(() -> new NotFoundException("EstadoEvento", "código", "'PRO'"))
            .getId());

    return estados;
  }

  public void anadirEventosPerdidos(Usuario nuevo, Integer idExp, String nivel)
      throws NotFoundException {
    NivelEvento niv =
        nivelEventoRepository
            .findByCodigo(nivel)
            .orElseThrow(() -> new NotFoundException("NivelEvento", "código", nivel));
    List<Map> ids = new ArrayList<>();
    if (nivel.equals("EXP"))
      ids = usuarioRepository.getLastUsuarioAsignado(idExp, nuevo.getPerfil().getId());
    else if (nivel.equals("EXP_PN"))
      ids = usuarioRepository.getLastUsuarioAsignadoPN(idExp, nuevo.getPerfil().getId());
    Integer id = null;
    if (!ids.isEmpty()) {
      Map ultimo = ids.get(ids.size() - 1);
      id = (Integer) ultimo.get("id");
    }
    if (id != null) {
      List<Evento> eventos =
          this.getFilteredEventos(
              null,
              null,
              id,
              niv.getId(),
              idExp,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              this.getEstadosReasignacion(),
              null,
              null,
              null,
              null,
              null,
              null,
              "fechaAlerta",
              "Asc",
              null,
              null);

      for (Evento e : eventos) {
        e.setDestinatario(nuevo);
      }

      eventoRepository.saveAll(eventos);
    }
  }

  public void reasignarDestinatarios(
      Usuario original, Usuario nuevo, Integer idObjeto, String codigoNivel)
      throws NotFoundException {
    NivelEvento niv =
        nivelEventoRepository
            .findByCodigo(codigoNivel)
            .orElseThrow(() -> new NotFoundException("NivelEvento", "código", codigoNivel));
    List<Evento> eventos = new ArrayList<>();
    if (original == null) {
      original = obtenerOriginal(nuevo, idObjeto, codigoNivel);
      if (original != null)
        eventos =
            this.getFilteredEventos(
                original.getId(),
                null,
                null,
                niv.getId(),
                idObjeto,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                this.getEstadosReasignacion(),
                null,
                null,
                null,
                null,
                null,
                null,
                "fechaAlerta",
                "Asc",
                null,
                null);
    } else {
      eventos =
          this.getFilteredEventos(
              original.getId(),
              null,
              null,
              niv.getId(),
              idObjeto,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              this.getEstadosReasignacion(),
              null,
              null,
              null,
              null,
              null,
              null,
              "fechaAlerta",
              "Asc",
              null,
              null);
    }

    for (Evento e : eventos) {
      if (!e.getClaseEvento().getCodigo().equals("COM")) {
        boolean cambio = false;
        if (e.getDestinatario().getId().equals(original.getId())) {
          e.setDestinatario(nuevo);
          cambio = true;
        }
        if (e.getEmisor().getId().equals(original.getId())) {
          e.setEmisor(nuevo);
          cambio = true;
        }
        if (cambio) eventoRepository.save(e);
      }
    }
  }

  private Usuario obtenerOriginal(Usuario nuevo, Integer idExpediente, String codigoNivel) {
    List<Usuario> usuarios = new ArrayList<>();
    if (codigoNivel.equals("EXP")) {
      usuarios =
          usuarioRepository.findAllByPerfilNombreAndAsignacionExpedientesExpedienteId(
              nuevo.getPerfil().getNombre(), idExpediente);
    } else if (codigoNivel.equals("EXP_PN")) {
      usuarios =
          usuarioRepository.findAllByPerfilNombreAndAsignacionExpedientesPNExpedienteId(
              nuevo.getPerfil().getNombre(), idExpediente);
    }
    if (usuarios.isEmpty()) return null;
    return usuarios.get(usuarios.size() - 1);
  }

  public void reasignarEventos(SubtipoEvento se, Perfil nuevo) throws NotFoundException {
    List<Integer> subtipos = new ArrayList<>();
    subtipos.add(se.getId());
    List<Evento> eventos =
        this.getFilteredEventos(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            subtipos,
            null,
            this.getEstadosReasignacion(),
            null,
            null,
            null,
            null,
            null,
            null,
            "fechaAlerta",
            "Asc",
            null,
            null);
    for (Evento evento : eventos) {
      Integer idExp = evento.getIdExpediente();
      if (idExp != null) {
        Expediente expediente = null;
        ExpedientePosesionNegociada expedientePN = null;
        Usuario dest = null;

        String codigo = evento.getNivel().getCodigo();
        if (!codigo.equals("EXP_PN") && !codigo.equals("BIEN_PN")) {
          if (codigo.equals("PRO")) {
            Propuesta pro =
                propuestaRepository
                    .findById(evento.getIdNivel())
                    .orElseThrow(() -> new NotFoundException("propuesta", evento.getIdNivel()));
            if (pro.getExpediente() != null) expediente = pro.getExpediente();
            else if (pro.getExpedientePosesionNegociada() != null)
              expedientePN = pro.getExpedientePosesionNegociada();
          } else if (codigo.equals("EST")) {
            Estimacion est =
                estimacionRepository
                    .findById(evento.getIdNivel())
                    .orElseThrow(() -> new NotFoundException("estimacion", evento.getIdNivel()));
            if (est.getExpediente() != null) expediente = est.getExpediente();
            else if (est.getExpedientePosesionNegociada() != null)
              expedientePN = est.getExpedientePosesionNegociada();
          } else {
            expediente =
                expedienteRepository
                    .findById(evento.getIdExpediente())
                    .orElseThrow(
                        () -> new NotFoundException("expediente", evento.getIdExpediente()));
          }
        } else {
          expedientePN =
              expedientePosesionNegociadaRepository
                  .findById(evento.getIdExpediente())
                  .orElseThrow(
                      () -> new NotFoundException("expedientePN", evento.getIdExpediente()));
        }

        if (expediente != null) dest = expediente.getUsuarioByPerfil(nuevo.getNombre());
        if (expedientePN != null) dest = expedientePN.getUsuarioByPerfil(nuevo.getNombre());

        if (dest != null) evento.setDestinatario(dest);
      }
    }

    eventoRepository.saveAll(eventos);
  }

  public Boolean cerrarEventosPropuesta(Integer idProp) throws NotFoundException {
    NivelEvento ne =
        nivelEventoRepository
            .findByCodigo("PRO")
            .orElseThrow(() -> new NotFoundException("NivelEvento", "código", "'PRO'"));
    EstadoEvento estado =
        estadoEventoRepository
            .findByCodigo("NREA")
            .orElseThrow(() -> new NotFoundException("EstadoEvento", "código", "'NREA'"));
    List<Evento> eventos =
        this.getFilteredEventos(
            null,
            null,
            null,
            ne.getId(),
            idProp,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            this.getEstadosReasignacion(),
            null,
            null,
            null,
            null,
            null,
            null,
            "fechaAlerta",
            "Asc",
            null,
            null);
    for (Evento e : eventos) {
      e.setEstado(estado);
    }
    eventoRepository.saveAll(eventos);
    return true;
  }

  public void eliminarEventosPropuesta(Propuesta p) {
    List<Evento> eventos =
        eventoRepository.findAllByIdNivelAndNivelCodigoAndEstadoCodigo(p.getId(), "PRO", "PEN");
    eventos.addAll(
        eventoRepository.findAllByIdNivelAndNivelCodigoAndEstadoCodigo(p.getId(), "PRO", "PRO"));

    EstadoEvento estado = estadoEventoRepository.findByCodigo("NREA").orElse(null);

    for (Evento evento : eventos) {
      evento.setEstado(estado);
      eventoRepository.save(evento);
    }
  }

  public Boolean comprobarEditable(Propuesta p){
    if (p.getExpediente() != null && p.getSubtipoPropuesta() != null && p.getSubtipoPropuesta().getPbc()){
      Cartera c = p.getExpediente().getCartera();
      if (c.getNombre().equals("SAREB"))
        return !comprobarDatosPropuesta(p.getSancionPropuesta(), p.getResultadoPBC())
          && comprobarCamposSareb(p);
      else if (c.getIdCarga().equals("5202"))
        return !comprobarDatosPropuesta(p.getSancionPropuesta(), p.getResultadoPBC())
          && comprobarCamposCerberus(p);
    }
    return true;
  }

  private Boolean comprobarCamposSareb(Propuesta p){
    return p.getImporteColabora() != null && p.getNumColabora() != null;
  }
  private Boolean comprobarCamposCerberus(Propuesta p){
    return p.getNumAdvisoryNote() != null && p.getFechaEnvioCES() != null &&
      p.getFechaEnvioWorkingGroup() != null && p.getFechaRecepcionAprobacionAdvisoryNote() != null;
  }
  private Boolean comprobarDatosPropuesta(SancionPropuesta sp, ResultadoPBC r){
    if (sp == null || r != null) return false;
    if (!sp.getCodigo().equals("AUT") && !sp.getCodigo().equals("AC")) return false;
    return true;
  }
  //Desuso por comprobación por idCarga
  private List<String> getCarterasCerberus(){
    List<String> nombres = new ArrayList<>();

    nombres.add("CERBERUS ORIGINAL");
    nombres.add("EGEO PROMONTORIA");
    nombres.add("JAIPUR");
    nombres.add("AGORA");

    return nombres;
  }

  public Integer eventoFromPropuesta(Integer propuestaId) {
    Integer evento = null;
    List<Evento> eventos =
      eventoRepository.findAllByNivelIdAndIdNivelOrderByFechaCreacionDesc(6, propuestaId);
    List<String> codigosDeWF = this.codigosDeWF();
    for (Evento e : eventos) {
      if (codigosDeWF.contains(e.getSubtipoEvento().getCodigo())) {
        evento = e.getId();
        break;
      }
    }
    return evento;
  }

  @Scheduled(cron = "0 0 1 * * *")
  public void caducar() {
    log.info("Iniciando tarea programada: EventoUseCaseImpl::caducar");

    Date hoy = Calendar.getInstance().getTime();
    List<Evento> eventos = eventoRepository.findAllByFechaLimiteLessThanAndEstadoCodigo(hoy, "PEN");
    eventos.addAll(eventoRepository.findAllByFechaLimiteLessThanAndEstadoCodigo(hoy, "PRO"));
    EstadoEvento estado = estadoEventoRepository.findByCodigo("NREA").orElse(null);

    for (Evento evento : eventos) {
      evento.setEstado(estado);
      eventoRepository.save(evento);
    }
  }
}
