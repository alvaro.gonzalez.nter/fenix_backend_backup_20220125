package com.haya.alaska.evento.application;

import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.BusquedaAgendaDto;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.evento.infrastructure.controller.dto.*;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.resultado_evento.infrastructure.controller.dto.ResultadoEventoOutputDTO;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.visita.domain.Visita;
import com.haya.alaska.visita_ra.domain.VisitaRA;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface EventoUseCase {
        EventoOutputDTO findEventoById(Integer id) throws NotFoundException;
    EventoOutputDTO create(EventoInputDTO input, Usuario usuario) throws NotFoundException, RequiredValueException;
    ListWithCountDTO<EventoOutputDTO> masiveCreate(EventoInputDTO input, List<Integer> objetivos, Usuario usuario)
            throws NotFoundException, RequiredValueException;
    void delete(Integer id) throws NotFoundException;
    List<SubtipoEventoOutputDTO> findSubtipo (Integer idPadre, Usuario usuario, Integer carteraId);
    List<SubtipoEventoOutputDTO> findSubtipos (List<Integer> tipos, Usuario usuario, Integer carteraId);
    List<CatalogoMinInfoDTO> findResultados (Integer idSubtipo, Integer idEvento) throws NotFoundException;
    List<CatalogoMinInfoDTO> findTipo (Integer idPadre) throws NotFoundException;
    List<CatalogoMinInfoDTO> findTipos (List<Integer> tipos) throws NotFoundException;
    EventoOutputDTO update(Integer eventoId, EventoInputDTO input, Usuario usuario) throws NotFoundException, RequiredValueException, InvalidCodeException, IOException;
    ListWithCountDTO<EventoOutputDTO> findAll(
            Integer loggedUser,Integer emisorId, Integer destinatarioId, Integer nivelId, Integer nivelObjectId,
            Date fCreaMin, Date fCreaMax,  Date fAlerMin, Date fAlerMax,
            Date fLimiMin, Date fLimiMax, List<Integer> clase, Integer tipo, Integer subtipo, List<Integer> subtipoList, Integer cartera, List<Integer> estado,
            Integer importancia, Integer resultado, String titulo, Boolean activo, Boolean revisado,
            String orderField, String orderDirection, Integer page, Integer size
    ) throws NotFoundException;
    void masiveDelete(List<Integer> objetivos) throws NotFoundException;
    UsuarioAgendaDto userByExpedienteAndPerfil(Integer idExpediente, Integer idPerfil, Boolean posesionNegociada) throws NotFoundException;
    void elevarPropuesta(Integer idPropuesta, Usuario emisor, Integer idCartera) throws NotFoundException, RequiredValueException, InvalidCodeException;
  List<UsuarioAgendaDto> getExpedientUsers(Integer idExpediente, Boolean pn) throws NotFoundException;

  Boolean getAuthorization(Integer subtipoId, Usuario usuario) throws NotFoundException;

  List<CatalogoMinInfoDTO> findTipoFiltrado(Integer idPadre, Integer carteraId) throws NotFoundException;
  List<CatalogoMinInfoDTO> findClaseFiltrado(Integer carteraId) throws NotFoundException;
  void updateWorkflow(WorkflowInputDTO input) throws NotFoundException;

  List<CarteraEstadoPropuestaOutputDTO> getEstadoActivo(Integer carteraId) throws NotFoundException;
  List<SubtipoEventoOutputDTO> findByCartera(Integer carteraId, Integer estadoPropuestaId) throws NotFoundException;

  List<ResultadoEventoOutputDTO>findResultadoEventoBySubTipoEvento(Integer subtipoeventoId);

  List<SubtipoEventoOutputDTO>findWorkflowCarteraResultadoEventoAndSuBtipoEvento(Integer carteraId, Integer resultadoEventoId, Integer subtipoEventoId);
  void solicitarSkipTracing(Integer idInter, Integer idCont, Usuario creador) throws NotFoundException, RequiredValueException, ForbiddenException;
  void eventoCierreST(SkipTracing st, Usuario creador, Usuario destinatario) throws NotFoundException, RequiredValueException;
  void crearEventoVisita(Visita visita, Usuario creador) throws NotFoundException, RequiredValueException;
  void crearEventoVisitaRA(Integer idExpediente, VisitaRA visita, Usuario creador) throws NotFoundException, RequiredValueException;
  void createEventoUtilidades(
    EventoInputDTO input, List<Integer> expedienteIdList, List<Integer> expedientePNIdList,
    boolean todos, boolean todosPN, BusquedaDto busqueda, Usuario logUser) throws NotFoundException, RequiredValueException;

  ListWithCountDTO<EventoOutputDTO> getListActividad(
    Integer loggedUser, BusquedaAgendaDto busquedaAgendaDto, Integer nivelId, Integer nivelObjectId,
    String orderField, String orderDirection, Integer page, Integer size) throws NotFoundException, ParseException;

  Integer eventoFromPropuesta(Integer propuestaId);

  EventoOutputDTO getLastWF(Integer idPropuesta) throws NotFoundException;

  Boolean eventoWFJudicial(Integer idContrato, Integer idInterviniente, Date fechaPeticion, Usuario logged) throws NotFoundException, RequiredValueException;
  void crearEventoCierrePBC(Integer idPropuesta, Usuario creador) throws NotFoundException, RequiredValueException;
  void alertaReasignaciones(List<Integer> idsRA, List<Integer> idsPN, Usuario creador, Integer idDestinatario) throws NotFoundException, RequiredValueException;
  void alertaFormalizacion (
    Propuesta propuesta, Usuario creador, Integer idDestinatario, String titulo, String descripcion)
    throws NotFoundException, RequiredValueException;
  void alertaFormalizacionPorPerfil (
    Propuesta propuesta, Usuario creador, String perfil, String titulo, String descripcion) throws NotFoundException, RequiredValueException;
  void alertaPteFirma(Propuesta p, Usuario creador) throws NotFoundException, RequiredValueException;
  void llamadaAbierta (
    Integer idExpediente, Integer idInterviniente, String telefono, Boolean pn, Usuario creador)
    throws NotFoundException, RequiredValueException;
}
