package com.haya.alaska.evento.application;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.domain.EventoExcel;
import com.haya.alaska.evento.infrastructure.controller.dto.InformeEventoInputDTO;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.expediente.application.ExpedienteUseCase;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.domain.ExpedienteCarteraExcel;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente.infrastructure.util.ExpedienteUtil;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.procedimiento.domain.Procedimiento;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class EventoExcelUseCaseImpl implements EventoExcelUseCase {


  @Autowired
  private CarteraRepository carteraRepository;
  @Autowired
  private EventoRepository eventoRepository;
  @Autowired
  private ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired
  private EventoUtil eventoUtil;
  @Autowired
  private ExpedienteUtil expedienteUtil;
  @Autowired
  private ExpedienteUseCase expedienteUseCase;
  @Autowired
  private ExpedienteRepository expedienteRepository;

  @Override
  public LinkedHashMap<String, List<Collection<?>>> findExcelActividadById(Integer carteraId, Integer mes, InformeEventoInputDTO informeEventoInputDTO,
                                                                           Usuario usuario, boolean posesionNegociada) throws Exception {

    Perfil perfilUsu = usuario.getPerfil();

    if (perfilUsu.getNombre().equals("Responsable de cartera") || perfilUsu.getNombre().equals("Supervisor") || usuario.esAdministrador()) {
      var datos = this.createActividadDataForExcel();
      if (carteraId != null) {
        Cartera cartera = carteraRepository.findById(carteraId)
          .orElseThrow(() -> new NotFoundException("Cartera", carteraId));
        this.addActividadDataForExcel(cartera, datos, mes, informeEventoInputDTO, posesionNegociada, usuario);
      } else {
        for (Cartera cartera : carteraRepository.findAll()) {
          this.addActividadDataForExcel(cartera, datos, mes, informeEventoInputDTO, posesionNegociada, usuario);
        }
      }
      return datos;
    } else {
      throw new IllegalArgumentException("El tipo del usuario debe ser Responsable de cartera o Supervisor para poder generar el Informe");
    }
  }


  private LinkedHashMap<String, List<Collection<?>>> createActividadDataForExcel() {

    LinkedHashMap<String, List<Collection<?>>> datos = new LinkedHashMap<>();

    List<Collection<?>> expediente = new ArrayList<>();
    expediente.add(ExpedienteCarteraExcel.cabeceras);

    List<Collection<?>> agenda = new ArrayList<>();
    agenda.add(EventoExcel.cabeceras);

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      datos.put("Connection", expediente);
      datos.put("Agenda", agenda);

    } else  {
      datos.put("Expediente", expediente);
      datos.put("Agenda", agenda);
    }

    return datos;
  }


  private void addActividadDataForExcel(Cartera cartera, LinkedHashMap<String, List<Collection<?>>> datos, Integer mes, InformeEventoInputDTO informeEventoInputDTO, boolean posesionNegociada, Usuario usuario) throws NotFoundException, ParseException {
    if (posesionNegociada) {
      addActividadDataForExcelPN(cartera, datos, mes, informeEventoInputDTO, usuario);
    } else {
      addActividadDataForExcelRA(cartera, datos, mes, informeEventoInputDTO, usuario);
    }
  }

  private void addActividadDataForExcelRA(Cartera cartera, LinkedHashMap<String, List<Collection<?>>> datos, Integer mes, InformeEventoInputDTO informeEventoInputDTO, Usuario usuario) throws ParseException, NotFoundException {

    List<Collection<?>> expedientes;
    List<Collection<?>> agendas;

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      expedientes = datos.get("Connection");
      agendas = datos.get("Agenda");

    } else  {
      expedientes = datos.get("Expediente");
      agendas = datos.get("Agenda");
    }

    List<Expediente> listadoExpedientes = new ArrayList<>();
    if (usuario.esAdministrador()){
      listadoExpedientes = expedienteRepository.findAllByCarteraId(cartera.getId());
    } else {
      listadoExpedientes = expedienteRepository.findAllByCarteraIdAndAsignacionesUsuarioId(cartera.getId(), usuario.getId());
    }


    List<Expediente> expedientesFilter = listadoExpedientes.stream().filter(
      expediente -> {
        Date fCE = expediente.getFechaCreacion();
        Date fCI = informeEventoInputDTO.getFechaCreacionExpedienteInicial();
        Date fCF = informeEventoInputDTO.getFechaCreacionExpedienteFinal();
        if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;

        Date fRE = expediente.getFechaCierre();
        Date fRI = informeEventoInputDTO.getFechaCierreExpedienteInicial();
        Date fRF = informeEventoInputDTO.getFechaCierreExpedienteFinal();
        if (!expedienteUtil.comprobarEntre(fRE, fRI, fRF)) return false;
        return usuario.contieneExpediente(expediente.getId(), false);
      }).collect(Collectors.toList());


    for (Expediente expediente : expedientesFilter) {

      Boolean comprobacion = expedienteUseCase.filtroMesInformes(expediente, mes);
      var estadoExpediente = expedienteUtil.getEstado(expediente);
      if (comprobacion) {
        if (informeEventoInputDTO.getEstadoExpediente() == null || estadoExpediente.getId().equals(informeEventoInputDTO.getEstadoExpediente())) {

          Evento accion = getLastEvento(expediente.getId(), 1);
          Evento alerta = getLastEvento(expediente.getId(), 2);
          Evento actividad = getLastEvento(expediente.getId(), 3);
          Procedimiento p = expedienteUtil.getUltimoProcedimiento(expediente);

          List<Evento> eventos = eventoRepository.findByIdExpediente(expediente.getId());


          List<Evento> eventosFilterCreacion = eventos.stream().filter(
            evento -> {
              if (informeEventoInputDTO.getTipoEvento() != null) {
                if (evento.getClaseEvento() == null || !evento.getClaseEvento().getId().equals(informeEventoInputDTO.getTipoEvento())) {
                  return false;
                }
              }

              Date fSP = evento.getFechaCreacion();
              Date fSI = informeEventoInputDTO.getFechaCreacionEventoInicial();
              Date fSF = informeEventoInputDTO.getFechaCreacionEventoFinal();
              if (!expedienteUtil.comprobarEntre(fSP, fSI, fSF)) return false;

              Date fEP = evento.getFechaLimite();
              Date fEI = informeEventoInputDTO.getFechaLimiteEventoInicial();
              Date fEF = informeEventoInputDTO.getFechaLimiteEventoFinal();
              if (!expedienteUtil.comprobarEntre(fEP, fEI, fEF)) return false;
              return true;
            }).collect(Collectors.toList());

          if(eventosFilterCreacion.size()==0)
            continue;

          expedientes.add(new ExpedienteCarteraExcel(expediente, accion, alerta, actividad, p).getValuesList());

          for (Evento evento : eventosFilterCreacion) {

            Date fechaCreacionB = evento.getFechaCreacion() != null ? evento.getFechaCreacion() : null;
            Date fechaLimiteB = evento.getFechaLimite() != null ? evento.getFechaLimite() : null;

            // if ((informeEventoInputDTO.getTipoEvento() == null || (evento.getClaseEvento() != null && evento.getClaseEvento().getId().equals(informeEventoInputDTO.getTipoEvento())))){
            Expediente ex = null;
            if (evento.getIdExpediente() != null)
              ex = expedienteRepository.findById(evento.getIdExpediente()).orElse(null);
            agendas.add(new EventoExcel(evento, ex != null ? ex.getIdConcatenado() : "").getValuesList());
            //}
          }
        }
      }
    }
  }

  private void addActividadDataForExcelPN(Cartera cartera, LinkedHashMap<String, List<Collection<?>>> datos, Integer mes, InformeEventoInputDTO informeEventoInputDTO, Usuario usuario) throws ParseException, NotFoundException {

     List<Collection<?>> expedientes;
     List<Collection<?>> agendas;

    if (LocaleContextHolder.getLocale().getLanguage() == null
      || LocaleContextHolder.getLocale().getLanguage().equals("en")){
      expedientes = datos.get("Connection");
      agendas = datos.get("Agenda");

    } else {
      expedientes = datos.get("Expediente");
      agendas = datos.get("Agenda");

    }

    List<ExpedientePosesionNegociada> listadoExpedientesPN = new ArrayList<>();
    if (usuario.esAdministrador()){
      listadoExpedientesPN = expedientePosesionNegociadaRepository.findAllByCarteraId(cartera.getId());
    } else {
      listadoExpedientesPN = expedientePosesionNegociadaRepository.findAllByCarteraIdAndAsignacionesUsuarioId(cartera.getId(), usuario.getId());
    }

    List<ExpedientePosesionNegociada> expedientesFilter = listadoExpedientesPN.stream().filter(
      expedientePN -> {
        Date fCE = expedientePN.getFechaCreacion();
        Date fCI = informeEventoInputDTO.getFechaCreacionExpedienteInicial();
        Date fCF = informeEventoInputDTO.getFechaCreacionExpedienteFinal();
        if (!expedienteUtil.comprobarEntre(fCE, fCI, fCF)) return false;

        Date fRE = expedientePN.getFechaCierre();
        Date fRI = informeEventoInputDTO.getFechaCierreExpedienteInicial();
        Date fRF = informeEventoInputDTO.getFechaCierreExpedienteFinal();
        if (!expedienteUtil.comprobarEntre(fRE, fRI, fRF)) return false;
        return usuario.contieneExpediente(expedientePN.getId(), true);
      }).collect(Collectors.toList());

    for (ExpedientePosesionNegociada expedientePosesionNegociada : expedientesFilter) {
      Boolean comprobacion = expedienteUseCase.filtroMesInformesPosesionNegociada(expedientePosesionNegociada, mes);
      var estadoExpediente = expedientePosesionNegociada.getEstadoExpedienteRecuperacionAmistosa();
      if (comprobacion) {
        if (informeEventoInputDTO.getEstadoExpediente() == null || estadoExpediente.getId().equals(informeEventoInputDTO.getEstadoExpediente())) {
          Evento accion = getLastEvento(expedientePosesionNegociada.getId(), 1);
          Evento alerta = getLastEvento(expedientePosesionNegociada.getId(), 2);
          Evento actividad = getLastEvento(expedientePosesionNegociada.getId(), 3);
          Procedimiento p = expedienteUtil.getUltimoProcedimiento(expedientePosesionNegociada);

          List<Evento> eventos = eventoRepository.findByIdExpediente(expedientePosesionNegociada.getId());

          List<Evento> eventosFilterCreacion = eventos.stream().filter(
            evento -> {
              if (informeEventoInputDTO.getTipoEvento() != null) {
                if (evento.getClaseEvento() == null || !evento.getClaseEvento().getId().equals(informeEventoInputDTO.getTipoEvento())) {
                  return false;
                }
              }
              Date fSP = evento.getFechaCreacion();
              Date fSI = informeEventoInputDTO.getFechaCreacionEventoInicial();
              Date fSF = informeEventoInputDTO.getFechaCreacionEventoFinal();
              if (!expedienteUtil.comprobarEntre(fSP, fSI, fSF)) return false;
              Date fEP = evento.getFechaLimite();
              Date fEI = informeEventoInputDTO.getFechaLimiteEventoInicial();
              Date fEF = informeEventoInputDTO.getFechaLimiteEventoFinal();
              if (!expedienteUtil.comprobarEntre(fEP, fEI, fEF)) return false;
              return true;
            }).collect(Collectors.toList());

          if(eventosFilterCreacion.size()==0)
            continue;

          expedientes.add(new ExpedienteCarteraExcel(expedientePosesionNegociada, accion, alerta, actividad, p).getValuesList());

          for (Evento evento : eventosFilterCreacion) {
            Date fechaCreacionB = evento.getFechaCreacion() != null ? evento.getFechaCreacion() : null;
            Date fechaLimiteB = evento.getFechaLimite() != null ? evento.getFechaLimite() : null;
            ExpedientePosesionNegociada ex = null;
            if (evento.getIdExpediente() != null)
              ex = expedientePosesionNegociadaRepository.findById(evento.getIdExpediente()).orElse(null);
            agendas.add(new EventoExcel(evento, ex != null ? ex.getIdConcatenado() : "").getValuesList());
          }
        }
      }
    }
  }


  public Evento getLastEvento(Integer idExpediente, Integer clase) {
    try {
      List<Integer> clases = new ArrayList<>();
      clases.add(clase);
      List<Evento> eventos = eventoUtil.getFilteredEventos(null, null, null, 1,
        idExpediente, null, null, null, null,
        null, null, clases, null, null, null, null, null, null, null, null,
        null, null, "fechaCreacion", "desc", 0, 100);

      Evento result = null;
      if (!eventos.isEmpty()) result = eventos.get(0);
      return result;
    } catch (Exception e) {
      return null;
    }
  }


}
