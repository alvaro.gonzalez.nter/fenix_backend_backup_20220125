package com.haya.alaska.evento.application;

import com.haya.alaska.evento.infrastructure.controller.dto.InformeEventoInputDTO;
import com.haya.alaska.usuario.domain.Usuario;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public interface EventoExcelUseCase {
  LinkedHashMap<String, List<Collection<?>>> findExcelActividadById(Integer carteraId, Integer mes, InformeEventoInputDTO informeEventoInputDTO,
                                                                    Usuario usuario, boolean posesionNegociada) throws Exception;
}
