package com.haya.alaska.evento.application;

import com.haya.alaska.asignacion_expediente.domain.AsignacionExpediente;
import com.haya.alaska.asignacion_expediente_posesion_negociada.domain.AsignacionExpedientePosesionNegociada;
import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.busqueda.application.BusquedaUseCaseImpl;
import com.haya.alaska.busqueda.infrastructure.controller.dto.BusquedaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.BusquedaAgendaDto;
import com.haya.alaska.busqueda.infrastructure.controller.dto.internal.IntervaloFecha;
import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.cartera.infrastructure.repository.CarteraRepository;
import com.haya.alaska.cartera_estado_propuesta.domain.CarteraEstadoPropuesta;
import com.haya.alaska.cartera_estado_propuesta.repository.CarteraEstadoPropuestaRepository;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoMinInfoDTO;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.clase_evento.infrastructure.repository.ClaseEventoRepository;
import com.haya.alaska.contrato.domain.Contrato;
import com.haya.alaska.contrato.infrastructure.repository.ContratoRepository;
import com.haya.alaska.contrato_interviniente.domain.ContratoInterviniente;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import com.haya.alaska.estado_evento.infrastructure.repository.EstadoEventoRepository;
import com.haya.alaska.estado_propuesta.domain.EstadoPropuesta;
import com.haya.alaska.estado_propuesta.infrastructure.repository.EstadoPropuestaRepository;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.evento.infrastructure.controller.dto.*;
import com.haya.alaska.evento.infrastructure.mapper.EventoMapper;
import com.haya.alaska.evento.infrastructure.repository.EventoRepository;
import com.haya.alaska.evento.infrastructure.util.EventoUtil;
import com.haya.alaska.evento.infrastructure.util.WorkflowUtil;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente.infrastructure.repository.ExpedienteRepository;
import com.haya.alaska.expediente_posesion_negociada.application.ExpedientePosesionNegociadaCaseImpl;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.expediente_posesion_negociada.infrastructure.repository.ExpedientePosesionNegociadaRepository;
import com.haya.alaska.importancia_evento.domain.ImportanciaEvento;
import com.haya.alaska.importancia_evento.infrastructure.repository.ImportanciaEventoRepository;
import com.haya.alaska.integraciones.gd.ServicioGestorDocumental;
import com.haya.alaska.interviniente.domain.Interviniente;
import com.haya.alaska.interviniente.infrastructure.repository.IntervinienteRepository;
import com.haya.alaska.nivel_evento.domain.NivelEvento;
import com.haya.alaska.nivel_evento.infrastructure.repository.NivelEventoRepository;
import com.haya.alaska.ocupante.domain.Ocupante;
import com.haya.alaska.ocupante.infrastructure.repository.OcupanteRepository;
import com.haya.alaska.perfil.domain.Perfil;
import com.haya.alaska.propuesta.aplication.PropuestaUseCaseImpl;
import com.haya.alaska.propuesta.domain.Propuesta;
import com.haya.alaska.propuesta.infrastructure.repository.PropuestaRepository;
import com.haya.alaska.resultado_evento.domain.ResultadoEvento;
import com.haya.alaska.resultado_evento.infrastructure.controller.dto.ResultadoEventoOutputDTO;
import com.haya.alaska.resultado_evento.infrastructure.repository.ResultadoEventoRepository;
import com.haya.alaska.shared.dto.ListWithCountDTO;
import com.haya.alaska.shared.exceptions.ForbiddenException;
import com.haya.alaska.shared.exceptions.InvalidCodeException;
import com.haya.alaska.shared.exceptions.NotFoundException;
import com.haya.alaska.shared.exceptions.RequiredValueException;
import com.haya.alaska.skip_tracing.aplication.SkipTracingUseCase;
import com.haya.alaska.skip_tracing.domain.SkipTracing;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.subtipo_evento.infrastructure.repository.SubtipoEventoRepository;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.tipo_evento.infrastructure.repository.TipoEventoRepository;
import com.haya.alaska.usuario.domain.Usuario;
import com.haya.alaska.usuario.infrastructure.controller.dto.UsuarioAgendaDto;
import com.haya.alaska.usuario.infrastructure.repository.UsuarioRepository;
import com.haya.alaska.visita.domain.Visita;
import com.haya.alaska.visita_ra.domain.VisitaRA;
import com.haya.alaska.workflow_propuesta.domain.WorkflowElevacionPropuesta;
import com.haya.alaska.workflow_propuesta.infrastructure.repository.WorkflowPropuestaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class EventoUseCaseImpl implements EventoUseCase {
  @Autowired EventoRepository eventoRepository;
  @Autowired WorkflowUtil workflowUtil;
  @Autowired EventoUtil eventoUtil;
  @Autowired EventoMapper eventoMapper;
  @Autowired ClaseEventoRepository claseEventoRepository;
  @Autowired TipoEventoRepository tipoEventoRepository;
  @Autowired SubtipoEventoRepository subtipoEventoRepository;
  @Autowired EstadoEventoRepository estadoEventoRepository;
  @Autowired ImportanciaEventoRepository importanciaEventoRepository;
  @Autowired ResultadoEventoRepository resultadoEventoRepository;
  @Autowired UsuarioRepository usuarioRepository;
  @Autowired NivelEventoRepository nivelEventoRepository;
  @Autowired WorkflowPropuestaRepository workflowPropuestaRepository;
  @Autowired CarteraEstadoPropuestaRepository carteraEstadoPropuestaRepository;
  @Autowired SkipTracingUseCase skipTracingUseCase;
  @Autowired BusquedaUseCaseImpl busquedaUseCase;
  @Autowired ExpedienteRepository expedienteRepository;
  @Autowired ExpedientePosesionNegociadaRepository expedientePosesionNegociadaRepository;
  @Autowired ContratoRepository contratoRepository;
  @Autowired IntervinienteRepository intervinienteRepository;
  @Autowired PropuestaRepository propuestaRepository;
  @Autowired EstadoPropuestaRepository estadoPropuestaRepository;
  @Autowired CarteraRepository carteraRepository;
  @Autowired PropuestaUseCaseImpl propuestaUseCase;
  @Autowired ExpedientePosesionNegociadaCaseImpl expedientePosesionNegociadaCase;
  @Autowired ServicioGestorDocumental servicioGestorDocumental;
  @Autowired OcupanteRepository ocupanteRepository;

  /**
   * Obtiene un evento con el id enviado. Si no lo encuentra en la base de datos, lanza una
   * excepción.
   *
   * @param id: El id de un evento en la base de datos.
   * @return EventoOutputDTO: un objeto DTO, con los datos del evento, para devolver al front.
   * @throws NotFoundException: lanza esta excepción si no se encuentra el evento en la base de
   *     datos.
   */
  @Override
  public EventoOutputDTO findEventoById(Integer id) throws NotFoundException {
    Evento evento =
        eventoRepository.findById(id).orElseThrow(() -> new NotFoundException("Evento", id));
    EventoOutputDTO result = eventoMapper.createEventoOutputDTO(evento);
    return result;
  }

  /**
   * Devuelve un listado de eventos que coincidan con los parámetros pasados como inputs.
   *
   * @param loggedUser: El id del usuario loggeado en la aplicación.
   * @param emisorId: El id del usuario emisor de los eventos.
   * @param destinatarioId: El id del usuario destinatario de los eventos.
   * @param nivelId: El id del 'NivelEvento' de los eventos.
   * @param nivelObjectId: El número de 'IdNivel' de los eventos, que es el id del objeto desde
   *     donde se ha generado el evento.
   * @param fCreaMin: 'FechaCreacion' minima que pueden tener los eventos.
   * @param fCreaMax: 'FechaCreacion' máxima que pueden tener los eventos.
   * @param fAlerMin: 'FechaAlerta' minima que pueden tener los eventos.
   * @param fAlerMax: 'FechaAlerta' máxima que pueden tener los eventos.
   * @param fLimiMin: 'FechaLimite' minima que pueden tener los eventos.
   * @param fLimiMax: 'FechaLimite' máxima que pueden tener los eventos.
   * @param clase: El id de la 'ClaseEvento' de los eventos.
   * @param tipo: El id del 'TipoEvento' de los eventos.
   * @param subtipo: El id del 'SubtipoEvento' de los eventos. Algunas llamadas a este método usan
   *     este parámetro, en vez de 'subtipoList'.
   * @param subtipoList: Una lista de ids de 'SubtipoEvento' que pueden tener los eventos. Algunas
   *     llamadas a este método usan este parámetro, en vez de 'subtipo'.
   * @param cartera: El id de la cartera a la que pertenecen los eventos.
   * @param estado: El id del 'EstadoEvento' de los eventos.
   * @param importancia: El id de la 'ImportanciaEvento' de los eventos.
   * @param resultado: El id del 'ResultadoEvento' de los eventos.
   * @param titulo: String que contienen los títulos de los eventos filtrados.
   * @param activo: Boolean que filtra si los eventos están activos o no.
   * @param revisado: Boolean que filtra si los eventos están revisados o no.
   * @param orderField: String que indica por cual campo ordenar.
   * @param orderDirection: String que indica la dirección de ordenado.
   * @param page: Integer, número de la página.
   * @param size: Integer, tamaño de la página.
   * @return ListWithCountDTO<EventoOutputDTO>: objeto que contiene una lista con una cantidad de
   *     'EventoOutputDTO' igual o menor que el 'page'. Ademas de un Integer, que es la cantidad
   *     máxima de Eventos que cumplen con el filtro.
   * @throws NotFoundException: devuelve esta excepción si algún método interno la lanza.
   */
  @Override
  public ListWithCountDTO<EventoOutputDTO> findAll(
      Integer loggedUser,
      Integer emisorId,
      Integer destinatarioId,
      Integer nivelId,
      Integer nivelObjectId,
      Date fCreaMin,
      Date fCreaMax,
      Date fAlerMin,
      Date fAlerMax,
      Date fLimiMin,
      Date fLimiMax,
      List<Integer> clase,
      Integer tipo,
      Integer subtipo,
      List<Integer> subtipoList,
      Integer cartera,
      List<Integer> estado,
      Integer importancia,
      Integer resultado,
      String titulo,
      Boolean activo,
      Boolean revisado,
      String orderField,
      String orderDirection,
      Integer page,
      Integer size)
      throws NotFoundException {
    // Introduce el id pasado en el subtipo en la lista subtipoList, si la lista está vacia.
    if (subtipoList == null || subtipoList.isEmpty()) {
      subtipoList = new ArrayList<>();
      if (subtipo != null) subtipoList.add(subtipo);
    }
    // Obtiene el listado de los objetos paginados y en DTOs filtrados.
    List<EventoOutputDTO> filteredEvents =
        filteredEvents(
            loggedUser,
            emisorId,
            destinatarioId,
            nivelId,
            nivelObjectId,
            fCreaMin,
            fCreaMax,
            fAlerMin,
            fAlerMax,
            fLimiMin,
            fLimiMax,
            clase,
            tipo,
            subtipoList,
            cartera,
            estado,
            importancia,
            resultado,
            titulo,
            activo,
            revisado,
            orderField,
            orderDirection,
            page,
            size);

    return new ListWithCountDTO<EventoOutputDTO>(
        filteredEvents,
        // Obtiene la cantidad de expedientes que cumplen con el filtrado
        eventoUtil
            .getCantidadEvents(
                loggedUser,
                emisorId,
                destinatarioId,
                nivelId,
                nivelObjectId,
                fCreaMin,
                fCreaMax,
                fAlerMin,
                fAlerMax,
                fLimiMin,
                fLimiMax,
                clase,
                tipo,
                subtipoList,
                cartera,
                estado,
                importancia,
                resultado,
                titulo,
                activo,
                revisado,
                null)
            .intValue());
  }

  /**
   * Devuelve un listado de eventos que coincidan con los parámetros pasados como inputs.
   *
   * @param loggedUser: El id del usuario loggeado en la aplicación.
   * @param emisorId: El id del usuario emisor de los eventos.
   * @param destinatarioId: El id del usuario destinatario de los eventos.
   * @param nivelId: El id del 'NivelEvento' de los eventos.
   * @param nivelObjectId: El número de 'IdNivel' de los eventos, que es el id del objeto desde
   *     donde se ha generado el evento.
   * @param fCreaMin: 'FechaCreacion' minima que pueden tener los eventos.
   * @param fCreaMax: 'FechaCreacion' máxima que pueden tener los eventos.
   * @param fAlerMin: 'FechaAlerta' minima que pueden tener los eventos.
   * @param fAlerMax: 'FechaAlerta' máxima que pueden tener los eventos.
   * @param fLimiMin: 'FechaLimite' minima que pueden tener los eventos.
   * @param fLimiMax: 'FechaLimite' máxima que pueden tener los eventos.
   * @param clase: El id de la 'ClaseEvento' de los eventos.
   * @param tipo: El id del 'TipoEvento' de los eventos. Algunas llamadas a este método usan este
   *     parámetro, en vez de 'subtipoList'.
   * @param subtipoList: Una lista de ids de 'SubtipoEvento' que pueden tener los eventos. Algunas
   *     llamadas a este método usan este parámetro, en vez de 'subtipo'.
   * @param cartera: El id de la cartera a la que pertenecen los eventos.
   * @param estado: El id del 'EstadoEvento' de los eventos.
   * @param importancia: El id de la 'ImportanciaEvento' de los eventos.
   * @param resultado: El id del 'ResultadoEvento' de los eventos.
   * @param titulo: String que contienen los títulos de los eventos filtrados.
   * @param activo: Boolean que filtra si los eventos están activos o no.
   * @param revisado: Boolean que filtra si los eventos están revisados o no.
   * @param orderField: String que indica por cual campo ordenar.
   * @param orderDirection: String que indica la dirección de ordenado.
   * @param page: Integer, número de la página.
   * @param size: Integer, tamaño de la página.
   * @return List<EventoOutputDTO>: objeto que contiene una lista con una cantidad de
   *     'EventoOutputDTO' igual o menor que el 'page'.
   * @throws NotFoundException: devuelve esta excepción si algún método interno la lanza.
   */
  private List<EventoOutputDTO> filteredEvents(
      Integer loggedUser,
      Integer emisorId,
      Integer destinatarioId,
      Integer nivelId,
      Integer nivelObjectId,
      Date fCreaMin,
      Date fCreaMax,
      Date fAlerMin,
      Date fAlerMax,
      Date fLimiMin,
      Date fLimiMax,
      List<Integer> clase,
      Integer tipo,
      List<Integer> subtipoList,
      Integer cartera,
      List<Integer> estado,
      Integer importancia,
      Integer resultado,
      String titulo,
      Boolean activo,
      Boolean revisado,
      String orderField,
      String orderDirection,
      Integer page,
      Integer size)
      throws NotFoundException {
    // Obtiene el listado de eventos filtrados y paginados.
    List<Evento> eventos =
        eventoUtil.getFilteredEventos(
            loggedUser,
            emisorId,
            destinatarioId,
            nivelId,
            nivelObjectId,
            fCreaMin,
            fCreaMax,
            fAlerMin,
            fAlerMax,
            fLimiMin,
            fLimiMax,
            clase,
            tipo,
            subtipoList,
            cartera,
            estado,
            importancia,
            resultado,
            titulo,
            activo,
            revisado,
            null,
            orderField,
            orderDirection,
            page,
            size);
    List<EventoOutputDTO> eventoDTOList = new ArrayList<>();
    if (eventos.size() == 0) {
      return eventoDTOList;
    }

    // Transforma el listado de eventos en EventoOutputDTOs
    for (Evento evento : eventos) {
      eventoDTOList.add(eventoMapper.createEventoOutputDTO(evento));
    }

    return eventoDTOList;
  }

  public List<Integer> todosSubtiposEventoConMismoCodigo(List<Integer> subtiposEvento) {

    List<Integer> idSubtipos = new ArrayList<>();

    for(Integer subtipoEvento: subtiposEvento) {
      SubtipoEvento subtipoInicial = subtipoEventoRepository.findById(subtipoEvento).orElse(null);
      if(subtipoInicial != null) {
        List<SubtipoEvento> subtipos = subtipoEventoRepository.findAllByCodigo(subtipoInicial.getCodigo());
        idSubtipos.addAll(subtipos.stream()
          .map(SubtipoEvento::getId)
          .collect(Collectors.toList()));
      }
    }
    return idSubtipos;
  }

  /**
   * TODO
   *
   * @param loggedUser
   * @param busquedaAgendaDto
   * @param nivelId
   * @param nivelObjectId
   * @param orderField
   * @param orderDirection
   * @param page
   * @param size
   * @return
   * @throws NotFoundException
   */
  @Override
  public ListWithCountDTO<EventoOutputDTO> getListActividad(
      Integer loggedUser,
      BusquedaAgendaDto busquedaAgendaDto,
      Integer nivelId,
      Integer nivelObjectId,
      String orderField,
      String orderDirection,
      Integer page,
      Integer size)
    throws NotFoundException, ParseException {
    if(busquedaAgendaDto.getSubtiposEvento() != null && busquedaAgendaDto.getSubtiposEvento().size() > 0)
      busquedaAgendaDto.setSubtiposEvento(this.todosSubtiposEventoConMismoCodigo(busquedaAgendaDto.getSubtiposEvento()));

    if(busquedaAgendaDto.getIntervaloFechas() != null && busquedaAgendaDto.getIntervaloFechas().size() > 0) {
      List<IntervaloFecha> fechas = new ArrayList<>();
      for(var fecha: busquedaAgendaDto.getIntervaloFechas()) {

        Date inicio = fecha.getFechaInicio();
        Date fin = fecha.getFechaFin();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        inicio = sdf.parse(sdf.format(inicio));
        fin = sdf.parse(sdf.format(fin));
        fin = new Date(fin.getTime() + (1000 * 60 * 60 * 24));
        fecha.setFechaInicio(inicio);
        fecha.setFechaFin(fin);
        fechas.add(fecha);
      }
        busquedaAgendaDto.setIntervaloFechas(fechas);
    }


    List<Evento> eventos =
        eventoUtil.filterListActividad(
            loggedUser, busquedaAgendaDto, nivelId, nivelObjectId, orderField, orderDirection);

    List<Evento> eventosSinDuplicar = new ArrayList<>();

    for(var evento: eventos) {
      if(!eventosSinDuplicar.contains(evento))
        eventosSinDuplicar.add(evento);
    }

    /*List<Evento> eventos2 = new ArrayList<>();
    if(busquedaAgendaDto.getSubtiposEvento() != null && busquedaAgendaDto.getSubtiposEvento().size() > 0) {
      for(var evento: eventos) {
        if(busquedaAgendaDto.getSubtiposEvento().contains(evento.getSubtipoEvento().getId()))
          eventos2.add(evento);
      }
    } else if(busquedaAgendaDto.getTiposEvento() != null && busquedaAgendaDto.getTiposEvento().size() > 0) {
      for(var evento: eventos) {
        if(busquedaAgendaDto.getTiposEvento().contains(evento.getTipoEvento().getId()))
          eventos2.add(evento);
      }
    } else eventos2 = eventos;*/

    List<EventoOutputDTO> result = new ArrayList<>();
    List<Evento> eventosFiltrados =
        eventosSinDuplicar.stream().skip(size * page).limit(size).collect(Collectors.toList());

    for (Evento evento : eventosFiltrados) {
      result.add(eventoMapper.createEventoOutputDTO(evento));
    }

    return new ListWithCountDTO<>(result, eventosSinDuplicar.size());
  }

  /**
   * @param input: Objeto que contiene todos datos para crear un evento.
   * @param usuario: El usuario logueado en la aplicación.
   * @return EventoOutputDTO: Un DTO con los datos del evento recién creado.
   * @throws NotFoundException: devuelve esta excepción si algún método interno la lanza.
   * @throws RequiredValueException: devuelve esta excepción si algún método interno la lanza.
   */
  @Override
  public EventoOutputDTO create(EventoInputDTO input, Usuario usuario)
      throws NotFoundException, RequiredValueException {
    Evento evento;
    // Convierte el DTO de input en un objeto evento
    evento = eventoMapper.createEvento(input, usuario);
    Evento eventoGuardado = this.eventoRepository.save(evento);
    // Comprueba si se cumplen las condiciones para iniciar el Workflow de propuestas
    if (evento.getResultado() != null
        && evento.getNivel().getCodigo().toUpperCase().equals("PRO")) {
      EventoInputDTO newInput = workflowUtil.workflowPropuesta(evento, false);
      if (newInput != null) this.create(newInput, usuario);
    }

    // Crea el output
    EventoOutputDTO result = eventoMapper.createEventoOutputDTO(eventoGuardado);
    return result;
  }

  /**
   * Crea nuevos eventos iguales para diferentes destinatarios.
   *
   * @param input: Objeto que contiene todos datos para crear un evento.
   * @param objetivos: Lista de ids de los usuarios a los que se van a enviar los eventos.
   * @param usuario: El usuario logueado en la aplicación.
   * @return ListWithCountDTO<EventoOutputDTO>: objeto que contiene una lista con una cantidad de
   *     'EventoOutputDTO'. Ademas de un Integer, que es la cantidad máxima de Eventos que cumplen
   *     con el filtro.
   * @throws NotFoundException: devuelve esta excepción si algún método interno la lanza.
   * @throws RequiredValueException: devuelve esta excepción si algún método interno la lanza.
   */
  @Override
  public ListWithCountDTO<EventoOutputDTO> masiveCreate(
      EventoInputDTO input, List<Integer> objetivos, Usuario usuario)
      throws NotFoundException, RequiredValueException {
    List<EventoOutputDTO> outputs = new ArrayList<>();

    // Por cada id enviado, se modifica el Input, se crea el evento, y se guara en La lista que se
    // devuelve.
    for (Integer destinatario : objetivos) {
      input.setDestinatario(destinatario);
      outputs.add(this.create(input, usuario));
    }

    return new ListWithCountDTO<EventoOutputDTO>(outputs, outputs.size());
  }

  /**
   * Se desactiva el evento en la base de datos.
   *
   * @param id: El id de un evento en la base de datos.
   * @throws NotFoundException: lanza esta excepción si no se encuentra el evento en la base de
   *     datos.
   */
  @Override
  public void delete(Integer id) throws NotFoundException {
    Evento evento =
        eventoRepository.findById(id).orElseThrow(() -> new NotFoundException("evento", id));
    evento.setActivo(false);
    eventoRepository.save(evento);
  }

  /**
   * NO SE USA. Se desactivan los eventos cuyas ids esten en la lista de input.
   *
   * @param objetivos: Lista de ids de los eventos que se van a desactivar.
   * @throws NotFoundException: lanza esta excepción si no se encuentra algún evento en la base de
   *     datos.
   */
  @Override
  public void masiveDelete(List<Integer> objetivos) throws NotFoundException {
    for (Integer id : objetivos) {
      this.delete(id);
    }
  }

  /**
   * Modifica el evento con id coincidente.
   *
   * @param eventoId: El id de un evento en la base de datos.
   * @param input: Objeto que contiene todos datos para crear un evento.
   * @param usuario: El usuario logueado en la aplicación.
   * @return EventoOutputDTO: Un DTO con los datos del evento modificado.
   * @throws NotFoundException: lanza esta excepción si no se encuentra algún evento en la base de
   *     datos.
   * @throws RequiredValueException: devuelve esta excepción si algún método interno la lanza.
   * @throws InvalidCodeException: lanza esta excepción si no se puede modificar el evento.
   * @throws IOException: devuelve esta excepción si algún método interno la lanza.
   */
  @Override
  public EventoOutputDTO update(Integer eventoId, EventoInputDTO input, Usuario usuario)
      throws NotFoundException, RequiredValueException, InvalidCodeException, IOException {
    Evento evento =
        eventoRepository
            .findById(eventoId)
            .orElseThrow(() -> new NotFoundException("evento", eventoId));
    Usuario emisor = evento.getEmisor();
    Usuario destinatario = evento.getDestinatario();
    // Si el usuario no es administrador, o no es el emisor o el destinatario, lanzará el error.
    if (!usuario.esAdministrador()
        && (emisor != null && !emisor.getId().equals(usuario.getId()))
        && (destinatario != null && !destinatario.getId().equals(usuario.getId()))) {
      // Control de idioma para el mensaje del error.
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCodeException(
            "You cannot modify an event of which you are not a recipient or sender");
      else
        throw new InvalidCodeException(
            "No se puede modificar un evento del que no eres destinatario o emisor");
    }
    // Si el evento ya tiene resultado, o tiene un estado terminado, lanzará el error.
    if (evento.getResultado() != null
        || (evento.getEstado() != null
            && (evento.getEstado().getCodigo().equals("REA")
                || evento.getEstado().getCodigo().equals("NREA")))) {
      // Control de idioma para el mensaje del error.
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCodeException("Cannot modify a finished event");
      else throw new InvalidCodeException("No se puede modificar un evento finalizado");
    }
    // Si la clase del evento es Comunicación, lanzará el error.
    if (input.getClaseEvento() == 4) {
      // Control de idioma para el mensaje del error.
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new InvalidCodeException("You cannot modify a communication");
      else throw new InvalidCodeException("No se puede modificar una comunicación");
    }
    // Modifica el evento con los datos del input.
    Evento eventoEditado = eventoMapper.modificarEvento(input, evento);
    // Posible inicio de Workflows
    if (eventoEditado.getResultado() != null) {
      // Inicio del workflow de Propuestas
      if (eventoEditado.getNivel().getCodigo().toUpperCase().equals("PRO")) {
        EventoInputDTO newInput = workflowUtil.workflowPropuesta(evento, false);
        if (newInput != null) this.create(newInput, emisor);
        // Inicio del workflow de Procedimientos
      } else if (eventoEditado.getNivel().getCodigo().toUpperCase().equals("JUD")) {
        EventoInputDTO newInput = workflowUtil.wfJudicial(evento, usuario);
        if (newInput != null) this.create(newInput, emisor);
        // Inicio del workflow de Skip Tracing
      } else if (eventoEditado.getNivel().getCodigo().toUpperCase().equals("CON_INT")
          && eventoEditado.getResultado().getCodigo().toUpperCase().equals("OK")) {
        skipTracingUseCase.generarSkipTracing(eventoEditado);
        // Inicio del workflow de Portal Deudor
      } else if (eventoEditado.getSubtipoEvento().getCodigo().toUpperCase().equals("PD_NDC")) {
        if (eventoEditado.getResultado().getCodigo().toUpperCase().equals("OK")) {
          workflowUtil.llamarContrasena(evento);
        } else {
          String[] parts = evento.getTitulo().split(": ");
          String nombreObt = parts[2];
          Integer idDocumentoDNI = Integer.parseInt(nombreObt);
        //  servicioGestorDocumental.eliminarDocumento(idDocumentoDNI);
        }
      }
    }
    // Guarda el evento actualizado
    Evento eventoActualizado = eventoRepository.saveAndFlush(eventoEditado);
    return eventoMapper.createEventoOutputDTO(eventoActualizado);
  }

  // Find Clases
  /**
   * Encuentra las clases activas para la cartera.
   *
   * @param carteraId: el id de la cartera en la que buscar las clases.
   * @return List<CatalogoMinInfoDTO>: una lista de los DTOs de las clases.
   * @throws NotFoundException: devuelve esta excepción si algún método interno la lanza.
   */
  @Override
  public List<CatalogoMinInfoDTO> findClaseFiltrado(Integer carteraId) throws NotFoundException {
    List<ClaseEvento> clases = claseEventoRepository.findAll();

    List<ClaseEvento> filtrados = new ArrayList<>();

    // Por cada clase, busca los tiposEvento que están activos. Si no hay ninguno, no devuelve la
    // clase.
    for (ClaseEvento ce : clases) {
      List<TipoEvento> tipos = findTipoByPadreAndCartera(ce.getId(), carteraId);
      if (!tipos.isEmpty()) filtrados.add(ce);
    }

    List<CatalogoMinInfoDTO> result =
        filtrados.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
    return result;
  }

  // Find Tipos
  /**
   * Encuentra todos los tipos que pertenezcan a la clase.
   *
   * @param idPadre: el id de la CaseEvento a la que pertenecen los TiposEvento.
   * @return List<CatalogoMinInfoDTO>: una lista de los DTOs de los tipos.
   * @throws NotFoundException: devuelve esta excepción si algún método interno la lanza.
   */
  @Override
  public List<CatalogoMinInfoDTO> findTipo(Integer idPadre) throws NotFoundException {
    List<TipoEvento> catalogos = findTipoByPadre(idPadre);
    //Filtro flujo de propuesta
    catalogos =
      catalogos.stream()
        .filter(te -> !te.getValor().toLowerCase().startsWith("flujo propuesta"))
        .collect(Collectors.toList());
    List<CatalogoMinInfoDTO> result =
        catalogos.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public List<CatalogoMinInfoDTO> findTipos (List<Integer> tipos) throws NotFoundException {
    List<TipoEvento> catalogos = new ArrayList<>();
    for(Integer idTipo: tipos) {
      catalogos.addAll(findTipos(idTipo));
    }

    List<CatalogoMinInfoDTO> result =
      catalogos.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
    return result;
  }

  /**
   * Encuentra los Tipos Eventos activas para la cartera y la clase y los devuelve como DTOs.
   *
   * @param idPadre: id de la Clase Evento a la que pertenecen los Tipos Eventos.
   * @param carteraId: id de la Cartera en la que los tipos están activos.
   * @return List<CatalogoMinInfoDTO>: una lista de los DTOs de los tipos activos.
   * @throws NotFoundException: devuelve esta excepción si algún método interno la lanza.
   */
  @Override
  public List<CatalogoMinInfoDTO> findTipoFiltrado(Integer idPadre, Integer carteraId)
      throws NotFoundException {
    // Filtra los tipos por si están acticos en la cartera.
    List<TipoEvento> catalogos = findTipoByPadreAndCartera(idPadre, carteraId);

    List<CatalogoMinInfoDTO> result =
        catalogos.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
    return result;
  }

  /**
   * Encuentra los tipos evento, excepto las alertas automáticas, pertenecientes a una clase.
   *
   * @param idPadre: id de la Clase Evento a la que pertenecen los Tipos.
   * @return List<TipoEvento>: la lista de los tipos filtrados.
   * @throws NotFoundException: devuelve esta excepción si algún método interno la lanza, o su no
   *     hay en la base de datos una clase con Código "ALE"
   */
  private List<TipoEvento> findTipoByPadre(Integer idPadre) throws NotFoundException {
    ClaseEvento ce = claseEventoRepository.findByCodigo("ALE").orElse(null);
    // Comprueba si la clase de Alerta existe en la Base de Datos
    if (ce == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("There is no event class for alerts.");
      else throw new NotFoundException("No existe una clase evento para alertas.");
    }
    List<TipoEvento> catalogos =
        this.tipoEventoRepository.findByClaseEventoIdAndActivoIsTrue(idPadre);

    // Filtra la Alerta Automática
    if (idPadre.equals(ce.getId()))
      catalogos =
          catalogos.stream()
              .filter(te -> !te.getCodigo().equals("ALE_02"))
              .collect(Collectors.toList());


    return catalogos;
  }

  private List<TipoEvento> findTipos(Integer idPadre) throws NotFoundException {
    ClaseEvento ce = claseEventoRepository.findByCodigo("ALE").orElse(null);
    // Comprueba si la clase de Alerta existe en la Base de Datos
    if (ce == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException("There is no event class for alerts.");
      else throw new NotFoundException("No existe una clase evento para alertas.");
    }
    List<TipoEvento> catalogos =
      this.tipoEventoRepository.findByClaseEventoIdAndActivoIsTrue(idPadre);

    return catalogos;
  }

  /**
   * Encuentra los tipos evento activos de la cartera.
   *
   * @param idPadre: id de la Clase Evento a la que pertenecen los Tipos.
   * @param carteraId: el id de la cartera en la que buscar los Tipos.
   * @return List<TipoEvento>: la lista de los tipos filtrados.
   * @throws NotFoundException: devuelve esta excepción si algún método interno la lanza.
   */
  private List<TipoEvento> findTipoByPadreAndCartera(Integer idPadre, Integer carteraId)
      throws NotFoundException {
    List<TipoEvento> catalogos = findTipoByPadre(idPadre);

    List<TipoEvento> filtrados = new ArrayList<>();

    for (TipoEvento te : catalogos) {
      List<SubtipoEvento> subtipos =
          this.subtipoEventoRepository.findByActivoIsTrueAndTipoEventoIdAndCarteraId(
              te.getId(), carteraId);
      if (!subtipos.isEmpty()) filtrados.add(te);
    }

    return filtrados;
  }

  // Find Subtipos
  /**
   * Encuentra los Subtipos Evento activos por cartera y Tipo Evento. Si la cartera es null, busca
   * los subtipos Evento no asociados a ninguna cartera.
   *
   * @param idPadre: id de la Tipo Evento a la que pertenecen los Subtipos.
   * @param usuario: el usuario logueado.
   * @param carteraId: el id de la cartera en la que buscar los Subtipos.
   * @return List<SubtipoEventoOutputDTO>: lista de los DTOs transformados.
   */
  @Override
  public List<SubtipoEventoOutputDTO> findSubtipo(
      Integer idPadre, Usuario usuario, Integer carteraId) {
    List<SubtipoEvento> catalogos;
    if (carteraId != null)
      catalogos =
          this.subtipoEventoRepository.findByActivoIsTrueAndTipoEventoIdAndCarteraId(
              idPadre, carteraId);
    else
      catalogos =
          this.subtipoEventoRepository.findByActivoIsTrueAndTipoEventoIdAndCarteraIsNull(idPadre);

    List<SubtipoEventoOutputDTO> result =
        catalogos.stream().map(SubtipoEventoOutputDTO::new).collect(Collectors.toList());
    return result;
  }

  @Override
  public List<SubtipoEventoOutputDTO> findSubtipos (List<Integer> tipos, Usuario usuario, Integer carteraId) {
    List<SubtipoEvento> catalogos;
    if (carteraId != null)
      catalogos =
        this.subtipoEventoRepository.findByActivoIsTrueAndCarteraIdAndTipoEventoIdIn(
          carteraId, tipos);
    else
      catalogos =
        this.subtipoEventoRepository.findByActivoIsTrueAndCarteraIsNullAndTipoEventoIdIn(tipos);

    List<SubtipoEventoOutputDTO> result =
      catalogos.stream().map(SubtipoEventoOutputDTO::new).collect(Collectors.toList());
    return result;
  }

  // Find Resultados

  /**
   * Encuentra los resultados asociados a un Subtipo Evento.
   *
   * @param idSubtipo: id del subtipo evento al que están asociados los resultados.
   * @param idEvento: id del evento en el que se está comprobado los resultados.
   * @return List<CatalogoMinInfoDTO>: lista de DTOs transformados de los resultados.
   * @throws NotFoundException: lanza esta excepción si no encuentra el subtipo, el evento, o si
   *     algún método interno la lanza.
   */
  @Override
  public List<CatalogoMinInfoDTO> findResultados(Integer idSubtipo, Integer idEvento)
      throws NotFoundException {
    SubtipoEvento se =
        subtipoEventoRepository
            .findById(idSubtipo)
            .orElseThrow(() -> new NotFoundException("SubtipoEvento", idSubtipo));
    // Si el id del evento está informado, empiezan filtros internos.
    if (idEvento != null) {
      Evento evento =
          eventoRepository
              .findById(idEvento)
              .orElseThrow(
                  () -> new NotFoundException("Evento", idEvento)); // Evento revisión gestor
      // Si el evento es de Propuesta, es de Subtipo Revisión Gestor, y tiene un evento padre
      // asociado; entra.
      if (evento.getNivel().getCodigo().equals("PRO")
          && evento.getSubtipoEvento().getCodigo().equals("REV09")
          && evento.getEventoPadre() != null) {
        Evento padre = evento.getEventoPadre();
        // Este filtro es para devolver solo un resultado
        if (workflowUtil.getFirmaToGestor(padre, padre.getResultado())) {
          List<ResultadoEvento> catalogos = new ArrayList<>();
          ResultadoEvento re =
              resultadoEventoRepository
                  .findByCodigo("COMP")
                  .orElseThrow(() -> new NotFoundException("Resultado Evento", "Codigo", "COMP"));
          catalogos.add(re);
          return catalogos.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
        }
        if (padre.getResultado() != null) {
          String resultadoPadre = padre.getResultado().getCodigo();
          // Comprueba que el resultado del padre sea Autorizado Condicionado, para añadir un
          // resultado. Mecánica de Workflow.
          if (resultadoPadre.equals("AU_CO") || workflowUtil.getEspecial(padre)) {
            List<ResultadoEvento> catalogos =
                this.resultadoEventoRepository.findBySubtipos_Id(idSubtipo);
            catalogos = comprobarResultados(catalogos, se, idEvento);
            return catalogos.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
          }
        }
      }
    }
    // Si no se cumplen las condiciones anteriores, devuelve los resultados correspondientes,
    // excepto Aceptar.
    List<ResultadoEvento> catalogos = this.resultadoEventoRepository.findBySubtipos_Id(idSubtipo);
    List<ResultadoEvento> catalogosFiltrados =
        catalogos.stream()
            .filter(cata -> !cata.getCodigo().equals("ACEP"))
            .collect(Collectors.toList());
    catalogosFiltrados = comprobarResultados(catalogosFiltrados, se, idEvento);
    List<CatalogoMinInfoDTO> result =
        catalogosFiltrados.stream().map(CatalogoMinInfoDTO::new).collect(Collectors.toList());
    return result;
  }

  /**
   * Hace comprobaciones adicionales de los Resultados Evento.
   *
   * @param resultados: Lista de Resultados a filtrar.
   * @param subtipo: el subtipo cuyos resultados se han obtenido.
   * @param idEvento: el id del evento donde se están buscando los resultados.
   * @return List<ResultadoEvento>: lista de resultados filtrados.
   * @throws NotFoundException: lanza esta excepción si no encuentra algún objeto en la base de
   *     datos.
   */
  private List<ResultadoEvento> comprobarResultados(
      List<ResultadoEvento> resultados, SubtipoEvento subtipo, Integer idEvento)
      throws NotFoundException {
    Evento e = null;
    if (idEvento != null)
      e =
          eventoRepository
              .findById(idEvento)
              .orElseThrow(() -> new NotFoundException("Evento", idEvento));

    List<ResultadoEvento> result = new ArrayList<>();
    // Si el subtipo no está asociado a ninguna cartera (evento de la agenda general), no hay
    // filtros que aplicar
    if (subtipo.getCartera() == null) return resultados;

    for (ResultadoEvento re : resultados) {
      // Si el resultado coincide con alguno de estos códigos, o si es el código aceptar y la
      // cartera del subtipo es de Cajamar; lo añade a los resultados directamente.
      if (re.getCodigo().equals("FORM")
          || re.getCodigo().equals("ELEV")
          || re.getCodigo().equals("AUTO")
          || re.getCodigo().equals("COMP")
          || (re.getCodigo().equals("ACEP")
              && workflowUtil.getCarterasCajamar().contains(subtipo.getCartera()))) {
        result.add(re);
        continue;
      }
      // Si el evento es de nivel propuesta, y se cumplen condiciones entre los eventos y
      // resultados, lo añade a los resultados directamente.
      if (e != null
          && e.getNivel().getCodigo().equals("PRO")
          && workflowUtil.getFirmaToGestor(e, re)) {
        result.add(re);
        continue;
      }
      // Se obtienen los posibles workflows
      List<WorkflowElevacionPropuesta> wepList =
          workflowPropuestaRepository.findAllByEventoOrigenIdAndResultadoEventoIdAndCarteraId(
              subtipo.getId(), re.getId(), subtipo.getCartera().getId());
      if (wepList.size() > 0) {
        WorkflowElevacionPropuesta wep = wepList.get(0);
        // Si el workflow existe y el evento de destino es Pendiente Formalización, estos códigos
        // tienen lógicas en las que, si uno de los dos están activos, la configuración del workflow
        // correspondiente está activa.
        if (wep.getEventoDestino() != null && wep.getEventoDestino().getCodigo().equals("REV06")) {
          if (e != null) {
            Propuesta propuesta = propuestaRepository.findById(e.getIdNivel()).orElse(null);
            if (propuesta != null) {
              EstadoPropuesta estado;
              if (propuesta.getSubtipoPropuesta().getPbc()) {
                estado = estadoPropuestaRepository.findByCodigo("PDFP").orElse(null);
                propuesta.setEstadoPropuesta(estado);
              } else {
                estado = estadoPropuestaRepository.findByCodigo("PDF").orElse(null);
                propuesta.setEstadoPropuesta(estado);
              }

              CarteraEstadoPropuesta cep =
                  carteraEstadoPropuestaRepository.findByCarteraIdAndEstadoId(
                      wep.getCartera().getId(), estado.getId());
              wep.setActivo(cep.getActivo());
            }
          }
        }
        // Si al final de la lógica, el workflow está activo, se añade el resultado
        if (wep.getActivo()) result.add(re);
      } else {
        result.add(re);
      }
    }
    return result;
  }

  // Esto a Dto //Se obtiene el resultado a partir de la relacion entre subtipo evento y
  // resultadoEvento
  @Override
  public List<ResultadoEventoOutputDTO> findResultadoEventoBySubTipoEvento(
      Integer subtipoeventoId) {
    List<ResultadoEvento> resultadoEventoList =
        resultadoEventoRepository.findBySubtipos_Id(subtipoeventoId);
    List<ResultadoEventoOutputDTO> resultadoEventoOutputDTOList = new ArrayList<>();
    for (ResultadoEvento resultado : resultadoEventoList) {
      ResultadoEventoOutputDTO resultadoEventoOutputDTO = new ResultadoEventoOutputDTO(resultado);
      resultadoEventoOutputDTOList.add(resultadoEventoOutputDTO);
    }
    return resultadoEventoOutputDTOList;
  }

  // Otros
  @Override
  public UsuarioAgendaDto userByExpedienteAndPerfil(
      Integer idExpediente, Integer idPerfil, Boolean pn) throws NotFoundException {
    Usuario user;
    if (pn) user = workflowUtil.userByPerfilPN(idExpediente, idPerfil);
    else user = workflowUtil.userByPerfil(idExpediente, idPerfil);
    if (user == null) return null;
    return new UsuarioAgendaDto(user);
  }

  @Override
  public void elevarPropuesta(Integer idPropuesta, Usuario emisor, Integer idCartera)
      throws NotFoundException, RequiredValueException, InvalidCodeException {

    Evento evento = new Evento();
    Propuesta propuesta =
        propuestaRepository
            .findById(idPropuesta)
            .orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));

    Cartera cartera =
        carteraRepository
            .findById(idCartera)
            .orElseThrow(() -> new NotFoundException("Cartera", idCartera));
    evento.setCartera(cartera);

    if (propuesta.getFechaValidez() == null) {
      propuesta = propuestaUseCase.actualizarFechaValidez(propuesta);
    }

    Integer idExpediente = 0;
    if (propuesta.getExpediente() != null) idExpediente = propuesta.getExpediente().getId();
    else if (propuesta.getExpedientePosesionNegociada() != null)
      idExpediente = propuesta.getExpedientePosesionNegociada().getId();
    evento.setIdExpediente(idExpediente);

    if (propuesta.getExpediente() != null) {
      evento.setCartera(propuesta.getExpediente().getCartera());
    } else if (propuesta.getExpedientePosesionNegociada() != null) {
      evento.setCartera(propuesta.getExpedientePosesionNegociada().getCartera());
    }

    EstadoPropuesta estadoPropuesta = propuesta.getEstadoPropuesta();
    if (estadoPropuesta != null) {
      String codigo = estadoPropuesta.getCodigo();
      if (!codigo.equals("EST")
          && !codigo.equals("PDP")
          && !codigo.equals("DEN")
          && !codigo.equals("DES")
          && !codigo.equals("CAD")
          && !codigo.equals("ANU")) {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new InvalidCodeException(
              "You cannot raise a proposal that is already being raised, or is already formalized.");
        else
          throw new InvalidCodeException(
              "No se puede elevar una propuesta que ya se está elevando, o ya esté formalizada.");
      }
    }

    evento.setTitulo("Elevación Propuesta: " + idPropuesta);
    evento.setComentariosDestinatario(
        "Evento de elevación de la propuesta de id: "
            + idPropuesta
            + ", realizada por el usuario: "
            + emisor.getNombre());
    Date now = Calendar.getInstance().getTime();
    Calendar c = Calendar.getInstance();
    c.setTime(now);
    c.add(Calendar.DATE, 1);
    evento.setFechaAlerta(c.getTime());
    c.add(Calendar.YEAR, 100);
    evento.setFechaLimite(c.getTime());

    SubtipoEvento sub =
        subtipoEventoRepository.findByCodigoAndCarteraId("PROP06", idCartera).orElse(null);
    evento.setSubtipoEvento(sub);
    evento.setTipoEvento(sub.getTipoEvento());
    evento.setClaseEvento(sub.getTipoEvento().getClaseEvento());

    evento.setEstado(estadoEventoRepository.findByCodigo("PEN").orElse(null));
    evento.setImportancia(importanciaEventoRepository.findByCodigo("ALT").orElse(null));
    evento.setResultado(resultadoEventoRepository.findByCodigo("ELEV").orElse(null));

    evento.setAutogenerado(true);
    evento.setComentariosDestinatario("Flujo Propuestas");
    evento.setNivel(nivelEventoRepository.findByCodigo("PRO").orElse(null));
    evento.setIdNivel(idPropuesta);
    EventoInputDTO newInput = workflowUtil.workflowPropuesta(evento, false);
    if (newInput != null) this.create(newInput, emisor);
  }

  @Override
  public Boolean getAuthorization(Integer subtipoId, Usuario usuario) throws NotFoundException {
    Perfil perfilUsu = usuario.getPerfil();
    SubtipoEvento stE =
        subtipoEventoRepository
            .findById(subtipoId)
            .orElseThrow(() -> new NotFoundException("SubtipoEvento", subtipoId));

    Perfil perfilSub = stE.getLimiteEmisor();
    if (perfilSub == null) return true;
    else return perfilSub.getId().equals(perfilUsu.getId());
  }

  @Override
  public List<UsuarioAgendaDto> getExpedientUsers(Integer idExpediente, Boolean pn)
      throws NotFoundException {
    List<Usuario> usuarios;
    if (pn) usuarios = usersInExpedientePN(idExpediente);
    else usuarios = usersInExpediente(idExpediente);

    List<UsuarioAgendaDto> result =
        usuarios.stream().map(UsuarioAgendaDto::new).collect(Collectors.toList());
    return result;
  }

  private List<Usuario> usersInExpediente(Integer idExpediente) throws NotFoundException {
    Expediente expediente =
        expedienteRepository
            .findById(idExpediente)
            .orElseThrow(() -> new NotFoundException("expediente", idExpediente));

    List<AsignacionExpediente> asignaciones = new ArrayList<>(expediente.getAsignaciones());
    List<Usuario> usuarios =
        asignaciones.stream().map(AsignacionExpediente::getUsuario).collect(Collectors.toList());
    return usuarios;
  }

  private List<Usuario> usersInExpedientePN(Integer idExpediente) throws NotFoundException {
    ExpedientePosesionNegociada expediente =
        expedientePosesionNegociadaRepository
            .findById(idExpediente)
            .orElseThrow(() -> new NotFoundException("expedientePosesionNegociada", idExpediente));

    List<AsignacionExpedientePosesionNegociada> asignaciones =
        new ArrayList<>(expediente.getAsignaciones());
    List<Usuario> usuarios =
        asignaciones.stream()
            .map(AsignacionExpedientePosesionNegociada::getUsuario)
            .collect(Collectors.toList());
    return usuarios;
  }

  // Un endpoint por cartera
  @Override
  public List<CarteraEstadoPropuestaOutputDTO> getEstadoActivo(Integer carteraId) {
    List<CarteraEstadoPropuesta> carteraEstadoPropuestalist =
        carteraEstadoPropuestaRepository.findAllByCarteraId(carteraId);
    List<CarteraEstadoPropuestaOutputDTO> listCarteraEstadoPropuestaDTO = new ArrayList<>();
    for (CarteraEstadoPropuesta carteraEstadoPropuesta : carteraEstadoPropuestalist) {
      CarteraEstadoPropuestaOutputDTO carteraEstadoPropuestaOutputDTO =
          new CarteraEstadoPropuestaOutputDTO(carteraEstadoPropuesta);
      listCarteraEstadoPropuestaDTO.add(carteraEstadoPropuestaOutputDTO);
    }
    List<String> inModificables = eventoUtil.inborrables();
    Comparator<CarteraEstadoPropuestaOutputDTO> comparator =
        Comparator.comparing(c -> c.getEstado().getId());
    listCarteraEstadoPropuestaDTO.sort(comparator);
    for (CarteraEstadoPropuestaOutputDTO c : listCarteraEstadoPropuestaDTO) {
      if (inModificables.contains(c.getEstado().getCodigo())) c.setActivo(null);
    }
    return listCarteraEstadoPropuestaDTO;
  }

  @Override
  public List<SubtipoEventoOutputDTO> findByCartera(Integer carteraId, Integer estadoPropuestaId)
      throws NotFoundException {

    // buscas entre esos workflows los eventos destinatarios que tambien tengan ese estado propuesta
    EstadoPropuesta ep =
        estadoPropuestaRepository
            .findById(estadoPropuestaId)
            .orElseThrow(() -> new NotFoundException("estadoPropuesta", estadoPropuestaId));
    if (ep.getCodigo().equals("PDFP"))
      ep =
          estadoPropuestaRepository
              .findByCodigo("PDF")
              .orElseThrow(() -> new NotFoundException("estadoPropuesta", "código", "'PDF'"));
    List<WorkflowElevacionPropuesta> listawork =
        workflowPropuestaRepository.findAllByCarteraIdAndEstadoPropuestaId(carteraId, ep.getId());

    List<SubtipoEventoOutputDTO> subtipoEventoOutputList = new ArrayList<>();
    List<SubtipoEvento> subtipos = new ArrayList<>();

    for (WorkflowElevacionPropuesta workflow : listawork) {
      SubtipoEvento subtipoEvento = workflow.getEventoDestino();
      subtipos.add(subtipoEvento);
    }
    if (!subtipos.isEmpty())
      subtipoEventoOutputList =
          subtipos.stream()
              .distinct()
              .map(SubtipoEventoOutputDTO::new)
              .collect(Collectors.toList());
    return subtipoEventoOutputList;
  }

  @Override
  public void updateWorkflow(WorkflowInputDTO input) throws NotFoundException {
    List<WorkflowElevacionPropuesta> workflows = new ArrayList<>();
    Integer c = input.getIdCartera();
    for (EstadoCarteraInputDTO in : input.getEstados()) {
      EstadoPropuesta ep =
          estadoPropuestaRepository
              .findById(in.getId())
              .orElseThrow(() -> new NotFoundException("estado Propuesta", in.getId()));
      String epS = ep.getCodigo();
      List<String> inmodificables = eventoUtil.inborrables();
      if (inmodificables.contains(epS)) in.setActivo(true);
      CarteraEstadoPropuesta cep;

      cep = carteraEstadoPropuestaRepository.findByCarteraIdAndEstadoId(c, ep.getId());
      cep.setActivo(in.getActivo());
      carteraEstadoPropuestaRepository.save(cep);

      if ((epS.equals("PDF") || epS.equals("PDFP")) && !in.getActivo()) {
        EstadoPropuesta ep1 =
            estadoPropuestaRepository
                .findByCodigo("PDF")
                .orElseThrow(() -> new NotFoundException("estado Propuesta", "código", "'PDF'"));
        CarteraEstadoPropuesta cep1 =
            carteraEstadoPropuestaRepository.findByCarteraIdAndEstadoId(c, ep1.getId());
        EstadoPropuesta ep2 =
            estadoPropuestaRepository
                .findByCodigo("PDFP")
                .orElseThrow(() -> new NotFoundException("estado Propuesta", "código", "'PDFP'"));
        CarteraEstadoPropuesta cep2 =
            carteraEstadoPropuestaRepository.findByCarteraIdAndEstadoId(c, ep2.getId());
        if (cep1.getActivo() || cep2.getActivo()) {
          continue;
        }
      }

      List<WorkflowElevacionPropuesta> wfs =
          workflowPropuestaRepository.findAllByCarteraIdAndEstadoPropuestaId(c, ep.getId());

      for (WorkflowElevacionPropuesta wep : wfs) {
        wep.setActivo(in.getActivo());
        workflows.add(wep);
      }
    }
    workflowPropuestaRepository.saveAll(workflows);
  }

  // Tengo que devolver los eventos Destino //SSacar su valor
  @Override
  public List<SubtipoEventoOutputDTO> findWorkflowCarteraResultadoEventoAndSuBtipoEvento(
      Integer carteraId, Integer resultadoEventoId, Integer eventoOrigenId) {

    List<WorkflowElevacionPropuesta> listWorkflowOut =
        workflowPropuestaRepository.findAllByEventoOrigenIdAndResultadoEventoIdAndCarteraId(
            eventoOrigenId, resultadoEventoId, carteraId);
    List<SubtipoEvento> eventoDestinoList = new ArrayList<>();
    List<SubtipoEventoOutputDTO> eventoOutputDTOList = new ArrayList<>();
    try {

      if (listWorkflowOut != null) {

        WorkflowElevacionPropuesta wep = listWorkflowOut.get(0);
        // Pillamos el subtipo evento Destino
        if (wep.getEventoDestino() != null) {
          SubtipoEvento newSubtipoEvento = wep.getEventoDestino();
          eventoDestinoList.add(newSubtipoEvento);
        }
        // Saca esas 4
        if (wep.getEventoOrigen().getCodigo().equals("REV09")
            && wep.getResultadoEvento().getCodigo().equals("ACEP")) {
          //    SubtipoEvento subtipoAntes=evento.getEventoPadre().getSubtipoEvento();

          SubtipoEvento sub1 =
              subtipoEventoRepository.findByCodigoAndCarteraId("REV05", carteraId).orElse(null);
          SubtipoEvento sub2 =
              subtipoEventoRepository.findByCodigoAndCarteraId("REV04", carteraId).orElse(null);
          SubtipoEvento sub3 =
              subtipoEventoRepository.findByCodigoAndCarteraId("REV07", carteraId).orElse(null);
          SubtipoEvento sub4 =
              subtipoEventoRepository.findByCodigoAndCarteraId("REV06", carteraId).orElse(null);
          eventoDestinoList.add(sub1);
          eventoDestinoList.add(sub2);
          eventoDestinoList.add(sub3);
          eventoDestinoList.add(sub4);
          // eventoDestinoList.add(wep.getEventoDestino());
        } else {

        }
      }
    } catch (Exception e) {
      return new ArrayList<>();
    }

    for (SubtipoEvento sub : eventoDestinoList) {
      SubtipoEventoOutputDTO subtipoEventoOutputDTO = new SubtipoEventoOutputDTO(sub);
      eventoOutputDTOList.add(subtipoEventoOutputDTO);
    }
    return eventoOutputDTOList;
  }

  @Override
  public void solicitarSkipTracing(Integer idInter, Integer idCont, Usuario creador)
      throws NotFoundException, RequiredValueException, ForbiddenException {
    EventoInputDTO newIn = new EventoInputDTO();

    Interviniente i =
        intervinienteRepository
            .findById(idInter)
            .orElseThrow(() -> new NotFoundException("Interviniente", idInter));
    ContratoInterviniente ci = i.getContratoInterviniente(idCont);
    Contrato c = ci.getContrato();
    Expediente e = c.getExpediente();
    Cartera ca = e.getCartera();

    skipTracingUseCase.comprobarCreados(i);

    SubtipoEvento se =
        subtipoEventoRepository
            .findByCodigoAndCarteraId("REV08", ca.getId())
            .orElseThrow(() -> new NotFoundException("subtipoEvento", "código", "'REV08'"));
    ImportanciaEvento ie =
        importanciaEventoRepository
            .findByCodigo("MED")
            .orElseThrow(() -> new NotFoundException("importanciaEvento", "código", "'MED'"));
    TipoEvento te = se.getTipoEvento();
    ClaseEvento ce = te.getClaseEvento();
    Perfil perDest = se.getLimiteDestinatario();
    if (perDest == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException(
            "There is no profile associated with the subtypeEvent as a recipient.");
      else
        throw new NotFoundException(
            "No existe un perfil asociado al subtipoEvento como destinatario.");
    }
    Usuario dest = e.getUsuarioByPerfil(perDest.getNombre());
    if (dest == null) {
      Locale loc = LocaleContextHolder.getLocale();
      if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
        throw new NotFoundException(
            "A user assigned to the file of the proposal with profile has not been found: "
                + perDest.getNombre());
      else
        throw new NotFoundException(
            "No se ha encontrado un usuario asignado al expediente de la propuesta con perfil: "
                + perDest.getNombre());
    }

    NivelEvento ne =
        nivelEventoRepository
            .findByCodigo("CON_INT")
            .orElseThrow(() -> new NotFoundException("Nivel Evento", "código", "'CON_INT'"));

    newIn.setCartera(ca.getId());
    newIn.setTitulo("Solicitud ST interviniente:" + idInter);

    newIn.setFechaCreacion(new Date());
    newIn.setFechaAlerta(new Date());
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, 1);
    newIn.setFechaLimite(calendar.getTime());

    newIn.setClaseEvento(ce.getId());
    newIn.setTipoEvento(te.getId());
    newIn.setSubtipoEvento(se.getId());

    newIn.setDestinatario(dest.getId());
    newIn.setImportancia(ie.getId());
    newIn.setAutogenerado(true);

    newIn.setNivel(ne.getId());
    newIn.setNivelId(ci.getId());
    newIn.setIdExpediente(e.getId());

    this.create(newIn, creador);
  }

  @Override
  public void eventoCierreST(SkipTracing st, Usuario creador, Usuario destinatario)
      throws NotFoundException, RequiredValueException {
    EventoInputDTO newIn = new EventoInputDTO();

    ContratoInterviniente ci = st.getContratoInterviniente();
    Contrato c = ci.getContrato();
    Expediente e = c.getExpediente();
    Cartera ca = e.getCartera();
    Interviniente i = ci.getInterviniente();

    String nombreResult = "";
    String nombre = i.getNombre() != null ? i.getNombre() : "";
    String apellidos = i.getApellidos() != null ? i.getApellidos() : "";
    if (i.getPersonaJuridica() != null && i.getPersonaJuridica()) {
      nombreResult = i.getRazonSocial() != null ? i.getRazonSocial() : "";
    } else {
      nombreResult = nombre + " " + apellidos;
    }

    SubtipoEvento se =
        subtipoEventoRepository
            .findByCodigoAndCarteraId("COM_ST", ca.getId())
            .orElseThrow(() -> new NotFoundException("subtipoEvento", "código", "'COM_ST'"));
    ImportanciaEvento ie =
        importanciaEventoRepository
            .findByCodigo("ALT")
            .orElseThrow(() -> new NotFoundException("importanciaEvento", "código", "'ALT'"));
    TipoEvento te = se.getTipoEvento();
    ClaseEvento ce = te.getClaseEvento();

    if (destinatario == null) {
      newIn.setTitulo("Cierre ST de Interviniente:" + i.getId());
      newIn.setComentariosDestinatario(
          "El SkipTracing de id: "
              + st.getId()
              + " se ha cerrado, para el interviniente: "
              + i.getId()
              + " "
              + nombreResult
              + ", con el resultado "
              + st.getEstadoST().getValor());

      destinatario = e.getUsuarioByPerfil("Gestor");
      if (destinatario == null) throw new NotFoundException("Gestor", "expediente", e.getId());
    } else {
      newIn.setTitulo("Asignación de ST: " + st.getId());
      newIn.setComentariosDestinatario(
          "El SkipTracing de id: "
              + st.getId()
              + " se ha asociado, para el interviniente: "
              + i.getId()
              + " "
              + nombreResult
              + ", en el nivel "
              + st.getNivelST().getValor());
    }

    Usuario dest = destinatario;

    NivelEvento ne =
        nivelEventoRepository
            .findByCodigo("SK_TR")
            .orElseThrow(() -> new NotFoundException("Nivel Evento", "código", "'SK_TR'"));

    newIn.setCartera(ca.getId());

    newIn.setFechaCreacion(new Date());
    newIn.setFechaAlerta(new Date());
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, 1);
    newIn.setFechaLimite(calendar.getTime());

    newIn.setClaseEvento(ce.getId());
    newIn.setTipoEvento(te.getId());
    newIn.setSubtipoEvento(se.getId());

    newIn.setDestinatario(dest.getId());
    newIn.setImportancia(ie.getId());
    newIn.setAutogenerado(true);

    newIn.setNivel(ne.getId());
    newIn.setNivelId(ci.getId());
    newIn.setIdExpediente(e.getId());

    this.create(newIn, creador);
  }

  @Override
  public Integer eventoFromPropuesta(Integer propuestaId) {
    return eventoUtil.eventoFromPropuesta(propuestaId);
  }

  @Override
  public void crearEventoVisita(Visita visita, Usuario creador)
      throws NotFoundException, RequiredValueException {
    BienPosesionNegociada bpn = visita.getBienPosesionNegociada();
    ExpedientePosesionNegociada epn = bpn.getExpedientePosesionNegociada();
    Cartera c = epn.getCartera();
    EventoInputDTO newInput = new EventoInputDTO();

    Usuario user =
        usuarioRepository
            .findById(creador.getId())
            .orElseThrow(() -> new NotFoundException("usuario", creador.getId()));

    SubtipoEvento se =
        subtipoEventoRepository
            .findByCodigoAndCarteraId("VIS01", c.getId())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "subtipoEvento", "código", "'VIS01'", "cartera", c.getNombre()));
    TipoEvento te = se.getTipoEvento();
    ClaseEvento ce = te.getClaseEvento();

    newInput.setSubtipoEvento(se.getId());
    newInput.setTipoEvento(te.getId());
    newInput.setClaseEvento(ce.getId());
    newInput.setIdExpediente(epn.getId());

    newInput.setImportancia(2); // Media importancia
    // Revisamos el estado evento pendiente
    EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("REA").orElse(null);
    if (estadoEvento == null) throw new NotFoundException("estado evento", "código", "'REA'");
    newInput.setEstado(estadoEvento.getId());
    newInput.setResultado(null);

    if (visita.getFecha() != null) {
      Calendar cAlerta = Calendar.getInstance();
      cAlerta.setTime(visita.getFecha());
      newInput.setFechaAlerta(cAlerta.getTime());
      newInput.setFechaLimite(cAlerta.getTime());
      newInput.setFechaCreacion(cAlerta.getTime());
    } else {
      newInput.setFechaAlerta(new Date());
      newInput.setFechaLimite(new Date());
      newInput.setFechaCreacion(new Date());
    }

    newInput.setNivel(8); // Nivel de bienPosesionNegociada
    newInput.setNivelId(bpn.getId());

    newInput.setDestinatario(user.getId());

    newInput.setTitulo("Visita " + visita.getId());
    // newInput.setDescripcion("Visita " + visita.getId() + "al bien " + bpn.getId());
    newInput.setComentariosDestinatario(visita.getNotas());

    create(newInput, user);
  }

  @Override
  public void crearEventoVisitaRA(Integer idExpediente, VisitaRA visita, Usuario creador)
    throws NotFoundException, RequiredValueException {
    Bien bien = visita.getBien();
    Expediente expediente = expedienteRepository.findById(idExpediente).orElse(null);
    if(expediente==null) {
      throw new NotFoundException("Expediente no encontrado");
    }
    Cartera c = expediente.getCartera();
    EventoInputDTO newInput = new EventoInputDTO();

    Usuario user =
      usuarioRepository
        .findById(creador.getId())
        .orElseThrow(() -> new NotFoundException("usuario", creador.getId()));

    SubtipoEvento se =
      subtipoEventoRepository
        .findByCodigoAndCarteraId("VIS01", c.getId())
        .orElseThrow(
          () ->
            new NotFoundException(
              "subtipoEvento", "código", "'VIS01'", "cartera", c.getNombre()));
    TipoEvento te = se.getTipoEvento();
    ClaseEvento ce = te.getClaseEvento();

    newInput.setSubtipoEvento(se.getId());
    newInput.setTipoEvento(te.getId());
    newInput.setClaseEvento(ce.getId());
    newInput.setIdExpediente(expediente.getId());

    newInput.setImportancia(2); // Media importancia
    // Revisamos el estado evento pendiente
    EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("REA").orElse(null);
    if (estadoEvento == null) throw new NotFoundException("estado evento", "código", "'REA'");
    newInput.setEstado(estadoEvento.getId());
    newInput.setResultado(null);

    if (visita.getFecha() != null) {
      Calendar cAlerta = Calendar.getInstance();
      cAlerta.setTime(visita.getFecha());
      newInput.setFechaAlerta(cAlerta.getTime());
      newInput.setFechaLimite(cAlerta.getTime());
    } else {
      newInput.setFechaAlerta(new Date());
      newInput.setFechaLimite(new Date());
    }

    newInput.setNivel(3); // Nivel de bien
    newInput.setNivelId(bien.getId());
    newInput.setFechaCreacion(new Date());

    newInput.setDestinatario(user.getId());

    newInput.setTitulo("Visita " + visita.getId());
    // newInput.setDescripcion("Visita " + visita.getId() + "al bien " + bpn.getId());
    newInput.setComentariosDestinatario(visita.getNotas());

    create(newInput, user);
  }

  @Override
  @Transactional
  public void createEventoUtilidades(
      EventoInputDTO input,
      List<Integer> expedienteIdList,
      List<Integer> expedientePNIdList,
      boolean todos,
      boolean todosPN,
      BusquedaDto busqueda,
      Usuario logUser)
      throws NotFoundException, RequiredValueException {

    List<Expediente> expedientesLista =
        obtenerExpedientes(expedienteIdList, todos, busqueda, logUser);
    List<ExpedientePosesionNegociada> expedientesPNLista =
        obtenerExpedientesPN(expedientePNIdList, todosPN, busqueda, logUser);
    SubtipoEvento se =
        subtipoEventoRepository
            .findById(input.getSubtipoEvento())
            .orElseThrow(() -> new NotFoundException("subtipoEvento", input.getSubtipoEvento()));

    input.setNivel(1);
    for (Expediente e : expedientesLista) {
      Usuario usuario = e.getGestor();
      Cartera c = e.getCartera();
      SubtipoEvento newSe =
          subtipoEventoRepository
              .findByCodigoAndCarteraId(se.getCodigo(), c.getId())
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "subtipoEvento", "código", se.getCodigo(), "cartera", c.getNombre()));
      input.setSubtipoEvento(newSe.getId());
      if (usuario != null) {
        input.setDestinatario(usuario.getId());
        input.setCartera(e.getCartera().getId());
        input.setNivelId(e.getId());
        eventoRepository.save(eventoMapper.createEvento(input, logUser));
      } else {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException(
              "The record of id: "
                  + (e.getIdConcatenado() != null
                      ? e.getIdConcatenado()
                      : e.getId() + " (does not have concatenated)")
                  + " has no handler to send the event to.");
        else
          throw new NotFoundException(
              "El expediente de id: "
                  + (e.getIdConcatenado() != null
                      ? e.getIdConcatenado()
                      : e.getId() + " (no tiene concatenado)")
                  + " no tiene gestor al que enviarle el evento.");
      }
    }

    input.setNivel(7);
    for (ExpedientePosesionNegociada e : expedientesPNLista) {
      Usuario usuario = e.getGestor();
      Cartera c = e.getCartera();
      SubtipoEvento newSe =
          subtipoEventoRepository
              .findByCodigoAndCarteraId(se.getCodigo(), c.getId())
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "subtipoEvento", "código", se.getCodigo(), "cartera", c.getNombre()));
      input.setSubtipoEvento(newSe.getId());
      if (usuario != null) {
        input.setDestinatario(usuario.getId());
        input.setCartera(e.getCartera().getId());
        input.setNivelId(e.getId());
        eventoRepository.save(eventoMapper.createEvento(input, logUser));
      } else {
        Locale loc = LocaleContextHolder.getLocale();
        if (loc.getLanguage() == null || loc.getLanguage().equals("en"))
          throw new NotFoundException(
              "The filePosesionNegociada de id: "
                  + (e.getIdConcatenado() != null
                      ? e.getIdConcatenado()
                      : e.getId() + " (does not have concatenated)")
                  + " does not have a manager to send the event to.");
        else
          throw new NotFoundException(
              "El expedientePosesionNegociada de id: "
                  + (e.getIdConcatenado() != null
                      ? e.getIdConcatenado()
                      : e.getId() + " (no tiene concatenado)")
                  + " no tiene gestor al que enviarle el evento.");
      }
    }
  }

  public void crearEventoPago(Contrato contrato, Usuario creador)
      throws NotFoundException, RequiredValueException {
    // Obtener el Expediente y cartera a raíz del contrato
    Integer idExpediente = contrato.getExpediente().getId();
    Cartera c = contrato.getExpediente().getCartera();

    EventoInputDTO newInput = new EventoInputDTO();

    Usuario user =
        usuarioRepository
            .findById(creador.getId())
            .orElseThrow(
                () -> new NotFoundException("No existe un usuario de id: " + creador.getId()));

    SubtipoEvento se =
        subtipoEventoRepository
            .findByCodigoAndCarteraId("PAGO01", c.getId())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "No existe el subtipoEvento con código: 'PAGO01' para la cartera "
                            + c.getNombre()));
    TipoEvento te = se.getTipoEvento();
    ClaseEvento ce = te.getClaseEvento();

    newInput.setSubtipoEvento(se.getId());
    newInput.setTipoEvento(te.getId());
    newInput.setClaseEvento(ce.getId());
    newInput.setIdExpediente(idExpediente);

    newInput.setImportancia(2); // Media importancia

    // Revisamos el estado evento pendiente
    EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("PEN").orElse(null);
    if (estadoEvento == null) {
      throw new NotFoundException("No existe el estado evento con código: PEN");
    }
    newInput.setEstado(estadoEvento.getId());
    newInput.setResultado(null);

    Calendar cAlerta = Calendar.getInstance();
    cAlerta.setTime(new Date());
    newInput.setFechaAlerta(new Date());
    newInput.setFechaLimite(new Date());
    newInput.setFechaCreacion(new Date());

    newInput.setNivel(2);
    newInput.setNivelId(contrato.getId());

    newInput.setDestinatario(user.getId());

    newInput.setTitulo("Pago referente al contrato " + contrato.getId());
    newInput.setComentariosDestinatario("Efectuado un Pago");

    create(newInput, user);
  }

  private List<Expediente> obtenerExpedientes(
      List<Integer> expedienteIdList, boolean todos, BusquedaDto busqueda, Usuario usuario)
      throws NotFoundException {
    List<Expediente> expedientesLista = new ArrayList<>();
    if (todos) {
      expedientesLista = busquedaUseCase.getDataExpedientesFilter(busqueda, usuario, null, null);
    } else if (expedienteIdList != null && !expedienteIdList.isEmpty()) {
      expedientesLista = expedienteRepository.findAllById(expedienteIdList);
    }
    return expedientesLista;
  }

  private List<ExpedientePosesionNegociada> obtenerExpedientesPN(
      List<Integer> expedientePNIdList, boolean todosPN, BusquedaDto busqueda, Usuario usuario)
      throws NotFoundException {
    List<ExpedientePosesionNegociada> expedientesLista = new ArrayList<>();
    if (todosPN) {
      expedientesLista =
          expedientePosesionNegociadaCase.filtrarExpedientesPosesionNegociadaBusqueda(
              busqueda, usuario, null, null, null, null, null, null, null, null, null, null, null,
              null, null);
    } else if (expedientePNIdList != null && !expedientePNIdList.isEmpty()) {
      expedientesLista = expedientePosesionNegociadaRepository.findAllById(expedientePNIdList);
    }
    return expedientesLista;
  }

  @Override
  public EventoOutputDTO getLastWF(Integer idPropuesta) throws NotFoundException {
    Integer idEvento = this.eventoFromPropuesta(idPropuesta);
    if (idEvento == null) return null;
    return this.findEventoById(idEvento);
  }

  @Override
  public Boolean eventoWFJudicial(
      Integer idContrato, Integer idInterviniente, Date fechaPeticion, Usuario logged)
      throws NotFoundException, RequiredValueException {
    Usuario creador = usuarioRepository.findById(logged.getId()).orElse(null);
    Contrato contrato = contratoRepository.findById(idContrato).orElse(null);
    EventoInputDTO newIn = new EventoInputDTO();

    Expediente e = contrato.getExpediente();
    Cartera ca = e.getCartera();

    Interviniente interviniente =
        intervinienteRepository
            .findById(idInterviniente)
            .orElseThrow(() -> new NotFoundException("interviniente", idInterviniente));
    SubtipoEvento se =
        subtipoEventoRepository
            .findByCodigoAndCarteraId("JU_PR", ca.getId())
            .orElseThrow(() -> new NotFoundException("subtipoEvento", "código", "'JU_PR'"));
    ImportanciaEvento ie =
        importanciaEventoRepository
            .findByCodigo("ALT")
            .orElseThrow(() -> new NotFoundException("importanciaEvento", "código", "'ALT'"));
    TipoEvento te = se.getTipoEvento();
    ClaseEvento ce = te.getClaseEvento();

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    newIn.setTitulo("Judicialización del contrato: " + contrato.getIdCarga());
    String comentarios = "Tipo de Procedimiento: PCO";
    comentarios =
        comentarios.concat(
            "\nInterviniente: "
                + interviniente.getNumeroDocumento()
                + " "
                + interviniente.getNombreReal());
    comentarios = comentarios.concat("\nFecha Peticion: " + sdf.format(fechaPeticion));
    newIn.setComentariosDestinatario(comentarios);

    Usuario dest;
    dest = e.getUsuarioByPerfil("Supervisor");
    if (dest == null) throw new NotFoundException("Supervisor", "expediente", e.getId());

    NivelEvento ne =
        nivelEventoRepository
            .findByCodigo("JUD")
            .orElseThrow(() -> new NotFoundException("Nivel Evento", "código", "'JUD'"));

    newIn.setCartera(ca.getId());

    newIn.setFechaCreacion(new Date());
    newIn.setFechaAlerta(new Date());
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, 1);
    newIn.setFechaLimite(calendar.getTime());

    newIn.setClaseEvento(ce.getId());
    newIn.setTipoEvento(te.getId());
    newIn.setSubtipoEvento(se.getId());

    newIn.setDestinatario(dest.getId());
    newIn.setImportancia(ie.getId());
    newIn.setAutogenerado(true);

    newIn.setNivel(ne.getId());
    newIn.setNivelId(contrato.getId());
    newIn.setIdExpediente(e.getId());

    this.create(newIn, creador);

    return true;
  }

  @Override
  public void crearEventoCierrePBC(Integer idPropuesta, Usuario creador)
      throws NotFoundException, RequiredValueException {
    Propuesta propuesta =
        propuestaRepository
            .findById(idPropuesta)
            .orElseThrow(() -> new NotFoundException("Propuesta", idPropuesta));

    Usuario user =
        usuarioRepository
            .findById(creador.getId())
            .orElseThrow(
                () -> new NotFoundException("No existe un usuario de id: " + creador.getId()));

    Cartera c = new Cartera();

    EventoInputDTO newInput = new EventoInputDTO();

    Integer idExpediente = 0;
    if (propuesta.getExpediente() != null) {
      Expediente ex = propuesta.getExpediente();
      c = ex.getCartera();
      idExpediente = ex.getId();
    } else if (propuesta.getExpedientePosesionNegociada() != null) {
      ExpedientePosesionNegociada ex = propuesta.getExpedientePosesionNegociada();
      c = ex.getCartera();
      idExpediente = ex.getId();
    } else {
      throw new NotFoundException("Propuesta", "Expediente");
    }

    newInput.setIdExpediente(idExpediente);

    Cartera finalC = c;
    SubtipoEvento se =
        subtipoEventoRepository
            .findByCodigoAndCarteraId("R_PBC", c.getId())
            .orElseThrow(
                () ->
                    new NotFoundException(
                        "No existe el subtipoEvento con código: 'R_PBC' para la cartera "
                            + finalC.getNombre()));

    TipoEvento te = se.getTipoEvento();
    ClaseEvento ce = te.getClaseEvento();

    newInput.setSubtipoEvento(se.getId());
    newInput.setTipoEvento(te.getId());
    newInput.setClaseEvento(ce.getId());
    newInput.setIdExpediente(idExpediente);

    newInput.setImportancia(3); // Alta importancia

    EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("PEN").orElse(null);
    if (estadoEvento == null) {
      throw new NotFoundException("Evento", "Codigo", "PEN");
    }
    newInput.setEstado(estadoEvento.getId());
    newInput.setResultado(null);

    Calendar cAlerta = Calendar.getInstance();
    cAlerta.setTime(new Date());
    newInput.setFechaAlerta(new Date());
    newInput.setFechaLimite(new Date());
    newInput.setFechaCreacion(new Date());

    newInput.setNivel(6);
    newInput.setNivelId(idPropuesta);

    EventoOutputDTO lastWF = this.getLastWF(idPropuesta);
    if (lastWF == null) throw new NotFoundException("Evento WorkFlow", "Propuesta", idPropuesta);
    Integer idUsuario = lastWF.getDestinatario().getId();
    newInput.setDestinatario(idUsuario);

    newInput.setTitulo("PBC");
    newInput.setComentariosDestinatario(
        "Recibida respuesta de PBC para la oferta: " + propuesta.getId());

    create(newInput, user);
  }

  private void alertaReasignacionExpediente(
      Integer expId, Boolean pn, Usuario creador, Integer idDestinatario, String descripcion)
      throws NotFoundException, RequiredValueException {
    EventoInputDTO newInput = new EventoInputDTO();
    if (expId != null) {
      Cartera c = new Cartera();
      if (pn){
        ExpedientePosesionNegociada expediente =
          expedientePosesionNegociadaRepository
            .findById(expId)
            .orElseThrow(() -> new NotFoundException("Expediente Posesión Negociada", expId));

        c = expediente.getCartera();

        newInput.setNivel(7);
      } else {
        Expediente expediente =
          expedienteRepository
            .findById(expId)
            .orElseThrow(() -> new NotFoundException("Expediente", expId));

        c = expediente.getCartera();

        newInput.setNivel(1);
      }

      newInput.setIdExpediente(expId);
      newInput.setCartera(c.getId());

      newInput.setNivelId(expId);

      Cartera finalC = c;
      SubtipoEvento se =
          subtipoEventoRepository
              .findByCodigoAndCarteraId("R_PER", c.getId())
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "No existe el subtipoEvento con código: 'R_PER' para la cartera "
                              + finalC.getNombre()));

      TipoEvento te = se.getTipoEvento();
      ClaseEvento ce = te.getClaseEvento();

      newInput.setSubtipoEvento(se.getId());
      newInput.setTipoEvento(te.getId());
      newInput.setClaseEvento(ce.getId());
    } else {
      NivelEvento nivelEvento =
          nivelEventoRepository
              .findByCodigo("GEN")
              .orElseThrow(
                  () ->
                      new NotFoundException("Nivel Evento", "Codigo", "GEN"));

      newInput.setNivel(nivelEvento.getId());
      newInput.setNivelId(0);

      SubtipoEvento se =
          subtipoEventoRepository
              .findByCodigoAndCarteraIsNull("R_PER")
              .orElseThrow(
                  () ->
                      new NotFoundException(
                          "No existe el subtipoEvento con código: 'R_PER' para la agenda general"));

      TipoEvento te = se.getTipoEvento();
      ClaseEvento ce = te.getClaseEvento();

      newInput.setSubtipoEvento(se.getId());
      newInput.setTipoEvento(te.getId());
      newInput.setClaseEvento(ce.getId());
    }

    Usuario user =
        usuarioRepository
            .findById(creador.getId())
            .orElseThrow(
                () -> new NotFoundException("No existe un usuario de id: " + creador.getId()));

    newInput.setImportancia(3); // Alta importancia

    EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("PEN").orElse(null);
    if (estadoEvento == null) {
      throw new NotFoundException("Evento", "Codigo", "PEN");
    }
    newInput.setEstado(estadoEvento.getId());
    newInput.setResultado(null);

    Calendar cAlerta = Calendar.getInstance();
    cAlerta.setTime(new Date());
    newInput.setFechaAlerta(new Date());
    newInput.setFechaLimite(new Date());
    newInput.setFechaCreacion(new Date());

    newInput.setDestinatario(idDestinatario);

    newInput.setTitulo("Asignación de Usuario");
    newInput.setComentariosDestinatario(descripcion);

    create(newInput, user);
  }

  public void alertaReasignaciones(List<Integer> idsRA, List<Integer> idsPN, Usuario creador, Integer idDestinatario) throws NotFoundException, RequiredValueException {
    Integer cantidad = 0;
    cantidad += idsRA.size();
    cantidad += idsPN.size();
    if (cantidad > 5){
      String ra = "";
      String pn = "";
      if (!idsRA.isEmpty()){
        List<Expediente> expedientes = expedienteRepository.findByIdIn(idsRA);
        ra = expedientes.stream().map(Expediente::getIdConcatenado).collect(Collectors.joining(", "));
      }
      if (!idsPN.isEmpty()){
        List<ExpedientePosesionNegociada> expedientes = expedientePosesionNegociadaRepository.findByIdIn(idsPN);
        pn = expedientes.stream().map(ExpedientePosesionNegociada::getIdConcatenado).collect(Collectors.joining(", "));
      }

      String descripcion = "Se le han reasignado los siguientes expedientes: \n";
      if (!ra.equals("")) descripcion += "Recuperación Amistosa: " + ra + ". \n";
      if (!pn.equals("")) descripcion += "Posesión Negociada: " + pn + ". \n";

      alertaReasignacionExpediente(null, null, creador, idDestinatario, descripcion);
    } else {
      for (Integer idRA : idsRA){
        Expediente exRA = expedienteRepository
          .findById(idRA)
          .orElseThrow(
            () -> new NotFoundException("Expediente", idRA));
        String descripcion = "Se le ha reasignado el siguiente expediente de Recuperación Amistosa: " + exRA.getIdConcatenado();
        alertaReasignacionExpediente(idRA, false, creador, idDestinatario, descripcion);
      }
      for (Integer idPN : idsPN){
        ExpedientePosesionNegociada exRA = expedientePosesionNegociadaRepository
          .findById(idPN)
          .orElseThrow(
            () -> new NotFoundException("Expediente Posesión Negociada", idPN));
        String descripcion = "Se le ha reasignado el siguiente expediente de Posesión Negociada: " + exRA.getIdConcatenado();
        alertaReasignacionExpediente(idPN, true, creador, idDestinatario, descripcion);
      }
    }
  }

  public void alertaFormalizacionPorPerfil (
    Propuesta propuesta, Usuario creador, String perfil, String titulo, String descripcion) throws NotFoundException, RequiredValueException {
    Usuario dest = null;
    if (propuesta.getExpediente() != null) {
      Expediente ex = propuesta.getExpediente();
      dest = ex.getUsuarioByPerfil(perfil);
    } else if (propuesta.getExpedientePosesionNegociada() != null) {
      ExpedientePosesionNegociada ex = propuesta.getExpedientePosesionNegociada();
      dest = ex.getUsuarioByPerfil(perfil);
    } else {
      throw new NotFoundException("Propuesta", "Expediente");
    }
    if (dest != null)
      alertaFormalizacion(propuesta, creador, dest.getId(), titulo, descripcion);
  }

  public void alertaPteFirma(Propuesta p, Usuario creador) throws NotFoundException, RequiredValueException {
    Integer dest = null;
    if (p.getExpediente() != null){
      Expediente ex = p.getExpediente();
      Usuario des = ex.getUsuarioByPerfil("Gestor formalización");
      if (des == null){
        Cartera c = ex.getCartera();
        if (c.getFormalizacion())
          des = c.getResponsableFormalizacion();
      }
      if (des != null) dest = des.getId();
    } else if (p.getExpedientePosesionNegociada() != null){
      ExpedientePosesionNegociada ex = p.getExpedientePosesionNegociada();
      Usuario des = ex.getUsuarioByPerfil("Gestor formalización");
      if (des == null){
        Cartera c = ex.getCartera();
        if (c.getFormalizacion())
          des = c.getResponsableFormalizacion();
      }
      if (des != null) dest = des.getId();
    }

    if (dest != null){
      String titulo = "Alerta Pendiente Firma";
      String descripcion = "La propuesta está en estado pendiente firma.";
      alertaFormalizacion(p, creador, dest, titulo, descripcion);
    }
  }

  public void alertaFormalizacion (
    Propuesta propuesta, Usuario creador, Integer idDestinatario, String titulo, String descripcion)
    throws NotFoundException, RequiredValueException {
    EventoInputDTO newInput = new EventoInputDTO();

    Cartera c = new Cartera();

    Integer idExpediente = 0;
    if (propuesta.getExpediente() != null) {
      Expediente ex = propuesta.getExpediente();
      c = ex.getCartera();
      idExpediente = ex.getId();
    } else if (propuesta.getExpedientePosesionNegociada() != null) {
      ExpedientePosesionNegociada ex = propuesta.getExpedientePosesionNegociada();
      c = ex.getCartera();
      idExpediente = ex.getId();
    } else {
      throw new NotFoundException("Propuesta", "Expediente");
    }

    newInput.setIdExpediente(idExpediente);
    newInput.setCartera(c.getId());

    newInput.setNivelId(propuesta.getId());
    newInput.setNivel(6);

    Cartera finalC = c;

    SubtipoEvento se =
      subtipoEventoRepository
        .findByCodigoAndCarteraId("A_FORM", c.getId())
        .orElseThrow(
          () ->
            new NotFoundException(
              "No existe el subtipoEvento con código: 'A_FORM' para la cartera "
                + finalC.getNombre()));

    TipoEvento te = se.getTipoEvento();
    ClaseEvento ce = te.getClaseEvento();

    newInput.setSubtipoEvento(se.getId());
    newInput.setTipoEvento(te.getId());
    newInput.setClaseEvento(ce.getId());

    Usuario user = null;
    if (creador != null)
      user =
          usuarioRepository
              .findById(creador.getId())
              .orElseThrow(
                  () -> new NotFoundException("No existe un usuario de id: " + creador.getId()));

    newInput.setImportancia(2); // MEdia importancia

    EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("PEN").orElse(null);
    if (estadoEvento == null) {
      throw new NotFoundException("Evento", "Codigo", "PEN");
    }
    newInput.setEstado(estadoEvento.getId());
    newInput.setResultado(null);

    Calendar cAlerta = Calendar.getInstance();
    cAlerta.setTime(new Date());
    newInput.setFechaCreacion(new Date());
    cAlerta.add(Calendar.HOUR, 1);
    newInput.setFechaAlerta(new Date());
    cAlerta.add(Calendar.DATE, 7);
    newInput.setFechaLimite(cAlerta.getTime());

    newInput.setDestinatario(idDestinatario);

    newInput.setTitulo(titulo);
    newInput.setComentariosDestinatario(descripcion);

    create(newInput, user);
  }

  public void llamadaAbierta (
    Integer idExpediente, Integer idInterviniente, String telefono, Boolean pn, Usuario creador)
    throws NotFoundException, RequiredValueException {
    EventoInputDTO newInput = new EventoInputDTO();

    Cartera c = new Cartera();

    String nombre = "";

    if (pn != null && pn){
      ExpedientePosesionNegociada ex = expedientePosesionNegociadaRepository
        .findById(idExpediente)
        .orElseThrow(
          () -> new NotFoundException("Expediente Posesión Negociada", idExpediente));
      c = ex.getCartera();
      NivelEvento ne = nivelEventoRepository.findByCodigo("BIEN_PN").orElseThrow(
        () -> new NotFoundException("NivelEvento", "Codigo", "BIEN_PN"));
      newInput.setNivel(ne.getId());
      Ocupante oc = ocupanteRepository
        .findById(idInterviniente)
        .orElseThrow(
          () -> new NotFoundException("Ocupante", idInterviniente));
      nombre = oc.getNombreCompleto();
    } else {
      Expediente ex = expedienteRepository
        .findById(idExpediente)
        .orElseThrow(
          () -> new NotFoundException("Expediente", idExpediente));
      c = ex.getCartera();
      NivelEvento ne = nivelEventoRepository.findByCodigo("BIEN").orElseThrow(
        () -> new NotFoundException("NivelEvento", "Codigo", "BIEN"));
      newInput.setNivel(ne.getId());
      Interviniente oc = intervinienteRepository
        .findById(idInterviniente)
        .orElseThrow(
          () -> new NotFoundException("Interviniente", idInterviniente));
      nombre = oc.getNombreReal();
    }

    newInput.setIdExpediente(idExpediente);
    newInput.setCartera(c.getId());

    newInput.setNivelId(idInterviniente);

    Cartera finalC = c;

    SubtipoEvento se =
      subtipoEventoRepository
        .findByCodigoAndCarteraId("LLAM01", c.getId())
        .orElseThrow(
          () ->
            new NotFoundException(
              "No existe el subtipoEvento con código: 'A_FORM' para la cartera "
                + finalC.getNombre()));

    TipoEvento te = se.getTipoEvento();
    ClaseEvento ce = te.getClaseEvento();

    newInput.setSubtipoEvento(se.getId());
    newInput.setTipoEvento(te.getId());
    newInput.setClaseEvento(ce.getId());

    Usuario user = null;
    if (creador != null)
      user =
          usuarioRepository
              .findById(creador.getId())
              .orElseThrow(
                  () -> new NotFoundException("No existe un usuario de id: " + creador.getId()));

    newInput.setImportancia(3); // Alta importancia

    EstadoEvento estadoEvento = estadoEventoRepository.findByCodigo("PEN").orElse(null);
    if (estadoEvento == null) {
      throw new NotFoundException("Evento", "Codigo", "PEN");
    }
    newInput.setEstado(estadoEvento.getId());
    newInput.setResultado(null);

    Calendar cAlerta = Calendar.getInstance();
    cAlerta.setTime(new Date());
    cAlerta.add(Calendar.DATE, 7);
    newInput.setFechaAlerta(new Date());
    newInput.setFechaLimite(cAlerta.getTime());
    newInput.setFechaCreacion(new Date());

    newInput.setDestinatario(creador.getId());

    newInput.setTitulo("Llamada a " + nombre);
    String descripcion = "Se ha realizado una llamada al número: " + telefono;
    newInput.setComentariosDestinatario(descripcion);

    create(newInput, user);
  }
}
