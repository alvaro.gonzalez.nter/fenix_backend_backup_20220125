package com.haya.alaska.evento.domain;

import com.haya.alaska.cartera.domain.Cartera;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.estado_evento.domain.EstadoEvento;
import com.haya.alaska.importancia_evento.domain.ImportanciaEvento;
import com.haya.alaska.nivel_evento.domain.NivelEvento;
import com.haya.alaska.periodicidad_evento.domain.PeriodicidadEvento;
import com.haya.alaska.resultado_evento.domain.ResultadoEvento;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Audited
@AuditTable(value = "HIST_MSTR_EVENTO")
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "MSTR_EVENTO")
public class Evento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @ToString.Include
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "ID_CARTERA", referencedColumnName = "ID")
    private Cartera cartera;

    @OneToOne
    @JoinColumn(name = "ID_EVENTO_PADRE")
    private Evento eventoPadre;

    @Column(name = "DES_TITULO", columnDefinition = "varchar(280)", nullable = false)
    private String titulo;  // En el alta

    @Lob
    @Column(name = "DES_COMENTARIOS_DESTINATARIO")
    private String comentariosDestinatario;  // Detalle Agenda

    @Column( name = "FCH_FECHA_LIMITE")
    private Date fechaLimite;
    @Column( name = "FCH_FECHA_CREACION", nullable = false)
    private Date fechaCreacion;  // Alta
    @Column( name = "FCH_FECHA_ALERTA", nullable = false)
    private Date fechaAlerta;  //

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
  @Fetch(FetchMode.SELECT)
  @LazyToOne(LazyToOneOption.NO_PROXY)
  @JoinColumn(name = "ID_CLASE_EVENTO", referencedColumnName = "ID", nullable = false)
    private ClaseEvento claseEvento;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
  @Fetch(FetchMode.SELECT)
  @LazyToOne(LazyToOneOption.NO_PROXY)
  @JoinColumn(name = "ID_TIPO_EVENTO", referencedColumnName = "ID", nullable = false)
    private TipoEvento tipoEvento;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
  @Fetch(FetchMode.SELECT)
  @LazyToOne(LazyToOneOption.NO_PROXY)
  @JoinColumn(name = "ID_SUBTIPO_EVENTO", referencedColumnName = "ID", nullable = false)
    private SubtipoEvento subtipoEvento;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
  @Fetch(FetchMode.SELECT)
  @LazyToOne(LazyToOneOption.NO_PROXY)
  @JoinColumn(name = "ID_PERIODICIDAD_EVENTO", referencedColumnName = "ID")
    private PeriodicidadEvento periodicidad;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
  @Fetch(FetchMode.SELECT)
  @LazyToOne(LazyToOneOption.NO_PROXY)
  @JoinColumn(name = "ID_ESTADO_EVENTO", referencedColumnName = "ID", nullable = false)
    private EstadoEvento estado;  // Vista Agenda

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
  @Fetch(FetchMode.SELECT)
  @LazyToOne(LazyToOneOption.NO_PROXY)
  @JoinColumn(name = "ID_IMPORTANCIA_EVENTO", referencedColumnName = "ID", nullable = false)
    private ImportanciaEvento importancia; //A veces llamado prioridad  //

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
  @Fetch(FetchMode.SELECT)
  @LazyToOne(LazyToOneOption.NO_PROXY)
  @JoinColumn(name = "ID_RESULTADO_EVENTO", referencedColumnName = "ID")
    private ResultadoEvento resultado;

    @ManyToOne
    @Fetch(FetchMode.SELECT)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "ID_EMISOR", referencedColumnName = "ID")
    private Usuario emisor; //A veces llamado gestor

  @ManyToOne
  @Fetch(FetchMode.SELECT)
  @LazyToOne(LazyToOneOption.NO_PROXY)
  @JoinColumn(name = "ID_DESTINATARIO", referencedColumnName = "ID", nullable = false)
  private Usuario destinatario;

  @Column(name = "IND_PROGRAMACION", columnDefinition = "tinyint(1)")
  private Boolean programacion;
  @Column(name = "IND_CORREO", columnDefinition = "tinyint(1)")
  private Boolean correo;
  @Column(name = "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;
  @Column(name = "IND_REVISADO", columnDefinition = "tinyint(1)")
  private Boolean revisado = false;
  @Column(name = "IND_AUTOGENERADO", columnDefinition = "tinyint(1)")
  private Boolean autogenerado;

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
  @Fetch(FetchMode.SELECT)
  @JoinColumn(name = "ID_NIVEL_EVENTO", referencedColumnName = "ID", nullable = false)
  private NivelEvento nivel;  // +-

  @Column(name = "NUM_ID_OBJETO_NIVEL", nullable = false)
  private Integer idNivel;


  @Column(name = "NUM_ID_RECOVERY")
  private Long idRecovery;

  @Column(name = "NUM_ID_EXPEDIENTE")
  private Integer idExpediente;
}
