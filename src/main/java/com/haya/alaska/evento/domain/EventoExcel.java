package com.haya.alaska.evento.domain;

import com.haya.alaska.shared.util.ExcelUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Collection;
import java.util.LinkedHashSet;

public class EventoExcel extends ExcelUtils {

  public static final LinkedHashSet<String> cabeceras = new LinkedHashSet<>();

  static {
    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      cabeceras.add("Connection Id");
      cabeceras.add("Event Id");
      cabeceras.add("Event");
      cabeceras.add("Event Type");
      cabeceras.add("Event Subtype");
      cabeceras.add("Level");
      cabeceras.add("Importance");
      cabeceras.add("Status");
      cabeceras.add("Issuer");
      cabeceras.add("Receiver");
      cabeceras.add("Creation Date");
      cabeceras.add("Deadline");
      cabeceras.add("Result");
      cabeceras.add("Issuer Comment");
      cabeceras.add("Receiver Comment");
      cabeceras.add("Date");
    } else {
      cabeceras.add("Id Expediente");
      cabeceras.add("Id Evento");
      cabeceras.add("Evento");
      cabeceras.add("Tipo Evento");
      cabeceras.add("Subtipo Evento");
      cabeceras.add("Nivel");
      cabeceras.add("Importancia");
      cabeceras.add("Estado");
      cabeceras.add("Emisor");
      cabeceras.add("Destinatario");
      cabeceras.add("Fecha Creacion");
      cabeceras.add("Fecha Limite");
      cabeceras.add("Resultado");
      cabeceras.add("Comentario Emisor");
      cabeceras.add("Comentario Destinatario");
      cabeceras.add("Fecha");
    }
  }

  public EventoExcel(Evento evento, String idExpediente) {
    super();

    if (LocaleContextHolder.getLocale().getLanguage() == null
        || LocaleContextHolder.getLocale().getLanguage().equals("en")) {
      this.add("Connection Id", idExpediente);
      this.add("Event Id", evento.getId());
      this.add(
          "Event",
          evento.getClaseEvento() != null ? evento.getClaseEvento().getValorIngles() : null);
      this.add(
          "Event Type",
          evento.getTipoEvento() != null ? evento.getTipoEvento().getValorIngles() : null);
      this.add(
          "Event Subtype",
          evento.getSubtipoEvento() != null ? evento.getSubtipoEvento().getValorIngles() : null);
      this.add("Level", evento.getNivel() != null ? evento.getNivel().getValorIngles() : null);
      this.add(
          "Importance",
          evento.getImportancia() != null ? evento.getImportancia().getValorIngles() : null);
      this.add("Status", evento.getEstado() != null ? evento.getEstado().getValorIngles() : null);
      this.add("Issuer", evento.getEmisor() != null ? evento.getEmisor().getNombre() : null);
      this.add(
          "Receiver",
          evento.getDestinatario() != null ? evento.getDestinatario().getNombre() : null);
      this.add("Creation Date", evento.getFechaCreacion());
      this.add("Deadline", evento.getFechaLimite());
      this.add(
          "Result", evento.getResultado() != null ? evento.getResultado().getValorIngles() : null);
      this.add("Issuer Comment", evento.getComentariosDestinatario());
      this.add("Receiver Comment", evento.getComentariosDestinatario());
      this.add("Date", evento.getFechaAlerta());
    } else {
      this.add("Id Expediente", idExpediente);
      this.add("Id Evento", evento.getId());
      this.add(
          "Evento", evento.getClaseEvento() != null ? evento.getClaseEvento().getValor() : null);
      this.add(
          "Tipo Evento", evento.getTipoEvento() != null ? evento.getTipoEvento().getValor() : null);
      this.add(
          "Subtipo Evento",
          evento.getSubtipoEvento() != null ? evento.getSubtipoEvento().getValor() : null);
      this.add("Nivel", evento.getNivel() != null ? evento.getNivel().getValor() : null);
      this.add(
          "Importancia",
          evento.getImportancia() != null ? evento.getImportancia().getValor() : null);
      this.add("Estado", evento.getEstado() != null ? evento.getEstado().getValor() : null);
      this.add("Emisor", evento.getEmisor() != null ? evento.getEmisor().getNombre() : null);
      this.add(
          "Destinatario",
          evento.getDestinatario() != null ? evento.getDestinatario().getNombre() : null);
      this.add("Fecha Creacion", evento.getFechaCreacion());
      this.add("Fecha Limite", evento.getFechaLimite());
      this.add(
          "Resultado", evento.getResultado() != null ? evento.getResultado().getValor() : null);
      this.add("Comentario Emisor", evento.getComentariosDestinatario());
      this.add("Comentario Destinatario", evento.getComentariosDestinatario());
      this.add("Fecha", evento.getFechaAlerta());
    }
  }

  @Override
  public Collection<String> getCabeceras() {
    return cabeceras;
  }
}
