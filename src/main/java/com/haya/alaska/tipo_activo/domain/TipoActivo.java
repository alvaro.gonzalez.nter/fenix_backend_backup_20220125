package com.haya.alaska.tipo_activo.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_ACTIVO")
@Entity
@Table(name = "LKUP_TIPO_ACTIVO")
public class TipoActivo extends Catalogo {
  public final static String  INMOBILIARIO="1";
  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ACTIVO")
  @NotAudited
  private Set<Bien> bienes = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ACTIVO_BIEN_PN")
  @NotAudited
  private Set<Segmentacion> segmentaciones = new HashSet<>();
}
