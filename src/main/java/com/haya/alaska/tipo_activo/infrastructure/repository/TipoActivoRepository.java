package com.haya.alaska.tipo_activo.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_activo.domain.TipoActivo;

@Repository
public interface TipoActivoRepository extends CatalogoRepository<TipoActivo, Integer> {
	
}
