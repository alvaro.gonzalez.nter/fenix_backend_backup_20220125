package com.haya.alaska.area_usuario.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haya.alaska.business_plan_estrategia.domain.BusinessPlanEstrategia;
import com.haya.alaska.grupo_usuario.domain.GrupoUsuario;
import com.haya.alaska.segmentacion.domain.Segmentacion;
import com.haya.alaska.segmentacion_pn.domain.SegmentacionPN;
import com.haya.alaska.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_MSTR_AREA_USUARIO")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MSTR_AREA_USUARIO")
public class AreaUsuario {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "DES_NOMBRE")
  private String nombre;

  @ManyToMany
  @JoinTable(name = "RELA_USUARIO_AREA_USUARIO",
    joinColumns = { @JoinColumn(name = "ID_AREA_USUARIO") },
    inverseJoinColumns = { @JoinColumn(name = "ID_USUARIO") }
  )
  @JsonIgnore
  @AuditJoinTable(name = "HIST_RELA_USUARIO_AREA_USUARIO")
  private Set<Usuario> usuario;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AREA_USUARIO")
  @NotAudited
  private List<GrupoUsuario> grupos = new ArrayList<>();

  @Column(name= "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AREA_USUARIO")
  @NotAudited
  private Set<BusinessPlanEstrategia> businessPlanEstrategias = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AREA")
  @NotAudited
  private Set<Segmentacion> segmentacion = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_AREA")
  @NotAudited
  private Set<SegmentacionPN> segmentacionPN = new HashSet<>();

}
