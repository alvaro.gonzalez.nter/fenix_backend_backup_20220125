package com.haya.alaska.area_usuario.infrastructure.repository;

import com.haya.alaska.area_usuario.domain.AreaUsuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AreaUsuarioRepository extends JpaRepository<AreaUsuario, Integer> {
  Optional<AreaUsuario> findByNombre(String nombre);
  List<AreaUsuario> findByGruposId(Integer idGrupo);
  List<AreaUsuario> findByUsuarioId(Integer idUsuario);
  List<AreaUsuario> findAllDistinctByUsuarioAsignacionExpedientesExpedienteIdIn(List<Integer> id);
  List<AreaUsuario> findAllDistinctByUsuarioAsignacionExpedientesPNExpedienteIdIn(List<Integer> id);
}
