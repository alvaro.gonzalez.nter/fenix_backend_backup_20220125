package com.haya.alaska.tipo_evento.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.tipo_evento.domain.TipoEvento;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TipoEventoRepository extends CatalogoRepository<TipoEvento, Integer> {
    List<TipoEvento> findByClaseEventoIdAndActivoIsTrue(Integer id);
}
