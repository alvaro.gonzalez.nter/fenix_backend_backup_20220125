package com.haya.alaska.tipo_evento.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.clase_evento.domain.ClaseEvento;
import com.haya.alaska.evento.domain.Evento;
import com.haya.alaska.subtipo_evento.domain.SubtipoEvento;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_TIPO_EVENTO")
@Entity
@Table(name = "LKUP_TIPO_EVENTO")
@Getter
@Setter
@NoArgsConstructor
public class TipoEvento extends Catalogo {
    @ManyToOne(fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ID_CLASE_EVENTO", nullable = false)
    ClaseEvento claseEvento;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_EVENTO")
    @NotAudited
    private Set<SubtipoEvento> subtipos = new HashSet<>();

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_EVENTO")
  @NotAudited
  private Set<Evento> eventos = new HashSet<>();
}
