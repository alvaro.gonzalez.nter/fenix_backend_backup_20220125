package com.haya.alaska.subtipo_estado.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.subtipo_estado.domain.SubtipoEstado;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubtipoEstadoRepository extends CatalogoRepository<SubtipoEstado, Integer> {
  List<SubtipoEstado> findAllByEstadoIdAndActivoIsTrue(Integer id);
}
