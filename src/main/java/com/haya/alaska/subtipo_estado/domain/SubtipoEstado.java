package com.haya.alaska.subtipo_estado.domain;

import com.haya.alaska.bien_posesion_negociada.domain.BienPosesionNegociada;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.expediente.domain.Expediente;
import com.haya.alaska.expediente_posesion_negociada.domain.ExpedientePosesionNegociada;
import com.haya.alaska.tipo_estado.domain.TipoEstado;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_SUBTIPO_ESTADO")
@Entity
@Getter
@Setter
@Table(name = "LKUP_SUBTIPO_ESTADO")
public class SubtipoEstado extends Catalogo {

  private static final long serialVersionUID = 1L;

  @ManyToOne(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_ESTADO")
  private TipoEstado estado;

  @OneToMany(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_SUBESTADO")
  @NotAudited
  private Set<Expediente> expedientes = new HashSet<>();

  @OneToMany(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_SUBESTADO")
  @NotAudited
  private Set<ExpedientePosesionNegociada> expedientesPosesionNegociada = new HashSet<>();

  @OneToMany(
      fetch = FetchType.LAZY,
      cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_SUBESTADO")
  @NotAudited
  private Set<BienPosesionNegociada> bienesPosesionNegociada = new HashSet<>();
}
