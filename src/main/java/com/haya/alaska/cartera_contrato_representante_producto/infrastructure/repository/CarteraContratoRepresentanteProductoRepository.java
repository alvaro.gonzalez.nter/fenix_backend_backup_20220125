package com.haya.alaska.cartera_contrato_representante_producto.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.haya.alaska.cartera_contrato_representante_producto.domain.CarteraContratoRepresentanteProducto;

@Repository
public interface CarteraContratoRepresentanteProductoRepository
    extends JpaRepository<CarteraContratoRepresentanteProducto, Integer> {}
