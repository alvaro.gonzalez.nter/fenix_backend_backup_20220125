package com.haya.alaska.cartera_contrato_representante_producto.domain;

import com.haya.alaska.cartera_contrato_representante.domain.CarteraContratoRepresentante;
import com.haya.alaska.producto.domain.Producto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@AuditTable(value = "HIST_RELA_CARTERA_CONTRATO_REPRESENTANTE_PRODUCTO")
@Entity
@Table(name = "RELA_CARTERA_CONTRATO_REPRESENTANTE_PRODUCTO")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class CarteraContratoRepresentanteProducto implements Serializable {

  /** */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @ManyToOne
  @JoinColumn(name= "ID_CARTERA_CONTRATO_REPRESENTANTE")
  private CarteraContratoRepresentante carteraContratoRepresentante;

  @Column(name = "NUM_ORDEN")
  private Integer orden;

  @JoinColumn(name = "ID_PRODUCTO")
  @ManyToOne(fetch = FetchType.LAZY)
  private Producto producto;

  @Column(name= "IND_ACTIVO", nullable = false, columnDefinition = "boolean default true")
  private Boolean activo = true;
}
