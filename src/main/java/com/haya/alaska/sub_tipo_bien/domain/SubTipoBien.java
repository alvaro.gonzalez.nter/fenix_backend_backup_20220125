package com.haya.alaska.sub_tipo_bien.domain;

import com.haya.alaska.bien.domain.Bien;
import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.tipo_bien.domain.TipoBien;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Audited
@AuditTable(value = "HIST_LKUP_SUB_TIPO_BIEN")
@Entity
@Table(name = "LKUP_SUB_TIPO_BIEN")
@Getter
@Setter
public class SubTipoBien extends Catalogo {

  private static final long serialVersionUID = 1L;

  @OneToMany(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_SUB_TIPO_BIEN")
  @NotAudited
  private Set<Bien> bienes = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY,
    cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinColumn(name = "ID_TIPO_BIEN")
  private TipoBien tipoBien;
}
