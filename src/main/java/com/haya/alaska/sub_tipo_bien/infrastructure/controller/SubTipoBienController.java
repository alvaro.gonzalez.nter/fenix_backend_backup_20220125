package com.haya.alaska.sub_tipo_bien.infrastructure.controller;

import com.haya.alaska.catalogo.domain.Catalogo;
import com.haya.alaska.catalogo.infrastructure.controller.dto.CatalogoDTO;
import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.sub_tipo_bien.infrastructure.repository.SubTipoBienRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/subTipoBien")
public class SubTipoBienController {

  @Autowired
  SubTipoBienRepository subTipoBienRepository;

  @ApiOperation(
    value = "Subtipos de bien de un tipo de bien",
    notes = "Subtipos de bien de un tipo de bien")
  @GetMapping("{idTipoBien}")
  public List<CatalogoDTO> getAll(
    @PathVariable("idTipoBien") Integer idTipoBien,
    @RequestParam(value = "inactivos", defaultValue = "false") boolean inactivos) {

    if(inactivos) {
      return subTipoBienRepository.findByTipoBienId(idTipoBien).stream().map(CatalogoDTO::new).collect(Collectors.toList());
    } else {
      return subTipoBienRepository.findByTipoBienIdAndActivoIsTrue(idTipoBien).stream().map(CatalogoDTO::new).collect(Collectors.toList());
    }
  }
}
