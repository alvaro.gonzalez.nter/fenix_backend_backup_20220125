package com.haya.alaska.sub_tipo_bien.infrastructure.repository;

import org.springframework.stereotype.Repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.sub_tipo_bien.domain.SubTipoBien;

import java.util.List;

@Repository
public interface SubTipoBienRepository extends CatalogoRepository<SubTipoBien, Integer> {

  List<SubTipoBien> findByTipoBienId(Integer idTipoBien);
  List<SubTipoBien> findByTipoBienIdAndActivoIsTrue(Integer idTipoBien);

}
