package com.haya.alaska.estado_ddl_ddt.domain;

import com.haya.alaska.catalogo.domain.Catalogo;
import lombok.NoArgsConstructor;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@AuditTable(value = "HIST_LKUP_ESTADO_DDL")
@Entity
@Table(name = "LKUP_ESTADO_DDL")
@NoArgsConstructor
public class EstadoDdlDdt extends Catalogo {
}
