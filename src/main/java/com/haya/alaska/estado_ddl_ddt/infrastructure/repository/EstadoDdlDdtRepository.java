package com.haya.alaska.estado_ddl_ddt.infrastructure.repository;

import com.haya.alaska.catalogo.infrastructure.repository.CatalogoRepository;
import com.haya.alaska.estado_ddl_ddt.domain.EstadoDdlDdt;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoDdlDdtRepository extends CatalogoRepository<EstadoDdlDdt, Integer> {}
