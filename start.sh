#!/bin/bash

file=${1:-environment.conf}

if [ ! -f "$file" ]; then
    echo "No se ha encontrado el fichero de configuración de la aplicación: $file"
    exit 1
fi

while read line; do
    export $line
done < $file

mvn clean install -Dmaven.test.skip=true

if [ $? -ne 0 ]; then
    echo "Error de compilación. No se ejecutará la aplicación."
    exit 1
fi

if [ "$LOG_ENABLED" == "true" ]; then
    mvn spring-boot:run | tee ../log/$(date +%Y%m%d_%H%M%S)
else
    mvn spring-boot:run
fi
