# Fenix Backend

Proyecto Fenix

### Requirements

````
java 11 -> https://openjdk.java.net/projects/jdk/11/
maven -> https://maven.apache.org/
project lombok -> https://projectlombok.org/
docker-compose -> https://docs.docker.com/compose/install/
java code style -> https://github.com/google/google-java-format
mysql -> docker run --name mysql -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=fenix -p 3306:3306 -d mysql
````

### Organización del proyecto
La organización de los paquetes es la siguiente: ( _https://dzone.com/articles/project-package-organization_ )

1. Existe un paquete **Config** que contiene las clases de configuración necesarias del _proyecto_ .
2. Existe un paquete **Controller** que contiene los controladores.
3. Existe un paquete **Model** que contiene las clases que forman las  _Entidades_  , los  _DTOs_  y los  _Enums_  necesarios. Para estos 2 últimos se cuenta con un paquete específico para cada uno respectivamente (**dto**, **enums**).
4. Existe un paquete **Repository** que contiene los repositorios.
5. Existe un paquete **Service** con los servicios necesarios para las diferentes  _Funcionalidades_  del proyecto. A su vez, dentro de este se encuentran **Job**, que contiene las clases con los  _Jobs_  que se utilizan en la aplicación, y **Util** que contiene clases con funciones comunes utilizadas en diferentes  _Servicios_ .

---

#### Swagger
Herramienta que nos permite **documentar** y **testear** las llamadas a la  _API_

	Ruta Swagger: "/swagger-ui.html"
#### Perfiles
Perfiles de lanzamiento de la aplicación.

	TEST: Perfil para lanzar los tests de la aplicación.
	DEV: Perfil para Desarrollo.
	PRE: Perfil para Pre-producción.
	PRO: Perfil para Pre-producción.

#### BD
Fenix utiliza BD MySQL. Para crear una BD MySQL en local utilizaremos el siguiente comando:

	docker run --name mysql -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=fenix -p 3306:3306 -d mysql

Actualización repositorio a https://gitlab.com/hre/hre_aaff/fenix/fenix_backend.git
